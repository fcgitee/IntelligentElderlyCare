from server.pao_python.pao.data import process_db, dataframe_to_list, DataProcess
import pandas as pd
import uuid
import datetime
import re
import pymongo
import hashlib
from enum import Enum
from server.pao_python.pao.service.security.security_service import RoleService
from server.pao_python.pao.service.security.security_utility import get_current_account_id
from server.service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_cur_time, insert_many_data, date_str_to_date, get_string_time
from server.service.buss_pub.bill_manage import BillManageService, TypeId, OperationType, Status
from server.service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from server.models.financial_manage import FinancialAccount
from server.service.buss_mis.financial_account import FinancialAccountObject
from server.service.buss_pub.import_excel import ImportExcelService
from server.service.constant import AccountType, PayType, PayStatue, AccountStatus, plat_id

# import_excel_service = ImportExcelService(
#     "127.0.0.1", 27017, 'IEC', 'admin', '123456', '123456', None)

# import_excel_service.createPermission('消防设施', ['新增','查询','编辑','删除'], '8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe', '平台_超级管理员')
# import_excel_service.createPermission('消防设施', ['新增','查询','编辑','删除'], '8f4cbe36-e355-11e9-84e4-a0a4c57e9ebe', '幸福院_超级管理员')
# import_excel_service = ImportExcelService(
#     "127.0.0.1", 27017, 'IEC', 'admin', '123456', '123456', None)

# import_excel_service.update_excel_worker([])
# 866058040132221
import jsonrpcclient
import json
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'IMgHardwareService.login_mg')
# print('result>>>>', result.data.result)
result = jsonrpcclient.request(
    'http://localhost:3100/remoteCall', 'IAllowanceService.update_statistics')
print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'IArticleService.get_app_home_page_news_list', {'status': '通过'}, 1, 10)
# print('result>>>>', result.data.result)
# import uuid
# print(uuid.uuid1())
# print(uuid.uuid1())
# print(uuid.uuid1())
# print(uuid.uuid1())
# new_password = 'Zhyl@20201'
# newpassword_tep = hashlib.sha256(new_password.encode('utf-8'))
# newpassword = newpassword_tep.hexdigest()
# print('密码：', newpassword)
# print(datetime.datetime.now())

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.dishes_list')
# print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.check_sign_in')
# print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.subsidies_item')
# print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.check_bed')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_product')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_service_item')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_activity_list')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_zone_list')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_shop_list')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_org_list')
# print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_app_order')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_follow_org')
# print('result>>>>', result.data)

# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.get_bed_user', {'org_id': 'dab486e2-d08d-11e9-90bb-144f8aec0be5'})
# print('result>>>>', result.data)
# result = jsonrpcclient.request(
#     'http://localhost:3100/remoteCall', 'Test.import_old_list_del', [{'id_card_num': '440622192202022112'}])
# print('result>>>>', result.data)
