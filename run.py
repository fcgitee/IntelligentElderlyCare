from main_server import app as application

app = application.app

if __name__ == '__main__':
    application.run()
