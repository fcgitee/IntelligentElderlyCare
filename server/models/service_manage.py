'''
版权：Copyright (c) 2019 China

创建日期：Friday August 9th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Friday, 9th August 2019 9:02:00 am
修改者: ymq(ymq) - <<email>>

说明
 1、服务相关表名
'''


class ServiceItem():
    '''collection name :PT_Service_Item'''
    name = 'PT_Service_Item'


class ServiceOption():
    '''collection name :PT_Service_Option'''
    name = 'PT_Service_Option'


class ServiceOrder():
    '''collection name :PT_Service_Order'''
    name = 'PT_Service_Order'


class ServicePackage():
    '''collection name :PT_Service_Item_Package'''
    name = 'PT_Service_Item_Package'


class ServiceProvider():
    '''collection name :PT_Service_Provider_Register'''
    name = 'PT_Service_Provider_Register'


class ServiceRecord():
    '''collection name :PT_Service_Record'''
    name = 'PT_Service_Record'
