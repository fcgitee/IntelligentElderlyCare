'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-11-06 16:38:30
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\models\financial_manage.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday July 30th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 30th July 2019 5:21:15 pm
修改者: ymq(ymq) - <<email>>

说明
 1、财务管理相关表名
'''


class FinancialBook():
    '''collection name :PT_Financial_Book'''
    name = 'PT_Financial_Book'
    col_user_id = 'user_id'
    col_book_name = 'account_book_name'


class FinancialAccount():
    '''collection name :PT_Financial_Account'''
    name = 'PT_Financial_Account'


class FinancialRecord():
    '''collection name :PT_Financial_Account_Record'''
    name = 'PT_Financial_Account_Record'


class FinancialSubject():
    '''collection name :PT_Financial_Subject'''
    name = 'PT_Financial_Subject'


class FinancialVoucher():
    '''collection name :PT_Financial_Voucher'''
    name = 'PT_Financial_Voucher'


class FinancialEntry():
    '''collection name :PT_Financial_Voucher_Entry'''
    name = 'PT_Financial_Voucher_Entry'


class FinancialCharge():
    '''collection name :PT_Financial_Charge_Back'''
    name = 'PT_Financial_Charge_Back'


class FinancialSetSubjectItem():
    '''collection name :PT_Financial_Subject_Item,服务项目与会计科目对照表'''
    name = 'PT_Financial_Subject_Item'


class FinancialBill():
    '''collection name :PT_Financial_Bill,账单表'''
    name = 'PT_Financial_Bill'


class FinancialReceivableProcess():
    '''collection name :PT_Financial_Receivable_Process,收付款过程记录 '''
    name = 'PT_Financial_Receivable_Process'


class FinancialAccountRecord():
    '''collection name :PT_Financial_Account_Record,账户出入记录 '''
    name = 'PT_Financial_Account_Record'
