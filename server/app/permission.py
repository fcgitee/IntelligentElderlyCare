# -*- coding: utf-8 -*-

'''
权限定义
'''


class Permission:
    UserManage_Select = {'permission': 'UserManage_Select', 'per_name': '用户增删改查权限',
                         'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': 'default'}
    RolePermission_Select = {'permission': 'RolePermission_Select', 'per_name': '角色增删改查权限',
                             'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': 'default'}
    SecuritySettings_Select = {'permission': 'SecuritySettings_Select', 'per_name': '安全设置增删改查权限',
                               'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': 'default'}

    def permission_list(self):
        permission_list = [self.UserManage_Select, self.RolePermission_Select, self.SecuritySettings_Select]
        return permission_list
