'''
版权：Copyright (c) 2019 China

创建日期：Sunday November 10th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Sunday, 10th November 2019 5:41:35 pm
修改者: ymq(ymq) - <<email>>

说明
 1、定时任务线程
'''
import uuid
from ..pao_python.pao.server import BaseServer
import threading
from ..service.time_task.subsidy_account_manage import SubsidyElderManage
# from apscheduler.schedulers.blocking import BlockingScheduler


class TimeTaskserver(BaseServer):

    job_ids = {'subsidy_elder_task': str(uuid.uuid1())}
    control_service = []

    def __init__(self, db_addr, db_port, db_name):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name

    def start(self):
        self.scheduler.start()
        if len(self.control_service) > 0:
            for i in self.control_service:
                i.pause = self.pause
                i.resume = self.resume
                self.session = i.session

    def stop(self):
        for key, value in self.job_ids.items():
            self.scheduler.remove_job(value)
            # print("{}任务被暂停了".format(key))

    def pause(self, job_key):
        self.scheduler.pause_job(self.job_ids[job_key])
        # print("{}任务被暂停了".format(job_key))

    def resume(self, job_key):
        self.scheduler.resume_job(self.job_ids[job_key])
        # print("{}任务被恢复了".format(job_key))

    def thread_subsidy_elder_manage(self):
        subsidy_elder_service = SubsidyElderManage(db_addr=self.db_addr, db_port=self.db_port,
                                                   db_name=self.db_name, inital_password=self.inital_password, session=self.session)
        self.scheduler = BlockingScheduler()
        self.scheduler.add_job(subsidy_elder_service.total_task, 'cron',
                               month='1-12', day='last', minute=59, second=59, id=self.job_ids["subsidy_elder_task"])
