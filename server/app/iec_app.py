import base64
import datetime
import hashlib
import json
import os
import random
import string
import time
from enum import Enum
import PIL.Image as Image
import dicttoxml
import requests
import xmltodict
from flask import (Response, g, make_response, render_template, request,
                   send_file, send_from_directory, session)

from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)
from server.service.app.app_pay import FinanicalAppPay
from server.service.app.my_order import PaySessionKey, ThirdPartPayStatus
from server.service.buss_pub.bill_manage import (BillManageService,
                                                 OperationType, Status, TypeId)

from ..pao_python.pao import log
from ..pao_python.pao.app import BaseApp, JsonRpcApp
from ..pao_python.pao.service.report.report_control import \
    register as display_control_register
from ..pao_python.pao.service.security import \
    security_register as login_service
from ..register import channel_register as channel_register
from ..register import general_purpose as general_purpose
from ..register import upload_file as upload_file
from ..register import new_route as new_route
from ..register.app import announcement_register as announcement_register
from ..register.app import app_pay_register as app_pay_register
from ..register.app import comfirm_order_register as comfirm_order_register
from ..register.app import comment_register as comment_register
from ..register.app import home_page_register as home_page_register
from ..register.app import mine_home_register as mine_home_register
from ..register.app import my_account_register as my_account_register
from ..register.app import my_order_register as my_order_register
from ..register.app import news_register as news_register
from ..register.app import reservation_register as reservation_register
from ..register.app import service_detail_register as service_detail_register
from ..register.app import \
    service_package_list_register as service_package_list_register
from ..register.buss_iec import \
    assessment_project_register as assessment_project_register
from ..register.buss_iec import \
    assessment_template_register as assessment_template_register
from ..register.buss_iec import \
    competence_assessment_register as competence_assessment_register
from ..register.buss_iec import subsidy_account_recharg_register
from ..register.buss_mis import \
    activity_manage_register as activity_manage_register
from ..register.buss_mis import \
    allowance_manage_register as allowance_manage_register
from ..register.buss_mis import \
    announcement_manage_register as announcement_manage_register
from ..register.buss_mis import annoutment_register as annoutment_register
from ..register.buss_mis import \
    article_audit_register as article_audit_register
from ..register.buss_mis import article_type_register as article_type_register
from ..register.buss_mis import \
    base_situation_register as base_situation_register
from ..register.buss_mis import \
    business_area_register as business_area_register
from ..register.buss_mis import carousel_manage_register as carousel_register
from ..register.buss_mis import \
    charitable_manage_register as charitable_manage_register
from ..register.buss_mis import \
    comment_manage_register as comment_manage_register
from ..register.buss_mis import comment_type_register as comment_type_register
from ..register.buss_mis import demand_register as demand_register
from ..register.buss_mis import device_register as device_register
from ..register.buss_mis import emicall_register as emicall_register
from ..register.buss_mis import \
    financial_account_register as financial_account_register
from ..register.buss_mis import \
    financial_manage_register as financial_manage_register
from ..register.buss_mis import \
    friends_circle_register as friends_circle_register
from ..register.buss_mis import health_care_register as health_care_register
from ..register.buss_mis import monitor_register as monitor_register
from ..register.buss_mis import new_manage_register as new_manage_register
from ..register.buss_mis import \
    official_account_follow_register as official_account_follow_register
from ..register.buss_mis import \
    operation_record_register as operation_record_register
from ..register.buss_mis import \
    opinion_feedback_register as opinion_feedback_register
from ..register.buss_mis import \
    research_manage_register as research_manage_register
from ..register.buss_mis import \
    service_follow_collection_register as service_follow_collection_register
from ..register.buss_mis import \
    service_item_package_register as service_item_package_register
from ..register.buss_mis import \
    service_operation_register as service_operation_register
from ..register.buss_mis import \
    service_option_register as service_option_register
from ..register.buss_mis import \
    service_order_task_register as service_order_task_register
from ..register.buss_mis import \
    service_personal_register as service_personal_register
from ..register.buss_mis import \
    service_record_register as service_record_register
from ..register.buss_mis import \
    service_scope_register as service_scope_register
from ..register.buss_mis import \
    services_item_category_register as services_item_category_register
from ..register.buss_mis import \
    services_project_register as services_project_register
from ..register.buss_mis import \
    social_groups_type_register as social_groups_type_register
from ..register.buss_mis import task_manage_register as task_manage_register
from ..register.buss_mis import \
    transaction_management_register as transaction_management_register
from ..register.buss_mis import \
    user_relationship_register as user_relationship_register
from ..register.buss_pub import \
    app_page_config_manage_register as app_page_config_manage_register
from ..register.buss_pub import \
    materiel_manage_register as materiel_manage_register
from ..register.buss_pub import bill_manage_register as bill_manage_register
from ..register.buss_pub import excel_register as excel_register
from ..register.buss_pub import \
    message_manage_register as message_manage_register
from ..register.buss_pub import \
    person_org_manage_register as person_org_manage_register
from ..register.buss_pub import \
    personnel_organizational_register as personnel_organizational_register
from ..register.buss_pub import role_register as role_register
from ..register.buss_pub import \
    security_login_register as security_login_register
from ..register.buss_pub import \
    security_register_register as security_register_register
from ..register.buss_pub import sms_manage_register as sms_manage_register
from ..register.buss_pub import shopping_mall_manage_register as shopping_mall_manage_register
from ..register.buss_pub import ledger_account_manage_register as ledger_account_manage_register
from ..register.buss_pub import call_center_nh_register as call_center_nh_register
from ..register.buss_pub import user_func_register as user_func_register
from ..register.callcenter import \
    emi_callcenter_register as emi_callcenter_register
from ..register.callcenter import \
    knowledge_base_register as knowledge_base_register
from ..register.display import display_register_hins as display_register_hins
from ..register.display import display_register_kun as display_register_kun
from ..register.display import display_register_ny as display_register_ny
from ..register.display import display_register_ye as display_register_ye
from ..register.display import display_register_z as display_register_z
from ..register.display import display_resgister_hwg as display_register_hwg
from ..register.display import ipad_register as ipad_register
from ..register.welfare_institution import \
    accommodation_process_register as accommodation_process_register
from ..register.welfare_institution import check_in_register
from ..register.welfare_institution import \
    cost_account_register as cost_account_register
from ..register.welfare_institution import cost_manage_register
from ..register.welfare_institution import display_register as display_register
from ..register.welfare_institution import \
    hospital_register as hospital_register
from ..register.welfare_institution import \
    hotel_zone_register as hotel_zone_register
from ..register.welfare_institution import \
    hotel_zone_type_register as hotel_zone_type_register
from ..register.welfare_institution import \
    hydropower_register as hydropower_register
from ..register.welfare_institution import \
    leave_record_register as leave_record_register
from ..register.welfare_institution import \
    nursing_manage_register as nursing_manage_register
from ..register.welfare_institution import \
    report_manage_register as report_manage_register
from ..register.welfare_institution import \
    requirement_option_register as requirement_option_register
from ..register.welfare_institution import \
    requirement_project_register as requirement_project_register
from ..register.welfare_institution import \
    requirement_record_register as requirement_record_register
from ..register.welfare_institution import \
    requirement_template_register as requirement_template_register
from ..register.welfare_institution import \
    requirement_type_register as requirement_type_register
from ..register.welfare_institution import \
    reservation_registration_register as reservation_registration_register
from ..register.welfare_institution import test as test_register
from ..service.callcenter.emi_callcenter import HandleEmiCallback
from ..service.buss_mis.device import DeviceService
from ..service.common import WechatSubject, cal_sign
from ..service.mongo_bill_service import MongoBillFilter
from ..service.wx_center.xfy_follow import OfficialAccountsCallback
from ..register.buss_mis import \
    official_account_follow_register as official_account_follow_register
from ..register.buss_mis import annoutment_register as annoutment_register
from ..register.welfare_institution import other_cost_register
from .permission import Permission
from ..register.mg_hardware import \
    mg_hardware_register as mg_hardware_register
from ..register.yx_hardware import \
    yx_hardware_register as yx_hardware_register
from ..register.buss_pub import my_home_register as my_home_register
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:16:57
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\app\iec_app.py
'''
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-21 11:43:26
@LastEditTime: 2019-12-05 13:33:23
@LastEditors: Please set LastEditors
'''
# from ..register.welfare_institution import nursing_register as nursing_func_register


# from alipay.aop.api.response.AlipayTradeWapPayResponse import AlipayTradeWapPayResponse
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-06-20 17:41:58
@LastEditTime: 2019-06-20 17:41:58
@LastEditors: your name
'''
'''
版权: Copyright (c) 2019 red

文件: idmis_app.py
创建日期: Mondy March 18th 2019
作者: yangziyi
说明:
1、智慧养老系统
'''
# from ..register.welfare-institution import nursing_func_register

user_id = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'
organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'


class Sign:
    def __init__(self, jsapi_ticket, url):
        self.ret = {
            'nonceStr': self.__create_nonce_str(),
            'jsapi_ticket': jsapi_ticket,
            'timestamp': self.__create_timestamp(),
            'url': url
        }

    def __create_nonce_str(self):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(15))

    def __create_timestamp(self):
        return int(time.time())

    def sign(self):
        string = '&'.join(['%s=%s' % (key.lower(), self.ret[key])
                           for key in sorted(self.ret)])
        self.ret['signature'] = hashlib.sha1(string.encode("utf8")).hexdigest()
        return self.ret


class IECApp(JsonRpcApp, MongoService):
    '''智慧养老系统APP类'''
    permission_table = Permission()
    wechatSubject = WechatSubject()

    def __init__(self, app_name, address, port, realm_name, alipay_payment_data, wechat_payment_data, emi_callcenter_data, device_gateway_host, sms_data, web_origins, db_addr,  db_port,  db_name, db_user, db_pwd, inital_password,  web_path,  upload_file,  certificate_path=None,  private_path=None,  debug=True, redis_address=None, redis_port=None, redis_password=None):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.realm_name = realm_name
        self.alipay_payment_data = alipay_payment_data
        self.inital_password = inital_password
        self.wechat_payment_data = wechat_payment_data
        self.emi_callcenter_data = emi_callcenter_data
        self.sms_data = sms_data
        self.web_path = web_path
        self.upload_file = upload_file
        self.device_gateway_host = device_gateway_host
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, {
                "user_id": user_id,
                'organization_id': organization_id
            }
        )
        self.redis_address = redis_address
        self.redis_port = redis_port
        self.redis_password = redis_password
        JsonRpcApp.__init__(self, app_name, address, port, web_origins,
                            certificate_path, private_path, debug, redis_address, redis_port, redis_password)

    def on_route_web(self, app):
        ''' wei 路由 '''
        @app.before_request
        def before_request():
            session.modified = True

        @app.after_request
        def apply_caching(response):
            # 监听所有请求的response返回
            response.headers["Server"] = None
            return response
        # 获取大白人脸识别
        @app.route('/getBlake2b', methods=['POST', 'GET'])
        def get_blake_2b():
            data = json.loads(request.data)
            str_data = ''
            first_in = True
            str_data = 'accessToken='+data['accessToken']+'&certToken=' + \
                data['certToken']+'&timestamp='+str(data['timestamp'])
            # for k, v in data.items():
            #     if not first_in:
            #         str_data += '&' + k + '=' + str(v)
            #     else:
            #         str_data += k + '=' + str(v)
            #         first_in = False

            h = hashlib.blake2b(str_data.encode('utf-8'))
            # h = hashlib.new('blake2b512', str_data.encode('utf-8'))
            # certTokenSignature
            return base64.b64encode(h.digest())

         # 获取大白人脸识别
        @app.route('/getWxxcxSign', methods=['POST', 'GET'])
        def get_wxxcx_sign():
            request_data = json.loads(request.data)
            token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxc12e6e381af896c8&secret=1804bb2ff1f6c2e84865b82d48373412'
            res_token = requests.get(token_url)
            res_data = res_token.json()

            jsapi_ticket_url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi' % (
                res_data['access_token'])
            res_jsapi_ticket = requests.get(jsapi_ticket_url)
            res_jsapi_ticket_data = res_jsapi_ticket.json()
            sign = Sign(res_jsapi_ticket_data['ticket'],
                        request_data['url'])
            sign.sign()
            return json.dumps({
                'nonceStr': sign.ret['nonceStr'],
                'jsapi_ticket': sign.ret['jsapi_ticket'],
                'timestamp': sign.ret['timestamp'],
                'url': sign.ret['url'],
                'debug': True,
                'signature': sign.ret['signature'],
            }, ensure_ascii=False)

        # 微信支付异步路由
        @app.route(self.wechat_payment_data['interface'], methods=['POST', 'GET'])
        def __wechat_payment_callback():  # 微信支付异步回调
            # TODO: 数据库录入结果
            def get_id_from_out_trade_no(out_trade_no):
                _filter = MongoBillFilter()
                _filter.match_bill((C('out_trade_no') == out_trade_no))\
                    .project({'_id': 0})
                res = self.query(_filter, 'IEC_third_part_trade_record')
                return res[0]['id']

            def third_pay_record_add_or_update(data):
                tabele_name = 'IEC_third_part_trade_record'
                result = self.bill_manage_server.add_bill(
                    OperationType.update.value
                    if 'id' in data.keys() else OperationType.add.value,
                    TypeId.thirdPartPay.value, data, tabele_name)
                return result

            trade_result_status: ThirdPartPayStatus
            remark = None
            try:
                return_data = xmltodict.parse(request.data)['xml']
                # 返回状态码，通信标识，非交易结果
                return_code = return_data['return_code']
                # 商户唯一订单号
                out_trade_no = return_data['out_trade_no']
                # 返回结果
                # return_msg = return_data['return_msg']
                if return_code == 'SUCCESS':
                    # 公众账号ID
                    appid = return_data['appid']
                    # 商户号
                    mch_id = return_data['mch_id']
                    # 设备号
                    # device_info = return_data['device_info']
                    # 随机字符串
                    nonce_str = return_data['nonce_str']
                    # 业务结果
                    result_code = return_data['result_code']
                    # 签名
                    sign = return_data['sign']
                    # TODO: 校验签名结果是否正确
                    if sign == cal_sign(return_data,
                                        self.wechat_payment_data['mch_key']):
                        # 结果、签名均正确，修改交易记录表
                        trade_result_status = ThirdPartPayStatus.payed
                        remark = None
                    else:
                        # 签名校验失败
                        trade_result_status = ThirdPartPayStatus.sign_fail
                        remark = {'desc': '签名校验失败'}
                else:
                    trade_result_status = ThirdPartPayStatus.fail
                    error_code = return_data['err_code']
                    err_code_des = return_data['err_code_des']
                    remark = {
                        'error_code': error_code,
                        'err_code_des': err_code_des
                    }
            finally:
                db_id = get_id_from_out_trade_no(out_trade_no)
                if trade_result_status == ThirdPartPayStatus.payed:
                    third_pay_record_add_or_update({
                        "id": db_id,
                        'transaction_id': return_data['transaction_id'],
                        "out_trade_no": return_data['out_trade_no'],
                        "total_fee": int(return_data['total_fee']),
                        "pay_time": datetime.datetime.now(),
                        # 已付款
                        "pay_status":  ThirdPartPayStatus.payed.value,
                        "remark": remark
                    })
                    # 调用财务接口
                    pay_params = getattr(g,  return_data['out_trade_no'], None)
                    if pay_params:
                        app_pay_service = FinanicalAppPay(
                            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
                        app_pay_service.bill_manage_server = BillManageService(
                            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, {
                                "user_id": user_id,
                                'organization_id': organization_id
                            })
                        app_pay_service.confirm_financial_sys(
                            '微信支付', pay_params)
                        setattr(g,  return_data['out_trade_no'], None)
                    attach = return_data['attach'] if 'attach' in return_data else None
                    # self.wechatSubject.notify(
                    #     {"out_trade_no": return_data['out_trade_no'], 'attach': attach})

                if trade_result_status == ThirdPartPayStatus.sign_fail:
                    third_pay_record_add_or_update({
                        "id":
                        db_id,
                        "pay_status":
                        ThirdPartPayStatus.sign_fail.value,
                        'remark':
                        remark
                    })

                if trade_result_status == ThirdPartPayStatus.fail:
                    third_pay_record_add_or_update({
                        "id": db_id,
                        # 已付款
                        "pay_status": ThirdPartPayStatus.fail.value,
                        'remark': remark
                    })
                return dicttoxml.dicttoxml(
                    {
                        'return_code': 'SUCCESS',
                        'return_msg': 'OK'
                    },
                    custom_root='xml',
                    cdata=True,
                    attr_type=False).decode('utf-8')

        # 支付宝路由
        @app.route(self.alipay_payment_data['interface'])
        def alipay_callback():
            pass
            # TODO: 数据库录入结果
            # print('========   支付宝返回数据 start============：')
            # print(request)
            # print('========   支付宝返回数据 end  ============')

            # postFrom = request.form
            # args = request.args
            # postValue = request.values

            # # 解析响应结果
            # response = AlipayTradeWapPayResponse()
            # response.parse_response_content(request)
            # # 响应成功的业务处理
            # if response.is_success():
            #     # 如果业务成功，可以通过response属性获取需要的值
            #   #print("get response trade_no:" + response.trade_no)
            # # 响应失败的业务处理
            # else:
            #     # 如果业务失败，可以从错误码中可以得知错误情况，具体错误码信息可以查看接口文档
            #   #print(response.code + "," + response.msg + "," + response.sub_code + "," + response.sub_msg)

            # 文件下载路由
        @app.route('/download/<path:filepath>')
        def __download(filepath):
            # 获取文件命加
            filename = os.path.basename(filepath)
            # 获取文件的绝度路径
            path = os.path.join(os.getcwd(), filepath)

            def send_file():
                ''' 流式读取 '''
                store_path = path
                with open(store_path, 'rb') as targetfile:
                    while 1:
                        data = targetfile.read(20 * 1024 * 1024)  # 每次读取20M
                        if not data:
                            break
                        yield data

            response = Response(send_file(),
                                content_type='application/octet-stream')
            # 如果不加上这行代码，导致下载的文件没有扩展名
            response.headers[
                "Content-disposition"] = 'attachment; filename=%s' % filename
            return response

        @app.route('/upload', methods=['POST', 'GET'])
        def __upload():
            result = {}
            dImg = {}
            type_path = ''
            if request.method == 'POST':
                f = request.files['file']
                # 判断是否为图片
                is_pic = False
                strlist = f.filename.split('.')
                # 后缀判断
                if len(strlist) > 1:
                    file_hz = strlist[len(strlist) - 1]
                    if file_hz.lower() not in ['png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'xls', 'xlsx', 'pdf', 'mp4', 'ogg']:
                        return {
                            'code': 403,
                            'desc': '禁止上传非法格式文件',
                        }
                else:
                    # 没后缀
                    return {
                        'code': 403,
                        'desc': '禁止上传无格式文件',
                    }

                for item in strlist:
                    if item in ['png', 'jpg', 'gif', 'jpeg']:
                        is_pic = True
                # 是图片
                if is_pic == True:
                    dImg = f
                    # 判断从哪上传的
                    if request.form.get("upload_type") == '防疫上报':
                        # 获取图片
                        sImg = Image.open(f)
                        # 获取图片大小
                        w, h = sImg.size
                        # 压缩图片
                        dImg = sImg.resize(
                            (int(w/2), int(h/2)), Image.ANTIALIAS)
                        type_path = 'fangyi'
                    else:
                        # 获取图片
                        sImg = Image.open(f)
                        # 获取图片大小
                        w, h = sImg.size
                        # 压缩图片
                        dImg = sImg.resize(
                            (int(w/2), int(h/2)), Image.ANTIALIAS)
                        type_path = 'other'
                    # 拼路径
                    time_id_file = str(int(time.time()))
                    time_id = time.strftime("%Y%m", time.localtime())
                    new_upload_file = os.path.join(self.upload_file,
                                                   type_path).replace('\\', '/')
                    upload_path = "{0}\\{1}".format(new_upload_file,
                                                    time_id)  # 以时间戳命名文件
                    file_path = os.path.join(self.web_path,
                                             upload_path).replace('\\', '/')
                    if not os.path.exists(file_path):  # 不存在改目录则会自动创建
                        os.makedirs(file_path)
                    # 保存文件
                    save_path = os.path.join(file_path, time_id_file+'_'+f.filename).replace(
                        '\\', '/')  # windows下路径要转化
                    dImg.save(save_path)
                    # 拼接文件url地址
                    result = {
                        'code': 200,
                        'desc': '上传成功',
                    }
                    result['url'] = save_path
                # 不是图片
                else:
                    # 拼路径
                    time_id_file = str(int(time.time()))
                    time_id = time.strftime("%Y%m", time.localtime())
                    upload_path = "{0}\\{1}".format(self.upload_file,
                                                    time_id)  # 以时间戳命名文件
                    file_path = os.path.join(self.web_path,
                                             upload_path).replace('\\', '/')
                    if not os.path.exists(file_path):  # 不存在改目录则会自动创建
                        os.makedirs(file_path)
                    # 保存文件
                    save_path = os.path.join(file_path, time_id_file+'_'+f.filename).replace(
                        '\\', '/')  # windows下路径要转化
                    f.save(save_path)
                    # 拼接文件url地址
                    result = {
                        'code': 200,
                        'desc': '上传成功',
                    }
                    result['url'] = save_path
            else:
                return {
                    'code': 403,
                    'desc': '禁止非法上传',
                }
            return json.dumps(result, ensure_ascii=False)

        # 易米呼叫中心回调地址
        # 呼叫请求回调地址
        @app.route('/emi/callrequest_callback', methods=['POST'])
        def __call_request_callback():
            return HandleEmiCallback.handle_call_request_callback(request.data, self.bill_manage_server, self.device_gateway_host)

        # 呼叫建立回调地址
        @app.route('/emi/callestablish_callback', methods=['POST'])
        def __call_establish_callback():
            return HandleEmiCallback.handle_call_establish_callback(request.data)

        # 呼叫挂机或失败回调地址
        @app.route('/emi/callhangup_callback', methods=['POST'])
        def __call_hangup_callback():
            return HandleEmiCallback.handle_call_hangup_callback(request.data, self.bill_manage_server)

        # 微信公众号回调
        @app.route('/wx/xfy_follow_callback', methods=['POST', 'GET'])
        def __xfy_follow_callback():
            official_account = OfficialAccountsCallback(
                request, self.bill_manage_server, session, self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd)
            return official_account.xfy_follow_callback()

        # 设备接收推送数据
        # 位置数据发送
        @app.route('/device/location', methods=['POST'])
        def __device_location():
            NewDeviceService = DeviceService(
                self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
            return NewDeviceService.device_api_callback('LOCATION', request, self.bill_manage_server)
        # SOS数据发送
        @app.route('/device/sos', methods=['POST'])
        def __device_sos():
            NewDeviceService = DeviceService(
                self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
            return NewDeviceService.device_api_callback('SOS', request, self.bill_manage_server)
        # 体温数据发送
        @app.route('/device/temperature', methods=['POST'])
        def __device_temperature():
            NewDeviceService = DeviceService(
                self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
            return NewDeviceService.device_api_callback('TEMPERATURE', request, self.bill_manage_server)
        # 电量数据发送
        @app.route('/device/electricity', methods=['POST'])
        def __device_electricity():
            NewDeviceService = DeviceService(
                self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
            return NewDeviceService.device_api_callback('ELECTRICITY', request, self.bill_manage_server)
        # 消息推送【含电子围栏报警】
        @app.route('/device/message', methods=['POST'])
        def __device_message():
            NewDeviceService = DeviceService(
                self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
            return NewDeviceService.device_api_callback('MESSAGE', request, self.bill_manage_server)

        @app.route('/', defaults={'path': ''}, methods=['POST', 'GET'])
        @app.route('/<path:path>')
        def __index(path):
            return render_template('/www/index.html')

        @app.route('/app', defaults={'path': ''}, methods=['POST', 'GET'])
        @app.route('/app/<path:path>')
        def __app_index(path):
            return render_template('/app/index.html')

        @app.route('/display', defaults={'path': ''}, methods=['POST', 'GET'])
        @app.route('/display/<path:path>')
        def __display_index(path):
            return render_template('/display/index.html')

    # 注册打开新路由服务
    def on_register_new_route(self, app, jsonrpc):
        new_route.register(app, self.web_path, self.upload_file,
                           jsonrpc, self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

    def on_regsiter_services(self, jsonrpc, session):
        ''' 注册文件上传服务 '''
        upload_file.register(jsonrpc, self.web_path, self.upload_file,
                             self.db_addr, self.db_port, self.db_name)
        permission_list = self.permission_table.permission_list()
        # 登录权限服务
        login_service.register(jsonrpc, self.db_addr, self.db_port,
                               self.db_name, self.db_user, self.db_pwd, permission_list,
                               self.inital_password, session)
        # 用户服务
        user_func_register.register(jsonrpc, self.db_addr, self.db_port,
                                    self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                    session)
        # 档案服务
        # nursing_func_register.register(
        #     jsonrpc, self.db_addr, self.db_port, self.db_name, self.inital_password, session)
        # 人员及组织机构服务
        personnel_organizational_register.register(jsonrpc, self.db_addr,
                                                   self.db_port, self.db_name,
                                                   self.db_user, self.db_pwd,
                                                   self.inital_password,
                                                   session)
        # 单据签核服务
        bill_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                      session)
        # 服务商登记服务
        service_operation_register.register(jsonrpc, self.db_addr,
                                            self.db_port, self.db_name, self.db_user, self.db_pwd,
                                            self.inital_password, session)
        # 业务区域管理服务
        business_area_register.register(jsonrpc, self.db_addr, self.db_port,
                                        self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                        session)
        # Mongo封装渠道
        channel_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 通用方法
        general_purpose.register(jsonrpc, self.db_addr, self.db_port,
                                 self.db_name, self.db_user, self.db_pwd,)
        # 交易管理服务
        transaction_management_register.register(jsonrpc, self.db_addr,
                                                 self.db_port, self.db_name, self.db_user, self.db_pwd,
                                                 self.inital_password, session)
        # 能力评估模板服务
        assessment_template_register.register(jsonrpc, self.db_addr,
                                              self.db_port, self.db_name, self.db_user, self.db_pwd,
                                              self.inital_password, session)
        # 评估项目服务
        assessment_project_register.register(jsonrpc, self.db_addr,
                                             self.db_port, self.db_name, self.db_user, self.db_pwd,
                                             self.inital_password, session)
        # 能力评估服务
        competence_assessment_register.register(jsonrpc, self.db_addr,
                                                self.db_port, self.db_name, self.db_user, self.db_pwd,
                                                self.inital_password, session)
        # 服务项目服务
        services_project_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                           session)
        # 服务项目类别服务
        services_item_category_register.register(jsonrpc, self.db_addr,
                                                 self.db_port, self.db_name, self.db_user, self.db_pwd,
                                                 self.inital_password, session)
        # 服务使用范围服务
        service_scope_register.register(jsonrpc, self.db_addr, self.db_port,
                                        self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                        session)
        # 服务选项服务
        service_option_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 服务包服务
        service_item_package_register.register(jsonrpc, self.db_addr,
                                               self.db_port, self.db_name, self.db_user, self.db_pwd,
                                               self.inital_password, session)
        # 住宿区域类型服务
        hotel_zone_type_register.register(jsonrpc, self.db_addr, self.db_port,
                                          self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                          session)
        # 住宿区域服务
        hotel_zone_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                     session)
        # 需求类型服务
        requirement_type_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                           session)
        # 需求项目服务
        requirement_project_register.register(jsonrpc, self.db_addr,
                                              self.db_port, self.db_name, self.db_user, self.db_pwd,
                                              self.inital_password, session)
        # 需求项目选项服务
        requirement_option_register.register(jsonrpc, self.db_addr,
                                             self.db_port, self.db_name, self.db_user, self.db_pwd,
                                             self.inital_password, session)
        # 财务服务
        financial_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                           session)
        # 接待管理
        # reservation_register.register(
        #     jsonrpc, self.db_addr, self.db_port, self.db_name, self.inital_password, session)
        # 服务记录服务
        service_record_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 预约登记服务
        reservation_registration_register.register(jsonrpc, self.db_addr,
                                                   self.db_port, self.db_name, self.db_user, self.db_pwd,
                                                   self.inital_password,
                                                   session)
        check_in_register.register(jsonrpc, self.db_addr, self.db_port,
                                   self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 服务记录服务
        service_record_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 需求模板服务
        requirement_template_register.register(jsonrpc, self.db_addr,
                                               self.db_port, self.db_name, self.db_user, self.db_pwd,
                                               self.inital_password, session)
        # 需求记录服务
        requirement_record_register.register(jsonrpc, self.db_addr,
                                             self.db_port, self.db_name, self.db_user, self.db_pwd,
                                             self.inital_password, session)
        # 公告服务
        announcement_manage_register.register(jsonrpc, self.db_addr,
                                              self.db_port, self.db_name, self.db_user, self.db_pwd,
                                              self.inital_password, session)
        # 文章类型服务
        article_type_register.register(jsonrpc, self.db_addr, self.db_port,
                                       self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                       session)
        # 公告管理服务
        announcement_manage_register.register(jsonrpc, self.db_addr,
                                              self.db_port, self.db_name, self.db_user, self.db_pwd,
                                              self.inital_password, session)
        # 新闻管理服务
        new_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                     session)
        # 活动服务
        activity_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                          self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                          session)
        # 任务服务
        task_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                      session)
        # 信息服务
        message_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 服务订单任务
        service_order_task_register.register(jsonrpc, self.db_addr,
                                             self.db_port, self.db_name, self.db_user, self.db_pwd,
                                             self.inital_password, session)
        # 服务人员服务
        service_personal_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                           session)
        # 补贴服务
        allowance_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                           session)
        # 调研服务
        research_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                          self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                          session)
        # 请假销假服务
        leave_record_register.register(jsonrpc, self.db_addr, self.db_port,
                                       self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                       session)
        # 评论类型服务
        comment_type_register.register(jsonrpc, self.db_addr, self.db_port,
                                       self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                       session)
        # 评论管理服务
        comment_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 文章审核服务
        article_audit_register.register(jsonrpc, self.db_addr, self.db_port,
                                        self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                        session)
        # 慈善服务
        charitable_manage_register.register(
            jsonrpc,
            self.db_addr,
            self.db_port,
            self.db_name,
            self.db_user,
            self.db_pwd,
            self.inital_password,
            session,
            self.wechatSubject
        )
        '''app端'''
        # 首页服务
        home_page_register.register(jsonrpc, self.db_addr, self.db_port,
                                    self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                    session)
        # 家属预约服务
        reservation_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                      session)
        # 确认订单服务
        comfirm_order_register.register(jsonrpc, self.db_addr, self.db_port,
                                        self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                        session)
        # 服务套餐列表服务
        service_package_list_register.register(jsonrpc, self.db_addr,
                                               self.db_port, self.db_name, self.db_user, self.db_pwd,
                                               self.inital_password, session)
        # 我的订单服务
        my_order_register.register(jsonrpc, self.db_addr, self.db_port,
                                   self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                   self.realm_name, self.wechat_payment_data,
                                   self.alipay_payment_data, session)
        # 服务详情服务
        service_detail_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)
        # 我的页面服务
        mine_home_register.register(jsonrpc, self.db_addr, self.db_port,
                                    self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                    session)
        # 评论服务
        comment_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 新闻服务
        news_register.register(jsonrpc, self.db_addr, self.db_port,
                               self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 账务管理服务
        financial_account_register.register(jsonrpc, self.db_addr,
                                            self.db_port, self.db_name, self.db_user, self.db_pwd,
                                            self.inital_password, session)

        # 医院管理
        hospital_register.register(jsonrpc, self.db_addr, self.db_port,
                                   self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 水电抄表
        hydropower_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                     session)

        # 费用结算服务
        cost_account_register.register(jsonrpc, self.db_addr, self.db_port,
                                       self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                       session)
        # 新闻服务
        announcement_register.register(jsonrpc, self.db_addr, self.db_port,
                                       self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                       session)
        # 社会群体类型服务
        social_groups_type_register.register(jsonrpc, self.db_addr,
                                             self.db_port, self.db_name, self.db_user, self.db_pwd,
                                             self.inital_password, session)
        # 轮播图片服务
        carousel_register.register(jsonrpc, self.db_addr, self.db_port,
                                   self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 我的账户服务
        my_account_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                     session)

        # 大屏服务
        display_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # excel文档服务
        excel_register.register(jsonrpc, self.db_addr, self.db_port,
                                self.db_name, self.db_user, self.db_pwd, self.inital_password, session, self.web_path, self.upload_file)

        # 大屏展示
        display_register_kun.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                      session)
        # '大屏'
        # display_register_hins.register(
        #     jsonrpc, self.db_addr, self.db_port, self.db_name, self.inital_password, session)

        # 大屏hwg
        display_register_hwg.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name,  self.db_user, self.db_pwd, self.inital_password,
                                      session)
        # 大屏服务测试——叶
        display_register_ye.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name,  self.db_user, self.db_pwd, self.inital_password,
                                     session)
        # xzx
        # display_register_ny.register(
        #     jsonrpc, self.db_addr, self.db_port, self.db_name, self.inital_password, session)
        # 大屏服务测试——z
        # display_register_z.register(
        #     jsonrpc, self.db_addr, self.db_port, self.db_name, self.inital_password, session)
        # 大屏控制
        display_control_register(jsonrpc, self.db_addr, self.db_port,
                                 self.db_name, self.db_user, self.db_pwd, [], self.inital_password,
                                 session)
        # 大屏控制——叶
        ipad_register.register(jsonrpc, self.db_addr, self.db_port,
                               self.db_name,  self.db_user, self.db_pwd, self.inital_password, session)

        # 安全模块-登录
        security_login_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session, self.sms_data)
        # 安全模块-注册服务
        security_register_register.register(jsonrpc, self.db_addr,
                                            self.db_port, self.db_name, self.db_user, self.db_pwd,
                                            self.inital_password, session)
        # 人员组织管理模块
        person_org_manage_register.register(jsonrpc, self.db_addr,
                                            self.db_port, self.db_name, self.db_user, self.db_pwd,
                                            self.inital_password, session)
        # 短信接口管理模块
        sms_manage_register.register(jsonrpc, self.db_addr,
                                     self.db_port, self.db_name, self.db_user, self.db_pwd,
                                     self.inital_password, session, self.sms_data, request)
        # 商城管理模块
        shopping_mall_manage_register.register(jsonrpc, self.db_addr,
                                               self.db_port, self.db_name, self.db_user, self.db_pwd,
                                               self.inital_password, session)
        # 南海呼叫中心管理模块
        call_center_nh_register.register(jsonrpc, self.db_addr,
                                         self.db_port, self.db_name, self.db_user, self.db_pwd,
                                         self.inital_password, session, request)
        # 角色设置
        role_register.register(jsonrpc, self.db_addr, self.db_port,
                               self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 设备模块
        device_register.register(jsonrpc, self.db_addr, self.db_port,
                                 self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 需求模块
        demand_register.register(jsonrpc, self.db_addr, self.db_port,
                                 self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 视频监控模块
        monitor_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 测试生成记录
        test_register.register(jsonrpc, self.db_addr, self.db_port,
                               self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 住宿过程
        accommodation_process_register.register(jsonrpc, self.db_addr,
                                                self.db_port, self.db_name, self.db_user, self.db_pwd,
                                                self.inital_password, session)

        # 费用管理
        cost_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                      session)

        # 护理管理
        nursing_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                         session)

        # 报表管理
        report_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                        self.db_name, self.db_user, self.db_pwd, self.inital_password,
                                        session)

        # app支付
        app_pay_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 易米呼叫中心
        emi_callcenter_register.register(jsonrpc, self.emi_callcenter_data, self.db_addr,
                                         self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 前端-后台-易米呼叫中心
        emicall_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 知识库
        knowledge_base_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 基本情况
        base_situation_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 补贴账户充值
        subsidy_account_recharg_register.register(jsonrpc, self.db_addr, self.db_port,
                                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 操作记录
        operation_record_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 人员关系
        user_relationship_register.register(jsonrpc, self.db_addr, self.db_port,
                                            self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 老友圈
        friends_circle_register.register(jsonrpc, self.db_addr, self.db_port,
                                         self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 关注收藏
        service_follow_collection_register.register(jsonrpc, self.db_addr, self.db_port,
                                                    self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 意见反馈
        opinion_feedback_register.register(jsonrpc, self.db_addr, self.db_port,
                                           self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 微信公众号关注统计
        official_account_follow_register.register(jsonrpc, self.db_addr, self.db_port,
                                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 系统公告
        annoutment_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 其他公告
        other_cost_register.register(jsonrpc, self.db_addr, self.db_port,
                                     self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 健康照护
        health_care_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # app页面设置
        app_page_config_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                                 self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 物料
        materiel_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                          self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 咪狗设备
        mg_hardware_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
        # 云芯设备
        yx_hardware_register.register(jsonrpc, self.db_addr, self.db_port,
                                      self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 我的家
        my_home_register.register(jsonrpc, self.db_addr, self.db_port,
                                  self.db_name, self.db_user, self.db_pwd, self.inital_password, session)

        # 分账登记服务
        ledger_account_manage_register.register(jsonrpc, self.db_addr, self.db_port,
                                                self.db_name, self.db_user, self.db_pwd, self.inital_password, session)
