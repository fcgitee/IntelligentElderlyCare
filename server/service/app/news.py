from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.data import dataframe_to_list
from ...models.financial_manage import FinancialBook, FinancialAccount, FinancialRecord, FinancialSubject, FinancialVoucher, FinancialEntry, FinancialCharge, FinancialSetSubjectItem
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
from ...service.buss_mis.news_manage import NewsService as NewsService2
from ...service.common import get_info
from server.pao_python.pao.data import string_to_date, date_to_string
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
import pandas as pd
import uuid
import copy
import datetime
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-02-18 18:01:53
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\app\news.py
'''
'''
说明
 1、新闻相关接口服务
'''


class NewsService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, db_user, db_pwd, self.inital_password, self.session)
        self.NewsService2 = NewsService2(
            self.db_addr, self.db_port, self.db_name, db_user, db_pwd, self.inital_password, self.session)

    def get_all_list(self, condition, page=None, count=None):
        '''获取新闻列表'''
        return NewsService2.get_news_list(self, N(), condition, page, count)
        keys = ['type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('status') == 'audit_success')\
               .inner_join_bill('PT_Article_Type', 'type_id', 'id', 'type_info')\
               .match((C('type_info.name') == values['type']))\
               .project({'_id': 0, 'type_info._id': 0})
        # print(_filter)
        res = self.page_query(_filter, 'PT_Article', page, count)
        return res

    def get_news_detail(self, data):
        '获取新闻详情'
        news_id = data.get('id')
        if news_id:
            _filter = MongoFilter()
            _filter.match((C('bill_status') == 'valid') & (C('id') == news_id))\
                .project({'_id': 0})
            res = self.page_query(_filter, 'PT_Article', None, None)
            _filter_comment = MongoFilter()
            _filter_comment.match((C('bill_status') == 'valid') & (C('comment_object_id') == news_id))\
                .project({'_id': 0})
            res_comment = self.query(_filter_comment, 'PT_Comment')
            res['result'][0]['comment_list'] = res_comment
            return res
        return 'no this news'
