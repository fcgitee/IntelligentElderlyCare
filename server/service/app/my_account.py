'''
@Author: your name
@Date: 2019-11-08 09:21:19
@LastEditTime: 2019-12-05 15:04:24
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\app\my_account.py
'''

from ...service.buss_mis.financial_account import FinancialAccountService
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_mis.comment_manage import CommentManageService
from enum import Enum
from ..constant import AccountType, AccountStatus, PayStatue, PayType


class MyAccountService(MongoService):
    ''' 我的账户服务 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_my_account(self):
        '''
        查看我的账户
        '''
        user_id = get_current_user_id(self.session)
        # user_id = 'a3d0cc18-f3ea-11e9-a2ea-144f8a6221df'
        # user_id = '136e5e8c-0141-11ea-93a1-7c2a3115762d'
        _filter = MongoBillFilter()
        _filter.match_bill(C('user_id') == user_id).project({'_id': 0})
        res = self.page_query(_filter, 'PT_Financial_Account', None, None)
        return res

    def get_account_detail(self, account_data):
        '''查看账户详情'''
        account_id = account_data.get('id')
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == account_id).project({'_id': 0})
        res = self.page_query(_filter, 'PT_Financial_Account', None, None)
        return res
