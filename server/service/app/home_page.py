from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:30:26
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\app\home_page.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Monday August 5th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Monday, 5th August 2019 3:53:38 pm
修改者: ymq(ymq) - <<email>>

说明
 1、app首页接口服务
'''


class HomePageService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def update_rotary_picture(self, data):
        '''新增/修改轮播图'''
        res = 'Fail'
        if 'id' not in list(data.keys()):
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.homePage.value, data, 'PT_Rotary_Picture')
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.homePage.value, data, 'PT_Rotary_Picture')
        if bill_id:
            res = 'Success'
        return res

    def get_rotary_picture(self):
        '''获取轮播图片'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('status') == '可用')\
               .project({'_id': 0, 'url': 1})
        res = self.query(_filter, 'PT_Rotary_Picture')
        return res
