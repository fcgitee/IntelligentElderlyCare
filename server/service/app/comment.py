'''
评论接口
'''
import datetime
import copy
import uuid
import pandas as pd
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from server.pao_python.pao.data import string_to_date, date_to_string, process_db
from ...service.common import get_current_user_id
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
from server.pao_python.pao.data import dataframe_to_list
from ...service.mongo_bill_service import MongoBillFilter


class CommentService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_comment_list(self, condition):
        '''获取评论列表'''
        keys = ['comment_object_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('audit_status') != 'audit_fail') & (
            C('comment_object_id') == values['comment_object_id'])).project({'_id': 0})
        res = self.page_query(_filter, 'PT_Comment', None, None)
        try:
            if res['result']:
                for comment in res['result']:
                    user_id = comment.get('comment_user_id')
                    _filter_user = MongoBillFilter()
                    _filter_user.match_bill(
                        C('id') == user_id).project({'_id': 0})
                    res_user = self.query(_filter_user, 'PT_User')
                    if res_user:
                        comment['comment_user'] = res_user[0].get('name')
                    else:
                        comment['comment_user'] = ''
        except Exception as e:
            return e
        return res

    def add_comment(self, comment):
        '''新增评论'''
        res = 'Fail'
        person_id = get_current_user_id(self.session)
        comment['comment_user_id'] = person_id
        comment['audit_status'] = 'auditing'
        comment['comment_date'] = datetime.datetime.now()
        comment_object_id = comment.get("id")
        comment.pop('id')
        _filter_obj = MongoFilter()
        _filter_obj.match((C('bill_status') == 'valid') & (C('id') == comment_object_id))\
            .project({'_id': 0})
        if len(self.query(_filter_obj, 'PT_Comment')) > 0:
            comment['father_comment_id'] = comment_object_id
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.comment.value, comment, 'PT_Comment')
            if bill_id:
                res = 'Success'
        else:
            comment['comment_object_id'] = comment_object_id
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.comment.value, comment, 'PT_Comment')
            if bill_id:
                res = 'Success'
        return res

    def get_my_comment(self):
        '''获取我的评论'''
        person_id = get_current_user_id(self.session)
        _filter_obj = MongoBillFilter()
        _filter_obj.lookup_bill('PT_Comment_Type', 'type_id', 'id', 'type')\
            .add_fields({
                'type': '$type.name',
                'comment_date': self.ao.date_to_string("$comment_date"),
            })\
            .match((C('bill_status') == 'valid') & (C('comment_user_id') == person_id))\
            .project({'_id': 0, 'type._id': 0})
        res = self.query(_filter_obj, 'PT_Comment')
        for result in res:
            comment_object_id = result.get('comment_object_id')
            _filter_obj = MongoFilter()
            _filter_obj.match((C('bill_status') == 'valid') & (
                C('id') == comment_object_id)).project({'_id': 0})
            if len(self.query(_filter_obj, 'PT_Service_Item')) > 0:
                res_obj = self.query(_filter_obj, 'PT_Service_Item')
                result['comment_object'] = res_obj[0].get('name')
            elif len(self.query(_filter_obj, 'PT_Article')) > 0:
                res_obj = self.query(_filter_obj, 'PT_Article')
                result['comment_object'] = res_obj[0].get('title')
            elif len(self.query(_filter_obj, 'PT_Activity')) > 0:
                res_obj = self.query(_filter_obj, 'PT_Activity')
                result['comment_object'] = res_obj[0].get('activity_name')
            elif len(self.query(_filter_obj, 'PT_Comment')) > 0:
                res_obj = self.query(_filter_obj, 'PT_Comment')
                result['father_comment'] = res_obj[0].get('content')
        return res

    def give_a_like(self, data):
        '''点赞'''
        result = 'Fail'
        person_id = get_current_user_id(self.session)
        like_date = datetime.datetime.now()
        comment_id = data.get('id')
        _filter_obj = MongoFilter()
        _filter_obj.match((C('bill_status') == 'valid') & (C('id') == comment_id))\
            .project({'_id': 0})
        res = self.query(_filter_obj, 'PT_Comment')
        if res:
            if len(res[0].get('like_list')) > 0:
                like_list = res[0].get('like_list')
                for like in like_list:
                    if person_id in like.get('like_user_id'):
                        return 'like already'
                like_list.append(
                    {'like_user_id': person_id, 'like_date': like_date})
                comment_data = {
                    "id": comment_id,
                    'like_list': like_list
                }
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.comment.value, comment_data, 'PT_Comment')
                if bill_id:
                    result = 'Success'
            else:
                like_list.append(
                    {'like_user_id': person_id, 'like_date': like_date})
                comment_data = {
                    "id": comment_id,
                    'like_list': like_list
                }
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.comment.value, comment_data, 'PT_Comment')
                if bill_id:
                    result = 'Success'
        else:
            res = 'no this comment'
        return result
