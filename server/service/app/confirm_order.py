import collections
import copy
import uuid
from datetime import datetime

import pandas as pd

from server.pao_python.pao.data import (dataframe_to_list, date_to_string,
                                        process_db, string_to_date, get_cur_time)
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...models.financial_manage import (FinancialAccount, FinancialBook,
                                        FinancialCharge, FinancialEntry,
                                        FinancialRecord,
                                        FinancialSetSubjectItem,
                                        FinancialSubject, FinancialVoucher)
from ...service.app.my_order import OrderStatus, RecordStatus
from ...service.app.service_detail import ServiceProductDetailService
from ...service.buss_mis.operation_record import (OperationRecordObject,
                                                  OperationRecordService,
                                                  OperationRecordType)
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import (SerialNumberType, execute_python, find_data,
                               get_current_user_id, get_info,
                               get_serial_number, get_common_project)
from ...service.mongo_bill_service import MongoBillFilter
from .my_order import ThirdPartPayStatus
from ...service.buss_pub.message_manage import MessageManageService
from ...service.buss_iec.subsidy_account_recharg import SubsidyAccountRechargService
import time

'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-09-25 09:48:29
@LastEditors: your name
'''
'''
说明
 1、app确认订单服务
'''


class ComfirmOrderService(MongoService):
    balance = 0
    account_id = ''
    amount = 0

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.account = {
            'balance': 0,
            'account_id': '',
        }
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.service_product_detail_service = ServiceProductDetailService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.OperationRecordService = OperationRecordService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.MessageManageService = MessageManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.subsidy_account_recharg_func = SubsidyAccountRechargService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_all_product(self, condition, all_product=None):
        # keys = ['id']
        # values = self.get_value(condition, keys)
        # _filter = MongoBillFilter()
        # _filter.match_bill((C('id') == values['id']))\
        #     .lookup_bill('PT_Service_Order', 'id', 'detail.product_id', 'order_info')\
        #     .add_fields({'pay_count': self.ao.size('$order_info'), })\
        #     .project({'_id': 0, 'order_info._id': 0})
        # res = self.query(_filter, 'PT_Service_Product')
        # porduct_item_list = []
        try:
            # if res:
                # product_queue = collections.deque()
                # product_queue.append(res[0])
                # while len(product_queue) != 0:
                #     res_f = product_queue.popleft()
                #     if res_f.get('service_product'):
                #         for product_id_list in res_f.get('service_product'):
                #             if product_id_list.get("product_id"):
                #                 res_child = self.service_product_detail_service.get_children_tree(
                #                     product_id_list.get("product_id"))
                #                 if res_child:
                #                     if res_child[0].get('service_product'):
                #                         product_queue.append(res_child[0])
                #                     else:
                #                         if product_id_list.get("remaining_times"):
                #                             res_child[0]['remaining_times'] = product_id_list.get(
                #                                 "remaining_times")
                #                         else:
                #                             res_child[0]['remaining_times'] = 1
                #                         porduct_item_list.append(res_child[0])
                #             else:
                #                 porduct_item_list.append(res_f)
                #     else:
                #         porduct_item_list.append(res_f)
            porduct_item_list = self.get_product(
                condition, [], 1, True, all_product)
            # t3 = time.time()
            # for product_item in porduct_item_list:
            #     product_item_price = 0
            #     if product_item.get('valuation_formula') and product_item.get('service_option'):
            #         if product_item.get('valuation_formula').get('formula'):
            #             product_item_price = execute_python(product_item.get('valuation_formula')[0].get(
            #                 'formula'), product_item.get('service_option'), 'name', 'option_value')
            #             product_item['valuation_amount'] = product_item_price

            # t4 = time.time()
            # print('算金额的耗时>>>', t4-t3)
        except Exception as e:
            pass
          # print(e)
        return porduct_item_list

    def get_product(self, condition, porduct_item_list, remaining_times, is_first=False, all_product=None):
        """递归"""
        if all_product != None and 'id' in condition.keys() and condition['id'] in all_product.keys():
            res = [all_product[condition['id']]]
        else:
            keys = ['id']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == values['id']))\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Service_Product')
        if len(res) > 0:
            if res[0]['is_service_item'] == 'false':
                # 套餐
                if res[0]['service_product']:
                    for product_id_list in res[0]['service_product']:
                        if is_first:
                            remaining_times = 1
                        if product_id_list.get("remaining_times"):
                            if type(product_id_list.get("remaining_times")) == str:
                                remaining_times = remaining_times * \
                                    int(product_id_list.get("remaining_times"))
                            else:
                                remaining_times = remaining_times * \
                                    product_id_list.get("remaining_times")
                        if product_id_list.get("product_id"):
                            porduct_item_list = self.get_product(
                                {"id": product_id_list.get("product_id")}, porduct_item_list, remaining_times, False, all_product)
            else:
                # 单品
                res[0]['remaining_times'] = remaining_times
                porduct_item_list.append(res[0])
        return porduct_item_list

    def get_purchaser_detail(self):
        '''获取购买者信息'''
        data = {}
        person_id = get_current_user_id(self.session)
        _filter_user = MongoBillFilter()
        _filter_user.match_bill((C('id') == person_id)).project({'_id': 0})
        res_user = self.query(_filter_user, 'PT_User')
        if res_user:
            if res_user[0]['personnel_info']:
                data['personnel_info'] = res_user[0]['personnel_info']
            if not res_user[0]['personnel_info'].get('address') and res_user[0].get('address'):
                data['personnel_info']['address'] = res_user[0]['address']
            else:
                data['personnel_info']['address'] = []
            if res_user[0]['personnel_info'].get('name'):
                pass
            elif res_user[0].get('name'):
                data['personnel_info']['name'] = res_user[0].get('name')
            else:
                data['personnel_info']['name'] = []

        return data

    def change_trade_status(self, status):
        '''修改付款状态'''
        status = None
        if status == '已付款':
            status = ThirdPartPayStatus.payed.value
        elif status == '未付款':
            status = ThirdPartPayStatus.unpay.value
        else:
            return False
        bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                   TypeId.serviceOrder.value, {'id': '', 'pay_status': status}, 'PT_Service_Record')
        if bill_id:
            res = 'Success'

        return res

    def _create_product_order(self, data, is_paying_first, pay_status=ThirdPartPayStatus.unpay.value, all_product=None, serial_number=None):
        '''创建订单（服务订单表）'''
        '''
        data: dict 订单数据
        data:{
            'service_product_id'：产品id,
            'count':产品数量,
            'name':产品名,
            'appointment_time'
        }
        is_paying_first : 0 or 1  是否先付费
        pay_status 支付状态
        return res
        '''
        order_data = {}
        res = "Fail"
        date_time = get_cur_time().strftime("%Y%m%d")
        t22 = time.time()
        if serial_number == None:
            def process_func(db):
                nonlocal serial_number
                serial_number = get_serial_number(
                    db, SerialNumberType.order.value)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
        order_code = 'D' + date_time + serial_number
        t23 = time.time()
        if 'name' in data.keys():
            person_id = data.get("name")
        else:
            person_id = get_current_user_id(self.session)
        order_data['purchaser_id'] = person_id
        order_data['order_code'] = order_code
        # 查询长者的长者类型存一下
        _filter_user = MongoBillFilter()
        _filter_user.match_bill((C('id') == person_id)).project({'_id': 0})
        res_user = self.query(_filter_user, 'PT_User')
        if len(res_user) > 0:
            if 'personnel_info' in res_user[0] and 'personnel_classification' in res_user[0]['personnel_info']:
                order_data['personnel_classification'] = res_user[0]['personnel_info']['personnel_classification']
        # 操做者id
        order_data['operator_id'] = get_current_user_id(self.session)
        pro_id = data.get("service_product_id")
        # 服务地址
        order_data['service_address'] = data.get("address")
        # 长者联系电话
        order_data['elder_telephone'] = data.get("telephone")
        if 'count' in data.keys() and data.get('count'):
            count = data.get('count')
        else:
            count = 1
        if all_product != None and pro_id in all_product.keys():
            res_product = [all_product[pro_id]]
        else:
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == pro_id)).project({'_id': 0})
            res_product = self.query(
                _filter, 'PT_Service_Product')
        service_product_list = {}
        try:
            if len(res_product) > 0:
                order_data['service_provider_id'] = res_product[0].get(
                    'organization_id')
                order_data['organization_id'] = res_product[0].get(
                    'organization_id')
                if 'service_product' in res_product[0]:
                    for p in res_product[0].get('service_product'):
                        if 'service_package_price' in p:
                            service_product_list[p['product_id']
                                                 ] = p['service_package_price']
            else:
                order_data['service_provider_id'] = ''
            if 'order_date' in data.keys() and data.get('order_date'):
                order_data['order_date'] = as_date(data.get('order_date'))
            else:
                order_data['order_date'] = datetime.now()
            product_list = self.get_all_product({'id': pro_id}, all_product)
            new_product_list = []
            record_list = []
            if product_list:
                for product in product_list:
                    pro_dict = {}
                    pro_dict['product_id'] = product.get('id')
                    pro_dict['product_name'] = product.get('name')
                    pro_dict['valuation_formula'] = product.get(
                        'valuation_formula')
                    pro_dict['service_option'] = product.get('service_option')
                    pro_dict['contract_terms'] = product.get(
                        'additonal_contract_terms')
                    pro_dict['remark'] = product.get('remark')
                    # 添加是否可分派
                    pro_dict['is_assignable'] = product.get('is_assignable')
                    pro_dict['count'] = count
                    pro_dict['all_times'] = product.get('remaining_times')

                    # 是套餐且套餐中存在service_package_price，则带上，没有则不带
                    if pro_dict['product_id'] in service_product_list:
                        pro_dict['service_package_price'] = service_product_list[pro_dict['product_id']]
                    new_product_list.append(pro_dict)
            order_data['pay_state'] = pay_status
            if 'order_state' in data.keys():
                order_data['status'] = data.get('order_state')
            else:
                order_data['status'] = OrderStatus.order_unserved.value
            order_data['detail'] = new_product_list
            order_new_data = get_info(order_data, self.session)
            order_new_data['is_paying_first'] = is_paying_first
            # 补充原有的数据
            origin_data = {}
            origin_data['product_id'] = res_product[0].get('id')
            origin_data['product_name'] = res_product[0].get('name')
            order_new_data['origin_product'] = origin_data
            # 补充
            if 'address' in data.keys():
                order_new_data['address'] = data.get('address')
            # order_new_data['order_state'] = data.get('order_state')
            if 'amout' in data.keys():
                order_new_data['amout'] = int(data.get('amout'))
            if 'comment' in data.keys():
                order_new_data['comment'] = data.get('comment')
            if 'buy_type' in data.keys():
                order_new_data['buy_type'] = data.get('buy_type')
            if 'appointment_time' in data.keys():
                order_new_data['buy_type'] = data.get('appointment_time')

            if is_paying_first == '1':
                for product in product_list:
                    record_dict = {}
                    record_dict['order_id'] = order_new_data.get('id')
                    record_dict['service_product_id'] = product.get('id')
                    record_dict['start_date'] = order_new_data.get(
                        'create_date')
                    record_dict['end_date'] = order_new_data.get('create_date')
                    record_dict['is_virtual'] = '1'
                    record_dict['status'] = RecordStatus.service_completed.value
                    record_dict['valuation_amount'] = product.get(
                        'valuation_amount')
                    record_dict['content'] = product.get('introduce')
                    if product.get('service_option'):
                        option_list = []
                        for option in product.get('service_option'):
                            if option.get('option_type') == 'service_before':
                                option_list.append(option)
                        record_dict['service_option'] = option_list
                    record_list.append(record_dict)
                return {'order_data': order_new_data, 'record_data': record_list}
            else:
                return {'order_data': order_new_data, 'record_data': []}

        except Exception as e:
          # print(e)
            pass
        return res

    def get_new_serial_number(self, number, max_length):
        '''组装流水号方法'''
        number = str(number)
        max_length = max_length
        random_length = max_length - len(number)
        random_str = ''
        if random_length >= 0:
            for i in range(random_length):
                random_str += '0'
            serial_value = random_str + number
        elif random_length < 0:
            serial_value = number
        return serial_value

    def create_product_order(self, data, is_paying_first='1', pay_status=ThirdPartPayStatus.unpay.value):
        '''
        data: list or dict 订单数据
        is_paying_first : 0 or 1  是否先付费
        return res
        '''
        res = "Fail"
        result = {}
        # 先查询一个全产品的对象
        t4 = time.time()
        all_product = {}
        _filter = MongoBillFilter()
        _filter.match_bill()\
            .project({'_id': 0})
        res_product = self.query(_filter, 'PT_Service_Product')
        if len(res_product) > 0:
            for product_data in res_product:
                all_product[product_data['id']] = product_data
        t5 = time.time()
      #print('订单：查询所有产品的时间》》', t5-t4)

        if type(data) == list:
            # 处理订单编号
            t1 = time.time()
            _filter = MongoBillFilter()
            _filter.match((C('type') == SerialNumberType.order.value))\
                .project({'_id': 0})
            serial_mes = self.query(
                _filter, 'PT_Serial_Number')
            t2 = time.time()
            #print('订单：查询流水表》》', t2-t1)
            if len(serial_mes) > 0:
                number = serial_mes[0]['number']
                max_length = serial_mes[0]['max_length']
            else:
                # 没有的话默认为-1，用于后面循环内部判断使用随机数
                number = -1

            insert_order_list = []
            insert_record_list = []
            try:
                t31 = time.time()
                for per_data in data:
                    if number == -1:
                        new_number = str(uuid.uuid1())
                    else:
                        # 生成+1的流水号
                        new_number = self.get_new_serial_number(
                            number, max_length)
                        number = number + 1
                    result = self._create_product_order(
                        per_data, is_paying_first, pay_status, all_product, new_number)
                    if result.get('order_data') and result.get('record_data'):
                        insert_order_list.append(result.get('order_data'))
                        insert_record_list.append(result.get('record_data'))
                        # bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                        #                                            TypeId.serviceOrder.value, [result.get('order_data'), result.get('record_data')], ['PT_Service_Order', 'PT_Service_Record'])
                        # if bill_id:
                        #     res = 'Success'
                    elif result.get('order_data') and (not result.get('record_data')):
                        insert_order_list.append(result.get('order_data'))
                        # bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                        #                                            TypeId.serviceOrder.value, result.get('order_data'), 'PT_Service_Order')
                        # if bill_id:
                        #     res = 'Success'
                t32 = time.time()
              #print('可变的时间》》', t32-t31)
                t14 = time.time()
            finally:
                t11 = time.time()
                # 流水表更新流水字段
                serial_mes[0]['number'] = number
                self.update({'id': serial_mes[0]['id']},
                            serial_mes[0], 'PT_Serial_Number')
                t12 = time.time()
              #print('订单：更新流水表》》', t12-t11)
            if len(insert_order_list) > 0:
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.serviceOrder.value, insert_order_list, ['PT_Service_Order'])
                if bill_id:
                    res = 'Success'
            if len(insert_record_list) > 0:
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.serviceOrder.value, insert_record_list, ['PT_Service_Record'])
                if bill_id:
                    res = 'Success'

            t15 = time.time()
            #print('订单：插入订单表》》', t15-t14)

        else:
            result = self._create_product_order(
                data, is_paying_first, pay_status, all_product)
            if result != 'Fail' and result.get('order_data') and result.get('record_data'):
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.serviceOrder.value, [result.get('order_data'), result.get('record_data')], ['PT_Service_Order', 'PT_Service_Record'])
                if bill_id:
                    res = 'Success'
            elif result != 'Fail' and result.get('order_data') and (not result.get('record_data')):
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.serviceOrder.value, result.get('order_data'), 'PT_Service_Order')
                if bill_id:
                    res = 'Success'
            else:
                res = '下单失败'

        # 添加使用记录
        if res == 'Success':
            # 插入一个消息提醒
            self.MessageManageService.add_new_message({
                # 购买信息标识
                "business_type": 'service_order',
                # 业务ID
                "business_id": result['order_data']['id']
            })
            real_account_data = OperationRecordObject(
                get_current_user_id(self.session), OperationRecordType.order, result['order_data']['id'], result['order_data']['origin_product']['product_name'])
            self.OperationRecordService.insert_record(
                real_account_data.to_dict())
        return res

    def batch_create_order(self, data, price, is_paying_first='0'):
        try:
            person_id = get_current_user_id(self.session)
            if 'name' in data.keys():
                person_id = data.get("name")
            res_account = []
            # 查询该长者的补贴账户
            if data.get("buy_type") == '补贴账户':
                _filter_account = MongoBillFilter()
                _filter_account.match_bill((C('user_id') == person_id) & (
                    C('account_type') == '补贴账户')).project({'_id': 0})
                res_account = self.page_query(
                    _filter_account, 'PT_Financial_Account', None, None)
                # 判断该长者是否有补贴账户
                if len(res_account['result']) <= 0:
                    return '该用户没有补贴账户'
            else:
                _filter_account = MongoBillFilter()
                _filter_account.match_bill((C('user_id') == person_id) & (
                    C('account_type') == '慈善账户')).project({'_id': 0})
                res_account = self.page_query(
                    _filter_account, 'PT_Financial_Account', None, None)
            self.account_id = res_account['result'][0]['id']
            self.amount = res_account['result'][0]['balance']

            # price = 0
            # # 服务产品的总价格需要服务选项累加起来
            # if res_product['result']:
            #     if res_product['result'][0]['is_service_item'] == 'false':
            #         pruduct_detail = self.service_product_detail_service.get_service_product_package_detail(
            #             {'id': pro_id})
            #         if len(pruduct_detail) > 0 and 'total_price' in pruduct_detail[0]:
            #             price = pruduct_detail[0].get(
            #                 'total_price')
            #         else:
            #             price = float(res_product['result'][0]['service_package_price'])
            #     else:
            #         for opn in res_product['result'][0]['service_option']:
            #             price += float(opn['option_value'])
            if type(price) == str:
              # print(111111111111111)
                price = float(price)
            if price == None or price == '':
                return '订单金额设置不正确'
            # 判断余额是否充足
            if res_account['result'][0]['balance'] < price:
                return '账户余额不足'
            self.balance = price
            res = self.create_product_order(
                data, is_paying_first, ThirdPartPayStatus.payed.value)
            if res == 'Success':
                self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.financial.value, {'id': self.account_id, 'balance': self.amount-self.balance}, 'PT_Financial_Account')
                self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.financial.value, {'balance': self.amount-self.balance, 'amount': (self.amount) * (-1), 'abstract': '服务购买', 'date': datetime.now(), 'remark': '', 'account_id': self.account_id}, 'PT_Financial_Account_Record')
            return res
        except Exception as e:
            self.logger.exception(e)
            return '系统异常，请联系管理员'

    def update_reserve_service(self, data):
        '''新增修改预约服务'''
        res = '保存失败'
        bill_id = ''
        try:
            # 新写的逻辑
            if 'id' in data.keys():
                # 编辑时 不允许修改长者信息和长者类型信息
                data_new = data.copy()
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.reserveService.value, data_new, 'PT_Reserve_Service')
            else:
                # 新增 ,先以长者为准去判断，存在长者id的先走单一长者逻辑新增，最后才走长者类型新增(暂时去掉)
                if 'name' in data.keys():
                    data_new = data.copy()
                    data_info = get_info(data_new, self.session)
                    _filter_reserve = MongoBillFilter()
                    _filter_reserve.match_bill(
                        (C('name') == data['name'])).project({'_id': 0})
                    res_reserve = self.query(
                        _filter_reserve, 'PT_Reserve_Service')
                    if len(res_reserve) > 0:
                        # 查询该长者记录是否已存在，存在则覆盖
                        # 20200608修改，如果存在，就返回【该长者已在预定服务内】
                        return 'Exists'
                        # data_info['id'] = res_reserve[0]['id']
                        # operation_type = OperationType.update.value
                    else:
                        operation_type = OperationType.add.value
                    bill_id = self.bill_manage_server.add_bill(
                        operation_type, TypeId.reserveService.value, data_info, 'PT_Reserve_Service')
                # if 'personnel_classification' in data.keys():
                #     return res
        except Exception as e:
            self.logger.exception(e)
        if bill_id:
            res = 'Success'
        return res

        # _filter_provider = MongoBillFilter()
        # _filter_provider.match_bill(
        #     (C('id') == data['service_provider_id'])).project({'_id': 0})
        # res_provider = self.query(_filter_provider, 'PT_User')
        # _filter_product = MongoBillFilter()
        # _filter_product.match_bill(
        #     (C('id') == data['service_product_id'])).project({'_id': 0})
        # res_product = self.query(_filter_product, 'PT_Service_Product')
        # if 'id' not in list(data.keys()):
        #     # 如果按照类型预服务
        #     if 'personnel_classification' in list(data.keys()):
        #         _filter_reserve = MongoBillFilter()
        #         _filter_reserve.match_bill(
        #             (C('personnel_classification') == data['personnel_classification'])).project({'_id': 0})
        #         res_reserve = self.query(_filter_reserve, 'PT_Reserve_Service')
        #         for reserve in res_reserve:
        #             self.bill_manage_server.add_bill(OperationType.delete.value,
        #                                              TypeId.assessmentProject.value, reserve['id'], 'PT_Reserve_Service')
        #         _filter_user = MongoBillFilter()
        #         _filter_user.match_bill(
        #             (C('personnel_info.personnel_classification') == data['personnel_classification'])).project({'_id': 0})
        #         res_user = self.query(_filter_user, 'PT_User')
        #         for user in res_user:
        #             data_new = data.copy()
        #             data_new['name'] = user['id']
        #             data_new['user_name'] = user['name']
        #             data_new['provider_name'] = res_provider[0]['name']
        #             data_new['provider_id'] = res_provider[0]['id']
        #             data_new['product_name'] = res_product[0]['name']
        #             data_new['product_id'] = res_product[0]['id']
        #             datas.append(get_info(data_new, self.session))
        #         bill_id = self.bill_manage_server.add_bill(
        #             OperationType.add.value, TypeId.reserveService.value, datas, ['PT_Reserve_Service'])
        #     else:
        #         _filter_reserve = MongoBillFilter()
        #         _filter_reserve.match_bill(
        #             (C('name') == data['name'])).project({'_id': 0})
        #         res_reserve = self.query(_filter_reserve, 'PT_Reserve_Service')
        #         if len(res_reserve) > 0:
        #             data_new = data.copy()
        #             data_new['id'] = res_reserve[0]['id']
        #             data_new['name'] = res_reserve[0]['name']
        #             data_new['user_name'] = res_reserve[0]['user_name']
        #             data_new['provider_name'] = res_reserve[0]['name']
        #             data_new['provider_id'] = res_reserve[0]['id']
        #             data_new['product_name'] = res_reserve[0]['name']
        #             data_new['product_id'] = res_reserve[0]['id']
        #             datas.append(get_info(data_new, self.session))
        #             bill_id = self.bill_manage_server.add_bill(
        #                 OperationType.add.value, TypeId.reserveService.value, datas, ['PT_Reserve_Service'])
        #         else:
        #             _filter_user = MongoBillFilter()
        #             _filter_user.match_bill(
        #                 (C('id') == data['name'])).project({'_id': 0})
        #             res_user = self.query(_filter_user, 'PT_User')
        #             data_new = data.copy()
        #             data_new['name'] = res_user[0]['id']
        #             data_new['user_name'] = res_user[0]['name']
        #             data_new['provider_name'] = res_provider[0]['name']
        #             data_new['provider_id'] = res_provider[0]['id']
        #             data_new['product_name'] = res_product[0]['name']
        #             data_new['product_id'] = res_product[0]['id']
        #             datas.append(get_info(data_new, self.session))
        #             bill_id = self.bill_manage_server.add_bill(
        #                 OperationType.add.value, TypeId.reserveService.value, datas, ['PT_Reserve_Service'])
        # else:
        #     _filter_user = MongoBillFilter()
        #     _filter_user.match_bill(
        #         (C('id') == data['name'])).project({'_id': 0})
        #     res_user = self.query(_filter_user, 'PT_User')
        #     data_new = data.copy()
        #     data_new['name'] = res_user[0]['id']
        #     data_new['user_name'] = res_user[0]['name']
        #     data_new['provider_name'] = res_provider[0]['name']
        #     data_new['provider_id'] = res_provider[0]['id']
        #     data_new['product_name'] = res_product[0]['name']
        #     data_new['product_id'] = res_product[0]['id']
        #     datas.append(get_info(data_new, self.session))
        #     bill_id = self.bill_manage_server.add_bill(
        #         OperationType.update.value, TypeId.reserveService.value, datas, ['PT_Reserve_Service'])
        # if bill_id:
        #     res = 'Success'
        # return res

    def get_reserve_service(self, condition, page=None, count=None):
        '''获取服务预约'''
        keys = ['id', 'user_name', 'provider_name',
                'product_name', 'user_id_card', 'personnel_classification_id', 'admin_area_id']

        values = self.get_value(condition, keys)
        admin_area_ids = N()
        # 查询行政区划及其下级的数组
        if 'admin_area_id' in condition.keys():
            admin_area_ids = []
            _filter = MongoBillFilter()
            _filter.match_bill(C('id') == values['admin_area_id'])\
                .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                .unwind('admin_info')\
                .match(C('admin_info.bill_status') == 'valid')\
                .project({'_id': 0, 'admin_id': '$admin_info.id'})
            query_res = self.query(_filter, 'PT_Administration_Division')
            admin_area_ids = [t['admin_id'] for t in query_res]
            admin_area_ids.append(condition['admin_area_id'])
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('user_name').like(values['user_name'])) & (
            C('provider_name').like(values['provider_name'])) & (
            C('product_name').like(values['product_name'])))\
            .lookup_bill('PT_User', 'name', 'id', 'user_info')\
            .match_bill((C('user_info.admin_area_id').inner(admin_area_ids)) & (C('user_info.id_card').like(values['user_id_card'])) & (C('user_info.personnel_info.personnel_classification') == values['personnel_classification_id']))\
            .lookup_bill('PT_Administration_Division', 'user_info.admin_area_id', 'id', 'division_info')\
            .add_fields({
                'admin_area_name': self.ao.array_elemat('$division_info.name', 0)
            })\
            .add_fields({
                'sort': self.ao.switch([self.ao.case(((F('status') == '成功')), 0)], 1),
            })\
            .sort({'sort': -1, 'modify_date': -1})\
            .project({
                'user_info.login_info': 0,
                'division_info': 0,
                **get_common_project({'user_info'})
            })
        res = self.page_query(_filter, 'PT_Reserve_Service', page, count)
        return res

    def delete_reserve_service(self, condition):
        '''删除预定服务'''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(condition, str):
                ids.append(condition)
            else:
                ids = condition
            for Reserve_Service_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Reserve_Service', {
                    'id': Reserve_Service_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.reserveService.value, data[0], 'PT_Reserve_Service')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def one_key_order(self, item_list):
        res = '下单失败，请联系管理员'
        now_month = datetime.now().month
        # 查询所有人员的补贴账户余额
        t11 = time.time()
        f_account = {}
        _filter = MongoBillFilter()
        _filter.match_bill((C('account_type') == '补贴账户') & (C('balance') > 0))\
            .sort({'date': -1})\
            .project({'_id': 0})
        res_account = self.query(_filter, 'PT_Financial_Account')
        if len(res_account) > 0:
            for account in res_account:
                if 'user_id' in account.keys() and 'balance' in account.keys() and 'id' in account.keys():
                    f_account[account['user_id']] = {
                        'id': account['id'], 'balance': account['balance']}

        t12 = time.time()
      #print('查询补贴账户数据》》', t12-t11)
        # 余额不足下单失败的长者
        data_no_list = []
        # 余额足够下单的长者
        data_list = []
        # 账户对象数组
        account_list = []
        # 账户流水对象数组
        account_record_list = []
        # 服务预约处理的对象数组
        reserve_list = []
        try:
            for item in item_list:
                status = '不更新'
                # # 查询长者账户余额
                account_balance = 0
                # res_balance = self.subsidy_account_recharg_func.get_account_balance(
                #     N(), {'user_id': item['name'], 'account_name': "补贴账户"}, None, None)
                if item['name'] in f_account.keys():
                    # if len(res_balance['result']) > 0:
                    account_id = f_account[item['name']]['id']
                    account_balance = f_account[item['name']]['balance']
                    # account_id = res_balance['result'][0]['id']
                    # account_balance = res_balance['result'][0]['balance']
                    # 判断该长者补贴账户是否有余额，有才去创建订单
                    if isinstance(item['count'], str):
                        item['count'] = int(item['count'])
                    if account_balance >= item['count']:
                        item['amout'] = item['count']
                        item['address'] = item['user_info'][0]['personnel_info']['address']
                        if 'user_info' in item.keys() and len(item['user_info']) > 0:
                            item['service_address'] = item['user_info'][0]['personnel_info']['address']
                            item['telephone'] = item['user_info'][0]['personnel_info']['telephone']
                        # 解决参数不对的问题
                        item['count'] = 1
                        data_list.append(item)
                        account_list.append(
                            {'id': account_id, 'balance': account_balance-item['amout']})
                        account_record_list.append({'balance': account_balance-item['amout'], 'amount': (
                            account_balance) * (-1), 'abstract': '服务购买', 'date': datetime.now(), 'remark': '', 'account_id': account_id})
                        if 'month' in item.keys() and item['month'] == now_month and 'status' in item.keys() and item['status'] == '成功':
                            status = '不更新'
                        else:
                            status = '成功'
                    else:
                        data_no_list.append(item['user_name'])
                        if 'month' in item.keys() and item['month'] == now_month and 'status' in item.keys() and item['status'] == '成功':
                            status = '不更新'
                        else:
                            status = '失败'
                else:
                    data_no_list.append(item['user_name'])
                    if 'month' in item.keys() and item['month'] == now_month and 'status' in item.keys() and item['status'] == '成功':
                        status = '不更新'
                    else:
                        status = '失败'
                if status != '不更新':
                    reserve_list.append(
                        {'id': item['id'], 'month': now_month, 'status': status})

            # 没有可创建订单的长者，而且有余额不足的，返回一个提示
            # if len(data_no_list) > 0 and len(data_list) == 0:
            #     return '长者【' + ','.join(data_no_list) + '】余额不足无法下单'

            if len(data_list) > 0:
                t11 = time.time()
                # 创建订单
                res = self.create_product_order(
                    data_list, '0', ThirdPartPayStatus.payed.value)
                t12 = time.time()
              #print('创建订单》》', t12-t11)
                if res == 'Success':
                    t14 = time.time()
                    self.bill_manage_server.add_bill(
                        OperationType.update.value, TypeId.financial.value, account_list, ['PT_Financial_Account'])
                    self.bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.financial.value, account_record_list, ['PT_Financial_Account_Record'])

                    t15 = time.time()
                  #print('更新账户表》》', t15-t14)
            # 更新服务记录表的每月下单状态
            if len(reserve_list) > 0:
                self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.financial.value, reserve_list, 'PT_Reserve_Service')
            # 存在没有可创建订单的长者，或者有余额不足的情况，返回一个提示
            if len(data_no_list) > 0:
                return '长者【' + ','.join(data_no_list) + '】余额不足无法下单,其余均成功！'
        except Exception as e:
            self.logger.exception(e)
        return res

    def all_buy_order(self):
        res = '下单失败，请联系管理员'
        # 查询所有人员的补贴账户余额
        t11 = time.time()
        f_account = {}
        _filter = MongoBillFilter()
        _filter.match_bill((C('account_type') == '补贴账户') & (C('balance') > 0))\
            .sort({'date': -1})\
            .project({'_id': 0})
        res_account = self.query(_filter, 'PT_Financial_Account')
        if len(res_account) > 0:
            for account in res_account:
                if 'user_id' in account.keys() and 'balance' in account.keys() and 'id' in account.keys():
                    f_account[account['user_id']] = {
                        'id': account['id'], 'balance': account['balance']}

        t12 = time.time()
      #print('查询补贴账户数据》》', t12-t11)
        now_month = datetime.now().month
        t13 = time.time()
        _filter = MongoBillFilter()
        _filter.match_bill((C('month').exists()) | (C('month') != now_month) | ((C('month') == now_month) & (C('status') != '成功')))\
            .sort({'date': -1})\
            .project({'_id': 0})
        item_list = self.query(_filter, 'PT_Reserve_Service')
      #print('需要下单服务预约的数量>>>>', len(item_list))
        t14 = time.time()
      #print('查询服务预约耗时》》', t14-t13)
        elder_ids = []
        if len(item_list) > 0:
            for item in item_list:
                elder_ids.append(item['name'])
        # 查询所有人员的补贴账户余额
        t01 = time.time()
        f_elder = {}
        _filter_elder = MongoBillFilter()
        _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者') & (C('id').inner(elder_ids)))\
            .sort({'date': -1})\
            .project({'_id': 0})
        res_elder = self.query(_filter_elder, 'PT_User')
        if len(res_elder) > 0:
            for elder in res_elder:
                if 'id' in elder.keys() and 'personnel_info' in elder.keys() and 'telephone' in elder['personnel_info'].keys() and 'address' in elder['personnel_info'].keys():
                    f_elder[elder['id']] = {'id': elder['id'], 'telephone': elder['personnel_info']
                                            ['telephone'], 'address': elder['personnel_info']['address']}

        t02 = time.time()
      #print('查询全部长者耗时》》', t02-t01)

        # 余额不足下单失败的长者
        data_no_list = []
        # 余额足够下单的长者
        data_list = []
        # 账户对象数组
        account_list = []
        # 账户流水对象数组
        account_record_list = []
        # 服务预约处理的对象数组
        reserve_list = []
        try:
            for item in item_list:
                status = '失败'
                # 查询长者账户余额
                account_balance = 0
                if item['name'] in f_account.keys():
                    account_id = f_account[item['name']]['id']
                    account_balance = f_account[item['name']]['balance']
                    # 判断该长者补贴账户是否有余额，有才去创建订单
                    if isinstance(item['count'], str):
                        item['count'] = int(item['count'])
                    if account_balance >= item['count']:
                        item['amout'] = item['count']
                        if 'name' in item.keys() and item['name'] in f_elder.keys():
                            item['address'] = f_elder[item['name']]['address']
                            item['service_address'] = f_elder[item['name']]['address']
                            item['telephone'] = f_elder[item['name']]['telephone']
                        # 解决参数不对的问题
                        item['count'] = 1
                        data_list.append(item)
                        account_list.append(
                            {'id': account_id, 'balance': account_balance-item['amout']})
                        account_record_list.append({'balance': account_balance-item['amout'], 'amount': (
                            account_balance) * (-1), 'abstract': '服务购买', 'date': datetime.now(), 'remark': '', 'account_id': account_id})
                        status = '成功'
                    else:
                        data_no_list.append(item['user_name'])
                else:
                    data_no_list.append(item['user_name'])
                reserve_list.append(
                    {'id': item['id'], 'month': now_month, 'status': status})
            if len(data_list) > 0:
              #print('生效的订单》》', len(data_list))
                t11 = time.time()
                # 创建订单
                res = self.create_product_order(
                    data_list, '0', ThirdPartPayStatus.payed.value)
                t12 = time.time()
              #print('创建订单》》', t12-t11)
                if res == 'Success':
                    t14 = time.time()
                    self.bill_manage_server.add_bill(
                        OperationType.update.value, TypeId.financial.value, account_list, ['PT_Financial_Account'])
                    self.bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.financial.value, account_record_list, ['PT_Financial_Account_Record'])

                    t15 = time.time()
                  #print('更新账户表》》', t15-t14)
            # 更新服务记录表的每月下单状态
            if len(reserve_list) > 0:
                self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.financial.value, reserve_list, 'PT_Reserve_Service')
            # 存在没有可创建订单的长者，或者有余额不足的情况，返回一个提示
            if len(data_no_list) > 0:
                return '长者【' + ','.join(data_no_list) + '】余额不足无法下单,其余均成功！'
        except Exception as e:
            self.logger.exception(e)
            #print('失败原因》》', e)
        return res

    def get_rt_order_list(self, condition, page=None, count=None):
        '''查询日托订单列表'''
        keys = ['id', 'user_name', 'provider_name',
                'product_name', 'user_id_card']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'org_info')\
            .add_fields({
                'org_name': '$org_info.name',
            })\
            .lookup_bill('PT_User', 'purchaser_id', 'id', 'user_info')\
            .add_fields({
                'user_name': '$user_info.name',
            })\
            .match_bill((C('org_info.organization_info.personnel_category') == '幸福院') & (C('org_info.personnel_type') == '2'))\
            .add_fields({
                'product_name': '$detail.product_name',
            })\
            .add_fields({
                'product_id': '$detail.product_id',
            })\
            .lookup_bill('PT_Service_Product', 'product_id', 'id', 'pro_info')\
            .add_fields({
                'service_item_id': '$pro_info.service_item_id',
            })\
            .lookup_bill('PT_Service_Item', 'service_item_id', 'id', 'item_info')\
            .add_fields({
                'item_type': '$item_info.item_type',
            })\
            .lookup_bill('PT_Service_Type', 'item_type', 'id', 'type_info')\
            .add_fields({
                'type_name': '$type_info.name',
            })\
            .project({'_id': 0, 'org_info._id': 0, 'user_info._id': 0, 'pro_info._id': 0, 'item_info._id': 0, 'type_info._id': 0})
        res = self.page_query(_filter, 'PT_Service_Order', page, count)
        return res

    def update_rt_order(self, data):
        data['order_date'] = as_date(data['order_date'])
        if 'id' in data.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.personnelClassification.value, data, 'PT_Service_Order')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.personnelClassification.value, data, 'PT_Service_Order')
            if bill_id:
                res = 'Success'
        return res

#############################################################

    # def add_comfirm_order(self, data):
    #     '''创建订单（服务订单表）'''
    #     order_data = {}
    #     res = "Fail"
    #     person_id = get_current_user_id(self.session)
    #     pk_id = data.get("service_pack_id")
    #     _filter = MongoFilter()
    #     _filter.match((C('bill_status') == 'valid') & (
    #         C('id') == pk_id)).project({'_id': 0})
    #     res_pk = self.page_query(
    #         _filter, 'PT_Service_Item_Package', None, None)
    #     try:
    #         if res_pk['result']:
    #             order_data['service_provider_id'] = res_pk['result'][0].get(
    #                 'organization')
    #         else:
    #             order_data['service_provider_id'] = ''
    #         order_data['order_date'] = datetime.datetime.now()
    #         order_data['purchaser_id'] = person_id
    #         service_item = []
    #         service_package_info = self.service_product_detail_service.get_service_package_detail(
    #             pk_id)
    #         if service_package_info:
    #             for key, item in enumerate(service_package_info[0].get('service_item')):
    #                 item_dict = {}
    #                 item_dict['service_item'] = item.get('service_item')
    #                 item_dict['service_type'] = item.get('service_type')
    #                 item_dict['count'] = data.get('count')
    #                 service_options = []
    #                 for option in item.get('service_options'):
    #                     option_dict = {}
    #                     option_dict['id'] = option.get('id')
    #                     if data.get('service_item')[key].get('key'):
    #                         option_dict['value'] = float(
    #                             data.get('service_item')[key].get('value'))
    #                         option_dict['name'] = data.get('service_item')[
    #                             key].get('key').split('-')[0]
    #                     else:
    #                         option_dict['value'] = 0
    #                         option_dict['name'] = ''
    #                     service_options.append(option_dict)
    #                 item_dict['service_options'] = service_options
    #                 service_item.append(item_dict)
    #         else:
    #             return 'no service package'
    #         order_data['service_item'] = service_item
    #         order_data['status'] = OrderStatus.order_unserved.value
    #         bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
    #                                                    TypeId.serviceOrder.value, order_data, 'PT_Service_Order')
    #         if bill_id:
    #             res = 'Success'
    #     except Exception as e:
    #       #print(e)
    #     return res

    def reset_record(self, item_list):
        res = '重置失败，请联系管理员'
        # 只能重置accounting_confirm：待结算的服务记录，更新对应的task表
        # 失败的数据数组
        fail_data = []
        # 需要更新的数据数组
        update_record_datas = []
        # 需要更新的数据数组
        update_task_datas = []
        # 需要更新的数据数组
        update_order_datas = []
        for item in item_list:
            record_id = item['id']
            accounting_confirm = item['accounting_confirm']
            record_code = item['record_code']
            order_id = item['order_id']
            new_record_data = {}
            new_task_data = {}
            # 过滤符合条件的服务记录
            if accounting_confirm == '待结算':
                # 找出最新的生效数据
                _filter = MongoBillFilter()
                _filter.match_bill((C('id') == record_id))\
                    .project({'_id': 0, 'GUID': 0, 'version': 0, 'bill_status': 0, 'valid_bill_id': 0, 'bill_operator': 0, 'invalid_bill_id': 0})
                query_res = self.query(_filter, 'PT_Service_Record')
                if len(query_res) > 0:
                    new_record_data = query_res[0].copy()
                    new_record_data['status'] = '未服务'
                    new_record_data['start_date'] = None
                    new_record_data['end_date'] = None
                    # 查询task表是否有对应数据
                    # 找出task表最新的生效数据
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('task_object_id') == record_id))\
                        .project({'_id': 0, 'GUID': 0, 'version': 0, 'bill_status': 0, 'valid_bill_id': 0, 'bill_operator': 0, 'invalid_bill_id': 0})
                    task_res = self.query(_filter, 'PT_Task')
                    if len(task_res) > 0:
                        new_task_data = task_res[0].copy()
                        new_task_data['task_state'] = '待接收'
                        new_task_data['begin_photo'] = None
                        new_task_data['end_photo'] = None
                        new_task_data['start_date'] = None
                        new_task_data['end_date'] = None
                        new_task_data['remarks'] = None
                        new_task_data['start_position'] = None
                        new_task_data['end_position'] = None
                        new_task_data['task_process_record'] = None
                        # 判断服务记录对应订单是否已完成，是的话改为服务中
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('id') == order_id))\
                            .project({'_id': 0, 'GUID': 0, 'version': 0, 'bill_status': 0, 'valid_bill_id': 0, 'bill_operator': 0, 'invalid_bill_id': 0})
                        order_res = self.query(_filter, 'PT_Service_Order')
                        if len(order_res) > 0:
                            if order_res[0]['status'] == '已完成':
                                order_data = {'id': order_id, 'status': '服务中'}
                                update_order_datas.append(order_data)
                        # 重置回version为1的数据
                        update_record_datas.append(new_record_data)
                        update_task_datas.append(new_task_data)
                    else:
                        fail_data.append(record_code)
                else:
                    fail_data.append(record_code)
            else:
                fail_data.append(record_code)
        # 更新数据
        tables = []
        update_datas = []
        if len(update_record_datas) > 0 and len(update_task_datas) > 0:
            tables.append('PT_Service_Record')
            tables.append('PT_Task')
            update_datas.append(update_record_datas)
            update_datas.append(update_task_datas)
        if len(update_order_datas) > 0:
            tables.append('PT_Service_Order')
            update_datas.append(update_order_datas)

        if len(tables) > 0 and len(update_datas) > 0:
            print('tables>>', tables)
            print('update_datas>>', update_datas)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.personnelClassification.value, update_datas, tables)
            if bill_id:
                res = 'Success'
        if len(fail_data) > 0:
            res = '工单编号【' + ','.join(fail_data) + '】重置失败,其余均成功！'
        return res
