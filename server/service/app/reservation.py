# -*- coding: utf-8 -*-

'''
人员及组织机构管理函数
'''
from server.pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from server.pao_python.pao.service.security.security_service import RoleService
from server.pao_python.pao.service.security.security_utility import get_current_account_id
from server.service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from server.service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from server.service.buss_pub.personnel_organizational import User, UserType
from ...service.mongo_bill_service import MongoBillFilter


class ReservateUser:
    def create_user(self, user):
        ''' 创建用户 '''
        self.id_card = user.get('id_card')
        self.name = user.get('name')
        self.telephone = user.get('telephone')
        # self.sex = user.get('sex')
        self.address = user.get('address')
        self.personnel_category = '4'
        self.id_card_type = user.get('id_card_type') if user.get(
            'id_card_type') else '身份证'

    def get_personnel(self, personnel):
        ''' 获取人员 '''
        self.create_user(personnel)
        self.personnel_info = {'personnel_category': 4, **personnel}
        self.personnel_type = UserType.Personnel
        return self.__dict__

    web_reservation = '0'
    online_reservation = '1'
    finish_reservation = '2'


class ReservationService(MongoService):
    ''' 家属预约服务 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def add_reservation(self, data):
        '''新增预约
        Arguments:
            data   {dict}      条件
        '''
        data['id_card_type'] = "身份证"
        user = ReservateUser().get_personnel(data)
        # print(user)
        if data.get('id_card'):
            _filter = MongoFilter()
            _filter.match((C('id_card') == user.get('id_card')))\
                .project({'_id': 0})
            res_user = self.query(_filter, 'PT_User')
            if len(res_user) == 0:
                bill_id1 = self.bill_manage_service.add_bill(OperationType.add.value,
                                                             TypeId.user.value, user, 'PT_User')
                # print(bill_id1)
                _filter_reservation = MongoBillFilter()
                _filter_reservation.match_bill((C('basic_information.id_card') ==
                                                user.get('id_card'))).project({'_id': 0})
                res_reservation = self.query(
                    _filter_reservation, 'PT_Reservation_Registration')
                # print(res_reservation)
                if len(res_reservation) == 0 and bill_id1:
                    reservation_data = {}
                    prerson_data = {'name': data["name"], 'id_card': data['id_card'],
                                    'brith_day': data['date_birth'], 'sex': data['sex'], 'telephone': data['telephone']}
                    reservation_data["basic_information"] = prerson_data
                    reservation_data['reservation_status'] = ReservateUser.web_reservation
                    reservation_data = get_info(reservation_data, self.session)
                    bill_id2 = self.bill_manage_service.add_bill(OperationType.add.value,
                                                                 TypeId.reservationRegistration.value, reservation_data, 'PT_Reservation_Registration')
                    if bill_id2:
                        res = {'result': 'add success', "status": '1'}
                        return res
                else:
                    res = {'result': 'reserved already', "status": '-1'}
                    return res
            else:
                _filter_reservation = MongoBillFilter()
                _filter_reservation.match_bill((C('basic_information.id_card') ==
                                                user.get('id_card'))).project({'_id': 0})
                res_reservation = self.query(
                    _filter_reservation, 'PT_Reservation_Registration')
                if len(res_reservation) == 0:
                    reservation_data = {}
                    prerson_data = {'name': data["name"], 'id_card': data['id_card'],
                                    'brith_day': data['date_birth'], 'sex': data['sex'], 'telephone': data['telephone']}
                    reservation_data["basic_information"] = prerson_data
                    reservation_data['reservation_status'] = ReservateUser.web_reservation
                    reservation_data = get_info(reservation_data, self.session)
                    bill_id2 = self.bill_manage_service.add_bill(OperationType.add.value,
                                                                 TypeId.reservationRegistration.value, reservation_data, 'PT_Reservation_Registration')
                    if bill_id2:
                        res = {'result': 'add success', "status": '1'}
                        return res
                else:
                    res = {'result': 'reserved already', "status": '-1'}
                    return res
        else:
            res = {'result': "have whitespace didn't filled", "status": '-1'}
            return res
