from server.pao_python.pao.data import dataframe_to_list, process_db
from ...models.financial_manage import FinancialBook, FinancialAccount, FinancialRecord, FinancialSubject, FinancialVoucher, FinancialEntry, FinancialCharge, FinancialSetSubjectItem
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
from ...service.common import get_info, get_current_user_id
from server.pao_python.pao.data import string_to_date, date_to_string
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
import pandas as pd
import uuid
import copy
import datetime
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:44:52
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\app\mine_home.py
'''
'''
说明
 1、我的页面服务
'''


class MineHomeService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_my_info(self):
        '''个人信息'''
        person_id = get_current_user_id(self.session)
        _filter = MongoFilter()
        _filter.match((C('bill_status') == 'valid') & (
            C('id') == person_id)).project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        return res
