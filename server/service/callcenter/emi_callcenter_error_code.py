from enum import Enum


class EmiCallCenterErrorCodeEnum(Enum):
    # 云总机开发凭条系统级错误
    E_100000 = ('600000', '目前尚不能提供的待开发功能')
    E_100001 = ('600001', '内部数据库访问失败')
    E_100002 = ('600002', '上传语音文件时间创建目录失败')
    E_100003 = ('600003', '上传语音文件是存储文件失败')
    E_100004 = ('600004', '系统内存分配失败')
    # 云总机服务平台错误
    E_100500 = ('600500', '创建云总机企业分机用户失败')
    E_100501 = ('600501', '更新云总机企业分机用户信息失败')
    E_100502 = ('600502', '调用云总机 EP_PROFILE 接口失败')
    E_100503 = ('600503', '获取云总机企业（用户）信息失败')
    E_100504 = ('600504', '删除云总机企业分机失败')
    E_100505 = ('600505', '与云总机的 HTTP 连接失败，或返回错误值')
    E_100506 = ('600506', '向云总机获取通话录音失败')
    E_100507 = ('600507', '向云总机申请直拨通话时，未返回总机号')
    E_100508 = ('600508', '向云总机获取用户信息失败')
    E_100509 = ('600509', '云总机创建技能组失败')
    E_100510 = ('600510', '云总机删除技能组失败')
    E_100511 = ('600511', '云总机添加技能组用户失败')
    E_100512 = ('600512', '云总机删除技能组用户失败')
    E_100513 = ('600513', '云总机呼叫中心接口返回错误')
    E_100514 = ('600514', '云总机更新工作时间失败')
    E_100515 = ('600515', '云总机删除工作时间失败')
    E_100516 = ('600516', '录音文件不存在')
    E_100517 = ('600517', '云总机访问录音授权服务器失败')
    E_100518 = ('600518', '云总机企业不存在')
    E_100519 = ('600519', '云总机没有分机号可分配')
    E_100520 = ('600520', '云总机没有设置工作时间')
    E_100521 = ('600521', '非法的工作时间')
    E_100522 = ('600522', '云总机不存在该工作时间方案')
    E_100523 = ('600523', '向呼叫中心企业签入失败')
    E_100524 = ('600524', '未开启呼叫中心功能')
    E_100525 = ('600525', 'VoIP 模式下，座席客户端（软电话）未注册')
    E_100526 = ('600526', '被叫号码超过呼叫次数限制（开启呼出防骚扰）')
    E_100527 = ('600527', '被叫非法（不能是本企业总机号）')
    E_100528 = ('600528', '移动座席模式，不能设置状态或进行转接')
    E_100529 = ('600529', '指定通话不存在')
    E_100530 = ('600530', '座席无监控权限')
    E_100531 = ('600531', '座席已经被监控')
    E_100532 = ('600532', '企业分机号已被使用')
    E_100533 = ('600533', '企业分机号不在有效分机号段内')
    E_100534 = ('600534', '手机号码不符合策略，禁止添加')
    E_100535 = ('600535', '创建的用户数目已经达到上限')
    E_100536 = ('600536', '异地号码无法绑定企业用户')
    E_100537 = ('600537', '非本网手机号无法绑定企业用户')
    E_100538 = ('600538', '不允许电话直拨')
    E_100539 = ('600539', '不允许网络通话')
    E_100540 = ('600540', '不允许总机回拨')
    E_100541 = ('600541', '呼叫分机号不存在')
    E_100542 = ('600542', '电子邮件地址已经存在')
    E_100543 = ('600543', '直线号码不属于用户所属企业')
    E_100544 = ('600544', '添加直线号码失败')
    E_100545 = ('600545', '没有权限设置呼叫方式')
    E_100546 = ('600546', '分机号码位数不符合设置')
    E_100547 = ('600547', '录音已经过期')
    E_100548 = ('600548', '录音文件尚在上传')
    E_100549 = ('600549', '没有找到父技能组')
    E_100550 = ('600550', '企业无外线总机号码')
    E_100551 = ('600551', '总机号码欠费')
    E_100552 = ('600552', '外呼类型受限（本地，长途，国际长途）')
    E_100553 = ('600553', '不在工作工作时间内')
    E_100554 = ('600554', '分机通话分钟数已用完')
    E_100555 = ('600555', '单位时间内拨打中国移动号码超过限制（上海联通）')
    E_100556 = ('600556', '语音文件不存在')
    E_100557 = ('600557', '语音文件不可用')
    E_100558 = ('600558', '号码不在白名单内')
    E_100559 = ('600559', '校验白名单失败（向运维查询号码是否在白名单失败）')
    E_100560 = ('600560', '监控不支持内线互拨场景')
    E_100561 = ('600561', '呼叫模式不匹配（不是呼叫中心企业）')
    E_100562 = ('600562', '未绑定 sip 话机')
    E_100563 = ('600563', 'sip 话机不在线')
    E_100564 = ('600564', '创建外呼任务失败')
    E_100565 = ('600565', '删除外呼任务失败')
    E_100566 = ('600566', '更新外呼任务失败')
    E_100567 = ('600567', '添加外呼任务的批次失败')
    E_100568 = ('600568', '删除外呼任务的批次的任务号码失败')
    E_100569 = ('600569', '查询外呼任务的批次的任务号码失败')
    E_100570 = ('600570', '查询外呼任务的批次的状态失败')
    E_100571 = ('600571', '启动外呼任务失败')
    E_100572 = ('600572', '结束外呼任务失败')
    E_100573 = ('600573', '暂停外呼任务失败')
    E_100574 = ('600574', '监控外呼任务失败')
    E_100575 = ('600575', '获取外呼任信息失败')
    E_100576 = ('600576', '获取外呼任务列表失败')
    E_100577 = ('600577', '获取批次信息失败')
    E_100578 = ('600578', '获取批次列表失败')
    E_100579 = ('600579', '没有找到技能组')
    E_100580 = ('600580', '没有该外呼任务')
    E_100581 = ('600581', '企业未开启')
    E_100582 = ('600582', '缺少任务的批次 id')
    E_100583 = ('600583', '没有任务号码（外呼任务中的客户号码）')
    E_100584 = ('600584', '任务启动时间不在企业的工作时间范围内')
    E_100585 = ('600585', '非进行中的任务不允许暂停')
    E_100586 = ('600586', '外呼任务名称不能为空')
    E_100587 = ('600587', '外呼时间段必须要 08:00-20:00 之间')
    E_100588 = ('600588', '外呼时间段不能超出企业工作时间范围')
    E_100589 = ('600589', '外呼速率必须在 0.8-1.2 之间')
    E_100590 = ('600590', '外呼任务名称已经存在')
    E_100591 = ('600591', '未完成的任务数已经达到上限')
    E_100592 = ('600592', '进行中或已完成的任务不允许该操作')
    E_100593 = ('600593', '缺少任务 id')
    E_100594 = ('600594', '进行中或正在导入号码的任务不允许删除')
    E_100595 = ('600595', '已完成，进行中，正在导入号码的任务不允许启动')
    E_100596 = ('600596', '未开始，已完成，正在导入号码的任务不允许结束')
    E_100597 = ('600597', '非 SIP 分机的分机号码不能在 SIP 分机号段内')
    E_100598 = ('600598', '当前任务不存在该批次')
    E_100599 = ('600599', '进行中，已完成，正在导入号码的批次不允许删除任务号码')
    E_100600 = ('600600', '任务名称格式错误')
    E_100601 = ('600601', '坐席已经存在于其他未完成的任务中')
    E_100602 = ('600602', '开始时间不能晚于结束时间')
    E_100603 = ('600603', '启动或暂停或结束任务失败（无此操作）')
    E_100604 = ('600604', '启动或暂停或结束任务失败（操作不能为空）')
    E_100605 = ('600605', '无坐席参与该任务')
    E_100606 = ('600606', '更新分机信息失败（callintype 不存在）')
    E_100607 = ('600607', 'SIP 话机分机号码不存在')
    E_100608 = ('600608', '临时坐席，在线坐席，号码不在白名单中的坐席不能参与批量外呼任务')
    E_100609 = ('600609', '缺少用户 Id，分机号，手机号或者工号')
    E_100610 = ('600610', '企业管理员用户名或密码错误')
    E_100611 = ('600611', '不在批量外呼任务的外呼时间段内')
    E_100612 = ('600612', '上传批次号码失败')
    E_100613 = ('600613', '手机号码不能为企业的直线号码或总机号码')
    E_100614 = ('600614', '回拨话机号码不能为企业的直线号码或总机号码')
    E_100615 = ('600615', '获取验证码失败')
    E_100616 = ('600616', '验证用户手机号码失败')
    E_100617 = ('600617', '用户手机号码尚未验证通过')
    E_100618 = ('600618', '用户手机号码改变，需重新验证')
    E_100619 = ('600619', '用户手机号码已经验证通过')
    E_100620 = ('600620', '暂时不允许重新获取验证码')
    E_100621 = ('600621', '验证码已经过期')
    E_100622 = ('600622', '生成验证码失败')
    E_100623 = ('600623', '针对该用户，尚未生成过验证码')
    E_100624 = ('600624', '验证码格式错误')
    E_100625 = ('600625', '验证次数超过限制次数')
    E_100626 = ('600626', '输入的验证码不正确')
    E_100627 = ('600627', '座席已关停')
    E_100628 = ('600628', '用户不在白名单内')
    E_100629 = ('600629', '用户在黑名单中')
    E_100630 = ('600630', '呼叫某用户次数超限（防骚扰机制）')
    E_100631 = ('600631', '总机号码不可用')
    E_100632 = ('600632', '密码长度错误（6-20 位）')
    E_100633 = ('600633', '批量外呼上传语音文件失败')
    E_100634 = ('600634', '座席名已存在')
    E_100635 = ('600635', '批量外呼语音文件不存在')
    E_100636 = ('600636', '批量外呼任务名称超长（20 个字符）')
    E_100637 = ('600637', '预设外呼速率值错误（0.5-1.0）')
    E_100638 = ('600638', '座席不存在')
    E_100639 = ('600639', '当前不能重呼外呼任务')
    E_100640 = ('600640', '任务数据去重出错')
    E_100641 = ('600641', '企业呼叫次数超限')
    E_100642 = ('600642', '号码在全网黑名单中')
    E_100643 = ('600643', '服务器外呼数量超限')
    E_100644 = ('600644', '该企业不支持多呼功能')
    E_100645 = ('600645', '外呼被叫号码过多')
    # 用户接口HTTP访问请求错误
    E_101000 = ('601000', 'HTTP 请求包头无 Authorization 参数')
    E_101001 = ('601001', 'HTTP 请求包头无 Content-Length 参数')
    E_101002 = ('601002', 'Authorization 参数 Base64 解码失败')
    E_101003 = ('601003', 'Authorization 参数解码后的格式错误，正确格式：<AccountSid:Timestamp>，注意以“:”隔开')
    E_101004 = ('601004', 'Authorization 参数不包含认证账户 ID')
    E_101005 = ('601005', 'Authorization 参数不包含时间戳')
    E_101006 = ('601006', 'Authorization 参数的账户 ID 不正确（应与 URL 中的账户 ID 一致）')
    E_101007 = ('601007', 'HTTP 请求使用的账号不存在')
    E_101008 = ('601008', 'HTTP 请求使用的账号已关闭')
    E_101009 = ('601009', 'HTTP 请求使用的账号已被锁定')
    E_101010 = ('601010', 'HTTP 请求使用的账户尚未校验')
    E_101011 = ('601011', 'HTTP 请求使用的子账户不存在')
    E_101012 = ('601012', 'HTTP 请求的 sig 参数校验失败')
    E_101013 = ('601013', 'HTTP 请求包体没有任何内容')
    E_101014 = ('601014', 'HTTP 请求包体 XML 格式错误')
    E_101015 = ('601015', 'HTTP 请求包体 XML 包中的功能名称错误')
    E_101016 = ('601016', 'HTTP 请求包体 XML 包无任何有效字段')
    E_101017 = ('601017', 'HTTP 请求包体 Json 格式错误')
    E_101018 = ('601018', 'HTTP 请求包体 Json 包中的功能名称错误')
    E_101019 = ('601019', 'HTTP 请求包体 Json 包无任何有效字段')
    E_101020 = ('601020', 'HTTP 请求包体中缺少 AppId')
    E_101021 = ('601021', 'HTTP 请求包体中缺少子账号 ID')
    E_101022 = ('601022', 'HTTP 请求包体中的开始时间不正确')
    E_101023 = ('601023', 'HTTP 请求包体中的结束时间不正确')
    E_101024 = ('601024', 'HTTP 请求包体中缺少总机号码')
    E_101025 = ('601025', 'HTTP 请求包体中的总机号码格式不正确')
    E_101026 = ('601026', 'HTTP 请求包体中缺少企业管理员用户名')
    E_101027 = ('601027', 'HTTP 请求包体中缺少企业管理员用户密码')
    E_101028 = ('601028', 'HTTP 请求包体中的总机号码已被预置，无法使用')
    E_101029 = ('601029', 'HTTP 请求包体中缺少用户绑定手机号码')
    E_101030 = ('601030', 'HTTP 请求包体中手机号码格式错误')
    E_101031 = ('601031', 'HTTP 请求包体中缺少直线号码')
    E_101032 = ('601032', 'HTTP 请求包体中缺少被叫号码')
    E_101033 = ('601033', 'HTTP 请求包体中被叫号码格式错误')
    E_101034 = ('601034', 'HTTP 请求包体中被叫号码非法')
    E_101035 = ('601035', 'HTTP 请求包体中主叫号码格式错误')
    E_101036 = ('601036', 'HTTP 请求包体中主叫号码非法')
    E_101037 = ('601037', 'HTTP 请求包体中无主叫号码')
    E_101038 = ('601038', 'HTTP 请求包体中无验证码')
    E_101039 = ('601039', 'HTTP 请求包体中验证码格式错误')
    E_101040 = ('601040', 'HTTP 请求包体中缺少呼叫 ID（callId）')
    E_101041 = ('601041', 'HTTP 请求包体的子账户 ID 非法')
    E_101042 = ('601042', 'HTTP 请求包体中缺少语音 ID（voiceId）')
    E_101043 = ('601043', 'HTTP 请求包体中的语音 ID 不正确')
    E_101044 = ('601044', 'HTTP 请求包头的 Content-Length 值过大（应不大于1024X1024）')
    E_101045 = ('601045', 'HTTP 请求包体中缺少 numberA')
    E_101046 = ('601046', 'HTTP 请求包体中缺少 numberB')
    E_101047 = ('601047', 'numberA 或 numberB 格式错误')
    E_101048 = ('601048', '呼叫来显模式数值错误')
    E_101049 = ('601049', '请求更新的子账户不属于本应用')
    E_101050 = ('601050', '按键反馈字段（getFeedBack）不正确')
    E_101051 = ('601051', '按键反馈模式字段（feedBackMode）不正确')
    E_101052 = ('601052', '按键反馈键值范围不正确（keyRange）')
    E_101053 = ('601053', '用户分机号未输入')
    E_101054 = ('601054', '呼叫时间限制值格式错误')
    E_101055 = ('601055', 'HTTP 请求包中缺少语音文本内容（或为空）')
    E_101056 = ('601056', 'HTTP 请求包中的语音文本 Id 格式错误')
    E_101057 = ('601057', 'HTTP 请求包中无模板参数')
    E_101058 = ('601058', 'HTTP 请求包中的用户工号（座席工号）格式错误')
    E_101059 = ('601059', 'HTTP 请求包中的父技能组 ID 格式错误（必须是数字）')
    E_101060 = ('601060', 'HTTP 请求包中无技能组名称')
    E_101061 = ('601061', 'HTTP 请求包中无技能组 ID')
    E_101062 = ('601062', 'HTTP 请求包中的技能组 ID 格式错误（必须是数字）')
    E_101063 = ('601063', 'HTTP 请求包中无座席工号')
    E_101064 = ('601064', 'HTTP 请求包中的座席模式值非法')
    E_101065 = ('601065', 'HTTP 请求包中的座席状态值非法')
    E_101066 = ('601066', 'HTTP 请求包中无转接对象')
    E_101067 = ('601067', '座席签入时，HTTP 请求包中无设备号码')
    E_101068 = ('601068', '查询用户时未输入手机号或直线号码')
    E_101069 = ('601069', '按键反馈提示播放次数设置数值非法')
    E_101070 = ('601070', '按键反馈等待输入时间数值非法')
    E_101071 = ('601071', '座席签入接口，输入的 deviceNumber 无效')
    E_101072 = ('601072', '更新密码接口，未输入新密码')
    E_101073 = ('601073', '密码长度非法')
    E_101074 = ('601074', '密码中未包含数字')
    E_101075 = ('601075', '密码中未包含字母')
    E_101076 = ('601076', '密码中包含非法字符')
    E_101077 = ('601077', '呼出限制接口未输入限制类型')
    E_101078 = ('601078', '呼出限制接口限制类型错误')
    E_101079 = ('601079', '呼出限制接口日限制值超出范围')
    E_101080 = ('601080', '呼出限制接口周限制值超出范围')
    E_101081 = ('601081', '呼出限制接口月限制值超出范围')
    E_101082 = ('601082', 'HTTP 请求包中缺少短信签名')
    E_101083 = ('601083', '短信签名不符合规范（3-8 个 UTF-8 字符）')
    E_101084 = ('601084', 'HTTP 请求包中缺少签名类型字段')
    E_101085 = ('601085', '签名类型值不正确')
    E_101086 = ('601086', 'HTTP 请求包中缺少签名 ID')
    E_101087 = ('601087', '签名 ID 无效（必须是数字）')
    E_101088 = ('601088', 'HTTP 请求包中缺少短信模板 ID')
    E_101089 = ('601089', '短信模板 ID 无效（必须是数字）')
    E_101090 = ('601090', '短信模板文本类型错误')
    E_101091 = ('601091', 'HTTP 请求包中缺少短信模板内容')
    E_101092 = ('601092', '短信模板内容不符合规范')
    E_101093 = ('601093', 'HTTP 请求包中缺少短信发送目标手机号')
    E_101094 = ('601094', '短信发送目标手机号非法')
    E_101095 = ('601095', 'HTTP 请求包中缺少短信发送模板参数')
    E_101096 = ('601096', '短信发送模板参数不正确')
    E_101097 = ('601097', '短信定时发送时间格式不正确')
    E_101098 = ('601098', 'HTTP 请求包中缺少短信验证码参数')
    E_101099 = ('601099', '短信验证码参数无效（4-8 位数字）')
    E_101100 = ('601100', '短信模板参数非法')
    E_101101 = ('601101', '短信模板参数过多')
    E_101102 = ('601102', '缺少短信模板参数')
    E_101103 = ('601103', '传入参数个数与实际短信模板参数个数不相同')
    E_101104 = ('601104', '短信模板内容非法（长度太长或编码格式不正确）')
    E_101105 = ('601105', '验证码短信模板中缺少 vc 变量')
    E_101106 = ('601106', '验证码短信模板的 vc 变量过多')
    E_101107 = ('601107', '短信模板类型错误')
    E_101108 = ('601108', '短信发送目标号码数量过多（超过 10 个）')
    E_101109 = ('601109', '被叫号码重复（多被叫情况下）')
    E_101110 = ('601110', '配置虚拟号码时，未输入总机号')
    E_101111 = ('601111', '配置虚拟号码时，输入的总机号码格式错误')
    E_101112 = ('601112', '有效期值格式错误')
    E_101113 = ('601113', '企业总机号不存在')
    E_101114 = ('601114', '企业总机号码已被使用')
    E_101115 = ('601115', '输入号码不是总机号或直线号')
    E_101116 = ('601116', '没有星期参数')
    E_101117 = ('601117', '没有开始时间参数')
    E_101118 = ('601118', '没有结束时间参数')
    E_101119 = ('601119', '星期参数值非法')
    E_101120 = ('601120', '开始时间参数值非法')
    E_101121 = ('601121', '结束时间参数值非法')
    E_101122 = ('601122', '开始时间大于结束时间')
    E_101123 = ('601123', '云总机不是呼叫中心企业')
    E_101124 = ('601124', '语音模板参数非法')
    E_101125 = ('601125', '语音模板组数与被叫数量不匹配')
    E_101126 = ('601126', '语音通知模板参数变量数超过限制')
    E_101127 = ('601127', '获取通话记录接口过于频繁')
    E_101128 = ('601128', '获取通话记录接口最大条目数值非法（1-500）')
    E_101129 = ('601129', '虚拟号码格式错误')
    E_101130 = ('601130', '未输入虚拟号码对应的用户真实号码')
    E_101131 = ('601131', '配对号码 ID 值格式错误')
    E_101132 = ('601132', '虚拟号码 ID 值格式错误')
    E_101133 = ('601133', '未输入配对号码列表')
    E_101134 = ('601134', '未输入虚拟号码列表')
    E_101135 = ('601135', '未输入有效期（maxAge）参数')
    E_101136 = ('601136', '未输入服务号码')
    E_101137 = ('601137', '输入服务号码非法（非数字）')
    E_101138 = ('601138', '输入服务号码格式错误（')
    E_101139 = ('601139', '总机号码类型值错误')
    E_101140 = ('601140', '输入分机号格式错误')
    E_101141 = ('601141', 'HTTP 请求包中缺少话机类型参数')
    E_101142 = ('601142', '话机类型参数格式错误')
    E_101143 = ('601143', 'sip 话机号码格式错误')
    E_101144 = ('601144', 'HTTP 请求包中缺少外呼任务名称参数')
    E_101145 = ('601145', 'HTTP 请求包中缺少坐席工号参数')
    E_101146 = ('601146', '外呼速率超过上限')
    E_101147 = ('601147', '外呼速率低于下限')
    E_101148 = ('601148', 'HTTP 请求包中缺少外呼任务 Id 参数')
    E_101149 = ('601149', '外呼任务 Id 格式错误')
    E_101150 = ('601150', 'HTTP 请求包中缺少外呼批次 Id 参数')
    E_101151 = ('601151', 'HTTP 请求包中缺少任务号码参数')
    E_101152 = ('601152', '任务号码数量超过上限')
    E_101153 = ('601153', '外呼批次格式错误')
    E_101154 = ('601154', '一次获取任务号码的数量格式错误')
    E_101155 = ('601155', '获取任务号码的开始偏移值格式错误')
    E_101156 = ('601156', '坐席工号的数量超过上限')
    E_101157 = ('601157', '外呼速率格式错误')
    E_101158 = ('601158', '任务或批次的状态参数格式错误')
    E_101159 = ('601159', '任务或批次的状态参数超出范围')
    E_101160 = ('601160', '查询列表限制数目参数格式错误')
    E_101161 = ('601161', '自动接通参数格式错误')
    E_101162 = ('601162', '自动接通参数超出范围')
    E_101163 = ('601163', '表示一次最多获取列表条数的参数格式错误')
    E_101164 = ('601164', '表示一次最多获取列表条数的参数超出取值范围')
    E_101165 = ('601165', '表示一次最多获取批次号码的参数超出下限')
    E_101166 = ('601166', '表示一次最多获取批次号码的参数格式错误')
    E_101167 = ('601167', '智能路由不支持选择外显总机号码')
    E_101168 = ('601168', 'HTTP 请求包中缺少 SIP 分机号码参数')
    E_101169 = ('601169', '用户私有数据 userData 格式错误')
    E_101170 = ('601170', '来源 IP 不在 IP 白名单内')
    E_101171 = ('601171', '不可以指定该号码外呼')
    E_101172 = ('601172', '帐号已存在')
    E_101173 = ('601173', '日期信息不存在')
    E_101174 = ('601174', '日期格式错误')
    E_101175 = ('601175', '时间格式错误')
    E_101176 = ('601176', '工作时间信息不存在')
    E_101177 = ('601177', 'type 格式错误')
    E_101178 = ('601178', '工作状态值格式错误')
    E_101179 = ('601179', '总机号码配置开关格式错误')
    E_101180 = ('601180', '总机号码配置开关值错误')
    E_101181 = ('601181', '总机号码模式格式错误')
    E_101182 = ('601182', '总机号码模式值错误')
    E_101183 = ('601183', '总机号码外呼模式格式错误')
    E_101184 = ('601184', '总机号码外呼模式值错误')
    E_101185 = ('601185', '接听语音文件 id 格式错误')
    E_101186 = ('601186', '语音文件不存在')
    E_101187 = ('601187', '语音配置开关格式错误')
    E_101188 = ('601188', '语音配置开关值错误')
    E_101189 = ('601189', '任务去重出错')
    E_101190 = ('601190', '未输入文本内容')
    E_101191 = ('601191', '语音来源格式错误')
    E_101192 = ('601192', '语音来源值错误')
    E_101193 = ('601193', '工号和技能组 id 至少传一个')
    E_101194 = ('601194', '总机号码列表格式有误')
    E_101195 = ('601195', '被叫号码数量过多')
    E_101196 = ('601196', '多呼功能已关闭')
    # 账号和应用资源错误
    E_102000 = ('602000', '账户所属省份错误')
    E_102001 = ('602001', '账户关联企业错误')
    E_102002 = ('602002', '应用 ID（appId）不存在')
    E_102003 = ('602003', '应用 ID 与主账户不匹配')
    E_102004 = ('602004', '应用状态为关闭，无法再提供服务')
    E_102005 = ('602005', '子账户与应用 ID 不匹配')
    E_102006 = ('602006', '请求包体中的子账户 ID 不存在')
    E_102007 = ('602007', '子账户与应用 ID 不匹配')
    E_102008 = ('602008', '子账户尚未关联或添加专用云总机企业')
    E_102009 = ('602009', '总机号码找不到对应省份')
    E_102010 = ('602010', '用户应用服务器连接失败')
    E_102011 = ('602011', '主账户欠费')
    E_102012 = ('602012', '用户当天调用接口次数已经超过设定值')
    E_102013 = ('602013', '应用未上线，号码无呼叫权限')
    E_102014 = ('602014', '应用尚未有子账户')
    E_102015 = ('602015', '本应用没有该用户')
    # 通话错误
    E_102100 = ('602100', '通话被用户应用服务器拒绝')
    E_102101 = ('602101', '通话被叫数超限')
    E_102102 = ('602102', '无法根据 callId 获得通话记录')
    E_102103 = ('602103', '主叫号码无呼叫权限')
    E_102104 = ('602104', 'callId 对应的呼叫记录与所属账户不匹配')
    E_102105 = ('602105', '呼叫状态为“失败”')
    E_102106 = ('602106', '呼叫状态尚不是挂断状态')
    E_102107 = ('602107', '呼叫时长太短')
    E_102108 = ('602108', '呼叫记录因通话失败或异常而无通话录音')
    E_102109 = ('602109', '呼叫尚在录音中，无法获取录音')
    E_102110 = ('602110', '本次呼叫没有录音')
    E_102111 = ('602111', '用户无主账户呼叫权限')
    E_102112 = ('602112', '没有呼叫该被叫的权限（即被叫不是分机绑定号码）')
    E_102113 = ('602113', '账户没有取消通话的权限')
    E_102114 = ('602114', '被叫日呼叫次数超过限制值')
    E_102115 = ('602115', '被叫周呼叫次数超过限制值')
    E_102116 = ('602116', '被叫月呼叫次数超过限制值')
    E_102117 = ('602117', '企业单位时间内呼叫次数超过限制')
    E_102118 = ('602118', '主账户没有可用资费包')
    E_102119 = ('602119', '子账户没有可用资费包')
    E_102120 = ('602120', '无法取消发送语音通知和语音验证码')
    E_102121 = ('602121', '呼叫能力受限，无法呼叫')
    # 云总机企业信息错误
    E_102300 = ('602300', '请求必须使用子账户认证')
    E_102301 = ('602301', '子账户所属云总机企业不存在')
    E_102302 = ('602302', '子账户尚未绑定或添加云总机企业')
    E_102303 = ('602303', '参数中的分机号码或密码错误，认证失败')
    E_102304 = ('602304', '子账户已经绑定了云总机企业')
    E_102305 = ('602305', '云总机企业已被添加到另一个子账户')
    E_102306 = ('602306', '云总机企业是系统预置企业，无法删除')
    E_102307 = ('602307', '用户手机号已经在云总机中注册')
    E_102308 = ('602308', '直线号码已被其他用户使用')
    E_102309 = ('602309', '输入的直线号码非法')
    E_102310 = ('602310', '直线号码不属于用户所属企业')
    E_102311 = ('602311', '用户手机号码尚未在企业中注册')
    E_102312 = ('602312', '用户呼叫限制时间格式错误')
    E_102313 = ('602313', '没有中间号码用于号码保护')
    E_102314 = ('602314', '未找到输入的号码配对')
    E_102315 = ('602315', '号码已经配对')
    E_102316 = ('602316', '输入的云总机企业分机号不存在')
    E_102317 = ('602317', '座席工号已被注册')
    E_102318 = ('602318', '座席工号尚未注册')
    E_102319 = ('602319', '座席已经加入了该技能组')
    E_102320 = ('602320', '技能组尚不存在')
    E_102321 = ('602321', '技能组组名已存在')
    E_102322 = ('602322', '子账户绑定云总机属于通用企业，无法添加')
    E_102323 = ('602323', '配对号码不存在')
    E_102324 = ('602324', '配对号码 ID 不存在')
    E_102325 = ('602325', '配对号码已经绑定')
    E_102326 = ('602326', '虚拟号码不存在')
    E_102327 = ('602327', '虚拟号码 ID 不存在')
    E_102328 = ('602328', '虚拟号码已经存在')
    E_102329 = ('602329', '虚拟号绑定号码已存在')
    E_102330 = ('602330', '当前企业未找到此总机号')
    E_102331 = ('602331', '虚拟号码不属于该企业')
    E_102332 = ('602332', '配对号码不属于该企业')
    E_102333 = ('602333', '关闭任务号码文件失败')
    E_102334 = ('602334', '不是呼叫中心企业')
    E_102335 = ('602335', '生成 json 请求文件失败')
    E_102336 = ('602336', '被风控关停坐席不可以删除')
    E_102337 = ('602337', '被风控关停坐席不可以编辑')
    E_102338 = ('602338', '企业通话并发过大')
    # 语音通知错误
    E_102400 = ('602400', '语音文件与应用 ID 不匹配')
    E_102401 = ('602401', '语音文件已被另一个企业使用')
    E_102402 = ('602402', '没有语音文本字段或语音文本长度为 0')
    E_102403 = ('602403', '未找到语音通知要使用的语音文本')
    E_102404 = ('602404', '语音文本长度超限')
    E_102405 = ('602405', '语音文本总数超限')
    E_102406 = ('602406', '语音文件个数超限')
    E_102407 = ('602407', '语音文件尚未审核')
    E_102408 = ('602408', '语音文件未审核通过')
    E_102409 = ('602409', '语音文本或模板尚未审核')
    E_102410 = ('602410', '语音文本或模板未审核通过')
    E_102411 = ('602411', '输入的是否模板参数值非法')
    E_102412 = ('602412', '语音模板参数数量为 0 或已经超限')
    E_102413 = ('602413', '语音文本模板参数数量与模板不符')
    E_102414 = ('602414', '语音文本模板，参数缺少或顺序有误')
    E_102415 = ('602415', '模板参数长度超过规定值')
    E_102416 = ('602416', '语音文件格式不符合规定')
    E_102417 = ('602417', '文本转语音转换失败')
    E_102418 = ('602418', '文本正在转语音过程中')
    # 呼叫中心接口错误
    E_102500 = ('602500', '座席工号不存在')
    E_102501 = ('602501', '呼叫中心内部错误')
    E_102502 = ('602502', '当前座席忙')
    E_102503 = ('602503', '呼叫中心不在工作时间')
    E_102504 = ('602504', '当前座席不是回拨模式')
    E_102505 = ('602505', '座席尚未签入')
    E_102506 = ('602506', '获取外线号码失败')
    E_102507 = ('602507', '云总机外呼失败')
    E_102511 = ('602511', '通话已经结束或不存在')
    E_102512 = ('602512', '座席不是班长，无监控权限')
    E_102513 = ('602513', '座席不属于该技能组')
    E_102514 = ('602514', '非新架构企业')
    E_102515 = ('602515', '坐席不在线')
    # API主账号管理工具相关接口错误
    E_102600 = ('602600', '手机号码已经存在')
    E_102601 = ('602601', '邮箱已经存在')
    E_102602 = ('602602', '省份 id 错误')
    E_102603 = ('602603', 'HTTP 请求包中缺少主账号 id 参数')
    E_102604 = ('602604', '主账户不是超级账户')
    E_102605 = ('602605', '认证 token 错误')
    E_102606 = ('602606', 'HTTP 请求包中缺少应用名称参数')
    E_102607 = ('602607', 'HTTP 请求包中缺少邮箱地址参数')
    E_102608 = ('602608', 'HTTP 请求包中缺少省份 id 参数')
    # 短信发送管理接口错误
    E_102700 = ('602700', '短信发送失败')
    E_102701 = ('602701', '本应用添加短信签名数已达上限（默认 16）')
    E_102702 = ('602702', '本应用添加短信模板数已达上限（默认 16）')
    E_102703 = ('602703', '应用 ID 不匹配')
    E_102704 = ('602704', '模板和签名类型不匹配')
    E_102705 = ('602705', '模板尚未通过审核')
    E_102706 = ('602706', '模板已经绑定签名')
    E_102707 = ('602707', '短信发送超时')
    E_102708 = ('602708', '签名不存在')
    E_102709 = ('602709', '模板不存在')
    E_102710 = ('602710', '模板尚未绑定签名')
    E_102711 = ('602711', '签名重复')
    E_102712 = ('602712', 'HTTP 请求包中缺少短信账号名参数')
    E_102713 = ('602713', 'HTTP 请求包中缺少短信账号密码参数')
    E_102714 = ('602714', '不支持该短信网关')
    E_102715 = ('602715', '短信账号无效')
    E_102716 = ('602716', '短信签名尚未审核通过')
    E_102720 = ('602720', '漫道返回：短信参数错误')
    E_102721 = ('602721', '漫道返回：短信内容过长')
    E_102722 = ('602722', '漫道返回：同一号码发送内容相同')