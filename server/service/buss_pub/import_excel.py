import datetime
import hashlib
import os
import time
import urllib
import urllib.request
from enum import Enum
import uuid
import re

import pandas as pd

from server.pao_python.pao.data import dataframe_to_list, date_to_string
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...models.financial_manage import FinancialAccount
from ...pao_python.pao.data import get_cur_time, process_db, string_to_date
from ...service.buss_mis.financial_account import FinancialAccountObject
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.buss_pub.personnel_organizational import UserCategory, UserType
from ...service.common import (SecurityConstant, UserType, find_data,
                               get_current_organization_id,
                               get_current_role_id, get_current_user_id,
                               get_info, get_str_to_age, get_string_time,
                               insert_data, update_data, insert_many_data)
from ...service.constant import (AccountStatus, AccountType, PayStatue,
                                 PayType, plat_id)
from ...service.mongo_bill_service import MongoBillFilter
from ...service.welfare_institution.accommodation_process import CheckInStatus

'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-10-31 15:50:52
@LastEditors: Please set LastEditors
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 9:52:29 am
修改者: ymq(ymq) - <<email>>

说明
 1、用户管理服务
'''
'''
20200316
活动列表，资讯列表，活动室，活动签到数据
20200317
更新长者资料
20200318
新增长者资料
12月1月2月3月服务订单数据
20200319
12月1月2月3月服务工单数据
'''

# 床位状态


class BedStatus(Enum):
    BlockUp = '停用'
    Empty = '空闲'
    Order = 'Order'
    Using = 'Using'
    TryCheckIn = 'TryCheckIn'

# 入住类型


class SignType(Enum):
    TryCheckIn = '试入住'
    FormalCheckIn = '正式入住'

# 入住状态


class CheckInStatus2(Enum):
    UnCheckIn = '待入住'
    CheckIn = '在住'
    Leave = '请假'
    CheckOut = '退住'

# 工单状态


class RecordStatus(Enum):
    WaitingDelivery = '待派工'
    WaitingExecuted = '待执行'
    Ongoing = '进行中'
    Completed = '已完成'

# 回访状态


class VisitStatus(Enum):
    NoVisited = '未回访'
    Visited = '已回访'

# 在世状态


class DieState(Enum):
    Normal = '健在'
    Late = '已故'

# 婚姻状态


class MarriageState(Enum):
    Unmarried = '未婚'
    Married = '已婚'
    Remarry = '再婚'
    Divorced = '离异'
    Widowed = '丧偶'

# 回访服务满意度


class ServiceEvaluation(Enum):
    VerySatisfied = '非常满意'
    Satisfied = '满意'
    General = '一般'
    Disappointment = '不满意'

# 服务周期


class Cycle(Enum):
    EveryDay = '每天'
    EveryWeek = '每周'
    EveryMonth = '每月'

# 支付方式


class PayStatus(Enum):
    NoPaid = '待支付'
    Paid = '已支付'

# 订单来源类型


class SourceType(Enum):
    Service = '服务'
    Goods = '商品'
    MonthBase = '月基础'
    BedPrice = '床位费'
    ChangeBed = '换床'
    Leave = '请假'
    Moment = '阶段性费用 '
    MonthService = '月服务'
    OnceProject = '一次性费用'
    CommodityCharge = '商品订单'
    CMDYTotalCharge = '商品总费用'
    CheckOut = '退住'
    InterimOrder = 'APP临时工单'
    Water = '冷水费'
    HotWater = '热水费'
    Electric = '电费'
    Expense = '长者证抵扣'
    Coupon = '积分抵扣'
    BedSubsidy = '床位费抵扣'
    Outpatient = '门诊'
    NursingCare = '托养医疗'
    HospitalExpenses = '住院费'
    ExaminationFee = '体检费'
    CableTV = '电视'
    Activity = '活动'
    Promotion = '试入住转正抵扣费'

# 付款方法


class BuyType(Enum):
    Cash = '现金'
    WipeCard = '刷卡'
    ExpenseCard = '消费卡'
    WeChat = '微信支付'
    Alipay = '支付宝'
    OnCredit = '挂账'
    Free = '减免'
    Deduction = '套餐抵扣'
    Allowance = '补贴账户'
    PackagePay = '套餐扣款'

# 订单创建方法


class CreateMethod(Enum):
    Manual = '手动'
    Automatic = '自动'

# 资讯状态枚举


class ArticleStatus(Enum):
    UnReleased = '不通过'
    Released = '通过'

# 人员类别


class Sex(Enum):
    Female = '女'
    Male = '男'

# 活动状态


class ActivityStatus(Enum):
    UnStart = '未开始'
    OnGoing = '进行中'
    Finished = '已结束'


# 工作人员类别


class WorkerCategory(Enum):
    Management = '管理人员'
    Professional = '专业技能人员'
    SocialWorker = '社工人员'
    Logistics = '后勤人员'

# 机构类别


class JiGouLeiBie(Enum):
    WelfareCentre = '养老院'
    Happiness = '幸福院'
    Common = '运营机构'
    Provider = '服务商'
    Street = '街道'
    Government = '政府'
    Community = '社区'

# 养老类型


class YangLaoLeiXing(Enum):
    OrgType = '机构养老'
    HomeType = '居家养老'
# 长者类别


class ZhangZheLeiBie(Enum):
    Expense = '自费'
    ThreeNo = '三无'
    FiveProtect = '五保'
    Lonely = '孤寡'
    Subsistence = '享受低保救济'

# 学历资质


class XueLiZiZhi(Enum):
    SmallSchool = '小学'
    HighSchool = '中学'
    PolytechnicSchool = '中专'
    HigherVocationalEducation = '高职'
    JuniorCollege = '大专'
    RegularCollegeCourse = '本科'
    Master = '硕士'
    Doctor = '博士'
    No = '无'

# 岗位枚举


class Post(Enum):
    Dean = '院长'
    Doctor = '医生'
    Logistics = '后勤'
    Nurse = '护士'
    PipeNurse = '护管员'
    SocialWorker = '社工'


class ImportExcelService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

        self.PT_User = 'PT_User2'
        self.PT_Account = 'PT_Financial_Account_New'
        self.PT_Role = 'PT_Set_Role_New'

    def date_to_format(self, num):
        if type(num) == float:
            num = int(num)
        if type(num) == int:
            num = num-2
            initDate = datetime.datetime.strptime(
                '1900-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
            return initDate+datetime.timedelta(days=+num)
            # return (initDate+datetime.timedelta(days=+num)).strftime("%Y-%m-%d %H:%M:%S")
        else:
            return num

    def strtodate(self, string):
        try:
            return datetime.datetime.strptime(
                string, '%Y-%m-%d %H:%M:%S')
        except Exception as e:
            return string

    def test(self, data):

        # 导入服务套餐
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构名称': 'jgmc', '服务套餐主键': 'fwtczj', '服务套餐名字': 'fwtcmz', '服务方式': 'fwfs', '服务套餐描述': 'fwtcms', '服务套餐价格': 'fwtcjg', '有效期': 'yxq', '服务套餐销量': 'fwtcxl', '是否发布到APP': 'sffbdapp',
                           '服务项目内容': 'fwxmnr', '服务项目名称': 'fwxmmc', '服务项目价格': 'fwxmjg', '服务次数': 'fwcs', '创建日期': 'cjrq', '发布日期': 'fbrq', '失效日期': 'sxrq', '审核状态': 'shzt', '审核建议': 'shjy', '审核日期': 'shrq', '服务套餐封面图': 'fwtcfmt', '服务套餐图片': 'fwtctp'}, inplace=True)
        condition = dataframe_to_list(pf)

        temp_org = {}

        json_str = ''

        ittm_arr = {}

        all_count = 0

        if len(condition) > 0:

            for ittm in condition:
                fwtczj = self.clearZFC(ittm['fwtczj'])
                if fwtczj not in ittm_arr:
                    ittm_arr[fwtczj] = 1
                else:
                    ittm_arr[fwtczj] = ittm_arr[fwtczj] + 1

            for item in condition:
                all_count = all_count + 1
              # print('当前第' + str(all_count) + '个')
                # 查询机构
                org_info = []
                jgmc = self.clearZFC(item['jgmc'])

                if jgmc not in temp_org:
                    _filter_org = MongoBillFilter()
                    _filter_org = _filter_org.match_bill((C('name') == jgmc) & (
                        C('personnel_type') == '2') & (C('organization_info.personnel_category') == '服务商'))
                    res_org = self.query(
                        _filter_org, 'PT_User_0316')

                    if len(res_org) == 1:
                        temp_org[jgmc] = res_org[0]
                        org_info = res_org[0]
                else:
                    org_info = temp_org[jgmc]

                product_id = ''

                # 找出服务套餐
                fwtcmz = self.clearZFC(item['fwtcmz'])
                _filter_tc = MongoBillFilter()
                _filter_tc.match_bill((C('name') == fwtcmz) & (
                    C('organization_id') == org_info['id']) & (C('is_service_item') == 'false'))
                res_tc = self.query(
                    _filter_tc, 'ZPT_Service_Product_0320')

                if len(res_tc) != 1:
                    pass
                  # print('不对劲', fwtcmz)

                # 根据机构ID和产品名字找到服务产品
                fwxmmc = self.clearZFC(item['fwxmmc'])
                _filter_pd1 = MongoBillFilter()
                _filter_pd1.match_bill((C('name') == fwxmmc) & (C('organization_id') == org_info['id']) & (C('audit_status') != 'NotPass'))\
                    .project({'_id': 0})
                res_pd1 = self.query(
                    _filter_pd1, 'ZPT_Service_Product_0320')

                if len(res_pd1) == 0:
                    _filter_pd2 = MongoBillFilter()
                    _filter_pd2.match_bill((C('name') == fwxmmc) & (C('organization_id') == org_info['id']))\
                        .project({'_id': 0})
                    res_pd2 = self.query(
                        _filter_pd2, 'ZPT_Service_Product_0320')

                    product_id = res_pd2[0]['id']
                else:
                    product_id = res_pd1[0]['id']

                # 套餐主键
                fwtczj = self.clearZFC(item['fwtczj'])
                BBB = False
                for itm in res_tc[0]['service_product']:
                    if itm['product_id'] == product_id:
                        BBB = True
                        itm['service_package_price'] = float(
                            self.clearZFC(item['fwxmjg']))

                res_tc[0]['primary_id'] = fwtczj
                if BBB == False:
                    pass
                  # print('没改过啊', fwtcmz)
                else:
                    def process_func_update(db):
                        update_data(db, 'ZPT_Service_Product_0320', res_tc[0], {
                            '_id': res_tc[0]['_id'],
                        })
                    process_db(self.db_addr, self.db_port, self.db_name,
                               process_func_update, self.db_user, self.db_pwd)

                # { updateOne: { "filter": { "_id": ObjectId("5e744c9f1424c9a41dd3ad64") }, "update": { $set: { "name": "修改后的真的是辛德拉" } } } }

        return 123

    def import_excel_api(self, stype, sdata):

        if stype == '活动':
            return self.import_excel_activity(sdata)
        elif stype == '资讯':
            return self.import_excel_article(sdata)
        elif stype == '服务订单':
            return self.import_excel_service_order(sdata)
        elif stype == '服务工单':
            return self.import_excel_service_record(sdata)
        elif stype == '工作人员':
            return self.import_excel_worker(sdata)
        elif stype == '长者':
            return self.import_excel_elder(sdata)
        elif stype == '评估记录':
            return self.import_excel_assessment_record(sdata)
        elif stype == '修复活动报名':
            return self.update_hdbm(sdata)
        else:
            return '没有可导入的设置'

        # return self.import_baoxian_old_age_user(data)

        # if '订单编号' in data[0]:
        #     return self.import_excel_service_order(data)
        # elif '床位主键' in data[0]:
        #     return self.import_excel_bed(data)
        # elif '工单号码' in data[0]:
        #     return self.import_excel_service_record(data)
        # elif '报名方式' in data[0]:
        #     return self.import_excel_signin(data)
        # elif '活动名称' in data[0]:
        #     return self.import_excel_activity(data)
        # elif '活动室唯一主键' in data[0]:
        #     return self.import_excel_activity_room(data)
        # elif '资讯标题' in data[0]:
        #     return self.import_excel_article(data)
        # elif '服务套餐主键' in data[10]:
        #     return self.import_excel_service_product(data)
        # elif '义工名字' in data[10]:
        #     return self.import_excel_volunteer(data)

    def import_old_age_user(self, data):

        pf = pd.DataFrame(data)
        pf.rename(columns={'镇（街）': 'place', '姓名': 'name', '身份证号码': 'id_card',
                           '性别': 'sex', '联系电话': 'telephone', '村（居委）': 'admin_area_id'}, inplace=True)

        condition = dataframe_to_list(pf)

        arr = []
        count = 0

        keys = ['place', 'admin_area_id',
                'id_card', 'name', 'sex', 'telephone']

        if len(condition) > 0:
            for item in condition:
                if 'id_card' in item and item['id_card'] != None and self.clearZFC(item['id_card']) != '':
                    count = count+1

                    newData = {}
                    # 补充原数据
                    newData = self._setValues(keys, item, newData)
                    newData['type'] = '高龄'

                    arr.append(newData)

        if len(arr) > 0:
            def process_func(db):
                insert_many_data(db, 'PT_User_0317', arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)

        return count

    def update_user_die_state(self, data):

        # 查询所有火化长者
        _filter = MongoBillFilter()
        _filter = _filter.match(
            (C('type') == '1') | (C('type') == '2'))
        res_hh = self.query(
            _filter, 'PT_User_huohua')

        allUser = {}

        # 查询所有长者
        _filter = MongoBillFilter()
        _filter = _filter.match_bill(
            (C('personnel_info.personnel_category') == '长者') & (C('personnel_type') == '1'))
        res = self.query(
            _filter, self.PT_User)

        for user_item in res:
            if 'id_card' in user_item:
                allUser[str(user_item['id_card'])] = user_item

        updateCount = 0

        if len(res_hh) > 0:
            for item in res_hh:
                if 'id_card' in item:
                    id_card = self.clearZFC(item['id_card'])
                    updateCount = updateCount + 1
                  # print('更新' + str(updateCount) + '个')
                    if id_card in allUser:
                        allUser[id_card]['personnel_info']['die_state'] = '已故'

                        def process_func_update(db):
                            update_data(db, self.PT_User, allUser[id_card], {
                                '_id': allUser[id_card]['_id'],
                            })
                        process_db(self.db_addr, self.db_port, self.db_name,
                                   process_func_update, self.db_user, self.db_pwd)

        return '更新' + str(updateCount) + '个'

    def import_baoxian_old_age_user(self, data):

        return self.update_user_die_state(data)

        pf = pd.DataFrame(data)
        pf.rename(columns={'姓名': 'name', '身份证号码': 'id_card', '性别': 'sex',
                           '联系电话': 'telephone', '区域': 'admin_area_id', '区域名称': 'admin_area_name'}, inplace=True)

        condition = dataframe_to_list(pf)

        middleTable = 'PT_UserDiff'
        areaTable = 'PT_AreaDiff'

        person_arr = []
        role_arr = []
        account_arr = []
        middle_arr = []
        area_arr = []
        temp_admin_area_id = {}

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'

        stype = ''
        if '补发金额' in condition[0]:
            stype = '高龄津贴'
        elif '困难类别' in condition[0]:
            stype = '保险'

        insertCount = 0
        updateCount = 0
        allCount = 0

        allUser = {}

        # 查询所有长者
        _filter = MongoBillFilter()
        _filter = _filter.match_bill(
            (C('personnel_info.personnel_category') == '长者') & (C('personnel_type') == '1'))
        res = self.query(
            _filter, self.PT_User)

        for user_item in res:
            if 'id_card' in user_item:
                allUser[str(user_item['id_card'])] = user_item

        del(res)

        if len(condition) > 0:
            for item in condition:
                allCount = allCount + 1
              # print('当前第' + str(allCount) + '个')
                if 'id_card' in item and item['id_card'] != None and self.clearZFC(item['id_card']) != '':

                    id_card = self.clearZFC(item['id_card'])
                    admin_area_id = self.clearZFC(item['admin_area_id'])
                    name = self.clearZFC(item['name'])
                    sex = self.clearZFC(item['sex'])
                    telephone = self.clearZFC(item['telephone'])
                    admin_area_name = self.clearZFC(item['admin_area_name'])

                    # 存一个区域id对应区域名称的关系
                    if admin_area_id not in temp_admin_area_id:
                        temp_admin_area_id[admin_area_id] = admin_area_name
                        areaDiff = get_info({'admin_area_id': admin_area_id,
                                             'admin_area_name': admin_area_name,
                                             }, self.session)
                        areaDiff['GUID'] = str(uuid.uuid1())
                        areaDiff['version'] = 1
                        areaDiff['bill_status'] = 'valid'
                        areaDiff['valid_bill_id'] = str(uuid.uuid1())
                        areaDiff['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'
                        area_arr.append(areaDiff)

                    # 找到数据
                    if id_card in allUser:
                        # 数据不一样
                        if allUser[id_card].get('name') != name or allUser[id_card].get('admin_area_id') != admin_area_id or allUser[id_card]['personnel_info'].get('sex') != sex:
                            # 中间表标记为修改
                            middle_data = {
                                'old_area_id': allUser[id_card].get('admin_area_id'),
                                'new_area_id': admin_area_id,
                                'old_name': allUser[id_card].get('name'),
                                'new_name': name,
                                'old_sex': allUser[id_card]['personnel_info'].get('sex'),
                                'new_sex': sex,
                                'old_data': allUser[id_card],
                                'id_card': id_card,
                                'stype': stype,
                            }
                            if stype == '保险':
                                middle_data['bx_data'] = {
                                    'name': name,
                                    'sex': sex,
                                    'id_card': id_card,
                                    'admin_area_id': admin_area_id,
                                    'telephone': telephone,
                                    'admin_area_name': admin_area_name,
                                }
                            elif stype == '高龄津贴':
                                middle_data['oa_data'] = {
                                    'name': name,
                                    'sex': sex,
                                    'id_card': id_card,
                                    'admin_area_id': admin_area_id,
                                    'telephone': telephone,
                                    'admin_area_name': admin_area_name,
                                }

                            middle_arr.append(middle_data)

                            # 更新数据
                            updateCount = updateCount + 1
                            allUser[id_card]['name'] = name
                            allUser[id_card]['id_card'] = id_card
                            allUser[id_card]['admin_area_id'] = admin_area_id
                            allUser[id_card]['personnel_info']['name'] = name
                            allUser[id_card]['personnel_info']['id_card'] = id_card
                            allUser[id_card]['personnel_info']['sex'] = sex
                            allUser[id_card] = self.setImportDate(
                                allUser[id_card], '20200317')

                            def process_func_update(db):
                                update_data(db, self.PT_User, allUser[id_card], {
                                    '_id': allUser[id_card]['_id'],
                                })
                            process_db(self.db_addr, self.db_port, self.db_name,
                                       process_func_update, self.db_user, self.db_pwd)
                    else:
                        # 直接插入数据
                        newPerson = {
                            "name": name,
                            "personnel_type": "1",
                            "admin_area_id": admin_area_id,
                            "town": "",
                            "id_card_type": "身份证",
                            "id_card": id_card,
                            "organization_id": default_organization_id,
                            "reg_date": '',
                            "login_info": [],
                            "personnel_info": {
                                "name": name,
                                "id_card": id_card,
                                "date_birth": None,
                                "sex": sex,
                                "telephone": telephone,
                                "remarks": "",
                                "personnel_category": "长者",
                                "address": "",
                                "role_id": default_role_id,
                                "is_alive": "",
                                "personnel_classification": "",
                                "pension_category": "",
                                "elders_category": "",
                                "education": "",
                                "political_outlook": "",
                                "nation": "",
                                "die_state": '健在',
                            }
                        }

                        person_data = get_info(newPerson, self.session)

                        person_data = self.setImportDate(
                            person_data, '20200318')
                        newPerson['GUID'] = str(uuid.uuid1())
                        newPerson['version'] = 1
                        newPerson['bill_status'] = 'valid'
                        newPerson['valid_bill_id'] = str(uuid.uuid1())
                        newPerson['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）

                        # 机构储蓄对象
                        org_account_data = FinancialAccountObject(
                            AccountType.account_recharg_wel, person_data['id'], person_data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                        org_account = get_info(
                            org_account_data.to_dict(), self.session)

                        org_account['GUID'] = str(uuid.uuid1())
                        org_account['version'] = 1
                        org_account['bill_status'] = 'valid'
                        org_account['valid_bill_id'] = str(uuid.uuid1())
                        org_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # app储蓄对象
                        app_account_data = FinancialAccountObject(
                            AccountType.account_recharg_app, person_data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                        app_account = get_info(
                            app_account_data.to_dict(), self.session)

                        app_account['GUID'] = str(uuid.uuid1())
                        app_account['version'] = 1
                        app_account['bill_status'] = 'valid'
                        app_account['valid_bill_id'] = str(uuid.uuid1())
                        app_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # 真实账户对象
                        real_account_data = FinancialAccountObject(
                            AccountType.account_real, person_data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                        real_account = get_info(
                            real_account_data.to_dict(), self.session)

                        real_account['GUID'] = str(uuid.uuid1())
                        real_account['version'] = 1
                        real_account['bill_status'] = 'valid'
                        real_account['valid_bill_id'] = str(uuid.uuid1())
                        real_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # 补贴账户对象
                        subsidy_account_data = FinancialAccountObject(
                            AccountType.account_subsidy, person_data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                        subsidy_account = get_info(
                            subsidy_account_data.to_dict(), self.session)

                        subsidy_account['GUID'] = str(uuid.uuid1())
                        subsidy_account['version'] = 1
                        subsidy_account['bill_status'] = 'valid'
                        subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                        subsidy_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # 慈善账户
                        charitable_account_data = FinancialAccountObject(
                            AccountType.account_charitable, person_data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
                        charitable_account = get_info(
                            charitable_account_data.to_dict(), self.session)

                        charitable_account['GUID'] = str(uuid.uuid1())
                        charitable_account['version'] = 1
                        charitable_account['bill_status'] = 'valid'
                        charitable_account['valid_bill_id'] = str(uuid.uuid1())
                        charitable_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # 主数据
                        person_arr.append(person_data)

                        role_data = {'role_id': default_role_id,
                                     'principal_account_id': person_data['id'], 'role_of_account_id': person_data['organization_id']}

                        role_data = get_info(role_data, self.session)

                        role_data['GUID'] = str(uuid.uuid1())
                        role_data['version'] = 1
                        role_data['bill_status'] = 'valid'
                        role_data['valid_bill_id'] = str(uuid.uuid1())
                        role_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        # role表的数组
                        role_arr.append(role_data)

                        # 账户表
                        account_arr.append(org_account)
                        account_arr.append(app_account)
                        account_arr.append(real_account)
                        account_arr.append(subsidy_account)
                        account_arr.append(charitable_account)

                        insertCount = insertCount+1
                      # print('新增' + str(insertCount) + '个')

        if len(person_arr) > 0:
            def process_func_all(db):
                # 中间表
                insert_many_data(db, areaTable, area_arr)
                insert_many_data(db, middleTable, middle_arr)
                insert_many_data(db, self.PT_User, person_arr)
                insert_many_data(db, self.PT_Role, role_arr)
                insert_many_data(db, self.PT_Account, account_arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func_all, self.db_user, self.db_pwd)

        return '总共' + str(allCount) + '||新增' + str(insertCount) + '||更新' + str(updateCount)

    def import_huohua_user2(self, data):

        pf = pd.DataFrame(data)
        pf.rename(columns={'火化日期': 'gone_date', '身份证号': 'id_card',
                           '遗体姓名': 'name', '性别': 'sex', '手机': 'telephone'}, inplace=True)

        condition = dataframe_to_list(pf)

        arr = []
        count = 0

        keys = ['gone_date', 'id_card', 'name', 'sex', 'telephone']

        if len(condition) > 0:
            for item in condition:
                if 'id_card' in item and item['id_card'] != None:
                    count = count+1

                    newData = {}
                    # 补充原数据
                    newData = self._setValues(keys, item, newData)
                    newData['type'] = '2'

                    arr.append(newData)

        if len(arr) > 0:
            def process_func(db):
                insert_many_data(db, 'Z_PT_User_huohua', arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)

      # print(count)
        return count

    def import_huohua_user(self, data):

        # return data

        # del(data[0])
        # del(data[0])
        # del(data[0])

        pf = pd.DataFrame(data)
        pf.rename(columns={'火化时间': 'gone_date', '身份证号码': 'id_card',
                           '死者姓名': 'name', '性别': 'sex', '家庭电话': 'telephone'}, inplace=True)
        condition = dataframe_to_list(pf)

        keys = ['gone_date', 'id_card', 'name', 'sex', 'telephone']

        arr = []

        count = 0

        if len(condition) > 0:
            for item in condition:

                if '__EMPTY_4' in item and item['__EMPTY_4'] != None and self.clearZFC(item['__EMPTY_4']) != '身份证号码':

                    count = count+1

                    newData = {}

                    newData['gone_date'] = self.clearZFC(item['__EMPTY'])
                    newData['id_card'] = self.clearZFC(item['__EMPTY_4'])
                    newData['name'] = self.clearZFC(item['__EMPTY_5'])
                    newData['sex'] = self.clearZFC(item['__EMPTY_6'])
                    newData['telephone'] = self.clearZFC(item['__EMPTY_12'])
                    newData['type'] = '2'

                    # 补充原数据
                    # newData = self._setValues(keys, item, newData)

                    arr.append(newData)

        if len(arr) > 0:
            def process_func(db):
                insert_many_data(db, 'Z_PT_User_huohua', arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)

      # print(count)
        return count

    def import_excel_test(self, data):

        _filter = MongoBillFilter()
        _filter = _filter.match(
            (C('personnel_info.personnel_category') == '长者'))
        res = self.query(
            _filter, self.PT_User)

        arr = []

        if len(res) > 0:
            for item in res:
                # 慈善账户
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_charitable, item['id'], None, AccountStatus.normal, 4, AccountType.account_charitable, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)
                subsidy_account['GUID'] = str(uuid.uuid1())
                subsidy_account['version'] = 1
                subsidy_account['bill_status'] = 'valid'
                subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                subsidy_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                arr.append(subsidy_account)

        def process_func(db):
            insert_many_data(db, 'ZZZPT_Financial_Account', arr)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

    def update_personnel(self, data):
        # 修改人员
        pf = pd.DataFrame(data)
        pf.rename(columns={'status': 'zszt', '区域编号': 'qybh', '行政区划': 'xzqh', '人员姓名': 'ryxm', '性别': 'xb', '出生日期': 'csrq', '身份证号': 'sfzh', '所属组织': 'sszz', '特殊人群': 'tsrq', '残疾人等级': 'cjrdj', '社保类型': 'sblx', '户籍性质': 'hjxz', '人户分离': 'rhfl', '与户主关系': 'yhzgx', '户籍地址': 'hjdz',
                           '户口迁入地': 'hkqrd', '迁入时间': 'qrsj', '房屋类型': 'fwlx', '工作单位': 'gzdw', '固定电话': 'gddh', '移动电话': 'yddh', '民族': 'mz', '政治面貌': 'zzmm', '学历': 'xl', '毕业院校': 'byyx', '毕业时间': 'bysj', '职业类别': 'zylb', '婚姻状态': 'hyzt', '宗教信仰': 'zjxy', '兴趣爱好': 'xqah', '备注': 'bz', '长者类型': 'zzlx', '长者类别': 'zzlb', '养老类型': 'yllx', '创建日期': 'cjrq'}, inplace=True)
        condition = dataframe_to_list(pf)

        usertb = self.PT_User
        financialtb = 'PT_Financial_Account'
        setroletb = 'PT_Set_Role'
        area_str = ''

        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_qybh = {}
        temp_sszz = {}
        temp_zzxl = {}

        count = 0

        ryxm = self.clearZFC(condition[0]['ryxm'])

        if ryxm == '硬件演示':
            area_str = 'cc'
            area_cn = '禅城'
        elif ryxm == '岑美梅':
            area_str = 'dl'
            area_cn = '大沥'
        elif ryxm == '李惠兴':
            area_str = 'dz'
            area_cn = '丹灶'
        elif ryxm == '黎丹丹':
            area_str = 'gc'
            area_cn = '桂城'
        elif ryxm == '李金弟':
            area_str = 'jj'
            area_cn = '九江'
        elif ryxm == '吴玉兰':
            area_str = 'na'
            area_cn = '空编号'
        elif ryxm == '柯少君':
            area_str = 'ls'
            area_cn = '里水'
        elif ryxm == '揭绍明':
            area_str = 'ot'
            area_cn = '其他'
        elif ryxm == '刘惠英':
            area_str = 'xq'
            area_cn = '西樵'
        elif ryxm == '刘巨亨':
            area_str = 'ss'
            area_cn = '狮山'
        elif ryxm == '吴秋庆':
            area_str = 'ss'
            area_cn = '狮山'

        usertb = usertb + '_' + area_str
        financialtb = financialtb + '_' + area_str
        setroletb = setroletb + '_' + area_str
        # 找出有没有
        if len(condition) > 0:
            for item in condition:

                ryxm = self.clearZFC(item['ryxm'])
                sfzh = self.clearZFC(item['sfzh'])

              # print(sfzh != None)

                if sfzh != None:
                    # 这里根据身份证做判断
                    _filter_isHave = MongoBillFilter()
                    _filter_isHave.match_bill(C('personnel_info.id_card') == sfzh)\
                        .project({'_id': 0})
                    res_isHave = self.query(
                        _filter_isHave, usertb)
                else:
                    # 这里根据名字做判断
                    _filter_isHave = MongoBillFilter()
                    _filter_isHave.match_bill((C('name') == ryxm) & ((C('personnel_info.id_card') == 'None') | (C('personnel_info.id_card') == N()) | (C('personnel_info.id_card') == "")))\
                        .project({'_id': 0})
                    res_isHave = self.query(
                        _filter_isHave, usertb)

                if len(res_isHave) > 0:
                    # if 'import_date' in res_isHave[0]:
                    #     continue
                    # 更新的
                    save_data = {
                        **res_isHave[0]
                    }
                else:
                    # 新增的
                    save_data = {
                        "name": "",
                        "personnel_type": "1",
                        "admin_area_id": default_area_id,
                        "town": "",
                        "id_card_type": "身份证",
                        "id_card": "",
                        "organization_id": default_organization_id,
                        "reg_date": '',
                        "login_info": [],
                        "personnel_info": {
                            "name": "",
                            "id_card": "",
                            "date_birth": "",
                            "sex": "",
                            "telephone": "",
                            "remarks": "",
                            "personnel_category": "长者",
                            "address": "",
                            "role_id": default_role_id,
                            "is_alive": "",
                            "personnel_classification": "",
                            "pension_category": "",
                            "elders_category": "",
                            "education": "",
                            "political_outlook": "",
                        }
                    }

                # 修改的名字
                save_data['name'] = ryxm
                save_data['personnel_info']['name'] = ryxm

                # 判断所属行政编号
                if 'qybh' in item and item['qybh'] != None:
                    qybh = self.clearZFC(item['qybh'])
                    if qybh in temp_qybh:
                      # print('已有区域ID，无需重复获取')
                        save_data['admin_area_id'] = temp_qybh[qybh]
                    else:
                        _filter_qybh = MongoBillFilter()
                        _filter_qybh.match_bill((C('code') == qybh))\
                            .project({'_id': 0})
                        res_qybh = self.query(
                            _filter_qybh, 'PT_Administration_Division56')

                        if len(res_qybh) > 0:
                            temp_qybh[qybh] = res_qybh[0]['id']
                            save_data['admin_area_id'] = res_qybh[0]['id']

                # 判断所属组织
                if 'sszz' in item and item['sszz'] != None:
                    sszz = self.clearZFC(item['sszz'])
                    if sszz in temp_sszz:
                      # print('已有组织ID，无需重复获取')
                        save_data['organization_id'] = temp_sszz[sszz]
                    else:
                        _filter_sszz = MongoBillFilter()
                        _filter_sszz.match_bill((C('name') == sszz))\
                            .project({'_id': 0})
                        res_sszz = self.query(
                            _filter_sszz, self.PT_User)

                        if len(res_sszz) > 0:
                            temp_sszz[sszz] = res_sszz[0]['id']
                            save_data['organization_id'] = res_sszz[0]['id']

                # 判断长者类型
                if 'zzlx' in item and item['zzlx'] != None:
                    zzlx = self.clearZFC(item['zzlx'])
                    if zzlx in temp_zzxl:
                      # print('已有长者类型，无需重复获取')
                        save_data['personnel_info']['personnel_classification'] = temp_zzxl[zzlx]
                    else:
                        _filter_zzlx = MongoBillFilter()
                        _filter_zzlx.match_bill((C('name') == zzlx))\
                            .project({'_id': 0})
                        res_zzlx = self.query(
                            _filter_zzlx, 'PT_Personnel_Classification')

                        if len(res_zzlx) > 0:
                            temp_zzxl[zzlx] = res_zzlx[0]['id']
                            save_data['personnel_info']['personnel_classification'] = res_zzlx[0]['id']

                # 判断生日
                if 'crrq' in item and item['crrq'] != None:
                    crrq = self.strtodate(self.clearZFC(item['crrq']))
                    save_data['personnel_info']['date_birth'] = crrq
                # 判断创建日期
                if 'cjrq' in item and item['cjrq'] != None:
                    cjrq = self.strtodate(self.clearZFC(item['cjrq']))
                    save_data['reg_date'] = cjrq
                    save_data['create_date'] = cjrq
                # 政治面貌
                save_data['personnel_info']['political_outlook'] = self.clearZFC(
                    item['zzmm'])
                # 民族
                save_data['personnel_info']['nation'] = self.clearZFC(
                    item['mz'])
                # 婚姻状态
                save_data['personnel_info']['remarks'] = self.clearZFC(
                    item['bz'])

                # 养老类型
                if 'xl' in item and item['xl'] != None:
                    xl = self.clearZFC(item['xl'])
                    if xl in XueLiZiZhi.__members__:
                        save_data['personnel_info']['education'] = XueLiZiZhi[xl].value
                    else:
                        save_data['personnel_info']['education'] = ''
                else:
                    save_data['personnel_info']['education'] = ''

                # 身份证号
                save_data['id_card'] = sfzh
                save_data['personnel_info']['id_card'] = sfzh

                # 性别
                if 'xb' in item:
                    xb = self.clearZFC(item['xb'])
                    save_data['personnel_info']['sex'] = '男' if xb == 'Male' else '女'
                # 地址
                if 'hjdz' in item and item['hjdz'] != None:
                    hjdz = self.clearZFC(item['hjdz'])
                    save_data['personnel_info']['address'] = hjdz
                # 电话
                if 'yddh' in item and item['yddh'] != None:
                    yddh = self.clearZFC(item['yddh'])
                    save_data['personnel_info']['telephone'] = yddh

                # 没有移动电话就用固定电话试试
                if 'telephone' not in save_data['personnel_info'] or ('telephone' in save_data['personnel_info'] and save_data['personnel_info']['telephone'] == '') and 'gddh' in item and item['gddh'] != None:
                    gddh = self.clearZFC(item['gddh'])
                    save_data['personnel_info']['telephone'] = gddh

                # 在世状态
                if 'zszt' in item and item['zszt'] != None:
                    zszt = self.clearZFC(item['zszt'])
                    save_data['personnel_info']['is_alive'] = True if zszt == 'Normal' else False

                # 养老类型
                if 'yllx' in item and item['yllx'] != None:
                    yllx = self.clearZFC(item['yllx'])
                    if yllx in YangLaoLeiXing.__members__:
                        save_data['personnel_info']['pension_category'] = YangLaoLeiXing[yllx].value
                    else:
                        save_data['personnel_info']['pension_category'] = ''
                else:
                    save_data['personnel_info']['pension_category'] = ''
                # 长者类别
                if 'zzlb' in item and item['zzlb'] != None:
                    zzlb = self.clearZFC(item['zzlb'])
                    if zzlb in ZhangZheLeiBie.__members__:
                        save_data['personnel_info']['elders_category'] = ZhangZheLeiBie[zzlb].value
                    else:
                        save_data['personnel_info']['elders_category'] = ''
                else:
                    save_data['personnel_info']['elders_category'] = ''

                # 导入标识
                save_data['import_date'] = '20191226'

                if len(res_isHave) > 0:
                    # 更新的
                    def process_func(db):
                        # print(save_data)
                        # return save_data
                        update_data(db, usertb, save_data, {
                            'id': res_isHave[0]['id'],
                        })
                    process_db("127.0.0.1", 27017, 'IEC',
                               process_func, 'admin', '123456')
                else:
                    data = get_info(save_data, self.session)
                    # return data

                    # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
                    # 机构储蓄对象
                    org_account_data = FinancialAccountObject(
                        AccountType.account_recharg_wel, data['id'], data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                    org_account = get_info(
                        org_account_data.to_dict(), self.session)
                    # app储蓄对象
                    app_account_data = FinancialAccountObject(
                        AccountType.account_recharg_app, data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                    app_account = get_info(
                        app_account_data.to_dict(), self.session)
                    # 真实账户对象
                    real_account_data = FinancialAccountObject(
                        AccountType.account_real, data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                    real_account = get_info(
                        real_account_data.to_dict(), self.session)
                    # 补贴账户对象
                    subsidy_account_data = FinancialAccountObject(
                        AccountType.account_subsidy, data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                    subsidy_account = get_info(
                        subsidy_account_data.to_dict(), self.session)
                    tables = [usertb, financialtb]
                    datas = [[data], [org_account, app_account,
                                      real_account, subsidy_account]]

                    set_role = {'role_id': default_role_id,
                                'principal_account_id': data['id'], 'role_of_account_id': data['organization_id']}
                    set_role_data = get_info(set_role, self.session)
                    datas.append(set_role_data)
                    tables.append(setroletb)
                  # print('开始导入人员：' + str(area_str) + '_' + str(data['name']))
                    try:
                        self.bill_manage_service.add_bill(OperationType.add.value,
                                                          TypeId.user.value, datas, tables)
                    except Exception as e:
                        pass
                      # print(e)

        return '导入成功'

    # 修复工作人员的role_id全是服务商工作人员的问题
    def update_worker(self, data):
        dbIp = "127.0.0.1"
        dbName = 'IEC'
        dbUser = 'admin'
        dbPwd = '123456'
        _filter_main = MongoBillFilter()
        _filter_main.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员') & (C('import_date').inner(['20200108'])))\
            .project({'_id': 0})
        res_main = self.query(
            _filter_main, self.PT_User)

        count = 1

        if len(res_main) > 0:
            for item in res_main:
              # print('第'+str(count)+'个    ' + item['id'])
                count = count+1
                # 根据organization_id去找所属组织
                _filter_organization = MongoBillFilter()
                _filter_organization.match((C('personnel_type') == '2') & (C('id') == item['organization_id']))\
                    .project({'_id': 0})
                res_organization = self.query(
                    _filter_organization, self.PT_User)

                # 先查set_role表对不对
                _filter_set_role = MongoBillFilter()
                _filter_set_role.match((C('principal_account_id') == item['id']) & (C('role_of_account_id') == item['organization_id']))\
                    .project({'_id': 0})
                res_set_role = self.query(
                    _filter_set_role, 'PT_Set_Role')

                if len(res_organization) > 0:
                    if res_organization[0]['organization_info']['personnel_category'] == '幸福院':
                        case_role_id = '8f3524cc-e355-11e9-9f87-a0a4c57e9ebe'
                        if len(res_set_role) > 0:
                            if res_set_role[0]['role_id'] == case_role_id:
                                continue

                        def process_func(db):
                            update_data(db, 'PT_Set_Role', {'role_id': case_role_id}, {
                                'principal_account_id': item['id'],
                                'role_of_account_id': item['organization_id'],
                                'bill_status': 'valid',
                            })
                        process_db(dbIp, 27017, dbName,
                                   process_func, dbUser, dbPwd)
                    elif res_organization[0]['organization_info']['personnel_category'] == '福利院':
                        case_role_id = '8e271ff0-e355-11e9-91a6-a0a4c57e9ebe'
                        if len(res_set_role) > 0:
                            if res_set_role[0]['role_id'] == case_role_id:
                                continue

                        def process_func(db):
                            update_data(db, 'PT_Set_Role', {'role_id': case_role_id}, {
                                'principal_account_id': item['id'],
                                'role_of_account_id': item['organization_id'],
                                'bill_status': 'valid',
                            })
                        process_db(dbIp, 27017, dbName,
                                   process_func, dbUser, dbPwd)
                    elif res_organization[0]['organization_info']['personnel_category'] == '服务商':
                        case_role_id = '8fa7a328-e355-11e9-98b5-a0a4c57e9ebe'
                        # 判断role_name是否包含管理员
                        if 'personnel_info' in item and 'role_name' in item['personnel_info']:
                            if '管理员' in item['personnel_info']['role_name']:
                                # 说明有多个角色
                                if ',' in item['personnel_info']['role_name']:
                                    # 增加一个服务商服务人员
                                    _filter_is_set = MongoBillFilter()
                                    _filter_is_set.match((C('role_id') == case_role_id) & (C('principal_account_id') == item['id']) & (C('role_of_account_id') == item['organization_id']))\
                                        .project({'_id': 0})
                                    res_is_set = self.query(
                                        _filter_is_set, 'PT_Set_Role')
                                    if len(res_is_set) == 0:
                                        self.bill_manage_service.add_bill(OperationType.add.value, TypeId.user.value, {
                                            'role_id': case_role_id, 'principal_account_id': item['id'], 'role_of_account_id': item['organization_id']}, 'PT_Set_Role')
                    else:
                        # 默认幸福院工作人员
                        case_role_id = '8f3524cc-e355-11e9-9f87-a0a4c57e9ebe'
                        if len(res_set_role) > 0:
                            if res_set_role[0]['role_id'] == case_role_id:
                                continue

                        def process_func(db):
                            update_data(db, 'PT_Set_Role', {'role_id': case_role_id}, {
                                'principal_account_id': item['id'],
                                'role_of_account_id': item['organization_id'],
                                'bill_status': 'valid',
                            })
                        process_db(dbIp, 27017, dbName,
                                   process_func, dbUser, dbPwd)
      # print(len(res_main))

    def import_excel_elder(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'长者姓名': 'name', '证件类型': 'id_card_type', '证件号码': 'id_card', '性别': 'sex', '出生日期': 'date_birth',
                           '电话': 'telephone', '在世状态': 'die_state', '兴趣爱好': 'hobby', '婚姻状态': 'marriage_state', '民族': 'nation', '组织机构': 'organization_name', '籍贯': 'native_place', '区域编号': 'organization_code', '政治面貌': 'political_outlook', '家庭住址': 'address', '身份证地址': 'id_card_address', '行政区划': "admin_area_id", '备注': 'remark', '照片': 'photo', '学历': 'education', '银行卡号': 'card_number', '养老类型': 'pension_category', '长者类别': 'elders_category', '创建日期': 'create_date'}, inplace=True)
        condition = dataframe_to_list(pf)

        out_per = ['name', 'id_card']
        in_per = ['name', 'sex', 'id_card', 'telephone', 'native_place', 'address',
                  'id_card_address', 'education', 'post', 'nursing_area', 'card_number', 'die_state', 'hobby', 'marriage_state', 'nation', 'political_outlook', 'pension_category', 'elders_category', 'remark']
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)
        condition = dataframe_to_list(pf)

        # 组织机构的缓存数组
        temp_organization = {}
        # 区域编号的缓存数组
        temp_area = {}
        # 长者类型的缓存数组
        tempLX = {}
        temp_elder = {}

        person_arr = []
        role_arr = []
        account_arr = []

        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        _filter_elder = MongoBillFilter()
        _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
            .project({'_id': 0})
        res_elder = self.query(
            _filter_elder, self.PT_User)

        if len(res_elder) > 0:
            for item in res_elder:
                if 'id_card' in item:
                    temp_elder[item['id_card']] = item
        ccc = 0
        if len(condition) > 0:
            for item in condition:
                # 判断这个人是否存在
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    if id_card in temp_elder:
                        ccc = ccc + 1
        if ccc > 0:
            pass
          # print(ccc)
            # return '有重复的长者身份证'
            # return '有重复的长者身份证【' + id_card + '】'

        if len(condition) > 0:

            count = 0

            for item in condition:
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    if id_card in temp_elder:
                        continue

                # 导入人员
                newData = {
                    "name": "",
                    "personnel_type": "1",
                    "admin_area_id": default_area_id,
                    "town": "",
                    "id_card_type": "身份证",
                    "id_card": "",
                    "organization_id": default_organization_id,
                    "reg_date": get_cur_time(),
                    "create_date": get_cur_time(),
                    "login_info": [],
                    "personnel_info": {
                        "name": "",
                        "id_card": "",
                        "id_card_address": "",
                        "date_birth": "",
                        "sex": "",
                        "telephone": "",
                        "marriage_state": "",
                        "remarks": "",
                        "native_place": "",
                        "personnel_category": "长者",
                        "address": "",
                        "role_id": default_role_id,
                        "die_state": "",
                        "hobby": "",
                        "personnel_classification": "",
                        "card_number": "",
                        "pension_category": "",
                        "elders_category": "",
                        "political_outlook": "",
                        "photo": [],
                    }
                }

                # 判断personnel_info里的数据是否一致
                newData = self._setValues(out_per, item, newData)
                # 判断personnel_info里的数据是否一致
                newData['personnel_info'] = self._setValues(
                    in_per, item, newData['personnel_info'])

                # 找出所属机构
                if 'organization_name' in item and item['organization_name'] != None:
                    organization_name = self.clearZFC(
                        item['organization_name'])

                    organization_id = self._getOrganizationId(
                        organization_name, temp_organization, default_organization_id, 'name')
                    newData['organization_id'] = organization_id
                    temp_organization[organization_name] = organization_id
                else:
                    newData['organization_id'] = default_organization_id
                    temp_organization[organization_name] = default_organization_id

                # 判断所属行政编号
                if 'organization_code' in item and item['organization_code']:
                    organization_code = self.clearZFC(
                        item['organization_code'])
                    newData['admin_area_id'] = self._getAdministratorArea2(
                        organization_code, temp_area, default_area_id)
                    temp_area[organization_code] = newData['admin_area_id']

                # 生日日期
                if 'date_birth' in item:
                    defaultValue = self._getDefaultValue(
                        'date_birth', newData['personnel_info'])
                    newData['personnel_info']['date_birth'] = self._setTimeFormat(
                        item['date_birth'], defaultValue)

                person_data = get_info(newData, self.session)

                person_data['GUID'] = str(uuid.uuid1())
                person_data['version'] = 1
                person_data['bill_status'] = 'valid'
                person_data['valid_bill_id'] = str(uuid.uuid1())
                person_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                person_data = self.setImportDate(
                    person_data, '20200610')

                # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）

                # 机构储蓄对象
                org_account_data = FinancialAccountObject(
                    AccountType.account_recharg_wel, person_data['id'], person_data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                org_account = get_info(
                    org_account_data.to_dict(), self.session)

                org_account['GUID'] = str(uuid.uuid1())
                org_account['version'] = 1
                org_account['bill_status'] = 'valid'
                org_account['valid_bill_id'] = str(uuid.uuid1())
                org_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                org_account = self.setImportDate(
                    org_account, '20200610')

                # app储蓄对象
                app_account_data = FinancialAccountObject(
                    AccountType.account_recharg_app, person_data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                app_account = get_info(
                    app_account_data.to_dict(), self.session)

                app_account['GUID'] = str(uuid.uuid1())
                app_account['version'] = 1
                app_account['bill_status'] = 'valid'
                app_account['valid_bill_id'] = str(uuid.uuid1())
                app_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                app_account = self.setImportDate(
                    app_account, '20200610')

                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, person_data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)

                real_account['GUID'] = str(uuid.uuid1())
                real_account['version'] = 1
                real_account['bill_status'] = 'valid'
                real_account['valid_bill_id'] = str(uuid.uuid1())
                real_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                real_account = self.setImportDate(
                    real_account, '20200610')

                # 补贴账户对象
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, person_data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)

                subsidy_account['GUID'] = str(uuid.uuid1())
                subsidy_account['version'] = 1
                subsidy_account['bill_status'] = 'valid'
                subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                subsidy_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                subsidy_account = self.setImportDate(
                    subsidy_account, '20200610')

                # 慈善账户
                charitable_account_data = FinancialAccountObject(
                    AccountType.account_charitable, person_data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
                charitable_account = get_info(
                    charitable_account_data.to_dict(), self.session)

                charitable_account['GUID'] = str(uuid.uuid1())
                charitable_account['version'] = 1
                charitable_account['bill_status'] = 'valid'
                charitable_account['valid_bill_id'] = str(uuid.uuid1())
                charitable_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                charitable_account = self.setImportDate(
                    charitable_account, '20200610')

                # 主数据
                person_arr.append(person_data)

                role_data = {'role_id': default_role_id,
                             'principal_account_id': person_data['id'], 'role_of_account_id': person_data['organization_id']}

                role_data = get_info(role_data, self.session)

                role_data['GUID'] = str(uuid.uuid1())
                role_data['version'] = 1
                role_data['bill_status'] = 'valid'
                role_data['valid_bill_id'] = str(uuid.uuid1())
                role_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                role_data = self.setImportDate(
                    role_data, '20200610')

                # role表的数组
                role_arr.append(role_data)

                # 账户表
                account_arr.append(org_account)
                account_arr.append(app_account)
                account_arr.append(real_account)
                account_arr.append(subsidy_account)
                account_arr.append(charitable_account)

                count = count + 1
              # print('长者表第'+str(count)+'个')

        def process_func(db):
            insert_many_data(db, self.PT_User, person_arr)
            insert_many_data(db, self.PT_Role, role_arr)
            insert_many_data(db, self.PT_Account, account_arr)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
      # print('服务长者OVER')

    def import_excel_worker(self, data):
        # 导入工作人员
        pf = pd.DataFrame(data)
        pf.rename(columns={'人员名称': 'name', '证件类型': 'id_card_type', '证件号码': 'id_card', '性别': 'sex', '出生日期': 'date_birth',
                           '电话': 'telephone', '组织机构': 'organization_name', '籍贯': 'native_place', '家庭地址': 'address', '身份证地址': 'id_card_address', '行政区划': "admin_area_id", '备注': 'remark', '护理区域': 'nursing_area', '照片': 'photo', '学历': 'education', "岗位": 'post', '银行卡号': 'card_number', '工作人员类别': 'worker_category', '薪酬': 'salary', '从业年限': 'work_years', '个人介绍': 'personnel_introduce', '毕业院校': 'graduated_school', '专业': 'major', '专业资格职称': 'professional_qualification_name', '创建日期': 'create_date'}, inplace=True)
        condition = dataframe_to_list(pf)

        out_per = ['name', 'id_card']
        in_per = ['name', 'sex', 'id_card', 'telephone', 'native_place', 'address',
                  'id_card_address', 'education', 'post', 'nursing_area', 'card_number', 'worker_category', 'salary', 'personnel_introduce', 'graduated_school', 'major', 'work_years', 'professional_qualification_name', 'remark']

        # 组织机构的缓存数组
        temp_organization = {}
        # 区域编号的缓存数组
        temp_area = {}
        # 工作人员缓存数组
        temp_worker = {}

        person_arr = []
        role_arr = []
        account_arr = []

        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'
        # 工作人员id数组
        default_role_id = {
            '平台': '8ebfa90a-e355-11e9-b46e-a0a4c57e9ebe',
            '民政局': '8f1d4064-e355-11e9-b3a2-a0a4c57e9ebe',
            '福利院': '8e271ff0-e355-11e9-91a6-a0a4c57e9ebe',
            '服务商': '8f9809cc-e355-11e9-9d97-a0a4c57e9ebe',
            '幸福院': '8f3524cc-e355-11e9-9f87-a0a4c57e9ebe',
        }
        role_id = ''
        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员'))\
            .project({'_id': 0})
        res_worker = self.query(
            _filter_worker, self.PT_User)

        if len(res_worker) > 0:
            for item in res_worker:
                if 'id_card' in item:
                    temp_worker[item['id_card']] = item

        if len(condition) > 0:
            for item in condition:
                # 判断这个人是否存在
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    if id_card in temp_worker:
                        return '有重复的工作人员身份证【' + id_card + '】'

        if len(condition) > 0:
            count = 0
            origin_data = {}
            for item in condition:
                if 'id_card' in item and item['id_card'] != None:
                    # 这里根据身份证做判断
                    id_card = self.clearZFC(item['id_card'])

                    if id_card in temp_worker:
                        origin_data = temp_worker[id_card]
                else:
                    # 没有身份证就用名字和机构判断是否有导入过
                    if 'name' in item and item['name'] != None:
                        name = self.clearZFC(item['name'])

                        # 找出所属机构
                        if 'organization_name' in item and item['organization_name'] != None:
                            organization_name = self.clearZFC(
                                item['organization_name'])

                            organization_data = self._getOrganizationData(
                                organization_name, temp_organization, default_organization_id, 'name')
                            if organization_data != '':
                                role_id = default_role_id[organization_data['organization_info']
                                                          ['personnel_category']]

                                # 名字
                                _filter_name_exists = MongoBillFilter()
                                _filter_name_exists.match_bill((C('personnel_info.name') == name) & (C('organization_id') == organization_data['id']) & (C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员'))\
                                    .project({'_id': 0})
                                res_name_exists = self.query(
                                    _filter_name_exists, self.PT_User)

                                if len(res_name_exists) > 0:
                                    origin_data = res_name_exists[0]

                if len(origin_data) > 0:
                    # 有旧数据
                    # 获取excel表对应的组织机构ID
                    if 'organization_name' in item and item['organization_name'] != None:
                        organization_name = self.clearZFC(
                            item['organization_name'])

                        organization_data = self._getOrganizationData(
                            organization_name, temp_organization, default_organization_id, 'name')
                        if organization_data != '':
                            origin_data['organization_id'] = organization_data['id']
                            role_id = default_role_id[organization_data['organization_info']
                                                      ['personnel_category']]

                    # 判断personnel_info里的数据是否一致
                    origin_data = self._setValues(out_per, item, origin_data)

                    # 判断personnel_info里的数据是否一致
                    origin_data['personnel_info'] = self._setValues(
                        in_per, item, origin_data['personnel_info'])

                    # 创建时间
                    if 'create_date' in item:
                        defaultValue = self._getDefaultValue(
                            'create_date', origin_data)
                        origin_data['create_date'] = self._setTimeFormat(
                            item['create_date'], defaultValue)
                        origin_data['reg_date'] = origin_data['create_date']
                    # 生日日期
                    if 'date_birth' in item:
                        defaultValue = self._getDefaultValue(
                            'date_birth', origin_data['personnel_info'])
                        origin_data['personnel_info']['date_birth'] = self._setTimeFormat(
                            item['date_birth'], defaultValue)
                    # 判断所属行政编号
                    if 'admin_area_id' in item:
                        admin_area_id = self.clearZFC(item['admin_area_id'])
                        defaultValue = self._getDefaultValue(
                            'admin_area_id', origin_data, default_area_id)
                        origin_data['admin_area_id'] = self._getAdministratorArea(
                            admin_area_id, temp_area, defaultValue)
                        temp_area[admin_area_id] = origin_data['admin_area_id']
                    # 照片
                    if 'photo' in item and item['photo'] != None:
                        photo = self.get_url_upload_url(
                            self.clearZFC(item['photo']), 'workerPhoto_0611')
                        if photo != '':
                            origin_data['personnel_info']['photo'] = [photo]

                    # 导入标识
                    origin_data = self.setImportDate(origin_data, '20200610')

                    def process_func(db):
                        update_data(db, self.PT_User, origin_data, {
                            'id': origin_data['id'],
                            'bill_status': 'valid',
                        })
                    process_db(self.db_addr, self.db_port, self.db_name,
                               process_func, self.db_user, self.db_pwd)
                else:
                    newData = {
                        "name": "",
                        "personnel_type": "1",
                        "admin_area_id": default_area_id,
                        "town": "",
                        "id_card_type": "身份证",
                        "id_card": "",
                        "organization_id": default_organization_id,
                        "reg_date": '',
                        "login_info": [],
                        "personnel_info": {
                            "name": "",
                            "id_card": "",
                            "date_birth": "",
                            "sex": "",
                            "telephone": "",
                            "remarks": "",
                            "personnel_category": "工作人员",
                            "address": "",
                            "is_alive": "",
                            "personnel_classification": "",
                            "native_place": '',
                            "id_card_address": '',
                            "nursing_area": '',
                            "post": '',
                            "education": '',
                            "role_name": '',
                            "worker_category": '',
                            "card_number": '',
                            "salary": '',
                            "personnel_introduce": '',
                            "graduated_school": '',
                            "major": '',
                            "work_years": '',
                            "professional_qualification_name": '',
                            "remark": '',
                            "photo": [],
                        }
                    }
                    # 判断personnel_info里的数据是否一致
                    newData = self._setValues(out_per, item, newData)
                    # 判断personnel_info里的数据是否一致
                    newData['personnel_info'] = self._setValues(
                        in_per, item, newData['personnel_info'])
                    # 获取excel表对应的组织机构ID
                    if 'organization_name' in item and item['organization_name'] != None:
                        organization_name = self.clearZFC(
                            item['organization_name'])

                        organization_data = self._getOrganizationData(
                            organization_name, temp_organization, default_organization_id, 'name')
                        if organization_data != '':
                            newData['organization_id'] = organization_data['id']
                            role_id = default_role_id[organization_data['organization_info']
                                                      ['personnel_category']]
                    # 创建时间
                    if 'create_date' in item:
                        newData['create_date'] = self._setTimeFormat(
                            item['create_date'])
                        newData['reg_date'] = newData['create_date']
                    else:
                        newData['create_date'] = get_cur_time()
                        newData['reg_date'] = newData['create_date']

                    # 生日日期
                    if 'date_birth' in item:
                        newData['personnel_info']['date_birth'] = self._setTimeFormat(
                            item['date_birth'])
                    # 判断所属行政编号
                    if 'admin_area_id' in item:
                        admin_area_id = self.clearZFC(item['admin_area_id'])
                        newData['admin_area_id'] = self._getAdministratorArea(
                            admin_area_id, temp_area, default_area_id)
                        temp_area[admin_area_id] = newData['admin_area_id']
                    # 照片
                    if 'photo' in item and item['photo'] != None:
                        photo = self.get_url_upload_url(
                            self.clearZFC(item['photo']), 'workerPhoto')
                        if photo != '':
                            newData['personnel_info']['photo'] = [photo]

                    person_data = get_info(newData, self.session)

                    person_data['GUID'] = str(uuid.uuid1())
                    person_data['version'] = 1
                    person_data['bill_status'] = 'valid'
                    person_data['valid_bill_id'] = str(uuid.uuid1())
                    person_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    person_data = self.setImportDate(
                        person_data, '20200610')

                    # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）

                    # 机构储蓄对象
                    org_account_data = FinancialAccountObject(
                        AccountType.account_recharg_wel, person_data['id'], person_data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                    org_account = get_info(
                        org_account_data.to_dict(), self.session)

                    org_account['GUID'] = str(uuid.uuid1())
                    org_account['version'] = 1
                    org_account['bill_status'] = 'valid'
                    org_account['valid_bill_id'] = str(uuid.uuid1())
                    org_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    org_account = self.setImportDate(
                        org_account, '20200610')

                    # app储蓄对象
                    app_account_data = FinancialAccountObject(
                        AccountType.account_recharg_app, person_data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                    app_account = get_info(
                        app_account_data.to_dict(), self.session)

                    app_account['GUID'] = str(uuid.uuid1())
                    app_account['version'] = 1
                    app_account['bill_status'] = 'valid'
                    app_account['valid_bill_id'] = str(uuid.uuid1())
                    app_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    app_account = self.setImportDate(
                        app_account, '20200610')

                    # 真实账户对象
                    real_account_data = FinancialAccountObject(
                        AccountType.account_real, person_data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                    real_account = get_info(
                        real_account_data.to_dict(), self.session)

                    real_account['GUID'] = str(uuid.uuid1())
                    real_account['version'] = 1
                    real_account['bill_status'] = 'valid'
                    real_account['valid_bill_id'] = str(uuid.uuid1())
                    real_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    real_account = self.setImportDate(
                        real_account, '20200610')

                    # 补贴账户对象
                    subsidy_account_data = FinancialAccountObject(
                        AccountType.account_subsidy, person_data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                    subsidy_account = get_info(
                        subsidy_account_data.to_dict(), self.session)

                    subsidy_account['GUID'] = str(uuid.uuid1())
                    subsidy_account['version'] = 1
                    subsidy_account['bill_status'] = 'valid'
                    subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                    subsidy_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    subsidy_account = self.setImportDate(
                        subsidy_account, '20200610')

                    # 慈善账户
                    charitable_account_data = FinancialAccountObject(
                        AccountType.account_charitable, person_data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
                    charitable_account = get_info(
                        charitable_account_data.to_dict(), self.session)

                    charitable_account['GUID'] = str(uuid.uuid1())
                    charitable_account['version'] = 1
                    charitable_account['bill_status'] = 'valid'
                    charitable_account['valid_bill_id'] = str(uuid.uuid1())
                    charitable_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    charitable_account = self.setImportDate(
                        charitable_account, '20200610')

                    # 主数据
                    person_arr.append(person_data)

                    role_data = {'role_id': role_id,
                                 'principal_account_id': person_data['id'], 'role_of_account_id': person_data['organization_id']}

                    role_data = get_info(role_data, self.session)

                    role_data['GUID'] = str(uuid.uuid1())
                    role_data['version'] = 1
                    role_data['bill_status'] = 'valid'
                    role_data['valid_bill_id'] = str(uuid.uuid1())
                    role_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                    role_data = self.setImportDate(
                        role_data, '20200610')

                    # role表的数组
                    role_arr.append(role_data)

                    # 账户表
                    account_arr.append(org_account)
                    account_arr.append(app_account)
                    account_arr.append(real_account)
                    account_arr.append(subsidy_account)
                    account_arr.append(charitable_account)

                    count = count + 1
                  # print('工作人员表第'+str(count)+'个')

        if len(person_arr) > 0:
          # print('开始插入' + str(len(person_arr)) + '条数据')
            def process_insert_func(db):
                insert_many_data(db, self.PT_User, person_arr)
                insert_many_data(db, self.PT_Role, role_arr)
                insert_many_data(db, self.PT_Account, account_arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_insert_func, self.db_user, self.db_pwd)
          # print('工作人员OVER')

            # # 导入标识
            # newData = self.setImportDate(newData, '20200610')

            # data = get_info(newData, self.session)

            # # 建立四个账户表
            # financialAccountsResult = self._getFinancialAccount(data)

            # data = financialAccountsResult[0]

            # tables = [self.PT_User, self.PT_Account]
            # datas = [[data], financialAccountsResult[1]]

            # set_role = {'role_id': role_id,
            #             'principal_account_id': data['id'], 'role_of_account_id': data['organization_id']}
            # set_role_data = get_info(set_role, self.session)
            # datas.append(set_role_data)
            # tables.append(self.PT_Role)
            # try:
            #     self.bill_manage_service.add_bill(OperationType.add.value,
            #                                       TypeId.user.value, datas, tables)
            # except Exception as e:
            #   #print(e)
        # return '导入成功'

    # 给user表的数据设置账户表
    def _getFinancialAccount(self, data):
        if 'organization_info' in data:
            # 组织类型为福利院
            if data['organization_info']['personnel_category'] == '福利院':
                # 新增一个真实账户
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data['id'], None, AccountStatus.normal, 1, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                data['agent_account'] = real_account['id']
                data['main_account'] = real_account['id']
                return [data, [real_account]]
            # 组织类型为服务商
            if data['organization_info']['personnel_category'] == '服务商':
                # 新增一个真实账户 + 补贴账户
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data['id'], None, AccountStatus.normal, 1, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                data['agent_account'] = real_account['id']
                data['main_account'] = real_account['id']
                # 补贴账户
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, data['id'], None, AccountStatus.normal, 2, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)
                return [data, [real_account, subsidy_account]]
        elif 'personnel_info' in data:
            # 个人用户创建四个账户

            # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
            # 机构储蓄对象
            org_account_data = FinancialAccountObject(
                AccountType.account_recharg_wel, data['id'], data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
            org_account = get_info(
                org_account_data.to_dict(), self.session)
            # app储蓄对象
            app_account_data = FinancialAccountObject(
                AccountType.account_recharg_app, data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
            app_account = get_info(
                app_account_data.to_dict(), self.session)
            # 真实账户对象
            real_account_data = FinancialAccountObject(
                AccountType.account_real, data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
            real_account = get_info(
                real_account_data.to_dict(), self.session)
            # 补贴账户对象
            subsidy_account_data = FinancialAccountObject(
                AccountType.account_subsidy, data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
            subsidy_account = get_info(
                subsidy_account_data.to_dict(), self.session)
            # 慈善账户
            charitable_account_data = FinancialAccountObject(
                AccountType.account_charitable, data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
            charitable_account = get_info(
                charitable_account_data.to_dict(), self.session)
            return [data, [org_account, app_account,
                           real_account, subsidy_account, charitable_account]]
        return False

    def import_excel_service_item(self, data):
        # 导入根据服务产品，导入服务项目，再生成服务产品
        pf = pd.DataFrame(data)
        pf.rename(columns={'服务方式': 'fwfs', '服务项目内容': 'fwxmnr', '服务项目价格': 'fwxmjg', '服务项目销售量': 'fwxmxsl', '服务周期': 'fwzq', '是否发布到app': 'sffbdapp',
                           '服务类型': 'fwlx', '审核状态': 'shzt', '审核建议': 'shjy', '审核日期': 'shrq', '封面图片': 'fmtp', '服务项目图片': 'fwxmtp', '服务机构': 'fwjg', '服务项目名称': 'fwxmmc', '创建日期': 'cjrq'}, inplace=True)
        condition = dataframe_to_list(pf)

        # 服务选项数据表
        service_option_table = 'ZPT_Service_Option'
        # 服务项目数据表
        service_item_table = 'ZPT_Service_Item'
        # 服务产品数据表
        service_product_table = 'ZPT_Service_Product'

        # 服务选项
        # 判断有没有import_date为20191231的服务选项，以这个为单价
        _filter_fwxx = MongoBillFilter()
        _filter_fwxx.match_bill((C('import_date') == '20191231'))\
            .project({'_id': 0})
        res_fwxx = self.query(
            _filter_fwxx, service_option_table)

        if len(res_fwxx) == 0:
            # 没有，新创一个
            new_service_option = {
                "name": "服务商产品单价",
                "default_value": "0",
                "is_modification_mermissible": "是",
                "remarks": "服务商产品单价",
                "option_type": "service_before",
                "option_value_type": "3",
                # 这是平台的组织机构ID
                "organization_id": "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                "option_value": "0",
                "import_date": "20191231"
            }
            new_service_option_info = get_info(
                new_service_option, self.session)

            service_option = new_service_option_info

            self.bill_manage_service.add_bill(OperationType.add.value,
                                              TypeId.serviceItem.value, new_service_option_info, service_option_table)
        else:
            service_option = res_fwxx[0]

        # 组织机构的储存
        temp_fwjg = {}

        if len(condition) > 0:
            for item in condition:
                # 服务项目基础模板
                default_service_item = {
                    "name": "",
                    "periodicity": "",
                    "organization_id": "",
                    "item_type": [],
                    "service_project_website": "",
                    "application_scope": [],
                    "is_standard_service": "是",
                    "is_allowance": "1",
                    "proportion": [
                        {
                            "contents": 0,
                            "title": "平台"
                        },
                        {
                            "contents": 1,
                            "title": "服务商"
                        },
                        {
                            "contents": 0,
                            "title": "慈善"
                        }
                    ],
                    "valuation_formula": [
                        {
                            "name": "标准计价公式",
                            "remark": "标准计价公式",
                            "formula": "float('服务商产品单价')"
                        }
                    ],
                    "contract_clauses": "",
                    "service_option": [
                        {
                            "default_value": service_option['default_value'],
                            "id":service_option['id'],
                            "is_modification_mermissible":service_option['is_modification_mermissible'],
                            "name":service_option['name'],
                            "option_type":service_option['option_type'],
                            "option_value_type":service_option['option_value_type'],
                            "remarks":service_option['remarks'],
                        }
                    ],
                    "import_date": "20191231"
                }

                # 替换导入的数据
                fwxmmc = self.clearZFC(item['fwxmmc'])
                default_service_item['name'] = fwxmmc
                # 服务类型
                if 'fwlx' in item and item['fwlx'] != None:
                    default_service_item['item_type'] = [
                        self.clearZFC(item['fwlx'])]
                # else:
                    # default_service_product['item_type'] = []

                # 服务周期
                if 'fwzq' in item and item['fwzq'] != None:
                    default_service_item['periodicity'] = self.clearZFC(
                        item['fwzq'])
                else:
                    default_service_product['periodicity'] = ''

                # 判断所属机构
                if 'fwjg' in item and item['fwjg'] != None:
                    fwjg = self.clearZFC(item['fwjg'])
                    if fwjg in temp_fwjg:
                        default_service_item['organization_id'] = temp_fwjg[fwjg]
                        # 赋值。下面生成服务产品就不用重复找了
                        item['organization_id'] = temp_fwjg[fwjg]
                    else:
                        _filter_fwjg = MongoBillFilter()
                        _filter_fwjg.match_bill((C('name') == fwjg))\
                            .project({'_id': 0})
                        res_fwjg = self.query(
                            _filter_fwjg, self.PT_User)

                        if len(res_fwjg) > 0:
                            temp_fwjg[fwjg] = res_fwjg[0]['id']
                            default_service_item['organization_id'] = res_fwjg[0]['id']
                            # 赋值。下面生成服务产品就不用重复找了
                            item['organization_id'] = res_fwjg[0]['id']

                # 导入一个服务选项
                service_option_info = get_info(
                    default_service_item, self.session)

                self.bill_manage_service.add_bill(OperationType.add.value,
                                                  TypeId.serviceItem.value, service_option_info, service_item_table)
                # 服务产品基础模板
                default_service_product = {
                    "name": "服务产品名称",
                    "introduce": "服务产品描述",
                    "remarks": "",
                    "state": 1,
                    "is_assignable": "1",
                    "is_external": "1",
                    "is_recommend": "是",
                    "organization_id": item['organization_id'],
                    "service_product_type": [],
                    "remaining_times": "1",
                    "picture_collection": [],
                    "additonal_contract_terms": "",
                    "service_product_introduce": "",
                    "is_service_item": "true",
                    "service_item_id": service_option_info['id'],
                    "valuation_formula": service_option_info['valuation_formula'],
                    "proportion": [
                        {
                            "contents": 0,
                            "title": "平台"
                        },
                        {
                            "contents": 1,
                            "title": "服务商"
                        },
                        {
                            "contents": 0,
                            "title": "慈善"
                        }
                    ],
                    "service_option": [
                        {
                            "default_value": service_option['default_value'],
                            "id":service_option['id'],
                            "is_modification_mermissible":service_option['is_modification_mermissible'],
                            "name":service_option['name'],
                            "option_type":service_option['option_type'],
                            "option_value_type":service_option['option_value_type'],
                            "remarks":service_option['remarks'],
                            # 这里要用价格
                            "option_value":self.clearZFC(item['fwxmjg'])
                        }
                    ],
                    "import_date": "20191231"
                }
                # 已有字段
                # 服务项目名称
                if 'fwxmmc' in item and item['fwxmmc'] != None:
                    default_service_product['name'] = self.clearZFC(
                        item['fwxmmc'])
                else:
                    default_service_product['name'] = ''
                # 服务项目内容
                if 'fwxmnr' in item and item['fwxmnr'] != None:
                    default_service_product['introduce'] = self.clearZFC(
                        item['fwxmnr'])
                else:
                    default_service_product['introduce'] = ''
                # 服务项目图片
                if 'fwxmtp' in item and item['fwxmtp'] != None:
                    fwxmtp = self.get_url_upload_url(
                        self.clearZFC(item['fwxmtp']))
                    if fwxmtp != '':
                        default_service_product['picture_collection'] = [
                            fwxmtp]
                else:
                    default_service_product['picture_collection'] = []
                # 以下字段是原表没有，但是额外储存的
                # 服务方式
                if 'fwfs' in item and item['fwfs'] != None:
                    default_service_product['service_mode'] = self.clearZFC(
                        item['fwfs'])
                else:
                    default_service_product['service_mode'] = ''
                # 销售量
                if 'fwxmxsl' in item and item['fwxmxsl'] != None:
                    default_service_product['sales_volume'] = self.clearZFC(
                        item['fwxmxsl'])
                else:
                    default_service_product['sales_volume'] = ''
                # 是否发布到APP
                if 'sffbdapp' in item and item['sffbdapp'] != None:
                    default_service_product['is_app'] = self.clearZFC(
                        item['sffbdapp'])
                else:
                    default_service_product['is_app'] = ''
                # 审核状态
                if 'shzt' in item and item['shzt'] != None:
                    default_service_product['audit_status'] = self.clearZFC(
                        item['shzt'])
                else:
                    default_service_product['audit_status'] = ''
                # 审核建议
                if 'shjy' in item and item['shjy'] != None:
                    default_service_product['audit_suggestion'] = self.clearZFC(
                        item['shjy'])
                else:
                    default_service_product['audit_suggestion'] = ''
                # 审核日期
                if 'shrq' in item and item['shrq'] != None:
                    default_service_product['audit_date'] = self.strtodate(
                        self.clearZFC(item['shrq']))
                else:
                    default_service_product['audit_date'] = ''
                # 封面图片
                if 'fmtp' in item and item['fmtp'] != None:
                    fmtp = self.get_url_upload_url(self.clearZFC(item['fmtp']))
                    if fmtp != '':
                        default_service_product['cover_photo'] = [fmtp]
                else:
                    default_service_product['cover_photo'] = []

                # 导入一个服务产品
                service_product_info = get_info(
                    default_service_product, self.session)

                # 替换创建时间
                if 'cjrq' in item and item['cjrq'] != None:
                    default_service_product['create_date'] = self.strtodate(
                        self.clearZFC(item['cjrq']))

                self.bill_manage_service.add_bill(OperationType.add.value,
                                                  TypeId.serviceProduct.value, service_product_info, service_product_table)

        return

    def import_excel_service_product(self, data):
        # 导入服务套餐
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构名称': 'jgmc', '服务套餐主键': 'fwtczj', '服务套餐名字': 'fwtcmz', '服务方式': 'fwfs', '服务套餐描述': 'fwtcms', '服务套餐价格': 'fwtcjg', '有效期': 'yxq', '服务套餐销量': 'fwtcxl', '是否发布到APP': 'sffbdapp',
                           '服务项目内容': 'fwxmnr', '服务项目名称': 'fwxmmc', '服务项目价格': 'fwxmjg', '服务次数': 'fwcs', '创建日期': 'cjrq', '发布日期': 'fbrq', '失效日期': 'sxrq', '审核状态': 'shzt', '审核建议': 'shjy', '审核日期': 'shrq', '服务套餐封面图': 'fwtcfmt', '服务套餐图片': 'fwtctp'}, inplace=True)
        condition = dataframe_to_list(pf)

        _filter = MongoBillFilter()
        _filter.match_bill((C('bill_status') == 'valid'))\
            .project({'_id': 0})
        res = self.query(
            _filter, 'PT_Service_Product3')

        count = 0
      # print(len(res))

        if len(res) > 0:
            for item in res:
                count = count + 1
              # print(str(count))
                for itm in item['service_product']:
                    itm['remaining_times'] = item['remaining_times']

                def process_func(db):
                    update_data(db, 'PT_Service_Product3', item, {
                        'id': item['id'],
                        'bill_status': 'valid',
                    })
                process_db("127.0.0.1", 27017, 'IEC',
                           process_func, 'admin', '123456')
        return

        # 服务项目数据表
        service_item_table = 'PT_Service_Product'
        # 服务产品数据表
        service_product_table = 'PT_Service_Product'

        # 储存所有的
        temp_all = {}
        # 服务机构
        temp_jgmc = {}

        if len(condition) > 0:
            for item in condition:
                default_service_product = {
                    "name": "",
                    "introduce": "",
                    "remarks": "",
                    "state": 1,
                    "is_external": "1",
                    "is_recommend": "是",
                    "organization_id": "",
                    "service_product_type": [],
                    "picture_collection": "",
                    "additonal_contract_terms": "",
                    "service_product_introduce": "",
                    "is_service_item": "false",
                    "service_product": [],
                    "is_assignable": "1",
                    "remaining_times": "1",
                }

                # 以主键作为套餐的标准
                if 'fwtczj' in item and item['fwtczj'] != None:
                    fwtczj = self.clearZFC(item['fwtczj'])
                    if fwtczj not in temp_all:
                        # 新的一个套餐

                        # 判断所属机构
                        if 'jgmc' in item and item['jgmc'] != None:
                            jgmc = self.clearZFC(item['jgmc'])
                            if jgmc in temp_jgmc:
                                default_service_product['organization_id'] = temp_jgmc[jgmc]
                                # 赋值。下面生成服务产品就不用重复找了
                                item['organization_id'] = temp_jgmc[jgmc]
                            else:
                                _filter_jgmc = MongoBillFilter()
                                _filter_jgmc.match_bill((C('name') == jgmc))\
                                    .project({'_id': 0})
                                res_jgmc = self.query(
                                    _filter_jgmc, self.PT_User)

                                if len(res_jgmc) > 0:
                                    temp_jgmc[jgmc] = res_jgmc[0]['id']
                                    default_service_product['organization_id'] = res_jgmc[0]['id']
                                    # 赋值。下面生成服务产品就不用重复找了
                                    item['organization_id'] = res_jgmc[0]['id']

                        # 服务套餐图片
                        if 'fwtctp' in item and item['fwtctp'] != None:
                            fwxmtp = self.get_url_upload_url(
                                self.clearZFC(item['fwtctp']))
                            if fwxmtp != '':
                                default_service_product['picture_collection'] = [
                                    fwxmtp]
                        else:
                            default_service_product['picture_collection'] = []
                        # 名字
                        if 'fwtcmz' in item and item['fwtcmz'] != None:
                            default_service_product['name'] = self.clearZFC(
                                item['fwtcmz'])
                        else:
                            default_service_product['name'] = ''
                        # 描述
                        if 'fwtcms' in item and item['fwtcms'] != None:
                            default_service_product['introduce'] = self.clearZFC(
                                item['fwtcms'])
                        else:
                            default_service_product['introduce'] = ''
                        # 服务方式
                        if 'fwfs' in item and item['fwfs'] != '':
                            default_service_product['service_mode'] = self.clearZFC(
                                item['fwfs'])
                        else:
                            default_service_product['service_mode'] = ''
                        # 服务套餐价格
                        if 'fwtcjg' in item and item['fwfs'] != '':
                            default_service_product['service_package_price'] = self.clearZFC(
                                item['fwtcjg'])
                        else:
                            default_service_product['service_package_price'] = ''
                        # 有效期
                        if 'yxq' in item and item['yxq'] != '':
                            default_service_product['term_of_validity'] = self.clearZFC(
                                item['yxq'])
                        else:
                            default_service_product['term_of_validity'] = ''
                        # 是否发布到app
                        if 'sffbdapp' in item and item['sffbdapp'] != '':
                            default_service_product['is_app'] = self.clearZFC(
                                item['sffbdapp'])
                        else:
                            default_service_product['is_app'] = ''
                        # 服务次数
                        if 'fwcs' in item and item['fwcs'] != '':
                            default_service_product['remaining_times'] = self.clearZFC(
                                item['fwcs'])
                        else:
                            default_service_product['remaining_times'] = '1'
                        # 销售量
                        if 'fwtcxl' in item and item['fwtcxl'] != None:
                            default_service_product['sales_volume'] = self.clearZFC(
                                item['fwtcxl'])
                        else:
                            default_service_product['sales_volume'] = ''
                        # 审核状态
                        if 'shzt' in item and item['shzt'] != None:
                            default_service_product['audit_status'] = self.clearZFC(
                                item['shzt'])
                        else:
                            default_service_product['audit_status'] = ''
                        # 审核建议
                        if 'shjy' in item and item['shjy'] != None:
                            default_service_product['audit_suggestion'] = self.clearZFC(
                                item['shjy'])
                        else:
                            default_service_product['audit_suggestion'] = ''
                        # 审核日期
                        if 'shrq' in item and item['shrq'] != None:
                            default_service_product['audit_date'] = self.strtodate(
                                self.clearZFC(item['shrq']))
                        else:
                            default_service_product['audit_date'] = ''
                        # 封面图片
                        if 'fwtcfm' in item and item['fwtcfm'] != None:
                            fwtcfm = self.get_url_upload_url(
                                self.clearZFC(item['fwtcfm']))
                            if fwtcfm != '':
                                default_service_product['cover_photo'] = [
                                    fwtcfm]
                        else:
                            default_service_product['cover_photo'] = []

                        # 发布日期
                        if 'fbrq' in item and item['fbrq'] != None:
                            default_service_product['release_date'] = self.strtodate(
                                self.clearZFC(item['fbrq']))
                        else:
                            default_service_product['release_date'] = ''
                        # 失效日期
                        if 'sxrq' in item and item['sxrq'] != None:
                            default_service_product['invalid_date'] = self.strtodate(
                                self.clearZFC(item['sxrq']))
                        else:
                            default_service_product['invalid_date'] = ''
                        # 服务项目
                        if 'fwxmmc' in item and item['fwxmmc'] != None:
                            fwxmmc = self.clearZFC(item['fwxmmc'])
                            # 判断机构及服务项目名称
                            _filter_fwxm = MongoBillFilter()
                            _filter_fwxm.match_bill((C('name') == fwxmmc) & (C('is_service_item') == "true") & (C('organization_id') == default_service_product['organization_id']))\
                                .project({'_id': 0})
                            res_fwxm = self.query(
                                _filter_fwxm, service_item_table)
                            default_service_product['service_product'].append({
                                'product_id': res_fwxm[0]['id']
                            })

                        # 导入一个服务产品
                        service_product_info = get_info(
                            default_service_product, self.session)

                        # 替换创建时间
                        if 'cjrq' in item and item['cjrq'] != None:
                            default_service_product['create_date'] = self.strtodate(
                                self.clearZFC(item['cjrq']))

                        temp_all[fwtczj] = service_product_info
                    else:
                        # 已经有直接添加一个服务选项id

                        # 服务项目
                        # 不允许这个为空，如果有错就直接抛出来
                        fwxmmc = self.clearZFC(item['fwxmmc'])
                        # 判断机构及服务项目名称
                        _filter_fwxm = MongoBillFilter()
                        _filter_fwxm.match_bill((C('name') == fwxmmc) & (C('is_service_item') == "true") & (C('organization_id') == temp_all[fwtczj]['organization_id']))\
                            .project({'_id': 0})
                        res_fwxm = self.query(
                            _filter_fwxm, service_item_table)
                        temp_all[fwtczj]['service_product'].append({
                            'product_id': res_fwxm[0]['id']
                        })

            # 遍历存起来
            for itm in temp_all.values():
                self.bill_manage_service.add_bill(OperationType.add.value,
                                                  TypeId.serviceProduct.value, itm, service_product_table)

    # 导入双鸭山人员
    def import_excel_sys_person(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'社区名称': 'area_name', '人员姓名': 'name', '性别': 'sex', '出生日期': 'date_birth', '身份证号': 'id_card', '特殊人群': 'special', '残疾人等级': 'broken_level', '社保类型': 'social_type', '户籍性质': 'place_type', '人户分离': 'person_place_leave', '与户主关系': 'relation', '户籍地址': 'address', '户口迁入地': 'place_from', '迁入时间': 'come_date', '房屋类型': 'home_type',
                           '工作单位': 'work_place', '固定电话': 'phone', '移动电话': 'telephone', '民族': 'nation', '政治面貌': 'political_outlook', '学历': 'education', '毕业院校': 'graduate_school', '毕业时间': 'graduate_date', '职业类型': 'work_type', '婚姻状况': 'marital_status', '宗教信仰': 'religious_belief', '兴趣爱好': 'hobby', '备注': 'remark', '长者类型': 'personnel_classification'}, inplace=True)
        condition = dataframe_to_list(pf)

        out_keys = ['name', 'id_card']
        in_keys = ['name', 'id_card', 'sex', 'telephone', 'education',
                   'nation', 'address', 'political_outlook', 'personnel_classification']
        other_keys = ['special', 'broken_level', 'social_type', 'place_type', 'person_place_leave', 'relation', 'place_from',
                      'home_type', 'work_place', 'phone', 'hobby', 'graduate_school', 'work_type', 'marital_status', 'religious_belief', 'hobby']
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认组织ID
        default_organization_id = 'c7e1ac7a-0d08-11ea-aebe-a0a4c57e8195'

        arr = []
        arr2 = []

        count = 0

        if len(condition) > 0:
            for item in condition:
                count = count + 1
                newData = {
                    'id_card_type': '身份证',
                    'personnel_type': '1',
                    'personnel_info': {
                        'personnel_category': '长者',
                        'role_id': default_role_id,
                        'other_params': {

                        }
                    },
                    'organization_id': default_organization_id,
                    'login_info': [],
                }
                otherParam = {}
                otherParam = self._setValues(other_keys, item, otherParam)
                newData = self._setValues(out_keys, item, newData)
                newData['personnel_info'] = self._setValues(
                    in_keys, item, newData['personnel_info'])
                newData['personnel_info']['other_params'] = otherParam

                _filter_code = MongoBillFilter()
                _filter_code.match_bill((C('name') == item['area_name']))\
                    .project({'_id': 0})
                area_info = self.query(
                    _filter_code, 'PT_Area_SYS')

                if len(area_info) > 0:
                    newData['admin_area_id'] = area_info[0]['id']

                if 'date_birth' in item and item['date_birth'] != None:
                    date_birth = self.clearZFC(item['date_birth'])
                    newData['personnel_info']['date_birth'] = self.strtodate(
                        date_birth)
                if 'come_date' in item and item['come_date'] != None:
                    come_date = self.clearZFC(item['come_date'])
                    newData['personnel_info']['other_params']['come_date'] = self.strtodate(
                        come_date)
                if 'graduate_date' in item and item['graduate_date'] != None:
                    graduate_date = self.clearZFC(item['graduate_date'])
                    newData['personnel_info']['other_params']['graduate_date'] = self.strtodate(
                        graduate_date)

                data = get_info(newData, self.session)

                # 建立四个账户表
                financialAccountsResult = self._getFinancialAccount(data)

                data = financialAccountsResult[0]

                tables = ['Z_PT_User_SYS', 'Z_PT_Financial_Account_SYS']
                datas = [[data], financialAccountsResult[1]]

                set_role = {'role_id': default_role_id,
                            'principal_account_id': data['id'], 'role_of_account_id': data['organization_id']}
                set_role_data = get_info(set_role, self.session)
                datas.append(set_role_data)
                tables.append('Z_PT_Set_Role_SYS')
                try:
                    self.bill_manage_service.add_bill(OperationType.add.value,
                                                      TypeId.user.value, datas, tables)
                except Exception as e:
                    pass
                  # print(e)

              # print('人员表第'+str(count)+'个')

    def import_excel_personnel3(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'status': 'zszt', '区域编号': 'qybh', '行政区划': 'xzqh', '人员姓名': 'ryxm', '性别': 'xb', '出生日期': 'csrq', '身份证号': 'sfzh', '所属组织': 'sszz', '特殊人群': 'tsrq', '残疾人等级': 'cjrdj', '社保类型': 'sblx', '户籍性质': 'hjxz', '人户分离': 'rhfl', '与户主关系': 'yhzgx', '户籍地址': 'hjdz',
                           '户口迁入地': 'hkqrd', '迁入时间': 'qrsj', '房屋类型': 'fwlx', '工作单位': 'gzdw', '固定电话': 'gddh', '移动电话': 'yddh', '民族': 'mz', '政治面貌': 'zzmm', '学历': 'xl', '毕业院校': 'byyx', '毕业时间': 'bysj', '职业类别': 'zylb', '婚姻状态': 'hyzt', '宗教信仰': 'zjxy', '兴趣爱好': 'xqah', '备注': 'bz', '长者类型': 'zzlx'}, inplace=True)
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)
        condition = dataframe_to_list(pf)

        # 组织机构的缓存数组
        tempZZ = {}
        # 区域编号的缓存数组
        tempQH = {}
        # 长者类型的缓存数组
        tempLX = {}

        usertb = self.PT_User
        financialtb = 'PT_Financial_Account'
        setroletb = 'PT_Set_Role'
        area_str = ''

        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        if len(condition) > 0:

            if condition[0]['ryxm'] == '邱月爱':
                area_str = 'cc'
                area_cn = '禅城'
            elif condition[0]['ryxm'] == '岑美梅':
                area_str = 'dl'
                area_cn = '大沥'
            elif condition[0]['ryxm'] == '���惠兴':
                area_str = 'dz'
                area_cn = '丹灶'
            elif condition[0]['ryxm'] == '黎丹丹':
                area_str = 'gc'
                area_cn = '桂城'
            elif condition[0]['ryxm'] == '李金弟':
                area_str = 'jj'
                area_cn = '九江'
            elif condition[0]['ryxm'] == '吴玉兰':
                area_str = 'na'
                area_cn = '空编号'
            elif condition[0]['ryxm'] == '柯少君':
                area_str = 'ls'
                area_cn = '里水'
            elif condition[0]['ryxm'] == '揭绍明':
                area_str = 'ot'
                area_cn = '其他'
            elif condition[0]['ryxm'] == '刘惠英':
                area_str = 'xq'
                area_cn = '西樵'
            elif condition[0]['ryxm'] == '刘巨亨':
                area_str = 'ss'
                area_cn = '狮山'

            usertb = usertb + '_' + area_str
            financialtb = financialtb + '_' + area_str
            setroletb = setroletb + '_' + area_str

            count = 0

            for item in condition:
                # 导入人员
                newData = {
                    "name": "",
                    "personnel_type": "1",
                    "admin_area_id": default_area_id,
                    "town": "",
                    "id_card_type": "身份证",
                    "id_card": "",
                    "organization_id": default_organization_id,
                    "reg_date": get_cur_time(),
                    "login_info": [],
                    "personnel_info": {
                        "name": "",
                        "id_card": "",
                        "date_birth": "",
                        "sex": "",
                        "telephone": "",
                        "remarks": "",
                        "personnel_category": "长者",
                        "address": "",
                        "role_id": default_role_id,
                        "is_alive": "",
                        "personnel_classification": "",
                    }
                }
                # 姓名
                if 'ryxm' in item and item['ryxm'] != None:
                    newData['name'] = item['ryxm']
                    newData['personnel_info']['name'] = item['ryxm']
                # 身份证号
                if 'sfzh' in item and item['sfzh'] != None:
                    newData['id_card'] = str(item['sfzh'])
                    newData['personnel_info']['id_card'] = str(item['sfzh'])
                # 性别
                if 'xb' in item and item['xb'] != None:
                    newData['personnel_info']['sex'] = '男' if item['xb'] == 'Male' else '女'
                # 地址
                if 'hjdz' in item and item['hjdz'] != None:
                    newData['personnel_info']['address'] = item['hjdz']
                # 电话
                if 'yddh' in item and item['yddh'] != None:
                    newData['personnel_info']['telephone'] = str(item['yddh'])
                # 在世状态
                if 'zszt' in item and item['zszt'] != None:
                    newData['personnel_info']['is_alive'] = True if item['zszt'] == 'Normal' else False
                # 长者类型
                if 'zzlx' in item and item['zzlx'] != None:
                    newData['personnel_info']['personnel_classification'] = item['zzlx']

                # 所属组织
                if 'sszz' in item and item['sszz'] != None:
                    if item['sszz'] in tempZZ:
                      # print('已有所属组织，无需重复获取')
                        newData['organization_id'] = tempZZ[item['sszz']]
                    else:
                        _filter_user = MongoBillFilter()
                        _filter_user.match_bill((C('name') == item['sszz']))\
                            .project({'_id': 0})
                        organization_info = self.query(
                            _filter_user, self.PT_User)

                        if len(organization_info) > 0:
                            tempZZ[item['sszz']] = organization_info[0]['id']
                            newData['organization_id'] = organization_info[0]['id']

                # 区域编号
                if 'qybh' in item and item['qybh'] != None:
                    qhbh = str(item['qybh'])
                    if qhbh in tempQH:
                      # print('已有区域编号，无需重复获取')
                        newData['admin_area_id'] = tempQH[qhbh]
                    else:
                        _filter_code = MongoBillFilter()
                        _filter_code.match_bill((C('code') == qhbh))\
                            .project({'_id': 0})
                        area_info = self.query(
                            _filter_code, 'PT_Administration_Division56')

                        if len(area_info) > 0:
                            tempQH[qhbh] = area_info[0]['id']
                            newData['admin_area_id'] = area_info[0]['id']

                # 人员类别
                if 'zzlx' in item and item['zzlx'] != None:
                    zzlx = str(item['zzlx'])
                    if zzlx in tempLX:
                      # print('已有长者类型，无需重复获取')
                        newData['personnel_info']['personnel_classification'] = tempLX[zzlx]
                    else:
                        _filter_leixing = MongoBillFilter()
                        _filter_leixing.match_bill((C('name') == zzlx))\
                            .project({'_id': 0})
                        lx_info = self.query(
                            _filter_leixing, 'PT_Personnel_Classification')

                        if len(lx_info) > 0:
                            tempQH[zzlx] = lx_info[0]['id']
                            newData['personnel_info']['personnel_classification'] = lx_info[0]['id']

                # 生日
                if 'csrq' in item and item['csrq'] != None:
                    newData['personnel_info']['date_birth'] = self.date_to_format(
                        item['csrq'])

                data = get_info(newData, self.session)

                # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
                # 机构储蓄对象
                org_account_data = FinancialAccountObject(
                    AccountType.account_recharg_wel, data['id'], data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                org_account = get_info(
                    org_account_data.to_dict(), self.session)
                # app储蓄对象
                app_account_data = FinancialAccountObject(
                    AccountType.account_recharg_app, data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                app_account = get_info(
                    app_account_data.to_dict(), self.session)
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                # 补贴账户对象
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)
                tables = [usertb, financialtb]
                datas = [[data], [org_account, app_account,
                                  real_account, subsidy_account]]

                set_role = {'role_id': default_role_id,
                            'principal_account_id': data['id'], 'role_of_account_id': data['organization_id']}
                set_role_data = get_info(set_role, self.session)
                datas.append(set_role_data)
                tables.append(setroletb)

                count = count + 1
            #   # print('开始导入第' + str((count)) + '个' + '_' +
            #     str(area_cn) + '_' + str(data['name']))
                try:
                    self.bill_manage_service.add_bill(OperationType.add.value,
                                                      TypeId.user.value, datas, tables)
                except Exception as e:
                    pass
                  # print(e)
        return '导入成功'

    def clearZFC(self, string):
        # 清除excel可能自带的换行符和空格符
        if string == None:
            return ''
        return str(string).strip().replace(
            '\r', '').replace('\n', '').replace('\t', '')

    # 设置机构类型
    def _setPersonnelCategory(self, value, defaultValue=''):
        if value != None:
            value = self.clearZFC(value)
            if value in JiGouLeiBie.__members__:
                return JiGouLeiBie[value].value
        if defaultValue != '':
            return defaultValue
        return ''

    # 设置时间格式
    def _setTimeFormat(self, value, defaultValue=''):
        if value != None:
            return self.strtodate(self.clearZFC(value))
        if defaultValue != '':
            return defaultValue
        return ''

    # 判断所属行政编号
    def _getAdministratorArea(self, value, temp={}, defaultValue=''):
        if value != None:
            value = self.clearZFC(value)
            if value in temp:
                return temp[value]
            _filter = MongoBillFilter()
            _filter.match_bill((C('name') == value))\
                .project({'_id': 0})
            res = self.query(
                _filter, 'PT_Administration_Division')

            if len(res) > 0:
                return res[0]['id']
        if defaultValue != '':
            return defaultValue
        return ''

    # 判断所属行政编号
    def _getAdministratorArea2(self, value, temp={}, defaultValue=''):
        if value != None:
            value = self.clearZFC(value)
            if value in temp:
                return temp[value]
            _filter = MongoBillFilter()
            _filter.match_bill((C('code') == value))\
                .project({'_id': 0})
            res = self.query(
                _filter, 'PT_Administration_Division')

            if len(res) > 0:
                return res[0]['id']
        if defaultValue != '':
            return defaultValue
        return ''

    # 根据产品名字和组织机构ID找出对应的产品详情
    def _getProductInfo(self, value, temp={}, organization_id=''):
        if value != None:
            value = self.clearZFC(value)
            if value in temp:
                return temp[value]
            _filter = MongoBillFilter()
            _filter.match_bill((C('name') == value) & (C('organization_id') == organization_id))\
                .project({'_id': 0})
            res = self.query(
                _filter, 'PT_Service_Product25')

            if len(res) > 0:
                return res
        return ''

    # 根据身份证号获取人员id
    def _getUserId(self, value, temp={}, defaultValue='', searchType='id_card', organization_id='', userType=''):
        if searchType == '':
            searchType = 'id_card'
        if value != None:
            value = self.clearZFC(value)
            if value in temp:
                return temp[value]
            _filter = MongoBillFilter()
            if searchType == 'name':
                _filter.match_bill((C('name') == value))
            else:
                _filter.match_bill((C('personnel_info.id_card') == value))
            # 组织机构
            if organization_id != '':
                _filter.match_bill((C('organization_id') == organization_id))
            # 工作人员或者长者
            if userType != '':
                if userType == 'worker':
                    _filter.match_bill(
                        (C('personnel_info.personnel_category') == '工作人员'))
                elif userType == 'elder':
                    _filter.match_bill(
                        (C('personnel_info.personnel_category') == '长者'))
            _filter.match_bill((C('personnel_type') == '1'))
            _filter.project({'_id': 0})
            res = self.query(
                _filter, self.PT_User)
            if len(res) > 0:
                return res[0]['id']
        if defaultValue != '':
            return defaultValue
        return ''

    # 根据组织编号获取机构数据
    def _getOrganizationData(self, value, temp={}, defaultValue='', searchType='code'):
        _filter = MongoBillFilter()
        if searchType == 'name':
            _filter.match_bill((C('name') == value) &
                               (C('personnel_type') == '2'))
        else:
            _filter.match_bill((C('organization_info.code') == value) & (
                C('personnel_type') == '2'))
        _filter.project({'_id': 0})
        res = self.query(
            _filter, self.PT_User)
        if len(res) > 0:
            return res[0]
        return ''

    # 根据组织编号获取机构id
    def _getOrganizationId(self, value, temp={}, defaultValue='', searchType='code'):
        if value != None:
            value = self.clearZFC(value)
            if value in temp:
                return temp[value]
        data = self._getOrganizationData(value, temp, defaultValue, searchType)
        if 'id' in data:
            return data['id']
        if defaultValue != '':
            return defaultValue
        return ''

    # 通过循环来填充数据
    def _setValues(self, keys, item, datas):
        for i in range(0, len(keys)):
            key = keys[i]
            if key in item and item[key] != None:
                value = self.clearZFC(item[key])
                # 如果旧数据有，而新数据没有，则跳过不替换
                if (key in datas and datas[key] != '' and value == '') or (key not in datas and value == ''):
                    continue
                datas[key] = self._getMeiJuValue(key, value)
        return datas

    # 判断是否是枚举类的数据，如果是就从枚举类上拿
    def _getMeiJuValue(self, key, value):
        MJ_ARRAY = ['sex', 'post', 'education', 'worker_category', 'create_method', 'buy_type', 'source_type', 'pay_status',
                    'periodicity', 'record_status', 'visit_status', 'service_satisfaction', 'checkin_status', 'sign_type', 'bed_status', 'article_status', 'activity_status', 'die_state', 'marriage_state', 'pension_category']
        if key in MJ_ARRAY:
            if key == 'sex' and value in Sex.__members__:
                return Sex[value].value
            elif key == 'post' and value in Post.__members__:
                return Post[value].value
            elif key == 'education' and value in XueLiZiZhi.__members__:
                return XueLiZiZhi[value].value
            elif key == 'worker_category' and value in WorkerCategory.__members__:
                return WorkerCategory[value].value
            elif key == 'create_method' and value in CreateMethod.__members__:
                return CreateMethod[value].value
            elif key == 'buy_type' and value in BuyType.__members__:
                return BuyType[value].value
            elif key == 'source_type' and value in SourceType.__members__:
                return SourceType[value].value
            elif key == 'pay_status' and value in PayStatus.__members__:
                return PayStatus[value].value
            elif key == 'periodicity' and value in Cycle.__members__:
                return Cycle[value].value
            elif key == 'record_status' and value in RecordStatus.__members__:
                return RecordStatus[value].value
            elif key == 'visit_status' and value in VisitStatus.__members__:
                return VisitStatus[value].value
            elif key == 'service_satisfaction' and value in ServiceEvaluation.__members__:
                return ServiceEvaluation[value].value
            elif key == 'checkin_status' and value in CheckInStatus2.__members__:
                return CheckInStatus2[value].value
            elif key == 'sign_type' and value in SignType.__members__:
                return SignType[value].value
            elif key == 'bed_status' and value in BedStatus.__members__:
                return BedStatus[value].value
            elif key == 'article_status' and value in ArticleStatus.__members__:
                return ArticleStatus[value].value
            elif key == 'activity_status' and value in ActivityStatus.__members__:
                return ActivityStatus[value].value
            elif key == 'die_state' and value in DieState.__members__:
                return DieState[value].value
            elif key == 'marriage_state' and value in MarriageState.__members__:
                return MarriageState[value].value
            elif key == 'pension_category' and value in YangLaoLeiXing.__members__:
                return YangLaoLeiXing[value].value
        return value

    # 获取默认数据
    def _getDefaultValue(self, key, item, defaultValue=''):
        if key not in item:
            if defaultValue != '':
                return defaultValue
            return ''
        return item[key]

    def import_excel_organization(self, data):
        # 导入机构
        pf = pd.DataFrame(data)
        pf.rename(columns={'名称': 'name', '编号': 'code', '地址': 'address', '经度': 'lon', '维度': 'lat', '法人': 'legal_person',
                           '联系电话': 'telephone', '机构性质': 'organization_nature', '行政区域（编号）': 'admin_area_id', '上级机构（编号）': 'sjjgbh', '星级': 'star_level', '简介': 'description', '备注': 'comment', '机构成立时间': 'reg_date', '类型': 'personnel_category'}, inplace=True)
        condition = dataframe_to_list(pf)

        usertb = self.PT_User
        financialtb = 'ZZPT_Financial_Account'

        temp_area = {}

        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'

        # 找出有没有
        if len(condition) > 0:
            for item in condition:

                # 这里根据编号做判断
                if 'code' in item and item['code'] != None:
                    code = self.clearZFC(item['code'])
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('organization_info.code') == code))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, usertb)
                else:
                    # 没有code的话，就用名字和幸福院类型作为判断依据
                    name = self.clearZFC(item['name'])
                    personnel_category = self.clearZFC(
                        item['personnel_category'])
                    personnel_category = JiGouLeiBie[personnel_category].value
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('name') == name) & (
                        C('organization_info.personnel_category') == personnel_category))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, usertb)

                out_org = ['name']

                in_org = ['code', 'telephone', 'lon', 'lat', 'address',
                          'comment', 'legal_person', 'organization_nature', 'description', 'star_level']

                # 有数据，用update_one更改修改后的数据
                if len(res) > 0:

                    # 判断organization_info里的数据是否一致
                    res[0] = self._setValues(out_org, item, res[0])

                    # 判断organization_info里的数据是否一致
                    res[0]['organization_info'] = self._setValues(
                        in_org, item, res[0]['organization_info'])

                    # 成立时间
                    if 'reg_date' in item:
                        defaultValue = self._getDefaultValue(
                            'reg_date', res[0])
                        res[0]['reg_date'] = self._setTimeFormat(
                            item['reg_date'], defaultValue)

                    # 机构类型
                    if 'personnel_category' in item:
                        res[0]['organization_info']['personnel_category'] = self._setPersonnelCategory(
                            item['personnel_category'])

                    # 判断所属行政编号
                    if 'admin_area_id' in item:
                        defaultValue = self._getDefaultValue(
                            'admin_area_id', res[0], default_area_id)
                        res[0]['admin_area_id'] = self._getAdministratorArea(
                            item['admin_area_id'], temp_area, defaultValue)
                        temp_area[self.clearZFC(
                            item['admin_area_id'])] = res[0]['admin_area_id']

                    # 导入标识
                    res[0] = self.setImportDate(res[0], '20200108')

                    def process_func(db):
                        update_data(db, usertb, res[0], {
                            'id': res[0]['id'],
                            'bill_status': 'valid',
                        })
                    process_db("127.0.0.1", 27017, 'IEC',
                               process_func, 'admin', '123456')
                else:
                    # 新增的机构数据
                    new_org = {
                        "name": "",
                        "admin_area_id": "da1e4201-70c5-407a-a088-c3a8cc91fef5",
                        "personnel_type": "2",
                        "town": "",
                        "organization_info": {
                            "code": "",
                            "telephone": "",
                            "organization_nature": "",
                            "lon": '',
                            "lat": '',
                            "personnel_category": "",
                            "legal_person": "",
                            "address": "",
                            "star_level": "",
                            "description": "",
                            "reg_date": "",
                            "super_org_id": "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                            "picture_list": []
                        },
                        "qualification_info": [],
                    }
                    # 新增

                    # 判断organization_info里的数据是否一致
                    new_org = self._setValues(out_org, item, new_org)

                    # 判断organization_info里的数据是否一致
                    new_org['organization_info'] = self._setValues(
                        in_org, item, new_org['organization_info'])

                    # 成立时间
                    if 'reg_date' in item:
                        new_org['reg_date'] = self._setTimeFormat(
                            item['reg_date'])

                    # 机构类型
                    if 'personnel_category' in item:
                        new_org['organization_info']['personnel_category'] = self._setPersonnelCategory(
                            item['personnel_category'])

                    # 判断所属行政编号
                    if 'admin_area_id' in item:
                        new_org['admin_area_id'] = self._getAdministratorArea(
                            item['admin_area_id'], temp_area)
                        temp_area[self.clearZFC(
                            item['admin_area_id'])] = new_org['admin_area_id']

                    # 导入标识
                    new_org = self.setImportDate(new_org, '20200108')

                    data = get_info(new_org, self.session)

                    # 建立账户表
                    financialAccountsResult = self._getFinancialAccount(data)

                    if financialAccountsResult != False:
                        # 主数据，账户表
                        datas = [financialAccountsResult[0],
                                 financialAccountsResult[1]]

                        try:
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.user.value, datas, [usertb, financialtb])
                        except Exception as e:
                            pass
                          # print(e)
                    else:
                        # 主数据
                        try:
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.user.value, data, usertb)
                        except Exception as e:
                            pass
                          # print(e)
        return '导入成功'

    # 导入资讯
    def import_excel_article(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构名称': 'organization_name', '资讯标题': 'title', '资讯内容': 'content', '封面图片': 'app_img_list',
                           '创建日期': 'create_date', '发布日期': 'audit_date', '状态': 'status', '创建人': 'author'}, inplace=True)
        condition = dataframe_to_list(pf)

        saveTable = 'ZPT_Article_0610'

        keys = ['title', 'content', 'app_img_list', 'create_date',
                'audit_date', 'status', 'author']

        # 默认平台的组织ID，导入的都是一百件大健康的
        default_organization_id = '87e94ca4-26ff-11ea-9fec-a0a4c57e9ebe'
        default_promulgator_id = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

        temp_organization = {}
        temp_worker = {}

        arr = []

        count = 0

        if len(condition) > 0:

            for item in condition:
                count = count + 1
                # 新增资讯数据
                newData = {}

                # 补充原数据
                newData = self._setValues(keys, item, newData)

                # 找出所属机构
                if 'organization_name' in item and item['organization_name'] != None:
                    organization_name = self.clearZFC(
                        item['organization_name'])

                    organization_id = self._getOrganizationId(
                        organization_name, temp_organization, default_organization_id, 'name')
                    newData['organization_id'] = organization_id
                    temp_organization[organization_name] = organization_id
                else:
                    newData['organization_id'] = default_organization_id
                    temp_organization[organization_name] = default_organization_id

                # 找出所属工作人员
                if 'author' in item and item['author'] != None:
                    author = self.clearZFC(item['author'])

                    _filter = MongoBillFilter()
                    _filter.match_bill((C('organization_id') == newData['organization_id']) & (C('personnel_info.personnel_category') == '工作人员') & (C('name') == author))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, self.PT_User)

                    if len(res) > 0:
                        newData['promulgator_id'] = res[0]['id']
                        temp_worker[author] = res[0]['id']

                # 照片
                if 'app_img_list' in item and item['app_img_list'] != None:
                    app_img_list = self.get_url_upload_url(
                        self.clearZFC('https://www.e-health100.com/' + item['app_img_list']), 'articlePhoto_0610')
                    if app_img_list != '':
                        newData['app_img_list'] = [app_img_list]
                else:
                    newData['app_img_list'] = []

                # 发布时间
                if 'audit_date' in item and item['audit_date'] != None:
                    audit_date = self.clearZFC(item['audit_date'])
                    newData['audit_date'] = self._setTimeFormat(audit_date)

                # 导入标识
                newData = self.setImportDate(newData, '20200610')

                newData['type_id'] = 'f1643398-cf91-11e9-9aab-000c29acd33e'

                # 补充主键
                newData['id'] = str(uuid.uuid1())

                # 默认审批步骤通过
                newData['step_no'] = -1
                newData['status'] = '通过'

                # 发布时间
                if 'create_date' in item and item['create_date'] != None:
                    create_date = self.clearZFC(item['create_date'])
                    newData['create_date'] = self._setTimeFormat(create_date)

                newData['GUID'] = str(uuid.uuid1())
                newData['modify_date'] = get_cur_time()
                newData['version'] = 1
                newData['bill_status'] = 'valid'
                newData['valid_bill_id'] = str(uuid.uuid1())
                newData['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('资讯表第'+str(count)+'个')

                # arr.append(data)

                def process_func(db):
                    insert_data(db, saveTable, newData)
                    # insert_many_data(db, saveTable, arr)
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

      # print('资讯表OVER')
        return 'FAILED'

    # 更新服务产品
    def update_service_product(self, data):
        _filter = MongoBillFilter()
        _filter.match_bill((C('bill_status') == 'valid') & (C('is_app') != None))\
            .project({'_id': 0})
        res = self.query(
            _filter, 'PT_Service_Product25')

        for item in res:

            # 是服务产品，对比服务产品是不是都一样
            if item['is_service_item'] == 'false':
                _filter = MongoBillFilter()
                _filter.match_bill((C('id') == item['id']))\
                    .project({'_id': 0})
                res_2 = self.query(
                    _filter, 'PT_Service_Product25')

                # if res_2[0] != res_2[1]:
                #     pass
                # print(item['id'])

                # print('over')

        return 'Failed'

    # 导入服务订单

    def import_excel_service_order(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'订单号': 'order_code', '购买人身份证': 'id_card', '服务地点': 'service_address', '长者联系方式': 'elder_telephone', '服务机构名称': 'organization_name', '服务套餐名字': 'service_package_name', '服务项目名称': 'service_option_name', '购买时间': 'order_date',
                           '付款状态': 'pay_status', '金额': 'amout', '服务次数': 'all_times', '服务内容': "remark"}, inplace=True)
        condition = dataframe_to_list(pf)

        serviceOrderTable = 'ZPT_Service_Order_1119'

        keys = ['order_code', 'id_card', 'service_address',
                'elder_telephone', 'pay_status', 'amout', 'remark']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}
        temp_user = {}
        temp_creator = {}
        temp_product = {}

        arr = []
        arr_all = {}

        count = 0

        allUser = {}

        # 查询所有长者
        _filter = MongoBillFilter()
        _filter = _filter.match_bill(
            (C('personnel_info.personnel_category') == '长者') & (C('personnel_type') == '1'))
        res = self.query(
            _filter, self.PT_User)

        ccc = 0

        for user_item in res:
            if 'id_card' in user_item:
                allUser[str(user_item['id_card'])] = user_item
            elif 'personnel_info' in user_item and 'id_card' in user_item['personnel_info']:
                ccc = ccc + 1
                allUser[str(user_item['personnel_info']
                            ['id_card'])] = user_item

      # print('cccc>>>>>>>', ccc)

        if len(condition) > 0:
            for item in condition:

                count = count + 1

                newData = {}

                # 补充原数据
                newData = self._setValues(keys, item, newData)

                # 创建日期和购买日期一样
                if 'order_date' in item and item['order_date'] != None:
                    order_date = self.strtodate(
                        self.clearZFC(item['order_date']))
                    newData['order_date'] = order_date
                    newData['create_date'] = order_date

                # 找出所属机构
                if 'organization_name' in item and item['organization_name'] != None:
                    organization_name = self.clearZFC(
                        item['organization_name'])

                    organization_id = self._getOrganizationId(
                        organization_name, temp_organization, default_organization_id, 'name')
                    newData['organization_id'] = organization_id
                    temp_organization[organization_name] = organization_id
                else:
                    newData['organization_id'] = default_organization_id
                    temp_organization[organization_name] = default_organization_id

                # 根据服务产品名字和组织机构ID查询对应的ID
                if 'service_option_name' in item and item['service_option_name'] != None:
                    service_option_name = self.clearZFC(
                        item['service_option_name'])
                    product_info = self._getProductInfo(
                        service_option_name, temp_product, newData['organization_id'])
                    temp_product[service_option_name] = product_info

                    if product_info != '':
                        if newData['order_code'] in arr_all:
                            arr_all[newData['order_code']]['detail'].append({
                                "product_id": product_info[0]['id'],
                                "product_name": service_option_name,
                                "valuation_formula": product_info[0]['valuation_formula'],
                                "service_option": product_info[0]['service_option'],
                                "is_assignable": "1",
                                "count": 1,
                                "all_times": int(item['all_times'])
                            })
                            continue
                        # 构建数据的detail
                        # 因为导入的excel都是单服务项目，所以这里没有做循环
                        newData['detail'] = []
                        newData['detail'].append({
                            "product_id": product_info[0]['id'],
                            "product_name": service_option_name,
                            "valuation_formula": product_info[0]['valuation_formula'],
                            "service_option": product_info[0]['service_option'],
                            "is_assignable": "1",
                            "count": 1,
                            "all_times": int(item['all_times'])
                        })

                        # 服务套餐名字
                        service_package_name = self.clearZFC(
                            item['service_package_name'])
                        package_info = self._getProductInfo(
                            service_package_name, {}, newData['organization_id'])

                        newData['origin_product'] = {
                            "product_id": package_info[0]['id'],
                            "product_name": service_package_name,
                        }

                # 购买者
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    if id_card in allUser:
                        newData['purchaser_id'] = allUser[id_card]['id']

                # 先付费
                # newData['is_paying_first'] = '1'
                # 已分派
                # newData['status'] = '已完成'
                # 转化为数字类型
                newData['amout'] = float(newData['amout'])
                # 提供商暂时用组织机构ID
                newData['service_provider_id'] = newData['organization_id']

                # 导入标识
                newData = self.setImportDate(
                    newData, '20201119')

                # 补充主键
                newData['id'] = str(uuid.uuid1())
                newData['GUID'] = str(uuid.uuid1())
                newData['version'] = 1
                newData['bill_status'] = 'valid'
                newData['valid_bill_id'] = str(uuid.uuid1())
                newData['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('服务订单第'+str(count)+'个')

                arr_all[newData['order_code']] = newData

        for itm in arr_all:
            arr.append(arr_all[itm])

        def process_func(db):
            insert_many_data(db, serviceOrderTable, arr)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
      # print('服务订单OVER')

    # 导入床位信息
    def import_excel_bed(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构主键': 'organization_code', '所属区域': 'zone_name', '大楼名称': 'building_name', '房号': 'room_name', '楼层': 'floor_name', '床位主键': 'bed_primary_id', '床号': 'bed_name', '床位状态': 'bed_status', '床位费用': 'bed_price',
                           '入住/退住': 'checkin_status', '每月费用': 'price_everymonth', '入住/退住日期': 'checkin_date', '处理日期': 'create_date', '结束日期': 'end_date', '长者名称': 'elder_name', '长者身份证': 'id_card', '签入类型': 'sign_type', '一次性费用': 'price_onetimes'}, inplace=True)
        condition = dataframe_to_list(pf)

        # 已经有了，就补充信息
        keys = ['bed_status', 'bed_price', 'checkin_status', 'price_everymonth',
                'elder_name', 'id_card', 'sign_type', 'price_onetimes']

        # 床位表
        bedTable = 'PT_Bed'
        # 区域表
        hotelZoneTable = 'PT_Hotel_Zone'

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}
        temp_user = {}
        temp_zone = {}
        temp_building = {}
        temp_floor = {}
        temp_room = {}

        # 数字格式转换
        numberChange = {
            '1': '一',
            '2': '二',
            '3': '三',
            '4': '四',
            '5': '五',
            '6': '六',
            '7': '七',
            '8': '八',
            '9': '九',
        }
        count = 0

        arr = []

        if len(condition) > 0:
            for item in condition:
                count = count + 1
              # print('导入床位信息第'+str(count)+'个')

                # 所属机构
                if 'organization_code' in item and item['organization_code'] != None:
                    organization_code = self.clearZFC(
                        item['organization_code'])
                    organization_id = self._getOrganizationId(
                        organization_code, temp_organization, default_organization_id)
                    temp_organization[organization_code] = organization_id
                else:
                    organization_id = default_organization_id
                    temp_organization[organization_code] = default_organization_id

                # 判断有没有区域信息
                if 'zone_name' in item and item['zone_name'] != None:
                    # 区域的typeid
                    area_type_id = '574ccb70-36a5-11ea-95d5-a0a4c57e9ebe'
                    zone_name = self.clearZFC(item['zone_name'])
                    if zone_name in temp_zone:
                        zone_name_id = temp_zone[zone_name]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('zone_name') == zone_name) & (C('organization_id') == organization_id) & (C('hotel_zone_type_id') == area_type_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, hotelZoneTable)

                        # 找出大区域的ID
                        if len(res) == 0:
                            zone_name_id = str(uuid.uuid1())
                            # 新增一个区域的信息
                            newArea = {
                                'id': zone_name_id,
                                'zone_name': zone_name,
                                'zone_number': zone_name,
                                'hotel_zone_type_id': area_type_id,
                                'remark': zone_name,
                                'organization_id': organization_id
                            }
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.hotelZone.value, newArea, hotelZoneTable)
                        else:
                            zone_name_id = res[0]['id']
                        # 缓存有问题，禁止缓存
                        # temp_zone[zone_name] = zone_name_id

                # 找出这个大区域下有没有这个楼的信息
                if 'building_name' in item and item['building_name'] != None:
                    # 栋楼的typeid
                    building_type_id = 'fb6562b6-ea32-11e9-b4aa-a0a4c57e9ebe'
                    building_name = self.clearZFC(item['building_name'])
                    if building_name in temp_building:
                        building_name_id = temp_building[building_name]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('zone_name') == building_name) & (C('upper_hotel_zone_id') == zone_name_id) & (C('organization_id') == organization_id) & (C('hotel_zone_type_id') == building_type_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, hotelZoneTable)

                        # 找出这个楼的ID
                        if len(res) == 0:
                            building_name_id = str(uuid.uuid1())
                            # 新增一个楼的信息
                            newBuilding = {
                                'id': building_name_id,
                                'zone_name': building_name,
                                'zone_number': building_name,
                                'hotel_zone_type_id': building_type_id,
                                'hotel_zone_type_name': '栋',
                                'remark': building_name,
                                'upper_hotel_zone_id': zone_name_id,
                                'organization_id': organization_id
                            }
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.hotelZone.value, newBuilding, hotelZoneTable)
                        else:
                            building_name_id = res[0]['id']
                        # 缓存有问题，禁止缓存
                        # temp_building[building_name] = building_name_id

                # 找出这个楼下有没有这个层的信息
                if 'floor_name' in item and item['floor_name'] != None:
                    # 楼层的typeid
                    room_type_id = 'f0c2b50a-ea32-11e9-a294-a0a4c57e9ebe'
                    floor_name = self.clearZFC(
                        item['building_name']) + numberChange[self.clearZFC(item['floor_name'])] + '楼'
                    if floor_name in temp_floor:
                        floor_name = temp_floor[floor_name]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('zone_name').like(floor_name)) & (C('upper_hotel_zone_id') == building_name_id) & (C('organization_id') == organization_id) & (C('hotel_zone_type_id') == room_type_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, hotelZoneTable)

                        # 找出这个层的ID
                        if len(res) == 0:
                            floor_name_id = str(uuid.uuid1())
                            # 新增一个楼层的信息
                            newFloor = {
                                'id': floor_name_id,
                                'zone_name': floor_name,
                                'zone_number': floor_name,
                                'hotel_zone_type_id': room_type_id,
                                'hotel_zone_type_name': '楼',
                                'remark': floor_name,
                                'upper_hotel_zone_id': building_name_id,
                                'organization_id': organization_id
                            }
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.hotelZone.value, newFloor, hotelZoneTable)
                        else:
                            floor_name_id = res[0]['id']
                        # 缓存有问题，禁止缓存
                        # temp_floor[floor_name] = floor_name_id

                # 根据最终找到的这个楼层的id，判断这个房是否已经录入
                if 'room_name' in item and item['room_name'] != None:
                    # 房的typeid
                    room_type_id = '01303228-ea33-11e9-b7d2-a0a4c57e9ebe'
                    room_name = self.clearZFC(item['room_name'])
                    if room_name[0] == '+':
                        search_room_name = '\\' + room_name
                    else:
                        search_room_name = room_name
                    if room_name in temp_room:
                        room_name_id = temp_room[room_name]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('zone_name').like(search_room_name)) & (C('upper_hotel_zone_id') == floor_name_id) & (C('organization_id') == organization_id) & (C('hotel_zone_type_id') == room_type_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, hotelZoneTable)

                        if len(res) == 0:
                            room_name_id = str(uuid.uuid1())
                            # 新增一个房的信息
                            if floor_name[len(floor_name) - 1] != '楼':
                                floor_remark = floor_name + '楼-' + room_name
                            else:
                                floor_remark = floor_name + '-' + room_name
                            newFloor = {
                                'id': room_name_id,
                                'zone_name': numberChange[self.clearZFC(item['floor_name'])] + '楼-' + room_name,
                                'zone_number': numberChange[self.clearZFC(item['floor_name'])] + '楼-' + room_name,
                                'hotel_zone_type_id': room_type_id,
                                'hotel_zone_type_name': '房',
                                'remark': floor_remark,
                                'upper_hotel_zone_id': floor_name_id,
                                'organization_id': organization_id,
                            }
                            self.bill_manage_service.add_bill(OperationType.add.value,
                                                              TypeId.hotelZone.value, newFloor, hotelZoneTable)
                        else:
                            room_name_id = res[0]['id']
                        # 缓存有问题，禁止缓存
                        # temp_room[room_name] = room_name_id

                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    residents_id = self._getUserId(id_card, temp_user)
                    temp_user[id_card] = residents_id
                else:
                    residents_id = ''
                    temp_user[id_card] = ''

                # 从区域到楼到层到房间都录好了，就去判断床位是否已经录入
                if 'bed_name' in item and item['bed_name'] != None:
                    bed_name = self.clearZFC(item['bed_name'])
                    # 根据房号和名字判断
                    keywords = search_room_name + '房-' + bed_name
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('name').like(keywords)) & (C('area_id') == room_name_id))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, bedTable)

                    if len(res) > 0:
                        # 更新
                        newData = self._setValues(keys, item, res[0])

                        # 导入标识
                        newData = self.setImportDate(newData, '20200116')
                        newData['residents_id'] = residents_id
                        newData['hotel_zone_type_name'] = '房'

                        # 处理时间
                        if 'checkin_date' in item and item['checkin_date'] != None:
                            newData['checkin_date'] = self.strtodate(
                                self.clearZFC(item['checkin_date']))
                        if 'create_date' in item and item['create_date'] != None:
                            newData['create_date'] = self.strtodate(
                                self.clearZFC(item['create_date']))
                        if 'end_date' in item and item['end_date'] != None:
                            newData['end_date'] = self.strtodate(
                                self.clearZFC(item['end_date']))

                        # 更新的
                        def process_func(db):
                            update_data(db, bedTable, newData, {
                                'id': newData['id'],
                                'bill_status': 'valid'
                            })
                        process_db("127.0.0.1", 27017, 'IEC',
                                   process_func, 'admin', '123456')
                    else:
                        # 新增
                        newData = {}
                        # 补充原数据
                        newData = self._setValues(keys, item, newData)

                        # 导入标识
                        newData = self.setImportDate(newData, '20200116')

                        room_name = self.clearZFC(item['room_name'])
                        # 新增一个楼层的信息
                        if room_name[len(room_name) - 1] != '房':
                            bedName = room_name + '房-' + bed_name + '床'
                        else:
                            bedName = room_name + '-' + bed_name + '床'

                        # 处理时间
                        if 'checkin_date' in item and item['checkin_date'] != None:
                            newData['checkin_date'] = self.strtodate(
                                self.clearZFC(item['checkin_date']))
                        if 'create_date' in item and item['create_date'] != None:
                            newData['create_date'] = self.strtodate(
                                self.clearZFC(item['create_date']))
                        if 'end_date' in item and item['end_date'] != None:
                            newData['end_date'] = self.strtodate(
                                self.clearZFC(item['end_date']))

                        # 补充数据
                        newData['area_id'] = room_name_id
                        newData['name'] = bedName
                        newData['bed_code'] = bedName
                        newData['remark'] = self.clearZFC(
                            item['building_name']) + '-' + bedName
                        newData['service_item_package_id'] = ''
                        newData['residents_id'] = residents_id
                        newData['organization_id'] = organization_id
                        newData['hotel_zone_type_name'] = '房'

                        newData = get_info(newData, self.session)

                        newData['GUID'] = str(uuid.uuid1())
                        newData['version'] = 1
                        newData['bill_status'] = 'valid'
                        newData['valid_bill_id'] = str(uuid.uuid1())
                        newData['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                        arr.append(newData)

                        # self.bill_manage_service.add_bill(OperationType.add.value,
                        #                             TypeId.bed.value, newData, bedTable)

        def process_func2(db):
            insert_many_data(db, bedTable, arr)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func2, self.db_user, self.db_pwd)

    # 导入服务记录（工单）
    def import_excel_service_record(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'所属订单编号': 'order_code', '预约时间': 'yuyue_date', '派单时间': 'assign_date', '工单号码': 'record_code', '工单状态': 'record_status', '工单金额': 'amout', '工单时间': 'create_date', '服务开始时间': 'start_date', '服务完成时间': 'end_date', '回访状态': 'visit_status', '回访时间': 'visit_date',
                           '回访服务满意度': 'service_satisfaction', '服务机构名字': 'organization_name', '服务人员身份证': 'servicer_id_card', '长者身份证': 'elder_id_card'}, inplace=True)
        condition = dataframe_to_list(pf)

        serviceOrderTable = 'ZPT_Service_Order_1119'
        serviceRecordTable = 'ZPT_Service_Record_1119'
        taskTable = 'ZPT_Task_1119'

        keys = ['record_code', 'record_status',
                'visit_status', 'service_satisfaction']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}
        temp_worker = {}
        temp_elder = {}
        temp_order = {}

        arr = []
        arrTask = []

        count = 0

        order_arr = {}
        order_arr2 = {}

        print('服务工单共'+str(len(condition))+'个')

        if len(condition) > 0:
            for item in condition:

                count = count + 1

                newServiceRecord = {}

        # for itm in all_arr:
        #     _filter = MongoBillFilter()
        #     _filter.match_bill((C('order_primary_id') == itm))\
        #         .project({'_id': 0})
        #     res = self.query(
        #         _filter, serviceOrderTable)

        #     for im in res:
        #         allTimes = 0
        #         for i in im['detail']:
        #             allTimes += i['count'] * i['all_times']
        #         if allTimes != all_arr[itm]:
        #           #print(im['order_primary_id'], allTimes, all_arr[itm])

                # 补充原数据
                newServiceRecord = self._setValues(
                    keys, item, newServiceRecord)

                # 根据订单号找回订单数据
                order_code = self.clearZFC(item['order_code'])
                if order_code in temp_order:
                    res = temp_order[order_code]
                else:
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('order_code') == order_code))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, serviceOrderTable)
                    temp_order[order_code] = res

                if len(res) == 0:
                    continue

                if order_code not in order_arr:
                    orderTemp = []
                    for itm in res[0]['detail']:
                        nmax = int(itm['count']) * int(itm['all_times'])
                        for x in range(0, nmax):
                            orderTemp.append(itm['service_option'])
                    order_arr[order_code] = orderTemp

                service_option_index = 0

                if order_code not in order_arr2:
                    order_arr2[order_code] = 0
                else:
                    service_option_index = order_arr2[order_code] + 1
                    order_arr2[order_code] = service_option_index

                # 订单ID
                newServiceRecord['order_id'] = res[0]['id']
                # 所属机构
                newServiceRecord['organization_id'] = res[0]['organization_id']
                # 产品ID
                newServiceRecord['service_product_id'] = res[0]['origin_product']['product_id']
                # 是否虚拟
                # newServiceRecord['is_virtual'] = '1'
                # 服务选项
                newServiceRecord['service_option'] = order_arr[order_code][service_option_index]
                # for itm in res[0]['detail']:
                #     newServiceRecord['service_option'].append(itm['service_option'])
                # 计价金额
                if 'amout' in res[0]:
                    newServiceRecord['valuation_amount'] = float(
                        res[0]['amout'])

                # 开始时间
                if 'start_date' in item and item['start_date'] != None:
                    newServiceRecord['start_date'] = self.strtodate(
                        self.clearZFC(item['start_date']))

                # 没有开始时间就用工单时间代替
                if 'start_date' not in newServiceRecord:
                    newServiceRecord['start_date'] = self.strtodate(
                        self.clearZFC(item['create_date']))

                # 结束时间
                if 'end_date' in item and item['end_date'] != None:
                    newServiceRecord['end_date'] = self.strtodate(
                        self.clearZFC(item['end_date']))

                # 回访时间
                if 'visit_date' in item and item['visit_date'] != None:
                    newServiceRecord['visit_date'] = self.strtodate(
                        self.clearZFC(item['visit_date']))

                # 工单时间/创建时间
                if 'create_date' in item and item['create_date'] != None:
                    newServiceRecord['create_date'] = self.strtodate(
                        self.clearZFC(item['create_date']))

                # 把工单状态转换为状态
                if 'record_status' in newServiceRecord:
                    newServiceRecord['status'] = newServiceRecord['record_status']
                    del(newServiceRecord['record_status'])

                # 服务人员
                if 'servicer_id_card' in item and item['servicer_id_card'] != None:
                    servicer_id_card = self.clearZFC(
                        item['servicer_id_card'])
                    servicer_id = self._getUserId(
                        servicer_id_card, temp_worker)
                    if servicer_id != '':
                        newServiceRecord['servicer_id'] = servicer_id
                    temp_worker[servicer_id_card] = servicer_id

                # 购买者
                if 'elder_id_card' in item and item['elder_id_card'] != None:
                    elder_id_card = self.clearZFC(item['elder_id_card'])
                    elder_id = self._getUserId(elder_id_card, temp_elder)
                    if elder_id != '':
                        newServiceRecord['elder_id'] = elder_id
                    temp_elder[elder_id_card] = elder_id

                # 导入标识
                newServiceRecord = self.setImportDate(
                    newServiceRecord, '20201119')

                newServiceRecord['id'] = str(uuid.uuid1())
                newServiceRecord['modify_date'] = newServiceRecord['create_date']
                newServiceRecord['GUID'] = str(uuid.uuid1())
                newServiceRecord['version'] = 1
                newServiceRecord['bill_status'] = 'valid'
                newServiceRecord['valid_bill_id'] = str(uuid.uuid1())
                newServiceRecord['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                # 新任务
                newTask = {}

                # 任务信息
                newTask['task_name'] = res[0]['origin_product']['product_name']
                if 'remark' in res[0]:
                    newTask['task_content'] = res[0]['remark']
                if 'servicer_id' in newServiceRecord:
                    newTask['task_receive_person'] = newServiceRecord['servicer_id']
                if 'service_address' in newServiceRecord:
                    newTask['task_address'] = newServiceRecord['service_address']
                    newTask['task_location'] = newServiceRecord['service_address']
                    del(newServiceRecord['service_address'])
                if 'organization_id' in newServiceRecord:
                    newTask['task_creator'] = newServiceRecord['organization_id']
                    newTask['organization_id'] = newServiceRecord['organization_id']
                if 'elder_id' in newServiceRecord:
                    newTask['purchaser_id'] = newServiceRecord['elder_id']
                if 'start_date' in newServiceRecord:
                    newTask['start_date'] = newServiceRecord['start_date']
                if 'end_date' in newServiceRecord:
                    newTask['end_date'] = newServiceRecord['end_date']

                newTask['task_object_id'] = newServiceRecord['id']

                if 'status' in newServiceRecord:
                    newTask['task_state'] = newServiceRecord['status']

                newTask['task_type'] = '0b4b50d2-1d49-11ea-b9cb-a0a4c57e9ebe'
                newTask['task_urgent'] = '一般'

                # 导入标识
                newTask = self.setImportDate(
                    newTask, '20201119')

                # 任务的创建时间等于工单时间
                if 'start_date' in newTask:
                    newTask['create_date'] = newTask['start_date']
                    newTask['modify_date'] = newTask['start_date']

                newTask['id'] = str(uuid.uuid1())
                newTask['GUID'] = str(uuid.uuid1())
                newTask['version'] = 1
                newTask['bill_status'] = 'valid'
                newTask['valid_bill_id'] = str(uuid.uuid1())
                newTask['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                print('服务工单第'+str(count)+'个')

                arr.append(newServiceRecord)
                arrTask.append(newTask)

                # 有分派日期，就把订单状态改为已分派
                if 'assign_date' in item and item['assign_date'] != None:
                    assign_date = self.strtodate(
                        self.clearZFC(item['assign_date']))

                    def process_func_update(db):
                        update_data(db, serviceOrderTable, {'assign_state': '已分派', 'assign_date': assign_date}, {
                            'id': res[0]['id'],
                        })
                    process_db(self.db_addr, self.db_port, self.db_name,
                               process_func_update, self.db_user, self.db_pwd)

        def process_func(db):
            insert_many_data(db, serviceRecordTable, arr)
            insert_many_data(db, taskTable, arrTask)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

    # 导入活动义工
    def import_excel_volunteer(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构编号': 'organization_code', '活动名称': 'activity_name', '义工名字': 'name',
                           '身份证号': 'id_card', '性别': 'sex', '手机': 'telephone', '生日': 'date_birth'}, inplace=True)
        condition = dataframe_to_list(pf)

        activityVolunteerTable = 'ZPT_Activity_Volunteer'
        activityTable = 'ZPT_Activity'

        keys = ['name', 'id_card', 'telephone']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}
        temp_activity = {}
        temp_user = {}

        arr = []

        count = 0

        if len(condition) > 0:
            for item in condition:

                count = count + 1

                newData = {}
                # 补充原数据
                newData = self._setValues(keys, item, newData)

                # 所属机构
                if 'organization_code' in item and item['organization_code'] != None:
                    organization_code = self.clearZFC(
                        item['organization_code'])
                    organization_id = self._getOrganizationId(
                        organization_code, temp_organization, default_organization_id)
                    newData['organization_id'] = organization_id
                    temp_organization[organization_code] = organization_id
                else:
                    newData['organization_id'] = default_organization_id

                # 根据活动名字找到id
                if 'activity_name' in item and item['activity_name'] != None:
                    activity_name = self.clearZFC(item['activity_name'])

                    _filter = MongoBillFilter()
                    _filter.match_bill((C('activity_name') == activity_name))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, activityTable)

                    if len(res) > 0:
                        newData['activity_id'] = res[0]['id']
                        temp_activity[activity_name] = res[0]['id']
                    else:
                        newData['activity_id'] = activity_name
                        temp_activity[activity_name] = activity_name

                # 根据所属机构和人员名字找到人员id
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    user_id = self._getUserId(id_card, temp_user)
                    if user_id != '':
                        newData['user_id'] = user_id
                    temp_user[id_card] = user_id

                # 人员生日
                if 'date_birth' in item and item['date_birth'] != None:
                    newData['date_birth'] = self.strtodate(
                        self.clearZFC(item['date_birth']))

                # 人员性别
                if 'sex' in item and item['sex'] != None:
                    sex = self.clearZFC(item['sex'])
                    if sex == 'Male':
                        newData['sex'] = '男'
                    elif sex == 'Female':
                        newData['sex'] = '女'

                # 导入标识
                newData = self.setImportDate(
                    newData, '20200113')

                data = get_info(newData, self.session)

                data['GUID'] = str(uuid.uuid1())
                data['version'] = 1
                data['bill_status'] = 'valid'
                data['valid_bill_id'] = str(uuid.uuid1())
                data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('活动义工第'+str(count)+'个')

                arr.append(data)

        def process_func(db):
            insert_many_data(db, activityVolunteerTable, arr)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

    # 导入活动签到

    def import_excel_signin(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'机构编号': 'organization_code', '活动名称': 'activity_name', '活动报名': 'activity_signin', '活动唯一主键': 'activity_primary_id', '参加状态': 'signin_status', '签到模式': 'signin_mode', '报名状态': 'enroll_status', '报名方式': 'enroll_type', '报名时间': 'create_date',
                           '参加人员姓名': 'name', '性别': 'sex', '身份证': 'id_card', '生日': 'date_birth', '活动报名人数': 'enroll_count'}, inplace=True)
        condition = dataframe_to_list(pf)

        activitySignInTable = 'ZPT_Activity_Sign_In_0316'
        activityTable = 'ZPT_Activity_0316'

        keys = ['activity_signin', 'signin_status', 'signin_mode',
                'enroll_status', 'enroll_type', 'enroll_count', 'id_card', 'name']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}
        temp_activity = {}
        temp_user = {}

        arr = []

        count = 0

        if len(condition) > 0:
            for item in condition:

                count = count + 1

                newData = {}

                # 补充原数据
                newData = self._setValues(keys, item, newData)

                # 所属机构
                if 'organization_code' in item and item['organization_code'] != None:
                    organization_code = self.clearZFC(
                        item['organization_code'])
                    organization_id = self._getOrganizationId(
                        organization_code, temp_organization, default_organization_id)
                    newData['organization_id'] = organization_id
                    temp_organization[organization_code] = organization_id
                else:
                    newData['organization_id'] = default_organization_id

                # 根据活动主键找到id
                if 'activity_primary_id' in item and item['activity_primary_id'] != None:
                    activity_primary_id = self.clearZFC(
                        item['activity_primary_id'])

                    if activity_primary_id in temp_activity:
                        newData['activity_id'] = temp_activity[activity_primary_id]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('primary_id') == activity_primary_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, activityTable)

                        if len(res) > 0:
                            newData['activity_id'] = res[0]['id']
                            temp_activity[activity_primary_id] = newData['activity_id']

                # 根据所属机构和人员名字找到人员id
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    if id_card in temp_user:
                        newData['user_id'] = temp_user[id_card]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('id_card') == id_card) | (C('personnel_info.id_card') == id_card))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, self.PT_User)

                        if len(res) > 0:
                            newData['user_id'] = res[0]['id']
                            temp_user[id_card] = newData['user_id']

                # 签到日期
                if 'enroll_date' in item and item['enroll_date'] != None:
                    newData['date'] = self.strtodate(
                        self.clearZFC(item['enroll_date']))

                # 签到人员生日
                if 'date_birth' in item and item['date_birth'] != None:
                    newData['date_birth'] = self.strtodate(
                        self.clearZFC(item['date_birth']))

                # 签到人员性别
                if 'sex' in item and item['sex'] != None:
                    sex = self.clearZFC(item['sex'])
                    if sex == 'Male':
                        newData['sex'] = '男'
                    elif sex == 'Female':
                        newData['sex'] = '女'

                # 导入标识
                newData = self.setImportDate(
                    newData, '20200317')

                data = get_info(newData, self.session)

                data['GUID'] = str(uuid.uuid1())
                data['modify_date'] = get_cur_time()
                data['version'] = 1
                data['bill_status'] = 'valid'
                data['valid_bill_id'] = str(uuid.uuid1())
                data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('活动签到第'+str(count)+'个')

                # arr.append(data)

                def process_func(db):
                    insert_data(db, activitySignInTable, data)
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

    # 导入活动室
    def import_excel_activity_room(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'活动室名称': 'activity_room_name', '机构编号': 'organization_code', '活动室容纳人数': 'max_quantity', '活动室功能': 'remarks', '活动室开放时间': 'begin_date', '活动室关闭时间': 'end_date', '所在楼层': 'floor', '房间编号': 'room_code',
                           '大楼名称': 'address', '活动室唯一主键': 'primary_id'}, inplace=True)
        condition = dataframe_to_list(pf)

        activityRoomTable = 'ZPT_Activity_Room_0316'

        keys = ['activity_room_name', 'max_quantity', 'remarks',
                'floor', 'room_code', 'contacts', 'phone', 'address', 'primary_id']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_organization = {}

        arr = []

        count = 0

        if len(condition) > 0:

            for item in condition:
                count = count + 1
                # 新增的活动室数据
                newActivityRoom = {}

                # 补充原数据
                newActivityRoom = self._setValues(keys, item, newActivityRoom)

                # 开始时间
                if 'begin_date' in item and item['begin_date'] != None:
                    begin_date = self.clearZFC(item['begin_date'])
                    newActivityRoom['begin_date'] = self._setTimeFormat(
                        begin_date)
                # 结束时间
                if 'end_date' in item and item['end_date'] != None:
                    end_date = self.clearZFC(item['end_date'])
                    newActivityRoom['end_date'] = self._setTimeFormat(end_date)
                # 所属机构
                if 'organization_code' in item and item['organization_code'] != None:
                    organization_code = self.clearZFC(
                        item['organization_code'])
                    organization_id = self._getOrganizationId(
                        organization_code, temp_organization, default_organization_id)
                    newActivityRoom['organization_id'] = organization_id
                    temp_organization[organization_code] = organization_id
                else:
                    newActivityRoom['organization_id'] = default_organization_id

                if 'max_quantity' in newActivityRoom:
                    if type(newActivityRoom['max_quantity']) == str:
                        newActivityRoom['max_quantity'] = int(
                            newActivityRoom['max_quantity'])

                # 导入标识
                newActivityRoom = self.setImportDate(
                    newActivityRoom, '20200316')

                newActivityRoom['id'] = str(uuid.uuid1())
                newActivityRoom['create_date'] = get_cur_time()
                newActivityRoom['modify_date'] = get_cur_time()
                newActivityRoom['GUID'] = str(uuid.uuid1())
                newActivityRoom['version'] = 1
                newActivityRoom['bill_status'] = 'valid'
                newActivityRoom['valid_bill_id'] = str(uuid.uuid1())
                newActivityRoom['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('活动室第'+str(count)+'个')

                # arr.append(newActivityRoom)

                def process_func(db):
                    insert_data(db, activityRoomTable, newActivityRoom)
                    # insert_many_data(db, activityRoomTable, arr)
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

    # 导入活动
    def import_excel_activity(self, data):

        pf = pd.DataFrame(data)
        pf.rename(columns={'机构名称': 'organization_name', '活动名称': 'activity_name', '活动图片': 'photo', '最大参与人数': 'max_quantity', '活动发布时间': 'create_date', '活动开始时间': 'begin_date',
                           '活动结束时间': 'end_date', '活动地点': 'address', '活动场室': 'activity_room_id', '联系人': 'contacts', '联系人电话': 'phone', '活动金额': 'amount', '活动介绍': 'introduce', '活动类型': 'activity_type_id', '活动报告': 'activity_report', '活动报告图片': 'activity_report_picture'}, inplace=True)
        condition = dataframe_to_list(pf)

        activityTypeTable = 'ZPT_Activity_Type'
        activityRoomTable = 'ZPT_Activity_Room_0316'
        activityTable = 'ZPT_Activity_0610'

        keys = ['activity_name', 'max_quantity', 'address',
                'contacts', 'phone', 'amount', 'introduce', 'activity_report']

        # 默认平台的组织ID
        default_organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'

        temp_type_id = {}
        temp_room_id = {}
        temp_worker = {}
        temp_organization = {}

        arr = []

        count = 0

        if len(condition) > 0:

            for item in condition:
                count = count + 1
                # 新增的活动数据
                newActivity = {}

                # 补充原数据
                newActivity = self._setValues(keys, item, newActivity)

                # 开始时间
                if 'begin_date' in item and item['begin_date'] != None:
                    begin_date = self.clearZFC(item['begin_date'])
                    newActivity['begin_date'] = self._setTimeFormat(begin_date)
                # 结束时间
                if 'end_date' in item and item['end_date'] != None:
                    end_date = self.clearZFC(item['end_date'])
                    newActivity['end_date'] = self._setTimeFormat(end_date)

                # 找出所属机构
                if 'organization_name' in item and item['organization_name'] != None:
                    organization_name = self.clearZFC(
                        item['organization_name'])

                    organization_id = self._getOrganizationId(
                        organization_name, temp_organization, default_organization_id, 'name')
                    newActivity['organization_id'] = organization_id
                    temp_organization[organization_name] = organization_id
                else:
                    newActivity['organization_id'] = default_organization_id
                    temp_organization[organization_name] = default_organization_id

                # 找出发布人
                if 'contacts' in item and item['contacts'] != None:
                    contacts = self.clearZFC(item['contacts'])

                    _filter = MongoBillFilter()
                    _filter.match_bill((C('organization_id') == newActivity['organization_id']) & (C('personnel_info.personnel_category') == '工作人员') & (C('name') == contacts))\
                        .project({'_id': 0})
                    res = self.query(
                        _filter, self.PT_User)

                    if len(res) > 0:
                        newActivity['apply_user_id'] = res[0]['id']

                # 照片
                if 'photo' in item and item['photo'] != None:
                    photo = self.get_url_upload_url(
                        self.clearZFC(item['photo']), 'activityPhoto_0610')
                    if photo != '':
                        newActivity['photo'] = [photo]
                else:
                    newActivity['photo'] = []

                # 找出对应的活动类型
                if 'activity_type_id' in item and item['activity_type_id'] != None:
                    activity_type_id = self.clearZFC(item['activity_type_id'])
                    if activity_type_id in temp_type_id:
                        newActivity['activity_type_id'] = temp_type_id[activity_type_id]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('keys') == activity_type_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, activityTypeTable)

                        if len(res) > 0:
                            newActivity['activity_type_id'] = res[0]['id']
                            temp_type_id[activity_type_id] = res[0]['id']
                        else:
                            newActivity['activity_type_id'] = activity_type_id
                            temp_type_id[activity_type_id] = activity_type_id

                # 找出对应的活动室
                if 'activity_room_id' in item and item['activity_room_id'] != None:
                    activity_room_id = self.clearZFC(item['activity_room_id'])
                    if activity_room_id in temp_room_id:
                        newActivity['activity_room_id'] = temp_room_id[activity_room_id]
                    else:
                        _filter = MongoBillFilter()
                        _filter.match_bill((C('organization_id') == newActivity['organization_id']) & (C('activity_room_name') == activity_room_id))\
                            .project({'_id': 0})
                        res = self.query(
                            _filter, activityRoomTable)

                        if len(res) > 0:
                            newActivity['activity_room_id'] = res[0]['id']
                            temp_room_id[activity_room_id] = res[0]['id']
                        else:
                            newActivity['activity_room_id'] = activity_room_id
                            temp_room_id[activity_room_id] = activity_room_id

                # 默认通过
                newActivity['status'] = '通过'
                newActivity['step_no'] = -1

                if 'max_quantity' in newActivity:
                    if type(newActivity['max_quantity']) == str:
                        newActivity['max_quantity'] = int(
                            newActivity['max_quantity'])

                # 转换
                if 'amount' not in newActivity:
                    newActivity['amount'] = 0
                if 'amount' in newActivity and type(newActivity['amount']) == str:
                    newActivity['amount'] = int(newActivity['amount'])

                # 导入标识
                newActivity = self.setImportDate(newActivity, '20200610')

                # 创建时间
                if 'create_date' in item and item['create_date'] != None:
                    create_date = self.clearZFC(item['create_date'])
                    newActivity['create_date'] = self._setTimeFormat(
                        create_date)
                    # 作为审核时间
                    newActivity['audit_date'] = newActivity['create_date']

                newActivity['id'] = str(uuid.uuid1())
                newActivity['GUID'] = str(uuid.uuid1())
                newActivity['modify_date'] = get_cur_time()
                newActivity['version'] = 1
                newActivity['bill_status'] = 'valid'
                newActivity['valid_bill_id'] = str(uuid.uuid1())
                newActivity['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

              # print('活动列表第'+str(count)+'个')

                # arr.append(data)

                def process_func(db):
                    insert_data(db, activityTable, newActivity)
                    # insert_many_data(db, activityTable, arr)
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

      # print('活动列表OVER')
        return 'FAILED'

    def import_excel_personnel55(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'姓名': 'xm', '性别': 'xb', '出生日期': 'csrq', '身份证号码': 'sfzhm', '所属机构': 'ssjg', '联系电话': 'lxdh',
                           'jg': '籍贯', '家庭地址': 'jtdz', '身份证地址': 'sfzdz', '行政区划编号': 'xzqybh', '行政区域': 'xzqy', '照片': 'zp', '学历资质': 'xlzz', '岗位': 'gw', '角色': 'js', '护理区域': 'hlqy', '人员类别': 'rylb'}, inplace=True)
        condition = dataframe_to_list(pf)

        tempJG = {}
        tempJGD = []

        if len(condition) > 0:
            for item in condition:
                # ssjg = str(item['ssjg'])
                ssjg = str(item['ssjg']).replace(
                    '\r', '').replace('\n', '').replace('\t', '')
                _filter_ssjg = MongoBillFilter()
                _filter_ssjg.match_bill((C('name') == ssjg))\
                    .project({'_id': 0})
                ssjg_res = self.query(
                    _filter_ssjg, self.PT_User)

                if len(ssjg_res) == 0:
                    if ssjg not in tempJG:
                        tempJGD.append(ssjg)
                        tempJG[ssjg] = ssjg

      # print(tempJGD)

    def import_excel_personnel4(self, data):

        pf = pd.DataFrame(data)
        pf.rename(columns={'名称': 'mc', '英文名称': 'ywmc', '编号': 'bh', '简称': 'jc', '首府': 'sf', '经度': 'jd',
                           '纬度': 'wd', '行政区域级别': 'xzqyjb', '上级名称': 'sjmc', '上级行政区域（编号）': 'sjxzqybh'}, inplace=True)
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)
        condition = dataframe_to_list(pf)

        tempBH = {}

        # return condition

        if len(condition) > 0:

            count = 0

            for item in condition:
                # 导入行政区域
                newData = {
                    "name": "",
                    "english_name": "",
                    "code": "",
                    "short_name": "",
                    "capital": '',
                    "location": {
                        "lon": "",
                        "lat": ""
                    },
                    "division_level": "",
                    "parent_id": "",
                }
                if 'mc' in item and item['mc'] != None:
                    newData['name'] = item['mc']
                if 'ywmc' in item and item['ywmc'] != None:
                    newData['english_name'] = item['ywmc']
                if 'bh' in item and item['bh'] != None:
                    newData['code'] = item['bh']
                if 'jc' in item and item['jc'] != None:
                    newData['short_name'] = item['jc']
                if 'sf' in item and item['sf'] != None:
                    newData['capital'] = item['sf']
                if 'jd' in item and item['jd'] != None:
                    newData['location']['lon'] = item['jd']
                if 'wd' in item and item['wd'] != None:
                    newData['location']['lat'] = item['wd']
                if 'xzqyjb' in item and item['xzqyjb'] != None:
                    newData['division_level'] = item['xzqyjb']
                if 'sjxzqybh' in item and item['sjxzqybh'] != None:
                    sjxzqybh = str(item['sjxzqybh'])
                    if sjxzqybh in tempBH:
                      # print('已有上级区域，无需重复获取')
                        newData['parent_id'] = tempBH[sjxzqybh]
                    else:
                        _filter_bianhao = MongoBillFilter()
                        _filter_bianhao.match_bill((C('code') == sjxzqybh))\
                            .project({'_id': 0})
                        bh_info = self.query(
                            _filter_bianhao, 'PT_Administration_Division56')

                        if len(bh_info) > 0:
                            tempBH[sjxzqybh] = bh_info[0]['id']
                            newData['parent_id'] = bh_info[0]['id']

                # return newData

                data = get_info(newData, self.session)

                count = count + 1
              # print('开始导入第' + str((count)) + '个')
                try:
                    self.bill_manage_service.add_bill(OperationType.add.value,
                                                      TypeId.administrationDivision.value, data, 'PT_Administration_Division56')
                except Exception as e:
                    pass
                  # print(e)
        return '导入成功'

    def get_url_upload_url(self, url_path, path_name='importProduct'):
        ''' 通过url导入图片 '''
        try:
            request = urllib.request.Request(url_path)
            response = urllib.request.urlopen(request)
        except Exception as e:
            return ''
        get_img = response.read()
        import string
        import random
        #####保存图片#########
        file_name = str(int(time.time())) + "_" + str(
            "".join(random.choice(string.ascii_letters + string.digits) for _ in range(8)))
        upload_path = "{0}\\{1}".format('upload', path_name)  # 以xfxz命名文件
        file_path = os.path.join('build', upload_path).replace('\\', '/')
        if not os.path.exists(file_path):
            # 不存在改目录则会自动创建
            os.makedirs(file_path)
        save_path = os.path.join(file_path, file_name+'.jpg').replace(
            '\\', '/')
        #  windows下路径要转化
        with open(save_path, 'wb') as fp:
            fp.write(get_img)
        return '/'+save_path

    # 从字符串中提取数字
    def _getNumberFromString(self, string):
        result = re.findall(r"\d+\.?\d*", string)
        if len(result) > 0:
            return result[0]
        else:
            return string

    # 设置导入标识

    def setImportDate(self, data, no):
        # 导入标识
        if 'import_date' in data:
            if type(data['import_date']) == str:
                # 新标识
                if data['import_date'] != no:
                    newArr = []
                    newArr.append(data['import_date'])
                    newArr.append(no)
                else:
                    newArr = []
                    newArr.append(no)
                data['import_date'] = newArr
                return data
            elif type(data['import_date']) == list:
                if no in data['import_date']:
                    return data
                else:
                    data['import_date'].append(no)
                return data
        else:
            data['import_date'] = []
            data['import_date'].append(no)
        return data

    def createPermission(self, permission, array, uid, uname):

        for item in array:

            newPermission = {
                'role_id': uid,
                'function': permission,
                'permission': item,
                'remarks': uname + '_' + item + permission,
                'is_allow_super_use': True,
            }

            data_info = get_info(newPermission, self.session)

            self.bill_manage_service.add_bill(OperationType.add.value,
                                              TypeId.permission.value, data_info, 'PT_Permission')

    def import_nhfl_excel_elder(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'长者姓名': 'name', '证件类型': 'id_card_type', '证件号码': 'id_card', '性别': 'sex', '出生日期': 'date_birth',
                           '电话': 'telephone', '在世状态': 'die_state', '兴趣爱好': 'hobby', '婚姻状态': 'marriage_state', '民族': 'nation', '组织机构': 'organization_name', '籍贯': 'native_place', '区域编号': 'organization_code', '政治面貌': 'political_outlook', '家庭住址': 'address', '身份证地址': 'id_card_address', '行政区划': "admin_area_id", '备注': 'remark', '照片': 'photo', '学历': 'education', '银行卡号': 'card_number', '养老类型': 'pension_category', '长者类别': 'elders_category', '创建日期': 'create_date'}, inplace=True)
        condition = dataframe_to_list(pf)

        out_per = ['name', 'id_card']
        in_per = ['name', 'sex', 'id_card', 'telephone', 'native_place', 'address',
                  'id_card_address', 'education', 'post', 'nursing_area', 'card_number', 'die_state', 'hobby', 'marriage_state', 'nation', 'political_outlook', 'pension_category', 'elders_category', 'remark']
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)
        condition = dataframe_to_list(pf)

        # 组织机构的缓存数组
        temp_organization = {}
        # 区域编号的缓存数组
        temp_area = {}
        # 长者类型的缓存数组
        tempLX = {}
        temp_elder = {}

        person_arr = []
        role_arr = []
        account_arr = []

        # 默认佛山市区域ID
        default_area_id = 'f082436e-262c-11ea-a9d6-a0a4c57e9ebe'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认平台的组织ID
        default_organization_id = '0b96b62e-e363-11e9-93c4-a0a4c57e9ebe'
        # print('查询开始》》')
        # _filter_elder = MongoBillFilter()
        # _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
        #     .project({'_id': 0})
        # res_elder = self.query(
        #     _filter_elder, self.PT_User)
        # print('人员数据》》》', len(res_elder))
        # if len(res_elder) > 0:
        #     for item in res_elder:
        #         if 'id_card' in item:
        #             temp_elder[item['id_card']] = item
        # ccc = 0
        # if len(condition) > 0:
        #     for item in condition:
        #         # 判断这个人是否存在
        #         if 'id_card' in item and item['id_card'] != None:
        #             id_card = self.clearZFC(item['id_card'])
        #             if id_card in temp_elder:
        #                 ccc = ccc + 1
        # if ccc > 0:
        #   #print(ccc)
        # return '有重复的长者身份证'
        # return '有重复的长者身份证【' + id_card + '】'
        cun_zai_elder = []
        yd_elder = []
        bcun_tong_name = []
        if len(condition) > 0:
            count = 0

            for item in condition:
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card'])
                    elder_name = self.clearZFC(item['name'])
                    _filter_elder = MongoBillFilter()
                    _filter_elder.match_bill((C('id_card') == id_card) & (C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
                        .project({'_id': 0})
                    res_elder = self.query(
                        _filter_elder, self.PT_User)
                    # if id_card in temp_elder:
                    if len(res_elder) > 0:
                        cun_zai_elder.append(res_elder[0]['id'])
                        if res_elder[0]['organization_id'] != '0b96b62e-e363-11e9-93c4-a0a4c57e9ebe':
                            yd_elder.append(res_elder[0]['id'])
                        # count = count + 1
                        # print('长者表第'+str(count)+'个')
                        continue
                    else:
                        _filter_elder = MongoBillFilter()
                        _filter_elder.match_bill((C('name') == elder_name) & (C('organization_id') == '0b96b62e-e363-11e9-93c4-a0a4c57e9ebe') & (C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
                            .project({'_id': 0})
                        res_elder = self.query(
                            _filter_elder, self.PT_User)
                        if len(res_elder) > 0:
                            bcun_tong_name.append(res_elder[0]['id'])
                # 导入人员
                newData = {
                    "name": "",
                    "personnel_type": "1",
                    "admin_area_id": default_area_id,
                    "town": "",
                    "id_card_type": "身份证",
                    "id_card": "",
                    "organization_id": default_organization_id,
                    "reg_date": get_cur_time(),
                    "create_date": get_cur_time(),
                    "login_info": [],
                    "personnel_info": {
                        "name": "",
                        "id_card": "",
                        "id_card_address": "",
                        "date_birth": "",
                        "sex": "",
                        "telephone": "",
                        "marriage_state": "",
                        "remarks": "",
                        "native_place": "",
                        "personnel_category": "长者",
                        "address": "",
                        "role_id": default_role_id,
                        "die_state": "",
                        "hobby": "",
                        "personnel_classification": "",
                        "card_number": "",
                        "pension_category": "",
                        "elders_category": "",
                        "political_outlook": "",
                        "photo": [],
                    }
                }

                # 判断personnel_info里的数据是否一致
                newData = self._setValues(out_per, item, newData)
                # 判断personnel_info里的数据是否一致
                newData['personnel_info'] = self._setValues(
                    in_per, item, newData['personnel_info'])

                # # 找出所属机构
                # if 'organization_name' in item and item['organization_name'] != None:
                #     organization_name = self.clearZFC(
                #         item['organization_name'])

                #     organization_id = self._getOrganizationId(
                #         organization_name, temp_organization, default_organization_id, 'name')
                #     newData['organization_id'] = organization_id
                #     temp_organization[organization_name] = organization_id
                # else:
                #     newData['organization_id'] = default_organization_id
                #     temp_organization[organization_name] = default_organization_id

                # # 判断所属行政编号
                # if 'organization_code' in item and item['organization_code']:
                #     organization_code = self.clearZFC(
                #         item['organization_code'])
                #     newData['admin_area_id'] = self._getAdministratorArea2(
                #         organization_code, temp_area, default_area_id)
                #     temp_area[organization_code] = newData['admin_area_id']

                # 生日日期
                if 'date_birth' in item:
                    defaultValue = self._getDefaultValue(
                        'date_birth', newData['personnel_info'])
                    newData['personnel_info']['date_birth'] = self._setTimeFormat(
                        item['date_birth'], defaultValue)

                person_data = get_info(newData, self.session)

                person_data['GUID'] = str(uuid.uuid1())
                person_data['version'] = 1
                person_data['bill_status'] = 'valid'
                person_data['valid_bill_id'] = str(uuid.uuid1())
                person_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                person_data = self.setImportDate(
                    person_data, '20200721')

                # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）

                # 机构储蓄对象
                org_account_data = FinancialAccountObject(
                    AccountType.account_recharg_wel, person_data['id'], person_data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                org_account = get_info(
                    org_account_data.to_dict(), self.session)

                org_account['GUID'] = str(uuid.uuid1())
                org_account['version'] = 1
                org_account['bill_status'] = 'valid'
                org_account['valid_bill_id'] = str(uuid.uuid1())
                org_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                org_account = self.setImportDate(
                    org_account, '20200721')

                # app储蓄对象
                app_account_data = FinancialAccountObject(
                    AccountType.account_recharg_app, person_data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                app_account = get_info(
                    app_account_data.to_dict(), self.session)

                app_account['GUID'] = str(uuid.uuid1())
                app_account['version'] = 1
                app_account['bill_status'] = 'valid'
                app_account['valid_bill_id'] = str(uuid.uuid1())
                app_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                app_account = self.setImportDate(
                    app_account, '20200721')

                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, person_data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)

                real_account['GUID'] = str(uuid.uuid1())
                real_account['version'] = 1
                real_account['bill_status'] = 'valid'
                real_account['valid_bill_id'] = str(uuid.uuid1())
                real_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                real_account = self.setImportDate(
                    real_account, '20200721')

                # 补贴账户对象
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, person_data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)

                subsidy_account['GUID'] = str(uuid.uuid1())
                subsidy_account['version'] = 1
                subsidy_account['bill_status'] = 'valid'
                subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                subsidy_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                subsidy_account = self.setImportDate(
                    subsidy_account, '20200721')

                # 慈善账户
                charitable_account_data = FinancialAccountObject(
                    AccountType.account_charitable, person_data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
                charitable_account = get_info(
                    charitable_account_data.to_dict(), self.session)

                charitable_account['GUID'] = str(uuid.uuid1())
                charitable_account['version'] = 1
                charitable_account['bill_status'] = 'valid'
                charitable_account['valid_bill_id'] = str(uuid.uuid1())
                charitable_account['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                charitable_account = self.setImportDate(
                    charitable_account, '20200721')

                # 主数据
                person_arr.append(person_data)

                role_data = {'role_id': default_role_id,
                             'principal_account_id': person_data['id'], 'role_of_account_id': person_data['organization_id']}

                role_data = get_info(role_data, self.session)

                role_data['GUID'] = str(uuid.uuid1())
                role_data['version'] = 1
                role_data['bill_status'] = 'valid'
                role_data['valid_bill_id'] = str(uuid.uuid1())
                role_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'

                role_data = self.setImportDate(
                    role_data, '20200721')

                # role表的数组
                role_arr.append(role_data)

                # 账户表
                account_arr.append(org_account)
                account_arr.append(app_account)
                account_arr.append(real_account)
                account_arr.append(subsidy_account)
                account_arr.append(charitable_account)

                # count = count + 1
                # print('长者表第'+str(count)+'个')
        if len(yd_elder) > 0:
            pass
          # print('需要移平台的长者》》', yd_elder)
            # if len(cun_zai_elder) > 0:
            #   #print('已存在的长者结果》》', len(cun_zai_elder))
        if len(bcun_tong_name) > 0:
            pass
          # print('身份证不存在，但福利院存在同名长者》》', bcun_tong_name)
        if len(person_arr) > 0:
            def process_func(db):
                insert_many_data(db, 'PT_User_New', person_arr)
                insert_many_data(db, self.PT_Role, role_arr)
                insert_many_data(db, self.PT_Account, account_arr)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
          # print('服务长者OVER')

    def update_hdbm(self, data):

        _filter = MongoBillFilter()
        _filter.match_bill((C('GUID').exists(False))
                           ).project({'_id': 1})

        res = self.query(_filter, "PT_Activity_Participate")

        if len(res) > 0:
            for item in res:
                def process_func(db):
                    update_data(db, 'PT_Activity_Participate', {
                        'GUID': str(uuid.uuid1()),
                    }, {
                        '_id': item['_id'],
                    })
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

        return '完成了'

    def import_excel_assessment_record(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'身份证': 'id_card', 'itemType': 'itemType', 'itemID': 'itemID', 'Parent_Name': 'Parent_Name',
                           'itemNO': 'itemNO',  'itemName': 'itemName', 'checkFlag': 'checkFlag',
                           'specialCode': 'specialCode', 'itemText': 'itemText', 'itemTextValue': 'itemTextValue'}, inplace=True)
        condition = dataframe_to_list(pf)

        # out_per = ['name', 'id_card']
        # in_per = ['name', 'sex', 'id_card', 'telephone', 'native_place', 'address',
        #           'id_card_address', 'education', 'post', 'nursing_area', 'card_number', 'die_state', 'hobby', 'marriage_state', 'nation', 'political_outlook', 'pension_category', 'elders_category', 'remark']
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)

        # person_arr = []
      # print('开始查询长者')
        temp_elder = {}
        _filter_elder = MongoBillFilter()
        _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
            .project({'_id': 0})
        res_elder = self.query(
            _filter_elder, self.PT_User)

      # print('结束查询长者')
        if len(res_elder) > 0:
            for item in res_elder:
                if 'id_card' in item:
                    temp_elder[item['id_card']] = item

        # 临时处理对象{'user_id':{template_list}}
        temp_data = {}
        # 失败长者
        fail_elder = []
        assessment_record = self.assessment_record
        if len(condition) > 0:

            count = 0
            now_date = get_cur_time()
            for data in condition:
                count = count + 1
                itemType = data['itemType']

                if data['id_card'] in temp_elder:
                    # 对方主表的唯一id
                    assessrecId = data['assessrecId']
                    # 找user_id
                    user_id = temp_elder[data['id_card']]['id']
                    # 是否选择 1为选择
                    check_value = data['checkFlag']
                    parentName = data['Parent_Name']
                    itemName = data['itemName']
                    itemTextValue = data['itemTextValue']
                    # 判断长者该行是否已经开始或是否已经结束
                    if assessrecId in temp_data.keys():
                        template = temp_data[assessrecId].copy()
                    else:
                        # 新的长者评估记录的开始
                        template = assessment_record.copy()
                    template['elder'] = user_id
                    template['nursing_time'] = now_date
                    template['assessrecId'] = assessrecId
                    if itemType == '1' and itemName == '基本信息表' and itemTextValue != None:
                        # 处理A2模块
                        # 查找是哪个题目的选项
                        mz, wh, hy = self.mapping_A_value(itemTextValue)
                        template['template_list'][0][1]['ass_info']['value'] = mz
                        template['template_list'][0][2]['ass_info']['value'] = wh
                        template['template_list'][0][4]['ass_info']['value'] = hy
                        temp_data[assessrecId] = template
                    if itemType == '3':
                        # 处理B、C模块
                        if itemName in self.item_name:
                            itemName = self.mapping_item_name(itemName)
                        if parentName == '穿衣' and itemName == '需极大帮助或完全依赖他人':
                            itemName = '需要极大帮助或完全依赖他人'
                        if itemType == '3' and check_value == '1':
                            # print('评估表第'+str(count)+'个')
                            # 判断是哪个问题，插入到哪里数据
                            # 1、通过映射先知道是哪一块
                            index = self.get_model_mapping(parentName)
                            if index == -2:
                                continue
                            # 2、循环这一块的数组，匹配ass_info.project_name 是否等于 parentName
                            template_list = template['template_list'][index]
                            for template_data in template_list:
                                if template_data['ass_info']['project_name'] == parentName:
                                    if parentName == '穿衣' and itemName == '可独立完成':
                                        itemName = '可以独立完成'

                                    if parentName == '穿衣' and itemName == '需部分帮助（能自己穿脱，但需他人帮助整理衣物、系扣/鞋带、拉拉链）':
                                        itemName = '需部分帮助（能自己穿脱，但需他人帮助整理衣物、系扣/鞋带、拉拉链）  '

                                    if parentName == '穿衣' and itemName == '需极大帮助或完全依赖他人':
                                        itemName = '需要极大帮助或完全依赖他人'
                                    # 3、等于则循环该块的ass_info.dataSource数组，匹配option_content 是否等于 itemName
                                    for dataSource in template_data['ass_info']['dataSource']:
                                        if dataSource['option_content'] == itemName:
                                            # 4、等于则使用score作为ass_info.value的值
                                            template_data['ass_info']['value'] = dataSource['score']

                            temp_data[assessrecId] = template

                  # print('长者表第'+str(count)+'个')
                else:
                    fail_elder.append(data['id_card'])
                    fail_elder = list(set(fail_elder))
            # if len(fail_elder) > 0:
              # print('失败长者身份证》》', len(fail_elder))
            if temp_data:
                res_list = []
                for key in temp_data.keys():
                    t = temp_data[key]
                    assessment_data = t.copy()
                    assessment_data['id'] = str(uuid.uuid1())
                    assessment_data['create_date'] = now_date
                    assessment_data['modify_date'] = now_date
                    assessment_data['GUID'] = str(uuid.uuid1())
                    assessment_data['version'] = 1
                    assessment_data['bill_status'] = 'valid'
                    assessment_data['valid_bill_id'] = str(uuid.uuid1())
                    assessment_data['bill_operator'] = '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'
                    assessment_data = self.setImportDate(
                        assessment_data, '20200722')
                  # print('assessrecId>>>', assessment_data['assessrecId'])
                    res_list.append(assessment_data)

              # print('最终数据》》', len(res_list))

                def process_func(db):
                    insert_many_data(
                        db, 'PT_Behavioral_Competence_Assessment_Record_New', res_list)
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)
              # print('长者评估数据导入OVER')

    def import_excel_assessment_record_list(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'assessrecId': 'assessrecId', '身份证号': 'id_card', 'assessDate': 'assessDate',
                           'assessResult': 'assessResult'}, inplace=True)
        condition = dataframe_to_list(pf)

      # print('开始查询长者')
        temp_elder = {}
        _filter_elder = MongoBillFilter()
        _filter_elder.project({'_id': 0})
        res_elder = self.query(
            _filter_elder, 'PT_Behavioral_Competence_Assessment_Record_New')

      # print('结束查询长者')
        if len(res_elder) > 0:
            for item in res_elder:
                if 'assessrecId' in item:
                    temp_elder[item['assessrecId']] = item

        # 临时处理对象{'user_id':{template_list}}
        temp_data = []
        if len(condition) > 0:

            count = 0
            # now_date = get_cur_time()
            for data in condition:
                assessrecId = data['assessrecId']
                count = count + 1
                if assessrecId in temp_elder:
                    # 找user_id
                    # user_id = temp_elder[data['id_card']]['id']
                    # # 导入列表根据对方唯一id，在我们的数据表中查找
                    # _filter_record_new = MongoBillFilter()
                    # _filter_record_new.match_bill((C('assessrecId') == assessrecId))\
                    #     .project({'_id': 0})
                    # res_record_new = self.query(
                    #     _filter_record_new, 'PT_Behavioral_Competence_Assessment_Record_New')
                    # print('评估记录长度》》', len(res_record_new))
                    # 判断是否存在
                    # if len(res_record_new) > 0:
                    # 存在则更新nursing_level和nursing_time字段
                    nursing_level = ''
                    if data['assessResult'] != '' and data['assessResult'] != None:
                        nursing_level = self.get_nursing_level(
                            data['assessResult'])
                    temp_ = {
                        'nursing_time': self.strtodate(data['assessDate']),
                        'nursing_level': nursing_level
                    }
                    # temp_data.append(temp_)

                    def process_func(db):
                        update_data(
                            db, 'PT_Behavioral_Competence_Assessment_Record_New', temp_, {'id': temp_elder[assessrecId]['id']})
                    process_db(self.db_addr, self.db_port, self.db_name,
                               process_func, self.db_user, self.db_pwd)
                  # print('更新第', count)

            # if len(temp_data)>0:
            #   #print('最终更新数据》》', len(temp_data))

            #     def process_func(db):
            #         insert_many_data(
            #             db, 'PT_Behavioral_Competence_Assessment_Record_New', res_list)
            #     process_db(self.db_addr, self.db_port, self.db_name,
            #                process_func, self.db_user, self.db_pwd)
      # print('长者评估数据导入OVER')

    def get_nursing_level(self, assessResult):
        res = '个性化护理'
        if '介护1' in assessResult:
            res = '介护1'
        if '介护2' in assessResult:
            res = '介护2'
        if '介助1' in assessResult:
            res = '介助1'
        if '介助2' in assessResult:
            res = '介助2'
        if '自理' in assessResult:
            res = '自理'
        return res

    def mapping_A_value(self, mapping_value):
        mz = '不详'
        wh = '1'
        hy = '5'
        if '汉族' in mapping_value:
            mz = '汉族'
        whcd = ['文盲', '小学', '初中', '高中/技校/中专', '大学专科及专科以上', '不详']
        for index, wh_data in enumerate(whcd):
            if wh_data in mapping_value:
                wh = str(index+1)
        hyzk = ['未婚', '已婚', '丧偶', '离婚', '未说明的婚姻状况']
        for inx, data in enumerate(hyzk):
            if data in mapping_value:
                hy = str(inx+1)
        return mz, wh, hy

    def get_model_mapping(self, parentName):
        '''返回对应模块的索引'''
        if parentName in ['进食', '洗澡', '修饰', '穿衣', '大便控制', '小便控制', '如厕', '床椅转移', '平地行走', '上下楼梯']:
            return 2

        elif parentName in ['认知功能', '攻击行为', '抑郁症状']:
            return 3

        elif parentName in ['意识水平', '视力', '听力', '沟通交流', '感知觉与沟通分级']:
            return 4

        elif parentName in ['生活能力', '工作能力', '时间/空间定向', '人物定向', '社会交往能力']:
            return 5

        elif parentName in ['老年人能力初步等级']:
            return 6

        elif parentName in ['老年人能力最终等级']:
            return 7
        else:
            return -2
    item_name = ['需要极大帮助或完全依赖他人', '需部分帮助（因肢体残疾、平衡能力差、过度虚弱、视力等问题，在一定程度上需他人地搀扶或使用拐杖、助行器等辅助用具）', '需极大帮助（因肢体残疾、平衡能力差、过度虚弱、视力等问题，在较大程度上依赖他人搀扶，或坐在轮椅上自行移动）',
                 '需部分帮助（需扶着楼梯、他人搀扶，或使用拐杖等）', '需要极大帮助或完全依赖他人', '嗜睡，表现为睡眠状态过度延长。当呼唤或推动患者的肢体时可唤醒，并能进行正确的交谈或执行指令，停止刺激后又继续入睡',
                 '昏迷，处于浅昏迷时对疼痛刺激有回避和痛苦表情；处于深昏迷时对刺激无反应（若评定为昏迷，直接评定为重度失能，可不进行以下项目的评估）', '昏睡，一般的外界刺激不能使其觉醒，给予较强烈的刺激时可有短时的意识清醒，醒后可简短回答提问，当刺激减弱后又很快进入睡眠状态',
                 '无困难，能与他人正常沟通和交流', '2 中度受损：意识清醒，但视力或听力中至少一项评为3，或沟通评为2；或嗜睡，视力或听力评定为3及以下，沟通评定为2及以下',
                 '0 能力完好：意识清醒，且视力和听力评为0或1，沟通评为0', '1 轻度受损：意识清醒，但视力或听力中至少一项评为2，或沟通评为1', '个人基本生活事务（如饮食、二便）需要部分帮助或完全依赖他人',
                 '时间观念很差，年、月、日不清楚，可知上午或下午；只能在左邻右舍间串门，对现住地不知名称和方位', '能适应单纯环境，主动接触人，初见面时难让人发现智力问题，不能理解隐喻语',
                 '脱离社会，可被动接触，不会主动待人，谈话中很多不适词句，容易上当受骗', '勉强可与人交往，谈吐内容不清楚，表情不恰当', '需极大帮助或完全依赖他人，或有留置营养管',
                 '需要极大帮助或完全依赖他人，或有留置营养管', '准备好洗澡水后，可以独立完成洗澡过程', '在洗澡过程中需要他人协助', '0 能力完好',
                 '1 轻度失能', '2 中度失能', '3 重度失能', '能力完好：日常生活活动、精神状态、感知觉与沟通分级均为0，社会参与的分级为0或1', '轻度失能：日常生活活动分级为0，但精神状态、感知觉与沟通中至少一项分级为1及以上，或社会参与的分级为2；或日常生活活动分级为1，精神状态、感知觉与沟通、社会参与中至少有一项的分级为0或1',
                 '中度失能：日常生活活动分级为1，但精神状态、感知觉与沟通、社会参与均为2，或有一项为3；或日常生活活动分级为2，且精神状态、感知觉与沟通、社会参与中有1-2项的分级为1或2',
                 '重度失能：日常生活活动的分级为3；或日常生活活动、精神状态、感知觉与沟通、社会参与分级均为2；或日常生活活动分级为2，且精神状态、感知觉与沟通、社会参与中至少有一项分级为3']

    def mapping_item_name(self, item_name):
        if item_name == '需要极大帮助或完全依赖他人':
            return '需极大帮助或完全依赖他人'
        if item_name == '需部分帮助（因肢体残疾、平衡能力差、过度虚弱、视力等问题，在一定程度上需他人地搀扶或使用拐杖、助行器等辅助用具）':
            return '需部分帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在一定 程度上需他人地搀扶或使用拐杖、助行器等辅助用具）'
        if item_name == '需极大帮助（因肢体残疾、平衡能力差、过度虚弱、视力等问题，在较大程度上依赖他人搀扶，或坐在轮椅上自行移动）':
            return '需极大帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在较大 程度上依赖他人搀扶，或坐在轮椅上自行移动）'
        if item_name == '需部分帮助（需扶着楼梯、他人搀扶，或使用拐杖等）':
            return '需部分帮助（需他人搀扶，或扶着楼梯、使用拐杖等）'
        if item_name == '需要极大帮助或完全依赖他人':
            return '需极大帮助或完全依赖他人'
        if item_name == '嗜睡，表现为睡眠状态过度延长。当呼唤或推动患者的肢体时可唤醒，并能进行正确的交谈或执行指令，停止刺激后又继续入睡':
            return '嗜睡，表现为睡眠状态过度延长。当呼唤或推动其肢体时可唤醒，并能进行正确的交谈或执行指令，停止刺激后又继续入睡'
        if item_name == '昏迷，处于浅昏迷时对疼痛刺激有回避和痛苦表情；处于深昏迷时对刺激无反应（若评定为昏迷，直接评定为重度失能，可不进行以下项目的评估）':
            return '昏迷，处于浅昏迷时对疼痛刺激有回避和痛苦表情；处于深昏迷时对刺激无反应（若评定为昏迷，直接评定为重度失能，可不进行以下项目的评估）'
        if item_name == '昏睡，一般的外界刺激不能使其觉醒，给予较强烈的刺激时可有短时的意识清醒，醒后可简短回答提问，当刺激减弱后又很快进入睡眠状态':
            return '昏睡，一般的外界刺激不能使其觉醒，给予较强烈的刺激时可有短时的意识清醒，醒后可简短回答提问，当刺激减弱后又很快进入睡眠状态'
        if item_name == '无困难，能与他人正常沟通和交流':
            return '无困难，能于他人正常沟通交流'
        if item_name == '2 中度受损：意识清醒，但视力或听力中至少一项评为3，或沟通评为2；或嗜睡，视力或听力评定为3及以下，沟通评定为2及以下':
            return '中度受损：意识清醒，但视力或听力中至少一项评为3，或沟通评为2； 或嗜睡，视力或听力评定为3及以下，沟通评定为2及以下'
        if item_name == '0 能力完好：意识清醒，且视力和听力评为0或1，沟通评为0':
            return '能力完好：意识清醒，且视力和听力评为0或1，沟通评为0'
        if item_name == '1 轻度受损：意识清醒，但视力或听力中至少一项评为2，或沟通评为1':
            return '轻度受损：意识清醒，但视力或听力中至少一项评为2，或沟通评为1'
        if item_name == '3 重度受损：意识清醒或嗜睡，但视力或听力中至少一项评为4，或沟通评为3；或昏睡/昏迷':
            return '重度受损：意识清醒或嗜睡，但视力或听力中至少一项评为4，或沟通评为3；或昏睡/昏迷'
        if item_name == '个人基本生活事务（如饮食、二便）需要部分帮助或完全依赖他人':
            return '个人基本生活事务（如饮食、二便）需要部分帮助或完全依赖他人帮助'
        if item_name == '时间观念很差，年、月、日不清楚，可知上午或下午；只能在左邻右舍间串门，对现住地不知名称和方位':
            return '时间观念很差，年、月、日不清楚，可知上午或下午；只能在左邻右舍间 串门，对现住地不知名称和方位'
        if item_name == '能适应单纯环境，主动接触人，初见面时难让人发现智力问题，不能理解隐喻语':
            return '能适应单纯环境，主动接触人，初见面时难以让人发现智力问题，不能理解隐喻语参'
        if item_name == '脱离社会，可被动接触，不会主动待人，谈话中很多不适词句，容易上当受骗':
            return '脱离社会，可被动接触，不会主动待人，说话中很多不适当的词句，容易上当受骗'
        if item_name == '勉强可与人交往，谈吐内容不清楚，表情不恰当':
            return '勉强与人交往，谈吐内容不清楚，表情不恰当'
        if item_name == '需要极大帮助或完全依赖他人，或有留置营养管':
            return '需极大帮助或完全依赖他人，或有留置营养管'
        if item_name == '准备好洗澡水后，可以独立完成洗澡过程':
            return '准备好洗澡水后，可自己独立完成洗澡过程'
        if item_name == '在洗澡过程中需要他人协助':
            return '在洗澡过程中需他人帮助'

        if item_name == '能力完好：日常生活活动、精神状态、感知觉与沟通分级均为0，社会参与的分级为0或1':
            return '能力完好'
        if item_name == '轻度失能：日常生活活动分级为0，但精神状态、感知觉与沟通中至少一项分级为1及以上，或社会参与的分级为2；或日常生活活动分级为1，精神状态、感知觉与沟通、社会参与中至少有一项的分级为0或1':
            return '轻度失能'
        if item_name == '中度失能：日常生活活动分级为1，但精神状态、感知觉与沟通、社会参与均为2，或有一项为3；或日常生活活动分级为2，且精神状态、感知觉与沟通、社会参与中有1-2项的分级为1或2':
            return '中度失能'
        if item_name == '重度失能：日常生活活动的分级为3；或日常生活活动、精神状态、感知觉与沟通、社会参与分级均为2；或日常生活活动分级为2，且精神状态、感知觉与沟通、社会参与中至少有一项分级为3':
            return '重度失能'
        if item_name == '0 能力完好':
            return '能力完好'
        if item_name == '1 轻度失能':
            return '轻度失能'
        if item_name == '2 中度失能':
            return '中度失能'
        if item_name == '3 重度失能':
            return '重度失能'

    # 数据结构
    assessment_record = {
        "nursing_level": "介护2",
        "total_score": 0,
        "nursing_time": '',
        "elder": "",
        "template": "11c453d6-0d1b-11ea-ac6d-005056882303",
        "organization_id": "0b96b62e-e363-11e9-93c4-a0a4c57e9ebe",
        'template_list': [
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "接受服务前初评",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": "接受服务后的常规评估",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": "状况发生变化后的即时评估",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "因评估结果有疑问进行的复评",
                                "score": "4",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "评估原因",
                        "selete_type": "1",
                        "value": "2",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "project_name": "民族",
                        "selete_type": "3",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "文盲",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": "小学",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": "初中",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "高中/技校/中专",
                                "score": "4",
                                "serial_number": 4
                            },
                            {
                                "option_content": "大学专科及专科以上",
                                "score": "5",
                                "serial_number": 5
                            },
                            {
                                "option_content": "不详",
                                "score": "6",
                                "serial_number": 6
                            }
                        ],
                        "project_name": "文化程度",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "project_name": "宗教信仰",
                        "selete_type": "3",
                        "value": "无",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "未婚",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": "已婚",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": "丧偶",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "离婚",
                                "score": "4",
                                "serial_number": 4
                            },
                            {
                                "option_content": "未说明的婚姻状况",
                                "score": "5",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "婚姻状况",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "独居",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": "与配偶/伴侣居住",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": "与子女居住",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "与父母居住",
                                "score": "4",
                                "serial_number": 4
                            },
                            {
                                "option_content": "与兄弟姐妹居住",
                                "score": "5",
                                "serial_number": 5
                            },
                            {
                                "option_content": "与其他亲属居住",
                                "score": "6",
                                "serial_number": 6
                            },
                            {
                                "option_content": "与非亲属关系的人居住",
                                "score": "7",
                                "serial_number": 7
                            },
                            {
                                "option_content": "养老机构",
                                "score": "8",
                                "serial_number": 8
                            }
                        ],
                        "project_name": "居住情况",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "城镇职工基本医疗保险",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": " 城镇居民基本医疗保险 ",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": " 新型农村合作医疗 ",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "贫困救助 ",
                                "score": "4",
                                "serial_number": 4
                            },
                            {
                                "option_content": "商业医疗保险",
                                "score": "5",
                                "serial_number": 5
                            },
                            {
                                "option_content": "全公费",
                                "score": "6",
                                "serial_number": 6
                            },
                            {
                                "option_content": "全自费",
                                "score": "7",
                                "serial_number": 7
                            },
                            {
                                "option_content": "其他",
                                "score": "8",
                                "serial_number": 8
                            }
                        ],
                        "project_name": "医疗费用支付方式",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "退休金/养老金",
                                "score": "1",
                                "serial_number": 1
                            },
                            {
                                "option_content": "子女补贴",
                                "score": "2",
                                "serial_number": 2
                            },
                            {
                                "option_content": "亲友资助",
                                "score": "3",
                                "serial_number": 3
                            },
                            {
                                "option_content": "其他补贴",
                                "score": "4",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "经济来源",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "轻度",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "中度",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "重度",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "痴呆",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "精神分裂症",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "双向情感障碍",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "偏执性精神障碍",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "分裂情感性障碍",
                                "score": "4",
                                "serial_number": 5
                            },
                            {
                                "option_content": "癫痫所致精神障碍",
                                "score": "5",
                                "serial_number": 6
                            },
                            {
                                "option_content": "精神发育迟滞伴发精神障碍",
                                "score": "6",
                                "serial_number": 7
                            }
                        ],
                        "project_name": "精神疾病",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "发生过1次",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "发生过2次",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "发生过3次及以上",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "跌倒（30天内）",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "发生过1次",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "发生过2次",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "发生过3次及以上",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "走失（30天内）",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "发生过1次",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "发生过2次",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "发生过3次及以上",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "噎食（30天内）",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "发生过1次",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "发生过2次",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "发生过3次及以上",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "自杀（30天内）",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                },
                {
                    "ass_info": {
                        "project_name": "其他",
                        "selete_type": "3",
                        "value": "",
                        "template_name": "A.2被评估者的基本信息表",
                        "serial_number": 1
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "project_name": "信息提供者的姓名",
                        "selete_type": "3",
                        "value": "",
                        "template_name": "A.3信息提供者及联系人信息表",
                        "serial_number": 2
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "配偶",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "子女",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "其他亲属",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "雇佣照顾者",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "其他",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "信息提供者与老人的关系",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "A.3信息提供者及联系人信息表",
                        "serial_number": 2
                    }
                },
                {
                    "ass_info": {
                        "project_name": "联系人姓名",
                        "selete_type": "3",
                        "value": "",
                        "template_name": "A.3信息提供者及联系人信息表",
                        "serial_number": 2
                    }
                },
                {
                    "ass_info": {
                        "project_name": "联系人电话",
                        "selete_type": "3",
                        "value": "",
                        "template_name": "A.3信息提供者及联系人信息表",
                        "serial_number": 2
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可独立进食（在合理的时间内独立进食准备好的食物）",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（进食过程中需要一定帮助，如协助把持餐具）",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需极大帮助或完全依赖他人，或有留置营养管",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "进食",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "准备好洗澡水后，可自己独立完成洗澡过程",
                                "score": "5",
                                "serial_number": 1
                            },
                            {
                                "option_content": "在洗澡过程中需他人帮助",
                                "score": "0",
                                "serial_number": 2
                            }
                        ],
                        "project_name": "洗澡",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可以独立完成",
                                "score": "5",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需要他人协助",
                                "score": "0",
                                "serial_number": 2
                            }
                        ],
                        "project_name": "修饰",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可以独立完成",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（能自己穿脱，但需他人帮助整理衣物、系扣/鞋带、拉拉链）  ",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需要极大帮助或完全依赖他人",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "穿衣",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可控制大便",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "偶尔失控（每周<1次），或需要他人提示",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "完全失控",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "大便控制",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可控制小便",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "偶尔失控（每天<1次，但每周>1次），或需要他人提示",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "完全失控，或留置导尿管",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "小便控制",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可独立完成",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（需他人搀扶去厕所、需他人帮忙冲水或整理衣裤等）",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需极大帮助或完全依赖他人",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "如厕",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可独立完成",
                                "score": "15",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（需他人搀扶或使用拐杖）",
                                "score": "10",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需极大帮助（较大程度上依赖他人搀扶和帮助）",
                                "score": "5",
                                "serial_number": 3
                            },
                            {
                                "option_content": "完全依赖他人",
                                "score": "0",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "床椅转移",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可独立在平地上行走45m",
                                "score": "15",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在一定 程度上需他人地搀扶或使用拐杖、助行器等辅助用具）",
                                "score": "10",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需极大帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在较大 程度上依赖他人搀扶，或坐在轮椅上自行移动）",
                                "score": "5",
                                "serial_number": 3
                            },
                            {
                                "option_content": "完全依赖他人",
                                "score": "0",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "平地行走",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可独立上下楼梯（连续上下10-15个台阶）",
                                "score": "10",
                                "serial_number": 1
                            },
                            {
                                "option_content": "需部分帮助（需他人搀扶，或扶着楼梯、使用拐杖等）",
                                "score": "5",
                                "serial_number": 2
                            },
                            {
                                "option_content": "需极大帮助或完全依赖他人",
                                "score": "0",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "上下楼梯",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.1日常生活活动评估表",
                        "serial_number": 3
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "画钟正确（画出一个闭锁圆，指针位置准确），且能回忆出2-3个词",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "画钟错误（画的圆不闭锁，或指针位置不准确），或只回忆出0-1个词",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "已确诊为认知障碍，如老年痴呆",
                                "score": "2",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "认知功能",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.2精神状态评估表",
                        "serial_number": 4
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无身体攻击行为（如打/踢/推/咬/抓/摔东西）和语言攻击行为（如骂人、语言威胁、尖叫）",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "每月有几次身体攻击行为，或每周有几次语言攻击行为",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "每周有几次身体攻击行为，或每日有语言攻击行为",
                                "score": "2",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "攻击行为",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.2精神状态评估表",
                        "serial_number": 4
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "情绪低落、不爱说话、不爱梳洗、不爱活动",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "有自杀念头或自杀行为",
                                "score": "2",
                                "serial_number": 3
                            }
                        ],
                        "project_name": "抑郁症状",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.2精神状态评估表",
                        "serial_number": 4
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "神志清醒，对周围环境警觉",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "嗜睡，表现为睡眠状态过度延长。当呼唤或推动其肢体时可唤醒，并能进行正确的交谈或执行指令，停止刺激后又继续入睡",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "昏睡，一般的外界刺激不能使其觉醒，给予较强烈的刺激时可有短时的意识清醒，醒后可简短回答提问，当刺激减弱后又很快进入睡眠状态",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "昏迷，处于浅昏迷时对疼痛刺激有回避和痛苦表情；处于深昏迷时对刺激无反应（若评定为昏迷，直接评定为重度失能，可不进行以下项目的评估）",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "意识水平",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.3感知觉与沟通评估表",
                        "serial_number": 5
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "能看清书报上的标准字体",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "能看清楚大字体，但看不清书报上的标准字体",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "视力有限，看不清报纸大标题，但能辨认物体",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "辨认物体有困难，但眼睛能跟随物体移动，只能看到光、颜色和形状",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "没有视力，眼睛不能跟随物体移动",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "视力",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.3感知觉与沟通评估表",
                        "serial_number": 5
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "可正常交谈，能听到电视、电话、门铃的声音",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "在轻声说话或说话距离超过2米时听不清",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "正常交流有些困难，需在安静的环静或大声说话才能听到",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "讲话者大声说话或说话很慢，才能部分听见",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "完全听不见",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "听力",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.3感知觉与沟通评估表",
                        "serial_number": 5
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "无困难，能于他人正常沟通交流",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "能够表达自己的需要及理解别人的话，但需要增加时间或给予帮助",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "表达需要或理解有困难，需频繁重复或简化口头表达",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "不能表达需要或理解他人的话",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "沟通交流",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.3感知觉与沟通评估表",
                        "serial_number": 5
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "能力完好：意识清醒，且视力和听力评为0或1，沟通评为0",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "轻度受损：意识清醒，但视力或听力中至少一项评为2，或沟通评为1",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "中度受损：意识清醒，但视力或听力中至少一项评为3，或沟通评为2； 或嗜睡，视力或听力评定为3及以下，沟通评定为2及以下",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "重度受损：意识清醒或嗜睡，但视力或听力中至少一项评为4，或沟通评为3；或昏睡/昏迷",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "感知觉与沟通分级",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.3感知觉与沟通评估表",
                        "serial_number": 5
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "除个人生活自理外（如饮食、洗漱、穿戴、二便），能料理家务（如做饭、洗衣）或当家管理事务",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "除个人生活自理外，能做家务，但欠好，家庭事务安排欠条理",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "个人生活能自理；只有在他人帮助下才能做些家务，但质量不好",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "个人基本生活事务能自理（如饮食、二便），在督促下可洗漱",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "个人基本生活事务（如饮食、二便）需要部分帮助或完全依赖他人帮助",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "生活能力",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.4社会参与评估表",
                        "serial_number": 6
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "原来熟练的脑力工作或体力技巧性工作可照常进行",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "原来熟练的脑力工作或体力技巧性工作能力有所下降",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "原来熟练的脑力工作或体力技巧性工作明显不如以往，部分遗忘",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "对熟练工作只有一些片段保留，技能全部遗忘",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "对以往的知识或技能全部磨灭",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "工作能力",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.4社会参与评估表",
                        "serial_number": 6
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "时间观念（年、月、日、时）清楚；可单独出远门，能很快掌握新环境的方位",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "时间观念有些下降，年、月、日清楚，但有时相差几天；可单独来往于近街，知道现住地的名称和方位，但不知回家路线",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "时间观念较差，年、月、日不清楚，可知上半年或下半年；只能单独在家附近行动，对现住地只知名称，不知道方位",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "时间观念很差，年、月、日不清楚，可知上午或下午；只能在左邻右舍间 串门，对现住地不知名称和方位 ",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "无时间观念；不能单独外出",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "时间/空间定向",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.4社会参与评估表",
                        "serial_number": 6
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "知道周围人们的关系，知道祖孙、叔伯、姑姨、侄子侄女等称谓的意义；可分辨陌生人的大致年龄和身份，可用适当称呼",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "只知家中亲密近亲的关系，不会分辨陌生人的大致年龄，不能称呼陌生人",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "只能称呼家中人，或只能照样称呼，不知其关系，不辨辈分",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "只认识常同住的亲人，可称呼子女或孙子女，可辨熟人和生人",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "只认识保护人，不辨熟人和生人",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "人物定向",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.4社会参与评估表",
                        "serial_number": 6
                    }
                },
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "参与社会，在社会环境有一定的适应能力，待人接物恰当",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "能适应单纯环境，主动接触人，初见面时难以让人发现智力问题，不能理解隐喻语参",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "脱离社会，可被动接触，不会主动待人，说话中很多不适当的词句，容易上当受骗",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "勉强与人交往，谈吐内容不清楚，表情不恰当",
                                "score": "3",
                                "serial_number": 4
                            },
                            {
                                "option_content": "难以与人接触",
                                "score": "4",
                                "serial_number": 5
                            }
                        ],
                        "project_name": "社会交往能力",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "B.4社会参与评估表",
                        "serial_number": 6
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "能力完好",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "轻度失能",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "中度失能",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "重度失能",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "老年人能力初步等级",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "C.2老年人能力初步等级",
                        "serial_number": 7
                    }
                }
            ],
            [
                {
                    "ass_info": {
                        "dataSource": [
                            {
                                "option_content": "能力完好",
                                "score": "0",
                                "serial_number": 1
                            },
                            {
                                "option_content": "轻度失能",
                                "score": "1",
                                "serial_number": 2
                            },
                            {
                                "option_content": "中度失能",
                                "score": "2",
                                "serial_number": 3
                            },
                            {
                                "option_content": "重度失能",
                                "score": "3",
                                "serial_number": 4
                            }
                        ],
                        "project_name": "老年人能力最终等级",
                        "selete_type": "1",
                        "value": "",
                        "template_name": "C.4老年人能力最终等级",
                        "serial_number": 8
                    }
                }
            ]
        ]
    }
