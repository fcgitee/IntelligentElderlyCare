'''
@Author: your name
@Date: 2019-10-14 17:34:58
@LastEditTime : 2019-12-23 19:52:22
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_pub\role.py
'''
from ...service.welfare_institution.accommodation_process import CheckInStatus
from ...service.common import get_info, get_current_role_id, UserType, operation_result, get_current_org_type, get_current_organization_id, delete_data, get_common_project
from ...pao_python.pao.data import process_db, dataframe_to_list, string_to_date
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
import pandas as pd
import uuid
from ...pao_python.pao.remote import JsonRpc2Error
import pandas as pd
import datetime
import copy


class Role(MongoService):
    '''角色设置管理'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_role_user_list(self, org_list, condition, page, count):
        '''获取角色设置列表'''
        keys = ['id', 'name', 'role_name', 'organization_name']
        values = self.get_value(condition, keys)
        role_ids = []
        role_data = {}
        if condition.get('role_name'):
            # 查询角色表数据数据
            _filter_role = MongoBillFilter()
            _filter_role.match_bill((C('name').like(values['role_name'])))\
                .project({'_id': 0})
            res_role = self.query(
                _filter_role, 'PT_Role')
            if len(res_role) > 0:
                for role in res_role:
                    role_data[role['id']] = role
                    role_ids.append(role['id'])
        else:
            role_ids = N()
        # 查询工作人员数据
        worker_ids = []
        worker_data = {}
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('personnel_type') == '1')
                                  & (C('name').like(values['name']))
                                  & (C('personnel_info.personnel_category') == '工作人员'))\
            .project({'_id': 0})
        res_worker = self.query(
            _filter_worker, 'PT_User')
        if len(res_worker) > 0:
            for worker in res_worker:
                worker_data[worker['id']] = worker
                worker_ids.append(worker['id'])
        else:
            worker_ids = N()

        # 查询机构
        org_ids = []
        org_data = {}
        if condition.get('organization_name'):
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('personnel_type') == '2')
                                   & (C('name').like(values['organization_name'])))\
                .project({'_id': 0})
            res_org = self.query(
                _filter_org, 'PT_User')
            if len(res_org) > 0:
                for org in res_org:
                    org_data[org['id']] = org
                    org_ids.append(org['id'])
        else:
            org_ids = N()

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id').inner(org_list)
             & (C('principal_account_id').inner(worker_ids))
             & (C('role_id').inner(role_ids))
             & (C('role_of_account_id').inner(org_ids))
             & (C('id') == values['id']))
        )\
            .sort({'modify_date': -1})\
            .project({'_id': 0, 'id': 1, 'modify_date': 1, 'principal_account_id': 1, 'role_of_account_id': 1, 'role_id': 1})
        res = self.page_query(_filter, 'PT_Set_Role', page, count)
        role_of_account_ids = []
        role_info_ids = []
        for i, x in enumerate(res['result']):
            # 机构
            role_of_account_ids.append(res['result'][i]['role_of_account_id'])
            # 角色
            if 'role_name' not in condition:
                role_info_ids.append(res['result'][i]['role_id'])

        role_of_account_data = {}
        # 有组织名字的筛选，不需要重新获取
        if type(org_ids) == list and len(org_ids) > 0:
            for org_id_data in org_data:
                role_of_account_data[org_id_data
                                     ] = org_data[org_id_data]
        elif len(role_of_account_ids) > 0:
            # 查询机构数据
            _filter_role_of_account = MongoBillFilter()
            _filter_role_of_account.match_bill((C('id').inner(role_of_account_ids)) & (C('name').like(values['organization_name'])))\
                .project({'_id': 0})
            res_role_of_account = self.query(
                _filter_role_of_account, 'PT_User')
            if len(res_role_of_account) > 0:
                for role_of_account in res_role_of_account:
                    role_of_account_data[role_of_account['id']
                                         ] = role_of_account
        if len(role_info_ids) > 0:
            # 查询角色表数据数据
            _filter_role = MongoBillFilter()
            _filter_role.match_bill(C('id').inner(role_info_ids) & C('name').like(values['role_name']))\
                .project({'_id': 0})
            res_role = self.query(
                _filter_role, 'PT_Role')
            if len(res_role) > 0:
                for role in res_role:
                    role_data[role['id']] = role
        for i, x in enumerate(res['result']):
            # 机构
            if 'role_of_account_id' in res['result'][i] and res['result'][i]['role_of_account_id'] in role_of_account_data.keys():
                res['result'][i]['role_of_account'] = role_of_account_data[res['result']
                                                                           [i]['role_of_account_id']]['name']
            # 工作人员
            if 'principal_account_id' in res['result'][i] and res['result'][i]['principal_account_id'] in worker_data.keys():
                res['result'][i]['name'] = worker_data[res['result']
                                                       [i]['principal_account_id']]['name']
            # 角色
            if 'role_id' in res['result'][i] and res['result'][i]['role_id'] in role_data.keys():
                res['result'][i]['role_name'] = role_data[res['result']
                                                          [i]['role_id']]['name']
                if 'comment' in role_data[res['result'][i]['role_id']]:
                    res['result'][i]['comment'] = role_data[res['result']
                                                            [i]['role_id']]['comment']

        return res

    def delete_role_user(self, condition):
        ids = []
        if isinstance(condition, str):
            ids.append({"id": condition})
        else:
            for id_str in condition:
                ids.append({"id": id_str})
        self.bill_manage_service.add_bill(OperationType.delete.value,
                                          TypeId.setRole.value, ids, 'PT_Set_Role')
        res = 'Success'
        return res

    def update_role_user(self, condition):
        '''新增/修改角色关联'''
        flag = OperationType.update.value if 'id' in condition.keys() else OperationType.add.value
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.setRole.value, condition, 'PT_Set_Role')
        return operation_result(bill_id)

    def update_role(self, condition):
        '''新增/修改角色'''
        hasId = True if 'id' in condition.keys() else False
        role_id = condition['id'] if hasId else str(uuid.uuid1())
        flag = OperationType.update.value if hasId else OperationType.add.value
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(condition['ids']))\
               .project({'_id': 0})
        res = self.query(_filter, 'PT_Permission')
        # 权限表数据组织
        pf = pd.DataFrame(res)
        col_n = ['function', 'permission', 'remarks', 'is_allow_super_use']
        df = pd.DataFrame(pf, columns=col_n)
        df['organization_id'] = condition['organization_id']
        df['role_id'] = role_id
        df['remarks'] = str(condition['role_type'] + '_' + condition['name'])
        con = dataframe_to_list(df)
        if hasId:  # 编辑
            _filter_all = MongoBillFilter()
            _filter_all.match_bill(C('role_id') == role_id)\
                       .project({'_id': 0})
            res = self.query(_filter_all, 'PT_Permission')
            pf_res = pd.DataFrame(res)
            res_ids = dataframe_to_list(pd.DataFrame(pf_res, columns=['id']))
            # 暴力一点，全删除

            def process_func(db):
                nonlocal res
                for ids in res_ids:
                    delete_data(db, 'PT_Permission', ids)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
            # bill_id = self.bill_manage_service.add_bill(
            #     OperationType.delete.value, TypeId.role.value, res_ids, 'PT_Permission')
        # 角色表数据组织
        role = {'id': role_id,
                'role_type': condition['role_type'], 'name': condition['name'], 'organization_id': condition['organization_id'],
                'comment': condition['comment'] if 'comment' in condition.keys() else ''
                }

        # 菜单权限
        if 'menu_permission' in condition and type(condition['menu_permission']) == list and len(condition['menu_permission']) > 0:
            for item in condition['menu_permission']:
                con.append({
                    'function': item,
                    'permission': '菜单',
                    'remarks': item + '_菜单',
                    'is_allow_super_use': True,
                    'organization_id': condition['organization_id'],
                    'role_id': role_id,
                })

        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.role.value, role, 'PT_Role')
        # 为角色新增权限
        permission_bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.role.value, con, 'PT_Permission')
        return 'Success' if bill_id and permission_bill_id else '保存失败'

    def get_role_type(self):
        role_id = get_current_role_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == role_id)\
               .project({'_id': 0})
        res = self.query(_filter, 'PT_Role')
        role_type_res = res[0]['role_type']
        role_type = ['平台', '福利院', '幸福院', '服务商',
                     '民政局'] if role_type_res == '平台' else [role_type_res]
        return role_type

    def delete_role(self, condition):
        ids = []
        if isinstance(condition, str):
            ids.append(condition)
        else:
            ids = condition
        _filter = MongoBillFilter()
        _filter.match_bill(C('role_id').inner(ids))\
               .project({'_id': 0})
        res = self.query(_filter, 'PT_Set_Role')
        if len(res):
            raise JsonRpc2Error(-36201, '该角色已经关联管理员账号，无法删除')
        delete_data = []
        for x in ids:
            delete_data.append({'id': x})
        bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                    TypeId.role.value, delete_data, 'PT_Role')
        return bill_id

    def get_role_all_list(self, org_list, condition, page, count):
        '''获取角色列表'''
        keys = ['id', 'name', 'function', 'permission', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'is_platform' in condition.keys():
            role_id = get_current_role_id(self.session)
            _filter_cur = MongoBillFilter()
            _filter_cur.match_bill(C('id') == role_id)\
                .project({'_id': 0})
            res = self.query(_filter_cur, 'PT_Role')
            cur_type = res[0]['role_type']
            _filter.match_bill(
                (((C('is_platform') == '平台') & (C('role_type') == cur_type))) | (C('organization_id').inner(org_list)))
        else:
            _filter.match_bill(C('organization_id').inner(org_list))
        _filter.match((C('id') == values['id'])
                      & (C('name').like(values['name']))
                      )\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .match_bill(C('org.name').like(values['org_name']))\
            .lookup_bill('PT_Permission', 'id', 'role_id', 'permiss_list')\
            .project({'user.login_info': 0, 'org.qualification_info': 0, **get_common_project({'user', 'permiss_list', 'org'})})\
            .sort({'modify_date': -1})
        res = self.page_query(_filter, 'PT_Role', page, count)
        return res

    def get_cur_person_role_pure_list(self, org_list, condition, page, count):
        '''快速的获取角色列表'''
        keys = ['id', 'name', 'function', 'permission', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'is_platform' in condition.keys():
            role_id = get_current_role_id(self.session)
            _filter_cur = MongoBillFilter()
            _filter_cur.match_bill(C('id') == role_id)\
                .project({'_id': 0})
            res = self.query(_filter_cur, 'PT_Role')
            cur_type = res[0]['role_type']
            _filter.match_bill(
                (((C('is_platform') == '平台') & (C('role_type') == cur_type))) | (C('organization_id').inner(org_list)))\
                .project({'_id': 0, 'id': 1, 'name': 1, 'organization_id': 1, 'is_platform': 1})\
                .sort({'modify_date': -1})
        res = self.page_query(_filter, 'PT_Role', page, count)
        return res

    def get_role_list_only(self, org_list, condition, page, count):
        keys = ['id', 'name', 'function', 'permission', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id').inner(org_list))
            & (C('name').like(values['org_name'])))\
            .project({'_id': 0, 'name': 1, 'id': 1})
        res_org = self.query(_filter, 'PT_User')
        if len(res_org):
            org_data = []
            org_ids = []
            for x in res_org:
                org_data.append(
                    {'org_id': x['id'], 'org_name': x['name']})
                org_ids.append(x['id'])
            _filter_user = MongoBillFilter()
            _filter_user.match_bill(C('organization_id').inner(org_ids)
                                    & (C('id') == values['id'])
                                    & (C('name').like(values['name']))
                                    )\
                .sort({'modify_date': -1})\
                .project({**get_common_project({}), 'org': 0})
            res = self.page_query(_filter_user, 'PT_Role', page, count)
            for i, y in enumerate(res['result']):
                for z in org_data:
                    if y['organization_id'] == z['org_id']:
                        res['result'][i]['org_name'] = z['org_name']
        else:
            res = {'result': [], 'total': 0}

        return res

    def get_permiss_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        role_id = condition['role_id'] if 'role_id' in condition.keys(
        ) else get_current_role_id(self.session)
        _filter.match_bill(C('role_id') == role_id)\
            .project({'_id': 0, 'GUID': 0, 'modify_date': 0, 'bill_status': 0, 'version': 0, 'valid_bill_id': 0})
        res = self.page_query(_filter, 'PT_Permission', page, count)

        newarr = {}
        newres = {
            'result': []
        }
        for item in res['result']:
            for itm in res['result']:
                if item['id'] == itm['id']:
                    if item['id'] in newarr:
                        newarr[item['id']] = newarr[item['id']] + 1
                    else:
                        newarr[item['id']] = 1
                        newres['result'].append(item)
        return newres

    def get_role_single(self, condition):
        _filter = MongoBillFilter()
        # 当前账号角色和组织id
        values = self.get_value(
            {
                'cur_role_id': get_current_role_id(self.session),
                'cur_organization_id': get_current_organization_id(self.session)
            }, ['cur_role_id', 'cur_organization_id'])
        _filter.match_bill(C('role_id') == condition['id'])\
               .lookup_bill('PT_Permission', 'function', 'function', 'permission_list')\
               .add_fields({'org_permiss_list': self.ao.array_filter("$permission_list", "aa", ((F('$aa.role_id') == values['cur_role_id']) & (F('$aa.permission') == (F('permission')))).f)})\
            .match_bill(
            C('permission_list.role_id') == values['cur_role_id'])\
            .inner_join_bill('PT_Role', 'role_id', 'id', 'role')\
            .project({'_id': 0, 'GUID': 0, 'modify_date': 0, 'bill_status': 0, 'version': 0, 'valid_bill_id': 0, 'permission_list': 0, 'org_permiss_list._id': 0, 'role._id': 0,
                      'org_permiss_list.GUID': 0, 'org_permiss_list.bill_status': 0, 'org_permiss_list.version': 0, 'org_permiss_list.modify_date': 0, 'org_permiss_list.valid_bill_id': 0,
                      'role.GUID': 0, 'role.bill_status': 0, 'role.version': 0, 'role.modify_date': 0, 'role.valid_bill_id': 0,
                      })
        res = self.query(_filter, 'PT_Permission')

        newarr = {}
        newres = []
        for item in res:
            for itm in res:
                if item['id'] == itm['id']:
                    if item['id'] in newarr:
                        newarr[item['id']] = newarr[item['id']] + 1
                    else:
                        newarr[item['id']] = 1
                        newres.append(item)
        return newres

    def get_menu_permmission_list(self):

        role_id = get_current_role_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == role_id)\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Role')
        role_type_res = res[0]['role_type']
        # 呼叫中心系统
        # 长者信息管理系统
        # 服务订单系统
        # 组织机构架设
        # 民政管理系统
        # 幸福院管理系统
        # 养老机构管理系统
        # 平台管理端

        if role_type_res == '服务商':
            return ['居家服务']
        elif role_type_res == '民政局':
            return ['民政部门']
        elif role_type_res == '幸福院':
            return ['社区幸福院']
        elif role_type_res == '养老机构':
            return ['机构养老']
        else:
            return ['居家服务', '民政部门', '社区幸福院', '机构养老', '平台运营方']

        # return ['福利院管理系统', '幸福院管理系统', '服务商管理系统']

    def get_role_menu_permmission_list(self, condition):

        _filter = MongoBillFilter()
        # 当前账号角色和组织id
        _filter.match_bill((C('role_id') == condition['id']) & (C('permission') == '菜单'))\
            .project({'_id': 0, **get_common_project()})
        res = self.query(_filter, 'PT_Permission')
        return res

    def get_sys_use_staticstics_list(self, condition, page, count):
        keys = ['id', 'org_name', 'org_type',
                'module_name', 'start_date', 'end_date']
        if condition.get('create_date'):
            condition['start_date'] = string_to_date(condition['create_date'])
            condition['end_date'] = datetime.timedelta(
                days=1) + condition['start_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match(
            ((C('module_name') != None) & (C('module_name') != ''))
            & (C('org_name').like(values['org_name']))
            & (C('org_type') == values['org_type'])
            & (C('module_name').like(values['module_name']))
            & (C('create_date') >= values['start_date']) & (C('create_date') <= values['end_date'])
        )\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'operation_data': 0})
        res = self.page_query(_filter, 'PT_Operation_Log', page, count)
        return res

    def get_staticstics_user(self, condition, page, count):
        keys = ['id', 'org_name', 'org_type',
                'module_name', 'start_date', 'end_date']
        if condition.get('create_date'):
            condition['start_date'] = string_to_date(condition['create_date'])
            condition['end_date'] = datetime.timedelta(
                days=1) + condition['start_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match(
            ((C('module_name') != None) & (C('module_name') != ''))
            & (C('org_name').like(values['org_name']))
            & (C('org_type') == values['org_type'])
            & (C('module_name').like(values['module_name']))
            & (C('create_date') >= values['start_date']) & (C('create_date') <= values['end_date'])
        )\
            .group({
                'user_name': '$user_name'}, [{'操作频率': self.ao.summation(1)}])\
            .project({'_id': 0, '操作频率': 1, 'user_name': 1})
        res = self.query(_filter, 'PT_Operation_Log')
        return {'result': [{'count': len(res)}], 'total': 1}

    def download_sys_use_staticstics(self, condition):
        keys = ['id', 'org_name', 'org_type',
                'module_name', 'start_date', 'end_date']
        data = []
        if condition.get('org_type'):
            if condition.get('create_date'):
                condition['start_date'] = string_to_date(
                    condition['create_date'])
                condition['end_date'] = datetime.timedelta(
                    days=1) + condition['start_date']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match(
                ((C('module_name') != None) & (C('module_name') != ''))
                & (C('org_name').like(values['org_name']))
                & (C('org_type') == values['org_type'])
                & (C('module_name').like(values['module_name']))
                & (C('create_date') >= values['start_date']) & (C('create_date') <= values['end_date'])
            )\
                .group({
                    '组织机构': '$org_name',
                    '组织类型': '$org_type',
                    '功能名称': '$module_name'}, [{'操作频率': self.ao.summation(1)}])\
                .sort({'org_name': -1})\
                .project({'_id': 0, '组织机构': 1, '功能名称': 1, '组织类型': 1})
            res = self.query(_filter, 'PT_Operation_Log')
            module_list = self.bill_manage_service.get_module_name_all()
            head_list = module_list[condition['org_type']]
            for i, y in enumerate(res):
                if y['功能名称'] in head_list:
                    new_head = copy.deepcopy(head_list)
                    new_head[y['功能名称']] = '已使用'
                    res[i].update(new_head)
                del res[i]['功能名称']
            has_org = []
            for i, x in enumerate(res):
                if x['组织机构'] in has_org:
                    for j, y in enumerate(data):
                        if x['组织机构'] == y['组织机构']:
                            for key in x:
                                if x[key] == '已使用':
                                    data[j][key] = x[key]
                else:
                    has_org.append(x['组织机构'])
                    data.append(x)
        return {'name': condition['org_type'] + '系统使用情况统计', 'value': data}

    def get_current_user_role_name(self):
        # 获取当前登录用户的角色名
        res = 'Fail'
        role_id = get_current_role_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == role_id))\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Role')
        return res
