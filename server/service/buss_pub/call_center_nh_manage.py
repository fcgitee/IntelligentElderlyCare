# -*- coding: utf-8 -*-

from ...pao_python.pao.data import process_db, get_cur_time, dataframe_to_list, DataProcess
import pandas as pd
import uuid
import random
import datetime
import json
from threading import Thread
from enum import Enum
from requests import post
import requests
from ...service.mongo_bill_service import MongoBillFilter
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id, get_user_id_or_false, get_common_project, get_current_organization_id, update_many_data, insert_many_data, get_current_role_id
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.mg_hardware.mg_hardware_manage import (MgHardwareService)
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)
import time
import paho.mqtt.client as mqtt

# 移动接口主域名
host = 'https://ccacs.deskpro.cn'

# 外呼接口
callout_url = host + '/ccacs/ws/call/callout'
# 保持接口
holdon_url = host + '/ccacs/ws/call/hold'
# 取消保持接口
unholdon_url = host + '/ccacs/ws/call/unhold'
# 签入接口
login_url = host + '/ccacs/ws/agent/login'
# 签出接口
logout_url = host + '/ccacs/ws/agent/logout'
# 挂断接口
hangup_url = host + '/ccacs/ws/call/releasecall'
# 转接接口
transout_url = host + '/ccacs/ws/call/transout'
# 轮询接口
poll_url = host + '/ccacs/ws/event/poll'
# 改变坐席状态接口
changestate_url = host + '/ccacs/ws/agent/setagentstate'

# MQTT帐号密码信息
mqtt_user = 'pageMqttAmdin'
mqtt_pass = 'efafda3456789%*+'
mqtt_host = 'devsvr.nuoiot.com'
mqtt_port = 1883

# 大屏获取数据帐号密码
push_user = 'displayer'
push_pass = 'Orgic@67'


class CallCenterNhService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, request={}):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.request = request
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.mg_hardware_service = MgHardwareService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

        # 呼叫中心记录主表
        self.call_center_db = 'PT_Call_Center_Nh'
        # 日志数据表
        self.call_center_log2_db = 'PT_Call_Center_Log2'
        # 记录推送到大屏的记录表
        self.call_center_push = 'PT_Call_Center_Push'

        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Connection': 'keep-alive',
            'Accept': '*/*'
        }

        # 任务的缓存池
        self.dsnCache = {}

        # 坐标的缓存池
        self.locationCache = {}

        # PT_User的缓存池
        self.userCache = {}

        # 机构坐标的缓存池
        self.orgLocationCache = {}

        # 家属关系的缓存池
        self.relationsCache = {}

        # 是否测试模式，线上一定要False，本地测试必须True
        self.TEST = False

        # 呼叫信息，本地测试才有用
        self.callId = {}

        # 当前登录的坐席信息，本地测试才有用
        self.agentInfo = {}

        # MQTT
        self.mqtt_client = mqtt.Client()
        self.threading = None

    def _print(self, obj):
        if self.TEST == True:
            print(obj)

    # 根据是否测试模式来决定操作方式
    def methodByIsTest(self, methodType, params={}):

        if methodType == 'getAgentInfo':
            # 获取储存的坐席信息
            if self.TEST == False:
                if 'agentInfo' in params:
                    return params['agentInfo']
                else:
                    return {}
            else:
                return self.agentInfo
        elif methodType == 'getCallId':
            # 获取储存的通话信息

            # 获取当前headers
            headers = self.headers.copy()
            # 获取当前session
            session = self.session.copy()

            if self.TEST == False:
                # 非测试模式需要在headers把cookie信息带过去
                headers['Cookie'] = session['Cookie']
                callId = session['callId']
            else:
                callId = self.callId.copy()

            return callId, headers

    # 储存日志
    def setLog(self, logMessage, session={}):

        # 获取当前登录的坐席信息
        agent_info = self.methodByIsTest('getAgentInfo', session)

        save_data = {
            'id': str(uuid.uuid1()),
            'agent_id': agent_info.get('agentId'),
            'session': session,
            'create_date': get_cur_time(),
            **logMessage,
        }

        if save_data.get("agent_id"):
            def process_func_insert_log(db):
                insert_data(db, self.call_center_log2_db, save_data)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func_insert_log, self.db_user, self.db_pwd)

    def api(self, apiType, condition):

        # 系统接口发起时间节点
        xt_api_begin = round(time.time(), 2)

        result = None
        isLog = True
        logs = {}

        if apiType == 'login':
            self._print('API操作：签入')
            result = self.login(condition)
        elif apiType == 'logout':
            self._print('API操作：签出')
            result = self.logout()
        elif apiType == 'callout':
            self._print('API操作：外呼')
            result = self.callout(condition)
        elif apiType == 'hangup':
            self._print('API操作：挂断')
            result = self.hangup()
        elif apiType == 'holdon':
            self._print('API操作：保持')
            result = self.holdon()
        elif apiType == 'unholdon':
            self._print('API操作：取消保持')
            result = self.unholdon()
        elif apiType == 'transout':
            self._print('API操作：转接')
            result = self.transout(condition)
        elif apiType == 'setbusy':
            self._print('API操作：示忙')
            result = self.changestate('busy')
        elif apiType == 'setfree':
            self._print('API操作：示闲')
            result = self.changestate('rest')
        elif apiType == 'getpool':
            # self._print('API操作：轮询')
            result = self.getpool(condition)
        elif apiType == 'get_record_url':
            isLog = False
            self._print('API操作：获取录音')
            result = self.get_record_url(condition)
        elif apiType == 'get_location':
            isLog = False
            self._print('API操作：获取机构定位')
            result = self.get_location(condition)
        elif apiType == 'get_user':
            self._print('API操作：获取用户信息')
            result = self.get_user(condition)
        elif apiType == 'get_phone':
            isLog = False
            self._print('API操作：根据任务ID获取手机号码')
            result = self.get_phone(condition)
        elif apiType == 'get_zh':
            isLog = False
            self._print('API操作：获取帐号信息')
            result = self.get_zh(condition)
        elif apiType == 'aaatest':
            isLog = False
            result = self.aaatest(condition)

        # 系统接口完成时间节点
        xt_api_finish = round(time.time(), 2)

        # 额外的日志
        if result and 'logs' in result:
            logs = result.get('logs')
            del(result['logs'])

        if isLog == True:

            try:

                user_id = get_current_user_id(self.session)
                session = self.session.copy()

                def insertALog(user_id, session):
                    # 保存记录
                    self.setLog({
                        'api_type': apiType,
                        'params': condition,
                        'xt_api_begin': xt_api_begin,
                        'xt_api_finish': xt_api_finish,
                        # 系统接口完成时间
                        'xt_api_times': round(xt_api_finish - xt_api_begin, 2),
                        'user_id': user_id,
                        **logs
                    }, session)

                threadLog = Thread(target=insertALog,
                                   args=(user_id, session,))
                threadLog.start()
            except Exception as ex:
                pass

        # 返回结果
        return result

    # 签入接口
    def login(self, condition):

        # 日志
        logs = {
            'method': '签入'
        }

        login_header = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Connection': 'keep-alive',
            'Accept': '*/*'
        }
        times = 0

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【签入】接口
        res = requests.post(json=condition, url=login_url,
                            headers=login_header)
        # 头信息
        logs['headers'] = login_header
        # 接口入参
        logs['condition'] = condition
        # 设置五次重签机制
        while(times < 5):
            if 'Set-Cookie' not in res.headers:
                times = times + 1
                res = requests.post(json=condition, url=login_url,
                                    headers=login_header)
            else:
                times = 6
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)

        res_headers = res.headers
        cookie = res.headers['Set-Cookie']

        # 储存cookie
        session_index = cookie.find('JSESSIONID')
        route_index = cookie.find('route')
        if self.TEST == False:
            self.session['Cookie'] = cookie[session_index:session_index +
                                            43] + '; ' + cookie[route_index:route_index + 38]
        else:
            self.headers['Cookie'] = cookie[session_index:session_index +
                                            43] + '; ' + cookie[route_index:route_index + 38]

        res = res.json()

        # 接口返回的信息
        logs['response'] = res

        # 记录接口返回的headers
        try:
            logs['response']['headers'] = res_headers
        except Exception as ex:
            pass

        if res.get('result') == '0':

            # 储存坐席登入信息
            if self.TEST == False:
                self.session['agentInfo'] = condition
            else:
                self.agentInfo = condition

            logs['message'] = '签入成功'

            self.mqtt_start()

            return {
                'code': 200,
                'msg': '签入成功！',
                'logs': logs
            }
        else:

            logs['message'] = '签入失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs
            }

    # 呼出接口
    def callout(self, condition):

        # 日志
        logs = {
            'method': '呼出'
        }

        # 获取当前headers
        headers = self.headers.copy()
        # 获取当前session
        session = self.session.copy()

        if self.TEST == False:
            headers['Cookie'] = session['Cookie']

        call_data = {
            # 被叫号码
            'called_no': condition.get('calledDigits'),
            # 呼出坐席
            'agent_id': condition.get('agentId'),
            # 主叫号码
            'caller_no': condition.get('callerDigits'),
            # 呼入类型
            'call_type': '呼出',
            # 是否接通
            'pickup': False,
            # 任务识别号
            # 'dsn': res['callId']['dsn'],
            # 是否紧急呼叫
            'emergency': 'no',
            # 其他数据
            'user_id': get_user_id_or_false(self.session),
            'bill_status': 'valid',
            'modify_date': datetime.datetime.now(),
            'version': 1,
            'GUID': str(uuid.uuid1()),
            'valid_bill_id': str(uuid.uuid1()),
            'bill_operator': get_user_id_or_false(self.session),
        }

        record_data = get_info(call_data, self.session)

        # 先插入通话记录
        def insertACall():
            def process_func_insert_record(db):
                insert_data(db, self.call_center_db, record_data)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func_insert_record, self.db_user, self.db_pwd)

        threadCall = Thread(target=insertACall, args=())
        threadCall.start()

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【呼出】接口
        res = requests.post(json=condition, url=callout_url,
                            headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = condition
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            # 储存通话的信息
            if self.TEST == False:
                self.session['callId'] = res['callId']
            else:
                self.callId = res['callId']

            # 储存dns任务，标识已插入
            self.dsnCache[res['callId']['dsn']] = record_data['id']

            # 更新任务识别号
            def updateADsn():
                # 延迟5秒
                time.sleep(5)

                def process_func_update(db):
                    update_data(db, self.call_center_db, {
                        'dsn': res['callId']['dsn']
                    }, {
                        'id': record_data['id'],
                        'bill_status': 'valid',
                    })
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func_update, self.db_user, self.db_pwd)

            threadDsn = Thread(target=updateADsn, args=())
            threadDsn.start()

            logs['message'] = '呼出成功'

            return {
                'code': 200,
                'msg': '呼出成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '呼出失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 挂断接口
    def hangup(self):

        # 日志
        logs = {
            'method': '挂断'
        }

        callId, headers = self.methodByIsTest('getCallId')

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【挂断】接口
        res = requests.post(
            json={'callId': callId}, url=hangup_url, headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = {'callId': callId}
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            logs['message'] = '挂断成功'

            return {
                'code': 200,
                'msg': '挂断成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '挂断失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 保持接口
    def holdon(self):

        # 日志
        logs = {
            'method': '保持'
        }

        callId, headers = self.methodByIsTest('getCallId')

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【保持】接口
        res = requests.post(
            json={'callId': callId}, url=holdon_url, headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = {'callId': callId}
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            logs['message'] = '保持成功'

            return {
                'code': 200,
                'msg': '保持成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '保持失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 取消保持接口
    def unholdon(self):

        # 日志
        logs = {
            'method': '取消保持'
        }

        callId, headers = self.methodByIsTest('getCallId')

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【取消保持】接口
        res = requests.post(
            json={'callId': callId}, url=unholdon_url, headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = {'callId': callId}
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            logs['message'] = '取消保持成功'

            return {
                'code': 200,
                'msg': '取消保持成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '取消保持失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 转接接口
    def transout(self, condition):

        # 日志
        logs = {
            'method': '转接'
        }

        # 获取当前headers
        headers = self.headers.copy()
        # 获取当前session
        session = self.session.copy()

        # 补全数据
        if self.TEST == False:
            condition['callId'] = session['callId']
        else:
            condition['callId'] = self.callId

        if self.TEST == False:
            headers['Cookie'] = session['Cookie']

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【转接】接口
        res = requests.post(
            json=condition, url=transout_url, headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = condition
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            logs['message'] = '转接成功'

            return {
                'code': 200,
                'msg': '转接成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '转接失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 签出接口
    def logout(self):

        # 日志
        logs = {
            'method': '签出'
        }

        # 获取当前headers
        headers = self.headers.copy()
        # 获取当前session
        session = self.session.copy()

        if self.TEST == False and 'Cookie' in session:
            headers['Cookie'] = session['Cookie']

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 调用移动【签出】接口
        res = requests.post(json={}, url=logout_url,
                            headers=headers).json()
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = {}
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        if res.get('result') == '0':

            # 由于下面会清空，所以先储存一下坐席ID以便保存
            logs['agent_id'] = self.methodByIsTest(
                'getAgentInfo', session).get('agentId')

            # 签出清空Cookie
            # 签出清空坐席登入信息
            try:
                if self.TEST == False:
                    del(self.session['Cookie'])
                    del(self.session['agentInfo'])
                else:
                    del(self.headers['Cookie'])
                    self.agentInfo = {}
            except Exception as ex:
                pass

            logs['message'] = '签出成功'

            # self.mqtt_shutdown()

            return {
                'code': 200,
                'msg': '签出成功！',
                'logs': logs,
            }
        else:

            logs['message'] = '签出失败'

            return {
                'code': 500,
                'msg': res.get('resultMsg'),
                'logs': logs,
            }

    # 轮询接口
    def getpool(self, condition):

        # 日志
        logs = {
            'method': '轮询'
        }

        # 获取当前headers
        headers = self.headers.copy()
        # 获取当前session
        session = self.session.copy()

        log_data = {}

        if self.TEST == False:
            if 'Cookie' not in session:
                return {
                    'code': 502,
                    'msg': '无Cookie',
                }
            headers['Cookie'] = session['Cookie']
        elif self.TEST == True:
            if 'Cookie' not in headers:
                return {
                    'code': 502,
                    'msg': '无Cookie',
                }

        # 移动接口发起时间节点
        logs['yd_api_begin'] = round(time.time(), 2)
        # 头信息
        logs['headers'] = headers
        # 接口入参
        logs['condition'] = {}
        try:
            # 设置连接活跃状态为False
            s = requests.session()
            s.keep_alive = False
            # 调用移动【轮询】接口
            res = requests.post(json={}, url=poll_url,
                                headers=headers, timeout=30).json()
        except Exception as ex:
            try:
                # 报错
                logs['response'] = str(ex)
            except Exception as e:
                pass
            return {
                'code': 200,
                'pool_info': {
                    'result': -1,
                    'events': [],
                },
                'warn_info': {},
                'logs': logs,
            }
        # 移动接口完成时间节点
        logs['yd_api_finish'] = round(time.time(), 2)
        # 移动接口完成时间
        logs['yd_api_times'] = round(
            logs['yd_api_finish'] - logs['yd_api_begin'], 2)
        # 接口返回的信息
        logs['response'] = res

        # 获取当前登录的坐席信息
        agent_info = self.methodByIsTest('getAgentInfo', session)

        # 获取当前的信息
        user_id = get_current_user_id(self.session)
        organization_id = get_current_organization_id(self.session)
        now_date = datetime.datetime.now()

        if res.get('result') == '0':
            logs['message'] = '轮询成功'
            if res.get('events') and type(res.get('events')) == list:
                # 表示有事件
                if len(res.get('events')) > 0:
                    logs['has_events'] = True
                update_datas = {}
                for item in res.get('events'):

                    # 300代表有通话
                    if item.get('eventId') == 300:

                        dsn = item.get('callId').get('dsn')

                        # 呼入的300马上插一条数据
                        if item.get('callDirection') == '1':

                            record_data = {
                                # 任务号
                                'dsn': dsn,
                                # 是否接通，默认否
                                'pickup': False,
                                # 用户ID
                                'user_id': user_id,
                                # 是否紧急呼叫
                                'emergency': 'no',
                                # 坐席工号，获取当前的坐席工号，306事件可再更新
                                'agent_id': agent_info.get('agentId'),
                                # 标识300事件时间
                                'e_300_dates': now_date,
                                'modify_date': now_date,
                                'create_date': now_date,
                                # 呼叫号码
                                'caller_no': item.get('callerAddress'),
                                # 被叫号码
                                'called_no': item.get('calledAddress'),
                                # 呼叫方向
                                'call_type': '呼入',
                                # 其他数据
                                'id': str(uuid.uuid1()),
                                'GUID': str(uuid.uuid1()),
                                'valid_bill_id': str(uuid.uuid1()),
                                'version': 1,
                                'bill_status': 'valid',
                                'bill_operator': user_id,
                                'organization_id': organization_id,
                            }

                            def process_func_insert_record(db):
                                insert_data(
                                    db, self.call_center_db, record_data)
                            process_db(self.db_addr, self.db_port, self.db_name,
                                       process_func_insert_record, self.db_user, self.db_pwd)

                            # 呼入的时候发送一个mqtt
                            def pushAMqtt(user_id, organization_id):

                                callerNumber = item.get('callerAddress')

                                # 首先从设备表查询这个号码
                                # 获取设备信息
                                device_result = self.mg_hardware_service.get_all_hardware_position(
                                    callerNumber)

                                if device_result.get('device_name') != '非绑定设备呼入':
                                    push_data = {
                                        'lat': device_result.get('device_lat'),
                                        'lng': device_result.get('device_lng'),
                                        'address': device_result.get('device_address'),
                                        'elder_id': device_result.get('user_info').get('id'),
                                        'elder_name': device_result.get('user_info').get('name'),
                                        'device_sim': callerNumber,
                                        'emergency_call': device_result.get('user_info').get('emergency_call'),
                                        'id': str(uuid.uuid1()),
                                        'GUID': str(uuid.uuid1()),
                                        'valid_bill_id': str(uuid.uuid1()),
                                        'modify_date': now_date,
                                        'create_date': now_date,
                                        'version': 1,
                                        'bill_status': 'valid',
                                        'bill_operator': user_id,
                                        'organization_id': organization_id,
                                    }

                                    def process_insert_a_push(db):
                                        insert_data(
                                            db, self.call_center_push, push_data)

                                        self.mqtt_publish(
                                            'web/bs/sos', json.dumps({'sos_uuid': push_data['id']}))

                                    process_db(self.db_addr, self.db_port, self.db_name,
                                               process_insert_a_push, self.db_user, self.db_pwd)

                            threadRecord = Thread(
                                target=pushAMqtt, args=(user_id, organization_id,))
                            threadRecord.start()

                        else:
                            # 更新通话的300时间
                            def updateA300():
                                # 延迟5秒
                                time.sleep(5)

                                def process_update_300_dates(db):
                                    update_data(db, self.call_center_db, {
                                        'e_300_dates': now_date
                                    }, {
                                        'dsn': dsn,
                                        'bill_status': 'valid'
                                    })
                                process_db(self.db_addr, self.db_port, self.db_name,
                                           process_update_300_dates, self.db_user, self.db_pwd)

                            threadRecord = Thread(
                                target=updateA300, args=())
                            threadRecord.start()

                        # 储存callId信息
                        if self.TEST == False:
                            self.session['callId'] = item.get('callId')
                        else:
                            self.callId = item.get('callId')
                        self._print('通话接通')

                    # 据说301代表挂断
                    elif item.get('eventId') == 301:

                        dsn = item.get('callId').get('dsn')

                        if dsn not in update_datas:
                            update_datas[dsn] = {}

                        update_datas[dsn]['e_301_dates'] = now_date

                    # 304通话状态变化事件
                    elif item.get('eventId') == 304:

                        dsn = item.get('callId').get('dsn')

                        if dsn not in update_datas:
                            update_datas[dsn] = {}

                        if item.get('callDirection') == 1:
                            update_datas[dsn]['call_type'] = '呼入'
                        elif item.get('callDirection') == 2:
                            update_datas[dsn]['call_type'] = '呼出'

                        # 被叫号码
                        update_datas[dsn]['called_no'] = item.get(
                            'calledAddress')
                        # 主叫号码
                        update_datas[dsn]['caller_no'] = item.get(
                            'callerAddress')
                        # 原始呼叫号码吧
                        update_datas[dsn]['orgicallednum'] = item.get(
                            'orgicallednum')

                        update_datas[dsn]['e_304_dates'] = now_date

                    # 里面包含了接触记录信息、录音开始
                    elif item.get('eventId') == 305:

                        dsn = item.get('callId').get('dsn')

                        if dsn not in update_datas:
                            update_datas[dsn] = {}

                        # 有录音再代表已接通
                        update_datas[dsn]['pickup'] = True
                        # 录音文件
                        update_datas[dsn]['record_file_name'] = item.get(
                            'recordFileName')
                        # 录音文件服务器标识
                        update_datas[dsn]['locationId'] = item.get(
                            'locationId')

                        update_datas[dsn]['e_305_dates'] = now_date

                    # 录音停止事件，补全坐席工号，录音开始，结束时间
                    elif item.get('eventId') == 306:

                        dsn = item.get('callId').get('dsn')

                        if dsn not in update_datas:
                            update_datas[dsn] = {}

                        # 通话坐席工号
                        update_datas[dsn]['agent_id'] = item.get('agentId')
                        # 通话开始时间
                        update_datas[dsn]['begin_time'] = datetime.datetime.strptime(
                            item.get('beginTime'), '%Y-%m-%d %H:%M:%S.%f')
                        # 通话结束时间
                        update_datas[dsn]['end_time'] = datetime.datetime.strptime(
                            item.get('endTime'), '%Y-%m-%d %H:%M:%S.%f')

                        update_datas[dsn]['e_306_dates'] = now_date

                    # 351代表坐席状态变更
                    elif item.get('eventId') == 351:
                        self._print('坐席变更状态')
                    # 352代表强制签出
                    elif item.get('eventId') == 352:
                        self._print('坐席被强制签出')
                    # 357代表通话事件记录，0振铃有dsn，1摘机，2坐席挂断，3用户挂断
                    elif item.get('eventId') == 357:
                        # 因为357事件只有0有dsn，其他事件暂时从储存域获取
                        if item.get('phoneState') in [2, 3]:
                            dsn = ''
                            if self.TEST == False:
                                if self.session['callId'] and self.session['callId'].get('dsn'):
                                    dsn = self.session['callId'].get('dsn')
                            else:
                                if self.callId and self.callId.get('dsn'):
                                    dsn = self.callId.get('dsn')

                            if dsn != '':
                                if dsn not in update_datas:
                                    update_datas[dsn] = {}
                                if item.get('phoneState') == 2:
                                    update_datas[dsn]['cut_type'] = '座席挂断'
                                elif item.get('phoneState') == 3:
                                    update_datas[dsn]['cut_type'] = '用户挂断'

                def updateMEvents():
                    # 延迟10秒
                    time.sleep(10)
                    for item in update_datas:
                        # 以主键id作为更新主键
                        def process_update_events(db):
                            update_data(db, self.call_center_db, update_datas[item], {
                                'dsn': item,
                                'bill_status': 'valid'
                            })
                        process_db(self.db_addr, self.db_port, self.db_name,
                                   process_update_events, self.db_user, self.db_pwd)

                threadUpdate = Thread(
                    target=updateMEvents, args=())
                threadUpdate.start()
        else:
            logs['message'] = '轮询失败'

        # self._print({'getpool>>>': res})

        # 爱关怀查询发起时间节点
        logs['agh_api_begin'] = round(time.time(), 2)

        warn_info = {}

        if 'pause_agh' in condition and condition['pause_agh'] == False:

            # 爱关怀的数据
            warn_info_result = self.get_agh_list(N(), {}, 1, 1)

            if len(warn_info_result['result']) > 0:
                warn_info = warn_info_result['result'][0]

                # 地址转换为坐标系
                if warn_info.get('user_info').get('personnel_info').get('address'):

                    address = warn_info.get('user_info').get(
                        'personnel_info').get('address')

                    # 百度地图查询发起时间节点
                    logs['map_api_begin'] = round(time.time(), 2)

                    if address in self.locationCache:
                        location = self.locationCache[address]
                    else:
                        self._print('从百度API获取定位')
                        location = requests.post(json={}, url='http://api.map.baidu.com/geocoder/v2/?address=' + address + '&output=json&ak=jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji',
                                                 headers={}).json()
                        self.locationCache[address] = location

                    # 百度地图查询完成时间节点
                    logs['map_api_finish'] = round(time.time(), 2)
                    # 百度地图查询完成时间
                    logs['map_api_times'] = round(
                        logs['map_api_finish'] - logs['map_api_begin'], 2)

                    warn_info['user_info']['device_lat'] = location.get(
                        'result').get('location').get('lat')
                    warn_info['user_info']['device_lng'] = location.get(
                        'result').get('location').get('lng')
                    warn_info['user_info']['device_name'] = address
                    warn_info['user_info']['device_address'] = address

        # 爱关怀查询完成时间节点
        logs['agh_api_finish'] = round(time.time(), 2)
        # 爱关怀查询完成时间
        logs['agh_api_times'] = round(
            logs['agh_api_finish'] - logs['agh_api_begin'], 2)
        # 记录整个接口查询时间
        logs['all_api_time'] = round(
            logs['agh_api_finish'] - logs['yd_api_begin'], 2)

        return {
            'code': 200,
            'pool_info': res,
            'warn_info': warn_info,
            'logs': logs,
        }

    # 改变坐席状态
    def changestate(self, state):
        res = None

        # 获取当前headers
        headers = self.headers.copy()
        # 获取当前session
        session = self.session.copy()

        if state == 'rest':

            logs = {
                'method': '示闲'
            }

            if self.TEST == False:
                headers['Cookie'] = session['Cookie']

            # 移动接口发起时间节点
            logs['yd_api_begin'] = round(time.time(), 2)
            # 调用移动【示闲】接口
            res = requests.post(
                json={'state': '4'}, url=changestate_url, headers=headers).json()
            # 头信息
            logs['headers'] = headers
            # 接口入参
            logs['condition'] = {'state': '4'}
            # 移动接口完成时间节点
            logs['yd_api_finish'] = round(time.time(), 2)
            # 移动接口完成时间
            logs['yd_api_times'] = round(
                logs['yd_api_finish'] - logs['yd_api_begin'], 2)
            # 接口返回的信息
            logs['response'] = res

            if res.get('result') == '0':

                logs['message'] = '示闲成功'

                return {
                    'code': 200,
                    'msg': '示闲成功！',
                    'logs': logs,
                }
            else:

                logs['message'] = '示闲失败'

                return {
                    'code': 500,
                    'msg': res.get('resultMsg'),
                    'logs': logs,
                }
        if state == 'busy':

            logs = {
                'method': '示忙'
            }

            if self.TEST == False:
                headers['Cookie'] = session['Cookie']

            # 移动接口发起时间节点
            logs['yd_api_begin'] = round(time.time(), 2)
            # 调用移动【示忙】接口
            res = requests.post(
                json={'state': '3'}, url=changestate_url, headers=headers).json()
            # 头信息
            logs['headers'] = headers
            # 接口入参
            logs['condition'] = {'state': '3'}
            # 移动接口完成时间节点
            logs['yd_api_finish'] = round(time.time(), 2)
            # 移动接口完成时间
            logs['yd_api_times'] = round(
                logs['yd_api_finish'] - logs['yd_api_begin'], 2)
            # 接口返回的信息
            logs['response'] = res

            if res.get('result') == '0':

                logs['message'] = '示忙成功'

                return {
                    'code': 200,
                    'msg': '示忙成功！',
                    'logs': logs,
                }
            else:

                logs['message'] = '示忙失败'

                return {
                    'code': 500,
                    'msg': res.get('resultMsg'),
                    'logs': logs,
                }

    # 获取录音地址
    def get_record_url(self, condition):

        record_list = condition['pathname'].split('/')

        locationId = condition.get('locationId')

        if not locationId:
            locationId = '25001'

        record_encry = requests.post(
            json={
                'beans': [],
                'params': {
                    'locationId': locationId,
                    'recordFileName': condition['pathname'],
                    'tenantId': record_list[len(record_list) - 1].split('_')[0],
                    'clientIp': self.request.remote_addr
                }
            }, url='https://clportal.deskpro.cn/ucpzb-media').json()
        return {
            'code': 200,
            'msg': 'https://clportal.deskpro.cn/ucppaas-media?encryString=' + record_encry.get('bean').get('encryString'),
        }

    def aaatest(self, condition):
        self.mqtt_start()

        import random

        number = random.randint(1, 6)

        if number == 1:
            lng = '113.14958'
            lat = '23.035191'
            device_name = '咪狗'
            device_address = '广东省佛山市禅城区人民政府'
            device_sim = '13113113111'
            user_info = {
                'id': 'zhangsan',
                'name': '张三',
                'emergency_call': '13333333333'
            }
        elif number == 2:
            lng = '113.118305'
            lat = '23.032503'
            device_name = '云芯'
            device_address = '广东省佛山市南海区人民政府'
            device_sim = '13413413444'
            user_info = {
                'id': 'lisi',
                'name': '李四',
                'emergency_call': '14444444444'
            }
        elif number == 3:
            lng = '112.903949'
            lat = '23.162863'
            device_name = '柏颐'
            device_address = '广东省佛山市三水区人民政府'
            device_sim = '13513513555'
            user_info = {
                'id': 'wangwu',
                'name': '王五',
                'emergency_call': '15555555555'
            }
        elif number == 4:
            lng = '112.898909'
            lat = '22.906059'
            device_name = '柏颐'
            device_address = '广东省佛山市高明区人民政府'
            device_sim = '13636363636'
            user_info = {
                'id': 'liuliu',
                'name': '刘六',
                'emergency_call': '13636363636'
            }
        elif number == 5:
            lng = '113.270766'
            lat = '23.135618'
            device_name = '柏颐'
            device_address = '广东省广州市人民政府'
            device_sim = '13737373737'
            user_info = {
                'id': 'tianqi',
                'name': '田七',
                'emergency_call': '13737373737'
            }
        elif number == 6:
            lng = '113.29971'
            lat = '22.811512'
            device_name = '柏颐'
            device_address = '广东省佛山市顺德区政府'
            device_sim = '13838383838'
            user_info = {
                'id': 'wangba',
                'name': '王八',
                'emergency_call': '13838383838'
            }

        lat = round(float(lat) + round(random.uniform(1, 10) / 1000, 6), 6)
        lng = round(float(lng) + round(random.uniform(1, 10) / 1000, 6), 6)
        device_address = device_address + device_address + device_address

        device_result = {
            'device_lat': str(lat),
            'device_lng': str(lng),
            'device_name': device_name,
            'device_address': device_address,
            'user_info': user_info
        }

        if device_result.get('device_name') != '非绑定设备呼入':
            record = {
                'lat': device_result.get('device_lat'),
                'lng': device_result.get('device_lng'),
                'address': device_result.get('device_address'),
                'elder_id': device_result.get('user_info').get('id'),
                'elder_name': device_result.get('user_info').get('name'),
                'device_sim': device_sim,
                'emergency_call': device_result.get('user_info').get('emergency_call'),
            }

            # 单据固定数据
            record['id'] = str(uuid.uuid1())
            record['GUID'] = str(uuid.uuid1())
            record['modify_date'] = datetime.datetime.now(
            )
            record['create_date'] = datetime.datetime.now(
            )
            record['version'] = 1
            record['bill_status'] = 'valid'
            record['valid_bill_id'] = str(
                uuid.uuid1())

            try:
                record['bill_operator'] = get_current_user_id(self.session)
                record['organization_id'] = get_current_organization_id(
                    self.session)
            except Exception as ex:
                pass

        def process_func_insert_push(db):
            insert_data(
                db, self.call_center_push, record)

            self.mqtt_publish(
                'web/bs/sos', json.dumps({'sos_uuid': record['id']}))

        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func_insert_push, self.db_user, self.db_pwd)

        return

    # 获取录音地址
    def get_record_list(self, org_list, condition, page, count):

        keys = ['id', 'caller_no', 'called_no',
                'call_type', 'agent_id', 'pickup', 'remark', 'start_date', 'end_date']
        if 'pickup' in condition:
            if condition['pickup'] == 'true':
                condition['pickup'] = True
            elif condition['pickup'] == 'false':
                condition['pickup'] = False
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('organization_id').inner(org_list))
                           & (C('call_type') == values['call_type'])
                           & (C('agent_id') == values['agent_id'])
                           & (C('pickup') == values['pickup'])
                           #    & (C('dsn') != None)
                           & (C('called_no').like(values['called_no']))
                           & (C('caller_no').like(values['caller_no']))
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('remark').like(values['remark'])))

        # 是否备注
        if 'is_remark' in condition:
            if condition.get('is_remark') == 'true':
                _filter.match_bill((C('remark') != None))
            elif condition.get('is_remark') == 'false':
                _filter.match_bill((C('remark') == None))

        _filter.sort({'modify_date': -1})\
            .project({**get_common_project(), 'user_info': 0, 'call_info': 0})
        res = self.page_query(_filter, self.call_center_db, page, count)

        if len(res['result']) > 0:
            worker_ids = []
            elder_ids = []
            elder_telephones = []
            device_id_tel = {}
            user_obj = {}
            for item in res['result']:
                if 'user_id' in item and item['user_id'] != '':
                    if item['user_id'] not in worker_ids:
                        worker_ids.append(item['user_id'])
                if 'called_no' in item and item['called_no'] != '' and item['called_no'] != '075766886188':
                    if item['called_no'] not in elder_telephones:
                        elder_telephones.append(item['called_no'])
                elif 'caller_no' in item and item['caller_no'] != '' and item['caller_no'] != '075766886188':
                    if item['caller_no'] not in elder_telephones:
                        elder_telephones.append(item['caller_no'])

            # 首先从设备表查询这个号码
            _filter_device = MongoBillFilter()
            _filter_device.match_bill((C('device_sim').inner(elder_telephones)))\
                .project({
                    '_id': 0,
                    'elder_id': 1,
                    'device_sim': 1,
                })
            res_device = self.query(_filter_device, 'PT_Device')

            if len(res_device) > 0:
                for item in res_device:
                    if item['elder_id'] not in elder_ids:
                        device_id_tel[item['elder_id']
                                      ] = item['device_sim']
                        elder_ids.append(item['elder_id'])

            # 也从长者表查询这个号码
            _filter_user = MongoBillFilter()
            _filter_user.match_bill(
                (C('id').inner(worker_ids))
                | (C('id').inner(elder_ids))
                | (C('personnel_info.telephone').inner(elder_telephones)))\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'personnel_info.telephone': 1,
                })
            res_user = self.query(_filter_user, 'PT_User')

            if len(res_user) > 0:
                for item in res_user:
                    user_obj[item['id']] = item['name']
                    if item['personnel_info']['telephone'] != '':
                        user_obj[item['personnel_info']
                                 ['telephone']] = item['name']
                    if item['id'] in device_id_tel:
                        user_obj[device_id_tel[item['id']]] = item['name']

            for item in res['result']:
                if 'called_no' in item and item['called_no'] in user_obj:
                    item['elder_name'] = user_obj[item['called_no']]
                elif 'caller_no' in item and item['caller_no'] in user_obj:
                    item['elder_name'] = user_obj[item['caller_no']]

                if 'user_id' in item and item['user_id'] in user_obj:
                    item['cs_name'] = user_obj[item['user_id']]

                item['relations'] = ''
                if item['called_no'] != '075766886188':
                    item['relations'] = self.get_telephone_relations(
                        item['called_no'])
                if item['relations'] == '' and item['caller_no'] != '075766886188':
                    item['relations'] = self.get_telephone_relations(
                        item['caller_no'])

        return res

    # 更新录音
    def update_record(self, condition):
        if 'id' in condition:
            def process_update_reocrd(db):
                update_data(db, self.call_center_db, condition, {
                    'id': condition['id'],
                    'bill_status': 'valid',
                })
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_update_reocrd, self.db_user, self.db_pwd)

            return {
                'code': 200,
                'msg': 'Success',
            }
        return {
            'code': 500,
            'msg': 'Failed'
        }

    # 获取机构坐标
    def get_location(self, condition):

        organization_id = get_current_organization_id(self.session)

        if organization_id in self.orgLocationCache:
            # 先从缓存找
            res = self.orgLocationCache[organization_id]
        else:
            # 没有再去查
            _filter_org = MongoBillFilter()
            _filter_org.match_bill(C('id') == organization_id).project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'organization_info': 1,
            })
            res = self.query(_filter_org, 'PT_User')
        if len(res) > 0:
            return {
                'code': 200,
                'result': {
                    'name': res[0]['name'],
                    'address': res[0]['organization_info']['address'],
                    'lng': res[0]['organization_info']['lon'],
                    'lat': res[0]['organization_info']['lat']
                }
            }
        return {
            'code': 500,
            'msg': "获取失败！"
        }

    # 根据dsn任务id获取手机号码
    def get_phone(self, condition):
        keys = ['dsn']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('dsn') == values['dsn']))\
            .project({'_id': 0, 'id': 1, 'dsn': 1, 'called_no': 1})
        res = self.bill_manage_service.query(_filter, self.call_center_db)

        if len(res) > 0:
            return {
                'code': 200,
                'msg': res[0]['called_no']
            }
        else:
            return {
                'code': 500,
                'msg': "获取失败！"
            }

    # 根据手机号码获取家属
    def get_telephone_relations(self, telephone):
        relations = ''

        # 先从缓存池看有没有
        if telephone in self.relationsCache:
            return self.relationsCache[telephone]

        try:
            _filter_family = MongoBillFilter()
            _filter_family.match_bill(
                C('personnel_info.family_name.contents').like(telephone))\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'personnel_info.family_name': 1,
                })
            family_list = self.bill_manage_service.query(
                _filter_family, 'PT_User')

            if len(family_list) > 0:
                relations = []
                for item in family_list:
                    if type(item['personnel_info']['family_name']) == list and len(item['personnel_info']['family_name']) > 0:
                        for itm in item['personnel_info']['family_name']:
                            if itm['contents'] == telephone:
                                if 'relation' in itm and itm['relation'] != '':
                                    relations.append(
                                        item['name'] + '（' + itm['relation'] + '）')
                                elif 'title' in itm and itm['title'] != '':
                                    relations.append(
                                        item['name'] + '（' + itm['title'] + '）')
                relations = '，'.join(relations)
                # 存进缓存池
                self.relationsCache[telephone] = relations
        except Exception as ex:
            pass
        return relations

    #  根据手机号码获取用户和设备信息
    def get_user(self, condition):

        # 日志
        logs = {
            'method': '获取用户'
        }

        result = {}
        device_result = {}
        user_result = {}

        # 获取设备发起时间节点
        logs['device_api_begin'] = round(time.time(), 2)
        # 获取设备信息
        device_result = self.mg_hardware_service.get_all_hardware_position(
            condition['telephone'])
        # 获取设备完成时间节点
        logs['device_api_finish'] = round(time.time(), 2)
        # 获取设备完成时间
        logs['device_api_times'] = round(
            logs['device_api_finish'] - logs['device_api_begin'], 2)

        if device_result['user_info']:
            user_result = device_result['user_info']
            del(device_result['user_info'])
            # 设备找到
            logs['message'] = '设备'
        else:
            if condition['telephone'] in self.userCache:
                # 先从缓存找
                user_list = self.userCache[condition['telephone']]
                # 缓存找到
                logs['message'] = '缓存'
            else:
                # 没有再去查
                _filter_user = MongoBillFilter()
                _filter_user.match_bill(
                    (C('personnel_info.telephone') == condition['telephone']))\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'name': 1,
                        'id_card': 1,
                        'id_card_type': 1,
                        'organization_id': 1,
                        'personnel_info.sex': 1,
                        'personnel_info.telephone': 1,
                        'personnel_info.address': 1,
                        'personnel_info.id_card': 1,
                        'personnel_info.name': 1,
                        'personnel_info.date_birth': 1,
                        'personnel_info.picture': 1,
                    })
                user_list = self.bill_manage_service.query(
                    _filter_user, 'PT_User')

                self.userCache[condition['telephone']] = user_list
                # 数据库找到
                logs['message'] = '数据库'

            if len(user_list) > 0:
                user_result = user_list[0]
                logs['response'] = user_list[0]
                logs['message'] = logs['message'] + '|成功'
            else:
                logs['message'] = logs['message'] + '|失败'

        # 找家属
        relations = self.get_telephone_relations(condition['telephone'])

        result = {**device_result, **user_result, 'relations': relations}

        if result['device_name'] in ['咪狗']:
            result['is_emergency'] = True
            logs['message'] = logs['message'] + '|紧急'

            if 'dsn' in condition and condition['dsn'] != '':
                def updateAEmergency():
                    # 查询这个dsn在数据库有没有
                    _filter_dsn = MongoBillFilter()
                    _filter_dsn = _filter_dsn.match_bill(
                        (C('dsn') == condition['dsn'])).project({'_id': 0, 'id': 1, 'dsn': 1, 'emergency': 1})
                    res_dsn = self.query(
                        _filter_dsn, self.call_center_db)

                    # 更新紧急呼叫状态
                    if len(res_dsn) > 0 and res_dsn[0]['emergency'] != 'yes':
                        def process_update_emergency(db):
                            update_data(db, self.call_center_db, {
                                'emergency': 'yes'
                            }, {
                                'id': res_dsn[0]['id'],
                                'bill_status': 'valid',
                            })
                        process_db(self.db_addr, self.db_port, self.db_name,
                                   process_update_emergency, self.db_user, self.db_pwd)

                threadEmergency = Thread(target=updateAEmergency, args=())
                threadEmergency.start()
        else:
            logs['message'] = logs['message'] + '|非紧急'
        return {
            'code': 200,
            'result': result,
            'logs': logs,
        }

    # 根据当前登录的帐号获取帐号可能绑定的呼叫中心帐号
    def get_zh(self, condition):
        condition = {
            'user_id': get_current_user_id(self.session)
        }
        result = self.get_call_center_zh_list(N(), condition, None, None)

        if len(result['result']) > 0:
            return {
                'code': 200,
                'result': result['result'][0]
            }
        else:
            return {
                'code': 500,
                'msg': '没有内置帐号信息'
            }

    # 获取移动呼叫中心帐号列表
    def get_call_center_zh_list(self, org_list, condition, page, count):
        '''移动呼叫中心帐号'''
        keys = ['name', 'agentId', 'id',
                'phoneNum', 'remark', 'user_id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('user_id') == values['user_id'])
            & (C('agentId').like(values['agentId']))
            & (C('phoneNum').like(values['phoneNum']))
            & (C('remark').like(values['remark']))
            & (C('organization_id').inner(org_list)))\
            .lookup('PT_User', 'user_id', 'id', 'user_info')\
            .add_fields({
                "name": self.ao.array_elemat("$user_info.name", 0),
            })\
            .match_bill((C('name').like(values['name'])))\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'user_id': 1,
                'agentId': 1,
                'skillIds': 1,
                'password': 1,
                'ccId': 1,
                'agentState': 1,
                'phoneNum': 1,
                'disPlayNumber': 1,
                'remark': 1,
                'modify_date': 1,
                'create_date': 1,
            })

        res = self.page_query(_filter, 'PT_Call_Center_Zh', page, count)

        return res

    # 更新移动呼叫中心帐号
    def update_call_center_zh(self, condition):
        '''更新移动呼叫中心帐号'''
        table = 'PT_Call_Center_Zh'

        if 'id' in condition:
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.Income.value, condition, table)
        else:
            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('user_id') == condition['user_id']))\
                .project({'_id': 0})

            is_exists = self.query(_filter, table)

            # 已经有这个帐号了
            if len(is_exists) > 0:
                return {
                    'code': 302,
                    'msg': is_exists[0]['id'],
                }

            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.Income.value, condition, table)
        if bill_id:
            return {
                'code': 200,
                'msg': 'Success',
            }
        else:
            return {
                'code': 500,
                'msg': 'Fail',
            }

    # 删除移动呼叫中心帐号
    def delete_call_center_zh(self, condition):
        '''删除移动呼叫中心帐号'''
        ids = []
        if isinstance(condition, str):
            ids.append({"id": condition})
        else:
            for id_str in condition:
                ids.append({"id": id_str})
        self.bill_manage_service.add_bill(OperationType.delete.value,
                                          TypeId.chargeType.value, ids, 'PT_Call_Center_Zh')
        res = 'Success'
        return res

    # 优化后的爱关怀警报列表
    def get_agh_pure_list(self, org_list, condition, page, count):
        keys = ['id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('check_status') == '待处理')
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_My_Room_Device', 'device_id', 'device_id', 'device_info')\
            .match((C("device_info") != None) & (C("device_info") != []))\
            .add_fields({
                "user_id": self.ao.array_elemat("$device_info.user_id", 0),
                "room_id": self.ao.array_elemat("$device_info.room_id", 0),
            })\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'time': 1,
                'level': 1,
                'value': 1,
                'user_id': 1,
                'room_id': 1,
                'device_id': 1,
                'warn_type': 1,
                'status': 1,
                'check_status': 1,
            })

        res = self.page_query(_filter, 'PT_My_Warn_Info', page, count)

        if len(res['result']) > 0:
            user_ids = []
            room_ids = []
            user_obj = {}
            room_obj = {}

            # 根据设备对象获取用户数组id和房间数组id
            for item in res['result']:
                if 'user_id' in item and item['user_id'] != None and item['user_id'] != '':
                    user_ids.append(item['user_id'])
                if 'room_id' in item and item['room_id'] != None and item['room_id'] != '':
                    room_ids.append(item['room_id'])

            # 根据用户的数组id获取用户数据
            if len(user_ids) > 0:
                _filter_user = MongoBillFilter()
                _filter_user.match_bill((C('id').inner(user_ids)))\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'name': 1,
                        'personnel_info.address': 1,
                        'personnel_info.telephone': 1,
                    })
                res_user = self.query(
                    _filter_user, 'PT_User')

                for i in res_user:
                    user_obj[i['id']] = i

            # 根据房间的数组id获取房间数据
            if len(room_ids) > 0:
                _filter_room = MongoBillFilter()
                _filter_room.match_bill((C('id').inner(room_ids)))\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'name': 1,
                    })
                res_room = self.query(_filter_room, 'PT_My_Room')

                for i in res_room:
                    room_obj[i['id']] = i

            # 给主数据补全用户数据和房间位置数据
            for item in res['result']:
                if 'user_id' in item and item['user_id'] != None and item['user_id'] != '' and item['user_id'] in user_obj:
                    item['user_info'] = user_obj[item['user_id']]
                if 'room_id' in item and item['room_id'] != None and item['room_id'] != '' and item['room_id'] in room_obj:
                    item['location'] = room_obj[item['room_id']].get(
                        'name')

        return res

    # 获取爱关怀警报列表

    def get_agh_list(self, org_list, condition, page, count):
        return self.get_agh_pure_list(org_list, condition, page, count)
        keys = ['id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('check_status') == '待处理')
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_My_Room_Device', 'device_id', 'device_id', 'device_info')\
            .match((C("device_info") != None) & (C("device_info") != []))\
            .add_fields({
                "user_id": self.ao.array_elemat("$device_info.user_id", 0),
                "room_id": self.ao.array_elemat("$device_info.room_id", 0),
            })\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .match((C("user_info") != None) & (C("user_info") != []))\
            .add_fields({
                "user_info": self.ao.array_elemat("$user_info", 0),
            })\
            .lookup_bill('PT_My_Room', 'room_id', 'id', 'room_info')\
            .add_fields({
                "location": self.ao.array_elemat("$room_info.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({'_id': 0,
                      'user_info._id': 0,
                      'device_info': 0,
                      'room_info': 0,
                      **get_common_project()
                      })

        res = self.page_query(_filter, 'PT_My_Warn_Info', page, count)

        return res

    # 更新爱关怀
    def update_agh(self, condition):
        if 'id' in condition:

            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('id') == condition['id'])
                & (C('check_status') == '待处理'))\
                .project({'_id': 0})

            is_exists = self.query(_filter, 'PT_My_Warn_Info')

            # 已经处理过了
            if len(is_exists) == 0:
                return {
                    'code': 302,
                    'msg': '该警报已处理！',
                }
            else:

                # 需要把同类型的警报全部处理
                _filter = MongoBillFilter()
                _filter.match_bill((C('level') == is_exists[0]['level'])
                                   & (C('device_id') == is_exists[0]['device_id'])
                                   & (C('warn_type') == is_exists[0]['warn_type'])
                                   & (C('check_status') == '待处理'))\
                    .project({'_id': 0})

                all_warn = self.query(_filter, 'PT_My_Warn_Info')
                ids = []
                # 拿出id的数组集
                for item in all_warn:
                    ids.append(item['id'])

                # 添加一个处理时间
                condition['handle_date'] = datetime.datetime.now()
                # 保存主处理的id
                condition['handle_id'] = condition['id']
                # 去掉处理的主id
                del(condition['id'])

                def process_func(db):
                    update_many_data(db, 'PT_My_Warn_Info',
                                     condition, {"id": {'$in': ids}})
                process_db(self.db_addr, self.db_port, self.db_name,
                           process_func, self.db_user, self.db_pwd)

                return {
                    'code': 200,
                    'msg': 'Success',
                }
        return {
            'code': 500,
            'msg': 'Failed'
        }

    # 获取爱关怀警报列表
    def get_device_warning_list(self, org_list, condition, page, count):
        keys = ['id', 'elder_name', 'elder_id_card',
                'device_type', 'device_id', 'host_id', 'start_date', 'end_date']

        if 'create_date' in list(condition.keys()):
            condition['start_date'] = condition['create_date'] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['create_date'] + \
                "T23:59:59.00Z"

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('device_id').like(values['device_id']))
            & (C('time') >= as_date(values['start_date']))
            & (C('time') <= as_date(values['end_date']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_My_Room_Device', 'device_id', 'device_id', 'device_info')\
            .match((C("device_info") != None) & (C("device_info") != []))\
            .add_fields({
                "user_id": self.ao.array_elemat("$device_info.user_id", 0),
                "room_id": self.ao.array_elemat("$device_info.room_id", 0),
            })\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .add_fields({
                "device_type_id": self.ao.array_elemat("$device_info.device_type", 0),
                "host_id": self.ao.array_elemat("$device_info.host_id", 0),
            })\
            .lookup('PT_My_Device_Type', 'device_type_id', 'id', 'type_info')\
            .lookup_bill('PT_My_Room', 'room_id', 'id', 'room_info')\
            .add_fields({
                "elder_name": self.ao.array_elemat("$user_info.name", 0),
                "elder_id_card": self.ao.array_elemat("$user_info.id_card", 0),
                "location": self.ao.array_elemat("$room_info.name", 0),
                "device_type": self.ao.array_elemat("$type_info.name", 0),
            })\
            .match_bill(
                (C('elder_name').like(values['elder_name']))
                & (C('host_id').like(values['host_id']))
                & (C('device_type').like(values['device_type']))
                & (C('elder_id_card').like(values['elder_id_card'])))\
            .sort({'modify_date': -1})\
            .project({'_id': 0,
                      'type_info': 0,
                      'user_info': 0,
                      'device_info': 0,
                      'room_info': 0,
                      **get_common_project()
                      })

        res = self.page_query(_filter, 'PT_My_Warn_Info', page, count)

        return res

    # 获取爱关怀设备列表
    def get_device_manage_list(self, org_list, condition, page, count):
        keys = ['id', 'elder_name', 'elder_id_card', 'host_id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list)))\
            .sort({'modify_date': -1})\
            .group(
                {
                    'user_id': '$user_id',
                })\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .add_fields({
                "elder_name": self.ao.array_elemat("$user_info.name", 0),
                "elder_id_card": self.ao.array_elemat("$user_info.id_card", 0),
                "elder_sex": self.ao.array_elemat("$user_info.personnel_info.sex", 0),
                "elder_date_birth": self.ao.array_elemat("$user_info.personnel_info.date_birth", 0),
                "elder_address": self.ao.array_elemat("$user_info.personnel_info.address", 0),
                "elder_id_card_address": self.ao.array_elemat("$user_info.id_card_address", 0),
                "elder_telephone": self.ao.array_elemat("$user_info.personnel_info.telephone", 0),
            })\
            .match(
            (C('elder_name').like(values['elder_name']))
            & (C('elder_id_card').like(values['elder_id_card'])))\
            .lookup_bill('PT_My_Room_Device', 'user_id', 'user_id', 'room_device_info2')

        if 'host_id' in condition:
            _filter.add_fields({'room_device_info': self.ao.array_filter(
                "$room_device_info2", "rdi", ((F('$rdi.host_id') == values['host_id'])).f)})
        else:
            _filter.add_fields({
                "room_device_info": "$room_device_info2",
            })

        _filter.add_fields({
            "device_count": self.ao.size("$room_device_info"),
        })\
            .match((C("device_count") != 0))\
            .project({'_id': 0,
                      'user_info': 0,
                      'room_device_info._id': 0,
                      'room_device_info2': 0,
                      **get_common_project()
                      })

        res = self.page_query(_filter, 'PT_My_Room_Device', page, count)

        return res

    # 根据长者获取长者的爱关怀设备列表
    def get_edler_device_list(self, org_list, condition, page, count):
        keys = ['id', 'user_id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('user_id') == values['user_id'])
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .lookup_bill('PT_My_Room', 'room_id', 'id', 'room_info')\
            .lookup('PT_My_Device_Type', 'device_type', 'id', 'type_info')\
            .add_fields({
                "elder_name": self.ao.array_elemat("$user_info.name", 0),
                "elder_id_card": self.ao.array_elemat("$user_info.id_card", 0),
                "elder_address": self.ao.array_elemat("$user_info.personnel_info.address", 0),
                "location": self.ao.array_elemat("$room_info.name", 0),
                "elder_telephone": self.ao.array_elemat("$user_info.personnel_info.telephone", 0),
                "device_type": self.ao.array_elemat("$type_info.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({'_id': 0,
                      'type_info': 0,
                      'user_info': 0,
                      'room_info': 0,
                      **get_common_project()
                      })

        res = self.page_query(_filter, 'PT_My_Room_Device', page, count)

        return res

    # 根据设备获取警报列表
    def get_device_warn_list(self, org_list, condition, page, count):
        keys = ['id', 'device_id']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('device_id') == values['device_id'])
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_My_Room', 'room_id', 'id', 'room_info')\
            .lookup('PT_My_Device_Type', 'device_type', 'id', 'type_info')\
            .lookup('PT_My_Warn_Info', 'device_id', 'device_id', 'warn_info')\
            .add_fields({
                "location": self.ao.array_elemat("$room_info.name", 0),
                "device_type": self.ao.array_elemat("$type_info.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'room_info': 0,
                'type_info': 0,
                'warn_info._id': 0,
                **get_common_project()
            })

        res = self.page_query(_filter, 'PT_My_Room_Device', page, count)

        return res

    # 获取呼叫中心日志
    def get_call_center_log_list(self, org_list, condition, page, count):

        # 只有超管能看
        role_id = get_current_role_id(self.session)

        if role_id != '8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe':
            return {
                'result': [],
                'total': 0,
            }

        keys = ['id', 'method', 'agent_id', 'has_events', 'event_id']
        if 'event_id' in condition and type(condition.get('event_id')) == str:
            condition['event_id'] = int(condition['event_id'])

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match(
            (C('id') == values['id'])
            & (C('agent_id') == values['agent_id'])
            & (C('method').like(values['method']))
            & (C('response.events.eventId') == values['event_id'])
        )

        if 'has_events' in condition:
            if condition.get('has_events') == 'true':
                _filter.match((C('has_events').exists(True)))
            elif condition.get('has_events') == 'false':
                _filter.match(C('has_events').exists(False))

        _filter.sort({'create_date': -1})\
            .project({
                '_id': 0,
            })

        res = self.page_query(_filter, self.call_center_log2_db, page, count)

        if len(res['result']) > 0:
            user_ids = []
            user_obj = {}

            for item in res['result']:
                if 'user_id' in item and item['user_id'] != '' and item['user_id'] not in user_ids:
                    user_ids.append(item['user_id'])

            if len(user_ids) > 0:
                _filter_user = MongoBillFilter()
                _filter_user = _filter_user.match_bill(
                    (C('id').inner(user_ids))).project({'_id': 0, 'id': 1, 'name': 1})
                res_user = self.query(
                    _filter_user, 'PT_User')

            if len(res_user) > 0:
                for item in res_user:
                    user_obj[item['id']] = item['name']

                for item in res['result']:
                    if 'user_id' in item and item['user_id'] in user_obj:
                        item['name'] = user_obj[item['user_id']]

        return res

    # MQTT发布信息
    def mqtt_publish(self, topic, payload):
        # print('mqtt_publish')
        self.mqtt_client.publish(topic, payload, 0, False)

    # 启动MQTT链接
    def mqtt_start(self):
        if self.threading is None:
            self.threading = Thread(target=self.__connect)
            self.threading.start()

    # 关闭MQTT链接
    def mqtt_shutdown(self):
        if self.threading:
            self.mqtt_client.disconnect()
            self.mqtt_client.loop_stop()
            self.threading = None

    # MQTT客户端连接
    def __connect(self):
        try:
            self.mqtt_client.username_pw_set(mqtt_user, mqtt_pass)
            self.mqtt_client.connect(mqtt_host, mqtt_port, 60)
            self.mqtt_client.loop_forever(1.0)
        except Exception as ex:
            pass

    # 获取呼叫中心呼入列表
    def get_call_center_push_list(self, org_list, condition, page, count):

        # 简单的验证
        if 'username' not in condition or 'password' not in condition or condition.get('username') != push_user or condition.get('password') != push_pass:
            return '403'

        keys = ['id', 'ids', 'device_sim',
                'elder_name', 'begin_date', 'end_date']

        if 'create_date' in list(condition.keys()) and type(condition['create_date']) == list:

            if len(condition['create_date']) > 0 and condition['create_date'][0] != '':
                begin_date_arr = condition['create_date'][0].split()
                condition['begin_date'] = begin_date_arr[0] + \
                    'T' + begin_date_arr[1] + 'Z'

            if len(condition['create_date']) > 1 and condition['create_date'][1] != '':
                end_date_arr = condition['create_date'][1].split()
                condition['end_date'] = end_date_arr[0] + \
                    'T' + end_date_arr[1] + 'Z'

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match(
            (C('id') == values['id'])
            & (C('id').inner(values['ids']))
            & (C('elder_name').like(values['elder_name']))
            & (C('device_sim').like(values['device_sim']))
            & (C('create_date') >= as_date(values['begin_date']))
            & (C('create_date') <= as_date(values['end_date']))
        )\
            .sort({'create_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'lat': 1,
                'lng': 1,
                'address': 1,
                'elder_id': 1,
                'elder_name': 1,
                'device_sim': 1,
                'emergency_call': 1,
                'create_date': 1,
            })
        res = self.page_query(_filter, self.call_center_push, page, count)

        # 获取总数
        _filter_all = MongoBillFilter()
        _filter_all.match(
            (C('id').exists(True))
        )\
            .project({
                '_id': 1,
            })
        res_all = self.query(_filter_all, self.call_center_push)

        res['all'] = len(res_all)

        return res

    # 坐席运营统计
    def get_seat_operation_list(self, condition, page, count):

        _filter = MongoBillFilter()
        _filter.match_bill()
        _filter.sort({'create_date': -1}).project({'_id': 0})
        res = self.query(_filter, self.call_center_db)

        tempResult = {}

        newReturn = {
            'result': [],
            'total': 10,
        }

        if len(res) > 0:
            if 'circle' in condition:
                emptyObj = {
                    'call_summary': 0,
                    'call_count': 0,
                    'service_demand_summary': 0,
                    'call_time_avg': 0,
                    'on_call_times': 0,
                    'on_call_rate': 0,
                    'handle_time_avg': 0,
                    'wait_time_avg': 0,
                    'call_time_avg': 0,
                    'good_rate': 0,
                    'bad_rate': 0,
                }
                for item in res:
                    if 'create_date' in item:
                        year = str(item['create_date'].year)
                        if condition['circle'] == '年度':
                            # 年份周期
                            circle_type = str(item['create_date'].year)
                            if circle_type not in tempResult:
                                tempResult[circle_type] = {
                                    'year': year, 'count_circle': year + '年', 'circle_type': circle_type, **emptyObj}
                        elif condition['circle'] == '季度':
                            month = item['create_date'].month
                            # 季度周期
                            circle_type = 0
                            circle_type_str = 0
                            if month >= 1 and month <= 3:
                                circle_type = '1'
                                circle_type_str = '第一'
                            elif month >= 4 and month <= 6:
                                circle_type = '2'
                                circle_type_str = '第二'
                            elif month >= 7 and month <= 9:
                                circle_type = '3'
                                circle_type_str = '第三'
                            elif month >= 10 and month <= 12:
                                circle_type = '4'
                                circle_type_str = '第四'
                            if circle_type not in tempResult:
                                tempResult[circle_type] = {
                                    'year': year, 'count_circle': circle_type_str + '季度', 'circle_type': circle_type, **emptyObj}
                        elif condition['circle'] == '月度':
                            # 月份周期
                            circle_type = str(item['create_date'].month)
                            if circle_type not in tempResult:
                                tempResult[circle_type] = {
                                    'year': year, 'count_circle': circle_type + '月', 'circle_type': circle_type, **emptyObj}

                        # 默认时长
                        item['duration'] = 0

                        # 接通次数
                        if 'pickup' in item:
                            if item['pickup'] == True:

                                # 通话时长
                                if 'begin_time' in item and 'end_time' in item:
                                    begin_timestamp = int(
                                        time.mktime(item['begin_time'].timetuple()))
                                    end_timestamp = int(
                                        time.mktime(item['end_time'].timetuple()))
                                    item['duration'] = end_timestamp - \
                                        begin_timestamp

                                tempResult[circle_type]['on_call_times'] += 1

                        tempResult[circle_type]['call_summary'] += item['duration']
                        tempResult[circle_type]['call_count'] += 1
                        tempResult[circle_type]['service_demand_summary'] += 1
                for item in tempResult:
                    # 秒转为分钟
                    tempResult[item]['call_summary'] = round(
                        tempResult[item]['call_summary'] / 60, 4)
                    # 平均通话时间
                    tempResult[item]['call_time_avg'] = round(
                        tempResult[item]['call_summary'] / tempResult[item]['call_count'], 4)
                    # 接通率
                    tempResult[item]['on_call_rate'] = str(round(
                        tempResult[item]['on_call_times'] / tempResult[item]['call_count'] * 100, 4)) + '%'
                    # 平均处理时间（小时）
                    tempResult[item]['handle_time_avg'] = round(
                        tempResult[item]['call_summary'] / tempResult[item]['call_count'] / 60, 4)
                    newReturn['result'].append(tempResult[item])

        return newReturn

    # 更新移动呼叫中心帐号
    def update_yingda(self, condition):
        '''接听时间记录'''
        res = '记录失败'
        table = 'PT_Call_Center_YD'
        bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                    TypeId.Income.value, condition, table)
        if bill_id:
            res = '记录失败'
        return res