# -*- coding: utf-8 -*-

'''
信息管理函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from enum import Enum
from ...service.mongo_bill_service import MongoBillFilter
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_common_project, get_user_id_or_false
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ..buss_pub.personnel_organizational import UserType
from server.pao_python.pao.data import process_db
from ...service.common import get_current_user_id


class MessageState(Enum):
    '''信息状态
    '''
    Already_read = '已读'
    Unread = '未读'


class MessageManageService(MongoService):
    ''' 信息管理 '''
    Message = 'PT_Message'
    Message_type = 'PT_Message_Type'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_all_message_list(self, condition, page, count):
        '''获取信息列表
        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id', 'name', 'message_state', 'receive_info']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('message_state') == values['message_state'])
                           & (C('receive_info') == values['receive_info'])
                           & (C('name').like(values['name'])))\
            .unwind('receive_info', True)\
            .lookup_bill(self.Message_type, 'message_type', 'id', 'message_type_info')\
            .unwind('message_type_info', True)\
            .lookup_bill('PT_User', 'message_creator', 'id', 'message_creator_info')\
            .unwind('message_creator_info', True)\
            .add_fields({'message_type_name': '$message_type_info.name', 'message_creator': '$message_creator_info.name'})\
            .project({'_id': 0, 'message_type_info': 0, 'message_creator_info': 0})
        res = self.page_query(_filter, self.Message, page, count)
        return res

    def get_unread_message_list(self, condition, page, count):
        '''获取未读的信息列表'''
        condition['message_state'] = MessageState.Unread.value
        condition['receive_info'] = get_current_user_id(self.session)
        return self.get_all_message_list(condition, page, count)

    def set_message_already_read(self, message):
        res = 'Fail'

        if 'id' in message.keys():
            message['message_state'] = MessageState.Already_read.value
        bill_id = self.bill_manage_server.add_bill(
            OperationType.update.value, TypeId.message.value, message, self.Message)
        if bill_id:
            res = 'Success'
        return res

    def add_new_message(self, data):
        # 创建一个未读信息
        res = 'Fail'
        # 未读状态
        data['message_state'] = MessageState.Unread.value  # 增加状态，待处理
        # 创建信息的人
        data['message_creator'] = get_current_user_id(self.session)

        # 信息推送时间处理
        if 'push_date' in data.keys():
            data['push_date'] = as_date(data['push_date'])

        # 接受信息的人
        if 'receive_info' not in data.keys():
            data['receive_info'] = []
        else:
            data['receive_info'] = [data['receive_info']]

        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.message.value, data, self.Message)
        if bill_id:
            res = 'Success'
        return res

    def get_message_type_list(self, condition, page, count):
        '''获取信息类型列表
        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('name').like(values['name'])))\
            .project({'_id': 0})
        res = self.page_query(_filter, self.Message_type, page, count)
        return res

    def add_new_message_type(self, data):
        # 创建一个信息类型
        res = 'Fail'

        # 检测是否有创建过
        keys = ['id', 'name']
        values = self.get_value({}, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('name').like(values['name'])))\
            .project({'_id': 0})
        res = self.query(_filter, self.Message_type)

        if(len(res) > 0):
            return 'exists'

        # 创建信息类型时间
        data['create_date'] = datetime.datetime.now()
        # 备注
        data['remark'] = '测试类型'
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.task.value, data, self.Message_type)
        if bill_id:
            res = 'Success'
        return res

    def get_app_online_message_list(self, condition, page, count):
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ['id', 'user_id', 'start_date',
                'end_date', 'status', 'type', 'is_new']
        values = self.get_value(condition, keys)

        isRewrite = False

        # 判断未读数量，如果未读数量大于入参，就以未读数量为准
        if 'check_unread' in condition and condition['check_unread'] == True:
            _filter_unread = MongoBillFilter()
            _filter_unread.match_bill((C('user_id') == values['user_id'])
                                      & (C('type') == 'request')
                                      & (C('status') == MessageState.Unread.value))\
                .project({
                    '_id': 0,
                    'id': 1,
                })
            res_unread = self.page_query(
                _filter_unread, 'PT_Message_Online', 1, 1)
            if res_unread['total'] > count:
                isRewrite = True
                count = res_unread['total']

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('type') == values['type'])
                           & (C('is_new') == values['is_new'])
                           & (C('user_id') == values['user_id'])
                           & (C('status') == values['status']))\
            .sort({
                'create_date': -1
            })\
            .project({
                '_id': 0,
                'id': 1,
                'content': 1,
                'status': 1,
                'type': 1,
                'remark': 1,
                'user_id': 1,
                'worker_id': 1,
                'create_date': 1,
            })
        res = self.page_query(_filter, 'PT_Message_Online', page, count)
        res['isRewrite'] = isRewrite

        # 获取最后回复时间
        if len(res['result']) > 0:
            user_ids = []
            create_date_obj = {}
            user_info_obj = {}
            worker_info_obj = {}
            for item in res['result']:
                if 'user_id' in item and item['user_id'] not in user_ids:
                    user_ids.append(item['user_id'])
                if 'worker_id' in item and item['worker_id'] not in user_ids:
                    user_ids.append(item['worker_id'])

            if len(user_ids) > 0:
                _filter_mo = MongoBillFilter()
                _filter_mo.match_bill(
                    (C('user_id').inner(user_ids))
                    & (C('type') == 'response')
                )\
                    .sort({'create_date': -1})\
                    .project({'_id': 0, 'create_date': 1, 'id': 1, 'user_id': 1, 'worker_id': 1})
                res_mo = self.query(_filter_mo, 'PT_Message_Online')

                if len(res_mo) > 0:
                    for item in res_mo:
                        if 'worker_id' in item and item['user_id'] not in worker_info_obj:
                            user_ids.append(item['user_id'])
                            worker_info_obj[item['user_id']
                                            ] = item['worker_id']
                        if item['user_id'] not in create_date_obj:
                            create_date_obj[item['user_id']
                                            ] = item['create_date']

                _filter_user = MongoBillFilter()
                _filter_user.match_bill(
                    (C('id').inner(user_ids))
                )\
                    .project({'_id': 0, 'name': 1, 'id': 1, 'personnel_info': 1})
                res_user = self.query(_filter_user, 'PT_User')

                if len(res_user) > 0:
                    for item in res_user:
                        if item['id'] not in user_info_obj:
                            user_info_obj[item['id']] = item

            for item in res['result']:
                # 咨询人员信息
                if 'user_id' in item and item['user_id'] in user_info_obj:
                    item['ask_name'] = user_info_obj[item['user_id']]['name']
                    item['telephone'] = user_info_obj[item['user_id']
                                                      ]['personnel_info']['telephone']
                # 最后咨询时间
                if 'user_id' in item and item['user_id'] in create_date_obj:
                    item['last_answer_date'] = create_date_obj[item['user_id']]
                # 客服人员信息
                if 'user_id' in item and item['user_id'] in worker_info_obj:
                    item['worker_name'] = user_info_obj[worker_info_obj[item['user_id']]]['name']

        return res

    # 更新备注
    def update_app_online_message_remark(self, data):
        res = 'Fail'

        if 'id' in data.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.message.value, data, 'PT_Message_Online')
        if bill_id:
            res = 'Success'
        return res

    def update_app_online_message(self, data):
        # 创建一个在线咨询
        res = 'Fail'
        # 未读状态
        data['status'] = MessageState.Unread.value  # 增加状态，待处理

        now_login_id = get_user_id_or_false(self.session)

        # 会话id
        session_id = ''

        if now_login_id != False:
            session_id = now_login_id
        else:
            session_id = str(uuid.uuid1())

        if 'user_id' not in data:
            # 保存这条消息属于谁，不代表这消息就是这人的
            data['user_id'] = session_id
            data['type'] = 'request'
        else:
            # 回复状态
            data['type'] = 'response'
            data['worker_id'] = get_user_id_or_false(self.session)
            # 回复状态，把所有状态修改为已读

            self.update({
                'user_id': data['user_id'],
                'type': 'request'
            }, {
                'status': MessageState.Already_read.value
            }, 'PT_Message_Online', True)

        # 创建信息的人
        data['creator_id'] = session_id

        # 是否新会话
        res_isnew = self.get_app_online_message_list({
            'is_new': True,
            'user_id': session_id
        }, 1, 1)

        if len(res_isnew['result']) == 0:
            # 新会话
            data['is_new'] = True
        else:
            # 旧会话，就把状态设置为未读
            self.update({
                'user_id': data['user_id'],
                'type': 'request',
                'is_new': True,
            }, {
                'status': MessageState.Unread.value
            }, 'PT_Message_Online', True)

        def process_func(db):
            nonlocal res
            data['create_date'] = datetime.datetime.now()
            data['modify_date'] = datetime.datetime.now()
            data['bill_status'] = 'valid'
            data['version'] = 1
            data['id'] = str(uuid.uuid1())
            data['GUID'] = str(uuid.uuid1())
            backVal = insert_data(db, 'PT_Message_Online', data)
            if backVal:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

        if res == 'Success':
            res = {
                'code': 'Success',
                'session_id': session_id
            }
        return res
