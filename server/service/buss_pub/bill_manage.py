'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-07-09 09:20:38
@LastEditTime : 2020-02-01 11:57:18
@LastEditors  : Please set LastEditors
'''

import copy
import datetime
import hashlib
import re
import uuid
from enum import Enum

import pandas as pd

from ...pao_python.pao.data import (DataProcess, data_to_string_date,
                                    dataframe_to_list, process_db)
from ...pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                     MongoService, N, as_date)
from ...pao_python.pao.service.security.security_utility import \
    get_current_account_id
from ...service.common import (
    delete_data, find_data, get_code, get_condition, get_cur_time,
    get_current_user_id, get_current_organization_id, get_current_org_type, get_current_user_name, get_current_organization_name, get_info, insert_data, update_data, get_current_role_id)
from flask import (request as request_info)


class Status(Enum):
    bill_add = 'new'
    bill_valid = 'valid'
    bill_invalid = 'invalid'


class OperationType(Enum):
    add = 'add'
    update = 'update'
    delete = 'del'

# 人员审核状态


class PersonalState(Enum):
    # 通过
    adopt = 'adopt'
    # 不通过
    noPassage = 'noPassage'
    # 审核中
    inReview = 'inReview'


class TypeId(Enum):
    '''审批类型Id'''
    user = 'user'
    orgInfoRecord = 'orgInfoRecord'
    chargeType = 'chargeType'
    Income = 'Income'
    organization = 'organization'
    serviceItem = 'serviceItem'
    transaction = 'transaction'
    serviceProvider = 'serviceProvider'
    serviceType = 'serviceType'
    serviceItemEvaluate = 'serviceItemEvaluate'
    businessArea = 'businessArea'
    # 床位类别
    bedPositionType = 'bedPositionType'
    # 服务项目
    servicesProject = 'servicesProject'
    # 服务项目类别
    servicesItemCategory = 'servicesItemCategory'
    # 服务适用范围
    serviceScope = 'serviceScope'
    # 服务选项
    serviceOption = 'serviceOption'
    # 服务包
    serviceItemPackage = 'serviceItemPackage'
    # 服务产品
    serviceProduct = 'serviceProduct'
    nursing = 'nursing'
    bed = 'bed'
    # 住宿记录
    accommodationRecord = 'accommodationRecord'
    # 财务管理
    financial = 'financial'
    # 服务记录
    serviceRecord = 'serviceRecord'
    # 台账
    serviceLedger = 'serviceLedger'
    # 预约登记
    reservationRegistration = 'reservationRegistration'
    # 服务订单
    serviceOrder = 'serviceOrder'
    # 需求模板
    requirementTemplate = 'requirementTemplate'
    # 首页
    homePage = 'homePage'
    # 评估项目
    assessmentProject = 'assessmentProject'
    # 评估模板
    assessmentTemplate = 'assessmentTemplate'
    # 行为能力评估
    behavioralCompetenceAssessment = 'behavioralCompetenceAssessment'
    # 能力评估
    competenceAssessment = 'competenceAssessment'
    # 入住登记
    checkIn = 'checkIn'
    # 服务人员登记
    servicePerson = 'servicePerson'
    # 住宿区域类型
    hotelZoneType = 'hotelZoneType'
    # 住宿区域
    hotelZone = 'hotelZone'
    # 其他费用
    otherCost = 'otherCost'
    # 费用类型
    costType = 'costType'
    # 需求项目
    requirementItem = 'requirementItem'
    # 需求类型
    requirementType = 'requirementType'
    # 需求类别
    requirementProject = 'requirementProject'
    # 公告
    announcement = 'announcement'
    # 文章类型
    articleType = 'articleType'
    # 评论类型
    commentType = 'commentType'
    # 新闻
    news = 'news'
    # 服务人员
    servicePersonal = 'servicePersonal'
    # 服务结算
    serviceSettlement = 'serviceSettlement'
    # 医疗欠费
    medicalArrears = 'medicalArrears'
    # 水电抄表
    hydropower = 'hydropower'
    # 评论管理
    comment = 'comment'
    # 任务管理
    task = 'task'
    # 任务类型管理
    taskType = 'taskType'
    # 信息管理
    message = 'message'
    # 退住
    retreat = 'retreat'
    # 补贴管理
    allowance = 'allowance'
    # 疾病档案
    disease = 'disease'
    # 补贴审批
    allowanceSubsidy = 'allowanceSubsidy'
    # 高龄津贴名单
    oldAgeList = 'oldAgeList'
    # 请假销假
    LeaveRecord = 'LeaveRecord'
    # 文章
    article = 'article'
    # 文章
    device = 'device'
    # 社会群体类型
    socialGroupType = 'socialGroupType'
    # 监控信息
    monitor = 'monitor'
    # 按钮一键生成与请假/销假相关的服务记录
    btnCreateRecord = 'btnCreateRecord'
    # 服务订单分派
    serviceTask = 'serviceTask'
    # PT_Role
    ptRole = 'ptRole'
    # PT_Permission
    ptPermission = 'ptPermission'
    # PT_Set_Role
    ptSetRole = 'ptSetRole'
    # 杂费
    incidental = 'incidental'
    # 餐费
    footCost = 'foodCost'
    # 医疗费用
    medicalCost = 'medicalCost'
    # 尿片费用
    diaperCost = 'diaperCost'
    # 角色关联
    setRole = 'setRole'
    # 角色设置
    role = 'role'
    # 护理项目
    nursingProject = 'nursingProject'
    # 护理模板
    nursingTemplate = 'nursingTemplate'
    # 护理记录
    # 护理等级
    nursingGrade = '护理等级'
    nursingRecode = 'nursingRecode'
    # 慈善管理
    charitableDonate = 'charitableDonate'
    # 慈善记录表
    charitableRecord = 'charitableRecord'
    # 慈善项目
    charitableProject = 'charitableProject'
    # 个人募捐
    charitableRecipient = 'charitableRecipient'
    # 慈善会资料
    charitableSociety = 'charitableSociety'
    # 捐款审批
    charitableAppropriation = 'charitableAppropriation'
    # 冠名基金
    charitableTitleFund = 'charitableTitleFund'
    # 志愿者服务类别
    volunteerServiceCategory = 'volunteerServiceCategory'
    # 收付款过程记录
    financialReceivableProcess = 'financialReceivableProcess'
    # 账户
    account = 'account'
    # 账户出入记录
    accountRecord = 'accountRecord'
    # 账户充值
    accountRecharge = 'accountRecharge'
    # 需求定义
    approvalDefine = 'approvalDefine'
    approvalProcess = 'approvalProcess'
    # 第三方支付
    thirdPartPay = 'thirdPartPay'
    # 活动
    activity = 'activity'
    # 活动报名
    activityParticipate = 'activityParticipate'
    # 活动签到
    activitySignin = 'activitySignin'
    # 活动室
    activityRoom = 'activityRoom'
    # 活动类型
    activityType = 'activityType'
    # 调查问卷
    questionnaire = 'questionnaire'
    # 活动室预约
    activityRoomReservation = 'activityRoomReservation'
    # 人员类别
    personnelClassification = 'personnelClassification'
    # 预约服务
    reserveService = 'reserveService'
    # 呼叫中心
    emiCallCenter = 'emiCallCenter'
    # 知识库
    knowledgeBase = 'knowledgeBase'
    # 行政区域
    administrationDivision = 'administrationDivision'
    # 人员关系
    userRelationShip = 'userRelationShip'
    # 老友圈
    friendsCircle = 'friendsCircle'
    # 收藏/关注
    followCollection = 'followCollection'
    # 意见反馈
    opinionFeedback = 'opinionFeedback'
    # 订单评论
    orderComment = 'orderComment'
    # 需求
    demand = 'demand'
    # 短信
    sms = 'sms'
    # 竞单
    bidding = 'bidding'
    # 草稿
    draft = 'draft'
    # 长者用餐
    elderFood = 'elderFood'
    # 收费减免
    deduction = 'deduction'
    # 排班管理
    setClass = 'setClass'
    # 排班类型
    setClassType = 'setClassType'
    # 护理收费
    nursingPay = 'nursingPay'
    # 权限
    permission = 'permission'
    # 评比方案
    ratingPlan = 'ratingPlan'
    # 评比填表
    rating = 'rating'
    # 团体
    groups = 'groups'
    # 团体成员
    groupsMember = 'groupsMember'
    # 服务计划
    servicePlan = 'servicePlan'
    # 账单
    bill = 'bill'
    # 项目协议与报告
    dealReport = 'dealReport'
    # 年度计划与总结
    yearSummary = 'yearSummary'
    # 月度计划
    monthPlan = 'monthPlan'
    # 圈子
    circle = 'circle'
    # 在线消息
    messageOnline = 'messageOnline'
    # APP页面设置
    appPage = 'appPage'
    # APP设置
    appConfig = 'appConfig'
    # 长者膳食
    elderMeals = 'elderMeals'
    # 计量单位
    units = 'units'
    # 物料分类
    thingSort = 'thingSort'
    # 物料档案
    thingArchives = 'thingArchives'
    # 幸福小站产品
    happinessStationProduct = 'happinessStationProduct'
    # 幸福院指标
    targetSettingDetails = 'targetSettingDetails'
    # 幸福院年度自评
    yearSelfAssessment = 'yearSelfAssessment'
    # 个案服务
    serviceSituation = 'serviceSituation'
    # 探访服务
    visitSituation = 'visitSituation'
    # 消防设施
    fireEquipment = 'fireEquipment'
    # 南海呼叫中心
    callCenterNh = 'callCenterNh'
    # 我的房间
    myRoom = 'myRoom'
    myHost = 'myHost'
    # 警报信息
    myWarn = 'myWarn'
    # 我的设备
    myDetail = 'myDetail'
    # 移动呼叫中心帐号
    callCenterZh = 'callCenterZh'
    # 适老化设备警报
    deviceWarning = 'deviceWarning'
    # APP隐私
    appPrivacyAgree = 'appPrivacyAgree'
    # 数据字典
    dataDictionary = 'dataDictionary'
    # 商品信息
    products = 'products'
    # 收货地址
    shipAddress = 'shipAddress'
    # 板块信息
    blocks = 'blocks'
    # 分账登记
    ledgerAccountRegister = 'ledgerAccountRegister'
    # 订单状态
    orderStatus = 'orderStatus'
    # 商品订单
    productOrders = 'productOrders'
    # 设备消息
    deviceMessage = 'deviceMessage'
    # 设备绑定
    deviceBind = 'deviceBind'


class BillData():
    '''单据对象'''

    def __init__(self, bill_type, type_id, initiator, related_parties, body_mes, table_name):
        '''构造函数'''
        self.bill_type = bill_type
        self.type_id = type_id
        self.initiator = initiator
        self.related_parties = related_parties
        self.body_mes = body_mes
        self.bill_code = get_code()
        self.bill_status = Status.bill_add.value
        self.version = 1
        self.table_name = table_name

    def to_dict(self):
        return self.__dict__


class BillManageService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session

    def add_bill(self, bill_type, type_id, datas, table_names, update_other_table=False):
        '''# 新增单据信息'''
        res = False

        def process_func(db):
            nonlocal res
            if isinstance(table_names, list):
                if isinstance(datas, list):
                    res = False
                else:
                    '''非法的传入参数，table_names为集合的时候，datas也必须为集合'''
                    return res
            bill_data = BillData(bill_type, type_id, '发起人',
                                 '关联方', datas, table_names)
            b_data = bill_data.to_dict()
            # 默认插入bill表的机构为平台，避免一些未登录调用的接口报错
            b_data['organization_id'] = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
            data_info = get_info(b_data, self.session)
            # 查询当前登录账号的用户Id
            person_id = get_current_user_id(self.session)
            data_info['initiator_id'] = person_id
            # 根据单据类型查找单据签核清单
            bill_type_mes = find_data(
                db, 'PT_Bill_Type', {'id': type_id})
            if len(bill_type_mes) > 0:
                data_info['signature_list'] = bill_type_mes[0]['signature_list']
                insert_data(db, 'PT_Bill', data_info)
            else:
                insert_data(db, 'PT_Bill', data_info)
                bill_id = self.sign_bill(data_info['id'])
                # 直接审核新增数据
                self.common_add(data_info['id'], data_info['body_mes'],
                                data_info['table_name'], bill_type, update_other_table)
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def sign_bill(self, bill_id):
        '''# 直接签核单据'''
        res = False

        def process_func(db):
            nonlocal res
            mes = find_data(db, 'PT_Bill', {'id': bill_id})
            if len(mes) > 0:
                bill_mes = get_info(mes[0], self.session)
                # 已签核
                # bill_mes['signature_status'] = 'Y'
                # 生效
                bill_mes['bill_status'] = Status.bill_valid.value
                sign_result = update_data(
                    db, 'PT_Bill', bill_mes, {'id': bill_id})
                if sign_result:
                    res = bill_id
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def common_add(self, bill_id, datas, table_names, option_type, update_other_table=False):
        res = False

        def process_func(db):
            nonlocal res
            table_name_list = []
            data_l = []
            if isinstance(table_names, list):
                if isinstance(datas, list):
                    table_name_list = table_names
                    if len(table_name_list) == 1 and len(datas) > 1:
                        data_l.append(datas)
                    else:
                        data_l = datas
                else:
                    '''非法的传入参数，table_names为集合的时候，datas也必须为集合'''
                    return res
            else:
                if isinstance(datas, list):
                    table_name_list.append(table_names)
                    if len(datas) > 1:
                        data_l.append(datas)
                    else:
                        data_l = datas
                else:
                    table_name_list.append(table_names)
                    data_l.append(datas)
            for i, table_name in enumerate(table_name_list):
                table_data = data_l[i]

                data_list = []
                if isinstance(table_data, list):
                    data_list = table_data
                else:
                    data_list.append(table_data)

                if option_type == OperationType.add.value:
                    self.add_data_many(bill_id, data_list, table_name)
                elif option_type == OperationType.update.value:
                    if update_other_table:
                        self.update_data_many_other_table(
                            bill_id, data_list, table_name)
                    else:
                        self.update_data_many(bill_id, data_list, table_name)
                elif option_type == OperationType.delete.value:
                    # for version_data in data_list:
                    self.del_data_many(bill_id, data_list, table_name)
                # for version_data in data_list:
                #     if option_type == OperationType.add.value:
                #         self.add_data(bill_id, version_data, table_name)
                #     if option_type == OperationType.update.value:
                #         self.update_data(
                #             bill_id, version_data, table_name)
                #     if option_type == OperationType.delete.value:
                #         self.del_data(bill_id, version_data, table_name)
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def add_data_many(self,  bill_id, version_data, table_name):
        '''
        1，为version_data增加相关的版本管理的数据
        2，插入处理过的数据到数据库
        '''
        operator_id = get_current_user_id(self.session)
        tep_data = [get_info(i, self.session) for i in version_data]
        insert_data = []
        for i in tep_data:
            i.update({'GUID': str(uuid.uuid1()), 'version': 1, 'bill_status': Status.bill_valid.value,
                      'valid_bill_id': bill_id, 'bill_operator': operator_id})
            insert_data.append(i)
        try:
            self.insert_many(insert_data, table_name)
        except:
            res = False
          #print(str(table_name)+' 表插入失败')
        else:
            # 记录操作日志
            # self.insert_log(table_name, '修改数据成功', insert_data)
            res = True
          #print(str(table_name)+' 表插入成功')
        return res

    def add_data(self,  bill_id, version_data, table_name):
        '''# 新增数据'''
        res = False

        def process_func(db):
            nonlocal res
            if 'id' in version_data.keys():
                data_mes = find_data(
                    db, table_name, {'id': version_data['id'], 'invalid_bill_id': bill_id})
                if len(data_mes) > 0:
                    data_info = data_mes[0].copy()
                    data_info.update(version_data)
                    data_info.pop("_id")
                    data_info.pop("invalid_bill_id")
                else:
                    data_info = get_info(version_data, self.session)
            else:
                data_info = get_info(version_data, self.session)
            data_info['GUID'] = str(uuid.uuid1())
            if 'version' in data_info.keys():
                data_info['version'] = data_info['version'] + 1
            else:
                data_info['version'] = 1
            data_info['bill_status'] = Status.bill_valid.value
            data_info['valid_bill_id'] = bill_id
            # 记录操作人
            data_info['bill_operator'] = get_current_user_id(self.session)

            insert_data(db, table_name, data_info)
            # if result:
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def update_data_many(self, bill_id, version_data, table_name):
        '''
        1,查询待更新数据的上一条数据，并修改
        2，根据查询结果更新待插入数据
        3,如果存在update_other_table，则改为：旧数据删除，然后插入其他表，新增正常
        '''
        operator_id = get_current_user_id(self.session)
        update_insert_data = []
        new_insert_data = []
        old_GUID = []

        ids = [i['id'] for i in version_data]
        _filter = MongoFilter()
        _filter.match((C('id').inner(ids)) & (C('bill_status') == Status.bill_valid.value))\
               .project({'_id': 0, 'invalid_bill_id': 0})
        old_datas = self.query(_filter, table_name)
        old_ids = [i['id'] for i in old_datas]
        for i in version_data:
            if i['id'] in old_ids:
                tep = [t for t in old_datas if t['id'] == i['id']]
                old_data = copy.deepcopy(tep[0])
                old_data['invalid_bill_id'] = bill_id
                old_data['bill_status'] = Status.bill_invalid.value
                #self.update({'GUID': old_data['GUID']},old_data,table_name)
                old_GUID.append(old_data['GUID'])
                old_data.update(i)
                old_data.pop("invalid_bill_id")
                old_data['version'] = old_data['version'] + 1
                old_data.update({'GUID': str(uuid.uuid1()), 'bill_status': Status.bill_valid.value,
                                 'valid_bill_id': bill_id, 'bill_operator': operator_id, 'modify_date': datetime.datetime.now()})
                update_insert_data.append(old_data)
            else:
                new_insert_data.append(i)
        if len(new_insert_data) > 0:
            res = self.add_data_many(bill_id, new_insert_data, table_name)
          #print(str(table_name)+' 表数据插入成功')
        if len(update_insert_data) > 0:
            try:
                self.update({'GUID': {'$in': old_GUID}}, {
                            'invalid_bill_id': bill_id, 'bill_status': Status.bill_invalid.value}, table_name, True)
                self.insert_many(update_insert_data, table_name)
            except:
                res = False
              #print(str(table_name)+' 表更新失败')
            else:
                res = True
              #print(str(table_name)+' 表更新成功')
        return res

    def update_data_many_other_table(self, bill_id, version_data, table_name):
        '''
        把invalid的数据插入到第三方表中
        1,改为：invalid的旧数据插入到第三方表，再把新数据根据id更新一下
        '''
        operator_id = get_current_user_id(self.session)
        update_insert_data = []
        other_insert_data = []
        new_insert_data = []
        old_GUID = []

        ids = [i['id'] for i in version_data]
        _filter = MongoFilter()
        _filter.match((C('id').inner(ids)) & (C('bill_status') == Status.bill_valid.value))\
               .project({'_id': 0, 'invalid_bill_id': 0})
        old_datas = self.query(_filter, table_name)
        old_ids = [i['id'] for i in old_datas]
        for i in version_data:
            if i['id'] in old_ids:
                tep = [t for t in old_datas if t['id'] == i['id']]
                other_old_data = copy.deepcopy(tep[0])
                other_old_data['invalid_bill_id'] = bill_id
                other_old_data['bill_status'] = Status.bill_invalid.value
                other_insert_data.append(other_old_data)
                #self.update({'GUID': old_data['GUID']},old_data,table_name)
                old_data = copy.deepcopy(tep[0])
                old_GUID.append(old_data['GUID'])
                old_data.update(i)
                old_data['version'] = old_data['version'] + 1
                old_data.update({'GUID': str(uuid.uuid1()), 'bill_status': Status.bill_valid.value,
                                 'valid_bill_id': bill_id, 'bill_operator': operator_id, 'modify_date': datetime.datetime.now()})
                update_insert_data.append(old_data)
            else:
                new_insert_data.append(i)
        if len(new_insert_data) > 0:
            res = self.add_data_many(bill_id, new_insert_data, table_name)
          #print(str(table_name)+' 表数据插入成功')
        if len(update_insert_data) > 0 and len(other_insert_data) > 0:
            try:
                # 插入新数据
                self.insert_many(update_insert_data, table_name)
                # 删除旧数据
                self.delete({'GUID': {'$in': old_GUID}}, table_name)
                # 把旧数据插入第三方表
                self.insert_many(other_insert_data, table_name+'_Bill')
            except:
                res = False
              #print(str(table_name)+' 表更新失败')
            else:
                res = True
              #print(str(table_name)+' 表更新成功')
        return res

    def update_data(self, bill_id, version_data, table_name):
        '''# 修改数据(把状态改为失效-) version_data是新修改的信息'''
        res = False

        def process_func(db):
            nonlocal res
            # 查询原来的数据
            data_info = find_data(
                db, table_name, {'id': version_data['id'], 'bill_status': Status.bill_valid.value})
            if len(data_info) > 0:
                data_info[0]['bill_status'] = Status.bill_invalid.value
                # 把原本数据的失效单据Id改为新的单据id
                data_info[0]['invalid_bill_id'] = bill_id
                data_info[0].pop('_id')
                update_data(db, table_name, data_info[0], {
                    'GUID': data_info[0]['GUID']})
                self.add_data(bill_id, version_data, table_name)
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_data(self, bill_id, version_data, table_name):
        '''# 删除数据(逻辑删除-状态改为失效)'''
        res = False

        def process_func(db):
            nonlocal res
            if 'id' in version_data:
                data_info = find_data(
                    db, table_name, {'id': version_data['id'], 'bill_status': Status.bill_valid.value})
                if len(data_info) > 0:
                    data_info[0]['bill_status'] = Status.bill_invalid.value
                    data_info[0]['invalid_bill_id'] = bill_id
                    # 记录操作人
                    data_info[0]['bill_operator'] = get_current_user_id(
                        self.session)
                    data_info[0].pop('_id')
                    update_data(db, table_name, data_info[0], {
                        'GUID': data_info[0]['GUID']})
                    # 记录操作日志
                    # self.insert_log(table_name, '删除数据成功', data_info)
            res = True
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_data_many(self, bill_id, version_data, table_name):
        '''
        1,查询待更新数据的上一条数据，并修改
        2，根据查询结果更新待删除数据(逻辑删除-状态改为失效)
        '''
        operator_id = get_current_user_id(self.session)
        old_GUID = []

        ids = [i['id'] for i in version_data]
        _filter = MongoFilter()
        _filter.match((C('id').inner(ids)) & (C('bill_status') == Status.bill_valid.value))\
               .project({'_id': 0, 'invalid_bill_id': 0})
        old_datas = self.query(_filter, table_name)
        old_ids = [i['id'] for i in old_datas]
        res = ''
        for i in version_data:
            if i['id'] in old_ids:
                tep = [t for t in old_datas if t['id'] == i['id']]
                old_data = copy.deepcopy(tep[0])
                old_GUID.append(old_data['GUID'])
        if len(old_GUID) > 0:
            try:
                self.update({'GUID': {'$in': old_GUID}}, {
                            'invalid_bill_id': bill_id, 'bill_status': Status.bill_invalid.value, 'bill_operator': operator_id}, table_name, True)
            except:
                res = False
              #print(str(table_name)+' 表更新失败')
            else:
                res = True
              #print(str(table_name)+' 表更新成功')
        return res

    def get_signatureBill_by_personId(self, org_list, condition, page, count):
        '''# 查询需要签核的单据'''
        _filter = MongoFilter()
        # persion_id = "a901a51e-a21f-11e9-b37a-144f8a6221df"
        persion_id = ''

        def process_func(db):
            nonlocal persion_id
            persion_id = get_current_user_id(self.session)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

        _filter.add_fields({'new_field': self.ao.array_filter(
            "$signature_list", "aa", ((F('$aa.signature_person_id') == persion_id) & (F('$aa.status') == '')).f)}).match((C("new_field") != None) & (C("new_field") != [])).add_fields({
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .match(C('organization_id').inner(org_list))\
            .project({'_id': 0, "body_mes._id": 0})
        res = self.page_query(_filter, "PT_Bill", page, count)
        # print(res, 'res')
        return res

    def get_signature_bill_by_initiatorId(self, condition, page, count):
        '''# 发起人查询已发起单据列表'''
        res = {'result': []}

        def process_func(db):
            nonlocal res
            person_id = get_current_user_id(self.session)
            cols = db['PT_Bill']
            condition['initiator_id'] = person_id
            if page and count:
                data_list = list(cols.find(condition)[
                                 (page-1)*count:(page-1)*count+count])
            else:
                data_list = list(cols.find(condition)[:])
            res['total'] = cols.find(condition).count()
            if len(data_list) > 0:
                for x in data_list:
                    otherStyleTime = data_to_string_date(x['create_date'])
                    x['create_date'] = otherStyleTime
                pd_equit = pd.DataFrame(data_list).drop(['_id'], axis=1)
                res['result'] = dataframe_to_list(pd_equit)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def signature_bill_by_personId(self, signatureContent):
        '''# 单据签核 signatureContent:{id,ramark}'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            person_id = get_current_user_id(self.session)

            data_info = find_data(
                db, 'PT_Bill', {'id': signatureContent['id']})
            if len(data_info) > 0:
                data = data_info[0]
                signature_list = data['signature_list']
                flag = True
                for signature in signature_list:
                    if signature['signature_person_id'] == person_id and signature['status'] == '':
                        signature['status'] = Status.bill_valid.value
                        signature['remark'] = signatureContent['remark']
                for result in signature_list:
                    if result['status'] == '':
                        flag = False
                if flag:
                    data['bill_status'] = Status.bill_valid.value
                    self.common_add(
                        data['id'], data['body_mes'], data['table_name'], data['bill_type'])
                    # if data['bill_type'] == 'add':
                    #     # 新增版本数据
                    #     self.add_data(data['id'], data['body_mes'],
                    #                   data['table_name'])
                    # if data['bill_type'] == 'update':
                    #     # 修改版本数据
                    #     self.update_data(data['id'], data['body_mes'],
                    #                      data['table_name'])
                    # if data['bill_type'] == 'del':
                    #     # 删除版本数据 问题来了：原本数据的id只能从body_mes中取
                    #     self.del_data(
                    #         data['id'], data['body_mes'], data['table_name'])
                Sign_result = update_data(
                    db, 'PT_Bill', data, {'id': signatureContent['id']})
            if Sign_result:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def refuse_bill_by_personId(self, signatureContent):
        '''# 拒签 signatureContent:{id,ramark}'''
        res = 'Fail'
        # 从session中取出account_id
        account_id = get_current_account_id(self.session)

        def process_func(db):
            nonlocal res
            nonlocal account_id
            person_id = ''
            # 根据accountid查找userid，再用userid找人员Id
            user_mes = find_data(db, 'PT_User', {'account_id': account_id})
            if len(user_mes) > 0:
                person_mes = find_data(db, 'PT_Personnel', {
                                       'user_id': user_mes[0]['id']})
                if len(person_mes) > 0:
                    person_id = person_mes[0]['id']
            data_info = find_data(
                db, 'PT_Bill', {'id': signatureContent['id']})
            if len(data_info) > 0:
                data = data_info[0]
                signature_list = data['signature_list']

                for signature in signature_list:
                    if signature['signature_person_id'] == person_id and signature['status'] == '':
                        signature['status'] = Status.bill_invalid.value
                        signature['remark'] = signatureContent['remark']
                        data['bill_status'] = Status.bill_invalid.value
                Sign_result = update_data(
                    db, 'PT_Bill', data, {'id': signatureContent['id']})
            if Sign_result:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def recovery_previous_version(self, data_id, table_name):
        '''# 恢复上一版本单据'''
        res = 'Fail'
        # 通过数据id + version-1找到上一版本数据，没有的话，就没有恢复数据功能，有的话就用生效单据Id找到单据信息，body_mes就是需要恢复的数据内容，然后用body_mes走单据流程

        def process_func(db):
            nonlocal res
            new_data_mes = find_data(
                db, table_name, {'id': data_id, 'bill_status': Status.bill_valid.value})
            if len(new_data_mes) > 0:
                # 上一版本数据信息
                previous_data = find_data(
                    db, table_name, {'id': data_id, 'version': int(new_data_mes[0]['version']) - 1})
                if len(previous_data) > 0:
                    previous_bill_data = find_data(db, 'PT_Bill', {
                        'id': previous_data[0]['valid_bill_id']})
                    if len(previous_bill_data) > 0:
                        body_mes = previous_bill_data[0]['body_mes']
                        type_id = previous_bill_data[0]['type_id']
                        bill_id = self.add_bill(
                            OperationType.update.value, type_id, body_mes, table_name)
                        if bill_id:
                            res = 'Success'
                else:
                    res = "没有上一版本数据可恢复"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    # def insert_log(self, operation_table, msg, operation_data):
    #     res = False
    #     try:
    #          # 获取当前登录人姓名
    #         user_name = get_current_user_name(self.session)
    #         # 获取当前登录人组织机构名称
    #         org_name = get_current_organization_name(self.session)
    #         # 获取当前登录人组织机构id
    #         org_id = get_current_organization_id(self.session)
    #         # 获取当前登录人组织机构类型
    #         org_type = get_current_org_type(self.session)
    #         # 获取当前登录人角色
    #         role_id = get_current_role_id(self.session)

    #         insert_data = [{
    #             'user_name': user_name,
    #             'org_name': org_name,
    #             'create_date': get_cur_time(),
    #             'operation_table': operation_table,
    #             'module_name': module_name_list[operation_table] if module_name_list.get(operation_table) else None,
    #             'msg': msg,
    #             'organization_id': org_id,
    #             'org_type': org_type,
    #             'role_id': role_id,

    #         }]
    #         # 插入记录表
    #         self.insert_many(insert_data, 'PT_Operation_Log')
    #       #print(str('PT_Operation_Log')+' 表插入成功')
    #     except:
    #         res = False
    #       #print(str('PT_Operation_Log')+' 表插入失败')
    #     return res

    def insert_logs(self, module_top, module_name, msg=None):
        try:

            # 获取当前登录人姓名
            user_name = get_current_user_name(self.session)
            # 获取当前登录人组织机构名称
            org_name = get_current_organization_name(self.session)
            # 获取当前登录人组织机构id
            org_id = get_current_organization_id(self.session)
            # 获取当前登录人组织机构类型
            org_type = get_current_org_type(self.session)
            # 获取当前登录人角色
            role_id = get_current_role_id(self.session)

            # referer = ''
            # if hasattr(request_info, 'headers') and 'Referer' in request_info.headers:
            #     referer = request_info.headers['Referer']

            # 获取相关功能模块名称
            insert_data = [{
                'user_name': user_name,
                'org_name': org_name,
                'organization_id': org_id,
                'org_type': org_type,
                'role_id': role_id,
                'create_date': get_cur_time(),
                'module_top': module_top,
                'module_name': module_name,
                # 'referer': referer,
                'msg': msg,
            }]
            # 插入记录表
            self.insert_many(insert_data, 'PT_Operation_Log')
        except Exception as e:
            # print('BillManageService.insert_logs: 记录发生错误了', e)
            insert_data = [{
                'user_name': '等保测试账号',
                'org_name': '智慧养老平台',
                'organization_id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67',
                'org_type': '平台',
                'role_id': '8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe',
                'create_date': get_cur_time(),
                'module_top': module_top,
                'module_name': module_name,
                # 'referer': referer,
                'msg': msg,
            }]
            # 插入记录表
            self.insert_many(insert_data, 'PT_Operation_Log')
            # print(str('PT_Operation_Log')+' 表插入失败')

    def get_module_name_all(self):
        return {
            '福利院': {
                '登录系统': '',
                '长者档案': '',
                '员工档案': '',
                '预约登记': '',
                '入住长者': '',
                '离开原因': '',
                '床位更换': '',
                '请销假记录': '',
                '护理服务内容': '',
                '长者护理记录': '',
                '其他费用': '',
                '预收款账单': '',
                '实收款核算及审核': '',
                '财务系统': '',
                '发布咨询管理': '',
                '发布评论管理': '',
                '机构信息档案': '',
                '机构运营情况': '',
                '评估项目': '',
                '评估模板管理': '',
                '长者评估': '',
                '从业人员补贴申请': '',
                '区域管理': '',
                '床位全览': '',
                '床位管理': '',
                '服务产品': '',
                '功能室管理': '',
                '计量单位': '',
                '物料分类': '',
                '物料档案': '',
                '药品档案': '',
                '疾病档案': '',
                '过敏档案': '',
                '监控管理': '',
                '用户管理': '',
                '角色管理': '',
                '安全设置': '',
                '疫情复工汇总表': '',
                '全省民政部门重点防疫场所': ""
            },
            '幸福院': {
                '登录系统': '',
                '机构信息档案': '',
                '员工档案': '',
                '运营机构信息': '',
                '项目协议与报告': '',
                '年度计划与总结': '',
                '年度自评': '',
                '月度计划': '',
                '活动管理': '',
                '发布咨询管理': '',
                '发布评论管理': '',
                '长者档案': '',
                # '健康信息': '',
                '个案服务情况': '',
                '探访服务情况': '',
                '长者评估': '',
                '团队管理': '',
                '长者膳食': '',
                '幸福小站': '',
                '消防设施管理': "",
                '验收管理': '',
                # '评比管理': "",
                '用户管理': '',
                '角色管理': '',
                '安全设置': '',
                '疫情复工汇总表': ''
            },
            '服务商': {
                '登录系统': '',
                '机构信息档案': '',
                '员工档案': '',
                '服务派工': '',
                '服务下单': '',
                '服务评价': '',
                '服务回访': '',
                '发布咨询管理': '',
                '发布评论管理': '',
                '服务产品': '',
                '用户管理': '',
                '角色管理': '',
                '安全设置': '',
                '疫情复工汇总表': ''
            },
            '平台': {
                '登录系统': '',
                '呼叫中心': '',
                '系统公告管理': '',
                '社区幸福院评比指标设置': '',
                '社区幸福院评比详细指标设置': '',
                '服务下单': '',
                '服务预定': '',
                '长者关怀设置': '',
                '长者设备管理': '',
                '发布咨询管理': '',
                '发布评论管理': '',
                '服务产品': '',
                '长者类型设置': '',
                '照护等级设定': '',
                '区域类型': '',
                '服务类型设置': '',
                '服务细项设置': '',
                '适用范围': '',
                '行政区划维护': "",
                'APP用户管理': '',
                '老友圈': '',
                '圈子管理': '',
                'APP设置': '',
                '在线咨询': '',
                'APP意见反馈': '',
                '机构信息档案': '',
                '员工档案': '',
                '长者档案': '',
                '智能设备管理': "",
                '用户管理': '',
                '角色管理': '',
                '安全设置': '',
                '疫情复工汇总表': '',
                '政府补贴账户': "",
                '公众号关注数统计': '',
            },
            '民政': {
                '登录系统': '',
                '养老补贴受理': '',
                '从业人员补贴受理': '',
                '高龄津贴': '',
                '用户管理': '',
                '角色管理': '',
                '安全设置': '',
                '疫情复工汇总表': ''
            }
        }

    # def get_module__name(self, table_name):
    #     return {
    #         # -------------------机构养老--------------------------
    #         # 入住管理
    #         'IEC_Check_In': '长者入住',
    #         'PT_Reservation_Registration': '预约登记',
    #         'PT_Accommodation_Record': '请销假记录',
    #         'Change_Bed': '床位更换',  # 自定义
    #         'PT_Leave_Reason': '离开原因',
    #         # 服务管理
    #         'PT_Nursing_Content': '护理内容',
    #         'PT_Nursing_Recode': '护理记录',
    #         'PT_Other_Cost': '其他费用',
    #         'PT_Financial_Bill': '费用核算/财务系统',
    #         'PT_Article': '发布资讯管理',
    #         'PT_Comment': '资讯评论管理',
    #         'PT_Org_Opertion_Record': '机构运营情况',
    #         'PT_User': '组织/长者/从业人员档案',
    #         'PT_Assessment_Project': '评估项目',
    #         'PT_Assessment_Template': '评估模板管理',
    #         'PT_Behavioral_Competence_Assessment_Record': '长者评估',
    #         'PT_Allowance_Manage': '从业人员补贴申请',
    #         'PT_Hotel_Zone': '区域管理',
    #         'PT_Bed': '床位管理',
    #         'PT_Activity_Room': '功能室管理',
    #         'PT_Unit': '计量单位',
    #         'PT_Thing_Sort': '物料分类',
    #         'PT_Thing_Archives': '物料档案',
    #         'PT_Medicine_file': '药品档案',
    #         'PT_Disease_file': '疾病档案',
    #         'PT_Allergy_file': '过敏档案',
    #         'PT_Monitor': '监控管理',
    #         'PT_Epidemic_Prevention_Day': '疫情复工汇总表',
    #         'PT_Org_Epidemic_Prevention': '全省民政部门重点场所防疫',
    #         # 社区幸福院
    #         'PT_Deal_Report': '项目协议与报告',
    #         'PT_Yearplan_Summary': '年度总结与报告',
    #         'PT_Happiness_Year_Self_Assessment': '年度总结与报告',
    #         'PT_Month_Plan': '月度计划',
    #         'PT_Activity': '活动列表',
    #         'PT_Activity_Participate': '活动报名',
    #         'PT_Activity_Sign_In': '活动签到',
    #         'PT_Healthy_Report': '健康信息',
    #         'PT_Service_Situation': '个案服务情况',
    #         'PT_Visit_Situation': '探访服务情况',
    #         'PT_Groups': '团队管理',
    #         'PT_Elder_Meals': '长者膳食',
    #         'PT_Happiness_Station_Product': '幸福小站',
    #         'PT_Fire_Equipment': '消防设施',
    #         'PT_Set_Role': '用户管理',
    #         'PT_Role': '角色管理',
    #         'PT_Service_Order': '服务下单',
    #         'PT_Set_Role': '账号创建',
    #         'PT_Role': '角色创建',
    #         # 'PT_Task': '服务派工',
    #         'PT_Service_Record': '服务派工',
    #         'PT_Service_Order_Comment': '服务评价',
    #         'PT_Service_Product': '服务产品',
    #         'Return_visit': '服务回访',  # 自定义
    #         'Statement_Of_Account': '服务商结算表',  # 自定义
    #         'Statement_Of_Account_Detail': '服务商记录明细统计',  # 自定义
    #         'PT_Service_Type': '服务类型',
    #         'PT_Call_Center_Nh': '呼叫中心',
    #         'PT_Annoutment': '公告管理',
    #         'PT_Reserve_Service': '服务预订',
    #         'PT_XFY_TargetSetting': '社区幸福院评比指标设置',
    #         'PT_XFY_TargetSetting_Details': '社区幸福院评比详细指标',
    #         'PT_Wx_Follow': '公众号关注统计',
    #         'PT_Care_Recode': '长者关怀记录',
    #         'PT_Care_Type': '长者关怀类型设置',
    #         'PT_Device': '设备管理/智能监护',
    #         'PT_Personnel_Classification': '长者类型设置',
    #         'PT_Nursing_Grade': '照护等级设定',
    #         'PT_Service_Item': '服务细项设置',
    #         'PT_Service_Scope': '服务适用范围',
    #         'PT_Administration_Division': '行政区划维护',
    #         'PT_Friend_Circle_Message': '老友圈',
    #         'PT_Circle': '圈子管理',
    #         'PT_App_Config': 'App设置',
    #         'PT_Message_Online': '在线咨询',
    #         'PT_Opinion_Feedback': 'APP意见反馈',
    #         'PT_Old_Age_List': '高龄津贴',
    #         'PT_Service_Follow_Collection': '服务关注/收藏'
    #     }
