import base64
import datetime
import hashlib
import json
import math
import os
import time
import urllib
import urllib.request
import uuid
from enum import Enum

import pandas as pd
import requests

from server.pao_python.pao.data import dataframe_to_list, date_to_string
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...models.financial_manage import FinancialAccount
from ...pao_python.pao.data import get_cur_time, process_db, string_to_date
from ...pao_python.pao.remote import JsonRpc2Error
from ...service.buss_mis.financial_account import FinancialAccountObject
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.buss_pub.import_excel import ImportExcelService
from ...service.buss_pub.personnel_organizational import UserCategory, UserType
from ...service.common import (SecurityConstant, UserType, find_data,
                               get_current_organization_id,
                               get_current_role_id, get_current_user_id,
                               get_info, get_str_to_age, get_string_time,
                               get_user_id_or_false, insert_data,
                               insert_many_data, update_data)
from ...service.constant import (AccountStatus, AccountType, PayStatue,
                                 PayType, plat_id)
from ...service.mongo_bill_service import MongoBillFilter
from ...service.welfare_institution.accommodation_process import CheckInStatus


'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-10-31 15:50:52
@LastEditors: Please set LastEditors
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 9:52:29 am
修改者: ymq(ymq) - <<email>>

说明
 1、用户管理服务
'''


class AppPageConfigService(MongoService):
    '''app页面设置'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.import_excel_servie = ImportExcelService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_app_page_config_list(self, org_list, condition, page, count):
        '''APP页面设置'''
        res = 'Fail'

        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'font_size': 1,
                      'font_color': 1,
                      'font_family': 1,
                      'remark': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_App_Page', page, count)

        return res

    def update_app_page_config(self, data):
        '''新增APP页面设置'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.appPage.value, data, 'PT_App_Page')
            if bill_id:
                res = 'Success'
        else:

            data['modify_date'] = get_cur_time()

            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.appPage.value, data, 'PT_App_Page')
            if bill_id:
                res = 'Success'
        return res

    def delete_app_page_config(self, ids):
        ''' 删除APP页面设置 '''
        res = '禁止删除'
        return res

    def get_app_config_list(self, org_list, condition, page, count):
        '''APP页面设置'''
        res = 'Fail'

        keys = ['id', 'type']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('type') == values['type']))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'type': 1,
                      'data': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_App_Config', page, count)

        return res

    def update_app_config(self, data):
        '''新增APP页面设置'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.appConfig.value, data, 'PT_App_Config')
            if bill_id:
                res = {
                    'code': 'Success',
                }
        else:
            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.appConfig.value, data_info, 'PT_App_Config')
            if bill_id:
                res = {
                    'code': 'Success',
                    'id': data_info['id']
                }
        return res

    def get_app_privacy_agree_list(self, org_list, condition, page, count):
        '''APP隐私查询'''
        res = 'Fail'

        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id']))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'ip': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Privacy_Agree', page, count)

        return res

    def delete_app_privacy_agree(self, condition):
        '''删除隐私'''
        ids = []
        if isinstance(condition, str):
            ids.append({"id": condition})
        else:
            for id_str in condition:
                ids.append({"id": id_str})
        self.bill_manage_service.add_bill(OperationType.delete.value,
                                          TypeId.appPrivacyAgree.value, ids, 'PT_Privacy_Agree')
        res = 'Success'
        return res
