import base64
import datetime
import hashlib
import json
import math
import os
import time
import urllib
import urllib.request
import uuid
from enum import Enum

import pandas as pd
import requests

from server.pao_python.pao.data import dataframe_to_list, date_to_string
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...models.financial_manage import FinancialAccount
from ...pao_python.pao.data import get_cur_time, process_db, string_to_date
from ...pao_python.pao.remote import JsonRpc2Error
from ...service.buss_mis.financial_account import FinancialAccountObject
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.buss_pub.import_excel import ImportExcelService
from ...service.buss_pub.personnel_organizational import UserCategory, UserType
from ...service.common import (SecurityConstant, UserType, find_data,
                               get_current_organization_id,
                               get_current_role_id, get_current_user_id,
                               get_info, get_str_to_age, get_string_time,
                               get_user_id_or_false, insert_data,
                               insert_many_data, update_data)
from ...service.constant import (AccountStatus, AccountType, PayStatue,
                                 PayType, plat_id)
from ...service.mongo_bill_service import MongoBillFilter
from ...service.welfare_institution.accommodation_process import CheckInStatus


'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-10-31 15:50:52
@LastEditors: Please set LastEditors
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 9:52:29 am
修改者: ymq(ymq) - <<email>>

说明
 1、用户管理服务
'''


class MaterielManageService(MongoService):
    '''物料管理'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_units_list(self, org_list, condition, page, count):
        '''计量单位'''
        res = 'Fail'

        keys = ['id', 'name', 'code']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
            & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'code': 1,
                      'name': 1,
                      'remark': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Unit', page, count)

        return res

    def update_units(self, data):
        '''新增计量单位'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.units.value, data, 'PT_Unit')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.units.value, data, 'PT_Unit')
            if bill_id:
                res = 'Success'
        return res

    def delete_units(self, ids):
        ''' 删除计量单位 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.units.value, {'id': i}, 'PT_Unit')
        if bill_id:
            res = 'Success'
        return res

    def get_thing_sort_list(self, org_list, condition, page, count):
        '''物料分类'''
        res = 'Fail'

        keys = ['id', 'name', 'code']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
            & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'code': 1,
                      'name': 1,
                      'remark': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Thing_Sort', page, count)

        return res

    def update_thing_sort(self, data):
        '''新增物料分类'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.thingSort.value, data, 'PT_Thing_Sort')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.thingSort.value, data, 'PT_Thing_Sort')
            if bill_id:
                res = 'Success'
        return res

    def delete_thing_sort(self, ids):
        ''' 删除物料分类 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.thingSort.value, {'id': i}, 'PT_Thing_Sort')
        if bill_id:
            res = 'Success'
        return res

    def get_thing_archives_list(self, org_list, condition, page, count):
        '''物料档案'''
        res = 'Fail'

        keys = ['id', 'name', 'code', 'sort_id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('sort_id') == values['sort_id'])
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Unit', 'unit_id', 'id', 'unit_info')\
            .lookup_bill('PT_Thing_Sort', 'sort_id', 'id', 'sort_info')\
            .add_fields({
                'unit_name': self.ao.array_elemat("$unit_info.name", 0),
                'sort_name': self.ao.array_elemat("$sort_info.name", 0)
            })\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'code': 1,
                      'name': 1,
                      'sort_id': 1,
                      'sort_name': 1,
                      'unit_id': 1,
                      'unit_name': 1,
                      'price': 1,
                      'promotion_price': 1,
                      'specifications': 1,
                      'model': 1,
                      'manufactor': 1,
                      'purchase_price': 1,
                      'picture': 1,
                      'remark': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Thing_Archives', page, count)

        return res

    def update_thing_archives(self, data):
        '''档案'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.thingArchives.value, data, 'PT_Thing_Archives')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.thingArchives.value, data, 'PT_Thing_Archives')
            if bill_id:
                res = 'Success'
        return res

    def delete_thing_archives(self, ids):
        ''' 删除物料分类 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.thingArchives.value, {'id': i}, 'PT_Thing_Archives')
        if bill_id:
            res = 'Success'
        return res