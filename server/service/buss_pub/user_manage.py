'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-28 16:02:43
@LastEditTime: 2019-08-31 16:55:18
@LastEditors: Please set LastEditors
'''

from ...pao_python.pao.data import get_cur_time
from ...service.common import get_info, get_random_id
import hashlib


class AccountData():
    '''账户对象'''

    def __init__(self, account_id, mobile, account='', email=''):
        '''构造函数'''
        self.id = account_id
        self.moblie = mobile
        self.account = account
        self.email = email

    def to_dict(self):
        return self.__dict__


class AccountRoleData():
    '''账户权限对象'''

    def __init__(self, account_id, role_id):
        '''构造函数'''
        self.id = get_random_id()
        self.account_id = account_id
        self.role_id = role_id
        self.date = get_cur_time()

    def to_dict(self):
        return self.__dict__


class LoginAuthenticateData():
    '''账户登录对象'''

    def __init__(self, auth_id, password):
        '''构造函数'''
        self.account_id = auth_id
        new_password_tep = hashlib.sha256(password.encode('utf-8'))
        new_password = new_password_tep.hexdigest()
        self.auth_token = new_password
        self.id = get_random_id()

    def to_dict(self):
        return self.__dict__


class RoleData():
    '''角色对象'''

    def __init__(self, role_id, role_name, permission, organization_id):
        '''构造函数'''
        self.id = role_id
        self.role_name = role_name
        self.permission = permission
        self.comment = role_name
        self.organization = organization_id

    def to_dict(self):
        return self.__dict__
