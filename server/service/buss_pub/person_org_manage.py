import base64
import datetime
import hashlib
import json
import math
import os
import time
import urllib
import urllib.request
import uuid
from enum import Enum

import pandas as pd
import requests

from server.pao_python.pao.data import dataframe_to_list, date_to_string
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...models.financial_manage import FinancialAccount
from ...pao_python.pao.data import get_cur_time, process_db, string_to_date
from ...pao_python.pao.remote import JsonRpc2Error
from ...service.buss_mis.financial_account import FinancialAccountObject
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.buss_pub.import_excel import ImportExcelService
from ...service.welfare_institution.hotel_zone import hotelZoneService
from ...service.buss_pub.personnel_organizational import UserCategory, UserType
from ...service.common import (SecurityConstant, UserType, find_data,
                               get_current_organization_id,
                               get_current_role_id, get_current_user_id,
                               get_info, get_str_to_age, get_string_time,
                               get_user_id_or_false, insert_data,
                               insert_many_data, update_data, get_common_project, get_client_ip)
from ...service.constant import (AccountStatus, AccountType, PayStatue,
                                 PayType, plat_id)
from ...service.mongo_bill_service import MongoBillFilter
from ...service.welfare_institution.accommodation_process import CheckInStatus
from ...service.common import GetInformationByIdCard

import time

'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-10-31 15:50:52
@LastEditors: Please set LastEditors
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 9:52:29 am
修改者: ymq(ymq) - <<email>>

说明
 1、用户管理服务
'''


class PersonOrgManageService(MongoService):
    '''人员及组织机构管理'''

    user_collection = 'PT_User'
    role_collection = 'PT_Role'
    permission_collection = 'PT_Permission'
    business_collection = 'PT_Business_Area'
    plat_visitor_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
    project_condition = {
        '_id': 0,
        'name': '$user_info.name',
        'id': '$user_info.id',
        'organization_id': '$user_info.organization_id',
        'id_card': '$user_info.id_card',
        'id_card_type': '$user_info.id_card_type',
        'personnel_info.personnel_category': '$user_info.personnel_info.personnel_category',
        'personnel_info.personnel_classification': '$user_info.personnel_info.personnel_classification',
        'personnel_category': '$user_info.personnel_info.personnel_category',
        'personnel_classification': '$user_info.personnel_info.personnel_classification',
        'personnel_info.sex': '$user_info.personnel_info.sex',
        'personnel_info.date_birth': '$user_info.personnel_info.date_birth',
        'telephone': '$user_info.personnel_info.telephone',
        'remarks': '$user_info.personnel_info.remarks',
        'card_number': '$user_info.personnel_info.card_number',
        'family_name': '$user_info.personnel_info.family_name',
        'nation': '$user_info.personnel_info.nation',
        'card_name': '$user_info.personnel_info.card_name',
        'native_place': '$user_info.personnel_info.native_place',
        'id_card_address': '$user_info.personnel_info.id_card_address',
        'account_name': '$user_info.login_info.login_check.account_name',
        # 'password': '$user_info.login_info.login_check.password',
        # 'contacts_telephone': '$user_info.personnel_info.contacts_telephone',
        # 'contacts': '$user_info.personnel_info.contacts',
        'address': '$user_info.personnel_info.address',
        'role_id': '$user_info.personnel_info.role_id',
        'admin_area_id': '$user_info.admin_area_id',
        'date_birth': '$user_info.personnel_info.date_birth',
        'sex': '$user_info.personnel_info.sex',
        'create_date': '$user_info.create_date'
        # 'personnel_info': '$user_info.personnel_info'
    }

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.import_excel_servie = ImportExcelService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.hotel_zone_service = hotelZoneService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def update_org_info_record(self, condition):
        # 机构信息档案表
        table = 'PT_Org_Info_Record'
        if 'id' in condition:
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.orgInfoRecord.value, condition, table)
        else:
            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.orgInfoRecord.value, condition, table)
        return 'Success'

    def get_current_org_info(self):
        '''获取当前机构信息'''
        user = self.get_current_user_info()
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == user[0]['organization_id'])\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        return res

    def get_org_info_record_list(self, org_list, condition, page, count):
        '''获取机构信息档案列表'''
        user_id = get_current_user_id(self.session)
        keys = ['name', 'id_card', 'id', 'tel']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['id']
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .lookup_bill('PT_Administration_Division', 'org_info_record.admin_area_id', 'id', 'div_name')\
            .add_fields({'org_name': "$org.name"})\
            .add_fields({'ser_zoom_info': {'id': "$div_name.id", 'name': "$div_name.name"}})\
            .project({'_id': 0, 'org': 0, 'div_name': 0})
        res = self.page_query(_filter, 'PT_Org_Info_Record', page, count)
        return res

    def get_charge_list(self, org_list, condition, page, count):
        '''获取收费类型列表'''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['id']
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(_filter, 'PT_Fee_Type', page, count)
        return res

    def get_operate_sitation_statistic(self, org_list, condition, page, count):
        '''获取运营情况统计列表'''
        condition['personnel_category'] = '幸福院'
        keys = ['id', 'name', 'organization_nature', 'admin_area_id', 'type',
                'personnel_category', 'personnel_type', 'lat', 'lon', 'sortByDistance', 'contract_status', 'star_level']
        values = self.get_value(condition, keys)

        # TODO: 当年
        _filter = MongoBillFilter()
        if org_list != [None]:
            _filter.match((C('id').inner(org_list)))
        _filter.match_bill((C('id') == values['id'])
                           & (C('personnel_type') == '2')
                           & (C('name').like(values['name']))
                           & (C('organization_info.star_level') == values['star_level'])
                           & (C('admin_area_id') == values['admin_area_id'])
                           & (C('organization_info.contract_status') == values['contract_status'])
                           & (C('organization_info.organization_nature').like(values['organization_nature']))
                           & (C('personnel_type') == values['personnel_type'])
                           & (C('organization_info.personnel_category') == values['personnel_category']))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_infos')\
            .add_fields({'org_name': '$organization_infos.name'})\
            .lookup_bill('PT_Activity', 'id', 'organization_id', 'organization_activity')\
            .add_fields({'activity_count': self.ao.size('$organization_activity'), })\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'division_info')\
            .add_fields({"division_name": self.ao.array_elemat("$division_info.name", 0)})\
            .project({'_id': 0, 'bed._id': 0, 'chenk_in': 0, 'service_product': 0,  'user._id': 0,
                      'organization_activity': 0, 'organization_infos': 0, 'organization_participate': 0, 'division_info': 0})
        res = self.page_query(_filter, 'PT_User', page, count)

        return res

    def update_charge_list(self, permission, condition):
        '''更新收费类型列表'''
        table = 'PT_Fee_Type'
        if 'id' in condition:
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.chargeType.value, condition, table)
        else:
            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.chargeType.value, condition, table)
        return 'Success'

    def get_month_salary_list(self, org_list, condition, page, count):
        '''获取工作人员列表'''
        keys = ['name', 'id_card', 'id',
                'organization_name', 'organization_id']

        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User', 'worker_id', 'id', 'user')\
            .match_bill((C('id') == values['id']))\
            .inner_join_bill('PT_User', 'user.organization_id', 'id', 'org')\
            .match((C("org.name").like(values['organization_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'user._id': 0, 'setRole._id': 0, 'new_field._id': 0, 'org._id': 0})

        res = self.page_query(_filter, 'PT_Income', page, count)
        # for i, x in enumerate(res['result']):
        #     data = res['result'][i]['personnel_info']
        #     if 'date_birth' in data.keys() and data['date_birth'] != '' and data['date_birth'] != None:
        #         date_res = get_string_time(data['date_birth'], '%Y-%m-%d')
        #         data['date_birth'] = date_res
        #         data['age'] = get_str_to_age(date_res)
        return res

    def update_month_salary(self, permission, condition):
        '''更新月收入统计列表'''
        table = 'PT_Income'
        if 'id' in condition:
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.Income.value, condition, table)
        else:
            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.Income.value, condition, table)
        return 'Success'

    def delete_charge_list(self, permission, condition):
        '''删除收费类型列表'''
        ids = []
        if isinstance(condition, str):
            ids.append({"id": condition})
        else:
            for id_str in condition:
                ids.append({"id": id_str})
        self.bill_manage_service.add_bill(OperationType.delete.value,
                                          TypeId.chargeType.value, ids, 'PT_Fee_Type')
        res = 'Success'
        return res

    def get_cur_org_info(self):
        cur_org_id = get_current_organization_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == cur_org_id)\
               .project({'_id': 0})
        res = self.query(_filter, self.user_collection)
        return res

    def get_current_user_info(self):
        '''获取当前用户信息'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == user_id)\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_list')\
            .add_fields({
                'org_name': '$org_list.name',
            })\
            .project({
                '_id': 0,
                'login_info.login_check.password': 0,
                'login_info.login_type': 0,
                'organization_info.qualification_info': 0,
                **get_common_project({"org_list"})
            })
        res = self.query(_filter, self.user_collection)
        # return res

        if len(res) > 0:
            # 查询当前登录的角色ID
            role_id = get_current_role_id(self.session)
            org_id = get_current_organization_id(self.session)
            res[0]['org_id'] = org_id
            # 找到角色ID对应的role_name
            _filter_role = MongoBillFilter()
            _filter_role.match_bill(C('id') == role_id)\
                .project({'_id': 0, })
            role_id_res = self.query(_filter_role, 'PT_Role')

            # 补全角色ID
            res[0]['role_id'] = role_id

            # 补全角色名
            if len(role_id_res) > 0 and 'name' in role_id_res[0]:
                res[0]['db_role_name'] = role_id_res[0]['name']
                res[0]['db_role_type'] = role_id_res[0]['role_type']
            else:
                res[0]['db_role_name'] = '暂无'
                res[0]['db_role_type'] = '暂无'

        return res

    def get_user_elder_birth_list(self, org_list, condition, page, count):

        keys = ['likestr', 'date_scope', 'old_type_name']

        # 查询区域信息
        area_ids = []
        area_data = {}
        _filter_area = MongoBillFilter()
        _filter_area.project({'_id': 0})
        res_area = self.query(_filter_area, 'PT_Administration_Division')
        if len(res_area) > 0:
            for area_info in res_area:
                area_data[area_info['id']] = area_info
                area_ids.append(area_info['id'])

        # 查询人员类型表数据
        classification_ids = []
        classification_data = {}
        _filter_classification = MongoBillFilter()
        _filter_classification.project({'_id': 0})
        res_classification = self.query(
            _filter_classification, 'PT_Personnel_Classification')
        if len(res_classification) > 0:
            for classification in res_classification:
                classification_data[classification['id']] = classification
                classification_ids.append(classification['id'])

        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '1')
                           & (C('personnel_info.date_birth').filterDate())
                           & (C('personnel_info.personnel_category') == '长者'))\
            .add_fields({
                'sex': '$personnel_info.sex',
                'address': '$personnel_info.address',
                'phone': '$personnel_info.telephone',
                'date_birth': '$personnel_info.date_birth',
                'date_birth_obj': self.ao.date_to_parts('$personnel_info.date_birth'),
            })\
            .add_fields({
                'month': '$date_birth_obj.month',
                'day': '$date_birth_obj.day',
                'month_day': self.ao.concat([self.ao.to_string('$date_birth_obj.month'), '-', self.ao.to_string('$date_birth_obj.day')]),
            })

        if 'date_scope' in condition:
            # 时间
            if condition['date_scope'] == '今天':
                _filter.match_bill((C('month') == get_cur_time().month) & (
                    C('day') == get_cur_time().day))
            elif condition['date_scope'] == '本周':

                current_day = get_cur_time().day
                current_day = 29
                current_month = get_cur_time().month

                match_array = []
                match_array.append(str(current_month) + '-' + str(current_day))
                for i in range(1, 7):
                    current_day += 1
                    if (current_month == 2 and current_day > 28) or (current_month in [1, 3, 5, 7, 8, 10, 12] and current_day > 31) or (current_day > 30):
                        current_day = 1
                        current_month += 1
                        if current_month == 13:
                            current_month = 1
                    match_array.append(str(current_month) +
                                       '-' + str(current_day))
                    _filter.match_bill((C('month_day').inner(match_array)))

            elif condition['date_scope'] == '本月':
                _filter.match_bill((C('month') == get_cur_time().month))

        _filter.project({
            '_id': 0,
            'id': 1,
            'name': 1,
            'sex': 1,
            'id_card': 1,
            'address': 1,
            'date_birth': 1,
            'phone': 1,
            'admin_area_id': 1,
            'personnel_info': 1
        })
        res = self.page_query(
            _filter, 'PT_User', page, count)
        for index in range(len(res['result'])):
            if 'personnel_info' in res['result'][index].keys() and 'personnel_classification' in res['result'][index]['personnel_info'].keys() and res['result'][index]['personnel_info']['personnel_classification'] != None and res['result'][index]['personnel_info']['personnel_classification'] != '':
                res['result'][index]['old_type_name'] = classification_data[res['result']
                                                                            [index]['personnel_info']['personnel_classification']]['name']
            if 'admin_area_id' in res['result'][index].keys():
                res['result'][index]['area_name'] = area_data[res['result']
                                                              [index]['admin_area_id']]['name']

        return res

    def get_user_elder_list(self, org_list, condition, page, count):
        '''获取当前登录账号入住长者列表 现在的用法已经转变为查询全部长者了'''
        keys = ['name', 'id_card', 'id', 'tel', 'die_state',
                'is_member', 'marriage_state', 'address', 'organization_name', 'old_type', 'old_type_name', 'personnel_classification', 'admin_name']

        values = self.get_value(condition, keys)

        admin_ids = []
        admin_data = {}
        if 'admin_name' in condition.keys():
            # 查询行政区划
            _filter_admin = MongoBillFilter()
            _filter_admin.match_bill((C('name').like(values['admin_name'])))\
                .project({'_id': 0, 'id': 1, 'name': 1})
            res_admin = self.query(
                _filter_admin, 'PT_Administration_Division')
            if len(res_admin) > 0:
                for admin in res_admin:
                    admin_data[admin['id']] = admin
                    admin_ids.append(admin['id'])
        # 查询人员类型表数据
        classification_ids = []
        classification_data = {}
        _filter_classification = MongoBillFilter()
        _filter_classification.match_bill((C('type') == values['old_type']) & (C('name') == values['old_type_name']))\
            .project({'_id': 0, 'id': 1, 'name': 1, 'type': 1})
        res_classification = self.query(
            _filter_classification, 'PT_Personnel_Classification')
        if len(res_classification) > 0:
            for classification in res_classification:
                classification_data[classification['id']] = classification
                classification_ids.append(classification['id'])

        # 查询工作人员人员数据
        user_data = {}
        _filter_user = MongoBillFilter()
        _filter_user.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员'))\
            .project({'_id': 0, 'id': 1, 'name': 1, 'organization_id': 1})
        res_user = self.query(
            _filter_user, 'PT_User')
        if len(res_user) > 0:
            for user in res_user:
                user_data[user['id']] = user

        # 查询机构数据
        org_ids = []
        org_data = {}
        _filter_org = MongoBillFilter()
        _filter_org.match_bill((C('personnel_type') == '2') & (C('name').like(values['organization_name'])))\
            .project({'_id': 0, 'id': 1, 'name': 1, 'admin_area_id': 1})
        res_org = self.query(
            _filter_org, 'PT_User')
        if len(res_org) > 0:
            for org in res_org:
                org_data[org['id']] = org
                org_ids.append(org['id'])

        _filter = MongoBillFilter()
        year = datetime.datetime.now().year
        _filter.match_bill((C('organization_id').inner(org_list))
                           & (C('personnel_info.personnel_classification') == values['personnel_classification'])
                           & (C('personnel_type') == UserType.Personnel)
                           & (C('personnel_info.personnel_category') == '长者')
                           & (C('personnel_info.telephone') == values['tel'])
                           & (C('name').like(values['name']))
                           & (C('id_card').like(values['id_card']))
                           & (C('id') == values['id'])
                           & (C('personnel_info.die_state') == values['die_state'])
                           & (C('personnel_info.is_member') == values['is_member'])
                           & (C('personnel_info.marriage_state') == values['marriage_state'])
                           & (C('personnel_info.address').like(values['address'])))
        if 'admin_name' in condition.keys():
            _filter.match_bill(C('admin_area_id').inner(admin_ids))
        if 'organization_name' in condition.keys():
            _filter.match_bill(C('organization_id').inner(org_ids))
        # 如果有过滤长者类型，就加这个条件
        if 'old_type_name' in condition or 'old_type' in condition:
            _filter.match_bill(
                (C('personnel_info.personnel_classification').inner(classification_ids)))

        _filter.sort({'modify_date': -1})\
            .project({'login_info.login_check.password': 0, **get_common_project()})

        res = self.page_query(_filter, 'PT_User', page, count)
        res_admin_ids = []
        for i, x in enumerate(res['result']):
            res_admin_ids.append(res['result'][i]['admin_area_id'])
            if 'personnel_classification' in res['result'][i]['personnel_info'] and res['result'][i]['personnel_info']['personnel_classification'] in classification_data.keys():
                res['result'][i]['old_type_name'] = classification_data[res['result']
                                                                        [i]['personnel_info']['personnel_classification']]['name']
                res['result'][i]['old_type'] = classification_data[res['result']
                                                                   [i]['personnel_info']['personnel_classification']]['type']
            if 'organization_id' in res['result'][i] and res['result'][i]['organization_id'] in org_data.keys():
                res['result'][i]['org'] = org_data[res['result']
                                                   [i]['organization_id']]

            if 'founders' in res['result'][i] and res['result'][i]['founders'] in user_data.keys():
                res['result'][i]['founders_info'] = user_data[res['result']
                                                              [i]['founders']]
                if 'name' in user_data[res['result'][i]['founders']].keys():
                    res['result'][i]['founders'] = user_data[res['result']
                                                             [i]['founders']]['name']

            if 'last_modifier' in res['result'][i] and res['result'][i]['last_modifier'] in user_data.keys():
                res['result'][i]['last_modifier_info'] = user_data[res['result']
                                                                   [i]['last_modifier']]
                if 'name' in user_data[res['result'][i]['last_modifier']].keys():
                    res['result'][i]['last_modifier'] = user_data[res['result']
                                                                  [i]['last_modifier']]['name']
        if 'admin_name' not in condition.keys():
            # 查询行政区划
            _filter_admin = MongoBillFilter()
            _filter_admin.match_bill((C('id').inner(res_admin_ids)))\
                .project({'_id': 0, 'id': 1, 'name': 1})
            res_admin = self.query(
                _filter_admin, 'PT_Administration_Division')
            if len(res_admin) > 0:
                for admin in res_admin:
                    admin_data[admin['id']] = admin
        for i, x in enumerate(res['result']):
            if 'admin_area_id' in res['result'][i] and res['result'][i]['admin_area_id'] in admin_data.keys():
                res['result'][i]['admin_name'] = admin_data[res['result']
                                                            [i]['admin_area_id']]['name']
        return res

    def get_user_elder_date_list(self, org_list, condition, page, count):
        '''获取当前登录账号入住长者列表 现在的用法已经转变为查询全部长者了'''
        res = ''
        return res

    def get_elder_list_all(self, condition, page, count):
        keys = ['name', 'id_card', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C("personnel_type") == '1')
            & (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('id_card').like(values['id_card']))
        )\
            .project({"_id": 0})
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def get_org_list_all(self, condition):
        keys = ['organization_type', 'id', 'personnel_category']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_info.personnel_category')
             == values['personnel_category'])
            & (C("id") == values['id'])
        )\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        return res

    def get_elder_list(self, org_list, condition, page, count):
        '''查询长者列表，单纯查询长者的信息'''
        current_date = get_cur_time()
        keys = ['name', 'id_card', 'id', 'tel',
                'type', 'sex', 'child_num', 'days']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_info.personnel_category') == '长者')
                           & (C('organization_id').inner(org_list))
                           & (C('personnel_type') == UserType.Personnel)
                           & (C('personnel_info.telephone') == values['tel'])
                           & (C('personnel_info.sex') == values['sex'])
                           & (C('personnel_info.child_num').like(values['child_num']))
                           & (C('name').like(values['name']))
                           & (C('id_card') == values['id_card'])
                           & (C('id') == values['id'])
                           )\
            .sort({'modify_date': -1})
        if 'type' in condition.keys() and condition['type'] == 'eldeFile':
            _filter.lookup_bill('PT_Old_Age_Allowance', 'id_card', 'id_card_num', 'rz_list')\
                .add_fields({
                    'new_date': self.ao.maximum('$rz_list.create_date')
                })\
                .add_fields({
                    'new_record': self.ao.array_filter('$rz_list', 'record', (F('$record.create_date') == '$new_date').f)
                })\
                .lookup_bill('PT_Activity_Participate', 'id', 'user_id', 'participate')\
                .add_fields({'participate_num': self.ao.size('$participate')})\
                .add_fields({'time_stamp': ((F('new_date')-current_date)*(-1)).f})\
                .add_fields({'days': self.ao.floor((F('time_stamp').__truediv__(86400000)).f)})
            if 'days' in condition.keys() and condition['days'] == '90天以上未认定':
                _filter.match_bill((C('days') > 90))
            elif 'days' in condition.keys() and condition['days'] == '61-90之间认定过':
                _filter.match_bill((C('days') > 60) & (C('days') <= 90))
            elif 'days' in condition.keys() and condition['days'] == '60天内认定过':
                _filter.match_bill((C('days') <= 60))
            elif 'days' in condition.keys() and condition['days'] == '无法认定':
                _filter.match_bill((C('days') == None))
            _filter.project({
                'participate': 0,
                **get_common_project({'rz_list', 'new_record', 'activity_list'})
            })
        _filter.project(
            {'login_info.login_check.password': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def get_elder_list_by_org(self, org_list, condition, page, count):
        '''获取当前机构下入住的长者'''
        keys = ['name', 'id_card', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id') == get_current_organization_id(self.session)) & (C('state') == CheckInStatus.check_in.value))\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
            .match((C('user_info.personnel_info.personnel_category') == '长者')
                   & (C('user_info.name').like(values['name']))
                   & (C('user_info.id_card').like(values['id_card']))
                   & (C('user_info.id') == values['id']))\
            .project(self.project_condition)
        res = self.page_query(_filter, 'IEC_Check_In', page, count)
        return res

    def get_user_worker_list(self, org_list, condition, page, count):
        '''获取工作人员列表'''
        keys = ['name', 'id_card', 'id',
                'organization_name', 'organization_id', 'work_state', 'role_name']

        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('name').like(values['name']))
                           & (C('id_card').like(values['id_card']))
                           & (C('id') == values['id'])
                           & (C('personnel_info.work_state') == values['work_state'])
                           & (C('personnel_info.personnel_category') == '工作人员'))\
               .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
               .lookup_bill('PT_Set_Role', 'id', 'principal_account_id', 'setRole')\
               .lookup_bill('PT_Role', 'personnel_info.role_id', 'id', 'Role')\
               .add_fields({'role_name': '$Role.name'})\
               .lookup_bill('PT_Income', 'id', 'worker_id', 'user_income')\
               .add_fields({'new_field': self.ao.array_filter("$setRole", "aa", ((F('$aa.role_of_account_id').inner(org_list))).f)})\
               .match((C("new_field") != None) & (C("new_field") != []))\
               .match((C("org.name").like(values['organization_name'])))\
               .match((C("role_name").like(values['role_name'])))\
               .sort({'modify_date': -1})\
               .project({'_id': 0, 'user_income._id': 0, 'setRole._id': 0, 'new_field._id': 0, 'org._id': 0, 'Role._id': 0})

        if 'current_org_id' in condition:
            org_id = get_current_organization_id(self.session)
            _filter.match_bill((C('organization_id') == org_id))
        res = self.page_query(_filter, 'PT_User', page, count)
        for i, x in enumerate(res['result']):
            data = res['result'][i]['personnel_info']
            if 'date_birth' in data.keys() and data['date_birth'] != '' and data['date_birth'] != None:
                date_res = get_string_time(data['date_birth'], '%Y-%m-%d')
                data['date_birth'] = date_res
                data['age'] = get_str_to_age(date_res)
        return res

    def get_worker_list(self, org_list, condition, page, count):
        '''获取工作人员列表'''
        keys = ['name', 'id_card', 'id',
                'organization_name', 'organization_id', 'work_state', 'role_name', 'account_name', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list)
                            & (C('name').like(values['name']))
                            & (C('id_card').like(values['id_card']))
                            & (C('id') == values['id'])
                            & (C('login_info.login_check.account_name').like(values['account_name']))
                            & (C('personnel_info.work_state') == values['work_state'])
                            & (C('personnel_info.personnel_category') == '工作人员')))\
            .sort({'modify_date': -1})\
            .project({'login_info.login_check.password': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def get_user_worker_list_yh(self, org_list, condition, page, count):
        '''获取工作人员列表'''
        keys = ['name', 'id_card', 'id',
                'organization_name', 'organization_id', 'work_state', 'role_name', 'account_name', 'type']
        if condition.get('organization_name') and '(' in condition.get('organization_name'):
            condition['organization_name'] = condition['organization_name'].split('(')[
                0]
        values = self.get_value(condition, keys)
        try:
            # 查询机构
            org_ids = []
            org_data = {}

            _filter_org = MongoBillFilter()
            _filter_org.match_bill(
                (C('personnel_type') == '2')
                & (C('organization_info.personnel_category') == values['type'])
                & (C('name').like(values['organization_name']))
            )\
                .project({**get_common_project(), 'login_info': 0, })

            res_org = self.query(
                _filter_org, 'PT_User')
            if len(res_org) > 0:
                for org in res_org:
                    org_data[org['id']] = org
                    org_ids.append(org['id'])

            # # 查询设置角色表
            # t11 = time.time()
            # print('org_list>>>', org_list)
            # set_role_ids = []
            # _filter_set_role = MongoBillFilter()
            # _filter_set_role.match_bill((C('role_of_account_id').inner(org_list)) & (C('role_of_account_id').inner(org_ids)))\
            #     .project({'_id': 0})
            # res_set_role = self.query(
            #     _filter_set_role, 'PT_Set_Role')
            # if len(res_set_role) > 0:
            #     for set_role in res_set_role:
            #         set_role_ids.append(set_role['principal_account_id'])

            # t12 = time.time()
            # print('最后查询人员数据》》', t12-t11)
            # print('set_role_ids>>', set_role_ids)
            _filter = MongoBillFilter()
            # if condition.get('organization_name'):
            _filter.match_bill(C('organization_id').inner(org_ids))
            _filter.match_bill((C('organization_id').inner(org_list)
                                & (C('name').like(values['name']))
                                & (C('id_card').like(values['id_card']))
                                & (C('id') == values['id'])
                                & (C('login_info.login_check.account_name').like(values['account_name']))
                                & (C('personnel_info.work_state') == values['work_state'])
                                & (C('personnel_info.personnel_category') == '工作人员')))\
                .sort({'modify_date': -1})\
                .project({'login_info.login_check.password': 0, **get_common_project()})
            if 'current_org_id' in condition:
                org_id = get_current_organization_id(self.session)
                _filter.match_bill((C('organization_id') == org_id))
            res = self.page_query(_filter, 'PT_User', page, count)
            res_org_ids = []
            res_user_income_ids = []
            res_founders_ids = []
            res_modifier_ids = []
            for i, x in enumerate(res['result']):
                res_org_ids.append(res['result'][i]['organization_id'])
                res_user_income_ids.append(res['result'][i]['id'])
                if 'founders' in res['result'][i].keys():
                    res_founders_ids.append(res['result'][i]['founders'])
                if 'last_modifier' in res['result'][i].keys():
                    res_modifier_ids.append(res['result'][i]['last_modifier'])
                data = res['result'][i]['personnel_info']
                if 'date_birth' in data.keys() and data['date_birth'] != '' and data['date_birth'] != None:
                    date_res = get_string_time(data['date_birth'], '%Y-%m-%d')
                    data['date_birth'] = date_res
                    data['age'] = get_str_to_age(date_res)
            if len(res_org_ids) > 0:
                _filter_org = MongoBillFilter()
                _filter_org.match_bill((C('id').inner(res_org_ids)) | (C('id').inner(res_founders_ids)) | (C('id').inner(res_modifier_ids)))\
                    .project({**get_common_project()})
                res_org = self.query(
                    _filter_org, 'PT_User')
                if len(res_org) > 0:
                    for org in res_org:
                        org_data[org['id']] = org
            income_data = {}
            if len(res_user_income_ids) > 0:
                _filter_income = MongoBillFilter()
                _filter_income.match_bill((C('worker_id').inner(res_user_income_ids)))\
                    .project({**get_common_project()})
                res_income = self.query(
                    _filter_income, 'user_income')
                if len(res_income) > 0:
                    for income in res_income:
                        if income['worker_id'] in income_data:
                            income_data[income['worker_id']].append(income)
                        else:
                            income_data[income['worker_id']] = [income]
            for i, x in enumerate(res['result']):
                if 'organization_id' in res['result'][i] and res['result'][i]['organization_id'] in org_data.keys():
                    res['result'][i]['org'] = org_data[res['result']
                                                       [i]['organization_id']]
                if 'founders' in res['result'][i] and res['result'][i]['founders'] in org_data.keys():
                    if 'name' in org_data[res['result'][i]['founders']].keys():
                        res['result'][i]['founders'] = org_data[res['result']
                                                                [i]['founders']]['name']
                if 'last_modifier' in res['result'][i] and res['result'][i]['last_modifier'] in org_data.keys():
                    if 'name' in org_data[res['result'][i]['last_modifier']].keys():
                        res['result'][i]['last_modifier'] = org_data[res['result']
                                                                     [i]['last_modifier']]['name']
                if 'id' in res['result'][i] and res['result'][i]['id'] in income_data.keys():
                    res['result'][i]['user_income'] = income_data[res['result'][i]['id']]
        except Exception as e:
            self.logger.exception(e)
        return res

    def get_user_all_list(self, org_list, condition, page, count):
        # 查询单个
        if 'id' in condition:
            keys = ['id', 'name', 'id_card']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == values['id']) & (C('personnel_type') == '1') & (C('name').like(values['name'])) & (C('id_card') == values['id_card']))\
                .project({'login_info.login_check.password': 0, **get_common_project()})
            res = self.page_query(
                _filter, 'PT_User', page, count)
            return res
        '''获取当前机构的工作人员和平台游客列表'''
        keys = ['name', 'id_card', 'id', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('role_of_account_id').inner(org_list)) | (C('role_id') == self.plat_visitor_id))\
               .group({'user_id': '$principal_account_id'})\
               .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
               .match((C('user_info.name').like(values['name']))
                      & (C('user_info.id') == (values['id']))
                      & (C('user_info.id_card') == (values['id_card']))
                      & (C('user_info.personnel_info.personnel_category').like(values['type'])))\
               .project(self.project_condition)
        res = self.page_query(_filter, 'PT_Set_Role', page, count)
        for i, x in enumerate(res['result']):
            data = res['result'][i]['personnel_info']
            if 'date_birth' in data.keys() and data['date_birth'] != '' and data['date_birth'] != None:
                date_res = get_string_time(data['date_birth'], '%Y-%m-%d')
                data['date_birth'] = date_res
                data['age'] = get_str_to_age(date_res)
        return res

    def update_elder_info(self, condition):
        user = self.get_current_user_info()
        if len(user) > 0 and 'id' in user[0]:
            condition['last_modifier'] = user[0]['name']
        condition['modify_date'] = get_cur_time()
        extend_info = {}
        if 'personnel_info' in condition.keys() and 'card_number' in condition['personnel_info'].keys():
            extend_info['card_number'] = condition['personnel_info']['card_number']
        if 'personnel_info' in condition.keys() and 'card_name' in condition['personnel_info'].keys():
            extend_info['card_name'] = condition['personnel_info']['card_name']
        if 'admin_area_id' not in condition.keys():
            condition['admin_area_id'] = user[0]['admin_area_id']
        res = {'id': '', 'res': 'Fail'}
        if 'id' in condition.keys():
            # 这里要做一个判断是否已存在该人员的判断：没有登录账号信息则合并，有登录账号信息则提示已存在
            _filter = MongoBillFilter()
            _filter.match_bill((C('id_card') == condition['id_card']))\
                .project({'_id': 0, })
            query_res = self.query(_filter, 'PT_User')
            if len(query_res) > 0 and query_res[0]['id'] != condition['id']:
                return {'id': '', 'res': '该人员：' + condition['name'] + '已存在其他机构名下，无需重新注册，请联系平台管理员进行转移'}

            # 判断是否转换机构，如果转换机构，需要确保原机构已经办理了退住
            if len(query_res) > 0 and query_res[0]['organization_id'] != condition['organization_id']:
                _filter_bed = MongoBillFilter()
                _filter_bed.match_bill(
                    (C('residents_id') == query_res[0]['id'])
                    & (C('organization_id') == query_res[0]['organization_id'])
                )\
                    .project({'_id': 0, 'id': 1})
                res_bed = self.query(_filter_bed, 'PT_Bed')
                if len(res_bed):
                    return {'res': '长者' + query_res[0]['name']+','+query_res[0]['id_card'] + '在原机构还在住，请联系原机构进行退住办理'}

            # 这里判断当前账号是否已存在
            if 'account_name' in condition['login_info'][0]['login_check'].keys() and condition['login_info'][0]['login_check']['account_name'] != '':
                _filter = MongoBillFilter()
                _filter.add_fields({'account_name': self.ao.array_filter("$login_info", "info", ((F('info.login_check.account_name') == condition['login_info'][0]['login_check']['account_name'])).f)})\
                    .match_bill((C("account_name") != None) & (C("account_name") != []))\
                    .project({'_id': 0, })
                res_account = self.query(_filter, 'PT_User')
                if len(res_account) > 0 and res_account[0]['id'] != condition['id']:
                    return {'id': '', 'res': '登录账号：'+condition['login_info'][0]['login_check']['account_name'] + '已存在，不能重复，请联系管理员处理！'}
            # 初始密码
            new_password = 'Zhyl@2020'
            newpassword_tep = hashlib.sha256(
                new_password.encode('utf-8'))
            newpassword = newpassword_tep.hexdigest()
            # 查一下该人员的信息,延用该账号原本密码
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == condition['id']))\
                .project({'_id': 0})
            res_user = self.query(_filter, 'PT_User')
            if len(res_user) > 0 and 'login_info' in res_user[0].keys() and len(res_user[0]['login_info']) > 0:
                login_check = res_user[0]['login_info'][0]['login_check']
                if 'password' in login_check.keys():
                    password = res_user[0]['login_info'][0]['login_check']['password']
                    newpassword = password
            condition['login_info'][0]['login_check']['password'] = newpassword
            time = condition['personnel_info']['date_birth']
            if time != None and time != '':
                condition['personnel_info']['date_birth'] = string_to_date(
                    time)
            data = condition
            table = 'PT_User'
            if extend_info:
                # 查找长者真实账户id
                _filter = MongoBillFilter()
                _filter.match_bill((C('user_id') == condition['id'])
                                   & (C('account_type') == AccountType.account_real))\
                    .project({'_id': 0})
                res_real = self.query(_filter, FinancialAccount().name)
                if len(res_real) > 0:
                    data = [condition, {'id': res_real[0]
                                        ['id'], 'extend_info':extend_info}]
                    table = ['PT_User', FinancialAccount().name]

            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.user.value, data, table)
            # 新增或者编辑set_role表
            # 查找set_role表
            _filter = MongoBillFilter()
            _filter.match_bill((C('principal_account_id') == condition['id']))\
                .project({'_id': 0})
            res_set_role = self.query(_filter, 'PT_Set_Role')
            if(condition['personnel_info']['personnel_category'] == '长者'):
                set_role = {'role_id': '8ed260f6-e355-11e9-875e-a0a4c57e9ebe',
                            'principal_account_id': condition['id'], 'role_of_account_id': condition['organization_id']}
            else:
                set_role = {'role_id': condition['personnel_info']['role_id'],
                            'principal_account_id': condition['id'], 'role_of_account_id': condition['organization_id']}
            if len(res_set_role) > 0 and len(res_set_role) == 1:
                # 更新
                set_role['id'] = res_set_role[0]['id']
                flag = OperationType.update.value
            else:
                # 新增
                flag = OperationType.add.value

            # 如果改机构，需要把不属于原来机构的角色删掉
            # 如果改机构，要确保原机构已经退住
            if len(res_user) > 0 and res_user[0]['organization_id'] != condition['organization_id']:
                for item in res_set_role:
                    if item['role_of_account_id'] != condition['organization_id']:
                        self.bill_manage_service.add_bill(OperationType.delete.value,
                                                          TypeId.setRole.value, {'id': item['id']}, 'PT_Set_Role')

            bill_id = self.bill_manage_service.add_bill(
                flag, TypeId.setRole.value, set_role, 'PT_Set_Role')
            if bill_id:
                res = {'id': condition['id'], 'res': 'Success'}
        else:
            # 这里要做一个判断是否已存在该人员的判断：没有登录账号信息则合并，有登录账号信息则提示已存在
            _filter = MongoBillFilter()
            _filter.match_bill((C('id_card') == condition['id_card']))\
                .project({'_id': 0})
            query_res = self.query(_filter, 'PT_User')
            flag = True
            # 暂时开放 query_res[0]['org']['id'] != '8476556c-26ff-11ea-8c15-a0a4c57e9ebe'，解决福利院讲政府备案的长者转到其组织机构下的问题
            if len(query_res) > 0:
                if query_res[0]['organization_id'] == '8476556c-26ff-11ea-8c15-a0a4c57e9ebe':
                    condition['id'] = query_res[0]['id']
                    flag = False
                else:
                    return {'id': '', 'res': '该人员：' + condition['name'] + '已存在其他机构名下，无需重新注册，请联系平台管理员进行转移'}
            # 判断手机号码是否已占用
            if 'telephone' in condition['personnel_info'].keys() and condition['personnel_info']['telephone'] != '' and condition['personnel_info']['telephone']:
                _filter = MongoBillFilter()
                _filter.match_bill((C('personnel_info.telephone') == condition['personnel_info']['telephone']))\
                    .project({'_id': 0})
                has_phone = self.query(_filter, 'PT_User')
                if len(has_phone) > 0:
                    return {'id': '', 'res': '该手机号码：' + condition['personnel_info']['telephone'] + '已注册使用'}
            if flag:
                # 这里判断当前账号是否已存在
                condition['create_time'] = get_cur_time()
                condition['founders'] = user[0]['id']
                # 账户名
                account_name = ''
                if 'account_name' in condition['login_info'][0]['login_check'].keys() and condition['login_info'][0]['login_check']['account_name'] != '':
                    # 账户名
                    account_name = condition['login_info'][0]['login_check']['account_name']
                    _filter = MongoBillFilter()
                    _filter.add_fields({'account_name': self.ao.array_filter("$login_info", "info", ((F('$info.login_check.account_name') == condition['login_info'][0]['login_check']['account_name'])).f)}).match_bill((C("account_name") != None) & (C("account_name") != []))\
                        .project({'_id': 0, })
                    res_account = self.query(_filter, 'PT_User')
                    if len(res_account) > 0:
                        return {'id': '', 'res': '登录账号：' + condition['login_info'][0]['login_check']['account_name'] + '已存在，不能重复，请联系管理员处理！'}
                    self.bill_manage_service.insert_logs(
                        '平台运营方', '创建账户', '创建账户' + account_name)
                # 初始密码
                new_password = 'Zhyl@2020'
                newpassword_tep = hashlib.sha256(new_password.encode('utf-8'))
                newpassword = newpassword_tep.hexdigest()
                condition['login_info'][0]['login_check']['password'] = newpassword
                data_id = get_info({}, self.session)
                condition['id'] = data_id['id']
                time = condition['personnel_info']['date_birth']
                if time != None and time != '':
                    condition['personnel_info']['date_birth'] = string_to_date(
                        time)
                if 'organization_id' in condition.keys() and condition['organization_id'] != '':
                    org_id = condition['organization_id']
                else:
                    org_id = get_current_organization_id(self.session)
                    condition['organization_id'] = org_id
                if(condition['personnel_info']['personnel_category'] == '长者'):
                    set_role = {'role_id': '8ed260f6-e355-11e9-875e-a0a4c57e9ebe',
                                'principal_account_id': data_id['id'], 'role_of_account_id': org_id}
                else:
                    set_role = {'role_id': condition['personnel_info']['role_id'],
                                'principal_account_id': data_id['id'], 'role_of_account_id': org_id}

                self.bill_manage_service.add_bill(OperationType.add.value,
                                                  TypeId.setRole.value, set_role, 'PT_Set_Role')
                # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
                # 机构储蓄对象
                org_account_data = FinancialAccountObject(
                    AccountType.account_recharg_wel, data_id['id'], org_id, AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                org_account = get_info(
                    org_account_data.to_dict(), self.session)
                # app储蓄对象
                app_account_data = FinancialAccountObject(
                    AccountType.account_recharg_app, data_id['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                app_account = get_info(
                    app_account_data.to_dict(), self.session)
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data_id['id'], None, AccountStatus.normal, 3, AccountType.account_real, extend_info, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                # 补贴账户对象
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, data_id['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)

                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.user.value, [[condition], [org_account, app_account, real_account, subsidy_account]], ['PT_User', FinancialAccount().name])
                if bill_id:
                    # 增加创建日志
                    self.bill_manage_service.insert_logs(
                        '平台运营方', '创建账户', '创建账户' + account_name)
                    res = {'id': condition['id'], 'res': 'Success'}
            else:
                # 这里再去查询置角色表、账户表

                # 设置账户表
                _filter_financial = MongoBillFilter()
                _filter_financial.match_bill(C('user_id') == query_res[0]['id'])\
                    .project({'_id': 0})
                account = self.query(_filter_financial, 'PT_Financial_Account')
                # 设置角色表
                _filter_set_role = MongoBillFilter()
                _filter_set_role.match_bill(C('principal_account_id') == query_res[0]['id'])\
                    .project({'_id': 0})
                set_role = self.query(_filter_set_role, 'PT_Set_Role')

                if len(set_role) > 0:
                    pf = pd.DataFrame(set_role)
                    pf['role_of_account_id'] = condition['organization_id']
                    pf['organization_id'] = condition['organization_id']
                    set_role = dataframe_to_list(pf)
                # set_role = query_res[0]['set_role']
                # set_role['role_of_account_id'] = condition['organization_id']
                # set_role['organization_id'] = condition['organization_id']
                # account = query_res[0]['financial']
                if len(account) > 0:
                    pf = pd.DataFrame(account)
                    pf['organization_id'] = condition['organization_id']
                    account = dataframe_to_list(pf)
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value, TypeId.user.value, [
                    condition, set_role, account], ['PT_User', 'PT_Set_Role', FinancialAccount().name])
                if bill_id:
                    res = {'id': condition['id'], 'res': 'Success'}

        return res

    def delete_elder_info(self, condition):
        ids = []
        id_list = []
        res = 'Fail'
        if isinstance(condition, str):
            ids.append({"id": condition})
            id_list.append(condition)
        else:
            for id_str in condition:
                ids.append({"id": id_str})
                id_list.append(id_str)
        _filter = MongoBillFilter()
        _filter.match_bill(C('residents_id').inner(id_list))\
               .project({'_id': 0, 'id': 1})
        res_bed = self.query(_filter, 'PT_Bed')
        if len(res_bed):
            res = '长者当前仍然占用床位，请先办理退住再删除！'
        else:
            bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                        TypeId.user.value, ids, 'PT_User')
            res = 'Success' if bill_id else 'Fail'
        return res

    # 入住长者情况统计
    def get_rzzzqktj_list(self, org_list, condition, page, count):

        keys = ['id', 'name', "admin_area_id"]
        values = self.get_value(condition, keys)

        organization_list = self.hotel_zone_service.get_organization_list(
            org_list, condition, page, count)
        org_ids = []
        org_obj = {}
        bed_org_ids = []

        for item in organization_list['result']:
            org_ids.append(item['id'])
            bed_org_ids.append(item['id'])
            org_obj[item['id']] = {
                'bed_count': 0,
                'check_in_count': 0,
            }

        bed_org_ids.append('7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67')
        _filter_bed = MongoBillFilter()
        _filter_bed\
            .match_bill((C('organization_id').inner(bed_org_ids)))

        _filter_bed.project({
            '_id': 0,
            'id': 1,
            'name': 1,
            'residents_id': 1,
            'organization_id': 1,
        })
        res_bed = self.query(_filter_bed, 'PT_Bed')

        user_ids = []
        bed_user_obj = {}

        for item in res_bed:
            if 'organization_id' in item:
                if(org_obj.get(item["organization_id"])):
                    # 机构床位+1
                    org_obj.get(item["organization_id"])['bed_count'] += 1
                    # 入住长者+1
                    if 'residents_id' in item and item['residents_id'] != None and item['residents_id'] != '':
                        org_obj.get(item["organization_id"])[
                            'check_in_count'] += 1
                        user_ids.append(item['residents_id'])
                        bed_user_obj[item['residents_id']
                                     ] = item['organization_id']

        for item in organization_list['result']:
            item['bed_count'] = 0
            item['check_in_count'] = 0
            if item['id'] in org_obj:
                item['bed_count'] = org_obj[item['id']]['bed_count']
                item['check_in_count'] = org_obj[item['id']]['check_in_count']

        res = organization_list

        # 当前年
        current_year = get_cur_time().year

        # 循环获取所有入住的长者数据
        if len(res['result']) > 0:

            user_obj = {}
            if len(user_ids) > 0:
                _filter_user = MongoBillFilter()
                _filter_user.match_bill(C('id').inner(user_ids))\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'name': 1,
                        'personnel_info': 1
                    })
                res_user = self.query(
                    _filter_user, 'PT_User')

                if len(res_user) > 0:
                    for item in res_user:
                        if item['id'] in bed_user_obj:
                            if bed_user_obj[item['id']] not in user_obj:
                                user_obj[bed_user_obj[item['id']]] = []
                            user_obj[bed_user_obj[item['id']]].append(item)
            # 遍历获取所有长者的id
            for item in res['result']:
                temp_jb = {}
                # 男性人数
                item['man_num'] = 0
                # 女性人数
                item['women_num'] = 0
                # 80岁以下人数
                item['80_num'] = 0
                # 80到99岁人数
                item['80_99_num'] = 0
                # 100岁以上人数
                item['100_num'] = 0
                # 自费人数
                item['zf_num'] = 0
                # 五保人数
                item['wb_num'] = 0
                # 三无人数
                item['sw_num'] = 0
                # 孤寡人数
                item['gg_num'] = 0
                # 享受低保救济人数
                item['dbjj_num'] = 0
                # 生活自理人数
                item['shzl_num'] = 0
                # 介助1
                item['jh1_num'] = 0
                # 介助2
                item['jh2_num'] = 0
                # 介护1
                item['jz1_num'] = 0
                # 介助2
                item['jz2_num'] = 0

                if item['id'] in user_obj and len(user_obj[item['id']]) > 0:
                    for im in user_obj[item['id']]:
                        # 没有就不进去
                        if 'personnel_info' not in im:
                            continue
                        # 统计性别
                        if 'sex' in im['personnel_info']:
                            if im['personnel_info']['sex'] == '男':
                                item['man_num'] += 1
                            elif im['personnel_info']['sex'] == '女':
                                item['women_num'] += 1
                        # 统计长者类别
                        if 'elders_category' in im['personnel_info']:
                            if im['personnel_info']['elders_category'] == '自费':
                                item['zf_num'] += 1
                            elif im['personnel_info']['elders_category'] == '五保':
                                item['zf_num'] += 1
                            elif im['personnel_info']['elders_category'] == '三无':
                                item['sw_num'] += 1
                            elif im['personnel_info']['elders_category'] == '孤寡':
                                item['gg_num'] += 1
                            elif im['personnel_info']['elders_category'] == '享受低保救济':
                                item['dbjj_num'] += 1
                        # 统计年龄
                        if 'date_birth' in im['personnel_info'] and isinstance(im['personnel_info']['date_birth'], datetime.datetime):
                            age = current_year - \
                                im['personnel_info']['date_birth'].year
                            if age < 80:
                                item['80_num'] += 1
                            elif 80 <= age <= 99:
                                item['80_99_num'] += 1
                            elif age > 99:
                                item['100_num'] += 1

            _filter_jb = MongoBillFilter()
            _filter_jb.match_bill((C('elder').inner(user_ids)) & (C('organization_id').inner(org_ids)) & (C('nursing_level').exists(True)))\
                .project({
                    '_id': 0,
                    'elder': 1,
                    'nursing_level': 1,
                    'organization_id': 1,
                    'modify_date': 1,
                })\
                .sort({'modify_date': -1})
            res_jb = self.query(
                _filter_jb, 'PT_Behavioral_Competence_Assessment_Record')

            if len(res_jb) > 0:
                for itm_jb in res_jb:
                    # 存在床位不属于但是评估等级属于的情况，所以要把床位的组织机构ID赋予长者评估的组织机构ID
                    if itm_jb['elder'] in bed_user_obj:
                        itm_jb['organization_id'] = bed_user_obj[itm_jb['elder']]
                    if itm_jb['organization_id'] not in temp_jb:
                        temp_jb[itm_jb['organization_id']] = {
                            'elder': [],
                            'jz1_num': 0,
                            'jz2_num': 0,
                            'jh1_num': 0,
                            'jh2_num': 0,
                            'shzl_num': 0,
                        }
                    # 有护理等级并且没存过（表示最新的）
                    if 'nursing_level' in itm_jb and itm_jb['elder'] not in temp_jb[itm_jb['organization_id']]['elder']:
                        # 存一个，表示最新的已存过
                        temp_jb[itm_jb['organization_id']
                                ]['elder'].append(itm_jb['elder'])
                        if '介护1' == itm_jb['nursing_level']:
                            temp_jb[itm_jb['organization_id']
                                    ]['jh1_num'] += 1
                        if '介护2' == itm_jb['nursing_level']:
                            temp_jb[itm_jb['organization_id']
                                    ]['jh2_num'] += 1
                        if '介助1' == itm_jb['nursing_level']:
                            temp_jb[itm_jb['organization_id']
                                    ]['jz1_num'] += 1
                        if '介助2' == itm_jb['nursing_level']:
                            temp_jb[itm_jb['organization_id']
                                    ]['jz2_num'] += 1
                        if '自理' == itm_jb['nursing_level']:
                            temp_jb[itm_jb['organization_id']
                                    ]['shzl_num'] += 1

                # 再循环一次，补充数据
                for item in res['result']:
                    if item['id'] in temp_jb:
                        item['shzl_num'] = temp_jb[item['id']]['shzl_num']
                        item['jz1_num'] = temp_jb[item['id']]['jz1_num']
                        item['jz2_num'] = temp_jb[item['id']]['jz2_num']
                        item['jh1_num'] = temp_jb[item['id']]['jh1_num']
                        item['jh2_num'] = temp_jb[item['id']]['jh2_num']

        new_list = {}
        new_list['total'] = 0
        new_list['result'] = []
        if 'name' in condition.keys():
            for index, item in enumerate(res['result']):
                if condition['name'] in item['name']:
                    new_list['result'].append(res['result'][index])
                    new_list['total'] += 1
        else:
            new_list = res
        return new_list

    # 入住长者情况统计-按平台统计
    def get_apttj_list(self, org_list, condition, page, count):
        # 定数据格式
        user_ids = []
        org_ids = []
        zongData = {
            "id": "a",
            "name": "全平台统计数据",
            "bed_count": 0,
            "check_in_count": 0,
            "women_num": 0,
            "man_num": 0,
            "80_num": 0,
            "80_99_num": 0,
            "100_num": 0,
            "zf_num": 0,
            "wb_num": 0,
            "sw_num": 0,
            "gg_num": 0,
            "dbjj_num": 0,
            "shzl_num": 0,
            "jz1_num": 0,
            "jz2_num": 0,
            "jh1_num": 0,
            "jh2_num": 0
        }
        organization_list = self.hotel_zone_service.get_organization_list(
            org_list, condition, page, count)
        organization_data = self.hotel_zone_service.get_organization_list(
            org_list, condition, page, organization_list["total"])
        for item in organization_data['result']:
            org_ids.append(item['id'])
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User', 'residents_id', 'id', 'user_info')
        _filter.lookup_bill('PT_Behavioral_Competence_Assessment_Record',
                            'residents_id', 'elder', 'elder_data')
        _filter.project({
            '_id': 0,
            'user_info': 1,
            'elder_data': 1,
            'organization_id': 1,
            'residents_id': 1
        })
        res_bed = self.query(_filter, 'PT_Bed')
        current_year = get_cur_time().year
        for item in res_bed:
            if 'organization_id' in item:
                # 统计总床位数
                user_ids.append(item['residents_id'])
                zongData["bed_count"] += 1
                if(item["residents_id"]):
                    # 统计入住长者总人数
                    zongData["check_in_count"] += 1
                    # 统计长者性别人数
                    if(item["user_info"] and item["user_info"]["personnel_info"] and item["user_info"]["personnel_info"]["sex"] == "女"):
                        zongData["women_num"] += 1
                    elif (item["user_info"] and item["user_info"]["personnel_info"] and item["user_info"]["personnel_info"]["sex"] == "男"):
                        zongData["man_num"] += 1
                    # 统计长者年龄数
                    age = current_year - \
                        item["user_info"]["personnel_info"]['date_birth'].year
                    if age < 80:
                        zongData['80_num'] += 1
                    elif 80 <= age <= 99:
                        zongData['80_99_num'] += 1
                    elif age > 99:
                        zongData['100_num'] += 1
                    # 统计长者类别
                    if 'elders_category' in item["user_info"]["personnel_info"]:
                        if item["user_info"]["personnel_info"]['elders_category'] == '自费':
                            zongData['zf_num'] += 1
                        elif item["user_info"]["personnel_info"]['elders_category'] == '五保':
                            zongData['zf_num'] += 1
                        elif item["user_info"]["personnel_info"]['elders_category'] == '三无':
                            zongData['sw_num'] += 1
                        elif item["user_info"]["personnel_info"]['elders_category'] == '孤寡':
                            zongData['gg_num'] += 1
                        elif item["user_info"]["personnel_info"]['elders_category'] == '享受低保救济':
                            zongData['dbjj_num'] += 1
        _filter_jb = MongoBillFilter()
        _filter_jb.match_bill((C('elder').inner(user_ids)) & (C('organization_id').inner(org_ids)) & (C('nursing_level').exists(True)))\
            .project({
                '_id': 0,
                'elder': 1,
                'nursing_level': 1,
                'organization_id': 1,
                'modify_date': 1,
            })\
            .sort({'modify_date': -1})
        res_jb = self.query(
            _filter_jb, 'PT_Behavioral_Competence_Assessment_Record')
        if len(res_jb) > 0:
            for itm_jb in res_jb:
                # 有护理等级并且没存过（表示最新的）
                if 'nursing_level' in itm_jb and itm_jb['elder'] not in itm_jb['organization_id']:
                    # 存一个，表示最新的已存过
                    if '介护1' == itm_jb['nursing_level']:
                        zongData['jh1_num'] += 1
                    if '介护2' == itm_jb['nursing_level']:
                        zongData['jh2_num'] += 1
                    if '介助1' == itm_jb['nursing_level']:
                        zongData['jz1_num'] += 1
                    if '介助2' == itm_jb['nursing_level']:
                        zongData['jz2_num'] += 1
                    if '自理' == itm_jb['nursing_level']:
                        zongData['shzl_num'] += 1
        res = {
            "total": 1,
            "result": [zongData]
        }

        return res

    # 入住长者情况统计-按区统计
    def get_aqtj_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'organization_id', 'id', 'org_list')
        _filter.lookup_bill('PT_Bed', 'org_list.id',
                            'organization_id', 'bed_list')
        _filter.lookup_bill(
            'PT_User', 'bed_list.residents_id', 'id', 'user_info')
        _filter.match_bill(C('bill_status') == 'valid')\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'parent_id': 1,
                "bed_list.residents_id": 1,
                "org_list.id": 1,
                "user_info": 1
            })
        res_bed = self.query(_filter, 'PT_Administration_Division')
        current_year = get_cur_time().year
        activeCityId = ""
        zongDataList = []
        user_ids = []
        org_ids = []
        for item in res_bed:
            if(item["name"] == "佛山市"):
                activeCityId = item["id"]
                break
        index = 0
        zongData = {}
        for item in res_bed:
            if(item["parent_id"] == activeCityId):
                index += 1
                zongData = {
                    "id": "a",
                    "name": item["name"],
                    "bed_count": len(item["bed_list"]),
                    "check_in_count": 0,
                    "women_num": 0,
                    "man_num": 0,
                    "80_num": 0,
                    "80_99_num": 0,
                    "100_num": 0,
                    "zf_num": 0,
                    "wb_num": 0,
                    "sw_num": 0,
                    "gg_num": 0,
                    "dbjj_num": 0,
                    "shzl_num": 0,
                    "jz1_num": 0,
                    "jz2_num": 0,
                    "jh1_num": 0,
                    "jh2_num": 0
                }
                if(len(item["org_list"]) > 0):
                    for i in item["org_list"]:
                        if(i["id"]):
                            org_ids.append(i['id'])
                if(len(item["bed_list"]) > 0):
                    for i in item["bed_list"]:
                        if(i["residents_id"]):
                            user_ids.append(i['residents_id'])
                            zongData["check_in_count"] += 1
                if(index == 1):
                    _filter_jb = MongoBillFilter()
                    _filter_jb.match_bill((C('elder').inner(user_ids)) & (C('organization_id').inner(org_ids)) & (C('nursing_level').exists(True)))\
                        .project({
                            '_id': 0,
                            'elder': 1,
                            'nursing_level': 1,
                            'organization_id': 1,
                            'modify_date': 1,
                        })\
                        .sort({'modify_date': -1})
                    res_jb = self.query(
                        _filter_jb, 'PT_Behavioral_Competence_Assessment_Record')
                if(len(item["user_info"]) > 0):
                    for i in item["user_info"]:
                        # 统计长者性别
                        if(i["personnel_info"] and i["personnel_info"]["sex"] and i["personnel_info"]["sex"] == "女"):
                            zongData["women_num"] += 1
                        elif(i["personnel_info"] and i["personnel_info"]["sex"] and i["personnel_info"]["sex"] == "男"):
                            zongData["man_num"] += 1
                        # 统计长者年龄
                        age = current_year - \
                            i["personnel_info"]['date_birth'].year
                        if age < 80:
                            zongData['80_num'] += 1
                        elif 80 <= age <= 99:
                            zongData['80_99_num'] += 1
                        elif age > 99:
                            zongData['100_num'] += 1
                        # 统计长者类别
                        if 'elders_category' in i["personnel_info"]:
                            if i["personnel_info"]['elders_category'] == '自费':
                                zongData['zf_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '五保':
                                zongData['zf_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '三无':
                                zongData['sw_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '孤寡':
                                zongData['gg_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '享受低保救济':
                                zongData['dbjj_num'] += 1
                        if len(res_jb) > 0:
                            for itm_jb in res_jb:
                                # 有护理等级并且没存过（表示最新的）
                                if 'nursing_level' in itm_jb and itm_jb['elder'] not in itm_jb['organization_id']:
                                    # 存一个，表示最新的已存过
                                    if '介护1' == itm_jb['nursing_level']:
                                        zongData['jh1_num'] += 1
                                    if '介护2' == itm_jb['nursing_level']:
                                        zongData['jh2_num'] += 1
                                    if '介助1' == itm_jb['nursing_level']:
                                        zongData['jz1_num'] += 1
                                    if '介助2' == itm_jb['nursing_level']:
                                        zongData['jz2_num'] += 1
                                    if '自理' == itm_jb['nursing_level']:
                                        zongData['shzl_num'] += 1
                zongDataList.append(zongData)
        res = {
            "total": len(zongDataList),
            "result": zongDataList
        }
        return res
    # 入住长者情况统计-按镇街统计

    def get_azjtj_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'organization_id', 'id', 'org_list')
        _filter.lookup_bill('PT_Bed', 'org_list.id',
                            'organization_id', 'bed_list')
        _filter.lookup_bill(
            'PT_User', 'bed_list.residents_id', 'id', 'user_info')
        _filter.match_bill(C('bill_status') == 'valid')\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'parent_id': 1,
                "bed_list.residents_id": 1,
                "org_list.id": 1,
                "user_info": 1
            })
        res_bed = self.query(_filter, 'PT_Administration_Division')
        current_year = get_cur_time().year
        activeCityId = ""
        zongDataList = []
        user_ids = []
        org_ids = []
        for item in res_bed:
            if(item["name"] == "南海区"):
                activeCityId = item["id"]
                break
        index = 0
        zongData = {}
        for item in res_bed:
            if(item["parent_id"] == activeCityId):
                index += 1
                zongData = {
                    "id": "a",
                    "name": item["name"],
                    "bed_count": len(item["bed_list"]),
                    "check_in_count": 0,
                    "women_num": 0,
                    "man_num": 0,
                    "80_num": 0,
                    "80_99_num": 0,
                    "100_num": 0,
                    "zf_num": 0,
                    "wb_num": 0,
                    "sw_num": 0,
                    "gg_num": 0,
                    "dbjj_num": 0,
                    "shzl_num": 0,
                    "jz1_num": 0,
                    "jz2_num": 0,
                    "jh1_num": 0,
                    "jh2_num": 0
                }
                if(len(item["org_list"]) > 0):
                    for i in item["org_list"]:
                        if(i["id"]):
                            org_ids.append(i['id'])
                if(len(item["bed_list"]) > 0):
                    for i in item["bed_list"]:
                        if(i["residents_id"]):
                            user_ids.append(i['residents_id'])
                            zongData["check_in_count"] += 1
                if(index == 1):
                    _filter_jb = MongoBillFilter()
                    _filter_jb.match_bill((C('elder').inner(user_ids)) & (C('organization_id').inner(org_ids)) & (C('nursing_level').exists(True)))\
                        .project({
                            '_id': 0,
                            'elder': 1,
                            'nursing_level': 1,
                            'organization_id': 1,
                            'modify_date': 1,
                        })\
                        .sort({'modify_date': -1})
                    res_jb = self.query(
                        _filter_jb, 'PT_Behavioral_Competence_Assessment_Record')
                if(len(item["user_info"]) > 0):
                    for i in item["user_info"]:
                        # 统计长者性别
                        if(i["personnel_info"] and i["personnel_info"]["sex"] and i["personnel_info"]["sex"] == "女"):
                            zongData["women_num"] += 1
                        elif(i["personnel_info"] and i["personnel_info"]["sex"] and i["personnel_info"]["sex"] == "男"):
                            zongData["man_num"] += 1
                        # 统计长者年龄
                        age = current_year - \
                            i["personnel_info"]['date_birth'].year
                        if age < 80:
                            zongData['80_num'] += 1
                        elif 80 <= age <= 99:
                            zongData['80_99_num'] += 1
                        elif age > 99:
                            zongData['100_num'] += 1
                        # 统计长者类别
                        if 'elders_category' in i["personnel_info"]:
                            if i["personnel_info"]['elders_category'] == '自费':
                                zongData['zf_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '五保':
                                zongData['zf_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '三无':
                                zongData['sw_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '孤寡':
                                zongData['gg_num'] += 1
                            elif i["personnel_info"]['elders_category'] == '享受低保救济':
                                zongData['dbjj_num'] += 1
                        if len(res_jb) > 0:
                            for itm_jb in res_jb:
                                # 有护理等级并且没存过（表示最新的）
                                if 'nursing_level' in itm_jb and itm_jb['elder'] not in itm_jb['organization_id']:
                                    # 存一个，表示最新的已存过
                                    if '介护1' == itm_jb['nursing_level']:
                                        zongData['jh1_num'] += 1
                                    if '介护2' == itm_jb['nursing_level']:
                                        zongData['jh2_num'] += 1
                                    if '介助1' == itm_jb['nursing_level']:
                                        zongData['jz1_num'] += 1
                                    if '介助2' == itm_jb['nursing_level']:
                                        zongData['jz2_num'] += 1
                                    if '自理' == itm_jb['nursing_level']:
                                        zongData['shzl_num'] += 1
                zongDataList.append(zongData)
        res = {
            "total": len(zongDataList),
            "result": zongDataList
        }

        return res
    # 从业人员情况统计

    def get_cyryqktj_list(self, org_list, condition, page, count):

        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill((C('id').inner(org_list))
                           & (C('personnel_type') == '2')
                           & (C('name').like(values['name']))
                           & (C('organization_info.personnel_category') == '福利院'))\
            .sort({'modify_date': -1})

        _filter.project({
            '_id': 0,
            'bill_operator': 0,
            'main_account': 0,
            'agent_account': 0,
            'qualification_info': 0,
            'reg_date': 0,
            'town': 0,
            'admin_area_id': 0,
            'modify_date': 0,
            'create_date': 0,
            'organization_info': 0,
            'personnel_type': 0,
            'organization_id': 0,
            **get_common_project()
        })

        res = self.page_query(_filter, 'PT_User', page, count)

        # 找出工作人员
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员') & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Set_Role', 'id', 'principal_account_id', 'setRole')\
            .add_fields({'roaid': self.ao.array_elemat("$setRole.role_of_account_id", 0)})\
            .project({'_id': 0, 'setRole': 0})
        res_worker = self.query(_filter_worker, 'PT_User')

        temp_worker = {}

        for rw in res_worker:
            if 'roaid' in rw:
                if rw['roaid'] in temp_worker:
                    temp_worker[rw['roaid']].append(rw)
                else:
                    temp_worker[rw['roaid']] = []
                    temp_worker[rw['roaid']].append(rw)
            elif 'organization_id' in rw:
                if rw['organization_id'] in temp_worker:
                    temp_worker[rw['organization_id']].append(rw)
                else:
                    temp_worker[rw['organization_id']] = []
                    temp_worker[rw['organization_id']].append(rw)

        # 根据工作人员的组织机构ID分类
        if 'result' in res and len(res['result']) > 0:
            for item in res['result']:
                # 从业人员总数
                item['employees_count'] = 0
                # 男性
                item['man_num'] = 0
                # 女性
                item['women_num'] = 0
                # 大专以下
                item['dzyx_num'] = 0
                # 大专
                item['dz_num'] = 0
                # 本科及以上
                item['bkys_num'] = 0
                # 院长
                item['yz_num'] = 0
                # 护士
                item['hs_num'] = 0
                # 护管员
                item['hly_num'] = 0
                # 社工
                item['sg_num'] = 0
                # 医生
                item['ys_num'] = 0
                # 后勤人员
                item['hqry_num'] = 0
                # 机构管理人员
                item['jggl_num'] = 0
                # 专业技能人员
                item['zyjn_num'] = 0
                # 35岁以下
                item['35_num'] = 0
                # 36-45
                item['36_45_num'] = 0
                # 46-55
                item['46_55_num'] = 0
                # 56岁以上
                item['56_num'] = 0
                # 人均薪酬
                item['yz_money'] = 0
                item['ys_money'] = 0
                item['hs_money'] = 0
                item['hly_money'] = 0
                item['hqry_money'] = 0
                item['sg_money'] = 0
                # 名字
                item['name'] = item['name']
                # 找到
                if item['id'] in temp_worker:
                    organizaiton_worker = temp_worker[item['id']]
                    # 从业人员总数
                    item['employees_count'] = len(
                        temp_worker[item['id']])

                    for itm in organizaiton_worker:
                        if 'personnel_info' in itm:
                            item_personnel_info = itm.get('personnel_info')
                            # 性别统计
                            sex = item_personnel_info.get('sex')
                            if sex == '男':
                                item['man_num'] += 1
                            elif sex == '女':
                                item['women_num'] += 1
                            # 岗位分类
                            worker_category = item_personnel_info.get(
                                'worker_category')
                            if worker_category == '管理人员':
                                item['jggl_num'] += 1
                            elif worker_category == '专业技能人员':
                                item['zyjn_num'] += 1

                            post = item_personnel_info.get('post')
                            if post == '院长':
                                item['yz_num'] += 1
                            elif post == '护士':
                                item['hs_num'] += 1
                            elif post == '护管员':
                                item['hly_num'] += 1
                            elif post == '后勤人员':
                                item['hqry_num'] += 1
                            elif post == '社工':
                                item['sg_num'] += 1
                            elif post == '医生':
                                item['ys_num'] += 1

                            # 学历
                            education = item_personnel_info.get(
                                'education')
                            if education in ['小学', '中学', '中专', '高职']:
                                item['dzyx_num'] += 1
                            elif education == '大专':
                                item['dz_num'] += 1
                            elif education in ['本科', '硕士', '博士']:
                                item['bkys_num'] += 1

                            # 年龄
                            id_card = itm.get('id_card')
                            if id_card and len(id_card) == 18:
                                get_infomation_by_idcard = GetInformationByIdCard(
                                    id_card)
                                age = get_infomation_by_idcard.get_age()
                                if age <= 35:
                                    item['35_num'] += 1
                                elif age > 35 and age < 46:
                                    item['36_45_num'] += 1
                                elif age > 46 and age < 55:
                                    item['46_55_num'] += 1
                                elif age > 55:
                                    item['56_num'] += 1

        return res

    def get_organization_list(self, org_list, condition, page, count):
        '''获取组织机构列表'''
        def convertDegreesToRadians(degrees):
            return degrees * math.pi / 180

        def coverGpsToBaiduLatLon(lat, lon):
            baidu_map_url = 'http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=%f&y=%f'

            data_req = requests.get((baidu_map_url % (float(lon), float(lat))))
            new_pos = data_req.json()
            if 'error' in new_pos.keys() and new_pos['error'] == 0:
                lon = float(base64.b64decode(
                    new_pos['x']).decode("utf-8"))
                lat = float(base64.b64decode(
                    new_pos['y']).decode("utf-8"))
                return lat, lon

        keys = ['id', 'name', 'organization_nature', 'admin_area_id', 'type',
                'personnel_category', 'personnel_type', 'lat', 'lon', 'sortByDistance', 'contract_status', 'star_level']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if condition.get('id'):
            _filter.match_bill(C('id') == values['id'])
        elif org_list != [None]:
            _filter.match((C('id').inner(org_list)))
        _filter.match_bill((C('id') == values['id'])
                           & (C('personnel_type') == '2')
                           & (C('organization_info.is_show_app') != '下架')
                           & (C('name').like(values['name']))
                           & (C('organization_info.star_level') == values['star_level'])
                           & (C('admin_area_id') == values['admin_area_id'])
                           & (C('organization_info.contract_status') == values['contract_status'])
                           & (C('organization_info.organization_nature').like(values['organization_nature']))
                           & (C('personnel_type') == values['personnel_type'])
                           & (C('organization_info.personnel_category') == values['personnel_category']))\
               .lookup_bill('PT_User', 'organization_info.super_org_id', 'id', 'organization_infos')\
               .lookup_bill('PT_Activity', 'id', 'organization_id', 'organization_activity')\
               .add_fields({'activity_count': self.ao.size('$organization_activity'), })\
               .add_fields({"activity_id": self.ao.array_elemat("$organization_activity.id", 0)})\
               .lookup_bill('PT_Activity_Participate', 'activity_id', 'activity_id', 'organization_participate')\
               .add_fields({'participate_count': self.ao.size('$organization_participate'), })\
               .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'follow_info')\
               .add_fields({'follow_count': self.ao.size('$follow_info'), })\
               .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'division_info')\
               .add_fields({"division_name": self.ao.array_elemat("$division_info.name", 0)})\
               .lookup_bill('PT_Bed', 'id', 'organization_id', 'bed')\
               .add_fields({'bed_count': self.ao.size('$bed'), })\
               .add_fields({'chenk_in_count': self.ao.size(self.ao.array_filter("$bed", "aa", ((F('$aa.residents_id') != None) & (F('$aa.residents_id') != '')).f))})\
               .add_fields({'organization_info.chenk_in_probability': self.ao.floor(self.ao.switch([self.ao.case(((F('bed_count') == 0)), 0)], (F('chenk_in_count').__truediv__('$bed_count') * (100)).f))})\
               .lookup_bill('PT_Service_Product', 'id', 'organization_id', 'service_product')
        user_id = get_user_id_or_false(self.session)
        if user_id != False:
            _filter.add_fields({'is_follow': self.ao.size(self.ao.array_filter(
                "$follow_info", "fi", ((F('$fi.user_id') == user_id) & (F('$fi.object') == '社区幸福院') & (F('$fi.type') == 'follow')).f))})

        # 从业人员
        if 'type' in condition and condition['type'] == 'cyry':
            _filter.sort({'modify_date': -1})
            _filter.project({'_id': 0, 'bed._id': 0, 'chenk_in': 0, 'service_product': 0, 'organization_infos._id': 0, 'user._id': 0,
                             'organization_activity': 0, 'organization_participate': 0, 'division_info': 0, 'follow_info': 0, 'order_info': 0})
            res = self.page_query(_filter, 'PT_User', page, count)

            # 找出工作人员
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Set_Role', 'id', 'principal_account_id', 'setRole')\
                .add_fields({'roaid': self.ao.array_elemat("$setRole.role_of_account_id", 0)})\
                .sort({'modify_date': -1})\
                .project({'_id': 0, 'setRole': 0})
            res_worker = self.query(_filter_worker, 'PT_User')

            temp_worker = {}

            for rw in res_worker:
                if 'roaid' in rw:
                    if rw['roaid'] in temp_worker:
                        temp_worker[rw['roaid']].append(rw)
                    else:
                        temp_worker[rw['roaid']] = []
                        temp_worker[rw['roaid']].append(rw)
                elif 'organization_id' in rw:
                    if rw['organization_id'] in temp_worker:
                        temp_worker[rw['organization_id']].append(rw)
                    else:
                        temp_worker[rw['organization_id']] = []
                        temp_worker[rw['organization_id']].append(rw)

            new_return = []

            # 当前年
            current_year = get_cur_time().year

            # 根据工作人员的组织机构ID分类
            if 'result' in res and len(res['result']) > 0:
                for item in res['result']:
                    new_item = {}
                    # 从业人员总数
                    new_item['employees_count'] = 0
                    # 男性
                    new_item['man_num'] = 0
                    # 女性
                    new_item['women_num'] = 0
                    # 大专以下
                    new_item['dzyx_num'] = 0
                    # 大专
                    new_item['dz_num'] = 0
                    # 本科及以上
                    new_item['bkys_num'] = 0
                    # 院长
                    new_item['yz_num'] = 0
                    # 护士
                    new_item['hs_num'] = 0
                    # 护管员
                    new_item['hly_num'] = 0
                    # 社工
                    new_item['sg_num'] = 0
                    # 医生
                    new_item['ys_num'] = 0
                    # 后勤人员
                    new_item['hqry_num'] = 0
                    # 机构管理人员
                    new_item['jggl_num'] = 0
                    # 专业技能人员
                    new_item['zyjn_num'] = 0
                    # 35岁以下
                    new_item['35_num'] = 0
                    # 36-45
                    new_item['36_45_num'] = 0
                    # 46-55
                    new_item['46_55_num'] = 0
                    # 56岁以上
                    new_item['56_num'] = 0
                    # 人均薪酬
                    new_item['yz_money'] = 0
                    new_item['ys_money'] = 0
                    new_item['hs_money'] = 0
                    new_item['hly_money'] = 0
                    new_item['hqry_money'] = 0
                    new_item['sg_money'] = 0
                    # 名字
                    new_item['name'] = item['name']
                    # 找到
                    if item['id'] in temp_worker:
                        organizaiton_worker = temp_worker[item['id']]
                        # 从业人员总数
                        new_item['employees_count'] = len(
                            temp_worker[item['id']])

                        for itm in organizaiton_worker:
                            if 'personnel_info' in itm:
                                item_personnel_info = itm.get('personnel_info')
                                # 性别统计
                                sex = item_personnel_info.get('sex')
                                if sex == '男':
                                    new_item['man_num'] += 1
                                elif sex == '女':
                                    new_item['women_num'] += 1
                                # 岗位分类
                                worker_category = item_personnel_info.get(
                                    'worker_category')
                                if worker_category == '管理人员':
                                    new_item['jggl_num'] += 1
                                elif worker_category == '专业技能人员':
                                    new_item['zyjn_num'] += 1

                                post = item_personnel_info.get('post')
                                if post == '院长':
                                    new_item['yz_num'] += 1
                                elif post == '护士':
                                    new_item['hs_num'] += 1
                                elif post == '护管员':
                                    new_item['hly_num'] += 1
                                elif post == '后勤人员':
                                    new_item['hqry_num'] += 1
                                elif post == '社工':
                                    new_item['sg_num'] += 1
                                elif post == '医生':
                                    new_item['ys_num'] += 1

                                # 学历
                                education = item_personnel_info.get(
                                    'education')
                                if education in ['小学', '中学', '中专', '高职']:
                                    new_item['dzyx_num'] += 1
                                elif education == '大专':
                                    new_item['dz_num'] += 1
                                elif education in ['本科', '硕士', '博士']:
                                    new_item['bkys_num'] += 1

                                # 年龄
                                id_card = itm.get('id_card')
                                if id_card and len(id_card) == 18:
                                    get_infomation_by_idcard = GetInformationByIdCard(
                                        id_card)
                                    age = get_infomation_by_idcard.get_age()
                                    if age <= 35:
                                        new_item['35_num'] += 1
                                    elif age > 35 and age < 46:
                                        new_item['36_45_num'] += 1
                                    elif age > 46 and age < 55:
                                        new_item['46_55_num'] += 1
                                    elif age > 55:
                                        new_item['56_num'] += 1

                    new_return.append(new_item)

            res['result'] = new_return
            return res
        # 入住长者
        elif 'type' in condition and condition['type'] == 'rzzz':
            current_year = get_cur_time().year
            _filter.unwind('bed')\
                .match_bill((C('bed.residents_id') != None) & (C('bed.residents_id') != ''))\
                .lookup_bill('PT_User', 'bed.residents_id', 'id', 'user')\
                .add_fields({'eldersCategory': '$user.personnel_info.elders_category'})\
                .unwind('eldersCategory')\
                .add_fields({'zf_num': self.ao.switch([self.ao.case(((F('eldersCategory') == '自费')), 1)], 0)})\
                .add_fields({'wb_num': self.ao.switch([self.ao.case(((F('eldersCategory') == '五保')), 1)], 0)})\
                .add_fields({'sw_num': self.ao.switch([self.ao.case(((F('eldersCategory') == '三无')), 1)], 0)})\
                .add_fields({'gg_num': self.ao.switch([self.ao.case(((F('eldersCategory') == '孤寡')), 1)], 0)})\
                .add_fields({'dbjj_num': self.ao.switch([self.ao.case(((F('eldersCategory') == '享受低保救济')), 1)], 0)})\
                .add_fields({'sex': '$user.personnel_info.sex'})\
                .unwind('sex')\
                .add_fields({'man_num': self.ao.switch([self.ao.case(((F('sex') == '男')), 1)], 0)})\
                .add_fields({'women_num': self.ao.switch([self.ao.case(((F('sex') == '女')), 1)], 0)})\
                .add_fields({'id_card': '$user.id_card'})\
                .unwind('id_card')\
                .add_fields({'id_card_length': self.ao.str_len_cp('$id_card')})\
                .add_fields({'year': self.ao.switch([
                    self.ao.case(((F('id_card_length') == 18)), self.ao.to_int(
                        self.ao.sub_str('$id_card', 6, 4))),
                    self.ao.case(((F('id_card_length') == 16)), self.ao.to_int(
                        self.ao.concat([("19"), (self.ao.sub_str('$id_card', 6, 4))])))
                ], current_year)})\
                .add_fields({'age': ((F('year')-current_year)*(-1)).f})\
                .add_fields({
                    '80_num': self.ao.switch([self.ao.case(((F('age') < 80)), 1)], 0),
                    '80_99_num': self.ao.switch([self.ao.case(((F('age') >= 80) & (F('age') < 100)), 1)], 0),
                    '100_num': self.ao.switch([self.ao.case(((F('age') >= 100)), 1)], 0)})\
                .group(
                {
                    'bed_count': '$bed_count',
                    'name': '$name',
                    'id': '$id',
                    'chenk_in_count': '$chenk_in_count',
                },
                [{
                    'man_num': self.ao.summation('$man_num')
                }, {
                    'women_num': self.ao.summation('$women_num')
                }, {
                    '80_num': self.ao.summation('$80_num')
                }, {
                    '80_99_num': self.ao.summation('$80_99_num')
                }, {
                    '100_num': self.ao.summation('$100_num')
                }, {
                    'zf_num': self.ao.summation('$zf_num')
                }, {
                    'wb_num': self.ao.summation('$wb_num')
                }, {
                    'sw_num': self.ao.summation('$sw_num')
                }, {
                    'gg_num': self.ao.summation('$gg_num')
                }, {
                    'dbjj_num': self.ao.summation('$dbjj_num')
                }])
            _filter.sort({'chenk_in_count': -1})
        if 'sort' in condition and condition['sort'] != None:
            if condition['sort'] == '活动数':
                _filter.sort({'activity_count': -1})
            elif condition['sort'] == '报名数':
                _filter.sort({'participate_count': -1})
            elif condition['sort'] == '关注度':
                _filter.sort({'follow_count': -1})
            elif condition['sort'] == '入住率':
                _filter.sort({'organization_info.chenk_in_probability': -1})
            else:
                _filter.sort({'create_date': -1})
        else:
            _filter.sort({'modify_date': -1})
        _filter.project({
            'chenk_in': 0,
            'user.login_info': 0,
            'service_product': 0,
            'organization_activity': 0,
            'organization_participate': 0,
            'division_info': 0,
            'follow_info': 0,
            'order_info': 0,
            'qualification_info': 0,
            **get_common_project({'bed', 'organization_infos', 'user'})
        })
        if 'lat' in condition and 'lon' in condition:
            lat = values['lat']
            lon = values['lon']
            # 纬度，经度
            lat_baidu, lon_baidu = coverGpsToBaiduLatLon(lat, lon)
            lat_d = convertDegreesToRadians(lat_baidu)
            # _filter.add_fields({'distance':self.ao.multiply([self.ao.acos(self.ao.add([self.ao.multiply([self.ao.sin(self.ao.degree_to_radians(self.ao.to_double("$organization_info.lat"))),
            #                                                                                              self.ao.sin(lat_d)]),
            #                                                                            self.ao.multiply([self.ao.cos(self.ao.degree_to_radians(self.ao.to_double("$organization_info.lat")))
            #                                                                                             ,self.ao.cos(lat_d)
            #                                                                                             ,self.ao.cos(self.ao.degree_to_radians(self.ao.substract[self.ao.to_double('$organization_info.lon')
            #                                                                                                                                                     ,lon_baidu]))])])),6371.004])})
            _filter.filter_objects.append({
                '$addFields': {
                    # 距离是以km单位的
                    "distance": {
                        '$multiply': [
                            {
                                '$acos': {
                                    '$add': [
                                        {
                                            '$multiply': [
                                                {
                                                    '$sin': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$sin': lat_d
                                                }
                                            ]
                                        },
                                        {
                                            '$multiply': [
                                                {
                                                    '$cos': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$cos': lat_d
                                                },
                                                {
                                                    '$cos': {
                                                        '$degreesToRadians': {
                                                            '$subtract': [
                                                                {"$toDouble": "$organization_info.lon"},
                                                                lon_baidu
                                                            ]
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            # 地球平均半径
                            6371.004
                        ]
                    }
                }
            })
        else:
            _filter.add_fields({'distance': None})

        if 'sortByDistance' in condition:
            _filter.sort({'distance': 1})
        # print('>>>>>>>>>>>>>>>', _filter.filter_objects)
        res = self.page_query(_filter, 'PT_User', page, count)

        # 入住长者补充长者护理级别
        if 'type' in condition and condition['type'] == 'rzzz':
            if len(res['result']) > 0:
                # 取值范围
                temp_org = []
                temp_jb = {}
                for item in res['result']:
                    # 默认数据
                    item['shzl_num'] = 0
                    item['jz_num'] = 0
                    item['jh_num'] = 0
                    # 把机构id存起来
                    temp_org.append(item['id'])

                _filter_jb = MongoBillFilter()
                _filter_jb.match_bill((C('organization_id').inner(temp_org)))\
                    .project({'_id': 0, **get_common_project()})\
                    .sort({'create_date': -1})
                res_jb = self.query(
                    _filter_jb, 'PT_Behavioral_Competence_Assessment_Record')

                if len(res_jb) > 0:
                    for itm_jb in res_jb:
                        if itm_jb['organization_id'] not in temp_jb:
                            temp_jb[itm_jb['organization_id']] = {
                                'elder': [],
                                'jz_num': 0,
                                'shzl_num': 0,
                                'jh_num': 0,
                            }
                        # 有护理等级并且没存过（表示最新的）
                        if 'nursing_level' in itm_jb and itm_jb['elder'] not in temp_jb[itm_jb['organization_id']]['elder']:
                            # 存一个，表示最新的已存过
                            temp_jb[itm_jb['organization_id']
                                    ]['elder'].append(itm_jb['elder'])
                            if '介护' in itm_jb['nursing_level']:
                                temp_jb[itm_jb['organization_id']
                                        ]['jh_num'] += 1
                            if '介助' in itm_jb['nursing_level']:
                                temp_jb[itm_jb['organization_id']
                                        ]['jz_num'] += 1
                            if '自理' in itm_jb['nursing_level']:
                                temp_jb[itm_jb['organization_id']
                                        ]['shzl_num'] += 1

                # 再循环一次，补充数据
                for item in res['result']:
                    if item['id'] in temp_jb:
                        item['shzl_num'] = temp_jb[item['id']]['shzl_num']
                        item['jz_num'] = temp_jb[item['id']]['jz_num']
                        item['jh_num'] = temp_jb[item['id']]['jh_num']
        return res

    def get_organization_tree_list_forName(self, org_list, condition, page, count):
        '''查询 组织机构树形结构  '''
        # t1 = time.time()
        keys = ['organization_type', 'type', 'type_list']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2')
                           & (C('id').inner(org_list))
                           )
        _filter.add_fields({
            'title': '$name',
            'value': '$name',
            'key': '$id',
            'children': [],
            'id': '$id',
        })\
            .project({'_id': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_User', page, count)

        # t2 = time.time()
        # dit1 = t2 - t1
        # print('第一次查询用户表耗时：', dit1)
        # 查询所有的组织机构给递归循环用，不用每次都去查
        # t3 = time.time()
        result = []
        all_org_filter = MongoBillFilter()
        all_org_filter.match_bill(
            (C('personnel_type') == '2')
            & (C('organization_info.personnel_category') == values['type'])
            & (C('organization_info.personnel_category').inner(values['type_list']))
        )\
            .add_fields({'super_org_id': '$organization_info.super_org_id'})\
            .add_fields({'personnel_category': '$organization_info.personnel_category'})\
            .project({'_id': 0})
        all_res = self.query(all_org_filter, 'PT_User')
        # t4 = time.time()
        # dit2 = t4 - t3
        # print('查询全部用户表耗时：', dit2)
        # t9 = time.time()
        all_org_map = {}
        # print('这里的长度》》》》》', len(all_res))
        for i, x in enumerate(all_res):
            personnel_category = ''
            if 'organization_info' in all_res[i].keys() and 'personnel_category' in all_res[i]['organization_info'].keys():
                personnel_category = all_res[i]['organization_info']['personnel_category']
            org_data = {
                'title': all_res[i]['name'],
                'value': all_res[i]['name'],
                'key': all_res[i]['id'],
                'children': [],
                'id': all_res[i]['id'],
            }
            if 'super_org_id' in all_res[i]['organization_info'].keys() and all_res[i]['organization_info']['super_org_id'] in all_org_map.keys():
                all_org_map[all_res[i]['organization_info']
                            ['super_org_id']].append(org_data)
            else:
                if 'super_org_id' in all_res[i]['organization_info'].keys():
                    all_org_map[all_res[i]['organization_info']
                                ['super_org_id']] = [org_data]
        # t10 = time.time()
        # dit9 = t10 - t9
        # print('转换用户表耗时：', dit9)

        # t4 = time.time()
        if len(res['result']) > 0:
            for data in res['result']:
                child_res, all_org_map = self.query_child(
                    data['id'], all_org_map)
                r_data, all_org_map = self.xh(child_res, data, all_org_map)
                result.append(r_data)

        res['result'] = result

        # t5 = time.time()
        # dit3 = t5 - t4
        # print('拼接最终数据耗时：', dit3)
        return res

    def get_organization_tree_list(self, org_list, condition, page, count):
        '''查询 组织机构树形结构  '''
        # t1 = time.time()
        keys = ['organization_type', 'type', 'type_list']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2')
                           & (C('id').inner(org_list))
                           )
        _filter.add_fields({
            'title': self.ao.concat([('$name'), ('('), ('$organization_info.personnel_category'), (')')]),
            'value': '$id',
            'key': '$id',
            'children': [],
            'id': '$id',
        })\
            .project({'_id': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_User', page, count)

        # t2 = time.time()
        # dit1 = t2 - t1
        # print('第一次查询用户表耗时：', dit1)
        # 查询所有的组织机构给递归循环用，不用每次都去查
        # t3 = time.time()
        result = []
        all_org_filter = MongoBillFilter()
        all_org_filter.match_bill(
            (C('personnel_type') == '2')
            & (C('organization_info.personnel_category') == values['type'])
            & (C('organization_info.personnel_category').inner(values['type_list']))
        )\
            .add_fields({'super_org_id': '$organization_info.super_org_id'})\
            .add_fields({'personnel_category': '$organization_info.personnel_category'})\
            .project({'_id': 0})
        all_res = self.query(all_org_filter, 'PT_User')
        # t4 = time.time()
        # dit2 = t4 - t3
        # print('查询全部用户表耗时：', dit2)
        # t9 = time.time()
        all_org_map = {}
        # print('这里的长度》》》》》', len(all_res))
        for i, x in enumerate(all_res):
            personnel_category = ''
            if 'organization_info' in all_res[i].keys() and 'personnel_category' in all_res[i]['organization_info'].keys():
                personnel_category = all_res[i]['organization_info']['personnel_category']
            org_data = {
                'title': all_res[i]['name']+'('+personnel_category+')',
                'value': all_res[i]['id'],
                'key': all_res[i]['id'],
                'children': [],
                'id': all_res[i]['id'],
            }
            if 'super_org_id' in all_res[i]['organization_info'].keys() and all_res[i]['organization_info']['super_org_id'] in all_org_map.keys():
                all_org_map[all_res[i]['organization_info']
                            ['super_org_id']].append(org_data)
            else:
                if 'super_org_id' in all_res[i]['organization_info'].keys():
                    all_org_map[all_res[i]['organization_info']
                                ['super_org_id']] = [org_data]
        # t10 = time.time()
        # dit9 = t10 - t9
        # print('转换用户表耗时：', dit9)

        # t4 = time.time()
        if len(res['result']) > 0:
            for data in res['result']:
                child_res, all_org_map = self.query_child(
                    data['id'], all_org_map)
                r_data, all_org_map = self.xh(child_res, data, all_org_map)
                result.append(r_data)

        res['result'] = result

        # t5 = time.time()
        # dit3 = t5 - t4
        # print('拼接最终数据耗时：', dit3)
        return res

    def xh(self, child_res, return_data, all_org_map):
        return_data['children'] = child_res
        if len(child_res) > 0 and all_org_map:
            for data in child_res:
                child_data, all_org_map = self.query_child(
                    data['id'], all_org_map)
                data, all_org_map = self.xh(child_data, data, all_org_map)
        return return_data, all_org_map

    def query_child(self, org_id, all_org_map):
        re_org = []
        if org_id in all_org_map.keys():
            re_org = all_org_map[org_id]
            del all_org_map[org_id]
        return re_org, all_org_map

    def judge_is_success(self, self_org_id, select_org_id):
        '''
            用于判断选择上级组织机构时，选择的机构是否正确（不为本组织的下级或自身）有个问题：如果选的是上级理论上应该无条件成功，但现在会递归所有下级，导致卡死
            解决方案：应该给组织机构分层级
            self_org_id: 本次编辑的组织机构Id
            select_org_id: 选择的组织机构Id
        '''
        flag = True
        if select_org_id == self_org_id:
            flag = False
            return flag
        ''' 暂时先注释，有解决选了上级会多重循环下级的方案后再放开'''
        # _filter = MongoBillFilter()
        # _filter.match_bill((C('organization_info.super_org_id') == self_org_id))\
        #     .project({'_id': 0})
        # res = self.query(_filter, 'PT_User')
        # for result in res:
        #     if result['id'] == select_org_id:
        #         flag = False
        #         return flag
        #     else:
        #         flag = self.judge_is_success(result['id'], select_org_id)
        #         if not flag:
        #             return flag
        return flag

    def update_organization(self, condition):
        '''新增/修改组织机构'''
        user = self.get_current_user_info()
        if 'admin_area_id' not in condition.keys():
            condition['admin_area_id'] = user[0]['admin_area_id']
        res = 'Fail'
        if 'id' in condition.keys():
            resl = self.judge_is_success(
                condition['id'], condition['organization_info']['super_org_id'])
            if not resl:
                return '不能选择当前组织机构'

            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('organization_info.super_org_id') == condition['id'])).project({'_id': 0})
            parent_data = self.query(_filter, "PT_User")
            _filter_old = MongoBillFilter()
            _filter_old.match_bill(
                (C('id') == condition['id'])).project({'_id': 0})
            old_data = self.query(_filter_old, "PT_User")
            if len(parent_data) > 0 and old_data[0]['organization_info']['super_org_id'] != condition['organization_info']['super_org_id']:
                return '该组织机构已有下级关联，不能更改上级组织机构'

            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.organization.value, condition, 'PT_User')
            if bill_id:
                res = 'Success'
        else:
            # resl = self.judge_is_success(
            #     get_current_organization_id(self.session), condition['organization_info']['super_org_id'])
            # if not resl:
            #     return '不能选择当前组织机构'
            data = condition
            tables = 'PT_User'
            data_id = get_info({}, self.session)
            condition['id'] = data_id['id']
            # 组织类型为福利院
            if condition['organization_info']['personnel_category'] == '福利院':
                # 新增一个真实账户
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data_id['id'], None, AccountStatus.normal, 1, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                condition['agent_account'] = real_account['id']
                condition['main_account'] = real_account['id']
                data = [condition, real_account]
                tables = ['PT_User', FinancialAccount().name]

            # 组织类型为服务商
            if condition['organization_info']['personnel_category'] == '服务商':
                # 新增一个真实账户 + 补贴账户
                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, data_id['id'], None, AccountStatus.normal, 1, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)
                condition['agent_account'] = real_account['id']
                condition['main_account'] = real_account['id']
                # 补贴账户
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, data_id['id'], None, AccountStatus.normal, 2, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)
                data = [[condition], [real_account, subsidy_account]]
                tables = ['PT_User', FinancialAccount().name]

            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.organization.value, data, tables)
            if bill_id:
                res = 'Success'
        return res

    def delete_organization(self, condition):
        ids = []
        current_org_id = get_current_organization_id(self.session)
        if isinstance(condition, str):
            ids.append({"id": condition})
            if current_org_id == condition:
                return '不能删除当前组织机构'
        else:
            for id_str in condition:
                ids.append({"id": id_str})
                if current_org_id == id_str:
                    return '不能删除当前组织机构'
        self.bill_manage_service.add_bill(OperationType.delete.value,
                                          TypeId.organization.value, ids, 'PT_User')
        res = 'Success'
        return res

    def get_wait_checkin_elder_list(self, org_list, condition, page, count):
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _fliter = MongoBillFilter()
        _fliter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('state') == '居住中')
        )
        res = self.query(_fliter, 'IEC_Check_In')
        pf = pd.DataFrame(res)
        user_list = []
        if 'user_id' in pf:
            user_list = pf['user_id'].tolist()
        _filter_wait = MongoBillFilter()
        _filter_wait.match_bill(
            (C('name').like(values['name']))
            & (C('organization_id').inner(org_list))
            & (C('id').nin(user_list))
            & (C('personnel_info.personnel_category') == '长者')
        ) \
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'id', 'elder', 'recode') \
            .project({
                'recode._id': 0
            })\
            .add_fields({'new_date': self.ao.maximum('$recode.create_date')})\
            .add_fields({'new_recode': self.ao.array_filter('$recode', 'record', (F('$record.create_date') == '$new_date').f)})\
            .inner_join_bill('PT_Assessment_Template', 'recode.template', 'id', 'template') \
            .match_bill(C('template.template_type') == '详细能力评估')\
            .project({'_id': 0, 'template': 0})
        user_wait = self.page_query(_filter_wait, 'PT_User', None, None)
        return user_wait

    def get_wait_checkin_elder_list_yh(self, org_list, condition, page, count):
        '''优化查询待入住长者列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _fliter = MongoBillFilter()
        _fliter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('state') == '居住中')
        )
        res = self.query(_fliter, 'IEC_Check_In')
        pf = pd.DataFrame(res)
        user_list = []
        if 'user_id' in pf:
            user_list = pf['user_id'].tolist()
        # 查询评估模板数据
        template_ids = []
        _fliter_assessment_template = MongoBillFilter()
        _fliter_assessment_template.match_bill((C('template_type') == '详细能力评估'))\
            .project({'_id': 0, 'id': 1})
        res_template = self.query(
            _fliter_assessment_template, 'PT_Assessment_Template')
        if len(res_template) > 0:
            for template in res_template:
                template_ids.append(template['id'])

        # 查询评估记录数据
        elder_ids = []
        record_data = {}
        _fliter_record = MongoBillFilter()
        _fliter_record.match_bill((C('template').inner(template_ids)) & (C('elder')).nin(user_list))\
            .sort({'elder': -1, 'create_date': -1})\
            .project({'_id': 0, 'elder': 1, 'create_date': 1, 'template': 1, 'total_score': 1, 'id': 1, 'organization_id': 1})
        res_record = self.query(
            _fliter_record, 'PT_Behavioral_Competence_Assessment_Record')
        if len(res_record) > 0:
            for record in res_record:
                if record['elder'] in record_data.keys():
                    record_data[record['elder']].append(record)
                else:
                    record_data[record['elder']] = [record]
                    elder_ids.append(record['elder'])

        _filter_wait = MongoBillFilter()
        _filter_wait.match_bill(
            (C('name').like(values['name']))
            & (C('organization_id').inner(org_list))
            & (C('id').nin(user_list))
            & (C('id').inner(elder_ids))
            & (C('personnel_info.personnel_category') == '长者')
        ) \
            .project({'_id': 0})
        user_wait = self.page_query(_filter_wait, 'PT_User', page, count)
        for i, x in enumerate(user_wait['result']):
            if 'id' in user_wait['result'][i] and user_wait['result'][i]['id'] in record_data.keys():
                user_wait['result'][i]['new_date'] = record_data[user_wait['result']
                                                                 [i]['id']][0]['create_date']
                user_wait['result'][i]['new_recode'] = record_data[user_wait['result'][i]['id']][0]

        return user_wait

    def get_personnel_classification_list(self, org_list, condition, page, count):
        '''获取人员类别列表
        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id', 'name', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('name').like(values['name']))
                           & (C('organization_id').inner(org_list))
                           & (C('type') == values['type'])
                           )\
            .sort({'modify_date': -1})\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Personnel_Classification', page, count)
        return res

    def update_personnel_classification(self, data):
        '''新增或修改人员类别'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.personnelClassification.value, data, 'PT_Personnel_Classification')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.personnelClassification.value, data, 'PT_Personnel_Classification')
            if bill_id:
                res = 'Success'
        return res

    def delete_personnel_classification(self, data):
        '''删除人员类别'''
        res = 'Fail'

        if len(data) > 0:

            ids = []

            for item in data:
                # 判断是否使用中
                _filter = MongoBillFilter()
                _filter.match_bill(
                    (C('personnel_info.personnel_classification') == item))
                _filter.project({'_id': 0})

                res_user = self.query(_filter, 'PT_User')

                if len(res_user) > 0:
                    return '该人员类别使用中，无法删除'

                ids.append({"id": item})

            bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                        TypeId.personnelClassification.value, ids, 'PT_Personnel_Classification')
            if bill_id:
                res = 'Success'

        return res

    def get_organization_xfy_tree_list(self, org_list, condition, page, count):
        ''' 查询幸福院树状列表 '''
        # 获取当前组织机构的行政区划Id
        _filter = MongoBillFilter()
        _filter.match_bill((C('id').inner(org_list)))\
            .project({'_id': 0})
        res_user = self.query(
            _filter, "PT_User")
        admin_area_id = ''
        if len(res_user) > 0:
            admin_area_id = res_user[0]['admin_area_id']
        # keys = ['id', 'name', 'is_no_myself']
        # values = self.get_value(condition, keys)
        # permission_keys = ['user_type']
        # permission_values = self.get_value(
        #     permission_condition, permission_keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == admin_area_id))\
            .add_fields({
                'title': '$name',
                'value': '$id',
                'key': '$id',
                'children': [],
                # 标识这是一个行政区域
                'is_division': True,
            })\
            .project({'_id': 0})
        res = self.page_query(
            _filter, "PT_Administration_Division", page, count)
        result = []
        if len(res['result']) > 0:
            for data in res['result']:
                new_res = self.query_child2(data['id'])
                r_data = self.xuhuan2(new_res, data)
                result.append(r_data)

        res['result'] = result
        return res

    def xuhuan2(self, new_res, return_data):
        if len(new_res) > 0:
            return_data['children'] = new_res
            # 应该在这里加幸福院吧
            for data in new_res:
                child_data = self.query_child2(data['id'])
                self.xuhuan2(child_data, data)
        return return_data

    def query_child2(self, admin_id):
        _filter = MongoBillFilter()
        _filter.match_bill((C('parent_id') == admin_id))\
            .add_fields({
                'title': '$name',
                'value': '$id',
                'key': '$id',
                # 标识这是一个行政区域
                'is_division': True,
                'children': []
            })\
            .project({'_id': 0})
        res = self.query(
            _filter, "PT_Administration_Division")

        if len(res) > 0:
            for item in res:
                xfyByAreaId = self.getXfyByAreaId(item['id'], '幸福院')
                if len(xfyByAreaId) > 0:
                    for itm in xfyByAreaId:
                        item['children'].append(itm)
        return res

    def getXfyByAreaId(self, admin_area_id, personnel_category):
        # 根据行政区域ID（admin_area_id）和机构类型（幸福院）列出列表
        condition = {
            'admin_area_id': admin_area_id,
            'personnel_category': personnel_category,
        }
        keys = ['personnel_category', 'admin_area_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('admin_area_id') == values['admin_area_id']) & (C('organization_info.personnel_category') == values['personnel_category']))\
            .add_fields({
                'title': '$name',
                'value': '$id',
                'key': '$id',
                # 标识这是一个组织机构
                'is_division': False,
            })\
            .project({'_id': 0})
        res = self.query(
            _filter, "PT_User")

        return res

    def get_current_user_reservation_registration(self, condition, page, count):
        '''获取当前用户及其家人的机构预约记录'''
        keys = ['id', 'organization_id']
        values = self.get_value(condition, keys)
        user_ids = [get_current_user_id(self.session)]
        # 获取家人档案相关联的人员Id
        _filter = MongoBillFilter()
        _filter.match_bill((C("main_relation_people").inner(user_ids)))\
            .project({"_id": 0})
        user_res = self.query(_filter, "PT_User_Relationship")
        if len(user_res) > 0:
            for user in user_res:
                user_ids.append(user['subordinate_relation_people'])
        _filter = MongoBillFilter()
        _filter.match_bill((C('elder_id').inner(user_ids)) & (C('elder_id') == values['id']) & (C('organization_id') == values['organization_id']))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder_info')\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization')\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'organization._id': 0, 'elder_info._id': 0})
        res = self.page_query(
            _filter, 'PT_Reservation_Registration', page, count)
        return res

    def get_current_user_activity_participate(self, condition, page, count):
        '''获取当前用户活动报名记录'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match(C('user_id') == user_id)\
            .lookup_bill('PT_Activity', 'activity_id', 'id', 'activity')\
            .lookup_bill('PT_User', 'activity.organization_id', 'id', 'organization')\
            .lookup_bill('PT_Activity_Participate', 'activity_id', 'activity_id', 'participate')\
            .add_fields({'participate_count': self.ao.size('$participate')})\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'activity._id': 0, 'organization._id': 0, 'participate': 0})
        res = self.page_query(_filter, 'PT_Activity_Participate', page, count)
        if len(res['result']) > 0:
            if len(res['result'][0]['activity']) > 0:
                now = datetime.datetime.now()
                res['result'][0]['activity'][0]['now_data'] = now
        return res

    def get_current_user_allowance_manage(self, condition, page, count):
        '''获取当前用户补贴申请记录'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('apply_user_id') == user_id)\
            .sort({'create_date': -1})\
            .project({'_id': 0, })
        res = self.page_query(_filter, 'PT_Allowance_Manage', page, count)
        return res

    def get_current_user_charitable_donate(self, condition, page, count):
        '''获取当前用户慈善申请记录'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill((C('apply_user_id') == user_id) & (C('donate_type') == '个人'))\
            .sort({'create_date': -1})\
            .project({'_id': 0, })
        res = self.page_query(_filter, 'PT_Charitable_Donate', page, count)
        return res

    def get_current_user_message_notice(self, org_list, condition, page, count):
        '''获取当前用户消息记录'''
        count = 99
        role_id = get_current_role_id(self.session)
        user_id = get_current_user_id(self.session)
        returnArray = []
        # 判断角色
        if self.checkPermission(role_id, '查看高龄津贴审核', '查询') == True:
            # if True == True:
            # 高龄津贴补助
          # print('高龄津贴补助权限')
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'old_age_allowance') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Old_Age_Allowance', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.id_card_num', 'personnel_info.id_card', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '高龄津贴',
                'data': res
            })
        if self.checkPermission(role_id, '床位管理', '查询') == True:
          # print('床位管理权限')
            # 床位预约信息
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'rservation_registration') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Reservation_Registration', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.elder_id', 'id', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '床位预约',
                'data': res
            })
        # if self.checkPermission(role_id, '活动列表', '查询') == True:
        if self.checkSpPermission(role_id, 'activityPublish', 1) == True:
            # 活动列表信息
          # print('活动列表权限')
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'activity') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Activity', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.apply_user_id', 'id', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .lookup_bill('PT_User', 'business_info.organization_id', 'id', 'org_info')\
                .add_fields({'org_info': self.ao.array_elemat("$org_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []) & (C("org_info") != None) & (C("org_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info', 'org_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '活动信息',
                'data': res
            })
        # if self.checkPermission(role_id, '新闻管理', '查询') == True:
        if self.checkSpPermission(role_id, 'newsPublish', 1) == True:
            # 新闻管理信息
          # print('资讯列表权限')
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'article') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Article', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.promulgator_id', 'id', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '养老资讯',
                'data': res
            })
        if self.checkPermission(role_id, '服务订单', '查询') == True:
            # 服务商的购买信息
          # print('服务订单权限')
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'service_order') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Service_Order', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.purchaser_id', 'id', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .lookup_bill('PT_Service_Product', 'business_info.origin_product.product_id', 'id', 'product_info')\
                .add_fields({'product_info': self.ao.array_elemat("$product_info", 0)})\
                .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
                .add_fields({'org_info': self.ao.array_elemat("$org_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []) & (C("product_info") != None) & (C("product_info") != []) & (C("org_info") != None) & (C("org_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info', 'org_info', 'product_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '服务产品',
                'data': res
            })
        if self.checkPermission(role_id, '报名列表', '查询') == True:
            # 报名列表信息
          # print('活动报名列表权限')
            _filter = MongoBillFilter()
            _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'activity_participate') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Activity_Participate', 'business_id', 'id', 'business_info')\
                .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
                .lookup_bill('PT_User', 'business_info.user_id', 'id', 'elder_info')\
                .add_fields({'elder_info': self.ao.array_elemat("$elder_info", 0)})\
                .lookup_bill('PT_Activity', 'business_info.activity_id', 'id', 'activity_info')\
                .add_fields({'activity_info': self.ao.array_elemat("$activity_info", 0)})\
                .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []) & (C("activity_info") != None) & (C("activity_info") != []))\
                .sort({'create_date': -1})\
                .project({**get_common_project({'business_info', 'elder_info', 'activity_info'})})
            res = self.page_query(_filter, 'PT_Message', page, count)
            returnArray.append({
                'name': '活动报名',
                'data': res
            })
        # if self.checkPermission(role_id, '慈善基金', '查询') == True:
        #     # 慈善基金申请信息
        #     _filter = MongoBillFilter()
        #     _filter.match_bill((C('message_state') == '未读') & (C('business_type') == 'title_fund'))\
        #         .lookup_bill('PT_Charitable_Donate', 'business_id', 'id', 'business_info')\
        #         .add_fields({'business_info': self.ao.array_elemat("$business_info", 0)})\
        #         .lookup_bill('PT_User', '$business_info.donate_user_id', 'id', 'elder_info')\
        #         .match((C("business_info") != None) & (C("business_info") != []) & (C("elder_info") != None) & (C("elder_info") != []))\
        #         .sort({'create_date': -1})\
        #         .project({'_id': 0, 'business_info._id': 0, 'elder_info._id': 0})
        #     res = self.page_query(_filter, 'PT_Message', page, count)
        #     returnArray.append({
        #         'name': '基金申请',
        #         'data': res
        #     })

        return returnArray

    def check_permission2(self, condition):
        role_id = get_current_role_id(self.session)
        return self.checkPermission(role_id, condition['f'], condition['p'])

    def check_permission(self, condition):
        if 'method' not in condition:
            return '缺少合法的方法字段！'
        # 这是用来判断是否有审批的权限，资讯详情，活动详情
        if condition['method'] == 'audit':
            if 'type' not in condition or condition['type'] not in ['article', 'activity']:
                return '缺少验证类型！'
            step_no = 1
            if condition['type'] == 'article':
                condition_type = 'newsPublish'
            elif condition['type'] == 'activity':
                condition_type = 'activityPublish'
            result = {
                'code': 'Success',
                'isAudit': self.checkSpPermission(get_current_role_id(self.session), condition_type, step_no),
            }
        # 这是用来判断我的发布页面服务商超管或者幸福院系列，服务商超管就显示产品，幸福院系列就显示活动
        elif condition['method'] == 'release':
            role_id = get_current_role_id(self.session)

            result = {}
            # 服务商超管ID
            if self.checkPermission(role_id, '服务产品', '查询') == True:
                result['is_product'] = True
            # 幸福院超管和幸福院工作人员
            if self.checkPermission(role_id, '活动列表', '查询') == True:
                result['is_activity'] = True

        # 这是用来判断我的受理有哪些权限的
        elif condition['method'] == 'acceptance':
            role_id = get_current_role_id(self.session)

            result = []
            # 床位管理
            if self.checkPermission(role_id, '床位管理', '查询') == True:
                result.append({
                    'name': '床位预约',
                })
            # 高龄津贴
            if self.checkPermission(role_id, '查看高龄津贴审核', '查询') == True:
                result.append({
                    'name': '高龄津贴',
                })
            # 冠名基金
            if self.checkSpPermission(role_id, 'titleFundApply', 2) == True:
                result.append({
                    'name': '冠名基金',
                })
            # 养老资讯
            if self.checkSpPermission(role_id, 'newsPublish', 1) == True:
                result.append({
                    'name': '养老资讯',
                })
            # 活动信息
            if self.checkSpPermission(role_id, 'activityPublish', 1) == True:
                result.append({
                    'name': '活动信息',
                })
        return result

    def get_current_user_acceptance(self, org_list, condition, page, count):
        '''获取当前用户受理记录'''
        role_id = get_current_role_id(self.session)
        user_id = get_current_user_id(self.session)
        res = []
        if 'type' in condition:
            stype = condition['type']
        else:
            stype = False
        # 判断角色
        if stype == '床位预约' and self.checkPermission(role_id, '床位管理', '查询') == True:
            # 床位预约信息
            _filter = MongoBillFilter()
            _filter.match_bill((C('bill_status') == 'valid') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_User', 'elder_id', 'id', 'elder_info')\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'elder_info._id': 0})
            res = self.page_query(
                _filter, 'PT_Reservation_Registration', page, count)
        if stype == '高龄津贴' and self.checkPermission(role_id, '查看高龄津贴审核', '查询') == True:
            # 高龄津贴补助
            _filter = MongoBillFilter()
            _filter.match_bill((C('bill_status') == 'valid') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_User', 'id_card_num', 'personnel_info.id_card', 'elder_info')\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'elder_info._id': 0})
            res = self.page_query(_filter, 'PT_Old_Age_Allowance', page, count)
        # if self.checkPermission(role_id, '冠名基金', '查询') == True:
        if stype == '冠名基金' and self.checkSpPermission(role_id, 'titleFundApply', 2) == True:

            # 获取审批定义id
            _filter_dtype = MongoBillFilter()
            _filter_dtype.match_bill((C('approval_type') == 'titleFundApply'))\
                .project({'_id': 0, })
            res_dtype = self.query(_filter_dtype, 'PT_Approval_Define')

            # 冠名基金
            _filter = MongoBillFilter()
            _filter.match_bill((C('donate_type') == '基金') & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Approval_Process', 'id', 'business_id', 'proccess_info')\
                .add_fields({'proccess': self.ao.array_filter(
                    "$proccess_info", "fi", ((F('$fi.proccess_user_id') == user_id) & (F('$fi.define_id') == res_dtype[0]['id']) & ((F('$fi.status') == '通过') | (F('$fi.status') == '不通过'))).f)})\
                .match((C("proccess") != None) & (C("proccess") != []))\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'proccess_info._id': 0, 'proccess._id': 0})
            res = self.page_query(_filter, 'PT_Charitable_Donate', page, count)
        if stype == '养老资讯' and self.checkSpPermission(role_id, 'newsPublish', 1) == True:

            # 获取审批定义id
            _filter_dtype = MongoBillFilter()
            _filter_dtype.match_bill((C('approval_type') == 'newsPublish'))\
                .project({'_id': 0, })
            res_dtype = self.query(_filter_dtype, 'PT_Approval_Define')
            # 养老资讯
            _filter = MongoBillFilter()
            _filter.match_bill((C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Approval_Process', 'id', 'business_id', 'proccess_info')\
                .add_fields({'proccess': self.ao.array_filter(
                    "$proccess_info", "fi", ((F('$fi.proccess_user_id') == user_id) & (F('$fi.define_id') == res_dtype[0]['id']) & ((F('$fi.status') == '通过') | (F('$fi.status') == '不通过'))).f)})\
                .match((C("proccess") != None) & (C("proccess") != []))\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'proccess_info._id': 0, 'proccess._id': 0})

            res = self.page_query(_filter, 'PT_Article', page, count)
        if stype == '活动信息' and self.checkSpPermission(role_id, 'activityPublish', 1) == True:

            # 获取审批定义id
            _filter_dtype = MongoBillFilter()
            _filter_dtype.match_bill((C('approval_type') == 'activityPublish'))\
                .project({'_id': 0, })
            res_dtype = self.query(_filter_dtype, 'PT_Approval_Define')
            # 活动信息
            _filter = MongoBillFilter()
            _filter.match_bill((C('organization_id').inner(org_list)))\
                .lookup_bill('PT_Approval_Process', 'id', 'business_id', 'proccess_info')\
                .add_fields({'proccess': self.ao.array_filter(
                    "$proccess_info", "fi", ((F('$fi.proccess_user_id') == user_id) & (F('$fi.define_id') == res_dtype[0]['id']) & ((F('$fi.status') == '通过') | (F('$fi.status') == '不通过'))).f)})\
                .match((C("proccess") != None) & (C("proccess") != []))\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'proccess_info._id': 0, 'proccess._id': 0})

            res = self.page_query(_filter, 'PT_Activity', page, count)

        return res

    def checkSpPermission(self, role_id, define_type, step_no):
        # 根据define_type在PT_Approval_Define表检查这个帐号是否有审核的权限
        _filter = MongoBillFilter()
        _filter.match_bill((C('approval_type') == define_type))\
            .project({'_id': 0, })
        res = self.query(_filter, 'PT_Approval_Define')

        # 有这个权限
        if len(res) > 0 and 'approval_list' in res[0]:
            for item in res[0]['approval_list']:
                if item['step_no'] == step_no and item['approval_user_id'] == role_id:
                    return True
        return False

    def checkPermission(self, role_id, name, operation):
        # 根据permission表检测这个帐号是否有查看的权限
        _filter = MongoBillFilter()
        _filter.match_bill((C('role_id') == role_id) & (C('function') == name) & (C('permission') == operation))\
            .sort({'create_date': -1})\
            .project({'_id': 0, })
        res = self.query(_filter, 'PT_Permission')

        if len(res) > 0:
            return True
        else:
            return False

    def import_excel_manage(self, stype, sdata):
        return self.import_excel_servie.import_excel_api(stype, sdata)

    def getOrderByBussiness(self, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .lookup_bill('PT_Service_Order', 'id', 'service_provider_id', 'order_list')\
            .add_fields({'unserved_num': self.ao.size(self.ao.array_filter(
                "$order_list", "order", ((F('$order.status') == '未服务')).f))})\
            .add_fields({'going_num': self.ao.size(self.ao.array_filter(
                "$order_list", "order", ((F('$order.status') == '服务中')).f))})\
            .add_fields({'completed_num': self.ao.size(self.ao.array_filter(
                "$order_list", "order", ((F('$order.status') == '已完成')).f))})\
            .add_fields({'assign_num': self.ao.size(self.ao.array_filter(
                "$order_list", "order", ((F('$order.status') == '已分派')).f))})\
            .project({
                '_id': 0,
                'assign_num': 1,
                'completed_num': 1,
                'going_num': 1,
                'unserved_num': 1,
                'organization_info.lat': 1,
                'organization_info.lon': 1,
            })
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def create_draft(self, data):

        res = 'Fail'

        data['user_id'] = get_current_user_id(self.session)

        data_info = get_info(data, self.session)

        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.draft.value, data_info, 'PT_Draft')
        if bill_id:
            res = 'Success'

        return res

    def get_draft_list(self, condition):

        keys = ['id', 'type']

        values = self.get_value(condition, keys)

        user_id = get_current_user_id(self.session)

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('user_id') == user_id) & (C('type') == values['type']))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.page_query(_filter, 'PT_Draft', 1, 1)

        return res

    def get_ys_xfy_list(self, org_list, condition, page, count):
        '''查询 验收幸福院列表  '''
        current_date = get_cur_time()
        keys = ['id', 'admin_area_id', 'name',
                'star_level', 'village', 'zhenjie']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('admin_area_id') == values['admin_area_id'])
                           & (C('name').like(values['name']))
                           & (C('personnel_type') == '2')
                           & (C('id').inner(org_list))
                           & (C('organization_info.personnel_category') == '幸福院'))\
            .add_fields({'level': '$organization_info.star_level'})\
            .add_fields({'zhenjie': '$organization_info.zhenjie'})\
            .add_fields({'check_date': '$organization_info.check_date'})\
            .add_fields({'zhenjie_ids': '$organization_info.zhenjie_ids'})\
            .add_fields({'address': '$organization_info.address'})\
            .add_fields({'time': ((F('organization_info.check_date')-current_date)*(-1)).f})\
            .add_fields({'days': self.ao.floor((F('time').__truediv__(86400000)).f)})\
            .add_fields({
                'star_level': self.ao.switch([
                    self.ao.case(
                        ((F('level') == '5') | (F('level') == 5)), '五星级'),
                    self.ao.case(
                        ((F('level') == '4') | (F('level') == 4)), '四星级'),
                    self.ao.case(
                        ((F('level') == '3') | (F('level') == 3)), '三星级'),
                    self.ao.case(
                        ((F('level') == '2') | (F('level') == 2)), '二星级'),
                    self.ao.case(
                        ((F('level') == '1') | (F('level') == 1)), '一星级'),
                    self.ao.case(
                        ((F('level') == '0') | (F('level') == 0)), '零星级'),
                ], '$level')})\
            .match_bill((C('star_level') == values['star_level']))\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'area_info')\
            .add_fields({'zhenjie': '$area_info.name'})
        if 'status' in condition.keys() and condition['status'] == '已验收':
            _filter.match_bill((C('days') >= 1))
        elif 'status' in condition.keys() and condition['status'] == '未验收':
            _filter.match_bill((C('days') < 1) | ((C('days') == None)))
        _filter.sort({'modify_date': -1})\
            .project({
                **get_common_project({'area_info'})
            })
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def update_ys_xfy(self, data):
        '''新增验收幸福院'''
        res = 'Fail'
        if 'id' in list(data.keys()):

            # 找出幸福院
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == data['id'])
                               & (C('personnel_type') == '2')
                               & (C('organization_info.personnel_category') == '幸福院'))\
                .project({'_id': 0, })

            result = self.query(_filter, 'PT_User')

            if len(result) == 0:
                return '没有找到该幸福院'

            data_info = result[0]

            if 'star_level' in data:
                data_info['organization_info']['star_level'] = data['star_level']
            if 'check_date' in data:
                data_info['organization_info']['check_date'] = as_date(
                    data['check_date'])
            if 'zhenjie_ids' in data:
                data_info['organization_info']['zhenjie_ids'] = data['zhenjie_ids']
            if 'name' in data:
                data_info['name'] = data['name']
            if 'admin_area_id' in data:
                data_info['admin_area_id'] = data['admin_area_id']

            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.personnelClassification.value, data_info, 'PT_User')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.personnelClassification.value, data, 'PT_User')
            if bill_id:
                res = 'Success'
        return res

    def get_elder_healthy_list(self, org_list, condition, page, count):

        # 获取长者信息列表
        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"

        keys = ['id', 'elder_sex', 'pk_member',
                'elder_name', 'begin_date', 'end_date']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match(
            (C('id') == values['id'])
            & (C('pk_member') != '')
            & (C('pk_member') == values['pk_member'])
            & (C('create_date') >= as_date(values['begin_date']))
            & (C('create_date') <= as_date(values['end_date'])))\
            .lookup('PT_User', 'pk_member', 'personnel_info.id_card', 'elder_info')\
            .lookup('PT_Healthy_Scheme_Data_Report', 'pk_phy_exam_report', 'pk_phy_exam_report', 'scheme_data_report')\
            .lookup('PT_Healthy_Physchemeitem', 'scheme_data_report.pk_phy_exam_scheme_item', 'pk_phy_exam_scheme_item', 'physchemeitem')\
            .lookup('PT_Healthy_Phyexamscheme', 'physchemeitem.pk_phy_exam_scheme', 'pk_phy_exam_scheme', 'phyexamscheme')\
            .lookup('PT_Healthy_Phyexamdept', 'physchemeitem.pk_phy_exam_dept', 'pk_phy_exam_scheme', 'phyexamdept')\
            .lookup('PT_Healthy_Examdata', 'scheme_data_report.pk_exam_data', 'pk_exam_data', 'examdata')\
            .lookup('PT_Healthy_Examdataitem', 'examdata.pk_exam_data_item', 'pk_exam_data_item', 'examdataitem')\
            .lookup('PT_Healthy_Examdatatype', 'examdataitem.pk_exam_data_type', 'pk_exam_data_type', 'examdatatype')\
            .match((C("elder_info") != None) & (C("elder_info") != []))\
            .add_fields({
                "exam_scheme": self.ao.array_elemat("$phyexamscheme.name", 0),
                "exam_class": self.ao.array_elemat("$phyexamdept.name", 0),
                "elder_name": self.ao.array_elemat("$elder_info.name", 0),
                "elder_sex": self.ao.array_elemat("$elder_info.personnel_info.sex", 0),
                "elder_telephone": self.ao.array_elemat("$elder_info.personnel_info.telephone", 0),
            })\
            .match((C('elder_name').like(values['elder_name'])) & (C('elder_sex') == values['elder_sex']))\
            .sort({'pk_phy_exam_report': -1})\
            .project({'_id': 0,
                      'elder_info': 0,
                      'examdata._id': 0,
                      'examdataitem._id': 0,
                      'examdatatype._id': 0,
                      'scheme_data_report': 0,
                      'physchemeitem': 0,
                      'phyexamdept': 0,
                      'modify_date': 0,
                      'estimate_status': 0,
                      'exam_evaluation': 0,
                      'organization_id': 0,
                      'physical_type': 0,
                      'pk_service_point': 0,
                      'pk_user': 0,
                      'update_time': 0,
                      'user_complaints': 0,
                      'version': 0,
                      })

        res = self.page_query(_filter, 'PT_Healthy_Report', page, count)

        # 补全体检项目
        if 'result' in res and len(res['result']) > 0:
            for item in res['result']:
                if 'examdataitem' in item and len(item['examdataitem']) > 0:
                    for itm in item['examdataitem']:

                        itm['pk_exam_data_item'] = self.toString(
                            itm['pk_exam_data_item'])

                        # 需要补全的数据，键，值，单位，范围
                        key = ''
                        value = ''
                        unit = itm['name']
                        status = ''

                        # 补全键
                        key = 'healthy_' + itm['pk_exam_data_item']

                        # 补全值和单位和范围
                        if 'examdata' in item and len(item['examdata']) > 0:
                            for i in item['examdata']:

                                i['pk_exam_data_item'] = self.toString(
                                    i['pk_exam_data_item'])
                                itm['pk_exam_data_item'] = self.toString(
                                    itm['pk_exam_data_item'])

                                if i['pk_exam_data_item'] == itm['pk_exam_data_item']:

                                    # 范围
                                    if 'value_status' in i:
                                        status = i['value_status']

                                    # 值
                                    value = i['value']
                                    break

                        key = self.toString(key)
                        value = self.toString(value)
                        unit = self.toString(unit)
                        status = self.toString(status)

                        if key != '':
                            if value != '':
                                item[key] = value + unit
                            else:
                                item[key] = ''
                            item[key + '_status'] = status

        return res

    def toString(self, string):
        if type(string) != str:
            return str(string)
        return string

    def get_elder_food_list(self, org_list, condition, page, count):
        '''长者用餐统计列表'''
        res = 'Fail'

        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"

        keys = ['id', 'elder_name', 'begin_date', 'end_date']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
            & (C('create_date') >= as_date(values['begin_date']))
            & (C('create_date') <= as_date(values['end_date'])))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder_info')\
            .match((C("elder_info") != None) & (C("elder_info") != []))\
            .add_fields({
                "elder_name": self.ao.array_elemat("$elder_info.name", 0),
                "user_name": "$elder_info.name",
            })\
            .match_bill((C('elder_name').like(values['elder_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'elder_id': 1,
                      'elder_name': 1,
                      'user_name': 1,
                      'use_food_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Elder_Food', page, count)

        return res

    def update_elder_food(self, data):
        '''新增长者用餐'''
        res = 'Fail'

        if 'use_food_date' in data:
            data['use_food_date'] = datetime.datetime.strptime(
                data['use_food_date'], '%Y-%m-%dT%H:%M:%S.%fZ')

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.elderFood.value, data, 'PT_Elder_Food')
            if bill_id:
                res = 'Success'
        else:

            data['create_id'] = get_current_user_id(self.session)

            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.elderFood.value, data, 'PT_Elder_Food')
            if bill_id:
                res = 'Success'
        return res

    def delete_elder_food(self, ids):
        ''' 删除长者用餐 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.elderFood.value, {'id': i}, 'PT_Elder_Food')
        if bill_id:
            res = 'Success'
        return res

    def get_deduction_list(self, org_list, condition, page, count):
        '''收费减免列表'''
        res = 'Fail'

        keys = ['id', 'pay_user_id_card', 'get_user_name', 'pay_user_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'get_user_id', 'id', 'get_info')\
            .lookup_bill('PT_User', 'pay_user_id', 'id', 'pay_info')\
            .match((C("get_info") != None) & (C("get_info") != []) & (C("pay_info") != None) & (C("pay_info") != []))\
            .add_fields({
                "get_user_name": self.ao.array_elemat("$get_info.name", 0),
                "pay_user_name": self.ao.array_elemat("$pay_info.name", 0),
                "pay_user_id_card": self.ao.array_elemat("$pay_info.id_card", 0),
            })\
            .match_bill((C('get_user_name').like(values['get_user_name'])) & (C('pay_user_name').like(values['pay_user_name'])) & (C('pay_user_id_card') == values['pay_user_id_card']))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'get_user_id': 1,
                      'pay_user_id': 1,
                      'get_user_name': 1,
                      'pay_user_name': 1,
                      'create_date': 1,
                      'get_card_no': 1,
                      'price': 1,
                      })

        res = self.page_query(_filter, 'PT_Deduction', page, count)

        return res

    def update_deduction(self, data):
        '''新增收费减免'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.deduction.value, data, 'PT_Deduction')
            if bill_id:
                res = 'Success'
        else:

            data['create_id'] = get_current_user_id(self.session)

            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.deduction.value, data, 'PT_Deduction')
            if bill_id:
                res = 'Success'
        return res

    def delete_deduction(self, ids):
        ''' 删除收费管理 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.deduction.value, {'id': i}, 'PT_Deduction')
        if bill_id:
            res = 'Success'
        return res

    def get_set_class_type_list(self, org_list, condition, page, count):
        '''排班类型'''
        res = 'Fail'

        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'time_range': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Set_Class_Type', page, count)

        return res

    def update_set_class_type(self, data):
        '''新增排班类型'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.setClassType.value, data, 'PT_Set_Class_Type')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.setClassType.value, data, 'PT_Set_Class_Type')
            if bill_id:
                res = 'Success'
        return res

    def delete_set_class_type(self, ids):
        ''' 删除排班类型 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.setClassType.value, {'id': i}, 'PT_Set_Class_Type')
        if bill_id:
            res = 'Success'
        return res

    def get_set_class_list(self, org_list, condition, page, count):
        '''排班'''
        res = 'Fail'

        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .lookup_bill('PT_Set_Class_Type', 'type_id', 'id', 'type_info')\
            .add_fields({
                "user_name": self.ao.array_elemat("$user_info.name", 0),
                "id_card_type": self.ao.array_elemat("$user_info.id_card_type", 0),
                "id_card": self.ao.array_elemat("$user_info.id_card", 0),
                "sex": self.ao.array_elemat("$user_info.personnel_info.sex", 0),
                "telephone": self.ao.array_elemat("$user_info.personnel_info.telephone", 0),
                "type_name": self.ao.array_elemat("$type_info.name", 0),
            })\
            .match_bill((C('user_name').like(values['name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'user_id': 1,
                      'type_id': 1,
                      'user_name': 1,
                      'sex': 1,
                      'id_card': 1,
                      'id_card_type': 1,
                      'telephone': 1,
                      'telephone': 1,
                      'id_card_type': 1,
                      'type_name': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Set_Class', page, count)

        return res

    def update_set_class(self, data):
        '''新增排班'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.setClass.value, data, 'PT_Set_Class')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.setClass.value, data, 'PT_Set_Class')
            if bill_id:
                res = 'Success'
        return res

    def delete_set_class(self, ids):
        ''' 删除排班 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.setClass.value, {'id': i}, 'PT_Set_Class')
        if bill_id:
            res = 'Success'
        return res

    def get_nursing_pay_list(self, org_list, condition, page, count):
        '''护理收费'''
        res = 'Fail'

        keys = ['id', 'name', 'order_code', 'project_id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('project_id') == values['project_id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order_info')\
            .lookup_bill('PT_Assessment_Project', 'project_id', 'id', 'project_info')\
            .add_fields({
                "project_name": self.ao.array_elemat("$project_info.project_name", 0),
                "order_code": self.ao.array_elemat("$order_info.order_code", 0),
                "nursing_name": self.ao.array_elemat("$user_info.name", 0),
            })\
            .match_bill((C('order_code').like(values['order_code'])))\
            .sort({'modify_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'user_id': 1,
                      'project_id': 1,
                      'order_id': 1,
                      'project_name': 1,
                      'nursing_name': 1,
                      'order_code': 1,
                      'begin_date': 1,
                      'end_date': 1,
                      'price': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Nursing_Pay', page, count)

        return res

    def update_nursing_pay(self, data):
        '''新增护理收费'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.nursingPay.value, data, 'PT_Nursing_Pay')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.nursingPay.value, data, 'PT_Nursing_Pay')
            if bill_id:
                res = 'Success'
        return res

    def delete_nursing_pay(self, ids):
        ''' 删除排班类型 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.nursingPay.value, {'id': i}, 'PT_Nursing_Pay')
        if bill_id:
            res = 'Success'
        return res

    def update_xfy_target_setting(self, data):
        '''新增幸福院评比指标'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.targetSettingDetails.value, data, 'PT_XFY_TargetSetting')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.targetSettingDetails.value, data, 'PT_XFY_TargetSetting')
            if bill_id:
                res = 'Success'
        return res

    def get_xfy_target_setting_list(self, condition, page, count):
        '''查询 幸福院评比指标列表  '''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'name': 1,
                'description': 1,
                'modify_date': 1,
                'create_date': 1,
                'id': 1,
            })
        res = self.page_query(_filter, 'PT_XFY_TargetSetting', page, count)
        return res

    def del_xfy_target_setting(self, ids):
        ''' 删除幸福院评比指标 '''

        # 先判断有没有二级指标绑定
        _filter = MongoBillFilter()
        _filter.match_bill((C('first_level_id').inner(ids))
                           ).project({'_id': 0, 'id': 1})
        result = self.query(_filter, 'PT_XFY_TargetSetting_Details')

        if len(result) > 0:
            return '该指标已被引用，删除后会影响引用的其他指标，请先删除已二级指标！'

        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.targetSettingDetails.value, {'id': i}, 'PT_XFY_TargetSetting')
        if bill_id:
            res = 'Success'
        return res

    def update_xfy_target_setting_details(self, data):
        '''新增幸福院评比详细指标'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.targetSettingDetails.value, data, 'PT_XFY_TargetSetting_Details')
            if bill_id:
                res = 'Success'
        else:
            data['code'] = self.get_max_code('PT_XFY_TargetSetting_Details')
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.targetSettingDetails.value, data, 'PT_XFY_TargetSetting_Details')
            if bill_id:
                res = 'Success'
        return res

    def get_xfy_target_setting_details_list(self, condition, page, count):
        '''查询 幸福院评比详细指标列表  '''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .lookup_bill('PT_XFY_TargetSetting', 'first_level_id', 'id', 'first_info')\
            .add_fields({
                "first_level_name": self.ao.array_elemat("$first_info.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'code': 1,
                'first_level_id': 1,
                'first_level_name': 1,
                'second_level_name': 1,
                'third_level_name': 1,
                'evaluation_rules': 1,
                'base_score': 1,
                'bonus_points': 1,
                'modify_date': 1,
                'create_date': 1,
            })
        res = self.page_query(
            _filter, 'PT_XFY_TargetSetting_Details', page, count)
        return res

    def del_xfy_target_setting_details(self, ids):
        ''' 删除幸福院评比详细指标 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.targetSettingDetails.value, {'id': i}, 'PT_XFY_TargetSetting_Details')
        if bill_id:
            res = 'Success'
        return res

    def upload_index_excel(self, data):
        pf = pd.DataFrame(data)
        pf.rename(columns={'一级指标': 'First_level_index', '二级指标': 'Two_level_index', '三级指标': 'Three_level_index', '评比细则': 'evaluation_rules',
                           '基础分': 'base_score', '基础分': 'bonus_points'}, inplace=True)
        First_level_index_list = pf['First_level_index'].values.tolist()
        _filter = MongoBillFilter()
        _filter.match_bill(C('First_level_index').inner(First_level_index_list)) \
               .project({'_id': 0, 'First_level_index': 1, 'id': 1})
        res = self.query(_filter, 'PT_XFY_TargetSetting')
        if len(res) == 0:
            raise JsonRpc2Error(-36101, '导入到数据中所有的一级类型有误，请仔细检查')
        index_pf = pd.DataFrame(res)
        new_pf = pd.merge(pf, index_pf, on=['First_level_index'])
        new_pf.rename(columns={'id': 'First_level_index_id'}, inplace=True)
        if len(new_pf.index) != len(pf.index):
            raise JsonRpc2Error(-36101, '导入到数据中某些一级类型有误，请仔细检查')
        new_pf = new_pf.drop(['First_level_index'], axis=1)
        condition = dataframe_to_list(new_pf)
        for i, x in enumerate(condition):
            try:
                parms = self.update_xfy_target_setting_details(x)
                if parms == 'Success':
                    return '导入成功'
                else:
                    return '导入失败'
            except:
                return '导入失败'

    def user_org_link(self, data):
        '''新增用户和当前账号所属机构关联数据'''
        res = 'Fail'
        isdata = {
            "user_id": data
        }
        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.ratingPlan.value, isdata, 'PT_User_Org_Link')
        if bill_id:
            res = 'Success'
        return res

    def user_org_link_list(self, org_list, condition, page=None, count=None):
        '''查询用户关联机构数据'''
        res = 'Fail'
        keys = ['name', 'id_card', 'id', 'sex', 'child_num', "days"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C("id") == values["id"]))
        _filter.inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
            .match_bill((C("user_info.name").like(values["name"]))
                        & (C("user_info.id_card").like(values["id_card"]))
                        & (C("user_info.personnel_info.sex") == values["sex"])
                        & (C('organization_id').inner(org_list))
                        & (C("user_info.child_num") == values["child_num"])
                        & (C('bill_status') == 'valid')
                        )\
            .project({'_id': 0, "user_info._id": 0})
        res = self.page_query(_filter, 'PT_User_Org_Link', page, count)
        return res

    def delete_user_org_link(self, condition):
        ''' 删除长者关联数据 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_User_Org_Link')
        if bill_id:
            res = 'Success'
        return res

    def get_rating_plan_list(self, org_list, condition, page, count):
        '''评比方案'''
        res = 'Fail'

        keys = ['id', 'name', 'org_name', 'org_id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('org_id') == values['org_id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'org_id', 'id', 'org_info')\
            .match((C("org_info") != None) & (C("org_info") != []))\
            .lookup_bill('PT_Administration_Division', 'org_info.admin_area_id', 'id', 'division_info')\
            .lookup_bill('PT_User', 'org_info.organization_info.super_org_id', 'id', 'sup_info')\
            .add_fields({
                "org_name": self.ao.array_elemat("$org_info.name", 0),
                "admin_area_name": self.ao.array_elemat("$division_info.name", 0),
                "parent_org_name": self.ao.array_elemat("$sup_info.name", 0),
                "org_type": self.ao.array_elemat("$org_info.organization_info.personnel_category", 0),
            })\
            .match((C("org_name").like(values['org_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'org_name': 1,
                      'org_id': 1,
                      'org_type': 1,
                      'name': 1,
                      'admin_area_name': 1,
                      'parent_org_name': 1,
                      'remark': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Rating_Plan', page, count)

        return res

    def update_rating_plan(self, data):
        '''新增评比方案'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.ratingPlan.value, data, 'PT_Rating_Plan')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.ratingPlan.value, data, 'PT_Rating_Plan')
            if bill_id:
                res = 'Success'
        return res

    def delete_rating_plan(self, ids):
        ''' 删除评比方案 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.ratingPlan.value, {'id': i}, 'PT_Rating_Plan')
        if bill_id:
            res = 'Success'
        return res

    def get_deal_and_report_list(self, org_list, condition, page, count):
        '''项目协议与报告'''
        res = 'Fail'

        keys = ['id', 'remark', 'org_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('remark').like(values['remark']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .match((C("org_info") != None) & (C("org_info") != []))\
            .add_fields({
                "org_name": self.ao.array_elemat("$org_info.name", 0),
            })\
            .match_bill((C("org_name").like(values['org_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'org_name': 1,
                      'org_id': 1,
                      'deal': 1,
                      'report': 1,
                      'remark': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Deal_Report', page, count)

        return res

    def update_deal_and_report(self, data):
        '''新增编辑项目协议与报告'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.dealReport.value, data, 'PT_Deal_Report')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.dealReport.value, data, 'PT_Deal_Report')
            if bill_id:
                res = 'Success'
        return res

    def delete_deal_and_report(self, ids):
        ''' 删除项目协议与报告 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.dealReport.value, {'id': i}, 'PT_Deal_Report')
        if bill_id:
            res = 'Success'
        return res

    def get_month_plan_list(self, org_list, condition, page, count):
        '''月度计划'''
        res = 'Fail'

        keys = ['id', 'name', 'remark']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('remark').like(values['remark']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .match((C("org_info") != None) & (C("org_info") != []))\
            .add_fields({
                "org_name": self.ao.array_elemat("$org_info.name", 0),
            })\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'plan': 1,
                      'org_name': 1,
                      'org_id': 1,
                      'remark': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Month_Plan', page, count)

        return res

    def update_month_plan(self, data):
        '''新增编辑月度计划'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.monthPlan.value, data, 'PT_Month_Plan')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.monthPlan.value, data, 'PT_Month_Plan')
            if bill_id:
                res = 'Success'
        return res

    def delete_month_plan(self, ids):
        ''' 删除月度计划 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.monthPlan.value, {'id': i}, 'PT_Month_Plan')
        if bill_id:
            res = 'Success'
        return res

    def get_org_bed_fund_list(self, org_list, condition, page, count):
        return False

    def get_org_operation_fund_list(self, org_list, condition, page, count):
        return False
        # 找出所有民办非营机构
        # res = 'Fail'
        # keys = ['id', 'remark']
        # values = self.get_value(condition, keys)
        # _filter = MongoBillFilter()
        # _filter.match_bill(
        #     (C('id') == values['id'])
        #     & (C('personnel_type') == '2')
        #     & (C('organization_info.organization_nature') == '民办非营利'))\
        #     .sort({'create_date': -1})\
        #     .project({'_id': 0})

        # res = self.page_query(_filter, 'PT_User', page, count)

        # # 如果有就循环
        # if 'result' in res and len(res['result']) > 0:
        #     # 初始化数据
        #     temp_org_ids = []

        #     # 当前年
        #     current_year = get_cur_time().year

        #     current_year_begin = datetime.datetime.strptime(
        #         str(current_year) + '-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        #     current_year_end = datetime.datetime.strptime(
        #         str(current_year) + '-12-31 23:59:59', '%Y-%m-%d %H:%M:%S')

        #     # 先循环一次，获取组织机构的区域id，方便过滤长者和活动
        #     for item_ids in res['result']:
        #         temp_org_ids.append(item_ids['id'])

        #     # 获取所有奖励
        #     _filter_income = MongoBillFilter()
        #     _filter_income.match_bill((C('organization_id').inner(temp_org_ids)))\
        #         .sort({'create_date': -1})\
        #         .project({'_id': 0})
        #     res_income = self.query(_filter_income, 'PT_XFY_Business_Income')

    def get_yearplan_and_summary_list(self, org_list, condition, page, count):
        '''年度计划与总结'''
        res = 'Fail'

        keys = ['id', 'remark', 'org_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('remark').like(values['remark']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .match((C("org_info") != None) & (C("org_info") != []))\
            .add_fields({
                "org_name": self.ao.array_elemat("$org_info.name", 0),
            })\
            .match_bill((C("org_name").like(values['org_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'org_name': 1,
                      'plan': 1,
                      'report': 1,
                      'org_id': 1,
                      'remark': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Yearplan_Summary', page, count)

        return res

    def update_yearplan_and_summary(self, data):
        '''新增编辑年度计划与报告'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.yearSummary.value, data, 'PT_Yearplan_Summary')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.yearSummary.value, data, 'PT_Yearplan_Summary')
            if bill_id:
                res = 'Success'
        return res

    def delete_yearplan_and_summary(self, ids):
        ''' 删除年度计划与报告 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.yearSummary.value, {'id': i}, 'PT_Yearplan_Summary')
        if bill_id:
            res = 'Success'
        return res

    def get_rating_list(self, org_list, condition, page, count):
        '''评比填表'''
        res = 'Fail'

        keys = ['id', 'name', 'org_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'org_id', 'id', 'org_info')\
            .match((C("org_info") != None) & (C("org_info") != []))\
            .lookup_bill('PT_Administration_Division', 'org_info.admin_area_id', 'id', 'division_info')\
            .lookup_bill('PT_User', 'org_info.organization_info.super_org_id', 'id', 'sup_info')\
            .add_fields({
                "org_name": self.ao.array_elemat("$org_info.name", 0),
                "admin_area_name": self.ao.array_elemat("$division_info.name", 0),
                "parent_org_name": self.ao.array_elemat("$sup_info.name", 0),
                "org_type": self.ao.array_elemat("$org_info.organization_info.personnel_category", 0),
            })\
            .match((C("org_name").like(values['org_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'org_name': 1,
                      'org_id': 1,
                      'org_type': 1,
                      'plan_id': 1,
                      'name': 1,
                      'admin_area_name': 1,
                      'parent_org_name': 1,
                      'remark': 1,
                      'score': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Rating', page, count)

        return res

    def update_rating(self, data):
        '''新增评比填表'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.rating.value, data, 'PT_Rating')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.rating.value, data, 'PT_Rating')
            if bill_id:
                res = 'Success'
        return res

    def delete_rating(self, ids):
        ''' 删除评比填表 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.rating.value, {'id': i}, 'PT_Rating')
        if bill_id:
            res = 'Success'
        return res

    def get_groups_volunteer_list(self, org_list, condition, page, count):
        '''获取团队志愿者列表'''
        current_organization_id = get_current_organization_id(self.session)
        current_organization_id = '02415d7a-06a9-11ea-ae8e-7c2a3115762d'
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id') == current_organization_id)).project(
            {'_id': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_Groups_Member', page, count)

        return res

    def get_groups_list(self, org_list, condition, page, count):
        '''团体列表'''
        res = 'Fail'

        keys = ['id', 'name', 'captain_name', 'org_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Groups_Member', 'id', 'group_id', 'member_list')\
            .sort({'create_date': -1})\
            .add_fields({'captain_info': self.ao.array_filter(
                "$member_list", "mi", ((F('$mi.type') == 'captain')).f)})\
            .add_fields({
                "captain_name": self.ao.array_elemat("$captain_info.name", 0),
                "captain_telephone": self.ao.array_elemat("$captain_info.telephone", 0),
                'member_num': self.ao.size('$member_list')
            })\
            .lookup_bill('PT_User', 'id', 'organization_id', 'org')\
            .project({'org._id': 0})\
            .add_fields({
                "org_name": "$org.name",
            })\
            .match_bill((C('captain_name').like(values['captain_name'])) & (C('org_name').like(values['org_name'])))\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'captain_name': 1,
                      'captain_telephone': 1,
                      'member_num': 1,
                      'description': 1,
                      'founded_date': 1,
                      'photo': 1,
                      'create_date': 1,
                      'member_list': 1,
                      }).project({
                          'member_list._id': 0,
                          'member_list.GUID': 0,
                          'member_list.create_date': 0,
                          'member_list.bill_status': 0,
                          'member_list.valid_bill_id': 0,
                          'member_list.version': 0,
                      })

        res = self.page_query(_filter, 'PT_Groups', page, count)

        return res

    def update_groups(self, data):
        '''新增团体'''
        res = 'Fail'

        if 'founded_date' in data:
            data['founded_date'] = string_to_date(data['founded_date'])

        # 新增团体成员
        if 'type' in data:
            # 删除标识字段
            if data['type'] in ['member', 'captain']:
                # 新增会员或队长
                return self.update_groups_member(data)
            del(data['type'])

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.groups.value, data, 'PT_Groups')
            if bill_id:
                res = 'Success'
        else:
            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.groups.value, data_info, 'PT_Groups')
            if bill_id:
                res = 'Success'
                res = data_info['id']
        return res

    def update_groups_member(self, data):
        '''新增团体成员'''

        # 根据身份证号判断是否存在
        _filter_id_card = MongoBillFilter()
        _filter_id_card.match_bill((C('id_card') == data['id_card']) & (
            C('group_id') == data['group_id'])).project({'_id': 0})
        res_id_card = self.query(_filter_id_card, 'PT_Groups_Member')

        if len(res_id_card) > 0:
            return '该身份证号已经有团员使用'

        if data['type'] == 'captain':
            # 修改队长信息
            _filter_captain = MongoBillFilter()
            _filter_captain.match_bill((C('group_id') == data['group_id']) & (
                C('type') == 'captain')).project({'_id': 0})
            res_captain = self.query(_filter_captain, 'PT_Groups_Member')

            if len(res_captain) > 0:
                # 以数据库的队长id为准
                data['id'] = res_captain[0]['id']

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.groupsMember.value, data, 'PT_Groups_Member')
            if bill_id:
                res = 'Success'
        else:
            data_info = get_info(data, self.session)

            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.groupsMember.value, data_info, 'PT_Groups_Member')
            if bill_id:
                if data['type'] == 'caoptain':
                    # 为方便搜索，在新增队长的时候，在主数据新增队长id
                    bill_id = self.bill_manage_service.add_bill(
                        OperationType.update.value, TypeId.groups.value, {'id': data_info['group_id'], 'captain_id': data_info['id']}, 'PT_Groups')
                    if bill_id:
                        res = 'Success'
                else:
                    res = 'Success'
        return res

    def delete_groups(self, condition):
        ''' 删除团体 '''
        res = 'Fail'
        if 'type' in condition:
            if condition['type'] == 'member':
                return self.delete_groups_member(condition)

        for i in condition:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.groups.value, {'id': i}, 'PT_Groups')
        if bill_id:
            res = 'Success'
        return res

    def delete_groups_member(self, condition):
        ''' 删除团体成员 '''
        res = 'Fail'
        if 'type' not in condition or 'id' not in condition:
            return res

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_Groups_Member')
        if bill_id:
            res = 'Success'
        return res

    def update_business_income(self, data):
        '''新增运营收入'''
        res = 'Fail'
        data['year'] = as_date(data['year'])
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.ratingPlan.value, data, 'PT_XFY_Business_Income')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.ratingPlan.value, data, 'PT_XFY_Business_Income')
            if bill_id:
                res = 'Success'
        return res

    def get_business_income(self, condition, page, count):
        '''查询运营收入'''
        res = 'Fail'

        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id']))\
            .project({'_id': 0})

        res = self.page_query(_filter, 'PT_XFY_Business_Income', page, count)

        return res

    def delete_business_income_byid(self, condition):
        ''' 删除运营收入 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_XFY_Business_Income')
        if bill_id:
            res = 'Success'
        return res

    def update_service_situation(self, data):
        '''新增个案服务情况'''
        res = 'Fail'

        json = {
            "status": res
        }

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.serviceSituation.value, data, 'PT_Service_Situation')
            if bill_id:
                json['status'] = 'Success'
        else:
            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.serviceSituation.value, data, 'PT_Service_Situation')
            if bill_id:
                json['status'] = 'Success'
                json['new_id'] = data_info['id']
        return json

    def padStart0(self, numberString):
        return str('0' + str(numberString)) if numberString < 10 else str(numberString)

    def get_service_situation_list(self, org_list, condition, page, count):
        '''查询个案服务情况'''
        res = 'Fail'

        keys = ['id', 'name', 'id_card', 'sex']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('id') == values['id'])
            & (C('sex') == values['sex'])
            & (C('id_card').like(values['id_card'])))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'user')\
            .match_bill(
                (C('user.name').like(values['name'])))\
            .add_fields({
                "name": self.ao.array_elemat("$user.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({'_id': 0, 'user': 0, **get_common_project()})

        res = self.page_query(_filter, 'PT_Service_Situation', page, count)

        # 处理时间和补全志愿者信息
        if len(res['result']) > 0 and 'id' in condition:
            # 找出所有志愿者信息
            temp_volunteer = {}
            _filter_volunteer = MongoBillFilter()
            _filter_volunteer.match_bill().project(
                {'_id': 0, 'id': 1, 'name': 1})
            res_volunteer = self.query(_filter_volunteer, 'PT_Groups_Member')
            for item in res_volunteer:
                temp_volunteer[item['id']] = item

            for item in res['result']:

                if 'situation_summary' in item and len(item['situation_summary']) > 0:
                    for itm in item['situation_summary']:
                        if 'day' in itm:
                            # 年份
                            year = str(itm['day'].year)
                            # 月份
                            month = self.padStart0(itm['day'].month)
                            # 日期
                            day = self.padStart0(itm['day'].day)
                            # 处理时间
                            if 'start' in itm:
                                # 开始的时
                                hour = self.padStart0(itm['start'].hour)
                                # 开始的分
                                minute = self.padStart0(itm['start'].minute)
                                # 开始的秒
                                second = self.padStart0(itm['start'].second)
                                itm['start_date'] = year + '-' + month + '-' + \
                                    day + ' ' + hour + ':' + minute + ':' + second
                            if 'end' in itm:
                                # 开始的时
                                hour = self.padStart0(itm['end'].hour)
                                # 开始的分
                                minute = self.padStart0(itm['end'].minute)
                                # 开始的秒
                                second = self.padStart0(itm['end'].second)
                                itm['end_date'] = year + '-' + month + '-' + \
                                    day + ' ' + hour + ':' + minute + ':' + second

                        # 补全志愿者信息
                        if 'volunteer' in itm and type(itm['volunteer']) == list:
                            itm['volunteer_list'] = []
                            for it in itm['volunteer']:
                                if it in temp_volunteer:
                                    itm['volunteer_list'].append(
                                        temp_volunteer[it]['name'])

        return res

    def delete_service_situation_byid(self, condition):
        ''' 删除个案服务情况 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_Service_Situation')
        if bill_id:
            res = 'Success'
        return res

    def update_situation_summary(self, data):
        '''新增情况总结'''
        res = 'Fail'

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == data['id']))\
            .project({'_id': 0})
        res = self.page_query(_filter, data['table_name'], 1, 1)
        if len(res['result']) > 0:
            data_info = res['result'][0]
            data['situation_summary']['modify_date'] = as_date(
                data['situation_summary']['modify_date'])
            data['situation_summary']['day'] = as_date(
                data['situation_summary']['day'])
            data['situation_summary']['start'] = as_date(
                data['situation_summary']['start'])
            data['situation_summary']['end'] = as_date(
                data['situation_summary']['end'])
            state = True
            for index in range(len(data_info['situation_summary'])):
                if data_info['situation_summary'][index]['title'] == data['situation_summary']['title']:
                    data_info['situation_summary'][index] = data['situation_summary']
                    state = False
            if state:
                data_info['situation_summary'].append(
                    data['situation_summary'])
            if data['table_name'] == 'PT_Visit_Situation':
                typeid = TypeId.visitSituation.value
            else:
                typeid = TypeId.serviceSituation.value
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, typeid, data_info, data['table_name'])
            if bill_id:
                res = 'Success'
        return res

    def delete_elder_info_byid(self, condition):
        ''' 删除长者 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_User')
        if bill_id:
            res = 'Success'
        return res

    def update_visit_situation(self, data):
        '''新增探访服务情况'''
        res = 'Fail'

        json = {
            "status": res
        }

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.visitSituation.value, data, 'PT_Visit_Situation')
            if bill_id:
                json['status'] = 'Success'
        else:
            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.visitSituation.value, data_info, 'PT_Visit_Situation')
            if bill_id:
                json['status'] = 'Success'
                json['new_id'] = data_info['id']
        return json

    def get_visit_situation_list(self, org_list, condition, page, count):
        '''查询探访服务情况'''
        res = 'Fail'

        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"

        keys = ['id', 'id_card', 'sex', 'name', 'begin_date', 'end_date']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('id') == values['id'])
            & (C('sex') == values['sex'])
            & (C('id_card').like(values['id_card']))
            & (C('create_date') >= as_date(values['begin_date']))
            & (C('create_date') <= as_date(values['end_date'])))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'user')\
            .match_bill(
                (C('user.name').like(values['name'])))\
            .add_fields({
                "name": self.ao.array_elemat("$user.name", 0),
            })\
            .sort({'modify_date': -1})\
            .project({'_id': 0, 'user': 0, **get_common_project()})

        res = self.page_query(_filter, 'PT_Visit_Situation', page, count)

        # 处理时间和补全志愿者信息
        if len(res['result']) > 0 and 'id' in condition:
            # 找出所有志愿者信息
            temp_volunteer = {}
            _filter_volunteer = MongoBillFilter()
            _filter_volunteer.match_bill().project(
                {'_id': 0, 'id': 1, 'name': 1})
            res_volunteer = self.query(_filter_volunteer, 'PT_Groups_Member')
            for item in res_volunteer:
                temp_volunteer[item['id']] = item

            for item in res['result']:

                if 'situation_summary' in item and len(item['situation_summary']) > 0:
                    for itm in item['situation_summary']:
                        if 'day' in itm:
                            # 年份
                            year = str(itm['day'].year)
                            # 月份
                            month = self.padStart0(itm['day'].month)
                            # 日期
                            day = self.padStart0(itm['day'].day)
                            # 处理时间
                            if 'start' in itm:
                                # 开始的时
                                hour = self.padStart0(itm['start'].hour)
                                # 开始的分
                                minute = self.padStart0(itm['start'].minute)
                                # 开始的秒
                                second = self.padStart0(itm['start'].second)
                                itm['start_date'] = year + '-' + month + '-' + \
                                    day + ' ' + hour + ':' + minute + ':' + second
                            if 'end' in itm:
                                # 开始的时
                                hour = self.padStart0(itm['end'].hour)
                                # 开始的分
                                minute = self.padStart0(itm['end'].minute)
                                # 开始的秒
                                second = self.padStart0(itm['end'].second)
                                itm['end_date'] = year + '-' + month + '-' + \
                                    day + ' ' + hour + ':' + minute + ':' + second

                        # 补全志愿者信息
                        if 'volunteer' in itm and type(itm['volunteer']) == list:
                            itm['volunteer_list'] = []
                            for it in itm['volunteer']:
                                if it in temp_volunteer:
                                    itm['volunteer_list'].append(
                                        temp_volunteer[it]['name'])

        return res

    def delete_visit_situation_byid(self, condition):
        ''' 删除探访服务情况 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_Visit_Situation')
        if bill_id:
            res = 'Success'
        return res

    # app用户管理
    def get_app_user_list(self, org_list, condition, page, count):

        keys = ['id', 'name', 'account', 'telephone']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('personnel_type') == '1')
            & (C('personnel_info.telephone') == values['telephone'])
            & (C('login_info.login_check.account_name').like(values['account']))
            & (C('name').like(values['name']))
            & (C('is_app') == True)
        )\
            .add_fields({
                "account": self.ao.array_elemat("$login_info.login_check.account_name", 0),
                "sex": "$personnel_info.sex",
                "telephone": "$personnel_info.telephone",
                "address": "$personnel_info.address",
                "card_number": "$personnel_info.card_number",
                "card_name": "$personnel_info.card_name",
            })\
            .sort({
                'modify_date': -1,
            })\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'account': 1,
                'sex': 1,
                'telephone': 1,
                'address': 1,
                'card_number': 1,
                'card_name': 1,
                'id_card': 1,
                'is_ban': 1,
                'create_date': 1,
            })

        res = self.page_query(_filter, 'PT_User', page, count)

        # 有id就查看账户余额，消费记录和入账记录
        if 'id' in condition and len(res['result']) > 0:

            # 总金额
            res['result'][0]['balance_all'] = 0
            # 机构账户余额
            res['result'][0]['balance_jg'] = 0
            # app储值账户余额
            res['result'][0]['balance_app'] = 0
            # 补贴账户余额
            res['result'][0]['balance_bt'] = 0
            # 真实账户余额
            res['result'][0]['balance_zs'] = 0
            # 慈善账户余额
            res['result'][0]['balance_cs'] = 0

            temp_account = []
            temp_account2 = {}

            # 补充帐号金额
            # 找出所有账户
            _filter_account = MongoBillFilter()
            _filter_account.match_bill(
                (C('user_id') == values['id'])
            )\
                .project({
                    '_id': 0, })
            res_account = self.query(_filter_account, 'PT_Financial_Account')

            if len(res_account) > 0:
                for item_account in res_account:

                    temp_account.append(item_account['id'])
                    temp_account2[item_account['id']] = item_account['name']

                    if type(item_account['balance']) != float:
                        item_account['balance'] = float(
                            item_account['balance'])

                    res['result'][0]['balance_all'] += item_account['balance']

                    if item_account['account_type'] == '机构储值账户':
                        res['result'][0]['balance_jg'] = item_account['balance']
                    elif item_account['account_type'] == '真实账户':
                        res['result'][0]['balance_zs'] = item_account['balance']
                    elif item_account['account_type'] == '补贴账户':
                        res['result'][0]['balance_bt'] = item_account['balance']
                    elif item_account['account_type'] == 'APP储值账户':
                        res['result'][0]['balance_app'] = item_account['balance']
                    elif item_account['account_type'] == '慈善账户':
                        res['result'][0]['balance_cs'] = item_account['balance']

            # 根据账户id找出所有记录
            _filter_account_record = MongoBillFilter()
            _filter_account_record.match_bill(
                (C('account_id').inner(temp_account))
            )\
                .project({
                    '_id': 0, })
            res_account_record = self.query(
                _filter_account_record, 'PT_Financial_Account_Record')

            res['result'][0]['ruzhang_list'] = []
            res['result'][0]['xiaofei_list'] = []

            if len(res_account_record) > 0:
                for item_ar in res_account_record:
                    if item_ar['account_id'] in temp_account2:
                        item_ar['account_type'] = temp_account2[item_ar['account_id']]
                    if item_ar['amount'] < 0:
                        # 小于0代表是消费
                        res['result'][0]['xiaofei_list'].append(item_ar)
                    elif item_ar['amount'] > 0:
                        # 大于0代表是入账
                        res['result'][0]['ruzhang_list'].append(item_ar)

        return res

    # 编辑app用户
    def change_app_user(self, condition):
        if self.check_permission2({'f': 'APP用户管理', 'p': '编辑'}) == False:
            return '无权限'

        if 'id' in condition:
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.user.value, condition, 'PT_User')
            if bill_id:
                return '操作成功'
            else:
                return '操作失败'
        return '无操作'

    # 服务商基本信息
    def get_servicer_list(self, org_list, condition, page, count):

        keys = ['id', 'name', 'organization_nature']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('personnel_type') == '2')
            & (C('name').like(values['name']))
            & (C('organization_info.personnel_category') == '服务商')
        )\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'division_info')\
            .add_fields({
                "division_name": self.ao.array_elemat("$division_info.name", 0),
                "description": "$organization_info.description",
                "address": "$organization_info.address",
                "taxes_level": "$organization_info.taxes_level",
                "unified_social": "$organization_info.unified_social",
                "legal_person": "$organization_info.legal_person",
                "organization_nature": "$organization_info.organization_nature",
            })\
            .match_bill((C('organization_nature').like(values['organization_nature'])))\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'description': 1,
                'division_name': 1,
                'admin_area_id': 1,
                'reg_date': 1,
                'address': 1,
                'taxes_level': 1,
                'unified_social': 1,
                'legal_person': 1,
                'organization_nature': 1,
            })

        res = self.page_query(_filter, 'PT_User', page, count)

        return res

    # 机构养老基本信息
    def get_organ_list(self, org_list, condition, page, count):

        keys = ['id', 'name', 'organization_nature',
                'star_level', 'admin_area_id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('personnel_type') == '2')
            & (C('organization_info.star_level') == values['star_level'])
            & (C('admin_area_id') == values['admin_area_id'])
            & (C('name').like(values['name']))
            & (C('organization_info.personnel_category') == '福利院')
        )\
            .lookup_bill('PT_Bed', 'id', 'organization_id', 'bed')\
            .add_fields({
                "bed_count": self.ao.size('$bed'),
                "address": "$organization_info.address",
                "telephone": "$organization_info.telephone",
                "unified_social": "$organization_info.unified_social",
                "build_area": "$organization_info.build_area",
                "star_level": "$organization_info.star_level",
                "legal_person": "$organization_info.legal_person",
                "organization_nature": "$organization_info.organization_nature",
                "organization_category": "$organization_info.personnel_category",
                "configured_medical": "$organization_info.configured_medical",
                "position_area": "$organization_info.position_area",
                "build_area": "$organization_info.build_area",
                "cooperative_medical": "$organization_info.cooperative_medical",
                "older_org_permission_no": "$organization_info.older_org_permission_no",
                "land_deed_no": "$organization_info.land_deed_no",
                "property_no": "$organization_info.property_no",
                "health_license_no": "$organization_info.health_license_no",
                "fire_safety_permit_no": "$organization_info.fire_safety_permit_no",
            })\
            .match_bill((C('organization_nature').like(values['organization_nature'])))\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'telephone': 1,
                'admin_area_id': 1,
                'address': 1,
                'bed_count': 1,
                'unified_social': 1,
                'star_level': 1,
                'legal_person': 1,
                'organization_nature': 1,
                'organization_category': 1,
                'configured_medical': 1,
                'position_area': 1,
                'build_area': 1,
                'cooperative_medical': 1,
                'older_org_permission_no': 1,
                'land_deed_no': 1,
                'property_no': 1,
                'health_license_no': 1,
                'fire_safety_permit_no': 1,
            })

        res = self.page_query(_filter, 'PT_User', page, count)

        return res

    def get_happiness_operation_list(self, org_list, condition, page, count):
        '''查询幸福院运营情况'''
        res = 'Fail'

        keys = ['id', 'star_level', 'name', 'division_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('personnel_type') == '2')
            & (C('name').like(values['name']))
            & (C('organization_info.personnel_category') == '幸福院')
        )\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'division_info')\
            .add_fields({
                "division_name": self.ao.array_elemat("$division_info.name", 0),
                "star_level": "$organization_info.star_level",
                "book_num": "$organization_info.book_num",
                "service_organization": "$organization_info.service_organization",
                "open_days": "$organization_info.open_days",
            })\
            .match_bill((C('division_name').like(values['division_name'])) & (C('star_level') == values['star_level']))\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'division_name': 1,
                'book_num': 1,
                'service_organization': 1,
                'open_days': 1,
                'admin_area_id': 1,
                'star_level': 1,
                'reg_date': 1,
            })

        res = self.page_query(_filter, 'PT_User', page, count)

        # 如果有就循环
        if 'result' in res and len(res['result']) > 0:
            # 初始化数据
            temp_activity = {}
            temp_elder = {}
            temp_worker = {}
            reward_year = {}
            reward_all = {}
            fund_year = {}
            fund_all = {}
            # 先循环一次，获取组织机构的区域id，方便过滤长者和活动
            temp_area_ids = []
            temp_org_ids = []
            for item_ids in res['result']:
                temp_area_ids.append(item_ids['admin_area_id'])
                temp_org_ids.append(item_ids['id'])

            # 获取所有长者
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者') & (C('admin_area_id').inner(temp_area_ids)))\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res_elder = self.query(_filter_elder, 'PT_User')

            # 整理长者的社区数量
            if len(res_elder) > 0:
                for itm_eld in res_elder:
                    if 'admin_area_id' in itm_eld:
                        if itm_eld['admin_area_id'] in temp_elder:
                            # 次数+1
                            temp_elder[itm_eld['admin_area_id']] += 1
                        else:
                            temp_elder[itm_eld['admin_area_id']] = 1

            # 获取社工人员
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员') & (C('personnel_info.post') == '社工'))\
                .lookup_bill('PT_Set_Role', 'id', 'principal_account_id', 'setRole')\
                .add_fields({'roaid': self.ao.array_elemat("$setRole.role_of_account_id", 0)})\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'setRole': 0})
            res_worker = self.query(_filter_worker, 'PT_User')

            # 整理社工的幸福院数量
            if len(res_worker) > 0:
                for itm_work in res_worker:
                    if 'roaid' in itm_work:
                        if itm_work['roaid'] in temp_worker:
                            # 次数+1
                            temp_worker[itm_work['roaid']] += 1
                        else:
                            temp_worker[itm_work['roaid']] = 1
                    else:
                        if itm_work['organization_id'] in temp_worker:
                            # 次数+1
                            temp_worker[itm_work['organization_id']] += 1
                        else:
                            temp_worker[itm_work['organization_id']] = 1

            # 当前年
            current_year = get_cur_time().year

            current_year_begin = datetime.datetime.strptime(
                str(current_year) + '-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
            current_year_end = datetime.datetime.strptime(
                str(current_year) + '-12-31 23:59:59', '%Y-%m-%d %H:%M:%S')

            # 获取所有活动
            _filter_activity = MongoBillFilter()
            _filter_activity.match_bill((C('begin_date') > current_year_begin) & (C('end_date') < current_year_end) & (C('organization_id').inner(temp_org_ids)))\
                .project({'_id': 0})
            res_activity = self.query(_filter_activity, 'PT_Activity')

            # 整理幸福院的活动数量
            if len(res_activity) > 0:
                for itm_act in res_activity:
                    if 'organization_id' in itm_act:
                        if itm_act['organization_id'] in temp_activity:
                            # 次数+1
                            temp_activity[itm_act['organization_id']] += 1
                        else:
                            temp_activity[itm_act['organization_id']] = 1

            # 获取所有奖励
            _filter_income = MongoBillFilter()
            _filter_income.match_bill((C('organization_id').inner(temp_org_ids)))\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res_income = self.query(_filter_income, 'PT_XFY_Business_Income')

            # 整理奖励
            if len(res_income) > 0:
                for itm_inc in res_income:
                    # 转类型
                    if type(itm_inc['year_all_amout']) != float:
                        year_all_amout = float(itm_inc['year_all_amout'])
                    else:
                        year_all_amout = itm_inc['year_all_amout']
                    if type(itm_inc['reward_amout']) != float:
                        reward_amout = float(itm_inc['reward_amout'])
                    else:
                        reward_amout = itm_inc['reward_amout']

                    if itm_inc['year'] > current_year_begin and itm_inc['year'] < current_year_end:
                        # 当年资助
                        fund_year[itm_inc['organization_id']] = year_all_amout
                        # 当年奖励
                        reward_year[itm_inc['organization_id']] = reward_amout

                    if 'organization_id' in itm_inc:
                        if itm_inc['organization_id'] in fund_all:
                            fund_all[itm_inc['organization_id']
                                     ] += year_all_amout
                        else:
                            fund_all[itm_inc['organization_id']
                                     ] = year_all_amout

                        if itm_inc['organization_id'] in reward_all:
                            reward_all[itm_inc['organization_id']
                                       ] += reward_amout
                        else:
                            reward_all[itm_inc['organization_id']
                                       ] = reward_amout

            # 循环补充数据
            for item in res['result']:
                # 补充社区长者总数
                if item['admin_area_id'] in temp_elder:
                    item['elder_num'] = temp_elder[item['admin_area_id']]
                else:
                    item['elder_num'] = 0

                # 补充幸福院社工总数
                if item['id'] in temp_worker:
                    item['social_worker_num'] = temp_worker[item['id']]
                else:
                    item['social_worker_num'] = 0

                # 补充活动总数
                if item['id'] in temp_activity:
                    item['current_activity_num'] = temp_activity[item['id']]
                else:
                    item['current_activity_num'] = 0

                # 补充当年获得建设资助
                if item['id'] in fund_year:
                    item['current_year_fund'] = fund_year[item['id']]
                else:
                    item['current_year_fund'] = 0
                # 建设资助累计（万元）
                if item['id'] in fund_all:
                    item['all_fund'] = fund_all[item['id']]
                else:
                    item['all_fund'] = 0
                # 获得评比奖励（万元）
                if item['id'] in reward_year:
                    item['rate_reward'] = reward_year[item['id']]
                else:
                    item['rate_reward'] = 0
                # 评比奖励累计（万元）
                if item['id'] in reward_all:
                    item['all_rate_reward'] = reward_all[item['id']]
                else:
                    item['all_rate_reward'] = 0

        return res

    def get_area_operation_list(self, org_list, condition, page, count):
        '''查询社区运营情况'''
        res = 'Fail'

        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like('社区'))
            & (C('name').like(values['name'])))\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
            })

        res = self.page_query(
            _filter, 'PT_Administration_Division', page, count)

        # 如果有就循环
        if 'result' in res and len(res['result']) > 0:
            # 初始化数据
            fund_year = {}
            fund_all = {}
            temp_xfy = {}
            temp_xfy_id = {}
            temp_org = []
            temp_org_area = {}
            temp_xfycg = {}
            temp_xfycg_data = {}
            # 先循环一次，获取组织机构的区域id，方便过滤长者
            temp_area_ids = []
            for item_ids in res['result']:
                temp_area_ids.append(item_ids['id'])

            # 当前年
            current_year = get_cur_time().year

            current_year_begin = datetime.datetime.strptime(
                str(current_year) + '-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
            current_year_end = datetime.datetime.strptime(
                str(current_year) + '-12-31 23:59:59', '%Y-%m-%d %H:%M:%S')

            # 获取所有奖励
            _filter_income = MongoBillFilter()
            _filter_income.lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
                .add_fields({"admin_area_id": self.ao.array_elemat("$org_info.admin_area_id", 0)})\
                .match_bill((C('admin_area_id').inner(temp_area_ids)))\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res_income = self.query(_filter_income, 'PT_XFY_Business_Income')

            # 整理奖励的社区数量
            if len(res_income) > 0:
                for itm_inc in res_income:
                    if 'admin_area_id' in itm_inc:
                        # 转类型
                        if type(itm_inc['year_all_amout']) != float:
                            year_all_amout = float(itm_inc['year_all_amout'])
                        else:
                            year_all_amout = itm_inc['year_all_amout']

                        if itm_inc['year'] > current_year_begin and itm_inc['year'] < current_year_end:
                            # 资助
                            fund_year[itm_inc['admin_area_id']
                                      ] = year_all_amout

                        if 'admin_area_id' in itm_inc:
                            if itm_inc['admin_area_id'] in fund_all:
                                fund_all[itm_inc['admin_area_id']
                                         ] += year_all_amout
                            else:
                                fund_all[itm_inc['admin_area_id']
                                         ] = year_all_amout

            # 获取所有3，4，5星幸福院
            _filter_xfy = MongoBillFilter()
            _filter_xfy.match_bill((C('admin_area_id').inner(temp_area_ids)) & (C('organization_info.personnel_category') == '幸福院') & (C('organization_info.star_level').inner(['3', '4', '5'])))\
                .add_fields({
                    "star_level": "$organization_info.star_level",
                })\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res_xfy = self.query(_filter_xfy, 'PT_User')

            if len(res_xfy) > 0:
                for itm_xfy in res_xfy:
                    # 把幸福院的组织id存起来，方便拿所有超管
                    temp_org.append(itm_xfy['id'])
                    # 把组织机构id和行政区划id绑定起来
                    temp_org_area[itm_xfy['id']] = itm_xfy['admin_area_id']

                    if itm_xfy['admin_area_id'] not in itm_xfy:
                        temp_xfy_id[itm_xfy['admin_area_id']] = []
                        temp_xfy[itm_xfy['admin_area_id']] = {
                            'star_3_num': 0,
                            'star_4_num': 0,
                            'star_5_num': 0,
                            'apply_num_year': 0,
                            'apply_num_all': 0,
                            'acceptance_num_year': 0,
                            'acceptance_num_all': 0,
                            'try_run_num': 0,
                        }

                    temp_xfy_id[itm_xfy['admin_area_id']].append(itm_xfy)

                    if itm_xfy['star_level'] == '3':
                        temp_xfy[itm_xfy['admin_area_id']]['star_3_num'] += 1
                    elif itm_xfy['star_level'] == '4':
                        temp_xfy[itm_xfy['admin_area_id']]['star_4_num'] += 1
                    elif itm_xfy['star_level'] == '5':
                        temp_xfy[itm_xfy['admin_area_id']]['star_5_num'] += 1

            # 找出所有幸福院超管
            _filter_xfycg = MongoBillFilter()
            _filter_xfycg.match_bill((C('role_id') == '8f4cbe36-e355-11e9-84e4-a0a4c57e9ebe') & (C('role_of_account_id').inner(temp_org)))\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res_xfycg = self.query(_filter_xfycg, 'PT_Set_Role')

            if len(res_xfycg) > 0:
                for itm_xfycg in res_xfycg:
                    key = temp_org_area[itm_xfycg['role_of_account_id']]
                    if key not in temp_xfycg_data:
                        temp_xfycg_data[key] = []
                    temp_xfycg_data[key].append(itm_xfycg)

                    if key not in temp_xfycg:
                        temp_xfycg[key] = 1
                    else:
                        temp_xfycg[key] += 1

            # 循环补充数据
            for item in res['result']:
                # 初始化数据
                item['build_fund_year'] = 0
                item['build_fund_all'] = 0
                item['star_3_num'] = 0
                item['star_4_num'] = 0
                item['star_5_num'] = 0
                item['try_run_num'] = 0
                item['apply_num_all'] = 0
                item['apply_num_year'] = 0
                item['acceptance_num_year'] = 0
                item['acceptance_num_all'] = 0

                # 补充当年获得建设资助
                if item['id'] in fund_year:
                    item['build_fund_year'] = fund_year[item['id']]
                # 建设资助累计（万元）
                if item['id'] in fund_all:
                    item['build_fund_all'] = fund_all[item['id']]
                # 社区幸福院数量
                if item['id'] in temp_xfy:
                    item['star_3_num'] = temp_xfy[item['id']]['star_3_num']
                    item['star_4_num'] = temp_xfy[item['id']]['star_4_num']
                    item['star_5_num'] = temp_xfy[item['id']]['star_5_num']
                # 试运行数量，平台运营商添加了一个新的社区幸福院的超管账号（有超管帐号）
                if item['id'] in temp_xfycg:
                    item['try_run_num'] = temp_xfycg[item['id']]

                now_timestamp = time.mktime(
                    datetime.datetime.now().timetuple())
                # 申请数就以是新开社区幸福院的超管账号的时间度量
                if item['id'] in temp_xfycg:
                    # 总申请数就是总数量
                    item['apply_num_all'] = len(temp_xfycg_data[item['id']])
                    # 当前时间
                    for itm_cg in temp_xfycg_data[item['id']]:
                        # 幸福院创建时间
                        # create_timestamp = time.mktime(itm_cg.get('create_date').timetuple())
                        # if now_timestamp - create_timestamp < (86400 * 30):
                        if itm_cg.get('create_date').year == datetime.datetime.now().year:
                            item['apply_num_year'] += 1

                if item['id'] in temp_xfy_id:
                    for itm_xfy_id in temp_xfy_id[item['id']]:
                        # 验收数
                        if 'organization_info' in itm_xfy_id and 'check_date' in itm_xfy_id['organization_info']:
                            # 当年验收数
                            check_timestamp = time.mktime(
                                itm_xfy_id['organization_info']['check_date'].timetuple())
                            # 超过一个小时才是已验收
                            if now_timestamp - check_timestamp > 86400:
                                item['acceptance_num_all'] += 1
                                if itm_xfy_id['organization_info']['check_date'].year == datetime.datetime.now().year:
                                    item['acceptance_num_year'] += 1

        return res

    def update_xfy_org_info(self, data):
        '''新增幸福院运营机构信息'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.ratingPlan.value, data, 'PT_Xfy_Org_Info')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.ratingPlan.value, data, 'PT_Xfy_Org_Info')
            if bill_id:
                res = 'Success'

        return res

    def get_xfy_org_info(self, condition, page, count):
        '''查询幸福院运营机构信息'''
        res = 'Fail'

        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id']))\
            .project({'_id': 0})

        res = self.page_query(_filter, 'PT_Xfy_Org_Info', page, count)
        return res

    def delete_xfy_org_info(self, condition):
        ''' 删除幸福院运营机构信息 '''
        res = 'Fail'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': condition['id']}, 'PT_Xfy_Org_Info')
        if bill_id:
            res = 'Success'
        return res

    def get_all_tj_info(self, org_list, condition, page, count):
        '''总体统计'''
        res = 'Fail'

        keys = ['id']
        values = self.get_value(condition, keys)

        yljg_list = self.get_org_list('福利院')
        xfy_list = self.get_org_list('幸福院')
        fws_list = self.get_org_list('服务商')
        elder_list = self.get_all_elder()
        # return org_list_new

        _filter = MongoBillFilter()
        _filter.match_bill((C('type') == '镇')
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Administration_Division', page, count)
        for index in range(len(res['result'])):
            res['result'][index]['yljg_list'] = []
            res['result'][index]['xfy_list'] = []
            res['result'][index]['fws_list'] = []
            res['result'][index]['bed_num'] = 0
            res['result'][index]['checkIn_num'] = 0
            res['result'][index]['all_order_num'] = 0
            res['result'][index]['elder_num'] = 0
            res['result'][index]['act_num'] = 0
            res['result'][index]['signIn_elder_num'] = 0
            res['result'][index]['xfy_service_elder_num'] = 0
            res['result'][index]['bd_elder_num'] = 0
            for index2 in range(len(yljg_list)):
                if yljg_list[index2]['parent_id'] == res['result'][index]['id']:
                    res['result'][index]['yljg_list'].append(yljg_list[index2])
                    res['result'][index]['bed_num'] = res['result'][index]['bed_num'] + \
                        yljg_list[index2]['bed_num']
                    res['result'][index]['checkIn_num'] = res['result'][index]['checkIn_num'] + \
                        yljg_list[index2]['checkIn_num']
            for index2 in range(len(xfy_list)):
                if xfy_list[index2]['parent_id'] == res['result'][index]['id']:
                    res['result'][index]['xfy_list'].append(xfy_list[index2])
                    res['result'][index]['act_num'] = res['result'][index]['act_num'] + \
                        xfy_list[index2]['act_num']
                    res['result'][index]['signIn_elder_num'] = res['result'][index]['signIn_elder_num'] + \
                        xfy_list[index2]['signIn_elder_num']
                    res['result'][index]['xfy_service_elder_num'] = res['result'][index]['xfy_service_elder_num'] + \
                        xfy_list[index2]['xfy_service_elder_num']
            for index2 in range(len(fws_list)):
                if fws_list[index2]['parent_id'] == res['result'][index]['id']:
                    res['result'][index]['fws_list'].append(fws_list[index2])
                    res['result'][index]['all_order_num'] = res['result'][index]['all_order_num'] + \
                        fws_list[index2]['all_order_num']
                    res['result'][index]['elder_num'] = res['result'][index]['elder_num'] + \
                        fws_list[index2]['elder_num']
            for index2 in range(len(elder_list)):
                if elder_list[index2]['admin_area_id'] == res['result'][index]['id']:
                    res['result'][index]['bd_elder_num'] = res['result'][index]['bd_elder_num'] + 1
            res['result'][index] = {
                'name': res['result'][index]['name'],
                'bed_num': res['result'][index]['bed_num'],
                'yljg_num': len(res['result'][index]['yljg_list']),
                'checkIn_num': res['result'][index]['checkIn_num'],
                'xfy_num': len(res['result'][index]['xfy_list']),
                'xfy_service_elder_num': res['result'][index]['xfy_service_elder_num'],
                'act_num': res['result'][index]['act_num'],
                'signIn_elder_num': res['result'][index]['signIn_elder_num'],
                'fws_num': len(res['result'][index]['fws_list']),
                'elder_num': res['result'][index]['elder_num'],
                'all_order_num': res['result'][index]['all_order_num'],
                'bd_elder_num': res['result'][index]['bd_elder_num'],
            }
        return res

    def get_org_list(self, type_name):
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2') & (C('organization_info.personnel_category') == type_name))\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'area_info')\
            .add_fields({'type': '$area_info.type'})\
            .unwind('type')\
            .add_fields({'area_name': '$area_info.name'})\
            .add_fields({'parent_id': '$area_info.parent_id'})\
            .unwind('parent_id')
        if type_name == '福利院':
            _filter.lookup_bill('PT_Bed', 'id', 'organization_id', 'bed_list')\
                .add_fields({'bed_num': self.ao.size('$bed_list')})\
                .lookup_bill('IEC_Check_In', 'id', 'organization_id', 'check_in_list')\
                .add_fields({'checkIn_num': self.ao.size('$check_in_list')})\
                .project({'bed_list._id': 0, 'check_in_list._id': 0})
        if type_name == '幸福院':
            _filter.lookup_bill('PT_Activity', 'id', 'organization_id', 'act_list')\
                .add_fields({'act_num': self.ao.size('$act_list')})\
                .lookup_bill('PT_Activity_Sign_In', 'id', 'organization_id', 'signIn_list')\
                .unwind('signIn_list', True)\
                .add_fields({'user_id': '$signIn_list.user_id'})\
                .group(
                {
                    'id': '$id',
                    'act_num': '$act_num',
                    'admin_area_id': '$admin_area_id',
                    'area_name': '$area_name',
                    'parent_id': '$parent_id',
                    'type': '$type',
                }, [{
                    'elder_list': self.ao.add_to_set('user_id')
                }, ])\
                .add_fields({'signIn_elder_num': self.ao.size('$elder_list')})\
                .lookup_bill('PT_Service_Situation', 'id', 'organization_id', 'service_situation_list')\
                .unwind('service_situation_list', True)\
                .add_fields({'elder_id': '$service_situation_list.elder_id'})\
                .group(
                {
                    'id': '$id',
                    'act_num': '$act_num',
                    'signIn_elder_num': '$signIn_elder_num',
                    'admin_area_id': '$admin_area_id',
                    'area_name': '$area_name',
                    'parent_id': '$parent_id',
                    'type': '$type',
                }, [{
                    'elder_list': self.ao.add_to_set('elder_id')
                }, ])\
                .add_fields({'xfy_service_elder_num': self.ao.size('$elder_list')})\
                .project({'act_list._id': 0, 'signIn_list._id': 0, 'service_situation_list._id': 0})
        if type_name == '服务商':
            _filter.lookup_bill('PT_Service_Order', 'id', 'service_provider_id', 'order_list')\
                .project({'order_list._id': 0})\
                .add_fields({'all_order_num': self.ao.size('$order_list')})\
                .unwind('order_list', True)\
                .add_fields({'purchaser_id': '$order_list.purchaser_id'})\
                .group(
                {
                    'id': '$id',
                    'all_order_num': '$all_order_num',
                    'admin_area_id': '$admin_area_id',
                    'area_name': '$area_name',
                    'parent_id': '$parent_id',
                    'type': '$type',
                }, [{
                    'elder_list': self.ao.add_to_set('purchaser_id')
                }, ])\
                .add_fields({'elder_num': self.ao.size('$elder_list')})
        _filter.project({'_id': 0, 'area_info._id': 0})
        res = self.query(_filter, 'PT_User')
        org_list = res
        area_list = self.get_area_list()
        org_list_new = self.xuhuan_tj(org_list, area_list)
        return org_list_new

    def xuhuan_tj(self, org_list, area_list):
        for index in range(len(org_list)):
            if org_list[index]['type'] != '镇':
                for index2 in range(len(area_list)):
                    if org_list[index]['parent_id'] == area_list[index2]['id'] and area_list[index2]['type'] == '镇':
                        org_list[index]['type'] = '镇'
                        org_list[index]['area_name'] = area_list[index2]['name']
                    elif org_list[index]['parent_id'] == area_list[index2]['id'] and area_list[index2]['type'] != '镇':
                        org_list[index]['parent_id'] = area_list[index2]['parent_id']
                        org_list[index]['type'] = area_list[index2]['type']
                        org_list[index]['area_name'] = area_list[index2]['name']
                        self.xuhuan_tj(org_list, area_list)
        return org_list

    def get_elder_area(self, elder_list, area_list):
        for index in range(len(elder_list)):
            for index2 in range(len(area_list)):
                if elder_list[index]['admin_area_id'] == area_list[index2]['id'] and area_list[index2]['type'] == '镇':
                    elder_list[index]['name'] = area_list[index2]['name']
                elif elder_list[index]['admin_area_id'] == area_list[index2]['id'] and area_list[index2]['type'] != '镇':
                    elder_list[index]['admin_area_id'] = area_list[index2]['parent_id']
                    self.get_elder_area([elder_list[index]], area_list)
        return elder_list

    def get_all_elder(self):
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_info.personnel_category') == '长者') & (C('admin_area_id') != None))\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        area_list = self.get_area_list()
        elder_list_new = self.get_elder_area(res, area_list)
        return elder_list_new

    def get_area_list(self):
        _filter = MongoBillFilter()
        _filter.match_bill((C('type') != None)).project({'_id': 0})
        res = self.query(_filter, 'PT_Administration_Division')
        return res

    def yljg_tj(self, org_list, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)

        yljg_list = self.get_yljg_list()
        _filter = MongoBillFilter()
        _filter.match_bill((C('type') == '镇')
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Administration_Division', page, count)
        for index in range(len(res['result'])):
            res['result'][index]['yljg_list'] = []
            res['result'][index]['all_bed_num'] = 0
            res['result'][index]['all_work_num'] = 0
            res['result'][index]['gb_bed_num'] = 0
            res['result'][index]['gb_checkIn_num'] = 0
            res['result'][index]['gb_work_num'] = 0
            res['result'][index]['gbmy_bed_num'] = 0
            res['result'][index]['gbmy_checkIn_num'] = 0
            res['result'][index]['gbmy_work_num'] = 0
            res['result'][index]['mbfyl_bed_num'] = 0
            res['result'][index]['mbfyl_checkIn_num'] = 0
            res['result'][index]['mbfyl_work_num'] = 0
            res['result'][index]['mb_bed_num'] = 0
            res['result'][index]['mb_checkIn_num'] = 0
            res['result'][index]['mb_work_num'] = 0
            for index2 in range(len(yljg_list)):
                if yljg_list[index2]['admin_area_id'] == res['result'][index]['id']:
                    res['result'][index]['yljg_list'].append(yljg_list[index2])
                    res['result'][index]['all_bed_num'] = res['result'][index]['all_bed_num'] + \
                        yljg_list[index2]['bed_num']
                    res['result'][index]['all_work_num'] = res['result'][index]['all_work_num'] + \
                        yljg_list[index2]['work_num']
                    if yljg_list[index2]['organization_info']['organization_nature'] == '公办':
                        res['result'][index]['gb_bed_num'] = res['result'][index]['gb_bed_num'] + \
                            yljg_list[index2]['bed_num']
                        res['result'][index]['gb_checkIn_num'] = res['result'][index]['gb_checkIn_num'] + \
                            yljg_list[index2]['checkIn_num']
                        res['result'][index]['gb_work_num'] = res['result'][index]['gb_work_num'] + \
                            yljg_list[index2]['work_num']
                    if yljg_list[index2]['organization_info']['organization_nature'] == '公办民营':
                        res['result'][index]['gbmy_bed_num'] = res['result'][index]['gbmy_bed_num'] + \
                            yljg_list[index2]['bed_num']
                        res['result'][index]['gbmy_checkIn_num'] = res['result'][index]['gbmy_checkIn_num'] + \
                            yljg_list[index2]['checkIn_num']
                        res['result'][index]['gbmy_work_num'] = res['result'][index]['gbmy_work_num'] + \
                            yljg_list[index2]['work_num']
                    if yljg_list[index2]['organization_info']['organization_nature'] == '民办非营利':
                        res['result'][index]['mbfyl_bed_num'] = res['result'][index]['mbfyl_bed_num'] + \
                            yljg_list[index2]['bed_num']
                        res['result'][index]['mbfyl_checkIn_num'] = res['result'][index]['mbfyl_checkIn_num'] + \
                            yljg_list[index2]['checkIn_num']
                        res['result'][index]['mbfyl_work_num'] = res['result'][index]['mbfyl_work_num'] + \
                            yljg_list[index2]['work_num']
                    if yljg_list[index2]['organization_info']['organization_nature'] == '民办企业':
                        res['result'][index]['mb_bed_num'] = res['result'][index]['mb_bed_num'] + \
                            yljg_list[index2]['bed_num']
                        res['result'][index]['mb_checkIn_num'] = res['result'][index]['mb_checkIn_num'] + \
                            yljg_list[index2]['checkIn_num']
                        res['result'][index]['mb_work_num'] = res['result'][index]['mb_work_num'] + \
                            yljg_list[index2]['work_num']
            res['result'][index] = {
                'name': res['result'][index]['name'],
                'all_bed_num': res['result'][index]['all_bed_num'],
                'yljg_num': len(res['result'][index]['yljg_list']),
                'all_work_num': res['result'][index]['all_work_num'],
                'gb_bed_num': res['result'][index]['gb_bed_num'],
                'gb_checkIn_num': res['result'][index]['gb_checkIn_num'],
                'gb_work_num': res['result'][index]['gb_work_num'],
                'gbmy_bed_num': res['result'][index]['gbmy_bed_num'],
                'gbmy_checkIn_num': res['result'][index]['gbmy_checkIn_num'],
                'gbmy_work_num': res['result'][index]['gbmy_work_num'],
                'mbfyl_bed_num': res['result'][index]['mbfyl_bed_num'],
                'mbfyl_checkIn_num': res['result'][index]['mbfyl_checkIn_num'],
                'mbfyl_work_num': res['result'][index]['mbfyl_work_num'],
                'mb_bed_num': res['result'][index]['mb_bed_num'],
                'mb_checkIn_num': res['result'][index]['mb_checkIn_num'],
                'mb_work_num': res['result'][index]['mb_work_num'],
                'yljg_list': res['result'][index]['yljg_list']
            }
        return res

    def get_all_work(self):
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员'))\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        return res

    def get_yljg_list(self):
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2') & (C('organization_info.personnel_category') == '福利院'))\
            .lookup_bill('PT_Bed', 'id', 'organization_id', 'bed_list')\
            .add_fields({'bed_num': self.ao.size('$bed_list')})\
            .lookup_bill('IEC_Check_In', 'id', 'organization_id', 'check_in_list')\
            .add_fields({'checkIn_num': self.ao.size('$check_in_list')})\
            .project({'_id': 0, 'area_info._id': 0, 'bed_list._id': 0, 'check_in_list._id': 0})
        res = self.query(_filter, 'PT_User')
        area_list = self.get_area_list()
        new_org_list = self.get_elder_area(res, area_list)
        work_list = self.get_all_work()
        # 得到工作人员数
        for index in range(len(new_org_list)):
            new_org_list[index]['work_num'] = 0
            for index2 in range(len(work_list)):
                if new_org_list[index]['id'] == work_list[index2]['organization_id']:
                    new_org_list[index]['work_num'] = new_org_list[index]['work_num'] + 1
        # # 得到当前机构的入住长者中的自费数
        # _filter2 = MongoBillFilter()
        # _filter2.match_bill((C('personnel_info.personnel_category') == '长者') & (C('admin_area_id') != None))\
        #     .project({'_id': 0})
        # elder_list = self.query(_filter2, 'PT_User')
        # for index in range(len(new_org_list)):
        #     for index2 in range(len(new_org_list[index]['check_in_list'])):
        #         for index3 in range(len(elder_list)):
        #             if elder_list[index3]['id'] == new_org_list[index]['check_in_list'][index2]['user_id':
        #                 if elder_list[index3]['personnel_info']['personnel_classification']
        return new_org_list

    def get_fws_list(self):
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2') & (C('organization_info.personnel_category') == '服务商'))\
            .lookup_bill('PT_Service_Order', 'id', 'service_provider_id', 'order_list')\
            .project({'order_list._id': 0})\
            .add_fields({'all_order_num': self.ao.size('$order_list')})\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        area_list = self.get_area_list()
        new_org_list = self.get_elder_area(res, area_list)
        work_list = self.get_all_work()
        # 得到工作人员数
        for index in range(len(new_org_list)):
            new_org_list[index]['work_num'] = 0
            for index2 in range(len(work_list)):
                if new_org_list[index]['id'] == work_list[index2]['organization_id']:
                    new_org_list[index]['work_num'] = new_org_list[index]['work_num'] + 1
        return new_org_list

    def jjfws_tj(self, org_list, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)

        fws_list = self.get_fws_list()
        _filter = MongoBillFilter()
        _filter.match_bill((C('type') == '镇')
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Administration_Division', page, count)
        for index in range(len(res['result'])):
            for index2 in range(len(fws_list)):
                res['result'][index]['fws_list'] = []
                res['result'][index]['all_order_num'] = 0
                res['result'][index]['all_elder_num'] = 0
                res['result'][index]['total_amout'] = 0
                res['result'][index]['all_worker_num'] = 0
                res['result'][index]['total_product_num'] = 0
                if fws_list[index2]['admin_area_id'] == res['result'][index]['id']:
                    res['result'][index]['fws_list'].append(fws_list[index2])
                    res['result'][index]['all_order_num'] = res['result'][index]['all_order_num'] + \
                        fws_list[index2]['all_order_num']
                    res['result'][index]['all_worker_num'] = res['result'][index]['all_worker_num'] + \
                        fws_list[index2]['work_num']
        return res

    def update_operation_situation(self, data):
        '''新增机构运营情况记录'''
        res = 'Fail'
        if 'id' in data.keys():
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.disease.value, data, 'PT_Org_Opertion_Record')
            if bill_id:
                res = 'Success'
        else:

            # 新增判断重复录入问题
            keys = ['organization_id', 'year']
            values = self.get_value(data, keys)
            _filter_exists = MongoBillFilter()
            _filter_exists.match_bill((C('year') == data['year'])
                                      & (C('organization_id') == data['organization_id']))\
                .project({'_id': 0})
            res_exists = self.query(
                _filter_exists, 'PT_Org_Opertion_Record')

            if len(res_exists) > 0:
                return '该年份已录入'

            data['create_time'] = datetime.datetime.now()

            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.disease.value, data_info, 'PT_Org_Opertion_Record')
            if bill_id:
                res = 'Success'
        return res

    def get_operation_situation_list(self, condition, page=None, count=None):
        '''查询机构运营情况记录列表'''
        keys = ['id', 'year', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('year').like(values['year'])))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .add_fields({
                'org_name': '$org_info.name'
            })\
            .match_bill((C('org_name').like(values['org_name'])))\
            .project({'_id': 0, 'org_info': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_Org_Opertion_Record', page, count)
        return res

    def del_operation_situation(self, ids):
        ''' 删除单条机构运营情况记录 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.disease.value, {'id': i}, 'PT_Org_Opertion_Record')
        if bill_id:
            res = 'Success'
        return res

    def get_elder_meals_list(self, org_list, condition, page, count):
        # 长者膳食
        keys = ['id', 'key', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('key') == values['key']) & (C('type') == values['type']))\
            .project({'_id': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_Elder_Meals', page, count)
        return res

    def update_elder_meals(self, data):
        '''新增长者膳食'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.elderMeals.value, data, 'PT_Elder_Meals')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.elderMeals.value, data, 'PT_Elder_Meals')
            if bill_id:
                res = 'Success'
        return res

    def get_happiness_station_product_list(self, org_list, condition, page, count):
        '''幸福小站产品'''
        res = 'Fail'

        keys = ['id', 'name', 'code', 'week_key']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
            & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'code': 1,
                      'name': 1,
                      'description': 1,
                      'unit': 1,
                      'price': 1,
                      'picture': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(
            _filter, 'PT_Happiness_Station_Product', page, count)

        # 弃用的lookup
        # .lookup_bill('PT_Happiness_Station_Product_Sales', 'id', 'product_id', 'sales_info')\
        # .add_fields({'sales_info2': self.ao.array_filter("$sales_info", "si", ((F('$si.week_key') == values['week_key'])).f)})\
        # .add_fields({"last_week_sales": self.ao.array_elemat("$sales_info2.week_sales", 0)})\

        if 'result' in res and len(res['result']) > 0:
            for item in res['result']:
                _filter_sales = MongoBillFilter()
                _filter_sales.match_bill((C('product_id') == item['id']))\
                    .project({'_id': 0})\
                    .sort({'create_date': -1})\
                    .limit(10)\
                    .project({'_id': 0,
                              'week_key': 1,
                              'week_sales': 1,
                              })
                res_sales = self.query(
                    _filter_sales, 'PT_Happiness_Station_Product_Sales')
                item['sales_info'] = res_sales
                if len(res_sales) > 0:
                    # 补全最新的销量
                    item['last_week_sales'] = res_sales[0]['week_sales']

        return res

    def update_happiness_station_product(self, data):
        '''更新幸福小站产品'''
        res = 'Fail'
        temp = {}
        main_id = ''

        # 产品上周销量
        if 'week_key' in data:
            temp['week_key'] = data['week_key']
            temp['week_sales'] = data['week_sales']
            del(data['week_key'])
            del(data['week_sales'])

        if 'id' in list(data.keys()):
            main_id = data['id']
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.happinessStationProduct.value, data, 'PT_Happiness_Station_Product')
            if bill_id:
                res = 'Success'
        else:
            data['code'] = self.get_max_code('PT_Happiness_Station_Product')
            data_info = get_info(data, self.session)
            main_id = data_info['id']
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.happinessStationProduct.value, data, 'PT_Happiness_Station_Product')
            if bill_id:
                res = 'Success'

        if res == 'Success':
            if 'week_key' in temp:
                temp['product_id'] = main_id
                res = self.set_happiness_station_product_sales(temp)

        return res

    def get_max_code(self, table):
        _filter = MongoBillFilter()
        _filter.match_bill((C('code') != None))\
            .project({'_id': 0})
        res = self.query(_filter, table)
        maxNum = 0
        if len(res) > 0:
            for item in res:
                if type(item['code']) != int:
                    item['code'] = int(item['code'])
                if item['code'] > maxNum:
                    maxNum = item['code']
        return maxNum + 1

    def set_happiness_station_product_sales(self, data):
        if 'product_id' in data:
            # 有编辑ID，查询是否已经有上周销量
            _filter = MongoBillFilter()
            _filter.match_bill((C('week_key') == data['week_key']) & (C('product_id') == data['product_id']))\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Happiness_Station_Product_Sales')

            if len(res) > 0:
                data['id'] = res[0]['id']
                # 有就编辑
                bill_id = self.bill_manage_service.add_bill(
                    OperationType.update.value, TypeId.happinessStationProduct.value, data, 'PT_Happiness_Station_Product_Sales')
                return bill_id
        # 统一新增
        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.happinessStationProduct.value, data, 'PT_Happiness_Station_Product_Sales')
        return bill_id

    def delete_happiness_station_product(self, ids):
        ''' 删除幸福小站产品 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.happinessStationProduct.value, {'id': i}, 'PT_Happiness_Station_Product')
        if bill_id:
            res = 'Success'
        return res

    def get_happiness_year_self_assessment_list(self, org_list, condition, page=None, count=None):
        '''查询社区幸福院年度自评列表'''
        keys = ['id', 'year', 'organization_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('year') == values['year']) & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_info')\
            .lookup_bill('PT_User', 'creator_id', 'id', 'creator_info')\
            .add_fields({
                "organization_name": self.ao.array_elemat("$organization_info.name", 0),
                "organization_address": self.ao.array_elemat("$organization_info.organization_info.address", 0),
                "creator_name": self.ao.array_elemat("$creator_info.name", 0),
            })\
            .match_bill(C('organization_name').like(values['organization_name']))\
            .project({
                '_id': 0,
                'creator_info': 0,
                'organization_info': 0,
                **get_common_project()
            })
        res = self.page_query(
            _filter, 'PT_Happiness_Year_Self_Assessment', page, count)
        return res

    def update_happiness_year_self_assessment(self, data):
        '''新增社区幸福院年度自评'''
        if 'year' not in data:
            data['year'] = datetime.datetime.now().year

        _filter = MongoBillFilter()
        _filter.match_bill((C('year') == data['year']))\
            .project({'_id': 0, **get_common_project()})
        res_exists = self.query(_filter, 'PT_Happiness_Year_Self_Assessment')

        stype = ''

        if len(res_exists) > 0:
            stype = 'edit'
            data['id'] = res_exists[0]['id']
        else:
            stype = 'add'

        data['creator_id'] = get_current_user_id(self.session)

        if stype == 'edit':
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.yearSelfAssessment.value, data, 'PT_Happiness_Year_Self_Assessment')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.yearSelfAssessment.value, data, 'PT_Happiness_Year_Self_Assessment')
            if bill_id:
                res = 'Success'
        return res

    def delete_happiness_year_self_assessment(self, ids):
        ''' 删除社区幸福院年度自评 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.yearSelfAssessment.value, {'id': i}, 'PT_Happiness_Year_Self_Assessment')
        if bill_id:
            res = 'Success'
        return res

    def get_fire_equipment_list(self, org_list, condition, page, count):
        '''消防设施'''
        res = 'Fail'

        keys = ['id', 'name', 'introduction', 'org_name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('introduction').like(values['introduction']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .project({'org._id': 0})\
            .add_fields({'org_name': '$org.name'})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .sort({'modify_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'org_name': 1,
                      'introduction': 1,
                      'photo': 1,
                      'modify_date': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Fire_Equipment', page, count)

        return res

    def update_fire_equipment(self, data):
        '''新增编辑消防设施'''
        res = 'Fail'

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.fireEquipment.value, data, 'PT_Fire_Equipment')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.fireEquipment.value, data, 'PT_Fire_Equipment')
            if bill_id:
                res = 'Success'
        return res

    def delete_fire_equipment(self, ids):
        ''' 删除消防设施 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.fireEquipment.value, {'id': i}, 'PT_Fire_Equipment')
        if bill_id:
            res = 'Success'
        return res

    def get_organization_fly_list(self, org_list, condition, page, count):
        '''获取组织机构列表'''
        def convertDegreesToRadians(degrees):
            return degrees * math.pi / 180

        def coverGpsToBaiduLatLon(lat, lon):
            baidu_map_url = 'http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=%f&y=%f'

            data_req = requests.get((baidu_map_url % (float(lon), float(lat))))
            new_pos = data_req.json()
            if 'error' in new_pos.keys() and new_pos['error'] == 0:
                lon = float(base64.b64decode(
                    new_pos['x']).decode("utf-8"))
                lat = float(base64.b64decode(
                    new_pos['y']).decode("utf-8"))
                return lat, lon

        keys = ['id', 'name', 'personnel_type', 'lat',
                'lon', 'sortByDistance', 'is_show_app']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        if org_list != [None]:
            _filter.match((C('id').inner(org_list)))
        _filter.filter_objects
        _filter.match_bill((C('id') == values['id'])
                           & (C('personnel_type') == '2')
                           & (C('organization_info.is_show_app') != '下架')
                           & (C('name').like(values['name']))
                           & (C('organization_info.personnel_category') == '福利院'))\
               .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'follow_info')\
               .add_fields({'follow_count': self.ao.size('$follow_info'), })\
               .lookup_bill('PT_Bed', 'id', 'organization_id', 'bed')\
               .add_fields({'bed_count': self.ao.size('$bed'), })\
               .add_fields({'chenk_in_count': self.ao.size(self.ao.array_filter("$bed", "aa", ((F('$aa.residents_id') != None) & (F('$aa.residents_id') != '')).f))})\
               .add_fields({'organization_info.chenk_in_probability': self.ao.floor(self.ao.switch([self.ao.case(((F('bed_count') == 0)), 0)], (F('chenk_in_count').__truediv__('$bed_count') * (100)).f))})
        if 'lat' in condition and 'lon' in condition:
            lat = values['lat']
            lon = values['lon']
            # 纬度，经度
            lat_baidu, lon_baidu = coverGpsToBaiduLatLon(lat, lon)
            lat_d = convertDegreesToRadians(lat_baidu)
            _filter.filter_objects.append({
                '$addFields': {
                    # 距离是以km单位的
                    "distance": {
                        '$multiply': [
                            {
                                '$acos': {
                                    '$add': [
                                        {
                                            '$multiply': [
                                                {
                                                    '$sin': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$sin': lat_d
                                                }
                                            ]
                                        },
                                        {
                                            '$multiply': [
                                                {
                                                    '$cos': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$cos': lat_d
                                                },
                                                {
                                                    '$cos': {
                                                        '$degreesToRadians': {
                                                            '$subtract': [
                                                                {"$toDouble": "$organization_info.lon"},
                                                                lon_baidu
                                                            ]
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            # 地球平均半径
                            6371.004
                        ]
                    }
                }
            })
        else:
            _filter.add_fields({'distance': None})
        if 'sortByDistance' in condition:
            _filter.sort({'distance': 1})
        if 'sort' in condition and condition['sort'] != None:
            if condition['sort'] == '关注度':
                _filter.sort({'follow_count': -1})
            elif condition['sort'] == '入住率':
                _filter.sort({'organization_info.chenk_in_probability': -1})
            else:
                _filter.sort({'create_date': -1})
        else:
            _filter.sort({'modify_date': -1})
        if 'sortByDistance' in condition:
            _filter.sort({'distance': 1})
        _filter.project({'_id': 0, 'bed._id': 0, 'chenk_in': 0, 'service_product': 0, 'organization_infos._id': 0, 'user._id': 0,
                         'organization_activity': 0, 'organization_participate': 0, 'division_info': 0, 'follow_info': 0, 'order_info': 0})
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def get_organization_xfy_list(self, org_list, condition, page, count):
        '''获取幸福院列表'''
        def convertDegreesToRadians(degrees):
            return degrees * math.pi / 180

        def coverGpsToBaiduLatLon(lat, lon):
            baidu_map_url = 'http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=%f&y=%f'

            data_req = requests.get((baidu_map_url % (float(lon), float(lat))))
            new_pos = data_req.json()
            if 'error' in new_pos.keys() and new_pos['error'] == 0:
                lon = float(base64.b64decode(
                    new_pos['x']).decode("utf-8"))
                lat = float(base64.b64decode(
                    new_pos['y']).decode("utf-8"))
                return lat, lon

        keys = ['id', 'name', 'personnel_type', 'lat', 'lon', 'sortByDistance']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        if org_list != [None]:
            _filter.match((C('id').inner(org_list)))
        _filter.filter_objects
        _filter.match_bill((C('id') == values['id'])
                           & (C('personnel_type') == '2')
                           & (C('organization_info.is_show_app') != '下架')
                           & (C('name').like(values['name']))
                           & (C('organization_info.personnel_category') == '幸福院'))\
               .lookup_bill('PT_Activity', 'id', 'organization_id', 'organization_activity')\
               .add_fields({'activity_count': self.ao.size('$organization_activity'), })\
               .add_fields({"activity_id": self.ao.array_elemat("$organization_activity.id", 0)})\
               .lookup_bill('PT_Activity_Participate', 'activity_id', 'activity_id', 'organization_participate')\
               .add_fields({'participate_count': self.ao.size('$organization_participate'), })\
               .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'follow_info')\
               .add_fields({'follow_count': self.ao.size('$follow_info'), })
        user_id = get_user_id_or_false(self.session)
        if user_id != False:
            _filter.add_fields({'is_follow': self.ao.size(self.ao.array_filter(
                "$follow_info", "fi", ((F('$fi.user_id') == user_id) & (F('$fi.object') == '社区幸福院') & (F('$fi.type') == 'follow')).f))})
        if 'lat' in condition and 'lon' in condition:
            lat = values['lat']
            lon = values['lon']
            # 纬度，经度
            lat_baidu, lon_baidu = coverGpsToBaiduLatLon(lat, lon)
            lat_d = convertDegreesToRadians(lat_baidu)
            _filter.filter_objects.append({
                '$addFields': {
                    # 距离是以km单位的
                    "distance": {
                        '$multiply': [
                            {
                                '$acos': {
                                    '$add': [
                                        {
                                            '$multiply': [
                                                {
                                                    '$sin': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$sin': lat_d
                                                }
                                            ]
                                        },
                                        {
                                            '$multiply': [
                                                {
                                                    '$cos': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                },
                                                {
                                                    '$cos': lat_d
                                                },
                                                {
                                                    '$cos': {
                                                        '$degreesToRadians': {
                                                            '$subtract': [
                                                                {"$toDouble": "$organization_info.lon"},
                                                                lon_baidu
                                                            ]
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            # 地球平均半径
                            6371.004
                        ]
                    }
                }
            })
        else:
            _filter.add_fields({'distance': None})

        if 'sortByDistance' in condition:
            _filter.sort({'distance': 1})
        if 'sort' in condition and condition['sort'] != None:
            if condition['sort'] == '活动数':
                _filter.sort({'activity_count': -1})
            elif condition['sort'] == '报名数':
                _filter.sort({'participate_count': -1})
            elif condition['sort'] == '关注度':
                _filter.sort({'follow_count': -1})
            else:
                _filter.sort({'create_date': -1})
        else:
            _filter.sort({'modify_date': -1})
        _filter.project({
            '_id': 0,
            'organization_activity': 0,
            'organization_participate': 0,
            'division_info': 0,
            'follow_info': 0,
            **get_common_project()
        })
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def import_elder(self, data):
        res = '导入失败'
        pf = pd.DataFrame(data)
        pf.rename(columns={'长者姓名': 'name', '证件类型': 'id_card_type', '证件号码': 'id_card', '性别': 'sex', '出生日期': 'date_birth',
                           '电话': 'telephone', '在世状态': 'die_state', '婚姻状况': 'marriage_state', '民族': 'nation', '籍贯': 'native_place',
                           '区域编号': 'organization_code', '家庭住址': 'address', '身份证地址': 'id_card_address', '备注': 'remark', '照片': 'photo',
                           '银行卡号': 'card_number', '创建日期': 'create_date', '持卡人': 'card_name'
                           }, inplace=True)
        condition = dataframe_to_list(pf)

        out_per = ['name', 'id_card']
        in_per = ['name', 'sex', 'id_card', 'telephone', 'native_place', 'address',
                  'id_card_address', 'education', 'post', 'nursing_area', 'card_name', 'card_number', 'die_state', 'hobby', 'marriage_state', 'nation', 'remark']
        # pf = pf.drop(['age', 'replacement_amount', 'total_amount', 'title'], axis=1)
        # condition = dataframe_to_list(pf)

        # 校验导入数据必填字段是否正确填写
        indx = 1
        if len(condition) > 0:
            for i in range(len(condition)):
                indx = indx + 1
                if 'name' not in condition[i] or condition[i]['name'] == None or self.clearZFC(condition[i]['name']) == '' or 'id_card' not in condition[i] or condition[i]['id_card'] == None or self.clearZFC(condition[i]['id_card']) == '' or 'date_birth' not in condition[i] or condition[i]['date_birth'] == None or self.clearZFC(condition[i]['date_birth']) == '':
                    return '第'+str(indx)+'行的长者姓名、身份证、出生年月日都必须要填写'

        # # 组织机构的缓存数组
        # temp_organization = {}
        # # 区域编号的缓存数组
        # temp_area = {}
        # # 长者类型的缓存数组
        # tempLX = {}
        temp_elder = {}

        person_arr = []
        role_arr = []
        account_arr = []
        # 默认佛山市区域ID
        default_area_id = 'da1e4201-70c5-407a-a088-c3a8cc91fef5'
        # 默认游客的角色ID
        default_role_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'
        # 默认平台的组织ID
        default_organization_id = get_current_organization_id(self.session)

        # 查询当前登陆人的信息
        _filter_oper = MongoBillFilter()
        _filter_oper.match_bill((C('id') == get_current_user_id(self.session)))\
            .project({'_id': 0})
        res_oper = self.query(
            _filter_oper, 'PT_User')
        if len(res_oper) > 0:
            default_area_id = res_oper[0]['admin_area_id']
        # 查询全部长者的信息
        _filter_elder = MongoBillFilter()
        _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者'))\
            .project({'_id': 0})
        res_elder = self.query(
            _filter_elder, 'PT_User')

        if len(res_elder) > 0:
            for item in res_elder:
                if 'id_card' in item:
                    temp_elder[item['id_card']] = item

        # 失败长者的身份证号
        fail_elder = []
        ccc = 0
        if len(condition) > 0:
            for i in reversed(range(len(condition))):
                # 判断这个人是否存在
                if 'id_card' in condition[i] and condition[i]['id_card'] != None:
                    # 判断身份证中是否有‘号和'号，有的话去除
                    id_card = self.clearZFC(condition[i]['id_card']).replace(
                        "'", "").replace("’", "")
                    if id_card in temp_elder:
                        ccc = ccc + 1
                        fail_elder.append(id_card)
                        del condition[i]
        if len(fail_elder) > 0:
            res = '系统已有'+str(len(fail_elder))+'位长者，以下证件号码长者导入失败：' + \
                '['+','.join(fail_elder)+']'

        if len(condition) > 0:
            count = 0
            for item in condition:
                if 'id_card' in item and item['id_card'] != None:
                    id_card = self.clearZFC(item['id_card']).replace(
                        "'", "").replace("’", "")
                    if id_card in temp_elder:
                        continue
                    else:
                        item['id_card'] = id_card
                # 导入人员
                newData = {
                    "name": "",
                    "personnel_type": "1",
                    "admin_area_id": default_area_id,
                    "town": "",
                    "id_card_type": "身份证",
                    "id_card": "",
                    "organization_id": default_organization_id,
                    "reg_date": get_cur_time(),
                    "create_date": get_cur_time(),
                    "login_info": [],
                    "personnel_info": {
                        "name": "",
                        "id_card": "",
                        "id_card_address": "",
                        "date_birth": "",
                        "sex": "",
                        "telephone": "",
                        "marriage_state": "",
                        "remarks": "",
                        "native_place": "",
                        "personnel_category": "长者",
                        "address": "",
                        "role_id": default_role_id,
                        "die_state": "",
                        "hobby": "",
                        "personnel_classification": "",
                        "card_number": "",
                        "pension_category": "",
                        "elders_category": "",
                        "political_outlook": "",
                        "photo": [],
                    }
                }

                # 判断personnel_info里的数据是否一致
                newData = self._setValues(out_per, item, newData)
                # 判断personnel_info里的数据是否一致
                newData['personnel_info'] = self._setValues(
                    in_per, item, newData['personnel_info'])
                # 生日日期
                if 'date_birth' in item:
                    date_birth = self.clearZFC(item['date_birth'])
                    try:
                        if len(date_birth) > 10:
                            date_birth = date_birth[0:10]
                        newData['personnel_info']['date_birth'] = datetime.datetime.strptime(
                            date_birth, '%Y-%m-%d')
                    except Exception as e:
                        try:
                            id_card_y = item['id_card'][6:10]
                            id_card_m = item['id_card'][10:12]
                            id_card_d = item['id_card'][12:14]
                            newData['personnel_info']['date_birth'] = datetime.datetime.strptime(
                                id_card_y+'-'+id_card_m+'-'+id_card_d, '%Y-%m-%d')
                        except Exception as es:
                            return '身份证：'+id_card+'的长者出生年日期字段有误，请修改后再导入！'
                            # newData['personnel_info']['date_birth'] = datetime.date.today()
                    # newData['personnel_info']['date_birth'] = self._setTimeFormat(
                    #     item['date_birth'], newData['personnel_info']['date_birth'])

                person_data = get_info(newData, self.session)

                person_data['GUID'] = str(uuid.uuid1())
                person_data['version'] = 1
                person_data['bill_status'] = 'valid'
                person_data['valid_bill_id'] = str(uuid.uuid1())
                person_data['bill_operator'] = get_current_user_id(
                    self.session)

              #print('组装好的数据》》》', person_data)
                # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）

                # 机构储蓄对象
                org_account_data = FinancialAccountObject(
                    AccountType.account_recharg_wel, person_data['id'], person_data['organization_id'], AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
                org_account = get_info(
                    org_account_data.to_dict(), self.session)

                org_account['GUID'] = str(uuid.uuid1())
                org_account['version'] = 1
                org_account['bill_status'] = 'valid'
                org_account['valid_bill_id'] = str(uuid.uuid1())
                org_account['bill_operator'] = get_current_user_id(
                    self.session)

                # app储蓄对象
                app_account_data = FinancialAccountObject(
                    AccountType.account_recharg_app, person_data['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
                app_account = get_info(
                    app_account_data.to_dict(), self.session)

                app_account['GUID'] = str(uuid.uuid1())
                app_account['version'] = 1
                app_account['bill_status'] = 'valid'
                app_account['valid_bill_id'] = str(uuid.uuid1())
                app_account['bill_operator'] = get_current_user_id(
                    self.session)

                # 真实账户对象
                real_account_data = FinancialAccountObject(
                    AccountType.account_real, person_data['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
                real_account = get_info(
                    real_account_data.to_dict(), self.session)

                real_account['GUID'] = str(uuid.uuid1())
                real_account['version'] = 1
                real_account['bill_status'] = 'valid'
                real_account['valid_bill_id'] = str(uuid.uuid1())
                real_account['bill_operator'] = get_current_user_id(
                    self.session)

                # 补贴账户对象
                subsidy_account_data = FinancialAccountObject(
                    AccountType.account_subsidy, person_data['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
                subsidy_account = get_info(
                    subsidy_account_data.to_dict(), self.session)

                subsidy_account['GUID'] = str(uuid.uuid1())
                subsidy_account['version'] = 1
                subsidy_account['bill_status'] = 'valid'
                subsidy_account['valid_bill_id'] = str(uuid.uuid1())
                subsidy_account['bill_operator'] = get_current_user_id(
                    self.session)

                # 慈善账户
                charitable_account_data = FinancialAccountObject(
                    AccountType.account_charitable, person_data['id'], None, AccountStatus.normal, 5, AccountType.account_charitable, {}, 0)
                charitable_account = get_info(
                    charitable_account_data.to_dict(), self.session)

                charitable_account['GUID'] = str(uuid.uuid1())
                charitable_account['version'] = 1
                charitable_account['bill_status'] = 'valid'
                charitable_account['valid_bill_id'] = str(uuid.uuid1())
                charitable_account['bill_operator'] = get_current_user_id(
                    self.session)

                # 主数据
                person_arr.append(person_data)

                role_data = {'role_id': default_role_id,
                             'principal_account_id': person_data['id'], 'role_of_account_id': person_data['organization_id']}

                role_data = get_info(role_data, self.session)

                role_data['GUID'] = str(uuid.uuid1())
                role_data['version'] = 1
                role_data['bill_status'] = 'valid'
                role_data['valid_bill_id'] = str(uuid.uuid1())
                role_data['bill_operator'] = get_current_user_id(self.session)

                # role表的数组
                role_arr.append(role_data)

                # 账户表
                account_arr.append(org_account)
                account_arr.append(app_account)
                account_arr.append(real_account)
                account_arr.append(subsidy_account)
                account_arr.append(charitable_account)

                count = count + 1
              # print('长者表第'+str(count)+'个')
        if len(person_arr) and len(role_arr) and len(account_arr):
            def process_func(db):
                nonlocal res
                insert_many_data(db, 'PT_User', person_arr)
                insert_many_data(db, 'PT_Set_Role', role_arr)
                insert_many_data(db, 'PT_Financial_Account', account_arr)
                res = '导入成功：' + str(len(person_arr))+',不成功：0'
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
      # print('服务长者OVER')
        return res

    def clearZFC(self, string):
        # 清除excel可能自带的换行符和空格符
        if string == None:
            return ''
        return str(string).strip().replace('\r', '').replace('\n', '').replace('\t', '')

    # 通过循环来填充数据
    def _setValues(self, keys, item, datas):
        for i in range(0, len(keys)):
            key = keys[i]
            if key in item and item[key] != None:
                value = self.clearZFC(item[key])
                # 如果旧数据有，而新数据没有，则跳过不替换
                if (key in datas and datas[key] != '' and value == '') or (key not in datas and value == ''):
                    continue
                datas[key] = value
        return datas

    # 设置时间格式
    def _setTimeFormat(self, value, defaultValue=''):
        if value != None:
            return self.strtodate(self.clearZFC(value))
        if defaultValue != '':
            return defaultValue
        return ''

    def strtodate(self, string):
        try:
            return datetime.datetime.strptime(
                string, '%Y-%m-%d %H:%M:%S')
        except Exception as e:
            return string

    def is_alert_yinsizhengce(self, condition):

        # 南海暂时不
        # return {
        #     'is_first': False,
        #     'agreed': True
        # }

        ip = get_client_ip()

        if ip:
            _filter = MongoBillFilter()
            _filter.match_bill(C('ip') == ip)\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Privacy_Agree')

            # 如果这个IP没有同意过
            if len(res) == 0:

                # 获取APP设置里的隐私政策
                _filter_app_config = MongoBillFilter()
                _filter_app_config.match_bill((C('type') == 'privacy_policy'))\
                    .project({'_id': 0})

                res_app_config = self.query(
                    _filter_app_config, 'PT_App_Config')

                # 如果有隐私政策
                if len(res_app_config) > 0:
                    return {
                        'is_first': True,
                        'content': res_app_config[0]['data']['content']
                    }
                else:
                    # 没隐私政策
                    return {
                        'is_first': False,
                        'zclen': 0
                    }
            else:
                # 已同意过
                return {
                    'is_first': False,
                    'agreed': True
                }
        else:
            # 没ip
            return {
                'is_first': False,
                'ip': False
            }

    def agree_yinsizhengce(self, condition):

        ip = get_client_ip()

        if not ip:
            return False

        insertData = {
            'GUID': str(uuid.uuid1()),
            'id': str(uuid.uuid1()),
            'ip': ip,
            'create_date': get_cur_time(),
            'modify_date': get_cur_time(),
            'version': 1,
            'bill_status': 'valid',
        }

        user_id = get_user_id_or_false(self.session)

        if user_id:
            insertData['user_id'] = user_id

        # 同意隐私政策
        def process_func(db):
            insert_data(db, 'PT_Privacy_Agree', insertData)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)

        return 'Success'

    def get_organization_list_new(self, org_list, condition, page, count):
        '''获取所有机构列表'''
        keys = ['name', 'id', 'org_type', 'org_nature', 'admin_area_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('personnel_type') == '2')
                           & (C('name').like(values['name']))
                           & (C('id').inner(org_list)))\
            .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'area_info')\
            .add_fields({'org_nature': '$organization_info.organization_nature'})\
            .add_fields({'org_type': '$organization_info.personnel_category'})\
            .add_fields({'area_name': '$area_info.name'})\
            .match_bill((C('org_type') == values['org_type'])
                        & (C('org_nature') == values['org_nature'])
                        & (C('admin_area_id') == values['admin_area_id']))\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'id': 1, 'name': 1, 'org_type': 1, 'org_nature': 1, 'area_name': 1, 'organization_info': 1, 'account_status': 1})
        res = self.page_query(_filter, 'PT_User', page, count)
        return res

    def del_organization(self, ids):
        ''' 删除组织机构 '''
        res = 'Fail'
        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.groupsMember.value, {'id': ids[0]}, 'PT_User')
        if bill_id:
            res = 'Success'
        return res

    def is_ptcg(self):
        ''' 判断是否平台超管 '''
        if get_current_role_id(self.session) == '8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe':
            return True
        return False

    def update_org_show_app(self, data):
        '''组织机构上架/下架'''
        res = 'Fail'
        if 'id' in data.keys():
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == data['id']))\
                .project({'_id': 0})
            res = self.query(
                _filter, 'PT_User')
            if len(res) > 0:
                res[0]['organization_info']['is_show_app'] = data['show']
                bill_id = self.bill_manage_service.add_bill(
                    OperationType.update.value, TypeId.disease.value, res, 'PT_User')
                if bill_id:
                    res = 'Success'
        return res

    def update_org_start_stop(self, data):
        '''组织机构启用/停用'''
        res = 'Fail'
        if 'id' in data.keys():
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == data['id']))\
                .project({'_id': 0})
            res = self.query(
                _filter, 'PT_User')
            if len(res) > 0:
                res[0]['account_status'] = data['show']
                bill_id = self.bill_manage_service.add_bill(
                    OperationType.update.value, TypeId.disease.value, res, 'PT_User')
                if bill_id:
                    res = 'Success'
        return res
