# -*- coding: utf-8 -*-

from ...pao_python.pao.data import process_db, get_cur_time, dataframe_to_list, DataProcess
import pandas as pd
import uuid
import random
import datetime
import requests
import json
from ...service.mongo_bill_service import MongoBillFilter
from ...service.common import get_user_id_or_false, get_current_user_id, get_info
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)


class LedgerAccountManageService(MongoService):
    '''分账管理服务'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, ledger_account_register_url="https://ibsbjstar.ccb.com.cn/CCBIS/ccbMain?CCB_IBSVersion=V6"):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.ledger_account_register_url = ledger_account_register_url

    def bank_card_payment(self, condition):
        '''银行卡支付
        order_id 订单编号
        merchant_id 平台的商户代码
        pos_id 柜台代码
        branch_id 建行代码
        payment 付款总金额
        mac mac校验域
        pub 公钥后30位
        remark1 备注1
        remark2 备注2
        client_ip 客户端ip
        reg_info 客户注册信息
        pro_info 商品信息
        referer 商户url
        installnum 分期期数 *
        third_app_info 客户端标识 *
        time_out 订单超时时间 *
        pay_map 支付方式位图 *
        '''
        update_order_res = 'Fail'
        params = json.dumps({"MERCHANTID": condition.get('merchant_id'), "POSID": condition.get('pos_id'), "BRANCHID": condition.get('branch_id'),
                             "PAYMENT": condition.get('payment'), "MAC": condition.get('mac'), "PUB": condition['pub'], "REMARK1": condition['remark1'],
                             "REMARK2": condition['remark2'], "CLIENTIP": condition['client_ip'], "REGINFO": condition['reg_info'], "PROINFO": condition['pro_info'],
                             "REFERER": condition['referer'], "INSTALLNUM": condition['installnum'], "THIIRDAOOINFO": condition['third_app_info'], "TIMEOUT": condition['time_out'],
                             "ORDERID": condition['order_id'], 'TXCODE': 'PAY4FZ', 'CURCODE': '01', "PAYMAP": condition['pay_map'], })
        headers = {"Content-Type": "application/json; charset=UTF-8"}
        result = requests.post(
            'https://ibsbjstar.ccb.com.cn/CCBIS/ccbMain', data=params, headers=headers).json()
        if result:
            #更新订单状态
            keys = ['order_number']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('order_number') == values['order_number']))
            order_res = self.query(_filter, 'PT_Order')
            if len(order_res) > 0:
                if order_res[0].get('ORDER_STATUS') == 'ORDER_STATUS_UNPAID':
                    order_update_info = {'id': order_res[0].get(
                        'id'), 'ORDER_STATUS': 'ORDER_STATUS_PAID'}
                    def process_func(db):
                        nonlocal update_order_res
                        if 'id' in order_update_info.keys():
                            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                                        TypeId.orderStatus.value, order_update_info, 'PT_Order')
                            if bill_id:
                                update_order_res = 'Success'
                        else:
                            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                                        TypeId.orderStatus.value, order_update_info, 'PT_Order')
                            if bill_id:
                                update_order_res = 'Success'
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
        return update_order_res

    def ledger_account_registraion(self, condition):
        '''分账登记
        order_id 订单编号
        merchant_id 平台的商户代码
        pos_id 柜台代码
        branch_id 建行代码
        payment 付款总金额
        mac mac校验域
        pub 公钥后30位
        commission_ratio 分账比例
        charitable_commission_ratio 慈善基金比例
        provider_card_number商户收款编号（账号）/账号类型
        ybj_card_number 壹佰健收款编号/账号类型
        '''
        res = 'Fail'
        provider_account = condition['payment'] * \
            float(condition['commission_ratio'])
        ybj_account = condition['payment']-provider_account
        provider_fzinfo1 = '{}^{}^^^{}^{}'.format(
            '01', condition['provider_card_number'], str(provider_account), '0')
        ybj_fzinfo1 = '{}^{}^^^{}^{}'.format(
            '01', condition['ybj_card_number'], str(ybj_account), '0')
        fzinfo1 = '{}!{}#{}'.format('22', provider_fzinfo1, ybj_fzinfo1)
        params = json.dumps({"MERCHANTID": condition.get('merchant_id'), "POSID": condition.get('pos_id'), "BRANCHID": condition.get('branch_id'),
                             "MAC": condition.get('mac'), "PUB": condition.get('pub'), "ORDERID": condition.get('order_id'), 'TXCODE': 'PAY4FZ',
                             'CURCODE': '01', 'FZINFO1': fzinfo1})
        # prin t(params)
        headers = {"Content-Type": "application/json; charset=UTF-8"}
        result = requests.post(
            self.ledger_account_register_url, data=params, headers=headers).json()
        payurl = ''
        if result.SUCCESS and result.SUCCESS == 'true':
            payurl = result.PAYURL
            ledger_account_info = {"MERCHANTID": condition.get('merchant_id'), "POSID": condition.get('pos_id'), "BRANCHID": condition.get('branch_id'),
                                   "MAC": condition.get('mac'), "PUB": condition['pub'], "ORDERID": condition['order_id'], 'TXCODE': 'PAY4FZ',
                                   'CURCODE': '01', 'FZINFO1': fzinfo1, 'pag_url': payurl}

            def process_func(db):
                nonlocal res
                if 'id' in ledger_account_info.keys():
                    bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                                TypeId.ledgerAccountRegister.value, ledger_account_info, 'PT_Share_Payment_Registration')
                    if bill_id:
                        res = 'Success'
                else:
                    bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                                TypeId.ledgerAccountRegister.value, ledger_account_info, 'PT_Share_Payment_Registration')
                    if bill_id:
                        res = 'Success'
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)

        return res

    def update_servicre_option(self, servicr_option):
        '''# 新增/修改服务选项'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in servicr_option.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.serviceOption.value, servicr_option, 'PT_Service_Option')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.serviceOption.value, servicr_option, 'PT_Service_Option')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
