'''
@Author: your name
@Date: 2019-10-29 14:30:31
@LastEditTime: 2019-12-05 15:08:17
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_pub\security_register.py
'''
from ...service.common import LoginType, get_current_user_id, get_current_organization_id, get_current_role_id, set_current_org_id, set_current_role_id, set_current_user_id, get_indentify_id, get_info
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter
import hashlib
import datetime
import random


class RegisterService(MongoService):

    user_collection = 'PT_User'
    role_collection = 'PT_Role'
    permission_collection = 'PT_Permission'
    set_role = 'PT_Set_Role'
    fail_res = 'Fail'
    success_res = 'Success'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def register(self, register_data):
        '''注册
        Args：
            login_data:关键字包括：login_type,account_name,password
        Return:
        '''
        data_dict = {
            'personnel_info': {},
            'login_info': {
                'login_check': {},
            }
        }
        res = self.fail_res
        data_info = get_info({}, self.session)
        data_dict['id'] = data_info['id']
        data_dict['name'] = register_data['userName']
        data_dict["personnel_type"] = '1'
        data_dict['personnel_info']['personnel_category'] = '长者'
        data_dict['login_info']['login_type'] = 'account'
        data_dict['login_info']['login_check']['account_name'] = register_data['userName']
        data_dict['login_info']['login_check']['password'] = hashlib.sha256(
            register_data['password'].encode('utf-8')).hexdigest()
        data_dict['personnel_info']['telephone'] = register_data['mobile']
        set_role = {'role_id': '8ed260f6-e355-11e9-875e-a0a4c57e9ebe',
                    'principal_account_id': data_info['id'], 'role_of_account_id': get_current_organization_id(self.session)}
        bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                    TypeId.user.value, [set_role, data_dict], ['PT_Set_Role', 'PT_User'])
        if bill_id:
            res = 'Success'
        return res
