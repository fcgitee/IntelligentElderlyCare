from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import get_current_user_id
from ...service.mongo_bill_service import MongoBillFilter
import uuid
import time
from server.pao_python.pao.data import string_to_date
import datetime


class MyHome(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service_new = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, {
                'user_id': '23b3630a-d92c-11e9-8b9a-983b8f0bcd67',
                'organization_id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
            })
        self.PT_My_Room = 'PT_My_Room'
        self.PT_My_Room_Type = 'PT_My_Room_Type'
        self.PT_My_Room_Device = 'PT_My_Room_Device'
        self.PT_My_Host = 'PT_My_Host'
        self.PT_My_Device_Type = 'PT_My_Device_Type'
        self.PT_My_Warn_Info = 'PT_My_Warn_Info'

    def get_my_room_list(self):
        '''查询房间列表'''
        data = []
        cur_user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('user_id') == cur_user_id)
        res = self.query(_filter, self.PT_My_Room)
        if len(res):
          # print('查询中.....')
            _filter.inner_join(self.PT_My_Room_Type, 'room_type', 'id', 'room_type_info')\
                   .lookup_bill(self.PT_My_Room_Device, 'id', 'room_id', 'device')\
                   .add_fields({
                       'device_num': self.ao.size('$device'),
                       'room_type': '$room_type_info.room_type'
                   })\
                .project({'_id': 0, 'room_type_info._id': 0, 'device._id': 0})
            data = self.query(_filter, self.PT_My_Room)
          # print(_filter.filter_objects)
        else:  # 没有房间，初始化创建房间
          # print('新建中......')
            new_room = []
            _filter_room_type = MongoBillFilter()
            _filter_room_type.match(
                (C('name') != '走廊')
                & (C('name') != '楼梯')
            )
            room_type_list = self.query(
                _filter_room_type, self.PT_My_Room_Type)
            for x in room_type_list:
                # 创建房间数据
                room_mes = {
                    'id': str(uuid.uuid1()),
                    'user_id': cur_user_id,
                    'name': x['name'],
                    'room_type': [x['id']]
                }
                new_room.append(room_mes)
                # 返回前端数据展示
                back_mes = {'room_type': x['room_type'], 'device_num': 0}
                data.append({
                    **room_mes,
                    **back_mes
                })
            # raise '错误'
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.myRoom.value, new_room, self.PT_My_Room)
            if not bill_id:
                data = []
        return data

    def update_room(self, condition):
        res = {'code': 400, 'msg': ''}
        condition['user_id'] = get_current_user_id(self.session)
        isAdd = False if condition.get('id') else True
        flag = OperationType.add.value if isAdd else OperationType.update.value
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.myRoom.value, condition, self.PT_My_Room)
        if bill_id:
            res = {
                'msg': '房间创建成功' if isAdd else '房间更改成功',
                'code': 200
            }
        else:
            res['msg'] = '房间创建失败' if isAdd else '房间更改失败'
        return res

    def get_room_type_list(self):
        _filter = MongoBillFilter()
        _filter.project({'_id': 0})
        res = self.query(_filter, self.PT_My_Room_Type)
        return res

    def get_my_room_detail(self, condition):
        _filter = MongoBillFilter()
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter.match_bill(C('id') == values['id'])\
            .lookup_bill(self.PT_My_Room_Device, 'id', 'room_id', 'device')\
            .lookup_bill(self.PT_My_Host, 'id', 'room_id', 'host')\
            .project({'_id': 0, 'device._id': 0, 'host._id': 0})
        res = self.query(_filter, self.PT_My_Room)
      # print(_filter.filter_objects)
        return res

    def get_my_room_warning_detail(self, condition):
        _filter = MongoBillFilter()
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter.match_bill(C('id') == values['id'])\
            .lookup_bill(self.PT_My_Room_Device, 'id', 'room_id', 'device')\
            .lookup(self.PT_My_Room_Type, 'room_type', 'id', 'room_type_info')\
            .lookup_bill(self.PT_My_Warn_Info, 'device.device_id', 'device_id', 'warn_info')\
            .add_fields({
                'room_type': "$room_type_info.room_type",
            })\
            .sort({'warn_info.time': -1})\
            .project({'_id': 0, 'device._id': 0, 'host._id': 0, 'room_type_info._id': 0, 'warn_info._id': 0})
        res = self.query(_filter, self.PT_My_Room)
        return res

    def del_room(self, room_id):
        res = {'msg': '删除房间前请移除所有设备!', 'code': 400}
        _filter = MongoBillFilter()
        _filter.lookup_bill(self.PT_My_Room_Device, 'id', 'room_id', 'device')\
               .lookup_bill(self.PT_My_Host, 'id', 'room_id', 'host')\
            .project({'_id': 0, 'host._id': 0, 'device._id': 0})
        room_res = self.query(_filter, self.PT_My_Room)
        device_num = len(room_res['device'])
        host_num = len(room_res['host'])
        if not (device_num and host_num):  # 删除房间前提是该房间没有设备和主机
            bill_id = bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.myRoom.value, {'id': room_id}, self.PT_My_Room)
            res['msg'] = '删除房间成功' if bill_id else '删除房间失败'
            res['code'] = 200 if bill_id else 400
        elif host_num:  # 存在主机
            res['msg'] = '删除房间前请移除所有设备!'
        return res

    def get_all_my_room_list(self, condition):
        keys = ['id']
        values = self.get_value(condition, keys)
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('user_id') == user_id)
        )\
            .project({'_id': 0})
        res = self.query(_filter, self.PT_My_Room)
        return res

    def add_my_host(self, condition):
        '''新增/编辑主机'''
        isOn = True  # 是否执行
        res = {'code': 400, "msg": ''}
        condition['user_id'] = get_current_user_id(self.session)
        isAdd = False if condition.get('id') else True
        flag = OperationType.add.value if isAdd else OperationType.update.value
        if isAdd:
            _filter = MongoBillFilter()
            _filter.match_bill(C('host_id') == condition['host_id'])\
                .project({"_id": 0})
            host_res = self.query(_filter, self.PT_My_Host)
            if len(host_res):
                res['msg'] = '该主机ID已存在，无法绑定，请检查主机ID是否重复'
                isOn = False
        if isOn:
            bill_id = self.bill_manage_service.add_bill(
                flag, TypeId.myHost.value, condition, self.PT_My_Host)
            if bill_id:
                res['msg'] = '主机绑定成功' if isAdd else '主机更改成功'
                res['code'] = 200
            else:
                res['msg'] = '主机绑定失败，请检查网络并主机信息确认无误再重新绑定' if isAdd else '主机更改失败'
        return res

    def get_my_host_list(self, condition):
        '''获取我的主机列表'''
        keys = ['id']
        values = self.get_value(condition, keys)
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('user_id') == user_id)
        )\
            .project({'_id': 0})
        res = self.query(_filter, self.PT_My_Host)
        return res

    def del_my_host(self, host_id):
        res = {'msg': '删除主机前请移除所有设备！', 'code': 400}
        _filter = MongoBillFilter()
        _filter.match_bill(C('host_id') == host_id)\
            .project({'_id': 0})
        device_res = self.query(_filter, self.PT_My_Room_Device)
        if not len(device_res):  # 删除主机的前提是该主机没有绑定设备
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.myHost.value, {'id': host_id}, self.PT_My_Host)
            res['msg'] = '删除主机成功' if bill_id else '删除主机失败'
            res['code'] = 200 if bill_id else 400
        return res

    def get_my_host_detail(self, condition):
        keys = ['id', 'host_id']
        values = self.get_value(condition, keys)
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('host_id') == values['host_id'])
            & (C('user_id') == user_id)
        )\
            .inner_join_bill(self.PT_My_Room, 'room_id', 'id', 'room_info')\
            .lookup_bill(self.PT_My_Room_Device, 'host_id', 'host_id', 'device')\
            .lookup_bill(self.PT_My_Room, 'device.room_id', 'id', 'device_room_info')\
            .project({'_id': 0, 'device._id': 0, 'room_info._id': 0, 'device_room_info._id': 0})
        res = self.query(_filter, self.PT_My_Host)
        return res

    def add_my_device(self, condition):
        '''新增/编辑设备'''
        isOn = True  # 是否执行
        res = {'code': 400, "msg": ''}
        condition['user_id'] = get_current_user_id(self.session)
        isAdd = False if condition.get('id') else True
        flag = OperationType.add.value if isAdd else OperationType.update.value
        if isAdd:
            _filter = MongoBillFilter()
            _filter.match_bill(C('device_id') == condition['device_id'])\
                .project({"_id": 0})
            host_res = self.query(_filter, self.PT_My_Room_Device)
            if len(host_res):
                res['msg'] = '该设备ID已存在，无法绑定，请检查设备ID是否重复'
                isOn = False
        if isOn:
            bill_id = self.bill_manage_service.add_bill(
                flag, TypeId.myHost.value, condition, self.PT_My_Room_Device)
            if bill_id:
                res['msg'] = '设备绑定成功' if isAdd else '设备更改成功'
                res['code'] = 200
            else:
                res['msg'] = '设备绑定失败，请检查网络并设备信息确认无误再重新绑定' if isAdd else '设备更改失败'
        return res

    def get_my_device_list(self, condition):
        return []

    def get_my_device_detail(self, condition):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['id'])\
            .project({'_id': 0})
        res = self.query(_filter, self.PT_My_Room_Device)
        return res

    def del_my_device(self, device_id):
        res = {'code': 400, 'msg': '设备删除失败'}
        bill_id = self.bill_manage_service.add_bill(
            OperationType.delete.value, TypeId.myDetail.value, {'id': device_id}, self.PT_My_Room_Device)
        if bill_id:
            res = {'code': 200, 'msg': '设备删除成功'}
        return res

    def get_device_type_list(self, condition):
        _filter = MongoBillFilter()
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter.match(C('id') == values['id'])\
               .project({'_id': 0})
        res = self.query(_filter, self.PT_My_Device_Type)
        return res

    def add_warn_info(self, condition):
        data = condition['data']
        now = time.localtime()
        now_format = time.strftime("%Y-%m-%d %H:%M:%S", now)
        info = {
            'id': str(uuid.uuid1()),
            'time': now_format,
            "level": self.__get_level(data['level']),
            "value": data['val'],
            "device_id": data['sn'],
            "warn_type": self.__get_warn_type(data['type']),
            'status': data['status'],
            'check_status': '待处理',
        }
        bill_id = self.bill_manage_service_new.add_bill(
            OperationType.add.value, TypeId.myWarn.value, info, self.PT_My_Warn_Info)
        return {"cmd": "alert", "id": data['sn'], "time": time.mktime(now), "code": 200} if bill_id else{'code': 500}

    def get_my_history_warn(self, condition):
        _filter = MongoBillFilter()
        keys = ['id', 'date', 'date_last']
        if condition.get('date'):
            date = string_to_date(condition['date'].split('T')[0])
            condition['date_last'] = date + datetime.timedelta(days=1)
            condition['date'] = date
        values = self.get_value(condition, keys)
        _filter.match_bill(
            C('id') == values['id']
        )\
            .lookup_bill(self.PT_My_Warn_Info, 'device_id', 'device_id', 'warn_info')\
            .match_bill(
                (C('warn_info.time') >= values['date'])
            & (C('warn_info.time') <= values['date_last'])
        )\
            .project({'_id': 0, 'warn_info._id': 0})
        res = self.query(_filter, self.PT_My_Room_Device)
        return res

    def __get_level(self, x):
        '''获取警报信息等级'''
        return {
            '1': '严重',
            '2': '异常',
            '3': '提示'
        }[x]

    def __get_warn_type(self, type):
        try:
            return {
                "01": '心率异常偏高',
                '02': '心率异常偏低',
                "03": '夜间离回床异常',
                "04": '滞留时间过长',
                "05": '入侵异常',
                "06": '离家异常',
                "07": '摔倒',
                '08': '烟火报警',
                "09": '燃气报警',
                "10": '漏水报警',
                "11": '其他'
            }[type]
        except:
            return type
