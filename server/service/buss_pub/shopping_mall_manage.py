# -*- coding: utf-8 -*-

from ...pao_python.pao.data import process_db, get_cur_time, dataframe_to_list, DataProcess
import pandas as pd
import uuid
import random
import datetime
import json
from ...service.mongo_bill_service import MongoBillFilter
from ...service.common import get_user_id_or_false, get_current_user_id, get_current_user_name, get_info, get_common_project
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)


class ShoppingMallManageService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_data_dictionary_list(self, org_list, condition, page, count):
        ''' 查询一级字典列表 '''

        keys = ['id', 'name', 'code', 'in_select']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
        ).sort({'modify_date': -1})

        if 'in_select' in condition:
            ''' 下拉列表的一级字典列表只需要ID和NAME '''
            _filter.project({
                '_id': 0,
                'id': 1,
                'name': 1,
            })
            return self.query(_filter, 'PT_Data_Dictionary')

        _filter.lookup_bill('PT_Data_Dictionary_Detail', 'id', 'dictionary_id', 'childs')\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'code': 1,
                'remark': 1,
                'index_no': 1,
                'is_sys': 1,
                'state': 1,
                'modify_date': 1,
                'create_date': 1,
                'organization_id': 1,
                'childs.id': 1,
                'childs.name': 1,
                'childs.code': 1,
                'childs.remark': 1,
                'childs.index_no': 1,
                'childs.is_sys': 1,
                'childs.dictionary_id': 1,
                'childs.state': 1,
                'childs.modify_date': 1,
                'childs.create_date': 1,
                'childs.organization_id': 1,
            })

        res = self.page_query(_filter, 'PT_Data_Dictionary', page, count)

        return res

    def get_data_dictionary_childs_list(self, org_list, condition, page, count):
        ''' 查询二级字典列表 '''

        keys = ['id', 'name', 'code', 'parent_code', 'parent_codes']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('parent_code') == values['parent_code'])
            & (C('parent_code').inner(values['parent_codes']))
            & (C('name').like(values['name']))
            & (C('code').like(values['code']))
        ).sort({'sort': -1, 'modify_date': -1})\

        if condition.get('in_select') == True:
            _filter.project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'code': 1,
                'parent_code': 1,
            })
        else:
            _filter.project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'code': 1,
                'remark': 1,
                'index_no': 1,
                'is_sys': 1,
                'dictionary_id': 1,
                'state': 1,
                'modify_date': 1,
                'create_date': 1,
                'organization_id': 1,
            })

        res = self.page_query(
            _filter, 'PT_Data_Dictionary_Detail', page, count)

        return res

    def update_data_dictionary(self, data):
        ''' 更新一/二级字典列表 '''
        verify = self.check_required(data, {
            'name.required': '字典名称不能为空',
            'code.required': '字典编号不能为空',
            'index_no.required': '排序号不能为空',
            'index_no.int': '排序号必须为数字类型',
            'state.required': '请选择状态',
            'is_sys.required': '请选择是否系统字典',
            'is_sys.boolean': '系统字典状态有误',
        })
        if verify['state'] != True:
            return verify['msg']

        data = self.filter_keys(
            data, ['id', 'dictionary_id', 'name', 'code', 'index_no', 'remark', 'state', 'is_sys'])

        res = 'Fail'

        if not data.get('dictionary_id'):
            PT_DB = 'PT_Data_Dictionary'
        else:
            PT_DB = 'PT_Data_Dictionary_Detail'

            # 查询父表获取数据
            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('id') == data.get('dictionary_id'))
            )\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'code': 1,
                })

            res = self.query(_filter, 'PT_Data_Dictionary')

            if len(res) == 0:
                return '找不到该父表数据'

            data['parent_name'] = res[0]['name']
            data['parent_code'] = res[0]['code']

        if 'id' in list(data.keys()) and data['id'] == 'parent':
            del(data['id'])

        data['update_by'] = get_current_user_id(self.session)

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.dataDictionary.value, data, PT_DB, True)
            if bill_id:
                res = 'Success'
        else:
            data['is_sys'] = True
            data['state'] = '启用'
            data['create_by'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.dataDictionary.value, data, PT_DB)
            if bill_id:
                res = 'Success'
        return res

    def delete_data_dictionary(self, condition):
        ''' 删除字典 '''
        result = 'Fail'
        res = []

        if 'type' not in condition or condition['type'] not in ['1', '2'] or 'ids' not in condition or type(condition['ids']) != list or len(condition['ids']) != 1:
            return result

        table = ''

        if condition['type'] == '1':
            table = 'PT_Data_Dictionary'

            # 第一级要判断下面是否有下二级
            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('dictionary_id') == condition['ids'][0])
            )\
                .project({
                    '_id': 0,
                    'id': 1,
                })

            res = self.query(_filter, 'PT_Data_Dictionary_Detail')

            if len(res) > 0:
                return '该分类下级还有二级分类，不可删除'

        elif condition['type'] == '2':
            table = 'PT_Data_Dictionary_Detail'

        for i in condition['ids']:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.dataDictionary.value, {'id': i}, table)
        if bill_id:
            result = 'Success'
        return result

    # 过滤字段
    def filter_keys(self, data, keys):
        new_data = {}
        for item in data:
            if item in keys:
                new_data[item] = data[item]
        return new_data

    # 验证规则
    def check_required(self, condition, rules, mode=1):
        msgArr = []
        msg = ''
        state = True
        for item in rules:
            if mode == 1 and len(msgArr):
                return {
                    'state': False,
                    'msg': msgArr[0],
                }
            item_rule = item.split('.')
            if item_rule[1] == 'required':
                # 必填
                if item_rule[0] not in condition or condition[item_rule[0]] in [None, '']:
                    msgArr.append(rules[item])
            elif item_rule[1] == 'int':
                # 整数
                if not isinstance(condition[item_rule[0]], int):
                    msgArr.append(rules[item])
            elif item_rule[1] == 'number':
                # 数字，整数或浮点数
                if not isinstance(condition[item_rule[0]], int) and not isinstance(condition[item_rule[0]], float):
                    msgArr.append(rules[item])
            elif item_rule[1] == 'boolean':
                # 真假
                if condition[item_rule[0]] not in [True, False]:
                    msgArr.append(rules[item])

        if len(msgArr):
            state = False
            msg = '，'.join(msgArr)

        return {
            'state': state,
            'msg': msg,
        }

    def get_data_dictionary_value(self, condition):
        keys = ['name', 'parent_code']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('name') == values['name'])
            & (C('parent_code') == values['parent_code'])
        )\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'code': 1,
            })
        res = self.query(_filter, 'PT_Data_Dictionary_Detail')

        if len(res):
            return res[0]['code']
        else:
            return False

    def get_bill_code(self, bill_type):
        ''' 获取流水号 '''

        # 循环次数
        times = 1
        # 订单号长度
        zf_len = 5
        # 时间
        now_time = get_cur_time()

        if bill_type == '商品':

            # 流水号ID
            bill_code_id = 'a0501970-e670-11ea-9710-a0a4c57e9ebe'
            # 订单前缀
            bill_code_pre = 'PO'

        elif bill_type == '订单':

            # 流水号ID
            bill_code_id = 'c0202014-e670-11ea-9710-a0a4c57e9ebe'
            # 订单前缀
            bill_code_pre = 'OD'

        else:
            return False

        res = self.getInc(
            {'id': bill_code_id}, {'number': 1}, 'PT_Serial_Number')

        while res['code'] != 200 and times < 5:
            times = times + 1
            res = self.getInc(
                {'id': bill_code_id}, {'number': 1}, 'PT_Serial_Number')

        if res['code'] != 200:
            return False

        return bill_code_pre + str(now_time.year) + str(now_time.month) + str(now_time.day) + str(res['res'].get('number')).zfill(zf_len)

    def get_last_day_of_month(self, any_day):
        ''' 获取获得一个月中的最后一天 '''
        next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
        return str((next_month - datetime.timedelta(days=next_month.day)).day).zfill(2)

    def get_financial_overview_list(self, org_list, condition, page, count):
        ''' 财务总览 '''

        if 'date_type' in condition and condition.get('date_type') in ['日汇总', '月汇总']:
            if condition.get('date_type') == '日汇总':
                condition['create_begin_date'] = condition['date_value'][0:10] + \
                    "T00:00:00.00Z"
                condition['create_end_date'] = condition['date_value'][0:10] + \
                    "T23:59:59.00Z"
            elif condition.get('date_type') == '月汇总':

                condition['create_begin_date'] = condition['date_value'][0:7] + \
                    "-01T00:00:00.00Z"

                month_last_day = self.get_last_day_of_month(
                    as_date(condition['create_begin_date']))
                condition['create_end_date'] = condition['date_value'][0:7] + \
                    "-" + month_last_day + "T23:59:59.00Z"

        keys = ['create_begin_date', 'create_end_date']
        values = self.get_value(condition, keys)

        # 获取这个机构下的所有订单
        _filter_orders = MongoBillFilter()
        _filter_orders.match_bill(
            (C('organization_id').inner(org_list))
            & (C('create_date') >= as_date(values['create_begin_date']))
            & (C('create_date') <= as_date(values['create_end_date']))
        )\
            .sort({
                'create_date': -1,
            })\
            .project({
                '_id': 0,
                'final_amount': 1,
                'postage_amount': 1,
                'order_status': 1,
                'settlement_status': 1,
                'transaction_rate_amount': 1,
                'pay_status': 1,
                'create_date': 1,
            })

        res_orders = self.query(_filter_orders, 'PT_Order')

        waiting = 0
        finished = 0
        paid = 0

        details_obj = {}
        details_arr = []

        for item in res_orders:
            if item['order_status'] in ['COMPLETED']:
                finished += (item['final_amount'] + item['postage_amount'])
            if item['settlement_status'] in ['已结算']:
                paid += (item['final_amount'] + item['postage_amount'])
            else:
                waiting += (item['final_amount'] + item['postage_amount'])

            if 'date_type' in condition and condition.get('date_type') == '日汇总':
                create_date_str = str(
                    item['create_date'].year) + '-' + str(item['create_date'].month).zfill(2) + '-' + str(item['create_date'].day).zfill(2)
            else:
                create_date_str = str(
                    item['create_date'].year) + '-' + str(item['create_date'].month).zfill(2)

            if create_date_str not in details_obj:
                details_obj[create_date_str] = {
                    'id': create_date_str,
                    'date': create_date_str,
                    'income': 0,
                    'outpaid': 0,
                    'account': 0,
                }

            details_obj[create_date_str]['income'] += (
                item['final_amount'] + item['postage_amount'])
            details_obj[create_date_str]['outpaid'] += (
                item['transaction_rate_amount'])

        for item in details_obj:
            details_arr.append(details_obj[item])

        return {
            'statistics': [
                {
                    'id': 'statistics',
                    'waiting': waiting,
                    'finished': finished,
                    'paid': paid,
                }
            ],
            'result': details_arr,
            'total': len(details_arr),
        }

    def get_entry_record_list(self, org_list, condition, page, count):
        ''' 入账记录 '''

        keys = ['service_provider_name']
        values = self.get_value(condition, keys)

        # 获取这个机构下的所有入账记录
        _filter_records = MongoBillFilter()
        _filter_records.match_bill(
            (C('service_provider_id').inner(org_list))
        )\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'service_provider_info')\
            .match_bill((C('service_provider_info.name').like(values['service_provider_name'])))\
            .add_fields({
                "service_provider_name": self.ao.array_elemat("$service_provider_info.name", 0),
                "bank_name": '中国建设银行',
            })\
            .sort({
                'create_date': -1,
            })\
            .project({
                '_id': 0,
                'service_provider_id': 1,
                'service_provider_name': 1,
                'in_amount': 1,
                'in_order_count': 1,
                'out_amount': 1,
                'out_order_count': 1,
                'payment_date': 1,
                'status': 1,
                'bank_name': 1,
                'create_date': 1,
            })

        res_record = self.page_query(
            _filter_records, 'PT_Day_Review_Record', page, count)

        return res_record

    def get_payment_record_list(self, org_list, condition, page, count):
        ''' 出/入账详细记录 '''

        # 获取这个时间范围下的所有入账记录
        _filter_records = MongoBillFilter()
        _filter_records.match_bill(
            (C('service_provider_id').inner(org_list))
        )\
            .sort({
                'create_date': -1,
            })\
            .project({
                '_id': 0,
                'id': 1,
            })

        res_record = self.page_query(
            _filter_records, 'PT_Day_Review_Record', page, count)

        return res_record

    def get_goods_list(self, org_list, condition, page, count):
        ''' 获取商品列表 '''
        keys = ['id', 'status', 'product_status', 'title', 'min_price',
                'max_price', 'min_sales_num', 'max_sales_num', 'product_category_id', 'active_status']

        if 'price' in condition:
            if 'before_value' in condition['price'] and condition['price']['before_value'] != '' and type(condition['price']['before_value']) == str:
                condition['min_price'] = float(
                    condition['price']['before_value'])
            if 'after_value' in condition['price'] and condition['price']['after_value'] != '' and type(condition['price']['after_value']) == str:
                condition['max_price'] = float(
                    condition['price']['after_value'])
            del(condition['price'])

        if 'sales_num' in condition:
            if 'before_value' in condition['sales_num'] and condition['sales_num']['before_value'] != '' and type(condition['sales_num']['before_value']) == str:
                condition['min_sales_num'] = int(
                    condition['sales_num']['before_value'])
            if 'after_value' in condition['sales_num'] and condition['sales_num']['after_value'] != '' and type(condition['sales_num']['after_value']) == str:
                condition['max_sales_num'] = int(
                    condition['sales_num']['after_value'])
            del(condition['sales_num'])

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('status') == values['status'])
            & (C('status') == values['product_status'])
            & (C('active_status') == values['active_status'])
            & (C('product_category_id') == values['product_category_id'])
            & (C('price') <= values['max_price'])
            & (C('price') >= values['min_price'])
            & (C('title').like(values['title']))
        )\
            .lookup_bill('PT_Order_Line', 'id', 'product_id', 'order_line_info')\
            .add_fields({
                "sales_num": self.ao.size("$order_line_info"),
            })\
            .match_bill((C('sales_num') <= values['max_sales_num'])
                        & (C('sales_num') >= values['min_sales_num'])
                        )\
            .lookup_bill('PT_Product_Stock', 'id', 'product_id', 'stock_info')\
            .add_fields({
                "stock_num": self.ao.array_elemat("$stock_info.stock_num", 0),
            })\
            .lookup_bill('PT_Service_Type', 'product_category_id', 'id', 'type_info')\
            .add_fields({
                "category_name": self.ao.array_elemat("$type_info.name", 0),
            })\
            .lookup_bill('PT_Data_Dictionary_Detail', 'status', 'code', 'dictionary_info')\
            .add_fields({
                "status_name": self.ao.array_elemat("$dictionary_info.name", 0),
            })\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_info')\
            .add_fields({
                "organization_name": self.ao.array_elemat("$organization_info.name", 0),
                "organization_picture": self.ao.array_elemat("$organization_info.organization_info.picture_list", 0),
                "organization_telephone": self.ao.array_elemat("$organization_info.organization_info.telephone", 0),
            })

        if 'sort' in condition.keys() and condition['sort'] != '':
            if condition.get('sort') == 'sales_num':
                _filter.sort({
                    'sales_num': -1,
                    'sort': -1,
                    'modify_date': -1,
                })
            elif condition.get('sort') == 'price':
                price = -1
                if condition.get('selected_price_direction') == 'reverse':
                    price = 1
                _filter.sort({
                    'price': price,
                    'sort': -1,
                    'modify_date': -1,
                })
        else:
            _filter.sort({
                'sort': -1,
                'modify_date': -1,
            })
        _filter.project({
            '_id': 0,
            'id': 1,
            'title': 1,
            'code': 1,
            'stock_num': 1,
            'category_name': 1,
            'video_url': 1,
            'video_cover_image_url': 1,
            'sort': 1,
            'sales_num': 1,
            'review_date': 1,
            'review_by_id': 1,
            'product_category_id': 1,
            'content': 1,
            'price': 1,
            'status': 1,
            'active_status': 1,
            'status_name': 1,
            'active_date': 1,
            'postage_type': 1,
            'postage_price': 1,
            'pay_type': 1,
            'express_fee': 1,
            'commission_ratio': 1,
            'transaction_rate': 1,
            'charitable_commission_ratio': 1,
            'image_urls': 1,
            'reason': 1,
            'organization_id': 1,
            'organization_name': 1,
            'organization_picture': 1,
            'modify_date': 1,
            'create_date': 1,
        })

        res = self.page_query(_filter, 'PT_Product', page, count)
        return res

    def get_services_list(self, org_list, condition, page, count):
        ''' 获取服务列表 '''
        keys = ['id', 'status', 'state']

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
            & (C('state') == values['state'])
            & (C('status') == values['status'])
        )\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_info')\
            .add_fields({
                "sales_num": 0,
                "postage_price": 0,
                "image_urls": "$picture_collection",
                "content": "$introduce",
                "title": "$name",
                "price": "$service_package_price",
                "organization_name": self.ao.array_elemat("$organization_info.name", 0),
                "organization_picture": self.ao.array_elemat("$organization_info.organization_info.picture_list", 0),
                "organization_telephone": self.ao.array_elemat("$organization_info.organization_info.telephone", 0),
            })

        if 'sort' in condition.keys() and condition['sort'] != '':
            if condition.get('sort') == 'sales_num':
                _filter.sort({
                    'sales_num': -1,
                    'sort': -1,
                    'modify_date': -1,
                })
            elif condition.get('sort') == 'price':
                price = -1
                if condition.get('selected_price_direction') == 'reverse':
                    price = 1
                _filter.sort({
                    'price': price,
                    'sort': -1,
                    'modify_date': -1,
                })
        else:
            _filter.sort({
                'sort': -1,
                'modify_date': -1,
            })
        _filter.project({
            '_id': 0,
            'id': 1,
            'title': 1,
            'content': 1,
            'sales_num': 1,
            'image_urls': 1,
            'price': 1,
            'postage_price': 1,
            'organization_id': 1,
            'organization_name': 1,
            'organization_picture': 1,
            'modify_date': 1,
            'create_date': 1,
        })

        res = self.page_query(_filter, 'PT_Service_Product', page, count)
        return res

    def update_goods_sh(self, data):
        res = 'Failed'
        verify = self.check_required(data, {
            'id.required': '商品ID不能为空',
            'status.required': '审核结果不能为空',
        })

        if verify['state'] != True:
            return verify['msg']

        # 审核的时候，根据原数据的active_date判断是否需要更改状态为上架
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == data['id'])
        )\
            .project({
                '_id': 0,
                'id': 1,
                'active_date': 1,
            })
        res = self.query(_filter, 'PT_Product')

        if len(res) == 0:
            return '找不到此产品'

        if 'active_date' in res and type(res['active_date']) == list and res['active_date'][0] == '保存后立即上架':
            data['active_status'] = '上架'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.products.value, data, 'PT_Product', True)
        if bill_id:
            res = 'Success'
        return res

    def update_goods(self, data):
        ''' 更新一/二级商品列表 '''
        verify = self.check_required(data, {
            'title.required': '商品名称不能为空',
            # 'sort.required': '排序不能为空',
            # 'sort.int': '排序必须为数字类型',
            'price.required': '价格不能为空',
            'price.number': '价格必须为数字类型',
            'stock_num.required': '库存不能为空',
            'stock_num.int': '库存必须为数字类型',
            'postage_type.required': '配送方式不能为空',
            'content.required': '商品详情不能为空',
        })
        if verify['state'] != True:
            return verify['msg']

        # if data.get('postage_type') and len(data['postage_type']) > 0 and data['postage_type'][0] == 'DELIVERY_TYPE_OFFLINE':
        #     data['postage_price'] = 0

        if data.get('express_fee') == '买家付费':
            if 'postage_price' not in data.keys() and data['postage_price'] != '':
                return '买家付费必须填入费用'
        elif data.get('express_fee') == '包邮':
            data['postage_price'] = 0

        # 线下自取有问题，暂时先默认0
        if 'postage_price' not in data:
            data['postage_price'] = 0

        data = self.filter_keys(
            data, ['id', 'title', 'price', 'stock_num', 'content', 'image_urls', 'video_url', 'video_cover_image_url', 'product_category_id', 'postage_type', 'postage_price', 'express_fee', 'active_date'])

        res = 'Fail'

        # 查询字典表
        status = self.get_data_dictionary_value({
            'name': '待审核',
            'parent_code': 'PRODUCT_STATUS'
        })

        if status == False:
            return '找不到字典表'

        data['status'] = status
        data['update_by'] = get_current_user_id(self.session)
        data['active_status'] = '下架'

        if 'id' in list(data.keys()):

            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('product_id') == data['id'])
            )\
                .project({
                    '_id': 0,
                    'id': 1,
                })
            res_ps = self.query(_filter, 'PT_Product_Stock')

            stock_id = ''

            if len(res_ps) == 0:
                product_stock = get_info({
                    'product_id': data['id']
                }, self.session)
                stock_id = product_stock['id']
                self.bill_manage_service.add_bill(
                    OperationType.add.value, TypeId.products.value, product_stock, 'PT_Product_Stock')
            else:
                stock_id = res_ps[0]['id']

            # 分出一个库存数据
            stock_data = {
                'id': stock_id,
                'product_id': data['id'],
                'stock_num': data['stock_num'],
            }

            del(data['stock_num'])

            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.products.value, [data, stock_data], ['PT_Product',
                                                                                        'PT_Product_Stock'], True)
            if bill_id:
                res = 'Success'
        else:
            # 创建人
            data['create_by'] = get_current_user_id(self.session)
            # 商品号
            data['code'] = self.get_bill_code('商品')

            # 商品表
            product_info = get_info(data, self.session)

            # 库存表
            product_stock = get_info({
                'product_id': product_info['id'],
                'stock_num': product_info['stock_num'],
            }, self.session)

            del(product_info['stock_num'])

            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.products.value, [product_info, product_stock], ['PT_Product', 'PT_Product_Stock'])
            if bill_id:
                res = 'Success'
        return res

    def change_goods(self, datas):
        ''' 批量的修改商品信息 '''
        res = 'Failed'
        for item in datas:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.products.value, item, 'PT_Product', True)
        if bill_id:
            res = 'Success'
        return res

    def copy_goods(self, condition):
        ''' 复制一个商品 '''

        res = 'Failed'

        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
        )\
            .lookup_bill('PT_Product_Stock', 'id', 'product_id', 'stock_info')\
            .add_fields({
                "stock_num": self.ao.array_elemat("$stock_info.stock_num", 0),
            })\
            .project({
                '_id': 0,
                'stock_info': 0,
                'create_date': 0,
                'modify_date': 0,
                **get_common_project()
            })

        res = self.query(_filter, 'PT_Product')

        if len(res) == 0:
            return '没有找到此商品'

        new_product_info = {
            **res[0],
        }

        # 删掉id
        del(new_product_info['id'])

        new_product_info = get_info(new_product_info, self.session)

        # 创建人
        new_product_info['create_by'] = get_current_user_id(self.session)
        # 商品号
        new_product_info['code'] = self.get_bill_code('商品')
        # 需要重新审核
        new_product_info['status'] = 'PENDING_APPROVAL'

        # 库存表
        product_stock = get_info({
            'product_id': new_product_info['id'],
            'stock_num': new_product_info['stock_num'],
        }, self.session)

        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.products.value, [new_product_info, product_stock], ['PT_Product', 'PT_Product_Stock'])
        if bill_id:
            res = 'Success'

        return res

    def delete_goods(self, condition):
        res = 'Failed'
        ''' 删除商品 '''
        for i in condition['ids']:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.products.value, {'id': i}, 'PT_Product')
        if bill_id:
            res = 'Success'
        return res

    def get_goods_detail_list_app(self, org_list, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
        )\
            .project({
                '_id': 0,
                'id': 1,
                'organization_id': 1,
                'title': 1,
                'price': 1,
                'postage_price': 1,
                'content': 1,
                'image_urls': 1,
                'img': 1,
                'desc': 1
            })

        res = self.page_query(_filter, 'PT_Product', page, count)
        if len(res) > 0:
            org_ids = []
            stock_num_ids = []
            org_data = {}
            stock_num_data = {}
            for i, x in enumerate(res['result']):
                org_ids.append(res['result'][i]['organization_id'])
                stock_num_ids.append(res['result'][i]['id'])

            if len(org_ids) > 0:
                _filter_org = MongoBillFilter()
                _filter_org.match_bill(
                    (C('id').inner(org_ids))
                )\
                    .add_fields({
                        'org_picture': self.ao.array_elemat('$organization_info.picture_list', 0)
                    })\
                    .project({'_id': 0, 'id': 1, 'name': 1, 'org_picture': 1})
                res_org = self.query(_filter_org, 'PT_User')
                if len(res_org) > 0:
                    for org in res_org:
                        org_data[org['id']] = org

            if len(stock_num_ids) > 0:
                _filter_stock_num = MongoBillFilter()
                _filter_stock_num.match_bill(
                    (C('product_id').inner(stock_num_ids))
                )\
                    .project({'_id': 0, 'product_id': 1, 'stock_num': 1})
                res_stock_num = self.query(
                    _filter_stock_num, 'PT_Product_Stock')
                if len(res_stock_num) > 0:
                    for stock_num in res_stock_num:
                        stock_num_data[stock_num['product_id']
                                       ] = stock_num['stock_num']
            for i, x in enumerate(res['result']):
                if 'organization_id' in res['result'][i] and res['result'][i]['organization_id'] in org_data.keys():
                    res['result'][i]['org_name'] = org_data[res['result']
                                                            [i]['organization_id']]['name']
                    res['result'][i]['org_picture'] = org_data[res['result']
                                                               [i]['organization_id']]['org_picture']
                if 'id' in res['result'][i] and res['result'][i]['id'] in stock_num_data.keys():
                    res['result'][i]['stock_num'] = stock_num_data[res['result'][i]['id']]
        return res

    def get_product_orders_list(self, org_list, condition, page, count):
        ''' 获取商品订单列表 '''
        keys = ['id', 'order_status', 'order_number',
                'settlement_status', 'product_category_id', 'title', 'buyer_name', 'buyer_mobile', 'create_user_name', 'create_user_mobile', 'address', 'create_begin_date', 'create_end_date', 'settlement_begin_date', 'settlement_end_date', 'postage_type']

        if 'create_date' in list(condition.keys()):
            condition['create_begin_date'] = condition['create_date'][0][0:10] + \
                "T00:00:00.00Z"
            condition['create_end_date'] = condition['create_date'][1][0:10] + \
                "T23:59:59.00Z"
        if 'settlement_date' in list(condition.keys()):
            condition['settlement_begin_date'] = condition['settlement_date'][0][0:10] + \
                "T00:00:00.00Z"
            condition['settlement_end_date'] = condition['settlement_date'][1][0:10] + \
                "T23:59:59.00Z"

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('order_status') == values['order_status'])
            & (C('order_number') == values['order_number'])
            & (C('settlement_status') == values['settlement_status'])
            & (C('create_user_name') == values['create_user_name'])
            & (C('create_user_mobile') == values['create_user_mobile'])
            & (C('address').like(values['address']))
            & (C('order_status') != 'DELETED')
            & (C('create_date') >= as_date(values['create_begin_date']))
            & (C('create_date') <= as_date(values['create_end_date']))
            & (C('settlement_date') >= as_date(values['settlement_begin_date']))
            & (C('settlement_date') <= as_date(values['settlement_end_date']))
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Order_Line', 'id', 'order_id', 'line_info')\
            .match_bill(
                (C('line_info.product_category_id')
                 == values['product_category_id'])
            & (C('line_info.product_title').like(values['title']))
            & (C('line_info.product_postage_type') == values['postage_type'])
        )\
            .lookup_bill('PT_User', 'create_user_id', 'id', 'buyer_info')\
            .add_fields({
                "buyer_name": self.ao.array_elemat("$buyer_info.name", 0),
                "buyer_mobile": self.ao.array_elemat("$buyer_info.personnel_info.telephone", 0),
            })\
            .match_bill(
                (C('buyer_name').like(values['buyer_name']))
            & (C('buyer_mobile').like(values['buyer_mobile']))
        )\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .add_fields({
                "organization_name": self.ao.array_elemat("$org_info.name", 0),
            })\
            .sort({
                'create_date': -1
            })\
            .project({
                '_id': 0,
                'id': 1,
                'order_id': 1,
                'order_number': 1,
                'fun_address': 1,
                'address': 1,
                'buyer_name': 1,
                'create_user_name': 1,
                'create_user_mobile': 1,
                'charitable_commission_ratio_amount': 1,
                'transaction_rate_amount': 1,
                'commission_ratio_amount': 1,
                'total_amount': 1,
                'postage_amount': 1,
                'product_amount': 1,
                'final_amount': 1,
                'pay_status': 1,
                'pay_date': 1,
                'order_status': 1,
                'create_date': 1,
                'modify_date': 1,
                'organization_id': 1,
                'organization_name': 1,
                'remark': 1,
                'line_info.id': 1,
                'line_info.product_id': 1,
                'line_info.logistics_id': 1,
                'line_info.logistics_number': 1,
                'line_info.number': 1,
                'line_info.unit_price': 1,
                'line_info.product_price': 1,
                'line_info.total_price': 1,
                'line_info.final_price': 1,
                'line_info.postage_price': 1,
                'line_info.commission_ratio': 1,
                'line_info.commission_ratio_amount': 1,
                'line_info.transaction_rate': 1,
                'line_info.transaction_rate_amount': 1,
                'line_info.charitable_commission_ratio': 1,
                'line_info.charitable_commission_ratio_amount': 1,
                'line_info.organization_id': 1,
                'line_info.remark': 1,
                'line_info.delivery_date': 1,
                'line_info.modify_date': 1,
                'line_info.create_date': 1,
            })
        res = self.page_query(_filter, 'PT_Order', page, count)

        if len(res['result']) > 0:
            product_ids = []
            product_obj = {}
            logistics_codes = []
            logistics_obj = {}
            order_status_codes = []
            order_status_obj = {}
            for item in res['result']:
                # 补全订单状态
                if item['order_status'] not in order_status_codes:
                    order_status_codes.append(item['order_status'])
                # 循环订单的子订单
                if item.get('line_info') and type(item.get('line_info')) == list:
                    for itm in item.get('line_info'):
                        if itm['product_id'] not in product_ids:
                            product_ids.append(itm['product_id'])
                        if itm['logistics_id'] not in logistics_codes:
                            logistics_codes.append(itm['logistics_id'])

            # 补全产品
            if len(product_ids) > 0:

                _filter_product = MongoBillFilter()
                _filter_product.match_bill(
                    (C('id').inner(product_ids))
                )\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'title': 1,
                        'content': 1,
                        'image_urls': 1,
                        'postage_type': 1,
                    })
                res_product = self.query(_filter_product, 'PT_Product')

                if len(res_product) > 0:
                    for item in res_product:
                        product_obj[item['id']] = item

            # 补全快递
            if len(logistics_codes) > 0:

                _filter_logistics = MongoBillFilter()
                _filter_logistics.match_bill(
                    (C('code').inner(logistics_codes))
                    & (C('parent_code') == 'LOGISTICS_TYPE')
                )\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'code': 1,
                        'name': 1,
                    })
                res_logistics = self.query(
                    _filter_logistics, 'PT_Data_Dictionary_Detail')

                if len(res_logistics) > 0:
                    logistics_obj = {}
                    for item in res_logistics:
                        logistics_obj[item['code']] = item

            # 补全订单状态
            if len(order_status_codes) > 0:

                _filter_order_status = MongoBillFilter()
                _filter_order_status.match_bill(
                    (C('code').inner(order_status_codes))
                    & (C('parent_code') == 'ORDER_STATUS')
                )\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'code': 1,
                        'name': 1,
                    })
                res_order_status = self.query(
                    _filter_order_status, 'PT_Data_Dictionary_Detail')

                if len(res_order_status) > 0:
                    order_status_obj = {}
                    for item in res_order_status:
                        order_status_obj[item['code']] = item

                for item in res['result']:
                    if item['order_status'] in order_status_obj:
                        item['order_status_name'] = order_status_obj[item['order_status']]['name']
                    if item.get('line_info') and type(item.get('line_info')) == list:
                        for itm in item.get('line_info'):
                            if itm['product_id'] in product_obj:
                                itm['product_title'] = product_obj[itm['product_id']]['title']
                                itm['product_content'] = product_obj[itm['product_id']]['content']
                                itm['product_image_urls'] = product_obj[itm['product_id']
                                                                        ]['image_urls']
                                itm['product_postage_type'] = product_obj[itm['product_id']
                                                                          ]['postage_type']
                            if itm['logistics_id'] in logistics_obj:
                                itm['logistics_name'] = logistics_obj[itm['logistics_id']]['name']

        return res

    def update_order_remark(self, datas):

        res = 'Failed'
        if 'id' not in datas or 'remark' not in datas:
            return '非法操作！'

        # 此接口只允许订单创建人修改备注
        _filter_exists = MongoBillFilter()
        _filter_exists.match_bill(
            (C('id') == datas.get('id'))
        )\
            .project({
                '_id': 0,
                'id': 1,
                'create_user_id': 1,
            })

        res_exists = self.query(_filter_exists, 'PT_Order')

        user_id = get_current_user_id(self.session)

        if len(res_exists) == 0 or res_exists[0]['create_user_id'] != user_id:
            return '非法操作'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.productOrders.value, {
                'id': datas['id'],
                'remark': datas['remark'],
                'update_by': user_id,
            }, 'PT_Order', True)
        if bill_id:
            res = 'Success'
        return res

    def update_order_status(self, datas):
        ''' 只允许订单创建人修改订单状态 '''

        res = 'Failed'
        if 'id' not in datas or 'status' not in datas:
            return '非法操作！'

        # 此接口只允许订单创建人修改状态
        _filter_exists = MongoBillFilter()
        _filter_exists.match_bill(
            (C('id') == datas.get('id'))
        )\
            .project({
                '_id': 0,
                'id': 1,
                'create_user_id': 1,
            })

        res_exists = self.query(_filter_exists, 'PT_Order')

        user_id = get_current_user_id(self.session)

        if len(res_exists) == 0 or res_exists[0]['create_user_id'] != user_id:
            return '非法操作'

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.productOrders.value, {
                'id': datas['id'],
                'order_status': datas['status'],
                'update_by': user_id,
            }, 'PT_Order', True)
        if bill_id:
            res = 'Success'
        return res

    def update_order(self, datas):
        ''' 只允许权限者修改订单信息 '''

        if 'order_status' in datas:
            # 发货
            if datas.get('order_status') == 'DELIVERED':
                return self.deliver_goods(datas)
            elif datas.get('order_status') == 'PAID':
                # 付款时间
                datas['pay_date'] = get_cur_time()

        if datas.get('pay_status') == 'REFUNDED':
            # 退款
            return self.return_money(datas)

        datas['update_by'] = get_current_user_id(self.session)

        bill_id = False
        res = 'Failed'

        if 'id' in list(datas.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.productOrders.value, datas, 'PT_Order', True)
        if bill_id:
            res = 'Success'
        return res

    def deliver_goods(self, datas):
        ''' 发货 '''

        # 先根据datas['id']更改Order_Line表，再根据Order_Line表的order_id修改订单状态

        if 'id' not in datas or datas['id'] == '' or datas['id'] == None:
            return '非法操作'

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == datas['id'])
        )\
            .project({
                '_id': 0,
            })

        res = self.query(_filter, 'PT_Order_Line')

        if len(res) == 0:
            return '找不到此订单'

        user_id = get_current_user_id(self.session)

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.productOrders.value, [{
                'id': res[0]['id'],
                'logistics_id': datas.get('logistics_id'),
                'logistics_number': datas.get('logistics_number'),
                'delivery_date': get_cur_time(),
                'update_by': user_id,
            }, {
                'id': res[0]['order_id'],
                'order_status': 'DELIVERED',
                'update_by': user_id,
            }], ['PT_Order_Line', 'PT_Order'], True)

        if bill_id:
            res = 'Success'
        return res

    def return_money(self, datas):
        ''' 退款 '''

        # 先根据datas['id']更改Order_Line表，再根据Order_Line表的order_id修改订单状态

        if 'id' not in datas or datas['id'] == '' or datas['id'] == None:
            return '非法操作'

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == datas['id'])
        )\
            .project({
                '_id': 0,
            })

        res = self.query(_filter, 'PT_Order_Line')

        if len(res) == 0:
            return '找不到此订单'

        user_id = get_current_user_id(self.session)

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.productOrders.value, [{
                'id': res[0]['id'],
                'cancel_return_price': datas.get('final_price') + datas.get('postage_price'),
                'refund_date': get_cur_time(),
                'update_by': user_id,
            }, {
                'id': res[0]['order_id'],
                'order_status': 'REFUNDED',
                'update_by': user_id,
            }], ['PT_Order_Line', 'PT_Order'], True)

        if bill_id:
            res = 'Success'
        return res

    def get_app_blocks_list(self, org_list, condition):

        keys = ['id', 'status']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('status') == True)
            & (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Data_Dictionary_Detail', 'type', 'code', 'dictionary_info')\
            .add_fields({
                "type_name": self.ao.array_elemat("$dictionary_info.name", 0),
            })\
            .lookup_bill('PT_Data_Dictionary_Detail', 'type', 'code', 'dictionary_info')\
            .sort({'sort': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'status': 1,
                'type': 1,
                'type_name': 1,
                'show_type': 1,
                'sort': 1,
                'desc': 1,
                'create_date': 1,
                'modify_date': 1,
            })

        res = self.query(_filter, 'PT_Recommend_Module')

        if len(res) > 0:
            list_ids = []

            if 'recommend_list_condition' in condition.keys():
                recommend_list_condition = condition['recommend_list_condition']
            else:
                recommend_list_condition = {}

            for i, x in enumerate(res):
                res[i]['details'] = []
                res[i]['all'] = True
                list_ids.append(res[i]['id'])

            if len(list_ids) > 0:
                list_res = self.get_recommend_list(
                    N(), {'recommend_module_ids': list_ids, **recommend_list_condition}, None, None)

                if len(list_res['result']) > 0:
                    # print(list_res['result'])
                    # list_res['result'].sort(key=lambda x:x["price"])
                    list_obj = {}
                    for item in list_res['result']:
                        if item['recommend_module_id'] not in list_obj:
                            list_obj[item['recommend_module_id']] = []
                        list_obj[item['recommend_module_id']].append(item)
                    # 根据模块ID赋予给原数据
                    for i, x in enumerate(res):
                        if res[i]['id'] in list_obj:
                            res[i]['details'] = list_obj[res[i]['id']]

        # res.append(list_res)
        return res

    def get_blocks_list(self, org_list, condition, page, count):
        '''获取板块信息'''
        keys = ['id', 'status']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('status') == values['status'])
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Data_Dictionary_Detail', 'type', 'code', 'dictionary_info')\
            .add_fields({
                "type_name": self.ao.array_elemat("$dictionary_info.name", 0),
            })\
            .sort({'sort': -1, 'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'product_id': 1,
                'name': 1,
                'status': 1,
                'type': 1,
                'type_name': 1,
                'show_type': 1,
                'sort': 1,
                'desc': 1,
                'create_date': 1,
                'modify_date': 1,
            })

        res = self.page_query(_filter, 'PT_Recommend_Module', page, count)
        return res

    def update_block(self, data):
        res = 'Failed'

        data['update_by'] = get_current_user_id(self.session)

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.blocks.value, data, 'PT_Recommend_Module', True)
        else:
            data['create_by'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.blocks.value, data, 'PT_Recommend_Module')
        if bill_id:
            res = 'Success'
        return res

    def delete_block(self, condition):
        ''' 删除板块 '''
        for i in condition['ids']:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.blocks.value, {'id': i}, 'PT_Recommend_Module')
        if bill_id:
            result = 'Success'
        return result

    def get_recommend_list(self, org_list, condition, page, count):
        '''首页获取板块推荐信息，区分商品/服务类型/居家服务'''
        keys = ['id', 'recommend_module_ids', 'recommend_module_id',
                'recommend_status', 'product_status', 'active_status', 'is_show_app']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('recommend_module_id') == values['recommend_module_id'])
            & (C('status') == values['recommend_status'])
            & (C('recommend_module_id').inner(values['recommend_module_ids']))
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Recommend_Module', 'recommend_module_id', 'id', 'recommend_module_info')\
            .add_fields({
                "module_name": self.ao.array_elemat("$recommend_module_info.name", 0),
            })\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .add_fields({
                "organization_name": self.ao.array_elemat("$org_info.name", 0),
            })

        if 'sort' in condition.keys():
            if condition.get('sort') == 'sales_num':
                _filter.sort({
                    'sales_num': -1,
                    'sort': -1,
                    'modify_date': -1,
                })
            elif condition.get('sort') == 'price':
                price = -1
                if condition.get('selected_price_direction') == 'reverse':
                    price = 1
                _filter.sort({
                    'price': price,
                    'sort': -1,
                    'modify_date': -1,
                })
            else:
                _filter.sort({
                    'sort': -1,
                    'modify_date': -1,
                })
        _filter.project({
            '_id': 0,
            'id': 1,
            'recommend_module_id': 1,
            'module_name': 1,
            'product_id': 1,
            'title': 1,
            'status': 1,
            'image_url': 1,
            'sort': 1,
            'type': 1,
            'organization_name': 1,
            'create_date': 1,
            'modify_date': 1,
        })

        res = self.page_query(_filter, 'PT_Recommend_List', page, count)

        # 需要对推荐明细做商品/服务类型/居家服务的区分
        if len(res['result']) > 0:
            type_ids = []
            product_ids = []
            service_ids = []

            type_obj = {}
            product_obj = {}
            service_obj = {}

            for item in res['result']:
                if item['type'] == '服务':
                    type_ids.append(item['product_id'])
                elif item['type'] == '商品':
                    product_ids.append(item['product_id'])
                elif item['type'] == '居家服务':
                    service_ids.append(item['product_id'])

            # 获取商品
            _filter_product = MongoBillFilter()
            _filter_product.match_bill(
                (C('id').inner(product_ids))
                & (C('status') == values['product_status'])
                & (C('active_status') == values['active_status'])
            )\
                .lookup_bill('PT_Order_Line', 'id', 'product_id', 'order_line_info')\
                .add_fields({
                    'product_id': "$id",
                    'product_name': "$title",
                    "sales_num": self.ao.size("$order_line_info"),
                })

            _filter_product.sort({'modify_date': -1})\
                .project({
                    '_id': 0,
                    'product_id': 1,
                    'product_name': 1,
                    'sales_num': 1,
                    'price': 1,
                })
            res_product = self.query(_filter_product, 'PT_Product')

            # 获取服务类型
            _filter_type = MongoBillFilter()
            _filter_type.match_bill(
                (C('id').inner(type_ids))
                & (C('is_show_app') == values['is_show_app'])
            )\
                .add_fields({
                    'product_id': "$id",
                    'product_name': "$name",
                    'sales_num': 0,
                    'price': 0,
                })

            _filter_type.sort({'modify_date': -1})\
                .project({
                    '_id': 0,
                    'product_id': 1,
                    'product_name': 1,
                    'sales_num': 1,
                    'price': 1,
                })
            res_type = self.query(_filter_type, 'PT_Service_Type')

            # 获取居家服务
            _filter_service = MongoBillFilter()
            _filter_service.match_bill(
                (C('id').inner(service_ids))
                & (C('state') == '启用')
                & (C('status') == '通过')
            )\
                .add_fields({
                    'product_id': "$id",
                    'product_name': "$name",
                    'sales_num': 0,
                    'price': "$service_package_price",
                })

            _filter_service.sort({'modify_date': -1})\
                .project({
                    '_id': 0,
                    'product_id': 1,
                    'product_name': 1,
                    'sales_num': 1,
                    'price': 1,
                })
            res_service = self.query(_filter_service, 'PT_Service_Product')

            if len(res_product) > 0:
                for item in res_product:
                    product_obj[item['product_id']] = item
            if len(res_type) > 0:
                for item in res_type:
                    type_obj[item['product_id']] = item
            if len(res_service) > 0:
                for item in res_service:
                    service_obj[item['product_id']] = item
            for item in res['result']:
                if item['type'] == '服务':
                    if item['product_id'] in type_obj:
                        item['product_name'] = type_obj[item['product_id']
                                                        ]['product_name']
                        item['price'] = type_obj[item['product_id']]['price']
                        item['sales_num'] = type_obj[item['product_id']]['sales_num']
                    else:
                        del(item)
                elif item['type'] == '商品':
                    if item['product_id'] in product_obj:
                        item['product_name'] = product_obj[item['product_id']
                                                           ]['product_name']
                        item['price'] = product_obj[item['product_id']]['price']
                        item['sales_num'] = product_obj[item['product_id']
                                                        ]['sales_num']
                    else:
                        del(item)
                elif item['type'] == '居家服务':
                    if item['product_id'] in service_obj:
                        item['product_name'] = service_obj[item['product_id']
                                                           ]['product_name']
                        item['price'] = service_obj[item['product_id']]['price']
                        item['sales_num'] = service_obj[item['product_id']
                                                        ]['sales_num']
                    else:
                        del(item)

        # 排序
        if len(res['result']) > 0:
            if condition.get('sort') == 'sales_num':
                # 按销量排序
                res['result'].sort(key=lambda x: x["sales_num"], reverse=True)
            elif condition.get('sort') == 'price':
                # 按价格排序
                reverse = False
                if 'selected_price_direction' in condition and condition.get('selected_price_direction') == 'unreverse':
                    reverse = True
                res['result'].sort(key=lambda x: x["price"], reverse=reverse)
            else:
                # 按默认排序
                res['result'].sort(
                    key=lambda x: x['sort'], reverse=True)
        return res

    def get_recommend_list_old(self, org_list, condition, page, count):
        '''获取板块推荐信息'''
        keys = ['id', 'recommend_module_ids', 'recommend_module_id',
                'recommend_status', 'product_status', 'active_status', 'is_show_app']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('recommend_module_id') == values['recommend_module_id'])
            & (C('status') == values['recommend_status'])
            & (C('recommend_module_id').inner(values['recommend_module_ids']))
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_Recommend_Module', 'recommend_module_id', 'id', 'recommend_module_info')\
            .add_fields({
                "module_name": self.ao.array_elemat("$recommend_module_info.name", 0),
            })\
            .lookup_bill('PT_Product', 'product_id', 'id', 'procuct_info')\
            .add_fields({
                "product_name": self.ao.array_elemat("$procuct_info.title", 0),
                "price": self.ao.array_elemat("$procuct_info.price", 0),
            })\
            .lookup_bill('PT_Service_Type', 'service_id', 'id', 'service_info')\
            .add_fields({
                "service_name": self.ao.array_elemat("$service_info.name", 0),
            })\
            .match_bill(
                ((C('procuct_info.status') == values['product_status'])
                 & (C('procuct_info.active_status') == values['active_status']))
                | (C('service_info.is_show_app') == values['is_show_app'])
        )\
            .lookup_bill('PT_Order_Line', 'product_id', 'product_id', 'order_line_info')\
            .add_fields({
                "sales_num": self.ao.size("$order_line_info"),
            })\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .add_fields({
                "organization_name": self.ao.array_elemat("$org_info.name", 0),
            })

        if 'sort' in condition.keys():
            if condition.get('sort') == 'sales_num':
                _filter.sort({
                    'sales_num': -1,
                    'sort': -1,
                    'modify_date': -1,
                })
            elif condition.get('sort') == 'price':
                price = -1
                if condition.get('selected_price_direction') == 'reverse':
                    price = 1
                _filter.sort({
                    'price': price,
                    'sort': -1,
                    'modify_date': -1,
                })
            else:
                _filter.sort({
                    'sort': -1,
                    'modify_date': -1,
                })
        _filter.project({
            '_id': 0,
            'id': 1,
            'recommend_module_id': 1,
            'module_name': 1,
            'product_id': 1,
            'product_name': 1,
            'service_id': 1,
            'service_name': 1,
            'title': 1,
            'status': 1,
            'sales_num': 1,
            'image_url': 1,
            'sort': 1,
            'price': 1,
            'type': 1,
            'show_type': 1,
            'organization_name': 1,
            'create_date': 1,
            'modify_date': 1,
        })

        res = self.page_query(_filter, 'PT_Recommend_List', page, count)
        return res

    def update_recommend(self, data):
        res = 'Failed'

        data['update_by'] = get_current_user_id(self.session)

        if 'id' in list(data.keys()) and data['id'] == 'parent':
            del(data['id'])

        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.blocks.value, data, 'PT_Recommend_List', True)
        else:
            data['create_by'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.blocks.value, data, 'PT_Recommend_List')
        if bill_id:
            res = 'Success'
        return res

    def delete_recommend(self, condition):
        ''' 删除板块推荐 '''
        result = 'Failed'
        for i in condition['ids']:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.blocks.value, {'id': i}, 'PT_Recommend_List')
        if bill_id:
            result = 'Success'
        return result

    def get_service_type_list(self, org_list, condition, page, count):
        ''' 获取14类 '''
        keys = ['id', 'parent_id']

        if 'parent_id' not in condition:
            condition['parent_id'] = None

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('is_show_app') == True)
            & (C('parent_id') == values['parent_id'])
        )

        if 'all' in condition:
            _filter.lookup_bill('PT_User', 'organization_id', 'id', 'organization_info')\
                .add_fields({
                    "organization_name": self.ao.array_elemat("$organization_info.name", 0),
                    "organization_picture": self.ao.array_elemat("$organization_info.organization_info.picture_list", 0),
                })

        _filter.sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'logo_image': 1,
                'service_des': 1,
                'organization_name': 1,
                'organization_picture': 1,
            })

        res = self.page_query(_filter, 'PT_Service_Type', page, count)

        # 如果
        if 'parent_id' in condition:

            _filter_parent = MongoBillFilter()
            _filter_parent.match_bill(
                (C('id') == values['parent_id'])
                & (C('is_show_app') == True)
            )

            _filter_parent.sort({'modify_date': -1})\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'logo_image': 1,
                    'service_des': 1,
                })

            res_parent = self.query(_filter_parent, 'PT_Service_Type')

            if len(res_parent) > 0:
                res['parent_info'] = res_parent[0]

        return res

    def get_service_product_list(self, org_list, condition, page, count):
        ''' 获取服务产品 '''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('state') == '启用')
            & (C('status') == '通过')
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list))
        )\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'picture_collection': 1,
            })

        res = self.page_query(_filter, 'PT_Service_Product', page, count)
        return res

    def get_consignee_address_detail_app(self, org_list, condition, page, count):
        '''查询收货人地址'''
        keys = ['id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
        )\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'mobile': 1,
                'fun_address': 1,
                'address': 1,
                'is_default': 1
            })

        res = self.page_query(_filter, 'PT_Ship_Address', page, count)
        return res

    def get_default_consignee_data(self):
        '''获取默认收货人地址信息'''
        # 获取当前登录人Id
        user_id = get_current_user_id(self.session)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('user_id') == user_id)
            & (C('is_default') == True)
        )\
            .sort({
                'modify_date': -1,
            })\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'address': 1,
                'mobile': 1,
                'fun_address': 1,
            })
        res = self.page_query(_filter, 'PT_Ship_Address', 1, 1)
        return res

    def save_consignee_address_app(self, data):
        '''编辑收货人地址'''
        res = 'Fail'
        # 获取当前登录人Id
        user_id = get_current_user_id(self.session)
        data['user_id'] = user_id
        operationData = []
        if 'id' in data.keys():
            # 编辑
            operationType = OperationType.update.value
            operationData.append(data)
            # 如果设置为默认地址，这把当前人员其他的地址改为非默认
            if 'is_default' in data and data['is_default']:
                # 查询其余所有地址
                _filter = MongoBillFilter()
                _filter.match_bill(
                    (C('user_id') == user_id)
                    & (C('id') != data['id'])
                    & (C('is_default') == True)
                )\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'is_default': 1
                    })
                res_address = self.query(_filter, 'PT_Ship_Address')
                if len(res_address) > 0:
                    for address in res_address:
                        operationData.append({
                            'id': address['id'],
                            'is_default': False
                        })
        else:
            # 新增
            operationType = OperationType.add.value
            operationData = data
        bill_id = self.bill_manage_service.add_bill(
            operationType, TypeId.shipAddress.value, operationData, 'PT_Ship_Address')
        if bill_id:
            res = 'Success'
        return res

    def get_capital_detail_list(self, org_list, condition, page, count):
        '''平台资金明细'''
        return {
            'result': [
                {
                    'id': '1',
                    'status': '通过',
                    'service_provider_name': '深圳小米科技有限公司',
                    'settled': 10000000,
                    'to_be_settled': 10000000,
                    'entry': 10000000,
                    'out_of_account': 10000000,
                    'surplus': 10000000,
                },
                {
                    'id': '2',
                    'status': '有误',
                    'service_provider_name': '深圳小米科技有限公司',
                    'settled': 10000000,
                    'to_be_settled': 10000000,
                    'entry': 9000000,
                    'out_of_account': 89000000,
                    'surplus': 123000000,
                },
            ],
            'total': 2
        }

    def get_separate_accounts_sh_list(self, org_list, condition, page, count):
        ''' 分账审核列表 '''

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('order_status') == 'COMPLETED')
            & (C('settlement_status') == '未结算')
        )\
            .lookup_bill('PT_Order_Line', 'id', 'order_id', 'line_info')\
            .sort({
                'create_date': -1
            })\
            .project({
                '_id': 0,
                'line_info.organization_id': 1,
                'line_info.final_price': 1,
                'line_info.create_date': 1,
            })
        res = self.page_query(_filter, 'PT_Order', None, None)

        return_data = {
            'result': [],
            'total': 0,
        }

        if len(res['result']) > 0:
            data_obj = {}
            for item in res['result']:
                for itm in item['line_info']:
                    if itm['organization_id'] not in data_obj:
                        data_obj[itm['organization_id']] = {}

                    order_date = str(itm.get('create_date').year) + '-' + str(
                        itm.get('create_date').month) + '-' + str(itm.get('create_date').day)
                    if order_date not in data_obj[itm['organization_id']]:
                        data_obj[itm['organization_id']][order_date] = []
                    data_obj[itm['organization_id']][order_date].append(itm)

            for item in data_obj:
                for itm in data_obj[item]:
                    data_info = {
                        'id': item,
                        'date': itm,
                        'income_money': 0,
                        'income_order_count': 0,
                        'output_money': 0,
                        'output_order_count': 0,
                    }
                    for im in data_obj[item][itm]:
                        data_info['income_money'] += im['final_price']
                        data_info['income_order_count'] += len(
                            data_obj[item][itm])
                    return_data['result'].append(data_info)

        return_data['total'] = len(return_data['result'])

        return return_data

    def accounts_sh(self, ids):

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id').inner(ids))
            & (C('order_stuats') == 'COMPLETED')
        )\
            .lookup_bill('PT_Order_Line', 'id', 'order_id', 'line_info')\
            .sort({
                'create_date': -1
            })\
            .project({
                '_id': 0,
            })
        res = self.query(_filter, 'PT_Order')
        return 'Success'

    def get_products_list_by_organization(self, org_list, condition, page, count):
        ''' 获取机构下的服务和商品 '''

        keys = ['id', 'title', 'sort']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('personnel_type') == '2')
            # & (C('organization_info.personnel_category') == '服务商')
            & (C('name').like(values['title']))
        )

        if 'sort' in condition.keys() and condition['sort'] != '':
            if condition.get('sort') == 'sales_num':
                _filter.sort({
                    'sales_num': -1,
                    'sort': -1,
                    'modify_date': -1,
                })
            elif condition.get('sort') == 'price':
                price = -1
                if condition.get('selected_price_direction') == 'reverse':
                    price = 1
                _filter.sort({
                    'price': price,
                    'sort': -1,
                    'modify_date': -1,
                })
        else:
            _filter.sort({
                'sort': -1,
                'modify_date': -1,
            })
        _filter.project({
            '_id': 0,
            'id': 1,
            'name': 1,
        })

        res = self.query(_filter, 'PT_User')

        result_obj = {
            'products': [],
            'services': [],
        }

        if len(res) > 0:
            org_ids = []
            for item in res:
                org_ids.append(item['id'])

            result_obj['services'] = self.get_services_list(
                org_ids, {'status': '通过', 'state': '启用', 'sort': condition.get('sort'), 'selected_price_direction': condition.get('selected_price_direction')}, None, None)

            result_obj['products'] = self.get_goods_list(
                org_ids, {'status': 'PASSED', 'active_status': '上架', 'sort': condition.get('sort'), 'selected_price_direction': condition.get('selected_price_direction')}, None, None)

        return result_obj

    # 入参实例
    # {
    #     product_data: [
    #         {
    #             'product_id': '123',
    #             'number': 1,
    #         }
    #     ],
    #     consignee_data: {
    #         'name': [],
    #         'mobile': [],
    #         'address': [],
    #         'fun_address': [],
    #     },
    #     remark: '',
    # }

    def create_order(self, data):
        ''' 创建订单 '''

        res = 'Failed'

        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return '请先登录'

        if type(data.get('product_data')) != list:
            return '非法操作'

        ids = []
        new_data = []

        for item in data.get('product_data'):
            if not item.get('product_id') or type(item.get('number')) != int:
                return '非法操作'
            if item.get('product_id') not in ids:
                ids.append(item.get('product_id'))
                new_data.append(item)
            else:
                # 如果存在，就在已有的加数量
                for itm in new_data:
                    if new_data[itm]['product_id'] == item.get('product_id'):
                        new_data[itm]['number'] += item.get('number')

        # 获取产品，审核通过并且是上架状态的
        _filter_product = MongoBillFilter()
        _filter_product.match_bill(
            (C('id').inner(ids))
            & (C('status') == 'PASSED')
            & (C('active_status') == '上架')
        )\
            .project({
                '_id': 0,
            })

        res_product = self.query(_filter_product, 'PT_Product')

        if len(res_product) == 0 or len(res_product) != len(ids):
            return '商品参数有误，请刷新重试'

        # 转换为以产品id为键值的字典
        res_product_dict = {}
        for item in res_product:
            res_product_dict[item.get('id')] = item

        # 获取商品库存
        _filter_stock = MongoBillFilter()
        _filter_stock.match_bill(
            (C('product_id').inner(ids))
        )\
            .project({
                '_id': 0,
                'id': 1,
                'product_id': 1,
                'stock_num': 1,
            })
        res_stock = self.query(_filter_stock, 'PT_Product_Stock')

        # 转换为以产品id为键值的字典
        res_stock_dict = {}
        for item in res_stock:
            res_stock_dict[item.get('product_id')] = item

        # 检查库存
        for item in data.get('product_data'):
            if res_stock_dict[item['product_id']]['stock_num'] == 0 or item['number'] > res_stock_dict[item['product_id']]['stock_num']:
                return '商品库存不足'

        # 总订单的数据
        order_data = {
            'type': '商品',
            'create_user_id': user_id,
            'create_user_name': data.get('consignee_data').get('name'),
            'create_user_mobile': data.get('consignee_data').get('mobile'),
            'order_number': self.get_bill_code('订单'),
            'fun_address': data.get('consignee_data').get('fun_address'),
            'address': data.get('consignee_data').get('address'),
            # 'commission_ratio': 0,
            'commission_ratio_amount': 0,
            'charitable_commission_ratio': 0,
            'charitable_commission_ratio_amount': 0,
            'transaction_rate': 0,
            'transaction_rate_amount': 0,
            'total_amount': 0,
            'postage_amount': 0,
            'cancle_return_amount': 0,
            'product_amount': 0,
            'final_amount': 0,
            'order_status': 'WAIT_PAY',
            'settlement_status': '未结算',
            'pay_status': 'WAIT_PAY',
            'from_type': 'APP',
            'remark': data.get('remark'),
            'create_by': user_id,
            'update_by': user_id,
        }

        order_data = get_info(order_data, self.session)

        order_line_data = []
        order_stock_data = []

        for item in data.get('product_data'):
            product_info = res_product_dict[item.get('product_id')]
            if not product_info.get('commission_ratio'):
                # if not product_info.get('commission_ratio') or not product_info.get('charitable_commission_ratio') or not product_info.get('transaction_rate'):
                return '商品尚未设置费率，请联系管理员'

            # 产品总金额 = 传进来的数量 x 产品单价
            product_price = item.get('number') * product_info.get('price')

            # 根据费率折算价格
            # 佣金费率
            commission_ratio = product_info.get('commission_ratio')
            if type(product_info.get('commission_ratio')) == str:
                commission_ratio = float(commission_ratio)
            commission_ratio = commission_ratio / 100

            # 慈善费率
            # charitable_commission_ratio = product_info.get(
            #     'charitable_commission_ratio')
            # if type(product_info.get('charitable_commission_ratio')) == str:
            #     charitable_commission_ratio = float(
            #         charitable_commission_ratio)
            # charitable_commission_ratio = charitable_commission_ratio / 100

            # 交易费率
            # transaction_rate = product_info.get('transaction_rate')
            # if type(product_info.get('transaction_rate')) == str:
            #     transaction_rate = float(transaction_rate)
            # transaction_rate = transaction_rate / 100

            # 佣金费，四舍五入保留两位
            commission_ratio_amount = round(
                product_price * commission_ratio, 2)
            # 慈善费，四舍五入保留两位
            # charitable_commission_ratio_amount = round(
            #     final_price * charitable_commission_ratio, 2)
            # 交易费，四舍五入保留两位
            # transaction_rate_amount = round(
            #     final_price * transaction_rate, 2)
            # 暂时设置为0
            charitable_commission_ratio_amount = 0
            transaction_rate_amount = 0

            # 服务商实收 = 订单总价 - 上面三个费加起来
            # total_price = final_price - commission_ratio_amount - \
            #     charitable_commission_ratio_amount - transaction_rate_amount
            # 订单金额
            # 订单最终价格 = 服务商实收 + 运费
            final_price = product_price + product_info.get('postage_price')
            # 服务商实收 = 订单最终价格 - 佣金
            total_price = final_price - commission_ratio_amount

            # 子订单的数据
            order_line_data.append({
                'order_id': order_data.get('id'),
                'service_provider_id': product_info.get('organization_id'),
                'order_number': order_data.get('order_number'),
                'product_id': product_info.get('id'),
                'product_title': product_info.get('title'),
                'product_category_id': product_info.get('product_category_id'),
                'product_postage_type': product_info.get('postage_type'),
                'number': item.get('number'),
                'unit_price': product_info.get('price'),
                'commission_ratio': product_info.get('commission_ratio'),
                'commission_ratio_amount': commission_ratio_amount,
                'charitable_commission_ratio': product_info.get('charitable_commission_ratio'),
                'charitable_commission_ratio_amount': charitable_commission_ratio_amount,
                'transaction_rate': product_info.get('transaction_rate'),
                'transaction_rate_amount': transaction_rate_amount,
                'postage_price': product_info.get('postage_price'),
                'cancel_return_price': 0,
                'product_price': product_price,
                'total_price': total_price,
                'final_price': final_price,
                'logistics_id': '',
                'logistics_number': '',
                'delivery_date': '',
                'arrival_date': '',
                'cancel_date': '',
                'cancel_by_id': '',
                'is_balance': '否',
                'is_day_review': '否',
                'remark': data.get('remark'),
            })

            # 库存表的数据
            order_stock_data.append({
                'id': res_stock_dict[item['product_id']]['id'],
                'stock_num': res_stock_dict[item['product_id']]['stock_num'] - item['number'],
            })

            # 总订单数据叠加
            order_data['commission_ratio_amount'] += commission_ratio_amount
            order_data['charitable_commission_ratio_amount'] += charitable_commission_ratio_amount
            order_data['transaction_rate_amount'] += transaction_rate_amount
            order_data['product_amount'] += product_price
            order_data['total_amount'] += total_price
            order_data['final_amount'] += final_price
            order_data['postage_amount'] += product_info.get('postage_price')

        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.products.value, [[order_data], order_line_data, order_stock_data], ['PT_Order', 'PT_Order_Line', 'PT_Product_Stock'])
        if bill_id:
            res = 'Success'
        return res
