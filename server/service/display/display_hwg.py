from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.personnel_organizational import UserType


class DisplayHwgService(MongoService):
    def __init__(self, db_addr, db_port, db_name,  db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session

    def ___list_transform(self, res):
        '''将提取list类型的数据
        eg: {x:['a']} >> {x:a}
        '''
        if res:
            for result in res:
                for key, value in result.items():
                    if type(value) == list:
                        if len(value) == 1:
                            result[key] = value[0]
                        elif len(value) == 0:
                            result[key] = ''
        return res

    def cop_detail_task_card(self, condition, page, count):
        # 长者信息
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Service_Record', 'task_service_record_id', 'id', 'service_record')\
            .lookup_bill('PT_Service_Order', 'service_record.order_id', 'id', 'service_order')\
            .lookup_bill('PT_User', 'service_order.purchaser_id', 'id', 'older')\
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'older.id', 'elder', 'behavioral_record')\
            .match_bill(C('id') == values['id'])\
            .add_fields({'y1': '$older.personnel_info.photo'})\
            .add_fields({'y2': '$older.personnel_info.name'})\
            .add_fields({'y3': '$older.personnel_info.contacts_telephone'})\
            .add_fields({'y4': '$behavioral_record.total_score'})\
            .add_fields({'y5': '$task_location'})\
            .add_fields({'y6': '无'})\
            .add_fields({'y7': '无'})\
            .add_fields({'y8': '无'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                'y5': 1,
                'y6': 1,
                'y7': 1,
                'y8': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_Task')
        return res

    def cop_detail_service_personal_card(self, condition, page, count):
        # 服务提供人员信息
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Service_Record', 'task_service_record_id', 'id', 'service_record')\
            .lookup_bill('PT_User', 'service_record.servicer_id', 'id', 'hander_user')\
            .match_bill(C('id') == values['id'])\
            .add_fields({'y1': '$hander_user.personnel_info.photo'})\
            .add_fields({'y2': '$hander_user.personnel_info.name'})\
            .add_fields({'y3': '$hander_user.personnel_info.contacts_telephone'})\
            .add_fields({'y4': '$hander_user.qualification_info.qualification_level'})\
            .add_fields({'y5': '$task_location'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                'y5': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_Task')
        return res

    def cop_detail_service_provider_card(self, condition, page, count):
        # 服务提供商信息
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Service_Record', 'task_service_record_id', 'id', 'service_record')\
            .lookup_bill('PT_Service_Product', 'service_record.serivce_product_id', 'id', 'service_product')\
            .lookup_bill('PT_User', 'service_product.org_id', 'id', 'hander_user')\
            .match_bill(C('id') == values['id'])\
            .add_fields({'y1': '$hander_user.organization_info.photo'})\
            .add_fields({'y2': '$hander_user.organization_info.name'})\
            .add_fields({'y3': '$hander_user.organization_info.telephone'})\
            .add_fields({'y4': '$hander_user.qualification_info.qualification_level'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_Task')
        return res

    def cop_detail_service_card(self, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Service_Record', 'task_service_record_id', 'id', 'record')\
            .lookup_bill('PT_Service_Order', 'record.order_id', 'id', 'order')\
            .lookup_bill('PT_Service_Product', 'record.serivce_product_id', 'id', 'product')\
            .lookup_bill('PT_User', 'product.org_id', 'id', 'service_provider')\
            .lookup_bill('PT_User', 'order.purchaser_id', 'id', 'service_accepter')\
            .lookup_bill('PT_User', 'record.servicer_id', 'id', 'servicer')\
            .match_bill(C('id') == values['id'])\
            .add_fields({'y1': '$id'})\
            .add_fields({'y2': '$product.name'})\
            .add_fields({'y3': '$task_address'})\
            .add_fields({'y4': '$record.start_date'})\
            .add_fields({'y5': '$service_provider.name'})\
            .add_fields({'y6': '$service_accepter.name'})\
            .add_fields({'y7': '$servicer.name'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                'y5': 1,
                'y6': 1,
                'y7': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_Task')
        return res

    def eld_detail_task_card(self, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .inner_join_bill('PT_Service_Order', 'id', 'purchaser_id', 'order')\
            .inner_join_bill('PT_Service_Record', 'order.id', 'order_id', 'record')\
            .inner_join_bill('PT_Task', 'record.id', 'task_service_record_id', 'task')\
            .inner_join_bill('PT_Service_Product', 'record.service_product_id', 'id', 'product')\
            .inner_join_bill('PT_User', 'order.service_provider_id', 'id', 'service_provider')\
            .inner_join_bill('PT_User', 'record.servicer_id', 'id', 'servicer')\
            .add_fields({'y1': '$task.task_code'})\
            .add_fields({'y2': '$product.name'})\
            .add_fields({'y3': '$task.task_location'})\
            .add_fields({'y4': '$task.task_process_record.date'})\
            .add_fields({'y5': '$service_provider.name'})\
            .add_fields({'y6': '$name'})\
            .add_fields({'y7': '$servicer.name'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                'y5': 1,
                'y6': 1,
                'y7': 1,
                '_id': 0,
            }).sort({'y4': -1}).limit(1)
        res = self.query(_filter, 'PT_User')
        res_list = self.___list_transform(res)
        return res_list

    def eld_detail_service_record_top10_list(self, condition, page, count):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .inner_join_bill('PT_Service_Order', 'id', 'purchaser_id', 'order')\
            .inner_join_bill('PT_Service_Record', 'order.id',  'order_id', 'record')\
            .inner_join_bill('PT_Service_Item', 'record.item_id', 'id', 'item')\
            .inner_join_bill('PT_Service_Product', 'item.id', 'service_item_id', 'service_product')\
            .add_fields({'y1': '$record.start_date'})\
            .add_fields({'y2': '$service_product.name'})\
            .project({
                'y1': 1,
                'y2': 1,
                '_id': 0,
            }).limit(10)
        res = self.query(_filter, 'PT_User')
      # print(_filter.filter_objects)
        return res

    def eld_detail_family_list(self, condition, page, count):
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User_Relationship', 'id', 'main_relation_people', 'relationship')\
            .lookup_bill('PT_User_Relationship_Type', 'relationship.relationship_type_id', 'id', 're_type')\
            .lookup_bill('PT_User', 'relationship.subordinate_relation_people', 'id', 'cong_user')\
            .match_bill(C('re_type.name') == '子女')\
            .add_fields({'y1': '$cong_user.name'})\
            .add_fields({'y2': '$re_type.type'})\
            .add_fields({'y3': '$cong_user.personnel_info.telephone'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_User')
        return res

    def eld_detail_elder(self, condition, page, count):
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Bed', 'id', 'residents_id', 'bed')\
            .lookup_bill('PT_Hotel_Zone', 'bed.area_id', 'id', 'hotel_zone')\
            .lookup_bill('PT_Organization', 'hotel_zone.organization_id', 'id', 'organization')\
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'id', 'elder', 'behavior')\
            .add_fields({'y1': '$personnel_info.photo'})\
            .add_fields({'y2': '$address'})\
            .add_fields({'y3': '$telephone'})\
            .add_fields({'y4': '$organization.name'})\
            .add_fields({'y5': '$bed.name'})\
            .add_fields({'y6': '$behavior.total_score'})\
            .project({
                'y1': 1,
                'y2': 1,
                'y3': 1,
                'y4': 1,
                'y5': 1,
                'y6': 1,
                '_id': 0,
            })
        res = self.query(_filter, 'PT_User')
        return res
