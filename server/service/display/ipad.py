'''
版权：Copyright (c) 2019 China

创建日期：Saturday September 7th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Saturday, 7th September 2019 7:29:36 pm
修改者: ymq(ymq) - <<email>>

说明
 1、
'''
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.personnel_organizational import UserType
from server.pao_python.pao.service.security.security_utility import SecurityConstant, get_current_account_id


class IpadService(MongoService):
    ''' 平板控制服务 '''

    business_area_collection = 'PT_Business_Area'  # 业务区划表名
    current_year = 2019

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session

    def get_welfare_center_list(self):
        '''养老机构'''
        _filter = self.__org_area()
        _filter.match(C('user_info.personnel_type') == UserType.Organizational)\
               .match(C('user_info.organization_info.personnel_category') == '福利院')\
               .project({'_id': 0, 'id': '$user_info.id', 'name': '$user_info.name'})
        res = self.query(_filter, self.business_area_collection)
        return res

    def get_community_home_list(self):
        '''社区幸福院'''
        _filter = self.__org_area()
        _filter.match(C('user_info.personnel_type') == UserType.Organizational)\
               .match(C('user_info.organization_info.personnel_category') == '幸福院')\
               .project({'_id': 0, 'id': '$user_info.id', 'name': '$user_info.name'})
        res = self.query(_filter, self.business_area_collection)
        return res

    def get_service_provider_list(self):
        '''服务商'''
        _filter = self.__org_area()
        _filter.match(C('user_info.personnel_type') == UserType.Organizational)\
               .match(C('user_info.organization_info.personnel_category') == '服务商')\
               .project({'_id': 0, 'id': '$user_info.id', 'name': '$user_info.name'})
        res = self.query(_filter, self.business_area_collection)
        return res

    def get_service_personal_list(self):
        '''服务人员'''
        _filter = self.__org_area()
        _filter.match(C('user_info.personnel_type') == UserType.Organizational)\
               .inner_join_bill('PT_User_Relationship', 'user_info.id', 'main_relation_people', 'relation_info')\
               .inner_join_bill('PT_User_Relationship_Type', 'relation_info.relationship_type_id', 'id', 'relation_type_info')\
               .match(C('relation_type_info.name') == '雇佣')\
               .inner_join_bill('PT_User', 'relation_info.subordinate_relation_people', 'id', 'service_info')\
               .project({'_id': 0, 'id': '$service_info.id', 'name': '$service_info.name', 'phone': '$service_info.telephone', 'source': '$user_info.name'})
        res = self.query(_filter, self.business_area_collection)
        return res

    def get_elder_list(self):
        '''长者'''
        _filter = self.__org_area()
        _filter.match((C('user_info.personnel_type') == UserType.Personnel)
                      & (C('user_info.personnel_info.dead') == 0)
                      & (C('user_info.personnel_info.personnel_category') == '长者'))\
               .add_fields({'year': self.ao.year('$user_info.personnel_info.date_birth')})\
               .add_fields({'age': ((F('year')-self.current_year)*(-1)).f})\
               .match(C('age') > 60)\
               .project({'_id': 0, 'id': '$user_info.id', 'name': '$user_info.name', 'phone': '$user_info.telephone', 'id_card': '$user_info.personnel_info.id_card'})
        res = self.query(_filter, self.business_area_collection)
        return res

    def get_comprehensive_list(self):
        '''综合--任务的明细'''
        pass

    ####################### 公共部分 ##############################

    def set_session_area(self):
        '''设置session的区域id'''
        account_id = get_current_account_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill(C('account_id') == account_id)\
               .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'div_info')\
               .graph_lookup('PT_Administration_Division', '$div_info.parent_id', 'parent_id', 'id', 'all_admin')\
               .unwind('all_admin')\
               .inner_join_bill('PT_Business_Area', 'all_admin.id', 'admin_id', 'area_info')
        res = self.query(_filter, 'PT_User')
        self.session[SecurityConstant.area] = res[0]['area_info']['id']
        return res[0]['area_info']['id']

    def get_current_user_area_id(self):
        '''通过session获取当前登录用户的区域id'''
        if SecurityConstant.area in list(self.session.keys()):
            return self.session[SecurityConstant.area]
        else:
            res = self.set_session_area()
            return res

    def __org_area(self):
        '''找到某业务区域内的所有user信息'''
        division_id = self.get_current_user_area_id()
        _filter = MongoBillFilter()
        _filter.match(C('id') == division_id)\
               .inner_join('PT_Administration_Division', 'admin_id', 'id', 'admin_info')\
               .graph_lookup('PT_Administration_Division', '$admin_info.parent_id', 'id', 'parent_id', 'all_admin')\
               .project({'admin_id': '$all_admin.id'})\
               .unwind('admin_id')\
               .inner_join_bill('PT_User', 'admin_id', 'admin_area_id', 'user_info')
        return _filter
