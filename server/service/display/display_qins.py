
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F,as_date
from ...service.mongo_bill_service import MongoBillFilter
from ..buss_pub.personnel_organizational import UserType
from ...service.common import GetInformationByIdCard
from enum import Enum
import datetime
from server.pao_python.pao.service.security.security_utility import SecurityConstant,get_current_account_id
from .display_ye import DisplayService
import json
class QinsDisplayService(MongoService):

    bus_area = '南海'  # 业务区域为南海
    business_area_collection = 'PT_Business_Area'  # 业务区划表名
    current_year = 2019  # 当前年份

    '''大屏服务'''
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd,inital_password,session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.new_service = DisplayService(
            db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)

###############################################################################################################################################################
    # 呼叫中心公共查询接口
    def call_center_query(self):
        # 呼叫中心测试代码
        caller_collection = 'PT_Call_Record'
        _filter_caller = MongoBillFilter()
        # TODO: 1、号码不唯一，怎么处理 2、长者信息里没有位置的话，需要一个默认弹框位置
        _filter_caller.match((C('call_status') == 1))\
            .lookup('PT_User', 'phone', 'telephone', "user_info")\
            .add_fields({
                'name':'$user_info.name',
                'sex':'$user_info.personnel_info.sex',
                'x':"$user_info.personnel_info.lon",
                'y':'$user_info.personnel_info.lat',
                'address':'$user_info.address'
                # 'phone':'phone'
            })\
            .unwind('name')\
            .unwind('x')\
            .unwind('y')\
            .unwind('sex')\
            .unwind('address')\
            .project({'user_info._id':0, '_id':0})
        res_caller = self.query(_filter_caller, caller_collection)

         # 计算年龄
        for result in res_caller:
            # TODO: userinfo是不是只有一个数组？？？
            id_card = result['user_info'][0].get('id_card')
            if id_card:
                if len(id_card) == 18:
                    get_infomation_by_idcard = GetInformationByIdCard(id_card)
                    result['age'] = get_infomation_by_idcard.get_age() 
                    
        return res_caller

    def new_aswhole_sum_statistical(self,condition,page,count):
        '''总数统计'''
        res = [
            [
                {
                    "name": "养老院",
                    "value": 19
                },
                {
                    "name": "床位总数",
                    "value": 4012
                },{
                    "name": "长者总数",
                    "value": 2849
                }
            ],
            [
                {
                    "name": "幸福院",
                    "value": 193
                },
                {
                    "name": "活动总数",
                    "value": 7133
                },{
                    "name": "长者总数",
                    "value": 2849
                }
            ],
            [
                {
                    "name": "居家服务商",
                    "value": 20
                },
                {
                    "name": "服务人员",
                    "value": 343
                },{
                    "name": "服务人次",
                    "value": 579439
                }
            ]
        ]
        return res
    def new_aswhole_sum_statistical_wf(self,condition,page,count):
        '''总数统计-养老院'''
        res = [
                {
                    "name": "养老院",
                    "value": 19
                },
                {
                    "name": "床位总数",
                    "value": 4012
                },{
                    "name": "长者总数",
                    "value": 2849
                }
            ]
        return res

    def new_aswhole_sum_statistical_hp(self,condition,page,count):
        '''总数统计-幸福院'''
        res = [
                {
                    "name": "幸福院",
                    "value": 193
                },
                {
                    "name": "活动总数",
                    "value": 7133
                },{
                    "name": "长者总数",
                    "value": 2849
                }
            ]
        return res
    
    def new_aswhole_sum_statistical_pv(self,condition,page,count):
        '''总数统计-居家服务商'''
        res = [
                {
                    "name": "居家服务商",
                    "value": 20
                },
                {
                    "name": "服务人员",
                    "value": 343
                },{
                    "name": "服务人次",
                    "value": 579439
                }
            ]
        return res

    def new_aswhole_all_elder_sum(self,condition,page,count):
        '''全区长者总数 仪表盘'''
        res = [{
            'name':'全区长者总数',
            'value':100,
        }]
        return res

    def new_aswhole_all_elder_sum_bar(self,condition,page,count):
        '''全区长者总数 柱状图'''
        res = [{
            'y':270135,
            'x':'全区长者总数'
        },{
            'y':0,
            'x':'登记在案长者数'
        }
        ]
        return res
    
    def new_org_detail_origin_quantity(self,condition,page,count):
        '''各镇街长者数量'''
        res = [
            {
                'x':'里水镇',
                'y':29804
            },{
                'x':'大沥镇',
                'y':51632
            },{
                'x':'西樵镇',
                'y':31598
            },{
                'x':'九江镇',
                'y':22216
            },{
                'x':'狮山镇',
                'y':55837
            },{
                'x':'桂城街道',
                'y':46031
            },
        ]
        return res

    def new_eld_statistics_subsidy_elder_quantity(self,condition,page,count):
        '''14类长者政策扶持统计'''
        res = [
            {
                'x':'A1',
                'y':77
            },{
                'x':'A2',
                'y':250
            },{
                'x':'A3',
                'y':116
            },{
                'x':'A4',
                'y':11
            },{
                'x':'A5',
                'y':3
            },{
                'x':'A6',
                'y':19
            },{
                'x':'A7',
                'y':53
            },{
                'x':'B8',
                'y':60
            },{
                'x':'B9',
                'y':27
            },{
                'x':'B10',
                'y':4
            },{
                'x':'B11',
                'y':0
            },{
                'x':'C12',
                'y':2076
            },{
                'x':'C13',
                'y':88
            },{
                'x':'C14',
                'y':0
            },
        ]
        return res
    
    def new_aswhole_sum_statistical_street(self,condition,page,count):
        '''各镇街机构数量统计'''
        res = [
            {
                'x':'里水镇',
                '养老院':2,
                '幸福院':35,
                '服务商':0
            },{
                'x':'大沥镇',
                '养老院':3,
                '幸福院':39,
                '服务商':0
            },{
                'x':'西樵镇',
                '养老院':3,
                '幸福院':13,
                '服务商':0
            },{
                'x':'九江镇',
                '养老院':1,
                '幸福院':10,
                '服务商':0
            },{
                'x':'狮山镇',
                '养老院':8,
                '幸福院':56,
                '服务商':4
            },{
                'x':'桂城街道',
                '养老院':1,
                '幸福院':24,
                '服务商':8
            },{
                'x':'丹灶镇',
                '养老院':1,
                '幸福院':16,
                '服务商':0
            },
        ]
        return res
    
    def new_aswhole_pension_trends(self,condition,page,count):
        '''养老机构数量趋势图'''
        res = [
            {
                'x':'2017',
                'y':0
            },{
                'x':'2018',
                'y':14
            },{
                'x':'2019',
                'y':19
            },
        ]
        return res

    def new_aswhole_service_personnel_sum(self,condition,page,count):
        '''各镇街服务人员数量统计'''
        res = [
            {
                'x':'里水镇',
                'y':151
            },{
                'x':'大沥镇',
                'y':172
            },{
                'x':'西樵镇',
                'y':164
            },{
                'x':'九江镇',
                'y':103
            },{
                'x':'狮山镇',
                'y':556
            },{
                'x':'桂城街道',
                'y':145
            },{
                'x':'丹灶镇',
                'y':157
            },
        ]
        return res

    def new_eld_statistics_time_elder_quantity(self,condition,page,count):
        '''长者数量趋势图'''
        res = [
            {
                'x':'2017',
                'y':224045
            },{
                'x':'2018',
                'y':256091
            },{
                'x':'2019',
                'y':270135
            },
        ]
        return res
    def new_donation_amount(self,condition,page,count):
        '''慈善金额泡泡'''
        res=[
            {'name':'慈善金额','value':75}
        ]
        return res

    def new_subsidy_amount(self,condition,page,count):
        '''补贴金额泡泡'''
        res=[
            {'name':'补贴金额','value':75}
        ]
        return res

    def new_cop_statistics_donation_income_quantity_line(self,condition,page,count):
        '''慈善补贴情况 曲线'''
        #假数据
        res = [
            {
                'x':'2013',
                'y1':7,
                'y2':6
            },{
                'x':'2014',
                'y1':8,
                'y2':7
            },{
                'x':'2015',
                'y1':10,
                'y2':6
            },{
                'x':'2016',
                'y1':12,
                'y2':8
            },{
                'x':'2017',
                'y1':19,
                'y2':13
            },{
                'x':'2018',
                'y1':24,
                'y2':20
            },{
                'x':'2019',
                'y1':30,
                'y2':25
            },
        ]
        return res

    def new_aswhole_call_center_bubble(self,condition,page,count):
        '''呼叫中心 xiao泡泡 '''
        res = [
            {
                'name':'呼叫热线量',
                'value':485
            },
            {
                'name':'呼入数量',
                'value':24370
            },{
                'name':'呼出数量',
                'value':15468
            },{
                'name':'普通呼叫',
                'value': '215'
            },{
                'name':'报警呼叫',
                'value': '41'
            }
        ]
        return res

    def new_aswhole_call_center_line(self,condition,page,count):
        '''呼叫中心 曲线'''
        # 假数据
        res = [
            {
                'x': '00:00',
                'y': 206
            },
            {
                'x': '01:00',
                'y': 234
            },
            {
                'x': '02:00',
                'y': 281
            },
            {
                'x': '03:00',
                'y': 253
            },
            {
                'x': '04:00',
                'y': 320
            },
            {
                'x': '05:00',
                'y': 336
            },
            {
                'x': '06:00',
                'y': 315
            },
            {
                'x': '07:00',
                'y': 348
            },
            {
                'x': '08:00',
                'y': 320
            },
            {
                'x': '09:00',
                'y': 308
            },
            {
                'x': '10:00',
                'y': 298
            },
            {
                'x': '11:00',
                'y': 306
            },
            {
                'x': '12:00',
                'y': 326
            },
            {
                'x': '13:00',
                'y': 346
            },
            {
                'x': '14:00',
                'y': 316
            },
            {
                'x': '15:00',
                'y': 336
            },
            {
                'x': '16:00',
                'y': 326
            },
            {
                'x': '17:00',
                'y': 320
            },
            {
                'x': '18:00',
                'y': 256
            },
            {
                'x': '19:00',
                'y': 286
            },
            {
                'x': '20:00',
                'y': 234
            },
            {
                'x': '21:00',
                'y': 206
            },
            {
                'x': '22:00',
                'y': 196
            },
            {
                'x': '23:00',
                'y': 186
            }
        ]
        return res
        
    def new_aswhole_call_center_big_bubble_left(self,condition,page,count):
        '''呼叫中心 大泡泡 左'''
        res = [
            {
                'name':'未接听率',
                'value':9
            }
        ]
        return res
    
    def new_aswhole_call_center_big_bubble_right(self,condition,page,count):
        '''呼叫中心 大泡泡 右'''
        res = [{
                'name':'满意率',
                'value':95
            }
        ]
        return res
    
    def new_aswhole_map(self,condition,page,count):
        '''地图'''
        res  =[
            
        ]
        return res
    
################################################第二版#####################
    def new_sum_statistical_bubble(self,condition,page,count):
        '''养老院总数统计--泡泡'''
        if condition.get('type') == '幸福院':
            return [{
                'name': "活动数量",
                'value': "7133"
            }, {
                'name': "活动人次",
                'value': "60620"
            }, {
                'name': "幸福院总数",
                'value': "193"
            }, {
                'name': "服务人员数量",
                'value': "475"
            },{
                'name': '服务长者数量',
                'value': '56465'
            }]
        elif condition.get('type') == '服务商':
            return [{
                'name': "服务商总数",
                'value': "20"
            }, {
                'name': "服务人员数量",
                'value': "343"
            }, {
                'name': '服务长者人次',
                'value': '579439'
            }]
        elif condition.get('id') == 'dad64fa2-d08d-11e9-8760-144f8aec0be5' and condition.get('type') == '幸福院':
            return [{
                'name': "活动数量",
                'value': "7133"
            }, {
                'name': "活动人次",
                'value': "60620"
            }, {
                'name': "幸福院总数",
                'value': "193"
            }, {
                'name': "服务人员数量",
                'value': "475"
            },{
                'name': '服务长者数量',
                'value': '56465'
            }]
        elif condition.get('id') == 'dab0b8f8-d08d-11e9-a8b4-144f8aec0be5' and condition.get('type') == '养老院':
            res = [
            {
                'name':'员工数量',
                'value':22
            },{
                'name':'床位总量',
                'value':116
            },{
                'name':'入住人数',
                'value':98
            },{
                'name':'预约数量',
                'value':2
            },{
                'name':'养老院',
                'value':19
            }
            ]
            return res
        elif condition.get('type') == '养老院':
            res = [
                {
                    'name':'员工数量',
                    'value':1438
                },{
                    'name':'入住人数',
                    'value':2849
                },{
                    'name':'养老院总数',
                    'value':19
                },{
                    'name':'床位总量',
                    'value':4012
                },{
                    'name':'预约数量',
                    'value':0
                },
            ]
            return res
        
    def new_sum_statistical_bubble_welfare_single(self,condition,page,count):
        '''养老院-单个--泡泡'''
        res = [
        {
            'name':'员工数量',
            'value':22
        },{
            'name':'床位总量',
            'value':116
        },{
            'name':'入住人数',
            'value':98
        },{
            'name':'预约数量',
            'value':2
        },{
            'name':'养老院',
            'value':19
        }
        ]
        return res
    
    def new_sum_statistical_bubble_happiness_single(self,condition,page,count):
        '''幸福院-单个--泡泡'''
        return [{
                'name': "活动数量",
                'value': "7133"
            }, {
                'name': "活动人次",
                'value': "60620"
            }, {
                'name': "幸福院总数",
                'value': "193"
            }, {
                'name': "服务人员数量",
                'value': "475"
            },{
                'name': '服务长者数量',
                'value': '56465'
            }]
    
    def new_sum_statistical_chart(self,condition,page,count):
        '''养老院总数统计--星级饼图'''
        if condition.get('type') == '服务商':
            return [{
                "x":"无评分",
                'y': 20
            }]
        elif condition.get('type') == '养老院':
            res = [
                {
                    'x':'五星',
                    'y':1,
                },{
                    'x':'四星',
                    'y':0,
                },{
                    'x':'三星',
                    'y':0,
                },{
                    'x':'二星',
                    'y':0,
                },{
                    'x':'一星',
                    'y':18,
                },
            ]
            return res
    
    def new_sum_statistical_chart_welfare_single(self,condition,page,count):
        '''养老院总数统计--星级饼图'''
        res = [
            {
                'x':'五星',
                'y':1,
            },{
                'x':'四星',
                'y':0,
            },{
                'x':'三星',
                'y':0,
            },{
                'x':'二星',
                'y':0,
            },{
                'x':'一星',
                'y':18,
            },
        ]
        return res
    
    def new_institutions_occupancy_ranking_bed(self,condition,page,count):
        '''机构入住人数排名--床位'''
        res = [
            {
                'y1':'南海区社会福利中心',
                'y2':1011
            },{
                'y1':'大沥敬老院',
                'y2':464
            },{
                'y1':'文华颐养公寓(狮山)',
                'y2':400
            },{
                'y1':'桂城敬老院',
                'y2':306
            },{
                'y1':'佛山市南海区西樵百西仁光颐老康复保健中心',
                'y2':207
            },{
                'y1':'佛山市南海区九江桃苑颐养院',
                'y2':188
            },{
                'y1':'西樵福利中心',
                'y2':166
            },{
                'y1':'里水镇颐年院（北院）',
                'y2':147
            },{
                'y1':'狮山镇松岗敬老院',
                'y2':136
            },{
                'y1':'佛山市南海区狮山镇官窑敬老院',
                'y2':134
            },{
                'y1':'黄岐沙溪信孚托老院',
                'y2':116
            },{
                'y1':'里水镇颐年院（南院）',
                'y2':108
            },{
                'y1':'佛山市南海区狮山镇罗村敬老院',
                'y2':108
            },{
                'y1':'九如城(狮山)康复中心',
                'y2':91
            },{
                'y1':'狮山镇小塘敬老院',
                'y2':88
            },{
                'y1':'黄岐梁泳钊颐养院',
                'y2':87
            },{
                'y1':'芭特顺健康老年公寓',
                'y2':70
            },{
                'y1':'丹灶敬老院',
                'y2':66
            },
        ]
        return res
    
    def new_institutions_occupancy_ranking_eld(self,condition,page,count):
        '''机构入住人数排名--长者数量'''
        res = [
            {
                'y1':'南海区社会福利中心',
                'y2':933
            },{
                'y1':'桂城敬老院',
                'y2':269
            },{
                'y1':'大沥敬老院',
                'y2':227
            },{
                'y1':'佛山市南海区西樵百西仁光颐老康复保健中心',
                'y2':133
            },{
                'y1':'西樵福利中心',
                'y2':132
            },{
                'y1':'文华颐养公寓(狮山)',
                'y2':131
            },{
                'y1':'佛山市南海区狮山镇官窑敬老院',
                'y2':109
            },{
                'y1':'狮山镇松岗敬老院',
                'y2':109
            },{
                'y1':'里水镇颐年院（南院）',
                'y2':98
            },{
                'y1':'佛山市南海区九江桃苑颐养院',
                'y2':98
            },{
                'y1':'佛山市南海区狮山镇罗村敬老院',
                'y2':95
            },{
                'y1':'黄岐梁泳钊颐养院',
                'y2':84
            },{
                'y1':'里水镇颐年院（北院',
                'y2':83
            },{
                'y1':'芭特顺健康老年公寓',
                'y2':65
            },{
                'y1':'狮山镇小塘敬老院',
                'y2':55
            },{
                'y1':'丹灶敬老院',
                'y2':49
            },{
                'y1':'狮山镇敬老院',
                'y2':37
            },{
                'y1':'九如城(狮山)康复中心',
                'y2':24
            },
        ]
        return res
    
    def new_household_registration_elder_sum(self,condition,page,count):
        '''入住机构户籍长者数量'''
        res = [
            {
                'x':'南海籍',
                'y':2249
            },{
                'x':'非南海籍',
                'y':243
            }]
        return res
    
    def new_household_registration_elder_sum_welfare_single(self,condition,page,count):
        '''入住机构户籍长者数量'''
        res = [
            {
                'x':'南海籍',
                'y':2249
            },{
                'x':'非南海籍',
                'y':243
            }]
        return res
    
    def new_org_statistics_street_quantity(self,condition,page,count):
        '''镇街养老机构数量'''
        res = [{
                'x':'里水镇',
                'y':2
            },{
                'x':'大沥镇',
                'y':3
            },{
                'x':'西樵镇',
                'y':3
            },{
                'x':'九江镇',
                'y':1
            },{
                'x':'狮山镇',
                'y':8
            },{
                'x':'桂城街道',
                'y':1
            },{
                'x':'丹灶镇',
                'y':1
            },]
        return res
    
    def new_org_statistics_time_subscribe_quantity(self,condition,page,count):
        '''养老机构预约数量走势'''
        res  =[
            {
                "x":"19-01",
                "y":14
            },{
                "x":"19-02",
                "y":18
            },{
                "x":"19-03",
                "y":21
            },{
                "x":"19-04",
                "y":25
            },{
                "x":"19-05",
                "y":26
            },{
                "x":"19-06",
                "y":23
            },{
                "x":"19-07",
                "y":18
            },{
                "x":"19-08",
                "y":14
            },{
                "x":"19-09",
                "y":10
            },
        ]
        return res

    def new_institutions_classification(self,condition,page,count):
        '''养老机构分类'''
        res = [
            {
                'x':'公办公营',
                'y':13
            },{
                'x':'公办民营',
                'y':0
            },{
                'x':'民办民营',
                'y':0
            },
        ]
        return res
    
    def new_activity_statistical(self,condition,page,count):
        '''活动列表--统计数据'''
        if condition.get('type') == '幸福院':
            return [{'y': 7133}, {'y': 60620}]
        elif condition.get('id') == 'dad64fa2-d08d-11e9-8760-144f8aec0be5' and condition.get('type') == '幸福院':
            return [{'y': 105}, {'y': 0}]
        elif condition.get('id') == 'dab0b8f8-d08d-11e9-a8b4-144f8aec0be5' and condition.get('type') == '养老院':
            res = [
            {
                'x':'活动次数',
                'y':7181
            },{
                'x':'参与人数',
                'y':60985
                },
            ]
            return res
        elif condition.get('type') == '养老院':
            res = [
                {
                    'x':'活动次数',
                    'y':7181
                },{
                    'x':'参与人数',
                    'y':60985
                },
            ]
            return res
    
    def new_activity_statistical_welfare_single(self,condition,page,count):
        '''活动列表--统计数据'''
        res = [
        {
            'x':'活动次数',
            'y':7181
        },{
            'x':'参与人数',
            'y':60985
            },
        ]
        return res
    
    def new_activity_statistical_happiness_single(self,condition,page,count):
        '''活动列表--统计数据'''
        return [{'y': 105}, {'y': 0}]
    
    def new_activity_list(self,condition,page,count):
        '''活动列表'''
        if condition.get('type') == '幸福院':
            return [{
                'x': '/build/upload/activities/act01.jpg',
                'y1': '保健班',
                'y2': '2019-09-20 09:00:00-2019-09-20 11:00:00',
                'y3': '钟边社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act02.jpg',
                'y1': '魅力中秋，相会狮岭”重大节日系列活动（入会探访）',
                'y2': '2019-09-20 09:00:00-2019-09-20 17:00:00',
                'y3': '狮岭村社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act03.jpg',
                'y1': '暖心探访',
                'y2': '2019-09-19 14:59:00-2019-09-19 16:29:00',
                'y3': '樵园社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act04.jpg',
                'y1': '联和社区幸福院长者生日会',
                'y2': '2019-09-19 14:29:00-2019-09-19 16:00:00',
                'y3': '联和社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act05.jpg',
                'y1': '2019水头社区幸福院第三季度“党群共筑，温情生日会',
                'y2': '2019-09-19 14:29:00-2019-09-19 17:00:00',
                'y3': '水头社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '恒常跳舞',
                'y2': '2019-09-19 09:30:00-2019-09-19 11:30:00',
                'y3': '五星社区幸福院',
                'y4': '已举行'
            }]
        elif condition.get('id') == 'dad64fa2-d08d-11e9-8760-144f8aec0be5' and condition.get('type') == '幸福院':
            return [{
                'x': '/build/upload/activities/act06.jpg',
                'y1': '联和社区幸福院长者生日会',
                'y2': '2019-09-18 09:00:00-2019-09-18 11:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '社团会议',
                'y2': '2019-09-12 15:00:00-2019-09-12 17:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '开心农场座谈会',
                'y2': '2019-07-16 13:00:00-2019-07-16 16:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }]
        elif condition.get('id') == 'dab0b8f8-d08d-11e9-a8b4-144f8aec0be5' and condition.get('type') == '养老院':
            res = [
                # 假数据
                {
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者学堂',
                    'y2':'2017-9-27 16:07:00~2018-9-27 16:07:00',
                    'y3':'丹灶敬老院',
                    'y4':'已举行',
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者迎新活动',
                    'y2':'2017-8-24 09:00:00~2017-8-24 10:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者积分活动',
                    'y2':'2017-8-30 09:00:00~2017-8-30 10:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'员工趣味活动',
                    'y2':'2017-8-31 12:00:00~2017-8-31 13:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'西樵民政中秋送温暖，爱心慰问活动',
                    'y2':'2017-10-1 09:39:00~2017-10-2 09:40:00',
                    'y3':'芭特顺健康老年公寓的客房',
                    'y4':'已举行',
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'庆国庆，贺中秋',
                    'y2':'2017-10-1 09:51:00~2017-10-2 12:00:00',
                    'y3':'芭特顺健康老年公寓的5楼大堂',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'西樵奇卡国际早教中心送爱心活动',
                    'y2':'2018-4-15 14:00:00~2018-4-15 16:00:00',
                    'y3':'佛山市南海区西樵百西仁光颐老康复保健中心',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'广东狮子会扬帆服务队慰问活动',
                    'y2':'2019-5-19 09:40:00~2019-5-19 12:00:00',
                    'y3':'佛山市南海区西樵百西科技工业园内',
                    'y4':'已举行'
                }
            ]
            return res
        elif condition.get('type') == '养老院':
            res = [
                {
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者学堂',
                    'y2':'2017-9-27 16:07:00~2018-9-27 16:07:00',
                    'y3':'丹灶敬老院',
                    'y4':'已举行',
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者迎新活动',
                    'y2':'2017-8-24 09:00:00~2017-8-24 10:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'长者积分活动',
                    'y2':'2017-8-30 09:00:00~2017-8-30 10:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'员工趣味活动',
                    'y2':'2017-8-31 12:00:00~2017-8-31 13:00:00',
                    'y3':'佛山市南海区狮山镇罗村敬老院',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'西樵民政中秋送温暖，爱心慰问活动',
                    'y2':'2017-10-1 09:39:00~2017-10-2 09:40:00',
                    'y3':'芭特顺健康老年公寓的客房',
                    'y4':'已举行',
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'庆国庆，贺中秋',
                    'y2':'2017-10-1 09:51:00~2017-10-2 12:00:00',
                    'y3':'芭特顺健康老年公寓的5楼大堂',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'西樵奇卡国际早教中心送爱心活动',
                    'y2':'2018-4-15 14:00:00~2018-4-15 16:00:00',
                    'y3':'佛山市南海区西樵百西仁光颐老康复保健中心',
                    'y4':'已举行'
                },{
                    'x':'/build/upload/123456789/clip_image002.jpg',
                    'y1':'广东狮子会扬帆服务队慰问活动',
                    'y2':'2019-5-19 09:40:00~2019-5-19 12:00:00',
                    'y3':'佛山市南海区西樵百西科技工业园内',
                    'y4':'已举行'
                }
            ]
            return res
    
    def new_activity_list_welfare_single(self,condition,page,count):
        '''活动列表'''
        res = [
            # 假数据
            {
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'长者学堂',
                'y2':'2017-9-27 16:07:00~2018-9-27 16:07:00',
                'y3':'丹灶敬老院',
                'y4':'已举行',
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'长者迎新活动',
                'y2':'2017-8-24 09:00:00~2017-8-24 10:00:00',
                'y3':'佛山市南海区狮山镇罗村敬老院',
                'y4':'已举行'
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'长者积分活动',
                'y2':'2017-8-30 09:00:00~2017-8-30 10:00:00',
                'y3':'佛山市南海区狮山镇罗村敬老院',
                'y4':'已举行'
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'员工趣味活动',
                'y2':'2017-8-31 12:00:00~2017-8-31 13:00:00',
                'y3':'佛山市南海区狮山镇罗村敬老院',
                'y4':'已举行'
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'西樵民政中秋送温暖，爱心慰问活动',
                'y2':'2017-10-1 09:39:00~2017-10-2 09:40:00',
                'y3':'芭特顺健康老年公寓的客房',
                'y4':'已举行',
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'庆国庆，贺中秋',
                'y2':'2017-10-1 09:51:00~2017-10-2 12:00:00',
                'y3':'芭特顺健康老年公寓的5楼大堂',
                'y4':'已举行'
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'西樵奇卡国际早教中心送爱心活动',
                'y2':'2018-4-15 14:00:00~2018-4-15 16:00:00',
                'y3':'佛山市南海区西樵百西仁光颐老康复保健中心',
                'y4':'已举行'
            },{
                'x':'/build/upload/123456789/clip_image002.jpg',
                'y1':'广东狮子会扬帆服务队慰问活动',
                'y2':'2019-5-19 09:40:00~2019-5-19 12:00:00',
                'y3':'佛山市南海区西樵百西科技工业园内',
                'y4':'已举行'
            }
        ]
        return res
    
    def new_activity_list_happiness_single(self,condition,page,count):
        '''活动列表'''
        return [{
                'x': '/build/upload/activities/act06.jpg',
                'y1': '联和社区幸福院长者生日会',
                'y2': '2019-09-18 09:00:00-2019-09-18 11:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '社团会议',
                'y2': '2019-09-12 15:00:00-2019-09-12 17:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '开心农场座谈会',
                'y2': '2019-07-16 13:00:00-2019-07-16 16:00:00',
                'y3': '罗湖社区幸福院',
                'y4': '已举行'
            }]

    def new_activity_trend_month(self,condition,page,count):
        '''每月活动数量趋势'''
        res = [
            {
                'x':'2017-8',
                'y':3
            },{
                'x':'2017-9',
                'y':1
            },{
                'x':'2017-10',
                'y':2
            },{
                'x':'2018-4',
                'y':1
            },{
                'x':'2019-5',
                'y':1
            },
        ]
        return res
    
    def new_video_monitoring(self,condition,page,count):
        '''视频播放'''
        res = [

        ]
        return res
    
    def new_org_detail_time_subscribe_quantity(self,condition,page,count):
        '''养老机构预约数量走势 单个'''
        res  =[
            {
                "x":"19-01",
                "y":14
            },{
                "x":"19-02",
                "y":18
            },{
                "x":"19-03",
                "y":21
            },{
                "x":"19-04",
                "y":25
            },{
                "x":"19-05",
                "y":26
            },{
                "x":"19-06",
                "y":23
            },{
                "x":"19-07",
                "y":18
            },{
                "x":"19-08",
                "y":14
            },{
                "x":"19-09",
                "y":10
            },
        ]
        return res

    def new_org_detail_personal_list(self,condition,page,count):
        '''服务人员列表'''
        res = [
            {
                'x':'/build/upload/service_person/default.jpg',
                'y1':'夏湖兰',
                'y2':'硕士',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'何燕青',
                'y2':'硕士',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'刘小明',
                'y2':'大专',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'林惠英',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'黄玉芬',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'刘运秀',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'蔡声良',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'谢桂金',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'杨臣华',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'张利红',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'黄红端',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'覃凤相',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'黄丽坚',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'林青梅',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'李丽英',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'曾荣新',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'万兰香',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'黄丽坚',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'李冬英',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'汤明根',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'陈柏万',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'马燕怡',
                'y2':'中学',
            },{
                'x':'/build/upload/service_person/default.jpg',
                'y1':'文细女',
                'y2':'中学',
            },
        ]
        return res
    
    def new_org_detail_service_products_list(self,condition,page,count):
        '''服务产品列表'''
        res = [
            {
                'x': '/build/upload/activities/act01.jpg',
                'y1': '测试服务产品1',
                'y2': '测试服务产品1简介'
            }, {
                'x': '/build/upload/activities/act02.jpg',
                'y1': '测试服务产品2',
                'y2': '测试服务产品2简介'
            }, {
                'x': '/build/upload/activities/act03.jpg',
                'y1': '测试服务产品3',
                'y2': '测试服务产品3简介'
            }, {
                'x': '/build/upload/activities/act04.jpg',
                'y1': '测试服务产品4',
                'y2': '测试服务产品4简介'
            }, {
                'x': '/build/upload/activities/act05.jpg',
                'y1': '测试服务产品5',
                'y2': '测试服务产品5简介'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '测试服务产品6',
                'y2': '测试服务产品6简介'
            }
        ]
        return res
    
    def new_monitor_list(self,condition,page,count):
        '''监控列表'''
        res = [
            {
                'x': '',
                'y1': '监控1',
                'y2': '监控1位置'
            }, {
                'x': '',
                'y1': '监控2',
                'y2': '监控2位置'
            }, {
                'x': '',
                'y1': '监控3',
                'y2': '监控3位置'
            }, {
                'x': '',
                'y1': '监控4',
                'y2': '监控4位置'
            }, {
                'x': '',
                'y1': '监控5',
                'y2': '监控5位置'
            }, {
                'x': '',
                'y1': '监控6',
                'y2': '监控6位置'
            }
        ]
        return res
    
    def new_org_detail_service_products_list_happiness_single(self,condition,page,count):
        '''服务产品列表'''
        res = [
            {
                'x': '/build/upload/activities/act01.jpg',
                'y1': '测试服务产品1',
                'y2': '测试服务产品1简介'
            }, {
                'x': '/build/upload/activities/act02.jpg',
                'y1': '测试服务产品2',
                'y2': '测试服务产品2简介'
            }, {
                'x': '/build/upload/activities/act03.jpg',
                'y1': '测试服务产品3',
                'y2': '测试服务产品3简介'
            }, {
                'x': '/build/upload/activities/act04.jpg',
                'y1': '测试服务产品4',
                'y2': '测试服务产品4简介'
            }, {
                'x': '/build/upload/activities/act05.jpg',
                'y1': '测试服务产品5',
                'y2': '测试服务产品5简介'
            }, {
                'x': '/build/upload/activities/act06.jpg',
                'y1': '测试服务产品6',
                'y2': '测试服务产品6简介'
            }
        ]
        return res

    def new_org_detail_picture_list(self,condition,page,count):
        '''机构图片轮播'''
        res = [
            '/build/upload/1010101010/clip_image002.jpg',
            '/build/upload/1010101010/clip_image004.jpg',
            '/build/upload/1010101010/clip_image006.jpg',
            '/build/upload/1010101010/clip_image008.jpg',
        ]
        return res
    
    def new_org_detail_welfare_centre(self, condition, page, count):
        '''机构介绍'''
        res = [
            {
                'y1':'1星',
                'y2':'公立',
                'y3':'养老院',
                'y4':'2002.8',
                'y5':'0757-85918272',
                'y6': '广东省佛山市南海区大沥镇大沥管理处曹边沙溪村沙溪公园内',
                'y7': '活动室、棋牌室',
                'x': '''沙溪托老院位于黄岐沙溪村沙溪公园内，占地面积约10000平方米。建筑面积1600平方；院里环境优美，有2000多平方的莲花池塘湖水碧波荡漾，湖面小亭玉立湖中连接九曲长廊；院内绿树成荫，阳光充足，十分宁静，是理想的养老之地。
			
			我院每年的节假日都会开展各种活动。特别是每年的重阳节运动有各个运动项目的比赛、在比赛和决赛中充分显示了他们当年的雄风。在颁奖台上的运动员骄傲荡漾在心里，一张张布满皱纹的脸；一双双渴望人们关注的眼睛，今天终于扬眉了一次；吐气一回；他们从运动会上看到了生存的意义，看到了生命的正能量。
			
			多年来我院得到社会各界人士的关心，他们送了了温暖、送来了情。你也许会从长者们的眼神中发现，他们看重的并不是那些送到手头的水果糕点，而是那些年青人的笑脸，那些稚嫩的红领巾天真的笑脸。长者们似乎从眼前闪过的笑脸中捕捉到了自己儿孙辈的娇态和爱意。
			
			沙溪托老院在今后的工作中，希望进一步得到各级领导的支持和帮助，把我们的工作做的更好。<br/>
            <br/>
            收费标准：<br/>
			<br/>
			自理1300元<br/>
			<br/>
			半护理1900元<br/>
			<br/>
			全护理2500元<br/>
			<br/>
            <br/>
            另每个月伙食费550元
            '''
            }
        ]
        return res

    def new_org_detail_welfare_centre_room_status(self, condition, page, count):
        '''床位总览'''
        res =  [
                {
                    'index': "一栋",
                    'info': [
                        {'No': "101-1",'type': 1},
                        {'No': "101-2",'type': 1},
                        {'No': "101-3",'type': 1},
                        {'No': "101-4",'type': 1},
                        {'No': "102-1",'type': 1},
                        {'No': "102-2",'type': 1},
                        {'No': "102-3",'type': 1},
                        {'No': "102-4",'type': 1},
                        {'No': "103-1",'type': 1},
                        {'No': "103-2",'type': 1},
                        {'No': "103-3",'type': 4},
                        {'No': "104-1",'type': 1},
                        {'No': "104-2",'type': 1},
                        {'No': "104-3",'type': 1},
                        {'No': "105-1",'type': 1},
                        {'No': "105-2",'type': 1},
                        {'No': "105-3",'type': 1},
                        {'No': "106-1",'type': 1},
                        {'No': "106-2",'type': 2},
                        {'No': "106-3",'type': 1},
                        {'No': "107-1",'type': 1},
                        {'No': "107-2",'type': 1},
                        {'No': "107-3",'type': 1},
                        {'No': "108-1",'type': 1},
                        {'No': "108-2",'type': 1},
                        {'No': "108-3",'type': 1},
                        {'No': "108-4",'type': 1},
                        {'No': "109-1",'type': 1},
                        {'No': "109-2",'type': 1},
                        {'No': "109-3",'type': 1},
                        {'No': "109-4",'type': 1},
                        {'No': "110-1",'type': 1},
                        {'No': "110-2",'type': 1},
                        {'No': "110-3",'type': 1},
                        {'No': "111-1",'type': 1},
                        {'No': "111-2",'type': 1},
                        {'No': "111-3",'type': 1},
                        {'No': "112-1",'type': 1},
                        {'No': "112-2",'type': 3},
                        {'No': "112-3",'type': 1},
                        {'No': "113-1",'type': 1},
                        {'No': "113-2",'type': 1},
                        {'No': "113-3",'type': 1},
                        {'No': "113-4",'type': 1},
                        {'No': "113-5",'type': 1},
                        {'No': "115-1",'type': 1},
                        {'No': "115-2",'type': 1},
                        {'No': "115-3",'type': 1},
                        {'No': "116-1",'type': 3},
                        {'No': "116-2",'type': 1},
                        {'No': "116-3",'type': 1},
                        {'No': "+1-1",'type': 1},
                        {'No': "+1-2",'type': 1},
                        {'No': "+1-3",'type': 1},
                        {'No': "+2-1",'type': 1},
                        {'No': "+2-2",'type': 1},
                        {'No': "+2-3",'type': 1},
                        {'No': "+3-1",'type': 1},
                        {'No': "+3-2",'type': 1},
                        {'No': "+3-3",'type': 1},
                        {'No': "特护1房-1",'type': 1},
                        {'No': "特护1房-2",'type': 1},
                        {'No': "特护2房-1",'type': 1},
                        {'No': "特护2房-2",'type': 1},
                        {'No': "201-1",'type': 1},
                        {'No': "201-2",'type': 2},
                        {'No': "201-3",'type': 1},
                        {'No': "201-4",'type': 1},
                        {'No': "202-1",'type': 1},
                        {'No': "202-2",'type': 1},
                        {'No': "202-3",'type': 1},
                        {'No': "203-1",'type': 1},
                        {'No': "203-2",'type': 1},
                        {'No': "203-3",'type': 1},
                        {'No': "204-1",'type': 1},
                        {'No': "204-2",'type': 1},
                        {'No': "204-3",'type': 1},
                        {'No': "205-1",'type': 1},
                        {'No': "205-2",'type': 1},
                        {'No': "205-3",'type': 1},
                        {'No': "206-1",'type': 1},
                        {'No': "206-2",'type': 1},
                        {'No': "206-3",'type': 1},
                        {'No': "207-1",'type': 1},
                        {'No': "207-2",'type': 1},
                        {'No': "207-3",'type': 1},
                        {'No': "208-1",'type': 1},
                        {'No': "208-2",'type': 1},
                        {'No': "208-3",'type': 1},
                        {'No': "209-1",'type': 1},
                        {'No': "209-2",'type': 1},
                        {'No': "209-3",'type': 1},
                        {'No': "210-1",'type': 1},
                        {'No': "210-2",'type': 1},
                        {'No': "210-3",'type': 1},
                        {'No': "211-1",'type': 2},
                        {'No': "211-2",'type': 1},
                        {'No': "211-3",'type': 1},
                        {'No': "212-1",'type': 1},
                        {'No': "212-2",'type': 1},
                        {'No': "212-3",'type': 1},
                        {'No': "212-4",'type': 1},
                        {'No': "212-5",'type': 1},
                        {'No': "212-6",'type': 1},
                        {'No': "213-1",'type': 1},
                        {'No': "213-2",'type': 1},
                        {'No': "213-3",'type': 3},
                        {'No': "214-1",'type': 1},
                        {'No': "214-2",'type': 1},
                        {'No': "214-3",'type': 1},
                        {'No': "215-1",'type': 1},
                        {'No': "215-2",'type': 1},
                        {'No': "215-3",'type': 1},
                        {'No': "215-4",'type': 1},
                        {'No': "215-5",'type': 1},
                        {'No': "215-6",'type': 2}
                    ]
                }
            ]
        # res = self.new_service.org_detail_welfare_centre_room_status(self, condition, page, count)
        # res = [
        #     {
        #         'bed': [],
        #         'hotel_zone_type_id': 'type002',
        #         'id': '002',
        #         'zone_name': '一栋',
        #         'zone_number': '002',
        #         'down_element': [],
        #         'upper_element': [],
        #         'upper_hotel_zone_id':[],
        #         'upper_hotel_zone_name': [],
        #         'user': [],
        #         'user_tmp': []
        #     },
        #     {
        #         'bed': [],
        #         'hotel_zone_type_id': 'type003',
        #         'id': '003',
        #         'zone_name': '一楼',
        #         'zone_number': '003',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "e0a05728-cfc6-11e9-bc86-000c29acd33e",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["002"],
        #         'upper_hotel_zone_name': ["一栋"],
        #         'user': [],
        #         'user_tmp': []
        #     },
        #     {
        #         'bed': [],
        #         'hotel_zone_type_id': 'type004',
        #         'id': '004',
        #         'zone_name': '二楼',
        #         'zone_number': '004',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "e0a05728-cfc6-11e9-bc86-000c29acd33e",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["002"],
        #         'upper_hotel_zone_name': ["一栋"],
        #         'user': [],
        #         'user_tmp': []
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "101001",
        #                 "id": "1011",
        #                 "name": "1",
        #                 "residents_id": "user10101"
        #             },
        #             {
        #                 "bed_code": "101002",
        #                 "id": "1012",
        #                 "name": "2",
        #                 "residents_id": "user10102"
        #             },
        #             {
        #                 "bed_code": "101003",
        #                 "id": "1013",
        #                 "name": "3",
        #                 "residents_id": "user10103"
        #             },
        #             {
        #                 "bed_code": "101004",
        #                 "id": "1014",
        #                 "name": "4",
        #                 "residents_id": "user10104"
        #             },
        #         ],
        #         'hotel_zone_type_id': 'type101',
        #         'id': '101',
        #         'zone_name': '101',
        #         'zone_number': '101',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10101',
        #                 "name": "杜月好",
        #                 "personnel_info": {
        #                     "name": "杜月好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10102',
        #                 "name": "卢顺金",
        #                 "personnel_info": {
        #                     "name": "卢顺金",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10103',
        #                 "name": "冼要",
        #                 "personnel_info": {
        #                     "name": "冼要",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10104',
        #                 "name": "陆妹",
        #                 "personnel_info": {
        #                     "name": "陆妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10101',
        #                 "name": "杜月好",
        #                 "personnel_info": {
        #                     "name": "杜月好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10102',
        #                 "name": "卢顺金",
        #                 "personnel_info": {
        #                     "name": "卢顺金",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10103',
        #                 "name": "冼要",
        #                 "personnel_info": {
        #                     "name": "冼要",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10104',
        #                 "name": "陆妹",
        #                 "personnel_info": {
        #                     "name": "陆妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "102001",
        #                 "id": "1021",
        #                 "name": "1",
        #                 "residents_id": "user10201"
        #             },
        #             {
        #                 "bed_code": "102002",
        #                 "id": "1022",
        #                 "name": "2",
        #                 "residents_id": "user10202"
        #             },
        #             {
        #                 "bed_code": "102003",
        #                 "id": "1023",
        #                 "name": "3",
        #                 "residents_id": "user10203"
        #             },
        #             {
        #                 "bed_code": "102004",
        #                 "id": "1024",
        #                 "name": "4",
        #                 "residents_id": "user10204"
        #             },
        #         ],
        #         'hotel_zone_type_id': 'type102',
        #         'id': '102',
        #         'zone_name': '102',
        #         'zone_number': '102',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10201',
        #                 "name": "李莲桂",
        #                 "personnel_info": {
        #                     "name": "李莲桂",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10202',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10203',
        #                 "name": "梁焕环",
        #                 "personnel_info": {
        #                     "name": "梁焕环",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10204',
        #                 "name": "黄丽珍",
        #                 "personnel_info": {
        #                     "name": "黄丽珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10201',
        #                 "name": "李莲桂",
        #                 "personnel_info": {
        #                     "name": "李莲桂",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10202',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10203',
        #                 "name": "梁焕环",
        #                 "personnel_info": {
        #                     "name": "梁焕环",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10204',
        #                 "name": "黄丽珍",
        #                 "personnel_info": {
        #                     "name": "黄丽珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "103001",
        #                 "id": "1031",
        #                 "name": "1",
        #                 "residents_id": "user10301"
        #             },
        #             {
        #                 "bed_code": "103002",
        #                 "id": "1032",
        #                 "name": "2",
        #                 "residents_id": "user10302"
        #             },
        #             {
        #                 "bed_code": "103003",
        #                 "id": "1033",
        #                 "name": "3",
        #                 "residents_id": "user10303"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type103',
        #         'id': '103',
        #         'zone_name': '103',
        #         'zone_number': '103',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10301',
        #                 "name": "陆景煊",
        #                 "personnel_info": {
        #                     "name": "陆景煊",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10302',
        #                 "name": "何国刚",
        #                 "personnel_info": {
        #                     "name": "何国刚",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10303',
        #                 "name": "梁耀新",
        #                 "personnel_info": {
        #                     "name": "梁耀新",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10301',
        #                 "name": "陆景煊",
        #                 "personnel_info": {
        #                     "name": "陆景煊",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10302',
        #                 "name": "何国刚",
        #                 "personnel_info": {
        #                     "name": "何国刚",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10303',
        #                 "name": "梁耀新",
        #                 "personnel_info": {
        #                     "name": "梁耀新",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "104001",
        #                 "id": "1041",
        #                 "name": "1",
        #                 "residents_id": "user10401"
        #             },
        #             {
        #                 "bed_code": "104002",
        #                 "id": "1042",
        #                 "name": "2",
        #                 "residents_id": "user10402"
        #             },
        #             {
        #                 "bed_code": "104003",
        #                 "id": "1043",
        #                 "name": "3",
        #                 "residents_id": "user10403"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type104',
        #         'id': '104',
        #         'zone_name': '104',
        #         'zone_number': '104',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10401',
        #                 "name": "郑铭坤",
        #                 "personnel_info": {
        #                     "name": "郑铭坤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10402',
        #                 "name": "冼裕宁",
        #                 "personnel_info": {
        #                     "name": "冼裕宁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10403',
        #                 "name": "陈锦洪",
        #                 "personnel_info": {
        #                     "name": "陈锦洪",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10401',
        #                 "name": "郑铭坤",
        #                 "personnel_info": {
        #                     "name": "郑铭坤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10402',
        #                 "name": "冼裕宁",
        #                 "personnel_info": {
        #                     "name": "冼裕宁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10403',
        #                 "name": "陈锦洪",
        #                 "personnel_info": {
        #                     "name": "陈锦洪",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "105001",
        #                 "id": "1051",
        #                 "name": "1",
        #                 "residents_id": "user10501"
        #             },
        #             {
        #                 "bed_code": "105002",
        #                 "id": "1052",
        #                 "name": "2",
        #                 "residents_id": "user10502"
        #             },
        #             {
        #                 "bed_code": "105003",
        #                 "id": "1053",
        #                 "name": "3",
        #                 "residents_id": "user10503"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type105',
        #         'id': '105',
        #         'zone_name': '105',
        #         'zone_number': '105',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10501',
        #                 "name": "梁洁芳",
        #                 "personnel_info": {
        #                     "name": "梁洁芳",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10502',
        #                 "name": "梁拾",
        #                 "personnel_info": {
        #                     "name": "梁拾",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10503',
        #                 "name": "邓润好",
        #                 "personnel_info": {
        #                     "name": "邓润好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10501',
        #                 "name": "梁洁芳",
        #                 "personnel_info": {
        #                     "name": "梁洁芳",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10502',
        #                 "name": "梁拾",
        #                 "personnel_info": {
        #                     "name": "梁拾",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10503',
        #                 "name": "邓润好",
        #                 "personnel_info": {
        #                     "name": "邓润好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "106001",
        #                 "id": "1061",
        #                 "name": "1",
        #                 "residents_id": "user10601"
        #             },
        #             {
        #                 "bed_code": "106002",
        #                 "id": "1062",
        #                 "name": "2",
        #                 "residents_id": "user10602"
        #             },
        #             {
        #                 "bed_code": "106003",
        #                 "id": "1063",
        #                 "name": "3",
        #                 "residents_id": "user10603"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type106',
        #         'id': '106',
        #         'zone_name': '106',
        #         'zone_number': '106',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10601',
        #                 "name": "曾润好",
        #                 "personnel_info": {
        #                     "name": "曾润好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10602',
        #                 "name": "徐碧杏",
        #                 "personnel_info": {
        #                     "name": "徐碧杏",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10603',
        #                 "name": "黄三女",
        #                 "personnel_info": {
        #                     "name": "黄三女",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10601',
        #                 "name": "曾润好",
        #                 "personnel_info": {
        #                     "name": "曾润好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10602',
        #                 "name": "徐碧杏",
        #                 "personnel_info": {
        #                     "name": "徐碧杏",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10603',
        #                 "name": "黄三女",
        #                 "personnel_info": {
        #                     "name": "黄三女",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "107001",
        #                 "id": "1071",
        #                 "name": "1",
        #                 "residents_id": "user10701"
        #             },
        #             {
        #                 "bed_code": "107002",
        #                 "id": "1072",
        #                 "name": "2",
        #                 "residents_id": "user10702"
        #             },
        #             {
        #                 "bed_code": "107003",
        #                 "id": "1073",
        #                 "name": "3",
        #                 "residents_id": "user10703"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type107',
        #         'id': '107',
        #         'zone_name': '107',
        #         'zone_number': '107',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10701',
        #                 "name": "邓景涛",
        #                 "personnel_info": {
        #                     "name": "邓景涛",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10702',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10703',
        #                 "name": "李国祥",
        #                 "personnel_info": {
        #                     "name": "李国祥",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10701',
        #                 "name": "邓景涛",
        #                 "personnel_info": {
        #                     "name": "邓景涛",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10702',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10703',
        #                 "name": "李国祥",
        #                 "personnel_info": {
        #                     "name": "李国祥",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "108001",
        #                 "id": "1081",
        #                 "name": "1",
        #                 "residents_id": "user10801"
        #             },
        #             {
        #                 "bed_code": "108002",
        #                 "id": "1082",
        #                 "name": "2",
        #                 "residents_id": "user10802"
        #             },
        #             {
        #                 "bed_code": "108003",
        #                 "id": "1083",
        #                 "name": "3",
        #                 "residents_id": "user10803"
        #             },
        #             {
        #                 "bed_code": "108004",
        #                 "id": "1084",
        #                 "name": "4",
        #                 "residents_id": "user10804"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type108',
        #         'id': '108',
        #         'zone_name': '108',
        #         'zone_number': '108',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10801',
        #                 "name": "李治安",
        #                 "personnel_info": {
        #                     "name": "李治安",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10802',
        #                 "name": "梁惠森",
        #                 "personnel_info": {
        #                     "name": "梁惠森",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10803',
        #                 "name": "卓成础",
        #                 "personnel_info": {
        #                     "name": "卓成础",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10804',
        #                 "name": "阮雨泉",
        #                 "personnel_info": {
        #                     "name": "阮雨泉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10801',
        #                 "name": "李治安",
        #                 "personnel_info": {
        #                     "name": "李治安",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10802',
        #                 "name": "梁惠森",
        #                 "personnel_info": {
        #                     "name": "梁惠森",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10803',
        #                 "name": "卓成础",
        #                 "personnel_info": {
        #                     "name": "卓成础",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10804',
        #                 "name": "阮雨泉",
        #                 "personnel_info": {
        #                     "name": "阮雨泉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "109001",
        #                 "id": "1091",
        #                 "name": "1",
        #                 "residents_id": "user10901"
        #             },
        #             {
        #                 "bed_code": "109002",
        #                 "id": "1092",
        #                 "name": "2",
        #                 "residents_id": "user10902"
        #             },
        #             {
        #                 "bed_code": "109003",
        #                 "id": "1093",
        #                 "name": "3",
        #                 "residents_id": "user10903"
        #             },
        #             {
        #                 "bed_code": "109004",
        #                 "id": "1094",
        #                 "name": "4",
        #                 "residents_id": "user10904"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type109',
        #         'id': '109',
        #         'zone_name': '109',
        #         'zone_number': '109',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user10901',
        #                 "name": "蔡莲好",
        #                 "personnel_info": {
        #                     "name": "蔡莲好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10902',
        #                 "name": "吴建娣",
        #                 "personnel_info": {
        #                     "name": "吴建娣",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10903',
        #                 "name": "林文香",
        #                 "personnel_info": {
        #                     "name": "林文香",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10904',
        #                 "name": "陈杏",
        #                 "personnel_info": {
        #                     "name": "陈杏",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user10901',
        #                 "name": "蔡莲好",
        #                 "personnel_info": {
        #                     "name": "蔡莲好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10902',
        #                 "name": "吴建娣",
        #                 "personnel_info": {
        #                     "name": "吴建娣",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10903',
        #                 "name": "林文香",
        #                 "personnel_info": {
        #                     "name": "林文香",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user10904',
        #                 "name": "陈杏",
        #                 "personnel_info": {
        #                     "name": "陈杏",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "110001",
        #                 "id": "1101",
        #                 "name": "1",
        #                 "residents_id": "user11001"
        #             },
        #             {
        #                 "bed_code": "110002",
        #                 "id": "1102",
        #                 "name": "2",
        #                 "residents_id": "user11002"
        #             },
        #             {
        #                 "bed_code": "110003",
        #                 "id": "1103",
        #                 "name": "3",
        #                 "residents_id": "user11003"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type110',
        #         'id': '110',
        #         'zone_name': '110',
        #         'zone_number': '110',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11001',
        #                 "name": "潘碧莲",
        #                 "personnel_info": {
        #                     "name": "潘碧莲",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11002',
        #                 "name": "叶少群",
        #                 "personnel_info": {
        #                     "name": "叶少群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11003',
        #                 "name": "许顺和",
        #                 "personnel_info": {
        #                     "name": "许顺和",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11001',
        #                 "name": "潘碧莲",
        #                 "personnel_info": {
        #                     "name": "潘碧莲",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11002',
        #                 "name": "叶少群",
        #                 "personnel_info": {
        #                     "name": "叶少群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11003',
        #                 "name": "许顺和",
        #                 "personnel_info": {
        #                     "name": "许顺和",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "111001",
        #                 "id": "1111",
        #                 "name": "1",
        #                 "residents_id": "user11101"
        #             },
        #             {
        #                 "bed_code": "111002",
        #                 "id": "1112",
        #                 "name": "2",
        #                 "residents_id": "user11102"
        #             },
        #             {
        #                 "bed_code": "111003",
        #                 "id": "1113",
        #                 "name": "3",
        #                 "residents_id": "user11103"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type111',
        #         'id': '111',
        #         'zone_name': '111',
        #         'zone_number': '111',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11101',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11102',
        #                 "name": "杜洁",
        #                 "personnel_info": {
        #                     "name": "杜洁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11103',
        #                 "name": "邹云清",
        #                 "personnel_info": {
        #                     "name": "邹云清",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11101',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11102',
        #                 "name": "杜洁",
        #                 "personnel_info": {
        #                     "name": "杜洁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11103',
        #                 "name": "邹云清",
        #                 "personnel_info": {
        #                     "name": "邹云清",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "112001",
        #                 "id": "1121",
        #                 "name": "1",
        #                 "residents_id": "user11201"
        #             },
        #             {
        #                 "bed_code": "112002",
        #                 "id": "1122",
        #                 "name": "2",
        #                 "residents_id": "user11202"
        #             },
        #             {
        #                 "bed_code": "112003",
        #                 "id": "1123",
        #                 "name": "3",
        #                 "residents_id": "user11203"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type112',
        #         'id': '112',
        #         'zone_name': '112',
        #         'zone_number': '112',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11201',
        #                 "name": "叶昌",
        #                 "personnel_info": {
        #                     "name": "叶昌",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11202',
        #                 "name": "曹成发",
        #                 "personnel_info": {
        #                     "name": "曹成发",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11203',
        #                 "name": "梁庆辉",
        #                 "personnel_info": {
        #                     "name": "梁庆辉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11201',
        #                 "name": "叶昌",
        #                 "personnel_info": {
        #                     "name": "叶昌",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11202',
        #                 "name": "曹成发",
        #                 "personnel_info": {
        #                     "name": "曹成发",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11203',
        #                 "name": "梁庆辉",
        #                 "personnel_info": {
        #                     "name": "梁庆辉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "113001",
        #                 "id": "1131",
        #                 "name": "1",
        #                 "residents_id": "user11301"
        #             },
        #             {
        #                 "bed_code": "113002",
        #                 "id": "1132",
        #                 "name": "2",
        #                 "residents_id": "user11302"
        #             },
        #             {
        #                 "bed_code": "113003",
        #                 "id": "1133",
        #                 "name": "3",
        #                 "residents_id": "user11303"
        #             },
        #             {
        #                 "bed_code": "113004",
        #                 "id": "1134",
        #                 "name": "4",
        #                 "residents_id": "user11304"
        #             },
        #             {
        #                 "bed_code": "113005",
        #                 "id": "1135",
        #                 "name": "5",
        #                 "residents_id": "user11305"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type113',
        #         'id': '113',
        #         'zone_name': '113',
        #         'zone_number': '113',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11301',
        #                 "name": "麦爱好",
        #                 "personnel_info": {
        #                     "name": "麦爱好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11302',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11303',
        #                 "name": "李妙群",
        #                 "personnel_info": {
        #                     "name": "李妙群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11304',
        #                 "name": "冯佩贤",
        #                 "personnel_info": {
        #                     "name": "冯佩贤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11305',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11301',
        #                 "name": "麦爱好",
        #                 "personnel_info": {
        #                     "name": "麦爱好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11302',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11303',
        #                 "name": "李妙群",
        #                 "personnel_info": {
        #                     "name": "李妙群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11304',
        #                 "name": "冯佩贤",
        #                 "personnel_info": {
        #                     "name": "冯佩贤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11305',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "115001",
        #                 "id": "1151",
        #                 "name": "1",
        #                 "residents_id": "user11501"
        #             },
        #             {
        #                 "bed_code": "115002",
        #                 "id": "1152",
        #                 "name": "2",
        #                 "residents_id": "user11502"
        #             },
        #             {
        #                 "bed_code": "115003",
        #                 "id": "1153",
        #                 "name": "3",
        #                 "residents_id": "user11503"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type115',
        #         'id': '115',
        #         'zone_name': '115',
        #         'zone_number': '115',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11501',
        #                 "name": "麦爱好",
        #                 "personnel_info": {
        #                     "name": "麦爱好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11502',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11503',
        #                 "name": "李妙群",
        #                 "personnel_info": {
        #                     "name": "李妙群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11501',
        #                 "name": "麦爱好",
        #                 "personnel_info": {
        #                     "name": "麦爱好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11502',
        #                 "name": "梁妹",
        #                 "personnel_info": {
        #                     "name": "梁妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11503',
        #                 "name": "李妙群",
        #                 "personnel_info": {
        #                     "name": "李妙群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "116001",
        #                 "id": "1161",
        #                 "name": "1",
        #                 "residents_id": "user11601"
        #             },
        #             {
        #                 "bed_code": "116002",
        #                 "id": "1162",
        #                 "name": "2",
        #                 "residents_id": "user11602"
        #             },
        #             {
        #                 "bed_code": "116003",
        #                 "id": "1163",
        #                 "name": "3",
        #                 "residents_id": "user11603"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type116',
        #         'id': '116',
        #         'zone_name': '116',
        #         'zone_number': '116',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user11601',
        #                 "name": "李津",
        #                 "personnel_info": {
        #                     "name": "李津",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11602',
        #                 "name": "张珂表",
        #                 "personnel_info": {
        #                     "name": "张珂表",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11603',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user11601',
        #                 "name": "李津",
        #                 "personnel_info": {
        #                     "name": "李津",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11602',
        #                 "name": "张珂表",
        #                 "personnel_info": {
        #                     "name": "张珂表",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user11603',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         'bed': [
        #             {
        #                 "bed_code": "+1001",
        #                 "id": "+11",
        #                 "name": "1",
        #                 "residents_id": "user+101"
        #             },
        #             {
        #                 "bed_code": "+1002",
        #                 "id": "+12",
        #                 "name": "2",
        #                 "residents_id": "user+102"
        #             },
        #             {
        #                 "bed_code": "+1003",
        #                 "id": "+13",
        #                 "name": "3",
        #                 "residents_id": "user+103"
        #             }
        #         ],
        #         'hotel_zone_type_id': 'type+1',
        #         'id': '+1',
        #         'zone_name': '+1',
        #         'zone_number': '+1',
        #         'down_element': [],
        #         'upper_element': [
        #             {
        #                 "hotel_zone_type_id": "type003",
        #                 "id": "003",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一楼",
        #                 "zone_number": "003"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         'upper_hotel_zone_id':["003","002"],
        #         'upper_hotel_zone_name': ["一楼","一栋"],
        #         'user': [
        #             {
        #                 "id": 'user+101',
        #                 "name": "周华",
        #                 "personnel_info": {
        #                     "name": "周华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user+102',
        #                 "name": "潘林",
        #                 "personnel_info": {
        #                     "name": "潘林",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user+103',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         'user_tmp': [
        #             {
        #                 "id": 'user+101',
        #                 "name": "周华",
        #                 "personnel_info": {
        #                     "name": "周华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user+102',
        #                 "name": "潘林",
        #                 "personnel_info": {
        #                     "name": "潘林",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": 'user+103',
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "201001",
        #                 "id": "2011",
        #                 "name": "1",
        #                 "residents_id": "user20101"
        #             },
        #             {
        #                 "bed_code": "201002",
        #                 "id": "2012",
        #                 "name": "2",
        #                 "residents_id": "user20102"
        #             },
        #             {
        #                 "bed_code": "201003",
        #                 "id": "2013",
        #                 "name": "3",
        #                 "residents_id": "user20103"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type201",
        #         "id": "201",
        #         "zone_name": "201",
        #         "zone_number": "201",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20101",
        #                 "name": "容裕韶",
        #                 "personnel_info": {
        #                     "name": "容裕韶",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20102",
        #                 "name": "林玉棠",
        #                 "personnel_info": {
        #                     "name": "林玉棠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20103",
        #                 "name": "冯海明",
        #                 "personnel_info": {
        #                     "name": "冯海明",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20103",
        #                 "name": "冯海明",
        #                 "personnel_info": {
        #                     "name": "陈炳泉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20101",
        #                 "name": "容裕韶",
        #                 "personnel_info": {
        #                     "name": "容裕韶",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20102",
        #                 "name": "林玉棠",
        #                 "personnel_info": {
        #                     "name": "林玉棠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20103",
        #                 "name": "冯海明",
        #                 "personnel_info": {
        #                     "name": "冯海明",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20103",
        #                 "name": "冯海明",
        #                 "personnel_info": {
        #                     "name": "陈炳泉",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "202001",
        #                 "id": "2021",
        #                 "name": "1",
        #                 "residents_id": "user20201"
        #             },
        #             {
        #                 "bed_code": "202002",
        #                 "id": "2022",
        #                 "name": "2",
        #                 "residents_id": "user20202"
        #             },
        #             {
        #                 "bed_code": "202003",
        #                 "id": "2023",
        #                 "name": "3",
        #                 "residents_id": "user20203"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type202",
        #         "id": "202",
        #         "zone_name": "202",
        #         "zone_number": "202",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20201",
        #                 "name": "刘瑞兰",
        #                 "personnel_info": {
        #                     "name": "刘瑞兰",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20202",
        #                 "name": "李胡左妹",
        #                 "personnel_info": {
        #                     "name": "李胡左妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20203",
        #                 "name": "朱秀华",
        #                 "personnel_info": {
        #                     "name": "朱秀华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20201",
        #                 "name": "刘瑞兰",
        #                 "personnel_info": {
        #                     "name": "刘瑞兰",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20202",
        #                 "name": "李胡左妹",
        #                 "personnel_info": {
        #                     "name": "李胡左妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20203",
        #                 "name": "朱秀华",
        #                 "personnel_info": {
        #                     "name": "朱秀华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "203001",
        #                 "id": "2031",
        #                 "name": "1",
        #                 "residents_id": "user20301"
        #             },
        #             {
        #                 "bed_code": "203002",
        #                 "id": "2032",
        #                 "name": "2",
        #                 "residents_id": "user20302"
        #             },
        #             {
        #                 "bed_code": "203003",
        #                 "id": "2033",
        #                 "name": "3",
        #                 "residents_id": "user20303"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type203",
        #         "id": "203",
        #         "zone_name": "203",
        #         "zone_number": "203",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20301",
        #                 "name": "何美珍",
        #                 "personnel_info": {
        #                     "name": "何美珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20302",
        #                 "name": "谢笑珠",
        #                 "personnel_info": {
        #                     "name": "谢笑珠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20303",
        #                 "name": "陈润枝",
        #                 "personnel_info": {
        #                     "name": "陈润枝",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20301",
        #                 "name": "何美珍",
        #                 "personnel_info": {
        #                     "name": "何美珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20302",
        #                 "name": "谢笑珠",
        #                 "personnel_info": {
        #                     "name": "谢笑珠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20303",
        #                 "name": "陈润枝",
        #                 "personnel_info": {
        #                     "name": "陈润枝",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "204001",
        #                 "id": "2041",
        #                 "name": "1",
        #                 "residents_id": "user20401"
        #             },
        #             {
        #                 "bed_code": "204002",
        #                 "id": "2042",
        #                 "name": "2",
        #                 "residents_id": "user20402"
        #             },
        #             {
        #                 "bed_code": "204003",
        #                 "id": "2043",
        #                 "name": "3",
        #                 "residents_id": "user20403"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type204",
        #         "id": "204",
        #         "zone_name": "204",
        #         "zone_number": "204",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20401",
        #                 "name": "原干荣",
        #                 "personnel_info": {
        #                     "name": "原干荣",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20402",
        #                 "name": "曾海宁",
        #                 "personnel_info": {
        #                     "name": "曾海宁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20403",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20401",
        #                 "name": "原干荣",
        #                 "personnel_info": {
        #                     "name": "原干荣",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20402",
        #                 "name": "曾海宁",
        #                 "personnel_info": {
        #                     "name": "曾海宁",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20403",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "205001",
        #                 "id": "2051",
        #                 "name": "1",
        #                 "residents_id": "user20501"
        #             },
        #             {
        #                 "bed_code": "205002",
        #                 "id": "2052",
        #                 "name": "2",
        #                 "residents_id": "user20502"
        #             },
        #             {
        #                 "bed_code": "205003",
        #                 "id": "2053",
        #                 "name": "3",
        #                 "residents_id": "user20503"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type205",
        #         "id": "205",
        #         "zone_name": "205",
        #         "zone_number": "205",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20501",
        #                 "name": "陈叙",
        #                 "personnel_info": {
        #                     "name": "陈叙",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20502",
        #                 "name": "李汉松",
        #                 "personnel_info": {
        #                     "name": "李汉松",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20503",
        #                 "name": "冯锦波",
        #                 "personnel_info": {
        #                     "name": "冯锦波",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20501",
        #                 "name": "陈叙",
        #                 "personnel_info": {
        #                     "name": "陈叙",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20502",
        #                 "name": "李汉松",
        #                 "personnel_info": {
        #                     "name": "李汉松",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20503",
        #                 "name": "冯锦波",
        #                 "personnel_info": {
        #                     "name": "冯锦波",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "206001",
        #                 "id": "2061",
        #                 "name": "1",
        #                 "residents_id": "user20601"
        #             },
        #             {
        #                 "bed_code": "206002",
        #                 "id": "2062",
        #                 "name": "2",
        #                 "residents_id": "user20602"
        #             },
        #             {
        #                 "bed_code": "206003",
        #                 "id": "2063",
        #                 "name": "3",
        #                 "residents_id": "user20603"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type206",
        #         "id": "206",
        #         "zone_name": "206",
        #         "zone_number": "206",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20601",
        #                 "name": "宗丽婵",
        #                 "personnel_info": {
        #                     "name": "宗丽婵",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20602",
        #                 "name": "简二妹",
        #                 "personnel_info": {
        #                     "name": "简二妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20603",
        #                 "name": "邹渐崧",
        #                 "personnel_info": {
        #                     "name": "邹渐崧",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20601",
        #                 "name": "宗丽婵",
        #                 "personnel_info": {
        #                     "name": "宗丽婵",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20602",
        #                 "name": "简二妹",
        #                 "personnel_info": {
        #                     "name": "简二妹",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20603",
        #                 "name": "邹渐崧",
        #                 "personnel_info": {
        #                     "name": "邹渐崧",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "207001",
        #                 "id": "2071",
        #                 "name": "1",
        #                 "residents_id": "user20701"
        #             },
        #             {
        #                 "bed_code": "207002",
        #                 "id": "2072",
        #                 "name": "2",
        #                 "residents_id": "user20702"
        #             },
        #             {
        #                 "bed_code": "207003",
        #                 "id": "2073",
        #                 "name": "3",
        #                 "residents_id": "user20703"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type207",
        #         "id": "207",
        #         "zone_name": "207",
        #         "zone_number": "207",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20701",
        #                 "name": "梁奀",
        #                 "personnel_info": {
        #                     "name": "梁奀",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20702",
        #                 "name": "麦钻好",
        #                 "personnel_info": {
        #                     "name": "麦钻好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20703",
        #                 "name": "欧群",
        #                 "personnel_info": {
        #                     "name": "欧群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20701",
        #                 "name": "梁奀",
        #                 "personnel_info": {
        #                     "name": "梁奀",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20702",
        #                 "name": "麦钻好",
        #                 "personnel_info": {
        #                     "name": "麦钻好",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20703",
        #                 "name": "欧群",
        #                 "personnel_info": {
        #                     "name": "欧群",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "208001",
        #                 "id": "2081",
        #                 "name": "1",
        #                 "residents_id": "user20801"
        #             },
        #             {
        #                 "bed_code": "208002",
        #                 "id": "2082",
        #                 "name": "2",
        #                 "residents_id": "user20802"
        #             },
        #             {
        #                 "bed_code": "208003",
        #                 "id": "2083",
        #                 "name": "3",
        #                 "residents_id": "user20803"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type208",
        #         "id": "208",
        #         "zone_name": "208",
        #         "zone_number": "208",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20801",
        #                 "name": "张壮守",
        #                 "personnel_info": {
        #                     "name": "张壮守",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20802",
        #                 "name": "王桃",
        #                 "personnel_info": {
        #                     "name": "王桃",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20803",
        #                 "name": "罗光",
        #                 "personnel_info": {
        #                     "name": "罗光",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20801",
        #                 "name": "张壮守",
        #                 "personnel_info": {
        #                     "name": "张壮守",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20802",
        #                 "name": "王桃",
        #                 "personnel_info": {
        #                     "name": "王桃",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20803",
        #                 "name": "罗光",
        #                 "personnel_info": {
        #                     "name": "罗光",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "209001",
        #                 "id": "2091",
        #                 "name": "1",
        #                 "residents_id": "user20901"
        #             },
        #             {
        #                 "bed_code": "209002",
        #                 "id": "2092",
        #                 "name": "2",
        #                 "residents_id": "user20902"
        #             },
        #             {
        #                 "bed_code": "209003",
        #                 "id": "2093",
        #                 "name": "3",
        #                 "residents_id": "user20903"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type209",
        #         "id": "209",
        #         "zone_name": "209",
        #         "zone_number": "209",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user20901",
        #                 "name": "梁慧銮",
        #                 "personnel_info": {
        #                     "name": "梁慧銮",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20902",
        #                 "name": "成艮桂",
        #                 "personnel_info": {
        #                     "name": "成艮桂",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20903",
        #                 "name": "梁锦流",
        #                 "personnel_info": {
        #                     "name": "梁锦流",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user20901",
        #                 "name": "梁慧銮",
        #                 "personnel_info": {
        #                     "name": "梁慧銮",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20902",
        #                 "name": "成艮桂",
        #                 "personnel_info": {
        #                     "name": "成艮桂",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user20903",
        #                 "name": "梁锦流",
        #                 "personnel_info": {
        #                     "name": "梁锦流",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "210001",
        #                 "id": "2101",
        #                 "name": "1",
        #                 "residents_id": "user21001"
        #             },
        #             {
        #                 "bed_code": "210002",
        #                 "id": "2102",
        #                 "name": "2",
        #                 "residents_id": "user21002"
        #             },
        #             {
        #                 "bed_code": "210003",
        #                 "id": "2103",
        #                 "name": "3",
        #                 "residents_id": "user21003"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type210",
        #         "id": "210",
        #         "zone_name": "210",
        #         "zone_number": "210",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21001",
        #                 "name": "李满",
        #                 "personnel_info": {
        #                     "name": "李满",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21002",
        #                 "name": "甘兆坤",
        #                 "personnel_info": {
        #                     "name": "甘兆坤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21003",
        #                 "name": "张镜棠",
        #                 "personnel_info": {
        #                     "name": "张镜棠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21001",
        #                 "name": "李满",
        #                 "personnel_info": {
        #                     "name": "李满",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21002",
        #                 "name": "甘兆坤",
        #                 "personnel_info": {
        #                     "name": "甘兆坤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21003",
        #                 "name": "张镜棠",
        #                 "personnel_info": {
        #                     "name": "张镜棠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "211001",
        #                 "id": "2111",
        #                 "name": "1",
        #                 "residents_id": "user21101"
        #             },
        #             {
        #                 "bed_code": "211002",
        #                 "id": "2112",
        #                 "name": "2",
        #                 "residents_id": "user21102"
        #             },
        #             {
        #                 "bed_code": "211003",
        #                 "id": "2113",
        #                 "name": "3",
        #                 "residents_id": "user21103"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type211",
        #         "id": "211",
        #         "zone_name": "211",
        #         "zone_number": "211",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21101",
        #                 "name": "梁宝珠",
        #                 "personnel_info": {
        #                     "name": "梁宝珠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21102",
        #                 "name": "黄笑",
        #                 "personnel_info": {
        #                     "name": "黄笑",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21103",
        #                 "name": "蒲惠贤",
        #                 "personnel_info": {
        #                     "name": "蒲惠贤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21101",
        #                 "name": "梁宝珠",
        #                 "personnel_info": {
        #                     "name": "梁宝珠",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21102",
        #                 "name": "黄笑",
        #                 "personnel_info": {
        #                     "name": "黄笑",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21103",
        #                 "name": "蒲惠贤",
        #                 "personnel_info": {
        #                     "name": "蒲惠贤",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "212001",
        #                 "id": "2121",
        #                 "name": "1",
        #                 "residents_id": "user21201"
        #             },
        #             {
        #                 "bed_code": "212002",
        #                 "id": "2122",
        #                 "name": "2",
        #                 "residents_id": "user21202"
        #             },
        #             {
        #                 "bed_code": "212003",
        #                 "id": "2123",
        #                 "name": "3",
        #                 "residents_id": "user21203"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type212",
        #         "id": "212",
        #         "zone_name": "212",
        #         "zone_number": "212",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21201",
        #                 "name": "吴宝珍",
        #                 "personnel_info": {
        #                     "name": "吴宝珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21202",
        #                 "name": "谭月芳",
        #                 "personnel_info": {
        #                     "name": "谭月芳",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21203",
        #                 "name": "杨銮贞",
        #                 "personnel_info": {
        #                     "name": "杨銮贞",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21204",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21205",
        #                 "name": "黄心月",
        #                 "personnel_info": {
        #                     "name": "黄心月",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21206",
        #                 "name": "叶娲",
        #                 "personnel_info": {
        #                     "name": "叶娲",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21201",
        #                 "name": "吴宝珍",
        #                 "personnel_info": {
        #                     "name": "吴宝珍",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21202",
        #                 "name": "谭月芳",
        #                 "personnel_info": {
        #                     "name": "谭月芳",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21203",
        #                 "name": "杨銮贞",
        #                 "personnel_info": {
        #                     "name": "杨銮贞",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21204",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21205",
        #                 "name": "黄心月",
        #                 "personnel_info": {
        #                     "name": "黄心月",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21206",
        #                 "name": "叶娲",
        #                 "personnel_info": {
        #                     "name": "叶娲",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "213001",
        #                 "id": "2131",
        #                 "name": "1",
        #                 "residents_id": "user21301"
        #             },
        #             {
        #                 "bed_code": "213002",
        #                 "id": "2132",
        #                 "name": "2",
        #                 "residents_id": "user21302"
        #             },
        #             {
        #                 "bed_code": "213003",
        #                 "id": "2133",
        #                 "name": "3",
        #                 "residents_id": "user21303"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type213",
        #         "id": "213",
        #         "zone_name": "213",
        #         "zone_number": "213",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21301",
        #                 "name": "何淑仪",
        #                 "personnel_info": {
        #                     "name": "何淑仪",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21302",
        #                 "name": "黎留",
        #                 "personnel_info": {
        #                     "name": "黎留",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21303",
        #                 "name": "谭国英",
        #                 "personnel_info": {
        #                     "name": "谭国英",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21301",
        #                 "name": "何淑仪",
        #                 "personnel_info": {
        #                     "name": "何淑仪",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21302",
        #                 "name": "黎留",
        #                 "personnel_info": {
        #                     "name": "黎留",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21303",
        #                 "name": "谭国英",
        #                 "personnel_info": {
        #                     "name": "谭国英",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "214001",
        #                 "id": "2141",
        #                 "name": "1",
        #                 "residents_id": "user21401"
        #             },
        #             {
        #                 "bed_code": "214002",
        #                 "id": "2142",
        #                 "name": "2",
        #                 "residents_id": "user21402"
        #             },
        #             {
        #                 "bed_code": "214003",
        #                 "id": "2143",
        #                 "name": "3",
        #                 "residents_id": "user21403"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type214",
        #         "id": "214",
        #         "zone_name": "214",
        #         "zone_number": "214",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21401",
        #                 "name": "陈乃女",
        #                 "personnel_info": {
        #                     "name": "陈乃女",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21402",
        #                 "name": "利玉华",
        #                 "personnel_info": {
        #                     "name": "利玉华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21403",
        #                 "name": "董亚毛",
        #                 "personnel_info": {
        #                     "name": "董亚毛",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21401",
        #                 "name": "陈乃女",
        #                 "personnel_info": {
        #                     "name": "陈乃女",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21402",
        #                 "name": "利玉华",
        #                 "personnel_info": {
        #                     "name": "利玉华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21403",
        #                 "name": "董亚毛",
        #                 "personnel_info": {
        #                     "name": "董亚毛",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     },
        #     {
        #         "bed": [
        #             {
        #                 "bed_code": "215001",
        #                 "id": "2151",
        #                 "name": "1",
        #                 "residents_id": "user21501"
        #             },
        #             {
        #                 "bed_code": "215002",
        #                 "id": "2152",
        #                 "name": "2",
        #                 "residents_id": "user21502"
        #             },
        #             {
        #                 "bed_code": "215003",
        #                 "id": "2153",
        #                 "name": "3",
        #                 "residents_id": "user21503"
        #             }
        #         ],
        #         "hotel_zone_type_id": "type215",
        #         "id": "215",
        #         "zone_name": "215",
        #         "zone_number": "215",
        #         "down_element": [],
        #         "upper_element": [
        #             {
        #                 "hotel_zone_type_id": "type004",
        #                 "id": "004",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "二楼",
        #                 "zone_number": "004"
        #             },
        #             {
        #                 "hotel_zone_type_id": "type002",
        #                 "id": "002",
        #                 "organization_id": "dab0b8f8-d08d-11e9-a8b4-144f8aec0be5",
        #                 "remark": "",
        #                 "zone_name": "一栋",
        #                 "zone_number": "002"
        #             }
        #         ],
        #         "upper_hotel_zone_id":["004","002"],
        #         "upper_hotel_zone_name": ["二楼","一栋"],
        #         "user": [
        #             {
        #                 "id": "user21501",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21502",
        #                 "name": "许茂文",
        #                 "personnel_info": {
        #                     "name": "许茂文",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21503",
        #                 "name": "梁仲庆",
        #                 "personnel_info": {
        #                     "name": "梁仲庆",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21504",
        #                 "name": "黎升华",
        #                 "personnel_info": {
        #                     "name": "黎升华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21505",
        #                 "name": "陈焯南",
        #                 "personnel_info": {
        #                     "name": "陈焯南",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21506",
        #                 "name": "何根成",
        #                 "personnel_info": {
        #                     "name": "何根成",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ],
        #         "user_tmp": [
        #             {
        #                 "id": "user21501",
        #                 "name": "空",
        #                 "personnel_info": {
        #                     "name": "空",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21502",
        #                 "name": "许茂文",
        #                 "personnel_info": {
        #                     "name": "许茂文",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21503",
        #                 "name": "梁仲庆",
        #                 "personnel_info": {
        #                     "name": "梁仲庆",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21504",
        #                 "name": "黎升华",
        #                 "personnel_info": {
        #                     "name": "黎升华",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21505",
        #                 "name": "陈焯南",
        #                 "personnel_info": {
        #                     "name": "陈焯南",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             },
        #             {
        #                 "id": "user21506",
        #                 "name": "何根成",
        #                 "personnel_info": {
        #                     "name": "何根成",
        #                     "personnel_category": "长者",
        #                 },
        #                 "personnel_type": "1",
        #             }
        #         ]
        #     }
        # ]
        return res
#################################################################幸福院、幸福院单体、服务商######################################################
    def new_com_statistics_star_quantity(self, dict,page,count):
        if dict.get('type') == '幸福院':
            return [{
                'x': '一星','y': 48
            }, {
                'x': '二星','y': 0
            }, {
                'x': '三星','y': 6
            }, {
                'x': '四星','y': 3
            }, {
                'x': '五星','y': 118
            }, {
                'x': '无评分','y': 18
            }]
        elif dict.get('id') == 'dad64fa2-d08d-11e9-8760-144f8aec0be5' and dict.get('type') == '幸福院':
            return [{
                'x': '一星',
                'y': 48
            }, {
                'x': '二星',
                'y': 0
            }, {
                'x': '三星',
                'y': 6
            }, {
                'x': '四星',
                'y': 3
            }, {
                'x': '五星',
                'y': 118
            }, {
                'x': '无评分',
                'y': 18
            }]
    
    def new_com_statistics_star_quantity_happiness_single(self, dict,page,count):
        return [{
            'x': '一星',
            'y': 48
        }, {
            'x': '二星',
            'y': 0
        }, {
            'x': '三星',
            'y': 6
        }, {
            'x': '四星',
            'y': 3
        }, {
            'x': '五星',
            'y': 118
        }, {
            'x': '无评分',
            'y': 18
        }]

    def new_service_ranking_score(self, dict,page,count):
        # 假数据
        return [
            {'y1': '春晖养老', 'y2': 4.9},
            {'y1': '红星社会工作服务', 'y2': 4.9},
            {'y1': '伯乐社会工作服务中心', 'y2': 4.8},
            {'y1': '启正社会工作服务中心', 'y2': 4.7},
            {'y1': '谷丰健康', 'y2': 4.6},
            {'y1': '仁德社会工作服务中心', 'y2': 4.5},
            {'y1': '日医健康服务', 'y2': 4.4},
            {'y1': '馨和社会工作服务中心', 'y2': 4.4},
            {'y1': '希望社工服务中心', 'y2': 4.2},
            {'y1': '南海区社会福利中心', 'y2': 4.0}
        ]
    
    def new_service_ranking_time(self, dict,page,count):
        # 前十，服务次数
        return [
            {'y1': '春晖养老', 'y2': 534708},
            {'y1': '红星社会工作服务', 'y2': 15319},
            {'y1': '伯乐社会工作服务中心', 'y2': 10707},
            {'y1': '启正社会工作服务中心', 'y2': 9903},
            {'y1': '谷丰健康', 'y2': 5789},
            {'y1': '仁德社会工作服务中心', 'y2': 1643},
            {'y1': '日医健康服务', 'y2': 640},
            {'y1': '馨和社会工作服务中心', 'y2': 215},
            {'y1': '希望社工服务中心', 'y2': 188},
            {'y1': '南海区社会福利中心', 'y2': 116}
        ]
    
    def new_hot_service_ranking(self, dict,page,count):
        # 前十，热门服务，购买次数
        return [
            {'y1': '（舒心）保健与家政服务套餐', 'y2': 23883},
            {'y1': '（安心）保健与家政服务套餐', 'y2': 9717},
            {'y1': '（贴心）保健与家政服务套餐', 'y2': 1880},
            {'y1': '清洁卫士套餐', 'y2': 554},
            {'y1': '企企理理家政保洁', 'y2': 320},
            {'y1': '居家怡心服务', 'y2': 316},
            {'y1': '健青-助洁套餐', 'y2': 183},
            {'y1': '健青-夏季助洁套餐', 'y2': 181},
            {'y1': '青心-夏季换季套餐', 'y2': 158},
            {'y1': '居家倾心服务', 'y2': 144},
        ]
    
    def new_com_detail_service_products_list(self, dict,page,count):
        return [
            {'x': '20以下', 'y': 1},
            {'x': '20~30', 'y': 376},
            {'x': '30~40', 'y': 346},
            {'x': '40~50', 'y': 465},
            {'x': '50以上', 'y': 286}
        ]
    
    def new_servicer_service_num_ranking(self, dict,page,count):
        return [
            {'y1': '陈焕兰', 'y2': '佛山市南海区春晖养老服务中心','y3': 23883},
            {'y1': '黎敏韶', 'y2': '佛山市南海区春晖养老服务中心','y3': 9717},
            {'y1': '杨碧华', 'y2': '佛山市南海区春晖养老服务中心','y3': 1880},
            {'y1': '李锦崧', 'y2': '佛山市南海区春晖养老服务中心','y3': 554},
            {'y1': '邹海燕', 'y2': '佛山市南海区春晖养老服务中心','y3': 320},
            {'y1': '邵惠芬', 'y2': '佛山市南海区佰乐社会工作服务中心','y3': 316},
            {'y1': '吴洁贞', 'y2': '佛山市南海区春晖养老服务中心','y3': 183},
            {'y1': '黄棣成', 'y2': '佛山市南海区春晖养老服务中心','y3': 181},
            {'y1': '罗建月', 'y2': '佛山市南海区春晖养老服务中心','y3': 158},
            {'y1': '黄超', 'y2': '佛山市南海区春晖养老服务中心','y3': 144},
        ]

    def new_servicer_appraise_ranking(self, dict,page,count):
        # 没有实际数据，使用原型图数据和服务量排名数据合起来
        return [
            {'y1': '陈焕兰', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.9},
            {'y1': '黎敏韶', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.9},
            {'y1': '杨碧华', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.8},
            {'y1': '李锦崧', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.7},
            {'y1': '邹海燕', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.7},
            {'y1': '邵惠芬', 'y2': '佛山市南海区佰乐社会工作服务中心','y3': 4.6},
            {'y1': '吴洁贞', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.6},
            {'y1': '黄棣成', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.5},
            {'y1': '罗建月', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.5},
            {'y1': '黄超', 'y2': '佛山市南海区春晖养老服务中心','y3': 4.4},
        ]
    
    def new_sep_detail_picture_list(self, dict,page,count):
        return [
            '/build/upload/service_provider/gufeng01.jpg',
            '/build/upload/service_provider/gufeng02.jpg',
            '/build/upload/service_provider/gufeng03.jpg',
            '/build/upload/service_provider/gufeng04.jpg',
            '/build/upload/service_provider/gufeng05.jpg',
        ]
    
    def new_sep_detail_service_provider(self, dict,page,count):
        return [
            {
                'x': '<p style="text-align: justify;">广东谷丰健康管理有限公司成立于2017年，是谷丰健康产业集团旗下子公司，以开放的态度，与各级政府、社会组织携手合作，创建幸福的养老社区。致力于实现老有所养、老有所医、老有所为、老有所学、老有所乐的目标。</p ><p style="text-align: justify;">主要开展的服务内容有：上门保洁、上门护理、日间照料、康复保健、重症（特殊）照护等。主要承接养老项目的运营、居家养老服务的开展、养老人才的培养和服务派遣等。</p ><p><br/></p >',
                'y1': '广东谷丰健康管理有限公司',
                'y2': '佛山市南海区南桂东路38号房地产发展大厦首层谷丰健康日间照料中心',
                'y3': '5星级',
                'y4': '服务商',
                'y5': '民办',
                'y6': '',
                'y7': '0757-86367050'
            }
        ]
    
    def new_eld_health_exceeding_standard_num(self, dict,page,count):
        return[
            {'x': '低密度蛋白偏高', 'y': 95},
            {'x': '甘油三酯', 'y': 34},
            {'x': '体温偏高', 'y': 2},
            {'x': '脉搏偏高', 'y': 40},
            {'x': '体水分率偏高', 'y': 44},
            {'x': '血压偏高', 'y': 347}
        ]
    
    def new_activity_ranking(self, dict,page,count):
        # 除了罗湖社区幸福院，都是假数据
        if dict.get('type') == '幸福院':
            return [
                {'y1': '夏东社区幸福院', 'y2': 114},
                {'y1': '塘联社区幸福院', 'y2': 113},
                {'y1': '莲塘社区幸福院', 'y2': 112},
                {'y1': '曹边社区幸福院', 'y2': 111},
                {'y1': '沥苑社区幸福院', 'y2': 110},
                {'y1': '林岳社区幸福院', 'y2': 109},
                {'y1': '儒林社区幸福院', 'y2': 108},
                {'y1': '中汇社区幸福院', 'y2': 107},
                {'y1': '沥雅社区幸福院', 'y2': 106},
                {'y1': '罗湖社区幸福院', 'y2': 105}
            ]
        elif dict.get('id') == 'dad64fa2-d08d-11e9-8760-144f8aec0be5' and dict.get('type') == '幸福院':
            return [{'y1': '夏东社区幸福院', 'y2': 114},
                {'y1': '塘联社区幸福院', 'y2': 113},
                {'y1': '莲塘社区幸福院', 'y2': 112},
                {'y1': '曹边社区幸福院', 'y2': 111},
                {'y1': '沥苑社区幸福院', 'y2': 110},
                {'y1': '林岳社区幸福院', 'y2': 109},
                {'y1': '儒林社区幸福院', 'y2': 108},
                {'y1': '中汇社区幸福院', 'y2': 107},
                {'y1': '沥雅社区幸福院', 'y2': 106},
                {'y1': '罗湖社区幸福院', 'y2': 105}
            ]
    
    def new_activity_ranking_happiness_single(self, dict,page,count):
        # 除了罗湖社区幸福院，都是假数据
        return [{'y1': '夏东社区幸福院', 'y2': 114},
            {'y1': '塘联社区幸福院', 'y2': 113},
            {'y1': '莲塘社区幸福院', 'y2': 112},
            {'y1': '曹边社区幸福院', 'y2': 111},
            {'y1': '沥苑社区幸福院', 'y2': 110},
            {'y1': '林岳社区幸福院', 'y2': 109},
            {'y1': '儒林社区幸福院', 'y2': 108},
            {'y1': '中汇社区幸福院', 'y2': 107},
            {'y1': '沥雅社区幸福院', 'y2': 106},
            {'y1': '罗湖社区幸福院', 'y2': 105}
        ]

    def new_eld_ranking(self, dict,page,count):
        # 假数据
        return [{'y1': '夏东社区幸福院', 'y2': 2014},
                {'y1': '塘联社区幸福院', 'y2': 1913},
                {'y1': '莲塘社区幸福院', 'y2': 1812},
                {'y1': '曹边社区幸福院', 'y2': 1711},
                {'y1': '沥苑社区幸福院', 'y2': 1610},
                {'y1': '林岳社区幸福院', 'y2': 1509},
                {'y1': '儒林社区幸福院', 'y2': 1408},
                {'y1': '中汇社区幸福院', 'y2': 1307},
                {'y1': '沥雅社区幸福院', 'y2': 1206},
                {'y1': '罗湖社区幸福院', 'y2': 1105}
            ]
    
    def new_eld_ranking_happiness_single(self, dict,page,count):
        # 假数据
        return [{'y1': '夏东社区幸福院', 'y2': 2014},
                {'y1': '塘联社区幸福院', 'y2': 1913},
                {'y1': '莲塘社区幸福院', 'y2': 1812},
                {'y1': '曹边社区幸福院', 'y2': 1711},
                {'y1': '沥苑社区幸福院', 'y2': 1610},
                {'y1': '林岳社区幸福院', 'y2': 1509},
                {'y1': '儒林社区幸福院', 'y2': 1408},
                {'y1': '中汇社区幸福院', 'y2': 1307},
                {'y1': '沥雅社区幸福院', 'y2': 1206},
                {'y1': '罗湖社区幸福院', 'y2': 1105}
            ]
    
    def new_happiness_station_people_num(self, dict,page,count):
        # 假数据
        return [{'y1':'8月份','y2': '夏东社区幸福院', 'y3': 2014},
                {'y1':'8月份','y2': '塘联社区幸福院', 'y3': 1913},
                {'y1':'8月份','y2': '莲塘社区幸福院', 'y3': 1812},
                {'y1':'8月份','y2': '曹边社区幸福院', 'y3': 1711},
                {'y1':'8月份','y2': '沥苑社区幸福院', 'y3': 1610},
                {'y1':'8月份','y2': '林岳社区幸福院', 'y3': 1509},
                {'y1':'8月份','y2': '儒林社区幸福院', 'y3': 1408},
                {'y1':'8月份','y2': '中汇社区幸福院', 'y3': 1307},
                {'y1':'8月份','y2': '沥雅社区幸福院', 'y3': 1206},
                {'y1':'8月份','y2': '罗湖社区幸福院', 'y3': 1105}
            ]
    
    def new_com_statistics_time_education_quantity(self, dict,page,count):
        # 假数据
        return [
                {'y1': '罗湖社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'37'},
                {'y1': '夏东社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'72'},
                {'y1': '塘联社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'63'},
                {'y1': '莲塘社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'51'},
                {'y1': '曹边社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'42'},
                {'y1': '沥苑社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'153'},
                {'y1': '林岳社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'450'},
                {'y1': '儒林社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'16'},
                {'y1': '中汇社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'75'},
                {'y1': '沥雅社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'20'}
            ]
    
    def new_com_statistics_time_education_quantity_happiness_single(self, dict,page,count):
        # 假数据
        return [
                {'y1': '罗湖社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'37'},
                {'y1': '夏东社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'72'},
                {'y1': '塘联社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'63'},
                {'y1': '莲塘社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'51'},
                {'y1': '曹边社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'42'},
                {'y1': '沥苑社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'153'},
                {'y1': '林岳社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'450'},
                {'y1': '儒林社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'16'},
                {'y1': '中汇社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'75'},
                {'y1': '沥雅社区幸福院', 'y2': 'xxx课程', 'y3':'2019-10-02 09:00', 'y4':'20'}
            ]
    
    def new_activity_and_education_people_num(self, dict,page,count):
        # 假数据
        return [
            {'x': '1月', 'y1': 5, 'y2': 7},
            {'x': '2月', 'y1': 9, 'y2': 6},
            {'x': '3月', 'y1': 14, 'y2': 9},
            {'x': '4月', 'y1': 25, 'y2': 14},
            {'x': '5月', 'y1': 18, 'y2': 18},
            {'x': '6月', 'y1': 20, 'y2': 21},
            {'x': '7月', 'y1': 16, 'y2': 25},
            {'x': '8月', 'y1': 34, 'y2': 26},
            {'x': '9月', 'y1': 24, 'y2': 23}
        ]
    
    def new_com_statistics_time_meals_quantity(self, dict,page,count):
        # 假数据
        return [
            {'x': '1月', 'y': 7},
            {'x': '2月', 'y': 6},
            {'x': '3月', 'y': 9},
            {'x': '4月', 'y': 14},
            {'x': '5月', 'y': 18},
            {'x': '6月', 'y': 21},
            {'x': '7月', 'y': 25},
            {'x': '8月', 'y': 26},
            {'x': '9月', 'y': 23}
        ]
    
    def new_statistics_meals_service_person_time_ranking(self, dict,page,count):
        # 假数据
        return [
            {'y1':'8月份','y2': '夏东社区幸福院', 'y3': 451},
            {'y1':'8月份','y2': '塘联社区幸福院', 'y3': 436},
            {'y1':'8月份','y2': '莲塘社区幸福院', 'y3': 427},
            {'y1':'8月份','y2': '曹边社区幸福院', 'y3': 398},
            {'y1':'8月份','y2': '沥苑社区幸福院', 'y3': 384},
            {'y1':'8月份','y2': '林岳社区幸福院', 'y3': 306},
            {'y1':'8月份','y2': '儒林社区幸福院', 'y3': 288},
            {'y1':'8月份','y2': '中汇社区幸福院', 'y3': 256},
            {'y1':'8月份','y2': '沥雅社区幸福院', 'y3': 128},
            {'y1':'8月份','y2': '罗湖社区幸福院', 'y3': 100}
        ]
    
    def new_servicer_list(self, dict,page,count):
        return [{
            'x': '/build/upload/service_person/default.jpg',
            'y1': '朱纪蓝',
            'y2': '女',
            'y3': '本科',
            'y4': '25岁'
        }]
    
    def new_detail_picture_list(self, dict,page,count):
        return [
            '/build/upload/happiness/luohu01.jpg',
            '/build/upload/happiness/luohu02.jpg',
            '/build/upload/happiness/luohu03.jpg',
            '/build/upload/happiness/luohu04.jpg',
            '/build/upload/happiness/luohu05.jpg'
            ]
        
    def new_detail_happiness(self, dict,page,count):
        return [
            {
                'x': "<p>服务类型：</p><p>①老年人社区服务 ②个性化关怀服务 ③志愿服务 ④老年教育 ⑤文体娱乐活动 ⑥健康教育 ⑦医疗协助 ⑧康复训练 ⑨心理慰藉 ⑩午休服务</p><p>服务简介：</p><p>幸福院位于外滩中路36号（原罗村老龄人活动中心、星光老年之家），建筑面积在1200平方米以上。设置功能室10个，包括：康复室、活动室、图书室、社工办公室、棋牌室、爱心食堂、日托室、医务室、粤曲室、运动室等，按照五星级社区幸福院相应运营管理要求的配置，提升社区长者解决生活中所面对困难的能力；提升社区长者的健康保健意识；丰富社区长者精神文化生活，提升社区长者的自我效能感；提高社会服务知晓度和美誉度；培育社区志愿服务和社区自治力量，营造邻里关爱氛围。</p><p>项目设置共有两个站点：</p><p>1、幸福专列寿星站。设有有图书室 、棋牌室 、康复室、舞蹈室，曲艺室等多个功能活动场室，常年为社区老人提供文体娱乐服务、教育咨询服务、心灵慰籍服务、志愿服务等。</p><p>2、幸福专列康复站。设置床位不少于10张的休息室，具备无障碍设施和通道，还有多功能媒体室，医疗保健室，心理疏导室，爱心餐厅，品茶空间等，为社区老人提供生活照料服务、保健康复服务、餐饮服务等服务。</p><p>罗湖社区现有户籍人口1.6万人，60岁以上的长者人口总数为1613人，约占户籍总人数的10%，其中80岁以上的独居老人19人，孤寡老人10人，残疾老人7人，低保老人2人。人口结构进入老龄化阶段。因此，社区长期致力推进社区养老服务的发展，重视长者的精神文化生活，用实际行动营造良好的社区孝德/树本氛围。</p><p>罗湖社区幸福院将立足于社区长者的需求，关注长者身心健康，提升社区长者晚年生活质量，搭建社区长者的支持网络，提升社区长者的社区参与度、有用感、归属感和幸福感。</p><p>服务对象：罗湖社区长者及家庭、重点关注孤寡、独居、空巢、残疾的长者。</p><p><br/></p>",
                # 'y1':'5星',
                # 'y2':'公立',
                # 'y3':'幸福院',
                # 'y4':'2002.8',
                # 'y5':'0757-81800553',
                # 'y6': '佛山市南海区狮山镇罗湖社区外滩中路36号',
                # 'y7': '康复室、活动室、图书室、社工办公室、棋牌室、爱心食堂、日托室、医务室、粤曲室、运动室'

                'y1': '罗湖社区幸福院',
                'y2': '佛山市南海区狮山镇罗湖社区外滩中路36号',
                'y3': '5星级',
                'y4': '幸福院',
                'y5': '公立',
                'y6': '',
                'y7': '0757-81800553'

                # 'y1': '罗湖社区幸福院',
                # 'y2': '佛山市南海区狮山镇罗湖社区外滩中路36号',
                # 'y3': '0757-81800553',
                # 'y4': '5星级',
                # 'y5': '①老年人社区服务 ②个性化关怀服务 ③志愿服务 ④老年教育 ⑤文体娱乐活动 ⑥健康教育 ⑦医疗协助 ⑧康复训练 ⑨心理慰藉 ⑩午休服务',
                # 'y6': '康复室、活动室、图书室、社工办公室、棋牌室、爱心食堂、日托室、医务室、粤曲室、运动室',
                # 'y7': '公立',
                # 'y8': '幸福院'
            }
        ]
    
    def new_com_detail_video_surveillance(self, dict,page,count):
        return []

    def new_happiness_courtyard_map(self, dict,page,count):
        with open('./server/service/display/org_location.json', 'r', encoding='UTF-8') as f:
            data = json.load(f)
            try:
                call_data  =self.call_center_query()
                data.append({
                    'mapType':'call',
                    'name':'长者呼叫',
                    'data':call_data,
                })
            except:
                pass
            
            return data