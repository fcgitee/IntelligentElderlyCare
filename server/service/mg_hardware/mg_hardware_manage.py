from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
import requests
import json
import hashlib

from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, gcj02_to_bd09, post_requests, get_common_project
import time
from urllib.request import urlopen, quote

from ...service.mongo_bill_service import MongoBillFilter
from ...service.yx_hardware.yx_hardware_manage import (YxHardwareService)
post_url = 'https://182.254.159.127:8443/'


class MgHardwareService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.yx_hardware_service = YxHardwareService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_all_hardware_position(self, telephone):
        res = {
            'device_name': '',
            'device_lng': '',
            'device_lat': '',
            'device_address': '',
            'user_info': {},
        }
        # 找设备
        _filter_device = MongoBillFilter()
        _filter_device.match_bill(
            (C('device_sim') == telephone))\
            .project({'_id': 0, **get_common_project()})
        device_list = self.bill_manage_server.query(
            _filter_device, 'PT_Device')
        if len(device_list) > 0 and 'device_id' in device_list[0]:
            device_name = device_list[0]['device_name']
            res['device_name'] = device_name
            elder_id = device_list[0]['elder_id']
            _filter_user = MongoBillFilter()
            _filter_user.match_bill((C('id') == elder_id))\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'id_card': 1,
                    'id_card_type': 1,
                    'organization_id': 1,
                    'personnel_info.sex': 1,
                    'personnel_info.telephone': 1,
                    'personnel_info.address': 1,
                    'personnel_info.id_card': 1,
                    'personnel_info.name': 1,
                    'personnel_info.date_birth': 1,
                })
            user_list = self.query(
                _filter_user, 'PT_User')
            if len(user_list) > 0:
                if 'emergency_call' in device_list[0]:
                    user_list[0]['emergency_call'] = device_list[0]['emergency_call']
                else:
                    user_list[0]['emergency_call'] = ''
                res['user_info'] = user_list[0]
            if device_name == '咪狗':
                device_result = self.get_position(
                    device_list[0]['device_id'])
                if device_result.get('code') == 0:
                    res['device_lng'] = device_result.get('data').get('lng')
                    res['device_lat'] = device_result.get('data').get('lat')
                    res['device_address'] = device_result.get(
                        'data').get('addr')
            if device_name == '云芯':
                device_result = self.yx_hardware_service.get_position(
                    device_list[0]['device_id'])
                if device_result.get('Ret') == 'Succ':
                    data = device_result.get('data')
                    if len(data) > 0:
                        res['device_lng'] = data[0].get('Lon')
                        res['device_lat'] = data[0].get('Lat')
                        res['device_address'] = data[0].get('Str')
                pass
        else:
            res['device_name'] = '非绑定设备呼入'
        return res

    def login_mg(self):
        '''
            咪狗登录
        '''
        res = False
        time_ = int(time.time())
        psw = 'e874964919c37c35'
        url_x = post_url + 'sos_api/login'
        # 调用https请求
        result = post_requests(
            url_x, {"user": "EagleSoS", "pwdauth": psw, "time": time_})
        code = result['code']
        if code == 0:
            # token_id = result['data']['token']
            # 把token存入表中，提供后面一直使用，由于发现token是永久有效的，所以不做记录了

            # def process_func(db):
            #     # 判断是否存在，存在则更新，不存在则新增

            #     insert_data(db, 'PT_Hardware_token', {'token': token_id})
            # process_db(self.db_addr, self.db_port, self.db_name,
            #            process_func, self.db_user, self.db_pwd)
            res = result
        return res

    # def binding(self, sn_no):
    #     '''绑定设备/解除绑定 暂时不用'''
    #     res = False
    #     # 调用https请求
    #     url_x = 'https://182.254.159.127:8443/sos_api/device_check'
    #     params = json.dumps(
    #         {"user": "sos_server", "sn": sn_no, "qrcode": '??'})
    #     # prin t(params)
    #     token_id = '5ef051eea899cd17cc5c4416'
    #     headers = {
    #         "Content-Type": "application/json; charset=UTF-8;token="+token_id}
    #     result = requests.post(url_x, data=params, headers=headers).json()
    #   #print('result>>>', result)
    #     code = result['code']
    #     return res

    def get_position(self, sn_no):
        if type(sn_no) != str:
            sn_no = str(sn_no)
        '''查看位置 '''
        sn_no = '80000' + sn_no
        # 调用https请求
        url_x = post_url + 'sos_api/curpos_get'
        token_id = '5ef057dca899cd17cc5c4e1b'
        result = post_requests(
            url_x, {"user": "sos_server", "sn": sn_no, "update": 1}, {"token": token_id})
        # print('result>>>', result)
        if result['code'] == 0:
            res_data = result['data']
            lat = res_data['lat']/1000000
            lng = res_data['lng']/1000000
            # 高德转百度经纬度
            bd_lng, bd_lat = gcj02_to_bd09(lng, lat)
            # 纬度
            res_data['lat'] = bd_lat
            # 经度
            res_data['lng'] = bd_lng

            res = {'code': result['code'], 'mes': '获取成功', 'data': res_data}
            # print('res>>',res)
        else:
            res = {'code': result['code'], 'mes': '获取失败', 'data': {}}
        return res

    def setSos(self, sn_no, nums):
        if type(sn_no) != str:
            sn_no = str(sn_no)
        '''设置SOS号码 '''
        sn_no = '80000' + sn_no
        # 调用https请求
        url_x = post_url + 'sos_api/sos_set'
        token_id = '5ef057dca899cd17cc5c4e1b'
        result = post_requests(
            url_x, {"user": "sos_server", "sn": sn_no, "nums": nums}, {"token": token_id})
        # print('result>>>', result)
        return result

    def get_track(self, sn_no, query_date):
        ''' 获取轨迹 '''
        if type(sn_no) != str:
            sn_no = str(sn_no)
        sn_no = '80000' + sn_no
        # 调用https请求
        url_x = post_url + 'sos_api/track_get'
        token_id = '5ef057dca899cd17cc5c4e1b'
        result = post_requests(
            url_x, {"user": "sos_server", "sn": sn_no, "date": query_date, "from": "00:00", "to": "23:55", "unzip": 1}, {"token": token_id})
        # 测试数据
        # result = {
        #     "code": 0,
        #     "msg": "Success",
        #     "data": {
        #         "sn": "333443",
        #         "geos": [
        #             {
        #                 "lng": 111100000,
        #                 "lat": 33400000,
        #                 "time": 1400033331,
        #                 "sp": 333,
        #                 "t": 1
        #             },
        #             {
        #                 "lng": 111100000,
        #                 "lat": 33500000,
        #                 "time": 1400033331,
        #                 "sp": 333,
        #                 "t": 1
        #             },
        #             {
        #                 "lng": 111000000,
        #                 "lat": 33400000,
        #                 "time": 1400033331,
        #                 "sp": 333,
        #                 "t": 1
        #             },
        #             {
        #                 "lng": 110900000,
        #                 "lat": 33300000,
        #                 "time": 1400033331,
        #                 "sp": 333,
        #                 "t": 1
        #             }
        #         ]
        #     }
        # }

        if result['code'] == 0:
            res_data = result.get('data').get('geos')

            if len(res_data):
                points = []
                for item in res_data:
                    lat = item['lat']/1000000
                    lng = item['lng']/1000000
                    # 高德转百度经纬度
                    bd_lng, bd_lat = gcj02_to_bd09(lng, lat)
                    points.append({
                        # 纬度
                        'lat': bd_lat,
                        # 经度
                        'lng': bd_lng,
                    })

            res = {'code': result['code'], 'mes': '获取成功', 'data': points}
            # print('res>>',res)
        else:
            res = {'code': result['code'],
                   'mes': '获取失败', 'data': result['msg']}

        return res

    # def notice_callback(self, sn_no, nums):
    #     '''报警回调通知 '''
    #     # 调用https请求
    #     url_x = 'https://182.254.159.127:8443/sos_api/sos_set'
    #     params = json.dumps(
    #         {"user": "sos_server", "sn": sn_no, "nums": nums})
    #     token_id = '5ef057dca899cd17cc5c4e1b'
    #     headers = {
    #         "Content-Type": "application/json; charset=UTF-8", "token": token_id}
    #     result = requests.post(
    #         url_x, data=params, headers=headers, verify=False).json()
    #   #print('result>>>', result)
    #     return result
