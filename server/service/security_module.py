from server.pao_python.pao.remote import UnauthorizedError
from server.pao_python.pao.service.data.mongo_db import (AggregationOperators,
                                                         C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ..service.buss_pub.security_login import LoginService
from ..service.common import (
    get_current_organization_id, get_current_role_id, get_current_user_id)
from ..service.mongo_bill_service import MongoBillFilter


'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-30 18:16:42
@LastEditTime: 2020-02-19 16:10:31
@LastEditors: Please set LastEditors
'''
'''
说明
 1、权限管理相关
'''


class FunctionName:
    '''功能名字枚举类'''
    user_manage_worker = '工作人员用户管理'
    org_info_record = '机构信息档案'
    elder_list = "长者列表"
    charge_type = '收费类型'
    operate_sitation_statistic = '社区养老运营情况统计'
    month_salary = '月收入统计'
    get_message_list_new = '消息列表'
    reservation_regist_look = '查看预约登记'
    reservation_regist = '预约登记'
    assessment_template_all = '评估模板'
    assessment_template_look = '查看评估模板'
    assessment_project_look = '查看评估项目'
    assessment_project_all = '评估项目'
    competenct_assessment_all = '评估记录'
    competenct_assessment_look = '查看评估记录'
    hydro_power_all = '水电抄表'
    hydro_power_look = '查看水电抄表'
    hydro_power_num = '电费抄表'
    hydro_power_num_look = '查看电费抄表'
    leave_reason_all = '离开原因'
    leave_record_all = '住宿记录'
    leave_record_look = '查看住宿记录'
    hotel_zone_all = '区域类型'
    hotel_zone_manange_all = '区域管理'
    hotel_zone_manange_look = '查看区域情况'
    bed_manange_all = '床位管理'
    bed_manange_look = '查看床位情况'
    elder_manage_all = '长者管理'
    elder_healthy = '健康信息'
    deduction = '收费减免'
    elder_food = '用餐管理'
    set_class = '排班管理'
    set_class_type = '排班类型'
    nursing_pay = '护理收费'
    org_elder_manage_all = '机构长者管理'
    elder_info_look = '查询长者资料'
    user_org_manage_all = '机构人员管理'
    user_worker_manage_all = '工作人员管理'
    organization_all = '组织机构'
    organization_look = '查看组织机构'
    administrative_division_manage_all = '行政区划管理'
    administrative_division_look = '查看行政区划'
    business_area_manage_all = '业务区域管理'
    business_area_manage_look = '查看业务区域'
    miscellaneous_fees_all = '杂费管理'
    miscellaneous_fees_look = '查看杂费管理'
    meal_fee_all = '餐费管理'
    meal_fee_look = '查看餐费管理'
    service_order = '服务订单'
    service_evaluate = '服务评价'
    service_return_visit = '服务回访'
    service_recode = '服务记录'
    service_recode_look = '查看服务记录'
    service_item_type = '服务项目类别'
    service_item_type_look = '查看服务项目类别'
    service_item = '服务项目'
    service_item_look = '查看服务项目'
    service_product = '服务产品'
    service_product_look = '查看服务产品'
    service_range = '服务适用范围'
    service_range_look = '查看服务适用范围'
    service_option = '服务选项'
    service_option_look = '查看服务选项'
    service_provider = '服务商登记'
    service_provider_apply = '服务商申请'
    service_woker = '服务人员登记'
    service_plan = '服务计划'
    service_woker_apply = '服务人员申请'
    service_task = '服务订单分派'
    service_task_manage = '服务订单分派管理'
    service_package_list = '服务套餐列表'
    article_type = '文章类型'
    article_type_look = '查看文章类型'
    comment_type = '评论类型'
    comment_type_look = '查看评论类型'
    announcement_management = '公告管理'
    announcement_management_look = '查看公告列表'
    new_management = '新闻管理'
    new_management_look = '查看新闻列表'
    comment_review = '评论审核'
    comment_review_look = '查看评论审核'
    article_review = '文章审核'
    article_review_look = '查看文章审核'
    comment_management = '评论管理'
    comment_management_look = '查看评论管理'
    research_site_management = '调研现场管理'
    other_cost = '其他费用'
    cost_type = '费用类型'
    check_in_all = '长者入住'
    check_in_all_statics = '入住长者情况统计'
    check_in_look = '查看长者入住'
    check_in_alone_all = '入住办理'
    check_in_alone_look = '入住办理'
    cost_account_all = '住宿核算'
    cost_account_manage = '住宿核算管理'
    check_in_room_all = '房间调整'
    receivables = '应收款账单'
    actual_receivables = '实收款账单'
    difference_detail = '差额明细表'
    personnel_manage_all = '人员管理'
    worker_manage_all = '工作人员管理'
    worker_manage_all_statics = '从业人员情况统计'
    org_worker_manage_all = '机构人员管理'
    device_manage_all = '设备管理'
    device_manage_look = '查看设备'
    monitor_manage_all = '监控管理'
    monitor_manage_look = '查看监控信息'
    social_groups_type_manage_all = '社会群体类型管理'
    social_groups_type_manage_look = '查看社会群体类型'

    role_manage_all = '全部角色管理'
    role_user_all = '角色关联'
    role_user_look = '查看角色关联'
    role_manage_look = '角色管理'
    signature_bill_all = '单据签核'
    signature_bill_look = '单据查询'

    task_manage_all = '任务列表'
    task_manage_look = '查看任务列表'
    task_type_manage_all = '任务类型'
    task_type_manage_look = '查看任务类型'
    task_examine_list_look = '审核任务列表'
    task_appoint_list_look_edit = '分派任务列表'

    service_worker_allowance_apply_all = '服务人员补贴申请'
    service_worker_allowance_apply_look = '查看服务人员补贴申请'
    allowance_check_look_edit = '补贴审核'
    allowance_definition_all = '补贴定义'
    allowance_project_list_all = '补贴项目'

    select_charges_look = '查看收费记录'
    select_charges_all = '收费记录管理'
    bank_deduction_record_look = '查看银行扣款记录'
    bank_deduction_record_all = '银行扣款记录管理'
    # bank_deduction_record_all = '全部收费记录管理'
    # bank_deduction_record_look = '查看全部收费记录'
  #print_or_derive_bank_deduction_look_export = '打印/导出银行扣款单'
  #print_or_derive_bank_deduction_manage = '打印/导出银行扣款单管理'
    unband_deduction_manage = '非银行扣款单管理'
    unband_deduction_Look = '查看非银行扣款单'
    account_flow_all = '账户流水'
    account_flow_manage_all = '账户流水管理'
    account_book_manage = '账簿管理'
    account_book_manage_all = '全部账簿管理'
    account_manage = '账户管理'
    account_manage_all = '全部账户管理'
    worker_charge_look = '收费员收费'
    worker_charge_manage_look = '收费员收费管理'
    worker_refund_look = '收费员退款'
    worker_refund_manage_look = '收费员退款管理'

    activity_type_all = '活动类型'
    activity_type_look = '查看活动类型'
    activity_list_all = '活动列表'
    activity_list_look = '查看活动列表'
    activity_participate_list_all = '报名列表'
    activity_participate_list_look = '查看报名列表'
    activity_sign_in_list_all = '签到列表'
    activity_sign_in_list_look = '查看签到列表'
    activity_room_list_all = '活动室列表'
    activity_room_list_look = '查看活动室列表'
    activity_room_reservation_list_all = '活动室预约列表'
    activity_information_list_all = '活动信息'
    activity_information_list_look = '查看活动信息'

    organization_xyf_all = '幸福院列表'

    nursing_project = '护理项目'
    nursing_project_look = '查看护理项目'

    nursing_template = '护理模板'
    nursing_recode = '护理记录'
    nursing_grade = '护理等级'

    charitable_manage_all = '慈善列表'
    charitable_manage_look = '查看慈善列表'
    charitable_project_manage_all = '慈善项目'
    charitable_project_manage_look = '查看慈善项目'
    charitable_donate_manage_all = '慈善捐赠'
    charitable_donate_manage_look = '查看慈善捐赠'
    volunteer_service_category_manage_all = '慈善项目'
    volunteer_service_category_manage_look = '查看慈善项目'
    charitable_recipient_manage_all = '募捐申请'
    charitable_recipient_manage_look = '查看募捐申请'
    charitable_appropriation_manage_all = '慈善拨款'
    charitable_appropriation_manage_look = '查看慈善拨款'
    charitable_information_manage_all = '慈善信息'
    charitable_information_manage_look = '查看慈善信息'
    charitable_society_manage_all = '慈善会资料'
    charitable_society_manage_look = '查看慈善会资料'
    charitable_title_fund_manage_all = '冠名基金'
    charitable_title_fund_manage_look = '查看冠名基金'

    report_happiness_worker_count = '幸福院工作人员统计报表'
    report_welfare_worker_count = '福利院工作人员统计报表'
    report_welfare_elder_count = '福利院入住长者统计报表'

    old_age_allowance_audit = '高龄津贴审核'
    old_age_allowance_audit_look = '查看高龄津贴审核'
    old_age_list = '高龄津贴名单'

    personnel_classification_all = '人员类别管理'
    personnel_classification_look = '查看人员类别管理'

    import_excel_manage = '导入人员列表'

    subsidy_account_recharg = '补贴账户充值'

    # 报表
    order_report = '订单统计报表'

    # 平台运营方
    annountmentAll = '系统公告'
    annountmentLook = '查看系统公告'
    appPage = 'APP页面设置'
    appConfig = 'APP设置'
    # 评比
    ratingPlan = '评比方案'
    rating = '评比填表'
    # 项目协议与报告
    dealReport = '项目协议与报告'
    # 年度计划与总结
    yearSummary = '年度计划与总结'
    # APP用户管理
    appUserManage = 'APP用户管理'
    # 服务商基本情况统计
    servicerBaseinfo = '服务商基本情况统计'
    # 机构基本情况统计
    organBaseinfo = '机构基本情况统计'
    # 幸福院运营情况
    happinessOperation = '幸福院运营情况统计'
    # 社区运营情况
    areaOperation = '验收及评级情况统计'
    # 民办非营机构运营资助
    orgOperationFund = '民办非营机构运营资助统计'
    # 民办非营机构床位资助
    orgBedFund = '民办非营机构床位资助'
    # 月度计划
    monthPlan = '月度计划'
    # 团体
    groups = '团体列表'
    service_provider_settlement = '服务商结算表'
    # app反馈
    app_feedback = 'APP意见反馈'
    # 圈子
    circle = '圈子管理'
    # 长者膳食
    elder_meals = '长者膳食'
    # 计量单位
    units = '计量单位'
    # 物料分类
    thing_sort = '物料分类'
    # 物料档案
    thing_archives = '物料档案'
    # 幸福小站
    happiness_station_product = '幸福小站'
    # 幸福院年度自评
    happiness_self_year_assessment = '社区幸福院年度自评'
    # 个案探访
    service_situation = '个案服务情况'
    # 探访服务
    visit_situation = '探访服务情况'
    # 消防设施
    fire_equipment = '消防设施'
    # 南海呼叫中心
    call_center_nh = '移动呼叫中心'
    call_center_zh = '移动呼叫中心帐号'
    # 我的房间
    my_home = '我的房间'
    my_device = '我的设备'
    my_warn = '我的警报'
    my_host = '我的主机'
    # 防疫
    focus_org = '全省民政部门重点场所防疫'
    epidemic_prevention_table = '防疫汇总表'
    # 系统使用情况统计
    sys_use_staticstics = '系统使用情况统计'
    # 适老化设备警报
    device_warning = '适老化设备警报'
    # 组织机构
    organization_name = '组织机构'
    # APP隐私
    appPrivacyAgree = 'appPrivacyAgree'
    # 数据字典
    data_dictionary = '数据字典'
    # 商品信息
    products = '商品信息'
    productsSh = '商品审核'
    # 商品订单
    productOrders = '商品订单'
    # 服务商对账，包含【服务商财务总览，服务商入账记录】
    spReconciliation = '服务商对账'
    # 平台对账，包含【平台财务总览，平台资金明细】
    ptReconciliation = '平台对账'
    # 分账登记
    ledgerAccountRegister = '分账登记'
    # 分账审核
    separateAccountsSH = '分账审核'
    # 板块信息
    blocks = '板块信息'
    # 疾病档案
    disease = '疾病档案'
    allergy = '过敏档案'
    medicine = '药品档案'
    # APP在线咨询
    appOnline = 'APP在线咨询'
    # 坐席运营统计
    seatOperation = '坐席运营统计'


class PermissionName:
    '''权限枚举类'''
    query = '查询'
    edit = '编辑'
    delete = '删除'
    add = '新增'
    review = '审核'
    export = '导出'
    introduce = '导入'


class SecurityModule(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.security_service = LoginService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    collection_user = 'PT_User'
    collection_admin = 'PT_Administration_Division'

    def org_current(self):
        '''获取当前角色所属账号关联的组织机构'''
        return [get_current_organization_id(self.session)]

    def org_all_subordinate(self):
        '''获取当前角色所属账号极其所有下属账号的组织机构'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == get_current_organization_id(self.session))\
            .graph_lookup('PT_User', '$id', 'id', 'organization_info.super_org_id', 'org_info')\
            .unwind('org_info')\
            .match(C('org_info.bill_status') == 'valid')\
            .project({'_id': 0, 'organization_id': '$org_info.id'})
        query_res = self.query(_filter, self.collection_user)
        res = [t['organization_id'] for t in query_res]
        res.append(get_current_organization_id(self.session))
        return res

    def judge_permission_query(self, function_name, permission_name):
        '''判断查询权限，返回组织机构id列表'''
        permission_data = self.security_service.get_permission_list(
            function_name, permission_name)
        if len(permission_data) == 0:
            raise UnauthorizedError()
        if permission_data[0]['is_allow_super_use']:
            org_list = self.org_all_subordinate()
        else:
            org_list = self.org_current()
        return org_list

    def judge_permission_other(self, function_name, permission_name):
        '''判断除查询外的其他权限，返回未授权错误或者True'''
        permission_data = self.security_service.get_permission_list(
            function_name, permission_name)
        if len(permission_data) == 0:
            raise UnauthorizedError()
        return True

    def get_top_admin_id(self):
        res = []
        _filter = MongoBillFilter()
        _filter.match_bill((C('parent_id') == 'top') & (C('division_level') == '1'))\
            .project({'_id': 0})
        query_res = self.query(_filter, self.collection_admin)
        if len(query_res) > 0:
            res = query_res[0]['id']
        return res
