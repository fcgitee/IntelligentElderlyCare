'''
版权：Copyright (c) 2019 China

创建日期：Wednesday November 13th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Wednesday, 13th November 2019 5:21:00 pm
修改者: ymq(ymq) - <<email>>

说明
 1、定时任务控制服务
'''
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter


class ControlTaskService(MongoService):

    def __init__(self, db_addr, db_port, db_name, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.session = session

    def pause(self, job_key):
        pass

    def resume(self, job_key):
        pass


