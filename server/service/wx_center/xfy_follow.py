
import xmltodict
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ..buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ..mongo_bill_service import MongoBillFilter
import uuid
import dicttoxml
from flask import abort
import hashlib
import threading
import pymongo
# 回调处理静态类


class OfficialAccountsCallback:
    def __init__(self, request, bill_manage_server, session, db_addr, db_port, db_name, db_user, db_pwd):
        self.request = request
        self.bill_manage_server = bill_manage_server
        self.session = session
        self.lock = threading.RLock()
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
    # 处理微信公众号请求回调

    def xfy_follow_callback(self):
        if self.request.method == 'GET':
            # 开发者模式验证
            token = '891a1cda08e50c3e2029c2701b6a5598'
            signature = self.request.args.get("signature")
            timestamp = self.request.args.get("timestamp")
            nonce = self.request.args.get("nonce")
            echostr = self.request.args.get("echostr")
            # 校验参数
            if not all([signature, timestamp, nonce]):
                abort(400)
            # 按照微信的流程进行计算签名
            array = [token, timestamp, nonce]
            # 排序
            array.sort()
            # 拼接字符串
            tmp_str = "".join(array)
            # 进行sha1加密，得到正确的签名值
            sign = hashlib.sha1(tmp_str.encode("utf-8")).hexdigest()
            if signature != sign:
                abort(403)
            else:
                return echostr
        else:
            message = 'fail'
            try:
                call_info = xmltodict.parse(self.request.data)["xml"]
                fromUserName = call_info['FromUserName']
                toUserName = call_info['ToUserName']
                createTime = call_info['CreateTime']
                eventKey = call_info['EventKey']
                msgType = call_info['MsgType']
                eventType = call_info['Event']

                if VxMessageUtil.MSGTYPE_EVENT == msgType:
                    if VxMessageUtil.MESSAGE_SUBSCIBE == eventType:
                        # 处理订阅事件
                        message = self.subscribeForText(self.bill_manage_server,
                                                        toUserName, fromUserName, createTime, eventKey, self.session)
                    elif VxMessageUtil.MESSAGE_UNSUBSCIBE == eventType:
                        # 处理取消订阅事件
                        message = self.unsubscribe(self.bill_manage_server,
                                                   toUserName, fromUserName, createTime, eventKey)
            except Exception as e:
                pass
              # print(e)
                # out.close()
            return dicttoxml.dicttoxml({'retcode': 0, 'reason': message}, custom_root='response', attr_type=False).decode('utf-8')
    # 响应订阅事件--回复文本消息

    def subscribeForText(self, bill_manage_server, toUserName, fromUserName, createTime, eventKey, session):
        message = 'fail'
        # if 'xfy_follow_from_user_name' not in session.keys():
        #     session['xfy_follow_from_user_name'] = fromUserName
        # 加锁
        self.lock.acquire(blocking=True, timeout=5)
        try:
            params = eventKey.split("_")
            organization_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
            if (len(params) == 2):
                organization_id = params[1]

            # 查询关注人是否已有关注明细，已关注过则不处理，没关注过则新增数据
            _filter = MongoBillFilter()
            _filter.match_bill((C('from_user_name') == fromUserName))\
                .sort({'create_date': -1})\
                .project({"_id": 0})
            res = bill_manage_server.query(_filter, "PT_Wx_Follow_Detail")
            if len(res) == 0:
                detail = {'id': str(uuid.uuid1()), 'to_user_name': toUserName, 'from_user_name': fromUserName,
                          'create_date': createTime, 'organization_id': organization_id, 'state': VxMessageUtil.MESSAGE_SUBSCIBE}
                bill_id = bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.activity.value, [detail], ['PT_Wx_Follow_Detail'])
                # 查询汇总表是否存在汇总信息，存在则更新关注数量+1，不存在则新增数据
                _filter = MongoBillFilter()
                _filter.match_bill((C('organization_id') == organization_id) & (C('bill_status') == 'valid'))\
                    .sort({'create_date': -1})\
                    .project({"_id": 0})
                res_total = bill_manage_server.query(_filter, "PT_Wx_Follow")
                # 解决很久以后又多调用一次回调的问题
                if len(res_total) > 0:
                    if 'from_user_name' not in res_total[0].keys() or ('from_user_name' in res_total[0].keys() and res_total[0]['from_user_name'] != fromUserName):
                        total = res_total[0]
                        # total['from_user_name'] = fromUserName
                        # total['follow_quantity'] = total['follow_quantity'] + 1
                        # bill_id = bill_manage_server.add_bill(
                        #     OperationType.update.value, TypeId.activity.value, [total], ['PT_Wx_Follow'])

                        # 直接更新汇总数
                        if isinstance(self.db_port, int):
                            client = pymongo.MongoClient(
                                self.db_addr, self.db_port)
                        else:
                            client = pymongo.MongoReplicaSetClient(
                                self.db_addr, replicaset=self.db_port)
                        db = client[self.db_name]
                        db.authenticate(self.db_user, self.db_pwd)
                        collection = db['PT_Wx_Follow']
                        bill_id = collection.update(
                            {"id": total['id'], "bill_status": "valid"}, {'$set': {'follow_quantity': total['follow_quantity'] + 1, 'from_user_name': fromUserName}}, multi=False, upsert=False)
                        client.close()
                else:
                    total = {'id': str(
                        uuid.uuid1()), 'organization_id': organization_id, 'follow_quantity': 1}
                    bill_id = bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.activity.value, [total], ['PT_Wx_Follow'])
                if bill_id:
                    message = 'success'
        finally:
            # 修改完成，释放锁
            self.lock.release()
            return message
        # 响应取消订阅事件

    def unsubscribe(self, bill_manage_server, toUserName, fromUserName, createTime, eventKey):
        # TODO 可以进行取关后的其他后续业务处理
        # System.out.println("用户：" + fromUserName + "取消关注~")
        return 'success'


class VxMessageUtil:
    ''' 消息枚举类 '''
    MSGTYPE_EVENT = "event"
    MESSAGE_SUBSCIBE = "subscribe"
    MESSAGE_UNSUBSCIBE = "unsubscribe"
    MESSAGE_TEXT = "text"
