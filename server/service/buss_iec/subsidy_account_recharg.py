'''
版权：Copyright (c) 2019 China

创建日期：Monday December 23rd 2019
创建者：ymq(ymq) - <<email>>

修改日期: Monday, 23rd December 2019 2:28:23 pm
修改者: ymq(ymq) - <<email>>

说明
 1、补贴账户充值
'''
from datetime import datetime
from ...service.constant import AccountType, AccountStatus, PayType, PayStatue, plat_id, Classification
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, TypeId, OperationType, Status

import time


class SubsidyAccountRechargService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def __get_account_balance(self, account_id):
        '''获取账户余额'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('account_id') == account_id)\
               .sort({'date': -1})\
               .limit(1)
        res = self.query(_filter, 'PT_Financial_Account_Record')
        if len(res) == 0:
            return 0
        else:
            return res[0]['balance']

    def get_subsidy_list(self, org_list, condition, page, count):
        '''获取补贴人员列表(只显示兜底老人)'''
        keys = ['personnel_classification_id', 'organization_id', 'balance_low', 'balance_upper',
                'name', 'card_id', 'social_worker_id']  # personnel_classification_id为列表，organization_id为单个，若选全部则不传该字段
        if condition.get('balance_low'):
            condition['balance_low'] = int(condition['balance_low'])
        elif condition.get('balance_upper'):
            condition['balance_upper'] = int(condition['balance_upper'])
        values = self.get_value(condition, keys)
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)
        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name']})
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list) & (C('admin_area_id').inner(admin_list)))
                           & (C('personnel_info.personnel_classification') != '') & (C('personnel_info.personnel_classification') != None) & (C('personnel_info.personnel_category') == '长者') & (C('personnel_info.die_state') == '健在'))\
               .lookup_bill('PT_Personnel_Classification', 'personnel_info.personnel_classification', 'id', 'classification_info')\
               .match_bill((C('classification_info.type') == '政府补贴'))\
               .add_fields({
                   'card_id': '$personnel_info.id_card',
                   'sex': '$personnel_info.sex',
                   'type': '$classification_info.name'
               })\
            .match_bill((C('personnel_info.personnel_classification') == values['personnel_classification_id']))\
            .inner_join_bill('PT_Financial_Account', 'id', 'user_id', 'account_info')\
            .lookup_bill('PT_Financial_Account_Record', 'account_info.id', 'account_id', 'record_all_info')\
            .add_fields({'record_info': self.ao.array_filter('$record_all_info', 'tep', ((F('$tep.amount') < 0) & (F('$tep.is_clean') != True)).f)})\
            .add_fields({'amount': self.ao.absolute(self.ao.summation('$record_info.amount'))})\
            .match((C('account_info.name') == AccountType.account_subsidy)
                   & (C('account_info.balance') >= values['balance_low'])
                   & (C('account_info.balance') <= values['balance_upper'])
                   & (C('name').like(values['name']))
                   & (C('personnel_info.id_card') == values['card_id']))\
            .graph_lookup('PT_Administration_Division', '$admin_area_id', 'parent_id', 'id', 'admin')\
            .add_fields({
                "sgj_list": sgj_list
            })\
            .add_fields({'town_info': self.ao.array_filter('$sgj_list', 'tep', (F('$tep.admin_area_id').inner('$admin.id').f))})\
            .add_fields({
                'social_worker': '$town_info.name'
            })
        _filter.project({'_id': 0, 'org_name': '$social_worker',
                         'type': '$classification_info.name', 'balance': '$account_info.balance', 'amount': 1, 'user_id': '$id', 'name': 1, 'card_id': 1, 'sex': 1})
        res = self.page_query(
            _filter, 'PT_User', page, count)
        return res

    def get_subsidy_list_yh(self, org_list, condition, page, count):
        '''优化获取补贴人员列表(只显示兜底老人)'''
        keys = ['personnel_classification_id', 'organization_id', 'balance_low', 'balance_upper',
                'name', 'card_id', 'social_worker_id']  # personnel_classification_id为列表，organization_id为单个，若选全部则不传该字段
        if condition.get('balance_low'):
            condition['balance_low'] = int(condition['balance_low'])
        elif condition.get('balance_upper'):
            condition['balance_upper'] = int(condition['balance_upper'])
        values = self.get_value(condition, keys)
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)
        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name']})
        # 查询长者类型
        classification_ids = []
        classification_data = {}
        _filter_classification = MongoBillFilter()
        _filter_classification.match_bill((C('type') == '政府补贴'))\
            .project({'_id': 0})
        res_classification = self.query(
            _filter_classification, 'PT_Personnel_Classification')
        if len(res_classification) > 0:
            for classification in res_classification:
                classification_data[classification['id']
                                    ] = classification['name']
                classification_ids.append(classification['id'])
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list))
                           & (C('personnel_info.personnel_classification').inner(classification_ids))
                           & (C('admin_area_id').inner(admin_list))
                           & (C('name').like(values['name']))
                           & (C('personnel_info.id_card') == values['card_id'])
                           & (C('personnel_info.personnel_classification') == values['personnel_classification_id'])
                           & (C('personnel_info.personnel_classification') != '')
                           & (C('personnel_info.personnel_classification') != None)
                           & (C('personnel_info.personnel_category') == '长者')
                           & (C('personnel_info.die_state') == '健在'))\
            .add_fields({
                'card_id': '$personnel_info.id_card',
                'sex': '$personnel_info.sex',
                'personnel_classification': '$personnel_info.personnel_classification'
            })\
            .graph_lookup('PT_Administration_Division', '$admin_area_id', 'parent_id', 'id', 'admin')\
            .add_fields({
                "sgj_list": sgj_list
            })\
            .add_fields({'town_info': self.ao.array_filter('$sgj_list', 'tep', (F('$tep.admin_area_id').inner('$admin.id').f))})\
            .add_fields({
                'social_worker': '$town_info.name'
            })
        _filter.project({'_id': 0, 'org_name': '$social_worker', 'type': '$classification_info.name',
                         'user_id': '$id', 'name': 1, 'card_id': 1, 'sex': 1})
        res = self.page_query(
            _filter, 'PT_User', page, count)

        user_ids = []
        for i, x in enumerate(res['result']):
            user_ids.append(res['result'][i]['user_id'])
            if 'personnel_classification' in res['result'][i] and res['result'][i]['personnel_classification'] in classification_data.keys():
                res['result'][i]['type'] = classification_data[res['result']
                                                               [i]['personnel_classification']]

        # 查询账号表
        _filter_account = MongoBillFilter()
        _filter_account.match_bill((C('name') == AccountType.account_subsidy) & (C('user_id').inner(user_ids)))\
            .project({'_id': 0, 'id': 1, 'user_id': 1, 'balance': 1})
        res_account = self.query(_filter_account, 'PT_Financial_Account')
        account_ids = []
        account_data = {}
        account_record_data = {}
        if len(res_account) > 0:
            for account in res_account:
                account_ids.append(account['id'])
            if len(account_ids) > 0:
                # 查询账号流水表
                _filter_account_record = MongoBillFilter()
                _filter_account_record.match_bill((C('account_id').inner(account_ids)) & (C('amount') < 0) & (C('is_clean') != True))\
                    .group({'account_id': '$account_id'}, [{'t_amount': self.ao.summation('$amount')}])\
                    .add_fields({'total_amount': self.ao.absolute('$t_amount')})\
                    .project({'_id': 0, 'account_id': 1, 'total_amount': 1})
                res_account_record = self.query(
                    _filter_account_record, 'PT_Financial_Account_Record')
                if len(res_account_record) > 0:
                    for account_record in res_account_record:
                        account_record_data[account_record['account_id']
                                            ] = account_record['total_amount']

            for account in res_account:
                if 'id' in account and account['id'] in account_record_data.keys():
                    account['total_amount'] = account_record_data[account['id']]
                else:
                    account['total_amount'] = 0
                account_data[account['user_id']] = account

        for i, x in enumerate(res['result']):
            if 'user_id' in res['result'][i] and res['result'][i]['user_id'] in account_data.keys():
                res['result'][i]['balance'] = account_data[res['result']
                                                           [i]['user_id']]['balance']
                res['result'][i]['amount'] = account_data[res['result']
                                                          [i]['user_id']]['total_amount']

        return res

    def recharg_subsidy_account(self, org_list, condition):
        '''充值补贴账户
        Args：condition中：amount和remark必须有，personnel_classification_id(数组)和id只允许传其中一个
        '''
        try:
            account_ids = []
            if condition.get('personnel_classification_id'):
                _filter = MongoBillFilter()
                _filter.match_bill(
                    (C('organization_id').inner(org_list))
                    & (C('id').inner(condition['personnel_classification_id']))
                    & (C('type') == Classification.government_subsidies)
                )\
                    .project({'_id': 0})
                class_data = self.query(_filter, 'PT_Personnel_Classification')
                if len(class_data) > 0:
                    # 查询用户表数据
                    user_ids = []
                    class_ids = [i['id'] for i in class_data]
                    _filter_user = MongoBillFilter()
                    _filter_user.match_bill((C('personnel_info.personnel_classification').inner(class_ids)) & (C('personnel_info.personnel_category') == '长者') & (C('personnel_info.die_state') == '健在'))\
                        .project({'_id': 0})
                    res_user = self.query(
                        _filter_user, 'PT_User')
                    if len(res_user) > 0:
                        for user in res_user:
                            user_ids.append(user['id'])

                    # 查询用户账户数据
                    _filter_account = MongoBillFilter()
                    _filter_account.match_bill((C('user_id').inner(user_ids)) & (C('name') == AccountType.account_subsidy))\
                        .project({'_id': 0})
                    res_account = self.query(
                        _filter_account, 'PT_Financial_Account')
                    if len(res_account) > 0:
                        for account in res_account:
                            account_ids.append({'account_id': account['id']})

            elif condition.get('id'):
                _filter = MongoBillFilter()
                _filter.match_bill((C('user_id') == condition['id']) & (C('name') == AccountType.account_subsidy))\
                    .project({'_id': 0, 'account_id': '$id'})
                account_ids = self.query(_filter, 'PT_Financial_Account')
            else:
                return False
            amount = int(condition['amount'])
            res_clean = False
            res_recharge = False
            if len(account_ids) > 0:
                # step1:清零
                clean_account_list = []
                clean_record_list = []

                t1 = time.time()
                _filter = MongoBillFilter()
                _filter.match_bill(C('account_id').inner([i['account_id'] for i in account_ids]))\
                    .group({'account_id': '$account_id'}, [{'new_date': self.ao.maximum('$date')}])\
                    .lookup_bill('PT_Financial_Account_Record', 'account_id', 'account_id', 'new_data')\
                    .add_fields({'newest': self.ao.array_elemat(self.ao.array_filter('$new_data', 'tep', (F('$tep.date') == '$new_date').f), 0)})\
                    .project({'balance': '$newest.balance', 'account_id': 1})

                balance_list = self.query(
                    _filter, 'PT_Financial_Account_Record')
                if len(balance_list) < len(account_ids):
                    for j in account_ids:
                        if j['account_id'] not in [i['account_id'] for i in balance_list]:
                            balance_list.append(
                                {'account_id': j['account_id'], 'balance': 0})
                for i in balance_list:
                    if i['balance'] > 0:
                        clean_account_list.append(
                            {'balance': 0, 'id': i['account_id']})
                        clean_record_list.append({'account_id': i['account_id'], 'amount': i['balance'] * (
                            -1), 'balance': 0, 'abstract': '补贴账户清零', 'is_clean': True, 'date': datetime.now(), 'remarks': condition['remark']})
                if len(clean_account_list) > 0:
                    self.bill_manage_server.add_bill(
                        OperationType.update.value, TypeId.financial.value, clean_account_list, 'PT_Financial_Account')
                    self.bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.financial.value, clean_record_list, 'PT_Financial_Account_Record')
                    res_clean = True
                t2 = time.time()
              # print('清零》》', t2-t1)
                # step2:充值
                if amount > 0:
                    t3 = time.time()
                    recharge_account_list = []
                    recharge_record_list = []
                    for i in account_ids:
                        recharge_account_list.append(
                            {'id': i['account_id'], 'balance': amount})
                        recharge_record_list.append({'balance': amount, 'amount': amount, 'abstract': '补贴金额发放', 'date': datetime.now(
                        ), 'remarks': condition['remark'], 'account_id': i['account_id']})
                    if len(recharge_account_list) > 0:
                        self.bill_manage_server.add_bill(
                            OperationType.update.value, TypeId.financial.value, recharge_account_list, 'PT_Financial_Account')
                        self.bill_manage_server.add_bill(
                            OperationType.add.value, TypeId.financial.value, recharge_record_list, 'PT_Financial_Account_Record')
                        res_recharge = True
                    t4 = time.time()
                  # print('充值》》', t4-t3)
            if amount == 0:
                return res_clean
            else:
                return res_recharge
        except Exception as e:
            self.logger.exception(e)
            return False

    def get_subsidy_account_detail(self, condition, page, count):
        '''获取长者补贴账户明细'''
        keys = ['id', 'start_date', 'end_date']  # id为必传参数
        if condition.get('id'):
            if condition.get('date_range'):
                condition['start_date'] = condition['date_range'][0][0:10] + \
                    "T00:00:00.00Z"
                condition['end_date'] = condition['date_range'][1][0:10] + \
                    "T23:59:59.00Z"
                # condition['start_date'] = condition['date_range'][0]
                # condition['end_date'] = condition['date_range'][1]
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('user_id') == condition['id']) & (C('name') == AccountType.account_subsidy))\
                   .inner_join_bill('PT_Financial_Account_Record', 'id', 'account_id', 'record_info')\
                   .add_fields({'order_create_date': '$record_info.create_date'})\
                   .match((C('order_create_date') > as_date(values['start_date'])) & (C('order_create_date') < as_date(values['end_date'])))\
                   .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
                   .lookup_bill('PT_User', 'bill_operator', 'id', 'operator_info')\
                   .sort({'order_create_date': -1})\
                   .project({'_id': 0, 'record_info._id': 0, 'user_info._id': 0, 'operator_info._id': 0})
            res = self.page_query(_filter, 'PT_Financial_Account', page, count)
        else:
            res = False
        return res

    def get_charitable_subsidy_list(self, org_list, condition, page, count):
        '''获取慈善账户列表'''
        keys = ['personnel_classification_id', 'organization_id', 'balance_low', 'balance_upper',
                'name', 'card_id']  # personnel_classification_id为列表，organization_id为单个，若选全部则不传该字段
        if condition.get('balance_low'):
            condition['balance_low'] = int(condition['balance_low'])
        elif condition.get('balance_upper'):
            condition['balance_upper'] = int(condition['balance_upper'])
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list)) & (C('personnel_info.personnel_category') == '长者'))\
               .add_fields({
                   'card_id': '$personnel_info.id_card',
                   'sex': '$personnel_info.sex',
               })\
            .lookup_bill('PT_Financial_Account', 'id', 'user_id', 'account_info')\
            .lookup_bill('PT_Financial_Account_Record', 'account_info.id', 'account_id', 'record_all_info')\
            .add_fields({'record_info': self.ao.array_filter('$record_all_info', 'tep', (F('$tep.amount') < 0).f)})\
            .add_fields({'amount': self.ao.absolute(self.ao.summation('$record_info.amount'))})\
            .match((C('account_info.name') == AccountType.account_charitable)
                   & (C('account_info.balance') >= values['balance_low'])
                   & (C('account_info.balance') <= values['balance_upper'])
                   & (C('name').like(values['name']))
                   & (C('personnel_info.id_card') == values['card_id']))
        _filter.project({'_id': 0,
                         'balance': '$account_info.balance', 'amount': 1, 'user_id': '$id', 'name': 1, 'card_id': 1, 'sex': 1})
        res = self.page_query(
            _filter, 'PT_User', page, count)
        return res

    def get_charitable_subsidy_list_yh(self, org_list, condition, page, count):
        '''优化获取慈善账户列表'''
        keys = ['personnel_classification_id', 'organization_id', 'balance_low', 'balance_upper',
                'name', 'card_id']  # personnel_classification_id为列表，organization_id为单个，若选全部则不传该字段
        if condition.get('balance_low'):
            condition['balance_low'] = int(condition['balance_low'])
        elif condition.get('balance_upper'):
            condition['balance_upper'] = int(condition['balance_upper'])
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list))
                           & (C('personnel_info.personnel_category') == '长者')
                           & (C('name').like(values['name']))
                           & (C('personnel_info.id_card') == values['card_id']))\
               .add_fields({
                   'card_id': '$personnel_info.id_card',
                   'sex': '$personnel_info.sex',
               })
        _filter.project({'_id': 0, 'user_id': '$id',
                         'name': 1, 'card_id': 1, 'sex': 1})
        res = self.page_query(
            _filter, 'PT_User', page, count)
        user_ids = []
        for i, x in enumerate(res['result']):
            user_ids.append(res['result'][i]['id'])

        # 查询账号表
        _filter_account = MongoBillFilter()
        _filter_account.match_bill((C('name') == AccountType.account_charitable) & (C('user_id').inner(user_ids)))\
            .project({'_id': 0, 'id': 1, 'user_id': 1, 'balance': 1})
        res_account = self.query(_filter_account, 'PT_Financial_Account')
        account_ids = []
        account_data = {}
        account_record_data = {}
        if len(res_account) > 0:
            for account in res_account:
                account_ids.append(account['id'])
            if len(account_ids) > 0:
                # 查询账号流水表
                _filter_account_record = MongoBillFilter()
                _filter_account_record.match_bill((C('account_id').inner(account_ids)) & (C('amount') < 0))\
                    .group({'account_id': '$account_id'}, [{'t_amount': self.ao.summation('$amount')}])\
                    .add_fields({'total_amount': self.ao.absolute('$t_amount')})\
                    .project({'_id': 0, 'account_id': 1, 'total_amount': 1})
                res_account_record = self.query(
                    _filter_account_record, 'PT_Financial_Account_Record')
                if len(res_account_record) > 0:
                    for account_record in res_account_record:
                        account_record_data[account_record['account_id']
                                            ] = account_record['total_amount']

            for account in res_account:
                if 'id' in account and account['id'] in account_record_data.keys():
                    account['total_amount'] = account_record_data[account['id']]
                else:
                    account['total_amount'] = 0
                account_data[account['user_id']] = account

        for i, x in enumerate(res['result']):
            if 'id' in res['result'][i] and res['result'][i]['id'] in account_data.keys():
                res['result'][i]['balance'] = account_data[res['result']
                                                           [i]['id']]['balance']
                res['result'][i]['amount'] = account_data[res['result']
                                                          [i]['id']]['total_amount']
        return res

    def recharg_charitable_account(self, org_list, condition):
        '''充值慈善账户
        Args：condition中：amount和remark必须有，personnel_classification_id(数组)和id只允许传其中一个
        '''
        if condition.get('id'):
            _filter = MongoBillFilter()
            _filter.match_bill((C('user_id') == condition['id']) & (C('name') == AccountType.account_charitable))\
                   .project({'_id': 0, 'account_id': '$id'})
            account_ids = self.query(_filter, 'PT_Financial_Account')
        else:
            return False
        amount = int(condition['amount'])
        if len(account_ids) > 0:
            # step1:清零
            clean_account_list = []
            clean_record_list = []
            for i in account_ids:
                balance = self.__get_account_balance(i['account_id'])
                if balance > 0:
                    clean_account_list.append(
                        {'balance': 0, 'id': i['account_id']})
                    clean_record_list.append({'account_id': i['account_id'], 'amount': balance * (
                        -1), 'balance': 0, 'abstract': '慈善账户清零', 'date': datetime.now(), 'remarks': condition['remark']})
            if len(clean_account_list) > 0:
                self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.financial.value, clean_account_list, 'PT_Financial_Account')
                self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.financial.value, clean_record_list, 'PT_Financial_Account_Record')
                res_clean = True
            # step2:充值
            if amount > 0:
                recharge_account_list = []
                recharge_record_list = []
                for i in account_ids:
                    recharge_account_list.append(
                        {'id': i['account_id'], 'balance': amount})
                    recharge_record_list.append({'balance': amount, 'amount': amount, 'abstract': '慈善金额充值', 'date': datetime.now(
                    ), 'remarks': condition['remark'], 'account_id': i['account_id']})
                if len(recharge_account_list) > 0:
                    self.bill_manage_server.add_bill(
                        OperationType.update.value, TypeId.financial.value, recharge_account_list, 'PT_Financial_Account')
                    self.bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.financial.value, recharge_record_list, 'PT_Financial_Account_Record')
                    res_recharge = True
        if amount == 0:
            return res_clean
        else:
            return res_recharge

    def get_charitable_account_detail(self, condition, page, count):
        '''获取长者慈善账户明细'''
        keys = ['id', 'start_date', 'end_date']  # id为必传参数
        if condition.get('id'):
            if condition.get('date_range'):
                condition['start_date'] = condition['date_range'][0]
                condition['end_date'] = condition['date_range'][1]
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('user_id') == condition['id']) & (C('name') == AccountType.account_charitable))\
                   .inner_join_bill('PT_Financial_Account_Record', 'id', 'account_id', 'record_info')\
                   .match((C('date') > as_date(values['start_date'])) & (C('date') < as_date(values['end_date'])))\
                   .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
                   .lookup_bill('PT_User', 'bill_operator', 'id', 'operator_info')\
                   .sort({'date': -1})\
                   .project({'_id': 0, 'record_info._id': 0, 'user_info._id': 0, 'operator_info._id': 0})
            res = self.page_query(_filter, 'PT_Financial_Account', page, count)
        else:
            res = False
        return res

    def get_account_balance(self, org_list, condition, page, count):
        '''获取长者账户余额'''
        res = 'False'
        keys = ['user_id', 'account_name']  # user_id为必传参数
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == values['user_id']) & (C('account_type') == values['account_name']))\
            .sort({'date': -1})\
            .project({'_id': 0})
        res = self.page_query(_filter, 'PT_Financial_Account', page, count)
        return res
