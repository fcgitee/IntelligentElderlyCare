
# -*- coding: utf-8 -*-

'''
能力评估模板
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import get_common_project, insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...service.buss_pub.bill_manage import BillManageService, TypeId, OperationType, Status
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date


class AssessmentProjectService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_assessment_project_list(self, org_list, condition, page, count):
        '''
            获取评估项目列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ['id', 'project_name', 'start_date',
                'end_date', 'assessment_type', 'selectlist', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('project_name').like(values['project_name']))
                           & (C('assessment_type') == values['assessment_type'])
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('selete_type').inner(values["selectlist"]))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill("PT_User", "organization_id", "id", "org")\
            .add_fields({"org_name": "$org.name"})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                "org._id": 0,
                **get_common_project('org')
            })
        res = self.page_query(
            _filter, 'PT_Assessment_Project', page, count)
        return res

    def get_assessment_project_list_all_look(self, condition, page, count):
        '''
            获取评估项目列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['id', 'project_name', 'start_date',
                'end_date', 'assessment_type', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('project_name').like(values['project_name']))
                           & (C('assessment_type') == values['assessment_type'])
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date'])))\
            .lookup_bill("PT_User", "organization_id", "id", "org")\
            .add_fields({"org_name": "$org.name"})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .project({'_id': 0, "org._id": 0})
        res = self.page_query(
            _filter, 'PT_Assessment_Project', page, count)
        return res

    def update_assessment_project(self, assessment_project):
        '''新增/修改评估项目

        Arguments:
            assessment_template   {dict}      条件
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            data = get_info({**assessment_project}, self.session)
            if 'id' in assessment_project.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.assessmentProject.value, data, 'PT_Assessment_Project')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.assessmentProject.value, data, 'PT_Assessment_Project')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_assessment_project(self, assessment_project_id_list):
        '''删除评估项目接口

        Arguments:
            assessment_template_id_list   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(assessment_project_id_list, str):
                ids.append(assessment_project_id_list)
            else:
                ids = assessment_project_id_list
            for assessment_project_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Assessment_Project', {
                    'id': assessment_project_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.assessmentProject.value, data[0], 'PT_Assessment_Project')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
