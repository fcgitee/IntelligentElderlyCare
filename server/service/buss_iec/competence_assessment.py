# -*- coding: utf-8 -*-

'''
能力评估模板
'''
from ...pao_python.pao.data import process_db, string_to_date, dataframe_to_list
from ...service.common import find_data, get_info, operation_result, get_string_time, get_current_user_id, get_common_project, get_current_organization_id
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
import pandas as pd
import datetime

tem_list = [
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "接受服务前初评", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "接受服务后的常规评估", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "状况发生变化后的即时评估", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "因评估结果有疑问进行的复评", 
                            "score" : "4", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "评估原因", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "project_name" : "民族", 
                    "selete_type" : "3", 
                    "value" : "汉"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "文盲", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "小学", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "初中", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "高中/技校/中专", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "大学专科及专科以上", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "不详", 
                            "score" : "6", 
                            "serial_number" : 6
                        }
                    ], 
                    "project_name" : "文化程度", 
                    "selete_type" : "1", 
                    "value" : "5/5"
                }
            }, 
            {
                "ass_info" : {
                    "project_name" : "宗教信仰", 
                    "selete_type" : "3", 
                    "value" : "无"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "未婚", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "已婚", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "丧偶", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "离婚", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "未说明的婚姻状况", 
                            "score" : "5", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "婚姻状况", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "独居", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "与配偶/伴侣居住", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "与子女居住", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "与父母居住", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "与兄弟姐妹居住", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "与其他亲属居住", 
                            "score" : "6", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "与非亲属关系的人居住", 
                            "score" : "7", 
                            "serial_number" : 7
                        }, 
                        {
                            "option_content" : "养老机构", 
                            "score" : "8", 
                            "serial_number" : 8
                        }
                    ], 
                    "project_name" : "居住情况", 
                    "selete_type" : "1", 
                    "value" : "8/8"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "城镇职工基本医疗保险", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : " 城镇居民基本医疗保险 ", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : " 新型农村合作医疗 ", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "贫困救助 ", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "商业医疗保险", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "全公费", 
                            "score" : "6", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "全自费", 
                            "score" : "7", 
                            "serial_number" : 7
                        }, 
                        {
                            "option_content" : "其他", 
                            "score" : "8", 
                            "serial_number" : 8
                        }
                    ], 
                    "project_name" : "医疗费用支付方式", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "退休金/养老金", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "子女补贴", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "亲友资助", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "其他补贴", 
                            "score" : "4", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "经济来源", 
                    "selete_type" : "1", 
                    "value" : "1/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "国语/普通话", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "广东话", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "客家话", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "方言", 
                            "score" : "4", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "主要语言", 
                    "selete_type" : "1", 
                    "value" : "1/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "过去90天内没有住院", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "31至90天内", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "15至30天内", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "8至14天内", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "过去7天内", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "现住院中", 
                            "score" : "6", 
                            "serial_number" : 6
                        }
                    ], 
                    "project_name" : "上一次住院至今", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "完整", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "佩戴活动式假牙", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "部分皮肤破损", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "皮肤有较深伤口", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "皮肤破损露出肌肉或骨骼", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "无法分类-例如：伤口底部腐肉或痂皮覆盖", 
                            "score" : "6", 
                            "serial_number" : 6
                        }
                    ], 
                    "project_name" : "皮肤状况", 
                    "selete_type" : "1", 
                    "value" : "3/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "完整", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "佩戴活动式假牙", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "牙齿损坏、不全、松动或其他牙齿缺陷", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "诉有口部或面部疼痛/不适", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "口吃", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "有咀嚼困难", 
                            "score" : "6", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "真牙或牙齿附近牙肉发炎或出血", 
                            "score" : "7", 
                            "serial_number" : 7
                        }
                    ], 
                    "project_name" : "牙齿或口腔", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "轻度", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "中度", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "重度", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "痴呆", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "不感兴趣，过去3天内没有参与", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "不感兴趣，过去3天内有参与", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "感兴趣，没有参与", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "感兴趣，定期参与，但过去3天没有参与", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "感兴趣，过去3天内参与", 
                            "score" : "5", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "活动喜好与参与", 
                    "selete_type" : "1", 
                    "value" : "2/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "接受专业护理", 
                            "score" : "1", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "检查和预防并发症", 
                            "score" : "2", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "协助服务使用者康复", 
                            "score" : "3", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "教育服务使用者", 
                            "score" : "4", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "长者暂托服务", 
                            "score" : "5", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "善终护理", 
                            "score" : "6", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "其他", 
                            "score" : "7", 
                            "serial_number" : 7
                        }
                    ], 
                    "project_name" : "服务使用者希望达到的照顾目标", 
                    "selete_type" : "1", 
                    "value" : "1/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "精神分裂症", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "双向情感障碍", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "偏执性精神障碍", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "分裂情感性障碍", 
                            "score" : "4", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "癫痫所致精神障碍", 
                            "score" : "5", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "精神发育迟滞伴发精神障碍", 
                            "score" : "6", 
                            "serial_number" : 7
                        }
                    ], 
                    "project_name" : "精神疾病", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "髋骨骨折", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "其他骨折", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "骨质酥松", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "半身瘫痪", 
                            "score" : "4", 
                            "serial_number" : 5
                        }, 
                        {
                            "option_content" : "多发性硬化症", 
                            "score" : "5", 
                            "serial_number" : 6
                        }, 
                        {
                            "option_content" : "下肢瘫痪", 
                            "score" : "6", 
                            "serial_number" : 7
                        }, 
                        {
                            "option_content" : "帕金森病", 
                            "score" : "7", 
                            "serial_number" : 8
                        }, 
                        {
                            "option_content" : "四肢瘫痪", 
                            "score" : "8", 
                            "serial_number" : 9
                        }, 
                        {
                            "option_content" : "中风后遗症", 
                            "score" : "9", 
                            "serial_number" : 10
                        }, 
                        {
                            "option_content" : "癫痫病", 
                            "score" : "10", 
                            "serial_number" : 11
                        }, 
                        {
                            "option_content" : "冠心病", 
                            "score" : "11", 
                            "serial_number" : 12
                        }, 
                        {
                            "option_content" : "慢性阻塞性肺病", 
                            "score" : "12", 
                            "serial_number" : 13
                        }, 
                        {
                            "option_content" : "充血性心脏衰竭", 
                            "score" : "13", 
                            "serial_number" : 14
                        }, 
                        {
                            "option_content" : "心脏病", 
                            "score" : "14", 
                            "serial_number" : 15
                        }, 
                        {
                            "option_content" : "肺部感染", 
                            "score" : "15", 
                            "serial_number" : 16
                        }, 
                        {
                            "option_content" : "消化道疾病", 
                            "score" : "16", 
                            "serial_number" : 17
                        }, 
                        {
                            "option_content" : "癌症", 
                            "score" : "17", 
                            "serial_number" : 18
                        }, 
                        {
                            "option_content" : "糖尿病", 
                            "score" : "18", 
                            "serial_number" : 19
                        }, 
                        {
                            "option_content" : "高血压", 
                            "score" : "19", 
                            "serial_number" : 20
                        }, 
                        {
                            "option_content" : "败血症", 
                            "score" : "20", 
                            "serial_number" : 21
                        }, 
                        {
                            "option_content" : "肾病综合症", 
                            "score" : "21", 
                            "serial_number" : 22
                        }, 
                        {
                            "option_content" : "甲状腺疾病", 
                            "score" : "22", 
                            "serial_number" : 23
                        }, 
                        {
                            "option_content" : "白内障", 
                            "score" : "23", 
                            "serial_number" : 24
                        }, 
                        {
                            "option_content" : "青光眼", 
                            "score" : "24", 
                            "serial_number" : 25
                        }, 
                        {
                            "option_content" : "类风湿关节炎", 
                            "score" : "25", 
                            "serial_number" : 26
                        }, 
                        {
                            "option_content" : "痛风", 
                            "score" : "26", 
                            "serial_number" : 27
                        }, 
                        {
                            "option_content" : "前列腺疾病", 
                            "score" : "27", 
                            "serial_number" : 28
                        }, 
                        {
                            "option_content" : "尿道感染", 
                            "score" : "28", 
                            "serial_number" : 29
                        }, 
                        {
                            "option_content" : "胆固醇过高", 
                            "score" : "29", 
                            "serial_number" : 30
                        }
                    ], 
                    "project_name" : "慢性疾病", 
                    "selete_type" : "2", 
                    "value" : [
                        "18/19", 
                        "19/20", 
                        "27/28"
                    ]
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "发生过1次", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "发生过2次", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "发生过3次及以上", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "跌倒（30天内）", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "发生过1次", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "发生过2次", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "发生过3次及以上", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "走失（30天内）", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "发生过1次", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "发生过2次", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "发生过3次及以上", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "噎食（30天内）", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "发生过1次", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "发生过2次", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "发生过3次及以上", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "自杀（30天内）", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "project_name" : "其他", 
                    "selete_type" : "3", 
                    "value" : "无"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "project_name" : "信息提供者的姓名", 
                    "selete_type" : "3", 
                    "value" : "陆玮森"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "配偶", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "子女", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "其他亲属", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "雇佣照顾者", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "其他", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "信息提供者与老人的关系", 
                    "selete_type" : "1", 
                    "value" : "2/3"
                }
            }, 
            {
                "ass_info" : {
                    "project_name" : "联系人姓名", 
                    "selete_type" : "3", 
                    "value" : "陆玮森"
                }
            }, 
            {
                "ass_info" : {
                    "project_name" : "联系人电话", 
                    "selete_type" : "3", 
                    "value" : "0757-85922026"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可独立进食（在合理的时间内独立进食准备好的食物）", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（进食过程中需要一定帮助，如协助把持餐具）", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需极大帮助或完全依赖他人，或有留置营养管", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "进食", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "准备好洗澡水后，可自己独立完成洗澡过程", 
                            "score" : "5", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "在洗澡过程中需他人帮助", 
                            "score" : "0", 
                            "serial_number" : 2
                        }
                    ], 
                    "project_name" : "洗澡", 
                    "selete_type" : "1", 
                    "value" : "0/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可以独立完成", 
                            "score" : "5", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需要他人协助", 
                            "score" : "0", 
                            "serial_number" : 2
                        }
                    ], 
                    "project_name" : "修饰", 
                    "selete_type" : "1", 
                    "value" : "0/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可以独立完成", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（能自己穿脱，但需他人帮助整理衣物、系扣/鞋带、拉拉链）  ", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需要极大帮助或完全依赖他人", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "穿衣", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可控制大便", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "偶尔失控（每周<1次），或需要他人提示", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "完全失控", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "大便控制", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可控制小便", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "偶尔失控（每天<1次，但每周>1次），或需要他人提示", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "完全失控，或留置导尿管", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "小便控制", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可独立完成", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（需他人搀扶去厕所、需他人帮忙冲水或整理衣裤等）", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需极大帮助或完全依赖他人", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "如厕", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可独立完成", 
                            "score" : "15", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（需他人搀扶或使用拐杖）", 
                            "score" : "10", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需极大帮助（较大程度上依赖他人搀扶和帮助）", 
                            "score" : "5", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "完全依赖他人", 
                            "score" : "0", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "床椅转移", 
                    "selete_type" : "1", 
                    "value" : "0/4"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可独立在平地上行走45m", 
                            "score" : "15", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在一定 程度上需他人地搀扶或使用拐杖、助行器等辅助用具）", 
                            "score" : "10", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需极大帮助（因肢体残疾、平衡能力差、过度衰弱、视力等问题，在较大 程度上依赖他人搀扶，或坐在轮椅上自行移动）", 
                            "score" : "5", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "完全依赖他人", 
                            "score" : "0", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "平地行走", 
                    "selete_type" : "1", 
                    "value" : "0/4"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可独立上下楼梯（连续上下10-15个台阶）", 
                            "score" : "10", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "需部分帮助（需他人搀扶，或扶着楼梯、使用拐杖等）", 
                            "score" : "5", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "需极大帮助或完全依赖他人", 
                            "score" : "0", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "上下楼梯", 
                    "selete_type" : "1", 
                    "value" : "0/3"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "画钟正确（画出一个闭锁圆，指针位置准确），且能回忆出2-3个词", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "画钟错误（画的圆不闭锁，或指针位置不准确），或只回忆出0-1个词", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "已确诊为认知障碍，如老年痴呆", 
                            "score" : "2", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "认知功能", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无身体攻击行为（如打/踢/推/咬/抓/摔东西）和语言攻击行为（如骂人、语言威胁、尖叫）", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "每月有几次身体攻击行为，或每周有几次语言攻击行为", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "每周有几次身体攻击行为，或每日有语言攻击行为", 
                            "score" : "2", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "攻击行为", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "情绪低落、不爱说话、不爱梳洗、不爱活动", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "有自杀念头或自杀行为", 
                            "score" : "2", 
                            "serial_number" : 3
                        }
                    ], 
                    "project_name" : "抑郁症状", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "神志清醒，对周围环境警觉", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "嗜睡，表现为睡眠状态过度延长。当呼唤或推动其肢体时可唤醒，并能进行正确的交谈或执行指令，停止刺激后又继续入睡", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "昏睡，一般的外界刺激不能使其觉醒，给予较强烈的刺激时可有短时的意识清醒，醒后可简短回答提问，当刺激减弱后又很快进入睡眠状态", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "昏迷，处于浅昏迷时对疼痛刺激有回避和痛苦表情；处于深昏迷时对刺激无反应（若评定为昏迷，直接评定为重度失能，可不进行以下项目的评估）", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "意识水平", 
                    "selete_type" : "1", 
                    "value" : "1/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "能看清书报上的标准字体", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "能看清楚大字体，但看不清书报上的标准字体", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "视力有限，看不清报纸大标题，但能辨认物体", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "辨认物体有困难，但眼睛能跟随物体移动，只能看到光、颜色和形状", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "没有视力，眼睛不能跟随物体移动", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "视力", 
                    "selete_type" : "1", 
                    "value" : "1/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "可正常交谈，能听到电视、电话、门铃的声音", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "在轻声说话或说话距离超过2米时听不清", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "正常交流有些困难，需在安静的环静或大声说话才能听到", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "讲话者大声说话或说话很慢，才能部分听见", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "完全听不见", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "听力", 
                    "selete_type" : "1", 
                    "value" : "1/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "无困难，能于他人正常沟通交流", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "能够表达自己的需要及理解别人的话，但需要增加时间或给予帮助", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "表达需要或理解有困难，需频繁重复或简化口头表达", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "不能表达需要或理解他人的话", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "沟通交流", 
                    "selete_type" : "1", 
                    "value" : "1/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "能力完好：意识清醒，且视力和听力评为0或1，沟通评为0", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "轻度受损：意识清醒，但视力或听力中至少一项评为2，或沟通评为1", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "中度受损：意识清醒，但视力或听力中至少一项评为3，或沟通评为2； 或嗜睡，视力或听力评定为3及以下，沟通评定为2及以下", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "重度受损：意识清醒或嗜睡，但视力或听力中至少一项评为4，或沟通评为3；或昏睡/昏迷", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "感知觉与沟通分级", 
                    "selete_type" : "1", 
                    "value" : "2/3"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "除个人生活自理外（如饮食、洗漱、穿戴、二便），能料理家务（如做饭、洗衣）或当家管理事务", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "除个人生活自理外，能做家务，但欠好，家庭事务安排欠条理", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "个人生活能自理；只有在他人帮助下才能做些家务，但质量不好", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "个人基本生活事务能自理（如饮食、二便），在督促下可洗漱", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "个人基本生活事务（如饮食、二便）需要部分帮助或完全依赖他人帮助", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "生活能力", 
                    "selete_type" : "1", 
                    "value" : "4/5"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "原来熟练的脑力工作或体力技巧性工作可照常进行", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "原来熟练的脑力工作或体力技巧性工作能力有所下降", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "原来熟练的脑力工作或体力技巧性工作明显不如以往，部分遗忘", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "对熟练工作只有一些片段保留，技能全部遗忘", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "对以往的知识或技能全部磨灭", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "工作能力", 
                    "selete_type" : "1", 
                    "value" : "3/4"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "时间观念（年、月、日、时）清楚；可单独出远门，能很快掌握新环境的方位", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "时间观念有些下降，年、月、日清楚，但有时相差几天；可单独来往于近街，知道现住地的名称和方位，但不知回家路线", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "时间观念较差，年、月、日不清楚，可知上半年或下半年；只能单独在家附近行动，对现住地只知名称，不知道方位", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "时间观念很差，年、月、日不清楚，可知上午或下午；只能在左邻右舍间 串门，对现住地不知名称和方位 ", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "无时间观念；不能单独外出", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "时间/空间定向", 
                    "selete_type" : "1", 
                    "value" : "2/3"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "知道周围人们的关系，知道祖孙、叔伯、姑姨、侄子侄女等称谓的意义；可分辨陌生人的大致年龄和身份，可用适当称呼", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "只知家中亲密近亲的关系，不会分辨陌生人的大致年龄，不能称呼陌生人", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "只能称呼家中人，或只能照样称呼，不知其关系，不辨辈分", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "只认识常同住的亲人，可称呼子女或孙子女，可辨熟人和生人", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "只认识保护人，不辨熟人和生人", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "人物定向", 
                    "selete_type" : "1", 
                    "value" : "1/2"
                }
            }, 
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "参与社会，在社会环境有一定的适应能力，待人接物恰当", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "能适应单纯环境，主动接触人，初见面时难以让人发现智力问题，不能理解隐喻语参", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "脱离社会，可被动接触，不会主动待人，说话中很多不适当的词句，容易上当受骗", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "勉强与人交往，谈吐内容不清楚，表情不恰当", 
                            "score" : "3", 
                            "serial_number" : 4
                        }, 
                        {
                            "option_content" : "难以与人接触", 
                            "score" : "4", 
                            "serial_number" : 5
                        }
                    ], 
                    "project_name" : "社会交往能力", 
                    "selete_type" : "1", 
                    "value" : "0/1"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "能力完好", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "轻度失能", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "中度失能", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "重度失能", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "老年人能力初步等级", 
                    "selete_type" : "1", 
                    "value" : "3/4"
                }
            }
        ], 
        [
            {
                "ass_info" : {
                    "dataSource" : [
                        {
                            "option_content" : "能力完好", 
                            "score" : "0", 
                            "serial_number" : 1
                        }, 
                        {
                            "option_content" : "轻度失能", 
                            "score" : "1", 
                            "serial_number" : 2
                        }, 
                        {
                            "option_content" : "中度失能", 
                            "score" : "2", 
                            "serial_number" : 3
                        }, 
                        {
                            "option_content" : "重度失能", 
                            "score" : "3", 
                            "serial_number" : 4
                        }
                    ], 
                    "project_name" : "老年人能力最终等级", 
                    "selete_type" : "1", 
                    "value" : "3/4"
                }
            }
        ]
]


class CompetenceAssessmentService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_competence_assessment_list(self, org_list, condition, page, count):
        '''
            获取能力评估列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['id', 'elder', 'template_name',
                'start_date', 'end_date', 'user_name', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'elder', 'id', 'user')\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .match_bill(C('org.name').like(values['org_name']))\
               .add_fields({'user_name': '$user.name'})\
               .lookup_bill('PT_Assessment_Template', 'template', 'id', 'template')\
               .add_fields({'template_name': '$template.template_name', 'template_type': '$template.template_type'})
        if condition.get('is_nursing'):
            _filter.match(C('template.template_type') == '护理评估')

        _filter.match_bill((C('nursing_time') >= as_date(values['start_date']))
                           & (C('nursing_time') <= as_date(values['end_date']))
                           & (C('id') == values['id'])
                           & (C('elder') == values['elder'])
                           & (C('template_name') == values['template_name'])
                           & (C('user_name').like(values['user_name']))
                           & (C('organization_id').inner(org_list)))\
            .sort({'modify_date': -1})\
            .project({'_id': 0, 'org._id': 0, 'user._id': 0, 'template._id': 0})
        # print(_filter.filter_objects)
        res = self.page_query(
            _filter, 'PT_Behavioral_Competence_Assessment_Record', page, count)
        return res

    def get_competence_assessment_list_yh(self, org_list, condition, page, count):
        '''
            获取能力评估列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['id', 'elder', 'template_name',
                'start_date', 'end_date', 'user_name', 'org_name']
        values = self.get_value(condition, keys)

        # 查询长者（过滤长者姓名）
        elder_ids = []
        elder_data = {}
        if condition.get('user_name'):
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1') & (C('name').like(values['user_name'])))\
                .project({'_id': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])

        org_ids = []
        org_data = {}
        if condition.get('org_name'):
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('personnel_type') == '2') & (C('name').like(values['org_name'])))\
                .project({'_id': 0})
            res_org = self.query(
                _filter_org, 'PT_User')
            if len(res_org) > 0:
                for org in res_org:
                    org_data[org['id']] = org
                    org_ids.append(org['id'])

        # 查询模板
        template_ids = []
        template_data = {}
        if condition.get('template_name') or condition.get('is_nursing'):
            _filter_template = MongoBillFilter()
            if condition.get('is_nursing'):
                _filter_template.match(C('template_type') == '护理评估')
            if condition.get('template_name'):
                _filter_template.match_bill(
                    (C('name').like(values['template_name'])))
            _filter_template.project({'_id': 0, **get_common_project()})
            res_template = self.query(
                _filter_template, 'PT_Assessment_Template')
            if len(res_template) > 0:
                for template in res_template:
                    template_data[template['id']] = template
                    template_ids.append(template['id'])

        _filter = MongoBillFilter()
        if condition.get('user_name'):
            _filter.match_bill((C('elder').inner(elder_ids)))
        if condition.get('template_name') or condition.get('is_nursing'):
            _filter.match_bill((C('template').inner(template_ids)))
        _filter.match_bill((C('nursing_time') >= as_date(values['start_date']))
                           & (C('nursing_time') <= as_date(values['end_date']))
                           & (C('id') == values['id'])
                           & (C('elder') == values['elder'])
                           & (C('organization_id').inner(org_list)))
        if condition.get('org_name'):
            _filter.match_bill((C('organization_id').inner(org_ids)))
        _filter.sort({'modify_date': -1})
        _filter.project({'_id': 0, 'user._id': 0, 'template._id': 0, **get_common_project()})
        res = self.page_query(
            _filter, 'PT_Behavioral_Competence_Assessment_Record', page, count)
        for i, x in enumerate(res['result']):
            if 'user_name' not in condition.keys() and 'elder' in res['result'][i]:
                elder_ids.append(res['result'][i]['elder'])
            if 'org_name' not in condition.keys() and 'organization_id' in res['result'][i]:
                org_ids.append(res['result'][i]['organization_id'])
            if 'template_name' not in condition.keys() and 'is_nursing' not in condition.keys() and 'template' in res['result'][i]:
                template_ids.append(res['result'][i]['template'])
        if 'user_name' not in condition.keys():
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(elder_ids)))\
                .project({'_id': 0, 'name': 1, 'id': 1})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
        if 'org_name' not in condition.keys():
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('id').inner(org_ids)))\
                .project({'_id': 0})
            res_org = self.query(
                _filter_org, 'PT_User')
            if len(res_org) > 0:
                for org in res_org:
                    org_data[org['id']] = org
        if 'template_name' not in condition.keys() and 'is_nursing' not in condition.keys() and len(template_ids) > 0:
            _filter_template = MongoBillFilter()
            _filter_template.match_bill((C('id').inner(template_ids)))\
                .project({'_id': 0, **get_common_project()})
            res_template = self.query(
                _filter_template, 'PT_Assessment_Template')
            if len(res_template) > 0:
                for template in res_template:
                    template_data[template['id']] = template
        for i, x in enumerate(res['result']):
            if 'elder' in res['result'][i] and res['result'][i]['elder'] in elder_data.keys():
                res['result'][i]['user'] = elder_data[res['result'][i]['elder']]
                res['result'][i]['user_name'] = elder_data[res['result']
                                                           [i]['elder']]['name']
            if 'organization_id' in res['result'][i] and res['result'][i]['organization_id'] in org_data.keys():
                res['result'][i]['org_name'] = org_data[res['result']
                                                           [i]['organization_id']]['name']
            if 'template' in res['result'][i] and res['result'][i]['template'] in template_data.keys():
                res['result'][i]['template'] = template_data[res['result']
                                                             [i]['template']]
                if 'template_name' in res['result'][i]['template'].keys():
                    res['result'][i]['template_name'] = res['result'][i]['template']['template_name']
                if 'template_type' in res['result'][i]['template'].keys():
                    res['result'][i]['template_type'] = res['result'][i]['template']['template_type']
        return res

    def update_competence_assessment(self, competence_assessment):
        '''新增/修改能力评估

        Arguments:
            competence_assessment   {dict}      条件
        '''
        res = 'fail'
        # 转换日期格式
        if 'nursing_time' in competence_assessment.keys():
            competence_assessment['nursing_time'] = string_to_date(
                competence_assessment['nursing_time'])
        flag = OperationType.update.value if 'id' in list(
            competence_assessment.keys()) else OperationType.add.value
        competence_assessment = get_info(
            competence_assessment, self.session)
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.competenceAssessment.value, competence_assessment, 'PT_Behavioral_Competence_Assessment_Record')
        res = operation_result(bill_id)
        return res

    def delete_competence_assessment(self, competence_assessment_id_list):
        '''删除能力评估

        Arguments:
           competence_assessment_id_list   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(competence_assessment_id_list, str):
                ids.append(competence_assessment_id_list)
            else:
                ids = competence_assessment_id_list
            for competence_assessment_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Behavioral_Competence_Assessment_Record', {
                    'id': competence_assessment_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.competenceAssessment.value, data[0], 'PT_Behavioral_Competence_Assessment_Record')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_nursing_grade_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('name').like(values['name'])))\
            .sort({'modify_date': -1})\
            .project({'_id': 0, **get_common_project()})
        res = self.page_query(
            _filter, 'PT_Nursing_Grade', page, count)
        return res

    def update_nursing_grade(self, condition):
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.nursingGrade.value, condition, 'PT_Nursing_Grade')
        res = operation_result(bill_id)
        return res

    def delete_nursing_grade(self, ids):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(ids))
        res = self.query(_filter, 'PT_Nursing_Grade')
        bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                    TypeId.nursingGrade.value, res, ['PT_Nursing_Grade'])
        res = operation_result(bill_id)
        return res

    def get_nursing_content_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'nursing_content',
                'create_date', 'founder', 'nursing_level', 'org_name']
        values = self.get_value(condition, keys)
        start = N()
        end = N()
        if(bool(1-isinstance(values['create_date'], N))):
            start = values['create_date']+"T00:00:00.00Z"
            end = values['create_date']+"T23:59:59.00Z"
        _filter.match_bill((C('id') == values['id'])
                           & (C('create_date') > as_date(start))
                           & (C('create_date') < as_date(end))
                           & (C('nursing_level').inner(values['nursing_level']))
                           & (C('nursing_content').like(values['nursing_content']))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'founder', 'id', 'user')\
            .add_fields({'founder': '$user.name'})\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({'org_name': '$org.name'})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .project({
                'user.login_info': 0,
                'org.qualification_info': 0,
                **get_common_project({'user', 'org'})
            })
        res = self.page_query(
            _filter, 'PT_Nursing_Content', page, count)
        return res

    def update_nursing_content(self, condition):
        '''新增/修改护理内容'''
        res = 'Fail'
        condition['founder'] = get_current_user_id(self.session)
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.nursingGrade.value, condition, 'PT_Nursing_Content')
        res = operation_result(bill_id)
        return res

    def delete_nursing_content(self, ids):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(ids))
        res = self.query(_filter, 'PT_Nursing_Content')
        bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                    TypeId.nursingGrade.value, res, ['PT_Nursing_Content'])
        res = operation_result(bill_id)
        return res

    def update_nursing_record(self, condition):
        '''新增/修改护理记录'''
        res = 'Fail'
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
        bill_id = self.bill_manage_service.add_bill(
            flag, TypeId.nursingGrade.value, condition, 'PT_Nursing_Record')
        res = operation_result(bill_id)
        return res

    def get_nursing_record_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'elder_name', 'create_date', 'operator', 'org_name']
        values = self.get_value(condition, keys)
        start = N()
        end = N()
        if(bool(1-isinstance(values['create_date'], N))):
            start = values['create_date']+"T00:00:00.00Z"
            end = values['create_date']+"T23:59:59.00Z"
        _filter.match_bill((C('id') == values['id'])
                           & (C('create_date') > as_date(start))
                           & (C('create_date') < as_date(end))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'elder', 'id', 'user')\
            .add_fields({'elder_name': '$user.name'})\
            .lookup_bill('PT_User', 'operator', 'id', 'operator_info')\
            .add_fields({'operator_name': '$operator_info.name'})\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({'org_name': '$org.name'})\
            .match_bill((C('elder_name').like(values['elder_name'])) & (C('operator').like(values['operator'])) & (C('org_name').like(values['org_name'])))\
            .project({
                'user.login_info': 0,
                'operator_info._id': 0,
                'org.qualification_info': 0,
                **get_common_project({'user', 'operator_info', 'org'})
            })
        res = self.page_query(
            _filter, 'PT_Nursing_Record', page, count)
        return res

    def delete_nursing_record(self, ids):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(ids))
        res = self.query(_filter, 'PT_Nursing_Record')
        bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                    TypeId.nursingGrade.value, res, ['PT_Nursing_Record'])
        res = operation_result(bill_id)
        return res
    

    def import_pinggu_record(self, data):
        res = '导入失败'
        # # 当前账号所属组织机构id
        # current_org_id = get_current_organization_id(self.session)
        # 获取excel导入数据
        pf = pd.DataFrame(data)
        im_data = dataframe_to_list(pf)
        #  获取导入身份证
        id_card_arr = []
        chuli_data = {}
        for p in im_data:
            id_card_arr.append(p['身份证'])
            chuli_data[p['身份证']] = p
        #  查询导入的长者在系统是否存在
        _filter = MongoBillFilter()
        _filter.match_bill(C('id_card').inner(id_card_arr))
        user_res = self.query(_filter, 'PT_User')
        insert_list = []
        if len(user_res) > 0:
            for user in user_res:
                new_list = tem_list
                new_list[1][0]['ass_info']['value']=chuli_data[user['id_card']]['信息提供者姓名']
                new_list[1][2]['ass_info']['value']=chuli_data[user['id_card']]['联系人姓名']
                new_list[1][3]['ass_info']['value']=chuli_data[user['id_card']]['联系人电话']
                # 每条评估数据的模板
                im_record = {
                'template_list': new_list,
                'nursing_level': chuli_data[user['id_card']]['护理等级'],
                'elder': user['id'],
                'template': '11c453d6-0d1b-11ea-ac6d-005056882303',
                'nursing_time': datetime.datetime.now(),
                'total_score': 73,
                'organization_id' : user['organization_id'],
                }
                insert_list.append(im_record)
                # 获取系统上不存在档案的长者
                if user['id_card'] in id_card_arr:
                    id_card_arr.remove(user['id_card'])
        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.competenceAssessment.value, insert_list, 'PT_Behavioral_Competence_Assessment_Record')
        if bill_id:
            res = '导入成功' 
        # new_list = tem_list
        # print(id_card_arr,666)
        if len(id_card_arr) > 0:
            return ','.join(str(i) for i in id_card_arr) + '，上述长者在系统上不存在档案，请先新建档案再导入。'
        return res

    
