'''
版权：Copyright (c) 2019 China

创建日期：Wednesday November 6th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Wednesday, 6th November 2019 4:36:53 pm
修改者: ymq(ymq) - <<email>>

说明
 1、常量枚举类
'''
# 平台的user_id
plat_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'


class AccountType():
    '''账户类型枚举'''
    account_recharg_app = 'APP储值账户'
    account_recharg_wel = '机构储值账户'
    account_subsidy = '补贴账户'
    account_charitable = '慈善账户'
    account_real = '真实账户'


class AccountStatus():
    '''账户状态枚举'''
    normal = '正常'
    frozen = '冻结'
    cancel = '注销'


class PayType():
    '''支付方式枚举'''
    bank = '银行划扣'
    recharge_app = 'APP储值账户划扣'
    recharge_wel = '机构储值账户划扣'
    subsidy = '补贴账户划扣'
    charitable = '慈善账户划扣'
    wechart = '微信支付'
    alipay = '支付宝支付'
    other = '其他'


class PayStatue():
    '''支付状态'''
    ready_pay = '待付款'  # 待支付
    success = '成功'
    fail = '失败'


class SubsidyRecordState():
    '''补贴状态'''
    valid = '生效'
    invalid = '失效'


class SubsidyRecordSubsidyType():
    '''补贴类型'''
    Seniors = '长者补贴'
    Post = '岗位补贴'
    Employment = '就业补贴'
    Charitable = '慈善补贴'


class UserType:
    ''' 人员类型
    Attribute:
        Personnel           人员
        Organizational      组织机构
    '''
    Personnel = '1'
    Organizational = '2'


class UserCategory:
    ''' 人员类别 

        Attribute
        Elder       长者
        StaffMember     工作人员
        Family      家属
        PlatformAdministrator  平台管理员
    '''
    Elder = '长者'
    StaffMember = '工作人员'
    Family = '家属'
    PlatformAdministrator = '平台管理员'


class ApplyState:
    ''' 服务人员审核状态
    '''
    success = '通过'
    fail = '不通过'
    apply = '已申请'


class FollowCollectionObject:
    ''' 关注收藏对象 '''
    welfare = '养老机构'
    community = '社区幸福院'
    provider = '居家服务商'
    news = '养老资讯'
    activity = '活动'
    service = '居家服务'


class FollowCollectionType:
    ''' 关注收藏类型 '''
    follow = 'follow'  # 关注
    collection = 'collection'  # 收藏
    like = 'like'  # 点赞
    likes = 'likes'  # 资讯用的点赞
    share = 'shares'  # 资讯用的分享
    forward = 'forward'  # 转发


class AccountingConfirm:
    ''' 服务商核算状态'''
    accounting_no = '待结算'  # 待结算
    confirming = '确认中'  # 确认中
    settled = '已结算'  # 已结算


class Classification:
    government_subsidies = '政府补贴'
    own_expense = '自费'
