from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
import requests
import json
import hashlib

from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, gcj02_to_bd09, wgs84_to_gcj02, post_requests, bd09_to_gcj02
import time
from urllib.request import urlopen, quote

import math

post_url = 'http://api.aiqiangua.com:8888/'
user_name = '18680006869'
user_pwd = 'Abc123456'


class ByHardwareService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Connection': 'keep-alive',
            'Accept': '*/*',
            'Cookie': 'user=2|1:0|10:1603771062|4:user|16:MTg2ODAwMDY4Njk=|426b5667300cf65ae24b8e468dcba0829eac5d9c4258055ec6b863a1e54e00f9; Expires=Wed, 28-Oct-2020 03:57:42 GMT; Max-Age=86400; Path=/',
        }

    def get_position(self, imei_id):
        ''' 获取实时定位 '''

        url = post_url + 'api/device/' + imei_id + '/data/locationdata/'

        res_location = requests.get(url, headers=self.headers)

        res_json = res_location.json()

        if res_json.get('success') == True:
            points = res_json.get('obj').get(
                'locationdata').get('point').get('coordinates')

            bd_lng, bd_lat = gcj02_to_bd09(points[0], points[1])

            return {
                'code': 0,
                'address': res_json.get('obj').get('locationdata').get('address'),
                'lng': bd_lng,
                'lat': bd_lat,
            }
        else:
            return {
                'code': 500,
                'msg': '获取失败！'
            }

    def get_footprint(self, imei_id):
        ''' 查看设备定位数据 '''

        url = post_url + 'api/locationdata/?device=' + imei_id

        res_footprint = requests.get(url, headers=self.headers)

        res_json = res_footprint.json()

        if res_json.get('success') == True:
            objs = res_json.get('objs')
            points = []

            for item in objs:
                coordinates = item.get('point').get('coordinates')
                bd_lng, bd_lat = gcj02_to_bd09(coordinates[0], coordinates[1])
                points.append({
                    'address': item.get('address'),
                    'lng': bd_lng,
                    'lat': bd_lat,
                })

            return {
                'code': 0,
                'points': points,
            }
        else:
            return {
                'code': 500,
                'msg': '获取失败！'
            }

    def set_fence(self, imei_id, points):
        ''' 设置电子围栏 '''

        gd_point = []

        # 把百度坐标点转换为高德坐标
        for item in points:
            bd_lng, bd_lat = bd09_to_gcj02(item['lng'], item['lat'])
            gd_point.append({
                "lng": str(bd_lng),
                "lat": str(bd_lat)
            })

        # 构造安全区域坐标字符串
        safe_area = gd_point[0]['lat'] + ',' + gd_point[0]['lng'] + ';' + gd_point[1]['lat'] + ',' + gd_point[1]['lng'] + ';' + gd_point[2]['lat'] + \
            ',' + gd_point[2]['lng'] + ';' + gd_point[3]['lat'] + ',' + \
            gd_point[3]['lng'] + ';' + \
            gd_point[0]['lat'] + ',' + gd_point[0]['lng']

        url = post_url + 'api/device/' + imei_id + '/fences/1/'

        condition = {
            'name': 'test',
            'safe_area': safe_area,
        }

        res_fence = requests.post(json=condition, url=url,
                                  headers=self.headers)

        res_json = res_fence.json()

        return res_json
