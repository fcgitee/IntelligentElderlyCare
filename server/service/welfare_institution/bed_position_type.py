# -*- coding: utf-8 -*-

'''
床位类别函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date


class BedPositionTypeService(MongoService):
    ''' 床位类别服务 '''

    def __init__(self, db_addr, db_port, db_name, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, inital_password, session)

    def get_bed_position_type_list(self, condition, page, count):
        '''获取床位类别列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoFilter()
        _filter.match((C('id') == values['id']))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Bed_Position_Type', page, count)
        return res

    def update_bed_position_type(self, bed_position_type):
        '''# 新增/修改床位类别'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in bed_position_type.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.bedPositionType.value, bed_position_type, 'PT_Bed_Position_Type')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.bedPositionType.value, bed_position_type, 'PT_Bed_Position_Type')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name, process_func,self.db_user, self.db_pwd)
        return res

    def delete_organizational(self, bed_position_type_ids):
        '''删除床位类别接口

        Arguments:
            bed_position_type_ids   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(bed_position_type_ids, str):
                ids.append(bed_position_type_ids)
            else:
                ids = bed_position_type_ids
            for bed_position_type_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Bed_Position_Type', {
                    'id': bed_position_type_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.bedPositionType.value, data[0], 'PT_Bed_Position_Type')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name, process_func,self.db_user, self.db_pwd)
        return res
