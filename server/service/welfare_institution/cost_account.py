from ...pao_python.pao.data import get_string_to_date, dataframe_to_list, get_string_to_date, get_first_last_day_month, DataProcess, string_to_date_only, data_to_string_date
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...pao_python.pao.commom import get_data
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.mongo_bill_service import MongoBillFilter, as_date
from ...service.welfare_institution.accommodation_process import AccommodationProcess
from ...service.buss_mis.service_operation import ServiceOperationService
from ...service.common import get_current_organization_id, get_current_user_id, get_random_id, get_common_project
import datetime
from ...service.welfare_institution.accommodation_process import CheckInStatus
import pandas as pd
from ...pao_python.pao.remote import JsonRpc2Error
import time
from ...service.app.my_order import RecordStatus
from .finance_class import Bill, BillStatus
import copy
from ...service.security_module import FunctionName


class CostAccount(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.accommodation_process = AccommodationProcess(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.service_operation_func = ServiceOperationService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_cost_account_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'start_date', 'end_date', 'name', 'cost_status', 'state']
        values = self.get_value(condition, keys)
        if condition.get('start_date') and condition['start_date'] != '':
            _filter.match_bill((C('create_date')) >=
                               as_date(values['start_date']))
        if condition.get('end_date') and condition['end_date'] != '':
            condition['end_date'] = get_string_to_date(
                condition['end_date']) + datetime.timedelta(days=1)
            _filter.match_bill((C('end_date')) < as_date(values['start_date']))
        _filter.match_bill((C('organization_id').inner(org_list)) & (C('cost_status') == None)) \
            .inner_join_bill('PT_Service_Record', 'id', 'id', 'recode')\
               .inner_join_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
               .inner_join_bill('PT_Service_Item', 'product.service_item_id', 'id', 'item') \
               .project({'product.id': 0, 'item.id': 0})\
               .add_fields({'new_recode': self.ao.merge_objects(['$recode', '$product', '$item'])}) \
               .group({'order_id': '$order_id'}, [{'recode': self.ao.push('$new_recode')}]) \
               .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order') \
               .inner_join_bill('PT_User', 'order.purchaser_id', 'id', 'user') \
               .match(C('user.name').like(values['name']))\
               .lookup_bill('PT_Bed', 'order.purchaser_id', 'residents_id', 'bed')\
               .project({'_id': 0, 'recode._id': 0, 'product._id': 0, 'item._id': 0, 'new_recode._id': 0, 'user._id': 0, 'order._id': 0, 'bed._id': 0, 'check._id': 0})
        res = self.page_query(_filter, 'PT_Service_Record', page, count)
        return res

    def cost_account(self, condition, select_date, service_operation_func):
        data = get_data(self, 'PT_Service_Record', {}, isList=False)
        flag = OperationType.add.value
        unCostId = []
        query_condition = {}
        res = ''
        # 判断是否该订单已经存在记录，若不存在，则调用接口生成记录
        if len(data['result']):
            ids = data['result']['order_id'].values
            for x in condition:
                if not x in ids:
                    unCostId.append(x)

            self.date_key = [['start_date', 'end_date']]
            date = get_string_to_date(select_date)
            format_date = get_first_last_day_month(date.year, date.month)
            query_condition['start_date'] = get_string_to_date(format_date[0])
            query_condition['end_date'] = get_string_to_date(format_date[1])
            if len(unCostId):
                # 计算水电总数
                values = self.get_value({'ids': unCostId}, ['ids'])
                _filter = MongoBillFilter()
                _filter.lookup_bill('PT_Bed', 'purchaser_id', 'residents_id', 'bed') \
                       .lookup_bill('IEC_Hydropower', 'bed.area_id', 'hotel_id', 'hydropower') \
                       .lookup_bill('PT_Accommodation_Record', 'purchaser_id', 'residents_id', 'recode') \
                       .match_bill((C('id').inner(values['ids']))) \
                       .add_fields({
                           'hydropower_num': self.ao.array_filter('$hydropower', 'recode_', ((F('$recode_.start_date') >= query_condition['start_date']) & (F('$recode_.end_date') <= query_condition['end_date'])).f),
                           'recode_list': self.ao.array_filter('$recode', 'up_', ((F('$up_.checkin_date') >= query_condition['start_date']) | (F('$up_.checkout_date') <= query_condition['end_date'])).f)
                       }) \
                    .add_fields({
                        'electricity_number': {'$sum': '$hydropower_num.electricity_number'},
                        'water_number': {'$sum': '$hydropower_num.water_number'}
                    }) \
                    .project({'_id': 0, 'bed._id': 0, 'hydropower._id': 0, 'hydropower_num._id': 0, 'recode._id': 0, 'recode_list._id': 0})

                res = self.page_query(_filter, 'PT_Service_Order', None, None)
                options = self.__get_accommodation_days(res, query_condition)
                res = []
                table_name = []
                for x in options:
                    data_recode = self.accommodation_process.get_item_info(
                        '住宿', x['id'], x['options'])
                    data_recode.update(
                        start_date=x['checkin_date'], end_date=x['end_date'])
                    res.update(data_recode)
                    recode = self.service_operation_func.add_service_record(
                        data_recode['id'], data_recode['service_product_id'], '0', data_recode)
                    res.append(recode)
                    table_name.append('PT_Service_Record')
                bill_id = self.bill_manage_server.add_bill(
                    flag, TypeId.hydropower.value, data, table_name)
        return 'Success' if bill_id else 'Fail'

    def __get_accommodation_days(self, result, condition):
        days = 0
        options = []
        for res in result['result']:
            for recode in res['recode_list']:
                # 跨月计算
                if recode['checkin_date'] < condition['start_date']:
                    # 若有请假，该记录的实际入住实际为1号到checkout_date
                    days = days + \
                        (recode['checkout_date'].day) if recode['checkout_date'] else (
                            condition['end_date'].day)
                else:
                    days = days + (recode['checkout_date'].day - recode['checkin_date'].day) if recode['checkout_date'] else (
                        condition['end_date'].day - recode['checkin_date'].day)
            options.append({
                'id': res['id'],
                'start_date': recode['checkin_date'],
                'end_date': recode['checkout_date'],
                'options': [{
                    '住宿天数': days,
                    '电费度数': res['electricity_number'],
                    '水费度数':res['water_number']
                }]

            })
        return options

    def get_receivables_yh_list(self, org_list, condition, page, count):
        '''优化后的预收款账单列表查询接口'''
        _filter = MongoBillFilter()
        keys = ['user_id', 'check_status', 'user_name',
                'type', 'user_type', 'start_date', 'end_date']
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        values = self.get_value(condition, keys)
        # 查询长者类型（过滤长者类型）
        elder_type_ids = N()
        elder_type_data = {}
        if condition.get('user_type'):
            elder_type_ids = []
            _filter_elder_type = MongoBillFilter()
            _filter_elder_type.match_bill((C('id') == values['user_type']))\
                .project({**get_common_project()})
            res_elder_type = self.query(
                _filter_elder_type, 'PT_Personnel_Classification')
            if len(res_elder_type) > 0:
                for elder_type in res_elder_type:
                    elder_type_data[elder_type['id']] = elder_type
                    elder_type_ids.append(elder_type['id'])

        # 查询长者（过滤长者姓名）
        elder_ids = []
        elder_data = {}
        if condition.get('user_name') or condition.get('user_type'):
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('name').like(values['user_name'])) & (C('personnel_info.personnel_classification').inner(elder_type_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])

        if len(elder_ids) > 0:
            _filter.match_bill((C('user_id').inner(elder_ids)))
        _filter.match_bill(
            (C('user_id') == values['user_id'])
            & (C('organization_id').inner(org_list))
            & (C('check_status') == values['check_status'])
            & (C('type') == BillStatus.advance.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .project({**get_common_project()})
        res = self.page_query(_filter, 'PT_Financial_Bill', page, count)
        order_ids = []
        order_record_ids = []
        for i, x in enumerate(res['result']):
            order_ids.append(res['result'][i]['order_id'])
            order_record_ids = order_record_ids + \
                res['result'][i]['recode_list']
            if 'user_name' not in condition.keys():
                elder_ids.append(res['result'][i]['user_id'])
        if 'user_name' not in condition.keys():
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(elder_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
        # 查询订单（最后查）
        order_data = {}
        _filter_order = MongoBillFilter()
        _filter_order.match_bill((C('id').inner(order_ids)))\
            .project({**get_common_project()})
        res_order = self.query(
            _filter_order, 'PT_Service_Order')
        if len(res_order) > 0:
            for order in res_order:
                order_data[order['id']] = order
                # order_ids.append(order['id'])

        # 查询服务记录（最后查）
        order_record_data = {}
        _filter_order_record = MongoBillFilter()
        _filter_order_record.match_bill((C('id').inner(order_record_ids)))\
            .project({**get_common_project()})
        res_order_record = self.query(
            _filter_order_record, 'PT_Service_Record')
        if len(res_order_record) > 0:
            for order_record in res_order_record:
                order_record_data[order_record['id']] = order_record
                # order_record_ids.append(order_record['id'])
        user_classification_ids = []
        for i, x in enumerate(res['result']):
            if 'order_id' in res['result'][i] and res['result'][i]['order_id'] in order_data.keys():
                res['result'][i]['order'] = order_data[res['result'][i]['order_id']]
            if 'recode_list' in res['result'][i]:
                # print('order_record_data>>>>', order_record_data)
                records = []
                for record_id in res['result'][i]['recode_list']:
                    if order_record_data and order_record_data.get(record_id):
                        records.append(order_record_data[record_id])
                res['result'][i]['recode'] = records
            if 'user_id' in res['result'][i] and res['result'][i]['user_id'] in elder_data.keys():
                res['result'][i]['user'] = elder_data[res['result'][i]['user_id']]
                if 'user_type' not in condition.keys():
                    user_classification_ids.append(
                        elder_data[res['result'][i]['user_id']]['personnel_info']['personnel_classification'])
        if len(user_classification_ids) > 0:
            _filter_elder_type = MongoBillFilter()
            _filter_elder_type.match_bill((C('id').inner(user_classification_ids)))\
                .project({**get_common_project()})
            res_elder_type = self.query(
                _filter_elder_type, 'PT_Personnel_Classification')
            if len(res_elder_type) > 0:
                elder_type_ids = []
                for elder_type in res_elder_type:
                    elder_type_data[elder_type['id']] = elder_type
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                result[i]['class'] = {}
                if 'user' in res['result'][i] and res['result'][i]['user']['personnel_info']['personnel_classification'] in elder_type_data.keys():
                    result[i]['class'] = elder_type_data[res['result']
                                                         [i]['user']['personnel_info']['personnel_classification']]
                product = {}
                for y in x['recode']:
                    product_name = y['service_product_id']
                    product_value = y['valuation_amount']
                    product[product_name] = product_value
                result[i]['total'] = result[i]['amount'] + result[i]['balance']
                result[i].update(product)
        return res

    def get_actual_receivables_yh_list(self, org_list, condition, page, count):
        keys = ['user_name', 'start_date', 'end_date', 'user_type']
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        _filter = MongoBillFilter()
        values = self.get_value(condition, keys)
        # 查询长者类型（过滤长者类型）
        elder_type_ids = N()
        elder_type_data = {}
        if condition.get('user_type'):
            elder_type_ids = []
            _filter_elder_type = MongoBillFilter()
            _filter_elder_type.match_bill((C('id') == values['user_type']))\
                .project({**get_common_project()})
            res_elder_type = self.query(
                _filter_elder_type, 'PT_Personnel_Classification')
            if len(res_elder_type) > 0:
                for elder_type in res_elder_type:
                    elder_type_data[elder_type['id']] = elder_type
                    elder_type_ids.append(elder_type['id'])

        # 查询长者（过滤长者姓名）
        elder_ids = []
        elder_data = {}
        if condition.get('user_name') or condition.get('user_type'):
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('name').like(values['user_name'])) & (C('personnel_info.personnel_classification').inner(elder_type_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])

        if len(elder_ids) > 0:
            _filter.match_bill((C('user_id').inner(elder_ids)))
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('type') == BillStatus.actual.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .project({**get_common_project()})
        res = self.page_query(_filter, BillStatus.name.value, page, count)
        for i, x in enumerate(res['result']):
            if 'user_name' not in condition.keys():
                elder_ids.append(res['result'][i]['user_id'])
        if 'user_name' not in condition.keys():
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(elder_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder

        user_classification_ids = []
        for i, x in enumerate(res['result']):
            if 'user_id' in res['result'][i] and res['result'][i]['user_id'] in elder_data.keys():
                res['result'][i]['user'] = elder_data[res['result'][i]['user_id']]
                if 'user_type' not in condition.keys():
                    user_classification_ids.append(
                        elder_data[res['result'][i]['user_id']]['personnel_info']['personnel_classification'])
        if len(user_classification_ids) > 0:
            _filter_elder_type = MongoBillFilter()
            _filter_elder_type.match_bill((C('id').inner(user_classification_ids)))\
                .project({**get_common_project()})
            res_elder_type = self.query(
                _filter_elder_type, 'PT_Personnel_Classification')
            if len(res_elder_type) > 0:
                elder_type_ids = []
                for elder_type in res_elder_type:
                    elder_type_data[elder_type['id']] = elder_type
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                product = {}
                result[i]['class'] = {}
                if 'user' in res['result'][i] and res['result'][i]['user']['personnel_info']['personnel_classification'] in elder_type_data.keys():
                    result[i]['class'] = elder_type_data[res['result']
                                                         [i]['user']['personnel_info']['personnel_classification']]
                if 'record_ss' in x:
                    for y in x['record_ss']:
                        product_name = y['service_product_id']
                        product_value = y['valuation_amount']
                        product[product_name] = product_value
                    result[i].update(product)
                    result[i]['total'] = result[i]['amount'] + \
                        result[i]['reduction']  # 实收金额
        return res

    def get_difference_detail_yh_list(self, org_list, condition, page, count):
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        _filter = MongoBillFilter()
        keys = ['user_id', 'check_status', 'user_name',
                'type', 'start_date', 'end_date']
        values = self.get_value(condition, keys)
        # 查询长者（过滤长者姓名）
        elder_ids = []
        elder_data = {}
        if condition.get('user_name'):
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('name').like(values['user_name'])))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])

        if len(elder_ids) > 0:
            _filter.match_bill((C('user_id').inner(elder_ids)))
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('check_status') == values['check_status'])
            & (C('type') == BillStatus.actual.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .project({**get_common_project()})
        res = self.page_query(_filter, BillStatus.name.value, page, count)
        for i, x in enumerate(res['result']):
            if 'user_name' not in condition.keys():
                elder_ids.append(res['result'][i]['user_id'])
        if 'user_name' not in condition.keys():
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(elder_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                if 'user_id' in result[i] and result[i]['user_id'] in elder_data.keys():
                    result[i]['user'] = elder_data[result[i]['user_id']]
                product = {}
                for y in x['difference']:
                    product_name = y['service_product_id']
                    product_value = y['valuation_amount']
                    product[product_name] = product_value
                result[i].update(product)
                result[i]['total'] = result[i]['amount'] + \
                    result[i]['reduction']
                result[i]['receive'] = result[i]['amount'] - \
                    result[i]['balance']  # 应收款金额
        return res

    def get_receivables_list(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['user_id', 'check_status', 'user_name',
                'type', 'user_type', 'start_date', 'end_date', 'org_name']
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        values = self.get_value(condition, keys)
        # .lookup_bill('PT_Personnel_Classification',
        #              'user.personnel_info.personnel_classification', 'id', 'class')\
        # .inner_join_bill('IEC_Check_In', 'user_id', 'user_id', 'check_in')\
        # .lookup_bill('PT_Bed', 'user_id', 'residents_id', 'bed')\

        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('check_status') == values['check_status'])
            & (C('type') == BillStatus.advance.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .match_bill(C('user.name').like(values['user_name']))\
            .lookup_bill('PT_Personnel_Classification',
                         'user.personnel_info.personnel_classification', 'id', 'class')\
            .match_bill(C('class.id') == values['user_type'])\
            .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .lookup_bill('PT_Service_Record', 'recode_list', 'id', 'recode')\
            .match_bill(C('user.id') == values['user_id'])\
            .project({'_id': 0, 'user._id': 0, 'order._id': 0, 'bed._id': 0, 'check_in._id': 0, 'recode._id': 0, 'class._id': 0})
        res = self.page_query(_filter, 'PT_Financial_Bill', page, count)
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                product = {}
                for y in x['recode']:
                    product_name = y['service_product_id']
                    product_value = y['valuation_amount']
                    product[product_name] = product_value
                result[i]['total'] = result[i]['amount'] + result[i]['balance']
                result[i].update(product)
        return res

    def get_actual_receivables_list(self, org_list, condition, page, count):
        keys = ['user_name', 'start_date', 'end_date', 'user_type', 'org_name']
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        _filter = MongoBillFilter()
        # .inner_join_bill('IEC_Check_In', 'user_id', 'user_id', 'check_in')\
        # .lookup_bill('PT_Bed', 'user_id', 'residents_id', 'bed')\

        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('type') == BillStatus.actual.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_Personnel_Classification',
                         'user.personnel_info.personnel_classification', 'id', 'class')\
            .match_bill(C('class.id') == values['user_type'])\
            .match_bill(
                (C('user.name').like(values['user_name']))
        )\
            .lookup_bill('PT_User',
                         'organization_id', 'id', 'org')\
            .add_fields({'org_name': '$org.name'})\
            .match_bill(C('org_name').like(values['org_name']))\
            .project({'_id': 0, 'user._id': 0, 'order._id': 0, 'bed._id': 0, 'check_in._id': 0, 'recode._id': 0, 'class._id': 0, 'org._id': 0})
        res = self.page_query(_filter, BillStatus.name.value, page, count)
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                product = {}
                for y in x['recode_list']:
                    product_name = y['service_product_id']
                    product_value = y['valuation_amount']
                    product[product_name] = product_value
                result[i].update(product)
                result[i]['total'] = result[i]['amount'] + \
                    result[i]['reduction']  # 实收金额
        return res

    def get_difference_detail_list(self, org_list, condition, page, count):
        if condition.get('date'):
            condition['start_date'] = string_to_date_only(
                condition['date'][0].split('T')[0])
            condition['end_date'] = string_to_date_only(
                condition['date'][1].split('T')[0])
        _filter = MongoBillFilter()
        keys = ['user_id', 'check_status', 'user_name',
                'type', 'start_date', 'end_date', 'org_name']
        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('check_status') == values['check_status'])
            & (C('type') == BillStatus.actual.value)
            & (values['start_date'] <= C('start_date'))
            & (values['end_date'] >= C('end_date'))
        )\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({'org_name': '$org.name'})\
            .match_bill(
                (C('user.name').like(values['user_name']))
                (C('org_name').like(values['org_name']))
                & (C('user.id') == values['user_id']))\
            .project({'_id': 0, 'user._id': 0, 'org._id': 0})
        res = self.page_query(_filter, BillStatus.name.value, page, count)
        if res['result']:
            result = res['result']
            for i, x in enumerate(result):
                product = {}
                for y in x['difference']:
                    product_name = y['service_product_id']
                    product_value = y['valuation_amount']
                    product[product_name] = product_value
                result[i].update(product)
                result[i]['total'] = result[i]['amount'] + \
                    result[i]['reduction']
                result[i]['receive'] = result[i]['amount'] - \
                    result[i]['balance']  # 应收款金额
        return res

    def add_receivables_data(self, condition, date, flag=True):
        data_bill = []  # 账单
        data_recode = []
        # 合计应收费用 = 应收费用 + 差额
        _filter = MongoBillFilter()
        start_date = string_to_date_only(date[0])
        end_date = string_to_date_only(date[1])
        cur_organization_id = get_current_organization_id(
            self.session)  # 当前组织机构id
        # 是否预收，通常情况下是预收，但退住时或者刚入住在核算时间内按实收，单独选择长者的话，不是入住，就是退住
        _filter.match_bill(C('organization_id') == cur_organization_id)\
            # 部分长者单独核算，可能包含已退住的长者(会把该长者以前的入住订单也算出来？？？)
        if len(condition):
            _filter.match_bill(C('user_id').inner(condition))
        else:  # 所有居住中长者进行核算
            _filter.match_bill(C('state') == CheckInStatus.check_in.value)
        _filter.inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_Personnel_Classification', 'user.personnel_info.personnel_classification', 'id', 'class')\
               .inner_join('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'name': '$user.name', 'type': '$class.type', 'user_id': '$user.id'})\
            .project({'_id': 0, 'recode._id': 0, 'user._id': 0, 'order._id': 0, 'class._id': 0, })
        res = self.query(_filter, 'IEC_Check_In')
        if not len(res):
            raise JsonRpc2Error(-12501, '找不到长者入住订单')
        # 计算床位，护理费服务记录
        all_bed_nursing_recode = self.__advance_bed_nursing_fee(
            res, start_date, end_date, flag)
        # 计算其他费用
        other_fee = self.__get_other_cost(res, start_date, end_date)
        all_recode = all_bed_nursing_recode + other_fee
        # 计算每个人的应收费用
        all_user_fee_pf = pd.DataFrame(all_recode)
        all_user_fee_pf['id'] = all_user_fee_pf.apply(
            lambda x: get_random_id(), axis=1)
        all_fee_pf = all_user_fee_pf[[
            'order_id', 'valuation_amount', 'user_id']]
        all_fee = dataframe_to_list(all_fee_pf.groupby(
            ['order_id', 'user_id'], as_index=False).sum())
        # 判断该时间内是否已经核算过了，
        # 1.核算且已发送：生成新的账单和服务记录，合计金额要抵扣掉之前的账单金额
        # 2.未核算：覆盖原来的账单和服务记录，账单状态为未审核(删除原来的数据，重新添加)
        for user_fee in all_fee:  # 核算每个长者
            user_id = user_fee['user_id']
            user_fee['pay_type'] = self.__get_pay_type(user_id, res)
            # 获取上次核算剩下的余额
            _filter_balance = MongoBillFilter()
            _filter_balance.match_bill(
                (C('user_id') == user_id)
                & (C('type') == BillStatus.actual.value))\
                .sort({'end_date': -1})\
                .skip(1)\
                .project({'_id': 0})
            balance_bill = self.query(
                _filter_balance, BillStatus.name.value)
            balance = balance_bill[0]['balance'] + \
                balance_bill[0]['reduction'] if len(balance_bill) else 0
            # 当前长者的服务记录和id
            user_pf = all_user_fee_pf[all_user_fee_pf['user_id'] == user_id]
            user_recode = dataframe_to_list(user_pf)
            for recode in user_recode:  # 时间格式错误，需转格式
                recode.update(start_date=start_date, end_date=end_date)
            recode_ids = user_pf['id'].tolist()  # 新的服务记录id
            # 查找未发送账单
            unsend_filter = MongoBillFilter()
            unsend_filter.match_bill(
                (C('user_id') == user_id)
                & (C('start_date') >= start_date)
                & (C('start_date') <= end_date)
                & (C('send_status') == BillStatus.unsend.value)
            )\
                .unwind('recode_list')\
                .inner_join_bill('PT_Service_Record', 'recode_list', 'id', 'recode')\
                .project({'_id': 0, 'recode._id': 0})
            unsend_bill = self.query(
                unsend_filter, BillStatus.name.value)
            if len(unsend_bill):  # 存在未发送账单，删除
                unsend_pf = pd.DataFrame(unsend_bill)
                del_recode = unsend_pf['recode'].tolist()
                self.bill_manage_service.add_bill(
                    OperationType.delete.value, TypeId.bill.value, [unsend_bill[0], del_recode], [BillStatus.name.value, 'PT_Service_Record'])
            # 查找已发送账单,合并求和
            send_filter = MongoBillFilter()
            send_filter.match_bill(
                (C('user_id') == user_id)
                & (C('start_date') >= start_date)
                & (C('start_date') <= end_date)
                & (C('send_status') == BillStatus.send.value)
            )\
                .unwind('recode_list')\
                .inner_join_bill('PT_Service_Record', 'recode_list', 'id', 'recode')\
                .add_fields({'valuation_amount': '$recode.valuation_amount', 'service_product_id': '$recode.service_product_id'})\
                .group({'user_id': '$user_id', 'service_product_id': '$service_product_id'},
                       [
                    {'valuation_amount': self.ao.summation(
                        '$valuation_amount')},
                    {'amount': self.ao.summation('$amount')}
                ])\
                .project({'_id': 0, 'recode._id': 0})
            send_bill = self.query(send_filter, BillStatus.name.value)
            if len(send_bill):
                # 当前生成的账单金额要扣除上次已发送的账单金额，服务记录也是，
                # 比如：第一次账单金额3000已发送，不可修改，过几天再次结算金额为3500，此时生成的新账单应该体现为500，而不是3500
                user_fee['valuation_amount'] = user_fee['valuation_amount'] - \
                    send_bill[0]['amount']  # 本次账单金额
                for fee in send_bill:
                    for recode in user_recode:
                        if fee['service_product_id'] == recode['service_product_id']:
                            recode['valuation_amount'] = recode['valuation_amount'] - \
                                fee['valuation_amount']
            user_fee.update(start_date=start_date, end_date=end_date,
                            balance=balance, type=BillStatus.advance.value, recode_list=recode_ids)
            data_bill.append(Bill(user_fee).to_dict())
            data_recode = data_recode + user_recode
        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.bill.value, [data_bill, data_recode], [BillStatus.name.value, 'PT_Service_Record'])
        return '成功' if bill_id else '失败'

    def add_actual_receivables_data(self, condition, date):
        flag = False  # 应收-True,实收-False
        data_bill = []  # 账单
        balance = 0  # 差额
        _filter = MongoBillFilter()
        start_date = string_to_date_only(date[0])
        end_date = string_to_date_only(date[1])
        cur_organization_id = get_current_organization_id(
            self.session)  # 当前组织机构id
        _filter.match_bill(C('organization_id') == cur_organization_id)
        if len(condition):  # 单独核算
            _filter.match_bill(C('user_id').inner(condition))
        else:  # 所有居住中长者进行核算
            _filter.match_bill(C('state') == CheckInStatus.check_in.value)
        _filter.inner_join_bill('PT_User', 'user_id', 'id', 'user')\
               .lookup_bill('PT_Personnel_Classification', 'user.personnel_info.personnel_classification', 'id', 'class')\
               .inner_join('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'name': '$user.name', 'type': '$class.type'})\
            .project({'_id': 0, 'user': 0, 'order._id': 0, 'class._id': 0})
        res = self.query(_filter, 'IEC_Check_In')
        if not len(res):
            raise JsonRpc2Error(-12501, '找不到长者入住订单')
        # 计算床位，护理费服务记录
        all_bed_nursing_recode = self.__actual_bed_nursing_fee(
            res, start_date, end_date, flag)
        # 计算其他费用
        other_fee = self.__get_other_cost(res, start_date, end_date)
        all_recode = all_bed_nursing_recode + other_fee
        all_user_fee_pf = pd.DataFrame(all_recode)

        all_fee_pf = all_user_fee_pf[[
            'order_id', 'valuation_amount', 'user_id']]
        all_fee = dataframe_to_list(all_fee_pf.groupby(
            ['order_id', 'user_id'], as_index=False).sum())
        # 计算每个人的应收费用
        for user_fee in all_fee:  # 核算每个长者
            # 查找本月应收款，计算出每个项目的差额
            user_id = user_fee['user_id']
            # 当前长者的服务记录和id
            user_pf = all_user_fee_pf[all_user_fee_pf['user_id'] == user_id]
            user_recode = dataframe_to_list(user_pf.groupby(
                ['order_id', 'user_id', 'service_product_id'], as_index=False).sum())
            unsend_filter = MongoBillFilter()
            unsend_filter.match_bill(
                (C('user_id') == user_id)
                & (C('start_date') >= start_date)
                & (C('start_date') <= end_date)
                & (C('type') == BillStatus.advance.value)
            )\
                .unwind('recode_list')\
                .inner_join_bill('PT_Service_Record', 'recode_list', 'id', 'recode')\
                .add_fields({'service_product_id': '$recode.service_product_id', 'valuation_amount': '$recode.valuation_amount'})\
                .group({'user_id': '$user_id', 'service_product_id': '$service_product_id'}, [{'valuation_amount': self.ao.summation('$valuation_amount')}])\
                .project({'_id': 0, 'service_product_id': 1, "valuation_amount": 1})
            unsend_bill = self.query(
                unsend_filter, BillStatus.name.value)
            for y in user_recode:  # 循环比对，每一个产品中实收款和预收款直接的差额
                hasflag = False  # 是否预收中没有收费的产品
                for x in unsend_bill:
                    if (x['service_product_id'] == y['service_product_id']):
                        # 计算产品差额
                        x['valuation_amount'] = y['valuation_amount'] - \
                            x['valuation_amount']
                        balance = balance + x['valuation_amount']
                        hasflag = True
                if not hasflag:
                    add_difference = {
                        'valuation_amount': y['valuation_amount'], 'service_product_id': y['service_product_id']}
                    balance = balance + y['valuation_amount']
                    unsend_bill.append(add_difference)
            user_fee.update(start_date=start_date, end_date=end_date,
                            balance=round(balance, 2), type=BillStatus.actual.value, difference=unsend_bill, recode_list=user_recode, reduction=0)
            data_bill.append(Bill(user_fee).to_dict())
        bill_id = self.bill_manage_service.add_bill(
            OperationType.add.value, TypeId.bill.value, [data_bill], [BillStatus.name.value])
        return '成功' if bill_id else '失败'

    def send_receivables_data(self, condition):
        org_id = get_current_organization_id(self.session)
        keys = ['check_status', 'send_status', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id') == org_id)
            & (C('check_status') == values['check_status'])
            & (C('send_status') == values['send_status'])
            & (C('type') == values['type'])
        ).project({'_id': 0})
        res = self.query(_filter, BillStatus.name.value)
        if len(res):
            pf_res = pd.DataFrame(res)
            pf_res['send_status'] = '已发送'
            pf_res = pf_res.drop(['start_date', 'end_date'], axis=1)
            data = dataframe_to_list(pf_res)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.bill.value, [data], [BillStatus.name.value])
            res = '发送成功' if bill_id else '发送失败'
        else:
            res = '没有可发送账单，请检查账单状态'
        return res

    def check_bill(self, condition, param):
        '''审核预收款'''
        res = '审核失败'
        org_id = get_current_organization_id(self.session)
        keys = ['send_status', 'type']
        values = self.get_value(param, keys)
        _filter = MongoBillFilter()
        if len(condition):  # 审核部分长者
            _filter.match_bill(C('user_id').inner(condition))
        _filter.match_bill(
            (C('organization_id') == org_id)
            & (C('send_status') != values['send_status'])
            & (C('type') == values['type'])
        ).project({'_id': 0})
        res = self.query(_filter, BillStatus.name.value)
        if len(res):
            pf_res = pd.DataFrame(res)
            pf_res['check_status'] = '通过'
            pf_res = pf_res.drop(['start_date', 'end_date'], axis=1)
            data = dataframe_to_list(pf_res)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.bill.value, [data], [BillStatus.name.value])
            res = '审核成功' if bill_id else '审核失败'
        else:
            res = '没有可审核账单，请检查账单状态'
        return res

    def check_bill_actual(self, condition, param):
        '''实收款审核'''
        res = '审核失败'
        org_id = get_current_organization_id(self.session)
        _filter = MongoBillFilter()
        if len(condition):  # 审核部分长者
            _filter.match_bill(C('user_id').inner(condition))
        else:  # 审核全部
            _filter.match_bill(C('check_status') == BillStatus.uncheck.value)
        _filter.match_bill(
            (C('organization_id') == org_id)
            & (C('type') == BillStatus.actual.value))\
            .project({'_id': 0})
        res = self.query(_filter, BillStatus.name.value)
        if len(res):
            pf_res = pd.DataFrame(res)
            pf_res['check_status'] = '通过'
            if param:
                pf_res['reduction'] = param['reduction']
                pf_res['remark'] = param['remark']
            pf_res = pf_res.drop(['start_date', 'end_date'], axis=1)
            data = dataframe_to_list(pf_res)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.bill.value, [data], [BillStatus.name.value])
            res = '审核成功' if bill_id else '审核失败'
        else:
            res = '没有可审核账单，请检查账单状态'
        return res

    def __get_other_cost(self, res, start_date, end_date):
        _filter = MongoBillFilter()
        user_pf = pd.DataFrame(res)['user_id'].tolist()
        _filter.match_bill(
            C('user_id').inner(user_pf)
        )\
            .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .inner_join_bill('PT_Other_Cost', 'user_id', 'user_id', 'cost')\
            .add_fields({
                'service_product_id': '$cost.product_id',
                'valuation_amount': '$cost.cost',
                'service_option': [],
                'start_date': start_date,
                'end_date': end_date,
                'status': RecordStatus.service_completed.value,
                'servicer_id': get_current_user_id(self.session)
            })\
            .project({
                '_id': 0,
                'order_id': 1,
                'service_product_id': 1,
                'valuation_amount': 1,
                'service_option': 1,
                'start_date': 1,
                'end_date': 1,
                'status': 1,
                'servicer_id': 1,
                'user_id': 1,
                'organization_id': 1
            })
        res = self.query(_filter, 'IEC_Check_In')
        return res

    def __get_days(self, users, start_date, end_date, isReceivables):
         # 核算时间段内的总天数
        check_total_day = (end_date - start_date).days + 1
        # 自然月总天数
        month = get_first_last_day_month(start_date.year, start_date.month)
        # 自然月第一天
        first = get_string_to_date(month[0])
        # 自然月最后一天
        last = get_string_to_date(month[1])
        # 自然月的总天数
        total_day = (last - first).days + 1
        days_user = []
        res = []
        for user in users:
            user_id = user['user_id']
            leave_day = 0  # 请假天数
            actual_days = 0  # 居住天数
            if isReceivables:  # 预收，不考虑请假情况
                actual_days = check_total_day
                leave_day = 0
            else:
                _filter = MongoBillFilter()
                _filter.match_bill(
                    (C('residents_id') == user_id)
                    & (
                        (C('checkout_date') <= end_date) & (
                            C('checkout_date') >= start_date)
                        | (C('checkin_date') <= end_date) & (C('checkin_date') >= start_date)
                    )
                )\
                    .project({'_id': 0, 'recode._id': 0})
                res = self.query(_filter, 'PT_Accommodation_Record')
            # 2.1、判断已存在的住宿记录，长者是一直都在，还是一直都未回来
                if len(res):  # 本月存在请销假
                    for recode in res:
                        hasOut = True if recode.get('checkout_date') else False
                        hasIn = True if recode.get('checkin_date') else False
                        checkin_date = string_to_date_only(data_to_string_date(
                            recode['checkin_date']))
                        if hasIn and hasOut:
                            checkout_date = string_to_date_only(data_to_string_date(
                                recode['checkout_date']))
                            if start_date > checkin_date:
                                actual_days = actual_days + \
                                    (checkout_date - start_date).days
                            else:
                                actual_days = actual_days + \
                                    (checkout_date - checkin_date).days
                          #print('居住天数', actual_days)
                        elif hasIn:  # (测过)
                            actual_days = actual_days + \
                                (end_date - checkin_date).days + 1
                    leave_day = total_day - actual_days
                else:  # 本月没有请销假，可能是一直都在，或者还未回来，处于请假中
                    # 查找最新一条，确定是请假中还是一直在
                    _filter_before = MongoBillFilter()
                    _filter_before.match_bill(C('residents_id') == user_id)\
                        .sort({'checkin_date': -1})\
                        .project({'_id': 0})
                    most_new_recode = self.query(
                        _filter_before, 'PT_Accommodation_Record')

                    if len(most_new_recode):  # (测过)
                        # 请假中，一直未归 or 一直都在
                        flag = True if 'checkout_date' in most_new_recode[0] else False
                        leave_day = total_day if flag else 0
                        actual_days = 0 if flag else total_day
                    else:
                        JsonRpc2Error(-36109, '找不到' +
                                      user['name'] + '长者的任何住宿记录，数据错误')

            days_user.append({'day_res': {'实际发生天数': actual_days, '请假天数': leave_day, '总天数': total_day}, 'user_id': user_id, 'recode': res}
                             )
        return days_user

    def __actual_bed_nursing_fee(self, res, start_date, end_date, isReceivables):
        # 1.长者本月才入住
        #  a.自费&其他类型长者的护理和床位都是按实际情况收取
        # 2.长者不是本月入住
        #  a.自费  护理按实际收，床位不考虑请假
        #  b.其他  护理和床位都不考虑请假
        # 计算核算时间段内的居住情况
        all_bed_nursing_recode = []
        actual_name = '实际发生天数'
        bed_name = '床位费'
        nursing_name = '护理费'
        hasCheck = False  # 是否本月刚办理入住
        bed_nursing_days = []  # 床位&护理的请销假情况
        days = self.__get_days(res, start_date, end_date, isReceivables)
        for day in days:
            # 获取该长者信息，用于判断是否自费类型长者
            user_id = day['user_id']
            user_pf = pd.DataFrame(res)
            user_pf = user_pf[user_pf['user_id'] == user_id]
            user_type = user_pf['type'].tolist()[0]

            bed_day = copy.deepcopy(day)
            bed_day['process_name'] = bed_name  # 标记床位费，用于计算床位费用，不标记无法计算
            nursing_day = copy.deepcopy(day)
            nursing_day['process_name'] = nursing_name
            if len(day['recode']):  # 有请销假，可能是本月入住，也有可能不是
                try:
                    recode_pf = pd.DataFrame(day['recode'])
                    remark = recode_pf['enter_remark'].tolist()
                    hasCheck = True if FunctionName.check_in_alone_all in remark else False  # 是否本月刚办理入住
                except:
                    hasCheck = False

                if hasCheck:  # 本月才入住
                    # 存在两种情况，
                    # a.如果本月入住之后，
                    #   若没有请过假，那么其他类型长者的实际护理天数 == 请假天数（day中得出的请假天数=入住前的空白时间+ 入住之后本月的请假天数）
                    #   若请过假，那么自费长者的实际护理天数 = 请假天数 - 空白时间（比如：长者5号入住，20号到24号请假，其他类型的长者请假护理费也是不退的，那么实际要算费用的护理天数= 30-5，小于前面计算出来的实际天数）
                    before_days = 0
                    for recode in day['recode']:
                        # 入住办理的记录，获取入住时间，算出入住前的空白时间
                        if recode['enter_remark'] == FunctionName.check_in_alone_all:
                            before_days = (
                                recode['checkin_date'] - start_date).days
                    actual_day = day['day_res']['总天数'] - \
                        before_days  # 不考虑请假的实际发生天数
                    if not (len(user_type) and user_type[0] == '自费'):
                        # 其他类型，护理费不考虑请假
                        nursing_day['day_res'][actual_name] = actual_day
                    bed_day['day_res'][actual_name] = actual_day
            if not hasCheck:
                # 不是本月才入住，分情况：
                #  a.自费  床位费按整月份收取，护理费按实际天数收取
                #  b.非自费 床位&护理都按整月份收取
                total_day = bed_day['day_res']['总天数']
                bed_day['day_res'][actual_name] = total_day
                if len(user_type) and user_type[0] == '自费':  # 长者自费类型
                    pass
                else:
                    nursing_day['day_res']['实际发生天数'] = total_day
            bed_nursing_days.append(bed_day)
            bed_nursing_days.append(nursing_day)
        for x in bed_nursing_days:
            cur = self.accommodation_process.create_record(
                x['user_id'], start_date, end_date, x['day_res'], x['process_name'])
            all_bed_nursing_recode = all_bed_nursing_recode + cur
        return all_bed_nursing_recode

    def __advance_bed_nursing_fee(self, res, start_date, end_date, isReceivables=True):
        '''预收床位/护理费'''
        # 计算核算时间段内的居住情况
        days = self.__get_days(res, start_date, end_date, isReceivables)
        # 计算床位，护理费服务记录
        all_bed_nursing_recode = []
        for day in days:
            cur = self.accommodation_process.create_record(
                day['user_id'], start_date, end_date, day['day_res'])
            all_bed_nursing_recode = all_bed_nursing_recode + cur
        return all_bed_nursing_recode

    def bill_down(self, org_list, condition):
        _filter = MongoBillFilter()
        keys = ['user_id', 'check_status', 'user_name', 'type', 'send_status']
        date = get_string_to_date(condition['date']+'-01')
        date_range = get_first_last_day_month(date.year, date.month)
        start_date = get_string_to_date(date_range[0])
        end_date = get_string_to_date(date_range[1])
        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('organization_id').inner(org_list))
            & (C('check_status') == values['check_status'])
            & (C('send_status') == values['send_status'])
            & (C('type') == BillStatus.advance.value)
            & (start_date <= C('start_date'))
            & (end_date >= C('end_date'))
        )\
            .inner_join_bill('IEC_Check_In', 'user_id', 'user_id', 'check_in')\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_Bed', 'user_id', 'residents_id', 'bed')\
            .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .lookup_bill('PT_Service_Record', 'recode_list', 'id', 'recode')\
            .project({'recode._id': 0})\
            .lookup_bill('PT_Personnel_Classification', 'user.personnel_info.personnel_classification', 'id', 'class')\
            .match_bill(C('user.id') == values['user_id'])\
            .add_fields({
                '开始时间': '$start_date',
                '结束时间': '$end_date',
                '长者姓名': '$user.name',
                '身份证': '$user.personnel_info.id_card',
                '性别': '$user.personnel_info.sex',
                '床位': '$bed.name',
                '长者类别': '$class.name',
                '入住日期': '$check_in.create_date',
                '审核状态': '$check_status',
                '上月差额': '$balance',
                '合计费用': '$amount',
            })\
            .project({
                '_id': 0,
                '开始时间': 1,
                '结束时间': 1,
                '长者姓名': 1,
                '身份证': 1,
                '性别': 1,
                '床位': 1,
                '长者类别': 1,
                '入住日期': 1,
                '审核状态': 1,
                '上月差额': 1,
                '合计费用': 1,
                'recode': 1,
                'balance': 1
            })
        res = self.query(_filter, 'PT_Financial_Bill')
        if len(res):
            for i, x in enumerate(res):
                res[i]['合计费用'] = res[i]['合计费用'] + res[i]['balance']
                res[i]['床位'] = res[i]['床位'][0] if len(res[i]['床位']) else[]
                res[i]['长者类别'] = res[i]['长者类别'][0] if len(
                    res[i]['长者类别']) else []
        return res

    def __get_pay_type(self, user_id, res):
        all_user = pd.DataFrame(res)
        user_pf = all_user[all_user['user_id'] == user_id]
        user = dataframe_to_list(user_pf)
        personnel_info = user[0]['user']['personnel_info']
        pay_type = '其他'
        if (personnel_info.get('card_name') and personnel_info.get('card_number')):
            if personnel_info['card_name'] and personnel_info['card_name']:
                pay_type = '银行划扣'
        return pay_type
