# # -*- coding: utf-8 -*-

# '''
# 护理档案函数
# '''
# from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
# import pandas as pd
# import uuid
# import datetime
# import re
# import hashlib
# from ...pao_python.pao.service.security.security_service import RoleService
# from ...pao_python.pao.service.security.security_utility import get_current_account_id
# from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
# from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
# from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info


# class NursingService(MongoServiceProcess):

#     def get_nursing_list(self, condition, page, count):
#         self._get_list(condition=condition, page=page, count=count, table_name='IEC_Nursing_Archives')
#         return self.res

#     def update_nursing(self, nursing):
#         '''编辑或新增护理档案'''
#         self._add_update_and_bill(nursing, 'IEC_Nursing_Archives')
#         return self.res

#     def delete_nursing(self, nursing_id_list):
#         self._del_data(nursing_id_list, 'IEC_Nursing_Archives')
#         return self.res

#     def nursing_type_update(self, condition):
#         self._add_update_and_bill(condition, 'IEC_Nursing_Tpye')
#         return self.res

#     def get_nursing_type_list(self, condition, page, count):
#         self._get_list(condition=condition,page=page,count=count,table_name='IEC_Nursing_Tpye')
#         return self.res

#     def nursing_type_delete(self, ids):
#         self._del_data(ids,'IEC_Nursing_Tpye')
#         return self.res

#     def get_nursing_relationship_list(self, condition, page, count):
#         self.equal_key = ['id']
#         self.blurry_key = ['staff_name', 'hotel_name', 'special_requirements', 'remarks']
#         self.project_param = ['_id', 'user._id', 'hotel._id', 'person_data._id', 'hotel_data._id']
#         self.add_fields_param = [{'staff_name': '$user.name','hotel_name':'$hotel.name'}]
#         self.lookup_param = [['PT_User', 'nursing_staff', 'id', 'user'], ['PT_Hotel_Zone', 'nursing_area', 'id', 'hotel']]
#         self._get_list(condition=condition,page=page,count=count,table_name='IEC_Nursing_RelationShip')
#         return self.res

#     def nursing_type_delete(self, ids):
#         self._del_data(ids, 'IEC_Nursing_Tpye')
#         return self.res