# -*- coding: utf-8 -*-

'''
需求模板函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter


class RequirementTemplateService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_requirement_template_list(self, org_list, condition, page, count):
        '''获取需求模板列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('template_name').like(values['name'])) & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Requirement_Project', 'dataSource.requirement_projects', 'id', 'pro_info')\
            .lookup_bill('PT_Requirement_Option', 'pro_info.dataSource.requirement_option_ids', 'id', 'opt_info')\
            .project({'_id': 0, 'pro_info._id': 0, 'opt_info._id': 0})
        res = self.page_query(_filter, 'PT_Requirement_Template', page, count)
        # print(res)
        for result in res.get('result'):
            for pro in result.get('pro_info'):
                for opt_id in pro.get('dataSource'):
                    for opt in result.get('opt_info'):
                        if opt.get('id') == opt_id.get('requirement_option_ids'):
                            opt_id["requirement_option"] = opt
                for pro_id in result.get('dataSource'):
                    if pro_id.get('requirement_projects') == pro.get('id'):
                        pro_id['requirement_options'] = pro.get(
                            'dataSource')
                        pro_id['project_name'] = pro.get(
                            'project_name')
        # print('>>>>>>', res, ">>>>>>>>")
        return res

    '''
    {
	'total': 1,
	'result': [{
		'template_name': '需求模板1',
		'requirement_type': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
		'create_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'remark': 'sd',
		'dataSource': [{
			'category_name': '分类name0',
			'requirement_projects': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'priority': '0'
		}, {
			'category_name': 'fneleinamen1',
			'requirement_projects': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'priority': '1'
		}],
		'status': '1',
		'id': '25246c7e-c25b-11e9-9ce7-f45fc101cb0b',
		'modify_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'GUID': '25246c7f-c25b-11e9-b8c5-f45fc101cb0b',
		'version': 1,
		'bill_status': 'valid',
		'valid_bill_id': '251c829d-c25b-11e9-b728-f45fc101cb0b',
		'pro_info': [{
			'project_name': '项目0',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': '068c8b6e-af90-11e9-a06c-144f8a6221df',
			'dataSource': [{
				'name': '选项名0',
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'GUID': 'da7c552c-b5ba-11e9-ac43-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'da6ea2ee-b5ba-11e9-a3aa-f45fc101cb0b'
		}, {
			'project_name': '项目n1',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': 'c8146022-b6b3-11e9-8194-000c2914e94c',
			'dataSource': [{
				'requirement_option_ids': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b'
			}, {
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'modify_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'GUID': '818abe62-c096-11e9-bfc6-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': '81665727-c096-11e9-82ff-f45fc101cb0b'
		}],
		'opt_info': [{
			'name': '入住需求',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '别墅',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '公寓',
				'weight': '2'
			}],
			'state': '1',
			'id': 'cb929e40-b4f4-11e9-bc43-c067472d5f12',
			'create_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'modify_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'GUID': 'cbb00288-b4f4-11e9-b618-c067472d5f12',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'cb92b7c1-b4f4-11e9-8c67-c067472d5f12'
		}, {
			'name': '入住需求2',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '海景房',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '别墅',
				'weight': '2'
			}],
			'state': '1',
			'id': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'GUID': 'fd48b080-b5ba-11e9-b08e-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'fd338c01-b5ba-11e9-87b7-f45fc101cb0b'
		}]
	    }]
    }'''

    def update_requirement_template(self, requirement_template):
        '''编辑或新增需求模板'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in requirement_template.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.requirementTemplate.value, requirement_template, 'PT_Requirement_Template')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.requirementTemplate.value, requirement_template, 'PT_Requirement_Template')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_requirement_template(self, ids):
        '''删除需求模板'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for id in ids:
                data = find_data(db, 'PT_Requirement_Template', {
                    'id': id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.requirementTemplate.value, data[0], 'PT_Requirement_Template')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res


'''
{
	'total': 1,
	'result': [{
		'template_name': '需求模板1',
		'requirement_type': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
		'create_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'remark': 'sd',
		'dataSource': [{
			'category_name': '分类name0',
			'requirement_projects': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'priority': '0'
		}, {
			'category_name': 'fneleinamen1',
			'requirement_projects': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'priority': '1'
		}],
		'status': '1',
		'id': '25246c7e-c25b-11e9-9ce7-f45fc101cb0b',
		'modify_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'GUID': '25246c7f-c25b-11e9-b8c5-f45fc101cb0b',
		'version': 1,
		'bill_status': 'valid',
		'valid_bill_id': '251c829d-c25b-11e9-b728-f45fc101cb0b',
		'pro_info': [{
			'project_name': '项目0',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': '068c8b6e-af90-11e9-a06c-144f8a6221df',
			'dataSource': [{
				'name': '选项名0',
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'GUID': 'da7c552c-b5ba-11e9-ac43-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'da6ea2ee-b5ba-11e9-a3aa-f45fc101cb0b'
		}, {
			'project_name': '项目n1',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': 'c8146022-b6b3-11e9-8194-000c2914e94c',
			'dataSource': [{
				'requirement_option_ids': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b'
			}, {
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'modify_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'GUID': '818abe62-c096-11e9-bfc6-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': '81665727-c096-11e9-82ff-f45fc101cb0b'
		}],
		'opt_info': [{
			'name': '入住需求',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '别墅',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '公寓',
				'weight': '2'
			}],
			'state': '1',
			'id': 'cb929e40-b4f4-11e9-bc43-c067472d5f12',
			'create_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'modify_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'GUID': 'cbb00288-b4f4-11e9-b618-c067472d5f12',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'cb92b7c1-b4f4-11e9-8c67-c067472d5f12'
		}, {
			'name': '入住需求2',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '海景房',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '别墅',
				'weight': '2'
			}],
			'state': '1',
			'id': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'GUID': 'fd48b080-b5ba-11e9-b08e-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'fd338c01-b5ba-11e9-87b7-f45fc101cb0b'
		}]
	}]
}
'''

'''
{
	'total': 1,
	'result': [{
		'template_name': '需求模板1',
		'requirement_type': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
		'create_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'remark': 'sd',
		'dataSource': [{
			'category_name': '分类name0',
			'requirement_projects': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'priority': '0'
		}, {
			'category_name': 'fneleinamen1',
			'requirement_projects': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'priority': '1'
		}],
		'status': '1',
		'id': '25246c7e-c25b-11e9-9ce7-f45fc101cb0b',
		'modify_date': datetime.datetime(2019, 8, 19, 16, 27, 13, 246000),
		'GUID': '25246c7f-c25b-11e9-b8c5-f45fc101cb0b',
		'version': 1,
		'bill_status': 'valid',
		'valid_bill_id': '251c829d-c25b-11e9-b728-f45fc101cb0b',
		'pro_info': [{
			'project_name': '项目0',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': '068c8b6e-af90-11e9-a06c-144f8a6221df',
			'dataSource': [{
				'name': '选项名0',
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': 'da6e56b6-b5ba-11e9-b899-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 49, 34, 563000),
			'GUID': 'da7c552c-b5ba-11e9-ac43-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'da6ea2ee-b5ba-11e9-a3aa-f45fc101cb0b'
		}, {
			'project_name': '项目n1',
			'requirement_type_id': '0edfc486-af75-11e9-bf75-144f8ab4ebb5',
			'requirement_desc': '需求描述2',
			'requirement_match_func': '1+1',
			'organization_id': 'c8146022-b6b3-11e9-8194-000c2914e94c',
			'dataSource': [{
				'requirement_option_ids': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b'
			}, {
				'requirement_option_ids': 'cb929e40-b4f4-11e9-bc43-c067472d5f12'
			}],
			'state': '1',
			'id': '81660942-c096-11e9-ab41-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'modify_date': datetime.datetime(2019, 8, 17, 10, 27, 6, 249000),
			'GUID': '818abe62-c096-11e9-bfc6-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': '81665727-c096-11e9-82ff-f45fc101cb0b'
		}],
		'opt_info': [{
			'name': '入住需求',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '别墅',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '公寓',
				'weight': '2'
			}],
			'state': '1',
			'id': 'cb929e40-b4f4-11e9-bc43-c067472d5f12',
			'create_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'modify_date': datetime.datetime(2019, 8, 2, 15, 11, 49, 191000),
			'GUID': 'cbb00288-b4f4-11e9-b618-c067472d5f12',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'cb92b7c1-b4f4-11e9-8c67-c067472d5f12'
		}, {
			'name': '入住需求2',
			'dataSource': [{
				'option_name': 'name1',
				'option_content': '海景房',
				'weight': '1'
			}, {
				'option_name': 'name2',
				'option_content': '别墅',
				'weight': '2'
			}],
			'state': '1',
			'id': 'fd333e02-b5ba-11e9-ac0e-f45fc101cb0b',
			'create_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'modify_date': datetime.datetime(2019, 8, 3, 14, 50, 32, 945000),
			'GUID': 'fd48b080-b5ba-11e9-b08e-f45fc101cb0b',
			'version': 1,
			'bill_status': 'valid',
			'valid_bill_id': 'fd338c01-b5ba-11e9-87b7-f45fc101cb0b'
		}]
	}]
}'''
