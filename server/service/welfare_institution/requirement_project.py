from ...service.mongo_bill_service import MongoBillFilter
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-21 11:43:26
@LastEditTime: 2019-12-05 16:06:50
@LastEditors: Please set LastEditors
'''
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-21 11:43:26
@LastEditTime: 2019-08-21 11:43:26
@LastEditors: your name
'''
# -*- coding: utf-8 -*-

'''
需求项目函数
'''


class requirementProjectService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_requirement_project_list(self, org_list, condition, page, count):
        self._filter = MongoBillFilter()
        collection_name = 'PT_Requirement_Project'
        keys = ['project_name', 'requirement_type_id', 'number', 'remark', 'requirement_desc',
                'requirement_match_func', 'organization_id', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter\
            .lookup_bill('PT_Requirement_Type', 'requirement_type_id', 'id', 'requirement_type_tmp')\
            .add_fields({'requirement_type': self.ao.array_filter('$requirement_type_tmp', '_tmp', (F('$_tmp.bill_status') == 'valid').f)})\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_tmp')\
            .add_fields({'organization': self.ao.array_filter('$organization_tmp', '_tmp', (F('$_tmp.bill_status') == 'valid').f)})\
            .lookup_bill('PT_Requirement_Option', 'dataSource.requirement_option_ids', 'id', 'req_options_tmp')\
            .add_fields({'req_options': self.ao.array_filter('$req_options_tmp', '_tmp', (F('$_tmp.bill_status') == 'valid').f)})\
            .project({
                'requirement_type._id': 0,
                'organization._id': 0,
                'req_options._id': 0
            })\
            .match((C('bill_status') == 'valid')
                   & (C('id') == values['id'])
                   & (C('project_name').like(values['project_name']))
                   & (C('requirement_type_id').like(values['requirement_type_id']))
                   & (C('requirement_desc').like(values['requirement_desc']))
                   & (C('requirement_match_func').like(values['requirement_match_func']))
                   & (C('remark').like(values['remark']))
                   & (C('organization_id').inner(org_list)))\
            .project({
                '_id': 0,
                'id': 1,
                'project_name': 1,
                'organization_id': 1,
                'requirement_type_id': 1,
                'requirement_desc': 1,
                'dataSource': 1,
                'requirement_match_func': 1,
                'req_options': 1,
                'requirement_type_name': '$requirement_type.name',
                'organization_name': '$organization.name',
            })
        res = self.page_query(_filter, collection_name, page, count)
        for result in res.get('result'):
            for req_option in result.get('req_options'):
                for req_option_id in result.get('dataSource'):
                    if req_option.get('id') == req_option_id.get('requirement_option_ids') and req_option.get('bill_status') == Status.bill_valid.value:
                        req_option_id["req_option"] = req_option.get(
                            'dataSource')
        return res

    def update(self, requirement_project):
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in requirement_project.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.requirementProject.value, requirement_project, 'PT_Requirement_Project')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.requirementProject.value, requirement_project, 'PT_Requirement_Project')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_requirement_project(self, nursing_id_list):
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for business_id in nursing_id_list:
                data = find_data(db, 'PT_Requirement_Project', {
                    'id': business_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.requirementProject.value, data[0], 'PT_Requirement_Project')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
