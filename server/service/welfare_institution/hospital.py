'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 16:05:38
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\welfare_institution\hospital.py
'''
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, get_cur_time, get_first_last_day_month, get_string_to_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.mongo_bill_service import MongoBillFilter
from ...service.common import insert_data, find_data


class HospitalManage(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_hospital_list(self, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter.lookup_bill('PT_User', 'elder', 'id', 'user') \
               .match_bill((C('id') == values['id']) & (C('name').like(values['name']))) \
            .add_fields({
                'name': '$user.name'
            })\
            .project({'_id': 0, 'user._id': 0})
        res = self.page_query(_filter, 'IEC_Hospital', page, count)
        return res

    def add_updeate_hospital(self, condition):
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in condition.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.medicalArrears.value, condition, 'IEC_Hospital')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.medicalArrears.value, condition, 'IEC_Hospital')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_hospital(self, ids):
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for id in ids:
                data = find_data(db, 'PT_Hotel_Zone_Type', {
                    'id': id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.hotelZoneType.value, data[0], 'PT_Hotel_Zone_Type')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
