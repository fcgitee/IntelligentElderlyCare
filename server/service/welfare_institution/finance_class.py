from enum import Enum


class Bill():
    '''核算账单类'''

    def __init__(self, condition):
        if condition.get('id'):
            self.id = condition['id']
        self.user_id = condition['user_id']
        self.recode_list = condition['recode_list']
        self.order_id = condition['order_id']
        self.amount = condition['valuation_amount']
        self.pay_status = BillStatus.unpay.value  # 支付状态
        # 支付类型
        self.pay_type = condition['pay_type'] if condition.get(
            'pay_type') else '其他'
        self.start_date = condition['start_date']
        self.end_date = condition['end_date']
        self.date = None  # 扣款时间
        self.check_status = BillStatus.uncheck.value  # 审核状态
        self.send_status = BillStatus.unsend.value  # 发送状态
        self.balance = condition['balance']  # 上次结算差额
        self.type = condition['type']  # 账单类型：预收/实收
        self.difference = condition['difference'] if condition.get(  # 差额数据
            'difference') else None
        self.reduction = condition['reduction'] if condition.get(
            'reduction') else 0  # 核减

    def to_dict(self):
        return self.__dict__


class BillStatus(Enum):
    name = 'PT_Financial_Bill'
    unsend = '未发送'
    send = '已发送'
    uncheck = '待审核'
    check = '通过'
    unpay = '待支付'
    pay = '已支付'
    advance = '预收'
    actual = '实收'
