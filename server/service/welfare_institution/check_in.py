'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-02 22:48:24
@LastEditTime: 2019-11-08 18:47:43
@LastEditors: Please set LastEditors
'''

import collections
import datetime
import time

from ...models.financial_manage import FinancialAccount
from ...pao_python.pao.commom import find_collection, get_data
from ...pao_python.pao.data import (DataProcess, dataframe_to_list,
                                    date_to_string, get_cur_time, get_date,
                                    process_db)
from ...pao_python.pao.remote import JsonRpc2Error
from ...pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                     MongoService, N, as_date)
from ...service.app.service_detail import ServiceProductDetailService
from ...service.buss_iec.competence_assessment import \
    CompetenceAssessmentService
from ...service.buss_mis.financial_account import FinancialAccountObject
from ...service.buss_mis.service_operation import ServiceOperationService
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId, as_date)
from ...service.buss_pub.personnel_organizational import (User, UserCategory,
                                                          UserType)
from ...service.common import (SerialNumberType, execute_python, get_area_id,
                               get_current_organization_id,
                               get_current_role_id, get_current_user_id,
                               get_info, get_serial_number, get_str_to_age,
                               get_string_time, get_common_project)
from ...service.constant import (AccountStatus, AccountType, PayStatue,
                                 PayType, plat_id)
from ...service.mongo_bill_service import MongoBillFilter
from .accommodation_process import CheckInStatus
from ...service.security_module import FunctionName
import pandas as pd


class CheckInService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.service_product_detail_service = ServiceProductDetailService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def check_if_pay_done(self, condition):
        keys = ['id', 'today']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('user_id') == values['id'])
            & (C('date') < as_date(values['today'])))

        res = self.query(_filter, 'PT_Financial_Bill')
        if len(res) > 0:
            return False
        else:
            return True

    def get_check_in_list_details(self, org_list, condition, page, count):
        if 'create_date' in condition.keys():
            condition['end_date'] = as_date(
                condition['create_date']) + datetime.timedelta(days=1)
        if condition.get('org_name') and '(' in condition.get('org_name'):
            condition['org_name'] = condition['org_name'].split('(')[0]
        keys = ['id', 'name', 'bed_number',
                'bed_name', 'create_date', 'user_id', 'end_date', 'user_number', 'retreat_date', 'org_name', 'id_card']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
            & (C('user_id') == values['user_id'])
            & (as_date(values['create_date']) < C('create_date'))
            & (values['end_date'] > C('create_date'))
            & (values['retreat_date'] > C('modify_date'))
            & (C('state') == CheckInStatus.check_in.value)
        )\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({'organization_name': '$org.name'})\
            .match_bill(C("org.name").like(values['org_name']))\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .match_bill(
            (C('user.name').like(values['name']))
            & (C('user.id_card').like(values['id_card']))
        )\
            .inner_join_bill('PT_Bed', 'user_id', 'residents_id', 'bed')\
            .add_fields({'area_id': '$bed.area_id'})\
            .unwind('area_id')\
            .lookup_bill('PT_Hotel_Zone', 'area_id', 'id', 'hotelZone')\
            .add_fields({'area_name': '$hotelZone.zone_name'})\
            .add_fields({'up_id1': '$hotelZone.upper_hotel_zone_id'})\
            .lookup_bill('PT_Hotel_Zone', 'up_id1', 'id', 'hotelZone2')\
            .add_fields({'up_id2': '$hotelZone2.upper_hotel_zone_id'})\
            .add_fields({'area_name2': '$hotelZone2.zone_name'})\
            .lookup_bill('PT_Hotel_Zone', 'up_id2', 'id', 'hotelZone3')\
            .add_fields({'up_id3': '$hotelZone3.upper_hotel_zone_id'})\
            .add_fields({'area_name3': '$hotelZone3.zone_name'})\
            .match_bill(C('bed.name').like(values['bed_name']))\
            .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'user_id', 'elder', 'recode')\
            .add_fields({
                'bed_number': '$bed.bed_code',
                'bed_name': '$bed.name',
                'bed_id': '$bed.id',
                'name': '$user.name',
                'family_name': '$user.personnel_info.family_name',
                'sex': '$user.personnel_info.sex',
                'personnel_category': '$user.personnel_info.personnel_category',
                'id_card': '$user.id_card',
                'recode': self.ao.slices('$recode', 1, -1),
                'age': '$user.personnel_info.age'
                # 'personnel_info': '$user.personnel_info'
            })\
            .unwind('area_name3')\
            .unwind('area_name2')\
            .unwind('area_name')\
            .unwind('bed_name')\
            .add_fields({'nursing_level': '$recode.nursing_level'})\
            .match_bill(
            (C('bed_number').like(values['bed_number']))
            & (C('bed_name').like(values['bed_name']))
        )\
            .add_fields({'bed_name': self.ao.concat([('$area_name3'), '>', ('$area_name2'), '>', ('$area_name'), '>', ('$bed_name')])})\
            .sort({'create_date': -1})\
            .project({
                '_id': 0,
                'user.login_info': 0,
                'org': 0,
                'recode': 0,
                'hotelZone': 0,
                'hotelZone2': 0,
                'hotelZone3': 0,
                **get_common_project({'order', 'bed', 'user'})
            })
        res = self.page_query(_filter, 'IEC_Check_In', page, count)
        for i, x in enumerate(res['result']):
            data = res['result'][i]['user']['personnel_info']['date_birth']
            date_res = get_string_time(data, '%Y-%m-%d')
            res['result'][i]['user']['personnel_info']['date_birth'] = date_res
            res['result'][i]['user']['personnel_info']['age'] = get_str_to_age(
                date_res)
        return res

    def get_check_in_list(self, org_list, condition, page, count):

        keys = ['id', 'name', 'bed_number',
                'bed_name', 'create_date', 'user_id', 'end_date', 'user_number', 'retreat_date', 'org_name', 'id_card', 'user_ids']
        values = self.get_value(condition, keys)
        user_list = {}
        user_ids = []
        org_list_new = {}
        checkIn_list = {}
        checkIn_ids = []
        recode_list = {}

        # 根据组织机构查询
        if condition.get('org_name'):
            if '(' in condition.get('org_name'):
                values['org_name'] = condition['org_name'].split('(')[0]
        _filter_org = MongoBillFilter()
        _filter_org.match_bill(
            (C('name').like(values['org_name']))
            & (C('personnel_type') == '2')
        )\
            .project({**get_common_project()})
        res_org = self.query(_filter_org, 'PT_User')
        if len(res_org):
            if 'org_name' in condition.keys():
                org_list = []
            for y in res_org:
                org_list_new[y['id']] = y
                if 'org_name' in condition.keys():
                    org_list.append(y['id'])
        # 按入住时间查询
        if condition.get('create_date'):
            condition['end_date'] = as_date(
                condition['create_date']) + datetime.timedelta(days=1)
        _filter_checkIn = MongoBillFilter()
        _filter_checkIn.match_bill(
            (C('organization_id').inner(org_list))
            & (C('state') == CheckInStatus.check_in.value)
            & (as_date(values['create_date']) < C('create_date'))
            & (values['end_date'] > C('create_date')))\
            .project({**get_common_project()})
        res_checkIn = self.query(_filter_checkIn, 'IEC_Check_In')
        if len(res_checkIn):
            for z in res_checkIn:
                checkIn_list[z['user_id']] = z
                checkIn_ids.append(z['user_id'])

        # 根据身份证和姓名查询
        _filter_user = MongoBillFilter()
        _filter_user.match_bill(
            (C('organization_id').inner(org_list))
            & (C('id').inner(checkIn_ids))
            & (C('id') == values['user_id'])
            & (C('name').like(values['name']))
            & (C('id_card').like(values['id_card']))
        )\
            .project({**get_common_project()})
        res_user = self.query(_filter_user, 'PT_User')
        if len(res_user):
            for i, x in enumerate(res_user):
                # 计算长者当前年龄x
                birth = get_string_time(
                    x['personnel_info']['date_birth'], '%Y-%m-%d')
                res_user[i]['personnel_info']['date_birth'] = birth
                res_user[i]['age'] = get_str_to_age(birth)
                user_ids.append(x['id'])
                user_list[x['id']] = res_user[i]

        # 查询床位
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
            & (C('residents_id').inner(checkIn_ids))
            & (C('residents_id') == values['user_id'])
            & (C('residents_id').inner(user_ids))
            & (C('name').like(values['bed_name']))
        )\
            .project({
                'recode': 0,
                **get_common_project()})
        res = self.page_query(_filter, 'PT_Bed', page, count)

        # 获取护理级别信息
        _filter_recode = MongoBillFilter()
        _filter_recode.match_bill(
            (C('organization_id').inner(org_list))
            & (C('elder').inner(checkIn_ids))
            & (C('elder') == values['user_id'])
            & (C('elder').inner(user_ids))
        )\
            .group({'user_id': '$elder'}, [{'recode': self.ao.push('$nursing_level')}])\
            .add_fields({'nursing_level': self.ao.slices('$recode', 1, -1), })\
            .project({"_id": 0, 'user_id': 1, 'nursing_level': 1})
        res_recode = self.query(
            _filter_recode, 'PT_Behavioral_Competence_Assessment_Record')
        for k in res_recode:
            recode_list[k['user_id']] = k['nursing_level']
        result = res['result']
        new = []
        for item in result:
            new.append(item['area_id'])
        area_id_1 = []
        area_id_2 = []
        area_id_3 = []
        area_info_1 = {}
        if len(res):
            # 查询床位所在路径
            # 获取床位向上第一层区域信息
            pd_res = pd.DataFrame(result)
            area_id_1 = pd_res['area_id'].tolist()
            _filter_zone_1 = MongoBillFilter()
            _filter_zone_1.match_bill(C('id').inner(area_id_1))\
                .project({'_id': 0, 'id': 1, 'zone_name': 1, 'upper_hotel_zone_id': 1})
            res_zone_1 = self.query(_filter_zone_1, 'PT_Hotel_Zone')
            # 获取向上第二层区域信息
            for zone_1 in res_zone_1:
                area_info_1[zone_1['id']] = zone_1['zone_name']
                if 'upper_hotel_zone_id' in zone_1:
                    zone_id_2 = zone_1['upper_hotel_zone_id']
                    area_info_1[zone_1['id']] = zone_id_2 + \
                        '>' + zone_1['zone_name']
                    area_id_2.append(zone_id_2)

                    _filter_zone_2 = MongoBillFilter()
                    _filter_zone_2.match_bill(C('id').inner(area_id_2))\
                        .project({'_id': 0, 'id': 1, 'zone_name': 1, 'upper_hotel_zone_id': 1})
                    res_zone_2 = self.query(_filter_zone_2, 'PT_Hotel_Zone')
                    for zone_2 in res_zone_2:
                        if 'upper_hotel_zone_id' in zone_2:
                            zone_id_3 = zone_2['upper_hotel_zone_id']
                            area_id_3.append(zone_id_3)
                            # 匹配第一层中保存的第二场id,拼接出"第二层>第一层"区域字符串
                            for info in area_info_1:
                                if zone_2['id'] in area_info_1[info]:
                                    info_1 = area_info_1[info]
                                    area_info_1[info] = zone_2['upper_hotel_zone_id']+'>'+info_1.replace(
                                        zone_2['id'], zone_2['zone_name'])

                            # 获取第三层区域信息
                            _filter_zone_3 = MongoBillFilter()
                            _filter_zone_3.match_bill(C('id').inner(area_id_3))\
                                .project({'_id': 0, 'id': 1, 'zone_name': 1, 'upper_hotel_zone_id': 1})
                            res_zone_3 = self.query(_filter_zone_3, 'PT_Hotel_Zone')
                            for zone_3 in res_zone_3:
                                # 匹配第一层中保存的第二场id,拼接出"第二层>第一层"区域字符串
                                for info in area_info_1:
                                    if zone_3['id'] in area_info_1[info]:
                                        info_1 = area_info_1[info]
                                        area_info_1[info] = info_1.replace(
                                            zone_3['id'], zone_3['zone_name'])

            # 拼接数据显示
            for i, j in enumerate(result):
                # 拼接上面查询的数据
                # 获取组织信息
                result[i]['organization_name'] = org_list_new[result[i]
                                                              ['organization_id']]['name']
                # 获取长者信息
                user_info = user_list[result[i]['residents_id']]
                result[i]['name'] = user_info['name']
                result[i]['id_card'] = user_info['id_card']
                result[i]['age'] = user_info['age']
                result[i]['sex'] = user_info['personnel_info']['sex']
                result[i]['date_birth'] = user_info['personnel_info']['date_birth']
                # 获取入住时间
                result[i]['create_date'] = checkIn_list[result[i]
                                                        ['residents_id']]['create_date']
                # 获取入住床位及区域路径
                result[i]['bed_name'] = area_info_1[result[i]
                                                    ['area_id']]
                result[i]['bed_id'] = result[i]['id']
                result[i]['user_id'] = result[i]['residents_id']
                # 获取护理等级
                result[i]['nursing_level'] = recode_list[result[i]['residents_id']
                                                         ] if result[i]['residents_id'] in list(recode_list.keys()) else []
        return res

    def add_check_in(self, elder_info, evaluation_info, service_item):
        # 当前时间
        now = get_cur_time()
        _filter = MongoBillFilter()
        keys = ['id_card', 'personnel_category']
        values = self.get_value(elder_info, keys)
        _filter.match_bill((C('personnel_info.personnel_category') == values['personnel_category']) & (C('id_card')) == values['id_card'])\
               .project({'_id': 0})
        res = self.page_query(_filter, 'PT_User', None, None)
        elder_info['personnel_category'] = UserCategory.Elder
        _filter_user = MongoBillFilter()
        # 组装长者表数据结构
        elder = {
            'personnel_info': elder_info,
            'personnel_type': UserType.Personnel,
            'name': elder_info['name'],
            'id_card': elder_info['id_card'],
            'id_card_type': '身份证',
            'admin_area_id': get_area_id(self, _filter_user)
        }
        # 判断是否已存在该长者
        if len(res['result']):
            elder['id'] = res['result'][0]['id']
            _filter_user = MongoBillFilter()
            _filter_user.match_bill((C('user_id') == elder['id']) & (
                C('state') == CheckInStatus.check_in.value))
            user_res = self.query(_filter_user, 'IEC_Check_In')
            if len(user_res):
                raise JsonRpc2Error('-36010', '当前长者已入住')
            self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.checkIn.value, elder, 'PT_User')
        else:
            elder = get_info(elder, self.session)
            self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.checkIn.value, elder, 'PT_User')
        # 补充长者更新信息
        # 为评估记录信息添加长者id
        evaluation_info['elder'] = elder['id']
        CompetenceAssessmentService(self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd,
                                    self.inital_password, self.session).update_competence_assessment(evaluation_info)
        # 为服务订单添加长者id
        order_code = ''

        def process_func(db):
            nonlocal order_code
            order_code = get_serial_number(db, SerialNumberType.order.value)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        order_recode = {
            'purchaser_id': elder['id'],
            'order_code': order_code,
            'service_provider_id': service_item['service_provider_id'] if 'service_provider_id' in service_item else get_current_organization_id(self.session),
            'status': '未服务',
            'order_date': now,
            'service_date': now,
            'detail': service_item['details'],
            'is_paying_first': '0'
        }
        # 生成订单
        order_recode = get_info(order_recode, self.session)
        # 住宿记录
        accommondation_recode = {
            'residents_id': elder['id'], 'checkin_date': get_cur_time(), "enter_remark": FunctionName.check_in_alone_all, 'status': '入住', 'bed_id': service_item.get('bed_id')}
        accommondation_recode = get_info(accommondation_recode, self.session)

        # 绑定床位
        bed_info = {
            'id': service_item.get('bed_id'),
            'residents_id': elder.get('id')
        }
        # 新增入住记录
        check_in_recode = get_info({
            'user_id': elder['id'], 'accommodation_id': accommondation_recode['id'], 'order_id': order_recode['id'], 'state': '居住中'}, self.session)

        # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
        # 机构储蓄对象
        org_id = get_current_organization_id(self.session)
        org_account_data = FinancialAccountObject(
            AccountType.account_recharg_wel, elder['id'], org_id, AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
        org_account = get_info(org_account_data.to_dict(), self.session)
        # app储蓄对象
        app_account_data = FinancialAccountObject(
            AccountType.account_recharg_app, elder['id'], plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
        app_account = get_info(app_account_data.to_dict(), self.session)
        # 真实账户对象
        real_account_data = FinancialAccountObject(
            AccountType.account_real, elder['id'], None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
        real_account = get_info(real_account_data.to_dict(), self.session)
        # 补贴账户对象
        subsidy_account_data = FinancialAccountObject(
            AccountType.account_subsidy, elder['id'], None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
        subsidy_account = get_info(
            subsidy_account_data.to_dict(), self.session)
        # 最终一起增加到数据库中
        self.bill_manage_service.add_bill(OperationType.update.value,
                                          TypeId.checkIn.value, [bed_info], ['PT_Bed'])
        self.bill_manage_service.add_bill(OperationType.add.value,
                                          TypeId.checkIn.value, [[order_recode], [accommondation_recode], [check_in_recode], [org_account, app_account, real_account, subsidy_account]], ['PT_Service_Order', 'PT_Accommodation_Record', 'IEC_Check_In', FinancialAccount().name])
        return {'create_date': now}

    def get_all_product(self, condition):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .lookup_bill('PT_User', 'org_id', 'id', 'org_info')\
            .unwind('org_info')\
            .unwind('org_info.qualification_info')\
            .lookup_bill('PT_Qualification_Type', 'org_info.qualification_info.qualification_type_id', 'id', 'qualification_type_info')\
            .lookup_bill('PT_Service_Order', 'id', 'detail.product_id', 'order_info')\
            .match((C('qualification_type_info.name').like('服务评级')))\
            .add_fields({'org_name': '$org_info.name',
                         'org_address': '$org_info.address',
                         'pay_count': self.ao.size('$order_info'),
                         'org_level': '$org_info.qualification_info.qualification_level'})\
            .project({'_id': 0, 'org_info._id': 0, 'qualification_type_info._id': 0, 'order_info._id': 0})
        res = self.query(_filter, 'PT_Service_Product')
        porduct_item_list = []
        try:
            if res:
                product_queue = collections.deque()
                product_queue.append(res[0])
                while len(product_queue) != 0:
                    res_f = product_queue.popleft()
                    if res_f.get('product_collection'):
                        for product_id_list in res_f.get('product_collection'):
                            res_child = self.service_product_detail_service.get_children_tree(
                                product_id_list.get("product_id"))
                            if res_child:
                                if res_child[0].get('product_collection'):
                                    product_queue.append(res_child[0])
                                else:
                                    porduct_item_list.append(res_child[0])
                    else:
                        porduct_item_list.append(res_f)
                for product_item in porduct_item_list:
                    product_item_price = 0
                    if product_item.get('valuation_formula') and product_item.get('option_list'):
                        product_item_price = execute_python(product_item.get(
                            'valuation_formula'), product_item.get('option_list'), 'option_name', 'option_value')
                        product_item['valuation_amount'] = product_item_price
        except Exception as e:
            pass
          # print(e)
        return porduct_item_list

    def create_product_order(self, pro_id, count):
        '''获取订单detail'''
        try:
            product_list = self.get_all_product({'id': pro_id})
            new_product_list = []
            if product_list:
                for product in product_list:
                    pro_dict = {}
                    pro_dict['product_id'] = product.get('id')
                    pro_dict['product_name'] = product.get('name')
                    pro_dict['valuation_formula'] = product.get(
                        'valuation_formula')
                    pro_dict['option_list'] = product.get('option_list')
                    pro_dict['contract_terms'] = product.get(
                        'additonal_contract_terms')
                    pro_dict['remark'] = product.get('remark')
                    pro_dict['count'] = count
                    new_product_list.append(pro_dict)
            return new_product_list
        except Exception as e:
            pass
          # print(e)

    def update_check_in(self, service_item):
        # 绑定床位
        bed_info = {
            'id': service_item['bed_id'],
            'residents_id': service_item['residents_id']
        }
        self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.checkIn.value, bed_info, 'PT_Bed')
        # 更新订单记录
        order_recode = {
            'purchaser_id': service_item['residents_id'],
            'service_provider_id': service_item['service_provider_id'],
            'service_item': service_item['service_item']
        }
        order_recode = get_info(order_recode, self.session)
        self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.checkIn.value, order_recode, 'PT_Service_Order')
        # 新增入住记录
        # check_in_recode = {'user_id':elder['id'],'accommodation_id':accommondation_recode['id'],'order_id':order_recode['id']}
        # self._add_update_and_bill(check_in_recode, 'IEC_Check_In')
        # self.res = {'create_date':get_date()}

    def add_check_in_special(self, service_item):
        now = get_cur_time()
        user_id = service_item['user_id']
        _filter_user = MongoBillFilter()
        _filter_user.match_bill((C('user_id') == user_id) & (
            C('state') == CheckInStatus.check_in.value))
        user_res = self.query(_filter_user, 'IEC_Check_In')
        if len(user_res):
            raise JsonRpc2Error('-36010', '当前长者已入住')
        # 为服务订单添加长者id
        order_code = ''

        def process_func(db):
            nonlocal order_code
            order_code = get_serial_number(db, SerialNumberType.order.value)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        order_recode = {
            'purchaser_id': user_id,
            'order_code': order_code,
            'service_provider_id': service_item['service_provider_id'] if 'service_provider_id' in service_item else get_current_organization_id(self.session),
            'status': '未服务',
            'order_date': now,
            'service_date': now,
            'detail': service_item['details'],
            'is_paying_first': '0'
        }
        # 生成订单
        order_recode = get_info(order_recode, self.session)
        # 住宿记录
        accommondation_recode = {
            'residents_id': user_id, 'checkin_date': get_cur_time(), "enter_remark": FunctionName.check_in_alone_all, 'status': '入住', 'bed_id': service_item.get('bed_id')}
        accommondation_recode = get_info(accommondation_recode, self.session)

        # 绑定床位
        bed_info = {
            'id': service_item.get('bed_id'),
            'residents_id': user_id
        }
        # 新增入住记录
        check_in_recode = get_info({
            'user_id': user_id, 'accommodation_id': accommondation_recode['id'], 'order_id': order_recode['id'], 'state': '居住中'}, self.session)
        if 'create_date' in service_item.keys():
            check_in_recode['create_date'] = as_date(
                service_item['create_date'])

        # 新增账户信息（1个机构储蓄、1个app储蓄、1个真实账户、1个补贴账户）
        # 机构储蓄对象
        org_id = get_current_organization_id(self.session)
        org_account_data = FinancialAccountObject(
            AccountType.account_recharg_wel, user_id, org_id, AccountStatus.normal, 1, AccountType.account_recharg_wel, {}, 0)
        org_account = get_info(org_account_data.to_dict(), self.session)
        # app储蓄对象
        app_account_data = FinancialAccountObject(
            AccountType.account_recharg_app, user_id, plat_id, AccountStatus.normal, 2, AccountType.account_recharg_app, {}, 0)
        app_account = get_info(app_account_data.to_dict(), self.session)
        # 真实账户对象
        real_account_data = FinancialAccountObject(
            AccountType.account_real, user_id, None, AccountStatus.normal, 3, AccountType.account_real, {}, 0)
        real_account = get_info(real_account_data.to_dict(), self.session)
        # 补贴账户对象
        subsidy_account_data = FinancialAccountObject(
            AccountType.account_subsidy, user_id, None, AccountStatus.normal, 4, AccountType.account_subsidy, {}, 0)
        subsidy_account = get_info(
            subsidy_account_data.to_dict(), self.session)
        # 最终一起增加到数据库中
        self.bill_manage_service.add_bill(OperationType.update.value,
                                          TypeId.checkIn.value, [bed_info], ['PT_Bed'])
        self.bill_manage_service.add_bill(OperationType.add.value,
                                          TypeId.checkIn.value, [[order_recode], [accommondation_recode], [check_in_recode], [org_account, app_account, real_account, subsidy_account]], ['PT_Service_Order', 'PT_Accommodation_Record', 'IEC_Check_In', FinancialAccount().name])
        return {'create_date': now}

    def update_create_date(self, condition):
        res = '修改失败'
        if 'create_date' in condition.keys():
            condition['create_date'] = as_date(
                condition['create_date'])
        _filter_checjIN = MongoBillFilter()
        _filter_checjIN.match_bill((C('user_id') == condition['user_id']))\
                       .project({'_id': 0})
        checkIN_res = self.query(_filter_checjIN, 'IEC_Check_In')
        if len(checkIN_res) > 0:
            _filter_order= MongoBillFilter()
            _filter_order.match_bill((C('id') == checkIN_res[0]['order_id']))\
                .project({'_id': 0})
            order_res = self.query(_filter_order, 'PT_Service_Order')
            _filter_record= MongoBillFilter()
            _filter_record.match_bill((C('id') == checkIN_res[0]['accommodation_id']))\
                .project({'_id': 0})
            record_res = self.query(_filter_record, 'PT_Accommodation_Record')
            _filter_bed= MongoBillFilter()
            _filter_bed.match_bill((C('residents_id') == condition['user_id']))\
                .project({'_id': 0})
            bed_res = self.query(_filter_bed, 'PT_Bed')
            checkIN_res[0]['create_date'] = condition['create_date']
            order_res[0]['create_date'] = condition['create_date']
            record_res[0]['create_date'] = condition['create_date']
            bed_res[0]['create_date'] = condition['create_date']
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                          TypeId.checkIn.value, [[order_res[0]], [record_res[0]], [checkIN_res[0]], [bed_res[0]]], ['PT_Service_Order', 'PT_Accommodation_Record', 'IEC_Check_In', 'PT_Bed'])
            if bill_id:
                res = '修改成功'
        return res