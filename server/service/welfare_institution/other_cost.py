from ...service.mongo_bill_service import MongoBillFilter
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.app.my_order import OrderStatus
from ...pao_python.pao.data import string_to_date_only
from ...service.common import get_info, get_current_user_id, operation_result, get_common_project


class OtherCostService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.table_name = 'PT_Other_Cost'
        self.table = 'PT_Cost_Type'

    def update_other_cost(self, condition):
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
      # print(condition)
        if 'T' in condition['date']:
            condition['date'] = string_to_date_only(
                condition['date'].split('T')[0])
        else:
            condition['date'] = string_to_date_only(condition['date'])
        data = OtherCost(condition).to_dict()
        bill_id = self.bill_manage_server.add_bill(
            flag, TypeId.otherCost.value, [data], [self.table_name])
        return operation_result(bill_id)

    def delete_other_cost(self, ids):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(ids))\
               .project({'_id': 0})
        res = self.query(_filter, self.table_name)
        bill_id = self.bill_manage_server.add_bill(
            OperationType.delete.value, TypeId.otherCost.value, [res], [self.table_name])
        return operation_result(bill_id)

    def get_other_cost(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'date', 'product_name', 'user_name', 'org_name']
        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
        )\
            .inner_join_bill('PT_User', 'user_id', 'id', 'user') \
            .lookup_bill('PT_User', 'organization_id', 'id', 'org') \
            .add_fields({'org_name': '$org.name', })\
            .inner_join_bill('PT_Service_Product', 'product_id', 'id', 'product')\
            .match(
                (C('user.name').like(values['user_name']))
                & (C('org_name').like(values['org_name']))
                & (C('product.name').like(values['product_name']))
        )\
            .project({
                'user.login_info': 0,
                'org.qualification_info': 0,
                **get_common_project({'user', 'product', 'org'})
            })
        res = self.page_query(_filter, self.table_name, page, count)
        return res

    def update_cost_type(self, condition):
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
        cost_type = CostType(condition).to_dict()
        bill_id = self.bill_manage_server.add_bill(
            flag, TypeId.costType.value, cost_type, self.table)
        return operation_result(bill_id)

    def get_cost_type(self, org_list, condition, page, count):
        _filter = MongoBillFilter()
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('organization_id').inner(org_list))
        )\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .project({'_id': 0, 'org._id': 0})
        res = self.page_query(_filter, self.table, page, count)
        return res

    def delete_cost_type(self, ids):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(ids))\
               .project({'_id': 0})
        res = self.query(_filter, self.table)
        bill_id = self.bill_manage_server.add_bill(
            OperationType.delete.value, TypeId.costType.value, [res], [self.table])
        return operation_result(bill_id)


class OtherCost():
    def __init__(self, condition):
        if condition.get('id'):
            self.id = condition['id']
        self.user_id = condition['user_id']
        self.date = condition['date']
        self.cost = condition['cost']
        self.product_id = condition['product_id']
        self.remark = condition['remark'] if condition.get('remark') else ''

    def to_dict(self):
        return self.__dict__


class CostType():
    def __init__(self, condition):
        if condition.get('id'):
            self.id = condition['id']
        self.remark = condition['remark']
        self.organization_id = condition['organization_id']
        self.name = condition['name']

    def to_dict(self):
        return self.__dict__
