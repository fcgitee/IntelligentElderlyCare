# -*- coding: utf-8 -*-

'''
能力评估模板
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter


class RequirementRecordService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_requirement_record_list(self, condition, page, count):
        '''
            获取需求记录列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        keys = ['id', 'elder', 'template_name', 'create_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'elder', 'id', 'user')\
            .add_fields({'user_name': '$user.name'})\
            .lookup_bill('PT_Requirement_Template', 'template', 'id', 'template_data')\
            .add_fields({'template_name': '$template_data.template_name', 'create_date': self.ao.date_to_string('$create_date')})\
            .match((C('bill_status') == Status.bill_valid.value) & (C('id') == values['id']) & (C('elder') == values['elder']) & (C('template_name') == values['template_name']) & (C('create_date') == values['create_date']))\
            .project({'_id': 0, 'user._id': 0, 'template_data._id': 0})
        res = self.page_query(
            _filter, 'PT_Requirement_Record', page, count)
        return res

    def update_requirement_record(self, requirement_record):
        '''新增/修改需求记录

        Arguments:
            requirement_record   {dict}      条件
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in requirement_record.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            'requirement_record', requirement_record, 'PT_Requirement_Record')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            'requirement_record', requirement_record, 'PT_Requirement_Record')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_requirement_record(self, requirement_record_id_list):
        '''删除需求记录

        Arguments:
           requirement_record_id_list   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(requirement_record_id_list, str):
                ids.append(requirement_record_id_list)
            else:
                ids = requirement_record_id_list
            for requirement_record_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Requirement_Record', {
                    'id': requirement_record_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      'requirement_record', data[0], 'PT_Requirement_Record')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
