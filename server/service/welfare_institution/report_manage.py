from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F
from ...service.mongo_bill_service import MongoBillFilter
'''
@Author: your name
@Date: 2019-11-04 13:48:54
@LastEditTime: 2019-12-05 15:30:24
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\welfare_institution\report_manage.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Friday November 1st 2019
创建者：ymq(ymq) - <<email>>

修改日期: Friday, 1st November 2019 9:48:38 am
修改者: ymq(ymq) - <<email>>

说明
 1、报表接口
'''


class ReportManage(MongoService):
    plat_visitor_id = '8ed260f6-e355-11e9-875e-a0a4c57e9ebe'  # 平台游客角色id
    user_collection = 'PT_User'
    role_collection = 'PT_Role'
    set_role_collection = 'PT_Set_Role'
    bed_collection = 'PT_Bed'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session

    def get_org_worker_list(self, org_list, condition, page, count):
        keys = ['organization_nature',
                'organization_name', 'organization_category']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id').inner(org_list))
                           & (C('name').like(values['organization_name']))
                           & (C('organization_info.personnel_category') == values['organization_category']))\
               .inner_join_bill(self.set_role_collection, 'id', 'role_of_account_id', 'role_set_info')\
               .inner_join_bill(self.role_collection, 'role_set_info.role_id', 'id', 'role_info')\
               .match(C('role_id') != self.plat_visitor_id)\
               .group({'user_id': '$role_set_info.principal_account_id', 'org_name': '$name', 'town': '$town', 'role_name': '$role_info.name'})\
               .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
               .project({'org_name': 1, 'town': 1, 'role_name': 1, 'user_id': 1, 'sex': '$user_info.personnel_info.sex'})\
               .add_fields({'female_number': self.ao.switch([self.ao.case((F('sex') == '女'), 1)], 0), 'manager_number': self.ao.switch([self.ao.case((F('role_name') == '超级管理员'), 1)], 0), 'worker_number': self.ao.switch([self.ao.case((F('role_name') != '超级管理员'), 1)], 0)})\
               .group({'town': '$town', 'org_name': '$org_name'}, [{'total_worker': self.ao.summation(1)}, {'female_worker': self.ao.summation('$female_number')}, {'manager_worker': self.ao.summation('$manager_number')}, {'normal_worker': self.ao.summation('$worker_number')}])
        res = self.page_query(_filter, self.user_collection, page, count)
        return res

    def get_check_in_elder_list(self, org_list, condition, page, count):
        '''福利院入住长者统计'''
        keys = ['organization_name', 'organization_nature']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_id').inner(org_list))\
               .lookup_bill('PT_User', 'residents_id', 'id', 'user_info')\
               .add_fields({'female': self.ao.array_filter('$user_info', 'tep', (F('$tep.personnel_info.sex') == '女').f)})\
               .add_fields({'number': self.ao.size('$user_info'), 'female_number': self.ao.size('$female'), 'age': '$user_info.personnel_info.age'})\
               .add_fields({'age_lt79': self.ao.cond((F('age') < 80).f, 1, 0), 'age_gt80_lt99': self.ao.cond(((F('age') >= 80) & (F('age') < 100)).f, 1, 0), 'age_lt100': self.ao.cond((F('age') >= 100).f, 1, 0)})\
               .inner_join_bill('PT_User', 'organization_id', 'id', 'org_info')\
               .match((C('org_info.name').like(values['organization_name'])) & (C('org_info.organization_info.organization_nature') == values['organization_nature']))\
               .unwind('age', True)\
               .group({'org_name': '$org_info.name'}, [{'bed_number': self.ao.summation(1)}, {'check_in_number': self.ao.summation('number')}, {'female_num': self.ao.summation('female')}, {'age_lt79': self.ao.summation('age_lt79')}, {'age_gt80_lt99': self.ao.summation('age_gt80_lt99')}, {'age_lt100': self.ao.summation('age_lt100')}])
        # print('_filter.filter_objects>>>>>>>>>>>', _filter.filter_objects)
        res = self.page_query(_filter, self.bed_collection, page, count)
        return res
