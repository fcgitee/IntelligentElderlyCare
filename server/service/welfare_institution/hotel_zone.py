'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-21 11:43:26
@LastEditTime: 2019-12-05 13:55:42
@LastEditors: Please set LastEditors
'''
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_common_project
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.mongo_bill_service import MongoBillFilter
# -*- coding: utf-8 -*-

'''
住宿区域函数
'''


class hotelZoneService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.collection_name = 'PT_Hotel_Zone'

    def get_hotel_zone_list(self, org_list, condition, page, count):
        new_result = {}
        new_result['total'] = 0
        new_result['result'] = []
        keys = ['zone_name', 'zone_number', 'organization_id', 'organization_type',
                'hotel_zone_type_id', 'upper_hotel_zone_id', 'remark', 'bed_id', 'id', 'hotel_zone_type_name', 'no_type_name', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        # 左连接自己，原表 upper 对应右表id
        # 左连接组织，
        # 左连接区域类型，
        # 增加upper列，内容为upper_hotel_zone对象，筛选条件为bill_status == valid
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('zone_name').like(values['zone_name']))
                           & (C('zone_number').like(values['zone_number']))
                           & (C('organization_id').like(values['organization_id']))
                           & (C('upper_hotel_zone_id').like(values['upper_hotel_zone_id']))
                           & (C('remark').like(values['remark']))
                           & (C('organization_id').inner(org_list))
                           & (C('hotel_zone_type_name') == values['hotel_zone_type_name'])
                           & (C('hotel_zone_type_name') != values['no_type_name'])) \
            .inner_join_bill('PT_User', 'organization_id', 'id', 'organization')\
            .match_bill(C('organization.name').like(values['org_name']))\
            .match_bill((C('organization.organization_info.personnel_category') == values['organization_type']))\
            .lookup_bill('PT_Hotel_Zone_Type', 'hotel_zone_type_id', 'id', 'type_info')\
            .add_fields({'organization_name': '$organization.name', 'organization_id': '$organization.id'})\
            .sort({'modify_date': -1})

        # 床位管理的下拉列表用的，下拉列表不需要所有字段拿出来，太大了
        if 'in_select' in condition and condition['in_select'] == True:
            _filter.project({
                '_id': 0,
                'id': 1,
                'zone_name': 1,
            })
        else:
            _filter.project({
                'organization.qualification_info': 0,
                'organization.personnel_type': 0,
                'organization.main_account': 0,
                **get_common_project({'organization', 'type_info'})
            })
        res = self.page_query(_filter, self.collection_name, page, count)
        new_result['total'] = res['total']
        if len(res['result']) > 0:
            param = res['result']
            new_result['result'] = self.find_up_area(param)
        return new_result

    def find_up_area(self, data):
        for index in range(len(data)):
            if len(data) > 0:
                if 'upper_hotel_zone_id' in data[index].keys():
                    if data[index]['upper_hotel_zone_id'] != None and data[index]['upper_hotel_zone_id'] != '' and 'zone_name' in data[index].keys():
                        data[index]['upper_hotel_zone_name'] = self.find_up_name(
                            data, data[index]['upper_hotel_zone_id'])
                        new_name = self.select_up_info(
                            data, data[index]['upper_hotel_zone_id'], data[index]['zone_name'], data[index]['id'])
                        if new_name != None and new_name != '':
                            data[index]['zone_name'] = new_name
        return data

    def find_up_name(self, data, upper_hotel_zone_id):
        res = ''
        for index in range(len(data)):
            if data[index]['id'] == upper_hotel_zone_id:
                res = data[index]['zone_name']
        return res

    def select_up_info(self, data, upper_hotel_zone_id, zone_name, self_id):
        new_zone_name = ''
        if upper_hotel_zone_id != self_id:
            for item in data:
                if item['id'] == upper_hotel_zone_id and 'zone_name' in item.keys():
                    new_zone_name = str(
                        item['zone_name']) + '>' + str(zone_name)
                    if 'upper_hotel_zone_id' in item.keys():
                        if item['upper_hotel_zone_id'] != None and item['upper_hotel_zone_id'] != '':
                            new_zone_name = self.select_up_info(
                                data, item['upper_hotel_zone_id'], new_zone_name, item['id'])
        return new_zone_name

    def get_room_status_pure_list(self, org_list, condition, page, count):
        PT_HOTEL_ZONE = 'PT_Hotel_Zone'
        PT_BED = 'PT_Bed'
        PT_USER = 'PT_User'
        PT_BCAR = 'PT_Behavioral_Competence_Assessment_Record'
        keys = ['is_none_user', 'room_type',
                'nursing_level', 'sex', 'organization_id']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter\
            .match_bill((C('organization_id') == values['organization_id'])
                        & (C('organization_id').inner(org_list)))\
            .sort({'zone_number': 1})\
            .project({
                '_id': 0,
                'bill_operator': 0,
                'create_date': 0,
                'modify_date': 0,
                'organization_id': 0,
                'remark': 0,
                **get_common_project()
            })
        res = self.query(_filter, PT_HOTEL_ZONE)

        # 需要对res进行层级排序，否则先后顺序不一致导致前端无法正确显示
        new_item = []
        times = 0
        new_ids_arr = []
        if len(res) > 0:
            while(len(res)) and times < 6:
                temp_res = []
                times += 1
                if times == 1:
                    for item in res:
                        if 'upper_hotel_zone_id' not in item or ('upper_hotel_zone_id' in item and item.get('upper_hotel_zone_id') == ''):
                            new_item.append(item)
                            new_ids_arr.append(item.get('id'))
                        else:
                            temp_res.append(item)
                else:
                    temp_ids_arr = []
                    for item in res:
                        if item.get('upper_hotel_zone_id') in new_ids_arr:
                            new_item.append(item)
                            temp_ids_arr.append(item.get('id'))
                            del(item)
                        else:
                            temp_res.append(item)
                    new_ids_arr = temp_ids_arr
                res = temp_res

        res = new_item

        # 查询所有床位，需要过滤非停用状态的床位
        org_list.append('7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67')
        _filter_bed = MongoBillFilter()
        _filter_bed\
            .match_bill((C('organization_id').inner(org_list)) & ((C('state').exists(False)) | ((C('state').exists(True)) & (C('state') == True))))

        # 查询空床的
        if 'is_none_user' in condition:
            _filter_bed.match_bill(
                (C('residents_id') == '') | (C('residents_id') == None))

        _filter_bed.project({
            '_id': 0,
            'id': 1,
            'bed_code': 1,
            'area_id': 1,
            'name': 1,
            'service_item_package_id': 1,
            'residents_id': 1,
            'organization_id': 1,
        })
        res_bed = self.query(_filter_bed, PT_BED)

        res_obj = {}
        res_bed_obj = {}
        res_user_obj = {}
        res_bed_user_obj = {}
        res_pg_obj = {}
        res_user_ids = []

        # 转换为以id为键值的
        for item in res:
            res_obj[item['id']] = item

        # 转换为以区域id为键值的
        for item in res_bed:
            if 'area_id' in item:
                if item['area_id'] not in res_bed_obj:
                    res_bed_obj[item['area_id']] = []
                res_bed_obj[item['area_id']].append(item)
            # 储存需要获取的入住长者信息
            if 'residents_id' in item and item['residents_id'] != '' and item['residents_id'] != None:
                res_user_ids.append(item['residents_id'])

        if len(res_user_ids) > 0:
            # 先查询长者的评估等级
            _filter_bcar = MongoBillFilter()
            _filter_bcar\
                .match_bill((C('elder').inner(res_user_ids)) & (C('nursing_level').exists(True)))\
                .project({
                    '_id': 0,
                    'elder': 1,
                    'nursing_level': 1,
                    'modify_date': 1,
                })\
                .sort({'modify_date': -1})
            res_bcar = self.query(_filter_bcar, PT_BCAR)

            for item in res_bcar:
                if item['elder'] not in res_pg_obj:
                    res_pg_obj[item['elder']] = item
            # 第一次循环只是给最新的一个评级，下面的循环才是真过滤
            if 'nursing_level' in condition:
                new_res_pg_obj = {}
                for item in res_pg_obj:
                    if res_pg_obj[item]['nursing_level'] == condition['nursing_level']:
                        new_res_pg_obj[item] = res_pg_obj[item]
                res_pg_obj = new_res_pg_obj

            # 查询所有入住的长者
            _filter_user = MongoBillFilter()
            _filter_user\
                .match_bill((C('id').inner(res_user_ids)) & (C('personnel_info.sex') == values['sex']))\
                .add_fields({
                    'sex': '$personnel_info.sex'
                })\
                .project({
                    '_id': 0,
                    'id': 1,
                    'name': 1,
                    'sex': 1,
                })
            res_user = self.query(_filter_user, PT_USER)

            for item in res_user:
                # 补全评估等级
                if item['id'] in res_pg_obj:
                    item['nursing_level'] = res_pg_obj[item['id']]['nursing_level']
                else:
                    if 'nursing_level' in condition:
                        continue
                res_user_obj[item['id']] = item

            # 补全床位的入住长者信息
            # 因为赋予了每个床位的长者信息，所以需要重构一次
            res_bed_obj = {}
            for item in res_bed:
                temp_user = False
                # 储存需要获取的入住长者信息
                if 'residents_id' in item and item['residents_id'] in res_user_obj:
                    # item['user'] = res_user_obj[item['residents_id']]
                    temp_user = res_user_obj[item['residents_id']]
                else:
                    # 在有性别筛选的情况下，把不符合的床位条件去掉
                    if 'sex' in condition:
                        # print('基于筛选性别滤床位', item['name'])
                        continue
                    if 'nursing_level' in condition:
                        # print('基于筛选评估级别过滤床位', item['name'])
                        continue
                    item['user'] = []
                if 'area_id' in item:
                    if item['area_id'] not in res_bed_obj:
                        res_bed_obj[item['area_id']] = []
                    res_bed_obj[item['area_id']].append(item)
                    if item['area_id'] not in res_bed_user_obj:
                        res_bed_user_obj[item['area_id']] = []
                    if temp_user != False:
                        res_bed_user_obj[item['area_id']].append(temp_user)

        newres = []

        for item in res:

            # 补全床位信息
            if item['id'] in res_bed_obj:
                if 'room_type' in condition:
                    if condition['room_type'] != len(res_bed_obj[item['id']]):
                        # print('基于筛选床数筛选床位', item['zone_name'])
                        continue
                item['bed'] = res_bed_obj[item['id']]
                item['bet_count'] = len(res_bed_obj[item['id']])
            else:
                # 旧版本的房态图，当筛选空房的时候，没有空床的房间会被过滤掉，所以这里相应的过滤掉
                # if 'hotel_zone_type_name' in item and item['hotel_zone_type_name'] in ['房']:
                #     print('基于筛选空房筛选床位', item['zone_name'])
                #     continue
                item['bed'] = []
                item['bet_count'] = 0

            # 解决有空数组和数组的问题
            if 'upper_hotel_zone_id' in item and type(item['upper_hotel_zone_id']) == list:
                if len(item['upper_hotel_zone_id']) > 0:
                    item['upper_hotel_zone_id'] = item['upper_hotel_zone_id'][0]
                else:
                    item['upper_hotel_zone_id'] = ''

            # 补全上级名字
            if 'upper_hotel_zone_id' in item and item['upper_hotel_zone_id'] in res_obj:

                item['upper_hotel_zone_name'] = [
                    res_obj[item['upper_hotel_zone_id']]['zone_name']]

                item['upper_element'] = [{
                    'id': res_obj[item['upper_hotel_zone_id']]['id'],
                    'zone_name': res_obj[item['upper_hotel_zone_id']]['zone_name'],
                }]

                item['upper_hotel_zone_id'] = [item['upper_hotel_zone_id']]
            else:
                item['upper_hotel_zone_name'] = []
                item['upper_hotel_zone_id'] = []
                item['upper_element'] = []

            # 补充总体长者信息
            if item['id'] in res_bed_user_obj:
                item['user'] = res_bed_user_obj[item['id']]
            else:
                item['user'] = []

            newres.append(item)

        return {
            'result': newres,
            'total': len(newres)
        }

    def get_organization_list(self, org_list, condition, page, count):

        _filter_hotel_zone = MongoBillFilter()
        _filter_hotel_zone.match_bill((C('organization_id').inner(org_list)))\
            .project({'_id': 0, 'organization_id': 1})
        res_hotel_zone = self.query(_filter_hotel_zone, 'PT_Hotel_Zone')

        hotel_zone_ids = []

        if len(res_hotel_zone) > 0:
            for item in res_hotel_zone:
                if item['organization_id'] not in hotel_zone_ids:
                    hotel_zone_ids.append(item['organization_id'])

        # print('区域总数', len(hotel_zone_ids))

        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_info.personnel_category') == '福利院') & (C('id').inner(hotel_zone_ids)) & (C('id').inner(org_list)))\
            .project({'_id': 0, 'id': 1, 'name': 1, 'modify_date': 1})\
            .sort({'modify_date': -1})
        if page and count:
            res = self.page_query(_filter, 'PT_User', page, count)
        else:
            res = self.query(_filter, 'PT_User')
        return res

    def get_room_status_list(self, org_list, condition, page, count):
        collection_name = 'PT_Hotel_Zone'
        keys = ['is_none_user', 'room_type', 'nursing_level', 'sex']
        values = self.get_value(condition, keys)
        a = ((F('$bed_.bill_status') == 'valid'))
        _filter = MongoBillFilter()
        _filter\
            .match_bill((C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Hotel_Zone', 'upper_hotel_zone_id', 'id', 'upper_hotel_zone')\
            .lookup_bill('PT_Bed', 'id', 'area_id', 'bed_tmp')\
            .project({
                'bed_tmp._id': 0
            })\
            .add_fields({'upper': self.ao.array_filter('$upper_hotel_zone', 'up_', (F('$up_.bill_status') == 'valid').f)})\
            .add_fields({'bed': self.ao.array_filter('$bed_tmp', 'bed_', a.f)})\
            .add_fields({'bet_count': self.ao.size('$bed')})\
            .project({
                '_id': 0,
                'id': 1,
                'zone_name': 1,
                'zone_number': 1,
                'bed': 1,
                'hotel_zone_type_id': 1,
                'upper_hotel_zone_name': '$upper.zone_name',
                'upper_hotel_zone_id': '$upper.id',
                'hotel_zone_type_name': '$hotel_zone_type.name',
                'bet_count': '$bet_count'
            })\
            .sort({'zone_number': 1})\
            .unwind('bed', True)\
            .lookup_bill('PT_User', 'bed.residents_id', 'id', 'user1')\
            .project({
                'user1._id': 0
            })\
            .unwind('user1', True)\
            .add_fields({'bed.user': '$user1'})\
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'bed.user.id', 'elder', 'level_info')\
            .project({
                'level_info._id': 0
            })\
            .add_fields({
                'new_date': self.ao.maximum('$level_info.create_date')
            })\
            .add_fields({
                'new_level_info': self.ao.array_filter('$level_info', 'record', (F('$record.create_date') == '$new_date').f)
            })\
            .add_fields({'nursing_level': '$new_level_info.nursing_level'})\
            .add_fields({'bed.user.nursing_level': '$nursing_level'})\
            .group(
                {
                    'bet_count': '$bet_count',
                    'hotel_zone_type_id': '$hotel_zone_type_id',
                    'id': '$id',
                    'upper_hotel_zone_id': '$upper_hotel_zone_id',
                    'upper_hotel_zone_name': '$upper_hotel_zone_name',
                    'zone_name': '$zone_name',
                    'zone_number': '$zone_number',
                },
                [{
                    'bed': self.ao.add_to_set('bed')
                }, ])\
            .lookup_bill('PT_User', 'bed.residents_id', 'id', 'user')\
            .add_fields({'user': '$user'})\
            .lookup_bill('PT_Behavioral_Competence_Assessment_Record', 'user.id', 'elder', 'level_info')\
            .add_fields({'nursing_level': '$level_info.nursing_level'})\
            .unwind('nursing_level', True)\
            .add_fields({'user.nursing_level': '$nursing_level'})\
            .graph_lookup('PT_Hotel_Zone', '$upper_hotel_zone_id', 'upper_hotel_zone_id', 'id', 'upper_element')\
            .add_fields({'down_element': []})\
            .group(
                {
                    'user': '$user',
                    'bed': '$bed',
                    'bet_count': '$bet_count',
                    'down_element': '$down_element',
                    'hotel_zone_type_id': '$hotel_zone_type_id',
                    'id': '$id',
                    'upper_element': '$upper_element',
                    'level_info': '$level_info',
                    'upper_hotel_zone_id': '$upper_hotel_zone_id',
                    'upper_hotel_zone_name': '$upper_hotel_zone_name',
                    'zone_name': '$zone_name',
                    'zone_number': '$zone_number',

                })\
            .sort({'zone_number': 1})\
            .project({
                'upper_element._id': 0,
                'user._id': 0,
                'level_info._id': 0
            })
        if 'is_none_user' in condition:
            _filter.unwind('bed', True)\
                .match(((C('bed.residents_id') == None))
                       | ((C('bed.residents_id') == '')))\
                .group(
                {
                    'user': '$user',
                    'bet_count': '$bet_count',
                    'down_element': '$down_element',
                    'hotel_zone_type_id': '$hotel_zone_type_id',
                    'id': '$id',
                    'upper_element': '$upper_element',
                    'level_info': '$level_info',
                    'upper_hotel_zone_id': '$upper_hotel_zone_id',
                    'upper_hotel_zone_name': '$upper_hotel_zone_name',
                    'zone_name': '$zone_name',
                    'zone_number': '$zone_number',

                },
                [{
                    'bed': self.ao.add_to_set('bed')
                }, ])\
                .sort({'zone_number': 1})
        if 'room_type' in condition.keys():
            _filter.match(
                ((C('bet_count') == values['room_type']) | (C('bet_count') == 0)))\
                .sort({'zone_number': 1})
        if 'nursing_level' in condition.keys():
            _filter.unwind('bed', True)\
                .match(((C('bed.user.nursing_level') == values['nursing_level']))
                       | ((C('bet_count') == 0)))\
                .group(
                {
                    'user': '$user',
                    'bet_count': '$bet_count',
                    'down_element': '$down_element',
                    'hotel_zone_type_id': '$hotel_zone_type_id',
                    'id': '$id',
                    'upper_element': '$upper_element',
                    'level_info': '$level_info',
                    'upper_hotel_zone_id': '$upper_hotel_zone_id',
                    'upper_hotel_zone_name': '$upper_hotel_zone_name',
                    'zone_name': '$zone_name',
                    'zone_number': '$zone_number',

                },
                [{
                    'bed': self.ao.add_to_set('bed')
                }, ])\
                .sort({'zone_number': 1})
        if 'sex' in condition.keys():
            _filter.unwind('bed', True)\
                .match(((C('bed.user.personnel_info.sex') == values['sex']))
                       | ((C('bet_count') == 0)))\
                .group(
                {
                    'user': '$user',
                    'bet_count': '$bet_count',
                    'down_element': '$down_element',
                    'hotel_zone_type_id': '$hotel_zone_type_id',
                    'id': '$id',
                    'upper_element': '$upper_element',
                    'level_info': '$level_info',
                    'upper_hotel_zone_id': '$upper_hotel_zone_id',
                    'upper_hotel_zone_name': '$upper_hotel_zone_name',
                    'zone_name': '$zone_name',
                    'zone_number': '$zone_number',

                },
                [{
                    'bed': self.ao.add_to_set('bed')
                }, ])\
                .sort({'zone_number': 1})
        res = self.page_query(_filter, collection_name, None, None)

        return res
        # print(res['result'][0])
        # for item in res['result']:
        #     for items in item['user']:
        #         _filter = MongoBillFilter()
        #         _filter.match_bill((C('elder') == items['id']))\
        #             .project({'_id': 0})
        #         assessment_info = self.query(
        #             _filter, 'PT_Behavioral_Competence_Assessment_Record')
        #         if len(assessment_info) > 0:
        #           #print('评估等级',assessment_info)
        #             if 'nursing_level' in assessment_info[0]:
        #                 items['nursing_level'] = assessment_info[0]['nursing_level']
        # return res

    def update_room_status(self, resident_source_id, bed_source_id, bed_dest_id):
        '''
            1、查出床源id（bed_source_id）对应数据
            2、查出床目的id（bed_dest_id）对应数据，取出床对应的长者id，并保存
            3、将源长者id（resident_source_id）修改进2查出的床位id
            4、将2中暂存的长者id存储进1中的长者id
        '''
        # 1、第一步
        collection_name = 'PT_Bed'
        _filter = MongoFilter()
        _filter\
            .match((C('bill_status') == 'valid') &
                   (C('id') == str(bed_source_id))
                   )
        bed_source = self.page_query(_filter, collection_name, None, None)

        # 2、第二步
        _filter = MongoFilter()
        _filter\
            .match((C('bill_status') == 'valid') &
                   (C('id') == str(bed_dest_id))
                   )
        bed_dest = self.page_query(_filter, collection_name, None, None)
        # 处理空床情况
        bed_dest_resident = None
        # 不用对bed_dest["result"][0]判空，在房态图中，床位一定存在，bed_dest['result'][0]同理
        if "residents_id" in bed_dest["result"][0].keys():
            bed_dest_resident = bed_dest['result'][0]['residents_id']

        # 3、第三步
        bed_dest['result'][0]['residents_id'] = resident_source_id

        # 4、第四步
        bed_source['result'][0]['residents_id'] = bed_dest_resident

        is_success_bed_source = self.bill_manage_server.add_bill(OperationType.update.value,
                                                                 TypeId.bed.value, bed_source['result'][0], 'PT_Bed')

        is_success_bed_dest = self.bill_manage_server.add_bill(OperationType.update.value,
                                                               TypeId.bed.value, bed_dest['result'][0], 'PT_Bed')

        self.res = 'Success' if is_success_bed_source and is_success_bed_dest else 'Fail'
        return self.res

    def update(self, hotel_zone):
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in hotel_zone.keys():
                if hotel_zone.get('upper_hotel_zone_id') and isinstance(hotel_zone['upper_hotel_zone_id'], list):
                    hotel_zone['upper_hotel_zone_id'] = hotel_zone['upper_hotel_zone_id'][0]
                bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                           TypeId.hotelZone.value, hotel_zone, 'PT_Hotel_Zone')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.hotelZone.value, hotel_zone, 'PT_Hotel_Zone')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_hotel_zone(self, nursing_id_list):
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for id in nursing_id_list:
                data = find_data(db, 'PT_Hotel_Zone', {
                    'id': id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.hotelZone.value, data[0], 'PT_Hotel_Zone')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_nursing_level_Byid(self, condition):
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('elder') == values['id']))\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
            })
        res = self.page_query(
            _filter, 'PT_Behavioral_Competence_Assessment_Record', 1, 1)
        return res

    def get_user_by_zone(self, id):
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == id)\
            .lookup_bill('PT_Bed', 'id', 'area_id', 'bed')\
               .lookup_bill('PT_User', 'bed.residents_id', 'id', 'user')\
               .add_fields({
                   'user_name': '$user.name',
                   'count': self.ao.size('$user')
               })\
            .project({'_id': 0, 'bed._id': 0, 'user._id': 0})
        res = self.query(_filter, self.collection_name)
        return res
