from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 16:06:43
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\welfare_institution\requirement_option.py
'''
# -*- coding: utf-8 -*-

'''
需求项目函数
'''


class requirementOptionService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_requirement_option_list(self, condition, page, count):
        self._filter = MongoFilter()
        collection_name = 'PT_Requirement_Option'
        keys = ['name', 'option_content', 'remark', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoFilter()
        _filter\
            .match((C('bill_status') == 'valid') & (C('id') == values['id']) & (C('name').like(values['name'])))\
            .project({
                '_id': 0,
            })
        res = self.page_query(_filter, collection_name, page, count)
        return res

    def update(self, requirement_option):
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in requirement_option.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.requirementItem.value, requirement_option, 'PT_Requirement_Option')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.requirementItem.value, requirement_option, 'PT_Requirement_Option')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_requirement_option(self, nursing_id_list):
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for business_id in nursing_id_list:
                data = find_data(db, 'PT_Requirement_Project', {
                    'id': business_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.requirementItem.value, data[0], 'PT_Requirement_Project')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
