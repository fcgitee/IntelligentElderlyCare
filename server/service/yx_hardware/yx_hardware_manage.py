from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
import requests
import json
import hashlib

from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, gcj02_to_bd09, wgs84_to_gcj02, post_requests
import time
from urllib.request import urlopen, quote

import math

post_url = 'http://fzd.xcloudtech.com:9090/DeviceMonitor/'
app_id = 'OC41OTc0MzA3NQ=='
app_key = 'B2EEB76BBEFD4759B5FF219AE275FFF4'


class YxHardwareService(MongoService):

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_position(self, imei_id):
        '''
            查看位置
            imei_id: 86111111111
        '''
        # 调用https请求
        url_x = post_url + 'deviceLocationQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'deviceList': imei_id})
        # print('result>>>', result)
        if result['Ret'] == 'Succ':
            res_data = result['data']
            for data in res_data:
                if 'Lat' in data.keys() and 'Lon' in data.keys():
                    lat = data['Lat']
                    lng = data['Lon']
                    # 高德转百度经纬度
                    bd_lng, bd_lat = gcj02_to_bd09(lng, lat)
                    # 纬度
                    data['Lat'] = bd_lat
                    # 经度
                    data['Lon'] = bd_lng
                else:
                    # 纬度
                    data['Lat'] = ''
                    # 经度
                    data['Lon'] = ''
            res = {'Ret': result['Ret'], 'Msg': '获取成功', 'data': res_data}
        else:
            res = {'Ret': result['Ret'], 'Msg': result['Msg'], 'data': []}
        return res

    def get_footprint(self, imei_id, query_date):
        '''
            设备足迹查询
            imei_id: 86111111111
            query_date： YYYYMMDD
        '''
        # time_ = int(time.time())
        # 调用https请求
        url_x = post_url + 'deviceFootprintQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': query_date})

        # 测试数据
        # result = {"Ret": 'Succ', 'data': {
        #     'CityNum': 1, 'PosNum': 2, 'Pos':
        #     [{
        #         'City': '广州市', 'Detail':
        #         [{'_id': '55cc463d3f183aab0c6f6d3f', 'Lon': 113.314935, 'Lat': 23.155923, 'Radius': 35, 'Dist': '天河区', 'Str': '江园街靠近望星楼(江园街店)', 'CT': '2016-03-02 00: 00: 23', 'UT': '2016-03-02 16: 05: 21', 'Tag': 1}, {'_id': '55cc463d3f183aab0c6f6d3g', 'Lon': 113.314786, 'Lat': 23.155898, 'Radius': 41, 'Dist': '天河区', 'Str': '江园街靠近望星楼(江园街店)', 'CT': '2016-03-02 17: 29: 16', 'UT': '2016-03-02 23: 59: 34', 'Tag': 1}
        #          ]
        #     }]
        # }}

        Ret = result['Ret']
        if Ret == 'Succ':
            res_data = result['data']
            for city_data in result['data']['Pos']:
                detail = city_data['Detail']
                for detail_data in detail:
                    if 'Lat' in detail_data.keys() and 'Lon' in detail_data.keys():
                        lat = detail_data['Lat']
                        lng = detail_data['Lon']
                        # 高德转百度经纬度
                        bd_lng, bd_lat = gcj02_to_bd09(lng, lat)
                        # 纬度
                        detail_data['Lat'] = bd_lat
                        # 经度
                        detail_data['Lon'] = bd_lng
                    else:
                        # 纬度
                        detail_data['Lat'] = ''
                        # 经度
                        detail_data['Lon'] = ''
            res = {'Ret': result['Ret'], 'Msg': '获取成功', 'data': res_data}
        else:
            res = {'Ret': result['Ret'], 'Msg': result['Msg'], 'data': {}}
        return res

    def set_sos(self, imei_id, sos):
        '''
            设备手表sos号码
            imei_id: 86111111111
            sos: 字符串格式，sos号码以逗号分隔，例如为'13662313580,18898815088'
        '''
        # 调用https请求
        url_x = post_url + 'setSOS.action'
        result = post_requests(url_x, {'UID': imei_id, 'SOS': sos})
        return result

    # ------------------------以后实现的功能接口-------------------------------------
    def set_location_frequency(self, imei_id, fqcy):
        '''
            设备足迹查询
            imei_id: 86111111111
            fqcy: 定位间隔，整型，单位秒
        '''
        # 调用https请求
        url_x = post_url + 'locationFrequency.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'FQCY': fqcy})
        return result

    def get_remote_heart_rate(self, imei_id):
        '''
            远程测心率数据（实时）
            imei_id: 86111111111
        '''
        # 调用https请求
        url_x = post_url + 'remoteHeartRate.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id})
        return result

    def get_device_heart_info(self, imei_id, date):
        '''
            心率数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'deviceHeartInfoQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result

    def get_device_blood_pressure_info(self, imei_id, date):
        '''
            血压数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'deviceBloodPressureInfoQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result

    def get_device_blood_oxygen(self, imei_id, date):
        '''
            血氧数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'deviceBloodOxygenQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result

    def get_device_step_info(self, imei_id, date):
        '''
            计步数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'deviceStepInfoQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result

    def get_device_sleep_info(self, imei_id, date):
        '''
            睡眠数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'deviceSleepInfoQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result

    def get_wd_info(self, imei_id, date):
        '''
            体温数据查询
            imei_id: 86111111111
            date: 查询数据的日期 YYYYMMDD
        '''
        # 调用https请求
        url_x = post_url + 'wdInfoQuery.action'
        result = post_requests(url_x, {
                               'appId': app_id, 'appKey': app_key, 'MID': imei_id, 'date': date})
        return result
