'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 17:14:46
@LastEditTime: 2019-10-11 14:31:01
@LastEditors: Please set LastEditors
'''
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from enum import Enum
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, get_cur_time
from ...service.mongo_bill_service import MongoBillFilter
# -*- coding: utf-8 -*-

'''
过程、步骤
'''


class ApprovalProcessService(MongoService):
    ''' 补贴管理 '''
    Allowance = 'PT_Allowance_Manage'
    
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def Process(self,user_res,data_info,):
        '''# 新增/修改申请'''
        # user_res 传参过来
        # data_info 传参过来
        #
        res = 'Fail'
        # 根据审批定义新增审核过程表数据，新增业务表数据
        _filter = MongoBillFilter()
        _filter.match_bill((C('approval_type') == 'subsidy'))\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Approval_Define')
        if len(res) > 0 and len(res[0]['approval_list']) > 0:
            for defineList in res:
                # 工作人员
                if defineList['type'] == "工作人员":
                    process_data_list = []
                    for approval in defineList['approval_list']:
                        # OrganizationID = ""
                        if approval['type'] == 'self':
                            # 根据用户的id找出所属机构id(所属服务商)
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('principal_account_id') == user_res[0]['id']))\
                                .project({'_id': 0})
                            aboutOrganizationIDList = self.query(_filter, 'PT_Set_Role')
                            aboutOrganizationID = aboutOrganizationIDList[0]['role_of_account_id']
                            # print(1111111111111111111111111111111111111111111111111111111111111,aboutOrganizationID)
                            # 根据服务商id找到所属行政区划
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == aboutOrganizationID))\
                                .project({'_id': 0})
                            aboutOrganizationInfo = self.query(_filter, 'PT_User')
                            area_id = aboutOrganizationInfo[0]['admin_area_id']
                            # print(22222222222222222222222222222222222222222222222222222222222222,area_id)
                            # 根据行政区划id找到当前区划下的所有组织机构,并从所有机构中找出民政局机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == area_id) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            AreaOrganizationInfo = self.query(_filter, 'PT_User')
                            mzOrganizationId = AreaOrganizationInfo[0]['id']
                            # print(33333333333333333333333333333333333333333333333333333333333333,mzOrganizationId)
                            # 根据角色id以及机构id确认所对应的用户id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == mzOrganizationId))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(_filter, 'PT_Set_Role')
                            # print(4444444444444444444444444444444444444444444444444444444444444,aboutUserIdList)
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            # print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                            process_data = {"define_id": res[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data_info['step_no'] = process_data_list[0]['step_no']
                            data = [data_info, process_data_list]
                            # print("xixiixixxixixixixixixixixixixixix",data)
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.allowance.value, data, [self.Allowance, 'PT_Approval_Process'])
                            # print(5555555555555555555555555555555555555555555555555555555555555555)
                        # 找到上级行政区划的机构id
                        if approval['type'] == 'next':
                            # 根据用户的id找出所属机构id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('principal_account_id') == user_res[0]['id']))\
                                .project({'_id': 0})
                            aboutOrganizationIDList = self.query(_filter, 'PT_Set_Role')
                            towmOrganizationID = aboutOrganizationIDList[0]['role_of_account_id']
                            # print(66666666666666666666666666666666666666666666666666666)
                           # 先查找当前机构的行政区划
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == towmOrganizationID))\
                                .project({'_id': 0})
                            aboutOrganizationInfo = self.query(_filter, 'PT_User')
                            area_id = aboutOrganizationInfo[0]['admin_area_id']
                            # print(7777777777777777777777777777777777777777777777777777777777)
                            # 根据当前行政区划id查找上一级区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == area_id))\
                                .project({'_id': 0})
                            areaInfo = self.query(_filter, 'PT_Administration_Division')
                            parentId = areaInfo[0]['parent_id']
                            # print(888888888888888888888888888888888888888888888888888888)
                            # 根据上级区划id查找当前区域下的所有组织机构,并从所有机构中找出民政局机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == parentId) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            upAreaOrganizationId = self.query(_filter, 'PT_User')
                            # print(9999999999999999999999999999999999999999999999999999999)
                            # OrganizationID = upAreaOrganizationInfo['']
                            # 根据角色id以及机构id确认所对应的用户id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == upAreaOrganizationId[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(_filter, 'PT_Set_Role')
                            # print(1010101010101010101010101011010101)
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            process_data = {"define_id": res[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            # print("hhahahhhahahahhahhahahahahahahah",process_data_list)
                            data_info['step_no'] = process_data_list[1]['step_no']
                            data = [data_info, process_data_list]
                            # print('bobobobobobobobobobobobobobobo',data)
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.allowance.value, data, [self.Allowance, 'PT_Approval_Process'])
                            # print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        res = 'Success'
                        return res
                if defineList['type'] == "长者":
                    process_data_list = []
                    for approval in defineList['approval_list']:
                        if approval['type'] == 'self':
                            # 根据长者的信息去user表查所属行政区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == user_res[0]['id']))\
                                .project({'_id': 0})
                            elderInfo = self.query(_filter, 'PT_User')
                            elderInfo = self.query(_filter, 'PT_User')
                            # print("长者11111111111111111111111111111111111111111111",elderInfo)
                            # 根据行政区划id找出当前区划下的民政机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == elderInfo[0]['admin_area_id']) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            mzOrganizationInfo = self.query(_filter, 'PT_User')
                            # print("长者2222222222222222222222222222222222",mzOrganizationInfo)
                            # 根据角色id以及机构id找出审核人id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == mzOrganizationInfo[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(_filter, 'PT_Set_Role')
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            # print("长者33333333333333333333333333333333333333333333",approval_user_id)
                            process_data = {"define_id": res[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data_info['step_no'] = process_data_list[0]['step_no']
                            data = [data_info, process_data_list]
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.allowance.value, data, [self.Allowance, 'PT_Approval_Process'])
                        if approval['type'] == 'next':
                            # 根据长者的信息去user表查所属行政区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == user_res[0]['id']))\
                                .project({'_id': 0})
                            elderInfo = self.query(_filter, 'PT_User')
                            elderInfo = self.query(_filter, 'PT_User')
                            # print("长者444444444444444444444444444444444444",elderInfo)
                            # 根据当前行政区划id查找上一级区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == elderInfo[0]['admin_area_id']))\
                                .project({'_id': 0})
                            upAreaInfo = self.query(_filter, 'PT_Administration_Division')
                            parentId = upAreaInfo[0]['parent_id']
                            # 根据上一级行政区划id找出区划下的民政机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == parentId) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            upAreaOrganizationInfo = self.query(_filter, 'PT_User')
                            # print("长者55555555555555555555555555555555",upAreaOrganizationId)
                            # 根据角色id和机构id确定审核人id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == upAreaOrganizationInfo[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(_filter, 'PT_Set_Role')
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            # print("长者666666666666666666666666666",approval_user_id)
                            process_data = {"define_id": res[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data_info['step_no'] = process_data_list[0]['step_no']
                            data = [data_info, process_data_list]
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.allowance.value, data, [self.Allowance, 'PT_Approval_Process'])
                        if approval['type'] == 'next_x':
                            # 根据长者的信息去user表查所属行政区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == user_res[0]['id']))\
                                .project({'_id': 0})
                            elderInfo = self.query(_filter, 'PT_User')
                            elderInfo = self.query(_filter, 'PT_User')
                            # print("长者444444444444444444444444444444444444",elderInfo)
                            # 根据当前行政区划id查找上一级区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == elderInfo[0]['admin_area_id']))\
                                .project({'_id': 0})
                            upAreaInfo = self.query(_filter, 'PT_Administration_Division')
                            parentId = upAreaInfo[0]['parent_id']
                            # 根据上一级行政区划id去找再上一级的行政区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == parentId))\
                                .project({'_id': 0})
                            upParentAreaInfo = self.query(_filter, 'PT_Administration_Division')
                            upParentId = upParentAreaInfo[0]['parent_id']
                            # 根据上一级行政区划id找出区划下的民政机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == upParentId) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            upAreaOrganizationInfo = self.query(_filter, 'PT_User')
                            # print("长者55555555555555555555555555555555",upAreaOrganizationId)
                            # 根据角色id和机构id确定审核人id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == upAreaOrganizationInfo[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(_filter, 'PT_Set_Role')
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            # print("长者666666666666666666666666666",approval_user_id)
                            process_data = {"define_id": res[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data_info['step_no'] = process_data_list[0]['step_no']
                            data = [data_info, process_data_list]
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.allowance.value, data, [self.Allowance, 'PT_Approval_Process'])
                            res = 'Success'
                            return res
    