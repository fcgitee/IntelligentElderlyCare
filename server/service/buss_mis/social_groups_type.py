from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_serial_number, SerialNumberType
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, data_to_string_date
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-20 11:29:53
@LastEditTime: 2019-12-05 16:02:43
@LastEditors: Please set LastEditors
'''
# -*- coding: utf-8 -*-

'''
社会群体类型函数
'''


class SocialGroupsTypeService(MongoService):
    ''' 社会群体类型管理 '''
    SocialGroupsType = 'PT_Social_Groups_Type'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_social_groups_type_list(self, org_list, condition, page=None, count=None):
        '''获取社会群体类型'''
        keys = ['id', 'name', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('name').like(values['name']))
                           & (C('type') == values['type'])
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(_filter, self.SocialGroupsType, page, count)
        return res

    def update_social_groups_type(self, socialGroupsType):
        '''# 修改社会群体类型 '''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            data_info = get_info({**socialGroupsType}, self.session)
            if 'id' in socialGroupsType.keys():
                flag = OperationType.update.value
            else:
                flag = OperationType.add.value
            bill_id = self.bill_manage_server.add_bill(
                flag, TypeId.socialGroupType.value, data_info, self.SocialGroupsType)
            if bill_id:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_social_groups_type(self, ids):
        ''' 删除社会群体类型 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.socialGroupType.value, {'id': i}, self.SocialGroupsType)
        if bill_id:
            res = 'Success'
        return res
