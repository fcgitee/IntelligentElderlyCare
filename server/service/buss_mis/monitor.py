'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 16:00:29
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\monitor.py
'''

from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_common_project
from ...service.buss_pub.bill_manage import BillManageService, TypeId, OperationType
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date


class MonitorService(MongoService):
    '''监控管理'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_monitor_list(self, org_list, condition, page, count):
        '''
            获取监控设备列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        keys = ['id', 'title', 'address', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .match_bill(
                (C('id') == values['id'])
                & (C('title').like(values['title']))
                & (C('org.name').like(values['org_name']))
                & (C('address').like(values['address'])) & (C('organization_id').inner(org_list)))\
            .project({'org.qualification_info': 0, **get_common_project({'org'})}).sort({'modify_date': -1})
        res = self.page_query(_filter, 'PT_Monitor', page, count)
        return res

    def update_monitor(self, monitor):
        '''添加/修改视频监控设备

        Arguments:
            monitor   {dict}      条件
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            data = get_info({**monitor}, self.session)
            if 'id' in monitor.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.monitor.value, data, 'PT_Monitor')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.monitor.value, data, 'PT_Monitor')
                if bill_id:
                    res = 'Success'

        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_monitor(self, ids):
        '''删除视频监控设备

        Arguments:
            device   {dict}      条件
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            for device_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Monitor', {
                    'id': device_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.monitor.value, data[0], 'PT_Monitor')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
