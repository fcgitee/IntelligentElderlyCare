
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
from requests import post
import requests
import json
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import get_common_project, insert_data, find_data, update_data, get_current_organization_id, get_user_id_or_false, get_current_user_id, delete_data, get_condition, get_info, get_string_time, get_str_to_age, gcj02_to_bd09
from ...service.buss_pub.bill_manage import BillManageService, TypeId, OperationType
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mg_hardware.mg_hardware_manage import MgHardwareService
from ...service.yx_hardware.yx_hardware_manage import YxHardwareService
from ...service.by_hardware.by_hardware_manage import ByHardwareService
from ...pao_python.pao.data import get_cur_time


class DeviceService(MongoService):
    '''设备管理'''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.mg_hardware_service = MgHardwareService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.yx_hardware_service = YxHardwareService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.by_hardware_service = ByHardwareService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

        self.device_cache = {

        }

    def get_device(self, org_list, condition, page, count):
        '''
            获取设备列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        keys = ['id', 'device_name', 'device_id',
                'elder_name', 'id_card', 'device_sim', 'user_id']
        # print(condition,page,count)
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('device_name').like(values['device_name']))
                           & (C('device_id') == values['device_id'])
                           & (C('device_sim') == values['device_sim'])
                           & (C('elder_id') == values['user_id'])
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'user')\
            .add_fields({
                "elder_name": self.ao.array_elemat("$user.name", 0),
                "id_card": self.ao.array_elemat("$user.id_card", 0),
                "telephone": self.ao.array_elemat("$user.personnel_info.telephone", 0),
                "address": self.ao.array_elemat("$user.personnel_info.address", 0),
                "sex": self.ao.array_elemat("$user.personnel_info.sex", 0),
            })\
            .match_bill((C('elder_name').like(values['elder_name'])) & (C('id_card').like(values['id_card'])))\
            .sort({'modify_date': -1})\
            .project({'_id': 0, 'user': 0, **get_common_project()})
        res = self.page_query(_filter, 'PT_Device', page, count)
        # for i, x in enumerate(res['result']):
        #         if len(res['result'][i]['user']) > 0:
        #             data = res['result'][i]['user'][0]['personnel_info']
        #             if 'date_birth' in data.keys() and data['date_birth'] != '' and data['date_birth'] != None:
        #                 date_res = get_string_time(
        #                     data['date_birth'], '%Y-%m-%d')
        #                 data['age'] = get_str_to_age(date_res)
        # res['test'] = self.test111()

        self.autoUpdateDeviceTerm(res)

        return res

    def autoUpdateDeviceTerm(self, result):
        if 'result' in result and len(result['result']) > 0:
            for item in result['result']:
                if 'service_date_type' in item and item['service_date_type'] == '年月日' and 'service_date_end' in item:
                    # 判断是否今天到期
                    now_date = datetime.datetime.now()
                    if "service_date_end" in item and item['service_date_end'].year == now_date.year and item['service_date_end'].month == now_date.month and item['service_date_end'].day == now_date.day and 'duration' in item and item['duration'] in ['one_month', 'one_year']:
                        adays = 0
                        if item['duration'] == 'one_month':
                            adays = 30
                        elif item['duration'] == 'one_year':
                            adays = 365

                        if adays > 0:
                            service_date_end = now_date - datetime.timedelta(
                                hours=now_date.hour, minutes=now_date.minute, seconds=now_date.second, microseconds=now_date.microsecond) + \
                                datetime.timedelta(days=adays, hours=23,
                                                   minutes=59, seconds=59)

                            def process_func_update_datas(db):
                                update_data(db, 'PT_Device', {
                                    'service_date_end': service_date_end
                                }, {
                                    'id': item['id'],
                                    'bill_status': 'valid',
                                })
                            process_db(self.db_addr, self.db_port, self.db_name,
                                       process_func_update_datas, self.db_user, self.db_pwd)

    def update_device(self, device):
        '''绑定设备

        Arguments:
            device   {dict}      条件
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'service_date_type' in device.keys():
                # if device['service_date'] == '一年':
                #     device['service_date_end'] = datetime.datetime(datetime.datetime.now().year + 1, datetime.datetime.now(
                #     ).month, datetime.datetime.now().day, datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second)
                # if device['service_date'] == '两年':
                #     device['service_date_end'] = datetime.datetime(datetime.datetime.now().year + 2, datetime.datetime.now(
                #     ).month, datetime.datetime.now().day, datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second)
                # if device['service_date'] == '三年':
                #     device['service_date_end'] = datetime.datetime(datetime.datetime.now().year + 3, datetime.datetime.now(
                #     ).month, datetime.datetime.now().day, datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second)
                if device['service_date_type'] == '无限':
                    device['service_date_end'] = datetime.datetime(datetime.datetime.now().year + 1000, datetime.datetime.now(
                    ).month, datetime.datetime.now().day, datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second)
                else:
                    device['service_date_end'] = as_date(
                        device['service_date_end'][0:10] + "T23:59:59.00Z")
            if 'device_name' in device.keys() and device['device_name'] == '咪狗' and 'emergency_call' in device.keys() and 'emergency_qz_phone' in device.keys() and 'emergency_family_phone' in device.keys():
                # 绑定设备的手机
                res_set_sos = self.mg_hardware_service.setSos(device['device_id'], [{"name": "紧急呼叫电话", "phone": int(device['emergency_call'])}, {
                    "name": "紧急求助电话", "phone": int(device['emergency_qz_phone'])}, {"name": "呼叫亲情电话", "phone": int(device['emergency_family_phone'])}])
                if res_set_sos['code'] != 0:
                    res = 'fail'
                    return res
                # token_url = 'https://182.254.159.127:8443/sos_api/login'
                # token_headers = {
                #     "Content-Type": "application/json; charset=UTF-8"}
                # token_params = json.dumps(
                #     {"user": "EagleSoS", "time": 1500666888, "pwdauth": "e874964919c37c35"})
                # token = requests.post(
                #     token_url, data=token_params, headers=token_headers, verify=False).json()
                # if token['code'] == 0:
                #     tokens = token['data']['token']
                #     set_url = 'https://182.254.159.127:8443/sos_api/sos_set'
                #     set_headers = {
                #         "Content-Type": "application/json; charset=UTF-8", "token": tokens}
                # set_params = json.dumps({"user": "sos_server", "sn": '80000' + device['device_id'], "nums": [{"name": "紧急呼叫电话", "phone": int(device['emergency_call'])}, {
                #                         "name": "紧急求助电话", "phone": int(device['emergency_qz_phone'])}, {"name": "呼叫亲情电话", "phone": int(device['emergency_family_phone'])}]})
                #     set_result = requests.post(
                #         set_url, data=set_params, headers=set_headers, verify=False).json()
                #     if set_result['code'] != 0:
                #         res = 'fail'
                #         return res
            if 'device_name' in device.keys() and device['device_name'] == '孝心环' and 'emergency_call' in device.keys() and 'emergency_qz_phone' in device.keys() and 'emergency_family_phone' in device.keys():
                url_L = 'http://2101xe1486.iask.in/Open/SendCommand' + '?Imei=' + \
                    device['device_id'] + '&CmdCode=0002&Params=' + device['emergency_call'] + \
                        '%2C' + device['emergency_qz_phone'] + \
                    '%2C' + device['emergency_family_phone']
                params_L = json.dumps({"Imei": device['device_id'], "CmdCode": '0002', "Params": device['emergency_call'] +
                                       ',' + device['emergency_qz_phone'] + ',' + device['emergency_family_phone']})
                headers_L = {"Content-Type": "application/json; charset=UTF-8"}
                result_L = requests.post(
                    url_L, data=params_L, headers=headers_L).json()
                if result_L['State'] != 0:
                    res = 'fail'
                    return res
            if 'device_name' in device.keys() and device['device_name'] == '云芯' and 'emergency_call' in device.keys() and 'emergency_qz_phone' in device.keys():
                # 云芯很特殊，需要先查询定位接口获取uid
                res_get_position = self.yx_hardware_service.get_position(
                    device['device_id'])
                if res_get_position['Ret'] == 'Succ':
                    uid = res_get_position['data'][0]['UID']
                    # print('uid>>', uid)
                    # 绑定设备的手机
                    res_set_sos = self.yx_hardware_service.set_sos(
                        uid, '' + device['emergency_call'] + ',' + device['emergency_qz_phone'])
                    if res_set_sos['Ret'] != 'Succ':
                        res = 'fail'
                        return res
            condition = {}
            if 'id' in device.keys():
                condition = {'id': device['id']}
            keys = ['id']
            values = self.get_value(condition, keys)
            # 判断设备ID是已存在
            _filter_device = MongoBillFilter()
            _filter_device.match_bill((C('id') != values['id']) & (C('device_name') == device['device_name']) & (C('device_id') == device['device_id']))\
                .project({'_id': 0})
            res_device = self.query(_filter_device, 'PT_Device')
            if len(res_device) > 0:
                res = '该设备Id已存在，不能重复'
                return res
            if 'id' in device.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.device.value, device, 'PT_Device')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.device.value, device, 'PT_Device')
                if bill_id:
                    res = 'Success'

        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def update_device_weilan(self, condition):
        ''' 更新电子围栏 '''
        res = 'Failed'
        if 'id' in condition.keys():

            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('id') == condition['id'])
            )\
                .project({"_id": 0, 'device_id': 1, 'device_name': 1, 'id': 1})
            res_device = self.query(_filter, 'PT_Device')

            if len(res_device) == 0 or 'device_id' not in res_device[0]:
                return '没有找到该设备'

            if res_device[0]['device_name'] == '柏颐':
                # 设置柏颐的电子围栏，只支持正方形
                device_result = self.by_hardware_service.set_fence(
                    res_device[0]['device_id'], condition.get('overlay').get('points'))

                # print('设置柏颐：', device_result)

                if device_result['success'] != True:
                    return res

            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.device.value, condition, 'PT_Device')
            if bill_id:
                res = 'Success'
        return res

    def delete_device(self, ids):
        '''解除绑定设备

        Arguments:
            device   {dict}      条件
        '''
        res = 'fail'
        for id in ids:
            data = {'id': id}
            bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                        TypeId.device.value, data, 'PT_Device')
            if bill_id:
                res = 'Success'

        return res

    def import_device(self, condition):
        pf = pd.DataFrame(condition)
        pf.rename(columns={'长者姓名': 'name',
                           '身份证': "id_card",
                           '设备ID': 'device_id',
                           '设备SIM卡': 'device_sim',
                           '设备名称': 'device_name',
                           }, inplace=True)
        id_card_list = pf['id_card'].values.tolist()
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('personnel_info.id_card').inner(id_card_list) | (C('id_card').inner(id_card_list))
             )
        )\
            .project({"_id": 0})
        res = self.query(_filter, 'PT_User')
        user_pf = pd.DataFrame(res)
        not_exit = []  # 不存在的长者
        for x in id_card_list:
            if not x in user_pf['id_card'].values.tolist() and not x in user_pf['personnel_info']['id_card'].values.tolist():
                not_exit.append(x)
        if len(not_exit):
            return str(not_exit) + '长者不存在，请先导入长者数据'
        else:
            user_pf = user_pf[['id', 'id_card']]
            new_pf = pd.merge(pf, user_pf, on=['id_card'])
            new_pf = new_pf[['id', 'device_id', 'device_sim', 'device_name']]
            new_pf['organization_id'] = new_pf.apply(
                lambda x: get_current_organization_id(self.session), axis=1)
            new_pf['emergency_call'] = new_pf.apply(
                lambda x: '', axis=1)
            new_pf['emergency_qz_phone'] = new_pf.apply(
                lambda x: '', axis=1)
            new_pf['emergency_family_phone'] = new_pf.apply(
                lambda x: '', axis=1)
            new_pf['import_date'] = new_pf.apply(
                lambda x: '20200803', axis=1)
            new_pf.rename(columns={'id': 'elder_id', }, inplace=True)
            import_elder = new_pf['elder_id'].values.tolist()
            # 判定是否已经存在
            _filter_device = MongoBillFilter()
            _filter_device.match_bill(C('elder_id').inner(import_elder))\
                          .project({'_id': 0})
            res_device = self.query(_filter_device, 'PT_Device')
            if len(res_device):
                device_elder_pf = pd.DataFrame(res_device)
                device_elder_id = device_elder_pf['elder_id'].values.tolist()
                new_pf = new_pf[~new_pf['elder_id'].isin(device_elder_id)]
            new_data = dataframe_to_list(new_pf)
            if len(new_data):
              # print(new_data)
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.device.value, new_data, 'PT_Device')
                return '导入成功' if bill_id else '导入失败'
            else:
                return '没有可导入的新数据，请检查设备是否已存在'

    def get_device_log(self, org_list, condition, page, count):
        '''
            获取设备日志列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match(C('device_id') == values['id'] & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(_filter, 'PT_Device_Log', page, count)
        return res

    def device_signin(self, device_type):
        ''' 设备签入 '''
        header = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Connection': 'keep-alive',
            'Accept': '*/*'
        }
        # 云芯的
        # condition = {
        #     'appId': 'OC41OTc0MzA3NQ==',
        #     'appKey': 'B2EEB76BBEFD4759B5FF219AE275FFF4',
        #     'MID': '860315001021717',
        #     'date': '20200915',
        # }
        # url = 'http://fzd.xcloudtech.com:9090/DeviceMonitor/deviceFootprintQuery.action'

        if device_type == 'T007':
            condition = {
                # 登录帐号
                'username': '18680006869',
                # 登录密码
                'password': 'Abc123456',
            }

        url = 'http://api.aiqiangua.com:8888/api/auth/login'
        res = requests.post(json=condition, url=url,
                            headers=header)

        cookie = res.headers['Set-Cookie']

        # result = res.json()

        return cookie

    def get_device_location(self):
        ''' 获取设备定位数据 '''

        headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Connection': 'keep-alive',
            'Accept': '*/*'
        }

        headers['Cookie'] = self.device_cache['T007_cookie']

        url = 'http://api.aiqiangua.com:8888/api/locationdata/?device=889464703021695&depth=1&rows_per_page=1'

        res_location = requests.get(url, headers=headers)

        res_json = res_location.json()

        print('result>>>', res_json)

    def device_api(self):

        self.device_cache['T007_cookie'] = 'user=2|1:0|10:1603771062|4:user|16:MTg2ODAwMDY4Njk=|426b5667300cf65ae24b8e468dcba0829eac5d9c4258055ec6b863a1e54e00f9; Expires=Wed, 28-Oct-2020 03:57:42 GMT; Max-Age=86400; Path=/'

        # 设备签入
        # if 'T007_cookie' not in self.device_cache:
        #     T007_cookie = self.device_signin('T007')
        #     print('T007_cookie>>>', T007_cookie)
        #     self.device_cache['T007_cookie'] = self.device_signin('T007')
        #     print('设备签入成功！')

        # self.get_device_location()
        # self.set_device_fence()

    def device_api_callback(self, action, request, bill_manage_service):
        if request.method == 'POST':
            if action in ['SOS', 'LOCATION', 'TEMPERATURE', 'ELECTRICITY', 'MESSAGE']:
                param = {**request.form, 'action': action, 'status': 0}
                bill_id = bill_manage_service.add_bill(OperationType.add.value,
                                                       TypeId.deviceMessage.value, param, 'PT_Device_Message')
                if bill_id:
                    return 'success'
        try:
            param = {**request.form}
            bill_id = bill_manage_service.add_bill(OperationType.add.value,
                                                   TypeId.deviceMessage.value, param, 'PT_Device_Message_Unuse')
        except Exception as ex:
            pass
        return 'failed'

    def get_device_self(self, condition, page, count):

        user_id = get_current_user_id(self.session)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('user_id') == user_id))\
            .lookup_bill('PT_Device', 'device_id', 'id', 'device')\
            .add_fields({
                "device_name": self.ao.array_elemat("$device.device_name", 0),
                "elder_id": self.ao.array_elemat("$device.elder_id", 0),
            })\
            .lookup_bill('PT_User', 'elder_id', 'id', 'user')\
            .add_fields({
                "elder_name": self.ao.array_elemat("$user.name", 0),
            })\
            .project({
                '_id': 0,
                'id': 1,
                'elder_name': 1,
                'device_name': 1,
                'device_id': 1,
                'create_date': 1,
            })
        res = self.page_query(_filter, 'PT_Device_Bind', page, count)

        return res

    def bind_device_self(self, condition):

        keys = ['id', 'device_id']
        values = self.get_value(condition, keys)

        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return '用户未登录！'

        # 判断设备是否存在
        _filter_device = MongoBillFilter()
        _filter_device.match_bill(
            C('device_id') == condition['imei'])\
            .project({'_id': 0})
        res_device = self.query(_filter_device, 'PT_Device')

        if len(res_device) == 0:
            return '没有找到该设备！'

        # 判断设备是否已绑定
        _filter_bind = MongoBillFilter()
        _filter_bind.match_bill(
            (C('user_id') == user_id)
            & (C('device_id') == res_device[0]['id']))\
            .project({'_id': 0})
        res_bind = self.query(_filter_bind, 'PT_Device_Bind')

        if len(res_bind) > 0:
            return '该设备已经绑定此用户！'

        bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                    TypeId.deviceBind.value, {'user_id': user_id, 'device_id': res_device[0]['id']}, 'PT_Device_Bind')

        if bill_id:
            return 'Success'
        else:
            return 'Failed'

    def unbind_device_self(self, condition):

        keys = ['id', 'device_id']
        values = self.get_value(condition, keys)

        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return '用户未登录！'

        # 判断设备是否存在
        _filter_device = MongoBillFilter()
        _filter_device.match_bill(
            C('device_id') == condition['imei'])\
            .project({'_id': 0})
        res_device = self.query(_filter_device, 'PT_Device')

        if len(res_device) == 0:
            return '没有找到该设备！'

        # 判断设备是否已绑定
        _filter_bind = MongoBillFilter()
        _filter_bind.match_bill(
            (C('user_id') == user_id)
            & (C('device_id') == res_device[0]['id']))\
            .project({'_id': 0})
        res_bind = self.query(_filter_bind, 'PT_Device_Bind')

        if len(res_bind) == 0:
            return '该设备尚未绑定此用户！'

        bill_id = self.bill_manage_service.add_bill(OperationType.delete.value,
                                                    TypeId.deviceBind.value, {'user_id': user_id, 'device_id': res_device[0]['id']}, 'PT_Device_Bind')

        if bill_id:
            return 'Success'
        else:
            return 'Failed'

    def get_device_footprint(self, condition):

        user_id = get_user_id_or_false(self.session)

        now_time = get_cur_time()

        now_ymd = str(now_time.year) + \
            str(now_time.month).zfill(2) + str(now_time.day).zfill(2)

        if user_id == False:
            return {
                'code': 403,
                'msg': '用户未登录！',
            }

        condition2 = {}

        # 根据绑定关系表的主键id获取设备imei
        if condition.get('from') == 'relation':
            # 根据id从绑定表获取设备id
            _filter_bind = MongoBillFilter()
            _filter_bind.match_bill(
                (C('id') == condition.get('id')))\
                .project({'_id': 0})
            res_bind = self.query(_filter_bind, 'PT_Device_Bind')

            if len(res_bind) == 0:
                return {
                    'code': 404,
                    'msg': '未找到该绑定数据！',
                }

            if res_bind[0]['user_id'] != user_id:
                return {
                    'code': 404,
                    'msg': '该设备未绑定此用户！',
                }

            condition2['id'] = res_bind[0]['device_id']

        elif condition.get('from') == 'device':

            condition2['device_id'] = condition.get('id')

        keys = ['id', 'device_id']
        values = self.get_value(condition2, keys)

        _filter_device = MongoBillFilter()
        _filter_device.match_bill(
            (C('id') == values['id'])
            & (C('device_id') == values['device_id']))\
            .sort({'modify_date': -1})\
            .project({'_id': 0})
        res_device = self.query(_filter_device, 'PT_Device')

        if len(res_device) == 0:
            return {
                'code': 404,
                'msg': '没有找到该设备！',
            }

        # 开始获取设备轨迹
        if res_device[0]['device_name'] == '咪狗':
            device_result = self.mg_hardware_service.get_track(
                res_device[0]['device_id'], now_ymd)

            if device_result['code'] == 0:
                return {
                    'code': device_result['code'],
                    'msg': device_result['data'],
                }
            else:
                return {
                    'code': device_result['code'],
                    'msg': device_result['data'],
                }
        elif res_device[0]['device_name'] == '云芯':
            device_result = self.yx_hardware_service.get_footprint(
                res_device[0]['device_id'], now_ymd)
            if device_result.get('Ret') == 'Succ':
                data = device_result.get('data')
                if data.get('Pos') and len(data['Pos']) and data['Pos'][0].get('Detail') and len(data['Pos'][0]['Detail']):
                    points = []
                    for item in data['Pos'][0]['Detail']:
                        points.append({
                            # 纬度
                            'lat': item['Lat'],
                            # 经度
                            'lng': item['Lon'],
                        })
                    return {
                        'code': 0,
                        'msg': points,
                    }
            else:
                return {
                    'code': 500,
                    'msg': device_result['Msg'],
                }
        elif res_device[0]['device_name'] == '柏颐':
            device_result = self.by_hardware_service.get_footprint(
                res_device[0]['device_id'])
            if device_result['code'] == 0:
                return {
                    'code': 0,
                    'msg': device_result['points'],
                }
        return {
            'code': 403,
            'msg': '获取轨迹数据失败！',
        }

    def get_device_location(self, condition):

        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return {
                'code': 403,
                'msg': '用户未登录！',
            }

        condition2 = {}

        # 根据绑定关系表的主键id获取设备imei
        if condition.get('from') == 'relation':
            # 根据id从绑定表获取设备id
            _filter_bind = MongoBillFilter()
            _filter_bind.match_bill(
                (C('id') == condition.get('id')))\
                .project({'_id': 0})
            res_bind = self.query(_filter_bind, 'PT_Device_Bind')

            if len(res_bind) == 0:
                return {
                    'code': 404,
                    'msg': '未找到该绑定数据！',
                }

            if res_bind[0]['user_id'] != user_id:
                return {
                    'code': 404,
                    'msg': '该设备未绑定此用户！',
                }

            condition2['id'] = res_bind[0]['device_id']

        elif condition.get('from') == 'device':

            condition2['device_id'] = condition.get('id')

        keys = ['id', 'device_id']
        values = self.get_value(condition2, keys)

        _filter_device = MongoBillFilter()
        _filter_device.match_bill(
            (C('id') == values['id'])
            & (C('device_id') == values['device_id']))\
            .sort({'modify_date': -1})\
            .project({'_id': 0})
        res_device = self.query(_filter_device, 'PT_Device')

        if len(res_device) == 0:
            return {
                'code': 404,
                'msg': '没有找到该设备！',
            }

        # 找出长者姓名
        user_name = ''

        if 'elder_id' in res_device[0]:
            _filter_user = MongoBillFilter()
            _filter_user.match_bill(
                (C('id') == res_device[0]['elder_id']))\
                .sort({'modify_date': -1})\
                .project({'_id': 0, 'name': 1, 'id': 1})
            res_user = self.query(_filter_user, 'PT_User')

            if len(res_user) > 0:
                user_name = res_user[0]['name']

        # 开始获取设备定位
        if res_device[0]['device_name'] == '咪狗':
            device_result = self.mg_hardware_service.get_position(
                res_device[0]['device_id'])

            if device_result['code'] == 0:
                device_result['data']['address'] = device_result['data']['addr']
                del(device_result['data']['addr'])

                msg = device_result['data']
                msg['user_name'] = user_name
                return {
                    'code': device_result['code'],
                    'msg': msg
                }
        elif res_device[0]['device_name'] == '云芯':
            device_result = self.yx_hardware_service.get_position(
                res_device[0]['device_id'])
            if device_result.get('Ret') == 'Succ':
                data = device_result.get('data')
                if len(data) > 0:
                    return {
                        'code': 0,
                        'msg': {
                            'lng': data[0].get('Lon'),
                            'lat': data[0].get('Lat'),
                            'address': data[0].get('Str'),
                            'user_name': user_name,
                        },
                    }
            else:
                return {
                    'code': 500,
                    'msg': device_result['Msg'],
                }
        elif res_device[0]['device_name'] == '柏颐':
            device_result = self.by_hardware_service.get_position(
                res_device[0]['device_id'])
            if device_result['code'] == 0:
                return {
                    'code': 0,
                    'msg': {
                        'lng': device_result['lng'],
                        'lat': device_result['lat'],
                        'address': device_result['address'],
                        'user_name': user_name,
                    },
                }
        return {
            'code': 403,
            'msg': '获取定位数据失败！',
        }

    def get_device_message_list(self, org_list, condition, page, count):
        '''
            获取警报列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][0:10] + \
                "T23:59:59.00Z"
        keys = ['user_id', 'type', 'imei', 'start_date', 'end_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('type') == values['type'])
            & (C('imei') == values['imei'])
            & (C('create_date') >= as_date(values['start_date']))
            & (C('create_date') <= as_date(values['end_date']))
        )\
            .lookup_bill('PT_Device', 'imei', 'device_id', 'device_info')\
            .add_fields({
                "device_name": self.ao.array_elemat("$device_info.device_name", 0),
                "elder_id": self.ao.array_elemat("$device_info.elder_id", 0),
            })\
            .match_bill(C('elder_id') == (values['user_id']))\
            .sort({'create_date': -1})\
            .project({
                '_id': 0,
                **get_common_project({'device_info'})
            })
        res = self.page_query(_filter, 'PT_Device_Message', page, count)
        return res

    def get_device_term_list(self, org_list, condition, page, count):
        '''
            获取设备到期列表
            Arguments:
                condition   {dict}      条件
                page        {number}    页码
                count       {number}    条数
        '''
        keys = ['term_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('due_reminder').exists(True))
            & (C('due_reminder').like(values['term_type']))
        )

        if 'term_type' in condition:
            if condition['term_type'] == 'three_days':
                adays = 3
            elif condition['term_type'] == 'one_week':
                adays = 7
            elif condition['term_type'] == 'one_month':
                adays = 30

            now = datetime.datetime.now()
            begin_date = now - datetime.timedelta(
                hours=now.hour, minutes=now.minute, seconds=now.second, microseconds=now.microsecond)
            end_date = begin_date + \
                datetime.timedelta(days=adays, hours=23,
                                   minutes=59, seconds=59)
            _filter.match(
                (C('service_date_end') >= begin_date)
                & (C('service_date_end') <= end_date)
            )

        _filter.add_fields({
            'due_size': self.ao.size("$due_reminder")
        })\
            .match(C('due_size') > 0)\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'device_id': 1,
                'service_date_end': 1,
                'device_name': 1,
                'due_reminder': 1,
                'due_size': 1,
            })
        res = self.page_query(_filter, 'PT_Device', page, count)
        return res
