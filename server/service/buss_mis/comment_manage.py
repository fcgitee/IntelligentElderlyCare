# -*- coding: utf-8 -*-

'''
评论管理函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import find_data, update_data, delete_data, get_condition, get_info, get_current_user_id, get_user_id_or_false, get_cur_time
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter


class CommentManageService(MongoService):
    ''' 评论管理服务 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_comment_list(self, org_id, condition, page, count):
        '''获取评论管理列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ['id', 'comment_object_id', 'type_id', 'level',
                'comment_user_id', 'start_date', 'end_date', 'audit_status', 'article_title', 'article_author','org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                        & (C('comment_date') >= as_date(values['start_date']))
                        & (C('comment_date') <= as_date(values['end_date']))
                        & (C('type_id') == values['type_id'])
                        & (C('audit_status') == values['audit_status'])
                        & (C('comment_user_id') == values['comment_user_id'])
                        & (C('comment_object_id') == values['comment_object_id'])
                        & (C('level').like(values['level'])))\
            .lookup_bill('PT_Article', 'comment_object_id', 'id', 'article_info')\
            .add_fields({
                'article_org_id': self.ao.array_elemat('$article_info.organization_id', 0),
            })\
            .match_bill((C('article_org_id').inner(org_id)))\
            .lookup_bill('PT_User', 'article_org_id', 'id', 'article_org')\
            .add_fields({
                'article_org_name': self.ao.array_elemat('$article_org.name', 0),
            })\
            .lookup_bill('PT_User', 'comment_user_id', 'id', 'comment_user')\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .project({'org._id': 0})\
            .add_fields({
                # 'type': '$type.name',
                'org_name': '$org.name',
                'comment_user_name': self.ao.array_elemat('$comment_user.name', 0),
                'article_title': self.ao.array_elemat('$article_info.title', 0),
                'article_author': self.ao.array_elemat('$article_info.author', 0),
            })\
            .match_bill((C('article_title').like(values['article_title']))
                    & (C('article_author').like(values['article_author']))
                    & (C('org_name').like(values['org_name'])))\
            .project({
                '_id': 0,
                'article_title': 1,
                'article_author': 1,
                'organization_id': 1,
                'content': 1,
                'reason': 1,
                'comment_object_id': 1,
                'type_id': 1,
                'comment_user_id': 1,
                'comment_user_name': 1,
                'audit_status': 1,
                'comment_date': 1,
                'audit_date': 1,
                'article_org_id': 1,
                'article_org_name': 1,
                'id': 1,
                'create_date': 1,
            })\
            .sort({'create_date': -1})
        res = self.page_query(_filter, 'PT_Comment', page, count)
        # for key,result in enumerate(res.get('result')):
        #     comment_object_id = result.get('comment_object_id')
        #     _filter_obj = MongoFilter()
        #     # result['issue_date'] = date_format(result.get('issue_date'))
        #     _filter_obj.match((C('bill_status') == 'valid') &(C('id') == comment_object_id)).project({'_id': 0})
        #     if len(self.page_query(_filter_obj, 'PT_Service_Item', page, count)['result'])>0:
        #         _filter_obj.match(C('name').like(values['comment_object_id']))
        #         res_obj = self.page_query(_filter_obj, 'PT_Service_Item', page, count)
        #         if res_obj['result']:
        #             result['comment_object'] = '服务项目-'+ res_obj['result'][0].get('name')
        #         else:
        #             res.get('result').pop(key)
        #             res['total'] =int(res.get('total')) - 1
        #     elif len(self.page_query(_filter_obj, 'PT_Article', page, count)['result'])>0:
        #         # print('111',self.page_query(_filter_obj, 'PT_Article', page, count)['result'])
        #         _filter_obj.match(C('title').like(values['comment_object_id']))
        #         res_obj = self.page_query(_filter_obj, 'PT_Article', page, count)
        #         if res_obj['result']:
        #             result['comment_object'] = '文章-'+ res_obj['result'][0].get('title')
        #         else:
        #             res.get('result').pop(key)
        #             res['total'] =int(res.get('total')) - 1
        #     elif len(self.page_query(_filter_obj, 'PT_Activity', page, count)['result'])>0:
        #         _filter_obj.match(C('activity_name').like(values['comment_object_id']))
        #         res_obj = self.page_query(_filter_obj, 'PT_Activity', page, count)
        #         if res_obj['result']:
        #             result['comment_object'] = '活动-'+ res_obj['result'][0].get('activity_name')
        #         else:
        #             res.get('result').pop(key)
        #             res['total'] =int(res.get('total')) - 1
        #     elif len(self.page_query(_filter_obj, 'PT_Comment', page, count)['result'])>0:
        #         res_obj = self.page_query(_filter_obj, 'PT_Comment', page, count)
        #         result['father_comment'] = '子评论-'+ res_obj['result'][0].get('content')
        return res

    def update_comment(self, comment):
        '''# 评论'''
        res = 'Fail'
        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return '请先登录'

        if 'id' not in comment.keys():

            if 'comment_object_id' not in comment.keys():
                return '没有评论的数据'

            # 找主数据
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == comment['comment_object_id']))\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Article')

            if len(res) == 0:
                return '没有评论的数据'

            comment['comment_user_id'] = user_id
            comment['audit_status'] = '待审批'
            comment['comment_date'] = get_cur_time()
            # 新增的，要把资讯的组织id作为数据的组织id
            comment['organization_id'] = res[0]['organization_id']

            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.comment.value, comment, 'PT_Comment')
            if bill_id:
                res = 'Success'
        else:
            if 'audit_status' in comment and comment['audit_status'] in ['通过', '不通过']:
                if comment['audit_status'] == '不通过' and ('reason' not in comment or comment['reason'] == ''):
                    return '不通过请填写原因'
                comment['audit_date'] = get_cur_time()
                comment['audit_user_id'] = user_id

            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.comment.value, comment, 'PT_Comment')
            if bill_id:
                res = 'Success'

        return res

    def delete_comment(self, comment_ids):
        '''删除评论接口

        Arguments:
            comment_ids   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(comment_ids, str):
                ids.append(comment_ids)
            else:
                ids = comment_ids
            for comment_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Comment', {
                    'id': comment_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.comment.value, data[0], 'PT_Comment')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name, process_func,self.db_user, self.db_pwd)
        return res

    def get_comment_audit_list(self, order_ids, condition, page, count):
        '''获取评论审核列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['id', 'comment_object_id', 'type_id', 'level',
                'comment_user_id', 'start_date', 'end_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()

        _filter.lookup_bill('PT_Comment_Type', 'type_id', 'id', 'type')\
            .add_fields({'type': '$type.name'})\
            .match_bill((C('audit_status') == 'audiing')
                        & C('organization_id').inner(order_ids)
                        & (C('comment_date') >= as_date(values['start_date']))
                        & (C('comment_date') <= as_date(values['end_date']))
                        & (C('id') == values['id'])
                        & (C('comment_object_id') == values['comment_object_id'])
                        & (C('type_id') == values['type_id'])
                        & (C('comment_user_id') == values['comment_user_id'])
                        & C('level').like(values['level']))\
            .project({'_id': 0, 'type._id': 0})
        res = self.page_query(_filter, 'PT_Comment', page, count)
        for key, result in enumerate(res.get('result')):
            comment_object_id = result.get('comment_object_id')
            _filter_obj = MongoBillFilter()
            _filter_obj.match((C('bill_status') == 'valid') & (
                C('id') == comment_object_id)).project({'_id': 0})
            if len(self.page_query(_filter_obj, 'PT_Service_Item', page, count)['result']) > 0:
                _filter_obj.match(C('name').like(values['comment_object_id']))
                res_obj = self.page_query(
                    _filter_obj, 'PT_Service_Item', page, count)
                if res_obj['result']:
                    result['comment_object'] = '服务项目-' + \
                        res_obj['result'][0].get('name')
                else:
                    res.get('result').pop(key)
                    res['total'] = int(res.get('total')) - 1
            elif len(self.page_query(_filter_obj, 'PT_Article', page, count)['result']) > 0:
                _filter_obj.match(C('title').like(values['comment_object_id']))
                res_obj = self.page_query(
                    _filter_obj, 'PT_Article', page, count)
                if res_obj['result']:
                    result['comment_object'] = '文章-' + \
                        res_obj['result'][0].get('title')
                else:
                    res.get('result').pop(key)
                    res['total'] = int(res.get('total')) - 1
            elif len(self.page_query(_filter_obj, 'PT_Activity', page, count)['result']) > 0:
                _filter_obj.match(C('activity_name').like(
                    values['comment_object_id']))
                res_obj = self.page_query(
                    _filter_obj, 'PT_Activity', page, count)
                if res_obj['result']:
                    result['comment_object'] = '活动-' + \
                        res_obj['result'][0].get('activity_name')
                else:
                    res.get('result').pop(key)
                    res['total'] = int(res.get('total')) - 1
            elif len(self.page_query(_filter_obj, 'PT_Comment', page, count)['result']) > 0:
                res_obj = self.page_query(
                    _filter_obj, 'PT_Comment', page, count)
                result['father_comment'] = '子评论-' + \
                    res_obj['result'][0].get('content')
        return res

    def comment_audit_success(self, comment):
        '''# 评论审核通过'''
        res = 'Fail'
        person_id = get_current_user_id(self.session)
        if comment['content']:
            comment.pop('content')
        else:
            pass
        comment['audit_user_id'] = person_id
        comment['audit_status'] = 'audit_success'
        comment['audit_date'] = datetime.datetime.now()
        bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                    TypeId.comment.value, comment, 'PT_Comment')
        if bill_id:
            res = 'Success'
        return res

    def comment_audit_fail(self, comment):
        '''# 评论审核不通过'''
        res = 'Fail'
        person_id = get_current_user_id(self.session)
        if comment['content']:
            comment.pop('content')
        else:
            pass
        comment['audit_user_id'] = person_id
        comment['audit_status'] = 'audit_fail'
        comment['audit_date'] = datetime.datetime.now()
        bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                    TypeId.comment.value, comment, 'PT_Comment')
        if bill_id:
            res = 'Success'
        return res
