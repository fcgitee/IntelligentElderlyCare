'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 17:14:46
@LastEditTime: 2019-10-10 15:53:24
@LastEditors: Please set LastEditors
'''

from server.pao_python.pao.service.data.mongo_db import MongoService, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import get_current_role_id, insert_data, insert_many_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id, get_user_id_or_false, get_current_organization_id, get_serial_number, SerialNumberType, get_common_project
from ...pao_python.pao.service.security.security_service import RoleService
from ...service.buss_mis.service_item_package import (
    ServiceItemPackageService)
import hashlib
import re
import math
import datetime
import time
import uuid
import pandas as pd
import requests
import base64
from enum import Enum
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, get_cur_time
from ...service.mongo_bill_service import MongoBillFilter
import dateutil
import calendar
import pandas as pd
from ...service.buss_mis.operation_record import OperationRecordObject, OperationRecordType, OperationRecordService
from ...service.buss_pub.person_org_manage import PersonOrgManageService
from ...service.buss_pub.message_manage import MessageManageService, MessageState
import gc
# -*- coding: utf-8 -*-

'''
活动管理函数
'''


class Auditstatus(Enum):
    # 通过
    adopt = '通过'
    # 不通过
    reject = '不通过'
    # 待审批
    unaudited = '待审批'
    # 未拨款
    unpay = '未拨款'
    # 待审批拨款
    waitpayed = '待拨款'
    # 已拨款
    payed = '已拨款'

# 捐款使用状态


class Participate():
    '''活动参与对象'''

    def __init__(self, user_id, activity_id, date, status):
        '''构造函数'''
        # 用户ID
        self.user_id = user_id
        # 活动ID
        self.activity_id = activity_id
        # 参与时间
        self.date = date
        # 参与状态
        self.status = status

    def to_dict(self):
        return self.__dict__


class SignIn():
    '''活动签到对象'''

    def __init__(self, user_id, activity_id, date):
        '''构造函数'''
        # 用户ID
        self.user_id = user_id
        # 活动ID
        self.activity_id = activity_id
        # 签到时间
        self.date = date

    def to_dict(self):
        return self.__dict__


class ParticipateStatus(Enum):
    # 参与
    participate = 'participate'
    # 取消
    cancel = 'cancel'


class ActivityStatus(Enum):
    # 报名中
    enroll = 'enroll'
    # 进行中
    progress = 'progress'
    # 已截止
    finish = 'finish'
    # 已报名
    participate = 'participate'
    # 已完成
    complete = 'complete'
    # 已满员
    fully = 'fully'


class ActivityManageService(MongoService):
    ''' 活动管理 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.OperationRecordService = OperationRecordService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.PersonOrgManageService = PersonOrgManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.MessageManageService = MessageManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.ServiceItemPackageService = ServiceItemPackageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def setTest(self):
        _filter = MongoBillFilter()
        _filter.sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.page_query(
            _filter, "PT_Activity", 1, 9999)
        for item in res.get('result'):
            data = {
                "define_id": "94dc3b2d-0b41-11ea-b83e-a0a4c57e9ebe",
                "business_id": item['id'],
                "approval_user_id": "8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe",
                "step_no": 1,
                "opinion": [],
                "status": "通过",
                "organization_id": "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                "id": str(uuid.uuid1()),
            }
            self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.activity.value, data, 'PT_Approval_Process')
            data1 = {
                "define_id": "94dc3b2d-0b41-11ea-b83e-a0a4c57e9ebe",
                "business_id": item['id'],
                "approval_user_id": "8eaf3fba-e355-11e9-b97e-a0a4c57e9ebe",
                "step_no": -1,
                "opinion": [],
                "status": "通过",
                "organization_id": "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                "id": str(uuid.uuid1()),
            }
            self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.activity.value, data1, 'PT_Approval_Process')

    def convertDegreesToRadians(self, degrees):
        return degrees * math.pi / 180

    def coverGpsToBaiduLatLon(self, lat, lon):
        baidu_map_url = 'http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=%f&y=%f'

        data_req = requests.get((baidu_map_url % (lon, lat)))
        new_pos = data_req.json()
        if 'error' in new_pos.keys() and new_pos['error'] == 0:
            lon = float(base64.b64decode(
                new_pos['x']).decode("utf-8"))
            lat = float(base64.b64decode(
                new_pos['y']).decode("utf-8"))
            return lat, lon

    def get_activity_pure_list(self, org_list, condition, page=None, count=None):
        '''单纯查询活动列表'''
        keys = ['id', 'activity_name',
                'activity_type_id', 'status', 'organization_name']

        values = self.get_value(condition, keys)

        org_inner = N()
        temp_activity_org = {}

        # 过滤发布方
        if 'organization_name' in condition:
            org_inner = []
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('personnel_type') == '2') & (
                C('name').like(values['organization_name'])))
            _filter_org.project({'_id': 0})

            res_org = self.query(
                _filter_org, "PT_User")

            if len(res_org) > 0:
                for item in res_org:
                    org_inner.append(item['id'])
                    temp_activity_org[item['id']] = item['name']

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) &
                           (C('activity_type_id') == values['activity_type_id']) &
                           (C('status') == values['status']) &
                           (C('activity_name').like(values['activity_name'])) &
                           (C("organization_id").inner(org_inner)) &
                           (C('organization_id').inner(org_list)))

        if 'sort' in condition and condition['sort'] != None:
            if condition['sort'] == '发布时间':
                _filter.sort({'create_date': -1})
            else:
                _filter.sort({'end_date': -1})
        else:
            _filter.sort({'step_no': -1, 'modify_date': -1})

        _filter.project({'_id': 0, **get_common_project()})

        res = self.page_query(
            _filter, "PT_Activity", page, count)

        if len(res['result']) > 0:

            now_date = get_cur_time()

            activity_rarticipate_ids = []
            activity_room_ids = []
            activity_group_ids = []
            activity_type_ids = []
            activity_org_ids = []

            for item in res['result']:

                # 补全活动参加人数(1)
                activity_rarticipate_ids.append(item['id'])

                # 补全活动室名字(1)
                if 'activity_room_id' in item:
                    activity_room_ids.append(item['activity_room_id'])

                # 补全活动类型名字(1)
                if 'activity_type_id' in item:
                    activity_type_ids.append(item['activity_type_id'])

                # 补全活动团体名字(1)
                if 'group_id' in item:
                    activity_group_ids.append(item['group_id'])

                # 补全活动类型名字(1)
                if 'activity_type_id' in item:
                    activity_type_ids.append(item['activity_type_id'])

                # 补全活动发布方名字(1)
                if 'organization_name' not in condition:
                    activity_org_ids.append(item['organization_id'])

            temp_activity_rarticipate = {}
            temp_activity_room = {}
            temp_activity_type = {}
            temp_activity_group = {}

            # 补全活动参加人数(2)
            if len(activity_rarticipate_ids) > 0:
                _filter_rarticipate = MongoBillFilter()
                _filter_rarticipate.match_bill((C('id') == values['id']) &
                                               (C('activity_id').inner(activity_rarticipate_ids)))\

                _filter_rarticipate.project({'_id': 0})

                res_rarticipate = self.query(
                    _filter_rarticipate, "PT_Activity_Participate")

                if len(res_rarticipate) > 0:
                    for item in res_rarticipate:
                        if item['activity_id'] not in temp_activity_rarticipate:
                            temp_activity_rarticipate[item['activity_id']] = 0
                        temp_activity_rarticipate[item['activity_id']] += 1

            # 补全活动室(2)
            if len(activity_room_ids) > 0:
                _filter_room = MongoBillFilter()
                _filter_room.match_bill((C('id').inner(activity_room_ids)))\

                _filter_room.project({'_id': 0})

                res_room = self.query(
                    _filter_room, "PT_Activity_Room")

                if len(res_room) > 0:
                    for item in res_room:
                        temp_activity_room[item['id']
                                           ] = item['activity_room_name']

            # 补全活动类型(2)
            if len(activity_type_ids) > 0:
                _filter_type = MongoBillFilter()
                _filter_type.match_bill((C('id').inner(activity_type_ids)))\

                _filter_type.project({'_id': 0})

                res_type = self.query(
                    _filter_type, "PT_Activity_Type")

                if len(res_type) > 0:
                    for item in res_type:
                        temp_activity_type[item['id']
                                           ] = item['name']

            # 补全活动团体(2)
            if len(activity_group_ids) > 0:
                _filter_group = MongoBillFilter()
                _filter_group.match_bill((C('id').inner(activity_group_ids)))\

                _filter_group.project({'_id': 0})

                res_group = self.query(
                    _filter_group, "PT_Groups")

                if len(res_group) > 0:
                    for item in res_group:
                        temp_activity_group[item['id']
                                            ] = item['name']

            # 补全机构名字（2）
            if len(activity_org_ids) > 0:
                _filter_org = MongoBillFilter()
                _filter_org.match_bill((C('id').inner(activity_org_ids)))
                _filter_org.project({'_id': 0})

                res_org = self.query(
                    _filter_org, "PT_User")

                if len(res_org) > 0:
                    for item in res_org:
                        temp_activity_org[item['id']] = item['name']

            for item in res['result']:
                # 补全活动参加人数(3)
                if item['id'] in temp_activity_rarticipate:
                    item['participate_count'] = temp_activity_rarticipate[item['id']]
                else:
                    item['participate_count'] = 0

                # 补全活动室(3)
                if 'activity_room_id' in item and item['activity_room_id'] in temp_activity_room:
                    item['activity_room_name'] = temp_activity_room[item['activity_room_id']]
                else:
                    item['activity_room_name'] = ''

                # 补全活动类型(3)
                if 'activity_type_id' in item and item['activity_type_id'] in temp_activity_type:
                    item['activity_type_name'] = temp_activity_type[item['activity_type_id']]
                else:
                    item['activity_type_name'] = ''

                # 补全活动团体(3)
                if 'group_id' in item and item['group_id'] in temp_activity_group:
                    item['activity_group_name'] = temp_activity_group[item['group_id']]
                else:
                    item['activity_group_name'] = ''

                # 补全机构名字(3)
                if 'organization_id' in item and item['organization_id'] in temp_activity_org:
                    item['organization_name'] = temp_activity_org[item['organization_id']]
                else:
                    item['organization_name'] = ''

                # 补全活动状态
                if 'begin_date' in item and item['begin_date'] > now_date:
                    # 开始时间大于当前时间
                    item['act_status'] = '报名中'
                    # 报名中再判断报名是否已满吧，什么需求？
                    if item['participate_count'] >= item['max_quantity']:
                        item['act_status'] = '已报满'
                elif 'begin_date' in item and item['begin_date'] < now_date and 'end_date' in item and item['end_date'] > now_date:
                    # 开始时间小于当前时间并且结束时间大于当前时间
                    item['act_status'] = '报名中'
                elif 'end_date' in item and item['end_date'] < now_date:
                    # 结束时间小于当前时间
                    item['act_status'] = '已结束'

        return res

    def get_activity_list(self, org_list, condition, page=None, count=None):
        '''查询活动列表'''
        keys = ['id', 'name', 'activity_name',
                'activity_type_id', 'begin_date', 'status', 'end_date', 'organization_id', 'lat', 'lon', 'user_id', 'organization_name']

        # 判断是否登录
        user_id = get_user_id_or_false(self.session)
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) &
                           (C('activity_type_id') == values['activity_type_id']) &
                           (C('organization_id') == values['organization_id']) &
                           (C('status') == values['status']) &
                           (C('activity_name').like(values['activity_name'])) &
                           (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organ')\
            .lookup_bill('PT_Activity_Participate', 'id', 'activity_id', 'participate')\
            .lookup_bill('PT_Activity_Room', 'activity_room_id', 'id', 'room_info')\
            .add_fields({'participate_count': self.ao.size('$participate'), })\
            .lookup_bill('PT_Activity_Sign_In', 'id', 'activity_id', 'signin')\
            .match_bill((C('signin.user_id') == values['user_id']))\
            .add_fields({'signin_count': self.ao.size('$signin'), })\
            .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'collection_info')\
            .add_fields({'collection_count': self.ao.size('$collection_info'), })\
            .lookup_bill('PT_Activity_Type', 'activity_type_id', 'id', 'activity_type')\
            .add_fields({
                'activity_room_name': self.ao.array_elemat('$room_info.activity_room_name', 0),
                'organization_star': self.ao.array_elemat('$organ.organization_info.star_level', 0),
                'organization_lat': self.ao.array_elemat('$organ.organization_info.lat', 0),
                'organization_lon': self.ao.array_elemat('$organ.organization_info.lon', 0),
                'organization_name': self.ao.array_elemat('$organ.name', 0),
                'activity_type_name': self.ao.array_elemat('$activity_type.name', 0),
                'organization_address': self.ao.array_elemat('$organ.organization_info.address', 0)})\
            .match_bill((C('organization_name').like(values['organization_name'])))
        if user_id != False:
            _filter.add_fields({'is_participate': self.ao.size(self.ao.array_filter(
                "$participate", "aa", ((F('$aa.user_id') == user_id)).f))})
            # 表示是我发布的
            if 'is_mine' in condition:
                _filter.match_bill((C('apply_user_id') == user_id))

        now_date = get_cur_time()
        # 判断活动状态的
        if 'act_status' in condition:
            # 报名中
            if condition['act_status'] == ActivityStatus.enroll.value:
                _filter.match_bill((C('begin_date') > now_date))
                _filter.add_fields({'c': self.ao.cond(
                    (F('participate_count') < F('max_quantity')).f, 1, 0)}).match((C('c') == 1))
            # 进行中
            elif condition['act_status'] == ActivityStatus.progress.value:
                _filter.match_bill((now_date >= C('begin_date'))
                                   & (C('end_date') >= now_date))
            # 已结束
            elif condition['act_status'] == ActivityStatus.finish.value:
                _filter.match_bill(now_date > C('end_date'))
            # 已满员
            elif condition['act_status'] == ActivityStatus.fully.value:
                _filter.add_fields({'c': self.ao.cond(
                    (F('participate_count') >= F('max_quantity')).f, 1, 0)}).match((C('c') == 1))

        # 活动时间内，开始时间大于开始范围或者结束时间小于结束范围都算吧
        if 'begin_date' in condition and 'end_date' in condition:
            _filter.match_bill((C('begin_date') >= condition['begin_date']) & (
                C('end_date') <= condition['end_date']))

        if 'sort' in condition and condition['sort'] != None:
            if condition['sort'] == '距离' and 'lat' in condition and 'lon' in condition:
                lat = values['lat']
                lon = values['lon']
                # 纬度，经度
                lat_baidu, lon_baidu = self.coverGpsToBaiduLatLon(lat, lon)
                lat_d = self.convertDegreesToRadians(lat_baidu)
                _filter.filter_objects.append({
                    '$addFields': {
                        # 距离是以km单位的
                        "distance": {
                            '$multiply': [
                                {
                                    '$acos': {
                                        '$add': [
                                            {
                                                '$multiply': [
                                                    {
                                                        '$sin': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                    },
                                                    {
                                                        '$sin': lat_d
                                                    }
                                                ]
                                            },
                                            {
                                                '$multiply': [
                                                    {
                                                        '$cos': {'$degreesToRadians': {"$toDouble": "$organization_info.lat"}}
                                                    },
                                                    {
                                                        '$cos': lat_d
                                                    },
                                                    {
                                                        '$cos': {
                                                            '$degreesToRadians': {
                                                                '$subtract': [
                                                                    {"$toDouble": "$organization_info.lon"},
                                                                    lon_baidu
                                                                ]
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                },
                                # 地球平均半径
                                6371.004
                            ]
                        }
                    }
                })
                _filter.sort({'distance': 1})
            elif condition['sort'] == '活动数':
                _filter.sort({'activity_count': -1})
            elif condition['sort'] == '报名数':
                _filter.sort({'participate_count': -1})
            elif condition['sort'] == '收藏数':
                _filter.sort({'collection_count': -1})
            elif condition['sort'] == '发布时间':
                _filter.sort({'create_date': -1})
            else:
                _filter.sort({'end_date': -1})
        else:
            _filter.sort({'modify_date': -1})
        _filter.project({
            '_id': 0,
            'activity_type': 0,
            'new_field': 0,
            'process': 0,
            'organ': 0,
            'room_info': 0,
            'collection_info': 0,
            'participate': 0,
            'signin': 0,
            **get_common_project()
        })

        res = self.page_query(
            _filter, "PT_Activity", page, count)

        # 判断是否登录
        user_id = get_user_id_or_false(self.session)

        # 活动列表添加活动状态
        if len(res['result']) > 0:
            for item in res['result']:
                if 'begin_date' in item and item['begin_date'] > now_date:
                    # 开始时间大于当前时间
                    item['act_status'] = '报名中'
                    # 报名中再判断报名是否已满吧，什么需求？
                    if item['participate_count'] >= item['max_quantity']:
                        item['act_status'] = '已报满'
                elif 'begin_date' in item and item['begin_date'] < now_date and 'end_date' in item and item['end_date'] > now_date:
                    # 开始时间小于当前时间并且结束时间大于当前时间
                    item['act_status'] = '报名中'
                elif 'end_date' in item and item['end_date'] < now_date:
                    # 结束时间小于当前时间
                    item['act_status'] = '已结束'

        # 活动详情添加个人状态
        if len(res['result']) > 0 and user_id != False and 'id' in condition:

            # 判断该活动是否已经报名过，添加个人状态
            _filter_ps = MongoBillFilter()
            _filter_ps.match_bill((C('user_id') == user_id) & (C('activity_id') == res['result'][0]['id']))\
                .project({'_id': 0, })
            res_ps = self.query(_filter_ps, 'PT_Activity_Participate')

            if len(res_ps) > 0:
                # 已经报名
                if res['result'][0]['begin_date'] > now_date:
                    res['result'][0]['per_status'] = '已报名未开始'
                elif res['result'][0]['begin_date'] < now_date and res['result'][0]['end_date'] > now_date:
                    res['result'][0]['per_status'] = '已报名进行中'
                elif res['result'][0]['end_date'] < now_date:
                    res['result'][0]['per_status'] = '已报名已结束'
                else:
                    res['result'][0]['per_status'] = '未定义'
            else:
                if res['result'][0]['begin_date'] > now_date:
                    res['result'][0]['per_status'] = '未报名未开始'
                elif res['result'][0]['begin_date'] < now_date and res['result'][0]['end_date'] > now_date:
                    res['result'][0]['per_status'] = '未报名已开始'
                elif res['result'][0]['end_date'] < now_date:
                    res['result'][0]['per_status'] = '未报名已结束'
                else:
                    res['result'][0]['per_status'] = '未定义'

            _filter_collection = MongoBillFilter()
            _filter_collection.match_bill((C('user_id') == user_id) & (C('object') == '活动') & (C('type') == 'collection') & (C('business_id') == res['result'][0]['id']))\
                .project({'_id': 0})
            res_collection = self.query(
                _filter_collection, "PT_Service_Follow_Collection")

            if len(res_collection) > 0 and len(res['result']) > 0:
                res['result'][0]['collection_data'] = res_collection[0]
        return res

    def get_activity_sign_in_list(self, org_list, condition, page=None, count=None):
        '''查询活动签到列表'''
        keys = ['id', 'activity_name', 'activity_id',
                'begin_date', 'end_date', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) &
                           (C('user_id') != None) &
                           (C('activity_id') == values['activity_id']) &
                           (C('activity_name').like(values['activity_name'])) &
                           (C('organization_id').inner(org_list)))\
            .lookup_bill("PT_User", "organization_id", "id", "org")\
            .add_fields({"org_name": "$org.name"})\
            .project({'org': 0})\
            .match_bill((C('org_name').like(values['org_name'])))

        if 'begin_date' in condition and 'end_date' in condition:
            _filter.match_bill(C('create_date') >= condition['begin_date'])
            _filter.match_bill(C('create_date') <= condition['end_date'])

        # 获取我发布的活动的签到长者
        if 'type' in condition and condition['type'] == 'my_activity':
            user_id = get_current_user_id(self.session)
            _filter.match_bill(C('activity_apply_user_id') == user_id)

        _filter.lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_Activity', 'activity_id', 'id', 'activity')\
            .add_fields({'activity_filter': self.ao.array_filter("$activity", "af", ((F('$af.bill_status') == 'valid')).f)}).match((C("activity_filter") != None) & (C("activity_filter") != []))\
            .add_fields({'user_filter': self.ao.array_filter("$user", "uf", ((F('$uf.bill_status') == 'valid')).f)}).match((C("user_filter") != None) & (C("user_filter") != []))\

        _filter.add_fields({
            'user_name': '$user.name',
            'activity_name': '$activity.activity_name',
            'create_date1': self.ao.date_to_string('$create_date'),
            'modify_date1': self.ao.date_to_string('$modify_date')})\
            .project({
                'user.login_info': 0,
                'activity_filter': 0,
                'user_filter': 0,
                **get_common_project({'user', 'is_mine', 'activity'})
            })

        res = self.page_query(
            _filter, "PT_Activity_Sign_In", page, count)
        return res

    def get_activity_participate_pure_list(self, org_list, condition, page=None, count=None):
        '''重构的查询活动参与列表'''
        keys = ['id', 'activity_name', 'is_pc', 'org_name']
        values = self.get_value(condition, keys)

        activity_ids = []
        activity_obj = {}
        org_ids = []
        org_obj = {}
        elder_ids = []
        elder_obj = {}

        # 筛选活动名字
        if 'activity_name' in condition:
            _filter_activity = MongoBillFilter()
            _filter_activity.match_bill((C('activity_name').like(values['activity_name'])))\
                .project({'_id': 0, 'id': 1, 'activity_name': 1, 'organization_id': 1})
            res_activity = self.query(
                _filter_activity, "PT_Activity")

            for item in res_activity:
                if item['id'] not in activity_ids:
                    activity_ids.append(item['id'])
                    activity_obj[item['id']] = item
        else:
            activity_ids = N()

        # 筛选机构名字
        if 'org_name' in condition:
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('personnel_type') == '2') & (C('name').like(values['org_name'])))\
                .project({'_id': 0, 'id': 1, 'name': 1})
            res_org = self.query(
                _filter_org, "PT_User")

            for item in res_org:
                if item['id'] not in org_ids:
                    org_ids.append(item['id'])
                    org_obj[item['id']] = item
        else:
            org_ids = N()

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('is_pc') == values['is_pc'])
                           & (C('activity_id').inner(activity_ids))
                           & (C('organization_id').inner(org_ids))
                           & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'activity_id': 1,
                'family_ids': 1,
                'is_pc': 1,
                'organization_id': 1,
                'create_date': 1
            })
        res = self.page_query(
            _filter, "PT_Activity_Participate", page, count)

        if len(res['result']) > 0:

            activity_participate_ids = []
            activity_participate_org_ids = []

            for ap_item in res['result']:
                activity_participate_ids.append(ap_item['activity_id'])
                if 'family_ids' in ap_item and type(ap_item['family_ids']) == list and len(ap_item['family_ids']) > 0:
                    elder_ids.append(ap_item['family_ids'][0])
                if 'organization_id' in ap_item:
                    activity_participate_org_ids.append(
                        ap_item['organization_id'])

            # 没有筛选活动名字
            if 'activity_name' not in condition and len(activity_participate_ids) > 0:
                _filter_activity = MongoBillFilter()
                _filter_activity.match_bill((C('id').inner(activity_participate_ids)))\
                    .project({'_id': 0, 'id': 1, 'activity_name': 1, 'organization_id': 1})
                res_activity = self.query(
                    _filter_activity, "PT_Activity")

                for itm in res_activity:
                    if itm['id'] not in activity_obj:
                        activity_obj[itm['id']] = itm

            # 没有筛选机构名字
            if 'org_name' not in condition and len(activity_participate_org_ids) > 0:
                _filter_org = MongoBillFilter()
                _filter_org.match_bill((C('personnel_type') == '2') & (C('id').inner(activity_participate_org_ids)))\
                    .project({'_id': 0, 'id': 1, 'name': 1})
                res_org = self.query(
                    _filter_org, "PT_User")

                for itm in res_org:
                    if itm['id'] not in org_obj:
                        org_obj[itm['id']] = itm

            # 补充报名人信息
            if len(elder_ids) > 0:
                _filter_elder = MongoBillFilter()
                _filter_elder.match_bill((C('personnel_type') == '1') & (C('id').inner(elder_ids)))\
                    .project({
                        '_id': 0,
                        'id': 1,
                        'name': 1,
                        'sex': "$personnel_info.sex",
                        'address': "$personnel_info.address",
                        'telephone': "$personnel_info.telephone",
                    })
                res_elder = self.query(
                    _filter_elder, "PT_User")

                for itm in res_elder:
                    if itm['id'] not in elder_obj:
                        elder_obj[itm['id']] = itm

            for ap_item in res['result']:

                # 补全活动信息
                if 'activity_id' in ap_item and ap_item['activity_id'] in activity_obj:
                    ap_item['activity_name'] = activity_obj[ap_item['activity_id']
                                                            ].get('activity_name')

                # 补全报名人信息
                if 'family_ids' in ap_item and type(ap_item['family_ids']) == list and len(ap_item['family_ids']) > 0 and ap_item['family_ids'][0] in elder_obj:
                    elder_temp = elder_obj[ap_item['family_ids'][0]]

                    ap_item['user_name'] = elder_temp.get('name')
                    ap_item['user_sex'] = elder_temp.get('sex')
                    ap_item['user_address'] = elder_temp.get('address')
                    ap_item['user_telephone'] = elder_temp.get('telephone')

                # 补全机构信息
                if 'organization_id' in ap_item and ap_item['organization_id'] in org_obj:
                    ap_item['org_name'] = org_obj[ap_item['organization_id']].get(
                        'name')

                if ap_item.get('family_ids'):
                    del(ap_item['family_ids'])
                if ap_item.get('activity_id'):
                    del(ap_item['activity_id'])

        return res

    def get_activity_participate_list(self, org_list, condition, page=None, count=None):
        '''查询活动参与列表'''
        keys = ['id', 'name', 'user_id', 'status',
                'activity_id', 'begin_date', 'end_date', 'activity_name', 'is_pc', 'org_name']
        # if 'user_id' not in condition:
        #     condition['user_id'] = get_current_user_id(self.session)
        now_date = get_cur_time()
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == values['user_id'])
                           & (C('id') == values['id'])
                           & (C('is_pc') == values['is_pc'])
                           & (C('name').like(values['name']))
                           & (C('activity_id').like(values['activity_id']))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Activity', 'activity_id', 'id', 'activity')\
            .lookup_bill('PT_Activity_Participate', 'activity_id', 'activity_id', 'participate')\
            .add_fields({'participate_count': self.ao.size('$participate'), })\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill("PT_User", "organization_id", "id", "org")\
            .project({'org._id': 0})\
            .add_fields({"org_name": "$org.name"})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .add_fields({
                'user_name': self.ao.array_elemat('$user.name', 0),
                'user_telephone': self.ao.array_elemat('$user.personnel_info.telephone', 0),
                'user_sex': self.ao.array_elemat('$user.personnel_info.sex', 0),
                'user_address': self.ao.array_elemat('$user.personnel_info.address', 0),
                'activity_name': self.ao.array_elemat('$activity.activity_name', 0),
                'begin_date': self.ao.array_elemat('$activity.begin_date', 0),
                'end_date': self.ao.array_elemat('$activity.end_date', 0),
                'photo': self.ao.array_elemat('$activity.photo', 0),
                'amount': self.ao.array_elemat('$activity.amount', 0),
                'activity_org_id': self.ao.array_elemat('$activity.organization_id', 0),
                'max_quantity': self.ao.array_elemat('$activity.max_quantity', 0),
                'activity_id':  self.ao.array_elemat('$activity.id', 0),
                        'create_date1': self.ao.date_to_string('$create_date'),
                        'modify_date1': self.ao.date_to_string('$modify_date')})\
            .lookup_bill('PT_User', 'activity_org_id', 'id', 'organization')\
            .add_fields({
                'organization_name': self.ao.array_elemat('$organization.name', 0)})

        if 'begin_date' in condition and 'end_date' in condition:
            _filter.match_bill(C('create_date') >= condition['begin_date'])
            _filter.match_bill(C('create_date') <= condition['end_date'])

        if 'activity_name' in condition:
            _filter.match_bill(
                C('activity_name').like(values['activity_name']))

        if 'status' in condition:
            # 报名中
            if condition['status'] == ActivityStatus.participate.value:
                # _filter.match_bill(C('begin_date') > now_date)
                pass
              # print(1)
                # 进行中
            elif condition['status'] == ActivityStatus.enroll.value:
                _filter.match_bill((C('begin_date') < now_date)
                                   & (C('end_date') > now_date))
            # 已截止
            elif condition['status'] == ActivityStatus.finish.value:
                _filter.match_bill(C('end_date') > now_date)
            else:
                _filter.match_bill(now_date >= C('begin_date'))
        _filter.sort({'create_date': -1})
        _filter.project({'_id': 0,
                         'user': 0,
                         'participate': 0,
                         'activity': 0,
                         'organization': 0})
        res = self.page_query(
            _filter, "PT_Activity_Participate", page, count)

        return res

    def update_activity(self, data):
        '''# 新增/修改活动'''
        res = 'Fail'

        # 草稿
        if 'type' in data and data['type'] == '草稿':
            return self.PersonOrgManageService.create_draft({
                'draft': data,
                'type': 'activity'
            })

        begin_datestr = ''

        # 开始日期和结束日期时间字符串转为时间对象
        if 'begin_date' in data and type(data['begin_date']) == str:
            begin_datestr = data['begin_date'][0:10]
            begin_date = data['begin_date'][0:10] + \
                ' '+data['begin_date'][11:19]
            data['begin_date'] = datetime.datetime.strptime(
                begin_date, '%Y-%m-%d %H:%M:%S')
        if 'end_date' in data and type(data['end_date']) == str:
            end_date = data['end_date'][0:10]+' '+data['end_date'][11:19]
            data['end_date'] = datetime.datetime.strptime(
                end_date, '%Y-%m-%d %H:%M:%S')

        if 'max_quantity' in data:
            data['max_quantity'] = int(data['max_quantity'])

        # 定义审批类型
        define_type = 'activityPublish'
        # 数据表
        ptdb = 'PT_Activity'
        # 类型id
        typeid = TypeId.activity.value

        # 审核的编辑
        if 'action' in data and data['action'] == 'sh':

            result = self.ServiceItemPackageService.change_approval_process(
                data, ptdb, typeid)

            if result == 'Success':

                # 审批流程要判断这个消息提醒是否可以设置已读
                _filter_msg = MongoBillFilter()
                _filter_msg.match_bill((C('business_id') == data['id']) & (C('message_state') == MessageState.Unread.value))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                res_msg = self.query(
                    _filter_msg, "PT_Message")

                if len(res_msg) > 0:
                    # 设置已读
                    self.MessageManageService.set_message_already_read({
                        'id': res_msg[0]['id']
                    })
            return result

        # 带有id，编辑状态
        if 'id' in list(data.keys()):

            # 重新编辑
            bill_id = self.ServiceItemPackageService.rechange(
                data, ptdb, typeid, define_type)

            res = bill_id
        else:

            # 获取当前用户
            user_id = get_current_user_id(self.session)

            # 新增一个待审批
            data['apply_user_id'] = user_id

            # 由于app端去掉了商家，所以这里默认给一个商家
            _filter_set_role = MongoBillFilter()
            _filter_set_role.match_bill((C('principal_account_id') == user_id))\
                .sort({'modify_date': -1}).project({'_id': 0})
            res_set_role = self.query(_filter_set_role, 'PT_Set_Role')

            if len(res_set_role) > 0:
                data['organization_id'] = res_set_role[0]['role_of_account_id']

            activityRoomIsAllow = False

            if 'activity_room_id' in data and data['activity_room_id'] != '' and data['activity_room_id'] != None:

                activityRoomIsAllow = self.checkActivityRoomIsAllow(
                    data['activity_room_id'], data['begin_date'], data['end_date'])
                # 判断这个时间段是否能预约
                if activityRoomIsAllow != True:
                    return activityRoomIsAllow

            # 主数据
            main_data = get_info(data, self.session)
            # 接入审批流程
            bill_id = self.ServiceItemPackageService.create_approval_process(
                main_data, ptdb, typeid, define_type)

            res = bill_id
            if res == 'Success':
                # 活动室预约成功了
                if activityRoomIsAllow != False:
                    # 插入活动室的预约
                    room_data = get_info({
                        'reservate_room_id': main_data['activity_room_id'],
                        'begin_date': main_data['begin_date'],
                        'end_date': main_data['end_date'],
                        'reservate_quantity': main_data['max_quantity'],
                        'contacts': main_data['contacts'],
                        'phone': main_data['phone'],
                        'user_id': main_data['apply_user_id'],
                        'reservate_date': main_data['begin_date'],
                        'organization_id': main_data['organization_id'],
                        'reservate_datestr': begin_datestr,
                    }, self.session)
                    bill_id = self.bill_manage_server.add_bill(
                        OperationType.add.value, TypeId.activityRoom.value, room_data, 'PT_Activity_Room_Reservate')

                # 插入一个消息提醒
                self.MessageManageService.add_new_message({
                    # 新闻标识
                    "business_type": 'activity',
                    # 业务ID
                    "business_id": main_data['id'],
                    # 组织机构ID
                    'organization_id': main_data['organization_id'],
                })

            # 审核是不会带活动数据的
            if res == 'Success' and 'activity_name' in data:
                real_account_data = OperationRecordObject(
                    get_current_user_id(self.session), OperationRecordType.wait_audit_activity, data['id'], data['activity_name'])
                self.OperationRecordService.insert_record(
                    real_account_data.to_dict())
        return res

    def padStart0(self, numberString):
        return str('0' + str(numberString)) if numberString < 10 else str(numberString)

    def checkActivityRoomIsAllow(self, room_id, begin_date, end_date):
        # 判断活动室是否可以预约
        # 新增的开始时间和结束时间都不能在已有的时间范围内，已有的时间范围都不能在新增的开始时间和结束时间内
        _filter_room = MongoBillFilter()
        _filter_room.match_bill((C('reservate_room_id') == room_id)
                                & (C('begin_date').filterDate())
                                & (C('end_date').filterDate())
                                & (((C('begin_date') <= begin_date) & (C('end_date') >= begin_date))
                                   | ((C('begin_date') <= end_date) & (C('end_date') >= end_date))
                                   | ((C('begin_date') >= end_date) & (C('end_date') <= end_date))))\
            .sort({'create_date': -1})\
            .project({'_id': 0, **get_common_project()})
        res_room = self.query(
            _filter_room, "PT_Activity_Room_Reservate")

        if len(res_room) > 0:
            zy_array = []
            for item in res_room:
                if item['begin_date'] <= begin_date and begin_date <= item['end_date']:
                    # 开始时间在已有范围内，必然占用
                    begin_date = item['begin_date']
                    end_date = begin_date
                elif item['begin_date'] <= end_date and end_date <= item['end_date']:
                    # 结束时间在已有范围内，必然占用
                    begin_date = end_date
                    end_date = item['end_date']
                elif begin_date <= item['begin_date'] and item['end_date'] <= end_date:
                    # 结束时间在已有范围内，必然占用
                    begin_date = begin_date
                    end_date = end_date

                begin_year = self.padStart0(begin_date.year)
                begin_month = self.padStart0(begin_date.month)
                begin_day = self.padStart0(begin_date.month)
                begin_hour = self.padStart0(begin_date.hour)
                begin_minute = self.padStart0(begin_date.minute)
                begin_second = self.padStart0(begin_date.second)

                end_year = self.padStart0(end_date.year)
                end_month = self.padStart0(end_date.month)
                end_day = self.padStart0(end_date.month)
                end_hour = self.padStart0(end_date.hour)
                end_minute = self.padStart0(end_date.minute)
                end_second = self.padStart0(end_date.second)

                begin_str = begin_year + '-' + begin_month + '-' + begin_day + \
                    ' ' + begin_hour + ':' + begin_minute + ':' + begin_second

                end_str = end_year + '-' + end_month + '-' + end_day + \
                    ' ' + end_hour + ':' + end_minute + ':' + end_second

                if begin_str == end_str:
                    zy_str = begin_str
                else:
                    zy_str = begin_str + ' - ' + end_str

                if zy_str not in zy_array:
                    zy_array.append(zy_str)

            return zy_array
        return True

    def del_activity(self, ids):
        ''' 删除活动 '''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for activity_id in ids:
                delete_data(db, 'PT_Activity', {'id': activity_id})
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def participate_activity(self, condition):
        ''' APP报名活动 '''
        res = 'Fail'

        activity_id = condition['id']

        user_id = get_user_id_or_false(self.session)
        if user_id == False:
            return '用户未登录！'

        now_date = get_cur_time()

        # 先判断是否已报名该活动、是否还在活动有效时间内
        _filter_activity = MongoBillFilter()
        _filter_activity.match_bill((C('id') == activity_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_activity = self.query(
            _filter_activity, "PT_Activity")

        # 找不到活动
        if len(res_activity) == 0:
            return '没有找到该活动！'
        # 已结束
        if now_date > res_activity[0]['end_date']:
            return '活动已结束，无法报名！'
        # 已开始
        if now_date >= res_activity[0]['begin_date']:
            return '活动已开始，无法报名！'

        # 判断是否报名了
        _filter_participate = MongoBillFilter()
        _filter_participate.match_bill((C('activity_id') == activity_id) & (C('user_id') == condition['family_ids'][0]))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_participate = self.query(
            _filter_participate, "PT_Activity_Participate")

        # 有报名数据
        if len(res_participate) > 0:
            return '您已报名该活动！'

        # 判断已报名数量
        _filter_participate_count = MongoBillFilter()
        _filter_participate_count.match_bill((C('activity_id') == activity_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_participate_count = self.page_query(
            _filter_participate_count, "PT_Activity_Participate", 1, 1)

        max_quantity = res_activity[0]['max_quantity']
        if type(res_activity[0]['max_quantity']) == str:
            max_quantity = int(max_quantity)

        if res_participate_count['total'] >= max_quantity:
            return '该活动报名已满员！'

        data_info = get_info({
            'user_id': user_id,
            'family_ids': condition['family_ids'],
            'activity_id': activity_id,
            'status': ParticipateStatus.participate.value,
            # 活动的组织机构ID
            'organization_id': res_activity[0]['organization_id'],
            'is_pc': False,
        }, self.session)

        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.activityParticipate.value, data_info, 'PT_Activity_Participate')
        if bill_id:
            res = 'Success'
        if res == 'Success':
            # 插入一个消息提醒
            self.MessageManageService.add_new_message({
                # 活动报名标识
                "business_type": 'activity_participate',
                # 业务ID
                "business_id": data_info['id'],
                # 组织机构ID
                'organization_id': res_activity[0]['organization_id'],
            })
            real_account_data = OperationRecordObject(
                get_current_user_id(self.session), OperationRecordType.participate_activity, activity_id, res_activity[0]['activity_name'])
            self.OperationRecordService.insert_record(
                real_account_data.to_dict())
        return res

    def participate_activity_pc(self, condition):
        ''' 后台补录 '''
        res = 'Fail'

        # 先判断是否已报名该活动、是否还在活动有效时间内
        _filter_activity = MongoBillFilter()
        _filter_activity.match_bill((C('id') == condition['activity_id']))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_activity = self.query(
            _filter_activity, "PT_Activity")

        # 找不到活动
        if len(res_activity) == 0:
            return '没有找到该活动！'

        # 判断是否报名了
        _filter_participate = MongoBillFilter()
        _filter_participate.match_bill((C('activity_id') == condition['activity_id']) & (C('user_id') == condition['family_ids'][0]))\
            .project({'_id': 0})
        res_participate = self.query(
            _filter_participate, "PT_Activity_Participate")

        # 有报名数据
        if len(res_participate) > 0:
            return '该长者已经报名该活动'

        data_info = get_info({
            'user_id': condition['family_ids'][0],
            'family_ids': condition['family_ids'],
            'activity_id': condition['activity_id'],
            'status': ParticipateStatus.participate.value,
            # 取活动的组织机构ID
            'organization_id': res_activity[0]['organization_id'],
            'is_pc': True,
        }, self.session)

        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.activityParticipate.value, data_info, 'PT_Activity_Participate')
        if bill_id:
            res = 'Success'
        return res

    def cancel_participate_activity(self, activity_id):
        '''取消参与活动'''
        res = 'Fail'

        user_id = get_user_id_or_false(self.session)
        if user_id == False:
            return '用户未登录！'

        now_date = get_cur_time()
        # 先判断是否已报名该活动、是否还在活动有效时间内
        _filter_activity = MongoBillFilter()
        _filter_activity.match_bill((C('id') == activity_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_activity = self.query(
            _filter_activity, "PT_Activity")

        # 找不到活动
        if len(res_activity) == 0:
            return '没有找到该活动！'
        # 已开始
        if now_date >= res_activity[0]['begin_date']:
            return '取消失败，活动已开始！'

        # 判断是否报名了
        _filter_participate = MongoBillFilter()
        _filter_participate.match_bill((C('activity_id') == activity_id) & (C('user_id') == user_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_participate = self.query(
            _filter_participate, "PT_Activity_Participate")

        # 没有报名数据
        if len(res_participate) == 0:
            return '未对该活动报名！'

        # 取消报名数据
        bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                   TypeId.activityParticipate.value, {'id': res_participate[0]['id']}, 'PT_Activity_Participate')
        if bill_id:
            res = 'Success'
        return res

    def sign_in_activity(self, activity_id):
        ''' 活动签到 '''
        res = 'Fail'

        user_id = get_user_id_or_false(self.session)
        if user_id == False:
            return '用户未登录！'

        now_date = get_cur_time()
        # 先判断是否已报名该活动、是否还在活动有效时间内
        _filter_activity = MongoBillFilter()
        _filter_activity.match_bill((C('id') == activity_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_activity = self.query(
            _filter_activity, "PT_Activity")

        # 找不到活动
        if len(res_activity) == 0:
            return '没有找到该活动！'
        # 已结束
        if now_date > res_activity[0]['end_date']:
            return '活动已结束，无法签到！'
        # 已开始
        if now_date <= res_activity[0]['begin_date']:
            return '活动未开始，无法签到！'

        # 判断是否报名了
        _filter_participate = MongoBillFilter()
        _filter_participate.match_bill((C('activity_id') == activity_id) & (C('user_id') == user_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_participate = self.query(
            _filter_participate, "PT_Activity_Participate")

        # 没有报名数据
        if len(res_participate) == 0:
            return '未对该活动报名！'

        # 判断是否签到了
        _filter_signin = MongoBillFilter()
        _filter_signin.match_bill((C('activity_id') == activity_id) & (C('user_id') == user_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_signin = self.query(
            _filter_signin, "PT_Activity_Sign_In")

        # 没有签到数据
        if len(res_signin) == 0:
            # 新增签到数据
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.activitySignin.value, {
                    'user_id': user_id,
                    'activity_id': activity_id,
                    'activity_apply_user_id': res_activity[0]['apply_user_id'],
                    'date': now_date
                }, 'PT_Activity_Sign_In')
        elif len(res_signin) > 0:
            # 更新签到数据
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.activitySignin.value, {
                    'id': res_signin[0]['id'],
                    'date': now_date
                }, 'PT_Activity_Sign_In')
        if bill_id:
            res = 'Success'
        return res

    def delete_activity_participate(self, ids):
        ''' 删除参与活动 '''
        res = 'Fail'

        # 取消报名数据
        if type(ids) == list:
            for item in ids:
                bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                           TypeId.activityParticipate.value, {'id': item}, 'PT_Activity_Participate')
        elif type(ids) == str:
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activityParticipate.value, {'id': ids}, 'PT_Activity_Participate')

        if bill_id:
            res = 'Success'
        return res

    def delete_activity_signin(self, ids):
        ''' 根据主键ID删除签到活动 '''
        res = 'Fail'

        # 取消报名数据
        if type(ids) == list:
            for item in ids:
                bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                           TypeId.activitySignin.value, {'id': item}, 'PT_Activity_Sign_In')
        elif type(ids) == str:
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activitySignin.value, {'id': ids}, 'PT_Activity_Sign_In')

        if bill_id:
            res = 'Success'
        return res

    def delete_sign_in_activity(self, activity_id):
        '''根据活动ID取消签到活动'''
        res = 'Fail'

        user_id = get_user_id_or_false(self.session)
        if user_id == False:
            return '用户未登录！'

        now_date = get_cur_time()
        # 先判断是否已报名该活动、是否还在活动有效时间内
        _filter_activity = MongoBillFilter()
        _filter_activity.match_bill((C('id') == activity_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_activity = self.query(
            _filter_activity, "PT_Activity")

        # 找不到活动
        if len(res_activity) == 0:
            return '没有找到该活动！'
        # 已结束
        if now_date > res_activity[0]['end_date']:
            return '活动已结束，无法取消签到！'
        # 已开始
        if now_date <= res_activity[0]['begin_date']:
            return '活动未开始，无法取消签到！'

        # 判断是否签到了
        _filter_signin = MongoBillFilter()
        _filter_signin.match_bill((C('activity_id') == activity_id) & (C('user_id') == user_id))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_signin = self.query(
            _filter_signin, "PT_Activity_Sign_In")

        # 没有签到数据
        if len(res_signin) == 0:
            return '未对该活动签到！'

        # 删除签到数据
        bill_id = self.bill_manage_server.add_bill(
            OperationType.delete.value, TypeId.activitySignin.value, {
                'id': res_signin[0]['id'],
            }, 'PT_Activity_Sign_In')
        if bill_id:
            res = 'Success'
        return res

    def get_questionnaire_list(self, org_list, condition, page=None, count=None):
        '''查询调查问卷列表'''
        keys = ['id', 'business_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('business_id') == values['business_id']) & (C('organization_id').inner(org_list)))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.page_query(
            _filter, "PT_Questionnaire", page, count)
        return res

    def update_questionnaire(self, data):
        '''新增调查问卷'''
        res = 'Fail'

        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.questionnaire.value, data, 'PT_Questionnaire')
        if bill_id:
            res = 'Success'
        return res

    def get_activity_type_list(self, org_list, condition, page=None, count=None):
        '''查询活动类型列表'''
        keys = ['id', 'name', 'org_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('name').like(values['name'])) & (C('organization_id').inner(org_list)))\
            .lookup_bill("PT_User", "organization_id", "id", "org")\
            .project({'org._id': 0})\
            .add_fields({"org_name": "$org.name"})\
            .match_bill((C('org_name').like(values['org_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'org': 0, **get_common_project()})
        res = self.page_query(
            _filter, "PT_Activity_Type", page, count)
        return res

    def update_activity_type(self, activity_type):
        '''新增/修改活动类型'''
        res = 'Fail'

        if 'id' in activity_type:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.activityType.value, activity_type, 'PT_Activity_Type')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.activityType.value, activity_type, 'PT_Activity_Type')
            if bill_id:
                res = 'Success'
        return res

        # def process_func(db):
        #     nonlocal res
        #     if 'id' in activity_type.keys():
        #         backVal = update_data(db, 'PT_Activity_Type', activity_type, {
        #             'id': activity_type['id']})
        #         if backVal:
        #             res = 'Success'
        #     else:
        #         # 获取流水号字段
        #         activity_type['num'] = get_serial_number(
        #             db, SerialNumberType.activity_type.value)
        #         data_info = get_info(activity_type, self.session)
        #         backVal = insert_data(db, 'PT_Activity_Type', data_info)
        #         if backVal:
        #             res = 'Success'
        # process_db(self.db_addr, self.db_port, self.db_name, process_func)
        # return res

    def del_activity_type(self, activity_type_ids):
        '''删除活动类型'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for activity_type_id in activity_type_ids:
                _filter = MongoBillFilter()
                _filter.match((C('activity_type_id') == activity_type_id)).project(
                    {'_id': 0})
                res = self.query(_filter, "PT_Activity")
                if len(res) > 0:
                    res = '该类型已被引用，无法删除'
                else:
                    delete_data(db, 'PT_Activity_Type', {
                                'id': activity_type_id})
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_activity_room_list(self, org_list, condition, page=None, count=None):
        '''查询活动室列表'''
        keys = ['id', 'activity_room_name',
                'organization_id', 'org_id', 'org_name']
        data_str = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        now_date = get_cur_time()
        if condition.get('org_name') and '(' in condition.get('org_name'):
            condition['org_name'] = condition['org_name'].split('(')[0]

        if 'get_now_org_id' in condition and condition['get_now_org_id'] == True:
            condition['organization_id'] = get_current_organization_id(
                self.session)

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'organization_id', 'id', 'user')\
            .add_fields({
                'organization_name': '$user.name'
            })\
            .lookup_bill('PT_Activity_Room_Reservate', 'id', 'reservate_room_id', 'reservate_info')\
            .add_fields({'is_reservate': self.ao.size(self.ao.array_filter("$reservate_info", "aa", ((F('$aa.reservate_datestr') != None) & (F('$aa.reservate_datestr') == data_str)).f))})\
            .add_fields({'after_reservate': self.ao.array_filter("$reservate_info", "bb", ((F('$bb.reservate_date') != None) & (F('$bb.reservate_date') > now_date)).f)})\
            .match_bill((C('id') == values['id']) & (C('activity_room_name').like(values['activity_room_name'])) & (C('organization_name').like(values['org_name'])) & (C('organization_id') == values['organization_id']) & (C('organization_id').inner(org_list)))

        # 获取当前的登录的用户ID的组织机构ID
        if 'org_id' in condition and condition['org_id'] == True:
            user_id = get_user_id_or_false(self.session)

            if user_id != False:
                _filter_set_role = MongoBillFilter()
                _filter_set_role.match_bill((C('principal_account_id') == user_id))\
                    .project({'_id': 0})
                res_set_role = self.query(_filter_set_role, 'PT_Set_Role')
                if len(res_set_role) > 0:
                    _filter.match_bill(
                        (C('organization_id') == res_set_role[0]['role_of_account_id']))

        _filter.sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'user': 0,
                'reservate_info': 0,
                'after_reservate': 0,
                **get_common_project()
            })
        res = self.page_query(
            _filter, "PT_Activity_Room", page, count)
        return res

    def update_activity_room(self, data):
        '''新增/修改活动室'''
        res = 'Fail'

        if 'max_quantity' in data:
            data['max_quantity'] = int(data['max_quantity'])
        if 'begin_date' in data:
            data['begin_date'] = as_date(data['begin_date'])
        if 'end_date' in data:
            data['end_date'] = as_date(data['end_date'])
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id') == get_current_organization_id(self.session))
            & (C('activity_room_name') == data['activity_room_name']))
        if 'id' in list(data.keys()):
            _filter.match_bill(C('id') != data['id'])
        has_exit = self.query(_filter, 'PT_Activity_Room')
        if len(has_exit):
            res = '该功能室已存在，请勿重复'
        else:
            if 'id' in list(data.keys()):
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.activityRoom.value, data, 'PT_Activity_Room')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.activityRoom.value, data, 'PT_Activity_Room')
                if bill_id:
                    res = 'Success'
        return res

    def del_activity_room(self, activity_room_ids):
        '''删除活动室'''
        res = 'Fail'

        # 删除活动室
        if type(activity_room_ids) == list:
            roomList = []
            for item in activity_room_ids:
                if self.__check_activity_room_is_reservate(item) == False:
                    return '该活动室已被预约！'
                roomList.append({'id': item})
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activityRoom.value, roomList, 'PT_Activity_Room')
        elif type(activity_room_ids) == str:
            if self.__check_activity_room_is_reservate(item) == False:
                return '该活动室已被预约！'
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activityRoom.value, {'id': activity_room_ids}, 'PT_Activity_Room')

        if bill_id:
            res = 'Success'
        return res

    # 判断活动室是否预约中
    def __check_activity_room_is_reservate(self, room_id):
        # 当前时间
        now_date = get_cur_time()
        _filter = MongoBillFilter()
        _filter.match_bill((C('reservate_room_id') == room_id) & (C('reservate_date') > now_date))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.query(
            _filter, "PT_Activity_Room_Reservate")
        if len(res) > 0:
            return False
        return True

    def get_activity_room_reservation_list(self, org_list, condition, page=None, count=None):
        '''查询活动室预约列表'''
        keys = ['id', 'reservate_room_id', 'user_id']

        # def process_func(db):
        #     nonlocal condition
        #     if 'is_oneself' in condition:
        #         condition['user_id'] = get_current_user_id(self.session)
        # process_db(self.db_addr, self.db_port, self.db_name, process_func)

        if 'is_oneself' in condition:
            condition['user_id'] = get_current_user_id(self.session)
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('reservate_room_id').like(values['reservate_room_id']))
                           & (C('user_id') == values['user_id'])
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_Activity_Room', 'reservate_room_id', 'id', 'activityRoom')\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                'activity_room_name': '$activityRoom.activity_room_name',
                'photo': "$activityRoom.photo",
                'open_date': '$activityRoom.open_date',
                'address': '$activityRoom.address',
                'organization_name': '$user.name',
                # 'reservate_date2': self.ao.date_to_string('$reservate_date'),
            })\
            .sort({'create_date': -1})\
            .project({'_id': 0, 'user._id': 0, 'activityRoom._id': 0})
        res = self.page_query(
            _filter, "PT_Activity_Room_Reservate", page, count)
        return res

    def update_activity_room_reservation(self, data):
        '''# 新增/修改活动室预约'''
        res = 'Fail'

        # 查询是否已经预约
        _filter_main = MongoBillFilter()
        _filter_main.match_bill((C('reservate_room_id') == data['reservate_room_id'])
                                & (C('reservate_datestr') == data['reservate_datestr'])).project({'_id': 0})
        main_data = self.query(
            _filter_main, 'PT_Activity_Room_Reservate')

        datakey = list(data.keys())

        # 有当前活动室的当天日子的预约
        if len(main_data) > 0:
            # 是否该数据的编辑操作
            if 'id' not in datakey or ('id' in datakey and main_data[0]['id'] != data['id']):
                # 已经预约
                return '该活动室该时间段已被预约！'

        if 'reservate_date' in data and type(data['reservate_date']) == str:
            data['reservate_date'] = datetime.datetime.strptime(
                data['reservate_date'], '%Y-%m-%dT%H:%M:%S.%fZ')

        if 'id' in datakey:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.activityRoomReservation.value, data, 'PT_Activity_Room_Reservate')
            if bill_id:
                res = 'Success'
        else:

            data['user_id'] = get_current_user_id(self.session)

            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.activityRoomReservation.value, data, 'PT_Activity_Room_Reservate')
            if bill_id:
                res = 'Success'
        return res

    def del_activity_room_reservation(self, ids):
        '''删除活动室预约'''
        res = 'Fail'

        # 删除活动室预约
        if type(ids) == list:
            delList = []
            for item in ids:
                delList.append({'id': item})
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activityRoomReservation.value, delList, 'PT_Activity_Room_Reservate')
        elif type(ids) == str:
            bill_id = self.bill_manage_server.add_bill(OperationType.delete.value,
                                                       TypeId.activityRoomReservation.value, {'id': ids}, 'PT_Activity_Room_Reservate')

        if bill_id:
            res = 'Success'
        return res

    def reservate_activity_room(self, activity_room_id, reservate_date):
        ''' 预约活动室 '''
        res = 'Fail'
        if reservate_date:
            reservate_date = reservate_date[0:10]

        def process_func(db):
            nonlocal res
            activity_room = {}
            activity_room['activity_room_id'] = activity_room_id
            activity_room['reservate_date'] = reservate_date
            activity_room['user_id'] = get_current_user_id(self.session)
            data = find_data(db, 'PT_Activity_Room_Reservate', activity_room)
            if len(data) > 0:
                res = '当前时间你已预约'
            else:
                data_info = get_info(activity_room, self.session)
                backVal = insert_data(
                    db, 'PT_Activity_Room_Reservate', data_info)
                if backVal:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_activity_comment_list(self, org_list, condition, page=None, count=None):
        '''查询评论列表'''

        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('type_id') == '42ec6dd2-c002-11e9-a315-f45fc101cb0b')
                           & (C('name').like(values['name']))
                           & (C('organization_id').inner(org_list)))

        if 'begin_date' in condition and 'end_date' in condition:
            _filter.match_bill(C('create_date') >= condition['begin_date'])
            _filter.match_bill(C('create_date') <= condition['end_date'])

        _filter.sort({'create_date': -1})
        _filter.project({
            '_id': 0,
        })

        res = self.page_query(
            _filter, "PT_Comment", page, count)

        return res

    def get_activity_information_list(self, org_list, condition, page=None, count=None):
        '''查询活动汇总信息列表'''
        monthData = condition['month'].split('-')
        thisMonthDates = calendar.monthrange(
            int(monthData[0]), int(monthData[1]))

        begin_date = str(condition['month']) + '-01 00:00:00'
        end_date = str(condition['month']) + '-' + \
            str(thisMonthDates[1]) + ' 23:59:59'
        begin_date = datetime.datetime.strptime(
            begin_date, "%Y-%m-%d %H:%M:%S")
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")

        newCondition = {}
        newCondition['begin_date'] = begin_date
        newCondition['end_date'] = end_date
        return {
            # 获取活动列表
            'activity_list': self.get_activity_list(org_list, newCondition, 1, 99),
            # 获取签到列表
            'signin_list': self.get_activity_sign_in_list(org_list, newCondition, 1, 99),
            # 获取报名列表
            'participate_list': self.get_activity_participate_list(org_list, newCondition, 1, 99),
            # 获取评论列表
            'comment_list': self.get_activity_comment_list(org_list, newCondition, 1, 99),
        }

    def clearZFC(self, string):
        # 清除excel可能自带的换行符和空格符
        if string == None:
            return ''
        return str(string).strip().replace(
            '\r', '').replace('\n', '').replace('\t', '')

    def upload_sign_in_list(self, data, date):
        pf = pd.DataFrame(data)
        pf.rename(columns={'活动名称': 'activity_name',
                           '长者身份证': 'id_card'}, inplace=True)
        condition = dataframe_to_list(pf)

        id_card_arr = []
        activity_name_arr = []

        if len(condition) > 0:
            for item in condition:
                if item.get('id_card') == None:
                    return '长者身份证必须全不为空！'
                if item.get('activity_name') == None:
                    return '活动名称必须全不为空！'
                if 'e+' in self.clearZFC(item.get('id_card')):
                    return '检测到长者身份证包含科学计数法，请选择长者身份证的单元格格式，设置为特殊分类下的文本格式，确认身份证无误，保存后重新上传！'

                id_card_arr.append(self.clearZFC(item.get('id_card')))
                activity_name_arr.append(self.clearZFC(item['activity_name']))

            organization_id = get_current_organization_id(self.session)

            # 修复对应组织机构
            _filter_activity = MongoBillFilter()
            _filter_activity.match_bill(
                (C('organization_id') == organization_id)
                & (C('activity_name').inner(activity_name_arr)))
            _filter_activity.sort({'modify_date': -1}).project(
                {'_id': 0, 'id': 1, 'activity_name': 1, 'organization_id': 1})

            res_activity = self.query(
                _filter_activity, "PT_Activity")

            tempInActivity = {}
            tempInUser = {}

            # 检测是否每一个活动都对应得上
            for item in res_activity:
                tempInActivity[item['activity_name']] = item
            notInArr = []
            for item in activity_name_arr:
                # 判断不在数组缓存里，就加入的找不到活动的数组里
                if item not in tempInActivity and item not in notInArr:
                    notInArr.append(item)
            if len(notInArr) > 0:
                return {
                    'msg': '以下活动未找到【' + ','.join(notInArr) + '】',
                    'code': -1
                }

            _filter_user = MongoBillFilter()
            _filter_user.match_bill((C('id_card').inner(id_card_arr)))
            _filter_user.project({'_id': 0, 'id': 1, 'name': 1, 'id_card': 1})

            res_user = self.query(
                _filter_user, "PT_User")

            # 检测是否每一个长者都对应得上
            for item in res_user:
                tempInUser[item['id_card']] = item
            notInArr = []
            for item in id_card_arr:
                if item not in tempInUser:
                    notInArr.append(item)
            if len(notInArr) > 0:
                return {
                    'msg': '以下身份证长者未找到【' + ','.join(notInArr) + '】',
                    'code': -1
                }

            # 通过验证，正式插入
            insertData = []
            # 报名时间
            if date != None:
                participate_date = datetime.datetime.strptime(
                    date, '%Y-%m-%d %H:%M:%S')
            else:
                participate_date = get_cur_time()
            for item in condition:

                id_card = self.clearZFC(item.get('id_card'))
                activity_name = self.clearZFC(item.get('activity_name'))

                insertData.append({
                    'activity_id': tempInActivity[activity_name]['id'],
                    'user_id': tempInUser[id_card]['id'],
                    'family_ids': [tempInUser[id_card]['id']],
                    'date': participate_date,
                    'create_date': get_cur_time(),
                    'modify_date': get_cur_time(),
                    'version': 1,
                    'bill_status': 'valid',
                    'id': str(uuid.uuid1()),
                    'GUID': str(uuid.uuid1()),
                    'is_pc': True,
                    'is_dr': True,
                    'organization_id': tempInActivity[activity_name]['organization_id'],
                    'status': 'participate',
                    'bill_operator': get_current_user_id(self.session),
                })

            def process_func(db):
                insert_many_data(db, 'PT_Activity_Participate', insertData)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)

            return {
                'msg': '成功导入' + str(len(insertData)) + '条报名数据',
                'code': '0'
            }
        return {
            'msg': '无可导入的数据',
            'code': -1
        }
