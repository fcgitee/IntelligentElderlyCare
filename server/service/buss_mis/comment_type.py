from ...pao_python.pao.commom import get_data
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:16:32
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\comment_type.py
'''
# -*- coding: utf-8 -*-

'''评论类型函数
'''


class CommentTypeService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_comment_type_list(self, order_ids, condition, page, count):
        collection_name = 'PT_Comment_Type'
        keys = ['name', 'code', 'remark', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoFilter()
        _filter\
            .match(C('organization_id').inner(order_ids) & (C('bill_status') == 'valid') & (C('id') == values['id']) & (C('name').like(values['name'])) & (C('code').like(values['code'])) & (C('remark').like(values['remark'])))\
            .project({'_id': 0})
        res = self.page_query(_filter, collection_name, page, count)
        # print(res)
        return res

    def update_comment_type(self, comment_type):
        ''' 新增/修改评论类型'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in comment_type.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.commentType.value, comment_type, 'PT_Comment_Type')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.commentType.value, comment_type, 'PT_Comment_Type')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_comment_type(self, comment_type_ids):
        """删除评论类型
        Arguments:
        comment_type_ids   {ids}      数据id
        """
        res = "fail"

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(comment_type_ids, str):
                ids.append(comment_type_ids)
            else:
                ids = comment_type_ids
            for comment_type_id in ids:
                # 查询被删除的数据信息
                data = find_data(
                    db,
                    "PT_Comment_Type",
                    {"id": comment_type_id, "bill_status": Status.bill_valid.value},
                )
                if len(data) > 0:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value,
                        TypeId.news.value,
                        data[0],
                        "PT_Comment_Type",
                    )
            res = "Success"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
