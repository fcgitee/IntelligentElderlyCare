from ...pao_python.pao.commom import get_data
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_user_id_or_false, get_current_role_id
from ...service.mongo_bill_service import MongoBillFilter
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.buss_pub.person_org_manage import PersonOrgManageService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:04:58
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\article_type.py
'''
# -*- coding: utf-8 -*-

'''
文章类型函数
'''


class ArticleTypeService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd,)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.PersonOrgManageService = PersonOrgManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_article_type_list(self, order_ids, condition, page, count):
        self._filter = MongoFilter()
        collection_name = 'PT_Article_Type'
        keys = ['name', 'code', 'remark', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('name').like(values['name'])) & (C('code').like(values['code'])) & (C('remark').like(values['remark'])))\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'code': 1,
                'description': 1,
                'remark': 1,
                'organization_id': 1,
            })
        res = self.page_query(_filter, collection_name, page, count)
        return res

    def update_article_type(self, article_type):
        ''' 新增/修改文章类型'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in article_type.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.articleType.value, article_type, 'PT_Article_Type')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.articleType.value, article_type, 'PT_Article_Type')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_article_type(self, article_type_ids):
        """删除文章类型
        Arguments:
        article_type_ids   {ids}      数据id
        """
        res = "fail"

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(article_type_ids, str):
                ids.append(article_type_ids)
            else:
                ids = article_type_ids
            for article_type_id in ids:
                # 查询被删除的数据信息
                data = find_data(
                    db,
                    "PT_Article_Type",
                    {"id": article_type_id, "bill_status": Status.bill_valid.value},
                )
                if data[0]['name'] in ['新闻','公告','共治共建共享','医养结合']:
                    res = '检测到系统保留字段，无法删除'
                    return
                if len(data) > 0:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value,
                        TypeId.news.value,
                        data[0],
                        "PT_Article_Type",
                    )
            res = "Success"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    # 检测是否有新增资讯类型权限
    def check_article_type_permission(self):
        user_id = get_user_id_or_false(self.session)
        flag = False
        if user_id:
            role_id = get_current_role_id(self.session)
            flag = self.PersonOrgManageService.checkPermission(role_id, '文章类型', '新增')
        return flag