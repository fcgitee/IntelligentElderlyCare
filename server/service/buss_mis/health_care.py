
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.buss_pub.person_org_manage import PersonOrgManageService
from ...service.buss_pub.message_manage import MessageManageService

# -*- coding: utf-8 -*-


class UserRelationShipObject():
    '''健康照护'''

    def __init__(self, main_relation_people, subordinate_relation_people, relationship_type_id):
        '''构造函数'''
        # 主关系id
        self.main_relation_people = main_relation_people
        # 关系人id
        self.subordinate_relation_people = subordinate_relation_people
        # 人员关系类型Id
        self.relationship_type_id = relationship_type_id

    def to_dict(self):
        return self.__dict__


"""
健康照护函数
"""


class HealthCareService(MongoService):
    """ 健康照护 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)
        self.MessageManageService = MessageManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def update_medicine_file(self, allowance):
        '''新增药品档案'''
        res = 'Fail'
        # print(allowance)
        if 'id' in allowance.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.allowance.value, allowance, 'PT_Medicine_file')
            if bill_id:
                res = 'Success'
        else:
            allowance['create_time'] = datetime.datetime.now()
            data_info = get_info(allowance, self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.allowance.value, data_info, 'PT_Medicine_file')
            if bill_id:
                res = 'Success'
        return res

    def get_medicine_file_list(self, org_list, condition, page=None, count=None):
        '''查询药品档案列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
            & (C('name').like(values['name'])))\
            .project({'_id': 0})
        res = self.page_query(_filter, 'PT_Medicine_file', page, count)
        # print(res)
        return res

    def del_medicine_file(self, ids):
        ''' 删除单条药品档案 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.allowance.value, {'id': i}, 'PT_Medicine_file')
        if bill_id:
            res = 'Success'
        return res

    def update_disease_file(self, allowance):
        '''新增疾病档案'''
        res = 'Fail'
        # print(allowance)
        if 'id' in allowance.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.disease.value, allowance, 'PT_Disease_file')
            if bill_id:
                res = 'Success'
        else:
            allowance['create_time'] = datetime.datetime.now()
            data_info = get_info(allowance, self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.disease.value, data_info, 'PT_Disease_file')
            if bill_id:
                res = 'Success'
        return res

    def get_disease_file_list(self, org_list, condition, page=None, count=None):
        '''查询疾病档案列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder')\
            .add_fields({
                'elder_name': '$elder.name'
            })\
            .match_bill((C('elder_name').like(values['name'])))\
            .project({'_id': 0, 'elder._id': 0})
        res = self.page_query(_filter, 'PT_Disease_file', page, count)
        # print(res)
        return res

    def del_disease_file(self, ids):
        ''' 删除单条疾病档案 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.disease.value, {'id': i}, 'PT_Disease_file')
        if bill_id:
            res = 'Success'
        return res

    def update_allergy_file(self, allowance):
        '''新增过敏档案'''
        res = 'Fail'
        # print(allowance)
        if 'id' in allowance.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.disease.value, allowance, 'PT_Allergy_file')
            if bill_id:
                res = 'Success'
        else:
            allowance['create_time'] = datetime.datetime.now()
            data_info = get_info(allowance, self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.disease.value, data_info, 'PT_Allergy_file')
            if bill_id:
                res = 'Success'
        return res

    def get_allergy_file_list(self, org_list, condition, page=None, count=None):
        '''查询过敏档案列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list))
        )\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder')\
            .add_fields({
                'elder_name': '$elder.name'
            })\
            .match_bill((C('elder_name').like(values['name'])))\
            .project({'_id': 0, 'elder._id': 0})
        res = self.page_query(_filter, 'PT_Allergy_file', page, count)
        # print(res)
        return res

    def del_allergy_file(self, ids):
        ''' 删除单条疾病档案 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.disease.value, {'id': i}, 'PT_Allergy_file')
        if bill_id:
            res = 'Success'
        return res

    def update_visit_question(self, allowance):
        '''新增意向回访'''
        res = 'Fail'
        # print(allowance)
        if 'id' in allowance.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.disease.value, allowance, 'PT_Return_Visit')
            if bill_id:
                res = 'Success'
        else:
            allowance['create_time'] = datetime.datetime.now()
            data_info = get_info(allowance, self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.disease.value, data_info, 'PT_Return_Visit')
            if bill_id:
                res = 'Success'
        return res

    def get_visit_question_list(self, condition, page=None, count=None):
        '''查询意向回访列表'''
        keys = ['id', 'name', 'question']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id']))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder')\
            .add_fields({
                'elder_name': '$elder.name'
            })\
            .match_bill((C('elder_name').like(values['name'])) & (C('question').like(values['question'])))\
            .project({'_id': 0, 'elder._id': 0})
        res = self.page_query(_filter, 'PT_Return_Visit', page, count)
        # print(res)
        return res

    def del_visit_question(self, ids):
        ''' 删除单条意向回访 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.disease.value, {'id': i}, 'PT_Return_Visit')
        if bill_id:
            res = 'Success'
        return res

    def update_deposit_setting(self, allowance):
        '''新增押金结算'''
        res = 'Fail'
        # print(allowance)
        if 'id' in allowance.keys():
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.disease.value, allowance, 'PT_Deposit_Record')
            if bill_id:
                res = 'Success'
        else:
            allowance['create_time'] = datetime.datetime.now()
            data_info = get_info(allowance, self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.disease.value, data_info, 'PT_Deposit_Record')
            if bill_id:
                res = 'Success'
        return res

    def get_deposit_setting_list(self, condition, page=None, count=None):
        '''查询押金结算列表'''
        keys = ['id', 'id_card', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .lookup_bill('PT_User', 'elder_id', 'id', 'elder')\
            .add_fields({
                'elder_name': '$elder.name'
            })\
            .add_fields({
                'id_card': '$elder.id_card'
            })\
            .match_bill((C('elder_name').like(values['name'])) & (C('id_card') == values['id_card']))\
            .project({'_id': 0, 'elder._id': 0})
        res = self.page_query(_filter, 'PT_Deposit_Record', page, count)
        return res

    def del_deposit_setting(self, ids):
        ''' 删除单条押金结算 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.disease.value, {'id': i}, 'PT_Deposit_Record')
        if bill_id:
            res = 'Success'
        return res
