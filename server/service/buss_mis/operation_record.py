from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import time
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
# -*- coding: utf-8 -*-


class OperationRecordObject():
    '''操作记录对象'''

    def __init__(self, user_id, operation_type, business_id, content):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 类型
        self.type = operation_type
        # 业务表Id
        self.business_id = business_id
        # 操作内容
        self.content = content

    def to_dict(self):
        return self.__dict__


class OperationRecordType:
    order = '订单'
    participate_activity = '报名活动'
    follow_org = '关注机构'
    publish_activity = '发布活动'
    wait_audit_activity = '待审核活动'
    sign_in_activity = '活动签到'


"""
操作记录管理函数
"""


class OperationRecordService(MongoService):
    """ 操作记录管理 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_record_list(self, ids, condition, page, count):
        """获取操作记录列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        """
        user_id = get_current_user_id(self.session)
        keys = ["id", "type"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match((C("user_id") == user_id) & (C("id") == values["id"]) & (C("type") == values["type"]))\
            .sort({'create_date': -1})\
            .limit(count)\
            .project({"_id": 0, })
        res = self.query(_filter, "PT_Operation_Record")
        if len(res) > 0:
            i = 0
            date = {}
            for date in res:
                i = i + 1
                if date['type'] == '活动签到':
                    __filet = MongoBillFilter()
                    __filet.match((C("id") == date['business_id']))\
                        .limit(count)\
                        .project({"_id": 0})
                    res = self.query(__filet, "PT_Activity")
                    # print(datetime.datetime.now())
                    # print(ress)
        res = self.query(_filter, "PT_Operation_Record")
        return res

    def insert_record(self, operation_record):
        """# 新增操作记录"""
        res = "Fail"
        operation = get_info(operation_record, self.session)

        def process_func(db):
            nonlocal res
            insert_id = insert_data(db, 'PT_Operation_Record', operation)
            if insert_id:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def insert_record_t(self):
        ''' 需要记录的地方使用下面的代码 '''
        real_account_data = OperationRecordObject(
            '23b3630a-d92c-11e9-8b9a-983b8f0bcd67', OperationRecordType.sign_in_activity, 'e60ae03a-06c1-11ea-abff-7c2a3115762d', '“幸福，你值得拥有”社区宣传活动')
        res = self.insert_record(real_account_data.to_dict())
        return res
