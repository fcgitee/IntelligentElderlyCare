# -*- coding: utf-8 -*-

"""
新闻管理函数
"""
import datetime
import hashlib
import re
import uuid

import pandas as pd

from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...pao_python.pao.data import (DataList, DataProcess, dataframe_to_list,
                                    process_db)
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import \
    get_current_account_id
from ...service.buss_mis.activity_manage import (ActivityManageService,
                                                 Auditstatus)
from ...service.buss_mis.service_item_package import (
    ServiceItemPackageService)
from ...service.buss_mis.service_follow_collection import \
    ServiceFollowCollectionService
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.buss_pub.message_manage import (MessageManageService,
                                                MessageState)
from ...service.common import (delete_data, find_data, get_condition,
                               get_current_role_id, get_current_user_id,
                               get_info, get_user_id_or_false, insert_data,
                               update_data, get_common_project, get_current_organization_name)
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.person_org_manage import PersonOrgManageService


class NewsService(MongoService):
    """ 新闻管理 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.ServiceFollowCollectionService = ServiceFollowCollectionService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.ActivityManageService = ActivityManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.ServiceItemPackageService = ServiceItemPackageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.MessageManageService = MessageManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.PersonOrgManageService = PersonOrgManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_app_home_page_news_list(self, org_ids, condition, page, count):
        keys = ['status', 'title']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('title').like(values['title'])) & (C("organization_id").inner(org_ids)) & (C("status") == values["status"]))\
               .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'action_info')\
               .add_fields({'likes_count': self.ao.size(self.ao.array_filter(
                   "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'likes')).f))})\
               .add_fields({'share_count': self.ao.size(self.ao.array_filter(
                   "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'share')).f))})\
               .add_fields({'collection_count': self.ao.size(self.ao.array_filter(
                   "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'collection')).f))})\
               .sort({'modify_date': -1})\
               .project({
                   "_id": 0,
                   "action_info": 0
               })
        res = self.page_query(_filter, "PT_Article", page, count)
        return res

    # 优化速度后的接口
    def get_news_pure_list(self, org_list, condition, page=None, count=None):
        """ 单纯查询新闻列表 """
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ["id", 'title', 'start_date', 'end_date',
                'status', 'organization_id', 'org_type', 'author', 'org_name']

        values = self.get_value(condition, keys)
        temp_news_org = {}
        # temp_news_pro = {}
        org_inner = N()
        # pro_inner = N()

        # 过滤机构类型
        if 'org_type' in condition:
            org_inner = []
            _filter_org = MongoBillFilter()
            _filter_org.match_bill((C('personnel_type') == '2') & (
                C('organization_info.personnel_category') == values["org_type"]))
            _filter_org.project({'_id': 0})

            res_org = self.query(
                _filter_org, "PT_User")

            if len(res_org) > 0:
                for item in res_org:
                    org_inner.append(item['id'])
                    temp_news_org[item['id']] = item

        # 过滤发布方
        # if 'promulgator' in condition:
        #     pro_inner = []
        #     _filter_pro = MongoBillFilter()
        #     _filter_pro.match_bill((C('personnel_type') == '1') & (
        #         C('name').like(values['promulgator'])))
        #     _filter_pro.project({'_id': 0})

        #     res_pro = self.query(
        #         _filter_pro, "PT_User")

        #     if len(res_pro) > 0:
        #         for item in res_pro:
        #             pro_inner.append(item['id'])
        #             temp_news_pro[item['id']] = item

        _filter = MongoBillFilter()
        _filter.match_bill((C('title').like(values['title']))
                           & (C("id") == values["id"])
                           & (C("organization_id") == values["organization_id"])
                           & (C("author").like(values['author']))
                           & (C("organization_id").inner(org_inner))
                           & (C("organization_id").inner(org_list))
                           & (C("status") == values["status"])
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date'])))

        # 表示是我发布的
        if 'is_mine' in condition and condition['is_mine'] == True:
            _filter.match_bill(
                (C('promulgator_id') == get_current_user_id(self.session)))

        # _filter.sort({'modify_date': -1})\
        #     .lookup_bill("PT_User", "organization_id", "id", "org")\
        #     .add_fields({'org_name': '$org.name'})\
        #     .match_bill((C('org_name').like(values['org_name'])))\
        #     .project({"_id": 0, 'org._id': 0, **get_common_project()})
        _filter.sort({'step_no': -1, 'modify_date': -1})\
            .project({"_id": 0, **get_common_project()})
        res = self.page_query(_filter, "PT_Article", page, count)

        if len(res['result']) > 0:

            news_type_ids = []
            # news_promulgator_ids = []
            news_organization_ids = []
            news_collection_ids = []

            for item in res['result']:

                # 补全资讯类型名字(1)
                if 'type_id' in item:
                    news_type_ids.append(item['type_id'])

                # 补全发布方名字(1)
                # if 'promulgator_id' in item:
                #     news_promulgator_ids.append(item['promulgator_id'])

                # 补全机构名字(1)
                if 'organization_id' in item:
                    news_organization_ids.append(item['organization_id'])

                if 'id' in item:
                    news_collection_ids.append(item['id'])

            temp_news_type = {}
            # temp_news_collection = {}
            temp_news_likes = {}
            temp_news_share = {}
            temp_news_col = {}

            temp_is_collection = {}

            # 补全资讯类型(2)
            if len(news_type_ids) > 0:
                _filter_type = MongoBillFilter()
                _filter_type.match_bill((C('id').inner(news_type_ids)))\

                _filter_type.project({'_id': 0})

                res_type = self.query(
                    _filter_type, "PT_Article_Type")

                if len(res_type) > 0:
                    for item in res_type:
                        temp_news_type[item['id']
                                       ] = item

            # 补全关注，分享，点赞数据
            if len(news_collection_ids) > 0:
                _filter_collection = MongoBillFilter()
                _filter_collection.match_bill((C('business_id').inner(news_collection_ids)) & (C('object') == '养老资讯'))\

                _filter_collection.project({'_id': 0})

                res_collection = self.query(
                    _filter_collection, "PT_Service_Follow_Collection")

                user_id = get_user_id_or_false(self.session)

                if len(res_collection) > 0:
                    for item in res_collection:
                        if item['business_id'] not in temp_news_likes:
                            temp_news_likes[item['business_id']] = 0
                        if item['business_id'] not in temp_news_share:
                            temp_news_share[item['business_id']] = 0
                        if item['business_id'] not in temp_news_col:
                            temp_news_col[item['business_id']] = 0

                        if item['type'] == 'likes':
                            temp_news_likes[item['business_id']] += 1
                        if item['type'] == 'share':
                            temp_news_share[item['business_id']] += 1
                        if item['type'] == 'collection':
                            temp_news_col[item['business_id']] += 1

                        if user_id and user_id == item['user_id']:
                            if item['business_id'] not in temp_is_collection:
                                temp_is_collection[item['business_id']] = {
                                    'is_collection': False,
                                    'is_likes': False,
                                    'is_share': False,
                                }
                            if item['type'] == 'likes':
                                temp_is_collection[item['business_id']
                                                   ]['is_likes'] = True
                            if item['type'] == 'share':
                                temp_is_collection[item['business_id']
                                                   ]['is_share'] = True
                            if item['type'] == 'collection':
                                temp_is_collection[item['business_id']
                                                   ]['is_collection'] = True

            # 补全发布方(2)
            # if 'promulgator' not in condition and len(news_promulgator_ids) > 0:
            #     _filter_promulgator = MongoBillFilter()
            #     _filter_promulgator.match_bill((C('id').inner(news_promulgator_ids)))\

            #     _filter_promulgator.project({'_id': 0})

            #     res_promulgator = self.query(
            #         _filter_promulgator, "PT_User")

            #     if len(res_promulgator) > 0:
            #         for item in res_promulgator:
            #             temp_news_pro[item['id']
            #                                   ] = item

            # 补全机构
            if 'org_type' not in condition and len(news_organization_ids) > 0:
                _filter_organization = MongoBillFilter()
                _filter_organization.match_bill((C('id').inner(news_organization_ids)))\

                _filter_organization.project({'_id': 0})

                res_organization = self.query(
                    _filter_organization, "PT_User")

                if len(res_organization) > 0:
                    for item in res_organization:
                        temp_news_org[item['id']
                                      ] = item

            for item in res['result']:

                # 补全资讯类型(3)
                if 'type_id' in item and item['type_id'] in temp_news_type:
                    item['type_name'] = temp_news_type[item['type_id']]['name']
                else:
                    item['type_name'] = ''

                # 补全发布方名字（3）
                # if 'promulgator_id' in item and item['promulgator_id'] in temp_news_pro:
                #     item['promulgator'] = temp_news_pro[item['promulgator_id']]['name']
                # else:
                #     item['promulgator'] = ''

                # 补全机构名字（3）
                if 'organization_id' in item and item['organization_id'] in temp_news_org:
                    item['org_name'] = temp_news_org[item['organization_id']]['name']
                else:
                    item['org_name'] = ''

                # 补全总点赞，分享，收藏数据
                if item['id'] in temp_news_likes:
                    item['likes_count'] = temp_news_likes[item['id']]
                else:
                    item['likes_count'] = 0

                if item['id'] in temp_news_share:
                    item['share_count'] = temp_news_share[item['id']]
                else:
                    item['share_count'] = 0

                if item['id'] in temp_news_col:
                    item['collection_count'] = temp_news_col[item['id']]
                else:
                    item['collection_count'] = 0

                # 补全当前用户点赞，分享，收藏数据
                if item['id'] in temp_is_collection:
                    item['is_likes'] = temp_is_collection[item['id']]['is_likes']
                    item['is_share'] = temp_is_collection[item['id']]['is_share']
                    item['is_collection'] = temp_is_collection[item['id']
                                                               ]['is_collection']
                else:
                    item['is_likes'] = False
                    item['is_share'] = False
                    item['is_collection'] = False

        return res

    def get_news_list(self, org_ids, condition, page, count):
        """获取新闻列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        """
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ["id", 'title', 'promulgator',
                'start_date', 'end_date', 'status', 'organization_id', 'org_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('title').like(values['title']))
                           & (C("id") == values["id"])
                           & (C("organization_id") == values["organization_id"])
                           & (C("status") == values["status"])
                           & (C('issue_date') >= as_date(values['start_date']))
                           & (C('issue_date') <= as_date(values['end_date'])))
        _filter.lookup_bill("PT_User", "promulgator_id", "id", "promulgator")\
            .lookup_bill("PT_User", "organization_id", "id", "org_info")\
            .match_bill(C('org_info.organization_info.personnel_category') == values['org_type'])\
            .lookup_bill("PT_Article_Type", "type_id", "id", "type_info")\
            .lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'action_info')\
            .add_fields({'org_name': '$org_info.name'})\
            .add_fields({"type_name": self.ao.array_elemat("$type_info.name", 0)})\
            .add_fields({'likes_count': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'likes')).f))})\
            .add_fields({'share_count': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'share')).f))})\
            .add_fields({'collection_count': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.object') == '养老资讯') & (F('$ai.type') == 'collection')).f))})\
            .add_fields({"promulgator": self.ao.array_elemat("$promulgator.name", 0)})\
            .match_bill((C("promulgator").like(values['promulgator'])))
        if 'user_id' in condition:
            # 表示是我发布的
            _filter.match_bill(
                (C('promulgator_id') == get_current_user_id(self.session)))

        # 判断本人是否点赞了
        user_id = get_user_id_or_false(self.session)
        if user_id != False:
            _filter.add_fields({'is_likes': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.user_id') == user_id) & (F('$ai.object') == '养老资讯') & (F('$ai.type') == 'likes')).f))})
            _filter.add_fields({'is_share': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.user_id') == user_id) & (F('$ai.object') == '养老资讯') & (F('$ai.type') == 'share')).f))})
            _filter.add_fields({'is_collection': self.ao.size(self.ao.array_filter(
                "$action_info", "ai", ((F('$ai.user_id') == user_id) & (F('$ai.object') == '养老资讯') & (F('$ai.type') == 'collection')).f))})

        _filter.sort({'modify_date': -1})\
            .project({
                "_id": 0,
                "promulgator._id": 0,
                "bill_status": 0,
                "bill_operator": 0,
                "import_date": 0,
                "modify_date": 0,
                "GUID": 0,
                "version": 0,
                "action_info": 0,
                "valid_bill_id": 0,
                "new_field._id": 0,
                "process._id": 0,
                'type_info': 0,
                'org_info': 0,
            })
        res = self.page_query(_filter, "PT_Article", page, count)
        return res

    def get_news_list_picture(self, condition):
        # keys = ["id"]
        # values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('step_no') == -1))\
            .lookup_bill("PT_Article_Type", "type_id", "id", "type")\
            .match_bill(C("type.name").like('新闻') | C("type.name").like('news'))\
            .add_fields({"picture": self.ao.array_elemat("$app_img_list", 0)})\
            .sort({'modify_date': -1})\
            .limit(5)\
            .project({"_id": 0, "id": 1, 'title': 1, 'picture': 1})
        res = self.query(_filter, "PT_Article")
        return res

    def update_news(self, data):
        """# 新增新闻"""
        res = "Fail"

        # 草稿
        if 'type' in data and data['type'] == '草稿':
            return self.PersonOrgManageService.create_draft({
                'draft': data,
                'type': 'article'
            })

        # 定义审批类型
        define_type = 'newsPublish'
        # 数据表
        ptdb = 'PT_Article'
        # 类型id
        typeid = TypeId.news.value

        # 审核的编辑
        if 'action' in data and data['action'] == 'sh':
            result = self.ServiceItemPackageService.change_approval_process(
                data, ptdb, typeid)

            if result == 'Success':

                # 审批流程要判断这个消息提醒是否可以设置已读
                _filter_msg = MongoBillFilter()
                _filter_msg.match_bill((C('business_id') == data['id']) & (C('message_state') == MessageState.Unread.value))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                res_msg = self.query(
                    _filter_msg, "PT_Message")

                if len(res_msg) > 0:
                    # 设置已读
                    self.MessageManageService.set_message_already_read({
                        'id': res_msg[0]['id']
                    })
            return result

        if 'id' in data.keys():

            # 重新编辑
            bill_id = self.ServiceItemPackageService.rechange(
                data, ptdb, typeid, define_type)

            res = bill_id
        else:

            # 补充一个创建人
            data['promulgator_id'] = get_current_user_id(
                self.session)

            # 没有资讯类型的，找一个新闻的类型补充给他
            if 'type_id' not in data:
                _filter_atype = MongoBillFilter()
                _filter_atype.match_bill((C('name').like('新闻')))\
                    .sort({'create_date': -1})\
                    .project({"_id": 0})
                res_atype = self.query(_filter_atype, "PT_Article_Type")

                if len(res_atype) > 0:
                    data['type_id'] = res_atype[0]['id']

            # 没有发布方的，补充一个机构名字给他
            if 'author' not in data:
                data['author'] = get_current_organization_name(self.session)

            # 主数据
            main_data = get_info(data, self.session)
            # 接入审批流程
            bill_id = self.ServiceItemPackageService.create_approval_process(
                main_data, ptdb, typeid, define_type)
            res = bill_id

            # 插入一个消息提醒
            self.MessageManageService.add_new_message({
                # 新闻标识
                "business_type": 'article',
                # 业务ID
                "business_id": main_data['id']
            })
        return res

    def delete_news(self, news_ids):
        """删除新闻
        Arguments:
        news_ids   {ids}      数据id
        """
        res = "fail"

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(news_ids, str):
                ids.append(news_ids)
            else:
                ids = news_ids
            for news_id in ids:
                # 查询被删除的数据信息
                data = find_data(
                    db,
                    "PT_Article",
                    {"id": news_id, "bill_status": Status.bill_valid.value},
                )
                if len(data) > 0:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value,
                        TypeId.news.value,
                        data[0],
                        "PT_Article",
                    )
            res = "Success"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def update_ctrl(self, condition):
        """# 点赞分享"""
        res = "Failed"
        keys = ["id", 'type']

        if 'id' not in condition or 'type' not in condition or condition['type'] not in ['share', 'likes', 'collection']:
            return '非法操作！'

        # 判断是否登录
        user_id = get_user_id_or_false(self.session)

        if user_id == False:
            return '用户未登录！'

        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .sort({'create_date': -1})\
            .project({"_id": 0})
        res_article = self.query(_filter, "PT_Article")

        if len(res_article) > 0:

            # 检查不让重复点赞
            _filter = MongoBillFilter()
            _filter.match_bill((C('user_id') == user_id) & (C('object') == '养老资讯') & (C('type') == condition['type']))\
                .sort({'create_date': -1})\
                .project({"_id": 0})
            res_sfc = self.query(_filter, "PT_Service_Follow_Collection")

            # 取消点赞
            if condition['type'] == 'likes' and len(res_sfc) > 0:
                if self.ServiceFollowCollectionService.delete_service_follow_collection(res_sfc[0]['id']) == 'Success':
                    return 'Success'

            # 调用接口
            if self.ServiceFollowCollectionService.update_service_follow_collection(values['id'], condition['type'], '养老资讯') != 'Success':
                if condition['type'] == 'likes':
                    return '点赞失败！'
                elif condition['type'] == 'share':
                    return '分享失败！'

            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.news.value, res_article[0], "PT_Article"
            )
            if bill_id:
                res = "Success"

        return res

    # 检查app发布的权限
    def check_app_role_type(self, condition):
        user_id = get_user_id_or_false(self.session)
        flag = False
        if user_id:
            role_id = get_current_role_id(self.session)

            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == role_id) & (C('role_type') == '幸福院'))\
                .sort({'create_date': -1})\
                .project({'_id': 0})
            res = self.query(
                _filter, "PT_Role")
            if len(res) > 0:
                flag = True
        return flag

    # def get_article_draft(self, condition):
    #     user_id = get_current_user_id(
    #         self.session)
    #     _filter = MongoBillFilter()
    #     _filter.match_bill((C('user_id') == user_id))\
    #         .sort({'create_date': -1})\
    #         .project({"_id": 0})
    #     res = self.page_query(_filter, "PT_Article_Draft", 1, 1)
    #     return res
