
import datetime
import re
from ...pao_python.pao.data import (DataProcess, data_to_string_date,
                                    dataframe_to_list, get_cur_time,
                                    process_db)
# from ...service.welfare_institution.accommodation_process import AccommodationProcess
from ...pao_python.pao.remote import JsonRpc2Error
from ...pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                     MongoService, N, as_date)
from ...service.app.my_order import RecordStatus
from ...service.buss_mis.transaction_management import TransactionService
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import (SerialNumberType, delete_data, find_data,
                               get_code, get_condition, get_current_user_id,
                               get_date_slot, get_info, get_serial_number,
                               insert_data, operation_result, update_data, get_user_id_or_false, execute_python, get_common_project)
from ...service.constant import ApplyState
from ...service.mongo_bill_service import MongoBillFilter

import time


class Order():
    '''单据对象'''

    def __init__(self, purchaser_id, service_provider_id, order_fee, detail, remark):
        '''构造函数'''
        self.purchaser_id = purchaser_id
        self.service_provider_id = service_provider_id
        self.order_fee = order_fee
        self.detail = detail
        self.remark = remark
        self.order_date = get_cur_time().strftime("%Y-%m-%d %H:%M:%S")
        self.status = 'new'

    def to_dict(self):
        return self.__dict__


class OrderRecord():
    '''订单记录对象'''

    def __init__(self, order_id, service_product_id, servicer_id, content, option_list, begin_date=None, end_date=None):
        '''构造函数'''
        # 服务订单ID
        self.order_id = order_id
        # 服务产品ID
        self.service_product_id = service_product_id
        # 开始时间
        self.start_date = begin_date
        # 服务完成时间
        self.end_date = end_date
        # 服务人员
        self.servicer_id = servicer_id
        # 服务内容
        self.content = content
        # 当前状态
        self.status = 'new'
        # 服务选项
        self.option_list = option_list
        # 计价金额(通过项目中的计价公式+订单项目的选项+服务记录的选项清单生成)
        self.valuation_amount = float(0)

    def to_dict(self):
        return self.__dict__


class Ledger():
    '''台账对象'''

    def __init__(self, user_id, service_end_date, total_amount, item_list, remark):
        '''构造函数'''
        # 用户ID
        self.user_id = user_id
        # 服务结束时间
        self.service_end_date = service_end_date
        # 总金额
        self.total_amount = total_amount
        # 服务项目列表
        self.item_list = item_list
        # 备注
        self.remark = remark

    def to_dict(self):
        return self.__dict__


class ServiceOrderCommentObject():
    '''新增/编辑服务订单评论'''

    def __init__(self, user_id, order_id, product_id, service_attitude, service_quality, opinion_remarks):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 订单id
        self.order_id = order_id
        # 服务产品id
        self.product_id = product_id
        # 服务态度评分
        self.service_attitude = service_attitude
        # 服务质量评分
        self.service_quality = service_quality
        # 意见备注
        self.opinion_remarks = opinion_remarks

    def to_dict(self):
        return self.__dict__


class ServiceOperationService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.transaction_service = TransactionService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

        # self.accommodation_process = AccommodationProcess(
        # db_addr, db_port, db_name, inital_password, session)
    def delete_order(self, order_ids):
        """删除订单
        Arguments:
        order_ids   {ids}      数据id
        """
        res = "fail"

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(order_ids, str):
                ids.append(order_ids)
            else:
                ids = order_ids
            for news_id in ids:
                # 查询被删除的数据信息
                data = find_data(
                    db,
                    "PT_Service_Order",
                    {"id": news_id, "bill_status": Status.bill_valid.value},
                )
                if len(data) > 0:
                    self.bill_manage_server.add_bill(
                        OperationType.delete.value,
                        TypeId.news.value,
                        data[0],
                        "PT_Service_Order",
                    )
            res = "Success"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_service_provider_list(self, order_ids, condition, page=None, count=None):
        '''查询服务商列表'''
        if 'date_range' in condition.keys():
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
            keys = ['id', '  ', 'start_date', 'end_date', 'address',
                    'sp_ability_level', 'sp_credit_level', 'business_name']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.lookup_bill('PT_User', 'service_provider_id', 'id', 'user')\
                .add_fields({'user_name': '$user.name',
                             'address': '$user.address',
                             'telephone': '$user.telephone',
                             })\
                .match_bill((C('id') == values['id'])
                            & (C('organization_id').inner(order_ids))
                            & (C('register_date') >= as_date(values['start_date']))
                            & (C('register_date') <= as_date(values['end_date']))
                            & (C('user_name').like(values['name']))
                            & (C('address').like(values['address'])))\
                .project({'_id': 0, 'user._id': 0})
            res = self.page_query(
                _filter, 'PT_Service_Provider_Register', page, count)
            return res

        if 'business_name' in condition.keys():
            keys = ['business_name']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('name').like(values['business_name'])) & (C('organization_info.personnel_category') == "服务商"))\
                .project({'_id': 0})
            res = self.page_query(
                _filter, 'PT_User', page, count)
            return res

        if 'admin_area_id' in condition.keys():
            keys = ['admin_area_id']
            values = self.get_value(condition, keys)
            _filter = MongoBillFilter()
            _filter.match_bill((C('admin_area_id') == values['admin_area_id']) & (C('organization_info.personnel_category') == "服务商"))\
                .project({'_id': 0})
            res = self.page_query(
                _filter, 'PT_User', page, count)
            return res
        keys = ['name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Service_Follow_Collection', 'id', 'business_id', 'flow_list')\
            .add_fields({'flow_list': self.ao.array_filter('$flow_list', 'record', (F('$record.type') == 'follow').f)})\
            .add_fields({'follow_num': self.ao.size('$flow_list')})\
            .match_bill((C('organization_info.personnel_category') == "服务商") & (C('name').like(values['name'])))\
            .project({'_id': 0, 'flow_list._id': 0})
        if 'profile' in condition.keys() and condition['profile'] == 'up':
            _filter.sort({'follow_num': -1})
        res = self.page_query(
            _filter, 'PT_User', page, count)
        return res

    def get_Subsidy_service_provider_list(self, condition, page, count):
        '''获取待审核服务商列表'''
        if 'date_range' in condition.keys():
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['id', 'name', 'organization_nature',
                'address', 'start_date', 'end_date']
        persion_id = get_current_user_id(self.session)
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_Approval_Process', 'id', 'business_id', 'process')\
               .lookup_bill('PT_User', 'service_provider_id', 'id', 'org')\
            .add_fields({'new_field': self.ao.array_filter(
                "$process", "aa", ((F('$aa.approval_user_id') == persion_id) & (F('$aa.step_no') == (F('step_no'))) & (F('$aa.status') == '待审批')).f)}).match((C("new_field") != None) & (C("new_field") != []))\
            .match_bill((C('register_date') >= as_date(values['start_date']))
                        & (C('register_date') <= as_date(values['end_date']))
                        & (C('id') == values['id'])
                        & (C('org.name').like(values['name']))
                        & (C('organization_nature').like(values['organization_nature']))
                        & (C('address').like(values['address'])))\
            .project({'_id': 0, 'new_field._id': 0, 'process._id': 0, 'org._id': 0})
        res = self.page_query(
            _filter, 'PT_Service_Provider_Register', page, count)
        return res

    def get_service_provider_byId(self, condition, page=None, count=None):
        '''根据id查询服务商'''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_User', page, count)
        return res

    def get_current_service_provider(self, condition, page=None, count=None):
        '''获取当前用户的供应商信息'''
        userid = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'service_provider_id', 'id', 'user') \
            .add_fields({'user_name': '$user.name',
                         'address': '$user.address',
                         'telephone': '$user.telephone',
                         'create_date': self.ao.date_to_string('$create_date'),
                         'modify_date': self.ao.date_to_string('$modify_date')}) \
            .match_bill((C('service_provider_id') == userid)) \
            .project({'_id': 0, 'user._id': 0})
        res = self.page_query(
            _filter, "PT_Service_Provider_Register", page, count)
        return res

    def update_service_provider(self, serviceProvider):
        '''新增/修改服务商'''
        res = 'Fail'

        # data_info = get_info({**serviceProvider},self.session)
      # print(serviceProvider)
        # def process_func(db):
        #     nonlocal res
        # if 'id' in serviceProvider.keys():
        #     flag = OperationType.update.value
        # else:
        #     flag = OperationType.add.value
        #     data_info['register_date'] = datetime.datetime.now()
        # if 'service_provider_id' not in data_info:
        #     data_info['service_provider_id'] = get_current_user_id(
        #         self.session)
        #     data_info['status'] = "正在申请"
        # bill_id = self.bill_manage_server.add_bill(flag,
        #                                            TypeId.serviceProvider.value, data_info, 'PT_Service_Provider_Register')
        # if bill_id:
        #     res = 'Success'
        # process_db(self.db_addr, self.db_port, self.db_name, process_func)
        # return res
        person_id = get_current_user_id(self.session)
        # 判断是否已经存在该组织机构
        _filter = MongoBillFilter()
        _filter.match_bill((C('name') == serviceProvider['name'])
                           & (C('admin_area_id') == serviceProvider['admin_area_id'])
                           & (C('organization_info.super_org_id') == serviceProvider['super_org_id'])
                           & (C('organization_info.personnel_category') == '服务商')
                           & (C('personnel_type') == '2'))\
            .project({'_id': 0})
        provider_res = self.query(_filter, 'PT_User')
        if len(provider_res) > 0:
            res = '该服务商已存在'
        else:
            # 查询是否已经存在申请记录
            _filter = MongoBillFilter()
            _filter.match_bill((C('name') == serviceProvider['name'])
                               & (C('admin_area_id') == serviceProvider['admin_area_id'])
                               & (C('super_org_id') == serviceProvider['super_org_id'])
                               & (C('organization_nature') == serviceProvider['organization_nature'])
                               & (C('step_no') != -1))\
                .project({'_id': 0})
            record_res = self.query(_filter, 'PT_Service_Provider_Register')
            if len(record_res) > 0:
                res = '已经申请过了，不可重复申请'
            else:
                data_info = get_info(serviceProvider, self.session)
                # 根据审批定义新增审核过程表数据，新增业务表数据
                _filter = MongoBillFilter()
                _filter.match_bill((C('approval_type') == 'ProviderApply'))\
                    .project({'_id': 0})
                res_define = self.query(_filter, 'PT_Approval_Define')
                if len(res_define) > 0 and len(res_define[0]['approval_list']) > 0:
                    for approval in res_define[0]['approval_list']:
                        if approval['step_no'] == 1:
                            process_data_list = []
                            # 查找属于平台的组织结构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('personnel_type') == '2') & (C('organization_info.personnel_category') == '平台'))\
                                .project({'_id': 0})
                            forumInfo = self.query(_filter, 'PT_User')
                          # print(forumInfo)
                            # 拿到平台的id以及角色id确定可以审核的人id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == forumInfo[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(
                                _filter, 'PT_Set_Role')
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            process_data = {"define_id": res_define[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data_info['step_no'] = process_data_list[0]['step_no']
                            data_info['status'] = ApplyState.apply
                            data_info['register_date'] = get_cur_time()
                            data_info['apply_user_id'] = person_id
                            data = [data_info, process_data_list]
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.servicePersonal.value, data, ['PT_Service_Provider_Register', 'PT_Approval_Process'])
                            if bill_id:
                                res = 'Success'
                        if approval['step_no'] == 2:
                            process_data_list = []
                            # 根据申请人的信息去user表查所属行政区划id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == person_id))\
                                .project({'_id': 0})
                            userInfo = self.query(_filter, 'PT_User')
                          # print("数据>>>>>>>>>>", userInfo)
                            # 根据行政区划id找出当前区划下的民政机构
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('admin_area_id') == userInfo[0]['admin_area_id']) & (C('personnel_type') == '2') & (C('organization_info.personnel_category') == '民政'))\
                                .project({'_id': 0})
                            mzOrganizationInfo = self.query(_filter, 'PT_User')
                          # print("这里报错吗", mzOrganizationInfo)
                            # 根据角色id以及机构id找出审核人id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('role_id') == approval['approval_user_id']) & (C('role_of_account_id') == mzOrganizationInfo[0]['id']))\
                                .project({'_id': 0})
                            aboutUserIdList = self.query(
                                _filter, 'PT_Set_Role')
                            approval_user_id = aboutUserIdList[0]['principal_account_id']
                            # print("长者33333333333333333333333333333333333333333333",approval_user_id)
                            process_data = {"define_id": res_define[0]['id'], "business_id": data_info['id'], "approval_user_id":
                                            approval_user_id, "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                            process_data_list.append(
                                get_info(process_data, self.session))
                            data = [process_data_list]
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.servicePersonal.value, data, ['PT_Approval_Process'])
                            if bill_id:
                                res = 'Success'
        return res

    def update_service_provider_record(self, reviewed_result):
        '''# 服务商审核'''
        res = 'Fail'

        persion_id = get_current_user_id(self.session)
      # print(reviewed_result)
        if 'id' in reviewed_result.keys():
            # 查询记录表的记录
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == reviewed_result['id']))\
                .project({'_id': 0})
            apply_record = self.query(_filter, 'PT_Service_Provider_Register')
            if len(apply_record) > 0:
                jlb_record = apply_record[0]
                # 查询过程表的数据
                _filter_process = MongoBillFilter()
                _filter_process.match_bill((C('business_id') == reviewed_result['id']) & (
                    C('approval_user_id') == persion_id) & (C('step_no') == jlb_record['step_no'])).project({'_id': 0})
                process_data = self.query(
                    _filter_process, 'PT_Approval_Process')
                if len(process_data) > 0 and 'subsidy_res' in reviewed_result.keys() and 'opinion' in reviewed_result.keys():
                    # 获取审批结果
                    subsidy_result = reviewed_result['subsidy_res']
                    process = process_data[0]
                    process['opinion'] = []
                    process['opinion'].append(reviewed_result['opinion'])
                    if subsidy_result == '通过':
                        process['status'] = '通过'
                        # 判断是否最后一步，最后一步则完结业务单据状态且step_no改为空,不是最后一步则把step_no改为下一步
                        _filter_process_length = MongoBillFilter()
                        _filter_process_length.match_bill(
                            (C('business_id') == reviewed_result['id'])).project({'_id': 0})
                        process_data_length = len(self.query(
                            _filter_process_length, 'PT_Approval_Process'))
                        if jlb_record['step_no'] > 0 and process_data_length == jlb_record['step_no']:
                            # 最后一步
                            jlb_record['status'] = '通过'
                            jlb_record['step_no'] = -1
                            # 往user表里新增一条服务商信息
                            user_insert_info = {"name": jlb_record['name'], "admin_area_id": jlb_record['admin_area_id'], "personnel_type": "2",
                                                "organization_info": {"telephone": jlb_record['telephone'], "introduction": jlb_record['introduction'], "picture_list": jlb_record['picture_list'],
                                                                      "organization_nature": jlb_record['organization_nature'], "lon": jlb_record['lon'], "lat": jlb_record['lat'],
                                                                      "personnel_category": "服务商", "super_org_id": jlb_record['super_org_id'], "legal_person": jlb_record['legal_person'],
                                                                      "business_license_url": jlb_record['business_license_url'], "address": jlb_record['address'],
                                                                      }
                                                }
                            organization_inset_info = get_info(
                                user_insert_info, self.session)
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.servicePersonal.value, organization_inset_info, ['PT_User'])
                          # print("新增服务商的信息>>>>>>>>>>>>", bill_id)
                            # 查询当前用户的信息
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('id') == persion_id))\
                                .project({'_id': 0})
                            user_info_res = self.query(_filter, 'PT_User')
                            user_info = user_info_res[0]
                            # 再把新增的服务商id绑到当前用户的组织机构id
                            user_info['organization_id'] = organization_inset_info['id']
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.update.value, TypeId.servicePersonal.value, user_info, ['PT_User'])
                            # 查询服务商超级管理员角色id
                            _filter = MongoBillFilter()
                            _filter.match_bill((C('name') == "超级管理员")
                                               & (C('role_type') == "服务商"))\
                                .project({'_id': 0})
                            role_res = self.query(_filter, 'PT_Role')
                          # print(role_res)
                            role_id = role_res[0]['id']
                            # 将当前用户的角色设置为服务商超级管理员
                            role_insert_info = {"principal_account_id": persion_id,
                                                "role_of_account_id": organization_inset_info['id'], "role_id": role_id}
                            bill_id = self.bill_manage_server.add_bill(
                                OperationType.add.value, TypeId.servicePersonal.value, role_insert_info, ['PT_Set_Role'])
                            if bill_id:
                                res = 'Success'
                        elif jlb_record['step_no'] > 0 and process_data_length > jlb_record['step_no']:
                            # 非最后一步
                            jlb_record['status'] = '审批中'
                            jlb_record['step_no'] = jlb_record['step_no'] + 1
                        else:
                            return '无法重复审批'
                    if subsidy_result == '不通过':
                        process['status'] = '不通过'
                        # 把业务单据的状态改为不通过
                        jlb_record['status'] = '不通过'
                    # 修改业务表和过程表
                    data = [jlb_record, process]
                    bill_id = self.bill_manage_server.add_bill(OperationType.update.value, TypeId.servicePersonal.value, data, [
                        'PT_Service_Provider_Register', 'PT_Approval_Process'])
                    if bill_id:
                        res = 'Success'
        return res

    def del_service_provider(self, ids):
        '''删除服务商'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for business_id in ids:
                data = find_data(db, 'PT_Service_Provider_Register', {
                    'id': business_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.serviceProvider.value, data[0], 'PT_Service_Provider_Register')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_service_type_list(self, condition, page=None, count=None):
        '''查询服务类型列表'''
        keys = ['id', 'is_top', 'name', 'is_show_app', 'parent_id']

        # 默认获取已开启的
        if 'all' in condition and condition['all'] == True:
            pass
          # print('获取全部')
        elif 'is_show_app' not in condition:
            condition['is_show_app'] = True
        elif 'id' in condition and 'is_show_app' not in condition:
            pass
          # print(1111111)

        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.add_fields({
            'create_date': self.ao.date_to_string('$create_date'),
            'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill((C('id') == values['id']) & (C('name').like(values['name'])) & (C('is_show_app') == values['is_show_app']) & (C('parent_id') == values['parent_id']))\
            .sort({'show_num': 1})\
            .project({'_id': 0})
        if 'is_top' in condition.keys():
            _filter.match((C('parent_id') == '') | (C('parent_id') == None))
        res = self.page_query(
            _filter, "PT_Service_Type", page, count)
        return res

    def get_service_type_list_tree(self, condition, page=None, count=None):
        '''查询服务类型树形结构列表'''
        _filter = MongoBillFilter()
        if 'id' in condition.keys():
            _filter.match_bill((C('id') == condition['id']))
        else:
            _filter.match_bill((C('parent_id') == '') |
                               (C('parent_id') == None))
        _filter.add_fields({
            'label': '$name',
            'value': '$name',
            'children': []
        })\
            .project({'_id': 0, 'id': 1, 'label': 1, 'value': 1, 'children': 1})
        res = self.page_query(_filter, 'PT_Service_Type', page, count)
        result = []
        if len(res['result']) > 0:
            for data in res['result']:
                new_res = self.query_child(data['id'], condition)
                r_data = self.xuhuan(new_res, data, condition)
                result.append(r_data)

        res['result'] = result
        return res

    def xuhuan(self, new_res, return_data, condition):
        if len(new_res) > 0:
            return_data['children'] = new_res
            for data in new_res:
                child_data = self.query_child(data['id'], condition)
                self.xuhuan(child_data, data, condition)
        return return_data

    def query_child(self, parent_id, condition):
        keys = ['']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('parent_id') == parent_id))\
            .add_fields({
                'label': '$name',
                'value': '$id',
                'children': []
            })\
            .project({'_id': 0, 'id': 1, 'label': 1, 'value': 1, 'children': 1})
        res = self.query(
            _filter, "PT_Service_Type")
        return res

    def get_newest_nunmber(self, stype, pid=''):

        _filter = MongoBillFilter()

        p_number = ''

        if stype == '1':
            # 父级
            _filter.match_bill((C('parent_id') == '') |
                               (C('parent_id') == None))
        elif stype == '2':
            # 子级
            _filter.match_bill((C('parent_id') == pid))

            _filter_p = MongoBillFilter()
            _filter_p.match_bill((C('id') == pid))
            _filter_p.project({'_id': 0})
            res_p = self.query(
                _filter_p, 'PT_Service_Type')

            if len(res_p) > 0:
                p_number = res_p[0]['number']
                if type(p_number) != str:
                    p_number = str(p_number)

        _filter.project({'_id': 0})
        res = self.query(
            _filter, 'PT_Service_Type')

        # 遍历获取最大的
        # 初始化为0
        maxNumber = 0
        # 初始化为00
        maxOriginNumber = '00'
        for item in res:
            if 'number' in item:
                # 取值
                itemNumber = item['number']
                # 转字符串
                if type(itemNumber) != str:
                    itemNumber = str(itemNumber)
                # 判断是不是数字
                if itemNumber.encode('UTF-8').isdigit():
                    # 比储存大的就获取
                    if int(itemNumber) > maxNumber:
                        maxNumber = int(itemNumber)

                        if stype == '2':
                            # 去除父级编号的原编号
                            reg = re.compile(r'^' + p_number + '')
                            maxOriginNumber = re.sub(reg, '', itemNumber)
                            # print('reg', reg, maxOriginNumber, itemNumber)
                        else:
                            maxOriginNumber = itemNumber

        if type(maxOriginNumber) == str:
            # 先转数字+1
            maxOriginNumber = int(maxOriginNumber)
            maxOriginNumber = maxOriginNumber+1
        # 少于10补0
        if maxOriginNumber < 10:
            maxOriginNumber = '0' + str(maxOriginNumber)

        if stype == '2':
            # 补全父级编号
            return p_number + maxOriginNumber
        else:
            return maxOriginNumber

    def update_service_type(self, serviceType):
        '''新增/修改类型'''
        res = 'Fail'

        data_info = get_info({**serviceType}, self.session)

        def process_func(db):
            nonlocal res
            if 'id' in serviceType.keys():
                flag = OperationType.update.value
            else:
                flag = OperationType.add.value

            # 保存编号
            if flag == OperationType.add.value:
                if 'parent_id' in serviceType and serviceType['parent_id'] != None and serviceType['parent_id'] != '':
                    # 获取子级当前最大的编号
                    n_number = self.get_newest_nunmber(
                        '2', serviceType['parent_id'])
                else:
                    # 获取父级当前最大的编号
                    n_number = self.get_newest_nunmber('1')

                # 编号
                data_info['number'] = n_number

            bill_id = self.bill_manage_server.add_bill(flag,
                                                       TypeId.serviceType.value, data_info, 'PT_Service_Type')
            if bill_id:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_service_type(self, ids):
        '''删除服务类型'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for business_id in ids:
                data = find_data(db, 'PT_Service_Type', {
                    'id': business_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.serviceType.value, data[0], 'PT_Service_Type')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def recovery_service_type(self, data_id):
        '''恢复上一版本数据'''
        return self.bill_manage_server.recovery_previous_version(data_id, 'PT_Service_Type')

    def get_service_item_list(self, order_ids, condition, page=None, count=None):
        '''查询服务项目列表'''
        keys = ['id', 'service_type_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_id').inner(order_ids))\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'user')\
            .lookup_bill('PT_Service_Type', 'service_type_id', 'id', 'type')\
            .add_fields({'service_type_name': '$type.name',
                         'service_provider_name': '$user.name',
                         'create_date': self.ao.date_to_string('$create_date'),
                         'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill((C('id') == values['id']) & (C('service_type_id').inner(values['service_type_id'])))\
            .project({'_id': 0, 'user._id': 0, 'type._id': 0})
        res = self.page_query(
            _filter, 'PT_Service_Item', page, count)
        return res

    def update_service_item(self, serviceItem):
        '''新增/修改服务项目 '''
        res = 'Fail'
        data_info = get_info({**serviceItem}, self.session)

        def process_func(db):
            nonlocal res
            if 'id' in serviceItem.keys():
                flag = OperationType.update.value
            else:
                flag = OperationType.add.value
            bill_id = self.bill_manage_server.add_bill(flag,
                                                       TypeId.serviceItem.value, data_info, 'PT_Service_Item')
            if bill_id:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_service_item(self, ids):
        ''' 删除服务项目 '''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for item_id in ids:
                data = find_data(db, 'PT_Service_Item', {
                    'id': item_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_server.add_bill(OperationType.delete.value,
                                                     TypeId.serviceItem.value, data[0], 'PT_Service_Item')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def recovery_service_item(self, data_id):
        '''恢复上一版本数据'''
        return self.bill_manage_server.recovery_previous_version(data_id, 'PT_Service_Item')

    def service_evaluate(self, serviceEvaluate):
        ''' 服务记录评价 serviceEvaluate：{id:'',evaluate:{}}'''
        res = 'Fail'

        def process_func(db):
            # 先查服务记录信息，再增加评价内容
            nonlocal res
            # 评价人Id取当前操作用户id
            user_id = get_current_user_id(self.session)
            evaluate = serviceEvaluate['evaluate']
            evaluate['evaluator_id'] = user_id
            record_mes = find_data(db, 'PT_Service_Record', {
                                   'id': serviceEvaluate['id']})
            if len(record_mes) > 0:
                if 'evaluate_list' in record_mes[0]:
                    record_mes[0]['evaluate_list'] = record_mes[0]['evaluate_list'] + evaluate
                else:
                    record_mes[0]['evaluate_list'] = evaluate
                bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                           TypeId.serviceItemEvaluate.value, record_mes[0], 'PT_Service_Record')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_service_order_fwyy_fwdd_list(self, order_ids, condition, page=None, count=None):
        '''查询服务订单列表--服务运营服务订单'''
        keys = ['id', 'purchaser_name', 'org_name', 'purchaser_id_card', 'service_provider_id', 'purchaser_id', 'status',
                'order_date', 'service_date', 'pay_date', 'order_state', 'origin_product', 'need_fp', 'transation_code', 'provider_type', 'organization_id', 'organization_name', 'admin_zj_name', 'provider_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        now_date = get_cur_time()
        if condition.get('order_date'):
            order_before_date = condition['order_date'][0][0:10] + \
                "T00:00:00.00Z"
            order_after_date = condition['order_date'][1][0:10] + \
                "T23:59:59.00Z"

            _filter.match_bill(
                (C('order_date') > as_date(order_before_date))
                & (C('order_date') < as_date(order_after_date))
            )

        if condition.get('service_date'):
            service_before_date = condition['service_date'][0][0:10] + \
                "T00:00:00.00Z"
            service_after_date = condition['service_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('service_date') > as_date(service_before_date))
                & (C('service_date') < as_date(service_after_date))
            )

        if condition.get('pay_date'):
            pay_before_date = condition['pay_date'][0][0:10] + \
                "T00:00:00.00Z"
            pay_after_date = condition['pay_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('pay_date') > as_date(pay_before_date))
                & (C('pay_date') < as_date(pay_after_date))
            )
        # 查询长者数据
        elder_ids = []
        elder_data = {}
        if condition.get('purchaser_name') or condition.get('purchaser_id_card'):
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1')
                                     & (C('personnel_info.personnel_category') == '长者')
                                     & (C('name').like(values['purchaser_name']))
                                     & (C('id_card').like(values['purchaser_id_card'])))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])

        # 查询服务商数据
        provider_ids = []
        provider_data = {}
        _filter_provider = MongoBillFilter()
        _filter_provider.match_bill((C('personnel_type') == '2'))
        if condition.get('provider_type'):
            _filter_provider.match_bill(
                (C('organization_info.personnel_category') == values['provider_type']))
        # if condition.get('need_fp'):
        #     _filter_provider.match_bill(
        #         (C('serviceProvider.organization_info.personnel_category') == '服务商'))
        if condition.get('org_name'):
            _filter_provider.match_bill(
                (C('name').like(values['org_name'])))
        _filter_provider.project({
            'qualification_info': 0,
            'main_account': 0,
            **get_common_project()
        })
        res_provider = self.query(
            _filter_provider, 'PT_User')
        if len(res_provider) > 0:
            for provider in res_provider:
                provider_data[provider['id']] = provider
                provider_ids.append(provider['id'])
        if condition.get('purchaser_name') or condition.get('purchaser_id_card'):
            _filter.match(
                (C('purchaser_id').inner(elder_ids))
            )
        if condition.get('provider_type'):
            _filter.match(
                (C('service_provider_id').inner(provider_ids))
            )
        _filter.match_bill((C('id') == values['id'])
                           & (C('status') == values['status'])
                           & (C('organization_id') == values['organization_id'])
                           & (C('organization_id').inner(order_ids))
                           & (C('order_code').like(values['transation_code'])))
        _filter.add_fields({
            'create_date': self.ao.date_to_string('$create_date'),
            'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill((C('purchaser_id') == values['purchaser_id'])
                        & (C('service_provider').like(values['organization_name']))
                        & (C('service_provider_id').inner(values['service_provider_id'])))\
            .add_fields({
                'now_date': now_date
            })\
            .sort({'order_date': -1})\
            .project({
                "admin": 0,
                "admin_gl": 0,
                "adminZj": 0,
                **get_common_project()
            })
        res = self.page_query(
            _filter, 'PT_Service_Order', page, count)
        order_ids = []
        purchaser_ids = []
        for i, x in enumerate(res['result']):
            if 'service_provider_id' in res['result'][i] and res['result'][i]['service_provider_id'] in provider_data.keys():
                res['result'][i]['serviceProvider'] = provider_data[res['result']
                                                                    [i]['service_provider_id']]
                res['result'][i]['service_provider'] = provider_data[res['result']
                                                                     [i]['service_provider_id']]['name']
            order_ids.append(res['result'][i]['id'])
            if condition.get('purchaser_name') or condition.get('purchaser_id_card'):
                if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] in elder_data.keys():
                    res['result'][i]['purchaser_info'] = elder_data[res['result']
                                                                    [i]['purchaser_id']]
                    res['result'][i]['purchaser'] = elder_data[res['result']
                                                               [i]['purchaser_id']]['name']
                    res['result'][i]['purchaser_id_card'] = elder_data[res['result']
                                                                       [i]['purchaser_id']]['id_card']
                    if 'address' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                        res['result'][i]['purchaser_address'] = elder_data[res['result']
                                                                           [i]['purchaser_id']]['personnel_info']['address']
            else:
                if 'purchaser_id' in res['result'][i]:
                    purchaser_ids.append(res['result'][i]['purchaser_id'])
        # 查询服务记录表数据
        record_data = {}
        _filter_record = MongoBillFilter()
        _filter_record.match_bill(C('order_id').inner(order_ids))\
            .project({**get_common_project()})
        res_record = self.query(
            _filter_record, 'PT_Service_Record')
        if len(res_record) > 0:
            for record in res_record:
                if record['order_id'] in record_data:
                    record_data[record['order_id']].append(record)
                else:
                    record_data[record['order_id']] = [record]
        for i, x in enumerate(res['result']):
            if 'id' in res['result'][i] and res['result'][i]['id'] in record_data.keys():
                if 'start_date' in record_data[res['result'][i]['id']][0]:
                    res['result'][i]['service_date'] = record_data[res['result']
                                                                   [i]['id']][0]['start_date']
                if 'end_date' in record_data[res['result'][i]['id']][-1]:
                    res['result'][i]['service_end_date'] = record_data[res['result']
                                                                       [i]['id']][-1]['end_date']
        if len(purchaser_ids) > 0:
            t11 = time.time()
            # 查询人员列表
            person_data = {}
            _filter_person = MongoBillFilter()
            _filter_person.match_bill((C('personnel_type') == '1')
                                      & (C('id').inner(purchaser_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_person = self.query(
                _filter_person, 'PT_User')
            if len(res_person) > 0:
                for person in res_person:
                    person_data[person['id']] = person

            t12 = time.time()
          # print('最后查询人员数据》》', t12-t11)
            for i, x in enumerate(res['result']):
                if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] in person_data.keys():
                    res['result'][i]['purchaser_info'] = person_data[res['result']
                                                                     [i]['purchaser_id']]
                    res['result'][i]['purchaser'] = person_data[res['result']
                                                                [i]['purchaser_id']]['name']
                    res['result'][i]['purchaser_id_card'] = person_data[res['result']
                                                                        [i]['purchaser_id']]['id_card']
                    if 'address' in person_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                        res['result'][i]['purchaser_address'] = person_data[res['result']
                                                                            [i]['purchaser_id']]['personnel_info']['address']
        return res

    def get_service_order_fwddfp_list(self, org_ids, condition, page=None, count=None):
        '''查询服务订单列表--服务商服务分派'''
        keys = ['id', 'purchaser_name', 'purchaser_id_card', 'service_provider_id', 'purchaser_id', 'status',
                'order_date', 'service_date', 'pay_date', 'order_state', 'origin_product', 'need_fp', 'transation_code',
                'provider_type', 'organization_id', 'organization_name', 'admin_zj_name', 'provider_name', 'is_assign', 'service_person']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        now_date = get_cur_time()
        if condition.get('order_date'):
            order_before_date = condition['order_date'][0][0:10] + \
                "T00:00:00.00Z"
            order_after_date = condition['order_date'][1][0:10] + \
                "T23:59:59.00Z"

            _filter.match_bill(
                (C('order_date') > as_date(order_before_date))
                & (C('order_date') < as_date(order_after_date))
            )

        if condition.get('service_date'):
            service_before_date = condition['service_date'][0][0:10] + \
                "T00:00:00.00Z"
            service_after_date = condition['service_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('service_date') > as_date(service_before_date))
                & (C('service_date') < as_date(service_after_date))
            )

        if condition.get('pay_date'):
            pay_before_date = condition['pay_date'][0][0:10] + \
                "T00:00:00.00Z"
            pay_after_date = condition['pay_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('pay_date') > as_date(pay_before_date))
                & (C('pay_date') < as_date(pay_after_date))
            )

        # 查询所有下级行政区划
        admin_first_ids = N()
        if condition.get('admin_zj_name'):
            admin_first_ids = []
            _filter_admin_first = MongoBillFilter()
            _filter_admin_first.match_bill(C('name').like(values['admin_zj_name']))\
                .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                .unwind('admin_info')\
                .match(C('admin_info.bill_status') == 'valid')\
                .project({
                    '_id': 0,
                    'admin_id': '$admin_info.id',
                    'admin_name': '$admin_info.name'
                })
            query_res = self.query(_filter_admin_first,
                                   'PT_Administration_Division')
            if len(query_res) > 0:
                for admin in query_res:
                    admin_first_ids.append(admin['admin_id'])
        # 查询长者数据
        elder_ids = []
        elder_data = {}
        if condition.get('purchaser_name') or condition.get('admin_zj_name'):
            t1 = time.time()
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1')
                                     & (C('personnel_info.personnel_category') == '长者')
                                     & (C('name').like(values['purchaser_name']))
                                     & (C('admin_area_id').inner(admin_first_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])
            t2 = time.time()
          # print('查询长者数据》》', t2-t1)
        # t3 = time.time()
        # 查询工作人员数据
        worker_ids = []
        worker_data = {}
        servicer_record_ids = []
        if condition.get('service_person'):
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('personnel_type') == '1')
                                      & (C('personnel_info.personnel_category') == '工作人员') & (C('name').like(values['service_person'])))\
                .project({'login_info': 0, **get_common_project()})
            res_worker = self.query(
                _filter_worker, 'PT_User')
            if len(res_worker) > 0:
                for worker in res_worker:
                    worker_data[worker['id']] = worker
                    worker_ids.append(worker['id'])
            # 查询服务记录表数据
            _filter_servicer_record = MongoBillFilter()
            _filter_servicer_record.match_bill(C('servicer_id').inner(worker_ids))\
                .project({**get_common_project()})
            res_servicer_record = self.query(
                _filter_servicer_record, 'PT_Service_Record')
            if len(res_servicer_record) > 0:
                for servicer_record in res_servicer_record:
                    if 'order_id' in servicer_record:
                        servicer_record_ids.append(servicer_record['order_id'])
        # t4 = time.time()
        # print('查询工作人员数据》》', t4-t3)
        t5 = time.time()
        # 查询服务商数据
        provider_ids = []
        provider_data = {}
        _filter_provider = MongoBillFilter()
        _filter_provider.match_bill(
            (C('personnel_type') == '2')
            & (C('organization_info.personnel_category') == values['provider_type'])
            & (C('name').like(values['organization_name']))
        )
        if condition.get('need_fp'):
            _filter_provider.match_bill(
                (C('organization_info.personnel_category') == '服务商'))
        _filter_provider.project({
            'qualification_info': 0,
            'main_account': 0,
            **get_common_project()
        })
        res_provider = self.query(
            _filter_provider, 'PT_User')
        if len(res_provider) > 0:
            for provider in res_provider:
                provider_data[provider['id']] = provider
                provider_ids.append(provider['id'])

        t6 = time.time()
      # print('查询服务商数据》》', t6-t5)
        t7 = time.time()
        if condition.get('need_fp'):
            _filter.match(
                (C('pay_state') == '已支付')
            )
        if condition.get('purchaser_name') or condition.get('admin_zj_name'):
            _filter.match(
                (C('purchaser_id').inner(elder_ids))
            )
        if condition.get('service_person'):
            _filter.match(
                (C('id').inner(servicer_record_ids))
            )
        _filter.match_bill((C('id') == values['id'])
                           & (C('status') == values['status'])
                           & (C('organization_id') == values['organization_id'])
                           & (C('organization_id').inner(org_ids))
                           & (C('organization_id').inner(provider_ids))
                           & (C('order_code').like(values['transation_code'])))
        _filter.add_fields({
            'create_date': self.ao.date_to_string('$create_date'),
            'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill((C('purchaser_id') == values['purchaser_id'])
                        & (C('service_provider_id').inner(values['service_provider_id'])))\
            .add_fields({
                'admin_name': self.ao.array_elemat('$admin.name', 0),
                'admin_zj_name': self.ao.array_elemat('$adminZj.name', 0),
                'now_date': now_date
            })\
            .add_fields({
                'assign': self.ao.switch([self.ao.case(((F('assign_state') == '已分派')), '已分派')], '未分派'),
            })\
            .match((C('assign') == values['is_assign']))\
            .sort({'order_date': -1})\
            .project({
                "admin": 0,
                "admin_gl": 0,
                "adminZj": 0,
                **get_common_project()
            })
        res = self.page_query(
            _filter, 'PT_Service_Order', page, count)
        t8 = time.time()
      # print('查询主表》》', t8-t7)
        order_ids = []
        # 长者及工作人员数组
        purchaser_ids = []
        purchaser_admin_ids = []
        for i, x in enumerate(res['result']):
            if 'service_provider_id' in res['result'][i] and res['result'][i]['service_provider_id'] in provider_data.keys():
                res['result'][i]['serviceProvider'] = provider_data[res['result']
                                                                    [i]['service_provider_id']]
                res['result'][i]['service_provider'] = provider_data[res['result']
                                                                     [i]['service_provider_id']]['name']
            order_ids.append(res['result'][i]['id'])
            if condition.get('purchaser_name'):
                if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] in elder_data.keys():
                    res['result'][i]['purchaser_info'] = elder_data[res['result']
                                                                    [i]['purchaser_id']]
                    res['result'][i]['purchaser'] = elder_data[res['result']
                                                               [i]['purchaser_id']]['name']
                    res['result'][i]['purchaser_id_card'] = elder_data[res['result']
                                                                       [i]['purchaser_id']]['id_card']
                    res['result'][i]['purchaser_address'] = elder_data[res['result']
                                                                       [i]['purchaser_id']]['personnel_info']['address']
                    res['result'][i]['purchaser_admin'] = elder_data[res['result']
                                                                     [i]['purchaser_id']]['admin_area_id']
                    purchaser_admin_ids.append(
                        elder_data[res['result'][i]['purchaser_id']]['admin_area_id'])
            else:
                if 'purchaser_id' in res['result'][i]:
                    purchaser_ids.append(res['result'][i]['purchaser_id'])

        t9 = time.time()
        # 查询服务记录表数据
        record_data = {}
        _filter_record = MongoBillFilter()
        _filter_record.match_bill(C('order_id').inner(order_ids))\
            .project({**get_common_project()})
        res_record = self.query(
            _filter_record, 'PT_Service_Record')
        if len(res_record) > 0:
            for record in res_record:
                if record['order_id'] in record_data:
                    record_data[record['order_id']].append(record)
                else:
                    record_data[record['order_id']] = [record]

        t10 = time.time()
      # print('查询服务记录表数据》》', t10-t9)
        for i, x in enumerate(res['result']):
            if 'id' in res['result'][i] and res['result'][i]['id'] in record_data.keys():
                if 'start_date' in record_data[res['result'][i]['id']][0]:
                    res['result'][i]['service_date'] = record_data[res['result']
                                                                   [i]['id']][0]['start_date']
                if 'end_date' in record_data[res['result'][i]['id']][-1]:
                    res['result'][i]['service_end_date'] = record_data[res['result']
                                                                       [i]['id']][-1]['end_date']
                if 'servicer_id' in record_data[res['result'][i]['id']][0]:
                    res['result'][i]['servicer_id'] = record_data[res['result']
                                                                  [i]['id']][0]['servicer_id']
                if condition.get('service_person') and 'servicer_id' in res['result'][i] and res['result'][i]['servicer_id'] in worker_data.keys():
                    res['result'][i]['service_person'] = worker_data[res['result']
                                                                     [i]['servicer_id']]['name']
                else:
                    if 'servicer_id' in res['result'][i]:
                        purchaser_ids.append(res['result'][i]['servicer_id'])
      # print('长者列表》》', len(purchaser_ids))
        if len(purchaser_ids) > 0:
            t11 = time.time()
            # 查询人员列表
            person_data = {}
            _filter_person = MongoBillFilter()
            _filter_person.match_bill((C('personnel_type') == '1')
                                      & (C('id').inner(purchaser_ids)))\
                .project({'login_info': 0, **get_common_project()})
            res_person = self.query(
                _filter_person, 'PT_User')
            if len(res_person) > 0:
                for person in res_person:
                    person_data[person['id']] = person

            t12 = time.time()
          # print('最后查询人员数据》》', t12-t11)
            for i, x in enumerate(res['result']):
                if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] in person_data.keys():
                    res['result'][i]['purchaser_info'] = person_data[res['result']
                                                                     [i]['purchaser_id']]
                    res['result'][i]['purchaser'] = person_data[res['result']
                                                                [i]['purchaser_id']]['name']
                    res['result'][i]['purchaser_id_card'] = person_data[res['result']
                                                                        [i]['purchaser_id']]['id_card']
                    res['result'][i]['purchaser_address'] = person_data[res['result']
                                                                        [i]['purchaser_id']]['personnel_info']['address']
                    res['result'][i]['purchaser_admin'] = person_data[res['result']
                                                                      [i]['purchaser_id']]['admin_area_id']
                    purchaser_admin_ids.append(
                        person_data[res['result'][i]['purchaser_id']]['admin_area_id'])
                if 'servicer_id' in res['result'][i] and res['result'][i]['servicer_id'] in person_data.keys():
                    res['result'][i]['service_person'] = person_data[res['result']
                                                                     [i]['servicer_id']]['name']
        if len(purchaser_admin_ids) > 0:
            # 查询行政区划数据
            admin_data = {}
            _filter_admin = MongoBillFilter()
            _filter_admin.match_bill((C('id').inner(purchaser_admin_ids)))\
                .lookup_bill('PT_Administration_Division', 'parent_id', 'id', 'admin_gl')\
                .lookup_bill('PT_Administration_Division', 'admin_gl.parent_id', 'id', 'adminZj')\
                .add_fields({
                    'admin_zj_name': self.ao.array_elemat('$adminZj.name', 0)
                })\
                .project({'_id': 0, 'id': 1, 'name': 1, 'admin_zj_name': 1})
            res_admin = self.query(
                _filter_admin, 'PT_Administration_Division')
            if len(res_admin) > 0:
                for admin in res_admin:
                    admin_data[admin['id']] = admin
            for i, x in enumerate(res['result']):
                if 'purchaser_admin' in res['result'][i] and res['result'][i]['purchaser_admin'] in admin_data.keys():
                    if 'admin_zj_name' in admin_data[res['result'][i]['purchaser_admin']].keys():
                        res['result'][i]['admin_zj_name'] = admin_data[res['result']
                                                                       [i]['purchaser_admin']]['admin_zj_name']
                    else:
                        res['result'][i]['admin_zj_name'] = admin_data[res['result']
                                                                       [i]['purchaser_admin']]['name']
        return res

    def get_service_order_list(self, order_ids, condition, page=None, count=None):
        '''查询服务订单列表'''
        keys = ['id', 'purchaser_name', 'purchaser_id_card', 'service_provider_id', 'purchaser_id', 'status',
                'order_date', 'service_date', 'pay_date', 'order_state', 'origin_product', 'need_fp', 'transation_code', 'provider_type', 'organization_id', 'organization_name', 'admin_zj_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        now_date = get_cur_time()
        if condition.get('order_date'):
            order_before_date = condition['order_date'][0][0:10] + \
                "T00:00:00.00Z"
            order_after_date = condition['order_date'][1][0:10] + \
                "T23:59:59.00Z"

            _filter.match_bill(
                (C('order_date') > as_date(order_before_date))
                & (C('order_date') < as_date(order_after_date))
            )

        if condition.get('service_date'):
            service_before_date = condition['service_date'][0][0:10] + \
                "T00:00:00.00Z"
            service_after_date = condition['service_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('service_date') > as_date(service_before_date))
                & (C('service_date') < as_date(service_after_date))
            )

        if condition.get('pay_date'):
            pay_before_date = condition['pay_date'][0][0:10] + \
                "T00:00:00.00Z"
            pay_after_date = condition['pay_date'][1][0:10] + \
                "T23:59:59.00Z"
            _filter.match_bill(
                (C('pay_date') > as_date(pay_before_date))
                & (C('pay_date') < as_date(pay_after_date))
            )

        _filter.match_bill((C('id') == values['id'])
                           & (C('status') == values['status'])
                           & (C('organization_id') == values['organization_id'])
                           & (C('organization_id').inner(order_ids))
                           & (C('order_code').like(values['transation_code'])))\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'serviceProvider')
        if condition.get('need_fp'):
            _filter.match_bill(
                (C('pay_state') == '已支付')
                & (C('serviceProvider.organization_info.personnel_category') == '服务商')
            )
        _filter.lookup_bill('PT_Service_Record', 'id', 'order_id', 'record_info')\
            .lookup_bill('PT_User', 'record_info.servicer_id', 'id', 'servicer_person_info')\
            .inner_join_bill('PT_User', 'purchaser_id', 'id', 'purchaser')\
            .match_bill((C('purchaser.name').like(values['purchaser_name']))
                        & (C('purchaser.id_card').like(values['purchaser_id_card']))
                        )\
            .lookup_bill('PT_Administration_Division', 'purchaser.admin_area_id', 'id', 'admin')\
            .lookup_bill('PT_Administration_Division', 'admin.parent_id', 'id', 'admin_gl')\
            .lookup_bill('PT_Administration_Division', 'admin_gl.parent_id', 'id', 'adminZj')\
            .add_fields({
                'admin_name': self.ao.array_elemat('$admin.name', 0),
                'admin_zj_name': self.ao.array_elemat('$adminZj.name', 0)
            })\
            .add_fields({
                'service_provider': '$serviceProvider.name',
                'service_person': self.ao.array_elemat("$servicer_person_info.name", 0),
                'service_date': self.ao.array_elemat("$record_info.start_date", 0),
                'service_end_date': self.ao.array_elemat("$record_info.end_date", -1),
                'purchaser': '$purchaser.name',
                'purchaser_id_card': '$purchaser.id_card',
                'purchaser_address': '$purchaser.personnel_info.address',
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill((C('purchaser_id') == values['purchaser_id'])
                        & (C('serviceProvider.organization_info.personnel_category') == values['provider_type'])
                        & (C('service_provider').like(values['organization_name']))
                        & (C('service_provider_id').inner(values['service_provider_id'])))\
            .lookup_bill('PT_User', 'operator_id', 'id', 'operator')\
            .add_fields({
                'now_date': now_date
            })\
            .sort({'order_date': -1})\
            .project({'_id': 0,
                      'serviceProvider._id': 0,
                      'purchaser._id': 0,
                      'bill_status': 0,
                      'GUID': 0,
                      'modify_date': 0,
                      'valid_bill_id': 0,
                      'version': 0,
                      'record_info._id': 0,
                      'servicer_person_info': 0,
                      'operator._id': 0,
                      "admin": 0,
                      "admin_gl": 0,
                      "adminZj": 0})
        res = self.page_query(
            _filter, 'PT_Service_Order', page, count)
        # # 拼接订单内容
        # if res['result']:
        #     for result in res['result']:
        #         if result.get('service_item'):
        #             order_list = []
        #             # number = 0
        #             for item in result.get('service_item'):
        #                 # number += 1
        #                 _filter_item = MongoBillFilter()
        #                 _filter_item.match_bill(C('id') == item.get(
        #                     'service_item')).project({'_id': 0})
        #                 res_item = self.query(_filter_item, 'PT_Service_Item')
        #                 if res_item:
        #                     item['service_name'] = res_item[0]['name']
        #                 else:
        #                     item['service_name'] = ''
        #                 if item.get('service_options'):
        #                     option_list = []
        #                     for option in item.get('service_options'):
        #                         if option.get('name') and option.get('value'):
        #                             option_content = ''.join(
        #                                 (option.get('name'), '(', str(option.get('value')), ')'))
        #                             option_list.append(option_content)
        #                         else:
        #                             option_list.append(' ')
        #                     if not item.get('count'):
        #                         item['count'] = 1
        #                     item_content = ''.join(
        #                         (item['service_name'], '-', ('-'.join(option_list))))+'*'+str(item.get('count'))
        #                     order_list.append(item_content)
        #             # print(order_list)
        #             order_list = '、'.join(order_list)
        #             result['order_list'] = order_list
        return res

    def update_service_order(self, service_order):
        ''' 新增/修改服务订单'''
        res = 'Fail'
        if service_order.get('order_date'):
            service_order['order_date'] = as_date(service_order['order_date'])
        if service_order.get('service_date'):
            service_order['service_date'] = as_date(
                service_order['service_date'])
        if service_order.get('pay_date'):
            service_order['pay_date'] = as_date(service_order['pay_date'])

        def process_func(db):
            nonlocal res
            if 'id' in service_order.keys():
                bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                           TypeId.serviceOrder.value, service_order, 'PT_Service_Order')
                if bill_id:
                    res = 'Success'
            else:
                service_order['order_code'] = get_serial_number(
                    db, SerialNumberType.order.value)
                bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                           TypeId.serviceOrder.value, service_order, 'PT_Service_Order')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def service_book(self, serviceItems, remark=''):
        '''服务预定(根据选择的服务项目新增一个服务订单)'''
        res = 'Fail'
        order_detail = []
        order_fee = 0
        for item in serviceItems:
            order_detail.append(
                {"id": item['id'], "item_content": item['item_content'], "remark": item['remark']})
            order_fee = order_fee + float(item['item_fee'])

        def process_func(db):
            nonlocal res
            nonlocal order_detail
            nonlocal order_fee
            order = Order(get_current_user_id(
                self.session), serviceItems[0]['service_provider_id'], order_fee, order_detail, remark)
            order_info = get_info(order.to_dict(), self.session)
            result = insert_data(db, 'PT_Service_Order', order_info)
            if result:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
    '''入参是服务订单明细对象'''

    def service_consult(self, order_id):
        '''服务协商(传入服务订单id，新增交易信息数据，并与订单绑定)'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            # 查询服务订单信息
            order_data = find_data(db, 'PT_Service_Order', {
                'id': order_id})
            if len(order_data) > 0:
                item_content = ""
                for detail in order_data[0]['detail']:
                    item_content = item_content + detail['item_content'] + " "
                # print(item_content, 'item_content')
                # 新增交易信息
                transaction = {'transaction_code': get_code(), 'party_a': order_data[0]['purchaser_id'],
                               'party_b': order_data[0]['service_provider_id'], 'transaction_date': get_cur_time().strftime("%Y-%m-%d %H:%M:%S"),
                               'transaction_money': order_data[0]['order_fee'], 'abstract': item_content,
                               'transaction_state': '进行中', 'transaction_content': item_content}
                # print(transaction, 'transaction')
                transation_id = self.transaction_service.add_transaction(
                    transaction)
                # 交易id绑定服务订单
                order_data[0]['transation_id'] = transation_id
                order_data[0]['status'] = Status.bill_valid.value
                result = update_data(db, 'PT_Service_Order',
                                     order_data[0], {'id': order_id})
                if result:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def change_service_order_status(self, order_id, new_status):
        '''修改服务订单状态'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            data = {}
            data['status'] = new_status
            result = update_data(db, 'PT_Service_Order',
                                 data, {'id': order_id})
            if result:
                res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def service_pay(self, serviceBook):
        '''服务支付'''
        res = 'Fail'
        # 支付功能待定
        return res

    def get_service_return_visit_list(self, org_list, condition, page=None, count=None):
        if 'date_range' in list(condition.keys()):
            condition['visit_begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['visit_end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        if 'end_date' in list(condition.keys()):
            condition['service_begin_date'] = condition['end_date'][0][0:10] + \
                "T00:00:00.00Z"
            condition['service_end_date'] = condition['end_date'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ['elder_name', 'provider_name', 'id', 'service_begin_date', 'service_end_date',
                'visit_status', 'visit_begin_date', 'visit_end_date', 'id_card', 'visit_name', 'service_satisfaction', 'record_code']
        values = self.get_value(condition, keys)
        t7 = datetime.datetime.now()
        # 查询长者数据
        elder_ids = []
        elder_data = {}
        _filter_elder = MongoBillFilter()
        _filter_elder.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者') & (C('name').like(values['elder_name'])) & (C('id_card').like(values['id_card'])))\
            .project({'qualification_info': 0, 'main_account': 0,  **get_common_project()})
        res_elder = self.query(
            _filter_elder, 'PT_User')
        if len(res_elder) > 0:
            for elder in res_elder:
                elder_data[elder['id']] = elder
                elder_ids.append(elder['id'])

        t8 = datetime.datetime.now()
      # print('t8-t7>>', t8-t7)
        t1 = datetime.datetime.now()
        # 查询订单表数据
        order_ids = []
        order_data = {}
        _filter_order = MongoBillFilter()
        _filter_order.match_bill((C('purchaser_id').inner(elder_ids)))\
            .project({**get_common_project()})
        res_order = self.query(
            _filter_order, 'PT_Service_Order')
        if len(res_order) > 0:
            for order in res_order:
                order_data[order['id']] = order
                order_ids.append(order['id'])

        t2 = datetime.datetime.now()
      # print('t2-t1>>', t2-t1)
        t3 = datetime.datetime.now()
        # 查询所有人数据（贼慢）
        visit_ids = []
        user_data = {}
        _filter_user = MongoBillFilter()
        if 'visit_name' in condition.keys():
            _filter_user.match_bill(C('name').like(values['visit_name']))
        _filter_user.match_bill(C('personnel_info.personnel_category') == '工作人员')\
            .project({'login_info': 0, **get_common_project()})
        res_user = self.query(
            _filter_user, 'PT_User')
        if len(res_user) > 0:
            for user in res_user:
                user_data[user['id']] = user
                visit_ids.append(user['id'])
        t4 = datetime.datetime.now()
      # print('t4-t3>>', t4-t3)
        t5 = datetime.datetime.now()
        # 查询服务商数据
        provider_ids = []
        provider_data = {}
        _filter_provider = MongoBillFilter()
        _filter_provider.match_bill((C('personnel_type') == '2') & (C('name').like(values['provider_name'])))\
            .project({'qualification_info': 0, 'main_account': 0,  **get_common_project()})
        res_provider = self.query(
            _filter_provider, 'PT_User')
        if len(res_provider) > 0:
            for provider in res_provider:
                provider_data[provider['id']] = provider
                provider_ids.append(provider['id'])

        t6 = datetime.datetime.now()
      # print('t6-t5>>', t6-t5)
        t9 = datetime.datetime.now()
        # 查询产品数据
        product_ids = []
        product_data = {}
        _filter_product = MongoBillFilter()
        _filter_product.match_bill((C('organization_id').inner(provider_ids)))\
            .project({**get_common_project()})
        res_product = self.query(
            _filter_product, 'PT_Service_Product')
        if len(res_product) > 0:
            for product in res_product:
                product_data[product['id']] = product
                product_ids.append(product['id'])

        t10 = datetime.datetime.now()
      # print('t10-t9>>', t10-t9)

        t11 = datetime.datetime.now()
        _filter = MongoBillFilter()
        if 'visit_name' in condition.keys():
            _filter.match_bill(C('visit_id').inner(visit_ids))
        _filter.match_bill((C('id') == values['id'])
                           & (C('status') == '已完成')
                           & (C('record_code').like(values['record_code']))
                           & (C('service_satisfaction') == values['service_satisfaction'])
                           & (C('visit_status') == values['visit_status'])
                           & (C('organization_id').inner(org_list))
                           & (C('service_product_id').inner(product_ids))
                           & (C('order_id').inner(order_ids))
                           & (C('end_date') >= as_date(values['service_begin_date']))
                           & (C('end_date') <= as_date(values['service_end_date']))
                           & (C('visit_date') >= as_date(values['visit_begin_date']))
                           & (C('visit_date') <= as_date(values['visit_end_date'])))\
            .sort({'create_date': -1})\
            .sort({'visit_status': -1})\
            .project({'_id': 0, 'id': 1, 'order_id': 1, 'servicer_id': 1, 'visit_id': 1, 'service_product_id': 1, 'record_code': 1, 'end_date': 1,  'visit_date': 1, 'visit_status': 1, 'service_satisfaction': 1, 'visit_satisfaction': 1, 'other_satisfaction': 1})
        res = self.page_query(
            _filter, "PT_Service_Record", page, count)
        t12 = datetime.datetime.now()
      # print('t12-t11>>', t12-t11)
        t13 = datetime.datetime.now()
        for i, x in enumerate(res['result']):
            if 'order_id' in res['result'][i] and res['result'][i]['order_id'] in order_data.keys():
                res['result'][i]['order_info'] = order_data[res['result'][i]['order_id']]
                res['result'][i]['create_date'] = order_data[res['result']
                                                             [i]['order_id']]['create_date']

            if 'order_info' in res['result'][i] and res['result'][i]['order_info']['purchaser_id'] in elder_data.keys():
                # res['result'][i]['elder_info'] = elder_data[res['result'][i]['order_info']['purchaser_id']]
                res['result'][i]['elder_name'] = elder_data[res['result']
                                                            [i]['order_info']['purchaser_id']]['name']
                res['result'][i]['elder_sex'] = elder_data[res['result'][i]
                                                           ['order_info']['purchaser_id']]['personnel_info']['sex']
                res['result'][i]['elder_telephone'] = elder_data[res['result'][i]
                                                                 ['order_info']['purchaser_id']]['personnel_info']['telephone']

            if 'servicer_id' in res['result'][i] and res['result'][i]['servicer_id'] in user_data.keys():
                # res['result'][i]['servicer_info'] = user_data[res['result'][i]['servicer_id']]
                res['result'][i]['servicer_name'] = user_data[res['result']
                                                              [i]['servicer_id']]['name']

            if 'visit_id' in res['result'][i] and res['result'][i]['visit_id'] in user_data.keys():
                # res['result'][i]['visit_info'] = user_data[res['result'][i]['visit_id']]
                res['result'][i]['visit_name'] = user_data[res['result']
                                                           [i]['visit_id']]['name']

            if 'service_product_id' in res['result'][i] and res['result'][i]['service_product_id'] in product_data.keys():
                res['result'][i]['product_info'] = product_data[res['result']
                                                                [i]['service_product_id']]
                res['result'][i]['product_name'] = product_data[res['result']
                                                                [i]['service_product_id']]['name']

            if 'product_info' in res['result'][i] and res['result'][i]['product_info']['organization_id'] in provider_data.keys():
                # res['result'][i]['provider_info'] = provider_data[res['result'][i]['product_info']['organization_id']]
                res['result'][i]['provider_name'] = provider_data[res['result']
                                                                  [i]['product_info']['organization_id']]['name']
            # del res['result'][i]['order_info']
            # del res['result'][i]['product_info']

        t14 = datetime.datetime.now()
      # print('t14-t13>>', t14-t13)
        return res

    def update_service_return_visit(self, condition):

        # print(condition,6666)
        res = 'Fail'
        # condition['id]是个数组[]
        if 'id' not in condition:
            return '没有可操作的工单！'
        if 'service_satisfaction' not in condition:
            return '请选择服务满意度！'
        # if 'visit_satisfaction' not in condition:
        #     return '请选择回访满意度！'

        visit_id = get_user_id_or_false(self.session)

        if visit_id == False:
            return '请先登录'

        _filter_main = MongoBillFilter()
        _filter_main.match_bill(
            (C('id').inner(condition['id']))).project({'_id': 0})
        res_main = self.query(
            _filter_main, "PT_Service_Record")

        if len(res_main) == 0:
            return '没有可操作的工单！'

        update_data = []
        for main in res_main:
            if 'visit_status' in main and main['visit_status'] == '已回访':
                return '工单号' + main['record_code'] + '此工单已回访！'

            # 保存回访数据
            main['service_satisfaction'] = condition['service_satisfaction']
            # res_main[0]['visit_satisfaction'] = condition['visit_satisfaction']
            main['visit_status'] = '已回访'
            main['visit_date'] = get_cur_time()
            main['visit_id'] = visit_id

            if 'other_satisfaction' in condition:
                main['other_satisfaction'] = condition['other_satisfaction']
            update_data.append(main)
        bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                   TypeId.serviceRecord.value, update_data, 'PT_Service_Record')
        if bill_id:
            res = 'Success'
        return res

    def get_service_evaluate_list(self, org_list, condition, page=None, count=None):
        '''查询服务评价列表'''
        keys = ['elder_name', 'service_provider_id', 'provider_name',
                'date_range', 'id', 'begin_date', 'end_date', 'is_evaluate']

        # 初始化数据
        elder_ids = []
        temp_elder = {}
        provider_ids = []
        temp_provider = {}
        order_ids = []
        product_ids = []
        temp_product = {}
        order_elder_ids = []
        temp_order_elder = {}
        temp_order_create = {}
        product_organization_ids = []
        temp_product_organization = {}

        # 时间范围条件查询
        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('order_id') != None)
                           & (C('servicer_id') != None)
                           & (C('service_product_id') != None)
                           & (C('create_date') >= as_date(values['begin_date']))
                           & (C('create_date') <= as_date(values['end_date'])))

        # 长者姓名条件查询与服务商名字条件查询[PT_User]
        if 'elder_name' in condition or 'provider_name' in condition:
            _filter_name = MongoBillFilter()
            _filter_name.match_bill(
                ((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '长者') & (
                    C('name').like(values['elder_name'])))
                |
                ((C('personnel_type') == '2') & (
                    C('name').like(values['provider_name'])))
            )\
                .project({'_id': 0})
            res_name = self.query(
                _filter_name, "PT_User")

            # 循环获取要过滤的id
            for item in res_name:
                if 'name' not in item:
                    item['name'] = ''
                if item['personnel_type'] == '1':
                    elder_ids.append(item['id'])
                    temp_elder[item['id']] = item['name']
                else:
                    provider_ids.append(item['id'])
                    temp_provider[item['id']] = item['name']

            # 服务商id可以直接过滤
            if 'provider_name' in condition:
                # 严重警告，骚操作！！！
                if len(provider_ids) == 0:
                    _filter.match_bill(C("id") == 'invalid')
                    # 这里返回是没有数据的
                    return self.page_query(
                        _filter, "PT_Service_Record", page, count)
                else:
                    # 产品
                    _filter_product = MongoBillFilter()
                    _filter_product.match_bill((C('organization_id').inner(provider_ids)))\
                        .project({'_id': 0})
                    res_product = self.query(
                        _filter_product, "PT_Service_Product")

                    for item in res_product:
                        if 'name' not in item:
                            item['name'] = ''
                        temp_product[item['id']] = item['name']
                        product_ids.append(item['id'])
                        product_organization_ids.append(
                            item['organization_id'])
                        temp_product_organization[item['id']
                                                  ] = item['organization_id']

                    # 对记录表下订单的条件，达到过滤服务商的作用
                    _filter.match_bill(
                        C("service_product_id").inner(product_ids))

            # 此时有分支，如果有长者姓名查询，那只能在这里查然后在最后match，会慢一点
            if 'elder_name' in condition:
                # 严重警告，骚操作！！！
                if len(elder_ids) == 0:
                    _filter.match_bill(C("id") == 'invalid')
                    # 这里返回是没有数据的
                    return self.page_query(
                        _filter, "PT_Service_Record", page, count)
                else:
                    # 暂时从订单表获取包含长者数组的信息
                    _filter_order = MongoBillFilter()
                    _filter_order.match_bill((C('purchaser_id').inner(elder_ids)))\
                        .project({'_id': 0})
                    res_order = self.query(
                        _filter_order, "PT_Service_Order")

                    for item in res_order:
                        order_ids.append(item['id'])
                        order_elder_ids.append(item['purchaser_id'])
                        temp_order_elder[item['id']] = item['purchaser_id']
                        temp_order_create[item['id']] = item['create_date']

                    # 对记录表下订单的条件，达到过滤长者的作用
                    _filter.match_bill(C("order_id").inner(order_ids))

        #  评论有条件
        if 'is_evaluate' in condition and condition['is_evaluate'] in ['已评价', '未评价']:
            _filter.lookup_bill('PT_Service_Order_Comment', 'id', 'record_id', 'comment_info')\
                # 从评论表获取数据
            if condition['is_evaluate'] == '已评价':
                _filter.match((C("comment_info") != None)
                              & (C("comment_info") != []))
                _filter.add_fields({
                    'service_attitude': self.ao.array_elemat("$comment_info.service_attitude", 0),
                    'service_quality': self.ao.array_elemat("$comment_info.service_quality", 0),
                    'opinion_remarks': self.ao.array_elemat("$comment_info.opinion_remarks", 0),
                })
            elif condition['is_evaluate'] == '未评价':
                _filter.match((C("comment_info") == None)
                              | (C("comment_info") == []))

        _filter.sort({'modify_date': -1})\
            .project({'_id': 0, **get_common_project()})
        res = self.page_query(
            _filter, "PT_Service_Record", page, count)

        # 有数据才做这些事
        if len(res['result']) > 0:
            # 找出订单数据
            # 需要找出产品数据，附带服务提供商
            product2_ids = []
            order2_ids = []
            provider2_ids = []
            provider_ids = []
            for item in res['result']:
                if 'service_product_id' in item:
                    product2_ids.append(item['service_product_id'])
                    order2_ids.append(item['order_id'])
                    provider_ids.append(item['servicer_id'])

            # 如果没有前置过滤，就后置补全数据【产品表】
            if len(product_ids) == 0:
                _filter_product = MongoBillFilter()
                _filter_product.match_bill((C('id').inner(product2_ids)))\
                    .project({'_id': 0})
                res_product = self.query(
                    _filter_product, "PT_Service_Product")

                for item in res_product:
                    if 'name' not in item:
                        item['name'] = ''
                    temp_product[item['id']] = item['name']
                    provider2_ids.append(item['organization_id'])

            # 如果没有前置过滤，就后置补全数据【服务商名字】
            if len(product_organization_ids) == 0:
                _filter_product = MongoBillFilter()
                _filter_product.match_bill((C('organization_id').inner(provider2_ids)))\
                    .project({'_id': 0})
                res_product = self.query(
                    _filter_product, "PT_Service_Product")

                for item in res_product:
                    product_organization_ids.append(item['organization_id'])

            _filter_provider = MongoBillFilter()
            _filter_provider.match_bill((C('personnel_type') == '2') & (C('id').inner(product_organization_ids)))\
                .project({'_id': 0})
            res_provider = self.query(
                _filter_provider, "PT_User")

            for item in res_provider:
                if 'name' not in item:
                    item['name'] = ''
                temp_provider[item['id']] = item['name']
            for item in res_product:
                temp_product_organization[item['id']
                                          ] = temp_provider[item['organization_id']]

            # 如果没有前置过滤，就后置补全数据【订单表-长者表】
            if len(order_elder_ids) == 0:
                _filter_order = MongoBillFilter()
                _filter_order.match_bill((C('id').inner(order2_ids)))\
                    .project({'_id': 0})
                res_order = self.query(
                    _filter_order, "PT_Service_Order")

                for item in res_order:
                    order_elder_ids.append(item['purchaser_id'])

            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(order_elder_ids)))\
                .project({'_id': 0})
            res_elder = self.query(
                _filter_elder, "PT_User")

            for item in res_elder:
                if 'name' not in item:
                    item['name'] = ''
                temp_elder[item['id']] = item['name']
            for item in res_order:
                if item['purchaser_id'] in temp_elder:
                    temp_order_elder[item['id']
                                     ] = temp_elder[item['purchaser_id']]
                    temp_order_create[item['id']
                                      ] = item['create_date']

            # 后置补全服务人员
            _filter_servicer = MongoBillFilter()
            _filter_servicer.match_bill((C('personnel_type') == '1') & (C('personnel_info.personnel_category') == '工作人员') & (C('id').inner(provider_ids)))\
                .project({'_id': 0})
            res_servicer = self.query(
                _filter_servicer, "PT_User")

            temp_servicer = {}
            for item in res_servicer:
                if 'name' not in item:
                    item['name'] = ''
                temp_servicer[item['id']] = item['name']

            for item in res['result']:
                # 补全产品名字和服务供应商名字
                if 'service_product_id' in item:
                    if item['service_product_id'] in temp_product:
                        item['product_name'] = temp_product[item['service_product_id']]
                    if item['service_product_id'] in temp_product_organization:
                        item['provider_name'] = temp_product_organization[item['service_product_id']]
                # 根据订单id补全长者名字和下单时间
                if 'order_id' in item:
                    if item['order_id'] in temp_order_elder:
                        item['elder_name'] = temp_order_elder[item['order_id']]
                        item['create_date'] = temp_order_create[item['order_id']]
                # 补全服务人员名字
                if 'servicer_id' in item:
                    if item['servicer_id'] in temp_servicer:
                        item['servicer_name'] = temp_servicer[item['servicer_id']]

        return res

        # _filter = MongoBillFilter()
        # _filter.match_bill((C('id') == values['id'])
        #                    & (C('organization_id').inner(org_list))
        #                    & (C('order_id') != None)
        #                    & (C('create_date') >= as_date(values['begin_date']))
        #                    & (C('create_date') <= as_date(values['end_date'])))\
        #     .lookup_bill('PT_Service_Order_Comment', 'id', 'record_id', 'comment_info')\
        #     .lookup_bill('PT_User', 'order_info.purchaser_id', 'id', 'elder_info')\
        #     .lookup_bill('PT_User', 'servicer_id', 'id', 'servicer_info')\
        #     .lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product_info')\
        #     .add_fields({'product_info': self.ao.array_elemat("$product_info", 0)})\
        #     .lookup_bill('PT_User', 'product_info.organization_id', 'id', 'provider_info')\
        #     .match((C("order_info") != None) & (C("order_info") != []) & (C("elder_info") != None) & (C("elder_info") != []) & (C("servicer_info") != None) & (C("servicer_info") != []) & (C("product_info") != None) & (C("product_info") != []) & (C("provider_info") != None) & (C("provider_info") != []))\

        # if 'is_evaluate' in condition:
        #     if condition['is_evaluate'] == '已评价':
        #         _filter.match((C("comment_info") != None)
        #                       & (C("comment_info") != []))
        #     elif condition['is_evaluate'] == '未评价':
        #         _filter.match((C("comment_info") == None)
        #                       | (C("comment_info") == []))

        # _filter.add_fields({
        #     'elder_name': self.ao.array_elemat("$elder_info.name", 0),
        #     'provider_name': self.ao.array_elemat("$provider_info.name", 0),
        #     'servicer_name': self.ao.array_elemat("$servicer_info.name", 0),
        #     'service_attitude': self.ao.array_elemat("$comment_info.service_attitude", 0),
        #     'service_quality': self.ao.array_elemat("$comment_info.service_quality", 0),
        #     'opinion_remarks': self.ao.array_elemat("$comment_info.opinion_remarks", 0),
        #     'product_name': '$product_info.name',
        #     'create_date': '$order_info.create_date', })\
        #     .match_bill((C('elder_name').like(values['elder_name'])) & (C('provider_name').like(values['provider_name'])))\
        #     .sort({'create_date': -1})\
        #     .project({'_id': 0, 'elder_name': 1, 'provider_name': 1, 'service_attitude': 1, 'service_quality': 1, 'product_name': 1, 'create_date': 1, 'servicer_name': 1, 'opinion_remarks': 1, 'id': 1, 'start_date': 1, 'end_date': 1})
        # res = self.page_query(
        #     _filter, "PT_Service_Record", page, count)
        # return res

    def add_service_record(self, order_id, product_id, is_virtual, other={}, is_update=False, order_res=None, record_code=None):
        '''新增服务记录（根据服务订单生成服务记录）'''
        # 判断服务记录是否存在
        # self.has_exit_recode(order_id, product_id, is_update, other=other)
        recode = {'order_id': order_id,
                  'service_product_id': product_id, 'service_option': []}
        if is_update:
            recode.update(id=is_update)
        recode['is_virtual'] = '1' if is_virtual else '0'
        # 2020/07/13 修改：为了不影响之前的逻辑，增加可空传入参数，传入的为订单的数组
        if order_res != None:
            res = order_res
        else:
            _filter = MongoBillFilter()
            _filter.match_bill((C('id') == order_id)) \
                .unwind('detail') \
                .match_bill(C('detail.product_id') == product_id)\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Service_Order')
        if len(res):
            item = res[0]['detail']
            # print('detail:<<<<<', item)
            option = item['service_option']
            if type(item['valuation_formula']) == list:
                method = item['valuation_formula'][0]['formula']
            elif type(item['valuation_formula']) == str:
                method = item['valuation_formula']
            # 后付费
            if res[0]['is_paying_first'] == '1':
                other['service_option'] = recode['service_option'] + \
                    other['service_option']

                recode['valuation_amount'] = round(execute_python(
                    method, other['service_option']), 2)  # 计价金额

                recode['status'] = RecordStatus.service_before.value
                recode['service_option'] = other['service_option']
            else:  # 先付费
                # 暂时改成不管虚拟服务记录这种情况，对于虚拟服务记录，其实与服务订单的金额一致的
                for x in option:
                    recode['service_option'].append(x)
                recode['valuation_amount'] = round(execute_python(
                    method, recode['service_option']), 2)  # 计价金额
                # if is_virtual == '1':  # 虚拟记录,只保存服务前选项
                #     for x in option:
                #         # print(x,'x>>>>')
                #         if x['option_type'] == 'service_before':
                #             recode['service_option'].append(x)

                #     recode['valuation_amount'] = round(self.execute_python(method, recode['service_option']),2)  # 计价金额
                # else:  #非虚拟记录，不生成金额，选项为服务后选项
                #     for x in option:
                #         if x['option_type'] == 'service_after':
                #             recode['service_option'].append(x)
                #     recode['valuation_amount'] = 0
        else:
            raise JsonRpc2Error('-35007', '该订单不存在')
        if record_code != None:
            record_code = record_code
        else:
            record_code = ''

            def process_func(db):
                nonlocal record_code
                date_time = get_cur_time().strftime("%Y%m%d")
                record_code = 'G' + date_time + \
                    get_serial_number(db, SerialNumberType.record.value)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
        recode['record_code'] = record_code
        recode = dict(recode, **other)
        return get_info(recode, self.session)

    def has_exit_recode(self, order_id, product_id, is_update, other={}):
        res = []
        if 'start_date' in other.keys() and 'end_date' in other.keys():
            start_date = other['start_date']
            end_date = other['end_date']
            _filter = MongoBillFilter()
            if is_update:  # 编辑，过滤自身
                _filter.match_bill(C('id') != is_update)
                # print(_filter.filter_objects)
            _filter.match_bill(
                (C('order_id') == order_id)
                & (C('service_product_id') == product_id)
                & (((C('start_date') >= start_date) & (end_date >= C('start_date')))
                   | ((C('start_date') < start_date) & (C('end_date') > start_date)))
            )\
                .project({'_id': 0})
            res = self.query(_filter, 'PT_Service_Record')
        if len(res):
            raise JsonRpc2Error('-35006', '服务记录已存在')

        # result = False
        # print('订单id',order_ids)
        # if process_name:
        #     for order_id in order_ids:
            # recode_param = self.accommodation_process.get_item_info(process_name, order_id, cal_values)
        #         _filters = MongoBillFilter()
        #         _filters.lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
        #                 .match_bill((
        #                             ((C('start_date') <= begin_date)
        #                             & (C('end_date') >= end_date))
        #                             | ((C('start_date') >= begin_date) & (C('end_date') <= end_date))
        #                             | ((C('start_date') >= begin_date) & (C('start_date') <= end_date))
        #                             | ((C('end_date') >= begin_date) & (C('end_date') <= end_date))
        #                         ) & (C('order_id') == order_id) & (C('service_product_id') == recode_param['service_product_id']))\
        #                 .project({'_id': 0})
        #         recode = self.query(_filters, 'PT_Service_Record')
        #         if not len(recode):
        #             order_record = OrderRecord(
        #                 order_id, recode_param['service_product_id'], '', '',recode_param['option_list'],begin_date, end_date)
        #             data_info = get_info(order_record.to_dict())
        #             result = self.bill_manage_server.add_bill(
        #                 OperationType.add.value, TypeId.serviceRecord.value, data_info, 'PT_Service_Record')
        # return operation_result(result)

        # res = 'Fail'
        # _filter = MongoBillFilter()
        # _filter.match_bill((C('id').inner(order_ids)))\
        #        .project({'_id': 0,'item._id': 0})
        # # .add_fields({
        # #             'create_date': self.ao.date_to_string('$create_date'),
        # #             'modify_date': self.ao.date_to_string('$modify_date')})\
        # order_data = self.query(
        #     _filter, "PT_Service_Order")

    #     def process_func(db):
    #         nonlocal res
    #         nonlocal order_data
    #         user_id = get_person_id(db, self.session)
    #         if len(order_data) > 0:
    #             for order in order_data:
    #                 # 遍历服务产品
    #                 for detail in order['service_item']:
    #                     begin_date = None
    #                     end_date = None
    #                     service_product_id = detail['service_item']
    #                     option_list = detail['service_options']
    #                     # 服务内容取项目的合同条款
    #                     content = detail['contract_clauses']
    #                     # 取项目中的周期性类型字段
    #                     # if 'periodicity' in detail:
    #                     #     periodicity_data = find_data(db, 'PT_Periodicity_Date', {
    #                     #         'id': detail['periodicity']})
    #                     #     if len(periodicity_data) > 0:
    #                     #         days = periodicity_data[0]['days']
    #                     #         date = get_date_slot(days, choice_date)
    #                     #         # begin_date = as_date(date['begin_date'])
    #                     #         begin_date = date['begin_date'].strftime(
    #                     #             "%Y-%m-%d %H:%M:%S")
    #                     #         end_date = date['end_date'].strftime(
    #                     #             "%Y-%m-%d %H:%M:%S")
    #                     #         # end_date = as_date(date['end_date'])
    #                     # else:
    #                     begin_date = as_date(
    #                         get_cur_time().strftime("%Y-%m-%d %H:%M:%S"))
    #                     end_date = as_date(
    #                         get_cur_time().strftime("%Y-%m-%d %H:%M:%S"))
    #                     _filters = MongoBillFilter()
    #                     _filters.lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
    #                         .add_fields({
    #                             'purchaser_id': '$order.purchaser_id',
    #                         })\
    #                         .match_bill((
    #                             ((C('start_date') <= begin_date)
    #                              & (C('end_date') >= end_date))
    #                             | ((C('start_date') >= begin_date) & (C('end_date') <= end_date))
    #                             | ((C('start_date') >= begin_date) & (C('start_date') <= end_date))
    #                             | ((C('end_date') >= begin_date) & (C('end_date') <= end_date))
    #                         ) & (C('purchaser_id') == order_data[0]['purchaser_id']) & (C('order_id') == order['id']) & (C('item_id') == service_product_id))\
    #                         .project({'_id': 0})
    #                     record = self.query(_filters, "PT_Service_Record")
    #                     if len(record) == 0:
    #                         # 如果存在数据，则证明已存在与该时间段重叠的数据
    #                         order_record = OrderRecord(
    #                             order['id'], service_product_id, user_id, content,option_list,begin_date, end_date)
    #                         data_info = get_info(order_record.to_dict())
    #                         result = self.bill_manage_server.add_bill(
    #                             OperationType.add.value, TypeId.serviceRecord.value, data_info, 'PT_Service_Record')
    #                         if result:
    #                             res = 'Success'
    #     process_db(self.db_addr, self.db_port, self.db_name, process_func)
    #     return res

    def add_service_record_option(self, record_id, options):
        '''记录服务记录项目值接口：记录选项值，然后根据公式计算出金额。入参options：[{'name','value'}]'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            nonlocal options
            record_mes = find_data(db, 'PT_Service_Record', {
                'id': record_id, 'bill_status': Status.bill_valid.value})
            if len(record_mes) > 0:
                if 'service_option' in record_mes[0]:
                    record_mes[0]['service_option'] = record_mes[0]['service_option'] + options
                else:
                    record_mes[0]['service_option'] = options
                # 获取产品id
                product_id = record_mes[0]['service_product_id']
                # 查找订单获取计价改订单下某个产品的定价公式
                order_mes = find_data(db, 'PT_Service_Order', {
                                      'id': record_mes[0]['order_id'], 'bill_status': Status.bill_valid.value})
                # 获取服务记录的产品id
                if len(order_mes) > 0:
                    item_detail = order_mes[0]['service_item']
                    if len(item_detail) > 0:
                        for item in item_detail:
                            options = options + item['service_option']
                    # 根据公式计算金额 TODO公式方法
                    valuation_formula = item_detail[0]['valuation_formula']
                    # 公式中的参数字段替换为值
                    valuation_amount = round(execute_python(
                        valuation_formula, options), 2)
                    record_mes[0]['valuation_amount'] = valuation_amount
                result = self.bill_manage_server.add_bill(OperationType.update.value,
                                                          TypeId.serviceRecord.value, record_mes[0], 'PT_Service_Record')
                if result:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    # def execute_python(self, valuation_formula, options):
    #     ''' 执行Python字符串方法'''
    #     names = locals()
    #     total = 0
    #     for option in options:
    #         if option.get('option_name'):
    #             names[str(option.get('option_name'))
    #                   ] = option.get('option_value')
    #         elif option.get('name'):
    #             names[str(option.get('name'))] = option.get('option_value')
    #     try:
    #         # print(valuation_formula)
    #         # print(names)
    #         total = eval(valuation_formula, names)
    #     except:
    #         total = 0
    #     return total

    def update_services_ledger(self, record_ids):
        ''' 生成台账数据'''
        res = 'Fail'
        # 查询记录，根据订单中的同一个人，合并生成台账记录
        _filter = MongoFilter()
        _filter.lookup('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({
                'purchaser_id': '$order.purchaser_id',
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .match((C('bill_status') == Status.bill_valid.value))\
            .project({'_id': 0,
                      'order._id': 0})
        record_data = self.query(_filter, "PT_Service_Record")

        def process_func(db):
            nonlocal res
            nonlocal record_data
            # if len(record_data) > 0:
            # for record in record_data:

            # ledger = Ledger()
            # data_info = get_info(order_record.to_dict())
            # result = self.bill_manage_server.add_bill(
            #     OperationType.add.value, TypeId.serviceLedger.value, data_info, 'PT_Service_Ledger')
            # if result:
            #     res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_service_ledger_list(self, condition, page=None, count=None):
        '''查询台账列表'''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoFilter()
        _filter.lookup('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                'user_name': '$user.name',
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .match((C('bill_status') == Status.bill_valid.value) & (C('id') == values['id']))\
            .project({'_id': 0,
                      'user._id': 0})
        res = self.page_query(
            _filter, "PT_Service_Ledger", page, count)
        return res

    def update_service_settlement(self, service_settlement):
        ''' 服务结算'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in service_settlement.keys():
                bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
                                                           TypeId.serviceSettlement.value, service_settlement, 'PT_Service_Record')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    # def update_service_provider_record(self, status, id):
    #     ''' 服务商申请审核'''
    #     res = 'Fail'
    #     _filter = MongoBillFilter()
    #     _filter.match_bill(C('service_provider_id') == id)\
    #         .project({'_id': 0, })
    #     res = self.page_query(
    #         _filter, "PT_Service_Provider_Register", 1, 1)
    #     res['result'][0]['status'] = status
    #     entity=res['result'][0]
    #   #print(res['result'])
    #   #print(id)

    #     def process_func(db):
    #         nonlocal entity
    #         nonlocal res
    #         bill_id = self.bill_manage_server.add_bill(OperationType.update.value,
    #                                                     TypeId.serviceSettlement.value, entity, 'PT_Service_Provider_Register')
    #         if bill_id:
    #             res = 'Success'
    #     process_db(self.db_addr, self.db_port, self.db_name, process_func)

    #     return res

    def add_service_order_comment(self, orderComments):
        """ 新增服务订单评论 orderComment： [{order_id, product_id, service_attitude,service_quality,opinion_remarks}] """
        res = 'Fail'
        for orderComment in orderComments:
            data = ServiceOrderCommentObject(
                get_current_user_id(self.session), orderComment['order_id'], orderComment['product_id'], orderComment['service_attitude'], orderComment['service_quality'], orderComment['opinion_remarks']).to_dict()
            if 'record_id' in orderComment:
                data['record_id'] = orderComment['record_id']
            data_info = get_info(data, self.session)
            bill_id = self.bill_manage_server.add_bill(OperationType.add.value,
                                                       TypeId.orderComment.value, data_info, 'PT_Service_Order_Comment')
            if bill_id:
                res = "Success"
        return res

    def delete_service_order(self, condition):
        '''删除订单'''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(condition, str):
                ids.append(condition)
            else:
                ids = condition
            del_ids = []
            for order_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Service_Order', {
                    'id': order_id, 'bill_status': 'valid'})
                if len(data) > 0:
                    record_list = find_data(db, 'PT_Service_Record', {
                                            'order_id': order_id, 'bill_status': 'valid'})
                    if data[0]['status'] != '未服务' or len(record_list) > 0:
                        res = '该订单已分派，不能删除'
                        return res
                    del_ids.append({'id': order_id})
            self.bill_manage_server.add_bill(OperationType.delete.value,
                                             TypeId.reserveService.value, del_ids, 'PT_Service_Order')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_service_plan_list(self, org_list, condition, page, count):
        '''服务计划'''
        res = 'Fail'

        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"

        keys = ['id', 'name', 'begin_date', 'end_date']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('name').like(values['name']))
            & (C('create_date') > as_date(values['begin_date']))
            & (C('create_date') < as_date(values['end_date']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'uploader_id', 'id', 'user')\
            .add_fields({
                'uploader_name': self.ao.array_elemat("$user.name", 0), })\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'uploader_name': 1,
                      'file': 1,
                      'remark': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Service_Plan', page, count)

        return res

    def update_service_plan(self, data):
        '''新增服务计划'''
        res = 'Fail'

        if 'id' in list(data.keys()):

            data['uploader_id'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.servicePlan.value, data, 'PT_Service_Plan')
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.servicePlan.value, data, 'PT_Service_Plan')
            if bill_id:
                res = 'Success'
        return res

    def delete_service_plan(self, ids):
        ''' 删除服务计划 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.servicePlan.value, {'id': i}, 'PT_Service_Plan')
        if bill_id:
            res = 'Success'
        return res
