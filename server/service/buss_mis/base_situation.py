'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 17:14:46
@LastEditTime: 2019-12-12 10:11:18
@LastEditors: Please set LastEditors
'''
import base64
import datetime
import hashlib
import json
import os
import re
import shutil
import uuid
from enum import Enum

import pandas as pd
import requests

from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...pao_python.pao.data import (DataList, DataProcess, dataframe_to_list,
                                    get_cur_time, process_db)
from ...pao_python.pao.remote import JsonRpc2Error
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import \
    get_current_account_id
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import (GetInformationByIdCard, ImageSize, ResultData,
                               delete_data, find_data, get_condition,
                               get_current_user_id, get_info, insert_data,
                               insert_many_data, operation_result, update_data, get_common_project)
from ...service.constant import SubsidyRecordState, SubsidyRecordSubsidyType
from ...service.mongo_bill_service import MongoBillFilter

# -*- coding: utf-8 -*-

'''
基本情况函数
'''


class BaseSituationService(MongoService):
    ''' 基本情况 '''
    PT_USER = 'PT_User'
    PT_ADMINISTRATION_DIVISION = 'PT_Administration_Division'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        if session:
            self.session = session
        else:
            self.session = {
                "user_id": '23b3630a-d92c-11e9-8b9a-983b8f0bcd67',
                'organization_id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
            }
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_base_situation_list(self, org_list, condition, page=None, count=None):
        '''查询基本情况列表'''
        keys = ['id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['id'])\
            .graph_lookup_bill('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
            .unwind('admin_info')\
            .inner_join_bill(self.PT_USER, 'admin_info.id', 'admin_area_id', 'org_info')\
            .match_bill((C('org_info.personnel_type') == '2')
                        & (C('org_info.organization_info.personnel_category') == '幸福院')
                        & (C('org_info.organization_id').inner(org_list)))\
            .lookup_bill(self.PT_USER, 'org_info.id', 'organization_id', 'user_info')\
            .add_fields({'correct_user_info': self.ao.array_filter('$user_info', 'tep', ((F('$tep.personnel_type') == '1') & (F('$tep.personnel_info.personnel_category') == '长者')).f)})\
            .add_fields({'elder_num': self.ao.size('$correct_user_info')})\
            .project({
                'user_info': 0,
                'correct_user_info': 0,
                **get_common_project({'org_info', 'admin_info'})
            })
        res = self.page_query(
            _filter, self.PT_ADMINISTRATION_DIVISION, page, count)
        return res
