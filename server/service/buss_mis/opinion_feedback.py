
from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import get_cur_time, process_db, dataframe_to_list, DataProcess, DataList
from ...service.constant import FollowCollectionObject, FollowCollectionType
from ...service.buss_pub.message_manage import MessageManageService
# -*- coding: utf-8 -*-

"""
意见反馈管理函数
"""


class OpinionFeedbackObject():
    '''意见反馈对象'''

    def __init__(self, user_id, content, status):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 内容
        self.content = content
        # 状态
        self.status = status

    def to_dict(self):
        return self.__dict__


class OpinionFeedbackService(MongoService):
    """ 意见反馈管理 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )
        self.MessageManageService = MessageManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def update_opinion_feedback(self, condition):
        """ 新增意见反馈 """
        res = 'Fail'
        user_id = get_current_user_id(self.session)
        if 'id' not in condition:
            dt = OpinionFeedbackObject(user_id, condition['content'], '未处理')
            data_info = get_info(dt.to_dict(), self.session)
            bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                        TypeId.opinionFeedback.value, data_info, 'PT_Opinion_Feedback')
        else:
            if 'process_content' not in condition or condition['process_content'] == '':
                return '请输入处理记录回复！'
            condition['process_user_id'] = get_current_user_id(self.session)
            condition['process_date'] = get_cur_time()
            condition['status'] = '已处理'
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.opinionFeedback.value, condition, 'PT_Opinion_Feedback')
            if bill_id:
                # 插入一个消息提醒
                self.MessageManageService.add_new_message({
                    # 高龄津贴标识
                    "business_type": 'app_feedback',
                    # 业务ID
                    "business_id": condition['id'],
                    # 针对用户
                    "receive_info": condition['user_id']
                })
        if bill_id:
            res = 'Success'
        return res

    # 获取反馈列表
    def get_app_feedback_list(self, org_list, condition, page, count):
        if 'date_range' in list(condition.keys()):
            condition['begin_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ['id', 'status', 'begin_date', 'end_date', 'remark']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('status') == values['status'])
                           & (C('remark').like(values['remark']))
                           & (C('create_date') >= as_date(values['begin_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .lookup_bill('PT_User', 'process_user_id', 'id', 'process')\
            .add_fields({
                'name': self.ao.array_elemat('$user.name', 0),
                'process_name': self.ao.array_elemat('$process.name', 0),
                'telephone': self.ao.array_elemat('$user.personnel_info.telephone', 0),
            })\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'user_id': 1,
                'name': 1,
                'remark': 1,
                'content': 1,
                'telephone': 1,
                'process_name': 1,
                'process_content': 1,
                'process_date': 1,
                'create_date': 1,
                'status': 1,
            })
        res = self.page_query(_filter, 'PT_Opinion_Feedback', page, count)
        return res
