'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 16:02:12
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\service_woker.py
'''
from ...pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import get_info
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.common import insert_data, find_data


class ServiceWoker(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_service_woker_list(self, order_ids, condition, page, count):
        keys = ['id', 'user_name', 'item_name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_id').inner(order_ids))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
               .match_bill((C('id') == values['id']) & C('user_name').like(values['user_name']) & C('item_name').like(values['item_name'])) \
               .add_fields({'user_name': '$user.name', 'create_date': self.ao.date_to_string('$create_date')})\
               .project({'_id': 0, 'user._id': 0})
        res = self.page_query(_filter, 'IEC_Service_Woker', page, count)
        return res

    def add_update_woker(self, condition):
        res = 'fail'

        def process_func(db):
            nonlocal res
            if 'id' in condition.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.servicePerson.value, condition, 'IEC_Service_Woker')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.servicePerson.value, condition, 'IEC_Service_Woker')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_woker(self, ids):
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for id in ids:
                data = find_data(db, 'IEC_Service_Woker', {
                    'id': id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value, TypeId.servicePerson.value, data[0], 'IEC_Service_Woker')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
