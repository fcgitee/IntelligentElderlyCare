'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 17:14:46
@LastEditTime: 2019-10-10 15:53:24
@LastEditors: Please set LastEditors
'''

import calendar
import datetime
import hashlib
import re
import uuid
from enum import Enum

import dateutil
import pandas as pd

from server.pao_python.pao.service.data.mongo_db import (
    C, F, MongoService, N, as_date)

from ...pao_python.pao.data import (DataList, DataProcess, dataframe_to_list,
                                    get_cur_time, process_db)
from ...pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                     MongoService, N, as_date)
from ...pao_python.pao.service.security.security_service import RoleService
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import (SerialNumberType, delete_data, find_data,
                               get_condition, get_current_role_id,
                               get_current_user_id, get_info,
                               get_serial_number, get_user_id_or_false,
                               insert_data, update_data)
from ...service.mongo_bill_service import MongoBillFilter

# -*- coding: utf-8 -*-

'''
易米云通函数
'''


class EmiCallService(MongoService):
    ''' 易米云通 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def login(self, condition):
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == condition['user_id']))
        res = self.query(_filter, "emi_role")

        if len(res) == 0:
            return False
        self.session["worker_no"] = res[0]["worker_no"]
        self.session["number"] = res[0]["number"]
        
        return True

    def get_role_id(self):
        return get_current_role_id(self.session)

    def get_work_no(self):
        return self.session["worker_no"]
        # return "0001"
        
    def get_sign_in_session(self):
        if "is_login_emi" in self.session:
            return self.session["is_login_emi"]
        else:
            return False
