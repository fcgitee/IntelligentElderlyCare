'''
@Author: your name
@Date: 2020-03-02 14:05:50
@LastEditTime: 2020-03-25 14:44:15
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\official_follow_manage.py
'''

from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.constant import FollowCollectionObject, FollowCollectionType
# -*- coding: utf-8 -*-


class OfficialFollowService(MongoService):
    """ 微信公众号关注 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_Statistics_list(self, condition, page=None, count=None):
        """获取微信公众号关注列表 

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        """
        keys = ["id", "organization_id", "sort", "zj_name"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'is_own' in condition.keys():
            user_id = get_current_user_id(self.session)
            _filter.match_bill((C('user_id') == user_id))
        _filter.match_bill((C("id") == values["id"])
                           & (C("organization_id") == values["organization_id"]))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .lookup_bill('PT_Administration_Division', 'org.admin_area_id', 'id', 'admin')\
            .lookup_bill('PT_Administration_Division', 'admin.parent_id', 'id', 'admin_gl')\
            .lookup_bill('PT_Administration_Division', 'admin_gl.parent_id', 'id', 'adminZj')\
            .add_fields({
                'name': self.ao.array_elemat('$org.name', 0),
                'admin_name': self.ao.array_elemat('$admin.name', 0),
                'admin_zj_name': self.ao.array_elemat('$adminZj.name', 0)
            })\
            .match(C('admin_zj_name').like(values["zj_name"]))\
            .sort({"follow_quantity": int(values["sort"])})\
            .project({"_id": 0, "org._id": 0, "admin": 0, "admin_gl": 0, "adminZj": 0})
        res = self.page_query(
            _filter, "PT_Wx_Follow", page, count)
        return res
