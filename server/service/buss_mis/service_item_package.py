# -*- coding: utf-8 -*-

'''
服务包函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_cur_time, get_current_organization_id, execute_python, get_current_role_id, get_current_user_id, get_common_project
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.mongo_bill_service import MongoBillFilter
from ...service.app.service_detail import ServiceProductDetailService


class ServiceItemPackageService(MongoService):
    ''' 服务包 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.detail_service = ServiceProductDetailService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def get_service_item_package(self, order_ids, condition, page=None, count=None):
        '''查询服务包列表'''
        keys = ['id', 'name', 'organization_name', 'is_external',
                'is_service_item', 'item_type_id', 'status', 'state', 'is_recommend']

        # 默认获取状态为已通过的和使用状态为启用的
        if 'all' in condition and condition['all'] == True:
            pass
          # print('获取全部')
        elif 'status' not in condition and 'state' not in condition:
            condition['status'] = '通过'
            condition['state'] = '启用'

        values = self.get_value(condition, keys)

        # 默认获取通过和启用的
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(order_ids)) & (C('status') == values['status']) & (C('is_recommend') == values['is_recommend']) & (C('state') == values['state']) & (C('is_service_item') == values['is_service_item']) & (C('id') == values['id']) & (C('is_external') == values['is_external']) & (C('name').like(values['name'])))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'user')\
            .add_fields({
                'organizational_name': '$user.name',
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .match_bill(C('user.name').like(values['organization_name']))\
            .lookup_bill('PT_Service_Item', 'service_item_id', 'id', 'item')

        _filter.sort({'modify_date': -1})
        if 'item_type_id' in condition and condition['item_type_id'] == '套餐':
            _filter.match_bill((C('is_service_item') == 'false'))
        else:
            _filter.match_bill(C('item.item_type') == values['item_type_id'])

        _filter.add_fields({'item_name': '$item.name'})\
            .project({'_id': 0, 'user._id': 0, 'item._id': 0, 'option._id': 0, **get_common_project()})\

        res = self.page_query(_filter, 'PT_Service_Product', page, count)
        # if res['result']:
        #     for result in res['result']:
        #         scope_list = []
        #         for scope_id in result['application_scope']:
        #             _filter_scope=  MongoBillFilter()
        #             _filter_scope.match_bill((C('id') == scope_id)).project({'_id': 0})
        #             res_scope = self.query(_filter_scope,'PT_Service_Scope')
        #             if res_scope:
        #                 scope_list.append(res_scope[0].get('name'))
        #         result['scope_name'] = '-'.join(scope_list)
        # 循环查询服务选项表
        # if 'id' in list(condition.keys()):
        #     if res['result'][0].get('service_item'):
        #         _filter_item = MongoBillFilter()
        #         _filter_item.match_bill(C('id') == res['result'][0].get('service_item_id')).project({'_.id':0})
        #         res_item = self.query(_filter_item,'PT_Service_Item')
        #         if res_item:
        #             res['result'][0]['service_item'][servicr_index]['item_type_id'] = res_item[0].get('item_type')
        #         if res['result'][0].get('service_item')[servicr_index].get('service_options'):
        #             for option_index in range(len(res['result'][0].get('service_item')[servicr_index].get('service_options'))):

        #                 _filter2 = MongoBillFilter()
        #                 _filter2.match_bill((C('id') == res['result'][0]['service_item']
        #                                 [servicr_index]['service_options'][option_index]['id']))
        #                 res2 = self.page_query(
        #                     _filter2, "PT_Service_Option", 1, 1)
        #                 if res2['result']:
        #                     res['result'][0]['service_item'][servicr_index]['service_options'][
        #                         option_index]['option_content'] = res2['result'][0].get('option_content')
        #                     res['result'][0]['service_item'][servicr_index]['service_options'][
        #                         option_index]['name'] = res2['result'][0].get('name')
        # keys = ['id', 'name','organization_name']
        # values = self.get_value(condition, keys)
        # _filter = MongoBillFilter()
        # _filter.lookup_bill('PT_User', 'org_id', 'id', 'user')\
        #     .add_fields({
        #         'organizational_name': '$user.name',
        #         'create_date': self.ao.date_to_string('$create_date'),
        #         'modify_date': self.ao.date_to_string('$modify_date')})\
        #     .match_bill((C('id') == values['id']) & C('name').like(values['name']) & C('$user.name').like(values['organization_name']))\
        #     .project({'_id': 0,'user._id': 0})
        # res = self.page_query(_filter, "PT_Service_Product", page, count)
        # if res['result']:
        #     for result in res['result']:
        #         scope_list = []
        #         for scope_id in result['application_scope']:
        #             _filter_scope=  MongoBillFilter()
        #             _filter_scope.match_bill((C('id') == scope_id)).project({'_id': 0})
        #             res_scope = self.query(_filter_scope,'PT_Service_Scope')
        #             if res_scope:
        #                 scope_list.append(res_scope[0].get('name'))
        #         result['scope_name'] = '-'.join(scope_list)
        #     # 循环查询服务选项表
        #     if 'id' in list(condition.keys()):
        #         if res['result'][0].get('service_item'):
        #             for servicr_index in range(len(res['result'][0].get('service_item'))):
        #                 _filter_item = MongoBillFilter()
        #                 _filter_item.match_bill(C('id') == res['result'][0].get('service_item')[servicr_index].get('service_item')).project({'_.id':0})
        #                 res_item = self.query(_filter_item,'PT_Service_Item')
        #                 if res_item:
        #                     res['result'][0]['service_item'][servicr_index]['item_type_id'] = res_item[0].get('item_type')
        #                 if res['result'][0].get('service_item')[servicr_index].get('service_options'):
        #                     for option_index in range(len(res['result'][0].get('service_item')[servicr_index].get('service_options'))):

        #                         _filter2 = MongoBillFilter()
        #                         _filter2.match_bill((C('id') == res['result'][0]['service_item']
        #                                         [servicr_index]['service_options'][option_index]['id']))
        #                         res2 = self.page_query(
        #                             _filter2, "PT_Service_Option", 1, 1)
        #                         if res2['result']:
        #                             res['result'][0]['service_item'][servicr_index]['service_options'][
        #                                 option_index]['option_content'] = res2['result'][0].get('option_content')
        #                             res['result'][0]['service_item'][servicr_index]['service_options'][
        #                                 option_index]['name'] = res2['result'][0].get('name')

        return res

    def get_service_item_package_pure(self, order_ids, condition, page=None, count=None):
        '''重构的查询服务包列表，暂时只有在审核界面用'''
        keys = ['id', 'name', 'org_name', 'is_external',
                'is_service_item', 'status', 'state', 'is_recommend']

        # 搜索启用的时候，暂时必须是通过的，因为前端就是这样判断的
        if 'state' in condition and condition['state'] == '启用':
            condition['status'] = '通过'

        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('organization_id').inner(order_ids))
                           & (C('status') == values['status'])
                           & (C('is_recommend') == values['is_recommend'])
                           & (C('state') == values['state'])
                           & (C('is_service_item') == values['is_service_item'])
                           & (C('is_external') == values['is_external'])
                           & (C('name').like(values['name'])))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({
                'organization_name': self.ao.array_elemat('$org.name', 0)
            })\
            .match_bill(C('organization_name').like(values['org_name']))\
            .sort({'step_no': -1, 'modify_date': -1})\
            .project({
                '_id': 0,
                'org': 0,
                **get_common_project()
            })\

        res = self.page_query(_filter, 'PT_Service_Product', page, count)

        if len(res['result']) > 0:

            item_ids = []
            product_ids = []

            # 循环子项，获取所有项目和产品的id‘’
            for item in res['result']:
                if item.get('is_service_item') == 'true' and item.get('service_item_id'):
                    # 服务单项
                    item_ids.append(item.get('service_item_id'))
                elif item.get('is_service_item') == 'false' and item.get('service_product') and type(item.get('service_product') == list) and len(item.get('service_product')):
                    # 服务套餐
                    for itm in item.get('service_product'):
                        product_ids.append(itm.get('product_id'))

            temp_res = {}

            # 服务选项
            if len(item_ids) > 0:

                _filter_item = MongoBillFilter()
                _filter_item.match_bill((C('id').inner(item_ids)))\
                    .project({
                        '_id': 0,
                        **get_common_project()
                    })\

                res_item = self.query(_filter_item, 'PT_Service_Item')

                if len(res_item) > 0:

                    for item in res_item:
                        temp_res[item['id']] = item

            # 服务产品
            if len(product_ids) > 0:

                _filter_product = MongoBillFilter()
                _filter_product.match_bill((C('id').inner(product_ids)))\
                    .project({
                        '_id': 0,
                        **get_common_project()
                    })\

                res_product = self.query(_filter_product, 'PT_Service_Product')

                if len(res_product) > 0:

                    for item in res_product:
                        temp_res[item['id']] = item

            # 把值分配回各自
            if len(temp_res.keys()) > 0:

                for item in res['result']:

                    item['childs'] = []

                    if item.get('is_service_item') == 'true' and item.get('service_item_id') and item.get('service_item_id') in temp_res:
                        item['childs'].append(
                            temp_res[item.get('service_item_id')])
                    elif item.get('is_service_item') == 'false' and item.get('service_product') and type(item.get('service_product') == list) and len(item.get('service_product')):
                        # 服务套餐
                        for itm in item.get('service_product'):
                            if itm.get('product_id') in temp_res:
                                item['childs'].append(
                                    temp_res[itm.get('product_id')])

        return res

    def update_service_item_package(self, service_item_package):
        ''' 新增/修改服务包'''
        res = 'Fail'

        # 审核的编辑
        if 'action' in service_item_package and service_item_package['action'] == 'sh':
            return self.change_approval_process(service_item_package, "PT_Service_Product", TypeId.serviceProduct.value)

        # 单纯的编辑
        if 'pureEdit' in service_item_package:
            # 重新编辑
            return self.rechange(service_item_package, 'PT_Service_Product', TypeId.serviceProduct.value, 'serviceProduct')

        # 解决没有组织机构id的问题
        if 'organization_id' not in service_item_package or ('organization_id' in service_item_package and service_item_package['organization_id'] == ''):
            service_item_package['organization_id'] = get_current_organization_id(
                self.session)
        # 对于机构产品、定义时候不能确定金额的产品

        # 在新增或者编辑产品时候保存该产品/套餐当前的金额(居家产品、在定义时候就能确定金额的产品)
        price = 0
        if service_item_package['is_service_item'] == 'true':
            # 单个产品，带入公式
            if 'valuation_formula' in service_item_package and 'service_option' in service_item_package:
                if len(service_item_package['valuation_formula']) > 0 and 'formula' in service_item_package['valuation_formula'][0]:
                    price = execute_python(service_item_package.get('valuation_formula')[0].get(
                        'formula'), service_item_package.get('service_option'), 'name', 'option_value')
        else:
            # 产品套餐
            if 'service_product' in service_item_package and len(service_item_package['service_product']) > 0:
                for item in service_item_package['service_product']:
                    pruduct_detail = self.detail_service.get_service_product_package_detail(
                        {'id': item.get('product_id')})
                    # print('len11111111111111', len(pruduct_detail), pruduct_detail[0])
                    product_item_price = 0
                    remaining_times = 1
                    if len(pruduct_detail) > 0:
                        if 'service_package_price' in pruduct_detail[0]:
                            product_item_price = pruduct_detail[0].get(
                                'service_package_price')
                        # print('2222222222222', product_item_price)
                        remaining_times = item.get(
                            'remaining_times')

                        # 默认1
                        if remaining_times == None:
                            remaining_times = 1

                        # 如果有设置服务套餐价格
                        if 'service_package_price' in item:
                            # 不为空，就作为价格
                            if item['service_package_price'] != '':
                                # print('33333333333333333333', product_item_price)
                                product_item_price = item['service_package_price']
                            else:
                                # 不应该出现在此
                                del(item['service_package_price'])

                    print(product_item_price)
                    print(remaining_times)
                    print(float(product_item_price))
                    print(int(remaining_times))
                    price += float(product_item_price) * \
                        int(remaining_times)
        service_item_package['service_package_price'] = price

        def process_func(db):
            nonlocal res
            if 'id' in service_item_package.keys():

                # 重新编辑
                bill_id = self.rechange(
                    service_item_package, 'PT_Service_Product', TypeId.serviceProduct.value, 'serviceProduct')

                res = bill_id
            else:

                # 补充一个创建人
                service_item_package['apply_user_id'] = get_current_user_id(
                    self.session)
                # 补充默认数据
                service_item_package['state'] = '启用'

                # 主数据
                main_data = get_info(service_item_package, self.session)
                # 接入审批流程
                bill_id = self.create_approval_process(
                    main_data, 'PT_Service_Product', TypeId.serviceProduct.value, 'serviceProduct')
                res = bill_id
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    # 重新编辑
    def rechange(self, main_data, table, typeId, define_type):

        _filter_main = MongoBillFilter()
        _filter_main.match_bill(C('id') == main_data['id'])\
            .project({'_id': 0})
        res_main = self.query(_filter_main, table)

        if 'pureEdit' in main_data:

            del(main_data['pureEdit'])
            # 单纯的编辑
            res = self.bill_manage_service.add_bill(OperationType.update.value,
                                                    typeId, main_data, table)

        elif 'status' in res_main[0] and res_main[0]['status'] in ['打回', '不通过']:
            # 打回和不通过的重新编辑，把主数据和过程表状态改回待审批
            main_data['status'] = '待审批'

            # 找到过程表
            _filter_process = MongoBillFilter()
            _filter_process.match_bill(
                (C('business_id') == main_data['id']) & (C('step_no') == res_main[0]['step_no'])).project({'_id': 0})
            process_data = self.query(
                _filter_process, 'PT_Approval_Process')

            process_data[0]['status'] = '待审批'

            res = self.bill_manage_service.add_bill(OperationType.update.value,
                                                    typeId, [process_data, main_data], ['PT_Approval_Process', table])
        elif 'status' in res_main[0] and res_main[0]['status'] == '通过':
            # 已通过的重新编辑，要重新审核
            # 先把审批表的数据都设置valid
            _filter_process = MongoBillFilter()
            _filter_process.match_bill(
                (C('business_id') == main_data['id'])).project({'_id': 0})
            process_data = self.query(
                _filter_process, 'PT_Approval_Process')

            if len(process_data) > 0:
                for item in process_data:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value, TypeId.approvalProcess.value, {'id': item['id']}, 'PT_Approval_Process')

            # 标识主数据不需要新增
            main_data['doNotInsert'] = True

            # 接入审批流程
            res = self.create_approval_process(
                main_data, table, typeId, define_type)
        else:
            # 单纯的编辑
            res = self.bill_manage_service.add_bill(OperationType.update.value,
                                                    typeId, main_data, table)

        return res

    # 审批
    def change_approval_process(self, main_data, table, typeId):
        res = 'Fail'

        if 'action_value' not in main_data or main_data['action_value'] not in ['1', '2', '3']:
            return '非法操作'

        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == main_data['id'])\
            .project({'_id': 0})
        res_main = self.query(_filter, table)

        if len(res_main) == 0:
            return '没有找到主数据'

        # 当前的role_id
        role_id = get_current_role_id(self.session)

        # 找过程表
        _filter = MongoBillFilter()
        _filter.match_bill((C('step_no') == res_main[0]['step_no']) & (C('business_id') == res_main[0]['id']) & (C('status') == '待审批'))\
            .project({'_id': 0})
        res_process = self.query(_filter, 'PT_Approval_Process')

        if len(res_process) == 0:
            return '没有找到审批数据'

        if res_process[0]['approval_user_id'] != role_id:
            return '当前没有审核权限'

        data_list = []
        table_list = []

        if main_data['action_value'] == '1':
            # 当前通过
            # 看还有没有下一步

            # 找过程表
            _filter = MongoBillFilter()
            _filter.match_bill((C('step_no') == (res_main[0]['step_no'] + 1)) & (C('business_id') == res_main[0]['id']))\
                .project({'_id': 0})
            res_process_next = self.query(_filter, 'PT_Approval_Process')

            if len(res_process_next) > 0:
                # 还有下一步，不需要改状态，但是要改步骤为下一步
                main_data['step_no'] = res_process_next[0]['step_no']
                # 过程表要改
                res_process[0]['status'] = '通过'
            else:
                # 审核流程完成
                main_data['step_no'] = -1
                main_data['status'] = '通过'
                # 增加主数据的审核时间
                main_data['audit_date'] = get_cur_time()
                res_process[0]['step_no'] = -1
                res_process[0]['status'] = '通过'
        elif main_data['action_value'] == '2':
            # 审核流程完成
            main_data['step_no'] = -1
            main_data['status'] = '不通过'
            # 当前不通过的原因
            main_data['reason'] = main_data['reason']
            # 增加主数据的审核时间
            main_data['audit_date'] = get_cur_time()
            res_process[0]['step_no'] = -1
            res_process[0]['status'] = '不通过'
            res_process[0]['opinion'] = [main_data['reason']]
        elif main_data['action_value'] == '3':
            # 当前打回
            main_data['status'] = '打回'
            main_data['audit_date'] = get_cur_time()
            main_data['reason'] = main_data['reason']
            res_process[0]['status'] = '打回'
            res_process[0]['opinion'] = [main_data['reason']]

        # 过程表的操作者
        res_process[0]['proccess_user_id'] = get_current_user_id(self.session)

        # 清除没必要的数据
        del(main_data['action'])
        del(main_data['action_value'])

        data_list.append(main_data)
        data_list.append(res_process[0])
        table_list.append(table)
        table_list.append('PT_Approval_Process')

        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, typeId, data_list, table_list)
        if bill_id:
            res = 'Success'

        return res

    # 生成审批记录
    def create_approval_process(self, main_data, main_table, typeId, approval_type):
        res = 'Fail'
        _filter = MongoBillFilter()
        _filter.match_bill(C('approval_type') == approval_type)\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Approval_Define')

        if len(res) > 0:
            data_list = []
            table_list = []
            # 插入记录表
            for approval in res[0]['approval_list']:
                process_data = {"define_id": res[0]['id'], "business_id": main_data['id'], "approval_user_id":
                                approval['approval_user_id'], "step_no": approval['step_no'], "opinion": [], "status": "待审批"}
                data_list.append(
                    get_info(process_data, self.session))
                table_list.append('PT_Approval_Process')

            if 'doNotInsert' not in main_data:
                # 主表
                table_list.append(main_table)
                # 给主数据赋予初始审批值
                main_data['step_no'] = 1
                main_data['status'] = '待审批'
                # 主数据
                data_list.append(main_data)

                bill_id = self.bill_manage_service.add_bill(
                    OperationType.add.value, typeId, data_list, table_list)
                if bill_id:
                    res = 'Success'
            else:
                del(main_data['doNotInsert'])

                # 给主数据赋予初始审批值
                main_data['step_no'] = 1
                main_data['status'] = '待审批'

                bill_id = self.bill_manage_service.add_bill(
                    OperationType.update.value, typeId, main_data, main_table)

                if bill_id:

                    bill_id = self.bill_manage_service.add_bill(
                        OperationType.add.value, typeId, data_list, table_list)
                    if bill_id:
                        res = 'Success'
        else:
            res = '无权限'
        return res

    def delete_service_item_package(self, service_item_package_ids):
        '''删除服务包接口

        Arguments:
            service_item_package   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(service_item_package_ids, str):
                ids.append(service_item_package_ids)
            else:
                ids = service_item_package_ids
            for service_scope_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Service_Product', {
                    'id': service_scope_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.serviceProduct.value, data[0], 'PT_Service_Product')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
