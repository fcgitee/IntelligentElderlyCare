
from ...service.buss_mis.financial_account import FinancialAccountService
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import get_user_id_or_false, get_current_user_id, get_current_role_id, insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_organization_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
from ...service.mongo_bill_service import MongoBillFilter
from ...service.buss_mis.comment_manage import CommentManageService
from enum import Enum
from ..constant import AccountType, AccountStatus, PayStatue, PayType
from ...service.buss_mis.business_area import BusinessAreaService

import time
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-07-29 18:09:15
@LastEditTime: 2020-03-25 10:47:03
@LastEditors: Please set LastEditors
'''
# -*- coding: utf-8 -*-

'''
服务选项函数
'''


class Financial():
    '''发送财务模块对象'''

    def __init__(self, user_id, org_id, amount, item):
        '''构造函数'''
        # # 用户ID
        # self.user_id = user_id
        # # 银行户主名
        # self.name = name
        # # 账户
        # self.card_id = account
        # # 金额
        # self.amount = amount
        # # 项目名称
        # self.item = item
        # # 组织id
        # self.organization_id = organization_id
        # # 截止日期
        # self.end_date = end_date
        # 付款方id
        self.pay_id = user_id
        # 收款方id
        self.receive_id = org_id
        # 金额
        self.amount = amount
        # 摘要
        self.abstract = item

    def to_dict(self):
        return self.__dict__


class CostStatus:
    '''服务记录发送状态'''
    success = '已发送'
    fail = '发送失败'


class ServiceRecordService(MongoService):
    ''' 服务记录 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.financial_account_service = FinancialAccountService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.comment_service = CommentManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.BusinessAreaService = BusinessAreaService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_service_record_list_all2(self, order_ids, condition, page, count):
        '''获取服务记录列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''

        # return self.get_service_record_list_all_app(order_ids, condition, page, count)

        if 'start_date' in list(condition.keys()):
            condition['start_date_s'] = condition['start_date'][0]
            condition['end_date_s'] = condition['start_date'][1]
        if 'end_date' in list(condition.keys()):
            condition['start_date_e'] = condition['end_date'][0]
            condition['end_date_e'] = condition['end_date'][1]
        keys = ['user_filter', 'id', 'service_order_code', 'start_date_s', 'end_date_s',
                'start_date_e', 'end_date_e', 'service_project_id', 'service_worker_id', 'service_project_name', 'states']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_id').inner(order_ids))\
            .lookup_bill('PT_User', 'servicer_id', 'id', 'user') \
            .lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .lookup_bill('PT_Task', 'id', 'task_object_id', 'task')\
            .lookup_bill('PT_User', 'order.purchaser_id', 'id', 'purchaser_info')\
            .lookup_bill('PT_User', 'order.service_provider_id', 'id', 'service_provider_info')\
            .add_fields({'service_order_code': '$order.order_code',
                         'service_project_name': '$product.name',
                         'service_product_img': '$product.picture_collection',
                         'service_worker_id': '$user.name',
                         'service_provider_id': '$order.service_provider_id',
                         'purchaser_id': '$order.purchaser_id',
                         'task_begin_photo': self.ao.array_elemat('$task.begin_photo', 0),
                         'task_end_photo': self.ao.array_elemat('$task.end_photo', 0),
                         'task_state':  self.ao.array_elemat('$task.task_state', 0),
                         'task_name': '$task.task_name',
                         'task_address': '$task.task_address',
                         'task_id':  self.ao.array_elemat('$task.id', 0),
                         'task_remark': '$task.remark',
                         'purchaser_name': '$purchaser_info.name',
                         'service_provider_name': '$service_provider_info.name',
                         'purchaser_telephone': '$purchaser_info.personnel_info.telephone',
                         #  'start_date': self.ao.date_to_string('$start_date'),
                         #  'end_date': self.ao.date_to_string('$end_date'),
                         #  'start_date': self.ao.date_to_string('$start_date', '%Y-%m-%d %H:%M:%S'),
                         #  'end_date': self.ao.date_to_string('$end_date', '%Y-%m-%d %H:%M:%S')
                         })\
            .match_bill(C('service_order_code').like(values['service_order_code'])
                        & (C('id') == values['id'])
                        & (C('start_date') >= as_date(values['start_date_s']))
                        & (C('start_date') <= as_date(values['end_date_s']))
                        & (C('end_date') >= as_date(values['start_date_e']))
                        & (C('end_date') <= as_date(values['end_date_e']))
                        & (C('service_product_id') == values['service_project_id'])
                        & (C('task.task_state') == values['states'])
                        & (C('service_project_name').like(values['service_project_name']))
                        & (C('servicer_id') == values['service_worker_id']))

        # # 获取服务商超级管理员的角色ID
        # _filter_fwscg = MongoBillFilter()
        # _filter_fwscg.match_bill((C('role_type') == '服务商') & (
        #     C('name') == '服务商超级管理员')).project({'_id': 0})
        # res_fwscg = self.query(_filter_fwscg, 'PT_Role')

        # fwscg_role_id = res_fwscg[0]['id']

        # # 获取平台超级管理员的角色ID
        # _filter_ptcg = MongoBillFilter()
        # _filter_ptcg.match_bill((C('role_type') == '平台') & (
        #     C('name') == '超级管理员')).project({'_id': 0})
        # res_ptcg = self.query(_filter_ptcg, 'PT_Role')

        # ptcg_role_id = res_ptcg[0]['id']

        # 当前的角色ID
        # cur_role_id = get_current_role_id(self.session)
        _filter.project({'_id': 0, 'user._id': 0, 'order._id': 0,
                         'product._id': 0, 'task': 0, 'purchaser_info': 0, 'service_provider_info': 0})
        res = self.page_query(
            _filter, 'PT_Service_Record', page, count)
        # if res.get('result'):
        #     for result in res.get('result'):
        #         if result.get('purchaser_id'):
        #             _filter_pur = MongoBillFilter()
        #             _filter_pur.match_bill((C('id') == result.get(
        #                 'purchaser_id')[0])).project({'_id': 0})
        #             res_pur = self.query(_filter_pur, 'PT_User')
        #             result['purchaser_name'] = res_pur[0]['name'] if res_pur else ''
        #         if result.get('service_provider_id'):
        #             _filter_pro = MongoBillFilter()
        #             _filter_pro.match_bill((C('id') == result.get(
        #                 'service_provider_id')[0])).project({'_id': 0})
        #             res_pro = self.query(_filter_pro, 'PT_User')
        #             result['service_provider_name'] = res_pro[0]['name'] if res_pro else ''
        #         if result.get('task_begin_photo') and len(result.get('task_begin_photo')) > 0:
        #             result['task_begin_photo'] = result['task_begin_photo'][0]
        #         if result.get('task_end_photo') and len(result.get('task_end_photo')) > 0:
        #             result['task_end_photo'] = result['task_end_photo'][0]
        #         if result.get('task_state') and len(result.get('task_state')) > 0:
        #             result['task_state'] = result['task_state'][0]
        #         if result.get('task_id') and len(result.get('task_id')) > 0:
        #             result['task_id'] = result['task_id'][0]
        return res

    def get_min_data(self, data):
        return data + 'T00:00:00.000Z'

    def get_max_data(self, data):
        return data + 'T23:59:59.999Z'

    def get_service_record_list_all_app(self, order_ids, condition, page, count):

        condition['service_worker_id'] = get_current_user_id(self.session)
        return self.get_service_record_list_yh(order_ids, condition, page, count)

    def get_service_record_list_yh(self, org_list, condition, page, count):
        ''' 优化后的服务记录查询列表
        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        if 'start_date' in list(condition.keys()):
            start_date_e = condition['start_date'][0][0:10]
            end_date_e = condition['start_date'][1][0:10]
            condition['start_date_s'] = start_date_e
            condition['end_date_s'] = str(datetime.datetime.strptime(
                end_date_e, '%Y-%m-%d').date() + datetime.timedelta(days=1))
        if 'end_date' in list(condition.keys()):
            start_date_e = condition['end_date'][0][0:10]
            end_date_e = condition['end_date'][1][0:10]
            condition['start_date_e'] = start_date_e
            condition['end_date_e'] = str(datetime.datetime.strptime(
                end_date_e, '%Y-%m-%d').date() + datetime.timedelta(days=1))
        if 'order_create_date' in list(condition.keys()):
            start_date_e = condition['order_create_date'][0][0:10]
            end_date_e = condition['order_create_date'][1][0:10]
            condition['start_date_c'] = start_date_e
            condition['end_date_c'] = str(datetime.datetime.strptime(
                end_date_e, '%Y-%m-%d').date() + datetime.timedelta(days=1))
          #print('start_date_c>>>', condition['start_date_c'])
          #print('end_date_c>>>', condition['end_date_c'])
        if 'order_date' in condition.keys():
            condition['mix_order_data'] = self.get_min_data(
                condition['order_date'].split('T')[0])
            condition['max_order_data'] = self.get_max_data(
                condition['order_date'].split('T')[0])
        keys = ['user_filter', 'id', 'service_order_code',  'service_record_code', 'start_date_s', 'end_date_s', 'visit_status',
                'start_date_e', 'end_date_e', 'start_date_c', 'end_date_c', 'service_project_id', 'service_worker_id', 'service_project_name', 'status', 'mix_order_data', 'max_order_data', 'name_or_id_card', 'task_name', 'servicer_name', 'purchaser_name', 'purchaser_id_card', 'service_provider_id', 'social_worker_id']
        values = self.get_value(condition, keys)
        # 查询社工局
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)

        t1 = time.time()
        # 查询长者数据
        elder_ids = []
        worker_ids = []
        worker_data = {}
        elder_data = {}
        if condition.get('purchaser_name') or condition.get('purchaser_id_card') or condition.get('name_or_id_card') or condition.get('social_worker_id'):
            t1 = time.time()
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1')
                                     & (C('personnel_info.personnel_category') == '长者')
                                     & (C('name').like(values['purchaser_name']))
                                     & (C('id_card').like(values['purchaser_id_card']))
                                     & (C('admin_area_id').inner(admin_list))
                                     & ((C('name').like(values['name_or_id_card']))
                                        | (C('id_card').like(values['name_or_id_card'])))
                                     )\
                .project({'_id': 0})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])
            t2 = time.time()
          #print('查询长者数据》》', t2-t1)
        # 查询工作人员
        if condition.get('service_worker_id') or condition.get('servicer_name'):
            t1 = time.time()
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('personnel_type') == '1')
                                      & (C('personnel_info.personnel_category') == '工作人员')
                                      & (C('name').like(values['servicer_name']))
                                      & (C('id') == values['service_worker_id'])
                                      )\
                .project({'_id': 0})
            res_worker = self.query(
                _filter_worker, 'PT_User')
            if len(res_worker) > 0:
                for worker in res_worker:
                    worker_data[worker['id']] = worker
                    worker_ids.append(worker['id'])
            t2 = time.time()
          #print('查询工作人员数据》》', t2-t1)
        t7 = time.time()
        # 查询服务产品数据
        product_ids = []
        product_data = {}
        if condition.get('service_project_name'):
            _filter_product = MongoBillFilter()
            _filter_product.match_bill()\
                .project({'_id': 0})
            res_product = self.query(
                _filter_product, 'PT_Service_Product')
            if len(res_product) > 0:
                for product in res_product:
                    product_data[product['id']] = product
                    product_ids.append(product['id'])

            t8 = time.time()
          #print('查询服务产品数据》》', t8-t7)

        # 查询服务订单数据
        t7 = time.time()
        order_ids = []
        order_data = {}
        if condition.get('service_order_code') or len(elder_ids) > 0:
            if len(elder_ids) == 0:
                elder_ids = N()
            _filter_order = MongoBillFilter()
            _filter_order.match_bill((C('order_code').like(values['service_order_code'])) & (C('purchaser_id').inner(elder_ids)))\
                .project({'_id': 0})
            res_order = self.query(
                _filter_order, 'PT_Service_Order')
            if len(res_order) > 0:
                for order in res_order:
                    order_data[order['id']] = order
                    order_ids.append(order['id'])

            t8 = time.time()
          #print('查询服务订单数据》》', t8-t7)

        # 查询服务任务数据
        t7 = time.time()
        task_ids = []
        task_data = {}
        if condition.get('task_name'):
            _filter_task = MongoBillFilter()
            _filter_task.match_bill()\
                .project({'_id': 0})
            res_task = self.query(
                _filter_task, 'PT_Task')
            if len(res_task) > 0:
                for task in res_task:
                    task_data[task['task_object_id']] = task
                    task_ids.append(task['task_object_id'])

            t8 = time.time()
          #print('查询服务任务数据》》', t8-t7)
        # 服务商
        provider_data = {}
        provider_ids = []
        t5 = time.time()
        _filter_provider = MongoBillFilter()
        _filter_provider.match_bill((C('personnel_type') == '2')
                                    & (C('organization_info.personnel_category') == '服务商')
                                    & (C('organization_info.contract_status') == '签约')
                                    & (C('id') == values['service_provider_id']))\
            .project({'_id': 0})
        res_provider = self.query(
            _filter_provider, 'PT_User')
        if len(res_provider) > 0:
            for provider in res_provider:
                provider_ids.append(provider['id'])
                provider_data[provider['id']] = provider
        t7 = time.time()
      #print('查询服务商数据》》'+str(len(provider_ids)), t7-t5)

        _filter = MongoBillFilter()
        if condition.get('purchaser_name') or condition.get('purchaser_id_card') or condition.get('name_or_id_card') or condition.get('service_order_code') or condition.get('social_worker_id'):
            _filter.match(
                (C('order_id').inner(order_ids))
            )
        if condition.get('servicer_name') or condition.get('service_worker_id'):
            _filter.match(
                (C('servicer_id').inner(worker_ids))
            )
        if condition.get('task_name'):
            _filter.match(
                (C('id').inner(task_ids))
            )
        if condition.get('service_project_name'):
            _filter.match(
                (C('service_product_id').inner(product_ids))
            )
        _filter.match_bill((C('organization_id').inner(org_list))
                           & (C('organization_id').inner(provider_ids))
                           & (C('record_code').like(values['service_record_code']))
                           & (C('id') == values['id'])
                           & (C('service_product_id') == values['service_project_id'])
                           & (C('start_date') >= as_date(values['start_date_s']))
                           & (C('start_date') < as_date(values['end_date_s']))
                           & (C('end_date') >= as_date(values['start_date_e']))
                           & (C('end_date') < as_date(values['end_date_e']))
                           & (C('create_date') >= as_date(values['start_date_c']))
                           & (C('create_date') < as_date(values['end_date_c']))
                           & (C('create_date') >= as_date(values['mix_order_data']))
                           & (C('create_date') <= as_date(values['max_order_data']))
                           & (C('status') == values['status'])
                           & (C('visit_status') == values['visit_status']))
        _filter.sort({'create_date': -1, 'purchaser_name': 1})
        _filter.project({'_id': 0, 'GUID': 0, 'bill_operator': 0, 'version': 0, 'valid_bill_id': 0,
                         'order': 0, 'bill_status': 0, 'modify_date': 0, 'service_option': 0})
        res = self.page_query(
            _filter, 'PT_Service_Record', page, count)
        # print(_filter.filter_objects,666)
        res_order_ids = []
        res_task_ids = []
        res_product_ids = []
        res_elder_ids = []
        res_worker_ids = []
        res_comment_ids = []
        for i, x in enumerate(res['result']):
            # 产品
            if 'service_project_name' not in condition:
                if 'service_product_id' in res['result'][i]:
                    res_product_ids.append(
                        res['result'][i]['service_product_id'])
            # 服务评价(存服务记录的id)
            if 'id' in res['result'][i]:
                res_comment_ids.append(res['result'][i]['id'])
            # 订单
            if 'service_order_code' not in condition and len(elder_ids) <= 0:
                if 'order_id' in res['result'][i]:
                    res_order_ids.append(res['result'][i]['order_id'])

            # 任务（任务记录的也是服务记录Id）
            if 'task_name' not in condition:
                if 'id' in res['result'][i]:
                    res_task_ids.append(res['result'][i]['id'])

            # 服务人员
            if 'service_worker_id' not in condition and 'servicer_name' not in condition:
                if 'servicer_id' in res['result'][i]:
                    res_worker_ids.append(res['result'][i]['servicer_id'])
        # 服务产品
        if len(res_product_ids) > 0:
            t11 = time.time()
            _filter_product = MongoBillFilter()
            _filter_product.match_bill(C('id').inner(res_product_ids))\
                .project({'_id': 0})
            res_product = self.query(
                _filter_product, 'PT_Service_Product')
            if len(res_product) > 0:
                for product in res_product:
                    product_data[product['id']] = product
            t12 = time.time()
          #print('最后查询产品数据》》', t12-t11)
        # 工作人员
        if len(res_worker_ids) > 0:
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id').inner(res_worker_ids)))\
                .project({'_id': 0})
            res_worker = self.query(
                _filter_worker, 'PT_User')
            if len(res_worker) > 0:
                for worker in res_worker:
                    worker_data[worker['id']] = worker
        # 任务
        if len(res_task_ids) > 0:
            _filter_task = MongoBillFilter()
            _filter_task.match_bill((C('task_object_id').inner(res_task_ids)))\
                .project({'_id': 0})
            res_task = self.query(
                _filter_task, 'PT_Task')
            if len(res_task) > 0:
                for task in res_task:
                    task_data[task['task_object_id']] = task
        # 服务评价
        order_comment_data = {}
        if len(res_comment_ids) > 0:
            _filter_order_comment = MongoBillFilter()
            _filter_order_comment.match_bill((C('record_id').inner(res_comment_ids)))\
                .project({'_id': 0})
            res_order_comment = self.query(
                _filter_order_comment, 'PT_Service_Order_Comment')
            if len(res_order_comment) > 0:
                for order_comment in res_order_comment:
                    order_comment_data[order_comment['id']] = order_comment

        # 订单
        if len(res_order_ids) > 0:
            _filter_order = MongoBillFilter()
            _filter_order.match_bill(C('id').inner(res_order_ids))\
                .project({'_id': 0})
            res_order = self.query(
                _filter_order, 'PT_Service_Order')
            if len(res_order) > 0:
                for order in res_order:
                    order_data[order['id']] = order

        for i, x in enumerate(res['result']):
            # 产品
            if 'service_product_id' in res['result'][i] and res['result'][i]['service_product_id'] in product_data.keys():
                res['result'][i]['service_project_name'] = product_data[res['result']
                                                                        [i]['service_product_id']]['name']
                res['result'][i]['service_product_img'] = product_data[res['result']
                                                                       [i]['service_product_id']]['picture_collection']
            # 服务评价
            if 'id' in res['result'][i] and res['result'][i]['id'] in order_comment_data.keys():
                res['result'][i]['service_attitude'] = order_comment_data[res['result']
                                                                          [i]['id']]['service_attitude']
                res['result'][i]['service_quality'] = order_comment_data[res['result']
                                                                         [i]['id']]['service_quality']
                res['result'][i]['opinion_remarks'] = order_comment_data[res['result']
                                                                         [i]['id']]['opinion_remarks']

            # 订单
            if 'order_id' in res['result'][i] and res['result'][i]['order_id'] in order_data.keys():
                res['result'][i]['service_order_code'] = order_data[res['result']
                                                                    [i]['order_id']]['order_code']
                res['result'][i]['service_provider_id'] = order_data[res['result']
                                                                        [i]['order_id']]['service_provider_id']
                if 'purchaser_id' in order_data[res['result'][i]['order_id']].keys():
                    res['result'][i]['purchaser_id'] = order_data[res['result']
                                                                  [i]['order_id']]['purchaser_id']

                # 购买人
                if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] not in elder_data.keys():
                    res_elder_ids.append(
                        order_data[res['result'][i]['order_id']]['purchaser_id'])

            # 任务（任务记录的也是服务记录Id）
            if 'id' in res['result'][i] and res['result'][i]['id'] in task_data.keys():
                if 'start_position' in task_data[res['result'][i]['id']]:
                    res['result'][i]['start_position'] = task_data[res['result']
                                                                   [i]['id']]['start_position']
                if 'end_position' in task_data[res['result'][i]['id']]:
                    res['result'][i]['end_position'] = task_data[res['result']
                                                                 [i]['id']]['end_position']
                if 'begin_photo' in task_data[res['result'][i]['id']]:
                    res['result'][i]['begin_photo'] = task_data[res['result']
                                                                [i]['id']]['begin_photo']
                if 'end_photo' in task_data[res['result'][i]['id']]:
                    res['result'][i]['end_photo'] = task_data[res['result']
                                                              [i]['id']]['end_photo']
                if 'task_state' in task_data[res['result'][i]['id']]:
                    res['result'][i]['task_state'] = task_data[res['result']
                                                               [i]['id']]['task_state']
                if 'task_name' in task_data[res['result'][i]['id']]:
                    res['result'][i]['task_name'] = task_data[res['result']
                                                              [i]['id']]['task_name']
                if 'task_address' in task_data[res['result'][i]['id']]:
                    res['result'][i]['task_address'] = task_data[res['result']
                                                                    [i]['id']]['task_address']
                if 'id' in task_data[res['result'][i]['id']]:
                    res['result'][i]['task_id'] = task_data[res['result']
                                                            [i]['id']]['id']
                if 'remarks' in task_data[res['result'][i]['id']]:
                    res['result'][i]['task_remark'] = task_data[res['result']
                                                                [i]['id']]['remarks']
                    res['result'][i]['remarks'] = task_data[res['result']
                                                            [i]['id']]['remarks']

            # 服务人员
            if 'servicer_id' in res['result'][i] and res['result'][i]['servicer_id'] in worker_data.keys():
                res['result'][i]['servicer_name'] = worker_data[res['result']
                                                                [i]['servicer_id']]['name']

        # 购买长者
        if len(res_elder_ids) > 0:
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('id').inner(res_elder_ids)))\
                .project({'_id': 0})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
        for i, x in enumerate(res['result']):
            # 服务商
            if 'service_provider_id' in res['result'][i] and res['result'][i]['service_provider_id'] in provider_data.keys():
                res['result'][i]['service_provider_name'] = provider_data[res['result']
                                                                          [i]['service_provider_id']]['name']
            # 购买长者
            if 'purchaser_id' in res['result'][i] and res['result'][i]['purchaser_id'] in elder_data.keys():
                res['result'][i]['purchaser_name'] = elder_data[res['result']
                                                                [i]['purchaser_id']]['name']

                res['result'][i]['purchaser_id_card'] = elder_data[res['result']
                                                                      [i]['purchaser_id']]['id_card']
                if 'guardian_id_card' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                    res['result'][i]['guardian_id_card'] = elder_data[res['result']
                                                                      [i]['purchaser_id']]['personnel_info']['guardian_id_card']
                if 'guardian_name' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                    res['result'][i]['guardian_name'] = elder_data[res['result']
                                                                   [i]['purchaser_id']]['personnel_info']['guardian_name']
                if 'guardian_telephone' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                    res['result'][i]['guardian_telephone'] = elder_data[res['result']
                                                                        [i]['purchaser_id']]['personnel_info']['guardian_telephone']
                if 'telephone' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                    res['result'][i]['purchaser_telephone'] = elder_data[res['result']
                                                                         [i]['purchaser_id']]['personnel_info']['telephone']
                if 'family_name' in elder_data[res['result'][i]['purchaser_id']]['personnel_info'].keys():
                    res['result'][i]['family_name'] = elder_data[res['result']
                                                                 [i]['purchaser_id']]['personnel_info']['family_name']
        return res

    def get_service_record_list_all(self, order_ids, condition, page, count):
        '''app获取服务记录列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''

        if 'start_date' in list(condition.keys()):
            condition['start_date_s'] = condition['start_date'][0]
            condition['end_date_s'] = condition['start_date'][1]
        if 'end_date' in list(condition.keys()):
            condition['start_date_e'] = condition['end_date'][0]
            condition['end_date_e'] = condition['end_date'][1]
        if 'order_date' in condition.keys():
            condition['mix_order_data'] = self.get_min_data(
                condition['order_date'].split('T')[0])
            condition['max_order_data'] = self.get_max_data(
                condition['order_date'].split('T')[0])
        keys = ['user_filter', 'id', 'service_order_code', 'start_date_s', 'end_date_s',
                'start_date_e', 'end_date_e', 'service_project_id', 'service_worker_id', 'service_project_name', 'status', 'mix_order_data', 'max_order_data', 'name_or_idcard', 'task_name', 'servicer_name', 'purchaser_name', 'purchaser_id_card']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('servicer_id') == values['service_worker_id'])
                           & (C('id') == values['id'])
                           & (C('start_date') >= as_date(values['start_date_s']))
                           & (C('start_date') <= as_date(values['end_date_s']))
                           & (C('end_date') >= as_date(values['start_date_e']))
                           & (C('end_date') <= as_date(values['end_date_e']))
                           & (C('create_date') >= as_date(values['mix_order_data']))
                           & (C('create_date') <= as_date(values['max_order_data']))
                           & (C('status') == values['status']))\
            .lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
            .lookup_bill('PT_Service_Order_Comment', 'id', 'record_id', 'comment_info')\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .lookup_bill('PT_Task', 'id', 'task_object_id', 'task')\
            .lookup_bill('PT_User', 'order.purchaser_id', 'id', 'purchaser_info')\
            .lookup_bill('PT_User', 'servicer_id', 'id', 'servicer_info')\
            .lookup_bill('PT_User', 'order.service_provider_id', 'id', 'service_provider_info')\
            .add_fields({'service_order_code': self.ao.array_elemat('$order.order_code', 0),
                         'service_project_name': self.ao.array_elemat('$product.name', 0),
                         'service_product_img': self.ao.array_elemat('$product.picture_collection', 0),
                         'service_provider_id': self.ao.array_elemat('$order.service_provider_id', 0),
                         'purchaser_id': self.ao.array_elemat('$order.purchaser_id', 0),
                         'start_position': self.ao.array_elemat('$task.start_position', 0),
                         'end_position': self.ao.array_elemat('$task.end_position', 0),
                         'begin_photo': self.ao.array_elemat('$task.begin_photo', 0),
                         'end_photo': self.ao.array_elemat('$task.end_photo', 0),
                         'task_state':  self.ao.array_elemat('$task.task_state', 0),
                         'task_name':  self.ao.array_elemat('$task.task_name', 0),
                         'task_address':  self.ao.array_elemat('$task.task_address', 0),
                         'task_id':  self.ao.array_elemat('$task.id', 0),
                         'task_remark':  self.ao.array_elemat('$task.remarks', 0),
                         'remarks':  self.ao.array_elemat('$task.remarks', 0),
                         'servicer_name':  self.ao.array_elemat('$servicer_info.name', 0),
                         'purchaser_name':  self.ao.array_elemat('$purchaser_info.name', 0),
                         'purchaser_id_card':  self.ao.array_elemat('$purchaser_info.id_card', 0),
                         'service_provider_name':  self.ao.array_elemat('$service_provider_info.name', 0),
                         'purchaser_telephone':  self.ao.array_elemat('$purchaser_info.personnel_info.telephone', 0),
                         'classification_name':  self.ao.array_elemat('$purchaser_info.personnel_info.classification_name', 0),
                         'service_attitude': self.ao.array_elemat("$comment_info.service_attitude", 0),
                         'service_quality': self.ao.array_elemat("$comment_info.service_quality", 0),
                         'opinion_remarks': self.ao.array_elemat("$comment_info.opinion_remarks", 0),
                         #  'start_date': self.ao.date_to_string('$start_date'),
                         #  'end_date': self.ao.date_to_string('$end_date'),
                         #  'start_date': self.ao.date_to_string('$start_date', '%Y-%m-%d %H:%M:%S'),
                         #  'end_date': self.ao.date_to_string('$end_date', '%Y-%m-%d %H:%M:%S')
                         })\
            .match_bill(C('service_order_code').like(values['service_order_code'])
                        & (C('service_product_id') == values['service_project_id'])
                        & (C('service_project_name').like(values['service_project_name']))
                        & (C('task_name').like(values['task_name']))
                        & (C('servicer_name').like(values['servicer_name']))
                        & (C('purchaser_info.name').like(values['purchaser_name']))
                        & (C('purchaser_info.id_card').like(values['purchaser_id_card']))
                        & (C('purchaser_info.name').like(values['name_or_idcard']))
                        & (C('purchaser_info.id_card').like(values['name_or_idcard'])))
        _filter.sort({'create_date': -1, 'purchaser_name': 1})
        _filter.project({'_id': 0, 'GUID': 0, 'bill_operator': 0, 'version': 0, 'valid_bill_id': 0, 'order': 0, 'bill_status': 0, 'modify_date': 0, 'service_option': 0,
                         'product': 0, 'task': 0, 'servicer_info': 0, 'purchaser_info': 0, 'service_provider_info': 0, 'comment_info': 0})
        res = self.page_query(
            _filter, 'PT_Service_Record', page, count)

        return res

    def update_service_record(self, service_record):
        '''# 新增/修改服务记录'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in service_record.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.serviceRecord.value, service_record, 'PT_Service_Record')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.serviceRecord.value, service_record, 'PT_Service_Record')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def get_record_evaluate_list(self, condition, page=None, count=None):
        '''获取服务记录的评价列表
        '''
        data = {}
        if condition.get('record_id'):
            _filter = MongoBillFilter()
            _filter.match_bill(
                (C('id') == condition.get('record_id'))).project({"_id": 0})
            res = self.query(_filter, 'PT_Service_Record')
            if res:
                data['comment_object_id'] = res[0].get('item_id')
            else:
                return {'result': [], 'total': 0}
        res_com = self.comment_service.get_comment_list(N(), data, page, count)
        # if not res_com['result']:
        #     return {'result':[],'total':0}
        return res_com

    def get_record_option_list(self, condition, page=None, count=None):
        '''获取服务记录的服务选项列表
        '''
        res = self.get_service_record_list_all(N, condition, 1, 1)
        option_list = []
        total = 0
        if res['result']:
            if res['result'][0].get('option_list'):
                option_list = res['result'][0].get('option_list')
                total = len(option_list)
        return {'result': option_list, 'total': total}

    def delete_service_record(self, service_record_ids):
        '''删除服务记录接口

        Arguments:
            servicr_option_ids   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(service_record_ids, str):
                ids.append(service_record_ids)
            else:
                ids = service_record_ids
            for service_record_ids in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Service_Record', {
                    'id': service_record_ids, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.serviceRecord.value, data[0], 'PT_Service_Record')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def invoking_financial(self, record_ids):
        '''发送服务记录到财务模块'''
        res = 'Fail'
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(record_ids)) \
               .inner_join_bill('PT_Service_Order', 'order_id', 'id', 'order') \
               .inner_join('IEC_Check_In', 'order_id', 'order_id', 'check')\
               .inner_join_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
            .lookup_bill('PT_Service_Item', 'product.service_item_id', 'id', 'item')\
            .add_fields({
                'purchaser_id': '$check.user_id',
                'item_name': '$item.name',
                'organization_id': '$item.organization',
                'create_date': self.ao.date_to_string('$create_date'),
                'modify_date': self.ao.date_to_string('$modify_date')})\
            .lookup_bill('PT_User', 'purchaser_id', 'id', 'user')\
            .add_fields({
                'account': '$user.personnel_info.card_number',
                'account_name': '$user.name'})\
            .project({'_id': 0, 'order._id': 0, 'item._id': 0, 'user._id': 0, 'item._id': 0, 'check._id': 0, 'product._id': 0})
        record_datas = self.query(_filter, "PT_Service_Record")
        financial_list = []
        success_ids = []
        fail_ids = []
        org_id = get_current_organization_id(self.session)
        if record_datas:
            for record in record_datas:
                purchaser_id = ''
                if len(record['purchaser_id']) > 0:
                    # print('用户id  ',record['purchaser_id'])
                    purchaser_id = record['purchaser_id']
                # account = ''
                # if len(record['account']) > 0:
                #     account = record['account'][0]
                # else:
                #     fail_ids.append(record['id'])
                #     continue
                # account_name = ''
                # if len(record['account_name']) > 0:
                #     account_name = record['account_name'][0]
                # else:
                #     fail_ids.append(record['id'])
                #     continue
                item_name = ''
                if len(record['item_name']) > 0:
                    item_name = record['item_name'][0]
                # organization_id = ''
                # if len(record['organization_id']) > 0:
                #     organization_id = record['organization_id'][0]
                financial = Financial(
                    purchaser_id, org_id, record['valuation_amount'], item_name)
                financial_list.append(financial.to_dict())
                success_ids.append(record['id'])
            # print(financial_list, 'financial_list>>>>>>')
            # print(success_ids, 'success_ids>>>>>')
            # print(fail_ids, 'fail_ids>>>>')
            result = self.financial_account_service.add_bank_charge_data(
                financial_list)
            recode = []
            table_name = []
            if fail_ids:
                for recode_id in fail_ids:
                    recode.append(
                        {'id': recode_id, 'cost_status': CostStatus.fail})
                    table_name.append('PT_Service_Record')
            if success_ids:
                for recode_success_id in success_ids:
                    recode.append({'id': recode_success_id,
                                   'cost_status': CostStatus.success})
                    table_name.append('PT_Service_Record')
            bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                        TypeId.serviceRecord.value, recode, table_name)
            if result:
                res = 'Success'
        return res

    def get_servicer_report_month_cg(self, org_list, condition):
        res = 'Fail'

        # 查询出佛山市下的所有的社区
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id').inner(org_list)) & (
            C('type') == '社区')).project({'_id': 0, 'id': 1, 'name': 1})
        shequ = self.query(
            _filter, "PT_Administration_Division")
        shequ_ids = []
        shequ_data = {}
        for index in range(len(shequ)):
            shequ_ids.append(shequ[index]['id'])
            shequ_data[shequ[index]['id']] = {
                'id': shequ[index]['id'],
                'name': shequ[index]['name'],
                'org': []
            }

        # 找出社区下面的所属机构
        _filter_fws = MongoBillFilter()
        _filter_fws.match_bill((C('admin_area_id').inner(shequ_ids)) & (C('personnel_type') == '2') & (
            C('organization_info.personnel_category') == '服务商')).project({'_id': 0, 'id': 1})
        fws = self.query(
            _filter_fws, "PT_User")
        fws_ids = []
        for index in range(len(fws)):
            fws_ids.append(fws[index]['id'])

        # 根据服务商查询出相关服务记录
        _filter_record = MongoBillFilter()
        _filter_record.match_bill((C('organization_id').inner(fws_ids))).project(
            {'_id': 0, 'id': 1, 'create_date': 1, 'order_id': 1})
        record = self.query(
            _filter_record, "PT_Service_Record")
        order_id = []
        record_data = {}
        for index in range(len(record)):
            order_id.append(record[index]['order_id'])
            record_data[record[index]['order_id']] = record[index]

        # 根据订单表id找出购买的长者
        user_id = []
        _filter_order = MongoBillFilter()
        _filter_order.match_bill((C('id').inner(order_id))).project(
            {'_id': 0, 'id': 1, 'purchaser_id': 1})
        order = self.query(
            _filter_order, "PT_Service_Order")
        for index in range(len(order)):
            if 'purchaser_id' in order[index].keys():
                record_data[order[index]['id']
                            ]['user_id'] = order[index]['purchaser_id']
                if order[index]['purchaser_id'] not in user_id:
                    user_id.append(order[index]['purchaser_id'])

        # 查询出长者的类型id
        type_id = []
        user_data = {}
        _filter_old = MongoBillFilter()
        _filter_old.match_bill((C('id').inner(user_id))).project(
            {'_id': 0, 'id': 1, 'personnel_info': 1})
        old = self.query(
            _filter_old, "PT_User")

        for index in range(len(old)):
            if 'personnel_classification' in old[index]['personnel_info'] and old[index]['personnel_info']['personnel_classification'] != '' and old[index]['personnel_info']['personnel_classification'] != None:
                type_id.append(old[index]['personnel_info']
                               ['personnel_classification'])
                user_data[old[index]['personnel_info']
                          ['personnel_classification']] = old[index]

        # 查询出id的类型名称
        _filter_type = MongoBillFilter()
        _filter_type.match_bill((C('id').inner(type_id))).project(
            {'_id': 0, 'name': 1, 'id': 1})
        type_info = self.query(
            _filter_type, "PT_Personnel_Classification")

        for index in range(len(type_info)):
            user_data[type_info[index]['id']
                      ]['type'] = type_info[index]['name']

        # for index in range(len(record_data)):
        #     if 'user_id' in record_data[index].keys():
        #         for index2 in range(len(user_data)):
        #             if 'type' in user_data[index2].keys():
        #                 if record_data[index]['user_id'] == user_data[index2]['id']:
        #                     record_data[index]['type'] = user_data[index2]['type']

        a = {
            '1': record_data,
            '2': user_data
        }

        return a

    def get_servicer_report_month(self, condition):
        # 查询服务记录表
        arr = []
        _filter = MongoBillFilter()
        _filter.sort({'create_date': 1})\
            .project({'_id': 0})
        res_list = self.query(_filter, 'PT_Service_Record')
        # print(res_list[1]['create_date'].year)
        # print(res_list[len(res_list)-1]['create_date'].year)
        year = res_list[1]['create_date'].year
        while(year <= res_list[len(res_list)-1]['create_date'].year):
            # 根据年份分类
            month = 1
            while(month <= 12):
                if month < 12:
                    start = datetime.datetime(year, month, 1, 0, 0, 0)
                    end = datetime.datetime(
                        year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
                else:
                    start = datetime.datetime(year, month, 1, 0, 0, 0)
                    end = datetime.datetime(
                        year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
                _filter = MongoBillFilter()
                _filter.match_bill((C('create_date') > start)
                                   & (C('create_date') < end))\
                    .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
                    .add_fields({'purchaser_id': '$order.purchaser_id'})\
                    .lookup_bill('PT_User', 'purchaser_id', 'id', 'user')\
                    .match_bill((C('user.organization_info.personnel_category') == '服务商'))\
                    .add_fields({'admin_area_id': '$user.admin_area_id'})\
                    .lookup_bill('PT_Administration_Division', 'admin_area_id', 'id', 'area')\
                    .add_fields({'parent_id': '$area.parent_id'})\
                    .lookup_bill('PT_Administration_Division', 'parent_id', 'id', 'parent_area')\
                    .add_fields({
                        'shequ': '$area.name',
                        'zhenjie': '$parent_area.name',
                        'valuation_amount': self.ao.switch([self.ao.case(((F('valuation_amount') == None)), 0.0)], '$valuation_amount'),
                    })\
                    .project({'_id': 0, 'order._id': 0, 'user._id': 0, 'area._id': 0, 'parent_area._id': 0})\
                    .group(
                        {
                            'purchaser_id': '$purchaser_id',
                            'shequ': '$shequ',
                            'zhenjie': '$zhenjie'
                        },
                        [{
                            'count': self.ao.summation('$valuation_amount')
                        }])\
                    .group(
                        {
                            'shequ': '$shequ',
                            'zhenjie': '$zhenjie'
                        },
                        [{
                            'count': self.ao.summation('$count')
                        },
                            {
                            'num': self.ao.summation(1)
                        }])\

                result = self.query(_filter, 'PT_Service_Record')
                # print("哈哈哈哈哈哈哈哈哈哈哈哈", len(result))
                for item in result:
                    item['date'] = str(year) + '-' + str(month)
                arr.append({'res': result})
                month = month + 1
            year = year + 1
        return arr

    def get_order_report(self, org_list, condition, page, count):
        keys = ['org_id']
        values = self.get_value(condition, keys)
        res = {'result': []}
        _filter = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyymm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            _filter.match_bill((C('create_date') > start)
                               & (C('create_date') < end))
        _filter.match((C('bill_status') == 'valid'))\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'order_id': '$order.id'})\
            .add_fields({'service_provider_id': '$order.service_provider_id'})\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'user')\
            .match_bill((C('user.organization_info.personnel_category') == '服务商') & (C('service_provider_id') == values['org_id']))\
            .add_fields({'service_provider_name': '$user.name'})\
            .add_fields({'current_year': self.ao.to_string(self.ao.year('$create_date'))})\
            .add_fields({'current_month': self.ao.to_string(self.ao.month('$create_date'))})\
            .add_fields({'month': self.ao.concat([('$current_year'), '-', ('$current_month')])})\
            .project({'month': 1, 'valuation_amount': 1, 'order_id': 1, 'service_provider_name': 1})\
            .group(
            {
                'service_provider_name': '$service_provider_name',
                'order_id': '$order_id',
                'year_month': '$month',
            },
            [{
                'valuation_amount': self.ao.summation('$valuation_amount')
            }])\
            .group(
            {
                'service_provider_name': '$service_provider_name',
                'year_month': '$year_month',
            },
            [{
                'order_quantity': self.ao.summation(1)
            }, {
                'valuation_amount_total': self.ao.summation('$valuation_amount')
            }])\
            .sort({'year_month': 1})
        res_old_age = self.query(_filter, 'PT_Service_Record')
        res['result'] = res_old_age
        return res

    def get_order_statistics(self, condition, page, count):
        # 订单统计

        keys = ['org_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyymm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            _filter.match_bill((C('create_date') > start)
                               & (C('create_date') < end))
        _filter.match_bill((C('service_provider_id') == values['org_id']))\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'org')\
            .match_bill((C('org.organization_info.personnel_category') == '服务商'))\
            .add_fields({'org_name': '$org.name'})\
            .add_fields({'current_year': self.ao.to_string(self.ao.year('$create_date'))})\
            .add_fields({'current_month': self.ao.to_string(self.ao.month('$create_date'))})\
            .add_fields({'month': self.ao.concat([('$current_year'), '-', ('$current_month')])})\
            .add_fields({
                        'amout': self.ao.switch([self.ao.case(((F('amout') == None)), 0.0)], '$amout'),
                        })\
            .add_fields({
                        'bt_order_num': self.ao.switch([self.ao.case(((F('buy_type') == '补贴账户')), 1)], 0),
                        'bt_amout': self.ao.switch([self.ao.case(((F('buy_type') == '补贴账户')), '$amout')], 0),
                        })\
            .add_fields({
                        'zf_order_num': self.ao.switch([self.ao.case(((F('buy_type') == '自费')), 1)], 0),
                        'zf_amout': self.ao.switch([self.ao.case(((F('buy_type') == '自费')), '$amout')], 0),
                        })\
            .add_fields({
                        'cs_order_num': self.ao.switch([self.ao.case(((F('buy_type') == '慈善账户')), 1)], 0),
                        'cs_amout': self.ao.switch([self.ao.case(((F('buy_type') == '慈善账户')), '$amout')], 0),
                        })\
            .group(
                {
                    'service_provider_id': '$service_provider_id',
                    'purchaser_id': '$purchaser_id',
                    'org_name': '$org_name',
                    'org': '$org',
                    'bt_order_num': '$bt_order_num',
                    'bt_amout': '$bt_amout',
                    'zf_order_num': '$zf_order_num',
                    'zf_amout': '$zf_amout',
                    'cs_order_num': '$cs_order_num',
                    'cs_amout': '$cs_amout',
                    'year_month': '$month',
                },
                [{
                    'total_order_num': self.ao.summation(1)
                },
                    {
                    'total_amout': self.ao.summation('$amout')
                },
                    {
                    'bt_order_num': self.ao.summation('$bt_order_num')
                },
                    {
                    'bt_amout': self.ao.summation('$bt_amout')
                },
                    {
                    'zf_order_num': self.ao.summation('$zf_order_num')
                },
                    {
                    'zf_amout': self.ao.summation('$zf_amout')
                },
                    {
                    'cs_order_num': self.ao.summation('$cs_order_num')
                },
                    {
                    'cs_amout': self.ao.summation('$cs_amout')
                }])\
            .group(
                {
                    'service_provider_id': '$service_provider_id',
                    'org_name': '$org_name',
                    'org': '$org',
                    'year_month': '$year_month',
                },
                [{
                    'total_order_num': self.ao.summation('$total_order_num')
                },
                    {
                    'buy_people': self.ao.summation(1)
                },
                    {
                    'total_amout': self.ao.summation('$total_amout')
                },
                    {
                    'bt_order_num': self.ao.summation('$bt_order_num')
                },
                    {
                    'bt_amout': self.ao.summation('$bt_amout')
                },
                    {
                    'zf_order_num': self.ao.summation('$zf_order_num')
                },
                    {
                    'zf_amout': self.ao.summation('$zf_amout')
                },
                    {
                    'cs_order_num': self.ao.summation('$cs_order_num')
                },
                    {
                    'cs_amout': self.ao.summation('$cs_amout')
                }])\
            .project({'_id': 0, 'org._id': 0})
        fws_list = self.page_query(
            _filter, 'PT_Service_Order', page, count)
        return fws_list

    def get_app_order_list(self, org_list, condition, page, count):

        if 'time' in list(condition.keys()):
            condition['start_date'] = condition['time'][0]
            condition['end_date'] = condition['time'][1]
        keys = ['start_date', 'end_date', 'org_id',
                'id', 'account_phone', 'buy_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('create_method') == 'app')
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'servicer_id', 'id', 'user')\
            .add_fields({'account': '$user.login_info.login_check.account_name'})\
            .unwind('account')\
            .add_fields({'phone': '$user.personnel_info.telephone'})\
            .add_fields({'elder_name': '$user.name'})\
            .match_bill((C('account').like(values['account_phone']))
                        | (C('phone').like(values['account_phone'])))\
            .lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
            .add_fields({'product_name': '$product.name'})\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'order_code': '$order.order_code'})\
            .add_fields({'buy_type': '$order.buy_type'})\
            .match_bill((C('buy_type').like(values['buy_type'])))\
            .add_fields({'service_provider_id': '$order.service_provider_id'})\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'org')\
            .add_fields({'org_name': '$org.name'})\
            .project({'_id': 0, 'user._id': 0, 'product._id': 0, 'order._id': 0, 'org._id': 0})
        fws_list = self.page_query(
            _filter, 'PT_Service_Record', page, count)
        return fws_list

    def get_cost_statistics(self, org_list, condition, page, count):

        if 'time' in list(condition.keys()):
            condition['start_date'] = condition['time'][0]
            condition['end_date'] = condition['time'][1]
        keys = ['start_date', 'end_date', 'org_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'org')\
            .add_fields({'type': '$org.organization_info.personnel_category'})\
            .add_fields({'org_name': '$org.name'})\
            .add_fields({'org_address': '$org.organization_info.address'})\
            .match_bill((C('type') == '幸福院'))\
            .add_fields({'current_year': self.ao.to_string(self.ao.year('$create_date'))})\
            .add_fields({'current_month': self.ao.to_string(self.ao.month('$create_date'))})\
            .add_fields({'month': self.ao.concat([('$current_year'), '-', ('$current_month')])})\
            .group(
                {
                    'service_provider_id': '$service_provider_id',
                    'org_name': '$org_name',
                    'org': '$org',
                    'year_month': '$month',
                    'org_address': '$org_address'
                },
                [{
                    'rt_total_order_num': self.ao.summation(1)
                },
                    {
                    'rt_total_amout': self.ao.summation('$amout')
                }, ])\
            .project({'_id': 0, 'org._id': 0})
        order_list = self.page_query(
            _filter, 'PT_Service_Order', page, count)
        return order_list
