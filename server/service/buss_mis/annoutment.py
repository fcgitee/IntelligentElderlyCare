from ...service.common import get_current_user_id, operation_result, find_data, get_common_project
from ...pao_python.pao.service.data.mongo_db import MongoService, C
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, string_to_date_only
from ...service.mongo_bill_service import MongoBillFilter
import datetime
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:04:58
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\article_type.py
'''
# -*- coding: utf-8 -*-

'''
文章类型函数
'''


class AnnoutmentService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd,)
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_annoutment_list_all(self, org_list, condition, page, count):
        keys = ['id', 'name', 'modify_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if condition.get('modify_date'):
            condition['modify_date'] = string_to_date_only(
                condition['modify_date'].split('T')[0])
            end_date = condition['modify_date'] + datetime.timedelta(days=1)
            _filter.match(
                (C('modify_date') >= condition['modify_date'])
                & (C('modify_date') <= end_date)
            )
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('organization_id').inner(org_list)
               & (C('name').like(values['name']))
               )
        )\
            .inner_join_bill('PT_User', 'operator_id', 'id', 'user')\
            .sort({'modify_date': -1})\
            .project({
                '_id': 0,
                'id': 1,
                'name': 1,
                'user.id': 1,
                'user.name': 1,
                'create_date': 1,
                'modify_date': 1,
            })
        res = self.page_query(_filter, 'PT_Annoutment', page, count)
        return res

    def update_annoutment(self, condition):
        ''' 新增/修改系统公告'''
        condition.update(operator_id=get_current_user_id(self.session))
        flag = OperationType.update.value if condition.get(
            'id') else OperationType.add.value
        bill_id = self.bill_manage_service.add_bill(flag,
                                                    TypeId.announcement.value, condition, 'PT_Annoutment')
        return operation_result(bill_id)

    def delete_annoutment(self, condition):
        """删除文章类型
        Arguments:
        article_type_ids   {ids}      数据id
        """
        res = 'Fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(condition, str):
                ids.append(condition)
            else:
                ids = condition
            for annoutment_id in ids:
                # 查询被删除的数据信息
                data = find_data(
                    db,
                    "PT_Annoutment",
                    {"id": annoutment_id, "bill_status": Status.bill_valid.value},
                )
                if len(data) > 0:
                    self.bill_manage_service.add_bill(
                        OperationType.delete.value,
                        TypeId.announcement.value,
                        data[0],
                        "PT_Annoutment",
                    )
            res = "Success"
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
