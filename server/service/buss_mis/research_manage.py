'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 17:14:46
@LastEditTime: 2019-12-05 16:00:40
@LastEditors: Please set LastEditors
'''
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from enum import Enum
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, get_cur_time
from ...service.mongo_bill_service import MongoBillFilter
# -*- coding: utf-8 -*-

'''
调研系统服务
'''


class ResearchManageService(MongoService):
    ''' 调研系统服务 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session

    def update_research_data(self, research_data):
        '''# 新增/修改调研数据'''
        res = 'Fail'
        #research = get_info(research)

        def process_func(db):
            nonlocal res
            if 'id' in research_data.keys():
                backVal = update_data(db, 'PT_Research_Data', research_data, {
                                      'id': research_data['id']})
                if backVal:
                    res = 'Success'
            else:
                data_info = get_info(research_data, self.session)
                backVal = insert_data(db, 'PT_Research_Data', data_info)
                if backVal:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
