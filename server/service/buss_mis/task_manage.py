# -*- coding: utf-8 -*-

'''
任务管理函数
'''
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
import pandas as pd
import uuid
import datetime
import re
import hashlib
from enum import Enum
from ...service.mongo_bill_service import MongoBillFilter
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ..buss_pub.personnel_organizational import UserType
from server.pao_python.pao.data import process_db, get_cur_time
from ...service.common import get_current_user_id, get_current_role_id
from ...service.app.my_order import RecordStatus
import datetime


class TaskState(Enum):
    '''任务状态类型枚举'''
    To_be_processed = '待处理'
    Failed_examine = '审核未通过'
    Adopt_examine = '审核通过'
    To_be_receive = '待接收'
    Ongoing = '服务中'
    Rejected = '已拒绝'
    Completed = '已完成'
    Evaluated = '已评价'


class UrgentLevel(Enum):
    '''紧急程度枚举
    '''
    Urgent = '紧急'
    Normal = '一般'


class TaskManageService(MongoService):
    ''' 任务管理 '''
    Task = 'PT_Task'
    Task_type = 'PT_Task_Type'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def update_task_type(self, data):
        '''新增或者修改任务类型数据'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.taskType.value, data, self.Task_type)
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.taskType.value, data, self.Task_type)
            if bill_id:
                res = 'Success'
        return res

    def get_task_type_list(self, org_list, condition, page, count):
        '''获取任务类型列表
        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('name').like(values['name']))
                           & (C('organization_id').inner(org_list)))\
            .project({'_id': 0})
        res = self.page_query(_filter, self.Task_type, page, count)
        return res

    def input_task_type(self):
        '''任务类型下拉框数据'''
        _filter = MongoBillFilter()
        _filter.match_bill()\
               .project({'_id': 0})
        res = self.query(_filter, self.Task_type)
        return res

    def input_task_urgent(self):
        '''任务紧急程度下拉框数据'''
        res = []
        for i in UrgentLevel:
            res.append(i.value)
        return res

    def input_person(self):
        '''人员姓名下拉框数据'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('personnel_type') == UserType.Personnel)\
               .project({'_id': 0, 'id': 1, 'name': 1})
        res = self.query(_filter, 'PT_User')
        return res

    def add_new_task(self, data, state):
        '''增加新任务（两种状态，一种为待接收，一种为待处理
        Arguments：
            data {dict} 任务数据，包含关键字：send_user_id(发送任务人的id)
        '''
        res = 'Fail'
        if state == TaskState.To_be_receive.value:
            operation = '接收任务'
        else:
            operation = '创建任务'
        data = self.__add_process_record(data, operation)  # 增加过程记录
        data['task_state'] = state  # 增加状态，待处理
        # 创建任务的人
        data['task_creator'] = get_current_user_id(self.session)
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.task.value, data, self.Task)
        if bill_id:
            res = 'Success'
        return res

    def __add_process_record(self, data, operation):
        date = datetime.datetime.now()
        # 该字段在审核不通过和拒绝接收任务时出现，需添加在过程记录字段中
        reason = data.pop('task_fail_reason', None)
        i_record = {'id': str(uuid.uuid1()), 'user_id': get_current_user_id(self.session),
                    'user_operate': operation, 'date': date, 'record_message': reason}
        if 'id' in data:
            _filter = MongoBillFilter()
            _filter.match_bill(C('id') == data['id'])
            res = self.query(_filter, self.Task)
            if len(res) > 0:
                # 没有的话，就给个空的，防止报错
                if 'task_process_record' not in res[0] or res[0]['task_process_record'] == None:
                    res[0]['task_process_record'] = []

                task_process_record = res[0]['task_process_record']
                task_process_record.append(i_record)
                data['task_process_record'] = task_process_record
            else:
                return 'Fail'
        else:
            data['task_process_record'] = [i_record]
        return data

    def task_examine(self, data, examine_res):
        '''任务审核,
        Arges:
        data : {'ids':['','',''],'task_fail_reason':''}
        examine_res : 为true时即为通过，为false时即为不通过
        '''
        res = 'Fail'
        bill_id_list = []
        examine_list = []
        for i in data['ids']:
            examine_data = {'id': i}
            if examine_res == False:
                examine_data['task_fail_reason'] = data['task_fail_reason']
            examine_list.append(
                self.__add_process_record(examine_data, '审核任务'))

        for examine_item in examine_list:
            if examine_item != 'Fail':
                if examine_res:
                    examine_item['task_state'] = TaskState.Adopt_examine.value
                else:
                    examine_item['task_state'] = TaskState.Failed_examine.value
                bill_id_list.append(self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.task.value, examine_item, self.Task))
        if False not in bill_id_list:
            res = 'Success'
        return res

    def task_receive(self, data, receive_res):
        '''接收任务,receive_res为true时即为接受任务，为false时即为拒绝任务'''
        res = 'Fail'
        data = self.__add_process_record(data, '接收任务')
        if data != 'Fail':
            if receive_res:
                data['task_state'] = TaskState.Ongoing.value
            else:
                data['task_state'] = TaskState.Rejected.value
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.task.value, data, self.Task)
            if bill_id:
                res = 'Success'
        return res

    def task_appoint(self, data):
        '''
        指派任务
        Arges:
        data : {'ids':['','',''],'task_receive_person':''}
        '''
        res = 'Fail'
        bill_id_list = []
        appoint_list = []
        for i in data['ids']:
            receive_data = {
                'id': i, 'task_receive_person': data['task_receive_person']}
            appoint_list.append(
                self.__add_process_record(receive_data, '指派任务'))

        for appoint_item in appoint_list:
            if appoint_item != 'Fail':
                appoint_item['task_state'] = TaskState.To_be_receive.value
                bill_id_list.append(self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.task.value, appoint_item, self.Task))
        if False not in bill_id_list:
            res = 'Success'
        return res

    def task_complete(self, data):
        '''完成任务'''
        res = 'Fail'
        data = self.__add_process_record(data, '完成任务')
        if data != 'Fail':
            data['task_state'] = TaskState.Completed.value
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.task.value, data, self.Task)
            if bill_id:
                res = 'Success'
        return res

    def back_start_service_order_record(self, record_id):
        '''撤回开始执行的服务记录'''
        res = 'Fail'
        EMPTY_DATA = '没有找到该数据'
        record_data = {}
        # 获取订单数据
        _fillter_record = MongoBillFilter()
        _fillter_record.match_bill((C('id') == record_id))\
            .project({'_id': 0})
        res_record = self.query(_fillter_record, 'PT_Service_Record')
        if len(res_record) > 0:
            order_id = res_record[0]['order_id']
            update_data = []
            update_table = []
            record_data['id'] = record_id
            record_data['start_date'] = None
            record_data['status'] = '未服务'
            # 服务中需要判断服务工单是否都完成了
            _fillter_other = MongoBillFilter()
            _fillter_other.match_bill((C('order_id') == order_id) & (C('id') != record_id) & ((C('status') == '已完成') | (C('status') == '服务中')))\
                .project({'_id': 0})
            other_record = self.query(_fillter_other, 'PT_Service_Record')
            # 如果该服务记录是唯一一个服务中的记录，就把订单改为未服务
            if len(other_record) == 0:
                update_data.append({'id': order_id, 'status': '未服务'})
                update_table.append('PT_Service_Order')

            update_data.append(record_data)
            update_table.append('PT_Service_Record')
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.task.value, update_data, update_table)
            if bill_id:
                res = 'Success'
            return res
        else:
            return EMPTY_DATA

    def save_time_service_order(self, data):
        res = 'Fail'
        EMPTY_DATA = '没有找到该数据'
        record_data = {}
        now_date = get_cur_time()
        # 当前月份
        now_month = datetime.datetime.now().month
        # 订单ID
        if 'order_id' not in data:
            return '没有订单ID'

        order_id = data['order_id']
        del(data['order_id'])
        if 'id' not in data:
            return EMPTY_DATA
        else:
            # 获取订单数据
            _fillter_order = MongoBillFilter()
            _fillter_order.match_bill((C('id') == order_id))\
                .project({'_id': 0})
            res_order = self.query(_fillter_order, 'PT_Service_Order')

            if len(res_order) == 0:
                return '没有找到订单数据'

            if res_order[0]['status'] == '已完成':
                return '该订单已完成'

            # 判断只能服务当前月的工单
            if now_month != res_order[0]['order_date'].month:
                return '只能开始服务当月的工单'

            # 找出任务主数据
            _fillter = MongoBillFilter()
            _fillter.match_bill(C('id') == data['id'])\
                .project({'_id': 0})
            data_result = self.query(_fillter, 'PT_Task')

            if len(data_result) == 0:
                return '没有找到任务数据'
            update_table = []
            update_data = []
            # 待接收 根据类型更新开始时间或结束时间
            record_data['id'] = data_result[0]['task_object_id']
            if data_result[0]['task_state'] == TaskState.To_be_receive.value:
                if 'type' in data and data['type'] == 'start':
                    record_data['start_date'] = now_date
                    record_data['status'] = '服务中'
                    if res_order[0]['status'] == '未服务':
                        # 待服务就把状态改成服务中
                        update_data.append({'id': order_id, 'status': '服务中'})
                        update_table.append('PT_Service_Order')
                if 'type' in data and data['type'] == 'end':
                    record_data['end_date'] = now_date
                update_data.append(record_data)
                update_table.append('PT_Service_Record')
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.update.value, TypeId.task.value, update_data, update_table)
        if bill_id:
            res = 'Success'
        return res

    def change_task_service_order(self, data):
        '''服务订单分派的任务完成判断逻辑'''
        res = 'Fail'
        # 当前月份
        now_month = datetime.datetime.now().month
        # 根据数据本体状态和传入的内容决定状态
        # 如果传了开始内容没有结束内容，那就表示进行中
        # 如果都传了，那就表示是完成了
        EMPTY_DATA = '没有找到该数据'
        MISSION_PRO = '完成任务'
        TASK_STATE = TaskState.Completed.value
        record_data = {}

        # 订单ID
        if 'order_id' not in data:
            return '没有订单ID'

        order_id = data['order_id']
        del(data['order_id'])

        if 'id' not in data:
            return EMPTY_DATA
        else:
            # 找出任务主数据
            _fillter = MongoBillFilter()
            _fillter.match_bill(C('id') == data['id'])\
                .project({'_id': 0})
            data_result = self.query(_fillter, 'PT_Task')

            if len(data_result) == 0:
                return '没有找到任务数据'
            create_date = data_result[0]['create_date']
            # 防止有问题
            if 'start_date' in data and data['start_date'] != '':
                data['start_date'] = datetime.datetime.strptime(
                    data['start_date'], '%Y-%m-%d %H:%M:%S')
                if data['start_date'].month != now_month:
                    return '开始时间只能是当月'
                if data['start_date'] < create_date:
                    return '开始时间不能小于工单时间'

            if 'end_date' in data and data['end_date'] != '':
                data['end_date'] = datetime.datetime.strptime(
                    data['end_date'], '%Y-%m-%d %H:%M:%S')
                if data['end_date'].month != now_month:
                    return '结束时间只能是当月'
                if data['end_date'] < create_date:
                    return '结束时间不能小于工单时间'

            # 获取订单数据
            _fillter_order = MongoBillFilter()
            _fillter_order.match_bill((C('id') == order_id))\
                .project({'_id': 0})
            res_order = self.query(_fillter_order, 'PT_Service_Order')

            if len(res_order) == 0:
                return '没有找到订单数据'

            if res_order[0]['status'] == '已完成':
                return '该订单已完成'

            # 待接收
            if data_result[0]['task_state'] == TaskState.To_be_receive.value:
                # 待接收必传开始图片和开始时间
                if data['begin_photo'] == '' or data['start_date'] == '':
                    return '当前任务状态为待接收，请选择开始时间和上传服务开始图片'
                # 检测是否同时上传结束数据
                if data['end_photo'] != '' or data['end_date'] != '':
                    if data['end_photo'] == '' or data['end_date'] == '':
                        return '请同时选择服务结束时间和上传服务结束图片'
                # 有服务完成图片，就表示完成了
                if data['end_photo'] != '':
                    MISSION_PRO = '完成任务'
                    TASK_STATE = TaskState.Completed.value
                    # 服务工单表的数据和状态
                    record_data['start_date'] = data['start_date']
                    record_data['end_date'] = data['end_date']
                    record_data['status'] = RecordStatus.service_completed.value
                else:
                    MISSION_PRO = '接收任务'
                    TASK_STATE = TaskState.Ongoing.value
                    # 服务工单表的数据和状态
                    record_data['start_date'] = data['start_date']
                    record_data['status'] = RecordStatus.service_going.value
            elif data_result[0]['task_state'] == TaskState.Ongoing.value:
                # 服务中，这是补全结束时间和结束图片
                # 禁止再传开始数据
                if 'begin_photo' in data:
                    del(data['begin_photo'])
                if 'start_date' in data:
                    del(data['start_date'])

                # 没有必传数据
                if data['end_photo'] == '' or data['end_date'] == '':
                    return '当前任务状态为服务中，请选择服务结束时间和上传服务结束图片'

                MISSION_PRO = '完成任务'
                TASK_STATE = TaskState.Completed.value
                # 服务工单表的数据和状态
                record_data['end_date'] = data['end_date']
                record_data['status'] = RecordStatus.service_completed.value

        data = self.__add_process_record(data, MISSION_PRO)
        if data != 'Fail':
            data['task_state'] = TASK_STATE
            # 补充服务记录表的主键
            record_data['id'] = data_result[0]['task_object_id']

            # 要修改的数据，服务工单，任务表
            if 'is_app' in data.keys():
                record_data['is_app'] = data['is_app']
                del data['is_app']
            update_data = [record_data, data]
            update_table = ['PT_Service_Record', self.Task]

            if res_order[0]['status'] == '服务中':
                # 服务中需要判断服务工单是否都完成了
                _fillter_record = MongoBillFilter()
                _fillter_record.match_bill((C('order_id') == order_id) & ((C('status') == '未服务') | (C('status') == '服务中')))\
                    .lookup_bill('PT_Task', 'id', 'task_object_id', 'task')\
                    .add_fields({
                        'task_id': self.ao.array_elemat('$task.id', 0), })\
                    .project({'_id': 0, 'task': 0})
                res_record = self.query(_fillter_record, 'PT_Service_Record')

                # 剩下一个服务中并且这个服务中的任务id等于当前
                if len(res_record) == 1 and res_record[0]['task_id'] == data['id']:
                    update_data.append({'id': order_id, 'status': '已完成'})
                    update_table.append('PT_Service_Order')
            elif res_order[0]['status'] == '未服务':
                # 待服务就把状态改成服务中
                update_data.append({'id': order_id, 'status': '服务中'})
                update_table.append('PT_Service_Order')
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.task.value, update_data, update_table)
            if bill_id:
                res = 'Success'
        return res

    def get_record_list(self, condition):
        keys = ['id', 'user_operate']
        values = self.get_value(condition, keys)
        if isinstance(values['id'], str):
            values['id'] = [values['id']]
        _filter = MongoBillFilter()
        _filter.match_bill(C('id').inner(values['id']))\
               .unwind('task_process_record')\
               .match(C('user_operate').like(values['user_operate']))\
               .inner_join_bill('PT_User', 'task_process_record.user_id', 'id', 'user_info')\
               .project({'_id': 0, 'task_name': 1, 'task_content': 1,
                         'user_name': '$user_info.name',
                         'user_operate': '$task_process_record.user_operate',
                         'record_message': '$task_process_record.record_message',
                         'date': '$task_process_record.date'})
        res = self.query(_filter, self.Task)
        return res

    def get_all_task_list(self, org_list, condition, page, count):
        '''获取任务列表
        Arguments:
            condition   {dict}      条件,其中task_state为列表格式
            page        {number}    页码
            count       {number}    条数
        '''

        keys = ['id', 'task_content', 'task_name', 'task_urgent',
                'task_type', 'task_state', 'task_receive_person']

        if 'task_type' in condition and condition['task_type'] == '服务分派':
            _filter_task_type = MongoBillFilter()
            _filter_task_type.match_bill(C('name') == '服务分派')\
                .project({'_id': 0})
            task_type_res = self.query(_filter_task_type, 'PT_Task_Type')

            if len(task_type_res) > 0:
                condition['task_type'] = task_type_res[0]['id']

        values = self.get_value(condition, keys)
        if isinstance(values['task_state'], str):
            values['task_state'] = [values['task_state']]

        _filter = MongoBillFilter()
        _filter.match_bill((C('task_state').inner(values['task_state']))
                           & (C('id') == values['id'])
                           & (C('task_type') == values['task_type'])
                           & (C('task_urgent') == values['task_urgent'])
                           & (C('task_content').like(values['task_content']))
                           & (C('task_name').like(values['task_name']))
                           & (C('task_receive_person') == values['task_receive_person'])
                           & (C('organization_id').inner(org_list)))

        if 'user_filter' in list(condition.keys()) and get_current_role_id(self.session) != '8f72e614-e355-11e9-a8b8-a0a4c57e9ebe':
            cur_user_id = get_current_user_id(self.session)
            _filter.match_bill(C('task_receive_person') == cur_user_id)

        _filter.lookup_bill('PT_User', 'purchaser_id', 'id', 'purchaser_info')\
            .lookup_bill('PT_User', 'organization_id', 'id', 'organization_info')\
            .lookup_bill('PT_Personnel_Classification', 'purchaser_info.personnel_info.personnel_classification', 'id', 'classification_info')\
            .lookup_bill(self.Task_type, 'task_type', 'id', 'task_type_info')\
            .unwind('task_type_info', True)\
            .lookup_bill('PT_User', 'task_receive_person', 'id', 'receiver_info')\
            .unwind('receiver_info', True)\
            .lookup_bill('PT_User', 'task_creator', 'id', 'creator_info')\
            .unwind('creator_info', True)\
            .add_fields({
                'task_type_name': '$task_type_info.name',
                'receiver_info_name': '$receiver_info.name',
                'task_creator_name': '$creator_info.name',
                'purchaser_name': '$purchaser_info.name',
                'purchaser_telephone': '$purchaser_info.personnel_info.telephone',
                'classification_name': '$classification_info.name',
                'organization_name': '$organization_info.name'})\
            .project({'_id': 0, 'purchaser_info': 0, 'organization_info': 0, 'classification_info': 0, 'task_type_info._id': 0, 'receiver_info._id': 0, 'creator_info._id': 0})
        res = self.page_query(_filter, self.Task, page, count)
        return res

    def get_to_be_processed_task_list(self, org_list, condition, page, count):
        '''获取待处理(未进行过审核)的任务列表'''
        condition['task_state'] = [TaskState.To_be_processed.value]
        return self.get_all_task_list(org_list, condition, page, count)

    def get_to_be_send_task_list(self, org_list, condition, page, count):
        '''获取待派单的任务列表'''
        condition['task_state'] = [
            TaskState.Adopt_examine.value, TaskState.Rejected.value]
        return self.get_all_task_list(org_list, condition, page, count)

    def get_to_be_receive_task_list(self, org_list, condition, page, count):
        '''<app>获取待接收的任务列表
        '''
        condition['task_state'] = [TaskState.To_be_receive.value]
        return self.get_all_task_list(org_list, condition, page, count)

    def get_ongoing_task_list(self, org_list, condition, page, count):
        '''<app>获取进行中的任务列表
        '''
        condition['task_state'] = [TaskState.Ongoing.value]
        return self.get_all_task_list(org_list, condition, page, count)

    def get_completed_task_list(self, org_list, condition, page, count):
        '''<app>获取已完成的任务列表'''
        condition['task_state'] = [TaskState.Completed.value]
        return self.get_all_task_list(org_list, condition, page, count)

    def get_task_service_order_list(self, org_list, condition, page, count):
        '''<app>获取服务任务列表'''
        condition['task_type'] = '服务分派'
        return self.get_all_task_list(org_list, condition, page, count)
