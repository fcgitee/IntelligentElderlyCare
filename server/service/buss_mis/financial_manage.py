'''
版权：Copyright (c) 2019 China

创建日期：Thursday July 25th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Thursday, 25th July 2019 9:00:01 am
修改者: ymq(ymq) - <<email>>

说明
 1、财务管理相关接口
'''
import datetime
import copy
import uuid
import pandas as pd
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from server.pao_python.pao.data import string_to_date, date_to_string
from ...service.common import get_info, get_current_user_id, get_common_project
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
from ...models.financial_manage import FinancialBook, FinancialAccount, FinancialRecord, FinancialSubject, FinancialVoucher, FinancialEntry, FinancialCharge, FinancialSetSubjectItem
from server.pao_python.pao.data import dataframe_to_list
from ...service.mongo_bill_service import MongoBillFilter
from ...service.constant import AccountType, AccountStatus, PayType, PayStatue, plat_id, AccountingConfirm
from ...service.app.my_order import RecordStatus
from datetime import timedelta
import time


class FinancialService(MongoService):
    '''财务管理服务'''
    book_id = '16e726be-b405-11e9-9f09-983b8f0bcd67'

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def input_user_name_list(self, condition):
        '''下拉框-经济主体姓名'''
        keys = ['user_id']
        values = self.get_value(condition, keys)
        # 过滤器配置
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['user_id'])\
               .project({'_id': 0, 'key': '$id', 'text': '$name'})

        res = self.query(_filter, 'PT_User')
        # print('用户列表', res)
        return res

    def input_account_book_list(self, condition):
        '''下拉框-账簿名称'''
        keys = ['book_id']
        values = self.get_value(condition, keys)
        # 过滤器配置
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['book_id'])\
               .project({'_id': 0, 'key': '$id', 'text': '$account_book_name'})

        res = self.query(_filter, FinancialBook().name)
        return res

    def input_account_list(self, condition):
        '''下拉框-账户名称'''
        keys = ['account_id']
        values = self.get_value(condition, keys)
        # 过滤器配置
        _filter = MongoBillFilter()
        _filter.match(C('id') == values['account_id'])\
               .project({'_id': 0, 'key': '$id', 'text': '$account_name'})

        res = self.query(_filter, 'PT_Financial_Account')
        return res

    def input_subject_list(self, condition):
        '''下拉框-会计科目'''
        keys = ['subject_id']
        values = self.get_value(condition, keys)
        # 过滤器配置
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['subject_id']) & (C('book_id') == self.book_id))\
               .project({'_id': 0, 'key': '$id', 'text': '$subject_name', 'code': '$subject_code'})

        res = self.query(_filter, FinancialSubject().name)
        return res

    def __get_initial_subject_data(self, book_id):
        '''初始化科目数据'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('book_id') == 'initial')
        init_data = self.query(_filter, FinancialSubject().name)
        init_df = pd.DataFrame(init_data)
        init_df = init_df[['id', 'subject_code', 'subject_name',
                           'subject_type', 'remark', 'balance', 'super_subject_id']]
        init_df = init_df.fillna('')
        init_df['book_id'] = book_id
        res = []
        for i in range(len(init_df)):
            new_id = str(uuid.uuid1())
            tep_id = init_df['id'].iloc[i]
            init_df = init_df.replace([tep_id], [new_id])
        for j in range(len(init_df)):
            tep = dict(init_df.iloc[j, :])
            tep['subject_code'] = int(tep['subject_code'])
            tep['balance'] = float(tep['balance'])
            res.append(tep)
        return res

    def update_account_book_data(self, data):
        '''新增/更新账簿数据'''
        res = 'Fail'
        if 'id' not in list(data.keys()):
            data['set_date'] = datetime.datetime.now()
            data['id'] = str(uuid.uuid1())
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.financial.value, data, FinancialBook().name)
            init_subject = self.__get_initial_subject_data(data['id'])
            for i in init_subject:
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.add.value, TypeId.financial.value, i, FinancialSubject().name)
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, data, FinancialBook().name)
            if bill_id:
                res = 'Success'
        return res

    def get_account_book_list(self, org_list, condition, page, count):
        '''获取账簿信息列表'''
        keys = ['user_name', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == values['id'])\
               .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
               .match(C('user_info.name').like(values['user_name']) & (C('organization_id').inner(org_list)))\
               .project({'_id': 0, 'user_name': '$user_info.name', 'user_id': '$user_info.id', 'account_book_name': 1, 'type_book': 1, 'set_date': 1, 'settle_account_date': 1, 'id': 1})
        res = self.page_query(_filter, FinancialBook().name, page, count)
        return res

    def get_account_list(self, org_list, condition, page, count):
        '''获取账户信息列表'''
        keys = ['id', 'name']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']) & (C('name') == values['name']))\
            .lookup_bill('PT_User', 'user_id', 'id', 'main_user')\
            .lookup_bill('PT_User', 'agent_user_id', 'id', 'agent_user')\
            .match((C('organization_id').inner(org_list)))\
            .add_fields({'main_user_name': '$main_user.name', 'agent_user_name': '$agent_user.name'})\
            .project({'_id': 0, 'main_user': 0, 'agent_user': 0})
        res = self.page_query(_filter, FinancialAccount().name, page, count)
        return res

    def get_user_account_sequence_all(self, condition, page, count):
        '''获取用户所属账户顺序信息'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == user_id))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Financial_Agent_Pay_Sequence', page, count)
        return res

    def update_account_data(self, data):
        '''更新/新增账户信息数据'''
        res = 'Fail'
        if(data['account_type'] == '真实账户'):
            obj = {'name': data['extend_account_name'],
                   'card_number': data['extend_card_number']}
            data['extend_info'] = obj
            del data['extend_account_name']
            del data['extend_card_number']
            # print(data)
        if 'id' not in list(data.keys()):
            data['balance'] = 0
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.financial.value, data, FinancialAccount().name)
            if bill_id:
                res = 'Success'
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, data, FinancialAccount().name)
            if bill_id:
                res = 'Success'
        return res

    def update_account_data_list(self, data):
        '''更新/新增账户顺序'''
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == user_id))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Financial_Agent_Pay_Sequence', None, None)

        if(len(res['result']) > 0):
            item = {'user_id': user_id, 'agent_pay_sequence': data,
                    'id': res['result'][0]['id']}
            self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, item, 'PT_Financial_Agent_Pay_Sequence')
        else:
            item = {'user_id': user_id, 'agent_pay_sequence': data}
            self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.financial.value, item, 'PT_Financial_Agent_Pay_Sequence')
        return 'Success'

    def __change_account_balance(self, account_id, number):
        '''在账户流水发生变化时修改账户表的余额'''
        _filter = MongoFilter()
        _filter.match((C('bill_status') == 'valid') & (C('id') == account_id))
        init_data = self.query(_filter, FinancialAccount().name)
        init_data = init_data[0]
        tep = init_data['balance']+number
        init_data['balance'] = tep
        bill_id = self.bill_manage_server.add_bill(
            OperationType.update.value, TypeId.financial.value, init_data, FinancialAccount().name)
        return bill_id

    def save_account_payment_data(self, data):
        '''保存账户付款数据'''
        res = 'Fail'
        date = datetime.datetime.now()
        data['date'] = date
        data['borrow_loan'] = 'loan'
        data['is_return'] = False
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialRecord().name)
        change = self.__change_account_balance(
            data['account_id'], (-1)*float(data['amount']))
        if bill_id and change:
            res = 'Success'
        return res

    def save_account_receive_data(self, data):
        '''保存账户收款数据'''
        res = 'Fail'
        date = datetime.datetime.now()
        data['date'] = date
        data['borrow_loan'] = 'borrow'
        data['is_return'] = False
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialRecord().name)
        change = self.__change_account_balance(
            data['account_id'], float(data['amount']))
        if bill_id and change:
            res = 'Success'
        return res

    def save_account_transfer_data(self, data):
        '''保存账户转账数据'''
        res = 'Fail'
        date = datetime.datetime.now()
        data['date'] = date
        data['is_return'] = False
        pay_account_id = data['pay_account_id']
        del data['pay_account_id']
        data['account_id'] = data.pop('receive_account_id')
        data['borrow_loan'] = 'borrow'
        data1 = copy.deepcopy(data)
        data['account_id'] = pay_account_id
        data['borrow_loan'] = 'loan'
        # print('data1: ', data1)
        # print('data: ', data)
        bill_id1 = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data1, FinancialRecord().name)
        change1 = self.__change_account_balance(
            data1['account_id'], (-1)*float(data1['amount']))
        bill_id2 = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialRecord().name)
        change2 = self.__change_account_balance(
            data['account_id'], float(data['amount']))
        if bill_id1 and bill_id2 and change1 and change2:
            res = 'Success'
        return res

    def get_account_flow_list(self, org_list, condition, page, count):
        '''获取账户流水列表'''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['start_date', 'end_date', 'abstract', 'account_id', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id'])
                           & (C('date') >= as_date(values['start_date']))
                           & (C('date') <= as_date(values['end_date']))
                           & (C('account_id') == values['account_id'])
                           & C('abstract').like(values['abstract'])
                           & (C('organization_id').inner(org_list)))\
            .lookup_bill(FinancialAccount().name, 'account_id', 'id', 'acc_name')\
            .project({'_id': 0, 'account': '$acc_name.account_name', 'abstract': 1, 'amount': 1, 'date': 1, 'id': 1, 'borrow_loan': 1, 'original_voucher': 1, 'account_id': 1, 'return_id': 1, 'red_reason': 1, 'is_return': 1})
        res = self.page_query(_filter, FinancialRecord().name, page, count)
        return res

    def update_subject_data(self, data):
        '''编辑会计科目数据'''
        res = 'Fail'
        data['book_id'] = self.book_id
        if 'id' in list(data.keys()):
            _a = data.pop('add_sub_data', data)  # data为当前科目数据
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, data, FinancialSubject().name)
        else:
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.financial.value, data, FinancialSubject().name)
        if bill_id:
            res = 'Success'
        return res

    def get_subject_list(self, condition, page, count):
        '''获取会计科目列表'''
        keys = ['book_id', 'subject_type', 'subject_name', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('book_id') == self.book_id)
                           & (C('id') == values['id'])
                           & (C('subject_type').like(values['subject_type']))
                           & (C('subject_name').like(values['subject_name'])))\
            .lookup_bill(FinancialSubject().name, 'super_subject_id', 'id', 'super_sub')\
            .lookup_bill(FinancialSubject().name, 'id', 'super_subject_id', 'add_sub_data')\
            .add_fields({'super_subject': '$super_sub.subject_name'})\
            .project({'_id': 0, 'super_sub._id': 0, 'add_sub_data._id': 0})
        # print(_filter.filter_objects)
        res = self.page_query(_filter, FinancialSubject().name, page, count)
        return res

    def del_subject_data(self, ids):
        '''删除科目，被引用的科目不允许删除'''
        res = 'Fail'
        for i in ids:
            _filter = MongoFilter()
            _filter.match((C('bill_status') == 'valid')
                          & (C('super_subject_id') == i))
            res = self.query(_filter, FinancialSubject().name)
            if len(res) < 1:
                bill_id = self.bill_manage_server.add_bill(
                    OperationType.delete.value, TypeId.financial.value, {'id': i}, FinancialSubject().name)
                if bill_id:
                    res = 'Success'
            else:
                res = '被引用不可删除'
        return res

    def get_voucher_list(self, condition, page, count):
        '''获取记账凭证列表'''
        if 'date_range' in list(condition.keys()) and len(condition['date_range']) == 2:
            condition['start_date'] = as_date(condition['date_range'][0])
            condition['end_date'] = as_date(
                condition['date_range'][1])+datetime.timedelta(days=1)
        keys = ['start_date', 'end_date', 'abstract', 'id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match((C('bill_status') == 'valid')
                      & (C('date') > values['start_date'])
                      & (C('date') < values['end_date'])
                      & (C('id') == values['id'])
                      & (C('abstract').like(values['abstract']))
                      & (C('book_id') == self.book_id))\
            .lookup_bill(FinancialEntry().name, 'entry_list', 'id', 'add_sub_data')\
            .project({'_id': 0, 'add_sub_data._id': 0})
        res = self.page_query(_filter, FinancialVoucher().name, page, count)
        return res

    def add_account_voucher(self, data):
        '''增加记账凭证'''
        res = 'Fail'
        data['book_id'] = self.book_id
        entry_data = data['add_sub_data']
        data['date'] = as_date(data['date'])
        entry_ids = [str(uuid.uuid1()) for i in range(len(entry_data))]
        data.pop('add_sub_data')
        data['entry_list'] = entry_ids
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialVoucher().name)
        for i in range(len(entry_data)):
            tep_insert = entry_data[i]
            tep_insert['id'] = entry_ids[i]
            self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.financial.value, tep_insert, FinancialEntry().name)
        if bill_id:
            res = 'Success'
        return res

    def query_account_voucher_list(self, condition, page, count):
        '''查看记账凭证分录列表'''
        keys = ['ids']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match((C('bill_status') == 'valid')
                      & (C('id').inner(values['ids'])))\
            .inner_join_bill(FinancialEntry().name, 'entry_list', 'id', 'entry_info')\
            .inner_join_bill(FinancialSubject().name, 'entry_info.entry_subject', 'id', 'subject_info')\
            .project({'_id': 0, 'date': 1, 'voucher': 1, 'entry_abstract': '$entry_info.entry_abstract', 'entry_subject': '$subject_info.subject_name', 'borrow_loan': '$entry_info.borrow_loan', 'amount': '$entry_info.amount'})
        res = self.page_query(_filter, FinancialVoucher().name, page, count)
        return res

    def return_account_data(self, data):
        '''红冲账户记录数据'''
        res = 'Fail'
        update_data = {}
        update_data['id'] = data['id']
        update_data['is_return'] = True
        data['return_id'] = data.pop('id')
        tep = data['borrow_loan']
        if tep == 'borrow':
            tep = 'loan'
        else:
            tep = 'borrow'
        data['borrow_loan'] = tep
        data.pop('account')
        data['date'] = datetime.datetime.now()
        bill_id1 = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialRecord().name)
        bill_id2 = self.bill_manage_server.add_bill(
            OperationType.update.value, TypeId.financial.value, update_data, FinancialRecord().name)
        if bill_id1 and bill_id2:
            res = 'Success'
        return res

    def return_account_voucher(self, data):
        '''红冲记账凭证数据'''
        res = 'Fail'
        _filter = MongoFilter()
        _filter.match((C('bill_status') == 'valid') & (C('id') == data['id']))\
               .project({'_id': 0})
        res = self.query(_filter, FinancialVoucher().name)
        return_data = res[0]
        return_data['return_id'] = return_data.pop('id')
        return_data['red_reason'] = data['red_reason']
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, return_data, FinancialVoucher().name)
        if bill_id:
            res = 'Success'
        return res

############################################银行对账接口##################################################

    def add_charge_back_detil_data(self, data):
        '''获取扣款明细数据'''
        # 存储扣款明细数据，添加列
        res = 'Fail'
        for i in data:
            i['is_charge'] = '未扣款'
            i['date'] = datetime.datetime.now()
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialCharge().name)
        if bill_id:
            res = 'Success'
        return res

    def get_not_set_item(self):
        '''获取未设置对应会计科目的服务项目类型'''
        _filter = MongoBillFilter()
        _filter.match(C('bill_status') == 'valid')\
               .group({'type': '$item_type'})\
               .lookup_bill(FinancialSetSubjectItem().name, 'type', 'item_type', 'unset')\
               .match(C('unset') == [])\
               .project({'type': 1})
        res = self.query(_filter, FinancialCharge().name)
        return res

    def set_subject_item(self, data):
        '''服务项目类型与会计科目映射关系设置'''
        res = 'Fail'
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.financial.value, data, FinancialSetSubjectItem().name)
        if bill_id:
            res = 'Success'
        return res

    def export_receipt_item(self, condition, page, count):
        '''查询收付款记录'''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['account_type', 'start_date', 'end_date']
        values = self.get_value(condition, keys)
        user_id = get_current_user_id(self.session)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == user_id) & (C('account_type') == values['account_type']))\
               .inner_join_bill('PT_Financial_Account_Record', 'id', 'account_id', 'record_info')\
               .match((C('date') > values['start_date']) & (C('date') < values['end_date']))\
               .sort({'date': -1})\
               .project({'_id': 0, 'id': '$record_info.id', 'abstract': '$record_info.abstract', 'amount': '$record_info.amount', 'date': '$record_info.date'})
        res = self.page_query(_filter, 'PT_Financial_Account', page, count)
        return res

    def export_receipt_details_data(self, record_id):
        '''查询收付款详情订单记录,返回对象'''
        _filter = MongoBillFilter()
        _filter.match_bill(C('id') == record_id)\
               .inner_join_bill('PT_Financial_Account', 'account_id', 'id', 'account_info')\
               .project({'_id': 0, 'date': 1, 'account_type': '$account_info.account_type', 'balance': 1, 'abstract': 1, 'amount': 1})
        res = self.page_query(_filter, 'PT_Financial_Account_Record', 1, 1)
        res = res[0]
        if res['amount'] > 0:
            res['type'] = '收入'
        else:
            res['type'] = '支出'
        return res

    def get_divide_record_list(self, condition, page, count):
        '''查询收入分成记录列表'''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['is_distributed', 'start_date', 'end_date',
                'organization_name', 'organization_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('is_distributed') == values['is_distributed'])
                           & (C('date') > as_date(values['start_date']))
                           & (C('date') < as_date(values['end_date'])))\
               .inner_join_bill('PT_User', 'user_id', 'id', 'user_info')\
               .match(C('user_info.name').like(values['organization_name']) & (C('user_info.orgnaization_info.personnel_type') == values['organization_type']))\
               .project({'_id': 0, 'organization_name': '$user_info.name', 'is_distributed': 1, 'divide_percent': 1, 'amount': 1, 'date': 1, 'financial_id': 1, 'abstract': 1})
        res = self.page_query(
            _filter, 'PT_Financial_Divide_Record', page, count)
        return res

    def get_subsidy_service_provider_list(self, condition, page, count):
        '''获取服务商补贴账户账单'''
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0]
            condition['end_date'] = condition['date_range'][1]
        keys = ['organization_name', 'start_date', 'end_date']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('date') > as_date(values['start_date']))
                           & (C('date') < as_date(values['end_date']))
                           & (C('pay_type') == PayType.subsidy)
                           & (C('pay_status') == PayStatue.success))\
               .inner_join_bill('PT_Financial_Account', 'receiver_account_id', 'id', 'account_info')\
               .inner_join_bill('PT_User', 'account_info.user_id', 'id', 'user_info')\
               .match(C('user_info.name').like(values['organization_name']))\
               .project({'_id': 0, 'organization_name': '$user_info.name', 'amount': 1, 'date': 1, 'financial_id': 1, 'abstract': '$abstract'})
        res = self.page_query(
            _filter, 'PT_Financial_Receivable_Process', page, count)
        return res

    def confirm_send(self, service_provider_id, year_month, buy_type, service_product_id, social_worker_id):
        '''用于发送记录给服务商确认是否结算'''
        res = 'Fail'
        now = datetime.datetime.strptime(year_month, '%Y-%m').date()
        # _prevMonth_ = now.month + 1
        # _year_ = now.year
        # if now.month + 1 > 12:
        #     _prevMonth_ = 1
        #     _year_ = now.year+1
        # start_date = datetime.datetime(_year_, now.month, 1, 0)  # 月初一号零时零分零秒
        # end_date = datetime.datetime(
        #     _year_, _prevMonth_, 1, 23, 59, 59) - timedelta(days=1)  # 月末23时59分59秒
        # start_date_new = str(start_date).split(' ')[0] + "T00:00:00.00Z"
        # end_date_new = str(end_date).split(' ')[0] + "T23:59:59.00Z"
        start = ''
        end = ''
        if now.month < 12:
            start = datetime.datetime(now.year, now.month, 1, 0, 0, 0)
            end = datetime.datetime(now.year, now.month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
        else:
            start = datetime.datetime(now.year, now.month, 1, 0, 0, 0)
            end = datetime.datetime(now.year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
        _filter = MongoBillFilter()
        _filter.match_bill((C('create_date') > start)
                           & (C('create_date') < end)
                           & (C('organization_id') == service_provider_id)
                           & (C('buy_type') == buy_type)
                           & (C('service_product_id') == service_product_id)
                           & (C('status') == RecordStatus.service_completed.value)
                           & (C('accounting_confirm') == AccountingConfirm.accounting_no))\
               .project({'_id': 0})
        res_d = self.query(
            _filter, 'PT_Service_Record')
        admin_list = []
        # 查询该社工局对应及其下所有行政区划id数组
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('id') == social_worker_id))\
            .project({'_id': 0})
        res_social_worker = self.query(_filter_worker, 'PT_User')
        if len(res_social_worker) > 0:
            social_worker = res_social_worker[0]['name']
            admin_id = res_social_worker[0]['admin_area_id']
            _filter_admin = MongoBillFilter()
            _filter_admin.match_bill(C('id') == admin_id)\
                .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                .unwind('admin_info')\
                .match(C('admin_info.bill_status') == 'valid')\
                .project({'_id': 0, 'admin_id': '$admin_info.id'})
            query_res = self.query(
                _filter_admin, 'PT_Administration_Division')
            admin_list = [t['admin_id'] for t in query_res]
            admin_list.append(admin_id)
        if len(res_d) > 0:
            return_datas = []
            order_ids = []
            order_admin_mapping = {}
            for data in res_d:
                order_ids.append(data['order_id'])
            # 查询订单
            _filter_order = MongoBillFilter()
            _filter_order.match_bill((C('id').inner(order_ids)))\
                .project({'_id': 0, 'id': 1, 'purchaser_id': 1})
            res_order = self.query(
                _filter_order, 'PT_Service_Order')
            if len(res_order) > 0:
                elder_ids = []
                for order in res_order:
                    # 订单id映射长者id
                    order_admin_mapping[order['id']] = order['purchaser_id']
                    elder_ids.append(order['purchaser_id'])
                # 查询长者
                _filter_elder = MongoBillFilter()
                _filter_elder.match_bill((C('id').inner(elder_ids)))\
                    .project({'_id': 0, 'id': 1, 'admin_area_id': 1})
                res_elder = self.query(
                    _filter_elder, 'PT_User')
                if len(res_elder) > 0:
                    for elder in res_elder:
                        for key in order_admin_mapping:
                            if order_admin_mapping[key] == elder['id']:
                                order_admin_mapping[key] = elder['admin_area_id']
            for r in res_d:
                if r['order_id'] in order_admin_mapping.keys() and order_admin_mapping[r['order_id']] in admin_list:
                    return_data = {
                        'id': r['id'],
                        'accounting_confirm': AccountingConfirm.confirming
                    }
                    return_datas.append(return_data)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, return_datas, ['PT_Service_Record'])
            if bill_id:
                res = 'Success'
        return res

    def accounting_confirm(self, service_provider_id, year_month, buy_type, service_product_id, social_worker_id):
        '''服务商结算确认'''
        res = 'Fail'
        now = datetime.datetime.strptime(year_month, '%Y-%m').date()
        # _prevMonth_ = now.month + 1
        # _year_ = now.year
        # if now.month + 1 > 12:
        #     _prevMonth_ = 1
        #     _year_ = now.year+1
        # start_date = datetime.datetime(_year_, now.month, 1, 0)  # 月初一号零时零分零秒
        # end_date = datetime.datetime(
        #     _year_, _prevMonth_, 1, 23, 59, 59) - timedelta(days=1)  # 月末23时59分59秒
        start = ''
        end = ''
        if now.month < 12:
            start = datetime.datetime(now.year, now.month, 1, 0, 0, 0)
            end = datetime.datetime(now.year, now.month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
        else:
            start = datetime.datetime(now.year, now.month, 1, 0, 0, 0)
            end = datetime.datetime(now.year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
        _filter = MongoBillFilter()
        _filter.match_bill((C('create_date') > start)
                           & (C('create_date') < end)
                           & (C('organization_id') == service_provider_id)
                           & (C('buy_type') == buy_type)
                           & (C('service_product_id') == service_product_id)
                           & (C('status') == RecordStatus.service_completed.value)
                           & (C('accounting_confirm') == AccountingConfirm.confirming))\
               .project({'_id': 0})
        res_d = self.query(
            _filter, 'PT_Service_Record')
        admin_list = []
        # 查询该社工局对应及其下所有行政区划id数组
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('id') == social_worker_id))\
            .project({'_id': 0})
        res_social_worker = self.query(_filter_worker, 'PT_User')
        if len(res_social_worker) > 0:
            social_worker = res_social_worker[0]['name']
            admin_id = res_social_worker[0]['admin_area_id']
            _filter_admin = MongoBillFilter()
            _filter_admin.match_bill(C('id') == admin_id)\
                .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                .unwind('admin_info')\
                .match(C('admin_info.bill_status') == 'valid')\
                .project({'_id': 0, 'admin_id': '$admin_info.id'})
            query_res = self.query(
                _filter_admin, 'PT_Administration_Division')
            admin_list = [t['admin_id'] for t in query_res]
            admin_list.append(admin_id)
        if len(res_d) > 0:
            return_datas = []
            order_ids = []
            order_admin_mapping = {}
            for data in res_d:
                order_ids.append(data['order_id'])
            # 查询订单
            _filter_order = MongoBillFilter()
            _filter_order.match_bill((C('id').inner(order_ids)))\
                .project({'_id': 0, 'id': 1, 'purchaser_id': 1})
            res_order = self.query(
                _filter_order, 'PT_Service_Order')
            if len(res_order) > 0:
                elder_ids = []
                for order in res_order:
                    # 订单id映射长者id
                    order_admin_mapping[order['id']] = order['purchaser_id']
                    elder_ids.append(order['purchaser_id'])
                # 查询长者
                _filter_elder = MongoBillFilter()
                _filter_elder.match_bill((C('id').inner(elder_ids)))\
                    .project({'_id': 0, 'id': 1, 'admin_area_id': 1})
                res_elder = self.query(
                    _filter_elder, 'PT_User')
                if len(res_elder) > 0:
                    for elder in res_elder:
                        for key in order_admin_mapping:
                            if order_admin_mapping[key] == elder['id']:
                                order_admin_mapping[key] = elder['admin_area_id']
            for r in res_d:
                if r['order_id'] in order_admin_mapping.keys() and order_admin_mapping[r['order_id']] in admin_list:
                    return_data = {
                        'id': r['id'],
                        'accounting_confirm': AccountingConfirm.settled
                    }
                    return_datas.append(return_data)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.update.value, TypeId.financial.value, return_datas, ['PT_Service_Record'])
            if bill_id:
                res = 'Success'
        return res

    def get_service_provider_settlement(self, org_list, condition, page=None, count=None):
        res = {'result': []}
        keys = ['org_id', 'social_worker_id', 'buy_type',
                'no_service_product', 'accounting_confirm']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyymm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            _filter.match_bill((C('end_date') >= start)
                               & (C('end_date') <= end))
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)
        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name'], 'user_id': ress['id']})
        # else:
        #     social_worker = '全部'
        # # 增加社工局返回字段
        # _filter.add_fields({
        #     "social_worker": social_worker
        # })
        _filter.match((C('bill_status') == 'valid') & (C('status') == RecordStatus.service_completed.value) & (C('buy_type') == values['buy_type']) & (C('accounting_confirm') == values['accounting_confirm']))\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'service_provider_id': '$order.service_provider_id', 'order_purchaser_id': '$order.purchaser_id'})\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'user')\
            .match_bill((C('user.organization_info.personnel_category') == '服务商') & (C('service_provider_id').inner(org_list)) & (C('service_provider_id') == values['org_id']))\
            .lookup_bill('PT_User', 'order_purchaser_id', 'id', 'elder')\
            .match_bill((C('elder.admin_area_id').inner(admin_list)))\
            .add_fields({'service_provider_name': '$user.name'})\
            .add_fields({'current_year': self.ao.to_string(self.ao.year('$create_date'))})\
            .add_fields({'current_month': self.ao.to_string(self.ao.month('$create_date'))})\
            .add_fields({'month': self.ao.concat([('$current_year'), '-', ('$current_month')])})\
            .graph_lookup('PT_Administration_Division', '$elder.admin_area_id', 'parent_id', 'id', 'admin')\
            .add_fields({
                "sgj_list": sgj_list
            })\
            .add_fields({'town_info': self.ao.array_filter('$sgj_list', 'tep', (F('$tep.admin_area_id').inner('$admin.id').f))})\
            .add_fields({
                'social_worker': '$town_info.name',
                'social_worker_id': self.ao.array_elemat('$town_info.user_id', 0)
            })
        if 'no_service_product' in condition.keys():
            _filter.project({'month': 1, 'valuation_amount': 1, 'service_provider_name': 1, 'service_provider_id': 1, 'social_worker': 1, 'buy_type': 1, 'accounting_confirm': 1})\
                .group(
                {
                    'service_provider_name': '$service_provider_name',
                    'service_provider_id': '$service_provider_id',
                    'year_month': '$month',
                    'social_worker': '$social_worker',
                    'buy_type': '$buy_type',
                    'accounting_confirm': '$accounting_confirm'
                },
                [{
                    'order_quantity': self.ao.summation(1)
                }, {
                    'subsidy_amount_total': self.ao.summation('$valuation_amount')
                }])
        else:
            _filter.lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
                .add_fields({'service_product_name': '$product.name'})\
                .project({'month': 1, 'valuation_amount': 1, 'service_provider_name': 1, 'service_provider_id': 1, 'service_product_id': 1, 'service_product_name': 1, 'social_worker_id': 1, 'social_worker': 1, 'buy_type': 1, 'accounting_confirm': 1})\
                .group(
                {
                    'service_provider_name': '$service_provider_name',
                    'service_provider_id': '$service_provider_id',
                    'service_product_id': '$service_product_id',
                    'service_product_name': '$service_product_name',
                    'year_month': '$month',
                    'social_worker_id': '$social_worker_id',
                    'social_worker': '$social_worker',
                    'buy_type': '$buy_type',
                    'accounting_confirm': '$accounting_confirm'
                },
                [{
                    'order_quantity': self.ao.summation(1)
                }, {
                    'subsidy_amount_total': self.ao.summation('$valuation_amount')
                }])
        _filter.add_fields({'district_burden': self.ao.switch([self.ao.case(((F('buy_type') == '补贴账户')), ((F('subsidy_amount_total'))*(0.5)).f)], 0)})\
            .add_fields({'town_burden': self.ao.switch([self.ao.case(((F('buy_type') == '补贴账户')), ((F('subsidy_amount_total'))*(0.5)).f)], 0)})\
            .sort({'year_month': 1})
        res_old_age = self.query(_filter, 'PT_Service_Record')
        res['result'] = res_old_age
        order_quantity_total = 0
        subsidy_amount_total_total = 0
        district_burden_total = 0
        town_burden_total = 0
        for data in res_old_age:
            order_quantity_total = order_quantity_total + \
                data['order_quantity']
            subsidy_amount_total_total = subsidy_amount_total_total + \
                data['subsidy_amount_total']
            district_burden_total = district_burden_total + \
                data['district_burden']
            town_burden_total = town_burden_total + data['town_burden']
        res['result'].append({
            'order_quantity': order_quantity_total,
            'subsidy_amount_total': subsidy_amount_total_total,
            'district_burden': district_burden_total,
            'town_burden': town_burden_total,
            'service_provider_name': '合计'
        })
        return res

    def get_service_provider_settlement_total(self, org_list, condition):
        res = {'result': []}
        keys = ['org_id', 'social_worker_id', 'buy_type', 'accounting_confirm']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyy/mm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
          #print('start>>', start)
          #print('end>>', end)
            _filter.match_bill((C('end_date') >= start)
                               & (C('end_date') <= end))
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)

        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name']})

        _filter.match((C('bill_status') == 'valid') & (C('status') == RecordStatus.service_completed.value) & (C('buy_type') == values['buy_type']) & (C('accounting_confirm') == values['accounting_confirm']))\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({'service_provider_id': '$order.service_provider_id', 'order_purchaser_id': '$order.purchaser_id'})\
            .lookup_bill('PT_User', 'service_provider_id', 'id', 'user')\
            .match_bill((C('user.organization_info.personnel_category') == '服务商') & (C('service_provider_id').inner(org_list)) & (C('service_provider_id') == values['org_id']))\
            .lookup_bill('PT_User', 'order_purchaser_id', 'id', 'elder')\
            .match_bill((C('elder.admin_area_id').inner(admin_list)))\
            .add_fields({'current_year': self.ao.to_string(self.ao.year('$create_date'))})\
            .add_fields({'current_month': self.ao.to_string(self.ao.month('$create_date'))})\
            .add_fields({'month': self.ao.concat([('$current_year'), '-', ('$current_month')])})\
            .graph_lookup('PT_Administration_Division', '$elder.admin_area_id', 'parent_id', 'id', 'admin')\
            .add_fields({
                "sgj_list": sgj_list
            })\
            .add_fields({'town_info': self.ao.array_filter('$sgj_list', 'tep', (F('$tep.admin_area_id').inner('$admin.id').f))})\
            .add_fields({
                'social_worker': '$town_info.name'
            })\
            .project({'month': 1, 'valuation_amount': 1, 'service_provider_id': 1, 'social_worker': 1, 'buy_type': 1})\
            .group(
            {
                'month': '$month',
                'service_provider_id': '$service_provider_id',
                'social_worker': '$social_worker',
                'buy_type': '$buy_type'
            },
            [{
                'order_quantity_total': self.ao.summation(1)
            }, {
                'valuation_amount_t': self.ao.summation('$valuation_amount')
            }])\
            .group(
            {
                'year_month': '$month',
                'social_worker': '$social_worker',
                'buy_type': '$buy_type'
            },
            [{
                'service_provider_quantity': self.ao.summation(1)
            }, {
                'order_quantity': self.ao.summation('$order_quantity_total')
            }, {
                'valuation_amount_total': self.ao.summation('$valuation_amount_t')
            }])\
            .add_fields({'district_burden': ((F('valuation_amount_total'))*(0.5)).f})\
            .add_fields({'town_burden': ((F('valuation_amount_total'))*(0.5)).f})\
            .add_fields({'subsidy_amount_total': '$valuation_amount_total'})\
            .sort({'year_month': 1})
        res_old_age = self.query(_filter, 'PT_Service_Record')
        res['result'] = res_old_age
        return res

    def get_service_provider_record_settlement(self, org_list, condition, page=None, count=None):
        '''服务商明细报表'''
        keys = ['month', 'org_id', 'social_worker_id', 'buy_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyy/mm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            _filter.match_bill((C('end_date') >= start)
                               & (C('end_date') <= end))
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0, 'login_info': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)
        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name']})

        # 增加社工局返回字段
        _filter.match((C('bill_status') == 'valid') & (C('status') == RecordStatus.service_completed.value) & (C('accounting_confirm') == AccountingConfirm.settled) & (C('buy_type') == values['buy_type']))\
            .lookup_bill('PT_Service_Order', 'order_id', 'id', 'order')\
            .add_fields({
                'order_purchaser_id': self.ao.array_elemat('$order.purchaser_id', 0),
                'order_service_provider_id': self.ao.array_elemat('$order.service_provider_id', 0),
                'order_code': self.ao.array_elemat('$order.order_code', 0),
                'buy_type': self.ao.array_elemat('$order.buy_type', 0)
            })\
            .lookup_bill('PT_User', 'order_service_provider_id', 'id', 'serviceProvider')\
            .add_fields({
                'service_provider_name': self.ao.array_elemat('$serviceProvider.name', 0)
            })\
            .match_bill((C('serviceProvider.organization_info.personnel_category') == '服务商') & (C('order_service_provider_id').inner(org_list)) & (C('order_service_provider_id') == values['org_id']))\
            .lookup_bill('PT_User', 'order_purchaser_id', 'id', 'user')\
            .match_bill((C('user.admin_area_id').inner(admin_list)))\
            .add_fields({
                'user_name': self.ao.array_elemat('$user.name', 0),
                'user_address': self.ao.array_elemat('$user.personnel_info.address', 0),
                'personnel_classification': self.ao.array_elemat('$user.personnel_info.personnel_classification', 0)
            })\
            .lookup_bill('PT_Personnel_Classification', 'personnel_classification', 'id', 'personnelClassification')\
            .add_fields({
                'classification': self.ao.array_elemat('$personnelClassification.name', 0)
            })\
            .lookup_bill('PT_Service_Product', 'service_product_id', 'id', 'product')\
            .add_fields({
                'product_service_item_id': self.ao.array_elemat('$product.service_item_id', 0),
                'price': '$valuation_amount',
            })\
            .lookup_bill('PT_Service_Item', 'product_service_item_id', 'id', 'item')\
            .add_fields({
                'item_type': self.ao.array_elemat('$item.item_type', 0),
                'item_name': self.ao.array_elemat('$item.name', 0)
            })\
            .lookup_bill('PT_Service_Type', 'item_type', 'id', 'serviceType')\
            .add_fields({
                'item_type_name': self.ao.array_elemat('$serviceType.name', 0),
            })\
            .graph_lookup('PT_Administration_Division', '$user.admin_area_id', 'parent_id', 'id', 'admin')\
            .add_fields({
                "sgj_list": sgj_list
            })\
            .add_fields({'town_info': self.ao.array_filter('$sgj_list', 'tep', (F('$tep.admin_area_id').inner('$admin.id').f))})\
            .add_fields({
                'social_worker': '$town_info.name'
            })\
            .project({'_id': 0, 'admin._id': 0, 'order._id': 0, 'serviceProvider._id': 0, 'user._id': 0, 'personnelClassification._id': 0, 'product._id': 0, 'item._id': 0, 'serviceType._id': 0})
        res = self.page_query(_filter, "PT_Service_Record", page, count)
        price_total = 0
        for data in res['result']:
            price_total = price_total + data['price']
        res['result'].append({
            'price': price_total,
            'service_provider_name': '合计'
        })
        for index, item in enumerate(res['result']):
            res['result'][index]['idx'] = index
        return res

    def get_service_provider_record_settlement_yh(self, org_list, condition, page=None, count=None):
        '''服务商明细报表--优化'''
        keys = ['month', 'org_id', 'social_worker_id', 'buy_type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter_hj = MongoBillFilter()
        if 'month' in condition.keys() and len(condition['month']) == 7:
            # str类型,格式：yyyy/mm
            month = int(condition['month'][5:])
            year = int(condition['month'][0:4])
            start = datetime.datetime.now()
            end = datetime.datetime.now()
            if month < 12:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year, month + 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            else:
                start = datetime.datetime(year, month, 1, 0, 0, 0)
                end = datetime.datetime(
                    year + 1, 1, 1, 23, 59, 59)-datetime.timedelta(days=1)
            _filter.match_bill((C('end_date') >= start)
                               & (C('end_date') <= end))
            _filter_hj.match_bill((C('end_date') >= start)
                                  & (C('end_date') <= end))
        admin_list = N()
        if 'social_worker_id' in condition.keys():
            social_worker = ''
            # 查询该社工局对应及其下所有行政区划id数组
            _filter_worker = MongoBillFilter()
            _filter_worker.match_bill((C('id') == values['social_worker_id']))\
                .project({'_id': 0, 'login_info': 0})
            res_social_worker = self.query(_filter_worker, 'PT_User')
            if len(res_social_worker) > 0:
                social_worker = res_social_worker[0]['name']
                admin_id = res_social_worker[0]['admin_area_id']
                _filter_admin = MongoBillFilter()
                _filter_admin.match_bill(C('id') == admin_id)\
                    .graph_lookup('PT_Administration_Division', '$id', 'id', 'parent_id', 'admin_info')\
                    .unwind('admin_info')\
                    .match(C('admin_info.bill_status') == 'valid')\
                    .project({'_id': 0, 'admin_id': '$admin_info.id'})
                query_res = self.query(
                    _filter_admin, 'PT_Administration_Division')
                admin_list = [t['admin_id'] for t in query_res]
                admin_list.append(admin_id)
        # 所有社工局对应行政区划列表
        t1 = time.time()
        sgj_list = []
        _filter_worker = MongoBillFilter()
        _filter_worker.match_bill((C('organization_info.personnel_category') == '街道'))\
            .graph_lookup('PT_Administration_Division', '$admin_area_id', 'id', 'parent_id', 'admin_info')\
            .add_fields({'admin_list': '$admin_info.id'})\
            .project({'_id': 0})
        res_social_workers = self.query(_filter_worker, 'PT_User')
        if len(res_social_workers) > 0:
            for ress in res_social_workers:
                ress['admin_list'].append(ress['admin_area_id'])
                sgj_list.append(
                    {'admin_area_id': ress['admin_area_id'], 'name': ress['name'], 'admin_area_id_list': ress['admin_list']})

        t2 = time.time()
        print('查询社工局行政区划列表时间》》', t2-t1)

        # 查询服务商数据
        provider_ids = []
        provider_data = {}
        t1 = time.time()
        _filter_provider = MongoBillFilter()
        _filter_provider.match_bill(
            (C('personnel_type') == '2')
            & (C('organization_info.personnel_category') == '服务商')
            & (C('id').inner(org_list))
            & (C('id') == values['org_id'])
        )
        _filter_provider.project({
            'qualification_info': 0,
            'main_account': 0,
            **get_common_project()
        })
        res_provider = self.query(
            _filter_provider, 'PT_User')
        if len(res_provider) > 0:
            for provider in res_provider:
                provider_data[provider['id']] = provider
                provider_ids.append(provider['id'])
        t2 = time.time()
        print('查询服务商时间》》', t2-t1)

        # 查询长者
        elder_ids = []
        elder_data = {}
        if 'social_worker_id' in condition.keys():
            # user_personnel_classification_ids = []
            # t1 = time.time()
            # _filter_classification = MongoBillFilter()
            # _filter_classification.match_bill((C('type') == '政府补贴'))\
            #     .project({'id': 1, 'name': 1})
            # res_classification = self.query(
            #     _filter_classification, 'PT_Personnel_Classification')
            # if len(res_classification) > 0:
            #     for classification in res_classification:
            #         user_personnel_classification_ids.append(
            #             classification['id'])
            # t2 = time.time()
            # print('查询长者类型时间》》', t2-t1)
            t1 = time.time()
            _filter_elder = MongoBillFilter()
            _filter_elder.match_bill((C('personnel_type') == '1')
                                     & (C('personnel_info.personnel_category') == '长者')
                                     & (C('admin_area_id').inner(admin_list)))\
                .project({'login_info': 0, **get_common_project()})
            res_elder = self.query(
                _filter_elder, 'PT_User')
            if len(res_elder) > 0:
                for elder in res_elder:
                    elder_data[elder['id']] = elder
                    elder_ids.append(elder['id'])
            t2 = time.time()
            print('查询长者时间》》', t2-t1)

        # 查询订单
        order_ids = []
        order_data = {}
        _filter_order = MongoBillFilter()
        if len(elder_ids) > 0:
            _filter_order.match_bill((C('purchaser_id').inner(elder_ids)))
        t1 = time.time()
        _filter_order.match_bill((C('service_provider_id').inner(provider_ids)))\
            .project({'id': 1, 'purchaser_id': 1, 'service_provider_id': 1, 'buy_type': 1, 'order_code': 1, 'personnel_classification': 1})
        res_order = self.query(
            _filter_order, 'PT_Service_Order')
        if len(res_order) > 0:
            for order in res_order:
                order_data[order['id']] = order
                order_ids.append(order['id'])
        t2 = time.time()
        print('查询订单时间》》', t2-t1)

        # 查询主数据-服务记录合计
        t1 = time.time()
        _filter_hj.match_bill((C('status') == RecordStatus.service_completed.value)
                              & (C('accounting_confirm') == AccountingConfirm.settled)
                              & (C('buy_type') == values['buy_type'])
                              & (C('order_id').inner(order_ids)))\
            .add_fields({
                'a': '1',
                'price': '$valuation_amount'
            })\
            .group(
            {
                'a': '$a'
            },
            [{
                'total_price': self.ao.summation('$price')
            }])
        res_hj = self.query(_filter_hj, "PT_Service_Record")
        t2 = time.time()
        print('查询服务记录合计金额时间》》', t2-t1)

        # 查询主数据-服务记录
        t1 = time.time()
        _filter.match_bill((C('status') == RecordStatus.service_completed.value)
                           & (C('accounting_confirm') == AccountingConfirm.settled)
                           & (C('buy_type') == values['buy_type'])
                           & (C('order_id').inner(order_ids)))\
            .project({'_id': 0, 'price': '$valuation_amount', 'order_id': 1, 'service_product_id': 1, 'start_date': 1, 'end_date': 1})
        res = self.page_query(_filter, "PT_Service_Record", page, count)
        t2 = time.time()
        print('查询服务记录时间》》', t2-t1)
        if len(res['result']) > 0:
            personnel_classification_ids = []
            personnel_classification_data = {}
            service_product_ids = []
            service_product_data = {}
            product_service_item_ids = []
            product_service_item_data = {}
            item_type_ids = []
            item_type_data = {}
            for i, x in enumerate(res['result']):
                if 'order_id' in res['result'][i] and res['result'][i]['order_id'] in order_data.keys():
                    res['result'][i]['order_purchaser_id'] = order_data[res['result']
                                                                        [i]['order_id']]['purchaser_id']
                    res['result'][i]['order_service_provider_id'] = order_data[res['result']
                                                                               [i]['order_id']]['service_provider_id']
                    res['result'][i]['order_code'] = order_data[res['result']
                                                                [i]['order_id']]['order_code']
                    res['result'][i]['buy_type'] = order_data[res['result']
                                                              [i]['order_id']]['buy_type']
                    if 'personnel_classification' in order_data[res['result'][i]['order_id']]:
                        res['result'][i]['personnel_classification'] = order_data[res['result']
                                                                                  [i]['order_id']]['personnel_classification']

                if 'order_service_provider_id' in res['result'][i] and res['result'][i]['order_service_provider_id'] in provider_data.keys():
                    res['result'][i]['service_provider_name'] = provider_data[res['result']
                                                                              [i]['order_service_provider_id']]['name']

                if 'social_worker_id' not in condition.keys() and 'order_purchaser_id' in res['result'][i]:
                    elder_ids.append(res['result'][i]['order_purchaser_id'])

                if 'service_product_id' in res['result'][i]:
                    service_product_ids.append(
                        res['result'][i]['service_product_id'])

            # 查询长者
            if 'social_worker_id' not in condition.keys() and len(elder_ids) > 0:
                t1 = time.time()
                _filter_elder = MongoBillFilter()
                _filter_elder.match_bill((C('personnel_type') == '1')
                                         & (C('personnel_info.personnel_category') == '长者')
                                         & (C('id').inner(elder_ids)))\
                    .project({'login_info': 0, **get_common_project()})
                res_elder = self.query(
                    _filter_elder, 'PT_User')
                if len(res_elder) > 0:
                    for elder in res_elder:
                        elder_data[elder['id']] = elder
                t2 = time.time()
                print('查询长者时间》》', t2-t1)

            for i, x in enumerate(res['result']):
                if 'order_purchaser_id' in res['result'][i] and res['result'][i]['order_purchaser_id'] in elder_data.keys():
                    res['result'][i]['user_name'] = elder_data[res['result']
                                                               [i]['order_purchaser_id']]['name']
                    if 'address' in elder_data[res['result'][i]['order_purchaser_id']]['personnel_info']:
                        res['result'][i]['user_address'] = elder_data[res['result']
                                                                      [i]['order_purchaser_id']]['personnel_info']['address']
                    if 'personnel_classification' not in res['result'][i]:
                        res['result'][i]['personnel_classification'] = elder_data[res['result']
                                                                                  [i]['order_purchaser_id']]['personnel_info']['personnel_classification']
                    res['result'][i]['admin_area_id'] = elder_data[res['result']
                                                                   [i]['order_purchaser_id']]['admin_area_id']

                if 'personnel_classification' in res['result'][i]:
                    personnel_classification_ids.append(
                        res['result'][i]['personnel_classification'])

            # 查询长者类型
            if len(personnel_classification_ids) > 0:
                t1 = time.time()
                _filter_classification = MongoBillFilter()
                _filter_classification.match_bill((C('id').inner(personnel_classification_ids)))\
                    .project({'id': 1, 'name': 1})
                res_classification = self.query(
                    _filter_classification, 'PT_Personnel_Classification')
                if len(res_classification) > 0:
                    for classification in res_classification:
                        # personnel_classification_ids.append(
                        #     classification['id'])
                        personnel_classification_data[classification['id']
                                                      ] = classification
                t2 = time.time()
                print('查询长者类型时间》》', t2-t1)
            # 查询服务产品
            if len(service_product_ids) > 0:
                t1 = time.time()
                _filter_product = MongoBillFilter()
                _filter_product.match_bill((C('id').inner(service_product_ids)))\
                    .project({'id': 1, 'service_item_id': 1, 'name': 1})
                res_product = self.query(
                    _filter_product, 'PT_Service_Product')
                if len(res_product) > 0:
                    for product in res_product:
                        service_product_data[product['id']] = product
                t2 = time.time()
                print('查询服务产品时间》》', t2-t1)

            for i, x in enumerate(res['result']):
                if 'personnel_classification' in res['result'][i] and res['result'][i]['personnel_classification'] in personnel_classification_data.keys():
                    res['result'][i]['classification'] = personnel_classification_data[res['result']
                                                                                       [i]['personnel_classification']]['name']

                if 'service_product_id' in res['result'][i] and res['result'][i]['service_product_id'] in service_product_data.keys():
                    res['result'][i]['product_service_item_id'] = service_product_data[res['result']
                                                                                       [i]['service_product_id']]['service_item_id']
                    product_service_item_ids.append(
                        res['result'][i]['product_service_item_id'])
                    res['result'][i]['item_name'] = service_product_data[res['result']
                                                                         [i]['service_product_id']]['name']

            # 查询服务项目
            if len(product_service_item_ids) > 0:
                t1 = time.time()
                _filter_item = MongoBillFilter()
                _filter_item.match_bill((C('id').inner(product_service_item_ids)))\
                    .project({'id': 1, 'item_type': 1})
                res_item = self.query(
                    _filter_item, 'PT_Service_Item')
                if len(res_item) > 0:
                    for item in res_item:
                        product_service_item_data[item['id']] = item
                t2 = time.time()
                print('查询服务项目时间》》', t2-t1)

            for i, x in enumerate(res['result']):
                if 'product_service_item_id' in res['result'][i] and res['result'][i]['product_service_item_id'] in product_service_item_data.keys():
                    res['result'][i]['item_type'] = product_service_item_data[res['result']
                                                                              [i]['product_service_item_id']]['item_type']
                    item_type_ids.append(res['result'][i]['item_type'])

            # 查询服务项目类型
            if len(item_type_ids) > 0:
                t1 = time.time()
                _filter_item_type = MongoBillFilter()
                _filter_item_type.match_bill((C('id').inner(item_type_ids)))\
                    .project({'id': 1, 'name': 1})
                res_item_type = self.query(
                    _filter_item_type, 'PT_Service_Type')
                if len(res_item_type) > 0:
                    for item_type in res_item_type:
                        item_type_data[item_type['id']] = item_type
                t2 = time.time()
                print('查询服务项目类型时间》》', t2-t1)

            for i, x in enumerate(res['result']):
                if 'item_type' in res['result'][i] and res['result'][i]['item_type'] in item_type_data.keys():
                    res['result'][i]['item_type_name'] = item_type_data[res['result']
                                                                        [i]['item_type']]['name']
                if 'admin_area_id' in res['result'][i]:
                    for sgj in sgj_list:
                        if res['result'][i]['admin_area_id'] in sgj['admin_area_id_list']:
                            res['result'][i]['social_worker'] = sgj['name']
                res['result'][i]['index_no'] = i+1
        if page == None and count == None:
            # 计算合计金额
            price_total = 0
            if len(res_hj) > 0:
                price_total = res_hj[0]['total_price']
            res['result'].append({
                'price': price_total,
                'service_provider_name': '合计'
            })
        return res
