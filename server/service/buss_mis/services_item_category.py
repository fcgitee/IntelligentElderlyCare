from ...service.mongo_bill_service import MongoBillFilter
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:49:52
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\service\buss_mis\services_item_category.py
'''
# -*- coding: utf-8 -*-

'''
服务项目类别函数
'''


class ServicesItemCategoryService(MongoService):
    ''' 服务项目类别 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.role_service = RoleService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    def get_services_item_category_list(self, order_ids, condition, page, count):
        '''获取服务项目类别列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        '''
        keys = ['id', 'name', 'number']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_id').inner(order_ids))\
            .match_bill((C('id') == values['id']) & (C('name').like(values['name'])) & (C('number') == (values['number'])))\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Services_Item_Category', page, count)
        return res

    def update_services_item_category(self, services_item_category):
        '''# 新增/修改服务项目类别'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in services_item_category.keys():
                bill_id = self.bill_manage_service.add_bill(OperationType.update.value,
                                                            TypeId.servicesItemCategory.value, services_item_category, 'PT_Services_Item_Category')
                if bill_id:
                    res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                            TypeId.servicesItemCategory.value, services_item_category, 'PT_Services_Item_Category')
                if bill_id:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def delete_services_item_category(self, services_item_category_ids):
        '''删除服务项目类别接口

        Arguments:
            services_item_category_ids   {ids}      数据id
        '''
        res = 'fail'

        def process_func(db):
            nonlocal res
            ids = []
            if isinstance(services_item_category_ids, str):
                ids.append(services_item_category_ids)
            else:
                ids = services_item_category_ids
            for services_item_category_id in ids:
                # 查询被删除的数据信息
                data = find_data(db, 'PT_Services_Item_Category', {
                    'id': services_item_category_id, 'bill_status': Status.bill_valid.value})
                if len(data) > 0:
                    self.bill_manage_service.add_bill(OperationType.delete.value,
                                                      TypeId.servicesItemCategory.value, data[0], 'PT_Services_Item_Category')
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
