
from server.pao_python.pao.service.data.mongo_db import MongoService, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_current_user_id
from ...pao_python.pao.service.security.security_utility import get_current_account_id
from ...pao_python.pao.service.security.security_service import RoleService
import hashlib
import re
import datetime
import uuid
import pandas as pd
from enum import Enum
from ...pao_python.pao.data import process_db, dataframe_to_list, DataProcess, DataList, get_cur_time
from ...service.mongo_bill_service import MongoBillFilter
import dateutil


class CarouselManageService(MongoService):
    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session

    def get_carousel_list(self, condition, page=None, count=None):
        '''查询轮播列表'''
        keys = ['id', 'name', 'type']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                'user_name': '$user.name',
                        'create_date': self.ao.date_to_string('$create_date'),
                        'modify_date': self.ao.date_to_string('$modify_date')})\
            .match((C('id') == values['id']) & (C('name').like(values['name'])) & (C('type') == values['type']))\
            .project({'_id': 0,
                      'user._id': 0})
        res = self.page_query(
            _filter, "PT_Carousel", page, count)
        return res

    def update_carousel(self, carousel):
        '''# 新增/修改'''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            if 'id' in carousel.keys():
                backVal = update_data(db, 'PT_Carousel', carousel, {
                                      'id': carousel['id']})
                if backVal:
                    res = 'Success'
            else:
                data_info = get_info(carousel, self.session)
                backVal = insert_data(db, 'PT_Carousel', data_info)
                if backVal:
                    res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res

    def del_carousel(self, ids):
        ''' 删除轮播 '''
        res = 'Fail'

        def process_func(db):
            nonlocal res
            for carousel_id in ids:
                delete_data(db, 'PT_Carousel', {'id': carousel_id})
            res = 'Success'
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return res
