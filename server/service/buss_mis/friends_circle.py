
import datetime
import hashlib
import re
import uuid
import copy

import pandas as pd

from server.pao_python.pao.service.data.mongo_db import (C, F, MongoFilter,
                                                         MongoService, N,
                                                         as_date)

from ...pao_python.pao.data import (DataList, DataProcess, dataframe_to_list,
                                    process_db, string_to_date_only)
from ...pao_python.pao.service.security.security_service import RoleService
from ...pao_python.pao.service.security.security_utility import \
    get_current_account_id
from ...service.buss_pub.bill_manage import (BillManageService, OperationType,
                                             Status, TypeId)
from ...service.common import (delete_data, find_data, get_condition,
                               get_current_user_id, get_info, insert_data,
                               update_data, get_common_project)
from ...service.mongo_bill_service import MongoBillFilter


# -*- coding: utf-8 -*-


class FriendsCircleMessageObject():
    '''老友圈对象'''

    def __init__(self, user_id, content, picture, location):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 内容
        self.content = content
        # 图片
        self.picture = picture
        # 定位
        self.location = location

    def to_dict(self):
        return self.__dict__


class FriendsCircleCommentObject():
    '''老友圈评论对象'''

    def __init__(self, user_id, fcm_id, content):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 信息id
        self.fcm_id = fcm_id
        # 内容
        self.content = content

    def to_dict(self):
        return self.__dict__


class FriendsCircleLikeObject():
    '''老友圈点赞对象'''

    def __init__(self, user_id, fcm_id, like):
        '''构造函数'''
        # 操作人id
        self.user_id = user_id
        # 信息id
        self.fcm_id = fcm_id
        # 点赞
        self.like = like

    def to_dict(self):
        return self.__dict__


"""
老友圈管理函数
"""


class FriendsCircleService(MongoService):
    """ 老友圈管理 """

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        DataProcess.__init__(self, db_addr, db_port, db_name, db_user, db_pwd)
        # self.db_name = db_name
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_service = BillManageService(
            db_addr, db_port, db_name, db_user, db_pwd, inital_password, session
        )

    def get_friends_circle_list(self, condition, page, count):
        """获取老友圈广场列表

        Arguments:
            condition   {dict}      条件
            page        {number}    页码
            count       {number}    条数
        """
        if 'date_range' in list(condition.keys()):
            condition['start_date'] = condition['date_range'][0][0:10] + \
                "T00:00:00.00Z"
            condition['end_date'] = condition['date_range'][1][0:10] + \
                "T23:59:59.00Z"
        keys = ["id", "is_own", "user_id", "fcm_id",
                "name", "start_date", "end_date"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if 'is_own' in condition.keys():
            user_id = get_current_user_id(self.session)
            _filter.match_bill((C('user_id') == user_id))
        _filter.match_bill((C("id") == values["id"])
                           & (C('create_date') >= as_date(values['start_date']))
                           & (C('create_date') <= as_date(values['end_date']))
                           & (C("user_id") == values["user_id"])
                           & (C("fcm_id") == values["fcm_id"]))\
            .lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                "name": self.ao.array_elemat("$user.name", 0),
                "user_picture": self.ao.array_elemat("$user.personnel_info.picture_list", 0)
            })\
            .match_bill((C('name').like(values['name'])))\
            .lookup_bill('PT_Friend_Circle_Comment', 'id', 'fcm_id', 'comment_list')\
            .add_fields({'comment_num': self.ao.size("$comment_list")})\
            .lookup_bill('PT_Friend_Circle_Like', 'id', 'fcm_id', 'clike')\
            .add_fields({'like_list': self.ao.array_filter("$clike", "aa", ((F('$aa.like') == True)).f)})\
            .lookup_bill('PT_User', 'like_list.user_id', 'id', 'like_user')\
            .add_fields({
                'clike_num': self.ao.size("$like_list"),
            })\
            .sort({'create_date': -1})\
            .project({"_id": 0, "comment_list._id": 0,  "clike": 0, "like_list._id": 0, "like_user": 0, "user": 0, **get_common_project()})
        res = self.page_query(_filter, "PT_Friend_Circle_Message", page, count)

        if 'id' in condition:
            # 编辑界面需要附带列表点赞用户列表
            user_ids = []
            if len(res['result']) > 0:
                for item in res['result']:
                    if 'comment_list' in item and type(item['comment_list']) == list and len(item['comment_list']) > 0:
                        for itm_comment in item['comment_list']:
                            if 'user_id' in itm_comment:
                                user_ids.append(itm_comment['user_id'])
                    if 'like_list' in item and type(item['like_list']) == list and len(item['like_list']) > 0:
                        for itm_like in item['like_list']:
                            if 'user_id' in itm_like:
                                user_ids.append(itm_like['user_id'])

            if len(user_ids) > 0:
                temp_user = {}
                _filter_user = MongoBillFilter()
                _filter_user.match_bill((C('id').inner(user_ids)))\
                    .project({'_id': 0, })
                res_user = self.query(_filter_user, 'PT_User')

                for item_user in res_user:
                    temp_user[item_user['id']] = item_user

                for item in res['result']:
                    if 'comment_list' in item and type(item['comment_list']) == list and len(item['comment_list']) > 0:
                        for itm_comment in item['comment_list']:
                            if 'user_id' in itm_comment and itm_comment['user_id'] in temp_user and 'name' in temp_user[itm_comment['user_id']]:
                                itm_comment['name'] = temp_user[itm_comment['user_id']]['name']
                    if 'like_list' in item and type(item['like_list']) == list and len(item['like_list']) > 0:
                        for itm_like in item['like_list']:
                            if 'user_id' in itm_like and itm_like['user_id'] in temp_user and 'name' in temp_user[itm_like['user_id']]:
                                itm_like['name'] = temp_user[itm_like['user_id']]['name']
        return res

    def get_my_friends_circle_list(self, condition, page, count):
        ''' 获取好友老友圈列表 '''
        return []

    def update_friends_circle(self, content, picture, location):
        """ 发布老友圈 """
        res = 'Fail'
        data = FriendsCircleMessageObject(
            get_current_user_id(self.session), content, picture, location).to_dict()
        data_info = get_info(data, self.session)
        bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                    TypeId.friendsCircle.value, data_info, 'PT_Friend_Circle_Message')
        if bill_id:
            res = "Success"
        return res

    def delete_friends_circle(self, friends_circle_ids):
        '''删除老友圈接口
        Arguments:
            friends_circle_ids   {ids}      数据id
        '''
        res = 'Fail'
        ids = []
        if isinstance(friends_circle_ids, str):
            ids.append(friends_circle_ids)
        else:
            ids = friends_circle_ids
        for friends_circle_id in ids:
            self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.friendsCircle.value, [{'id': friends_circle_id}], ['PT_Friend_Circle_Message'])
        res = 'Success'
        return res

    def update_commont(self, fcm_id, common):
        ''' 发布评论 '''
        user_id = get_current_user_id(self.session)
        data = FriendsCircleCommentObject(user_id, fcm_id, common).to_dict()
        data_info = get_info(data, self.session)
        bill_id = self.bill_manage_service.add_bill(OperationType.add.value,
                                                    TypeId.friendsCircle.value, data_info, 'PT_Friend_Circle_Comment')
        if bill_id:
            res = "Success"
        return res

    def del_commont(self, comment_ids):
        ''' 删除评论 '''
        res = 'Fail'
        ids = []
        if isinstance(comment_ids, str):
            ids.append(comment_ids)
        else:
            ids = comment_ids
        for comment_id in ids:
            self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.friendsCircle.value, [{'id': comment_id}], ['PT_Friend_Circle_Comment'])
        res = 'Success'
        return res

    def update_like(self, fcm_id):
        ''' 点赞 type:0 取消点赞，1点赞'''
        res = "Fail"
        user_id = get_current_user_id(self.session)
        res_data = []

        def process_func(db):
            nonlocal res_data
            res_data = find_data(db, "PT_Friend_Circle_Like", {
                                 "fcm_id": fcm_id, "user_id": user_id, "bill_status": Status.bill_valid.value})
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        operation_type = OperationType.add.value
        data = {}
        if len(res_data) > 0:
            '''更新已有的'''
            data = res_data[0]
            operation_type = OperationType.update.value
            if 'like' in data.keys() and data['like']:
                data['like'] = False
                res = "Success2"
            else:
                data['like'] = True
                res = "Success1"
            data.pop('_id')
        else:
            '''新增新的'''
            operation_type = OperationType.add.value
            data_info = {'user_id': user_id, 'fcm_id': fcm_id}
            data = get_info(data_info, self.session)
            data['like'] = True
            res = "Success1"

        bill_id = self.bill_manage_service.add_bill(operation_type,
                                                    TypeId.friendsCircle.value, data, 'PT_Friend_Circle_Like')
        if bill_id:
            return res
        return 'Fail'

    def get_commont_list(self, condition, page, count):
        ''' 查询评论列表 '''
        keys = ["id", "is_own", "content"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C("id") == values["id"]))
        if 'is_own' in condition.keys():
            user_id = get_current_user_id(self.session)
            _filter.match_bill((C('user_id') == user_id))
        _filter.lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                "name": self.ao.array_elemat("$user.name", 0),
                "user_picture": self.ao.array_elemat("$user.personnel_info.picture_list", 0)
            })\
            .lookup_bill('PT_Friend_Circle_Message', 'fcm_id', 'id', 'circle')\
            .add_fields({'circle_object': self.ao.array_elemat("$circle", 0)})\
            .match_bill((C("circle_object.content").like(values["content"]) | C("content").like(values["content"]) | C("name").like(values["content"])))\
            .lookup_bill('PT_User', 'circle_object.user_id', 'id', 'circle_object_user')\
            .add_fields({'circle_user_name': self.ao.array_elemat("$circle_object_user.name", 0), 'circle_user_picture': self.ao.array_elemat("$circle_object_user.personnel_info.picture_list", 0), 'circle_content': '$circle_object.content'})\
            .sort({'create_date': -1})\
            .project({"_id": 0, "comment._id": 0,  "user": 0, "circle": 0, 'circle_object_user': 0, 'circle_object': 0})
        res = self.page_query(_filter, "PT_Friend_Circle_Comment", page, count)
        return res

    def get_like_list(self, condition, page, count):
        ''' 查询点赞列表 '''
        keys = ["id", "is_own", "content"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('like') == True) & (C("id") == values["id"]))
        if 'is_own' in condition.keys():
            user_id = get_current_user_id(self.session)
            _filter.match_bill((C('user_id') == user_id))
        _filter.lookup_bill('PT_User', 'user_id', 'id', 'user')\
            .add_fields({
                "name": self.ao.array_elemat("$user.name", 0),
                "user_picture": self.ao.array_elemat("$user.personnel_info.picture_list", 0)
            })\
            .lookup_bill('PT_Friend_Circle_Message', 'fcm_id', 'id', 'circle')\
            .add_fields({'circle_object': self.ao.array_elemat("$circle", 0)})\
            .match_bill((C("circle_object.content").like(values["content"]) | C("name").like(values["content"])))\
            .lookup_bill('PT_User', 'circle_object.user_id', 'id', 'circle_object_user')\
            .add_fields({'circle_user_name': self.ao.array_elemat("$circle_object_user.name", 0), 'circle_user_picture': self.ao.array_elemat("$circle_object_user.personnel_info.picture_list", 0), 'circle_content': '$circle_object.content'})\
            .sort({'create_date': -1})\
            .project({"_id": 0, "comment._id": 0,  "user": 0, "circle": 0, 'circle_object_user': 0, 'circle_object': 0})
        res = self.page_query(_filter, "PT_Friend_Circle_Like", page, count)
        return res

    def get_circle_list(self, org_list, condition, page, count):
        '''圈子'''
        res = 'Fail'

        keys = ['id', 'name', 'creator_name', 'description']
        values = self.get_value(condition, keys)

        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id') == values['id'])
            & (C('description').like(values['description']))
            & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'creator_id', 'id', 'user')\
            .add_fields({
                "creator_name": self.ao.array_elemat("$user.name", 0),
            })\
            .match_bill((C('creator_name').like(values['creator_name'])))\
            .sort({'create_date': -1})\
            .project({'_id': 0,
                      'id': 1,
                      'name': 1,
                      'creator_name': 1,
                      'description': 1,
                      'create_date': 1,
                      })

        res = self.page_query(_filter, 'PT_Circle', page, count)

        return res

    def update_circle(self, data):
        '''新增编辑圈子'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.circle.value, data, 'PT_Circle')
            if bill_id:
                res = 'Success'
        else:
            data['creator_id'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.circle.value, data, 'PT_Circle')
            if bill_id:
                res = 'Success'
        return res

    def delete_circle(self, ids):
        ''' 删除圈子 '''
        res = 'Fail'
        for i in ids:
            bill_id = self.bill_manage_service.add_bill(
                OperationType.delete.value, TypeId.circle.value, {'id': i}, 'PT_Circle')
        if bill_id:
            res = 'Success'
        return res

    def update_epidemic_prevention(self, data):
        """ 新增工作人员出入档案 """
        res = 'Fail'
        if 'id' in list(data['file_info'].keys()):
            day_info = self.get_epidemic_prevention()
            if len(day_info) > 0:
                data['day_info']['id'] = day_info[0]['id']
                data['day_info']['status'] = None
                bill_id = self.bill_manage_service.add_bill(
                    OperationType.update.value, TypeId.circle.value, data['day_info'], 'PT_Epidemic_Prevention_Day')
                if bill_id:
                    user_id = get_current_user_id(self.session)
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('user_id') == user_id))\
                        .sort({'create_date': -1})\
                        .project({'_id': 0})
                    result = self.query(_filter, 'PT_Epidemic_Prevention_Day')
                    if len(result) > 0:
                        data['file_info']['daily_id'] = result[0]['id']
                        data['file_info']['status'] = None
                        data['file_info']['create_time'] = datetime.datetime.now()
                        new_data = get_info(data['file_info'], self.session)
                        bill_id = self.bill_manage_service.add_bill(
                            OperationType.update.value, TypeId.circle.value, new_data, 'PT_Epidemic_Prevention')
                        if bill_id:
                            res = 'Success'
            else:
                bill_id = self.bill_manage_service.add_bill(
                    OperationType.add.value, TypeId.circle.value, data['day_info'], 'PT_Epidemic_Prevention_Day')
                if bill_id:
                    user_id = get_current_user_id(self.session)
                    _filter = MongoBillFilter()
                    _filter.match_bill((C('user_id') == user_id))\
                        .sort({'create_date': -1})\
                        .project({'_id': 0})
                    result = self.query(_filter, 'PT_Epidemic_Prevention_Day')
                    if len(result) > 0:
                        data['file_info']['daily_id'] = result[0]['id']
                        data['file_info']['create_time'] = datetime.datetime.now()
                        new_data = get_info(data['file_info'], self.session)
                        bill_id = self.bill_manage_service.add_bill(
                            OperationType.update.value, TypeId.circle.value, new_data, 'PT_Epidemic_Prevention')
                        if bill_id:
                            res = 'Success'
        else:
            bill_id_day = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.circle.value, data['day_info'], 'PT_Epidemic_Prevention_Day')
            if bill_id_day:
                user_id = get_current_user_id(self.session)
                _filter = MongoBillFilter()
                _filter.match_bill((C('user_id') == user_id))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                result = self.query(_filter, 'PT_Epidemic_Prevention_Day')
                if len(result) > 0:
                    data['file_info']['daily_id'] = result[0]['id']
                    data['file_info']['create_time'] = datetime.datetime.now()
                    bill_id = self.bill_manage_service.add_bill(
                        OperationType.add.value, TypeId.circle.value, data['file_info'], 'PT_Epidemic_Prevention')
                    if bill_id:
                        res = 'Success'
        return res

    def get_epidemic_prevention(self):
        '''查询当天工作人员出入登记'''
        res = 'Fail'
        user_id = get_current_user_id(self.session)
        if datetime.datetime.now().month < 10:
            start = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        else:
            start = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == user_id)
                           & (C('create_date') > as_date(start))
                           & (C('create_date') < as_date(end)))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Epidemic_Prevention_Day')
        return res

    def get_day_epidemic_prevention_all(self, condition, page, count):
        '''查询某个人每天的出入登记记录'''
        res = 'Fail'
        keys = ["user_id"]
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id') == values['user_id']))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.page_query(
            _filter, 'PT_Epidemic_Prevention_Day', page, count)
        return res

    def get_epidemic_prevention_all(self, org_list, condition, page, count):
        '''查询所有工作人员出入档案'''
        # res = 'Fail'
        # keys = ["id", "type", "user_id", "user_name",
        #         "id_card", "create_date", "org_name", "status"]
        # values = self.get_value(condition, keys)
        # _filter = MongoBillFilter()
        # if condition.get('create_date'):
        #     condition['create_date'] = string_to_date_only(
        #         condition['create_date'].split('T')[0])
        #     end_date = condition['create_date'] + datetime.timedelta(days=1)
        #     _filter.match(
        #         (C('create_date') >= condition['create_date'])
        #         & (C('create_date') <= end_date)
        #     )
        # _filter.match_bill((C('id') == values['id']) & (C('user_id') == values['user_id']) & (C('organization_id').inner(org_list)))\
        #     .lookup_bill('PT_Epidemic_Prevention_Day', 'daily_id', 'id', 'day_info')\
        #     .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
        #     .lookup_bill('PT_User', 'user_id', 'id', 'user_info')\
        #     .add_fields({
        #         "type": '$org_info.organization_info.personnel_category',
        #         "org_name": "$org_info.name",
        #         "user_name": "$user_info.name",
        #         "id_card": "$user_info.id_card",
        #         "phone": "$user_info.personnel_info.telephone",
        #         "area_id": "$user_info.admin_area_id"
        #     })\
        #     .lookup_bill('PT_Administration_Division', 'area_id', 'id', 'area_info')\
        #     .add_fields({
        #         "area_name": '$area_info.name',
        #     })\
        #     .unwind('type')\
        #     .match_bill((C('type') == values['type'])
        #                 & (C('user_name').like(values['user_name']))
        #                 & (C('id_card') == values['id_card'])
        #                 & (C('org_name').like(values['org_name']))
        #                 & (C('status') == values['status']))\
        #     .unwind('day_info')\
        #     .unwind('id_card')\
        #     .sort({'status': 1, 'is_normal': -1,  'modify_date': -1, })\
        #     .project({'_id': 0, 'day_info._id': 0, 'org_info._id': 0, 'user_info._id': 0, 'area_info._id': 0})
        # res = self.page_query(_filter, 'PT_Epidemic_Prevention', page, count)
        # return res

        file_res = 'Fail'
        keys = ["id", "user_id", "create_time", "status",
                "type", "user_name", "id_card",  "org_name"]
        values = self.get_value(condition, keys)

        # 查询工作人员数据
        worker_ids = []
        worker_data = {}
        admin_ids = []
        _filte_filter_workerr_worker = MongoBillFilter()
        _filte_filter_workerr_worker.match_bill((C('personnel_type') == '1')
                                                & (C('personnel_info.personnel_category') == '工作人员')
                                                & (C('name').like(values['user_name']))
                                                & (C('id_card').like(values['id_card']))
                                                & (C('id')==values['user_id'])
                                                & (C('organization_id').inner(org_list)))\
            .project({**get_common_project()})
        res_worker = self.query(
            _filte_filter_workerr_worker, 'PT_User')
        if len(res_worker) > 0:
            for worker in res_worker:
                worker_data[worker['id']] = worker
                worker_ids.append(worker['id'])
                admin_ids.append(worker['admin_area_id'])

        # 查询机构数据
        org_ids = []
        org_data = {}
        org_list_new = []
        _filter_org = MongoBillFilter()
        _filter_org.match_bill((C('personnel_type') == '2')
                               & (C('name').like(values['org_name']))
                               & (C('organization_info.personnel_category') == values['type'])
                               & (C('id').inner(org_list)))\
            .project({**get_common_project()})
        res_org = self.query(
            _filter_org, 'PT_User')
        org_list_new = res_org
        if len(res_org) > 0:
            for org in res_org:
                org_data[org['id']] = org
                org_ids.append(org['id'])

        # 查询人员所属的区域
        admin_data = {}
        _filter_admin = MongoBillFilter()
        _filter_admin.match_bill((C('id').inner(admin_ids)))\
            .project({**get_common_project()})
        res_admin = self.query(_filter_admin, 'PT_Administration_Division')
        if len(res_admin) > 0:
            for admin in res_admin:
                admin_data[admin['id']] = admin
        # 查询档案数据
        file_data = {}
        file_day_id=[]
        _filter_file = MongoBillFilter()
        _filter_file.match_bill((C('id') == values['id'])
                            & (C('user_id').inner(worker_ids))
                            & (C('organization_id').inner(org_ids)))\
            .project({**get_common_project()})
        file_res = self.query(
            _filter_file, 'PT_Epidemic_Prevention')
        if len(file_res) > 0:
            for file_item in file_res:
                file_data[file_item['user_id']] = file_item
                file_day_id.append(file_item['daily_id'])

        # 查询符合条件的信息
        _filter = MongoBillFilter()
        _filter.match_bill((C('user_id').inner(worker_ids))
                        & (C('organization_id').inner(org_ids)))
        if condition.get('status'):
            if condition['status'] == '未判定':
                _filter.match_bill((C('status') == '') | (C('status') == None))
            else:
                _filter.match_bill((C('status') == values['status']))
        if condition.get('id'):
            _filter.match_bill((C('id') == file_day_id[0]))
        start = N()
        end = N()
        if condition.get('create_time'):
            if(bool(1-isinstance(values['create_time'], N))):
                start = values['create_time']+"T00:00:00.00Z"
                end = values['create_time']+"T23:59:59.00Z"
            _filter.match_bill((C('create_date') >= as_date(start))
                            & (C('create_date') <= as_date(end)))
        else:
            if 'id' not in condition and 'user_id' not in condition:
                if datetime.datetime.now().month < 10:
                    start = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                        '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
                    end = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                        '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
                else:
                    start = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                        '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
                    end = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                        '-'+str(datetime.datetime.now().day) +''+"T23:59:59.00Z"
                _filter.match_bill((C('create_date') >= as_date(start))
                        & (C('create_date') <= as_date(end)))
            else:
                if len(file_res)>0:
                    _filter.match_bill((C('id') == file_res[0]['daily_id']))
        _filter.sort({'status': 1})\
            .project({**get_common_project()})
        day_res = self.page_query(
            _filter, 'PT_Epidemic_Prevention_Day', page, count)

        new_res={}
        new_res['total']=day_res['total']
        new_res['result']=[]
        if len(day_res['result'])>0 and len(file_res)>0:
            for i, day_item in enumerate(day_res['result']):
                obj={}
                obj = file_data[day_item['user_id']]
                obj['create_time'] = day_item['modify_date']
                obj['user_name'] = worker_data[day_item['user_id']]['name']
                obj['area_name'] = admin_data[worker_data[day_item['user_id']]['admin_area_id']]['name']
                obj['id_card'] = worker_data[day_item['user_id']]['id_card']
                obj['phone'] = worker_data[day_item['user_id']]['personnel_info']['telephone']
                obj['org_name'] = org_data[day_item['organization_id']]['name']
                obj['day_info'] = day_item
                new_res['result'].append(obj)
        return new_res

        # # 查询符合条件的档案信息
        # _filter = MongoBillFilter()
        # if condition.get('user_id'):
        #     _filter.match_bill((C('user_id') == values['user_id']))
        # else:
        #     _filter.match_bill((C('user_id').inner(worker_ids))
        #                        & (C('organization_id').inner(org_ids)))
        # if condition.get('status'):
        #     if condition['status'] == '未判定':
        #         _filter.match_bill((C('status') == '') | (C('status') == None))
        #     else:
        #         _filter.match_bill((C('status') == values['status']))
        
        # _filter.match_bill((C('id') == values['id']))\
        #     .sort({'modify_date': -1})\
        #     .sort({'status': 1})\
        #     .project({**get_common_project()})
        # file_res = self.page_query(
        #     _filter, 'PT_Epidemic_Prevention', page, count)
        # new_list = {}
        # new_list['result'] = []
        # new_list['total'] = 0
        # user_list = []
        # new_file_list = {}
        # if len(file_res['result']) > 0:
        #     # 日期查询
        #     if condition.get('create_time'):
        #         start = N()
        #         end = N()
        #         if(bool(1-isinstance(values['create_time'], N))):
        #             start = values['create_time']+"T00:00:00.00Z"
        #             end = values['create_time']+"T23:59:59.00Z"
        #         for i, file_item in enumerate(file_res['result']):
        #             if file_item['user_id']:
        #                 user_list.append(file_item['user_id'])
        #                 new_file_list[file_item['user_id']] = file_item
        #         _filter_day_record = MongoBillFilter()
        #         _filter_day_record.match_bill((C('user_id').inner(user_list))
        #                                       & (C('create_date') >= as_date(start))
        #                                       & (C('create_date') <= as_date(end)))\
        #             .sort({'status': 1})\
        #             .project({'_id': 0})
        #         if condition.get('status'):
        #             if condition['status'] == '未判定':
        #                 _filter_day_record.match_bill((C('status') == '') | (C('status') == None))
        #             else:
        #                 _filter_day_record.match_bill((C('status') == values['status']))
        #         day_record = self.query(
        #             _filter_day_record, 'PT_Epidemic_Prevention_Day')
        #         for i, day_item in enumerate(day_record):
        #             new_file_list[day_item['user_id']
        #                           ]['daily_id'] = day_item['id']
        #             new_file_list[day_item['user_id']
        #                           ]['create_time'] = day_item['create_date']
        #             new_file_list[day_item['user_id']]['day_info'] = day_item
        #             if 'status' in day_item:
        #                 new_file_list[day_item['user_id']]['status'] = day_item['status']
        #             new_file_list[day_item['user_id']
        #                           ]['user_name'] = worker_data[day_item['user_id']]['name']
        #             new_file_list[day_item['user_id']
        #                           ]['org_name'] = org_data[day_item['organization_id']]['name']
        #             new_file_list[day_item['user_id']
        #                           ]['id_card'] = worker_data[day_item['user_id']]['id_card']
        #             new_file_list[day_item['user_id']]['phone'] = worker_data[day_item['user_id']
        #                                                                       ]['personnel_info']['telephone']
        #             new_file_list[day_item['user_id']
        #                           ]['area_name'] = admin_data[worker_data[day_item['user_id']]['admin_area_id']]['name']
        #             new_list['result'].append(
        #                 new_file_list[day_item['user_id']])
        #             new_list['total'] = file_res['total']

        #     else:
        #         daily_id_list = []
        #         # print(111)
        #         for i, file_item in enumerate(file_res['result']):
        #             if file_item.get('daily_id'):
        #                 daily_id_list.append(file_item['daily_id'])
        #                 new_file_list[file_item['user_id']] = file_item
        #         _filter_day_record = MongoBillFilter()
        #         _filter_day_record.match_bill((C('id').inner(daily_id_list)))\
        #             .sort({'status': 1})\
        #             .project({'_id': 0})
        #         day_record = self.query(
        #             _filter_day_record, 'PT_Epidemic_Prevention_Day')
        #         for i, day_item in enumerate(day_record):
        #             file_res['result'][i]['create_time'] = day_item['create_date']
        #             file_res['result'][i]['day_info'] = day_item
        #             if 'status' in day_item:
        #                 file_res['result'][i]['status'] = day_item['status']
        #             file_res['result'][i]['user_name'] = worker_data[day_item['user_id']]['name']
        #             file_res['result'][i]['org_name'] = org_data[day_item['organization_id']]['name']
        #             file_res['result'][i]['id_card'] = worker_data[day_item['user_id']]['id_card']
        #             file_res['result'][i]['phone'] = worker_data[day_item['user_id']
        #                                                          ]['personnel_info']['telephone']
        #             file_res['result'][i]['area_name'] = admin_data[worker_data[day_item['user_id']]
        #                                                             ['admin_area_id']]['name']
        #         new_list = file_res

        # return new_list

    # def get_epidemic_prevention_all_yh(self, org_list, condition, page, count):
    #     '''优化查询所有工作人员出入档案'''
    #     res = 'Fail'
    #     keys = ["id", "user_id", "create_time", "status",
    #             "type", "user_name", "id_card",  "org_name"]
    #     values = self.get_value(condition, keys)

    #     # 查询工作人员数据
    #     worker_ids = []
    #     worker_data = {}
    #     if condition.get('user_name') or condition.get('id_card'):
    #         _filter_worker = MongoBillFilter()
    #         _filter_worker.match_bill((C('personnel_type') == '1')
    #                                   & (C('personnel_info.personnel_category') == '工作人员')
    #                                   & (C('name').like(values['user_name']))
    #                                   & (C('id_card').like(values['id_card'])))\
    #             .project({'login_info': 0, **get_common_project()})
    #         res_worker = self.query(
    #             _filter_worker, 'PT_User')
    #         if len(res_worker) > 0:
    #             for worker in res_worker:
    #                 worker_data[worker['id']] = worker
    #                 worker_ids.append(worker['id'])

    #     # 查询机构数据
    #     org_ids = []
    #     org_data = {}
    #     org_list_new = []
    #     if condition.get('org_name') or condition.get('type'):
    #         _filter_org = MongoBillFilter()
    #         _filter_org.match_bill((C('personnel_type') == '2')
    #                                & (C('name').like(values['org_name']))
    #                                & (C('organization_info.personnel_category') == values['type'])
    #                                & (C('organization_id').inner(org_list)))\
    #             .project({'qualification_info': 0, 'main_account': 0, **get_common_project()})
    #         res_org = self.query(
    #             _filter_org, 'PT_User')
    #         org_list_new = res_org
    #         if len(res_org) > 0:
    #             for org in res_org:
    #                 org_data[org['id']] = org
    #                 org_ids.append(org['id'])

    #     _filter = MongoBillFilter()
    #     if condition.get('user_name') or condition.get('id_card'):
    #         _filter.match((C('user_id').inner(worker_ids)))
    #     if condition.get('org_name') or condition.get('type'):
    #         _filter.match((C('organization_id').inner(org_ids)))
    #     res = {}
    #     res['total'] = 0
    #     res['result'] = []
    #     # 根据时间筛选
    #     if condition.get('create_time'):
    #         print(values['create_time'])
    #         start = N()
    #         end = N()
    #         if(bool(1-isinstance(values['create_time'], N))):
    #             start = values['create_time']+"T00:00:00.00Z"
    #             end = values['create_time']+"T23:59:59.00Z"
    #         # 查询所属类型下的所有档案数据
    #         _filter.match_bill((C('organization_id').inner(org_ids))
    #                            & (C('status') == values['status']))\
    #             .sort({'status': 1, 'is_normal': -1,  'modify_date': -1, })\
    #             .project({'_id': 0})
    #         file_res = self.query(_filter, 'PT_Epidemic_Prevention')
    #         # for i, org in enumerate(org_list_new):
    #         #     for i, file_item in enumerate(file_res):
    #         #         if file_item['organization_id'] == org['id'] and org['organization_info']['personnel_category'] != values['type']:
    #         #             del file_res[i]
    #         for i, file_item in enumerate(file_res):
    #             # 根据id查找出日期当天最新的那条记录
    #             if file_item['user_id']:
    #                 _filter_day_record = MongoBillFilter()
    #                 _filter_day_record.match_bill((C('user_id') == file_item['user_id'])
    #                                               & (C('create_date') >= as_date(start))
    #                                               & (C('create_date') <= as_date(end)))\
    #                     .project({'_id': 0})
    #                 day_record = self.query(
    #                     _filter_day_record, 'PT_Epidemic_Prevention_Day')
    #                 if len(day_record) > 0:
    #                     print(file_res[i], '牛啊')
    #                     file_res[i]['daily_id'] = day_record[0]['id']
    #                     file_res[i]['create_time'] = day_record[0]['create_date']
    #                     res['result'] = file_res
    #                     res['total'] = len(file_res)
    #                 else:
    #                     del file_res[i]
    #                     res['result'] = file_res
    #                     res['total'] = len(file_res) - 1
    #     else:
    #         _filter.match_bill((C('id') == values['id'])
    #                            & (C('user_id') == values['user_id'])
    #                            & (C('organization_id').inner(org_list))
    #                            & (C('status') == values['status']))\
    #             .sort({'status': 1, 'is_normal': -1,  'modify_date': -1, })\
    #             .project({**get_common_project()})
    #         res = self.page_query(
    #             _filter, 'PT_Epidemic_Prevention', page, count)
    #     admin_ids = []
    #     day_ids = []

    #     for i, x in enumerate(res['result']):
    #         day_ids.append(res['result'][i]['daily_id'])
    #         if 'org_name' not in condition or 'type' not in condition:
    #             org_ids.append(res['result'][i]['organization_id'])
    #         if 'user_name' not in condition or 'id_card' not in condition:
    #             worker_ids.append(res['result'][i]['user_id'])

    #     day_data = {}
    #     if len(day_ids) > 0:
    #         _filter_day = MongoBillFilter()
    #         _filter_day.match_bill((C('id').inner(day_ids)))\
    #             .project({**get_common_project()})
    #         res_day = self.query(
    #             _filter_day, 'PT_Epidemic_Prevention_Day')
    #         if len(res_day) > 0:
    #             for day in res_day:
    #                 day_data[day['id']] = day

    #     if len(org_ids) > 0:
    #         _filter_org = MongoBillFilter()
    #         _filter_org.match_bill((C('personnel_type') == '2')
    #                                & (C('id').inner(org_ids)))\
    #             .project({'qualification_info': 0, 'main_account': 0, **get_common_project()})
    #         res_org = self.query(_filter_org, 'PT_User')
    #         if len(res_org) > 0:
    #             for org in res_org:
    #                 org_data[org['id']] = org
    #     if len(worker_ids) > 0:
    #         _filter_worker = MongoBillFilter()
    #         _filter_worker.match_bill((C('personnel_type') == '1')
    #                                   & (C('personnel_info.personnel_category') == '工作人员')
    #                                   & (C('id').inner(worker_ids)))\
    #             .project({'login_info': 0, **get_common_project()})
    #         res_worker = self.query(
    #             _filter_worker, 'PT_User')
    #         if len(res_worker) > 0:
    #             for worker in res_worker:
    #                 worker_data[worker['id']] = worker

    #     for i, x in enumerate(res['result']):

    #         if 'daily_id' in res['result'][i] and res['result'][i]['daily_id'] in day_data.keys():
    #             res['result'][i]['day_info'] = day_data[res['result'][i]['daily_id']]

    #         if 'organization_id' in res['result'][i] and res['result'][i]['organization_id'] in org_data.keys():
    #             res['result'][i]['type'] = org_data[res['result'][i]
    #                                                 ['organization_id']]['organization_info']['personnel_category']
    #             res['result'][i]['org_name'] = org_data[res['result']
    #                                                     [i]['organization_id']]['name']
    #             res['result'][i]['org_info'] = org_data[res['result']
    #                                                     [i]['organization_id']]

    #         if 'user_id' in res['result'][i] and res['result'][i]['user_id'] in worker_data.keys():
    #             res['result'][i]['user_name'] = worker_data[res['result']
    #                                                         [i]['user_id']]['name']
    #             res['result'][i]['id_card'] = worker_data[res['result']
    #                                                       [i]['user_id']]['id_card']
    #             res['result'][i]['area_id'] = worker_data[res['result']
    #                                                       [i]['user_id']]['admin_area_id']
    #             res['result'][i]['phone'] = worker_data[res['result']
    #                                                     [i]['user_id']]['personnel_info']['telephone']
    #             res['result'][i]['user_info'] = worker_data[res['result'][i]['user_id']]
    #         if 'area_id' in res['result'][i].keys():
    #             admin_ids.append(res['result'][i]['area_id'])
    #     admin_data = {}
    #     if len(admin_ids) > 0:
    #         _filter_admin = MongoBillFilter()
    #         _filter_admin.match_bill((C('id').inner(admin_ids)))\
    #             .project({**get_common_project()})
    #         res_admin = self.query(_filter_admin, 'PT_Administration_Division')
    #         if len(res_admin) > 0:
    #             for admin in res_admin:
    #                 admin_data[admin['id']] = admin

    #     for i, x in enumerate(res['result']):
    #         if 'area_id' in res['result'][i] and res['result'][i]['area_id'] in admin_data.keys():
    #             res['result'][i]['area_info'] = admin_data[res['result'][i]['area_id']]
    #             res['result'][i]['area_name'] = admin_data[res['result']
    #                                                        [i]['area_id']]['name']

    #     return res

    def decide_epidemic_prevention(self, data):
        """ 判定 """
        res = 'Fail'
        keys = ["id", 'status']
        values = self.get_value(data, keys)
        _filter = MongoBillFilter()
        _filter.match_bill((C('id') == values['id']))\
            .project({'_id': 0})
        result = self.query(_filter, 'PT_Epidemic_Prevention')
        if len(result) > 0:
            _filter2 = MongoBillFilter()
            _filter2.match_bill((C('id') == result[0]['daily_id']))\
                .project({'_id': 0})
            day_info = self.query(_filter2, 'PT_Epidemic_Prevention_Day')
            day_info[0]['status'] = values['status']
            result[0]['status'] = values['status']
            record_info = {}
            record_info['record_id'] = result[0]['id']
            record_info['caozuo_time'] = datetime.datetime.now()
            record_info['caozuo_ren'] = get_current_user_id(self.session)
            record_info['result'] = values['status']
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.circle.value, result[0], 'PT_Epidemic_Prevention')
            bill_id2 = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.circle.value, day_info[0], 'PT_Epidemic_Prevention_Day')
            bill_id3 = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.circle.value, record_info , 'PT_Epidemic_Prevention_Record')
            if bill_id and bill_id2 and bill_id3:
                res = 'Success'
        return res

    def decide_by_once(self, org_list, data, recode):
        keys = ["id", 'status', 'type']
        res = "Fail"
        values = self.get_value(data, keys)
        # 查询今天所有未判定的记录
        if datetime.datetime.now().month < 10:
            start = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        else:
            start = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        _filter = MongoBillFilter()
        if recode:
            _filter.match_bill(C('id').inner(recode))\
                .project({'_id': 0})
        else:
            _filter.match_bill((C('status') == None)
                               | (C('status') == '')
                               & (C('create_date') > as_date(start))
                               & (C('create_date') < as_date(end))
                               & (C('organization_id').inner(org_list)))\
                .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
                .add_fields({
                    "type": '$org_info.organization_info.personnel_category'
                })\
                .unwind('type')\
                .match_bill((C('type') == values['type']))\
                .sort({'create_date': -1})\
                .project({'_id': 0, 'org_info._id': 0})
        res_info = self.query(_filter, 'PT_Epidemic_Prevention_Day')
        list1 = []
        list2 = []
        if len(res_info) > 0:
            for item in res_info:
                _filter2 = MongoBillFilter()
                _filter2.match_bill((C('daily_id') == item['id']))\
                    .project({'_id': 0})
                result = self.query(_filter2, 'PT_Epidemic_Prevention')
                if len(result) > 0:
                    result[0]['status'] = values['status']
                    item['status'] = values['status']
                    list1.append(result[0])
                    list2.append(item)
        bill_id = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.circle.value, list2, 'PT_Epidemic_Prevention_Day')
        bill_id2 = self.bill_manage_service.add_bill(
            OperationType.update.value, TypeId.circle.value,  list1, 'PT_Epidemic_Prevention')
        if bill_id and bill_id2:
            res = 'Success'
        else:
            res = '暂无待判定的数据'
        return res

    def update_day_info(self, data):
        """ 更新每日登记信息 """
        res = 'Fail'
        day_info = self.get_epidemic_prevention()
        if len(day_info) > 0:
            data['id'] = day_info[0]['id']
            data['status'] = None
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.circle.value, data, 'PT_Epidemic_Prevention_Day')
            if bill_id:
                user_id = get_current_user_id(self.session)
                _filter = MongoBillFilter()
                _filter.match_bill((C('user_id') == user_id))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                result = self.query(_filter, 'PT_Epidemic_Prevention')
                if len(result) > 0:
                    result[0]['daily_id'] = day_info[0]['id']
                    result[0]['status'] = None
                    result[0]['create_time'] = datetime.datetime.now()
                    bill_id = self.bill_manage_service.add_bill(
                        OperationType.update.value, TypeId.circle.value, result[0], 'PT_Epidemic_Prevention')
                    if bill_id:
                        res = 'Success'
        else:
            data['status'] = None
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.circle.value, data, 'PT_Epidemic_Prevention_Day')
            if bill_id:
                day_info = self.get_epidemic_prevention()
                user_id = get_current_user_id(self.session)
                _filter = MongoBillFilter()
                _filter.match_bill((C('user_id') == user_id))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                result = self.query(_filter, 'PT_Epidemic_Prevention')
                if len(result) > 0:
                    result[0]['daily_id'] = day_info[0]['id']
                    result[0]['status'] = None
                    result[0]['create_time'] = datetime.datetime.now()
                    bill_id = self.bill_manage_service.add_bill(
                        OperationType.update.value, TypeId.circle.value, result[0], 'PT_Epidemic_Prevention')
                    if bill_id:
                        res = 'Success'
        return res

    def update_org_day_info(self, data):
        '''新增机构每日登记信息'''
        res = 'Fail'
        if 'id' in list(data.keys()):
            bill_id = self.bill_manage_service.add_bill(
                OperationType.update.value, TypeId.circle.value, data, 'PT_Org_Epidemic_Prevention')
            if bill_id:
                res = 'Success'
        else:
            data['creator_id'] = get_current_user_id(self.session)
            bill_id = self.bill_manage_service.add_bill(
                OperationType.add.value, TypeId.circle.value, data, 'PT_Org_Epidemic_Prevention')
            if bill_id:
                res = 'Success'
        return res

    def get_org_day_info(self, data):
        '''查询机构每日登记信息'''
        res = 'Fail'
        user_id = get_current_user_id(self.session)
        if datetime.datetime.now().month < 10:
            start = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        else:
            start = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        _filter = MongoBillFilter()
        _filter.match_bill((C('organization_id') == data['org_id'])
                           & (C('create_date') > as_date(start))
                           & (C('create_date') < as_date(end)))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Org_Epidemic_Prevention')
        return res

    def get_org_day_info_all(self, org_list, condition, page, count):
        '''查询机构所有每日登记信息'''
        res = 'Fail'
        keys = ["id", "area_name", "org_name",
                'modify_date', 'organization_id']
        values = self.get_value(condition, keys)
        _filter = MongoBillFilter()
        if condition.get('modify_date'):
            condition['modify_date'] = string_to_date_only(
                condition['modify_date'].split('T')[0])
            end_date = condition['modify_date'] + datetime.timedelta(days=1)
            _filter.match(
                (C('modify_date') >= condition['modify_date'])
                & (C('modify_date') <= end_date)
            )
        _filter.match_bill((C('id') == values['id']) & (C('organization_id').inner(org_list)))\
            .lookup_bill('PT_User', 'organization_id', 'id', 'org_info')\
            .add_fields({
                "area_id": "$org_info.admin_area_id",
            })\
            .lookup_bill('PT_User', 'creator_id', 'id', 'user_info')\
            .lookup_bill('PT_Administration_Division', 'area_id', 'id', 'area_info')\
            .add_fields({
                "org_name": "$org_info.name",
                "area_name": '$area_info.name',
                "user_name": '$user_info.name',
            })\
            .match_bill((C('area_name').like(values['area_name'])) & (C('org_name').like(values['org_name'])) & (C('organization_id') == values['organization_id']))\
            .project({
                'user_info.login_info': 0,
                'org_info.qualification_info': 0,
                'org_info.main_account': 0,
                **get_common_project({'org_info', 'area_info', 'user_info'})
            })
        res = self.page_query(
            _filter, 'PT_Org_Epidemic_Prevention', page, count)
        return res

    def upload_day_info_excl(self, data):
        res = '导入失败'
      # print(data)
        pf = pd.DataFrame(data)
        pf.rename(columns={
            "证件号码": 'id_card',
            "近期住址": 'backWork_address',
            "体温": 'temperature',
            "出现症状": 'symptom',
            "是否逗留或途径疫情重灾区（湖北等地）": 'stay_disaster_area',
            "是否接触过疫情重灾区人员（从湖北等地返回）": 'contact_disaster_area_people',
            "一月以来是否有工作地/常住地以外的旅行史": 'trip_history',
            "出发日期": 'begin_date',
            "出发交通工具": 'backWork_transportation',
            "停留城市": 'stay_city',
            "是否已回到本市": 'is_back',
            "到达日期": 'arrive_time',
            "到达交通工具": 'reach_transportation',
            "预计到达日期": 'unarrive_time',
            "是否有同行人": 'has_partner',
            "同行人数": 'partner_num',
            "行程描述": 'trip_description',
            "当天是否提供服务": 'is_work',
        }, inplace=True)
        id_card_list = pf['id_card'].values.tolist()
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('personnel_info.id_card').inner(id_card_list) | (C('id_card').inner(id_card_list))
             )
        )\
            .project({"_id": 0})
        res = self.query(_filter, 'PT_User')
        user_pf = pd.DataFrame(res)
        not_exit = []  # 不存在的长者
        for x in id_card_list:
            if not x in user_pf['id_card'].values.tolist() and not x in user_pf['personnel_info']['id_card'].values.tolist():
                not_exit.append(x)
        if len(not_exit):
            return str(not_exit) + '这些工作人员不存在，请先导入工作人员数据'
        else:
            user_pf = user_pf[['id', 'id_card']]
            new_pf = pd.merge(pf, user_pf, on=['id_card'])
            new_pf.rename(columns={'id': 'user_id', }, inplace=True)
            new_data = dataframe_to_list(new_pf)
            not_exit_file = []  # 不存在的档案
            for item in new_data:
                user_id = item['user_id']
                # 判断这个人有没有档案
                _filter2 = MongoBillFilter()
                _filter2.match_bill((C('user_id') == user_id))\
                    .sort({'create_date': -1})\
                    .project({'_id': 0})
                file_info = self.query(_filter2, 'PT_Epidemic_Prevention')
                if len(file_info) == 0:
                    not_exit_file.append(item['id_card'])
            if len(not_exit_file) > 0:
                return str(not_exit_file) + '这些工作人员不存在防疫档案，请先建立档案'
            else:
                # 判断今天有没有数据
                for item in new_data:
                    user_id = item['user_id']
                    res_day = self.get_today_info_by_userid(user_id)
                    if len(res_day) > 0:
                        item['id'] = res_day[0]['id']
                        del item['id_card']
                        bill_id = self.bill_manage_service.add_bill(
                            OperationType.update.value, TypeId.circle.value, item, 'PT_Epidemic_Prevention_Day')
                        if bill_id:
                            res = '导入成功'
                    else:
                        del item['id_card']
                        bill_id = self.bill_manage_service.add_bill(
                            OperationType.add.value, TypeId.circle.value, item, 'PT_Epidemic_Prevention_Day')
                        if bill_id:
                            res = '导入成功'
                    # 更新档案信息
                    _filter3 = MongoBillFilter()
                    _filter3.match_bill((C('user_id') == user_id))\
                        .sort({'create_date': -1})\
                        .project({'_id': 0})
                    file_info = self.query(_filter3, 'PT_Epidemic_Prevention')
                    _filter4 = MongoBillFilter()
                    _filter4.match_bill((C('user_id') == user_id))\
                        .sort({'create_date': -1})\
                        .project({'_id': 0})
                    new_day_info = self.query(
                        _filter4, 'PT_Epidemic_Prevention_Day')
                    if len(file_info) > 0 and len(new_day_info) > 0:
                        file_info[0]['daily_id'] = new_day_info[0]['id']
                        file_info[0]['status'] = None
                        bill_id = self.bill_manage_service.add_bill(
                            OperationType.update.value, TypeId.circle.value, file_info, 'PT_Epidemic_Prevention')
                        if bill_id:
                            res = '导入成功'

        return res

    def get_today_info_by_userid(self, user_id):
        if datetime.datetime.now().month < 10:
            start = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+'0'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        else:
            start = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T00:00:00.00Z"
            end = str(datetime.datetime.now().year)+'-'+str(datetime.datetime.now().month) + \
                '-'+str(datetime.datetime.now().day)+''+"T23:59:59.00Z"
        _filter2 = MongoBillFilter()
        _filter2.match_bill((C('user_id') == user_id)
                            & (C('create_date') > as_date(start))
                            & (C('create_date') < as_date(end)))\
            .sort({'create_date': -1})\
            .project({'_id': 0})
        res_day = self.query(_filter2, 'PT_Epidemic_Prevention_Day')
        return res_day
