'''说明
 1、新闻相关接口注册
'''
from server.service.app.announcement import AnnouncementService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    announcement_service = AnnouncementService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAnnouncementDetailService.get_announcement_detail')
    def __get_announcement_detail(data):
        res = announcement_service.get_announcement_detail(data)
        return res

    @jsonrpc.method('IAnnouncementListService.get_announcement_list')
    def __get_announcement_list():
        res = announcement_service.get_announcement_list({'type': "公告"})
        return res
