'''
 1、评论相关接口注册
'''
from server.service.app.comment import CommentService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    comment_service = CommentService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICommentService.get_comment_list_all')
    def __get_comment_list(condition):
        res = comment_service.get_comment_list(condition)
        return res

    @jsonrpc.method('ICommentService.get_my_comment')
    def __get_my_comment():
        res = comment_service.get_my_comment()
        return res

    @jsonrpc.method('ICommentService.add_comment')
    def __add_comment(comment):
        res = comment_service.add_comment(comment)
        return res

    @jsonrpc.method('ICommentService.give_a_like')
    def __give_a_like(data):
        res = comment_service.give_a_like(data)
        return res
