'''说明
 1、新闻相关接口注册
'''
from server.service.app.news import NewsService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    news_service = NewsService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('INewsDetailService.get_news_detail')
    def __get_news_detail(data):
        res = news_service.get_news_detail(data)
        return res

    @jsonrpc.method('INewsListService.get_news_list')
    def __get_news_list(condition, page=None, count=None):
        res = news_service.get_all_list(condition, page, count)
        # res = news_service.get_all_list({'type': '新闻'})
        return res
