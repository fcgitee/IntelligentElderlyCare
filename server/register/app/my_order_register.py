from server.service.app.my_order import MyOrderService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:44:24
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\app\my_order_register.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday August 6th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 6th August 2019 6:08:31 pm
修改者: ymq(ymq) - <<email>>

说明
 1、我的订单部分接口注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, realm_name, wechat_payment_data, alipay_payment_data, session):
    my_order_service = MyOrderService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceOrderService.my_order_list')
    def __my_order_list(condition, page=None, count=None):
        res = my_order_service.my_order_list(condition, page, count)
        return res

    @jsonrpc.method('IServiceOrderService.service_order')
    def __service_order(condition, page=None, count=None):
        res = my_order_service.service_order(condition, page, count)
        return res

    @jsonrpc.method('IServiceOrderService.upload_service_img')
    def __upload_service_img(condition):
        res = my_order_service.upload_service_img(condition)
        return res

    @jsonrpc.method('IServiceOrderService.wechat_payment')
    def __wechat_payment_request(condition):
        res = my_order_service.wechat_payment_request(
            condition, realm_name, wechat_payment_data)
        return res

    @jsonrpc.method('IServiceOrderService.alipay')
    def __alipay_request(condition):
        res = my_order_service.alipay_request(
            condition, realm_name, alipay_payment_data)
        return res

    @jsonrpc.method('IServiceOrderService.third_trade_query')
    def __third_trade_query(condition):
        res = my_order_service.third_trade_query(condition)
        return res

    @jsonrpc.method('IServiceOrderService.get_user_id')
    def __get_user_id():
        res = my_order_service.get_user_id()
        return res
