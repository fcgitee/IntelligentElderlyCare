'''
版权：Copyright (c) 2019 China

创建日期：Monday August 5th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Monday, 5th August 2019 4:11:07 pm
修改者: ymq(ymq) - <<email>>

说明
 1、app首页接口注册
'''
from server.service.app.home_page import HomePageService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    home_page_service = HomePageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHomepageService.update_rotary_picture')
    def __update_rotary_picture(data):
        res = home_page_service.update_rotary_picture(data)
        return res

    @jsonrpc.method('IHomepageService.get_rotary_picture')
    def __get_rotary_picture():
        res = home_page_service.get_rotary_picture()
        return res
