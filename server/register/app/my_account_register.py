'''
@Author: your name
@Date: 2019-11-08 09:21:19
@LastEditTime: 2019-12-05 15:04:34
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\app\my_account_register.py
'''
from ...service.app.my_account import MyAccountService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N
# -*- coding: utf-8 -*-
'''
活动管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    my_account_manage_func = MyAccountService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMyAccountService.get_my_account')
    def __get_my_account():
        res = my_account_manage_func.get_my_account()
        return res

    @jsonrpc.method('IMyAccountService.get_account_detail')
    def __get_account_detail(account_data):
        res = my_account_manage_func.get_account_detail(account_data)
        return res
