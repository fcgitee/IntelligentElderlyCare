from server.service.app.service_detail import ServiceProductDetailService


'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:43:27
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\app\service_detail_register.py
'''
'''
 1、服务详情服务注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_package_detail_service = ServiceProductDetailService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceProductDetailService.get_service_package_detail')
    def __get_service_detail(pk_id):
        res = service_package_detail_service.get_service_package_detail(pk_id)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_service_product_package_detail')
    def __get_service_product_package_detail(condition, page, count):
        res = service_package_detail_service.get_service_product_package_detail(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_service_product_list')
    def __get_service_product_list(condition, page=None, count=None):
        res = service_package_detail_service.get_service_product_list_yh(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_service_product_item_package_list')
    def __get_service_product_item_package_list(condition, page=None, count=None):
        res = service_package_detail_service.get_service_product_item_package_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_product_package_list')
    def __get_product_package_list(condition, page=None, count=None):
        res = service_package_detail_service.get_product_package_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_server_list')
    def __get_service_product_list(condition, page, count):
        res = service_package_detail_service.get_server_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceProductDetailService.get_server_worker_list')
    def __get_service_product_list(condition, page=None, count=None):
        res = service_package_detail_service.get_server_worker_list(
            condition, page, count)
        return res
