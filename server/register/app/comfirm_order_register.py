from server.service.app.confirm_order import ComfirmOrderService


'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime : 2020-01-20 11:45:20
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\app\comfirm_order_register.py
'''
'''
 1、确认订单接口注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    create_order_service = ComfirmOrderService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IComfirmOrderService.create_product_order')
    def __create_product_order(data, is_paying_first='1'):
        '''创建订单'''
        res = create_order_service.create_product_order(data, is_paying_first)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务下单', '服务购买')
        return res

    @jsonrpc.method('IComfirmOrderService.batch_create_order')
    def __batch_create_order(data, price):
        '''创建订单'''
        res = create_order_service.batch_create_order(
            data, price)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务下单', '服务购买')
        return res

    @jsonrpc.method('IComfirmOrderService.get_purchaser_detail')
    def __get_purchaser_detail():
        '''获取购买者信息'''
        res = create_order_service.get_purchaser_detail()
        return res

    @jsonrpc.method('IComfirmOrderService.change_trade_status')
    def __change_trade_status(status):
        '''修改订单状态'''
        res = create_order_service.change_trade_status(status)
        return res

    @jsonrpc.method('IComfirmOrderService.update_reserve_service')
    def __update_reserve_service(data):
        '''编辑预约服务'''
        res = create_order_service.update_reserve_service(data)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务预定', '新建/编辑服务预定')
        return res

    @jsonrpc.method('IComfirmOrderService.get_reserve_service')
    def __get_reserve_service(condition, page=None, count=None):
        '''查询服务预定'''
        res = create_order_service.get_reserve_service(
            condition, page, count)
        return res

    @jsonrpc.method('IComfirmOrderService.delete_reserve_service')
    def __delete_reserve_service(condition):
        '''删除服务预定'''
        res = create_order_service.delete_reserve_service(condition)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务预定', '删除服务预定')
        return res

    @jsonrpc.method('IComfirmOrderService.one_key_order')
    def __one_key_order(item_list):
        '''一键下单'''
        res = create_order_service.one_key_order(item_list)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务下单', '一键下单')
        return res

    @jsonrpc.method('IComfirmOrderService.get_rt_order_list')
    def __get_rt_order_list(condition, page=None, count=None):
        '''查询服务预定'''
        res = create_order_service.get_rt_order_list(
            condition, page, count)
        return res

    @jsonrpc.method('IComfirmOrderService.update_rt_order')
    def __update_rt_order(data):
        '''查询服务预定'''
        res = create_order_service.update_rt_order(
            data)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务下单', '服务购买')
        return res

    @jsonrpc.method('IComfirmOrderService.all_buy_order')
    def __all_buy_order():
        '''全部预约服务下单'''
        res = create_order_service.all_buy_order()
        return res

    @jsonrpc.method('IComfirmOrderService.reset_record')
    def __reset_record(item_list):
        '''一键重置服务记录'''
        res = create_order_service.reset_record(item_list)
        create_order_service.bill_manage_server.insert_logs(
            '平台运营方', '服务记录', '重置服务记录')
        return res
