'''
 1、我的页面接口注册
'''
from server.service.app.mine_home import MineHomeService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    mine_home_service = MineHomeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMineHomeService.get_my_info')
    def __get_my_info():
        res = mine_home_service.get_my_info()
        return res
