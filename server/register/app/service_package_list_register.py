from ...service.security_module import SecurityModule, FunctionName, PermissionName
from server.service.app.service_package_list import ServicePackageListService
from ...service.common import insert_data, find_data, update_data, delete_data, get_condition, get_info, get_cur_time, get_current_organization_id, execute_python, get_current_role_id, get_current_user_id, get_common_project

'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-09-25 09:48:29
@LastEditors: your name
'''
'''
 1、服务列表服务注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_package_list_service = ServicePackageListService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServicePackageListService.get_service_package_list')
    def __get_service_package_list():
        res = service_package_list_service.get_service_package_list()
        return res

    @jsonrpc.method('IServicePackageListService.get_service_product_package_list')
    def __get_service_product_package_list(condition, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.service_package_list, PermissionName.query)
        res = service_package_list_service.get_service_product_package_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServicePackageListService.get_service_product_package_all')
    def __get_service_product_package_all(condition):
        res = service_package_list_service.get_service_product_package_all(
            condition)
        return res

    @jsonrpc.method('IServiceItemPackage.get_service_product_list')
    def __get_service_product_package_list(condition, page=None, count=None):
        org_list = get_current_organization_id(session)
      #print('组织机构', org_list)
        # raise '错误'
        res = service_package_list_service.get_service_product_list(
            [org_list])
        return res
