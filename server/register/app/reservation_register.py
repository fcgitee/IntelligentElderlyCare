from server.service.app.reservation import ReservationService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:31:44
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\app\reservation_register.py
'''

'''
 1、家属预约注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd,  inital_password, session):
    reservation_service = ReservationService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IReservationService.add_reservation')
    def __add_reservation(data):
        res = reservation_service.add_reservation(data)
        return res
