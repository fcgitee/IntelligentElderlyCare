'''
版权：Copyright (c) 2019 China

创建日期：Friday November 8th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Friday, 8th November 2019 10:58:41 am
修改者: ymq(ymq) - <<email>>

说明
 1、
'''
from server.service.app.app_pay import FinanicalAppPay


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    app_pay_service = FinanicalAppPay(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAppPayService.receive_pay_info')
    def __receive_pay_info(pay_info):
        res = app_pay_service.receive_pay_info(pay_info)
        return res

    @jsonrpc.method('IAppPayService.confirm_pay')
    def __confirm_pay(pay_type, pay_info):
        res = app_pay_service.confirm_financial_sys(pay_type, pay_info)
        return res

    @jsonrpc.method('IAppPayService.insert')
    def __insert_data(data, collection_name):
        res = app_pay_service.insert_data(data, collection_name)
        return res
