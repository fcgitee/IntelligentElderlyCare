'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-15 12:11:54
@LastEditTime: 2020-02-20 09:37:17
@LastEditors: Please set LastEditors
'''

from ...service.buss_mis.allowance_manage import AllowanceManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N

# -*- coding: utf-8 -*-
'''
补贴管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    allowance_manage_func = AllowanceManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAllowanceService.get_allowance_list_all')
    def __get_allowance_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_worker_allowance_apply_all, PermissionName.query)
        res = allowance_manage_func.get_allowance_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_list_look')
    def __get_allowance_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_worker_allowance_apply_look, PermissionName.query)
        res = allowance_manage_func.get_allowance_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_list')
    def __get_allowance_list(condition, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.service_worker_allowance_apply_look, PermissionName.query)
        res = allowance_manage_func.get_allowance_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_currentUser_allowance_list')
    def __get_allowance_list(condition, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.service_worker_allowance_apply_look, PermissionName.query)
        res = allowance_manage_func.get_currentUser_allowance_list(
            condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_applied_list_look_edit')
    def __get_allowance_applied_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.allowance_check_look_edit, PermissionName.query)
        res = allowance_manage_func.get_allowance_applied_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.update_allowance')
    def __update_allowance(allowance):
        res = allowance_manage_func.update_allowance(allowance)
        allowance_manage_func.bill_manage_server.insert_logs(
            '机构养老', '从业人员补贴申请', '新增/编辑从业人员补贴申请')
        return res

    @jsonrpc.method('IAllowanceService.del_allowance')
    def __del_allowance(ids):
        __p = security_service.judge_permission_other(
            FunctionName.service_worker_allowance_apply_all, PermissionName.delete)
        res = allowance_manage_func.del_allowance(ids)
        allowance_manage_func.bill_manage_server.insert_logs(
            '机构养老', '从业人员补贴申请', '删除从业人员补贴申请')
        return res

    @jsonrpc.method('IAllowanceService.get_assessment_template')
    def __get_assessment_template():
        permission_data = security_service.judge_permission_query(
            FunctionName.allowance_definition_all, PermissionName.query)
        res = allowance_manage_func.get_assessment_template(permission_data)
        return res

    @jsonrpc.method('IAllowanceService.add_allowance_define')
    def __add_allowance_define(allowance_define):
        if 'id' in list(allowance_define.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.allowance_definition_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.allowance_definition_all, PermissionName.add)
        res = allowance_manage_func.add_allowance_define(allowance_define)
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_define_list')
    def __get_allowance_define_list(condition, page=None, count=None):
        res = allowance_manage_func.get_allowance_define_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_define_list_all')
    def __get_allowance_define_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.allowance_definition_all, PermissionName.query)
        res = allowance_manage_func.get_allowance_define_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.del_allowance_define')
    def __del_allowance_define(ids):
        __p = security_service.judge_permission_other(
            FunctionName.allowance_definition_all, PermissionName.delete)
        res = allowance_manage_func.del_allowance_define(ids)
        return res

    @jsonrpc.method('IAllowanceService.subsidy_approval')
    def __del_allowance_define(subsidy_data):
        __p = security_service.judge_permission_other(
            FunctionName.service_worker_allowance_apply_all, PermissionName.edit)
        res = allowance_manage_func.subsidy_approval(subsidy_data)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '养老补贴受理', '养老补贴审批')
        return res

    @jsonrpc.method('IAllowanceService.add_allowance_project')
    def __add_allowance_project(allowance):
        res = allowance_manage_func.add_allowance_project(allowance)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '养老补贴受理', '添加补贴项目')
        return res

    @jsonrpc.method('IAllowanceService.get_allowance_project_list_all')
    def __get_allowance_project_list(allowance, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.allowance_project_list_all, PermissionName.query)
        res = allowance_manage_func.get_allowance_project_list(
            allowance, page=None, count=None)
        return res

    @jsonrpc.method('IAllowanceService.del_allowance_project')
    def __get_allowance_project_list(ids):
        res = allowance_manage_func.del_allowance_project(ids)
        return res

    @jsonrpc.method('IAllowanceService.get_reviewed_idea')
    def __get_allowance_project_list(condition, page=None, count=None):
        res = allowance_manage_func.get_reviewed_idea(condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.user_identification')
    def __get_allowance_project_list(condition):
        res = allowance_manage_func.user_identification(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴认证')
        return res

    @jsonrpc.method('IAllowanceService.apply_old_age_allowance')
    def __apply_old_age_allowance(condition):
        res = allowance_manage_func.apply_old_age_allowance(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴申请')
        return res

    @jsonrpc.method('IAllowanceService.manual_authentication')
    def __manual_authentication(condition):
        res = allowance_manage_func.manual_authentication(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴申请')
        return res

    @jsonrpc.method('IAllowanceService.check_elder_exists')
    def __check_elder_exists(condition):
        res = allowance_manage_func.check_elder_exists(condition)
        return res

    @jsonrpc.method('IAllowanceService.apply_disabled_person_allowance')
    def __apply_disabled_person_allowance(condition):
        res = allowance_manage_func.apply_disabled_person_allowance(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '残疾人认证', '残疾人认证申请')
        return res

    @jsonrpc.method('IAllowanceService.get_old_age_allowance_status')
    def __get_old_age_allowance_status(condition):
        res = allowance_manage_func.get_old_age_allowance_status(condition)
        return res

    @jsonrpc.method('IAllowanceService.audit_old_allowance')
    def __audit_old_allowance(condition):
        __p = security_service.judge_permission_other(
            FunctionName.old_age_allowance_audit, PermissionName.edit)
        res = allowance_manage_func.audit_old_allowance(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴审核')
        return res

    @jsonrpc.method('IAllowanceService.get_audit_old_allowance_list')
    def __get_audit_old_allowance_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.old_age_allowance_audit_look, PermissionName.query)
        res = allowance_manage_func.get_audit_old_allowance_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_audit_old_allowance_no_check_list')
    def __get_audit_old_allowance_no_check_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.old_age_allowance_audit_look, PermissionName.query)
        res = allowance_manage_func.get_audit_old_allowance_no_check_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_audit_disabled_person_allowance_list')
    def __get_audit_disabled_person_allowance_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.old_age_allowance_audit_look, PermissionName.query)
        res = allowance_manage_func.get_audit_disabled_person_allowance_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_old_age_list')
    def __get_old_age_list(condition, page=None, count=None):
        res = allowance_manage_func.get_old_age_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.get_disabled_person_list')
    def __get_disabled_person_list(condition, page=None, count=None):
        res = allowance_manage_func.get_disabled_person_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.upload_disabled_person_list')
    def __upload_disabled_person_list(data):
        security_service.judge_permission_other(
            FunctionName.old_age_list, PermissionName.add)
        res = allowance_manage_func.upload_disabled_person_list(data)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '残疾人认证', '残疾人导入')
        return res

    @jsonrpc.method('IAllowanceService.upload_old_age_list')
    def __upload_old_age_list(data):
        security_service.judge_permission_other(
            FunctionName.old_age_list, PermissionName.add)
        res = allowance_manage_func.upload_old_age_list(data)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴导入')
        return res

    @jsonrpc.method('IAllowanceService.get_disabled_person_statistics')
    def __get_disabled_person_statistics(condition):
        res = allowance_manage_func.get_disabled_person_statistics(condition)
        return res
    #  大白
    @jsonrpc.method('IAllowanceService.get_certToken')
    def __get_certToken(condition):
        res = allowance_manage_func.get_certToken(condition)
        return res

    @jsonrpc.method('IAllowanceService.get_rz_result')
    def __get_rz_result(condition):
        res = allowance_manage_func.get_rz_result(condition)
        return res

    @jsonrpc.method('IAllowanceService.apply_old_allowance')
    def __apply_old_allowance(condition):
        res = allowance_manage_func.apply_old_allowance(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴申请')
        return res

    @jsonrpc.method('IAllowanceService.get_uncertified_person_list')
    def __get_uncertified_person_list(condition, page=None, count=None):
        res = allowance_manage_func.get_uncertified_person_list(
            condition, page, count)
        return res

    @jsonrpc.method('IAllowanceService.update_disabled_info')
    def __update_disabled_info(data):
        security_service.judge_permission_other(
            FunctionName.old_age_list, PermissionName.add)
        res = allowance_manage_func.update_disabled_info(data)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '残疾人认证', '残疾人信息修改')
        return res

    @jsonrpc.method('IAllowanceService.shengchengtaizhang')
    def __shengchengtaizhang(data):
        security_service.judge_permission_other(
            FunctionName.old_age_list, PermissionName.add)
        res = allowance_manage_func.shengchengtaizhang(data)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '一键生成台账')
        return res

    @jsonrpc.method('IAllowanceService.get_old_age_statistics')
    def __get_old_age_statistics(condition):
        res = allowance_manage_func.get_old_age_statistics(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴统计报表')
        return res

    @jsonrpc.method('IAllowanceService.del_old_age_allowance')
    def __del_old_age_allowance(condition):
        res = allowance_manage_func.del_old_age_allowance(condition)
        allowance_manage_func.bill_manage_server.insert_logs(
            '民政', '高龄津贴', '高龄津贴认证删除')
        return res

    @jsonrpc.method('IAllowanceService.update_statistics')
    def __set_statistics():
        res = allowance_manage_func.update_statistics()
        return res
