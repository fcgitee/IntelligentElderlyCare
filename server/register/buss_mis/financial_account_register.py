'''
版权：Copyright (c) 2019 China

创建日期：Thursday August 15th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Thursday, 15th August 2019 4:00:33 pm
修改者: ymq(ymq) - <<email>>

说明
 1、财务账务管理接口
'''
from server.service.buss_mis.financial_account import FinancialAccountService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.constant import PayStatue, PayType


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    financial_account_service = FinancialAccountService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IFiancialAccountService.get_unpay_account_list_look')
    def __get_unpay_account_list(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_charge_look, PermissionName.query)
        res = financial_account_service.get_unpay_account_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IFiancialAccountService.get_unpay_account_list_manage')
    def __get_unpay_account_list(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_charge_manage_look, PermissionName.query)
        res = financial_account_service.get_unpay_account_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IFiancialAccountService.get_need_pay_account_list_look')
    def __get_need_pay_account_list(condition):
        '''收费员退款 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_refund_look, PermissionName.query)
        res = financial_account_service.get_need_pay_account_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IFiancialAccountService.get_need_pay_account_list_manage')
    def __get_need_pay_account_list(condition):
        '''收费员退款管理'''
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_refund_manage_look, PermissionName.query)
        res = financial_account_service.get_need_pay_account_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IFiancialAccountService.add_bank_charge_data')
    def __add_bank_charge_data(paying_data_list):
        ''' 收费员收款'''
        res = financial_account_service.add_bank_charge_data(paying_data_list)
        return res

    @jsonrpc.method('IFiancialAccountService.get_charge_record_list_look')
    def __get_charge_record_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.select_charges_look, PermissionName.query)
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_charge_record_list_all')
    def __get_charge_record_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.select_charges_all, PermissionName.query)
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_bank_unreconciliation_list_look_export')
    def __get_bank_unreconciliation_list_look_export(condition=None, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.select_charges_all, PermissionName.query)
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_bank_unreconciliation_list_manage')
    def __get_bank_unreconciliation_list_manage(condition=None, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.select_charges_all, PermissionName.query)
        condition['pay_type'] = PayType.bank
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_bank_reconciliation_list_look')
    def __get_bank_reconciliation_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.bank_deduction_record_look, PermissionName.query)
        res = financial_account_service.get_bank_reconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_unbank_reconciliation_list_manage')
    def __get_bank_unreconciliation_list_manage(condition=None, page=None, count=None):
        '''非银行划扣账单'''
        permission_data = security_service.judge_permission_query(
            FunctionName.unband_deduction_manage, PermissionName.query)
        condition['pay_type'] = PayType.other
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_unbank_reconciliation_list_Look')
    def __get_bank_unreconciliation_list_manage(condition=None, page=None, count=None):
        '''非银行划扣账单'''
        permission_data = security_service.judge_permission_query(
            FunctionName.unband_deduction_Look, PermissionName.query)
        condition['pay_type'] = PayType.other
        res = financial_account_service.get_bank_unreconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.update_unbank_reconciliation')
    def __update_unbank_reconciliation(condition):
        '''非银行划扣账单编辑'''
        security_service.judge_permission_other(
            FunctionName.unband_deduction_manage, PermissionName.edit)
        res = financial_account_service.update_unbank_reconciliation(condition)
        financial_account_service.bill_manage_server.insert_logs(
            '机构养老', '财务系统', '编辑财务系统')
        return res

    @jsonrpc.method('IFiancialAccountService.get_bank_reconciliation_list_all')
    def __get_bank_reconciliation_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.bank_deduction_record_all, PermissionName.query)
        res = financial_account_service.get_bank_reconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.get_bank_reconciliation_list_look')
    def __get_bank_reconciliation_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.bank_deduction_record_look, PermissionName.query)
        res = financial_account_service.get_bank_reconciliation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialAccountService.exprot_excel_manage')
    def __exprot_excel_manage(financial_id=None, page=None, size=None):
        permission_data = security_service.judge_permission_other(
            FunctionName.print_or_derive_bank_deduction_manage, PermissionName.export)
        res = financial_account_service.exprot_excel(financial_id, None, None)
        financial_account_service.bill_manage_server.insert_logs(
            '机构养老', '财务系统', '导出财务系统')
        return res

    @jsonrpc.method('IFiancialAccountService.complete_pay_account')
    def __complete_pay_account(datas):
        ''' 收费员收款'''
        res = financial_account_service.complete_pay_account(datas)
        return res

    @jsonrpc.method('IFiancialAccountService.upload_excel_manage')
    def __upload_excel_manage(file_data):
        security_service.judge_permission_other(
            FunctionName.print_or_derive_bank_deduction_manage, PermissionName.export)
        res = financial_account_service.upload_excel_manage(file_data)
        financial_account_service.bill_manage_server.insert_logs(
            '机构养老', '财务系统', '导入财务系统')
        return res

    @jsonrpc.method('IFiancialAccountService.upload_excel_band_record')
    def __upload_excel_band_record(file_data):
        security_service.judge_permission_other(
            FunctionName.print_or_derive_bank_deduction_manage, PermissionName.export)
        res = financial_account_service.upload_excel_band_record(file_data)
        financial_account_service.bill_manage_server.insert_logs(
            '机构养老', '财务系统', '导入辑财务系统')
        return res
