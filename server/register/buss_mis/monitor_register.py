from ...service.buss_mis.monitor import MonitorService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
# -*- coding: utf-8 -*-
'''
视频监控相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    monitor_func = MonitorService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMonitorService.get_monitor_list_all')
    def __get_monitor(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.monitor_manage_all, PermissionName.query)
        res = monitor_func.get_monitor_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IMonitorService.get_monitor_list_look')
    def __get_monitor(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.monitor_manage_look, PermissionName.query)
        res = monitor_func.get_monitor_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IMonitorService.update_monitor')
    def __update_monitor(monitor):
        if 'id' in list(monitor.keys()):
            security_service.judge_permission_other(
                FunctionName.monitor_manage_all, PermissionName.edit)
        else:
            security_service.judge_permission_other(
                FunctionName.monitor_manage_all, PermissionName.add)
        res = monitor_func.update_monitor(monitor)
        monitor_func.bill_manage_service.insert_logs(
            '机构养老', '监控管理', '新建/编辑监控管理')
        return res

    @jsonrpc.method('IMonitorService.delete_monitor')
    def __delete_monitor(ids):
        security_service.judge_permission_other(
            FunctionName.monitor_manage_all, PermissionName.delete)
        res = monitor_func.delete_monitor(ids)
        monitor_func.bill_manage_service.insert_logs(
            '机构养老', '监控管理', '删除监控管理')
        return res
