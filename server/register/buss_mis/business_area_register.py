from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.business_area import BusinessAreaService
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-02-19 16:16:08
@LastEditors: Please set LastEditors
'''

# -*- coding: utf-8 -*-
'''
业务区域管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    business_area_func = BusinessAreaService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IBusinessAreaService.get_business_area_list_all')
    def __get_business_area_list(condition, page=None, count=None):
        '''业务区域管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.business_area_manage_all, PermissionName.query)
        res = business_area_func.get_business_area_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.get_business_area_list_look')
    def __get_business_area_list(condition, page=None, count=None):
        '''查看 业务区域 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.business_area_manage_look, PermissionName.query)
        res = business_area_func.get_business_area_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.add_business_area')
    def __add_business_area(businessArea):
        if 'id' in list(businessArea.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.business_area_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.business_area_manage_all, PermissionName.add)
        res = business_area_func.add_business_area(businessArea)
        return res

    @jsonrpc.method('IBusinessAreaService.del_business_area')
    def __del_business_area(ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.business_area_manage_all, PermissionName.delete)
        res = business_area_func.del_business_area(ids)
        return res

    @jsonrpc.method('IBusinessAreaService.update_business_area_dispose')
    def __update_business_area_dispose(businessAreaDispose):
        permission_data = security_service.judge_permission_other(
            FunctionName.business_area_manage_all, PermissionName.edit)
        res = business_area_func.update_business_area_dispose(
            businessAreaDispose)
        return res

    @jsonrpc.method('IBusinessAreaService.copy_business_area_dispose')
    def __copy_business_area_dispose(business_area_id):
        permission_data = security_service.judge_permission_other(
            FunctionName.business_area_manage_all, PermissionName.edit)
        res = business_area_func.copy_business_area_dispose(
            business_area_id)
        return res

    @jsonrpc.method('IBusinessAreaService.get_business_area_item')
    def __get_business_area_item(condition, page=None, count=None):
        res = business_area_func.get_business_area_item(
            condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.get_admin_division_list_all')
    def __get_admin_division_list_all(condition, page=None, count=None):
        '''行政区划管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.administrative_division_manage_all, PermissionName.query)
        res = business_area_func.get_admin_division_list(permission_data,
                                                         condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.get_admin_division_list_look')
    def __get_admin_division_list_look(condition, page=None, count=None):
        ''' 查询 行政区划'''
        res = business_area_func.get_admin_division_list(
            condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.get_admin_division_list')
    def __get_admin_division_list(condition, page=None, count=None):
        ''' 查询 行政区划'''
        permission_data = security_service.org_current()
        res = business_area_func.get_admin_division_tree_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IBusinessAreaService.get_admin_division_list_no_login')
    def __get_admin_division_list_no_login(condition, page=None, count=None):
        ''' 查询 行政区划'''
        ''' 获取顶级行政区划id '''
        admin_id = security_service.get_top_admin_id()
        res = business_area_func.get_admin_division_tree_list([],
                                                              condition, page, count, admin_id)
        return res

    @jsonrpc.method('IBusinessAreaService.update_admin_division')
    def __update_admin_division(adminDivision):
        if 'id' in list(adminDivision.keys()):
            security_service.judge_permission_other(
                FunctionName.administrative_division_manage_all, PermissionName.edit)
        else:
            security_service.judge_permission_other(
                FunctionName.administrative_division_manage_all, PermissionName.add)
        res = business_area_func.update_admin_division(adminDivision)
        business_area_func.bill_manage_server.insert_logs(
            '平台运营方', '行政区划维护', '新建/编辑行政区划维护')
        return res

    @jsonrpc.method('IBusinessAreaService.del_admin_division')
    def __del_admin_division(ids):
        security_service.judge_permission_other(
            FunctionName.administrative_division_manage_all, PermissionName.delete)
        res = business_area_func.del_admin_division(ids)
        business_area_func.bill_manage_server.insert_logs(
            '平台运营方', '行政区划维护', '删除行政区划维护')
        return res

    @jsonrpc.method('IBusinessAreaService.get_all_admin_division_list')
    def __get_all_admin_division_list(condition, page=None, count=None):
        ''' 查询 行政区划'''
        res = business_area_func.get_all_admin_division_list(
            condition, page, count)
        return res
