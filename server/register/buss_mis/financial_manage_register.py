'''
版权：Copyright (c) 2019 China

创建日期：Thursday July 25th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Thursday, 25th July 2019 8:59:28 am
修改者: ymq(ymq) - <<email>>

说明
 1、财务管理服务接口
'''
from server.service.buss_mis.financial_manage import FinancialService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    financial_service = FinancialService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IFiancialService.input_user_name_list')
    def __input_user_name_list(condition):
        res = financial_service.input_user_name_list(condition)
        return res

    @jsonrpc.method('IFiancialService.input_account_book_list')
    def __input_account_book_list(condition):
        res = financial_service.input_account_book_list(condition)
        return res

    @jsonrpc.method('IFiancialService.input_account_list')
    def __input_account_list(condition):
        res = financial_service.input_account_list(condition)
        return res

    @jsonrpc.method('IFiancialService.input_subject_list')
    def __input_subject_list(condition):
        res = financial_service.input_subject_list(condition)
        return res

    @jsonrpc.method('IFiancialService.update_account_book_data')
    def __update_account_book_data(data):
        if 'id' in list(data.keys()):
            '''账簿管理 新增/修改'''
            permission_data = security_service.judge_permission_other(
                FunctionName.account_book_manage, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.account_book_manage, PermissionName.add)
        res = financial_service.update_account_book_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_account_book_list_manage')
    def __get_account_book_list(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.account_book_manage, PermissionName.query)
        res = financial_service.get_account_book_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.get_account_book_list_all')
    def __get_account_book_list(condition, page, count):
        '''全部账簿管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.account_book_manage_all, PermissionName.query)
        res = financial_service.get_account_book_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.update_account_book_data_all')
    def __update_account_book_data(data):
        if 'id' in list(data.keys()):
            '''全部账簿管理 新增/修改'''
            permission_data = security_service.judge_permission_other(
                FunctionName.account_book_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.account_book_manage_all, PermissionName.add)
        res = financial_service.update_account_book_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_account_list_manage')
    def __get_account_list(condition, page, count):
        '''账户管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.account_manage, PermissionName.query)
        res = financial_service.get_account_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.update_account_data')
    def __update_account_data(data):
        '''账户管理 新增/修改'''
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.account_manage, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.account_manage, PermissionName.add)
        res = financial_service.update_account_data(data)
        return res

    @jsonrpc.method('IFiancialService.update_account_data_list')
    def __update_account_data_list(data):
        '''账户列表修改代付顺序'''
        res = financial_service.update_account_data_list(data)
        return res

    @jsonrpc.method('IFiancialService.get_account_list_all')
    def __get_account_list(condition, page, count):
        '''全部账户管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.account_manage_all, PermissionName.query)
        res = financial_service.get_account_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.get_user_account_sequence_all')
    def __get_user_account_sequence_all(condition, page=None, count=None):
        '''当前用户下的账户顺序 查询'''
        res = financial_service.get_user_account_sequence_all(
            condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.update_account_data')
    def __update_account_data(data):
        if 'id' in list(data.keys()):
            '''全部账户管理 新增/修改'''
            permission_data = security_service.judge_permission_other(
                FunctionName.account_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.account_manage_all, PermissionName.add)
        res = financial_service.update_account_data(data)
        return res

    @jsonrpc.method('IFiancialService.save_account_payment_data')
    def __save_account_payment_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_all, PermissionName.add)
        res = financial_service.save_account_payment_data(data)
        return res

    @jsonrpc.method('IFiancialService.save_account_receive_data')
    def __save_account_receive_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_all, PermissionName.add)
        res = financial_service.save_account_receive_data(data)
        return res

    @jsonrpc.method('IFiancialService.save_account_transfer_data')
    def __save_account_transfer_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_all, PermissionName.add)
        res = financial_service.save_account_transfer_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_account_flow_list_all')
    def __get_account_flow_list(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.account_flow_all, PermissionName.query)
        res = financial_service.get_account_flow_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.get_account_flow_list_manage')
    def __get_account_flow_list(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.account_flow_manage_all, PermissionName.query)
        res = financial_service.get_account_flow_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.save_account_payment_data_manage')
    def __save_account_payment_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_manage_all, PermissionName.add)
        res = financial_service.save_account_payment_data(data)
        return res

    @jsonrpc.method('IFiancialService.save_account_receive_data_manage')
    def __save_account_receive_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_manage_all, PermissionName.add)
        res = financial_service.save_account_receive_data(data)
        return res

    @jsonrpc.method('IFiancialService.save_account_transfer_data_manage')
    def __save_account_transfer_data(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.account_flow_manage_all, PermissionName.add)
        res = financial_service.save_account_transfer_data(data)
        return res

    @jsonrpc.method('IFiancialService.add_account_voucher')
    def __add_account_voucher(data):
        res = financial_service.add_account_voucher(data)
        return res

    @jsonrpc.method('IFiancialService.update_subject_data')
    def __update_subject_data(data):
        res = financial_service.update_subject_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_subject_list')
    def __get_subject_list(condition, page, count):
        res = financial_service.get_subject_list(condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.del_subject_data')
    def __del_subject_data(ids):
        res = financial_service.del_subject_data(ids)
        return res

    @jsonrpc.method('IFiancialService.get_voucher_list')
    def __get_voucher_list(condition, page, count):
        res = financial_service.get_voucher_list(condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.add_account_voucher')
    def __add_account_voucher(data):
        res = financial_service.add_account_voucher(data)
        return res

    @jsonrpc.method('IFiancialService.query_account_voucher_list')
    def __query_account_voucher_list(condition, page=None, count=None):
        res = financial_service.query_account_voucher_list(
            condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.return_account_data')
    def __return_account_data(data):
        res = financial_service.return_account_data(data)
        return res

    @jsonrpc.method('IFiancialService.return_account_voucher')
    def __return_account_voucher(data):
        res = financial_service.return_account_voucher(data)
        return res
    ############################################银行对账接口##################################################

    @jsonrpc.method('IFiancialService.add_charge_back_detil_data')
    def __add_charge_back_detil_data(data):
        res = financial_service.add_charge_back_detil_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_not_set_item')
    def __get_not_set_item():
        res = financial_service.get_not_set_item()
        return res

    @jsonrpc.method('IFiancialService.set_subject_item')
    def __set_subject_item(data):
        res = financial_service.set_subject_item(data)
        return res

    @jsonrpc.method('IFiancialService.billing_record')
    def __export_receipt_item(condition, page=None, count=None):
        '''查询收付款记录'''
        res = financial_service.export_receipt_item(condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.billing_details_record')
    def __export_receipt_details_data(data):
        '''查询收付款详情订单记录'''
        res = financial_service.export_receipt_details_data(data)
        return res

    @jsonrpc.method('IFiancialService.get_divide_record_list')
    def __get_divide_record_list(condition, page, count):
        '''查询收入分成记录列表'''
        res = financial_service.get_divide_record_list(condition, page, count)
        return res

    @jsonrpc.method('IFiancialService.get_subsidy_service_provider_list')
    def __get_subsidy_service_provider_list(condition, page, count):
        '''获取服务商补贴账户账单'''
        res = financial_service.get_subsidy_service_provider_list(
            condition, page, count)
        return res

    @jsonrpc.method('ReportManage.get_service_provider_settlement')
    def __get_service_provider_settlement(condition, page=None, count=None):
        '''获取服务商结算'''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_provider_settlement, PermissionName.query)
        res = financial_service.get_service_provider_settlement(permission_data,
                                                                condition, page, count)
        return res

    @jsonrpc.method('ReportManage.get_service_provider_settlement_total')
    def __get_service_provider_settlement_total(condition):
        '''获取服务商结算汇总'''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_provider_settlement, PermissionName.query)
        res = financial_service.get_service_provider_settlement_total(permission_data,
                                                                      condition)
        return res

    @jsonrpc.method('ReportManage.get_service_provider_record_settlement')
    def __get_service_provider_record_settlement(condition, page=None, count=None):
        '''获取服务商服务记录明细统计'''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_provider_settlement, PermissionName.query)
        res = financial_service.get_service_provider_record_settlement_yh(permission_data,
                                                                          condition, page, count)
        return res

    @jsonrpc.method('ReportManage.confirm_send')
    def __confirm_send(service_provider_id, year_month, buy_type, service_product_id, social_worker_id):
        '''用于发送记录给服务商确认是否结算'''
        res = financial_service.confirm_send(
            service_provider_id, year_month, buy_type, service_product_id, social_worker_id)
        return res

    @jsonrpc.method('ReportManage.accounting_confirm')
    def __accounting_confirm(service_provider_id, year_month, buy_type, service_product_id, social_worker_id):
        '''服务商结算确认'''
        res = financial_service.accounting_confirm(
            service_provider_id, year_month, buy_type, service_product_id, social_worker_id)
        return res
