from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.service_order_task import ServiceOrderTaskService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:08:35
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\service_order_task_register.py
'''
# -*- coding: utf-8 -*-
'''
服务订单分派任务注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_order_task_func = ServiceOrderTaskService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceOrderTaskService.get_service_order_task_list')
    def __get_service_order_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_task, PermissionName.query)
        res = service_order_task_func.get_service_order_task_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IServiceOrderTaskService.get_service_order_task_list_all')
    def __get_service_order_task_list_manage(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_task_manage, PermissionName.query)
        res = service_order_task_func.get_service_order_task_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IServiceOrderTaskService.update_service_order_task_manage')
    def __update_service_order_task_manage(service_order_task):
        security_service.judge_permission_other(
            FunctionName.service_task_manage, PermissionName.edit)
        res = service_order_task_func.update_service_order_task(
            service_order_task)
        service_order_task_func.bill_manage_server.insert_logs(
            '服务商', '服务派工', '新建/编辑服务派工')
        return res

    @jsonrpc.method('IServiceOrderTaskService.get_services_item_category_list')
    def __get_services_item_category_list():
        res = service_order_task_func.get_services_item_category_list()
        return res

    @jsonrpc.method('IServiceOrderTaskService.cancel_assign_service_order_task_manage')
    def __cancel_assign_service_order_task_manage(order_id):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_task_manage, PermissionName.edit)
        res = service_order_task_func.cancel_assign_service_order_task_manage(
            order_id)
        return res
