# -*- coding: utf-8 -*-
'''
服务项目类别注册
'''
from ...service.buss_mis.services_item_category import ServicesItemCategoryService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    services_item_category_func = ServicesItemCategoryService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServicesItemCategoryService.get_services_item_category_list')
    def __get_services_item_category_list(condition, page=None, count=None):
        res = services_item_category_func.get_services_item_category_list(N(),
                                                                          condition, page, count)
        return res

    @jsonrpc.method('IServicesItemCategoryService.get_services_item_category_list_all')
    def __get_services_item_category_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item_type, PermissionName.query)
        res = services_item_category_func.get_services_item_category_list(permission_data,
                                                                          condition, page, count)
        return res

    @jsonrpc.method('IServicesItemCategoryService.get_services_item_category_list_look')
    def __get_services_item_category_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item_type_look, PermissionName.query)
        res = services_item_category_func.get_services_item_category_list(permission_data,
                                                                          condition, page, count)
        return res

    @jsonrpc.method('IServicesItemCategoryService.update_services_item_category')
    def __update_services_item_category(services_item_category):
        Permission_name = PermissionName.add if services_item_category.get(
            'id') else PermissionName.edit
        permission_data = security_service.judge_permission_other(
            FunctionName.service_item_type, Permission_name)
        res = services_item_category_func.update_services_item_category(
            services_item_category)
        return res

    @jsonrpc.method('IServicesItemCategoryService.delete_services_item_category')
    def __delete_services_item_category(services_item_category_ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_item_type, PermissionName.delete)
        res = services_item_category_func.delete_services_item_category(
            services_item_category_ids)
        return res
