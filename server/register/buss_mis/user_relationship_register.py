'''
@Author: your name
@Date: 2020-01-09 15:33:21
@LastEditTime : 2020-01-09 17:56:28
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\operation_record_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.user_relation_ship import UserRelationShipService
# -*- coding: utf-8 -*-
'''
人员关系管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    user_relation_ship_func = UserRelationShipService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IUserRelationShipService.get_user_relation_ship_list')
    def __get_user_relation_ship_list(condition, page=None, count=None):
        res = user_relation_ship_func.get_user_relation_ship_list(
            condition, page, count)
        return res

    @jsonrpc.method('IUserRelationShipService.update_user_relation_ship')
    def __update_user_relation_ship(data):
        res = user_relation_ship_func.update_user_relation_ship(data)
        return res

    @jsonrpc.method('IUserRelationShipService.delete_user_relation_ship')
    def __delete_user_relation_ship(ids):
        res = user_relation_ship_func.delete_user_relation_ship(ids)
        return res

    @jsonrpc.method('IUserRelationShipService.get_relationShip_type')
    def __get_relationShip_type(condition, page=None, count=None):
        res = user_relation_ship_func.get_relationShip_type(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IUserRelationShipService.update_care_record')
    def __update_care_record(data):
        res = user_relation_ship_func.update_care_record(data)
        user_relation_ship_func.bill_manage_service.insert_logs(
            '平台运营方', '长者关怀记录', '新建/编辑长者关怀记录')
        return res

    @jsonrpc.method('IUserRelationShipService.get_care_record')
    def __get_care_record(condition, page=None, count=None):
        res = user_relation_ship_func.get_care_record(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IUserRelationShipService.get_care_type_list')
    def __get_care_type_list(condition, page=None, count=None):
        res = user_relation_ship_func.get_care_type_list(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IUserRelationShipService.update_care_type')
    def __update_care_type(data):
        res = user_relation_ship_func.update_care_type(data)
        user_relation_ship_func.bill_manage_service.insert_logs(
            '平台运营方', '长者关怀类型设置', '新建/编辑长者关怀类型设置')
        return res

    @jsonrpc.method('IUserRelationShipService.del_care_record')
    def __del_care_record(ids):
        res = user_relation_ship_func.del_care_record(ids)
        user_relation_ship_func.bill_manage_service.insert_logs(
            '平台运营方', '长者关怀记录', '删除长者关怀记录')
        return res

    @jsonrpc.method('IUserRelationShipService.del_care_type')
    def __del_care_type(ids):
        res = user_relation_ship_func.del_care_type(ids)
        user_relation_ship_func.bill_manage_service.insert_logs(
            '平台运营方', '长者关怀类型设置', '删除长者关怀类型设置')
        return res
