'''
@Author: your name
@Date: 2020-01-09 15:33:21
@LastEditTime : 2020-01-09 16:30:25
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\operation_record_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.operation_record import OperationRecordService
# -*- coding: utf-8 -*-
'''
最近记录管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    operation_record_func = OperationRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IOperationRecordService.get_record_list')
    def __get_record_list(condition, page=None, count=None):
        res = operation_record_func.get_record_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IOperationRecordService.insert_record')
    def __insert_record(operation_record):
        res = operation_record_func.insert_record(operation_record)
        return res

    @jsonrpc.method('IOperationRecordService.insert_record_t')
    def __insert_record_t():
        res = operation_record_func.insert_record_t()
        return res
