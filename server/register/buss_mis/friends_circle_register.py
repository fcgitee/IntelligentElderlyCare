'''
@Author: your name
@Date: 2020-01-10 09:35:50
@LastEditTime : 2020-01-13 15:12:28
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\friends_circle_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.friends_circle import FriendsCircleService
# -*- coding: utf-8 -*-
'''
老友圈管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    friends_circle_func = FriendsCircleService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IFriendsCircleService.get_friends_circle_list')
    def __get_friends_circle_list(condition, page=None, count=None):
        '''获取老友圈广场列表'''
        res = friends_circle_func.get_friends_circle_list(
            condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.update_friends_circle')
    def __update_friends_circle(content, picture, location=None):
        """ 发布老友圈 """
        res = friends_circle_func.update_friends_circle(
            content, picture, location)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '老友圈', '编辑老友圈')
        return res

    @jsonrpc.method('IFriendsCircleService.delete_friends_circle')
    def __delete_friends_circle(friends_circle_ids):
        '''删除老友圈接口'''
        res = friends_circle_func.delete_friends_circle(friends_circle_ids)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '老友圈', '删除老友圈')
        return res

    @jsonrpc.method('IFriendsCircleService.update_commont')
    def __update_commont(fcm_id, common):
        ''' 发布评论 '''
        res = friends_circle_func.update_commont(fcm_id, common)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '老友圈', '评论老友圈')
        return res

    @jsonrpc.method('IFriendsCircleService.del_commont')
    def __del_commont(comment_ids):
        ''' 删除评论 '''
        res = friends_circle_func.del_commont(comment_ids)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '老友圈', '删除老友圈评论')
        return res

    @jsonrpc.method('IFriendsCircleService.update_like')
    def __update_like(fcm_id):
        ''' 点赞取消点赞'''
        res = friends_circle_func.update_like(fcm_id)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '老友圈', '老友圈点赞')
        return res

    @jsonrpc.method('IFriendsCircleService.get_commont_list')
    def __get_commont_list(condition, page=None, count=None):
        '''获取老友圈评论列表'''
        res = friends_circle_func.get_commont_list(
            condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.get_like_list')
    def __get_like_list(condition, page=None, count=None):
        '''获取老友圈点赞列表'''
        res = friends_circle_func.get_like_list(
            condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.get_circle_list_all')
    def __get_circle_list_all(condition, page=None, count=None):
        ''' 圈子 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.circle, PermissionName.query)
        res = friends_circle_func.get_circle_list(permission_data,
                                                  condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.get_circle_list')
    def __get_circle_list(condition, page=None, count=None):
        ''' 圈子 '''
        res = friends_circle_func.get_circle_list(N(),
                                                  condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.update_circle')
    def __update_circle(condition):
        '''圈子修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.circle, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.circle, PermissionName.add)
        res = friends_circle_func.update_circle(condition)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '圈子管理', '新增/编辑圈子管理')
        return res

    @jsonrpc.method('IFriendsCircleService.delete_circle')
    def __delete_month_plan(condition):
        '''圈子删除'''
        __P = security_service.judge_permission_other(
            FunctionName.circle, PermissionName.delete)
        res = friends_circle_func.delete_circle(condition)
        friends_circle_func.bill_manage_service.insert_logs(
            '平台', '圈子管理', '圈子管理删除')
        return res

    @jsonrpc.method('IFriendsCircleService.update_epidemic_prevention')
    def __update_epidemic_prevention(content):
        """ 新增工作人员出入档案 """
        permiss_name = PermissionName.edit if content.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.epidemic_prevention_table, permiss_name)
        res = friends_circle_func.update_epidemic_prevention(content)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老', '疫情复工汇总表', 'app防疫上报')
        return res

    @jsonrpc.method('IFriendsCircleService.get_epidemic_prevention')
    def __get_epidemic_prevention():
        """ 查询工作人员出入档案 """
        security_service.judge_permission_query(
            FunctionName.epidemic_prevention_table, PermissionName.query)
        res = friends_circle_func.get_epidemic_prevention()
        return res

    @jsonrpc.method('IFriendsCircleService.get_epidemic_prevention_all')
    def __get_epidemic_prevention_all(condition, page=None, count=None):
        ''' 查询所有工作人员出入档案 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.epidemic_prevention_table, PermissionName.query)
        res = friends_circle_func.get_epidemic_prevention_all(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.decide_epidemic_prevention')
    def __decide_epidemic_prevention(content):
        """ 判定 """
        security_service.judge_permission_other(
            FunctionName.epidemic_prevention_table, PermissionName.edit)
        res = friends_circle_func.decide_epidemic_prevention(content)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老', '疫情复工汇总表', '判定疫情复工汇总表')
        return res

    @jsonrpc.method('IFriendsCircleService.get_day_epidemic_prevention_all')
    def __get_day_epidemic_prevention_all(condition, page=None, count=None):
        ''' 查询某个人每天的出入登记记录 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.epidemic_prevention_table, PermissionName.query)
        res = friends_circle_func.get_day_epidemic_prevention_all(
            condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.update_day_info')
    def __update_day_info(content):
        """ 更新每日登记信息 """
        security_service.judge_permission_other(
            FunctionName.epidemic_prevention_table, PermissionName.add)
        res = friends_circle_func.update_day_info(content)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老', '疫情复工汇总表', '新增/编辑防疫上报')
        return res

    @jsonrpc.method('IFriendsCircleService.update_org_day_info')
    def __update_org_day_info(content):
        """ 新增机构每日登记信息 """
        permiss_name = PermissionName.edit if content.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.focus_org, permiss_name)
        res = friends_circle_func.update_org_day_info(content)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老', '全省民政部门重点防疫场所', '新增/编辑防疫上报（机构）')
        return res

    @jsonrpc.method('IFriendsCircleService.get_org_day_info')
    def __get_org_day_info(content):
        """ 查询机构每日登记信息 """
        security_service.judge_permission_query(
            FunctionName.focus_org, PermissionName.query)
        res = friends_circle_func.get_org_day_info(content)
        return res

    @jsonrpc.method('IFriendsCircleService.get_org_day_info_all')
    def __get_org_day_info_all(condition, page=None, count=None):
        ''' 查询机构所有每日登记信息 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.focus_org, PermissionName.query)
        res = friends_circle_func.get_org_day_info_all(permission_data,
                                                       condition, page, count)
        return res

    @jsonrpc.method('IFriendsCircleService.decide_by_once')
    def __decide_by_once(content, recode=None):
        """ 一键判定 """
        permission_data = security_service.judge_permission_query(
            FunctionName.epidemic_prevention_table, PermissionName.query)
        res = friends_circle_func.decide_by_once(
            permission_data, content, recode)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老', '疫情复工汇总表', '判定疫情复工汇总表')
        return res

    @jsonrpc.method('IFriendsCircleService.upload_day_info_excl')
    def __upload_day_info_excl(content):
        """ 导入每日疫情数据 """
        security_service.judge_permission_other(
            FunctionName.epidemic_prevention_table, PermissionName.add)
        res = friends_circle_func.upload_day_info_excl(content)
        friends_circle_func.bill_manage_service.insert_logs(
            '机构养老/幸福院/服务商', '疫情复工汇总表', '导入每日疫情数据')
        return res
