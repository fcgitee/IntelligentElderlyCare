from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.services_project import ServicesProjectService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:48:48
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\services_project_register.py
'''
# -*- coding: utf-8 -*-
'''
住宿服务项目注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    services_project_func = ServicesProjectService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServicesProjectService.get_services_project_list')
    def __get_services_project_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = services_project_func.get_services_project_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IServicesProjectService.get_services_project_list_all')
    def __get_services_project_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item, PermissionName.query)
        res = services_project_func.get_services_project_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IServicesProjectService.get_services_project_list_look')
    def __get_services_project_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item_look, PermissionName.query)
        res = services_project_func.get_services_project_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IServicesProjectService.get_services_project_list_by_scope')
    def __get_services_project_list_by_scope(condition):
        permission_data = security_service.org_all_subordinate()
        res = services_project_func.get_services_project_list2(
            permission_data, condition, None, None)
        return res['result']

    @jsonrpc.method('IServicesProjectService.get_services_project_list2')
    def __get_services_project_list2(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = services_project_func.get_services_project_list2(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IServicesProjectService.get_all_item_from_product')
    def __get_all_item_from_product(condition, page=None, count=None):
        res = services_project_func.get_all_item_from_product(
            condition)
        return res

    @jsonrpc.method('IServicesProjectService.update_services_project')
    def __update_services_project(services_project):
        Permission_name = PermissionName.edit if services_project.get(
            'id') else PermissionName.add
        permission_data = security_service.judge_permission_other(
            FunctionName.service_item, Permission_name)
        res = services_project_func.update_services_project(
            services_project)
        return res

    @jsonrpc.method('IServicesProjectService.delete_services_project')
    def __delete_services_project(services_project_ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_item, PermissionName.delete)
        res = services_project_func.delete_services_project(
            services_project_ids)
        return res

    @jsonrpc.method('IServicesProjectService.get_services_periodicity_list')
    def __get_services_periodicity_list(condition, page=None, count=None):
        res = services_project_func.get_services_periodicity_list(
            condition, page, count)
        return res
