# -*- coding: utf-8 -*-
'''
单据管理相关函数注册
'''
from ...service.buss_mis.service_operation import ServiceOperationService
from ...service.buss_mis.service_woker import ServiceWoker
from ...service.buss_pub.bill_manage import OperationType, TypeId
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_operation_func = ServiceOperationService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    service_woker = ServiceWoker(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceOperationService.get_service_provider_list_all')
    def __get_service_provider_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_provider, PermissionName.query)
        res = service_operation_func.get_service_provider_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_provider_byId')
    def __get_service_provider_byId(condition, page=None, count=None):
        res = service_operation_func.get_service_provider_byId(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_current_service_provider')
    def __delete_service_personal(condition, page=None, count=None):
        res = service_operation_func.get_current_service_provider(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_provider')
    def __update_service_provider(serviceProvider):
        Permission_name = PermissionName.add if serviceProvider.get(
            'id') else PermissionName.edit
        permission_data = security_service.judge_permission_other(
            FunctionName.service_provider, Permission_name)
        res = service_operation_func.update_service_provider(serviceProvider)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_provider_add')
    def __update_service_provider(serviceProvider):
        security_service.judge_permission_other(
            FunctionName.service_provider, PermissionName.add)
        res = service_operation_func.update_service_provider(serviceProvider)
        return res

    @jsonrpc.method('IServiceOperationService.del_service_provider')
    def __del_service_provider(ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_provider, PermissionName.delete)
        res = service_operation_func.del_service_provider(ids)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_type_list')
    def __get_service_type_list(condition, page=None, count=None):
        res = service_operation_func.get_service_type_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_type_list_tree')
    def __get_service_type_list_tree(condition, page=None, count=None):
        res = service_operation_func.get_service_type_list_tree(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_type')
    def __update_service_type(serviceType):
        res = service_operation_func.update_service_type(serviceType)
        service_operation_func.bill_manage_server.insert_logs(
            '平台运营方', '服务类型设置', '新建/编辑服务类型设置')
        return res

    @jsonrpc.method('IServiceOperationService.del_service_type')
    def __del_service_type(ids):
        res = service_operation_func.del_service_type(ids)
        service_operation_func.bill_manage_server.insert_logs(
            '平台运营方', '服务类型设置', '删除服务类型设置')
        return res

    @jsonrpc.method('IServiceOperationService.recovery_service_type')
    def __del_service_type(data_id):
        res = service_operation_func.recovery_service_type(data_id)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_item_list_all')
    def __get_service_item_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item, PermissionName.query)
        res = service_operation_func.get_service_item_list(permission_data,
                                                           condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_item_list_look')
    def __get_service_item_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_item_look, PermissionName.query)
        res = service_operation_func.get_service_item_list(permission_data,
                                                           condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_item')
    def __update_service_item(serviceItem):
        permission_name = PermissionName.add if serviceItem.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.service_item, permission_name)
        res = service_operation_func.update_service_item(serviceItem)
        service_operation_func.bill_manage_server.insert_logs(
            '平台运营方', '服务细项设置', '新建/编辑服务细项设置')
        return res

    @jsonrpc.method('IServiceOperationService.del_service_item')
    def __del_service_item(ids):
        security_service.judge_permission_other(
            FunctionName.service_item, PermissionName.delete)
        res = service_operation_func.del_service_item(ids)
        service_operation_func.bill_manage_server.insert_logs(
            '平台运营方', '服务细项设置', '删除服务细项设置')
        return res

    @jsonrpc.method('IServiceOperationService.recovery_service_item')
    def __del_service_type(data_id):
        res = service_operation_func.recovery_service_item(data_id)
        return res

    @jsonrpc.method('IServiceOperationService.service_evaluate')
    def __service_evaluate(serviceEvaluate):
        res = service_operation_func.service_evaluate(serviceEvaluate)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_order_list')
    def __get_service_order_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_order, PermissionName.query)
        res = service_operation_func.get_service_order_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_order_fwyy_fwdd_list')
    def __get_service_order_fwyy_fwdd_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_order, PermissionName.query)
        res = service_operation_func.get_service_order_fwyy_fwdd_list(permission_data,
                                                                      condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_order_fwddfp_list')
    def __get_service_order_fwddfp_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_order, PermissionName.query)
        res = service_operation_func.get_service_order_fwddfp_list(permission_data,
                                                                   condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_order_list_al')
    def __get_service_order_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = service_operation_func.get_service_order_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.service_book')
    def __service_evaluate(serviceItems):
        ''' 服务预定 '''
        res = service_operation_func.service_book(serviceItems)
        return res

    @jsonrpc.method('IServiceOperationService.service_consult')
    def __service_evaluate(order_id):
        ''' 服务协商 '''
        res = service_operation_func.service_consult(order_id)
        return res

    @jsonrpc.method('IServiceOperationService.change_service_order_status')
    def __service_evaluate(order_id, new_status):
        ''' 修改服务订单状态 '''
        res = service_operation_func.change_service_order_status(
            order_id, new_status)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_order')
    def __update_service_order(serviceOrder):
        ''' 修改服务订单状态 '''
        res = service_operation_func.update_service_order(serviceOrder)
        return res

    @jsonrpc.method('IServiceOperationService.service_pay')
    def __service_evaluate(order_id):
        ''' 服务支付 '''
        res = service_operation_func.service_pay(order_id)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_evaluate_list')
    def __get_service_evaluate_list(condition, page=None, count=None):
        ''' 服务评价列表查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_evaluate, PermissionName.query)
        res = service_operation_func.get_service_evaluate_list(permission_data,
                                                               condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_return_visit_list')
    def __get_service_return_visit_list(condition, page=None, count=None):
        ''' 服务回访列表查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_return_visit, PermissionName.query)
        res = service_operation_func.get_service_return_visit_list(permission_data,
                                                                   condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_return_visit')
    def __update_service_return_visit(condition):
        ''' 修改工单服务回访状态 '''
        security_service.judge_permission_query(
            FunctionName.service_return_visit, PermissionName.query)
        res = service_operation_func.update_service_return_visit(condition)
        service_operation_func.bill_manage_server.insert_logs(
            '', '服务回访', '编辑服务回访')
        return res

    @jsonrpc.method('IServiceOperationService.update_service_return_visit_record')
    def __update_service_return_visit_record(condition):
        ''' 修改工单服务回访状态 '''
        res = service_operation_func.update_service_return_visit(condition)
        service_operation_func.bill_manage_server.insert_logs(
            '', '服务回访', '编辑服务回访')
        return res

    # @jsonrpc.method('IServiceOperationService.get_service_item_package')
    # def __service_evaluate(condition, page=None, count=None):
    #     ''' 服务评价列表查询 '''
    #     res = service_operation_func.get_service_item_package(
    #         condition, page=None, count=None)
    #     return res

    @jsonrpc.method('IServiceOperationService.get_service_woker_list')
    def __get_service_woker_list(condition, page=None, count=None):
        ''' 服务人员登记信息列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_woker, PermissionName.query)
        res = service_woker.get_service_woker_list(permission_data,
                                                   condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceOperationService.service_woker_update')
    def __add_update_and_bill(condition):
        ''' 新增/编辑服务人员登记信息 '''
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        permission_data = security_service.judge_permission_other(
            FunctionName.service_woker, Permission_name)
        res = service_woker.add_update_woker(condition)
        return res

    @jsonrpc.method('IServiceOperationService.service_woker_delete')
    def ___del_data(ids):
        ''' 删除服务人员登记信息 '''
        permission_data = security_service.judge_permission_other(
            FunctionName.service_woker, PermissionName.delete)
        res = service_woker.delete_woker(ids)
        return res

    @jsonrpc.method('IServiceOperationService.add_service_record_option')
    def ___add_service_record_option(record_id, options):
        ''' 记录服务记录项目值 '''
        res = service_operation_func.add_service_record_option(
            record_id, options)
        return res

    @jsonrpc.method('IServiceOperationService.add_service_record')
    def ___add_service_record(order_ids):
        ''' 新增服务记录（根据服务订单生成服务记录）'''
        res = service_operation_func.add_service_record(
            order_ids, False, False)
        return res

    @jsonrpc.method('IServicesLedgerService.get_service_ledger_list')
    def ___get_service_ledger_list(condition, page=None, count=None):
        ''' 查询台账列表 '''
        res = service_operation_func.get_service_ledger_list(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IServicesLedgerService.update_services_ledger')
    def ___get_service_ledger_list(record_ids):
        ''' 新增费用台账数据 '''
        res = service_operation_func.update_services_ledger(record_ids)
        return res

    # @jsonrpc.method('IServicesLedgerService.invoking_financial')
    # def ___invoking_financial(record_ids):
    #     ''' 发送费用记录到财务模块 '''
    #     res = service_operation_func.invoking_financial(record_ids)
    #     return res

    @jsonrpc.method('IServiceOperationService.update_service_settlement')
    def __update_service_settlement(service_settlement):
        ''' 服务结算 '''
        res = service_operation_func.update_service_settlement(
            service_settlement)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_provider_record')
    def __update_service_provider_record(reviewed_result):
        '''审核服务商申请'''
        res = service_operation_func.update_service_provider_record(
            reviewed_result)
        return res

    @jsonrpc.method('IServiceOperationService.get_Subsidy_service_provider_list_all')
    def __get_Subsidy_service_provider_list(condition, page=None, count=None):
        ''' 待审核服务商列表查询'''
        res = service_operation_func.get_Subsidy_service_provider_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.add_service_order_comment')
    def __add_service_order_comment(orderComments):
        ''' 新增服务订单评论 '''
        res = service_operation_func.add_service_order_comment(orderComments)
        return res

    @jsonrpc.method('IServiceOperationService.delete_service_order')
    def __delete_service_order(order_id):
        ''' 删除服务订单 '''
        res = service_operation_func.delete_service_order(order_id)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_plan_list_all')
    def __get_service_plan_list_all(condition, page=None, count=None):
        ''' 服务计划列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_plan, PermissionName.query)
        res = service_operation_func.get_service_plan_list(permission_data,
                                                           condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceOperationService.get_service_plan_list')
    def __get_service_plan_list(condition, page=None, count=None):
        ''' 服务计划列表 '''
        res = service_operation_func.get_service_plan_list(N(),
                                                           condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_plan')
    def __update_service_plan(condition):
        ''' 服务计划 '''
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        permission_data = security_service.judge_permission_other(
            FunctionName.service_plan, Permission_name)
        res = service_operation_func.update_service_plan(condition)
        return res

    @jsonrpc.method('IServiceOperationService.delete_service_plan')
    def ___delete_service_plan(ids):
        ''' 删除服务人员登记信息 '''
        permission_data = security_service.judge_permission_other(
            FunctionName.service_plan, PermissionName.delete)
        res = service_operation_func.delete_service_plan(ids)
        return res
