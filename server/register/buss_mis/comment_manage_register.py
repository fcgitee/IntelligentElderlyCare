# -*- coding: utf-8 -*-
'''
评论类型相关函数注册
'''
from ...service.buss_mis.comment_manage import CommentManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    comment_manage_func = CommentManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICommentManageService.get_comment_list_all')
    def __get_comment_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_management, PermissionName.query)
        res = comment_manage_func.get_comment_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICommentManageService.get_comment_list_look')
    def __get_comment_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_management_look, PermissionName.query)
        res = comment_manage_func.get_comment_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICommentManageService.update_comment')
    def __update_comment(comment):
        if 'id' in list(comment.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.comment_management, PermissionName.edit)
        res = comment_manage_func.update_comment(comment)
        comment_manage_func.bill_manage_service.insert_logs(
            '机构养老', '发布评论管理', '新增/编辑发布评论管理')
        return res

    @jsonrpc.method('ICommentManageService.delete_comment')
    def __delete_comment(Ids):
        security_service.judge_permission_other(
            FunctionName.comment_management, PermissionName.delete)
        res = comment_manage_func.delete_comment(Ids)
        comment_manage_func.bill_manage_service.insert_logs(
            '机构养老', '发布评论管理', '删除发布评论管理')
        return res

    # 获取审核列表
    @jsonrpc.method('ICommentManageService.get_comment_audit_list_all')
    def __get_comment_audit_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_review, PermissionName.query)
        # condition['audit_status'] = '待审批'
        res = comment_manage_func.get_comment_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICommentManageService.get_comment_audit_list_look')
    def __get_comment_audit_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_review_look, PermissionName.query)
        res = comment_manage_func.get_comment_audit_list(
            permission_data, condition, page, count)
        return res

    # 审核通过
    @jsonrpc.method('ICommentManageService.comment_audit_success')
    def __comment_audit_success(comment):
        security_service.judge_permission_other(
            FunctionName.comment_review, PermissionName.edit)
        res = comment_manage_func.comment_audit_success(comment)
        comment_manage_func.bill_manage_service.insert_logs(
            '机构养老', '发布评论管理', '审核通过')
        return res

    # 审核不通过
    @jsonrpc.method('ICommentManageService.comment_audit_fail')
    def __comment_audit_fail(comment):
        security_service.judge_permission_other(
            FunctionName.comment_review, PermissionName.edit)
        res = comment_manage_func.comment_audit_fail(comment)
        comment_manage_func.bill_manage_service.insert_logs(
            '机构养老', '发布评论管理', '审核不通过')
        return res
