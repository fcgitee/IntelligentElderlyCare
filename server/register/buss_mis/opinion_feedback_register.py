'''
@Author: your name
@Date: 2020-01-14 16:42:16
@LastEditTime : 2020-01-14 16:50:09
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\opinion_feedback_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.opinion_feedback import OpinionFeedbackService
# -*- coding: utf-8 -*-
'''
意见反馈管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    opinion_feedback_func = OpinionFeedbackService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IOpinionFeedbackService.update_opinion_feedback')
    def __update_opinion_feedback(condition):
        '''点击意见反馈'''
        res = opinion_feedback_func.update_opinion_feedback(condition)
        opinion_feedback_func.bill_manage_service.insert_logs(
            '平台运营方', 'APP意见反馈', '新增/编辑APP意见反馈')
        return res

    @jsonrpc.method('IOpinionFeedbackService.get_app_feedback_list')
    def __get_app_feedback_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.app_feedback, PermissionName.query)
        res = opinion_feedback_func.get_app_feedback_list(permission_data,
                                                          condition, page, count)
        return res
