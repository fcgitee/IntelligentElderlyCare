# -*- coding: utf-8 -*-
'''
慈善管理注册
'''
from ...service.buss_mis.charitable_manage import CharitableManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N
from ...service.common import get_user_id_or_false, Subject


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, subject: Subject):
    charitable_manage_func = CharitableManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    subject.attach(charitable_manage_func)

    @jsonrpc.method('ICharitableService.get_charitable_information_list_all')
    def __get_charitable_information_list(condition, page=None, count=None):
        # 获取慈善汇总列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_information_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_information_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_donate_list_all')
    def __get_charitable_donate_list(condition, page=None, count=None):
        # 获取慈善捐款列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_donate_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_donate_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_donate')
    def __update_charitable_donate(data):
        # 新增慈善捐款
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_donate_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_donate_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_donate(data)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_project_list_all')
    def __get_charitable_project_list(condition, page=None, count=None):
        # 获取慈善项目列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_project_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_project_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_project')
    def __update_charitable_project(data):
        # 新增慈善项目
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_project_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_project_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_project(data)
        return res
    @jsonrpc.method('ICharitableService.get_charitable_recipient_apply_list')
    def __get_charitable_recipient_apply_list(condition, page=None, count=None):
        res = charitable_manage_func.get_charitable_recipient_apply_list(
            N(), condition, page=None, count=None)
        return res
    @jsonrpc.method('ICharitableService.get_charitable_recipient_apply_list_all')
    def __get_charitable_recipient_apply_list(condition, page=None, count=None):

        # if charitable_manage_func.get_session_user_islogin() == False:
        #     return '请先登录'
        # 获取个人募捐申请列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_recipient_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_recipient_apply_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_recipient_apply')
    def __update_charitable_recipient_apply(data):
        # 新增个人募捐申请
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_recipient_manage_all, PermissionName.edit)
        # else:
            # permission_data = security_service.judge_permission_other(
            #     FunctionName.charitable_recipient_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_recipient_apply(data)
        return res

    @jsonrpc.method('ICharitableService.get_volunteer_service_category_list_all')
    def __get_volunteer_service_category_list(condition, page=None, count=None):
        # 获取志愿者服务项目列表
        permission_data = security_service.judge_permission_query(
            FunctionName.volunteer_service_category_manage_all, PermissionName.query)
        res = charitable_manage_func.get_volunteer_service_category_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_volunteer_service_category')
    def __update_volunteer_service_category(data):
        # 新增志愿者服务项目
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.volunteer_service_category_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.volunteer_service_category_manage_all, PermissionName.add)
        res = charitable_manage_func.update_volunteer_service_category(data)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_appropriation_list_all')
    def __get_charitable_appropriation_list(condition, page=None, count=None):
        # 获取慈善拨款审核列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_appropriation_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_appropriation_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_appropriation')
    def __update_charitable_appropriation(data):
        # 新增慈善拨款审批
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_appropriation_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_appropriation_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_appropriation(data)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_society_list_all')
    def __get_charitable_society_list(condition, page=None, count=None):
        # 获取慈善会列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_society_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_society_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.get_recipient_step')
    def __get_recipient_step(condition, page=None, count=None):
        # 获取募捐申请步骤
        res = charitable_manage_func.get_recipient_step(
            N(), condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_society_list')
    def __get_charitable_society_list(condition, page=None, count=None):
        # 获取慈善会列表
        res = charitable_manage_func.get_charitable_society_list(
            N(), condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_society')
    def __update_charitable_society(data):
        # 新增编辑慈善会资料
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_society_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_society_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_society(data)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_title_fund_list_all')
    def __get_charitable_title_fund_list(condition, page=None, count=None):
        # 获取冠名基金列表
        permission_data = security_service.judge_permission_query(
            FunctionName.charitable_title_fund_manage_all, PermissionName.query)
        res = charitable_manage_func.get_charitable_title_fund_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.get_charitable_title_fund_list')
    def __get_charitable_title_fund_list(condition, page=None, count=None):
        # 获取冠名基金列表
        res = charitable_manage_func.get_charitable_title_fund_list(
            N(), condition, page=None, count=None)
        return res

    @jsonrpc.method('ICharitableService.update_charitable_title_fund')
    def __update_charitable_title_fund(data):
        # 新增编辑冠名基金
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_title_fund_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.charitable_title_fund_manage_all, PermissionName.add)
        res = charitable_manage_func.update_charitable_title_fund(data)
        return res

    @jsonrpc.method('ICharitableService.checkPayStatus')
    def __checkPayStatus(condition):
        # 获取冠名基金列表
        res = charitable_manage_func.checkPayStatus(condition)
        return res
