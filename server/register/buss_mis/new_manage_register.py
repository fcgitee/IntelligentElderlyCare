from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.news_manage import NewsService
# -*- coding: utf-8 -*-
'''
新闻管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    news_func = NewsService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IArticleService.get_news_list')
    def __get_news_list(condition, page=None, count=None):
        res = news_func.get_news_list(N(),
                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.get_app_home_page_news_list')
    def __get_app_home_page_news_list(condition, page=None, count=None):
        res = news_func.get_app_home_page_news_list(N(),
                                                    condition, page, count)
        return res

    # @jsonrpc.method('IArticleService.get_article_draft')
    # def __get_article_draft(condition):
    #     res = news_func.get_article_draft(condition)
    #     return res

    @jsonrpc.method('IArticleService.get_news_pure_list_all')
    def __get_news_pure_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.new_management, PermissionName.query)
        res = news_func.get_news_pure_list(permission_data,
                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.get_news_pure_list')
    def __get_news_pure_list(condition, page=None, count=None):
        res = news_func.get_news_pure_list(N(),
                                      condition, page, count)
        return res


    @jsonrpc.method('IArticleService.get_news_list_all')
    def __get_news_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.new_management, PermissionName.query)
        res = news_func.get_news_list(permission_data,
                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.get_news_list_picture')
    def __get_news_list_picture(condition):
        res = news_func.get_news_list_picture(condition)
        return res

    @jsonrpc.method('IArticleService.get_news_list_look')
    def __get_news_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.new_management_look, PermissionName.query)
        res = news_func.get_news_list(permission_data,
                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.update_news')
    def __update_news(news):
        Permission_name = PermissionName.edit if news.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.announcement_management, Permission_name)
        res = news_func.update_news(
            news)
        news_func.bill_manage_service.insert_logs(
            '机构养老', '发布咨询管理', '新增/编辑发布咨询管理')
        return res

    @jsonrpc.method('IArticleService.update_ctrl')
    def __update_ctrl(condition):
        res = news_func.update_ctrl(condition)
        return res

    @jsonrpc.method('IArticleService.check_app_role_type')
    def __check_app_role_type(condition):
        res = news_func.check_app_role_type(condition)
        return res

    @jsonrpc.method('IArticleService.delete_news')
    def __delete_news(news_ids):
        security_service.judge_permission_other(
            FunctionName.announcement_management, PermissionName.delete)
        res = news_func.delete_news(
            news_ids)
        news_func.bill_manage_service.insert_logs(
            '机构养老', '发布咨询管理', '删除发布咨询管理')
        return res
