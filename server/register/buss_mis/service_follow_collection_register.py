'''
@Author: your name
@Date: 2020-01-13 10:26:44
@LastEditTime : 2020-01-13 13:15:28
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\service_follow_collection_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.service_follow_collection import ServiceFollowCollectionService
# -*- coding: utf-8 -*-
'''
服务关注收藏管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_follow_collection_func = ServiceFollowCollectionService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceFollowCollectionService.get_service_follow_collection_list')
    def __get_service_follow_collection_list(condition, page=None, count=None):
        ''' 查询关注/收藏列表'''
        res = service_follow_collection_func.get_service_follow_collection_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceFollowCollectionService.update_service_follow_collection')
    def __update_service_follow_collection(business_id, s_type, s_object):
        '''点击关注/收藏'''
        res = service_follow_collection_func.update_service_follow_collection(
            business_id, s_type, s_object)
        return res

    @jsonrpc.method('IServiceFollowCollectionService.delete_service_follow_collection')
    def __delete_service_follow_collection(ids):
        '''取消关注/收藏'''
        res = service_follow_collection_func.delete_service_follow_collection(
            ids)
        return res

    @jsonrpc.method('IServiceFollowCollectionService.delete_service_follow_collection_by_business_id')
    def __delete_service_follow_collection_by_business_id(condition):
        '''根据业务ID取消关注/收藏'''
        res = service_follow_collection_func.delete_service_follow_collection_by_business_id(
            condition)
        return res
