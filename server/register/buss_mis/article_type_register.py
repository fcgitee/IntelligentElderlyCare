# -*- coding: utf-8 -*-
'''
文章类型相关函数注册
'''
from ...service.buss_mis.article_type import ArticleTypeService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    article_type_func = ArticleTypeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IArticleTypeService.get_article_type_list_all')
    def __get_article_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.article_type, PermissionName.query)
        res = article_type_func.get_article_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IArticleTypeService.get_article_type_list_look')
    def __get_article_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.article_type_look, PermissionName.query)
        res = article_type_func.get_article_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IArticleTypeService.update_article_type')
    def __update_article_type(article_type):
        Permission_name = PermissionName.edit if article_type.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.article_type, Permission_name)
        res = article_type_func.update_article_type(article_type)
        return res

    @jsonrpc.method('IArticleTypeService.delete_article_type')
    def __delete_article_type(Ids):
        security_service.judge_permission_other(
            FunctionName.article_type, PermissionName.delete)
        res = article_type_func.delete_article_type(Ids)
        return res

    @jsonrpc.method('IArticleTypeService.check_article_type_permission')
    def __check_article_type_permission():
        res = article_type_func.check_article_type_permission()
        return res
