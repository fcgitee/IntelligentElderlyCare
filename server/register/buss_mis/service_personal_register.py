# -*- coding: utf-8 -*-
'''
住宿服务项目注册
'''
from ...service.buss_mis.service_personal import ServicesPersonalService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_personal_func = ServicesPersonalService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceOperationService.get_service_personal_list')
    def __get_service_personal_list(condition, page=None, count=None):
        res = service_personal_func.get_service_personal_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_personal_apply')
    def __update_service_personal_apply(service_personal_apply):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_woker_apply, PermissionName.add)
        res = service_personal_func.update_service_personal_apply(
            service_personal_apply)
        return res

    @jsonrpc.method('IServiceOperationService.update_service_personal')
    def __update_service_personal(service_personal):
        res = service_personal_func.update_service_personal(
            service_personal)
        return res

    @jsonrpc.method('IServiceOperationService.delete_service_personal')
    def __delete_service_personal(service_personal_ids):
        res = service_personal_func.delete_service_personal(
            service_personal_ids)
        return res

    @jsonrpc.method('IServiceOperationService.subsidy_service_personal_apply')
    def __subsidy_service_personal_apply(reviewed_result):
        res = service_personal_func.subsidy_service_personal_apply(
            reviewed_result)
        return res

    @jsonrpc.method('IServiceOperationService.get_work_people_statistics')
    def __get_work_people_statistics(condition):
        res = service_personal_func.get_work_people_statistics(
            condition)
        return res

    @jsonrpc.method('IServiceOperationService.get_all_statistics')
    def __get_all_statistics(condition, page=None, count=None):
        res = service_personal_func.get_all_statistics(
            condition, page, count)
        return res