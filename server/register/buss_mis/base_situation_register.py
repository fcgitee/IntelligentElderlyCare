'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-15 12:11:54
@LastEditTime: 2019-12-05 14:06:13
@LastEditors: Please set LastEditors
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_mis.base_situation import BaseSituationService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)


# -*- coding: utf-8 -*-
'''
基本情况统计相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    base_situation_func = BaseSituationService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    # security_service = SecurityModule(
    #     db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IBaseSituationService.get_base_situation_list')
    def __get_activity_list(condition, page=None, count=None):
        res = base_situation_func.get_base_situation_list(
            N(), condition, page, count)
        return res
