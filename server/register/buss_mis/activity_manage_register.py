'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-15 12:11:54
@LastEditTime: 2019-12-05 14:06:13
@LastEditors: Please set LastEditors
'''
from ...service.buss_mis.activity_manage import ActivityManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N
# -*- coding: utf-8 -*-
'''
活动管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    activity_manage_func = ActivityManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IActivityService.get_activity_list')
    def __get_activity_list(condition, page=None, count=None):
        res = activity_manage_func.get_activity_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_list_all')
    def __get_activity_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_list(
            permission_data, condition, page, count)
        return res
        
    @jsonrpc.method('IActivityService.get_activity_pure_list')
    def __get_activity_pure_list(condition, page=None, count=None):
        res = activity_manage_func.get_activity_pure_list(
            N(), condition, page, count)
        return res
    @jsonrpc.method('IActivityService.get_activity_pure_list_all')
    def __get_activity_pure_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_pure_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_list_look')
    def __get_activity_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_list_look, PermissionName.query)
        res = activity_manage_func.get_activity_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_sign_in_list_all')
    def __get_activity_sign_in_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_sign_in_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_sign_in_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_sign_in_list_look')
    def __get_activity_sign_in_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_sign_in_list_look, PermissionName.query)
        res = activity_manage_func.get_activity_sign_in_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_sign_in_list')
    def __get_activity_sign_in_list(condition, page=None, count=None):
        res = activity_manage_func.get_activity_sign_in_list(N(),
                                                             condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_participate_list')
    def __get_activity_participate_list(condition, page=None, count=None):
        res = activity_manage_func.get_activity_participate_list(N(),
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_participate_pure_list_all')
    def __get_activity_participate_pure_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_participate_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_participate_pure_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_participate_list_all')
    def __get_activity_participate_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_participate_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_participate_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_participate_list_look')
    def __get_activity_participate_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_participate_list_look, PermissionName.query)
        res = activity_manage_func.get_activity_participate_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IActivityService.update_activity')
    def __update_activity(activity):
        if 'id' in activity.keys():
            __p = security_service.judge_permission_other(
                FunctionName.activity_list_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.activity_list_all, PermissionName.add)
        res = activity_manage_func.update_activity(activity)
        return res

    @jsonrpc.method('IActivityService.del_activity')
    def __del_activity(ids):
        __p = security_service.judge_permission_other(
            FunctionName.activity_list_all, PermissionName.delete)
        res = activity_manage_func.del_activity(ids)
        return res

    @jsonrpc.method('IActivityService.del_activity_room')
    def __del_activity_room(ids):
        __p = security_service.judge_permission_other(
            FunctionName.activity_room_list_all, PermissionName.delete)
        res = activity_manage_func.del_activity_room(ids)
        return res

    @jsonrpc.method('IActivityService.del_activity_room_reservation')
    def __del_activity_room_reservation(ids):
        __p = security_service.judge_permission_other(
            FunctionName.activity_room_reservation_list_all, PermissionName.delete)
        res = activity_manage_func.del_activity_room_reservation(ids)
        return res

    @jsonrpc.method('IActivityService.participate_activity')
    def __participate_activity(condition):
        res = activity_manage_func.participate_activity(condition)
        return res

    @jsonrpc.method('IActivityService.participate_activity_pc')
    def __participate_activity_pc(condition):
        condition['family_ids'] = [condition['elder_id']]
        del(condition['elder_id'])
        res = activity_manage_func.participate_activity_pc(condition)
        return res

    @jsonrpc.method('IActivityService.cancel_participate_activity')
    def __cancel_participate_activity(condition):
        res = activity_manage_func.cancel_participate_activity(condition)
        return res

    @jsonrpc.method('IActivityService.sign_in_activity')
    def __sign_in_activity(activity_id):
        res = activity_manage_func.sign_in_activity(activity_id)
        return res

    @jsonrpc.method('IActivityService.delete_activity_participate')
    def __delete_activity_participate(activity_id):
        __p = security_service.judge_permission_other(
            FunctionName.activity_participate_list_all, PermissionName.delete)
        res = activity_manage_func.delete_activity_participate(activity_id)
        return res

    @jsonrpc.method('IActivityService.delete_sign_in_activity')
    def __delete_sign_in_activity(activity_id):
        __p = security_service.judge_permission_other(
            FunctionName.activity_sign_in_list_all, PermissionName.delete)
        res = activity_manage_func.delete_sign_in_activity(activity_id)
        return res

    @jsonrpc.method('IActivityService.delete_activity_signin')
    def __delete_activity_signin(ids):
        __p = security_service.judge_permission_other(
            FunctionName.activity_sign_in_list_all, PermissionName.delete)
        res = activity_manage_func.delete_activity_signin(ids)
        return res

    @jsonrpc.method('IActivityService.get_activity_type_list')
    def __get_activity_type_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = activity_manage_func.get_activity_type_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_type_list_all')
    def __get_activity_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_type_all, PermissionName.query)
        res = activity_manage_func.get_activity_type_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_type_list_look')
    def __get_activity_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_type_look, PermissionName.query)
        res = activity_manage_func.get_activity_type_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IActivityService.update_activity_type')
    def __update_activity_type(activity_type):
        if 'id' in activity_type.keys():
            __p = security_service.judge_permission_other(
                FunctionName.activity_type_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.activity_type_all, PermissionName.add)
        res = activity_manage_func.update_activity_type(activity_type)
        return res

    @jsonrpc.method('IActivityService.del_activity_type')
    def __del_activity_type(activity_type_id):
        __p = security_service.judge_permission_other(
            FunctionName.activity_type_all, PermissionName.delete)
        res = activity_manage_func.del_activity_type(activity_type_id)
        return res

    @jsonrpc.method('IActivityService.get_activity_room_list')
    def __get_activity_room_list(condition, page=None, count=None):
        res = activity_manage_func.get_activity_room_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_room_list_all')
    def __get_activity_room_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_room_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_room_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_activity_room_list_look')
    def __get_activity_room_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_room_list_look, PermissionName.query)
        res = activity_manage_func.get_activity_room_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IActivityService.update_activity_room')
    def __update_activity_room(activity_room):
        res = activity_manage_func.update_activity_room(activity_room)
        activity_manage_func.bill_manage_server.insert_logs(
            '机构养老', '功能室管理', '新增/编辑功能室管理')
        return res

    @jsonrpc.method('IActivityService.get_activity_room_reservation_list_all')
    def __get_activity_room_reservation_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_room_reservation_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_room_reservation_list(permission_data,
                                                                      condition, page, count)
        return res

    @jsonrpc.method('IActivityService.reservate_activity_room')
    def __reservate_activity_room(activity_room_id, reservate_date):
        res = activity_manage_func.reservate_activity_room(
            activity_room_id, reservate_date)
        activity_manage_func.bill_manage_server.insert_logs(
            '机构养老', '功能室管理', '新增/编辑功能室管理')
        return res

    @jsonrpc.method('IActivityService.update_activity_room_reservation')
    def __update_activity_room_reservation(activity_room_reservation):
        res = activity_manage_func.update_activity_room_reservation(
            activity_room_reservation)
        activity_manage_func.bill_manage_server.insert_logs(
            '机构养老', '功能室管理', '新增/编辑功能室管理')
        return res

    @jsonrpc.method('IActivityService.get_activity_information_list_all')
    def __get_activity_information_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.activity_information_list_all, PermissionName.query)
        res = activity_manage_func.get_activity_information_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IActivityService.get_questionnaire_list')
    def __get_questionnaire_list(condition, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.activity_information_list_all, PermissionName.query)
        res = activity_manage_func.get_questionnaire_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IActivityService.update_questionnaire')
    def __update_questionnaire(data):
        res = activity_manage_func.update_questionnaire(data)
        return res

    @jsonrpc.method('IActivityService.upload_sign_in_list')
    def __upload_sign_in_list(data, date):
        res = activity_manage_func.upload_sign_in_list(data, date)
        return res
