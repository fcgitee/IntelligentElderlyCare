'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-19 14:21:32
@LastEditTime: 2019-12-05 15:03:08
@LastEditors: Please set LastEditors
'''

from ...service.buss_mis.carousel_manage import CarouselManageService
# -*- coding: utf-8 -*-
'''
轮播管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    carousel_manage_func = CarouselManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICarouselService.get_carousel_list')
    def __get_carousel_list(condition, page=None, count=None):
        res = carousel_manage_func.get_carousel_list(condition, page, count)
        return res

    @jsonrpc.method('ICarouselService.update_carousel')
    def __update_carousel(carousel):
        res = carousel_manage_func.update_carousel(carousel)
        return res

    @jsonrpc.method('ICarouselService.del_carousel')
    def __del_carousel(ids):
        res = carousel_manage_func.del_carousel(ids)
        return res
