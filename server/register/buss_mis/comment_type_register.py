# -*- coding: utf-8 -*-
'''
评论类型相关函数注册
'''
from ...service.buss_mis.comment_type import CommentTypeService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    comment_type_func = CommentTypeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICommentTypeService.get_comment_type_list_all')
    def __get_comment_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_type, PermissionName.query)
        res = comment_type_func.get_comment_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICommentTypeService.get_comment_type_list_look')
    def __get_comment_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.comment_type_look, PermissionName.query)
        res = comment_type_func.get_comment_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICommentTypeService.update_comment_type')
    def __update_comment_type(comment_type):
        Permission_name = PermissionName.edit if comment_type.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.comment_type, Permission_name)
        res = comment_type_func.update_comment_type(comment_type)
        return res

    @jsonrpc.method('ICommentTypeService.delete_comment_type')
    def __delete_comment_type(Ids):
        security_service.judge_permission_other(
            FunctionName.comment_type, PermissionName.delete)
        res = comment_type_func.delete_comment_type(Ids)
        return res
