'''
文章审核相关函数注册
'''
from ...service.buss_mis.article_audit import ArticleAuditService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    article_audit_func = ArticleAuditService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    # 获取审核列表
    @jsonrpc.method('IArticleService.get_article_audit_list_all')
    def __get_article_audit_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.article_review, PermissionName.query)
        res = article_audit_func.get_article_audit_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IArticleService.get_article_audit_list_look')
    def __get_article_audit_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.article_review_look, PermissionName.query)
        res = article_audit_func.get_article_audit_list(
            permission_data, condition, page, count)
        return res

    # 审核通过
    @jsonrpc.method('IArticleService.article_audit_success')
    def __article_audit_success(article):
        security_service.judge_permission_other(
            FunctionName.article_review, PermissionName.edit)
        res = article_audit_func.article_audit_success(article)
        return res

    # 审核不通过
    @jsonrpc.method('IArticleService.article_audit_fail')
    def __article_audit_fail(article):
        security_service.judge_permission_other(
            FunctionName.article_review, PermissionName.edit)
        res = article_audit_func.article_audit_fail(article)
        return res
