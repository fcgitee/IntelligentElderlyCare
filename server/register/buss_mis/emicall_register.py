'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-15 12:11:54
@LastEditTime: 2019-12-05 14:06:13
@LastEditors: Please set LastEditors
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_mis.emicall import EmiCallService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)


# -*- coding: utf-8 -*-
'''
易米云通相关函数注册
'''
def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    emi_func = EmiCallService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IEmiService.login')
    def __login(condition):
        res = emi_func.login(condition)
        return res

    @jsonrpc.method('IEmiService.get_role_id')
    def __get_role_id():
        res = emi_func.get_role_id()
        return res

    @jsonrpc.method('IEmiService.get_sign_in_session')
    def __get_role_id():
        res = emi_func.get_sign_in_session()
        return res

    @jsonrpc.method('IEmiService.get_work_no')
    def __get_work_no():
        res = emi_func.get_work_no()
        return res
