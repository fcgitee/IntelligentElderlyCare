'''
@Author: your name
@Date: 2020-01-09 15:33:21
@LastEditTime : 2020-01-09 17:56:28
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\operation_record_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.health_care import HealthCareService
# -*- coding: utf-8 -*-
'''
健康照护注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    health_care_func = HealthCareService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHealthCareService.update_medicine_file')
    def __update_medicine_file(condition):
        res = health_care_func.update_medicine_file(
            condition)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '药品档案', '新建/编辑药品档案')
        return res

    @jsonrpc.method('IHealthCareService.get_medicine_file_list')
    def __get_medicine_file_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.medicine, PermissionName.query)
        res = health_care_func.get_medicine_file_list(permission_data,
                                                      condition, page, count)
        return res

    @jsonrpc.method('IHealthCareService.del_medicine_file')
    def __del_medicine_file(id):
        res = health_care_func.del_medicine_file(
            id)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '药品档案', '删除药品档案')
        return res

    @jsonrpc.method('IHealthCareService.update_disease_file')
    def __update_disease_file(condition):
        res = health_care_func.update_disease_file(
            condition)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '疾病档案', '新建/编辑疾病档案')
        return res

    @jsonrpc.method('IHealthCareService.get_disease_file_list')
    def __get_disease_file_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.disease, PermissionName.query)
        res = health_care_func.get_disease_file_list(permission_data,
                                                     condition, page, count)
        return res

    @jsonrpc.method('IHealthCareService.del_disease_file')
    def __del_disease_file(id):
        res = health_care_func.del_disease_file(
            id)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '疾病档案', '删除疾病档案')
        return res

    @jsonrpc.method('IHealthCareService.update_allergy_file')
    def __update_allergy_file(condition):
        res = health_care_func.update_allergy_file(
            condition)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '过敏档案', '新建/编辑过敏档案')
        return res

    @jsonrpc.method('IHealthCareService.get_allergy_file_list')
    def __get_allergy_file_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.allergy, PermissionName.query)
        res = health_care_func.get_allergy_file_list(permission_data,
                                                     condition, page, count)
        return res

    @jsonrpc.method('IHealthCareService.del_allergy_file')
    def __del_allergy_file(id):
        res = health_care_func.del_allergy_file(
            id)
        health_care_func.bill_manage_server.insert_logs(
            '机构养老', '过敏档案', '删除过敏档案')
        return res

    @jsonrpc.method('IHealthCareService.update_visit_question')
    def __update_visit_question(condition):
        res = health_care_func.update_visit_question(
            condition)
        return res

    @jsonrpc.method('IHealthCareService.get_visit_question_list')
    def __get_visit_question_list(condition, page=None, count=None):
        res = health_care_func.get_visit_question_list(
            condition, page, count)
        return res

    @jsonrpc.method('IHealthCareService.del_visit_question')
    def __del_visit_question(id):
        res = health_care_func.del_visit_question(
            id)
        return res

    @jsonrpc.method('IHealthCareService.update_deposit_setting')
    def __update_deposit_setting(condition):
        res = health_care_func.update_deposit_setting(
            condition)
        return res

    @jsonrpc.method('IHealthCareService.get_deposit_setting_list')
    def __get_deposit_setting_list(condition, page=None, count=None):
        res = health_care_func.get_deposit_setting_list(
            condition, page, count)
        return res

    @jsonrpc.method('IHealthCareService.del_deposit_setting')
    def __del_deposit_setting(id):
        res = health_care_func.del_deposit_setting(
            id)
        return res
