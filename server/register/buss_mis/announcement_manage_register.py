# -*- coding: utf-8 -*-
'''
公告管理注册
'''
from ...service.buss_mis.announcement_manage import AnnouncementService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    announcement_func = AnnouncementService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IArticleService.get_announcement_list_all')
    def __get_announcement_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.announcement_management, PermissionName.query)
        res = announcement_func.get_announcement_list(permission_data,
                                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.get_announcement_list_look')
    def __get_announcement_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.announcement_management_look, PermissionName.query)
        res = announcement_func.get_announcement_list(permission_data,
                                                      condition, page, count)
        return res

    @jsonrpc.method('IArticleService.update_announcement')
    def __update_announcement(announcement):
        Permission_name = PermissionName.edit if announcement.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.announcement_management, Permission_name)
        res = announcement_func.update_announcement(
            announcement)
        return res

    @jsonrpc.method('IArticleService.delete_announcement')
    def __delete_announcement(announcement_ids):
        security_service.judge_permission_other(
            FunctionName.announcement_management, PermissionName.delete)
        res = announcement_func.delete_announcement(
            announcement_ids)
        return res
