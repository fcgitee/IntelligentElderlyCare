from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.service_item_package import ServiceItemPackageService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:56:18
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\service_item_package_register.py
'''

# -*- coding: utf-8 -*-
'''
服务包注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_item_package_func = ServiceItemPackageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceItemPackage.get_service_item_package_all')
    def __get_service_item_package(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_product, PermissionName.query)
        res = service_item_package_func.get_service_item_package(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IServiceItemPackage.get_service_item_package_look')
    def __get_service_item_package_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_product_look, PermissionName.query)
        res = service_item_package_func.get_service_item_package(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IServiceItemPackage.get_service_item_package')
    def __get_service_item_package(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = service_item_package_func.get_service_item_package(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IServiceItemPackage.get_service_item_package_pure')
    def __get_service_item_package_pure(condition={}, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = service_item_package_func.get_service_item_package_pure(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IServiceItemPackage.update_service_item_package')
    def _update_service_item_package(service_item_package):
        Permission_name = PermissionName.add if service_item_package.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.service_product, Permission_name)
        res = service_item_package_func.update_service_item_package(
            service_item_package)
        service_item_package_func.bill_manage_service.insert_logs(
            '机构养老', '服务产品', '新建/编辑服务产品')
        return res

    @jsonrpc.method('IServiceItemPackage.delete_service_item_package')
    def __delete_service_item_package(service_item_package_ids):
        security_service.judge_permission_other(
            FunctionName.service_product, PermissionName.delete)
        res = service_item_package_func.delete_service_item_package(
            service_item_package_ids)
        service_item_package_func.bill_manage_service.insert_logs(
            '机构养老', '服务产品', '删除服务产品')
        return res
