# -*- coding: utf-8 -*-
'''
服务记录注册
'''
from ...service.buss_mis.service_record import ServiceRecordService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_record_func = ServiceRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceRecordService.get_service_record_list_all')
    def __get_service_record_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = service_record_func.get_service_record_list_yh(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.get_service_record_list_all_app')
    def __get_service_record_list_all_app(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = service_record_func.get_service_record_list_all_app(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.get_service_record_list_look')
    def __get_service_record_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_recode_look, PermissionName.query)
        res = service_record_func.get_service_record_list_yh(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.update_service_record')
    def __update_service_record(service_record):
        Permission_name = PermissionName.add if service_record.get(
            'id') else PermissionName.edit
        permission_data = security_service.judge_permission_other(
            FunctionName.service_recode, Permission_name)
        res = service_record_func.update_service_record(
            service_record)
        return res

    @jsonrpc.method('IServiceRecordService.delete_service_record')
    def __delete_service_record(service_record_ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.service_recode, PermissionName.delete)
        res = service_record_func.delete_service_record(
            service_record_ids)
        return res

    @jsonrpc.method('IServiceRecordService.send_record')
    def __send_record(service_record_ids):
        res = service_record_func.invoking_financial(
            service_record_ids)
        return res

    @jsonrpc.method('IServiceRecordService.get_record_option_list')
    def __get_record_option_list(condition, page=None, count=None):
        res = service_record_func.get_record_option_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.get_record_evaluate_list')
    def __get_record_evaluate_list(condition, page=None, count=None):
        res = service_record_func.get_record_evaluate_list(
            condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.get_servicer_report_month')
    def __get_servicer_report_month(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.order_report, PermissionName.query)
        res = service_record_func.get_servicer_report_month_cg(
            permission_data, condition)
        return res

    @jsonrpc.method('IServiceRecordService.get_order_report')
    def __get_order_report(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.order_report, PermissionName.query)
        res = service_record_func.get_order_report(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IServiceRecordService.get_order_statistics')
    def __get_order_statistics(condition, page=None, count=None):
        res = service_record_func.get_order_statistics(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceRecordService.get_app_order_list')
    def __get_app_order_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = service_record_func.get_app_order_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('IServiceRecordService.get_cost_statistics')
    def __get_cost_statistics(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = service_record_func.get_cost_statistics(
            permission_data, condition, page=None, count=None)
        return res
