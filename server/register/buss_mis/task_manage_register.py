# -*- coding: utf-8 -*-
'''
任务管理注册
'''
from ...service.buss_mis.task_manage import TaskManageService, TaskState
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    task_manage_func = TaskManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ITaskService.add_new_process_task')
    def __add_new_process_task(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.task_examine_list_look, PermissionName.add)
        res = task_manage_func.add_new_task(
            data, TaskState.To_be_processed.value)
        return res

    @jsonrpc.method('ITaskService.add_new_receive_task')
    def __add_new_receive_task(data):
        permission_data = security_service.judge_permission_other(
            FunctionName.task_manage_all, PermissionName.add)
        res = task_manage_func.add_new_task(
            data, TaskState.To_be_receive.value)
        return res

    @jsonrpc.method('ITaskService.get_record_list')
    def __get_record_list(condition):
        res = task_manage_func.get_record_list(condition)
        return res

    @jsonrpc.method('ITaskService.get_task_type_list_all')
    def __get_task_type_list(condition, page=None, count=None):
        '''获取任务类型列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.task_type_manage_all, PermissionName.query)
        res = task_manage_func.get_task_type_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.input_task_type')
    def __input_task_type():
        res = task_manage_func.input_task_type()
        return res

    @jsonrpc.method('ITaskService.input_task_urgent')
    def __input_task_urgent():
        res = task_manage_func.input_task_urgent()
        return res

    @jsonrpc.method('ITaskService.input_person')
    def __input_person():
        res = task_manage_func.input_person()
        return res

    @jsonrpc.method('ITaskService.update_task_type')
    def __update_task_type(data):
        if 'id' in list(data.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.task_type_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.task_type_manage_all, PermissionName.add)
        res = task_manage_func.update_task_type(data)
        return res

    @jsonrpc.method('ITaskService.task_examine_passed')
    def __task_examine_passed(data):
        '''审核通过'''
        permission_data = security_service.judge_permission_other(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_examine(data, True)
        return res

    @jsonrpc.method('ITaskService.task_examine_failed')
    def __task_examine_failed(data):
        '''审核未通过'''
        permission_data = security_service.judge_permission_other(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_examine(data, False)
        return res

    @jsonrpc.method('ITaskService.task_receive_passed')
    def __task_receive_passed(data):
        '''接受任务'''
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_receive(data, True)
        return res

    @jsonrpc.method('ITaskService.task_receive_failed')
    def __task_receive_failed(data):
        '''拒绝任务'''
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_receive(data, False)
        return res

    @jsonrpc.method('ITaskService.task_appoint')
    def __task_appoint(data):
        '''指派任务'''
        permission_data = security_service.judge_permission_other(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_appoint(data)
        return res

    @jsonrpc.method('ITaskService.task_complete')
    def __task_complete(data):
        '''完成任务'''
        permission_data = security_service.judge_permission_other(
            FunctionName.task_manage_all, PermissionName.edit)
        res = task_manage_func.task_complete(data)
        return res

    @jsonrpc.method('ITaskService.get_all_task_list_all')
    def __get_all_task_list(condition, page=None, count=None):
        ''' 获取任务列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.query)
        res = task_manage_func.get_all_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_all_task_list_look')
    def __get_all_task_list(condition, page=None, count=None):
        ''' 查看 任务列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_look, PermissionName.query)
        res = task_manage_func.get_all_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_to_be_processed_task_list_look')
    def __get_to_be_processed_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_examine_list_look, PermissionName.query)
        res = task_manage_func.get_to_be_processed_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_to_be_send_task_list_look_edit')
    def __get_to_be_send_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_appoint_list_look_edit, PermissionName.query)
        res = task_manage_func.get_to_be_send_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_to_be_receive_task_list')
    def __get_to_be_receive_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.query)
        res = task_manage_func.get_to_be_receive_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_ongoing_task_list')
    def __get_ongoing_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.query)
        res = task_manage_func.get_ongoing_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_completed_task_list')
    def __get_completed_task_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_manage_all, PermissionName.query)
        res = task_manage_func.get_completed_task_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.get_task_service_order_list_all')
    def __get_task_service_order_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.task_appoint_list_look_edit, PermissionName.query)
        res = task_manage_func.get_task_service_order_list(
            permission_data, condition, page=None, count=None)
        return res

    @jsonrpc.method('ITaskService.change_task_service_order')
    def __change_task_service_order(data):
        security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = task_manage_func.change_task_service_order(data)
        return res

    @jsonrpc.method('ITaskService.save_time_service_order')
    def __save_time_service_order(data):
        security_service.judge_permission_query(
            FunctionName.service_recode, PermissionName.query)
        res = task_manage_func.save_time_service_order(data)
        return res

    @jsonrpc.method('ITaskService.back_start_service_order_record')
    def __back_start_service_order_record(record_id):
        res = task_manage_func.back_start_service_order_record(record_id)
        return res
