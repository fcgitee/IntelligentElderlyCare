# -*- coding: utf-8 -*-
'''
服务选项注册
'''
from ...service.buss_mis.services_option import ServicrOptionService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    servicre_option_func = ServicrOptionService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServicrOptionService.get_servicre_option_list')
    def __get_servicr_option_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = servicre_option_func.get_servicre_option_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IServicrOptionService.get_servicre_option_list_all')
    def __get_servicr_option_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_option, PermissionName.query)
        res = servicre_option_func.get_servicre_option_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IServicrOptionService.get_servicre_option_list_look')
    def __get_servicr_option_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_option_look, PermissionName.query)
        res = servicre_option_func.get_servicre_option_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IServicrOptionService.update_servicre_option')
    def __update_servicr_option(service_option):
        Permission_name = PermissionName.add if service_option.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.service_option, Permission_name)
        res = servicre_option_func.update_servicre_option(
            service_option)
        return res

    @jsonrpc.method('IServicrOptionService.delete_servicre_option')
    def __delete_servicr_option(service_option_ids):
        security_service.judge_permission_other(
            FunctionName.service_option, PermissionName.delete)
        res = servicre_option_func.delete_servicre_option(
            service_option_ids)
        return res
