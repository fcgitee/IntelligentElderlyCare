from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.service_scope import ServiceScopeService
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:50:29
@LastEditors: Please set LastEditors
'''

# -*- coding: utf-8 -*-
'''
服务项目类别注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    service_scope_func = ServiceScopeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IServiceScopeService.get_service_scope_list_all')
    def __get_service_scope_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_range, PermissionName.query)
        res = service_scope_func.get_service_scope_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IServiceScopeService.get_service_scope_list_look')
    def __get_service_scope_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.service_range_look, PermissionName.query)
        res = service_scope_func.get_service_scope_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IServiceScopeService.get_service_scope_list')
    def __get_service_scope_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = service_scope_func.get_service_scope_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IServiceScopeService.update_service_scope')
    def __update_service_scope(service_scope):
        Permission_name = PermissionName.add if service_scope.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.service_range, Permission_name)
        res = service_scope_func.update_service_scope(
            service_scope)
        service_scope_func.bill_manage_service.insert_logs(
            '平台运营方', '适用范围', '新建/编辑适用范围')
        return res

    @jsonrpc.method('IServiceScopeService.delete_service_scope')
    def __delete_service_scope(service_scope_ids):
        security_service.judge_permission_other(
            FunctionName.service_range, PermissionName.delete)
        res = service_scope_func.delete_service_scope(
            service_scope_ids)
        service_scope_func.bill_manage_service.insert_logs(
            '平台运营方', '适用范围', '删除适用范围')
        return res
