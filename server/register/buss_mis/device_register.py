from ...service.buss_mis.device import DeviceService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...pao_python.pao.service.data.mongo_db import N
# -*- coding: utf-8 -*-
'''
设备相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    device_func = DeviceService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IDeviceService.get_device_all')
    def __get_device(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.device_manage_all, PermissionName.query)
        res = device_func.get_device(permission_data, condition, page, count)
        return res

    @jsonrpc.method('IDeviceService.get_device_look')
    def __get_device(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.device_manage_look, PermissionName.query)
        res = device_func.get_device(permission_data, condition, page, count)
        return res

    @jsonrpc.method('IDeviceService.update_device')
    def __update_device(device):
        if 'id' in list(device.keys()):
            security_service.judge_permission_other(
                FunctionName.device_manage_all, PermissionName.edit)
        else:
            security_service.judge_permission_other(
                FunctionName.device_manage_all, PermissionName.add)
        res = device_func.update_device(device)
        device_func.bill_manage_service.insert_logs(
            '平台运营方', '长者设备管理', '新建/编辑长者设备管理')
        return res

    @jsonrpc.method('IDeviceService.update_device_weilan')
    def __update_device_weilan(device):
        if 'id' in list(device.keys()):
            security_service.judge_permission_other(
                FunctionName.device_manage_all, PermissionName.edit)
        else:
            security_service.judge_permission_other(
                FunctionName.device_manage_all, PermissionName.add)
        res = device_func.update_device_weilan(device)
        device_func.bill_manage_service.insert_logs(
            '平台运营方', '长者设备管理', '新建/编辑长者设备管理')
        return res

    @jsonrpc.method('IDeviceService.delete_device')
    def __delete_device(ids):
        security_service.judge_permission_other(
            FunctionName.device_manage_all, PermissionName.delete)
        res = device_func.delete_device(ids)
        device_func.bill_manage_service.insert_logs(
            '平台运营方', '长者设备管理', '删除长者设备管理')
        return res

    @jsonrpc.method('IDeviceService.import_device')
    def __import_device(condition):
        security_service.judge_permission_other(
            FunctionName.device_manage_all, PermissionName.add)
        res = device_func.import_device(condition)
        device_func.bill_manage_service.insert_logs(
            '平台运营方', '长者设备管理', '导入长者设备管理')
        return res

    @jsonrpc.method('IDeviceService.get_device_log_all')
    def __get_device_log(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.device_manage_all, PermissionName.query)
        res = device_func.get_device_log(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IDeviceService.get_device_message_list_all')
    def __get_device_message_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.device_manage_all, PermissionName.query)
        res = device_func.get_device_message_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IDeviceService.get_device_term_list_all')
    def __get_device_term_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.device_manage_all, PermissionName.query)
        res = device_func.get_device_term_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IDeviceService.device_api')
    def __device_api():
        res = device_func.device_api()
        return res

    @jsonrpc.method('IDeviceService.get_device_self')
    def __get_device_self(condition, page=None, count=None):
        ''' 获取帐号绑定的设备 '''
        res = device_func.get_device_self(condition, page=None, count=None)
        return res

    @jsonrpc.method('IDeviceService.bind_device_self')
    def __bind_device_self(condition):
        ''' 绑定设备 '''
        res = device_func.bind_device_self(condition)
        return res

    @jsonrpc.method('IDeviceService.unbind_device')
    def __unbind_device(condition):
        ''' 解绑设备 '''
        res = device_func.unbind_device_self(condition)
        return res

    @jsonrpc.method('IDeviceService.get_device_location')
    def __get_device_location(condition):
        ''' 获取设备定位 '''
        res = device_func.get_device_location(condition)
        return res

    @jsonrpc.method('IDeviceService.get_device_footprint')
    def __get_device_footprint(condition):
        ''' 获取设备足迹 '''
        res = device_func.get_device_footprint(condition)
        return res
