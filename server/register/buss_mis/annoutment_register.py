from ...service.buss_mis.annoutment import AnnoutmentService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    annoutment_service = AnnoutmentService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAnnoutmentService.get_annoutment_list_all')
    def __get_article_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.annountmentAll, PermissionName.query)
        res = annoutment_service.get_annoutment_list_all(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAnnoutmentService.get_annoutment_list_look')
    def __get_article_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.annountmentLook, PermissionName.query)
        res = annoutment_service.get_annoutment_list_all(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAnnoutmentService.update_annoutment')
    def __update_annoutment(condition):
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.annountmentAll, Permission_name)
        res = annoutment_service.update_annoutment(condition)
        annoutment_service.bill_manage_service.insert_logs(
            '平台运营方', '系统公告管理', '新建/编辑系统公告管理')
        return res

    @jsonrpc.method('IAnnoutmentService.delete_annoutment')
    def __update_annoutment(ids):
        security_service.judge_permission_other(
            FunctionName.annountmentAll, PermissionName.delete)
        res = annoutment_service.delete_annoutment(ids)
        annoutment_service.bill_manage_service.insert_logs(
            '平台运营方', '系统公告管理', '删除系统公告管理')
        return res
