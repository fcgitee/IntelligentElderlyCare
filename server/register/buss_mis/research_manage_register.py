'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-15 12:11:54
@LastEditTime: 2019-08-16 11:10:25
@LastEditors: Please set LastEditors
'''

from ...service.buss_mis.research_manage import ResearchManageService

# -*- coding: utf-8 -*-
'''
补贴管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    research_manage_func = ResearchManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IResearchService.get_research_list')
    @jsonrpc.method('IResearchService.update_research_data')
    def __update_research_data(research_data):
        res = research_manage_func.update_research_data(research_data)
        return res
