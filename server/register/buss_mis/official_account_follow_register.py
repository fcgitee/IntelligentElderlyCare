'''
@Author: your name
@Date: 2020-01-14 16:42:16
@LastEditTime: 2020-03-02 14:07:29
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\opinion_feedback_register.py
'''
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.official_follow_manage import OfficialFollowService
# -*- coding: utf-8 -*-
'''
微信公众号关注汇总管理注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    official_account_follow_func = OfficialFollowService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IOfficialFollowService.get_Statistics_list')
    def __get_Statistics_list(condition, page=None, count=None):
        '''获取汇总列表'''
        res = official_account_follow_func.get_Statistics_list(
            condition, page, count)
        official_account_follow_func.bill_manage_service.insert_logs(
            '平台运营方', '公众号关注数统计', '公众号关注数统计导出')
        return res
