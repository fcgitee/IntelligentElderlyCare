from server.service.buss_mis.transaction_management import TransactionService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:43:31
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_mis\transaction_management_register.py
'''

'''
版权：Copyright (c) 2019 China

创建日期：Friday July 12th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Wednesday, 17th July 2019 10:36:02 am
修改者: ymq(ymq) - <<email>>

说明
 1、交易管理相关接口注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    transaction_service = TransactionService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ITransactionService.add_transaction')
    def __add_transaction(data):
        res = transaction_service.add_transaction(data)
        return res

    @jsonrpc.method('ITransactionService.get_transaction_list')
    def __get_transaction_list(condition, page, count):
        res = transaction_service.get_transaction_list(
            condition, page, count)
        return res

    @jsonrpc.method('ITransactionService.del_transaction_by_id')
    def __del_transaction_by_id(transaction_id):
        res = transaction_service.del_transaction_by_id(transaction_id)
        return res

    @jsonrpc.method('ITransactionService.cancel_transaction_by_id')
    def __cancel_transaction_by_id(transaction_id):
        res = transaction_service.cancel_transaction_by_id(transaction_id)
        return res

    @jsonrpc.method('ITransactionService.return_transaction_by_id')
    def __return_transaction_by_id(transaction_id):
        res = transaction_service.return_transaction_by_id(transaction_id)
        return res

    @jsonrpc.method('ITransactionService.confirm_transaction_by_id')
    def __confirm_transaction_by_id(transaction_id):
        res = transaction_service.confirm_transaction_by_id(transaction_id)
        return res

    @jsonrpc.method('ITransactionService.modify_transaction_content')
    def __modify_transaction_content(data):
        res = transaction_service.modify_transaction_content(data)
        return res

    @jsonrpc.method('ITransactionService.add_transaction_comment')
    def __add_transaction_comment(data):
        res = transaction_service.add_transaction_comment(data)
        return res

    @jsonrpc.method('ITransactionService.del_transaction_comment')
    def __del_transaction_comment(data):
        res = transaction_service.del_transaction_comment(data)
        return res

    @jsonrpc.method('ITransactionService.query_transaction_comment_list')
    def __query_transaction_comment_list(condition, page, count):
        res = transaction_service.query_transaction_comment_list(
            condition, page, count)
        return res

    @jsonrpc.method('ITransactionService.query_comment_details_list')
    def __query_comment_details_list(condition, page=None, count=None):
        res = transaction_service.query_comment_details_list(
            condition, page, count)
        return res
