from ...service.buss_mis.demand import DemandService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
# -*- coding: utf-8 -*-
'''
需求相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    demand_func = DemandService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IDemandService.get_demand')
    def __get_demand(condition, page=None, count=None):
        res = demand_func.get_demand(condition, page, count)
        return res

    @jsonrpc.method('IDemandService.get_demand_details')
    def __get_demand_details(condition, page=None, count=None):
        res = demand_func.get_demand_details(condition, page, count)
        return res

    @jsonrpc.method('IDemandService.update_demand')
    def __update_demand(demand):
        res = demand_func.update_demand(demand)
        return res

    @jsonrpc.method('IDemandService.update_demand_bidding')
    def __update_demand_bidding(bidding):
        res = demand_func.update_demand_bidding(bidding)
        return res

    @jsonrpc.method('IDemandService.cancel_demand')
    def __cancel_demand(bidding):
        res = demand_func.cancel_demand(bidding)
        return res
