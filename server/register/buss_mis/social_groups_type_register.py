from ...service.buss_mis.social_groups_type import SocialGroupsTypeService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-20 11:28:24
@LastEditTime: 2019-12-05 15:02:17
@LastEditors: Please set LastEditors
'''
# -*- coding: utf-8 -*-
'''
社会群体类型管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    social_groups_type_func = SocialGroupsTypeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ISocialGroupsTypeService.get_social_groups_type_list_all')
    def __get_social_groups_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.social_groups_type_manage_all, PermissionName.query)
        res = social_groups_type_func.get_social_groups_type_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('ISocialGroupsTypeService.get_social_groups_type_list_look')
    def __get_social_groups_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.social_groups_type_manage_look, PermissionName.query)
        res = social_groups_type_func.get_social_groups_type_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('ISocialGroupsTypeService.update_social_groups_type')
    def __update_social_groups_type(socialGroupsType):
        if 'id' in list(socialGroupsType.keys()):
            permission_data = security_service.judge_permission_other(
                FunctionName.social_groups_type_manage_all, PermissionName.edit)
        else:
            permission_data = security_service.judge_permission_other(
                FunctionName.social_groups_type_manage_all, PermissionName.add)
        res = social_groups_type_func.update_social_groups_type(
            socialGroupsType)
        return res

    @jsonrpc.method('ISocialGroupsTypeService.del_social_groups_type')
    def __del_social_groups_type(ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.social_groups_type_manage_all, PermissionName.delete)
        res = social_groups_type_func.del_social_groups_type(ids)
        return res
