'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:42:28
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\channel_register.py
'''
# -*- coding: utf-8 -*-
from ..service.buss_pub.bill_manage import BillManageService
from ..service.buss_mis.business_area import BusinessAreaService
from ..service.buss_mis.service_operation import ServiceOperationService
from ..pao_python.pao.remote import JsonRpc2Error


class ErrorCode:
    COMMAND_NOT_FOUND = -36001


class ErrorMessage:
    COMMAND_NOT_FOUND = "命令不存在"


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    '''服务注册'''

    # # 单据服务
    # bill_manage_service = BillManageServer(
    #     db_addr, db_port, db_name, inital_password, session)
    # # 业务区域服务
    # business_area_service = BusinessAreaServer(
    #     db_addr, db_port, db_name, inital_password, session)
    # # 服务商服务
    # service_provider_service = ServiceProviderServer(
    #     db_addr, db_port, db_name, inital_password, session)

    # 命令列表
    command = {

    }

    @jsonrpc.method('IChannelService.query')
    def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
        # 判断命令是否存在
        if commandID not in list(command.keys()):
            raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,
                                ErrorMessage.COMMAND_NOT_FOUND)

        return command[commandID](paramValues, startIndex, maxCount)
