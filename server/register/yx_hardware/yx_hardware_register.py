
from ...service.yx_hardware.yx_hardware_manage import YxHardwareService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    yx_hardware_service = YxHardwareService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IYxHardwareService.get_position')
    def __get_position(imei_id):
        '''获取设备定位'''
        res = yx_hardware_service.get_position(imei_id)
        return res

    @jsonrpc.method('IYxHardwareService.get_footprint')
    def __get_footprint(imei_id, query_date):
        '''设备足迹查询'''
        res = yx_hardware_service.get_footprint(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.set_sos')
    def __set_sos(imei_id, sos):
        '''设备手表sos号码'''
        res = yx_hardware_service.set_sos(imei_id, sos)
        return res

# --------------------以后实现的功能接口-----------------------------
    @jsonrpc.method('IYxHardwareService.set_location_frequency')
    def __set_location_frequency(imei_id, fqcy):
        '''设置定位频率接口'''
        res = yx_hardware_service.set_location_frequency(imei_id, fqcy)
        return res

    @jsonrpc.method('IYxHardwareService.get_device_heart_info')
    def __get_device_heart_info(imei_id, query_date):
        '''心率数据查询'''
        res = yx_hardware_service.get_device_heart_info(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_device_blood_pressure_info')
    def __get_device_blood_pressure_info(imei_id, query_date):
        '''血压数据查询'''
        res = yx_hardware_service.get_device_blood_pressure_info(
            imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_device_blood_oxygen')
    def __get_device_blood_oxygen(imei_id, query_date):
        '''血氧数据查询'''
        res = yx_hardware_service.get_device_blood_oxygen(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_device_step_info')
    def __get_device_step_info(imei_id, query_date):
        '''计步数据查询'''
        res = yx_hardware_service.get_device_step_info(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_device_sleep_info')
    def __get_device_sleep_info(imei_id, query_date):
        '''睡眠数据查询'''
        res = yx_hardware_service.get_device_sleep_info(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_wd_info')
    def __get_wd_info(imei_id, query_date):
        '''体温数据查询'''
        res = yx_hardware_service.get_wd_info(imei_id, query_date)
        return res

    @jsonrpc.method('IYxHardwareService.get_remote_heart_rate')
    def __get_remote_heart_rate(imei_id):
        '''远程测心率数据（实时）'''
        res = yx_hardware_service.get_remote_heart_rate(imei_id)
        return res
