# -*- coding: utf-8 -*-
'''
需求项目选项相关函数注册
'''
from ...service.welfare_institution.requirement_option import requirementOptionService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    requirement_option_func = requirementOptionService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRequirementOptionService.get_list')
    def __get_requirement_project_list(condition, page=None, count=None):
        res = requirement_option_func.get_requirement_option_list(
            condition, page, count)
        return res

    @jsonrpc.method('IRequirementOptionService.update')
    def __update_user(requirement_project):
        res = requirement_option_func.update(requirement_project)
        return res

    @jsonrpc.method('IRequirementOptionService.delete')
    def __del_hotel_zone_project(Ids):
        res = requirement_option_func.del_requirement_option(Ids)
        return res
