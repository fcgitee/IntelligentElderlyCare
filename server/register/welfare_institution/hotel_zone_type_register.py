from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.accommodation_process import AccommodationProcess
from ...service.welfare_institution.leave_record import LeaveRecordService
from ...service.welfare_institution.hotel_zone_type import hotelZoneTypeService
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:54:46
@LastEditors: Please set LastEditors
'''


# -*- coding: utf-8 -*-
'''
住宿区域类型相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    hotel_zone_type_func = hotelZoneTypeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    accommodation_record = LeaveRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    accommodation_process = AccommodationProcess(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHotelZoneTypeService.get_list_all')
    def __get_hotel_zone_type_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.hotel_zone_all, PermissionName.query)
        res = hotel_zone_type_func.get_hotel_zone_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_list')
    def __get_hotel_zone_type_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_type_func.get_hotel_zone_type_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.update')
    def __update_user(hotel_zone_type):
        if 'id' in list(hotel_zone_type.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.hotel_zone_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.hotel_zone_all, PermissionName.add)
        res = hotel_zone_type_func.update(hotel_zone_type)
        return res

    @jsonrpc.method('IHotelZoneTypeService.delete')
    def __del_hotel_zone_type(Ids):
        __p = security_service.judge_permission_other(
            FunctionName.hotel_zone_all, PermissionName.delete)
        res = hotel_zone_type_func.del_hotel_zone_type(Ids)
        return res

    # @jsonrpc.method('INursingItemService.get_nursing_item_list')
    # def __get_nursing_list(condition, page, count):
    #     res = hotel_zone_type_func.get_nursing_list(condition, page, count)
    #     return {'result': [{'Description': '3333', 'GUID': None, 'bill_status': None, 'create_date': None, 'fee': '33001', 'id': '19d5c934-9c74-11e9-b904-144f8a6221df', 'modify_date': None, 'nursing_type': '123', 'state': '3333vm', 'valid_bill_id': None, 'version': None}], 'total': 10}

    @jsonrpc.method('IHotelZoneTypeService.get_bed_list_all')
    def __get_bed_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.bed_manange_all, PermissionName.query)
        res = hotel_zone_type_func.get_bed_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_bed_list_look')
    def __get_bed_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.bed_manange_look, PermissionName.query)
        res = hotel_zone_type_func.get_bed_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_bed_list')
    def __get_bed_list_yh(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_type_func.get_bed_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_valid_bed_count')
    def __get_valid_bed_count(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_type_func.get_valid_bed_count(
            permission_data, condition)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_bed_position')
    def __get_bed_position(condition):
        res = hotel_zone_type_func.get_bed_position(condition)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_residents_of_bed')
    def __get_residents_of_bed(condition):
        res = hotel_zone_type_func.get_residents_of_bed(condition)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_bed_list_no_user')
    def __get_bed_list_no_user(condition, page=None, count=None):
        org_list = security_service.org_all_subordinate()
        res = hotel_zone_type_func.get_bed_list_no_user(
            org_list, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.get_bed_list_byId')
    def __get_bed_list_byId(condition, page=None, count=None):
        res = hotel_zone_type_func.get_bed_list_byId(condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneTypeService.update_bed')
    def __update_bed(bed):
        if 'id' in list(bed.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.bed_manange_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.bed_manange_all, PermissionName.add)
        res = hotel_zone_type_func.update_bed(bed)
        hotel_zone_type_func.bill_manage_service.insert_logs(
            '机构养老', '床位管理', '新建/编辑床位管理')
        return res

    @jsonrpc.method('IHotelZoneTypeService.delete_bed')
    def __delete_bed(ids):
        __p = security_service.judge_permission_other(
            FunctionName.bed_manange_all, PermissionName.delete)
        res = hotel_zone_type_func.delete_bed(ids)
        hotel_zone_type_func.bill_manage_service.insert_logs(
            '机构养老', '床位管理', '删除床位管理')
        return res

    @jsonrpc.method('IHotelZoneTypeService.retreat_bed')
    def __retreat_bed(elderId):
        res = hotel_zone_type_func.retreat_bed(
            elderId, accommodation_record, accommodation_process)
        return res
