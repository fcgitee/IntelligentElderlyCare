from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.requirement_template import RequirementTemplateService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:05:11
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\requirement_template_register.py
'''
# -*- coding: utf-8 -*-
'''
需求模板相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    requirement_template_func = RequirementTemplateService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRequirementTemplateService.get_requirement_template_list')
    def __get_requirement_template_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = requirement_template_func.get_requirement_template_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IRequirementTemplateService.update_requirement_template')
    def __update_requirement_template(requirement_template):
        res = requirement_template_func.update_requirement_template(
            requirement_template)
        return res

    @jsonrpc.method('IRequirementTemplateService.delete_requirement_template')
    def __delete_requirement_template(Ids):
        res = requirement_template_func.delete_requirement_template(Ids)
        return res
