# # -*- coding: utf-8 -*-
# '''
# 护理档案相关函数注册
# '''
# from ...service.welfare_institution.nursing import NursingService


# def register(jsonrpc, db_addr, db_port, db_name, inital_password, session):
#     Nursing_func = NursingService(
#         db_addr, db_port, db_name, inital_password, session)

#     @jsonrpc.method('INursingService.get_nursing_list')
#     def __get_nursing_list(condition, page, count):
#         res = Nursing_func.get_nursing_list(condition, page, count)
#         return res

#     @jsonrpc.method('INursingService.update')
#     def __update_user(Nursing):
#         res = Nursing_func.update_nursing(Nursing)
#         return res

#     @jsonrpc.method('INursingService.delete')
#     def __delete_user(NursingIDs):
#         res = Nursing_func.delete_nursing(NursingIDs)
#         return res

#     @jsonrpc.method('INursingItemService.get_nursing_item_list')
#     def __get_nursing_list(condition, page, count):
#         res = Nursing_func.get_nursing_list(condition, page, count)
#         return {'result': [{'Description': '3333', 'GUID': None, 'bill_status': None, 'create_date': None, 'fee': '33001', 'id': '19d5c934-9c74-11e9-b904-144f8a6221df', 'modify_date': None, 'nursing_type': '123', 'state': '3333vm', 'valid_bill_id': None, 'version': None}], 'total': 10}
   
#     @jsonrpc.method('INursingService.get_nursing_type_list')
#     def __get_nursing_type_list(condition, page, count):
#         res = Nursing_func.get_nursing_type_list(condition, page, count)
#         return res

#     @jsonrpc.method('INursingService.nursing_type_update')
#     def __nursing_type_update(condition):
#         res = Nursing_func.nursing_type_update(condition)
#         return res
    
#     @jsonrpc.method('INursingService.nursing_type_delete')
#     def __nursing_type_update(ids):
#         res = Nursing_func.nursing_type_delete(ids)
#         return res

#     @jsonrpc.method('INursingService.get_nursing_relationship_list')
#     def __get_nursing_relationship_list(condition, page=1, count=10):
#         '''护理关系列表'''
#         res = Nursing_func.get_nursing_relationship_list(condition, page, count)
#         return res

#     @jsonrpc.method('INursingService.nursing_relationship_update')
#     def __nursing_relationship_update(condition, page=1, count=10):
#         '''新建/编辑护理关系'''
#         Nursing_func._add_update_and_bill(condition,'IEC_Nursing_RelationShip')
#         return Nursing_func.res
    
#     @jsonrpc.method('INursingService.nursing_relationship_delete')
#     def __nursing_relationship_delete(ids):
#         '''删除护理关系'''
#         Nursing_func._del_data(ids,'IEC_Nursing_RelationShip')
#         return Nursing_func.res