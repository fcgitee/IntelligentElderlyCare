'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-21 11:48:32
@LastEditTime: 2019-09-05 20:30:53
@LastEditors: Please set LastEditors
'''

import os
import json
import time
from server.pao_python.pao.data import process_db
from server.pao_python.pao.service.data.mongo_db import MongoFilter, C


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    @jsonrpc.method('IBigScreenService.query')
    def __query(command_id, paramValues=None, startIndex=None, maxCount=None):

        if command_id == "duidieBarTest-yiyang-rate":
            return [
                {"name": "机构评分", "value": 2.5}
            ]

        if command_id == "duidieBarTest-fuwujigouqingkuang-chizhuanyezheng":
            return [
                {"classify": "院长", '持专业证': 7, '其他': 10},
                {"classify": "医生", '持专业证': 1, '其他': 2},
                {"classify": "护士", '持专业证': 4, '其他': 11},
                {"classify": "护理员", '持专业证': 4, '其他': 11},
                {"classify": "社工", '持专业证': 4, '其他': 11},
                {"classify": "后勤人员", '持专业证': 4, '其他': 11},
            ]

        if command_id == "yanglaojigouxiangqing-jigoujieshao-describe":
            return [
                {"name": "机构名称", 'value': ["养老机构1"]},
                {"name": "机构类型", 'value': ["公建民营"]},
                {"name": "机构性质", 'value': ["事业单位"]},
                {"name": "法定代表人", 'value': ["法人1"]},
                {"name": "地址", 'value': ["地址1"]},
                {"name": "办公电话", 'value': ["16913103122"]},
                {"name": "床位数（张）", 'value': ["1010"]},
                {"name": "占地面积（㎡）①", 'value': ["1000㎡"]},
                {"name": "建筑面积（㎡）②", 'value': ["1000㎡"]},
                {"name": "配置医疗机构名称", 'value': ["医疗机构1"]},
                {"name": "合作机构医疗名称", 'value': ["无"]},
                {"name": "等级", 'value': ["五级"]},
            ]

        if command_id == "duidieBarTest-jujiayanglaoxiangqing-yichang":
            return [
                {"date": "7.13", '未见异常次数': 7, '不规律心律次数': 10},
                {"date": "7.14", '未见异常次数': 1, '不规律心律次数': 2},
                {"date": "7.15", '未见异常次数': 4, '不规律心律次数': 11},
                {"date": "7.16", '未见异常次数': 4, '不规律心律次数': 11},
                {"date": "7.17", '未见异常次数': 4, '不规律心律次数': 11},
                {"date": "7.18", '未见异常次数': 4, '不规律心律次数': 11},
                {"date": "7.19", '未见异常次数': 4, '不规律心律次数': 11},
            ]

        if command_id == "duidieBarTest-yiyang":
            return [
                {"year": "2015", '养老机构': 7, '社区幸福院': 10, '居家养老': 2},
                {"year": "2016", '养老机构': 12, '社区幸福院': 2, '居家养老': 3},
                {"year": "2017", '养老机构': 4, '社区幸福院': 11, '居家养老': 3},
            ]

        if command_id == "duidieBarTest-xingfuyuanqingkuang-xingji":
            return [
                {"type": "五星", '优良': 7, '合格': 10, '不合格': 7},
                {"type": "四星", '优良': 12, '合格': 2, '不合格': 6},
                {"type": "三星", '优良': 4, '合格': 11, '不合格': 8},
            ]

        if command_id == "duidieBarTest-xingfuyuanqingkuang-jigouyanshou":
            return [
                {"area": "狮山", '验收': 7, '未验收': 10},
                {"area": "大沥", '验收': 12, '未验收': 2},
                {"area": "里水", '验收': 4, '未验收': 11},
                {"area": "丹灶", '验收': 4, '未验收': 11},
                {"area": "西樵", '验收': 4, '未验收': 11},
                {"area": "桂城", '验收': 4, '未验收': 11},
                {"area": "九江", '验收': 4, '未验收': 11},
            ]

        if command_id == "duidieLineTest-nianling":
            return [
                {"year": "2015", '60岁以下': 7, '60-69岁': 10, '70-79岁': 2},
                {"year": "2016", '60岁以下': 12, '60-69岁': 2, '70-79岁': 3},
                {"year": "2017", '60岁以下': 4, '60-69岁': 11, '70-79岁': 3},
            ]

        if command_id == "duidieBarTest-nenglitongji":
            return [
                {"year": "2015", '能力完好': 7, '轻度失能': 10, '中度失能': 2, '重度失能': 5},
                {"year": "2016", '能力完好': 12, '轻度失能': 2, '中度失能': 3, '重度失能': 1},
                {"year": "2017", '能力完好': 4, '轻度失能': 11, '中度失能': 3, '重度失能': 11},
            ]

        if command_id == "duidieBarTest-yanglaojigouzhangzheqingkuang-nannv":
            return [
                {"age": "60岁以下", '男': 7, '女': 10},
                {"age": "60-69岁", '男': 12, '女': 12},
                {"age": "70-79岁", '男': 4, '女': 11},
                {"age": "80-89岁", '男': 4, '女': 11},
                {"age": "90-99岁", '男': 4, '女': 12},
                {"age": "100岁以上", '男': 4, '女': 15},
            ]

        if command_id == "duidieBarTest-xingfuyuanqingkuang-nannv":
            return [
                {"age": "60岁以下", '男': 7, '女': 10},
                {"age": "60-69岁", '男': 12, '女': 12},
                {"age": "70-79岁", '男': 4, '女': 11},
                {"age": "80-89岁", '男': 4, '女': 11},
                {"age": "90-99岁", '男': 4, '女': 12},
                {"age": "100岁以上", '男': 4, '女': 15},
            ]

        if command_id == "duidieBarTest-jigoufenlei":
            return [
                {"classify": "公办", '工商登记': 7, '民非登记': 10, '事业单位': 21},
                {"classify": "民办", '工商登记': 12, '民非登记': 12, '事业单位': 31},
                {"classify": "公建民营", '工商登记': 4, '民非登记': 11, '事业单位': 11},
            ]

        if command_id == "duidieLineTest-yanglaojigoushu":
            return [
                {"year": "2015", '公办': 7, '民办': 10, '中度失能': 2, '公建民营': 5},
                {"year": "2016", '公办': 12, '民办': 2, '中度失能': 3, '公建民营': 1},
                {"year": "2017", '公办': 4, '民办': 11, '中度失能': 3, '公建民营': 11},
            ]

        if command_id == "duidieLineTest-zizhuzhangzheshu":
            return [
                {"year": "2015", 'A1': 7, 'A2': 10, 'A3': 2, 'A4': 5, 'A5': 3, 'A6': 9,
                    'A7': 10, 'B8': 5, 'B9': 1, 'B10': 2, 'B11': 5, 'C12': 0, 'C13': 1, 'C14': 9},
                {"year": "2016", 'A1': 12, 'A2': 2, 'A3': 3, 'A4': 1, 'A5': 3, 'A6': 9,
                    'A7': 10, 'B8': 5, 'B9': 1, 'B10': 2, 'B11': 5, 'C12': 0, 'C13': 1, 'C14': 9},
                {"year": "2017", 'A1': 4, 'A2': 11, 'A3': 3, 'A4': 1, 'A5': 3, 'A6': 9,
                    'A7': 10, 'B8': 5, 'B9': 1, 'B10': 2, 'B11': 5, 'C12': 0, 'C13': 1, 'C14': 9},
            ]

        if command_id == "duidieLineTest-shequxingfuyuanshu":
            return [
                {"year": "2015", '五星': 7, '四星': 10, '三星': 2},
                {"year": "2016", '五星': 12, '四星': 2, '三星': 3},
                {"year": "2017", '五星': 4, '四星': 11, '三星': 3},
            ]

        if command_id == "zhangzhefenbu-diyu":
            return [
                {"year": "2015", '狮山': 7, '大沥': 10, '丹灶': 2},
                {"year": "2016", '狮山': 12, '大沥': 2, '丹灶': 3},
                {"year": "2017", '狮山': 4, '大沥': 11, '丹灶': 3},
            ]

        if command_id == "zhangzhefenbu-nianling":
            return [
                {"year": "2015", '男': 7, '女': 10},
                {"year": "2016", '男': 12, '女': 2, },
                {"year": "2017", '男': 4, '女': 13},
            ]

        if command_id == "zhangzheshu-jiankangzhibiao":
            return [
                {"year": "2015", '甘油三酯偏高': 7, '体温偏高': 10,
                    '血压偏高': 12, '低密度蛋白偏高': 7, '体费水率偏高': 1},
                {"year": "2016", '甘油三酯偏高': 12, '体温偏高': 2,
                    '血压偏高': 2, '低密度蛋白偏高': 17, '体费水率偏高': 3},
                {"year": "2017", '甘油三酯偏高': 4, '体温偏高': 13,
                    '血压偏高': 3, '低密度蛋白偏高': 9, '体费水率偏高': 4},
            ]

        if command_id == "get_organization_count":
            return [
                {"name": "服务长者", 'value': ["25万人"]},
                {"name": "养老机构", 'value': ["20家"]},
                {"name": "社区幸福院", 'value': ["199家"]},
                {"name": "居家服务商", 'value': ["28家"]},
                # {"name": "服务长者", 'value': ["25万人"]},
                # {"name": "养老机构", 'value': ["20家"]},
                # {"name": "社区幸福院", 'value': ["199家"]},
                # {"name": "居家服务商", 'value': ["28家"]},
            ]

        if command_id == "get_senior_citizens":
            current_dir1 = os.path.dirname(__file__)
          # print(current_dir1)
            f = open(current_dir1+"/temp_json.json", encoding='utf-8')
            tmp = f.read()

            # 获取找Ta的SOS数据
            data = {}

            def __process_func(db):
                # 获取当前时间戳（毫秒级，13位）
                current_millis = int(round(time.time() * 1000))
                _filter = MongoFilter()
                # 消息类型为sos告警，时间为前后3分钟内
                _filter.match((C('msgType') == 10001) & (C('time') > (
                    current_millis - 180000)) & (C('time') < (current_millis + 180000)))
                collection = db["test_device_alarm"]
                result_data = list(
                    collection.aggregate(_filter.filter_objects))
                if len(result_data) > 0:
                    nonlocal data
                    data = {}
                    data["name"] = "长者1"
                    data["lat"] = result_data[0]["lat"]
                    data["lon"] = result_data[0]["lon"]
                    data["address"] = result_data[0]["address"]
                    data["battery_energy"] = result_data[0]["battery_energy"]
                    data["battery_status"] = '充电中' if result_data[0]["battery_status"] == 1 else "未充电"
                    data["time"] = result_data[0]["msgCreateTime"]

            process_db(db_addr, db_port, db_name,
                       __process_func, self.db_user, self.db_pwd)

            return_data = json.loads(tmp)
            # return_data.append(
            #     {
            #         "mapType": "call",
            #         "name": "居家服务商",
            #         "data":[data]
            #     }
            # )
            return return_data

        if command_id == 'yiyang-jigouzonglan':
            return [
                {"year": "2015", '书画': 7, '唱歌': 10, '声乐': 12, '影视': 7},
                {"year": "2016", '书画': 12, '唱歌': 2, '声乐': 2, '影视': 17},
                {"year": "2017", '书画': 4, '唱歌': 13, '声乐': 3, '影视': 9},
            ]

        if command_id == 'yiyang-xingfuyuanzonglan':
            return [
                {"year": "2015", '养生': 7, '舞蹈': 10,
                    '手工': 12, '环保': 7, '花卉': 7, '游戏': 7},
                {"year": "2016", '养生': 12, '舞蹈': 2,
                    '手工': 2, '环保': 17, '花卉': 7, '游戏': 7},
                {"year": "2017", '养生': 4, '舞蹈': 13,
                    '手工': 3, '环保': 9, '花卉': 7, '游戏': 7},
            ]

        if command_id == 'yiyang-jujiafuwuzonglan':
            return [
                {"year": "2015", '助洁服务': 7, '养生保健': 10,
                    '助聊服务': 12, '助医服务': 7, '助急服务': 7},
                {"year": "2016", '助洁服务': 12, '养生保健': 2,
                    '助聊服务': 2, '助医服务': 17, '助急服务': 7},
                {"year": "2017", '助洁服务': 4, '养生保健': 13,
                    '助聊服务': 3, '助医服务': 9, '助急服务': 7},
            ]

        if command_id == 'yiyang-jigouzonglan-table':
            return [
                {"organization_name": "佛山市南海区狮山镇罗村敬老院", 'older_num': 106},
                {"organization_name": "里水镇颐年院（北院）", 'older_num': 96},
                {"organization_name": "里水镇颐年院（南院）", 'older_num': 89},
                {"organization_name": "黄岐梁永钊颐养院", 'older_num': 82},
                {"organization_name": "狮山镇小塘敬老院", 'older_num': 66},
                {"organization_name": "丹灶敬老院", 'older_num': 58},
                {"organization_name": "狮山镇敬老院", 'older_num': 53},
                {"organization_name": "九如城（狮山）康复中心", 'older_num': 39},
                {"organization_name": "南海区社会福利中心", 'older_num': 113},
                {"organization_name": "桂城敬老院", 'older_num': 1157},
                {"organization_name": "大沥敬老院", 'older_num': 274},
                {"organization_name": "文华颐养公寓（狮山）", 'older_num': 166},
                {"organization_name": "佛山市南海区西樵百西仁光颐老康复保健中心", 'older_num': 141},
                {"organization_name": "佛山市南海区狮山镇罗村敬老院", 'older_num': 106},
                {"organization_name": "里水镇颐年院（北院）", 'older_num': 96},
                {"organization_name": "里水镇颐年院（南院）", 'older_num': 89},
                {"organization_name": "黄岐梁永钊颐养院", 'older_num': 82},
                {"organization_name": "狮山镇小塘敬老院", 'older_num': 66},
                {"organization_name": "丹灶敬老院", 'older_num': 58},
                {"organization_name": "狮山镇敬老院", 'older_num': 53},
                {"organization_name": "九如城（狮山）康复中心", 'older_num': 39},
                {"organization_name": "南海区社会福利中心", 'older_num': 113},
                {"organization_name": "桂城敬老院", 'older_num': 1157},
                {"organization_name": "大沥敬老院", 'older_num': 274},
                {"organization_name": "文华颐养公寓（狮山）", 'older_num': 166},
                {"organization_name": "佛山市南海区西樵百西仁光颐老康复保健中心", 'older_num': 141},
            ]

        if command_id == 'yiyang-xingfuyuanzonglan-table':
            return [
                {"organization_name": "雅瑶社区幸福院", 'older_num': 106},
                {"organization_name": "罗湖社区幸福院", 'older_num': 96},
                {"organization_name": "河西社区幸福院", 'older_num': 89},
                {"organization_name": "平地社区幸福院", 'older_num': 82},
                {"organization_name": "水头社区幸福院", 'older_num': 66},
                {"organization_name": "樵乐社区幸福院", 'older_num': 58},
                {"organization_name": "颜峰社区幸福院", 'older_num': 53},
                {"organization_name": "盐步社区幸福院", 'older_num': 39},
                {"organization_name": "歧阳社区幸福院", 'older_num': 113},
                {"organization_name": "泌冲社区幸福院", 'older_num': 1157},
                {"organization_name": "南海区里水镇金和社区幸福院", 'older_num': 274},
                {"organization_name": "河东社区幸福院", 'older_num': 166},
                {"organization_name": "大沥镇华夏社区幸福院", 'older_num': 141},
                {"organization_name": "岐城社区幸福院", 'older_num': 106},
            ]

        if command_id == 'yiyang-jujiafuwuzonglan-table':
            return [
                {"organization_name": "佛山市南海区春晖养老服务中心", 'older_num': 106},
                {"organization_name": "佛山市南海区启正社会工作服务中心", 'older_num': 96},
                {"organization_name": "佛山市红星社会工作服务中心", 'older_num': 89},
                {"organization_name": "广东谷丰健康管理有限公司", 'older_num': 82},
                {"organization_name": "佛山市南海区佰乐社会工作服务中心", 'older_num': 66},
                {"organization_name": "佛山市禅城区馨和社会工作服务中心", 'older_num': 58},
                {"organization_name": "广州市心明爱社会工作服务中心", 'older_num': 53},
                {"organization_name": "广东善缘社会工作服务中心", 'older_num': 39},
                {"organization_name": "佛山市仁德社会工作服务中心", 'older_num': 113},
                {"organization_name": "南海区只会养老综合服务管理平台", 'older_num': 1157},
                {"organization_name": "佛山市希望社工服务中心", 'older_num': 274},
                {"organization_name": "佛山市南海区社会福利中心", 'older_num': 166},
                {"organization_name": "佛山市南海区文华颐养公寓", 'older_num': 141},
                {"organization_name": "佛山市南海区扬帆社会工作服务中心", 'older_num': 106},
            ]

        if command_id == 'yiyang-jujiafuwushangshu':
            return [
                {"year": "2015", 'num': 77},
                {"year": "2016", 'num': 12},
                {"year": "2017", 'num': 4},
            ]
        if command_id == 'yiyang-hujiaozhongxin-jietonglv':
            return [
                {"name": "接通率", 'value': 77},
            ]

        if command_id == 'yiyang-hujiaozhongxin-manyilv':
            return [
                {"name": "接通率", 'value': 18},
            ]

        if command_id == 'yiyang-hujiaofuwuzonglan-bubble':
            return [
                {"name": "呼叫热线量", 'value': 36978},
                {"name": "呼入总量", 'value': 23137},
                {"name": "呼出总量", 'value': 13841},
                {"name": "报警呼叫", 'value': 11264},
                {"name": "普通呼叫", 'value': 25714}
            ]

        if command_id == 'yiyang-hujiaofuwuzonglan-square':
            return [
                {"name": "响应时间", 'value': ["58秒"]},
                {"name": "未接听", 'value': [0]},
                {"name": "突发事件", 'value': [2]},
            ]

        if command_id == 'yiyang-24hujiaojilu':
            return [
                {"name": "1", 'value': 1},
                {"name": "2", 'value': 0},
                {"name": "3", 'value': 2},
                {"name": "4", 'value': 1},
                {"name": "5", 'value': 3},
                {"name": "6", 'value': 5},
                {"name": "7", 'value': 6},
                {"name": "8", 'value': 1},
                {"name": "9", 'value': 3},
                {"name": "10", 'value': 4},
                {"name": "11", 'value': 7},
                {"name": "12", 'value': 1},
                {"name": "13", 'value': 0},
                {"name": "14", 'value': 0},
                {"name": "15", 'value': 1},
                {"name": "16", 'value': 4},
                {"name": "17", 'value': 8},
                {"name": "18", 'value': 1},
                {"name": "19", 'value': 3},
                {"name": "20", 'value': 3},
                {"name": "21", 'value': 2},
                {"name": "22", 'value': 0},
                {"name": "23", 'value': 9},
                {"name": "24", 'value': 1},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-ruzhu-yuyue-square':
            return [
                {"name": "入住长者数", 'value': ["3866人", "↑ 6.5%"]},
                {"name": "预约长者数", 'value': ["1000人", "↑ 6.5%"]},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-huji':
            return [
                {"name": "香港", 'value': 1},
                {"name": "澳门", 'value': 5},
                {"name": "台湾", 'value': 2},
                {"name": "本地", 'value': 1},
                {"name": "省内非本", 'value': 3},
                {"name": "广西", 'value': 5},
                {"name": "湖南", 'value': 6},
                {"name": "广西", 'value': 1},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-zinvshu':
            return [
                {"name": "无", 'value': 1},
                {"name": "1个", 'value': 5},
                {"name": "2个", 'value': 2},
                {"name": "3个", 'value': 1},
                {"name": "4个", 'value': 3},
                {"name": "5个", 'value': 5},
                {"name": "5个以上", 'value': 5},
            ]

        if command_id == 'yanglaojigouzhengtizhangzheqingkuang-jiankang':
            return [
                {"name": "甘油三酯偏高", 'value': 1},
                {"name": "体温偏高", 'value': 5},
                {"name": "血压偏高", 'value': 2},
                {"name": "低密度蛋白偏高", 'value': 1},
                {"name": "体费水率偏高", 'value': 3},
                {"name": "脉搏偏高", 'value': 5},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-nenglipinggu':
            return [
                {"name": "能力完好", 'value': 4},
                {"name": "轻度失能", 'value': 11},
                {"name": "中度失能", 'value': 3},
                {"name": "重度失能", 'value': 5},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-laorenfenlei':
            return [
                {"name": "1类老人", 'value': 30},
                {"name": "2类老人，介助老人", 'value': 11},
                {"name": "3类老人，生活自理老人", 'value': 70},
            ]
        # 机构养老时间轴
        if command_id == 'yanglaojigouzhangzheqingkuang-time-line':
            return [
                {'value': '1', 'text': '1月'},
                {'value': '2', 'text': '2月'},
                {'value': '3', 'text': '3月'},
                {'value': '4', 'text': '4月'},
                {'value': '5', 'text': '5月'}
            ]
        if command_id == 'yanglaojigouzhengtiqingkuang-jigoutongji-square':
            return [
                {"name": "可提供养老床位数", 'value': ["3917张", "↑ 6.5%"]},
                {"name": "养老机构数", 'value': ["21家", "↑ 6.5%"]},
                {"name": "年度总收入", 'value': ["1000万元", "↑ 6.5%"]},
                {"name": "配置医疗机构的机构数", 'value': ["10家", "↑ 6.5%"]},
                {"name": "机构养老服务种类", 'value': ["30项", "↑ 6.5%"]},
                {"name": "年度总支出", 'value': ["1000万元", "↑ 6.5%"]},
            ]

        if command_id == 'yanglaojigouzhangzheqingkuang-xingfuyuanzongshu-square':
            return [
                {"name": "幸福院总数", 'value': ["199家", "↑ 6.5%"]},
                {"name": "服务长着数", 'value': ["11.26万人", "↑ 6.5%"]},
            ]

        if command_id == 'yanglaojigouzhengtiqingkuang-jigoudengji':
            return [
                {"name": "五级", 'value': 30},
                {"name": "四级", 'value': 11},
                {"name": "三级", 'value': 70},
            ]

        if command_id == 'yanglaojigouzhengtiqingkuang-jianshetouru':
            return [
                {"name": "九江", 'value': 30},
                {"name": "狮山", 'value': 70},
                {"name": "大沥", 'value': 70},
                {"name": "里水", 'value': 20},
                {"name": "丹灶", 'value': 10},
                {"name": "西樵", 'value': 10},
                {"name": "桂城", 'value': 8},
            ]

        if command_id == "yanglaojigouzhengtiqingkuang-xinzengchuangweizizhu":
            return [
                {"type": "新建", '投资': 7, '机构数': 15},
                {"type": "扩建", '投资': 12, '机构数': 22},
                {"type": "改建", '投资': 4, '机构数': 11},
                {"type": "转制", '投资': 4, '机构数': 12},
            ]

        if command_id == 'yanglaojigouzhengtiqingkuang-niandutouruzijin':
            return [
                {"name": "区级财政", 'value': 7},
                {"name": "镇级财政", 'value': 12},
                {"name": "福利金", 'value': 4},
                {"name": "机构自筹", 'value': 4},
            ]

        if command_id == 'duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng':
            return [
                {"classify": "院长", '持专业证': 7, '其他': 10},
                {"classify": "医生", '持专业证': 12, '其他': 2},
                {"classify": "护士", '持专业证': 4, '其他': 11},
                {"classify": "护理员", '持专业证': 4, '其他': 21},
                {"classify": "社工", '持专业证': 4, '其他': 13},
                {"classify": "护士", '持专业证': 4, '其他': 1},
                {"classify": "后勤人员", '持专业证': 4, '其他': 5},
            ]

        if command_id == 'duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-xueli':
            return [
                {"name": "大专以下", 'value': 70},
                {"name": "大专", 'value': 12},
                {"name": "本科", 'value': 41},
            ]

        if command_id == 'duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-nannv':
            return [
                {"age": "35岁以下", '男': 7, '女': 10},
                {"age": "36-45岁", '男': 12, '女': 2},
                {"age": "46-55岁", '男': 4, '女': 11},
                {"age": "56岁以上", '男': 4, '女': 21},
            ]

        if command_id == 'bed_overview':
            return [
                {
                    'index': "1号楼",
                    'info': [
                        {'No': "101-1", 'type': 1},
                        {'No': "101-2", 'type': 1},
                        {'No': "102-1", 'type': 2},
                        {'No': "102-2", 'type': 3},
                        {'No': "202-3", 'type': 3},
                        {'No': "201-1", 'type': 1},
                        {'No': "301-2", 'type': 1},
                        {'No': "402-1", 'type': 2},
                        {'No': "402-2", 'type': 3},
                        {'No': "502-3", 'type': 4},
                    ]
                },
                {
                    'index': "2号楼",
                    'info': [
                        {'No': "101-2", 'type': 6},
                        {'No': "301-2", 'type': 6},
                        {'No': "102-1", 'type': 2}
                    ]
                },
                {
                    'index': "3号楼",
                    'info': [
                        {'No': "101-1", 'type': 1},
                        {'No': "101-2", 'type': 5},
                        {'No': "102-1", 'type': 2},
                        {'No': "102-2", 'type': 3},
                        {'No': "102-3", 'type': 3},
                        {'No': "101-1", 'type': 1},
                        {'No': "101-2", 'type': 6},
                        {'No': "102-1", 'type': 2},
                        {'No': "102-2", 'type': 6},
                        {'No': "102-3", 'type': 3},
                        {'No': "102-2", 'type': 6},
                        {'No': "102-3", 'type': 3},
                    ]
                },
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-renshufenbu':
            return [
                {"name": "全区长者数", 'value': 70},
                {"name": "幸福院覆盖人数", 'value': 12},
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-quyufenbu':
            return [
                {"name": "九江", 'value': 2},
                {"name": "狮山", 'value': 40},
                {"name": "大沥", 'value': 28},
                {"name": "里水", 'value': 14},
                {"name": "丹灶", 'value': 8},
                {"name": "西樵", 'value': 6},
                {"name": "桂城", 'value': 4},
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-juzhuqingkuang':
            return [
                {"name": "独居", 'value': 2},
                {"name": "与子女同住", 'value': 40},
                {"name": "与配偶同住", 'value': 28},
                {"name": "与亲友同住", 'value': 14},
                {"name": "与保姆同住", 'value': 8},
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-huodong':
            return [
                {"name": "文体活动", 'value': 2},
                {"name": "亲朋活动", 'value': 40},
                {"name": "膳食", 'value': 28},
                {"name": "教育", 'value': 14},
                {"name": "其他", 'value': 8},
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-jiankang':
            return [
                {"name": "甘油三酯偏高", 'value': 2},
                {"name": "体温偏高", 'value': 40},
                {"name": "血压偏高", 'value': 28},
                {"name": "低密度蛋白偏高", 'value': 14},
                {"name": "体费水率偏高", 'value': 8},
                {"name": "脉搏偏高", 'value': 8},
            ]

        if command_id == 'yiyang-yiyangzhengtiqingkuang-Graph':
            return [
                {"name": "社区幸福院在建和试运行的199家", 'value': 199},
                {"name": "可提供养老床位数约3800张", 'value': 5293},
                {"name": "社区幸福院五星级的115家", 'value': 115},
                {"name": "居家养老服务订单497,430单", 'value': 497430},
                {"name": "南海区运行的养老机构20家", 'value': 20},
                {"name": "居家养老服务商28家", 'value': 28},
            ]

        # 居家养老-当天-居家服务长者整天情况-方块
        if command_id == 'jujiayanglaodangtian-zhangzhezhengtiqingkuang':
            return [
                {"name": "居家养老长者总数", 'value': ["11万人", "↑ 6.5%"]},
                {"name": "居家养老服务金额", 'value': ["30万元", "↑ 6.5%"]},
                {"name": "使用APP长者总数", 'value': ["2万人", "↑ 6.5%"]},
                {"name": "使用智能手环长者总数", 'value': ["1万人", "↑ 6.5%"]}
            ]

        # 居家养老-当天-居家服务长者整体情况-十四类补贴类型人数
        if command_id == 'jujiayanglaodangtian-shisileibutieleixingrenshu':
            return [
                {"name": "A1", 'value': 130},
                {"name": "A2", 'value': 520},
                {"name": "A3", 'value': 250},
                {"name": "A4", 'value': 15},
                {"name": "A5", 'value': 5},
                {"name": "A6", 'value': 25},
                {"name": "A7", 'value': 50},
                {"name": "B8", 'value': 10},
                {"name": "B9", 'value': 5},
                {"name": "B10", 'value': 20},
                {"name": "B11", 'value': 40},
                {"name": "C12", 'value': 520},
                {"name": "C13", 'value': 520},
                {"name": "C14", 'value': 30}
            ]

        # 居家养老-当天-居家服务长者整体情况-男女人数
        if command_id == "jujiayanglaodangtian-nannv":
            return [
                {"age": "60岁以下", '男': 0.005, '女': 0.005},
                {"age": "60-69岁", '男': 0.58, '女': 0.61},
                {"age": "70-79岁", '男': 0.39, '女': 0.46},
                {"age": "80-89岁", '男': 0.13, '女': 0.24},
                {"age": "90-99岁", '男': 0.05, '女': 0.05},
                {"age": "100岁以上", '男': 0.005, '女': 0.005},
            ]

        # 居家养老-当天-居家服务长者整体情况-饼图
        if command_id == 'jujiayanglaodangtian-jujiafuwuzhangzhezhengtiqingkuang-pie':
            return [
                {"name": "香港", 'value': 1},
                {"name": "澳门", 'value': 5},
                {"name": "台湾", 'value': 2},
                {"name": "本地", 'value': 1},
                {"name": "省内非本", 'value': 3},
                {"name": "广西", 'value': 5},
                {"name": "湖南", 'value': 6},
                {"name": "广西", 'value': 1},
            ]

        # 居家养老-当天-居家服务长者整体情况-居家服务资金流入
        if command_id == 'jujiayanglaodangtian-jujiafuwuzhangzhezhengtiqingkuang-jujiafuwuzijinliuru':
            return [
                {"name": "区级", 'value': 10},
                {"name": "镇街级", 'value': 10},
                {"name": "彩票公益金", 'value': 2},
                {"name": "公益性捐款", 'value': 1},
                {"name": "捐助活动募集", 'value': 1},
                {"name": "自费", 'value': 0.2},
                {"name": "家属代缴", 'value': 0.4}
            ]

        # 居家养老-当天-顶部-方块
        if command_id == 'jujiayanglaodangtian-dingbu-square':
            return [
                {"name": "居家服务商", 'value': ["28 家"]},
                {"name": "服务长者", 'value': ["11 万人"]}
            ]

        # 居家养老-当天-地图
        if command_id == "jujiayanglaodangtian-ditu":
            current_dir1 = os.path.dirname(__file__)
          # print(current_dir1)
            f = open(current_dir1+"/temp_jujiayanglaodangtian_ditu.json")
            tmp = f.read()

            return json.loads(tmp)

        # 居家养老-当天-居家服务状态-服务类型订单数
        if command_id == "jujiayanglaodangtian-jujiafuwuzhuangtai-fuwuleixingdingdanshu":
            return [
                {"name": "文体活动", 'value': 5},
                {"name": "亲朋活动", 'value': 2},
                {"name": "膳食", 'value': 50},
                {"name": "教育", 'value': 20},
                {"name": "其他", 'value': 10}
            ]

        # 居家养老-当天-居家服务状态-饼图
        if command_id == "jujiayanglaodangtian-jujiafuwuzhuangtai-pie":
            return [
                {"name": "当天订单", 'value': 0.67},
                {"name": "暂无需求", 'value': 0.33}
            ]

        # 居家养老-当天-居家服务状态-等待服务-表格
        if command_id == "jujiayanglaodangtian-jujiafuwuzhuangtai-dengdaifuwu-table":
            return [
                {"older_name": "长者1", "age": 60, "support_type": "A1", "street": "镇街1", "service_type": "膳食", "service_org": "机构1",
                    "service_person": "人员1", "order_time": "2019.07.23 9:00", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者2", "age": 61, "support_type": "A2", "street": "镇街2", "service_type": "卫生整洁",
                    "service_org": "机构2", "service_person": "人员2", "order_time": "2019.07.23 9:01", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者3", "age": 62, "support_type": "A3", "street": "镇街3", "service_type": "教育", "service_org": "机构3",
                    "service_person": "人员3", "order_time": "2019.07.23 9:02", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者4", "age": 60, "support_type": "B8", "street": "镇街1", "service_type": "卫生整洁",
                    "service_org": "机构4", "service_person": "人员4", "order_time": "2019.07.23 9:03", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者5", "age": 61, "support_type": "A3", "street": "镇街2", "service_type": "教育", "service_org": "机构5",
                    "service_person": "人员5", "order_time": "2019.07.23 9:04", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者4", "age": 62, "support_type": "B8", "street": "镇街3", "service_type": "膳食", "service_org": "机构4",
                    "service_person": "人员3", "order_time": "2019.07.23 9:05", "booking_time": "2019.07.23 9:30"},
                {"older_name": "长者5", "age": 60, "support_type": "B9", "street": "镇街7", "service_type": "洗衣洗被",
                    "service_org": "机构5", "service_person": "人员4", "order_time": "2019.07.23 9:06", "booking_time": "2019.07.23 9:30"}
            ]

        # 居家养老-当天-居家服务商整体情况-方块
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang':
            return [
                {"name": "居家养老服务种类", 'value': ["30项", "↑ 6.5%"]},
                {"name": "居家服务机构总数", 'value': ["28家", "↑ 6.5%"]}
            ]

        # 居家养老-当天-居家服务商整体情况-右边-方块
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-right':
            return [
                {"name": "好评率", 'value': ["98%"]},
                {"name": "服务客户", 'value': ["2843"]},
                {"name": "累计派工", 'value': ["497430"]}
            ]

        # 居家养老-当天-居家服务商整体情况-Top10服务商订单数
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-Top10':
            return [
                {"name": "机构1", 'value': 90},
                {"name": "机构2", 'value': 80},
                {"name": "机构3", 'value': 70},
                {"name": "机构4", 'value': 75},
                {"name": "机构5", 'value': 60},
                {"name": "机构6", 'value': 50},
                {"name": "机构7", 'value': 40},
                {"name": "机构8", 'value': 30},
                {"name": "机构9", 'value': 20},
                {"name": "机构10", 'value': 10},
                {"name": "其他", 'value': 12}
            ]

        # 居家养老-当天-居家服务商整体情况-登记类型-饼图
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-dengjileix-pie':
            return [
                {"name": "工商登记", 'value': 0.48},
                {"name": "民非登记", 'value': 0.40},
                {"name": "事业单位", 'value': 0.12}
            ]

        # 居家养老-当天-居家服务商整体情况-评价情况-饼图
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-pingjiaqingkuang-pie':
            return [
                {"name": "5星", 'value': 0.47},
                {"name": "4星", 'value': 0.22},
                {"name": "3星", 'value': 0.20},
                {"name": "2星", 'value': 0.1},
                {"name": "1星", 'value': 0.01}
            ]

        # 居家养老-当天-居家服务商整体情况-人员类型-堆叠柱状图
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-renyuanleixing-bar':
            return [
                {"type": "院长", '持专业证': 1, '其他': 0},
                {"type": "医生", '持专业证': 10, '其他': 0},
                {"type": "护士", '持专业证': 16, '其他': 2},
                {"type": "护理员", '持专业证': 50, '其他': 50},
                {"type": "社工", '持专业证': 20, '其他': 10},
                {"type": "后期人员", '持专业证': 0, '其他': 80},
            ]

        # 居家养老-当天-居家服务商整体情况-专业情况-饼图
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-zhuangyeqingkuang-pie':
            return [
                {"name": "大专以下", 'value': 0.25},
                {"name": "大专", 'value': 0.40},
                {"name": "本科以上", 'value': 0.35}
            ]

        # 居家养老-当天-居家服务商整体情况-专业情况-饼图
        if command_id == 'jujiayanglaodangtian-jujiafuwushangzhengtiqingkuang-zhuangyeqingkuang-pie':
            return [
                {"name": "大专以下", 'value': 0.25},
                {"name": "大专", 'value': 0.40},
                {"name": "本科以上", 'value': 0.35}
            ]

        # 社区幸福院整体情况-桑基图
        if command_id == 'shequxingfuyuan-sankey':
            return [{
                "names": ["膳食", "其他", "文体活动", "亲朋活动", "教育", "桂城", "狮山", "西樵", "里水", "大沥", "九江", "丹灶"],
                "links":[
                    {
                        "source": "膳食",
                        'target': "桂城",
                        "value": "3"
                    },
                    {
                        "source": "膳食",
                        "target": '狮山',
                        "value": "3"
                    },
                    {
                        "source": "膳食",
                        "target": '西樵',
                        "value": "3"
                    },
                    {
                        "source": "其他",
                        "target": '狮山',
                        "value": "1"
                    },
                    {
                        "source": "其他",
                        "target": '西樵',
                        "value": "6"
                    },
                    {
                        "source": "其他",
                        "target": '里水',
                        "value": "2"
                    },
                    {
                        "source": "文体活动",
                        "target": '桂城',
                        "value": "2"
                    },
                    {
                        "source": "文体活动",
                        "target": '大沥',
                        "value": "1"
                    },
                    {
                        "source": "亲朋活动",
                        "target": '里水',
                        "value": "2"
                    },
                    {
                        "source": "亲朋活动",
                        "target": '九江',
                        "value": "1"
                    },
                    {
                        "source": "教育",
                        "target": '大沥',
                        "value": "1"
                    },
                    {
                        "source": "教育",
                        "target": '丹灶',
                        "value": "10"
                    },
                    {
                        "source": "教育",
                        "target": '九江',
                        "value": "1"
                    }
                ]
            }]

        if command_id == 'xingfuyuanzhengtiqingkuang-fuwuqingkuang-table':
            return [
                {
                    "name": "社区幸福院", 'street': "桂城",
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "active_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "name": "社区幸福院", 'street': "大沥",
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "active_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "name": "社区幸福院", 'street': "九江",
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "active_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "name": "社区幸福院", 'street': "桂城",
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "active_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                }
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-chengjie':
            return [
                {"name": "伯乐", 'value': 1},
                {"name": "春晖", 'value':  2},
                {"name": "颐康", 'value':  3},
                {"name": "谷丰", 'value': 4},
                {"name": "启正", 'value': 5},
                {"name": "大众", 'value': 6},
                {"name": "鵬星", 'value': 9},
                {"name": "北斗星", 'value': 3},
                {"name": "善缘", 'value': 4},
                {"name": "狮爱", 'value': 5},
                {"name": "心明爱", 'value': 8},
                {"name": "其他", 'value': 7},
            ]

        if command_id == 'shequxingfuyuan-sunburst':
            return [
                {
                    "name": '主管',
                    "value": 20,
                    "children": [
                        {
                            "name": '中级工程师',
                            "value": 15,
                        }
                    ]
                },
                {
                    "name": '专职人员',
                    "value": 60,
                    "children": [
                        {
                            "name": '持证人数',
                            "value": 20
                        }
                    ]
                }
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-shidian':
            return [
                {"name": "生活用品", 'value': 1},
                {"name": "洗浴用品", 'value':  2},
                {"name": "寝室用品", 'value':  3},
                {"name": "助行工具", 'value': 4},
                {"name": "居家适老化改造", 'value': 5},
            ]

        if command_id == 'xingfuyuanzhengtiqingkuang-xingfuyuangeshu':
            return [
                {"name": "大沥", 'value': 1},
                {"name": "狮山", 'value':  2},
                {"name": "里水", 'value':  3},
                {"name": "西樵", 'value': 4},
                {"name": "九江", 'value': 5},
                {"name": "丹灶", 'value': 5},
                {"name": "桂城", 'value': 5},
            ]

        if command_id == 'cishanguanli-zijinbili':
            return [
                {"name": "个人捐赠", 'value': 1},
                {"name": "企业捐赠", 'value':  2},
                {"name": "其他", 'value':  3},
            ]

        if command_id == 'cishanguanli-zhangzherenshu':
            return [
                {"name": "人力成本", 'value': 1},
                {"name": "设备更新", 'value':  2},
                {"name": "其他", 'value':  3},
            ]

        if command_id == 'xingfuyuanxiangqing-table':
            return [
                {
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "start_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "start_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "start_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                },
                {
                    "active_type": "教育", "active_address": "医疗保健室",
                    "sign_num": 45, "start_time": "2019-08-24",
                    "service_duration": "1h", "worker_num": 5,
                }
            ]

        if command_id == 'xingfuyuan-Graph':
            return [
                {"name": "多功能活动室"},
                {"name": "休息室"},
                {"name": "阅览室"},
                {"name": "医疗保健室"},
                {"name": "心理疏导室"},
                {"name": "康复训练室"}
            ]

        if command_id == 'xingfuyuanxiangqing-fuwuleixingcishu':
            return [
                {"name": "文体活动", 'value': 1},
                {"name": "亲朋活动", 'value':  2},
                {"name": "膳食", 'value':  3},
                {"name": "西樵", 'value': 4},
                {"name": "教育", 'value': 5},
                {"name": "其他", 'value': 5},
            ]

        if command_id == 'xingfuyuanxiangqing-chengjiaocishu':
            return [
                {"name": "生活用品", 'value': 1},
                {"name": "洗浴用品", 'value':  2},
                {"name": "寝室用品", 'value':  3},
                {"name": "助行工具", 'value': 4},
                {"name": "居家适老化改造", 'value': 5},
            ]

        if command_id == 'xingfuyuanxiangqing-pingjungoumainianling':
            return [
                {"name": "生活用品", 'value': 85},
                {"name": "洗浴用品", 'value':  90},
                {"name": "寝室用品", 'value':  65},
                {"name": "助行工具", 'value': 60},
                {"name": "居家适老化改造", 'value': 76},
            ]

        if command_id == 'xingfuyuanxiangqing-xingfuyuanjieshao-describe':
            return [
                {"name": "社区幸福院名", 'value': ["A幸福院"]},
                {"name": "所属镇街", 'value':  ["镇街1"]},
                {"name": "挂牌时间", 'value':  ["2019.01.01"]},
                {"name": "验收时间", 'value': ["2019.01.01"]},
                {"name": "建筑面积", 'value': ["76㎡"]},
                {"name": "建设方式", 'value': ["闲置物业改建"]},
                {"name": "地址", 'value': ["中控大厦"]},
                {"name": "星级情况", 'value': ["五星"]},
                {"name": "评比结果", 'value': ["90/（排第50名）"]},
            ]

        if command_id == 'xingfuyuanxiangqing-chengyunjigou':
            return [
                {"name": "承接运营机构", 'value': ["A机构"]},
                {"name": "管理人员", 'value':  ["A主管 社会工作师（13900000000）"]},
                {"name": "专职人员", 'value': [
                    "A专职 助理社工师（13900000000）", "B专职（13900000000）"]},
                {"name": "督导人员", 'value': ["何谋聪"]},
                {"name": "活动室数", 'value': ["12室"]},
                {"name": "日托床位数", 'value': ["10张"]},
                {"name": "联系方式", 'value': ["159131313126"]},
                {"name": "单位性质", 'value': ["办公"]},
            ]

        if command_id == 'jujiayanglaoxiangqing-zhangzhexinxi-describe':
            return [
                {"name": "长者姓名", 'value': ["何谋聪"]},
                {"name": "年龄（周岁）", 'value':  ["60"]},
                {"name": "性别", 'value': ["女"]},
                {"name": "所属镇街", 'value': ["镇街1"]},
                {"name": "居住地址", 'value': ["地址1"]},
                {"name": "服务地址", 'value': ["地址1", "地址2"]},
                {"name": "资助类型", 'value': ["A1类"]},
                {"name": "每月补贴金额（元）", 'value': ["500"]},
                {"name": "每月高龄金额（元）", 'value': ["0"]},
                {"name": "居家服务次数（次）", 'value': ["76"]},
            ]

        if command_id == 'fuwujigouqingkuang-jigouxinxi-describe':
            return [
                {"name": "服务机构", 'value': ["机构1"]},
                {"name": "注册所在地", 'value':  ["南海"]},
                {"name": "机构地址", 'value': ["地址2"]},
                {"name": "法人", 'value': ["法人1"]},
                {"name": "单位性质", 'value': ["民非登记"]},
                {"name": "联系电话", 'value': ["81234567"]},
                {"name": "整体评价", 'value': ["5星"]},
                {"name": "服务项目", 'value': ["20种"]},
                {"name": "服务区域", 'value': ["桂城街道", "狮山镇", "..."]},
            ]

        if command_id == 'jujiayanglaoxiangqing-fuwuleixingcishu':
            return [
                {"name": "卫生整洁", 'value': 30},
                {"name": "洗衣洗被", 'value':  20},
                {"name": "膳食", 'value': 10},
                {"name": "心理慰藉", 'value': 15},
                {"name": "其他", 'value': 26},
            ]

        if command_id == 'jujiayanglaoxiangqing-fuwujine-pie':
            return [
                {"name": "政府支付", 'value': 30},
                {"name": "社会捐助", 'value':  20},
                {"name": "家庭支出", 'value': 10},
            ]

        if command_id == 'jujiayanglaoxiangqing-fuwuxiangqing-table':
            return [
                {
                    "service_type": "膳食", "service_organization": "机构1",
                    "attendant": "人员1", "order_time": "2019-08-24"
                },
                {
                    "service_type": "卫生整洁", "service_organization": "机构2",
                    "attendant": "人员2", "order_time": "2019-08-24"
                },
                {
                    "service_type": "教育", "service_organization": "机构3",
                    "attendant": "人员3", "order_time": "2019-08-24"
                },
                {
                    "service_type": "膳食", "service_organization": "机构4",
                    "attendant": "人员4", "order_time": "2019-08-24"
                },
                {
                    "service_type": "卫生整洁", "service_organization": "机构5",
                    "attendant": "人员5", "order_time": "2019-08-24"
                },
                {
                    "service_type": "教育", "service_organization": "机构6",
                    "attendant": "人员6", "order_time": "2019-08-24"
                },
                {
                    "service_type": "洗衣洗被", "service_organization": "机构7",
                    "attendant": "人员7", "order_time": "2019-08-24"
                }
            ]

        if command_id == 'jujiayanglaoxiangqing-shuimianzhiliang-pie':
            return [
                {"name": "深睡", 'value': 30},
                {"name": "浅睡", 'value':  20},
                {"name": "快速眼动", 'value': 10},
                {"name": "清醒", 'value': 15},
            ]

        if command_id == 'jujiayanglaoxiangqing-fuwuleixingdingdanshu':
            return [
                {"name": "文体活动", 'value': 30},
                {"name": "亲朋活动", 'value':  20},
                {"name": "膳食", 'value': 10},
                {"name": "教育", 'value': 15},
                {"name": "其他", 'value': 15},
            ]

        if command_id == 'jujiafuwuzhuangtai-zhangzherenshu-pie':
            return [
                {"name": "当天订单", 'value': 67},
                {"name": "暂无需求", 'value':  33},
            ]

        if command_id == 'jujiafuwuzhuangtai-fuwuxiangqing-table':
            return [
                {
                    "older": "膳食", "service_organization": "机构1",
                    "attendant": "人员1", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "卫生整洁", "service_organization": "机构2",
                    "attendant": "人员2", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "教育", "service_organization": "机构3",
                    "attendant": "人员3", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "膳食", "service_organization": "机构4",
                    "attendant": "人员4", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "卫生整洁", "service_organization": "机构5",
                    "attendant": "人员5", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "教育", "service_organization": "机构6",
                    "attendant": "人员6", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                },
                {
                    "older": "洗衣洗被", "service_organization": "机构7",
                    "attendant": "人员7", "order_time": "2019-08-24",
                    "age": 67, "subsidize_type": "A1",
                    "street": "镇街1", "service_duration": "1h"
                }
            ]

        if command_id == 'fuwujigouqingkuang-fuwudanshu':
            return [
                {"name": "文体活动", '服务类型次数': 67, '全区平均': 33},
                {"name": "亲朋活动",  '服务类型次数': 56, '全区平均': 21},
                {"name": "膳食",  '服务类型次数': 10, '全区平均': 43},
                {"name": "教育",  '服务类型次数': 32, '全区平均': 12},
                {"name": "其他",  '服务类型次数': 11, '全区平均': 41},
            ]

        if command_id == 'fuwujigouqingkuang-fuwuleixingcishu':
            return [
                {"name": "文体活动", '服务类型平均时长': 67, '全区平均': 33},
                {"name": "亲朋活动",  '服务类型平均时长': 56, '全区平均': 21},
                {"name": "膳食",  '服务类型平均时长': 10, '全区平均': 43},
                {"name": "教育",  '服务类型平均时长': 32, '全区平均': 12},
                {"name": "其他",  '服务类型平均时长': 11, '全区平均': 41},
            ]

        if command_id == 'fuwujigouqingkuang-xuelirenshu-pie':
            return [
                {"name": "大专", 'value': 67},
                {"name": "大专以上",  'value': 56},
                {"name": "本科以上",  'value': 10},
            ]

        if command_id == 'yanglaojigouxiangqing-monitor':
            return [
                {"title": "东门监控 1",
                    'img_src': "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"},
                {"title": "东门监控 2",
                    'img_src': "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"},
                {"title": "东门监控 1",
                    'img_src': "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"},
                {"title": "东门监控 2",
                    'img_src': "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"},
                {"title": "西门监控",  'img_src': "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"},
            ]

        if command_id == 'xingfuyuanxiangqing-carousel':
            return [
                {
                    "carousel_img":
                    [
                        "http://pic32.nipic.com/20130813/3347542_160503703000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic32.nipic.com/20130813/3347542_160503703000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ],
                    "preview":
                    [
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ]
                }
            ]
        if command_id == 'jujiayanglaoxiangqing-carousel':
            return [
                {
                    "carousel_img":
                    [
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ],
                    "preview":
                    [
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ]
                }
            ]

        if command_id == 'yanglaojigouxiangqing-carousel':
            return [
                {
                    "carousel_img":
                    [
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ],
                    "preview":
                    [
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg",
                        "http://pic31.nipic.com/20130801/11604791_100539834000_2.jpg"
                    ]
                }
            ]
