from ...service.welfare_institution.other_cost import OtherCostService
from ...service.buss_pub.bill_manage import TypeId
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    other_cost = OtherCostService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICostService.update_other_cost')
    def __update_other_cost(condition):
        '''新增/编辑其他费用'''
        permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.other_cost, permission_name)
        res = other_cost.update_other_cost(condition)
        other_cost.bill_manage_server.insert_logs(
            '机构养老', '其他费用', '新增/编辑其他费用')
        return res

    @jsonrpc.method('ICostService.get_other_cost')
    def __get_other_cost(condition, page=None, count=None):
        '''其他费用列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_all, PermissionName.query)
        return other_cost.get_other_cost(permission_data, condition, page, count)

    @jsonrpc.method('ICostService.delete_other_cost')
    def ___delete_other_cost(condition):
        '''删除其他费用'''
        security_service.judge_permission_other(
            FunctionName.other_cost, PermissionName.delete)
        res = other_cost.delete_other_cost(condition)
        other_cost.bill_manage_server.insert_logs(
            '机构养老', '其他费用', '删除其他费用')
        return res

    @jsonrpc.method('ICostService.update_cost_type')
    def __update_cost_type(condition):
        '''新增/编辑费用类型'''
        permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.cost_type, permission_name)
        res = other_cost.update_cost_type(condition)
        other_cost.bill_manage_server.insert_logs(
            '机构养老', '其他费用', '新增/编辑其他费用')
        return res

    @jsonrpc.method('ICostService.get_cost_type')
    def __get_other_cost_list(condition, page=None, count=None):
        '''费用类型列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.cost_type, PermissionName.query)
        return other_cost.get_cost_type(permission_data, condition, page, count)

    @jsonrpc.method('ICostService.delete_cost_type')
    def __delete_other_cost(ids):
        '''删除费用类型'''
        security_service.judge_permission_other(
            FunctionName.cost_type, PermissionName.delete)
        other_cost.bill_manage_server.insert_logs(
            '机构养老', '其他费用', '删除其他费用')
        return other_cost.delete_cost_type(ids)
