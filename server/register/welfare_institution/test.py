'''
@Author: your name
@Date: 2019-09-27 18:47:24
@LastEditTime: 2019-12-06 12:34:39
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\test.py
'''
from server.pao_python.pao.service.data.mongo_db import MongoService, MongoFilter, C, N, F, as_date
from ...service.buss_pub.bill_manage import BillManageService, OperationType, Status, TypeId
import pandas as pd
from server.pao_python.pao.data import dataframe_to_list, process_db
from ...service.mongo_bill_service import MongoBillFilter
from ...service.common import get_random_id, get_cur_time, insert_many_data, delete_data, GetInformationByIdCard, get_str_to_age
import uuid
from ...service.buss_mis.allowance_manage import AllowanceManageService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    allowance_manage_func = AllowanceManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    service = ServiceRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('Test.add_role_person')
    def __get_check_in_list(person, set_role_user, role, permission_list):
      # print('用户数据:', person)
      # print('角色-用户设置:', set_role_user)
      # print('角色数据:', role)
      # print('权限列表:', permission_list)
        res = service.add_person_role(
            person, set_role_user, role, permission_list)
        return res

    @jsonrpc.method('Test.happy_rate')
    def __get_happy_rate(condition):
        '''幸福院评比'''
        res = service.happy_rate(condition)
        return res

    @jsonrpc.method('Test.happy_build_process')
    def __happy_build_process(condition):
        '''幸福院建设情况'''
        res = service.happy_build_process(condition)
        return res

    @jsonrpc.method('Test.dishes_list')
    def __dishes_list():
        '''养老系统_餐饮-菜品---MZ_CYCP2'''
        res = service.dishes_list()
        return res

    @jsonrpc.method('Test.check_sign_in')
    def __check_sign_in():
        '''养老系统_入住-签约服务---MZ_RZQYFW2'''
        res = service.check_sign_in()
        return res

    @jsonrpc.method('Test.subsidies_item')
    def __subsidies_item():
        '''智慧养老平台_补贴项目---MZ_BTXM2'''
        res = service.subsidies_item()
        return res

    @jsonrpc.method('Test.check_bed')
    def __subsidies_item():
        '''智慧养老平台_床位---MZ_CW2'''
        res = service.check_bed()
        return res

    @jsonrpc.method('Test.get_product')
    def __get_product():
        '''智慧养老平台_服务套餐与服务类型关系'''
        res = service.get_product()
        return res

    @jsonrpc.method('Test.get_service_item')
    def __get_service_item():
        '''智慧养老平台_服务套餐与服务类型关系'''
        res = service.get_service_item()
        return res

    @jsonrpc.method('Test.get_activity_list')
    def __get_activity_list():
        '''智慧养老平台_活动-活动收费---MZ_HDHDSF2'''
        res = service.get_activity_list()
        return res

    @jsonrpc.method('Test.get_zone_list')
    def __get_zone_list():
        '''智慧养老平台_基础数据-房间信息---MZ_JCSJFJXX2'''
        res = service.get_zone_list()
        return res

    @jsonrpc.method('Test.get_shop_list')
    def __get_shop_list():
        '''智慧养老平台_基础数据-商品信息---MZ_JCSJSPXX2'''
        res = service.get_shop_list()
        return res

    @jsonrpc.method('Test.get_org_list')
    def __get_org_list():
        '''智慧养老平台_机构运营情况记录---MZ_JGYYQKJL2'''
        res = service.get_org_list()
        return res

    @jsonrpc.method('Test.get_app_order')
    def __get_app_order():
        '''智慧养老平台_app服务订单---MZ_FWDD2'''
        res = service.get_app_order()
        return res

    @jsonrpc.method('Test.get_follow_org')
    def __get_follow_org():
        '''智慧养老平台_App用户关注机构---MZ_YHGZJG2'''
        res = service.get_follow_org()
        return res

    @jsonrpc.method('Test.get_app_user')
    def __get_app_user():
        '''智慧养老平台_app用户---MZ_YH2'''
        res = service.get_app_user()
        return res

    @jsonrpc.method('Test.get_bed_user')
    def __get_app_user(condition):
        '''智慧养老平台_app用户---MZ_YH2'''
        res = service.get_bed_user(condition)
        return res

    @jsonrpc.method('Test.old_allowence')
    def __get_old_allowence(condition):
        res = service.old_allowence(condition)
        return res

    @jsonrpc.method('Test.insert_old_allowance')
    def __insert_old_allowance(condition):
        res = service.insert_old_allowance(condition)
        return res

    @jsonrpc.method('Test.import_old_list_del')
    def __import_old_list_del(condition):
        res = service.import_old_list_del(condition)
        allowance_manage_func.update_statistics()
        return res

    @jsonrpc.method('Test.add_gov_data')
    def __add_gov_data(condition):
        res = service.add_gov_data(condition)
        return res

    @jsonrpc.method('Test.add_platform_data')
    def __add_platform_data(condition):
        res = service.add_platform_data(condition)
        return res

    @jsonrpc.method('Test.deal_status_old')
    def __deal_status_old(condition):
        res = service.deal_status_old(condition)
        return res


class ServiceRecordService(MongoService):
    ''' 服务记录 '''

    def __init__(self, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
        self.db_addr = db_addr
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pwd = db_pwd
        self.inital_password = inital_password
        self.session = session
        self.bill_manage_server = BillManageService(
            self.db_addr, self.db_port, self.db_name, self.db_user, self.db_pwd, self.inital_password, self.session)

    def add_person_role(self, person, set_role_user, role, permission_list):
        bill_id = self.bill_manage_server.add_bill(OperationType.add.value, TypeId.user.value, [
                                                   person, set_role_user, role, permission_list], ['PT_User', 'PT_Set_Role', 'PT_Role', 'PT_Permission'])
        return '新增成功' if bill_id else '新增失败'

    def happy_rate(self, condition):
        pf = pd.DataFrame(condition)
        pf.rename(columns={'排名': 'rank',
                           '幸福院名称': "name",
                           '运营管理': 'operation_score',
                           '业务成效': 'business_score',
                           '财务管理': 'financial_score',
                           '品牌建设': 'brand_build_score',
                           '安全管理': 'safety_score',
                           '社会评价': 'social_evaluation_score',
                           '总分': 'total_score',
                           '年份': 'year',
                           '评定': "assessment"}, inplace=True)
        name = pf['name'].values.tolist()
        pf['assessment'] = pf.apply(
            lambda x: self._get_assessment(x.total_score), axis=1)
        # 检查是否存在组织机构
        _filter_org = MongoBillFilter()
        _filter_org.match_bill(C('personnel_type') == '2')\
                   .project({'_id': 0, 'name': 1})
        org = self.query(_filter_org, 'PT_User')
        org_pf = pd.DataFrame(org)
        org_list = org_pf['name'].values.tolist()
        not_exit = []  # 不存在的机构
        for x in name:
            if not x in org_list:
              # print('我进来了', x)
                not_exit.append(x)
        # 导入数据处理
        _filter = MongoBillFilter()
        _filter.match_bill(C('name').inner(name)) \
            .add_fields({'organzation_id': '$id'})\
            .project({'_id': 0, 'organzation_id': 1, 'name': 1})
        res = self.query(_filter, 'PT_User')
        user_pf = pd.DataFrame(res)
        new_pf = pd.merge(pf, user_pf, on=['name'])
        new_pf = new_pf[['organzation_id', 'year',
                         'rank', 'operation_score', 'business_score', 'financial_score', 'brand_build_score', 'safety_score', 'social_evaluation_score', 'total_score', 'assessment']]
        new_pf['id'] = new_pf.apply(lambda x: get_random_id(), axis=1)

        def process_func(db):
            nonlocal new_pf
            new_res = dataframe_to_list(new_pf)
            for i, x in enumerate(new_res):
                new_res[i]['create_date'] = get_cur_time()
            insert_many_data(db, 'BS_Happy_House_Rate', new_res)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return str(not_exit) + '机构不存在' if len(not_exit) else '导入成功'

    def happy_build_process(self, condition):
        pf = pd.DataFrame(condition)
        pf.rename(columns={'镇街': 'town', '幸福院间数': "total",
                           '已建成数量': 'completed_total'}, inplace=True)
        res = dataframe_to_list(pf)
        for i, x in enumerate(res):
            res[i]['create_date'] = get_cur_time()

        def process_func(db):
            insert_many_data(db, 'BS_Happy_Build_Progress', res)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        return '导入成功'

    def _get_assessment(self, score):
        if score >= 85:
            return '优良'
        elif score >= 80:
            return '合格'
        else:
            return '不合格'

    ###################数政局数据提供#############################

    def dishes_list(self):
        _filter = MongoBillFilter()
        _filter.unwind('data')\
            .add_fields({'new': self.ao.object_to_array('$data')})\
            .unwind('new')\
            .add_fields({'meals': '$new.v'})\
            .match_bill(
                (C('meals') != '无')
                & (C('meals').nin(['0', '1', '2', '3', '4', '5', '6', '主食', '副主食', '主菜', '副主菜', '配菜']))
        )\
            .add_fields({
                '名称': '$meals',
                '类型': '$type',
                '创建时间': '$create_date',
                '修改时间': '$modify_date',
                '唯一值': '$id'
            })\
            .group({'名称': '$名称', '类型': '$类型'}, [
                {'创建时间': self.ao.first('$create_date')},
                {
                    '修改时间': self.ao.first('$modify_date'),
                }, {
                    '唯一值': self.ao.first('$id')}
            ])\
            .project({'_id': 0, '创建时间': 1, '修改时间': 1, '名称': 1, '类型': 1, '唯一值': 1})

        res = self.query(_filter, 'PT_Elder_Meals')
        return res

    def dishes_list_2(self):
        _filter = MongoBillFilter()
        _filter.unwind('data')\
            .add_fields({'new': self.ao.object_to_array('$data')})\
            .unwind('new')\
            .add_fields({'meals': '$new.v'})\
            .match_bill(
                (C('meals') != '无')
                & (C('meals').nin(['0', '1', '2', '3', '4', '5', '6', '主食', '副主食', '主菜', '副主菜', '配菜']))
        )\
            .add_fields({
                '餐品': '$meals',
                '餐饮类型': '$type',
                '创建时间': '$create_date',
                '修改时间': '$modify_date',
                '唯一值': '$id'
            })\
            .group({'餐品': '$餐品', '餐饮类型': '$餐饮类型'}, [
                {'创建时间': self.ao.first('$create_date')},
                {
                    '修改时间': self.ao.first('$modify_date'),
                }, {
                    '唯一值': self.ao.first('$id')}
            ])\
            .project({'_id': 0, '创建时间': 1, '修改时间': 1, '餐品': 1, '餐饮类型': 1, '唯一值': 1})

        res = self.query(_filter, 'PT_Elder_Meals')
        return res

    def check_sign_in(self):
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User', 'user_id', 'id', 'user')\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .match_bill(
                (C('org.personnel_type') == '2')
                & (C('org.organization_info.personnel_category') == '福利院')
        )\
            .add_fields({
                '唯一值': '$id',
                '入住人': '$user.name',
                '住宿记录表ID': '$accommodation_id',
                '订单ID': '$order_id',
                '状态': '$state',
                '所属组织': '$org.name'
            })\
            .project({'_id': 0, '唯一值': 1, '入住人': 1, '住宿记录表ID': 1, '订单ID': 1, '状态': 1, '所属组织': 1})
        res = self.query(_filter, 'IEC_Check_In')
      # print("入住人信息>>>>>>>>>>>")
      # print(_filter.filter_objects)
      # print(res)
        return res

    def subsidies_item(self):
        _filter = MongoBillFilter()
        _filter.match_bill(C("is_allowance") == '1')\
            .add_fields({
                '名称': '$name',
                '唯一值': '$id',
                '创建时间': '$modify_date',
                '备注': '',
                '补贴模版': '',
                '条款文件': ''
            })\
            .project({'_id': 0, '名称': 1, '唯一值': 1, '创建时间': 1, '备注': 1, '补贴模版': 1, '条款文件': 1})
        res = self.query(_filter, 'PT_Service_Item')
      # print("补贴项目>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    # 智慧养老平台_床位---MZ_CW2
    def check_bed(self):
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_Hotel_Zone', 'area_id', 'id', 'zone')\
            .lookup_bill('PT_User', 'residents_id', 'id', 'user')\
            .lookup_bill("PT_Service_Product", 'service_item_package_id', 'id', 'product')\
            .add_fields({
                '唯一值': '$id',
                '区域ID': '$area_id',
                '名称': "$name",
                '床位编号': "$bed_code",
                '备注': '$remark',
                '服务包': '$product.name',
                '入住人': "$user.name"
            })\
            .project({"_id": 0, '唯一值': 1, '区域ID': 1, '名称': 1, '床位编号': 1, '备注': 1, '服务包': 1, '入住人': 1})
        res = self.query(_filter, 'PT_Bed')
      # print('床位信息>>>>>>>>>>>>')
      # print(_filter.filter_objects)
        return res

    def get_product(self):
        _filter = MongoBillFilter()
        _filter.match_bill(C("is_service_item") != 'true')\
            .add_fields({
                '唯一值': '$id',
                '创建时间': '$create_date',
                '修改时间': '$modify_date',
                '服务套餐ID': "$id",
                '服务类型ID': '',
                '关系名称': '',
            })\
            .project({'_id': 0, '唯一值': 1,
                      '创建时间': 1,
                      '修改时间': 1,
                      '服务套餐ID': 1,
                      '服务类型ID': 1,
                      '关系名称': 1, })
        res = self.query(_filter, 'PT_Service_Product')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_service_item(self):
        '''智慧养老平台_服务项目---MZ_FWXM2'''
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .inner_join_bill('PT_Service_Type', 'item_type', 'id', 'item_type')\
            .add_fields({
                '名称': "$name",
                '组织机构': '$org.name',
                '计价公式': '$valuation_formula.formula',
                '项目类型': '$item_type.name',
                '唯一值': '$id',
                '项目介绍地址': '',
                '应用': '',
                '合同条款': '',
                '服务选项': '$service_option.name'
            })\
            .project({'_id': 0,
                      '名称': 1,
                      '组织机构': 1,
                      '计价公式': 1,
                      '项目类型': 1,
                      '唯一值': 1,
                      '项目介绍地址': 1,
                      '应用': 1,
                      '合同条款': 1,
                      '服务选项': 1})
        res = self.query(_filter, 'PT_Service_Item')
      # print(">>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_activity_list(self):
        _filter = MongoBillFilter()
        _filter.add_fields({
            '唯一值': '$id',
            '创建时间': '$create_date',
            '修改时间': '$modify_date',
            '活动ID': '$id',
            '费用': 0,
        })\
            .project({'_id': 0,
                      '唯一值': 1,
                      '创建时间': 1,
                      '修改时间': 1,
                      '活动ID': 1,
                      '费用': 1, })
        res = self.query(_filter, 'PT_Activity')
      # print(">>>>>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_zone_list(self):
        _filter = MongoBillFilter()
        _filter.inner_join_bill('PT_Hotel_Zone_Type', 'hotel_zone_type_id', 'id', 'hotel_type')\
            .match_bill(C('hotel_type.name') == '房')\
            .inner_join_bill('PT_Hotel_Zone', 'upper_hotel_zone_id', 'id', 'area')\
            .add_fields({
                '唯一值': '$id',
                '房间名称': '$zone_name',
                '房间编号': '$zone_number',
                '类型': '$hotel_type.name',
                '备注': '$remark',
                '所属区域': '$area.zone_name',
            }).project({
                '_id': 0,
                '唯一值': 1,
                '房间名称': 1,
                '房间编号': 1,
                '类型': 1,
                '备注': 1,
                '所属区域': 1,
            })
        res = self.query(_filter, 'PT_Hotel_Zone')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_shop_list(self):
        _filter = MongoBillFilter()
        _filter.match_bill(C("is_service_item") != 'true')\
            .inner_join_bill('PT_User', 'organization_id', 'id', 'org')\
            .add_fields({
                '唯一值': '$id',
                '单价': '$service_package_price',
                '名称': '$name',
                '备注': "",
                '所属机构': '$org.name',
                '单位': '',
            })\
            .project({'_id': 0,
                      '唯一值': 1,
                      '单价': 1,
                      '名称': 1,
                      '备注': 1,
                      '所属机构': 1,
                      '单位': 1,
                      })
        res = self.query(_filter, 'PT_Service_Product')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_org_list(self):
        _filter = MongoBillFilter()
        _filter.add_fields({
            '唯一值': '$id',
            '创建时间': '$create_date',
            '修改时间': '$modify_date',
            '机构ID': '$id',
            '运营记录': ''
        }).project({'_id': 0,
                    '唯一值': 1,
                    '创建时间': 1,
                    '修改时间': 1,
                    '机构ID': 1,
                    '运营记录': 1
                    })
        res = self.query(_filter, 'PT_User')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_app_order(self):
        _filter = MongoBillFilter()
        _filter.match_bill(C('organization_info.personnel_category') == '服务商')\
            .inner_join_bill('PT_Service_Order', 'id', 'service_provider_id', 'order')\
            .inner_join_bill('PT_Service_Record', 'order.id', 'order_id', 'recode_list')\
            .inner_join_bill('PT_User', 'recode_list.servicer_id', 'organization_id', 'worder')\
            .add_fields({
                '唯一值': '$id',
                '服务商': '$name',
                '服务人员': '$worder.name',
                '订单日期': '$create_date',
                '服务时间': '$start_date',
                '支付时间': '$order_date',
                '备注': '',
                '订单编号': '$order_code',
                '服务项目': '$order.detail.product_name',
            })\
            .project({
                "_id": 0,
                '唯一值': 1,
                '服务商': 1,
                '服务人员': 1,
                '订单日期': 1,
                '服务时间': 1,
                '支付时间': 1,
                '备注': 1,
                '订单编号': 1,
                '服务项目': 1,
            })

        res = self.query(_filter, 'PT_User')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_follow_org(self):
        _filter = MongoBillFilter()
        _filter.add_fields({
            '唯一值': '$id',
            '创建时间': '$create_date',
            '修改时间': '$modify_date',
            '机构ID': '$business_id',
            '用户ID': '$user_id',
        })\
            .project({
                "_id": 0,
                '唯一值': 1,
                '创建时间': 1,
                '修改时间': 1,
                '机构ID': 1,
                '用户ID': 1,
            })
        res = self.query(_filter, 'PT_Service_Follow_Collection')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_app_user(self):
        _filter = MongoBillFilter()
        _filter.match_bill(C('login_info') != [])\
            .add_fields({
                '唯一值': '$id',
                '身份证': '$id_card',
                '证件类型': '$id_card_type',
                '名称': '$name',
                '电话': '$personnel_info.telephone',
                '人员类型': '$personnel_info.personnel_category',
                '简介': '',
                '地址': '$personnel_info.address',
                '人员其他信息': ''
            })\
            .project({
                '_id': 0,
                '唯一值': 1,
                '身份证': 1,
                '证件类型': 1,
                '名称': 1,
                '电话': 1,
                '人员类型': 1,
                '简介': 1,
                '地址': 1,
                '人员其他信息': 1
            })
        res = self.query(_filter, 'PT_User')
      # print(">>>>>>>>>>>>>")
      # print(_filter.filter_objects)
        return res

    def get_bed_user(self, condition):
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('organization_id') == condition['org_id'])
            & ((C('residents_id') != '') & (C('residents_id') != None))
        )\
            .project({'_id': 0, 'residents_id': 1})
        res = self.query(_filter, 'PT_Bed')
        print('床位入住人数>', len(res))
        # pd_res = pd.DataFrame(res)
        # residents_ids = pd_res['residents_id'].tolist()
        _filter_check = MongoBillFilter()
        _filter_check.match_bill(
            (C('organization_id') == condition['org_id'])
        )\
            .project({'_id': 0, 'id': 1})
        res_check = self.query(_filter_check, 'PT_User')
        print('长者表里的人数>', len(res_check))
        _filter_check2 = MongoBillFilter()
        _filter_check2.match_bill(
            (C('organization_id') == condition['org_id'])
            & (C('state') == '居住中')
        )\
            .project({'_id': 0, 'id': 1})
        res_check2 = self.query(_filter_check2, 'IEC_Check_In')
        print('入住人数', len(res_check2))
        has_id = []
        is_exit_id = []
        pd_check = pd.DataFrame(res_check)
        user_ids = pd_check['id'].tolist()
        for x in res:
            if x['residents_id'] not in user_ids:
                is_exit_id.append(x['residents_id'])
            else:
                has_id.append(x['residents_id'])
            # if x['residents_id'] not in has_id:
            #     has_id.append(x['residents_id'])
            # else:
            #     is_exit_id.append(x['residents_id'])
        print('has_id>>>>>>>>', len(has_id))
        print('is_exit_id>>>>>', is_exit_id)

    def old_allowence(self, condition):
        # 查询所有名单
        _filter_old = MongoBillFilter()
        _filter_old.project({'_id': 0, 'id_card_num': 1})
        res_old = self.query(_filter_old, 'PT_Old_Age_List')
        pf_old = pd.DataFrame(res_old)
        old_id_cards = pf_old['id_card_num'].tolist()
        # 查询已认证名单
        _filter = MongoBillFilter()
        _filter.match_bill(C("bill_status") == "valid")\
            .project({'_id': 0, 'id_card_num': 1})
        res = self.query(_filter, 'PT_Old_Age_Allowance')
        pf_user = pd.DataFrame(res)
        id_cards = pf_user['id_card_num'].tolist()
        allowance_data = []
        old_data = []
        for i, x in enumerate(condition):
            # 拼接新增认证名单
            if x['id_card_num'] not in id_cards:
                allowance_data.append({**condition[i],
                                       'flag': '2020年下半年高龄津贴认证名单 - 新',
                                       'apply_status': '认证成功',
                                       'photo': []
                                       })
            # 拼接新增名单
            if x['id_card_num'] not in old_id_cards:
                old_data.append({
                    "id": str(uuid.uuid1()),
                    "town_street": x['town_street'],
                    "village": x['village'],
                    "name": x['name'],
                    "sex": x['sex'],
                    "id_card_num": x['id_card_num'],
                    "age": x['age'],
                    "bank_card_num": x['bank_card_num'],
                    "amount": x['amount'],
                    "replacement_amount": x['replacement_amount'],
                    "total_amount": x['total_amount'],
                })
        if len(allowance_data):
            print('>>>>>>新增的认证数据')
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.user.value, allowance_data, ['PT_Old_Age_Allowance'])
        if len(old_data):
            print("新增的名单数据>>>", old_data)

            def process_func(db):
                insert_many_data(db, 'PT_Old_Age_List', old_data)
            process_db(self.db_addr, self.db_port, self.db_name,
                       process_func, self.db_user, self.db_pwd)
            # bill_id = self.bill_manage_server.add_bill(
            #     OperationType.add.value, TypeId.user.value, old_data, ['PT_Old_Age_List'])
        return 'Success'

    def import_old_list_del(self, condition):
        print('文件数据', len(condition))
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号码'].tolist()
        data = []
        for x in id_cards:
            x = x.replace(' ', '')
            x = x.replace('’', '')
            data.append(x)
        new_condition = {'id_card_num': {
            "$in": data}}

        def process_func(db):
            _filter_list = MongoBillFilter()
            _filter_list.match(C('id_card_num').inner(data)) \
                        .project({'_id': 0})
            res_old = self.query(_filter_list, 'PT_Old_Age_List')
            print('查询到要删除的名单', len(res_old))
            print(res_old)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.user.value, res_old, ['PT_Old_Age_List_2'])
            delete_data(db, 'PT_Old_Age_List', new_condition)
        process_db(self.db_addr, self.db_port, self.db_name,
                   process_func, self.db_user, self.db_pwd)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id_card_num').inner(data))
        )\
            .project({'_id': 0, 'id': 1, 'id_card_num': 1, 'name': 1})
        res = self.query(_filter, 'PT_Old_Age_Allowance')
        print('要删除的已认证数据', len(res))
        if len(res):
            # pass
            bill_id = self.bill_manage_server.add_bill(
                OperationType.delete.value, TypeId.user.value, res, ['PT_Old_Age_Allowance'])
            return bill_id

    def insert_old_allowance(self, condition):
        res = ''
        _filter_old = MongoBillFilter()
        _filter_old.match_bill(
            (C('create_date') >= as_date('2020-01-01'))
            & (C('id_card_num') == condition['id_card_num']))\
            .project({'_id': 0})
        res_old = self.query(_filter_old, 'PT_Old_Age_Allowance')
        if not len(res_old):
            _filter = MongoBillFilter()
            _filter.match(C('id_card_num') == condition['id_card_num'])\
                .add_fields({'apply_status': '认证成功'})\
                .project({'_id': 0, 'town_street': 1, 'village': 1, 'name': 1, 'sex': 1, 'id_card_num': 1, 'bank_card_num': 1, 'amount': 1, 'replacement_amount': 1, 'total_amount': 1, 'organization_id': 1, 'apply_status': 1})
            res_old = self.query(_filter, 'PT_Old_Age_List')
            print('找到长者名单', res_old)
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.user.value, res_old, 'PT_Old_Age_Allowance')
            if bill_id:
                res = '完成认证'
            else:
                res = '认证失败'
        else:

            res = '已认证，请勿重复'
        print('处理结果>>>>>', res)
        return res

    def get_old_list(self, condition):
        print('文件数据', len(condition))
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号码'].tolist()
        print("身份证号码", len(id_cards))
        print(id_cards)
        _filter = MongoBillFilter()
        _filter.match(
            (C('id_card_num').inner(id_cards))
        )\
            .project({'_id': 0})
        res = self.query(_filter, 'PT_Old_Age_List')
        print('导出数据>', len(res))
        bill_id = self.bill_manage_server.add_bill(
            OperationType.add.value, TypeId.user.value, res, 'PT_Old_Age_List_2')
        return bill_id

    def get_del_allowance(self, condition):
        print('文件数据', len(condition))
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号码'].tolist()
        print("身份证号码", len(id_cards))
        print(id_cards)
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C('id_card_num').inner(id_cards))
        )\
            .project({'_id': 0, 'id_card_num': 1})
        res = self.query(_filter, 'PT_Old_Age_Allowance')
        print('查询已经被删除的认证数据>>>', len(res))
        print(res)
        # bill_id = self.bill_manage_server.add_bill(
        #     OperationType.add.value, TypeId.user.value, res, 'PT_Old_Age_Allowance_2')
        # return bill_id

    def add_gov_data(self, condition):
        # 导入的长者数据
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号'].tolist()
        print('我到这里了')
        # 获取平台中缺失的长者，补充进平台中
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C("bill_status") == "valid")
            & (C("personnel_info.die_state") == '健在')
            & (C("personnel_info.personnel_category") == "长者")) \
            .project({'_id': 0, 'id_card': 1, 'name': 1})
        platform_old = self.query(_filter, 'PT_User')
        print('查完平台数据')
        over_70_old = []
        for i, x in enumerate(platform_old):
            id_card = x['id_card']
            # print('第', i, '个长者')
            # print(x)
            # print('进来了1', id_card)
            if len(id_card) == 18:
                older = GetInformationByIdCard(id_card)
                age = older.get_age()
                # print('进来了3')
                # 统计不在名单内且超过了70岁的平台中的长者
                if age >= 70 and (id_card not in id_cards):
                    over_70_old.append(x)
        print('平台超过70岁的长者数量', len(over_70_old))
        return over_70_old

    def deal_status_old(self, condition):
        '''更新平台长者生存状态'''
        # 导入的长者数据
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号'].tolist()
        data = []
        for x in id_cards:
            x = x.replace(' ', '')
            x = x.replace('’', '')
            data.append(x)
        _filter = MongoBillFilter()
        _filter.match_bill(C('id_card').inner(data)) \
            .project({'_id': 0})
        res = self.query(_filter, 'PT_User')
        print('逝世长者数量', len(res))
        for i, x in enumerate(res):
            res[i]['personnel_info']['die_state'] = '已故'
            res[i]['personnel_info']['flag'] = '修改'
        bill_id = self.bill_manage_server.add_bill(
            OperationType.update.value, TypeId.user.value, res, 'PT_User')
        return bill_id

    def add_platform_data(self, condition):
        '''补充平台的长者数据'''
        # 导入的长者数据
        pf_user = pd.DataFrame(condition)
        id_cards = pf_user['身份证号'].tolist()
        # 获取平台中缺失的长者，补充进平台中
        _filter = MongoBillFilter()
        _filter.match_bill(
            (C("bill_status") == "valid")
            & (C("personnel_info.die_state") != "已故")
            & (C("personnel_info.personnel_category") == "长者"))\
            .project({'_id': 0, 'id_card': 1})
        platform_old = self.query(_filter, 'PT_User')
        platform_old_pf = pd.DataFrame(platform_old)
        platform_id_cards = platform_old_pf['id_card'].tolist()
        new_old_id = []
        # 对比找出平台中没有的长者数据
        for x in id_cards:
            if x not in platform_id_cards:
                new_old_id.append(x)
        _filter_twon = MongoBillFilter()
        _filter_twon.match_bill(C("bill_status") == "valid")\
                    .project({"_id": 0, "id": 1, 'name': 1})
        res_twon = self.query(_filter_twon, 'PT_Administration_Division')

        if len(new_old_id):
            new_old_data = []
            for i, y in enumerate(condition):
                print('第', i, '个长者')
                print(y)
                if y['身份证号'] in new_old_id:
                    print('哈哈哈1')
                    for j, z in enumerate(res_twon):
                        # print('>>>>>', y['村'])
                        if y['村'] in z['name']:
                            # print('哈哈哈2')
                            y['admin_area_id'] = z['id']
                    # print('我在这里1')
                    id_card = y['身份证号']
                    name = y['姓名']
                    older = GetInformationByIdCard(id_card)
                    # print('我在这里2')
                    new_old_data.append({
                        'name': y['姓名'],
                        'id_card': y['身份证号'],
                        "id_card_type": "身份证号",
                        "organization_id": "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                        "personnel_type": "1",
                        "admin_area_id": y['admin_area_id'] if y.get('admin_area_id') else '',
                        "flag": '从高龄津贴补充',
                        "login_info": [

                        ],
                        "personnel_info": {
                            'name': y['姓名'],
                            "personnel_classification": '271beaa8-1c8f-11ea-b623-144f8a6221df',
                            'id_card': y['身份证号'],
                            'sex': y['性别'],
                            'date_birth': as_date(older.get_birthday()),
                            "personnel_category": "长者",
                            "role_id": "8ed260f6-e355-11e9-875e-a0a4c57e9ebe",
                            "personnel_classification": "",
                            "telephone": "",
                            "address": "",
                            "die_state": '健在',
                            "is_alive": True,
                        }
                    })
                    # print('我在这里3')

            # print(new_old_data)
            # print('新增长者数据', len(new_old_data))
            bill_id = self.bill_manage_server.add_bill(
                OperationType.add.value, TypeId.user.value, new_old_data, 'PT_User2')
            return bill_id
