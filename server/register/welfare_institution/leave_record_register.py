from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.leave_record import LeaveRecordService
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:11:29
@LastEditors: Please set LastEditors
'''

# -*- coding: utf-8 -*-
'''
住宿记录相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    LeaveRecord_func = LeaveRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ILeaveRecordService.update_leave_record')
    def __update_leave_record(LeaveRecord):
        if 'id' in list(LeaveRecord.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.leave_record_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.leave_record_all, PermissionName.add)
        res = LeaveRecord_func.update_leave_record(LeaveRecord)
        LeaveRecord_func.bill_manage_service.insert_logs(
            '机构养老', '请销假记录', '新增/编辑请销假记录')
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_record_list')  # 暂未用
    def __get_leave_record_list(condition, page=None, count=None):
        res = LeaveRecord_func.get_leave_record_list(condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_list_all')
    def __get_leave_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.leave_record_all, PermissionName.query)
        res = LeaveRecord_func.get_leave_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_list_look')
    def __get_leave_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.leave_record_look, PermissionName.query)
        res = LeaveRecord_func.get_leave_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_list')
    def __get_leave_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = LeaveRecord_func.get_leave_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.del_leave_record')
    def __del_leave_record(ids):
        __p = security_service.judge_permission_other(
            FunctionName.leave_record_all, PermissionName.delete)
        res = LeaveRecord_func.del_leave_record(ids)
        LeaveRecord_func.bill_manage_service.insert_logs(
            '机构养老', '请销假记录', '删除请销假记录')
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_reason_list_all')
    def __get_leave_reason_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.leave_reason_all, PermissionName.query)
        res = LeaveRecord_func.get_leave_reason_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.get_leave_reason_list')
    def __get_leave_reason_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = LeaveRecord_func.get_leave_reason_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ILeaveRecordService.update_leave_reason')
    def __update_leave_reason(leave_reason):
        if 'id' in list(leave_reason.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.leave_reason_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.leave_reason_all, PermissionName.add)
        res = LeaveRecord_func.update_leave_reason(leave_reason)
        LeaveRecord_func.bill_manage_service.insert_logs(
            '机构养老', '离开原因', '新增/编辑离开原因')
        return res

    @jsonrpc.method('ILeaveRecordService.del_leave_reason')
    def __del_leave_reason(ids):
        __p = security_service.judge_permission_other(
            FunctionName.leave_reason_all, PermissionName.delete)
        res = LeaveRecord_func.del_leave_reason(ids)
        LeaveRecord_func.bill_manage_service.insert_logs(
            '机构养老', '离开原因', '删除离开原因')
        return res
