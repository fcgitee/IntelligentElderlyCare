# -*- coding: utf-8 -*-
'''
@Description: 部分住宿过程相关函数注册
@Author: 叶梦晴
@Date: 2019-09-02 16:06:55
@LastEditTime: 2019-12-05 15:28:44
@LastEditors: Please set LastEditors
'''
from ...service.welfare_institution.accommodation_process import AccommodationProcess
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    AccommodationProcess_func = AccommodationProcess(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAccommodationProcessService.change_room_process')
    def __change_room_process(data, noChangeBed=None):
        #   data : 包括字段user_id,new_bed_id
        # data = {
        #     "user_id": resident_source_id,
        #     "new_bed_id": bed_dest_id
        # }
        __p = security_service.judge_permission_other(
            FunctionName.check_in_room_all, PermissionName.edit)
        res = AccommodationProcess_func.change_room_process(data, noChangeBed)
        AccommodationProcess_func.bill_manage_server.insert_logs(
            '机构养老', '床位更换', '床位更换')
        return res

    @jsonrpc.method('IAccommodationProcessService.create_accommodation_data')
    def __create_accommodation_data(data):
        res = AccommodationProcess_func.create_accommodation_data(data)
        return res

    @jsonrpc.method('IAccommodationProcessService.auto_creat_service_record')
    def __auto_creat_service_record():
        __p = security_service.judge_permission_other(
            FunctionName.cost_account_all, PermissionName.add)
        res = AccommodationProcess_func.auto_creat_service_record()
        return res

    @jsonrpc.method('IAccommodationProcessService.auto_creat_service_record_manage')
    def __auto_creat_service_record():
        __P = security_service.judge_permission_other(
            FunctionName.cost_account_manage, PermissionName.add)
        res = AccommodationProcess_func.auto_creat_service_record()
        return res

    @jsonrpc.method('IAccommodationProcessService.check_out_process')
    def __check_out_process(user_id):
        res = AccommodationProcess_func.check_out_process(user_id)
        return res
