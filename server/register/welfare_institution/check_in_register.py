'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:02:54
@LastEditors: Please set LastEditors
'''

from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)
from ...service.welfare_institution.check_in import CheckInService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    check_in = CheckInService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('CheckInService.get_check_in_list_all')
    def __get_check_in_list_all(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all, PermissionName.query)
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.get_check_in_list_details')
    def __get_check_in_list_all(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all, PermissionName.query)
        res = check_in.get_check_in_list_details(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.get_check_in_list_look')
    def __get_check_in_list_look(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_look, PermissionName.query)
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.check_if_pay_done')
    def check_if_pay_done(condition):
        res = check_in.check_if_pay_done(
            condition)
        return res

    # 入住办理-九江

    @jsonrpc.method('CheckInService.check_in_alone_all')
    def __get_check_in_alone_all(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_alone_all, PermissionName.query)
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.check_in_alone_look')
    def __check_in_alone_look(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_alone_look, PermissionName.query)
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.get_check_in_list')
    def __get_check_in_list(condition, page=None, count=None):
        '''入住登记列表'''
        permission_data = security_service.org_all_subordinate()
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CheckInService.get_check_in_list_room_all')
    def __get_check_in_list_look(condition, page=None, count=None):
        '''房间调整查询列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_room_all, PermissionName.query)
        res = check_in.get_check_in_list(
            permission_data, condition, page, count)
        return res

    # @jsonrpc.method('CheckInService.delete_check_in_all')
    # def __delete_check_in(ids):
    #     '''删除入住登记'''
    #     __p = security_service.judge_permission_other(FunctionName.check_in_all, PermissionName.delete)
    #     res = check_in._del_data(ids, 'IEC_Check_In')
    #     return res

    @jsonrpc.method('CheckInService.add_check_in')
    def __add_check_in(elder, evaluation_info, service_item):
        '''新建入住登记'''
        __p = security_service.judge_permission_other(
            FunctionName.check_in_all, PermissionName.add)
        res = check_in.add_check_in(elder, evaluation_info, service_item)
        return res

    @jsonrpc.method('CheckInService.add_check_in_special')
    def __add_check_in(service_item):
        '''新建入住登记'''
        __p = security_service.judge_permission_other(
            FunctionName.check_in_alone_all, PermissionName.add)
        res = check_in.add_check_in_special(service_item)
        check_in.bill_manage_service.insert_logs(
            '机构养老', '入住长者', '入住办理')
        return res

    @jsonrpc.method('CheckInService.update_check_in')
    def __update_check_in(condition):
        '''更新入住登记'''
        __p = security_service.judge_permission_other(
            FunctionName.check_in_all, PermissionName.edit)
        res = check_in.update_check_in(condition)
        return res

    @jsonrpc.method('CheckInService.update_create_date')
    def __update_create_date(condition):
        '''更新入住时间'''
        __p = security_service.judge_permission_other(
            FunctionName.check_in_all, PermissionName.edit)
        res = check_in.update_create_date(condition)
        return res
