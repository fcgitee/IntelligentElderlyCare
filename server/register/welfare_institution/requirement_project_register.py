from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.requirement_project import requirementProjectService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:57:51
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\requirement_project_register.py
'''
# -*- coding: utf-8 -*-
'''
需求项目相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    requirement_project_func = requirementProjectService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRequirementProjectService.get_list')
    def __get_requirement_project_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = requirement_project_func.get_requirement_project_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IRequirementProjectService.update')
    def __update_user(requirement_project):
        res = requirement_project_func.update(requirement_project)
        return res

    @jsonrpc.method('IRequirementProjectService.delete')
    def __del_hotel_zone_project(Ids):
        res = requirement_project_func.del_requirement_project(Ids)
        return res
