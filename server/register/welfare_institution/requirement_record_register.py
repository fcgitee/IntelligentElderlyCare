from ...service.welfare_institution.requirement_record import RequirementRecordService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:04:31
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\requirement_record_register.py
'''
# -*- coding: utf-8 -*-
'''
需求记录服务注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    requirement_record_func = RequirementRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRequirementRecordService.get_requirement_record_list')
    def __get_requirement_record_list(condition, page, count):
        res = requirement_record_func.get_requirement_record_list(
            condition, page, count)
        return res

    @jsonrpc.method('IRequirementRecordService.update_requirement_record')
    def __update_requirement_record(requirement_record):
        res = requirement_record_func.update_requirement_record(
            requirement_record)
        return res

    @jsonrpc.method('IRequirementRecordService.delete_requirement_record')
    def __delete_requirement_record(requirement_record_ids):
        res = requirement_record_func.delete_requirement_record(
            requirement_record_ids)
        return res
