'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:01:41
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\reservation_registration_register.py
'''
# -*- coding: utf-8 -*-
'''
预约登记服务注册
'''




from ...service.welfare_institution.reservation_registration import ReservationRegistrationService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    reservation_registration_func = ReservationRegistrationService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IReservationRegistrationService.get_reservation_registration_list_all')
    def __get_reservation_registration_list_all(condition, page=None, count=None):
        org_list = security_service.judge_permission_query(
            FunctionName.reservation_regist, PermissionName.query)
        res = reservation_registration_func.get_reservation_registration_list(
            org_list, condition, page, count)
        return res

    @jsonrpc.method('IReservationRegistrationService.get_reservation_registration_list_look')
    def __get_reservation_registration_list_look(condition, page=None, count=None):
        org_list = security_service.judge_permission_query(
            FunctionName.reservation_regist_look, PermissionName.query)
        res = reservation_registration_func.get_reservation_registration_list(
            org_list, condition, page, count)
        return res

    @jsonrpc.method('IReservationRegistrationService.update_reservation_registration')
    def __update_reservation_registration_all(reservation_registration):
        if 'id' in list(reservation_registration.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.reservation_regist, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.reservation_regist, PermissionName.add)
        res = reservation_registration_func.update_reservation_registration(
            reservation_registration)
        reservation_registration_func.bill_manage_service.insert_logs(
            '机构养老', '预约登记', '新增/编辑预约登记')
        return res

    @jsonrpc.method('IReservationRegistrationService.app_update_reservation_registration')
    def __app_update_reservation_registration(reservation_registration):
        res = reservation_registration_func.app_update_reservation_registration(
            reservation_registration)
        reservation_registration_func.bill_manage_service.insert_logs(
            '机构养老', '预约登记', '新增/编辑预约登记')
        return res

    @jsonrpc.method('IReservationRegistrationService.delete_reservation_registration')
    def __delete_reservation_registration_all(reservation_registration_ids):
        __p = security_service.judge_permission_other(
            FunctionName.reservation_regist, PermissionName.delete)
        res = reservation_registration_func.delete_reservation_registration(
            reservation_registration_ids)
        reservation_registration_func.bill_manage_service.insert_logs(
            '机构养老', '预约登记', '删除预约登记')
        return res
