'''
@Author: your name
@Date: 2019-10-21 08:37:02
@LastEditTime: 2019-10-29 23:57:56
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\nursing_manage_register.py
'''
from ...service.buss_pub.bill_manage import TypeId
from ...service.welfare_institution.nursing_manage import NursingManage
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.nursing_manage import NursingTable


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    nursing_manage = NursingManage(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    ########################### 护理项目 ###################################
    @jsonrpc.method('INursingService.update_nursing_project')
    def __add_or_update(condition):
        '''新增/编辑护理项目'''
        Permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.nursing_project, Permission_name)
        res = nursing_manage.update_nursing_project(
            condition, NursingTable.project.value, TypeId.nursingProject.value)
        return res

    @jsonrpc.method('INursingService.get_nursing_project_list')
    def __get_nursing_project_list(condition, page=None, count=None):
        '''获取护理项目列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_project, PermissionName.query)
        res = nursing_manage.get_nursing_project_list(
            permission_data, NursingTable.project.value, condition, page, count)
        return res

    @jsonrpc.method('INursingService.delete_nursing_project')
    def __delete_nursing_project(ids):
        '''删除护理项目'''
        security_service.judge_permission_other(
            FunctionName.nursing_project, PermissionName.delete)
        res = nursing_manage.delete_nursing_project(
            ids, NursingTable.project.value, TypeId.nursingProject.value)
        return res

    ############################# 护理模板 ###################################
    @jsonrpc.method('INursingTemplateService.update_nursing_template')
    def __update_nursing_template(condition):
        '''新增/编辑护理模板'''
        Permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.nursing_template, Permission_name)
        res = nursing_manage.update_nursing_project(
            condition, NursingTable.template.value, TypeId.nursingTemplate.value)
        return res

    @jsonrpc.method('INursingService.get_nursing_template_list')
    def __get_nursing_template_list(condition, page=None, count=None):
        '''获取护理模板列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_template, PermissionName.query)
        res = nursing_manage.get_nursing_template_list(
            permission_data, condition,  page, count)
        return res

    @jsonrpc.method('INursingTemplateService.delete_nursing_template')
    def __delete_nursing_template(ids):
        '''删除护理模板'''
        security_service.judge_permission_other(
            FunctionName.nursing_template, PermissionName.delete)
        res = nursing_manage.delete_nursing_project(
            ids, NursingTable.template.value, TypeId.nursingTemplate.value)
        return res

    ############################## 护理记录 #####################################
    @jsonrpc.method('INursingService.update_nursing_recode')
    def __update_nursing_recode(condition):
        '''新增/编辑护理记录'''
        Permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        # security_service.judge_permission_other(FunctionName.nursing_recode, Permission_name)
        res = nursing_manage.update_nursing_recode(condition)
        return res

    @jsonrpc.method('INursingService.get_nursing_recode_list')
    def __get_nursing_recode_list(condition, page=None, count=None):
        '''获取护理记录列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_template, PermissionName.query)
        res = nursing_manage.get_nursing_recode_list(
            permission_data, condition, page, count)
        return res
    
    @jsonrpc.method('INursingService.get_nursing_recode_list_look')
    def __get_nursing_recode_list(condition, page=None, count=None):
        '''获取护理记录列表'''
        res = nursing_manage.get_nursing_recode_list_look(
            condition, page, count)
        return res

    @jsonrpc.method('INursingTemplateService.delete_nursing_recode')
    def __delete_nursing_recode(ids):
        '''删除护理记录'''
        security_service.judge_permission_other(
            FunctionName.nursing_recode, PermissionName.delete)
        res = nursing_manage.delete_nursing_project(
            ids, NursingTable.recode.value, TypeId.nursingRecode.value)
        return res
