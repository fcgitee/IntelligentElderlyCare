'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:54:12
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\hydropower_register.py
'''
from ...service.buss_pub.bill_manage import TypeId
from ...service.welfare_institution.hydropower import Hydropower
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    hydropower = Hydropower(db_addr, db_port, db_name, db_user, db_pwd,
                            inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHydropowerService.update_hydropower')
    def __add_or_update(condition):
        '''新增/编辑水电抄表'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.hydro_power_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.hydro_power_all, PermissionName.add)
        res = hydropower.update_hydropower(condition)
        return res

    @jsonrpc.method('IHydropowerService.get_hydropower_list_all')
    def __get_hospital_list_all(condition, page=None, count=None):
        '''水电列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_all, PermissionName.query)
        return hydropower.get_hydropower_list(permission_data, condition, page, count)

    @jsonrpc.method('IHydropowerService.get_hydropower_list_look')
    def __get_hospital_list_all(condition, page=None, count=None):
        '''查看水电列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_look, PermissionName.query)
        return hydropower.get_hydropower_list(permission_data, condition, page, count)

    @jsonrpc.method('IHydropowerService.jiujiang_update_hydropower')
    def __jiujiang_add_or_update(condition):
        '''新增/编辑水电抄表-九江'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.hydro_power_num, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.hydro_power_num, PermissionName.add)
        res = hydropower.jiujiang_update_hydropower(condition)
        return res

    @jsonrpc.method('IHydropowerService.get_jiujiang_hydropower_list_all')
    def __get_jiujiang_hydropower_list_all(condition, page=None, count=None):
        '''水电列表-九江'''
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_num, PermissionName.query)
        return hydropower.get_jiujiang_hydropower_list(permission_data, condition, page, count)

    @jsonrpc.method('IHydropowerService.get_jiujiang_hydropower_list_look')
    def __get_jiujiang_hydropower_list_all(condition, page=None, count=None):
        '''查看水电列表-九江'''
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_num_look, PermissionName.query)
        return hydropower.get_jiujiang_hydropower_list(permission_data, condition, page, count)

    @jsonrpc.method('IHydropowerService.delete_jiujiang_hydropower')
    def __delete_jiujiang_hydropower(ids):
        permission_data = security_service.judge_permission_other(
            FunctionName.hydro_power_num, PermissionName.delete)
        return hydropower.delete_jiujiang_hydropower(ids, 'IEC_Hydropower_Special')

    @jsonrpc.method('IHydropowerService.upload_hydropower')
    def __upload_jiujiang_hydropower(condition):
        security_service.judge_permission_other(
            FunctionName.hydro_power_num, PermissionName.add)
        return hydropower.upload_hydropower(condition)

    @jsonrpc.method('IHydropowerService.export_template')
    def __export_template():
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_num, PermissionName.query)
        return hydropower.export_template(permission_data)

    @jsonrpc.method('IHydropowerService.update_water_electricity')
    def __update_water_electricity(condition):
        '''新增/编辑水电抄表'''
        permission_name = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.hydro_power_all, permission_name)
        res = hydropower.update_water_electricity(condition)
        return res

    @jsonrpc.method('IHydropowerService.get_water_electricity')
    def __get_water_electricity(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.hydro_power_all, PermissionName.query)
        return hydropower.get_water_electricity(permission_data, condition, page, count)

    @jsonrpc.method('IHydropowerService.delete_water_electricity')
    def __delete_water_electricity(ids):
        '''删除水电抄表'''
        security_service.judge_permission_other(
            FunctionName.hydro_power_all, PermissionName.delete)
        res = hydropower.delete_water_electricity(ids)
        return res
