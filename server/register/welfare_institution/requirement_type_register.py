# -*- coding: utf-8 -*-
'''
需求类型相关函数注册
'''
from ...service.welfare_institution.requirement_type import requirementTypeService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    requirement_type_func = requirementTypeService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRequirementTypeService.get_list')
    def __get_requirement_type_list(condition, page=None, count=None):
        res = requirement_type_func.get_requirement_type_list(
            condition, page, count)
        return res

    @jsonrpc.method('IRequirementTypeService.update')
    def __update_user(requirement_type):
        res = requirement_type_func.update(requirement_type)
        return res

    @jsonrpc.method('IRequirementTypeService.delete')
    def __del_hotel_zone_type(Ids):
        res = requirement_type_func.del_requirement_type(Ids)
        return res
