from ...service.welfare_institution.hospital import HospitalManage
from ...service.buss_pub.bill_manage import TypeId


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    hospital_manage = HospitalManage(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHospitalService.update_hospital')
    def __add_or_update(condition):
        '''新增/编辑医疗欠费'''
        res = hospital_manage.add_updeate_hospital(condition)
        return res

    @jsonrpc.method('IHospitalService.get_hospital_list')
    def __get_hospital_list(condition, page=None, count=None):
        '''医疗欠费列表'''
        return hospital_manage.get_hospital_list(condition, page, count)

    @jsonrpc.method('IHospitalService.delete_hospital')
    def ___del_data(condition):
        '''删除医疗欠费列表'''
        res = hospital_manage.delete_hospital(condition, 'IEC_Hospital')
        return res
