from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.hotel_zone import hotelZoneService
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:55:56
@LastEditors: Please set LastEditors
'''
# -*- coding: utf-8 -*-
'''
住宿区域相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    hotel_zone_func = hotelZoneService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IHotelZoneService.get_list_all')
    def __get_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.hotel_zone_manange_all, PermissionName.query)
        res = hotel_zone_func.get_hotel_zone_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneService.get_list_look')
    def __get_hotel_zone_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.hotel_zone_manange_look, PermissionName.query)
        res = hotel_zone_func.get_hotel_zone_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneService.get_list')
    def __get_hotel_zone_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_func.get_hotel_zone_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneService.get_room_status_list')
    def __get_hotel_zone_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_func.get_room_status_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneService.get_room_status_pure_list')
    def __get_room_status_pure_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_func.get_room_status_pure_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IHotelZoneService.update_room_status')
    def __get_hotel_zone_list(resident_source_id, bed_source_id, bed_dest_id):
        res = hotel_zone_func.update_room_status(
            resident_source_id, bed_source_id, bed_dest_id)
        hotel_zone_func.bill_manage_server.insert_logs(
            '机构养老', '床位全览', '新增/编辑床位全览')
        return res

    @jsonrpc.method('IHotelZoneService.update')
    def __update_user(hotel_zone):
        if 'id' in list(hotel_zone.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.hotel_zone_manange_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.hotel_zone_manange_all, PermissionName.add)
        res = hotel_zone_func.update(hotel_zone)
        hotel_zone_func.bill_manage_server.insert_logs(
            '机构养老', '区域管理', '新增/编辑区域管理')
        return res

    @jsonrpc.method('IHotelZoneService.delete')
    def __del_hotel_zone(Ids):
        __p = security_service.judge_permission_other(
            FunctionName.hotel_zone_manange_all, PermissionName.delete)
        res = hotel_zone_func.del_hotel_zone(Ids)
        hotel_zone_func.bill_manage_server.insert_logs(
            '机构养老', '区域管理', '删除区域管理')
        return res

    @jsonrpc.method('IHotelZoneService.get_nursing_level_Byid')
    def __get_nursing_level_Byid(condition):
        res = hotel_zone_func.get_nursing_level_Byid(condition)
        return res

    @jsonrpc.method('IHotelZoneService.get_user_by_zone')
    def __get_user_by_zone(id):
        res = hotel_zone_func.get_user_by_zone(id)
        return res

    @jsonrpc.method('IHotelZoneService.get_organization_list')
    def __get_organization_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = hotel_zone_func.get_organization_list(
            permission_data, condition, page, count)
        return res