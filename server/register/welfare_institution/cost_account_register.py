'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 14:59:56
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\cost_account_register.py
'''
from ...service.welfare_institution.cost_account import CostAccount
from ...service.buss_pub.bill_manage import TypeId
from ...service.buss_mis.service_operation import ServiceOperationService
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_mis.service_record import ServiceRecordService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    costAccount = CostAccount(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    service_operation_func = ServiceOperationService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    service_record_func = ServiceRecordService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    # @jsonrpc.method('ICostAccountService.update_cost_account')
    # def __add_or_update(condition):
    #     '''新增/编辑费用核算'''
    #     costAccount._add_update_and_bill(condition, 'IEC_Hospital',TypeId.medicalArrears.value)
    #     return costAccount.res

    @jsonrpc.method('ICostAccountService.get_cost_account_list_all')
    def __get_hospital_list_all(condition, page=None, count=None):
        '''费用核算列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.cost_account_all, PermissionName.query)
        res = costAccount.get_cost_account_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICostAccountService.get_cost_account_list_manage')
    def __get_hospital_list(condition, page=None, count=None):
        '''费用核算列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.cost_account_manage, PermissionName.query)
        res = costAccount.get_cost_account_list(
            permission_data, condition, page, count)
        return res

    # @jsonrpc.method('ICostAccountService.delete_cost_account')
    # def ___del_data(condition):
    #     '''删除费用核算列表'''
    #     costAccount._del_data(condition, 'IEC_Hospital')
    #     return costAccount.res

    @jsonrpc.method('ICostAccountService.cost_account')
    def __cost_account(condition, select_date):
        '''核算'''
        __p = security_service.judge_permission_other(
            FunctionName.cost_account_manage, PermissionName.edit)
        res = costAccount.cost_account(
            condition, select_date, service_operation_func)
        return res

    @jsonrpc.method('ICostAccountService.cost_account_manage')
    def __cost_account_invoking_financial(recode_ids):
        '''发送账单'''
        __p = security_service.judge_permission_other(
            FunctionName.cost_account_manage, PermissionName.edit)
        res = service_record_func.invoking_financial(recode_ids)
        return res

    @jsonrpc.method('ICostAccountService.add_receivables_data')
    def __add_receivables_data(condition, date):
        '''核算应收款账单'''
        security_service.judge_permission_other(
            FunctionName.receivables, PermissionName.add)
        res = costAccount.add_receivables_data(condition, date)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '预收款账单', '新增预收款账单')
        return res

    @jsonrpc.method('ICostAccountService.get_receivables_list')
    def __get_receivables_list(condition, page=None, count=None):
        '''应收款账单'''
        org_list = security_service.judge_permission_query(
            FunctionName.receivables, PermissionName.query)
        res = costAccount.get_receivables_yh_list(org_list,
                                                  condition, page, count)
        return res

    @jsonrpc.method('ICostAccountService.send_receivables_data')
    def __send_receivables_data():
        '''核算应收款账单'''
        security_service.judge_permission_other(
            FunctionName.receivables, PermissionName.add)
        condition = {"check_status": "通过",
                     "send_status": "未发送",
                     "type": "预收",
                     }
        res = costAccount.send_receivables_data(condition)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '预收款账单', '发送预收款账单')
        return res

    @jsonrpc.method('ICostAccountService.check_bill')
    def __check_bill(condition):
        '''核算应收款账单'''
        security_service.judge_permission_other(
            FunctionName.receivables, PermissionName.add)
        param = {"send_status": "已发送",
                 "type": "预收",
                 }
        res = costAccount.check_bill(condition, param)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '预收款账单', '核算预收款账单')
        return res

    @jsonrpc.method('ICostAccountService.add_actual_receivables_data')
    def __add_actual_receivables_data(condition, date):
        '''核算实收款账单'''
        security_service.judge_permission_other(
            FunctionName.actual_receivables, PermissionName.add)
        res = costAccount.add_actual_receivables_data(condition, date)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '实收款核算及审核', '新增实收款核算及审核')
        return res

    @jsonrpc.method('ICostAccountService.get_actual_receivables_list')
    def __get_receivables_list(condition, page=None, count=None):
        '''应收款账单'''
        org_list = security_service.judge_permission_query(
            FunctionName.actual_receivables, PermissionName.query)
        res = costAccount.get_actual_receivables_yh_list(org_list,
                                                         condition, page, count)
        return res

    @jsonrpc.method('ICostAccountService.check_bill_actual')
    def __check_bill_actual(condition, param=None):
        '''核算应收款账单'''
        security_service.judge_permission_other(
            FunctionName.actual_receivables, PermissionName.add)
        res = costAccount.check_bill_actual(condition, param)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '实收款核算及审核', '核算实收款核算及审核')
        return res

    @jsonrpc.method('ICostAccountService.get_difference_detail_list')
    def __get_receivables_list(condition, page=None, count=None):
        '''应收款账单'''
        org_list = security_service.judge_permission_query(
            FunctionName.difference_detail, PermissionName.query)
        res = costAccount.get_difference_detail_yh_list(org_list,
                                                        condition, page, count)
        return res

    @jsonrpc.method('ICostAccountService.bill_down')
    def __get_receivables_list(condition):
        '''导出收款账单'''
        org_list = security_service.judge_permission_query(
            FunctionName.receivables, PermissionName.query)
        res = costAccount.bill_down(org_list, condition)
        costAccount.bill_manage_service.insert_logs(
            '机构养老', '实收款核算及审核', '导出实收款核算及审核')
        return res
