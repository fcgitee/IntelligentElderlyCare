# -*- coding: utf-8 -*-
'''
费用管理相关函数注册
'''
from ...service.welfare_institution.cost_manage import CostManageService
from ...pao_python.pao.service.data.mongo_db import N
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_pub.bill_manage import BillManageService, OperationType, TypeId, Status


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    costManageService = CostManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('CostManageService.get_incidental_list')
    def __get_incidental_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.miscellaneous_fees_all, PermissionName.query)
        res = costManageService.get_incidental_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CostManageService.get_incidental_list_look')
    def __get_incidental_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.miscellaneous_fees_look, PermissionName.query)
        res = costManageService.get_incidental_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('CostManageService.update_incidental')
    def __update_incidental(data):
        permission_name = PermissionName.edit if 'id' in data.keys() else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.miscellaneous_fees_all, permission_name)
        res = costManageService.update_incidental(
            data, {'table_name': 'PT_Incidental_Cost', 'name': '杂费', 'type': TypeId.incidental.value})
        return res

    @jsonrpc.method('CostManageService.upload_incidental')
    def __upload_incidental(data):
        security_service.judge_permission_other(
            FunctionName.miscellaneous_fees_all, PermissionName.add)
        res = costManageService.upload_incidental(
            data, {'table_name': 'PT_Incidental_Cost', 'name': '杂费'}, TypeId.incidental.value)
        return res

    @jsonrpc.method('CostManageService.upload_food_cost')
    def __upload_food_cost(data):
        security_service.judge_permission_other(
            FunctionName.meal_fee_all, PermissionName.add)
        res = costManageService.upload_incidental(
            data, {'table_name': 'PT_Food_Cost', 'name': '伙食费'}, TypeId.footCost.value)
        return res

    @jsonrpc.method('CostManageService.delete_incidental')
    def __delete_incidental(ids):
        security_service.judge_permission_other(
            FunctionName.miscellaneous_fees_all, PermissionName.delete)
        res = costManageService.delete_incidental(
            ids, 'PT_Incidental_Cost', TypeId.incidental.value)
        return res

    @jsonrpc.method('CostManageService.get_food_cost_list')
    def __get_food_cost_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.meal_fee_all, PermissionName.query)
        res = costManageService.get_incidental_list(
            permission_data, condition, page, count, 'PT_Food_Cost')
        return res

    @jsonrpc.method('CostManageService.get_food_cost_list_look')
    def __get_food_cost_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.meal_fee_look, PermissionName.query)
        res = costManageService.get_incidental_list(
            permission_data, condition, page, count, 'PT_Food_Cost')
        return res

    @jsonrpc.method('CostManageService.update_food_cost')
    def __update_food_cost(data):
        permission_name = PermissionName.edit if 'id' in data.keys() else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.meal_fee_all, permission_name)
        res = costManageService.update_incidental(
            data, {'table_name': 'PT_Food_Cost', 'name': '伙食费', 'type': TypeId.footCost.value})
        return res

    @jsonrpc.method('CostManageService.delete_food_cost')
    def __delete_food_cost(ids):
        security_service.judge_permission_other(
            FunctionName.meal_fee_all, PermissionName.delete)
        res = costManageService.delete_incidental(
            ids, 'PT_Food_Cost', TypeId.footCost.value)
        return res

    @jsonrpc.method('CostManageService.get_medical_cost_list')
    def __get_medical_cost_list(condition, page=None, count=None):
        res = costManageService.get_medical_cost_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('CostManageService.update_medical_cost')
    def __update_medical_cost(data):
        res = costManageService.update_medical_cost(data)
        return res

    @jsonrpc.method('CostManageService.delete_medical_cost')
    def __delete_medical_cost(ids):
        res = costManageService.delete_medical_cost(ids)
        return res

    @jsonrpc.method('CostManageService.get_diaper_cost_list')
    def __get_diaper_cost_list(condition, page=None, count=None):
        res = costManageService.get_diaper_cost_list(condition, page, count)
        return res

    @jsonrpc.method('CostManageService.update_diaper_cost')
    def __update_diaper_cost(data):
        res = costManageService.update_diaper_cost(data)
        return res

    @jsonrpc.method('CostManageService.delete_diaper_cost')
    def __delete_diaper_cost(ids):
        res = costManageServicecondition.delete_diaper_cost(ids)
        return res

    @jsonrpc.method('CostManageService.export_template')
    def __export_template():
        '''下载杂费模板'''
        permission_data = security_service.judge_permission_query(
            FunctionName.miscellaneous_fees_all, PermissionName.query)
        res = costManageService.export_template(permission_data)
        return res
