from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.welfare_institution.report_manage import ReportManage
'''
@Author: your name
@Date: 2019-11-04 13:48:54
@LastEditTime: 2019-12-05 15:30:31
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\welfare_institution\report_manage_register.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Friday November 1st 2019
创建者：ymq(ymq) - <<email>>

修改日期: Friday, 1st November 2019 3:33:44 pm
修改者: ymq(ymq) - <<email>>

说明
 1、报表接口注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    report_manage_service = ReportManage(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ReportManage.get_happiness_worker_list')
    def __get_happiness_worker_list(condition, page=None, count=None):
        '''获取幸福院工作人员统计列表'''
        condition['organization_category'] = '幸福院'
        permission_data = security_service.judge_permission_query(
            FunctionName.report_happiness_worker_count, PermissionName.query)
        res = report_manage_service.get_org_worker_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ReportManage.get_welfare_worker_list')
    def __get_welfare_worker_list(condition, page=None, count=None):
        '''获取福利院工作人员统计列表'''
        condition['organization_category'] = '福利院'
        permission_data = security_service.judge_permission_query(
            FunctionName.report_welfare_worker_count, PermissionName.query)
        res = report_manage_service.get_org_worker_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ReportManage.get_check_in_elder_list')
    def __get_check_in_elder_list(condition, page=None, count=None):
        '''福利院入住长者统计'''
        permission_data = security_service.judge_permission_query(
            FunctionName.report_welfare_elder_count, PermissionName.query)
        res = report_manage_service.get_check_in_elder_list(
            permission_data, condition, page, count)
        return res
