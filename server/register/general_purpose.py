'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:45:45
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\general_purpose.py
'''
from ..pao_python.pao.commom import ServiceProcess


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd,):

    service_process = ServiceProcess(
        db_addr, db_port, db_name, db_user, db_pwd,)

    @jsonrpc.method('IServiceProcess.query')
    def __query(col_name, condition, page=1, pageSize=10):
        res = service_process.query(col_name, condition, page, pageSize)
        return res

    @jsonrpc.method('IServiceProcess.delete')
    def __delete(col_name, ids):
        res = service_process.delete(col_name, ids)
        return res

    @jsonrpc.method('IServiceProcess.insert')
    def __delete(col_name, ids):
        res = service_process.insert(col_name, ids)
        return res
