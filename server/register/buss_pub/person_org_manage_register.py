'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-03-04 17:08:50
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\person_org_manage_register.py
'''

from server.pao_python.pao.remote import UnauthorizedError

from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_pub.person_org_manage import PersonOrgManageService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)


'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 8:51:29 pm
修改者: ymq(ymq) - <<email>>

说明
 1、
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    person_org_manage_service = PersonOrgManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IPersonOrgManageService.get_current_user_info')
    def __get_current_user_info():
        '''获取当前登录用户信息'''
        res = person_org_manage_service.get_current_user_info()
        return res

############################################################################################

    @jsonrpc.method('IPersonOrgManageService.get_elder_list_all')
    def __get_elder_list_all(condition, page=None, count=None):
        '''获取长者列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_manage_all, PermissionName.query)
        res = person_org_manage_service.get_user_elder_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_operate_sitation_statistic')
    def __get_operate_sitation_statistic(condition, page=None, count=None):
        '''获取运营情况统计列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.operate_sitation_statistic, PermissionName.query)
        res = person_org_manage_service.get_operate_sitation_statistic(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_charge_list')
    def __get_charge_list(condition, page=None, count=None):
        '''收费类型列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.charge_type, PermissionName.query)
        res = person_org_manage_service.get_charge_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_charge_list')
    def __update_charge_list(condition):
        '''更新收费类型列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.charge_type, PermissionName.edit)
        res = person_org_manage_service.update_charge_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_month_salary')
    def __update_month_salary(condition):
        '''更新月收入统计列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.month_salary, PermissionName.edit)
        res = person_org_manage_service.update_month_salary(
            permission_data, condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_charge_list')
    def __delete_charge_list(condition):
        '''删除收费类型列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.charge_type, PermissionName.delete)
        res = person_org_manage_service.delete_charge_list(
            permission_data, condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_wait_checkin_elder_list')
    def __get_user_list_area(condition={}, page=None, count=None):
        '''获取等待入住的长者列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_manage_all, PermissionName.query)
        res = person_org_manage_service.get_wait_checkin_elder_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_list_by_org')
    def __get_elder_list_by_org(condition, page=None, count=None):
        '''获取当前机构长者列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.org_elder_manage_all, PermissionName.query)
        res = person_org_manage_service.get_elder_list_by_org(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_list_look')
    def __get_user_list_area(condition, page=None, count=None):
        '''查询长者资料'''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_info_look, PermissionName.query)
        res = person_org_manage_service.get_user_elder_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_elder_info')
    def __update_elder_info_all(condition):
        '''长者管理 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.elder_manage_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.elder_manage_all, PermissionName.add)
        res = person_org_manage_service.update_elder_info(condition)
        # 记录使用
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '长者档案', '新增/编辑长者')
        return res['res']

    @jsonrpc.method('IPersonOrgManageService.delete_elder_info')
    def __delete_elder_info_all(condition):
        '''长者管理 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.elder_manage_all, PermissionName.delete)
        res = person_org_manage_service.delete_elder_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '长者档案', '删除长者')
        return res
###################################################################

    @jsonrpc.method('IPersonOrgManageService.get_organization_list_all')
    def __get_organization_list_all(condition, page=None, count=None):
        '''获取组织机构列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.organization_all, PermissionName.query)
        res = person_org_manage_service.get_organization_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_org_info_record_list')
    def __get_org_info_record_list(condition, page=None, count=None):
        '''获取机构信息档案列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.operate_sitation_statistic, PermissionName.query)
        res = person_org_manage_service.get_org_info_record_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_org_info_record')
    def __update_org_info_record(condition):
        '''更新机构信息档案'''
        permission_data = security_service.judge_permission_query(
            FunctionName.org_info_record, PermissionName.edit)
        res = person_org_manage_service.update_org_info_record(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '机构信息档案', '新增/编辑机构信息档案')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_org_list')
    def __get_org_list(condition):
        res = person_org_manage_service.get_org_list_all(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_org_info')
    def __get_current_org_info():
        '''获取当前机构信息'''
        res = person_org_manage_service.get_current_org_info()

    @jsonrpc.method('IPersonOrgManageService.get_cur_org_info')
    def __get_cur_org_info():
        '''获取当前组织机构'''
        res = person_org_manage_service.get_cur_org_info()
        return res

    @jsonrpc.method('IPersonOrgManageService.update_organization_info')
    def __update_organization_info(condition):
        '''组织机构 修改/新增'''
        if 'id' in list(condition.keys()):
            __P = security_service.judge_permission_other(
                FunctionName.organization_all, PermissionName.edit)
        else:
            __P = security_service.judge_permission_other(
                FunctionName.organization_all, PermissionName.add)
        res = person_org_manage_service.update_elder_info(condition)
        return res['res']

    @jsonrpc.method('IPersonOrgManageService.delete_organization_info')
    def __delete_elder_info_all(condition):
        '''组织机构 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.organization_all, PermissionName.delete)
        res = person_org_manage_service.delete_elder_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '机构信息档案', '删除机构信息档案')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_list_look')
    def __get_organization_list_look(condition, page=None, count=None):
        '''查询 组织机构'''
        permission_data = security_service.judge_permission_query(
            FunctionName.organization_look, PermissionName.query)
        res = person_org_manage_service.get_organization_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_worker_list_all')
    def __get_worker_list_all(condition, page=None, count=None):
        '''工作人员管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_manage_all, PermissionName.query)
        res = person_org_manage_service.get_user_worker_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_month_salary_list')
    def __get_user_list_area(condition, page=None, count=None):
        '''工作人员管理 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_manage_all, PermissionName.query)
        res = person_org_manage_service.get_month_salary_list(
            permission_data, condition, page, count)

        return res

    @jsonrpc.method('IPersonOrgManageService.update_personnel_info')
    def __update_elder_info_all(condition):
        '''人员管理 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.personnel_manage_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.personnel_manage_all, PermissionName.add)
        res = person_org_manage_service.update_elder_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '员工档案', '新增/编辑员工档案')
        return res['res']

    @jsonrpc.method('IPersonOrgManageService.delete_personnel_info')
    def __delete_elder_info_all(condition):
        '''工作人员管理 删除'''
        __p = security_service.judge_permission_other(
            FunctionName.personnel_manage_all, PermissionName.delete)
        res = person_org_manage_service.delete_elder_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '员工档案', '删除员工档案')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_organization')
    def __delete_organization(condition):
        '''组织机构 删除'''
        __p = security_service.judge_permission_other(
            FunctionName.organization_all, PermissionName.delete)
        res = person_org_manage_service.delete_organization(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_worker_info')
    def __update_elder_info_all(condition):
        '''工作人员管理 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.worker_manage_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.worker_manage_all, PermissionName.add)
        res = person_org_manage_service.update_elder_info(condition)
        return res['res']

    @jsonrpc.method('IPersonOrgManageService.update_organization')
    def __update_organization(condition):
        '''组织机构 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.organization_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.organization_all, PermissionName.add)
        res = person_org_manage_service.update_organization(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_business_income')
    def __update_business_income(condition):
        '''运营收入 修改/新增'''
        res = person_org_manage_service.update_business_income(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_business_income')
    def __get_business_income(condition, page=None, count=None):
        '''运营收入 查询'''
        res = person_org_manage_service.get_business_income(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_business_income_byid')
    def _delete_business_income_byid(condition):
        '''运营收入 删除'''
        res = person_org_manage_service.delete_business_income_byid(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_worker_info')
    def __delete_elder_info_all(condition):
        '''工作人员管理 删除'''
        __p = security_service.judge_permission_other(
            FunctionName.worker_manage_all, PermissionName.delete)
        res = person_org_manage_service.delete_elder_info(condition)
        return res

    # @jsonrpc.method('IPersonOrgManageService.get_org_worker_list_all')
    # def __get_user_list_area(condition, page=None, count=None):
    #     '''机构人员管理 查询'''
    #     permission_condtion = {'user_type': ['工作人员']}
    #     permission_data = security_service.judge_permission_query(
    #         FunctionName.org_worker_manage_all, PermissionName.query)
    #     res = person_org_manage_service.get_user_all_list(permission_data,
    #                                                       permission_condtion, condition, page=None, count=None)
    #     return res

    # @jsonrpc.method('IPersonOrgManageService.update_org_worker_info')
    # def __update_elder_info_all(condition):
    #     '''机构人员管理 修改/新增'''
    #     if 'id' in list(condition.keys()):
    #         __p = security_service.judge_permission_other(
    #             FunctionName.org_worker_manage_all, PermissionName.edit)
    #     else:
    #         __p = security_service.judge_permission_other(
    #             FunctionName.org_worker_manage_all, PermissionName.add)
    #     res = person_org_manage_service.update_elder_info(condition)
    #     return res

    # @jsonrpc.method('IPersonOrgManageService.delete_org_worker_info')
    # def __delete_elder_info_all(condition):
    #     '''机构人员管理 删除'''
    #     __p = security_service.judge_permission_other(
    #         FunctionName.org_worker_manage_all, PermissionName.delete)
    #     res = person_org_manage_service.delete_elder_info(condition)
    #     return res

#############################人员组织列表下拉框查询接口，不做功能权限判断，仅做范围控制##############################################
    @jsonrpc.method('IPersonOrgManageService.get_organization_list')
    def __get_organization_list(condition, page=None, count=None):
        '''查询 组织机构'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_organization_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_fly_list')
    def __get_organization_fly_list(condition, page=None, count=None):
        '''app查询福利院列表（不判断范围）'''
        res = person_org_manage_service.get_organization_fly_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_xfy_list')
    def __get_organization_xfy_list(condition, page=None, count=None):
        '''app查询幸福院列表（不判断范围）'''
        res = person_org_manage_service.get_organization_xfy_list(
            N(), condition, page, count)
        return res

    # @jsonrpc.method('IPersonOrgManageService.get_organization_fws_list')
    # def __get_organization_all_list(condition, page=None, count=None):
    #     '''app查询服务商列表（不判断范围）'''
    #     res = person_org_manage_service.get_organization_fws_list(
    #         N(), condition, page, count)
    #     return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_all_list')
    def __get_organization_all_list(condition, page=None, count=None):
        '''查询 组织机构（不判断范围）'''
        res = person_org_manage_service.get_organization_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_tree_list_forName')
    def __get_organization_tree_list(condition, page=None, count=None):
        '''查询 组织机构树形结构'''
        permission_data = security_service.org_current()
        res = person_org_manage_service.get_organization_tree_list_forName(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_tree_list')
    def __get_organization_tree_list(condition, page=None, count=None):
        '''查询 组织机构树形结构'''
        permission_data = security_service.org_current()
        res = person_org_manage_service.get_organization_tree_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_list_new')
    def __get_organization_list_new(condition, page=None, count=None):
        '''查询 组织机构-新'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_organization_list_new(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_org_show_app')
    def __update_org_show_app(data):
        '''验收幸福院'''
        res = person_org_manage_service.update_org_show_app(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '组织机构', '上架/下架', '组织机构上架/下架')
        return res

    @jsonrpc.method('IPersonOrgManageService.update_org_start_stop')
    def __update_org_start_stop(data):
        '''验收幸福院'''
        res = person_org_manage_service.update_org_start_stop(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '组织机构', '启用/停用', '组织机构启用/停用')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_ys_xfy_list')
    def __get_ys_xfy_list(condition, page=None, count=None):
        '''查询 验收幸福院列表'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_ys_xfy_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_xfy_target_setting_list')
    def __get_xfy_target_setting_list(condition, page=None, count=None):
        '''查询 幸福院评比指标列表'''
        res = person_org_manage_service.get_xfy_target_setting_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_xfy_target_setting_details_list')
    def __get_xfy_target_setting_details_list(condition, page=None, count=None):
        '''查询 幸福院评比详细指标列表'''
        res = person_org_manage_service.get_xfy_target_setting_details_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_ys_xfy')
    def __update_ys_xfy(data):
        '''验收幸福院'''
        res = person_org_manage_service.update_ys_xfy(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '验收管理', '编辑验收管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.update_xfy_target_setting')
    def __update_xfy_target_setting(data):
        '''新增幸福院评比指标'''
        res = person_org_manage_service.update_xfy_target_setting(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '社区幸福院评比指标设置', '新增社区幸福院评比指标设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.update_xfy_target_setting_details')
    def __update_xfy_target_setting_details(data):
        '''新增幸福院评比详细指标'''
        res = person_org_manage_service.update_xfy_target_setting_details(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '社区幸福院评比详细指标设置', '新增社区幸福院评比详细指标设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.del_xfy_target_setting')
    def __del_xfy_target_setting(ids):
        '''删除 幸福院评比指标'''
        res = person_org_manage_service.del_xfy_target_setting(ids)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '社区幸福院评比指标设置', '删除社区幸福院评比指标设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.del_xfy_target_setting_details')
    def __del_xfy_target_setting_details(ids):
        '''删除 幸福院评比详细指标'''
        res = person_org_manage_service.del_xfy_target_setting_details(ids)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '新增社区幸福院评比详细指标设置', '删除新增社区幸福院评比详细指标设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.upload_index_excel')
    def __upload_index_excel(condition):
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '新增社区幸福院评比详细指标设置', '上传新增社区幸福院评比详细指标设置')
        return person_org_manage_service.upload_index_excel(condition)

    @jsonrpc.method('IPersonOrgManageService.user_org_link')
    def __user_org_link(condition):
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台', '社区幸福院老人档案长者列表关联数据', '新增社区幸福院老人档案长者列表关联数据')
        return person_org_manage_service.user_org_link(condition)

    @jsonrpc.method('IPersonOrgManageService.user_org_link_list')
    def __user_org_link_list(condition, page=None, count=None):
        '''查询 查询用户关联机构数据'''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_list, PermissionName.query)
        res = person_org_manage_service.user_org_link_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_user_org_link')
    def __delete_user_org_link(condition):
        '''删除 删除用户关联机构数据'''
        res = person_org_manage_service.delete_user_org_link(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organization_xfy_tree_list')
    def __get_organization_xfy_tree_list(condition, page=None, count=None):
        '''查询幸福院组织机构树形结构'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_organization_xfy_tree_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_personnel_elder')
    def __get_elder_list(condition, page=None, count=None):
        '''查询长者'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_user_elder_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_user_elder_birth_list')
    def __get_user_elder_birth_list(condition, page=None, count=None):
        '''查询长者生日'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_user_elder_birth_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_list')
    def __get_elder_list(condition, page=None, count=None):
        '''查询长者，单纯长者的信息，不关联其他表'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_elder_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_all_elder_list')
    def __get_elder_list_all(condition, page=None, count=None):
        '''查询长者'''
        res = person_org_manage_service.get_elder_list_all(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_personnel_worker')
    def __get_elder_list(condition, page=None, count=None):
        '''查询工作人员'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_user_worker_list_yh(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_worker_list')
    def __get_elder_list(condition, page=None, count=None):
        '''单纯查询工作人员'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_worker_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_personnel_list_all')
    def __get_elder_list(condition, page=None, count=None):
        '''查询全部人员（机构范围内的工作人员和全部平台游客）'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_user_all_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_personnel_classification_list')
    def __get_personnel_classification_list(condition, page=None, count=None):
        '''查询人员类别'''
        # permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_personnel_classification_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_personnel_classification')
    def __update_personnel_classification(condition):
        '''人员类别修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.personnel_classification_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.personnel_classification_all, PermissionName.add)
        res = person_org_manage_service.update_personnel_classification(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台运营方', '长者类型设置', '新建/编辑长者类型设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_personnel_classification')
    def __delete_personnel_classification(condition):
        '''长者管理 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.personnel_classification_all, PermissionName.delete)
        res = person_org_manage_service.delete_personnel_classification(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台运营方', '长者类型设置', '删除长者类型设置')
        return res

    @jsonrpc.method('IPersonOrgManageService.import_excel_manage')
    def __import_excel_manage(stype, sdata={}):
        '''从excel导入数据'''
        __P = security_service.judge_permission_other(
            FunctionName.import_excel_manage, PermissionName.add)
        res = person_org_manage_service.import_excel_manage(stype, sdata)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_rzzzqktj_list')
    def __get_rzzzqktj_list(condition, page=None, count=None):
        '''查询长者入住统计情况'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all_statics, PermissionName.query)
        res = person_org_manage_service.get_rzzzqktj_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_apttj_list')
    def __get_apttj_list(condition, page=None, count=None):
        '''查询入住长者情况统计-按平台统计'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all_statics, PermissionName.query)
        res = person_org_manage_service.get_apttj_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_aqtj_list')
    def __get_aqtj_list(condition, page=None, count=None):
        '''查询入住长者情况统计-按区统计'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all_statics, PermissionName.query)
        res = person_org_manage_service.get_aqtj_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_azjtj_list')
    def __get_azjtj_list(condition, page=None, count=None):
        '''查询入住长者情况统计-按镇街统计'''
        permission_data = security_service.judge_permission_query(
            FunctionName.check_in_all_statics, PermissionName.query)
        res = person_org_manage_service.get_azjtj_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_cyryqktj_list')
    def __get_cyryqktj_list(condition, page=None, count=None):
        '''查询从业人员情况统计情况'''
        permission_data = security_service.judge_permission_query(
            FunctionName.worker_manage_all_statics, PermissionName.query)
        res = person_org_manage_service.get_cyryqktj_list(
            permission_data, condition, page, count)
        return res

#############################人员组织列表下拉框查询接口，不做功能权限、范围判断##############################################
    @jsonrpc.method('IPersonOrgManageService.get_all_organization_list')
    def __get_all_organization_list(condition, page=None, count=None):
        '''查询 全部组织机构'''
        permission_data = security_service.judge_permission_query(
            FunctionName.organization_all, PermissionName.query)
        res = person_org_manage_service.get_organization_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.getOrderByBussiness')
    def __getOrderByBussiness(condition, page=None, count=None):
        '''查询 全部组织机构'''
        res = person_org_manage_service.getOrderByBussiness(
            condition, page, count)
        return res

# 插入权限
# @jsonrpc.method('IPersonOrgManageService.addPersiom')
# def __addPersiom(conditon):
#     for x in conditon:

    @jsonrpc.method('IPersonOrgManageService.get_current_user_reservation_registration')
    def __get_current_user_reservation_registration(condition, page=None, count=None):
        '''获取当前用户机构预约记录'''
        res = person_org_manage_service.get_current_user_reservation_registration(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_user_activity_participate')
    def __get_current_user_activity_participate(condition, page=None, count=None):
        '''获取当前用户活动报名记录'''
        res = person_org_manage_service.get_current_user_activity_participate(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_user_allowance_manage')
    def __get_current_user_allowance_manage(condition, page=None, count=None):
        '''获取当前用户补贴申请记录'''
        res = person_org_manage_service.get_current_user_allowance_manage(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_user_charitable_donate')
    def __get_current_user_charitable_donate(condition, page=None, count=None):
        '''获取当前用户慈善申请记录'''
        res = person_org_manage_service.get_current_user_charitable_donate(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_user_acceptance')
    def __get_current_user_acceptance(condition, page=None, count=None):
        '''获取当前用户受理记录'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_current_user_acceptance(permission_data,
                                                                    condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_current_user_message_notice')
    def __get_current_user_message_notice(condition, page=None, count=None):
        '''获取当前用户消息提醒'''
        permission_data = security_service.org_all_subordinate()
        res = person_org_manage_service.get_current_user_message_notice(permission_data,
                                                                        condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.check_permission')
    def __check_permission(condition):
        '''判断活动和资讯的权限'''
        res = person_org_manage_service.check_permission(
            condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.create_draft')
    def __create_draft(data):
        res = person_org_manage_service.create_draft(
            data)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_draft_list')
    def __get_draft_list(data):
        res = person_org_manage_service.get_draft_list(
            data)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_healthy_list_all')
    def __get_elder_healthy_list_all(condition, page=None, count=None):
        ''' 长者健康列表查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_healthy, PermissionName.query)
        res = person_org_manage_service.get_elder_healthy_list(permission_data,
                                                               condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_healthy_list')
    def __get_elder_healthy_list(condition, page=None, count=None):
        ''' 长者健康列表查询 '''
        res = person_org_manage_service.get_elder_healthy_list(N(),
                                                               condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_food_list_all')
    def __get_elder_food_list_all(condition, page=None, count=None):
        ''' 长者用餐管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_food, PermissionName.query)
        res = person_org_manage_service.get_elder_food_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_food_list')
    def __get_elder_food_list(condition, page=None, count=None):
        ''' 长者用餐管理查询 '''
        res = person_org_manage_service.get_elder_food_list(N(),
                                                            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_elder_food')
    def __update_elder_food_all(condition):
        '''长者用餐 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.elder_food, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.elder_food, PermissionName.add)
        res = person_org_manage_service.update_elder_food(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_elder_food')
    def __delete_elder_food_all(condition):
        '''长者用餐 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.elder_food, PermissionName.delete)
        res = person_org_manage_service.delete_elder_food(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_deduction_list_all')
    def __get_deduction_list_all(condition, page=None, count=None):
        ''' 收费减免管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.deduction, PermissionName.query)
        res = person_org_manage_service.get_deduction_list(permission_data,
                                                           condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_deduction_list')
    def __get_deduction_list(condition, page=None, count=None):
        ''' 收费减免管理查询 '''
        res = person_org_manage_service.get_deduction_list(N(),
                                                           condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_deduction')
    def __update_deduction_all(condition):
        '''收费减免 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.deduction, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.deduction, PermissionName.add)
        res = person_org_manage_service.update_deduction(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_deduction')
    def __delete_deduction_all(condition):
        '''收费减免 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.deduction, PermissionName.delete)
        res = person_org_manage_service.delete_deduction(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_set_class_type_list_all')
    def __get_set_class_type_list_all(condition, page=None, count=None):
        ''' 排班类型管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.set_class_type, PermissionName.query)
        res = person_org_manage_service.get_set_class_type_list(permission_data,
                                                                condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_set_class_type_list')
    def __get_set_class_type_list(condition, page=None, count=None):
        ''' 排班类型管理查询 '''
        res = person_org_manage_service.get_set_class_type_list(N(),
                                                                condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_set_class_type')
    def __update_set_class_type_all(condition):
        '''排班类型 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.set_class_type, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.set_class_type, PermissionName.add)
        res = person_org_manage_service.update_set_class_type(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_set_class_type')
    def __delete_set_class_type_all(condition):
        '''排班类型 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.set_class_type, PermissionName.delete)
        res = person_org_manage_service.delete_set_class_type(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_set_class_list_all')
    def __get_set_class_list_all(condition, page=None, count=None):
        ''' 排班管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.set_class, PermissionName.query)
        res = person_org_manage_service.get_set_class_list(permission_data,
                                                           condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_set_class_list')
    def __get_set_class_list(condition, page=None, count=None):
        ''' 排班管理查询 '''
        res = person_org_manage_service.get_set_class_list(N(),
                                                           condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_set_class')
    def __update_set_class_all(condition):
        '''排班 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.set_class, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.set_class, PermissionName.add)
        res = person_org_manage_service.update_set_class(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_set_class')
    def __delete_set_class_all(condition):
        '''排班 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.set_class, PermissionName.delete)
        res = person_org_manage_service.delete_set_class(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_nursing_pay_list_all')
    def __get_nursing_pay_list_all(condition, page=None, count=None):
        ''' 护理收费管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_pay, PermissionName.query)
        res = person_org_manage_service.get_nursing_pay_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_nursing_pay_list')
    def __get_nursing_pay_list(condition, page=None, count=None):
        ''' 护理收费管理查询 '''
        res = person_org_manage_service.get_nursing_pay_list(N(),
                                                             condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_nursing_pay')
    def __update_nursing_pay_all(condition):
        '''护理收费 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.nursing_pay, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.nursing_pay, PermissionName.add)
        res = person_org_manage_service.update_nursing_pay(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_nursing_pay')
    def __delete_nursing_pay_all(condition):
        '''护理收费 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.nursing_pay, PermissionName.delete)
        res = person_org_manage_service.delete_nursing_pay(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_rating_plan_list_all')
    def __get_rating_plan_list_all(condition, page=None, count=None):
        ''' 评比方案查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.ratingPlan, PermissionName.query)
        res = person_org_manage_service.get_rating_plan_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_rating_plan_list')
    def __get_rating_plan_list(condition, page=None, count=None):
        ''' 评比方案查询 '''
        res = person_org_manage_service.get_rating_plan_list(N(),
                                                             condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_rating_plan')
    def __update_rating_plan_all(condition):
        '''评比方案修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.ratingPlan, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.ratingPlan, PermissionName.add)
        res = person_org_manage_service.update_rating_plan(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_rating_plan')
    def __delete_rating_plan_all(condition):
        '''评比方案删除'''
        __P = security_service.judge_permission_other(
            FunctionName.ratingPlan, PermissionName.delete)
        res = person_org_manage_service.delete_rating_plan(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_rating_list_all')
    def __get_rating_list_all(condition, page=None, count=None):
        ''' 评比填表查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.rating, PermissionName.query)
        res = person_org_manage_service.get_rating_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_rating_list')
    def __get_rating_list(condition, page=None, count=None):
        ''' 评比填表查询 '''
        res = person_org_manage_service.get_rating_list(N(),
                                                        condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_rating')
    def __update_rating_all(condition):
        '''评比填表修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.rating, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.rating, PermissionName.add)
        res = person_org_manage_service.update_rating(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_rating')
    def __delete_rating_all(condition):
        '''评比填表删除'''
        __P = security_service.judge_permission_other(
            FunctionName.rating, PermissionName.delete)
        res = person_org_manage_service.delete_rating(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_groups_list_all')
    def __get_groups_list_all(condition, page=None, count=None):
        ''' 团体查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.groups, PermissionName.query)
        res = person_org_manage_service.get_groups_list(permission_data,
                                                        condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_groups_list')
    def __get_groups_list(condition, page=None, count=None):
        ''' 团体查询 '''
        res = person_org_manage_service.get_groups_list(N(),
                                                        condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_groups_volunteer_list')
    def __get_groups_volunteer_list(condition, page=None, count=None):
        ''' 团体志愿者查询 '''
        res = person_org_manage_service.get_groups_volunteer_list(N(),
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_groups')
    def __update_groups_all(condition):
        '''团体修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.groups, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.groups, PermissionName.add)
        res = person_org_manage_service.update_groups(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '团队管理', '新增/编辑团队管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_groups')
    def __delete_groups_all(condition):
        '''团体删除'''
        __P = security_service.judge_permission_other(
            FunctionName.groups, PermissionName.delete)
        res = person_org_manage_service.delete_groups(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '团队管理', '删除团队管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_deal_and_report_list_all')
    def __get_deal_and_report_list_all(condition, page=None, count=None):
        ''' 项目协议与报告 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.dealReport, PermissionName.query)
        res = person_org_manage_service.get_deal_and_report_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_deal_and_report_list')
    def __get_deal_and_report_list(condition, page=None, count=None):
        ''' 项目协议与报告 '''
        res = person_org_manage_service.get_deal_and_report_list(N(),
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_deal_and_report')
    def __update_deal_and_report(condition):
        '''项目协议与报告修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.dealReport, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.dealReport, PermissionName.add)
        res = person_org_manage_service.update_deal_and_report(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '项目协议与报告', '新增/编辑项目协议与报告')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_deal_and_report')
    def __delete_deal_and_report(condition):
        '''项目协议与报告删除'''
        __P = security_service.judge_permission_other(
            FunctionName.dealReport, PermissionName.delete)
        res = person_org_manage_service.delete_deal_and_report(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '项目协议与报告', '删除项目协议与报告')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_yearplan_and_summary_list_all')
    def __get_yearplan_and_summary_list_all(condition, page=None, count=None):
        ''' 年度计划与总结 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.yearSummary, PermissionName.query)
        res = person_org_manage_service.get_yearplan_and_summary_list(permission_data,
                                                                      condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_yearplan_and_summary_list')
    def __get_yearplan_and_summary_list(condition, page=None, count=None):
        ''' 年度计划与总结 '''
        res = person_org_manage_service.get_yearplan_and_summary_list(N(),
                                                                      condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_yearplan_and_summary')
    def __update_yearplan_and_summary(condition):
        '''年度计划与总结修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.yearSummary, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.yearSummary, PermissionName.add)
        res = person_org_manage_service.update_yearplan_and_summary(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '年度计划与总结', '新增/编辑年度计划与总结')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_yearplan_and_summary')
    def __delete_yearplan_and_summary(condition):
        '''年度计划与总结删除'''
        __P = security_service.judge_permission_other(
            FunctionName.yearSummary, PermissionName.delete)
        res = person_org_manage_service.delete_yearplan_and_summary(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '年度计划与总结', '删除年度计划与总结')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_month_plan_list_all')
    def __get_month_plan_list_all(condition, page=None, count=None):
        ''' 月度计划 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.monthPlan, PermissionName.query)
        res = person_org_manage_service.get_month_plan_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_month_plan_list')
    def __get_month_plan_list(condition, page=None, count=None):
        ''' 月度计划 '''
        res = person_org_manage_service.get_month_plan_list(N(),
                                                            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_month_plan')
    def __update_month_plan(condition):
        '''月度计划修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.monthPlan, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.monthPlan, PermissionName.add)
        res = person_org_manage_service.update_month_plan(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '月度计划', '新增/编辑月度计划')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_month_plan')
    def __delete_month_plan(condition):
        '''月度计划删除'''
        __P = security_service.judge_permission_other(
            FunctionName.monthPlan, PermissionName.delete)
        res = person_org_manage_service.delete_month_plan(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '月度计划', '删除月度计划')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_happiness_operation_list')
    def __get_happiness_operation_list(condition, page=None, count=None):
        ''' 幸福院运营情况 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.happinessOperation, PermissionName.query)
        res = person_org_manage_service.get_happiness_operation_list(permission_data,
                                                                     condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_servicer_list')
    def __get_servicer_list(condition, page=None, count=None):
        ''' 服务商情况 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.servicerBaseinfo, PermissionName.query)
        res = person_org_manage_service.get_servicer_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_organ_list')
    def __get_servicer_list(condition, page=None, count=None):
        ''' 机构养老情况 '''
        permission_data = security_service.judge_permission_query(
            N(), PermissionName.query)
        res = person_org_manage_service.get_organ_list(permission_data,
                                                       condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_app_user_list')
    def __get_app_user_list(condition, page=None, count=None):
        ''' APP用户管理 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.appUserManage, PermissionName.query)
        res = person_org_manage_service.get_app_user_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.check_permission2')
    def __check_permission2(condition):
        ''' 检查权限 '''
        res = person_org_manage_service.check_permission2(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.change_app_user')
    def __change_app_user(condition):
        ''' 编辑用户 '''
        res = person_org_manage_service.change_app_user(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '平台运营方', 'APP用户管理', '编辑APP用户管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_area_operation_list')
    def __get_area_operation_list(condition, page=None, count=None):
        ''' 验收及评级情况统计 '''
        security_service.judge_permission_query(
            FunctionName.areaOperation, PermissionName.query)
        res = person_org_manage_service.get_area_operation_list(N(),
                                                                condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_org_operation_fund_list')
    def __get_org_operation_fund_list(condition, page=None, count=None):
        ''' 民办非营机构运营资助 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.orgOperationFund, PermissionName.query)
        res = person_org_manage_service.get_org_operation_fund_list(permission_data,
                                                                    condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_org_bed_fund_list')
    def __get_org_bed_fund_list(condition, page=None, count=None):
        ''' 民办非营机构床位资助 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.orgBedFund, PermissionName.query)
        res = person_org_manage_service.get_org_bed_fund_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_service_situation')
    def __update_service_situation(condition):
        '''个案服务情况 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.service_situation, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.service_situation, PermissionName.add)
        res = person_org_manage_service.update_service_situation(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '个案服务情况', '新增/编辑个案服务情况')

        return res

    @jsonrpc.method('IPersonOrgManageService.get_service_situation_list')
    def __get_service_situation_list(condition, page=None, count=None):
        '''个案服务情况 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.service_situation, PermissionName.query)
        res = person_org_manage_service.get_service_situation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_service_situation_byid')
    def __delete_service_situation_byid(condition):
        '''删除个案服务情况'''
        security_service.judge_permission_query(
            FunctionName.service_situation, PermissionName.delete)
        res = person_org_manage_service.delete_service_situation_byid(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '个案服务情况', '删除个案服务情况')
        return res

    @jsonrpc.method('IPersonOrgManageService.update_situation_summary')
    def __update_situation_summary(condition):
        '''新增情况总结'''
        res = person_org_manage_service.update_situation_summary(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_elder_info_byid')
    def __delete_elder_info_byid(condition):
        '''删除长者'''
        res = person_org_manage_service.delete_elder_info_byid(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_visit_situation')
    def __update_visit_situation(condition):
        '''探访服务情况 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.visit_situation, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.visit_situation, PermissionName.add)
        res = person_org_manage_service.update_visit_situation(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '探访服务情况', '新增/编辑探访服务情况')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_visit_situation_list')
    def __get_visit_situation_list(condition, page=None, count=None):
        '''探访服务情况 查询'''
        permission_data = security_service.judge_permission_query(
            FunctionName.visit_situation, PermissionName.query)
        res = person_org_manage_service.get_visit_situation_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_visit_situation_byid')
    def __delete_visit_situation_byid(condition):
        '''删除探访服务情况'''
        security_service.judge_permission_query(
            FunctionName.visit_situation, PermissionName.delete)
        res = person_org_manage_service.delete_visit_situation_byid(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '探访服务情况', '删除探访服务情况')
        return res

    @jsonrpc.method('IPersonOrgManageService.update_xfy_org_info')
    def __update_xfy_org_info(condition):
        '''新增幸福院运营机构信息'''
        res = person_org_manage_service.update_xfy_org_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '运营机构信息', '新增运营机构信息')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_xfy_org_info')
    def __get_xfy_org_info(condition, page=None, count=None):
        '''查询幸福院运营机构信息'''
        res = person_org_manage_service.get_xfy_org_info(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_xfy_org_info')
    def __delete_xfy_org_info(condition):
        '''删除幸福院运营机构信息'''
        res = person_org_manage_service.delete_xfy_org_info(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '运营机构信息', '删除运营机构信息')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_all_tj_info')
    def __get_all_tj_info(condition, page=None, count=None):
        '''总体统计'''
        res = person_org_manage_service.get_all_tj_info(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.yljg_tj')
    def __yljg_tj(condition, page=None, count=None):
        '''综合统计-养老机构'''
        res = person_org_manage_service.yljg_tj(N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.jjfws_tj')
    def __jjfws_tj(condition, page=None, count=None):
        '''综合统计-居家服务'''
        res = person_org_manage_service.jjfws_tj(N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_operation_situation')
    def __update_operation_situation(condition):
        res = person_org_manage_service.update_operation_situation(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '机构运营情况', '新增/编辑机构运营情况')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_operation_situation_list')
    def __get_operation_situation_list(condition, page=None, count=None):
        res = person_org_manage_service.get_operation_situation_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.del_operation_situation')
    def __del_operation_situation(id):
        res = person_org_manage_service.del_operation_situation(
            id)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '机构运营情况', '删除机构运营情况')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_meals_list_all')
    def __get_elder_meals_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.elder_meals, PermissionName.query)
        res = person_org_manage_service.get_elder_meals_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_elder_meals_list')
    def __get_elder_meals_list(condition, page=None, count=None):
        res = person_org_manage_service.get_elder_meals_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_elder_meals')
    def __update_elder_meals(condition):
        # 都是新增
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.elder_meals, PermissionName.add)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.elder_meals, PermissionName.add)
        res = person_org_manage_service.update_elder_meals(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '长者膳食', '新增/编辑长者膳食')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_happiness_station_product_list_all')
    def __get_happiness_station_product_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.happiness_station_product, PermissionName.query)
        res = person_org_manage_service.get_happiness_station_product_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_happiness_station_product_list')
    def __get_happiness_station_product_list(condition, page=None, count=None):
        res = person_org_manage_service.get_happiness_station_product_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_happiness_station_product')
    def __update_happiness_station_product(condition):
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.happiness_station_product, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.happiness_station_product, PermissionName.add)
        res = person_org_manage_service.update_happiness_station_product(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '幸福小站', '新增/编辑幸福小站')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_happiness_station_product')
    def __delete_happiness_station_product(id):
        security_service.judge_permission_query(
            FunctionName.happiness_station_product, PermissionName.delete)
        res = person_org_manage_service.delete_happiness_station_product(
            id)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '幸福小站', '删除幸福小站')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_happiness_year_self_assessment_list_all')
    def __get_happiness_year_self_assessment_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.happiness_self_year_assessment, PermissionName.query)
        res = person_org_manage_service.get_happiness_year_self_assessment_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_happiness_year_self_assessment_list')
    def __get_happiness_year_self_assessment_list(condition, page=None, count=None):
        res = person_org_manage_service.get_happiness_year_self_assessment_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_happiness_year_self_assessment')
    def __update_happiness_year_self_assessment(condition):
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.happiness_self_year_assessment, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.happiness_self_year_assessment, PermissionName.add)
        res = person_org_manage_service.update_happiness_year_self_assessment(
            condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '年度自评', '新建/编辑年度自评')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_happiness_year_self_assessment')
    def __delete_happiness_year_self_assessment(id):
        security_service.judge_permission_query(
            FunctionName.happiness_self_year_assessment, PermissionName.delete)
        res = person_org_manage_service.delete_happiness_year_self_assessment(
            id)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '年度自评', '删除年度自评')
        return res

    @jsonrpc.method('IPersonOrgManageService.get_fire_equipment_list_all')
    def __get_fire_equipment_list_all(condition, page=None, count=None):
        ''' 消防设施 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.fire_equipment, PermissionName.query)
        res = person_org_manage_service.get_fire_equipment_list(permission_data,
                                                                condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.get_fire_equipment_list')
    def __get_fire_equipment_list(condition, page=None, count=None):
        ''' 消防设施 '''
        res = person_org_manage_service.get_fire_equipment_list(N(),
                                                                condition, page, count)
        return res

    @jsonrpc.method('IPersonOrgManageService.update_fire_equipment')
    def __update_fire_equipment(condition):
        '''消防设施修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.fire_equipment, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.fire_equipment, PermissionName.add)
        res = person_org_manage_service.update_fire_equipment(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '消防设施管理', '新建/编辑消防设施管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.delete_fire_equipment')
    def __delete_fire_equipment(condition):
        '''消防设施删除'''
        __P = security_service.judge_permission_other(
            FunctionName.fire_equipment, PermissionName.delete)
        res = person_org_manage_service.delete_fire_equipment(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '幸福院', '消防设施管理', '删除消防设施管理')
        return res

    @jsonrpc.method('IPersonOrgManageService.import_elder')
    def __import_elder(data):
        res = person_org_manage_service.import_elder(data)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '长者档案', '导入长者档案')
        return res

    # 检测是否需要弹隐私政策
    @jsonrpc.method('IPersonOrgManageService.is_alert_yinsizhengce')
    def __is_alert_yinsizhengce(condition):
        res = person_org_manage_service.is_alert_yinsizhengce(condition)
        return res

    # 同意隐私政策
    @jsonrpc.method('IPersonOrgManageService.agree_yinsizhengce')
    def agree_yinsizhengce(condition):
        res = person_org_manage_service.agree_yinsizhengce(condition)
        return res

    @jsonrpc.method('IPersonOrgManageService.del_organization')
    def __del_organization(condition):
        '''长者管理 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.organization_name, PermissionName.delete)
        res = person_org_manage_service.del_organization(condition)
        person_org_manage_service.bill_manage_service.insert_logs(
            '机构养老', '组织机构', '删除组织机构')
        return res

    @jsonrpc.method('IPersonOrgManageService.is_ptcg')
    def __is_ptcg():
        '''判断是否平台超管'''
        res = person_org_manage_service.is_ptcg()
        return res
