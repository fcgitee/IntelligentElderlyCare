
# -*- coding: utf-8 -*-
from server.pao_python.pao.remote import UnauthorizedError
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_pub.shopping_mall_manage import ShoppingMallManageService
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    shopping_mall_manage_service = ShoppingMallManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IShoppingMallManageService.get_data_dictionary_list_all')
    def __get_data_dictionary_list_all(condition, page=None, count=None):
        ''' 获取数据字典 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.data_dictionary, PermissionName.query)
        res = shopping_mall_manage_service.get_data_dictionary_list(permission_data,
                                                                    condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_data_dictionary_list')
    def __get_data_dictionary_list(condition, page=None, count=None):
        ''' 获取数据字典 '''
        res = shopping_mall_manage_service.get_data_dictionary_list(N(),
                                                                    condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_data_dictionary')
    def __update_data_dictionary(condition):
        ''' 修改/新增数据字典 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.data_dictionary, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.data_dictionary, PermissionName.add)
        res = shopping_mall_manage_service.update_data_dictionary(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.delete_data_dictionary')
    def __delete_data_dictionary(condition):
        ''' 删除数据字典 '''
        __P = security_service.judge_permission_other(
            FunctionName.data_dictionary, PermissionName.delete)
        res = shopping_mall_manage_service.delete_data_dictionary(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_goods_detail_list_app')
    def __get_goods_detail_list_all(condition, page=None, count=None):
        ''' 获取商品明细 '''
        res = shopping_mall_manage_service.get_goods_detail_list_app(N(),
                                                                     condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_data_dictionary_childs_list_all')
    def __get_data_dictionary_childs_list_all(condition, page=None, count=None):
        ''' 获取数据字典【子】 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.data_dictionary, PermissionName.query)
        res = shopping_mall_manage_service.get_data_dictionary_childs_list(permission_data,
                                                                           condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_data_dictionary_childs_list')
    def __get_data_dictionary_childs_list(condition, page=None, count=None):
        ''' 获取数据字典【子】 '''
        res = shopping_mall_manage_service.get_data_dictionary_childs_list(N(),
                                                                           condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_goods_list_all')
    def __get_goods_list_all(condition, page=None, count=None):
        ''' 获取商品信息 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.products, PermissionName.query)
        res = shopping_mall_manage_service.get_goods_list(permission_data,
                                                          condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_goods_list')
    def __get_goods_list(condition, page=None, count=None):
        ''' 获取商品信息 '''
        res = shopping_mall_manage_service.get_goods_list(N(),
                                                          condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_services_list')
    def __get_services_list(condition, page=None, count=None):
        ''' 获取居家服务信息 '''
        res = shopping_mall_manage_service.get_services_list(N(),
                                                          condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_goods')
    def __update_goods(condition):
        ''' 修改/新增商品 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.products, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.products, PermissionName.add)
        res = shopping_mall_manage_service.update_goods(condition)
        return res
        
    @jsonrpc.method('IShoppingMallManageService.change_goods')
    def __change_goods(condition):
        ''' 批量修改/新增商品 '''
        __p = security_service.judge_permission_other(
            FunctionName.products, PermissionName.edit)
        res = shopping_mall_manage_service.change_goods(condition)
        return res
        
    @jsonrpc.method('IShoppingMallManageService.copy_goods')
    def __copy_goods(condition):
        ''' 复制商品 '''
        __p = security_service.judge_permission_other(
            FunctionName.products, PermissionName.add)
        res = shopping_mall_manage_service.copy_goods(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_goods_sh')
    def __update_goods_sh(condition):
        ''' 审核商品 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.productsSh, PermissionName.edit)
        res = shopping_mall_manage_service.update_goods_sh(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.delete_goods')
    def __delete_goods(condition):
        ''' 删除商品 '''
        __P = security_service.judge_permission_other(
            FunctionName.products, PermissionName.delete)
        res = shopping_mall_manage_service.delete_goods(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_order_remark')
    def __update_order_remark(condition):
        ''' 修改订单备注 '''
        res = shopping_mall_manage_service.update_order_remark(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_order_status')
    def __update_order_status(condition):
        ''' 修改订单状态 '''
        res = shopping_mall_manage_service.update_order_status(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_order')
    def __update_order(condition):
        ''' 修改订单信息 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.productOrders, PermissionName.edit)
        res = shopping_mall_manage_service.update_order(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_product_orders_list_all')
    def __get_product_orders_list_all(condition, page=None, count=None):
        ''' 获取商品订单信息 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.productOrders, PermissionName.query)
        res = shopping_mall_manage_service.get_product_orders_list(permission_data,
                                                                   condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_sp_financial_overview_list_all')
    def __get_sp_financial_overview_list_all(condition, page=None, count=None):
        ''' 服务商财务总览 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.spReconciliation, PermissionName.query)
        res = shopping_mall_manage_service.get_financial_overview_list(permission_data,
                                                                       condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_blocks_list_all')
    def __get_blocks_list_all(condition, page=None, count=None):
        ''' 首页板块信息 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.blocks, PermissionName.query)
        res = shopping_mall_manage_service.get_blocks_list(permission_data,
                                                           condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_app_blocks_list')
    def __get_app_blocks_list(condition):
        ''' APP首页板块信息 '''
        res = shopping_mall_manage_service.get_app_blocks_list(N(),
                                                               condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_block')
    def __update_block(condition):
        ''' 编辑板块信息 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.blocks, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.blocks, PermissionName.add)
        res = shopping_mall_manage_service.update_block(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.delete_block')
    def __delete_block(condition):
        ''' 删除板块 '''
        __P = security_service.judge_permission_other(
            FunctionName.blocks, PermissionName.delete)
        res = shopping_mall_manage_service.delete_block(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_recommend_list_all')
    def __get_recommend_list_all(condition, page=None, count=None):
        ''' 获取板块推荐列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.blocks, PermissionName.query)
        if condition.get('id') == 'parent':
            return {
                'result': [],
                'total': 0
            }
        res = shopping_mall_manage_service.get_recommend_list(permission_data,
                                                              condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_recommend_product_service_list')
    def __get_recommend_product_service_list(condition, page=None, count=None):
        ''' 获取板块推荐列表 '''
        res = shopping_mall_manage_service.get_recommend_product_service_list(N(),
                                                              condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.update_recommend')
    def __update_recommend(condition):
        ''' 编辑板块推荐 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.blocks, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.blocks, PermissionName.add)
        res = shopping_mall_manage_service.update_recommend(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.delete_recommend')
    def __delete_recommend(condition):
        ''' 删除板块推荐 '''
        __P = security_service.judge_permission_other(
            FunctionName.blocks, PermissionName.delete)
        res = shopping_mall_manage_service.delete_recommend(condition)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_service_type_list')
    def __get_service_type_list(condition, page=None, count=None):
        ''' 获取14类 '''
        res = shopping_mall_manage_service.get_service_type_list(N(),
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_service_product_list')
    def __get_service_product_list(condition, page=None, count=None):
        ''' 获取分类下的产品 '''
        res = shopping_mall_manage_service.get_service_product_list(N(),
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_sp_entry_record_list_all')
    def __get_sp_entry_record_list_all(condition, page=None, count=None):
        ''' 服务商入账记录 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.spReconciliation, PermissionName.query)
        res = shopping_mall_manage_service.get_entry_record_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_pt_financial_overview_list_all')
    def __get_pt_financial_overview_list_all(condition, page=None, count=None):
        ''' 平台财务总览 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.ptReconciliation, PermissionName.query)
        res = shopping_mall_manage_service.get_financial_overview_list(permission_data,
                                                                       condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_pt_entry_record_list_all')
    def __get_pt_entry_record_list_all(condition, page=None, count=None):
        ''' 平台入账记录 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.ptReconciliation, PermissionName.query)
        res = shopping_mall_manage_service.get_entry_record_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_capital_detail_list_all')
    def __get_capital_detail_list_all(condition, page=None, count=None):
        ''' 平台资金明细 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.ptReconciliation, PermissionName.query)
        res = shopping_mall_manage_service.get_capital_detail_list(permission_data,
                                                                   condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_separate_accounts_sh_list_all')
    def __get_separate_accounts_sh_list_all(condition, page=None, count=None):
        ''' 分账审核列表 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.separateAccountsSH, PermissionName.query)
        res = shopping_mall_manage_service.get_separate_accounts_sh_list(permission_data,
                                                                         condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_product_orders_list')
    def __get_product_orders_list(condition, page=None, count=None):
        ''' 获取商品订单信息 '''
        res = shopping_mall_manage_service.get_product_orders_list(N(),
                                                                   condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_products_list_by_organization')
    def __get_products_list_by_organization(condition, page=None, count=None):
        ''' 获取机构下的服务和商品 '''
        res = shopping_mall_manage_service.get_products_list_by_organization(N(),
                                                                             condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_consignee_address_detail_app')
    def __get_consignee_address_detail_app(condition, page=None, count=None):
        ''' 获取收货人信息 '''
        res = shopping_mall_manage_service.get_consignee_address_detail_app(N(),
                                                                            condition, page, count)
        return res

    @jsonrpc.method('IShoppingMallManageService.save_consignee_address_app')
    def __save_consignee_address_app(data):
        ''' 保存收货人地址信息 '''
        res = shopping_mall_manage_service.save_consignee_address_app(data)
        return res

    @jsonrpc.method('IShoppingMallManageService.get_default_consignee_data')
    def __get_default_consignee_data():
        ''' 获取默认收货人地址信息 '''
        res = shopping_mall_manage_service.get_default_consignee_data()
        return res

    @jsonrpc.method('IShoppingMallManageService.create_order')
    def __create_order(data):
        ''' 下单 '''
        res = shopping_mall_manage_service.create_order(data)
        return res
