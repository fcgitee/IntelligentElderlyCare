from ...pao_python.pao.service.security.security_service import RoleService
from ...service.buss_pub.user_func import UserServer
# -*- coding: utf-8 -*-

'''
用户相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    User_func = UserServer(db_addr, db_port, db_name, db_user, db_pwd,
                           inital_password, session)
    Role_func = RoleService(db_addr, db_port, db_name, db_user, db_pwd,
                            inital_password, session)

    @jsonrpc.method('IUserService.get_user_list')
    def __get_user_list(condition, page, count):
        res = User_func.get_user_list(condition, page, count)
        return res

    @jsonrpc.method('IUserService.get_current_user')
    def __get_current_user():
        res = User_func.get_current_user()
        return res

    @jsonrpc.method('IUserService.get_current_role')
    def __get_current_role():
        res = Role_func.get_current_role()
        return res

    @jsonrpc.method('IUserService.get_user_by_id')
    def __get_user_by_id(user_id):
        res = User_func.get_user_by_id(user_id)
        return res

    @jsonrpc.method('IUserService.update')
    def __update_user(user):
        user['caict_account'] = 'root'
        user['caict_pw'] = '123456'
        res = User_func.update_user(user)
        return res

    @jsonrpc.method('IUserService.delete')
    def __delete_user(userIDs):
        res = User_func.delete_user(userIDs)
        return res

    @jsonrpc.method('IUserService.reset_password')
    def __reset_password(user_id):
        res = User_func.reset_password(user_id)
        return res
