
# -*- coding: utf-8 -*-
from server.pao_python.pao.remote import UnauthorizedError
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_pub.sms_manage import SmsManageService
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, sms_data, request):
    sms_manage_service = SmsManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, sms_data, request)

    @jsonrpc.method('ISmsManageService.send_sms')
    def __send_sms(condition):
        '''发送短信'''
        res = sms_manage_service.send_sms(condition)
        return res
