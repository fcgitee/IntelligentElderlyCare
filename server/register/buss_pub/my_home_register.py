'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-03-04 17:08:50
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\person_org_manage_register.py
'''

from server.pao_python.pao.remote import UnauthorizedError

from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_pub.my_home import MyHome
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)
from ...service.security_module import SecurityModule, FunctionName, PermissionName


'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 8:51:29 pm
修改者: ymq(ymq) - <<email>>

说明
 1、
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    my_home_service = MyHome(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMyHomeService.get_my_room_list')
    def __get_my_room_list():
        '''获取我的房间列表'''
        security_service.judge_permission_query(
            FunctionName.my_home, PermissionName.query)
        res = my_home_service.get_my_room_list()
        return res

    @jsonrpc.method('IMyHomeService.update_room')
    def __update_room(condition):
        '''新建/编辑房间'''
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.my_home, Permission_name)
        res = my_home_service.update_room(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_room_type_list')
    def __get_room_type_list():
        '''获取我的房间类型列表'''
        res = my_home_service.get_room_type_list()
        return res

    @jsonrpc.method('IMyHomeService.get_my_room_detail')
    def __get_my_room_detail(condition):
        '''获取我的房间详情'''
        security_service.judge_permission_query(
            FunctionName.my_home, PermissionName.query)
        res = my_home_service.get_my_room_detail(condition)
        return res

    @jsonrpc.method('IMyHomeService.del_room')
    def __del_room(id):
        '''删除我的房间'''
        security_service.judge_permission_other(
            FunctionName.my_home, PermissionName.delete)
        res = my_home_service.del_room(id)
        return res

    @jsonrpc.method('IMyHomeService.get_all_my_room_list')
    def __get_all_my_room_list(condition):
        '''获取我的全部房间列表（不涉及设备）'''
        security_service.judge_permission_query(
            FunctionName.my_home, PermissionName.query)
        res = my_home_service.get_all_my_room_list(condition)
        return res

    @jsonrpc.method('IMyHomeService.add_my_host')
    def __add_host(condition):
        '''新增/编辑我的主机'''
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.my_host, Permission_name)
        res = my_home_service.add_my_host(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_host_list')
    def __get_my_host_list(condition):
        '''获取我的全部主机列表（不涉及设备）'''
        security_service.judge_permission_query(
            FunctionName.my_host, PermissionName.query)
        res = my_home_service.get_my_host_list(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_host_detail')
    def __get_my_host_detail(condition):
        '''获取我的主机详情'''
        security_service.judge_permission_query(
            FunctionName.my_host, PermissionName.query)
        res = my_home_service.get_my_host_detail(condition)
        return res

    @jsonrpc.method('IMyHomeService.del_my_host')
    def __del_my_host(host_id):
        '''删除我的主机'''
        security_service.judge_permission_other(
            FunctionName.my_host, PermissionName.delete)
        res = my_home_service.del_my_host(host_id)
        return res

    @jsonrpc.method('IMyHomeService.add_my_device')
    def __add_my_device(condition):
        '''新增/编辑我的设备'''
        Permission_name = PermissionName.add if condition.get(
            'id') else PermissionName.edit
        security_service.judge_permission_other(
            FunctionName.my_device, Permission_name)
        res = my_home_service.add_my_device(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_device_list')
    def __get_my_device_list(condition):
        '''获取我的全部设备列表（不涉及设备）'''
        security_service.judge_permission_query(
            FunctionName.my_device, PermissionName.query)
        res = my_home_service.get_my_device_list(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_device_detail')
    def __get_my_device_detail(condition):
        '''获取我的设备详情'''
        security_service.judge_permission_query(
            FunctionName.my_device, PermissionName.query)
        res = my_home_service.get_my_device_detail(condition)
        return res

    @jsonrpc.method('IMyHomeService.del_my_device')
    def __del_my_device(device_id):
        '''删除我的设备'''
        security_service.judge_permission_other(
            FunctionName.my_device, PermissionName.delete)
        res = my_home_service.del_my_device(device_id)
        return res

    @jsonrpc.method('IMyHomeService.get_device_type_list')
    def __get_device_list(condition):
        '''获取我的设备类型列表'''
        res = my_home_service.get_device_type_list(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_room_warning_detail')
    def __get_my_room_warning_detail(condition):
        '''获取我的房间详情（警报信息）'''
        security_service.judge_permission_query(
            FunctionName.my_home, PermissionName.query)
        res = my_home_service.get_my_room_warning_detail(condition)
        return res

    @jsonrpc.method('IMyHomeService.add_warn_info')
    def __get_my_room_warning_detail(condition):
        '''添加警报信息'''
        res = my_home_service.add_warn_info(condition)
        return res

    @jsonrpc.method('IMyHomeService.get_my_history_warn')
    def __get_my_history_warn(condition):
        '''我的历史警报信息'''
        security_service.judge_permission_query(
            FunctionName.my_warn, PermissionName.query)
        res = my_home_service.get_my_history_warn(condition)
        return res
