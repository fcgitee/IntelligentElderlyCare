'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-03-04 17:08:50
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\app_page_config_register.py
'''

from server.pao_python.pao.remote import UnauthorizedError

from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_pub.app_page_config_manage import AppPageConfigService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    app_page_config_service = AppPageConfigService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAppPageConfigService.get_app_page_config_list_all')
    def __get_app_page_config_list_all(condition, page=None, count=None):
        ''' app页面设置管理查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.appPage, PermissionName.query)
        res = app_page_config_service.get_app_page_config_list(permission_data,
                                                               condition, page, count)
        return res

    @jsonrpc.method('IAppPageConfigService.get_app_page_config_list')
    def __get_app_page_config_list(condition, page=None, count=None):
        ''' app页面设置管理查询 '''
        res = app_page_config_service.get_app_page_config_list(N(),
                                                               condition, page, count)
        return res

    @jsonrpc.method('IAppPageConfigService.update_app_page_config')
    def __update_app_page_config(condition):
        '''app页面设置 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.appPage, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.appPage, PermissionName.add)
        res = app_page_config_service.update_app_page_config(condition)
        app_page_config_service.bill_manage_service.insert_logs(
            '平台运营方', 'APP设置', 'app页面设置')
        return res

    @jsonrpc.method('IAppPageConfigService.delete_app_page_config')
    def __delete_app_page_config(condition):
        '''app页面设置 删除'''
        __P = security_service.judge_permission_other(
            FunctionName.appPage, PermissionName.delete)
        res = app_page_config_service.delete_app_page_config(condition)
        app_page_config_service.bill_manage_service.insert_logs(
            '平台运营方', 'APP设置', 'APP设置删除')
        return res

    @jsonrpc.method('IAppPageConfigService.get_app_config_list')
    def __get_app_config_list(condition, page=None, count=None):
        ''' app设置查询 '''
        res = app_page_config_service.get_app_config_list(N(),
                                                          condition, page, count)
        return res

    @jsonrpc.method('IAppPageConfigService.update_app_config')
    def __update_app_config(condition):
        '''app页面修改/新增'''
        if 'type' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.appConfig, PermissionName.edit)
        res = app_page_config_service.update_app_config(condition)
        app_page_config_service.bill_manage_service.insert_logs(
            '平台运营方', 'APP设置', '新增/编辑APP设置')
        return res

    @jsonrpc.method('IAppPageConfigService.get_app_privacy_agree_list')
    def __get_app_privacy_agree_list(condition, page=None, count=None):
        '''查询同意隐私'''
        permission_data = security_service.judge_permission_query(
            FunctionName.appConfig, PermissionName.edit)
        # permission_data = N()
        res = app_page_config_service.get_app_privacy_agree_list(permission_data,
                                                                 condition, page, count)
        return res

    @jsonrpc.method('IAppPageConfigService.delete_app_privacy_agree')
    def __delete_app_privacy_agree(condition):
        '''删除同意隐私'''
        __P = security_service.judge_permission_other(
            FunctionName.appConfig, PermissionName.edit)
        res = app_page_config_service.delete_app_privacy_agree(condition)
        app_page_config_service.bill_manage_service.insert_logs(
            '平台运营方', 'APP设置', '删除同意隐私')
        return res
