from ...service.buss_pub.excel_manage import ExcelManageService
import threading
import sys
import traceback
import json
from datetime import datetime
import psutil
'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-22 16:01:27
@LastEditTime: 2020-03-19 11:22:55
@LastEditors: Please set LastEditors
'''
# -*- coding: utf-8 -*-
'''
导入导出管理相关函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, web_path, upload_file):
    excel_manage_func = ExcelManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, web_path, upload_file)

    @jsonrpc.method('IExcelService.export_excel')
    def __export_excel(method, condition):
        '''导出'''
        res = excel_manage_func.export_excel(method, condition)
        return res

    @jsonrpc.method('IExcelService.import_excel_admin')
    def __import_excel_admin():
        '''导入'''
        res = excel_manage_func.import_excel_admin()
        return res

    @jsonrpc.method('IExcelService.import_excel_org')
    def __import_excel_org():
        '''导入'''
        res = excel_manage_func.import_excel_org()
        return res

    @jsonrpc.method('IExcelService.import_excel_user')
    def __import_excel_user():
        '''导入'''
        res = excel_manage_func.import_excel_user()
        return res

    @jsonrpc.method('IExcelService.change_user_data')
    def __change_user_data():
        ''' 建立已加入的用户及机构的账户信息 '''
        res = excel_manage_func.change_user_data()
        return res

    @jsonrpc.method('IExcelService.import_excel_activity')
    def __import_excel_activity():
        '''导入活动'''
        res = excel_manage_func.import_excel_activity()
        return res

    @jsonrpc.method('IExcelService.change_org_photo')
    def __change_org_photo():
        '''导入活动'''
        res = excel_manage_func.change_org_photo()
        return res

    @jsonrpc.method('IExcelService.import_excel_service_record')
    def __import_excel_service_record():
        '''导入服务记录'''
        res = excel_manage_func.import_excel_service_record()
        return res

    @jsonrpc.method('IExcelService.import_excel_user_nh')
    def __import_excel_user_nh(url):
        '''导入南海长者资料'''
        res = excel_manage_func.import_excel_user_nh(url)
        return res

    @jsonrpc.method('IExcelService.import_excel_servicer')
    def __import_excel_servicer():
        '''导入服务人员'''
        res = excel_manage_func.import_excel_servicer()
        return res

    @jsonrpc.method('IExcelService.change_old_age_mes')
    def __change_old_age_mes():
        '''修改高龄津贴申请数据'''
        res = excel_manage_func.change_old_age_mes()
        return res

    @jsonrpc.method('IExcelService.import_excel_xf_product')
    def __import_excel_xf_product():
        '''导入幸福小栈产品接口'''
        res = excel_manage_func.import_excel_xf_product()
        return res

    @jsonrpc.method('IExcelService.import_picture_files')
    def __import_picture_files(file_path):
        '''导入文件夹图片接口'''
        res = excel_manage_func.import_picture_files(file_path)
        return res

    @jsonrpc.method('IExcelService.get_url_upload_url')
    def __get_url_upload_url(file_path):
        '''通过Url获取图片'''
        res = excel_manage_func.get_url_upload_url(file_path)
        return res

    @jsonrpc.method('IExcelService.insert_government_subsidy_user')
    def __insert_government_subsidy_user():
        '''通过Url获取图片'''
        res = excel_manage_func.get_admin()
        return res

    @jsonrpc.method('IExcelService.import_excel_monitor')
    def __import_excel_monitor():
        '''导入摄像头信息'''
        res = excel_manage_func.import_excel_monitor()
        return res

    @jsonrpc.method('IExcelService.import_permission')
    def __import_permission():
        '''导入权限'''
        res = excel_manage_func.import_permission()
        return res

    @jsonrpc.method('IExcelService.import_groups')
    def _import_groups():
        '''导入活动小组'''
        res = excel_manage_func.import_groups()
        return res

    @jsonrpc.method('IExcelService.update_user_id_card')
    def __update_user_id_card():
        '''通过Url获取图片'''
        res = excel_manage_func.xfh_bind_phone()
        return res

    @jsonrpc.method('IExcelService.threading')
    def _threading():
        threads = threading.enumerate()
        threads_num = threading.active_count()
      #print('threads_num', threads_num)
        data_res = {}
        curren_frames = sys._current_frames()
        for th in threads:
          #print('thread_name', th.name)
            if th.is_alive() and th.ident in curren_frames.keys():
                stack = traceback.extract_stack(curren_frames[th.ident])
                stack_format = traceback.format_list(stack)
              # print(json.dumps(stack_format))
                data_res[th.name] = stack_format
        return {'threads_num': threads_num, 'data_res': data_res}

    @jsonrpc.method('IExcelService.threadingInfo')
    def _threadingInfo(thID):
        stack = traceback.extract_stack(sys._current_frames()[thID])
        stack_format = traceback.format_list(stack)
        return stack_format

    @jsonrpc.method('IExcelService.sysInfo')
    def _sysInfo():
        def bytes2human(n):
            symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
            prefix = {}
            for i, s in enumerate(symbols):
                prefix[s] = 1 << (i + 1) * 10
            for s in reversed(symbols):
                if n >= prefix[s]:
                    value = float(n) / prefix[s]
                    return '%.1f%s' % (value, s)

            return "%sB" % n

        def get_cpu_info():
            '''
            cpu info
            '''

            logical_cpu_count = psutil.cpu_count()
            cpu_count = psutil.cpu_count(logical=False)
            cpu_percent = psutil.cpu_percent(interval=1)

            return dict(cpu_count=cpu_count, logical_cpu_count=logical_cpu_count, cpu_percent=cpu_percent)

        def get_memory_info():
            virtual_mem = psutil.virtual_memory()
            mem_total = bytes2human(virtual_mem.total)
            mem_percent = virtual_mem.percent
            mem_free = bytes2human(
                virtual_mem.free + (virtual_mem.buffers if hasattr(virtual_mem, 'buffers') else 0) + (virtual_mem.cached if hasattr(virtual_mem, 'cached') else 0))
            mem_used = bytes2human(virtual_mem.used)

            swap_mem = psutil.swap_memory()
            swap_mem_total = bytes2human(swap_mem.total)
            swap_mem_percent = swap_mem.percent
            swap_mem_free = bytes2human(swap_mem.free)
            swap_mem_used = bytes2human(swap_mem.used)

            return dict(
                memory=dict(mem_total=mem_total, mem_percent=mem_percent,
                            mem_free=mem_free, mem_used=mem_used),
                swap_memory=dict(mem_total=swap_mem_total, mem_percent=swap_mem_percent,
                                 mem_free=swap_mem_free, mem_used=swap_mem_used)
            )

        def get_boot_info():
            boot_time = datetime.fromtimestamp(
                psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")
            return dict(boot_time=boot_time)

        def collect_monitor_data():
            data = {}
            data.update(get_boot_info())
            data.update(get_cpu_info())
            data.update(get_memory_info())

            return data

        sysInfo = collect_monitor_data()

        return sysInfo

    @jsonrpc.method('IExcelService.processInfo')
    def _sysInfo():
        def get_process_info():
            pids = psutil.pids()
            process_info = []
            for pid in pids:
                try:
                    p = psutil.Process(pid)
                    name = None
                    try:
                        name = p.name()
                    except Exception as e:
                        pass

                    exe = None
                    try:
                        exe = p.exe()
                    except Exception as e:
                        pass

                    cwd = None
                    try:
                        cwd = p.cwd()
                    except Exception as e:
                        pass

                    cmdline = None
                    try:
                        cmdline = p.cmdline()
                    except Exception as e:
                        pass

                    ppid = None
                    try:
                        ppid = p.ppid()
                    except Exception as e:
                        pass

                    children_id = None
                    try:
                        children = p.children()
                        children_id = []
                        for child in children:
                            children_id.append(child.pid)
                    except Exception as e:
                        pass

                    status = None
                    try:
                        status = p.status()
                    except Exception as e:
                        pass

                    username = None
                    try:
                        username = p.username()
                    except Exception as e:
                        pass

                    create_time = None
                    try:
                        create_time = p.create_time()
                    except Exception as e:
                        pass

                    terminal = None
                    try:
                        terminal = p.terminal()
                    except Exception as e:
                        pass

                    cpu_times = None
                    try:
                        cpu_times = p.cpu_times()
                    except Exception as e:
                        pass

                    memory_info = None
                    try:
                        memory_info = p.memory_info()
                    except Exception as e:
                        pass

                    open_files = None
                    try:
                        open_files = p.open_files()
                    except Exception as e:
                        pass

                    connections = None
                    try:
                        connections = p.connections()
                    except Exception as e:
                        pass

                    num_threads = None
                    try:
                        num_threads = p.num_threads()
                    except Exception as e:
                        pass

                    threads = None
                    try:
                        threads = p.threads()
                    except Exception as e:
                        pass

                    environ = None
                    try:
                        environ = p.environ()
                    except Exception as e:
                        pass

                    process_info.append(dict(
                        name=name,
                        exe=exe,
                        cwd=cwd,
                        cmdline=cmdline,
                        ppid=ppid,
                        children_id=children_id,
                        status=status,
                        username=username,
                        create_time=create_time,
                        terminal=terminal,
                        cpu_times=cpu_times,
                        memory_info=memory_info,
                        open_files=open_files,
                        connections=connections,
                        num_threads=num_threads,
                        threads=threads,
                        environ=environ
                    ))
                except Exception as e:
                    pass
                  # print(e)

            return process_info
        processInfo = get_process_info()

        return processInfo
