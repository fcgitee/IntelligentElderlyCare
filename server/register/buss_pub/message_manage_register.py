# -*- coding: utf-8 -*-
'''
信息系统
'''
from ...service.buss_pub.message_manage import MessageManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    message_manage_func = MessageManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMessageService.add_new_message')
    def __add_new_message(data):
        '''保存一条信息'''
        res = message_manage_func.add_new_message(data)
        return res

    @jsonrpc.method('IMessageService.add_new_message_type')
    def __add_new_message_type(data):
        '''保存一条信息类型'''
        res = message_manage_func.add_new_message_type(data)
        return res

    @jsonrpc.method('IMessageService.set_message_already_read')
    def __set_message_already_read(data):
        '''设置一条信息为已读'''
        res = message_manage_func.set_message_already_read(data)
        return res

    @jsonrpc.method('IMessageService.get_all_message_list')
    def __get_all_message_list(condition, page=None, count=None):
        '''获取信息列表'''
        res = message_manage_func.get_all_message_list(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IMessageService.get_unread_message_list')
    def __get_unread_message_list(condition, page=None, count=None):
        '''获取未读信息列表'''
        res = message_manage_func.get_unread_message_list(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IMessageService.get_message_type_list')
    def __get_message_type_list(condition, page=None, count=None):
        '''获取信息类型列表'''
        res = message_manage_func.get_message_type_list(
            condition, page=None, count=None)
        return res

    @jsonrpc.method('IMessageService.get_app_online_message_list')
    def __get_app_online_message_list(condition, page=None, count=None):
        '''获取app在线咨询列表'''
        res = message_manage_func.get_app_online_message_list(
            condition, page, count)
        return res

    @jsonrpc.method('IMessageService.update_app_online_message')
    def __update_app_online_message(data):
        '''保存一条app在线咨询'''
        res = message_manage_func.update_app_online_message(data)
        message_manage_func.bill_manage_server.insert_logs(
            '平台运营方', '在线咨询', '保存一条app在线咨询')
        return res

    @jsonrpc.method('IMessageService.update_app_online_message_remark')
    def __update_app_online_message_remark(data):
        '''修改备注'''
        __p = security_service.judge_permission_other(
            FunctionName.appOnline, PermissionName.edit)
        res = message_manage_func.update_app_online_message_remark(data)
        message_manage_func.bill_manage_server.insert_logs(
            '平台运营方', '在线咨询', '修改备注')
        return res
