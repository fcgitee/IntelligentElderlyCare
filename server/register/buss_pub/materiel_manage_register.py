'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2020-03-04 17:08:50
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\materiel_manage_register.py
'''

from server.pao_python.pao.remote import UnauthorizedError

from ...pao_python.pao.service.data.mongo_db import N
from ...service.buss_pub.materiel_manage import MaterielManageService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    materiel_manage_service = MaterielManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMaterielService.get_units_list_all')
    def __get_units_list_all(condition, page=None, count=None):
        ''' 物料单位查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.units, PermissionName.query)
        res = materiel_manage_service.get_units_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.get_units_list')
    def __get_units_list(condition, page=None, count=None):
        ''' 物料单位查询 '''
        res = materiel_manage_service.get_units_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.update_units')
    def __update_units(condition):
        '''物料单位修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.units, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.units, PermissionName.add)
        res = materiel_manage_service.update_units(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '计量单位', '新建/编辑计量单位')
        return res

    @jsonrpc.method('IMaterielService.delete_units')
    def __delete_units(condition):
        '''物料单位删除'''
        __P = security_service.judge_permission_other(
            FunctionName.units, PermissionName.delete)
        res = materiel_manage_service.delete_units(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '计量单位', '删除计量单位')
        return res

    @jsonrpc.method('IMaterielService.get_thing_sort_list_all')
    def __get_thing_sort_list_all(condition, page=None, count=None):
        ''' 物料分类查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.thing_sort, PermissionName.query)
        res = materiel_manage_service.get_thing_sort_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.get_thing_sort_list')
    def __get_thing_sort_list(condition, page=None, count=None):
        ''' 物料分类查询 '''
        res = materiel_manage_service.get_thing_sort_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.update_thing_sort')
    def __update_thing_sort(condition):
        '''物料分类修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.thing_sort, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.thing_sort, PermissionName.add)
        res = materiel_manage_service.update_thing_sort(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '物料分类', '新建/编辑物料分类')
        return res

    @jsonrpc.method('IMaterielService.delete_thing_sort')
    def __delete_thing_sort(condition):
        '''物料分类删除'''
        __P = security_service.judge_permission_other(
            FunctionName.thing_sort, PermissionName.delete)
        res = materiel_manage_service.delete_thing_sort(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '物料分类', '删除物料分类')
        return res

    @jsonrpc.method('IMaterielService.get_thing_archives_list_all')
    def __get_thing_archives_list_all(condition, page=None, count=None):
        ''' 物料档案查询 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.thing_archives, PermissionName.query)
        res = materiel_manage_service.get_thing_archives_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.get_thing_archives_list')
    def __get_thing_archives_list(condition, page=None, count=None):
        ''' 物料档案查询 '''
        res = materiel_manage_service.get_thing_archives_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IMaterielService.update_thing_archives')
    def __update_thing_archives(condition):
        '''物料档案修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.thing_archives, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.thing_archives, PermissionName.add)
        res = materiel_manage_service.update_thing_archives(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '物料档案', '新建/编辑物料档案')
        return res

    @jsonrpc.method('IMaterielService.delete_thing_archives')
    def __delete_thing_archives(condition):
        '''物料档案删除'''
        __P = security_service.judge_permission_other(
            FunctionName.thing_archives, PermissionName.delete)
        res = materiel_manage_service.delete_thing_archives(condition)
        materiel_manage_service.bill_manage_service.insert_logs(
            '机构养老', '物料档案', '删除物料档案')
        return res
