
'''
版权：Copyright (c) 2019 China

创建日期：Tuesday September 17th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Tuesday, 17th September 2019 11:52:07 am
修改者: ymq(ymq) - <<email>>

说明
 1、
'''
from ...service.buss_pub.security_login import LoginService
from ...service.security_module import (FunctionName, PermissionName,
                                        SecurityModule)
from ...service.common import (get_current_user_name)
from server.service.buss_pub.bill_manage import (BillManageService)


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, sms_data):
    login_service = LoginService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, sms_data)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    # bill_manage_server2 = BillManageService(
    #     db_addr, db_port, db_name, db_user, db_pwd, inital_password, {
    #         "user_id": '23b3630a-d92c-11e9-8b9a-983b8f0bcd67',
    #         'organization_id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67'
    #     }
    # )

    @jsonrpc.method('ILoginService.login')
    def __login(data):
        res = login_service.login(data)
        login_service.bill_manage_service.insert_logs(
            '', '登录系统', data['account_name'] + '账号登录')
        return res

    @jsonrpc.method('ILoginService.get_role_list')
    def __get_role_list():
        res = login_service.get_role_list()
        return res

    @jsonrpc.method('ILoginService.set_role_org_session')
    def __set_role_area_session(role_id, org_id, org_name, org_type):
        res = login_service.set_role_org_session(
            role_id, org_id, org_name, org_type)
        return res

    @jsonrpc.method('ILoginService.add_user_data')
    def __add_user_data(data):
        res = login_service.add_user_data(data)
        return res

    @jsonrpc.method('ILoginService.add_role_data')
    def __add_role_data(data):
        res = login_service.add_role_data(data)
        return res

    @jsonrpc.method('ILoginService.add_permission_data')
    def __add_permission_data(data):
        res = login_service.add_permission_data(data)
        return res

    @jsonrpc.method('ILoginService.add_set_role_data')
    def __add_set_role_data(data):
        res = login_service.add_set_role_data(data)
        return res

    @jsonrpc.method('ILoginService.get_function_list')
    def __get_function_list():
        res = login_service.get_function_list()
        return res

    @jsonrpc.method('ILoginService.get_notice_list')
    def __get_notice_list():
        res = login_service.get_notice_list()
        return res

    @jsonrpc.method('ILoginService.get_message_list_new')
    def __get_message_list_new(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.get_message_list_new, PermissionName.query)
        res = login_service.get_message_list_new(
            permission_data, security_service, FunctionName, PermissionName, condition, page, count)
        return res

    @jsonrpc.method('ISecurityService.bind_mobile')
    def __bind_mobile(mobile, identify_code):
        res = login_service.bind_mobile(mobile, identify_code)
        login_service.bill_manage_service.insert_logs(
            '', '绑定手机号码', '绑定手机号码')
        # bill_manage_server2.insert_logs(
        #     '', '绑定手机号码', get_current_user_name(session) + '绑定手机号码')
        return res

    @jsonrpc.method('ISecurityService.modify_email')
    def __modify_email(new_email, identify_code):
        res = login_service.modify_email(new_email, identify_code)
        login_service.bill_manage_service.insert_logs(
            '', '修改邮箱', '修改邮箱')
        # bill_manage_server2.insert_logs(
        #     '', '修改邮箱', get_current_user_name(session) + '修改邮箱')
        return res

    @jsonrpc.method('ISecurityService.check')
    def __check(target, target_type, identify_code):
        # 修改密码前验证身份
        res = login_service.check(target, target_type, identify_code)
        return res

    @jsonrpc.method('ILoginService.modify_password')
    def __modify_password(data):
        # 修改密码
        res = login_service.modify_password(data)
        login_service.bill_manage_service.insert_logs(
            '', '修改密码', '修改密码')
        # bill_manage_server2.insert_logs(
        #     '', '修改密码', get_current_user_name(session) + '修改密码')
        return res

    @jsonrpc.method('ILoginService.modify_user_info')
    def __modify_user_info(new_info):
        # 验证身份通过后修改个人信息
        res = login_service.modify_user_info(new_info)
        login_service.bill_manage_service.insert_logs(
            '', '修改个人信息', '修改个人信息')
        # bill_manage_server2.insert_logs(
        #     '', '修改个人信息', get_current_user_name(session) + '修改个人信息')
        return res

    @jsonrpc.method('ILoginService.register')
    def __register(data):
        res = login_service.register(data)
        # bill_manage_server2.insert_logs(
        #     '', '账户注册', get_current_user_name(session) + '账户注册')
        return res

    # 忘记密码，重设
    @jsonrpc.method('ILoginService.retrieve_password')
    def __retrieve_password(data):
        res = login_service.retrieve_password(data)
        login_service.bill_manage_service.insert_logs(
            '', '忘记密码', '重设密码')
        return res

    # 重新绑定手机
    @jsonrpc.method('ILoginService.modify_mobile')
    def __modify_mobile(data):
        res = login_service.modify_mobile(data)
        login_service.bill_manage_service.insert_logs(
            '', '重新绑定手机', '重新绑定手机')
        return res

    # 首次修改密码
    @jsonrpc.method('ILoginService.first_modify_password')
    def __first_modify_password(data):
        res = login_service.first_modify_password(data)
        login_service.bill_manage_service.insert_logs(
            '', '首次修改密码', '首次修改密码')
        # bill_manage_server2.insert_logs(
        #     '', '首次修改密码', get_current_user_name(session) + '首次修改密码')
        return res
