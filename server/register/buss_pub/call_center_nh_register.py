
# -*- coding: utf-8 -*-
from server.pao_python.pao.remote import UnauthorizedError
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_pub.call_center_nh_manage import CallCenterNhService
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, request):
    call_center_nh_service = CallCenterNhService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session, request)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICallCenterNhManageService.api')
    def __api(apiType, condition):
        ''' 统一接口 '''
        res = call_center_nh_service.api(apiType, condition)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_record_list_all')
    def __get_record_list_all(condition, page=None, count=None):
        ''' 呼叫中心 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.call_center_nh, PermissionName.query)
        res = call_center_nh_service.get_record_list(permission_data,
                                                     condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_record_list')
    def __get_record_list(condition={}, page=None, count=None):
        ''' 呼叫中心 '''
        res = call_center_nh_service.get_record_list(N(),
                                                     condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.update_record')
    def __update_record(condition):
        ''' 呼叫中心 '''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.call_center_nh, PermissionName.edit)
            res = call_center_nh_service.update_record(condition)
            return res
        return {
            'code': 500,
            'msg': '非法操作'
        }

    @jsonrpc.method('ICallCenterNhManageService.get_call_center_zh_list_all')
    def __get_call_center_zh_list_all(condition, page=None, count=None):
        ''' 移动呼叫中心帐号 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.call_center_zh, PermissionName.query)
        res = call_center_nh_service.get_call_center_zh_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_call_center_zh_list')
    def __get_call_center_zh_list(condition, page=None, count=None):
        ''' 移动呼叫中心帐号 '''
        res = call_center_nh_service.get_call_center_zh_list(N(),
                                                             condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.update_call_center_zh')
    def __update_call_center_zh(condition):
        '''移动呼叫中心帐号修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.call_center_zh, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.call_center_zh, PermissionName.add)
        res = call_center_nh_service.update_call_center_zh(condition)
        call_center_nh_service.bill_manage_service.insert_logs(
            '平台运营方', '呼叫中心', '移动呼叫中心帐号修改/新增')
        return res

    @jsonrpc.method('ICallCenterNhManageService.delete_call_center_zh')
    def __delete_call_center_zh(condition):
        '''移动呼叫中心帐号删除'''
        __P = security_service.judge_permission_other(
            FunctionName.call_center_zh, PermissionName.delete)
        res = call_center_nh_service.delete_call_center_zh(condition)
        call_center_nh_service.bill_manage_service.insert_logs(
            '平台运营方', '呼叫中心', '移动呼叫中心帐号删除')
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_agh_list')
    def __get_agh_list(condition, page=None, count=None):
        ''' 爱关怀列表 '''
        res = call_center_nh_service.get_agh_list(N(),
                                                  condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.update_agh')
    def __update_agh(condition):
        '''更新爱关怀'''
        res = call_center_nh_service.update_agh(condition)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_device_warning_list_all')
    def __get_device_warning_list_all(condition, page=None, count=None):
        ''' 适老化设备警报 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.device_warning, PermissionName.query)
        res = call_center_nh_service.get_device_warning_list(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_device_warning_list')
    def __get_device_warning_list(condition, page=None, count=None):
        ''' 适老化设备警报 '''
        res = call_center_nh_service.get_device_warning_list(N(),
                                                             condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_device_manage_list_all')
    def __get_device_manage_list_all(condition, page=None, count=None):
        ''' 适老化设备管理 '''
        permission_data = security_service.judge_permission_query(
            FunctionName.device_warning, PermissionName.query)
        res = call_center_nh_service.get_device_manage_list(permission_data,
                                                            condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_device_manage_list')
    def __get_device_manage_list(condition, page=None, count=None):
        ''' 适老化设备管理 '''
        res = call_center_nh_service.get_device_manage_list(N(),
                                                            condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_edler_device_list')
    def __get_edler_device_list(condition, page=None, count=None):
        ''' 根据长者ID获取适老化设备管理 '''
        res = call_center_nh_service.get_edler_device_list(N(),
                                                           condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_device_warn_list')
    def __get_device_warn_list(condition, page=None, count=None):
        ''' 根据设备ID获取警报信息 '''
        res = call_center_nh_service.get_device_warn_list(N(),
                                                          condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_call_center_log_list_all')
    def __get_call_center_log_list_all(condition, page=None, count=None):
        ''' 获取呼叫中心日志 '''
        res = call_center_nh_service.get_call_center_log_list(N(),
                                                              condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_call_center_push_list')
    def __get_call_center_push_list(condition, page=None, count=None):
        ''' 获取呼叫推送列表 '''
        res = call_center_nh_service.get_call_center_push_list(N(),
                                                               condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.get_seat_operation_list')
    def __get_seat_operation_list(condition, page=None, count=None):
        '''获取坐席运营统计列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.seatOperation, PermissionName.query)
        res = call_center_nh_service.get_seat_operation_list(
            condition, page, count)
        return res

    @jsonrpc.method('ICallCenterNhManageService.update_yingda')
    def __update_yingda(condition):
        '''获取坐席运营统计列表'''
        res = call_center_nh_service.update_yingda(
            condition)
        return res
