'''
@Author: your name
@Date: 2019-10-14 17:34:58
@LastEditTime : 2019-12-23 19:36:00
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\role_register.py
'''
from ...service.buss_pub.role import Role
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from server.pao_python.pao.remote import UnauthorizedError
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    role_service = Role(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IPermissionService.get_role_user_list_all')
    def __get_role_user_list_all(condition, page=None, count=None):
        '''查询角色设置列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.role_user_all, PermissionName.query)
        res = role_service.get_role_user_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_role_user_list_look')
    def __get_role_list_look(condition, page=None, count=None):
        '''查询角色设置列表'''
        permission_data = security_service.judge_permission_query(
            FunctionName.role_user_look, PermissionName.query)
        res = role_service.get_role_user_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.delete_role_user')
    def __delete_role_user(condition):
        '''删除角色设置列表'''
        security_service.judge_permission_other(
            FunctionName.role_user_all, PermissionName.delete)
        res = role_service.delete_role_user(condition)
        role_service.bill_manage_service.insert_logs(
            '', '用户管理', '删除用户管理')
        return res

    # @jsonrpc.method('IPermissionService.get_role')
    # def __get_role_list(condition, page=None, count=None):
    #     '''查询单条角色设置列表'''
    #     permission_data = security_service.judge_permission_query(
    #         FunctionName.role_manage_all, PermissionName.query)
    #     res = role_service.get_role(
    #         permission_data, condition, page=None, count=None)
    #     return res

    @jsonrpc.method('IPermissionService.update_role_user')
    def __update_role_user(condition):
        '''角色设置 修改/新增'''
        permiss_name = PermissionName.edit if 'id' in condition.keys() else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.role_manage_all, permiss_name)
        res = role_service.update_role_user(condition)
        role_service.bill_manage_service.insert_logs(
            '', '用户管理', '新建/编辑用户管理')
        return res

    @jsonrpc.method('IPermissionService.update_role')
    def __update_role(condition):
        '''角色设置 修改/新增'''
        if 'id' in list(condition.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.role_manage_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.role_manage_all, PermissionName.add)
        res = role_service.update_role(condition)
        role_service.bill_manage_service.insert_logs(
            '', '角色管理', '新建/编辑角色管理')
        return res

    @jsonrpc.method('IPermissionService.delete_roles')
    def __delete_role(condition):
        '''角色设置 删除'''
        __p = security_service.judge_permission_other(
            FunctionName.role_manage_all, PermissionName.delete)
        res = role_service.delete_role(condition)
        role_service.bill_manage_service.insert_logs(
            '', '角色管理', '删除角色管理')
        return res

    # 角色列表
    @jsonrpc.method('IPermissionService.get_role_list_all')
    def __get_role_all_list(condition={}, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.role_manage_all, PermissionName.query)
        res = role_service.get_role_list_only(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_sys_use_staticstics_list')
    def __get_sys_use_staticstics_list(condition, page=None, count=None):
        security_service.judge_permission_query(
            FunctionName.sys_use_staticstics, PermissionName.query)
        res = role_service.get_sys_use_staticstics_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_staticstics_user')
    def __get_staticstics_user(condition, page=None, count=None):
        security_service.judge_permission_query(
            FunctionName.sys_use_staticstics, PermissionName.query)
        res = role_service.get_staticstics_user(
            condition, page, count)
        return res

    # 角色列表
    @jsonrpc.method('IPermissionService.get_role_list')
    def __get_role_list(condition={}, page=None, count=None):
        res = role_service.get_role_all_list(
            N(), condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_role_list_look')
    def __get_role_list_look(condition={}, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.role_manage_look, PermissionName.query)
        res = role_service.get_role_all_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_permiss_list')
    def __get_role_all_list(condition={}, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.role_manage_all, PermissionName.query)
        res = role_service.get_permiss_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_role_type')
    def __get_role_type():
        res = role_service.get_role_type()
        return res

    @jsonrpc.method('IPermissionService.get_role_single')
    def __get_role_single(condition):
        security_service.judge_permission_query(
            FunctionName.role_manage_all, PermissionName.query)
        res = role_service.get_role_single(condition)
        return res

    @jsonrpc.method('IPermissionService.get_cur_person_role')
    def __get_cur_person_role(condition={}, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = role_service.get_role_all_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_cur_person_role_pure_list')
    def __get_cur_person_role_pure_list(condition={}, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = role_service.get_cur_person_role_pure_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IPermissionService.get_menu_permmission_list')
    def __get_menu_permmission_list(condition={}, page=None, count=None):
        res = role_service.get_menu_permmission_list()
        return res

    @jsonrpc.method('IPermissionService.get_role_menu_permmission_list')
    def __get_role_menu_permmission_list(condition={}, page=None, count=None):
        res = role_service.get_role_menu_permmission_list(condition)
        return res

    @jsonrpc.method('IPermissionService.download_sys_use_staticstics')
    def __get_role_menu_permmission_list(condition):
        res = role_service.download_sys_use_staticstics(condition)
        return res

    @jsonrpc.method('IPermissionService.get_current_user_role_name')
    def __get_current_user_role_name():
        res = role_service.get_current_user_role_name()
        return res
