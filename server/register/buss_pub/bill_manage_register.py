# -*- coding: utf-8 -*-
'''
单据管理相关函数注册
'''
from ...service.buss_pub.bill_manage import BillManageService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd,  inital_password, session):
    bill_manage_func = BillManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IBillManageService.get_signature_bill_by_personId_all')
    def __get_service_provider_list(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.signature_bill_all, PermissionName.query)
        res = bill_manage_func.get_signatureBill_by_personId(permission_data,
                                                             condition, page, count)
        return res

    @jsonrpc.method('IBillManageService.get_signature_bill_by_initiatorId_look')
    def __get_signature_bill_by_initiatorId(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.signature_bill_all, PermissionName.query)
        res = bill_manage_func.get_signature_bill_by_initiatorId(
            condition, page, count)
        return res

    @jsonrpc.method('IBillManageService.signature_bill_by_personId')
    def __signature_bill_by_personId(signatureContent):
        permission_data = security_service.judge_permission_query(
            FunctionName.signature_bill_all, PermissionName.edit)
        res = bill_manage_func.signature_bill_by_personId(signatureContent)
        return res

    @jsonrpc.method('IBillManageService.refuse_bill_by_personId')
    def __refuse_bill_by_personId(signatureContent):
        permission_data = security_service.judge_permission_query(
            FunctionName.signature_bill_all, PermissionName.edit)
        res = bill_manage_func.refuse_bill_by_personId(signatureContent)
        return res
