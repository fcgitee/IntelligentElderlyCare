'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:34:59
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\personnel_organizational_register.py
'''
from ...service.buss_pub.personnel_organizational import PersonnelOrganizationalService

# -*- coding: utf-8 -*-
'''
人员及组织机构函数注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    personnel_organizational = PersonnelOrganizationalService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IPersonnelOrganizationalService.get_login_role_list')
    def __get_login_role_list():
        res = personnel_organizational.get_login_role_list()
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.get_personnel_list')
    def __get_nursing_list(condition, page=None, count=None):
        res = personnel_organizational.get_personnel_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.update_personnel')
    def __update_user(personnel):
        res = personnel_organizational.update_personnel(personnel)
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.delete_personnel')
    def __delete_user(personnel_ids):
        res = personnel_organizational.delete_personnel(personnel_ids)
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.get_organizational_list')
    def __get_nursing_list(condition, page=None, count=None):
        res = personnel_organizational.get_organizational_list(
            condition, page, count)
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.update_organizational')
    def __update_user(organizational):
        res = personnel_organizational.update_organizational(
            organizational)
        return res

    @jsonrpc.method('IPersonnelOrganizationalService.delete_organizational')
    def __delete_user(organizational_ids):
        res = personnel_organizational.delete_organizational(
            organizational_ids)
        return res

    # @jsonrpc.method('IPersonnelOrganizationalService.get_organization_by_active')
    # def __get_organization_by_active():
    #     res = personnel_organizational.get_organization_by_active()
    #     return res

    @jsonrpc.method('IPersonnelOrganizationalService.get_user_session')
    def __get_user_session():
        res = personnel_organizational.get_user_session()
        return res
