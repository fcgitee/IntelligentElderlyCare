
# -*- coding: utf-8 -*-
from server.pao_python.pao.remote import UnauthorizedError
from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_pub.ledger_account_manage import LedgerAccountManageService
from ...pao_python.pao.service.data.mongo_db import N


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    ledger_account_manage_service = LedgerAccountManageService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)


    # @jsonrpc.method('IShoppingMallManageService.get_separate_accounts_sh_list_all')
    # def __get_separate_accounts_sh_list_all(condition, page=None, count=None):
    #     ''' 分账审核列表 '''
    #     permission_data = security_service.judge_permission_query(
    #         FunctionName.separateAccountsSH, PermissionName.query)
    #     res = shopping_mall_manage_service.get_separate_accounts_sh_list(permission_data,
    #                                                                      condition, page, count)
    #     return res

    @jsonrpc.method('ILedgerAccountManageService.confirm_payment')
    def __confirm_payment(condition, page=None, count=None):
        ''' 获取商品订单信息 '''
        res = ledger_account_manage_service.confirm_payment(N(),
                                                                   condition, page, count)
        return res

    # @jsonrpc.method('IShoppingMallManageService.confirm_payment')
    # def __confirm_payment(condition, page=None, count=None):
    #     ''' 获取机构下的服务和商品 '''
    #     res = ledger_account_manage_service.confirm_payment(N(),
    #                                                                          condition, page, count)
    #     return res

    # @jsonrpc.method('IShoppingMallManageService.confirm_payment')
    # def __confirm_payment(condition, page=None, count=None):
    #     ''' 获取收货人信息 '''
    #     res = ledger_account_manage_service.confirm_payment(N(),
    #                                                                         condition, page, count)
    #     return res

    # @jsonrpc.method('IShoppingMallManageService.save_consignee_address_app')
    # def __save_consignee_address_app(data):
    #     ''' 保存收货人地址信息 '''
    #     res = ledger_account_manage_service.save_consignee_address_app(data)
    #     return res
