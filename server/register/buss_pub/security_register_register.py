'''
@Author: your name
@Date: 2019-10-29 14:30:31
@LastEditTime: 2019-12-05 15:26:16
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_pub\security_register_register.py
'''
from ...service.buss_pub.security_register import RegisterService
from ...service.common import (get_current_user_name)


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    register_service = RegisterService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IRegisterService.register')
    def __register(data):
        res = register_service.register(data)
        register_service.bill_manage_service.insert_logs(
            '', '注册', get_current_user_name(session) + '注册')
        return res
