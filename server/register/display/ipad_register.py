from ...service.display.ipad import IpadService
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 15:38:21
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\display\ipad_register.py
'''
'''
版权：Copyright (c) 2019 China

创建日期：Saturday September 7th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Saturday, 7th September 2019 7:29:23 pm
修改者: ymq(ymq) - <<email>>

说明
 1、
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    ipad_service = IpadService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('TabletControlService.get_welfare_center_list')
    def __get_welfare_center_list():
        res = ipad_service.get_welfare_center_list()
        return res

    @jsonrpc.method('TabletControlService.get_community_home_list')
    def __get_community_home_list():
        res = ipad_service.get_community_home_list()
        return res

    @jsonrpc.method('TabletControlService.get_service_provider_list')
    def __get_service_provider_list():
        res = ipad_service.get_service_provider_list()
        return res

    @jsonrpc.method('TabletControlService.get_service_personal_list')
    def __get_service_personal_list():
        res = ipad_service.get_service_personal_list()
        return res

    @jsonrpc.method('TabletControlService.get_elder_list')
    def __get_elder_list():
        res = ipad_service.get_elder_list()
        return res
