# @Author: linjinkun
# @Date: 2019-09-05 09:48:21
# @Last Modified by:   linjinkun
# @Last Modified time: 2019-09-05 09:48:21

from ...service.display.display_kun import DisplayShowService
from ...pao_python.pao.remote import JsonRpc2Error


class ErrorCode:
    COMMAND_NOT_FOUND = -36001


class ErrorMessage:
    COMMAND_NOT_FOUND = "命令不存在"


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    '''服务注册'''

    # 测试服务
    display_show = DisplayShowService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    # 命令列表
    command = {
        ############ 服务人员统计 #############
        'spe_statistics_total_quantity': display_show.spe_statistics_total_quantity,
        'spe_statistics_welfare_centre_quantity': display_show.spe_statistics_welfare_centre_quantity,
        'spe_statistics_community_quantity': display_show.spe_statistics_community_quantity,
        'spe_statistics_community_quantity': display_show.spe_statistics_community_personal_quantity,
        'spe_statistics_education_service_personal_quantity': display_show.spe_statistics_education_service_personal_quantity,
        'spe_statistics_org_service_personal_quantity': display_show.spe_statistics_org_service_personal_quantity,
        'spe_statistics_age_service_personal_quantity': display_show.spe_statistics_age_service_personal_quantity,
        'spe_statistics_sex_service_personal_quantity': display_show.spe_statistics_sex_service_personal_quantity,
        'spe_statistics_service_personal_evaluate_quantity': display_show.spe_statistics_service_personal_evaluate_quantity,
        'spe_statistics_education_evaluate_quantity': display_show.spe_statistics_education_evaluate_quantity,
        'spe_statistics_age_evaluate_quantity': display_show.spe_statistics_age_evaluate_quantity,
        'spe_statistics_sex_evaluate_quantity': display_show.spe_statistics_sex_evaluate_quantity,
        'spe_statistics_service_personal_top10_quantity': display_show.spe_statistics_service_personal_top10_quantity,
        'spe_statistics_education_quantity': display_show.spe_statistics_education_quantity,
        'spe_statistics_age_quantity': display_show.spe_statistics_age_quantity,
        'spe_statistics_sex_quantity': display_show.spe_statistics_sex_quantity,
        'spe_statistics_service_personal_income_top10_quantity': display_show.spe_statistics_service_personal_income_top10_quantity,
        'spe_statistics_education_income_quantity': display_show.spe_statistics_education_income_quantity,
        'spe_statistics_age_income_quantity': display_show.spe_statistics_age_income_quantity,
        'spe_statistics_sex_income_quantity': display_show.spe_statistics_sex_income_quantity,
        'cop_statistics_street_fee_quantity': display_show.cop_statistics_street_fee_quantity,
        'cop_statistics_elder_fee_top10_quantity': display_show.cop_statistics_elder_fee_top10_quantity,
        ################### 养老机构 明细###################
        'org_detail_total_quantity': display_show.org_detail_total_quantity,
        'org_detail_bed_quantity': display_show.org_detail_bed_quantity,
        'org_detail_elder_quantity': display_show.org_detail_elder_quantity,
        'org_detail_welfare_centre': display_show.org_detail_welfare_centre,
        'org_detail_picture_list': display_show.org_detail_picture_list,
        'org_detail_activity_picture_list': display_show.org_detail_activity_picture_list,
        'org_detail_video_surveillance': display_show.org_detail_video_surveillance,
        'org_detail_personal_list': display_show.org_detail_personal_list,
        'org_detail_service_products_list': display_show.org_detail_service_products_list,
        'org_detail_service_products': display_show.org_detail_service_products,
        'org_detail_sex_quantity': display_show.org_detail_sex_quantity,
        'org_detail_education_quantity': display_show.org_detail_education_quantity,
        'org_detail_age_quantity': display_show.org_detail_age_quantity,
        'org_detail_ability_quantity': display_show.org_detail_ability_quantity,
        'org_detail_sex_personnel_quantity': display_show.org_detail_sex_personnel_quantity,
        'org_detail_age_personnel_quantity': display_show.org_detail_age_personnel_quantity,
        'org_detail_education_personnel_quantity': display_show.org_detail_education_personnel_quantity,
        'org_detail_qualifications_personnel_quantity': display_show.org_detail_qualifications_personnel_quantity,
        'org_detail_post_personnel_quantity': display_show.org_detail_post_personnel_quantity,
        'org_detail_live_bed_quantity': display_show.org_detail_live_bed_quantity,
        'org_detail_time_live_quantity': display_show.org_detail_time_live_quantity,
        # 'org_detail_total_quantity':display_show.org_detail_total_quantity,
        # 'org_detail_service_personal_total_quantity':display_show.org_detail_service_personal_total_quantity,
        # 'org_detail_profit_total_quantity':display_show.org_detail_profit_total_quantity
        ################### 综合统计 ###################
        'cop_statistics_elder_age_fee_quantity': display_show.cop_statistics_elder_age_fee_quantity,
        'cop_statistics_service_personal_evaluate_top12_quantity': display_show.cop_statistics_service_personal_evaluate_top12_quantity,
        'cop_statistics_elder_evaluate_top13_quantity': display_show.cop_statistics_elder_evaluate_top13_quantity,
        'cop_statistics_age_evaluate_quantity': display_show.cop_statistics_age_evaluate_quantity,
        'cop_statistics_street_evaluate_quantity': display_show.cop_statistics_street_evaluate_quantity,
        'cop_statistics_item_top10_quantity': display_show.cop_statistics_item_top10_quantity,
        'cop_statistics_org_top10_quantity': display_show.cop_statistics_org_top10_quantity,
        'cop_statistics_fee_total_quantity': display_show.cop_statistics_fee_total_quantity,
        'cop_statistics_total_quantity': display_show.cop_statistics_total_quantity,
        'cop_statistics_service_personal_top10_quantity': display_show.cop_statistics_service_personal_top10_quantity,
        'cop_statistics_elder_top10_quantity': display_show.cop_statistics_elder_top10_quantity,
        'cop_statistics_elder_age_quantity': display_show.cop_statistics_elder_age_quantity,
        'cop_statistics_street_quantity': display_show.cop_statistics_street_quantity,
        'cop_statistics_item_fee_top10_quantity': display_show.cop_statistics_item_fee_top10_quantity,
        'cop_statistics_org_fee_top10_quantity': display_show.cop_statistics_org_fee_top10_quantity,
        'cop_statistics_service_personal_fee_top10_quantity': display_show.cop_statistics_service_personal_fee_top10_quantity,
        'cop_statistics_org_evaluate_top11_quantity': display_show.cop_statistics_org_evaluate_top11_quantity,
        'cop_statistics_item_evaluate_top10_quantity': display_show.cop_statistics_item_evaluate_top10_quantity,
        'cop_statistics_evaluate_quantity': display_show.cop_statistics_evaluate_quantity,
        'org_statistics_health_indicators_personnel_quantity': display_show.org_statistics_health_indicators_personnel_quantity,
        'org_statistics_qualifications_personnel_quantity': display_show.org_statistics_qualifications_personnel_quantity,
        'org_statistics_identity_affiliation_personnel_quantity': display_show.org_statistics_identity_affiliation_personnel_quantity,
        'eld_statistics_ability_elder_quantity': display_show.eld_statistics_ability_elder_quantity

    }

    @jsonrpc.method('Kun.query')
    def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
        # 判断命令是否存在
        if commandID not in list(command.keys()):
            raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,
                                ErrorMessage.COMMAND_NOT_FOUND)

        return command[commandID](paramValues, startIndex, maxCount)
