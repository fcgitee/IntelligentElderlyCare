# from ...service.display.display_hins import DisplayService
# from ...pao_python.pao.remote import JsonRpc2Error

# class ErrorCode:
#     COMMAND_NOT_FOUND = -36001

# class ErrorMessage:
#     COMMAND_NOT_FOUND = "命令不存在"


# def register(jsonrpc, db_addr, db_port, db_name,inital_password, session):
#     '''服务注册'''

#     # 测试服务
#     display_service=DisplayService(db_addr, db_port, db_name,inital_password,session)

#     # 命令列表
#     command={
#             'com_statistics_total_quantity':display_service.com_statistics_total_quantity, # 按行政区划和用户类型统计幸福院数量
#             'com_statistics_service_quantity':display_service.com_statistics_service_quantity, #社区养老 统计 幸福院服务总次数
#             'com_statistics_nature_quantity':display_service.com_statistics_nature_quantity, #社区养老 统计 幸福院数量（性质）
#             'com_statistics_star_quantity':display_service.com_statistics_star_quantity, #社区养老 统计 幸福院数量（星级）
#             'com_statistics_service_personal_quantity':display_service.com_statistics_service_personal_quantity, #社区养老 统计 服务人员总人数
#             'com_statistics_community_score':display_service.com_statistics_community_score, #社区养老 统计 服务评分(幸福院)
#             'com_statistics_org_score':display_service.com_statistics_org_score, #社区养老 统计 服务评分(运营机构)
#             'com_statistics_service_personal_score':display_service.com_statistics_service_personal_score, #社区养老 统计 服务评分(服务人员)
#             'com_statistics_type_activity_quantity':display_service.com_statistics_type_activity_quantity, #社区养老 统计 活动次数(活动类型)
#             'com_statistics_age_activity_quantity':display_service.com_statistics_age_activity_quantity, # 社区养老 统计 活动次数(年龄段)
#             'com_statistics_education_activity_quantity':display_service.com_statistics_education_activity_quantity, # 社区养老 统计 活动次数(学历)
#             'com_statistics_education_attend_class_quantity':display_service.com_statistics_education_attend_class_quantity,#社区养老 统计 上课次数(学历)
#             # 'com_detail_picture_list':display_service.com_detail_picture_list, #社区养老 明细 图片列表
#             'com_detail_total_quantity':display_service.com_detail_total_quantity, #社区养老 明细 总服务次数
#             'org_detail_service_personal_total_quantity':display_service.org_detail_service_personal_total_quantity,# 社区养老 明细 总服务人数
#             'com_detail_picture_list':display_service.com_detail_picture_list, #社区养老 明细 图片列表
#             'org_detail_activity_picture_list':display_service.org_detail_activity_picture_list, #社区养老 明细 活动图片列表
#             'com_detail_video_surveillance':display_service.com_detail_video_surveillance, #社区养老 明细 摄像头列表（视频监控）
#             'com_detail_personal_list':display_service.com_detail_personal_list,#社区养老 明细 人员风采轮播
#             'com_detail_activity':display_service.com_detail_activity, #社区养老 明细 最新活动情况
#             'com_statistics_curriculum_attend_class_quantity':display_service.com_statistics_curriculum_attend_class_quantity,#社区养老 统计 上课次数(课程)
#             'org_map_location_welfare_centre':display_service.org_map_location_welfare_centre
#             }

#     @jsonrpc.method('HBigScreenService.query')
#     def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
#         # 判断命令是否存在
#         if commandID not in list(command.keys()):
#             raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND, ErrorMessage.COMMAND_NOT_FOUND)

#         return command[commandID](paramValues, startIndex, maxCount)
