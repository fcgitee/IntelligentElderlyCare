'''
版权：Copyright (c) 2019 China

创建日期：Thursday September 5th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Thursday, 10th October 2019 5:04:09 pm
修改者: 何伟聪(hewc) - avenger463010817@live.com

说明
 1、
'''
from ...service.display.display_ye import DisplayService
from ...pao_python.pao.remote import JsonRpc2Error
from ...service.display.display_kun import DisplayShowService
from ...service.display.display_hins import ShowDisplayService
from ...service.display.display_ny import DisplayNyService
from ...service.display.display_hwg import DisplayHwgService
from ...service.display.display_qins import QinsDisplayService


class ErrorCode:
    COMMAND_NOT_FOUND = -36001


class ErrorMessage:
    COMMAND_NOT_FOUND = "命令不存在"


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd,inital_password, session):
    '''服务注册'''

    # 测试服务
    display_service = DisplayService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)
    display_show = DisplayShowService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)
    display_service_show = ShowDisplayService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)
    display_service_ny = DisplayNyService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)
    display_service_hwg = DisplayHwgService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)
    display_service_qins = QinsDisplayService(
        db_addr, db_port, db_name, db_user, db_pwd,inital_password, session)

    # 命令列表
    command = {
        'test': display_service.cal_age_test,
        'org_statistics_total_quantity': display_service.org_statistics_total_quantity,
        'org_statistics_check_in_total_quantity': display_service.org_statistics_check_in_total_quantity,
        'org_statistics_service_personal_total_quantity': display_service.org_statistics_service_personal_total_quantity,
        'org_statistics_nature_quantity': display_service.org_statistics_nature_quantity,
        'org_statistics_welfare_centre_bed_quantity': display_service.org_statistics_welfare_centre_bed_quantity,
        'org_statistics_live_bed_quantity': display_service.org_statistics_live_bed_quantity,
        'org_statistics_education_personnel_quantity': display_service.org_statistics_education_personnel_quantity,
        'org_statistics_sex_personnel_quantity': display_service.org_statistics_sex_personnel_quantity,
        'org_statistics_age_personnel_quantity': display_service.org_statistics_age_personnel_quantity,
        'org_statistics_sex_subscribe_quantity': display_service.org_statistics_sex_subscribe_quantity,
        'org_statistics_age_subscribe_quantity': display_service.org_statistics_age_subscribe_quantity,
        'org_detail_welfare_centre_room_status': display_service.org_detail_welfare_centre_room_status,
        'sep_statistics_total_quantity': display_service.sep_statistics_total_quantity,
        'sep_statistics_service_personal_quantity': display_service.sep_statistics_service_personal_quantity,
        'sep_statistics_service_quantity': display_service.sep_statistics_service_quantity,
        'sep_statistics_service_provider_score_top10_quantity': display_service.sep_statistics_service_provider_score_top10_quantity,
        'sep_statistics_nature_score_quantity': display_service.sep_statistics_nature_score_quantity,
        'sep_statistics_service_provider_service_personal_top10_quantity': display_service.sep_statistics_service_provider_service_personal_top10_quantity,
        'sep_statistics_nature_service_personal_quantity': display_service.sep_statistics_nature_service_personal_quantity,
        'sep_statistics_service_provider_service_top10_quantity': display_service.sep_statistics_service_provider_service_top10_quantity,
        'sep_statistics_nature_service_quantity': display_service.sep_statistics_nature_service_quantity,
        'sep_statistics_service_provider_income_top10_quantity': display_service.sep_statistics_service_provider_income_top10_quantity,
        'sep_statistics_nature_income_quantity': display_service.sep_statistics_nature_income_quantity,
        'sep_detail_total_quantity': display_service.sep_detail_total_quantity,
        'sep_detail_education_quantity': display_service.sep_detail_education_quantity,
        'sep_detail_income_quantity': display_service.sep_detail_income_quantity,
        'sep_detail_service_provider': display_service.sep_detail_service_provider,
        'sep_detail_activity_picture_list': display_service.sep_detail_activity_picture_list,
        'org_statistics_ability_personnel_quantity': display_service.org_statistics_ability_personnel_quantity,
        'org_statistics_ability_subscribe_quantity': display_service.org_statistics_ability_subscribe_quantity,
        'eld_statistics_total_quantity': display_service.eld_statistics_total_quantity,
        'eld_statistics_welfare_centre_elder_total_quantity': display_service.eld_statistics_welfare_centre_elder_total_quantity,
        'eld_statistics_community_elder_total_quantity': display_service.eld_statistics_community_elder_total_quantity,
        'eld_statistics_street_elder_quantity': display_service.eld_statistics_street_elder_quantity,
        'eld_statistics_pension_mode_elder_quantity': display_service.eld_statistics_pension_mode_elder_quantity,
        'eld_statistics_sex_elder_quantity': display_service.eld_statistics_sex_elder_quantity,
        'eld_statistics_age_elder_quantity': display_service.eld_statistics_age_elder_quantity,
        'eld_statistics_protein_elder_quantity': display_service.eld_statistics_protein_elder_quantity,
        'eld_statistics_triglyceride_elder_quantity': display_service.eld_statistics_triglyceride_elder_quantity,
        'eld_statistics_temperature_elder_quantity': display_service.eld_statistics_temperature_elder_quantity,
        'eld_statistics_pulse_elder_quantity': display_service.eld_statistics_pulse_elder_quantity,
        'eld_statistics_water_elder_quantity': display_service.eld_statistics_water_elder_quantity,
        'eld_statistics_blood_pressure_elder_quantity': display_service.eld_statistics_blood_pressure_elder_quantity,
        'eld_statistics_elder_service_top10_quantity': display_service.eld_statistics_elder_service_top10_quantity,
        'eld_statistics_protein_service_quantity': display_service.eld_statistics_protein_service_quantity,
        'eld_statistics_triglyceride_service_quantity': display_service.eld_statistics_triglyceride_service_quantity,
        'eld_statistics_temperature_service_quantity': display_service.eld_statistics_temperature_service_quantity,
        'eld_statistics_pulse_service_quantity': display_service.eld_statistics_pulse_service_quantity,
        'eld_statistics_water_service_quantity': display_service.eld_statistics_water_service_quantity,
        'eld_statistics_blood_pressure_service_quantity': display_service.eld_statistics_blood_pressure_service_quantity,
        'eld_statistics_age_service_quantity': display_service.eld_statistics_age_service_quantity,
        'eld_statistics_elder_buy_money_top10_quantity': display_service.eld_statistics_elder_buy_money_top10_quantity,
        'eld_statistics_protein_buy_money_quantity': display_service.eld_statistics_protein_buy_money_quantity,
        'eld_statistics_triglyceride_buy_money_quantity': display_service.eld_statistics_triglyceride_buy_money_quantity,
        'eld_statistics_temperature_buy_money_quantity': display_service.eld_statistics_temperature_buy_money_quantity,
        'eld_statistics_pulse_buy_money_quantity': display_service.eld_statistics_pulse_buy_money_quantity,
        'eld_statistics_water_buy_money_quantity': display_service.eld_statistics_water_buy_money_quantity,
        'eld_statistics_blood_pressure_buy_money_quantity': display_service.eld_statistics_blood_pressure_buy_money_quantity,
        'eld_statistics_age_buy_money_quantity': display_service.eld_statistics_age_buy_money_quantity,
        # 'cop_statistics_service_provider_total_quantity': display_service.sep_statistics_total_quantity,
        'cop_statistics_service_personal_quantity': display_service.sep_statistics_service_personal_quantity,
        'cop_statistics_elder_quantity': display_service.eld_statistics_total_quantity,
        'org_statistics_street_quantity': display_service.org_statistics_street_quantity,
        'org_statistics_street_bed_quantity': display_service.org_statistics_street_bed_quantity,
        'org_statistics_time_subscribe_quantity': display_service.org_statistics_time_subscribe_quantity,
        'sep_statistics_street_service_provider_quantity': display_service.sep_statistics_street_service_provider_quantity,
        'sep_statistics_time_service_provider_quantity': display_service.sep_statistics_time_service_provider_quantity,
        'sep_map_location_service_provider': display_service.sep_map_location_service_provider,
        'sep_detail_promotional_video_surveillance': display_service.sep_detail_promotional_video_surveillance,
        'sep_detail_video_surveillance': display_service.sep_detail_video_surveillance,
        'sep_detail_picture_list': display_service.sep_detail_picture_list,
        'sep_card_location_service_provider': display_service.sep_card_location_service_provider,
        'eld_statistics_time_elder_quantity': display_service.eld_statistics_time_elder_quantity,
        'eld_statistics_time_elder_age': display_service.eld_statistics_time_elder_age,
        'eld_statistics_time_elder_sex': display_service.eld_statistics_time_elder_sex,



        #### kun #####
        ############ 服务人员统计 #############
        'spe_statistics_total_quantity': display_show.spe_statistics_total_quantity,
        'spe_statistics_welfare_centre_quantity': display_show.spe_statistics_welfare_centre_quantity,
        'spe_statistics_community_personal_quantity': display_show.spe_statistics_community_personal_quantity,
        'spe_statistics_community_quantity': display_show.spe_statistics_community_quantity,
        'spe_statistics_education_service_personal_quantity': display_show.spe_statistics_education_service_personal_quantity,
        'spe_statistics_org_service_personal_quantity': display_show.spe_statistics_org_service_personal_quantity,
        'spe_statistics_age_service_personal_quantity': display_show.spe_statistics_age_service_personal_quantity,
        'spe_statistics_sex_service_personal_quantity': display_show.spe_statistics_sex_service_personal_quantity,
        'spe_statistics_service_personal_evaluate_quantity': display_show.spe_statistics_service_personal_evaluate_quantity,
        'spe_statistics_education_evaluate_quantity': display_show.spe_statistics_education_evaluate_quantity,
        'spe_statistics_age_evaluate_quantity': display_show.spe_statistics_age_evaluate_quantity,
        'spe_statistics_sex_evaluate_quantity': display_show.spe_statistics_sex_evaluate_quantity,
        'spe_statistics_service_personal_top10_quantity': display_show.spe_statistics_service_personal_top10_quantity,
        'spe_statistics_education_quantity': display_show.spe_statistics_education_quantity,
        'spe_statistics_age_quantity': display_show.spe_statistics_age_quantity,
        'spe_statistics_sex_quantity': display_show.spe_statistics_sex_quantity,
        'spe_statistics_service_personal_income_top10_quantity': display_show.spe_statistics_service_personal_income_top10_quantity,
        'spe_statistics_education_income_quantity': display_show.spe_statistics_education_income_quantity,
        'spe_statistics_age_income_quantity': display_show.spe_statistics_age_income_quantity,
        'spe_statistics_sex_income_quantity': display_show.spe_statistics_sex_income_quantity,
        'cop_statistics_street_fee_quantity': display_show.cop_statistics_street_fee_quantity,
        'cop_statistics_elder_fee_top10_quantity': display_show.cop_statistics_elder_fee_top10_quantity,
        'org_statistics_identity_affiliation_personnel_quantity': display_show.org_statistics_identity_affiliation_personnel_quantity,

        ################### 养老机构 明细###################
        'org_detail_total_quantity': display_show.org_detail_total_quantity,
        'org_detail_bed_quantity': display_show.org_detail_bed_quantity,
        'org_detail_elder_quantity': display_show.org_detail_elder_quantity,
        'org_detail_welfare_centre': display_show.org_detail_welfare_centre,
        'org_detail_picture_list': display_show.org_detail_picture_list,
        'org_detail_activity_picture_list': display_show.org_detail_activity_picture_list,
        'org_detail_video_surveillance': display_show.org_detail_video_surveillance,
        'org_detail_personal_list': display_show.org_detail_personal_list,
        'org_detail_service_products_list': display_show.org_detail_service_products_list,
        'org_detail_service_products': display_show.org_detail_service_products,
        'org_detail_sex_quantity': display_show.org_detail_sex_quantity,
        'org_detail_education_quantity': display_show.org_detail_education_quantity,
        'org_detail_age_quantity': display_show.org_detail_age_quantity,
        'org_detail_ability_quantity': display_show.org_detail_ability_quantity,
        'org_detail_sex_personnel_quantity': display_show.org_detail_sex_personnel_quantity,
        'org_detail_age_personnel_quantity': display_show.org_detail_age_personnel_quantity,
        'org_detail_education_personnel_quantity': display_show.org_detail_education_personnel_quantity,
        'org_detail_qualifications_personnel_quantity': display_show.org_detail_qualifications_personnel_quantity,
        'org_detail_post_personnel_quantity': display_show.org_detail_post_personnel_quantity,
        'org_detail_live_bed_quantity': display_show.org_detail_live_bed_quantity,
        'org_detail_time_live_quantity': display_show.org_detail_time_live_quantity,
        'org_detail_service_price_picture': display_show.org_detail_service_price_picture,
        # 'org_detail_total_quantity':display_show.org_detail_total_quantity,
        # 'org_detail_service_personal_total_quantity':display_show.org_detail_service_personal_total_quantity,
        # 'org_detail_profit_total_quantity':display_show.org_detail_profit_total_quantity
        ################### 综合统计 ###################
        'cop_statistics_elder_age_fee_quantity': display_show.cop_statistics_elder_age_fee_quantity,
        'cop_statistics_service_personal_evaluate_top12_quantity': display_show.cop_statistics_service_personal_evaluate_top12_quantity,
        'cop_statistics_elder_evaluate_top13_quantity': display_show.cop_statistics_elder_evaluate_top13_quantity,
        'cop_statistics_age_evaluate_quantity': display_show.cop_statistics_age_evaluate_quantity,
        'cop_statistics_street_evaluate_quantity': display_show.cop_statistics_street_evaluate_quantity,
        'cop_statistics_item_top10_quantity': display_show.cop_statistics_item_top10_quantity,
        'cop_statistics_org_top10_quantity': display_show.cop_statistics_org_top10_quantity,
        'cop_statistics_fee_total_quantity': display_show.cop_statistics_fee_total_quantity,
        'cop_statistics_total_quantity': display_show.cop_statistics_total_quantity,
        'cop_statistics_service_personal_top10_quantity': display_show.cop_statistics_service_personal_top10_quantity,
        'cop_statistics_elder_top10_quantity': display_show.cop_statistics_elder_top10_quantity,
        'cop_statistics_elder_age_quantity': display_show.cop_statistics_elder_age_quantity,
        'cop_statistics_street_quantity': display_show.cop_statistics_street_quantity,
        'cop_statistics_item_fee_top10_quantity': display_show.cop_statistics_item_fee_top10_quantity,
        'cop_statistics_org_fee_top10_quantity': display_show.cop_statistics_org_fee_top10_quantity,
        'cop_statistics_service_personal_fee_top10_quantity': display_show.cop_statistics_service_personal_fee_top10_quantity,
        'cop_statistics_org_evaluate_top11_quantity': display_show.cop_statistics_org_evaluate_top11_quantity,
        'cop_statistics_item_evaluate_top10_quantity': display_show.cop_statistics_item_evaluate_top10_quantity,
        'cop_statistics_evaluate_quantity': display_show.cop_statistics_evaluate_quantity,
        'org_statistics_health_indicators_personnel_quantity': display_show.org_statistics_health_indicators_personnel_quantity,
        'org_statistics_qualifications_personnel_quantity': display_show.org_statistics_qualifications_personnel_quantity,
        'cop_statistics_elder_type_fee_quantity': display_show.cop_statistics_elder_type_fee_quantity,
        'cop_statistics_elder_type_quantity': display_show.cop_statistics_elder_type_quantity,
        'eld_statistics_ability_elder_quantity': display_show.eld_statistics_ability_elder_quantity,
        'cop_statistics_elder_type_evaluate_quantity': display_show.cop_statistics_elder_type_evaluate_quantity,

        ############################################HIns#################################################

        # 按行政区划和用户类型统计幸福院数量
        'com_statistics_total_quantity': display_service_show.com_statistics_total_quantity,
        # 社区养老 统计 幸福院服务总次数
        'com_statistics_service_quantity': display_service_show.com_statistics_service_quantity,
        # 社区养老 统计 幸福院数量（性质）
        'com_statistics_nature_quantity': display_service_show.com_statistics_nature_quantity,
        # 社区养老 统计 幸福院数量（星级）
        'com_statistics_star_quantity': display_service_show.com_statistics_star_quantity,
        'com_statistics_service_personal_quantity': display_service_show.com_statistics_service_personal_quantity,  # 社区养老 统计 服务人员总人数
        # 社区养老 统计 服务评分(幸福院)
        'com_statistics_community_score': display_service_show.com_statistics_community_score,
        # 社区养老 统计 服务评分(运营机构)
        'com_statistics_org_score': display_service_show.com_statistics_org_score,
        # 社区养老 统计 服务评分(服务人员)
        'com_statistics_service_personal_score': display_service_show.com_statistics_service_personal_score,
        # 社区养老 统计 活动次数(活动类型)
        'com_statistics_type_activity_quantity': display_service_show.com_statistics_type_activity_quantity,
        # 社区养老 统计 活动次数(年龄段)
        'com_statistics_age_activity_quantity': display_service_show.com_statistics_age_activity_quantity,
        # 社区养老 统计 活动次数(学历)
        'com_statistics_education_activity_quantity': display_service_show.com_statistics_education_activity_quantity,
        # 社区养老 统计 上课次数(学历)
        'com_statistics_education_attend_class_quantity': display_service_show.com_statistics_education_attend_class_quantity,
        'com_detail_total_quantity': display_service_show.com_detail_total_quantity,  # 社区养老 明细 总服务次数
        'org_detail_service_personal_total_quantity': display_service_show.org_detail_service_personal_total_quantity,  # 社区养老 明细 总服务人数
        'com_detail_picture_list': display_service_show.com_detail_picture_list,  # 社区养老 明细 图片列表
        'com_detail_activity_picture_list': display_service_show.com_detail_activity_picture_list,  # 社区养老 明细 活动图片列表
        'com_detail_video_surveillance': display_service_show.com_detail_video_surveillance,  # 社区养老 明细 摄像头列表（视频监控）
        'com_detail_personal_list': display_service_show.com_detail_personal_list,  # 社区养老 明细 人员风采轮播
        'com_detail_activity': display_service_show.com_detail_activity,  # 社区养老 明细 最新活动情况
        # 社区养老 统计 上课次数(课程)
        'com_statistics_curriculum_attend_class_quantity': display_service_show.com_statistics_curriculum_attend_class_quantity,
        'org_map_location_welfare_centre': display_service_show.org_map_location_welfare_centre,
        'com_statistics_service_provider_quantity': display_service_show.com_statistics_service_provider_quantity,
        'com_statistics_type_activity_quantity': display_service_show.com_statistics_type_activity_quantity,
        'com_statistics_sex_activity_quantity': display_service_show.com_statistics_sex_activity_quantity,
        'org_card_location_welfare_centre': display_service_show.org_card_location_welfare_centre,
        'com_detail_service_price_picture': display_service_show.com_detail_service_price_picture,
        'com_detail_service_products_list': display_service_show.com_detail_service_products_list,
        'eld_map_location': display_service_show.eld_map_location,
        'eld_card_location': display_service_show.eld_card_location,
        'org_detail_profit_total_quantity': display_service_show.org_detail_profit_total_quantity,
        'com_map_location_community': display_service_show.com_map_location_community,
        'com_map_location_community': display_service_show.com_map_location_community,
        'com_card_location_community': display_service_show.com_card_location_community,
        'org_detail_community': display_service_show.org_detail_community,
        'com_statistics_time_sign_in_quantity': display_service_show.com_statistics_time_sign_in_quantity,
        'com_statistics_time_activity_quantity': display_service_show.com_statistics_time_activity_quantity,
        'com_statistics_time_meals_quantity': display_service_show.com_statistics_time_meals_quantity,
        'com_statistics_time_education_quantity': display_service_show.com_statistics_time_education_quantity,
        'com_statistics_time_activity_personal_quantity': display_service_show.com_statistics_time_activity_personal_quantity,
        'com_statistics_street_quantity': display_service_show.com_statistics_street_quantity,
        'spe_statistics_type_service_personal_quantity': display_service_show.spe_statistics_type_service_personal_quantity,
        'spe_statistics_source_service_personal_quantity': display_service_show.spe_statistics_source_service_personal_quantity,
        'spe_card_location': display_service_show.spe_card_location,
        'spe_map_location': display_service_show.spe_map_location,
        'spe_detail_task_card': display_service_show.spe_detail_task_card,
        'eld_detail_elder_map': display_service_show.eld_detail_elder_map,
        'eld_detail_activity_picture_list': display_service_show.eld_detail_activity_picture_list,
        'spe_detail_card': display_service_show.spe_detail_card,
        'spe_detail_picture_list': display_service_show.spe_detail_picture_list,
        'spe_detail_activity_picture_list': display_service_show.spe_detail_activity_picture_list,
        'spe_detail_service_record_top10_list': display_service_show.spe_detail_service_record_top10_list,
        'com_statistics_curriculum_type_attend_class_quantity': display_service_show.com_statistics_curriculum_type_attend_class_quantity,
        'com_statistics_community_meals_quantity': display_service_show.com_statistics_community_meals_quantity,
        'com_statistics_service_provider_meals_quantity': display_service_show.com_statistics_service_provider_meals_quantity,
        'com_statistics_community_happiness_station_quantity': display_service_show.com_statistics_community_happiness_station_quantity,
        'spe_detail_live_video': display_service_show.spe_detail_live_video,
        'eld_statistics_condition_elder_quantity': display_service_show.eld_statistics_condition_elder_quantity,
        'eld_statistics_subsidy_elder_quantity': display_service_show.eld_statistics_subsidy_elder_quantity,
        'org_statistics_children_personnel_quantity': display_service_show.org_statistics_children_personnel_quantity,
        'org_statistics_time_income_quantity': display_service_show.org_statistics_time_income_quantity,
        'sep_statistics_service_provider_satisfaction_quantity': display_service_show.sep_statistics_service_provider_satisfaction_quantity,
        'sep_statistics_nature_satisfaction_quantity': display_service_show.sep_statistics_nature_satisfaction_quantity,
        'eld_statistics_condition_service_quantity': display_service_show.eld_statistics_condition_service_quantity,
        'eld_statistics_condition_buy_money_quantity': display_service_show.eld_statistics_condition_buy_money_quantity,
        'spe_statistics_time_service_personal_quantity': display_service_show.spe_statistics_time_service_personal_quantity,
        'cop_map_location': display_service_show.cop_map_location,
        'cop_card_location': display_service_show.cop_card_location,
        'cop_hot_location_quantity': display_service_show.cop_hot_location_quantity,
        'org_detail_origin_quantity': display_service_show.org_detail_origin_quantity,
        'org_detail_star_personnel_quantity': display_service_show.org_detail_star_personnel_quantity,
        'cop_statistics_service_provider_total_quantity': display_service_show.cop_statistics_service_provider_total_quantity,
        'com_detail_service_products': display_service_show.com_detail_service_products,
        'org_statistics_time_expenditure_quantity': display_service_show.org_statistics_time_expenditure_quantity,
        ###############################################################
        'cop_statistics_donation_income_quantity': display_service_ny.cop_statistics_donation_income_quantity,
        'cop_statistics_donor_income_top10_quantity': display_service_ny.cop_statistics_donor_income_top10_quantity,
        'cop_statistics_item_income_top10_quantity': display_service_ny.cop_statistics_item_income_top10_quantity,
        'cop_statistics_org_use_amount_quantity': display_service_ny.cop_statistics_org_use_amount_quantity,
        'cop_statistics_type_use_amount_quantity': display_service_ny.cop_statistics_type_use_amount_quantity,
        'cop_statistics_donor_use_amount_top10_quantity': display_service_ny.cop_statistics_donor_use_amount_top10_quantity,
        'cop_statistics_item_use_amount_top10_quantity': display_service_ny.cop_statistics_item_use_amount_top10_quantity,
        'cop_statistics_time_quantity': display_service_ny.cop_statistics_time_quantity,
        'cop_statistics_time_profit_quantity': display_service_ny.cop_statistics_time_profit_quantity,
        'cop_statistics_time_income_quantity': display_service_ny.cop_statistics_time_income_quantity,
        'cop_statistics_time_grant_quantity': display_service_ny.cop_statistics_time_grant_quantity,
        'cop_statistics_time_subsidy_quantity': display_service_ny.cop_statistics_time_subsidy_quantity,

        'cop_detail_task_map': display_service_show.cop_detail_task_map,
        'cop_detail_live_video': display_service_show.cop_detail_live_video,
        'spe_detail_task_map': display_service_show.spe_detail_task_map,
        'spe_detail_service_personal_map': display_service_show.spe_detail_service_personal_map,
        'spe_detail_income_quantity': display_service_show.spe_detail_income_quantity,
        'spe_detail_evaluate_quantity': display_service_show.spe_detail_evaluate_quantity,
        'spe_detail_total_quantity': display_service_show.spe_detail_total_quantity,
        'eld_detail_service_live_video_card': display_service_show.eld_detail_service_live_video_card,
        'spe_detail_total_service_person': display_service_show.spe_detail_total_service_person,  # 服务人员-服务人次
        #
        'cop_detail_task_card': display_service_hwg.cop_detail_task_card,
        'cop_detail_service_personal_card': display_service_hwg.cop_detail_service_personal_card,
        'cop_detail_service_provider_card': display_service_hwg.cop_detail_service_provider_card,
        'cop_detail_service_card': display_service_hwg.cop_detail_service_card,
        'eld_detail_task_card': display_service_hwg.eld_detail_task_card,
        'eld_detail_service_record_top10_list': display_service_hwg.eld_detail_service_record_top10_list,
        'eld_detail_family_list': display_service_hwg.eld_detail_family_list,
        'eld_detail_elder': display_service_hwg.eld_detail_elder,

        ###################新版大屏接口#####################################################################
        'new_aswhole_sum_statistical': display_service_qins.new_aswhole_sum_statistical,
        'new_aswhole_sum_statistical_wf': display_service_qins.new_aswhole_sum_statistical_wf,
        'new_aswhole_sum_statistical_hp': display_service_qins.new_aswhole_sum_statistical_hp,
        'new_aswhole_sum_statistical_pv': display_service_qins.new_aswhole_sum_statistical_pv,
        'new_aswhole_all_elder_sum': display_service_qins.new_aswhole_all_elder_sum,
        'new_aswhole_all_elder_sum_bar': display_service_qins.new_aswhole_all_elder_sum_bar,
        'new_org_detail_origin_quantity': display_service_qins.new_org_detail_origin_quantity,
        'new_eld_statistics_subsidy_elder_quantity': display_service_qins.new_eld_statistics_subsidy_elder_quantity,
        'new_aswhole_sum_statistical_street': display_service_qins.new_aswhole_sum_statistical_street,
        'new_aswhole_pension_trends': display_service_qins.new_aswhole_pension_trends,
        'new_aswhole_service_personnel_sum': display_service_qins.new_aswhole_service_personnel_sum,
        'new_eld_statistics_time_elder_quantity': display_service_qins.new_eld_statistics_time_elder_quantity,
        'new_cop_statistics_donation_income_quantity_line': display_service_qins.new_cop_statistics_donation_income_quantity_line,
        'new_donation_amount': display_service_qins.new_donation_amount,
        'new_subsidy_amount': display_service_qins.new_subsidy_amount,
        'new_cop_statistics_donation_income_quantity_line': display_service_qins.new_cop_statistics_donation_income_quantity_line,
        'new_aswhole_call_center_bubble': display_service_qins.new_aswhole_call_center_bubble,
        'new_aswhole_call_center_line': display_service_qins.new_aswhole_call_center_line,
        'new_aswhole_call_center_big_bubble_left': display_service_qins.new_aswhole_call_center_big_bubble_left,
        'new_aswhole_call_center_big_bubble_right': display_service_qins.new_aswhole_call_center_big_bubble_right,
        'new_aswhole_map': display_service_qins.new_aswhole_map,
        'new_sum_statistical_bubble': display_service_qins.new_sum_statistical_bubble,
        'new_sum_statistical_chart': display_service_qins.new_sum_statistical_chart,
        'new_institutions_occupancy_ranking_bed': display_service_qins.new_institutions_occupancy_ranking_bed,
        'new_institutions_occupancy_ranking_eld': display_service_qins.new_institutions_occupancy_ranking_eld,
        'new_household_registration_elder_sum': display_service_qins.new_household_registration_elder_sum,
        'new_org_statistics_street_quantity': display_service_qins.new_org_statistics_street_quantity,
        'new_org_statistics_time_subscribe_quantity': display_service_qins.new_org_statistics_time_subscribe_quantity,
        'new_institutions_classification': display_service_qins.new_institutions_classification,
        'new_activity_statistical': display_service_qins.new_activity_statistical,
        'new_activity_list': display_service_qins.new_activity_list,
        'new_activity_trend_month': display_service_qins.new_activity_trend_month,
        'new_video_monitoring': display_service_qins.new_video_monitoring,
        'new_org_detail_time_subscribe_quantity': display_service_qins.new_org_detail_time_subscribe_quantity,
        'new_org_detail_personal_list': display_service_qins.new_org_detail_personal_list,
        'new_org_detail_service_products_list': display_service_qins.new_org_detail_service_products_list,
        'new_org_detail_picture_list': display_service_qins.new_org_detail_picture_list,
        'new_org_detail_welfare_centre': display_service_qins.new_org_detail_welfare_centre,
        'new_org_detail_welfare_centre_room_status': display_service_qins.new_org_detail_welfare_centre_room_status,
        'new_com_statistics_star_quantity': display_service_qins.new_com_statistics_star_quantity,
        'new_service_ranking_score': display_service_qins.new_service_ranking_score,
        'new_service_ranking_time': display_service_qins.new_service_ranking_time,
        'new_hot_service_ranking': display_service_qins.new_hot_service_ranking,
        'new_com_detail_service_products_list': display_service_qins.new_com_detail_service_products_list,
        'new_servicer_service_num_ranking': display_service_qins.new_servicer_service_num_ranking,
        'new_servicer_appraise_ranking': display_service_qins.new_servicer_appraise_ranking,
        'new_sep_detail_picture_list': display_service_qins.new_sep_detail_picture_list,
        'new_sep_detail_service_provider': display_service_qins.new_sep_detail_service_provider,
        'new_eld_health_exceeding_standard_num': display_service_qins.new_eld_health_exceeding_standard_num,
        'new_activity_ranking': display_service_qins.new_activity_ranking,
        'new_eld_ranking': display_service_qins.new_eld_ranking,
        'new_happiness_station_people_num': display_service_qins.new_happiness_station_people_num,
        'new_com_statistics_time_education_quantity': display_service_qins.new_com_statistics_time_education_quantity,
        'new_activity_and_education_people_num': display_service_qins.new_activity_and_education_people_num,
        'new_com_statistics_time_meals_quantity': display_service_qins.new_com_statistics_time_meals_quantity,
        'new_statistics_meals_service_person_time_ranking': display_service_qins.new_statistics_meals_service_person_time_ranking,
        'new_servicer_list': display_service_qins.new_servicer_list,
        'new_detail_picture_list': display_service_qins.new_detail_picture_list,
        'new_detail_happiness': display_service_qins.new_detail_happiness,
        'new_com_detail_video_surveillance': display_service_qins.new_com_detail_video_surveillance,
        'new_happiness_courtyard_map': display_service_qins.new_happiness_courtyard_map,
        'new_aswhole_map': display_service_qins.new_happiness_courtyard_map,
        'new_service_map': display_service_qins.new_happiness_courtyard_map,
        'new_sum_statistical_bubble_welfare_single': display_service_qins.new_sum_statistical_bubble_welfare_single,
        'new_sum_statistical_chart_welfare_single': display_service_qins.new_sum_statistical_chart_welfare_single,
        'new_household_registration_elder_sum_welfare_single': display_service_qins.new_household_registration_elder_sum_welfare_single,
        'new_activity_statistical_welfare_single': display_service_qins.new_activity_statistical_welfare_single,
        'new_activity_list_welfare_single': display_service_qins.new_activity_list_welfare_single,
        'new_sum_statistical_bubble_happiness_single': display_service_qins.new_sum_statistical_bubble_happiness_single,
        'new_com_statistics_star_quantity_happiness_single': display_service_qins.new_com_statistics_star_quantity_happiness_single,
        'new_activity_ranking_happiness_single': display_service_qins.new_activity_ranking_happiness_single,
        'new_eld_ranking_happiness_single': display_service_qins.new_eld_ranking_happiness_single,
        'new_com_statistics_time_education_quantity_happiness_single': display_service_qins.new_com_statistics_time_education_quantity_happiness_single,
        'new_activity_statistical_happiness_single': display_service_qins.new_activity_statistical_happiness_single,
        'new_activity_list_happiness_single': display_service_qins.new_activity_list_happiness_single,
        'new_org_detail_service_products_list_happiness_single': display_service_qins.new_org_detail_service_products_list_happiness_single,
        'new_monitor_list': display_service_qins.new_monitor_list,
        # 'new_test_bubble': display_service_qins.new_test_bubble,
        # 'new_test_describe': display_service_qins.new_test_describe,
    }

    @jsonrpc.method('IBigScreenService.query')
    def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
        # 判断命令是否存在
        if commandID not in list(command.keys()):
            raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,
                                ErrorMessage.COMMAND_NOT_FOUND)

        return command[commandID](paramValues, startIndex, maxCount)
