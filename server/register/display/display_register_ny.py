

# class ErrorCode:
#     COMMAND_NOT_FOUND = -36001
    
# class ErrorMessage:
#     COMMAND_NOT_FOUND = "命令不存在"


# def register(jsonrpc, db_addr, db_port, db_name, inital_password, session):
#     '''服务注册'''
    
#     # 测试服务
#     display_service=DisplayService(db_addr, db_port, db_name,inital_password, session)
    
#     # 命令列表
#     command={
#         'cop_statistics_donation_income_quantity': display_service.cop_statistics_donation_income_quantity,
#         'cop_statistics_donor_income_top10_quantity': display_service.cop_statistics_donor_income_top10_quantity,
#         'cop_statistics_item_income_top10_quantity': display_service.cop_statistics_item_income_top10_quantity,
#         'cop_statistics_org_use_amount_quantity': display_service.cop_statistics_org_use_amount_quantity,
#         'cop_statistics_type_use_amount_quantity': display_service.cop_statistics_type_use_amount_quantity,
#         'cop_statistics_donor_use_amount_top10_quantity': display_service.cop_statistics_donor_use_amount_top10_quantity,
#         'cop_statistics_item_use_amount_top10_quantity': display_service.cop_statistics_item_use_amount_top10_quantity,
#         'cop_statistics_time_quantity': display_service.cop_statistics_time_quantity,
#         'cop_statistics_time_profit_quantity': display_service.cop_statistics_time_profit_quantity,
#         'cop_statistics_time_income_quantity': display_service.cop_statistics_time_income_quantity,
#         'cop_statistics_time_grant_quantity': display_service.cop_statistics_time_grant_quantity,
#         'cop_statistics_time_subsidy_quantity': display_service.cop_statistics_time_subsidy_quantity,
#             }
    
#     @jsonrpc.method('DisPlayService.query')
#     def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
#         # 判断命令是否存在
#         if commandID not in list(command.keys()):
#             raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,ErrorMessage.COMMAND_NOT_FOUND)
            
#         return command[commandID](paramValues,startIndex,maxCount)
