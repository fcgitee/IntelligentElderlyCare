
# from ...service.display.display_z import DisplayShowService
# from ...pao_python.pao.remote import JsonRpc2Error


# class ErrorCode:
#     COMMAND_NOT_FOUND = -36001


# class ErrorMessage:
#     COMMAND_NOT_FOUND = "命令不存在"


# def register(jsonrpc, db_addr, db_port, db_name, inital_password, session):
#     '''服务注册'''

#     # 测试服务
#     display_service = DisplayShowService(
#         db_addr, db_port, db_name, inital_password, session)

#     # 命令列表
#     command = {
#         # 'spe_detail_total_quantity': display_service.spe_detail_total_quantity,  # 服务人员-总服务次数
#         # 'spe_detail_total_service_person': display_service.spe_detail_total_service_person,  # 服务人员-服务人次
#         # 'spe_detail_income_quantity': display_service.spe_detail_income_quantity,  # 服务人员-服务收入
#         # 'spe_detail_card':display_service.spe_detail_card, #服务人员-明细-卡片
#     }

#     @jsonrpc.method('IDisPlayService.query')
#     def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
#         # 判断命令是否存在
#         if commandID not in list(command.keys()):
#             raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,
#                                 ErrorMessage.COMMAND_NOT_FOUND)

#         return command[commandID](paramValues, startIndex, maxCount)
