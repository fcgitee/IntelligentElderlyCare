'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 15:37:51
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\display\display_resgister_hwg.py
'''
from ...service.display.display_hwg import DisplayHwgService
from ...service.display.display_ny import DisplayNyService
from ...pao_python.pao.remote import JsonRpc2Error


class ErrorCode:
    COMMAND_NOT_FOUND = -36001


class ErrorMessage:
    COMMAND_NOT_FOUND = "命令不存在"


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    '''服务注册'''

    # 测试服务
    display_service = DisplayHwgService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    display_service2 = DisplayNyService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    # 命令列表
    command = {
        'cop_detail_task_card': display_service.cop_detail_task_card,
        'cop_detail_service_personal_card': display_service.cop_detail_service_personal_card,
        'cop_detail_service_provider_card': display_service.cop_detail_service_provider_card,
        'cop_detail_service_card': display_service.cop_detail_service_card,
        'eld_detail_task_card': display_service.eld_detail_task_card,
        'eld_detail_service_record_top10_list': display_service.eld_detail_service_record_top10_list,
        'eld_detail_family_list': display_service.eld_detail_family_list,
        'eld_detail_elder': display_service.eld_detail_elder,
        'cop_statistics_donor_use_amount_top10_quantity': display_service2.cop_statistics_donor_use_amount_top10_quantity,
        'cop_statistics_item_use_amount_top10_quantity': display_service2.cop_statistics_item_use_amount_top10_quantity,
        'cop_statistics_time_quantity': display_service2.cop_statistics_time_quantity,
        'cop_statistics_time_profit_quantity': display_service2.cop_statistics_time_profit_quantity,
    }

    @jsonrpc.method('IHwgDisPlayService.query')
    def __query_service(commandID, paramValues=None, startIndex=None, maxCount=None):
        # 判断命令是否存在
        if commandID not in list(command.keys()):
            raise JsonRpc2Error(ErrorCode.COMMAND_NOT_FOUND,
                                ErrorMessage.COMMAND_NOT_FOUND)

        return command[commandID](paramValues, startIndex, maxCount)
