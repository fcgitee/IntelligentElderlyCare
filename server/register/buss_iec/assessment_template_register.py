
# -*- coding: utf-8 -*-
'''
评估模板服务注册
'''
from ...service.buss_iec.assessment_template import AssessmentTemplateService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    assessment_template_func = AssessmentTemplateService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAssessmentTemplateService.get_assessment_template_list_all')
    def __get_nursing_list_all(condition, page=None, count=None):
        org_list = security_service.judge_permission_query(
            FunctionName.assessment_template_all, PermissionName.query)
        res = assessment_template_func.get_assessment_template_list(
            org_list, condition, page, count)
        return res

    @jsonrpc.method('IAssessmentTemplateService.get_assessment_template_list_look')
    def __get_nursing_list_look(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.assessment_template_look, PermissionName.query)
        res = assessment_template_func.get_assessment_template_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAssessmentTemplateService.get_assessment_template_list')
    def __get_nursing_list_look(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = assessment_template_func.get_assessment_template_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAssessmentTemplateService.get_assessment_template_list_all_look')
    def __get_nursing_list_look(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = assessment_template_func.get_assessment_template_list_all_look(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('IAssessmentTemplateService.get_assessment_template')
    def __get_nursing_list(condition, page=None, count=None):
        res = assessment_template_func.get_assessment_template(
            condition, page, count)
        return res

    @jsonrpc.method('IAssessmentTemplateService.update_assessment_template')
    def __update_user(assessment_template):
        if 'id' in list(assessment_template.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.assessment_template_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.assessment_template_all, PermissionName.add)
        res = assessment_template_func.update_assessment_template(
            assessment_template)
        assessment_template_func.bill_manage_service.insert_logs(
            '机构养老', '评估模板管理', '新增/编辑评估模板管理')
        return res

    @jsonrpc.method('IAssessmentTemplateService.delete_assessment_template')
    def __delete_user(assessment_template_ids):
        __p = security_service.judge_permission_other(
            FunctionName.assessment_template_all, PermissionName.delete)
        res = assessment_template_func.delete_assessment_template(
            assessment_template_ids)
        assessment_template_func.bill_manage_service.insert_logs(
            '机构养老', '评估模板管理', '删除评估模板管理')
        return res
