from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_iec.competence_assessment import CompetenceAssessmentService
from ...pao_python.pao.service.data.mongo_db import N
'''
@Author: your name
@Date: 2019-09-25 09:48:29
@LastEditTime: 2019-12-05 13:46:32
@LastEditors: your name
@Description: In User Settings Edit
@FilePath: \IntelligentElderlyCare\server\register\buss_iec\competence_assessment_register.py
'''
# -*- coding: utf-8 -*-
'''
能力评估服务注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    competence_assessment_func = CompetenceAssessmentService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ICompetenceAssessmentService.get_competence_assessment_list_all')
    def __get_competence_assessment_list_all(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.competenct_assessment_all, PermissionName.query)
        res = competence_assessment_func.get_competence_assessment_list_yh(permission_data,
                                                                           condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.get_competence_assessment_list_look')
    def __get_competence_assessment_list_look(condition, page, count):
        permission_data = security_service.judge_permission_query(
            FunctionName.competenct_assessment_look, PermissionName.query)
        res = competence_assessment_func.get_competence_assessment_list_yh(permission_data,
                                                                           condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.get_competence_assessment_list')
    def __get_competence_assessment_list(condition, page, count):
        permission_data = security_service.org_all_subordinate()
        res = competence_assessment_func.get_competence_assessment_list_yh(permission_data,
                                                                           condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.update_competence_assessment')
    def __update_competence_assessment(competence_assessment):
        if 'id' in list(competence_assessment.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.competenct_assessment_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.competenct_assessment_all, PermissionName.add)
        res = competence_assessment_func.update_competence_assessment(
            competence_assessment)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '长者评估', '新增/编辑长者评估')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.delete_competence_assessment')
    def __delete_competence_assessment(competence_assessment_ids):
        __p = security_service.judge_permission_other(
            FunctionName.competenct_assessment_all, PermissionName.delete)
        res = competence_assessment_func.delete_competence_assessment(
            competence_assessment_ids)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '长者评估', '删除长者评估')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.get_nursing_grade_list')
    def __get_nursing_grade_list(condition, page=None, count=None):
        # permission_data = security_service.judge_permission_query(
        #     FunctionName.nursing_grade, PermissionName.query)
        res = competence_assessment_func.get_nursing_grade_list(N(),
                                                                condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.update_nursing_grade')
    def __update_nursing_grade(condition):
        flag = PermissionName.edit if condition.get(
            'id') else PermissionName.add
        security_service.judge_permission_other(
            FunctionName.nursing_grade, flag)
        res = competence_assessment_func.update_nursing_grade(
            condition)
        competence_assessment_func.bill_manage_service.insert_logs(
            '平台', '照护等级设定', '新增/编辑照护等级设定')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.delete_nursing_grade')
    def __delete_nursing_grade(ids):
        security_service.judge_permission_other(
            FunctionName.nursing_grade, PermissionName.delete)
        res = competence_assessment_func.delete_nursing_grade(
            ids)
        competence_assessment_func.bill_manage_service.insert_logs(
            '平台', '照护等级设定', '删除照护等级设定')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.get_nursing_content_list')
    def __get_nursing_content_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = competence_assessment_func.get_nursing_content_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.update_nursing_content')
    def __update_nursing_content(condition):
        res = competence_assessment_func.update_nursing_content(condition)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '护理服务内容', '新增/编辑护理服务内容')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.delete_nursing_content')
    def __delete_nursing_content(condition):
        res = competence_assessment_func.delete_nursing_content(condition)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '护理服务内容', '删除护理服务内容')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.update_nursing_record')
    def __update_nursing_record(condition):
        res = competence_assessment_func.update_nursing_record(condition)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '长者护理记录', '新增/编辑长者护理记录')
        return res

    @jsonrpc.method('ICompetenceAssessmentService.get_nursing_record_list')
    def __get_nursing_record_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = competence_assessment_func.get_nursing_record_list(
            permission_data, condition, page, count)
        return res

    @jsonrpc.method('ICompetenceAssessmentService.delete_nursing_record')
    def __delete_nursing_record(condition):
        res = competence_assessment_func.delete_nursing_record(condition)
        competence_assessment_func.bill_manage_service.insert_logs(
            '机构养老', '长者护理记录', '删除长者护理记录')
        return res
    
    @jsonrpc.method('ICompetenceAssessmentService.import_pinggu_record')
    def __import_pinggu_record(condition):
        res = competence_assessment_func.import_pinggu_record(condition)
        return res
