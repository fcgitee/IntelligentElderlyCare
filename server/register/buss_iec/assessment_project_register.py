
# -*- coding: utf-8 -*-
'''
评估项目服务注册
'''
from ...service.buss_iec.assessment_project import AssessmentProjectService
from ...service.security_module import SecurityModule, FunctionName, PermissionName


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    assessment_project_func = AssessmentProjectService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IAssessmentProjectService.get_assessment_project_list_all')
    def __get_nursing_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.assessment_project_all, PermissionName.query)
        res = assessment_project_func.get_assessment_project_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.get_nursing_project_list')
    def __get_nursing_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_project, PermissionName.query)
        res = assessment_project_func.get_assessment_project_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.get_nursing_project_list_look')
    def __get_nursing_list_all(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.nursing_project_look, PermissionName.query)
        res = assessment_project_func.get_assessment_project_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.get_assessment_project_list_look')
    def __get_nursing_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.assessment_project_look, PermissionName.query)
        res = assessment_project_func.get_assessment_project_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.get_assessment_project_list')
    def __get_nursing_list(condition, page=None, count=None):
        permission_data = security_service.org_all_subordinate()
        res = assessment_project_func.get_assessment_project_list(permission_data,
                                                                  condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.get_assessment_project_list_all_look')
    def __get_nursing_list(condition, page=None, count=None):
        res = assessment_project_func.get_assessment_project_list_all_look(
            condition, page, count)
        return res

    @jsonrpc.method('IAssessmentProjectService.update_assessment_project')
    def __update_user(assessment_project):
        if 'id' in list(assessment_project.keys()):
            __p = security_service.judge_permission_other(
                FunctionName.assessment_project_all, PermissionName.edit)
        else:
            __p = security_service.judge_permission_other(
                FunctionName.assessment_project_all, PermissionName.add)
        res = assessment_project_func.update_assessment_project(
            assessment_project)
        assessment_project_func.bill_manage_service.insert_logs(
            '机构养老', '评估项目', '新增/编辑评估项目')
        return res

    @jsonrpc.method('IAssessmentProjectService.delete_assessment_project')
    def __delete_user(assessment_project_ids):
        __p = security_service.judge_permission_other(
            FunctionName.assessment_project_all, PermissionName.delete)
        res = assessment_project_func.delete_assessment_project(
            assessment_project_ids)
        assessment_project_func.bill_manage_service.insert_logs(
            '机构养老', '评估项目', '删除评估项目')
        return res

    @jsonrpc.method('IAssessmentProjectService.delete_nursing_project')
    def __delete_user(assessment_project_ids):
        __p = security_service.judge_permission_other(
            FunctionName.nursing_project, PermissionName.delete)
        res = assessment_project_func.delete_assessment_project(
            assessment_project_ids)
        return res
