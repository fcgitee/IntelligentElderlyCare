from ...service.security_module import SecurityModule, FunctionName, PermissionName
from ...service.buss_iec.subsidy_account_recharg import SubsidyAccountRechargService
'''
版权：Copyright (c) 2019 China

创建日期：Monday December 23rd 2019
创建者：ymq(ymq) - <<email>>

修改日期: Monday, 23rd December 2019 3:13:01 pm
修改者: ymq(ymq) - <<email>>

说明
 1、补贴账户充值注册
'''


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    subsidy_account_recharg_func = SubsidyAccountRechargService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('ISubsidyAccountRechargService.get_subsidy_list')
    def __get_subsidy_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.get_subsidy_list_yh(permission_data,
                                                               condition, page, count)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.get_account_balance')
    def __get_account_balance(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.get_account_balance(permission_data,
                                                               condition, page, count)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.recharg_subsidy_account')
    def __recharg_subsidy_account(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.recharg_subsidy_account(
            permission_data, condition)
        subsidy_account_recharg_func.bill_manage_server.insert_logs(
            '平台运营方', '政府补贴账户', '政府补贴账户充值')
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.clearn_subsidy_account')
    def __clearn_subsidy_account(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        condition['amount'] = 0
        res = subsidy_account_recharg_func.recharg_subsidy_account(
            permission_data, condition)
        subsidy_account_recharg_func.bill_manage_server.insert_logs(
            '平台运营方', '政府补贴账户', '政府补贴账户清零')
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.get_subsidy_account_detail')
    def __get_subsidy_account_detail(condition, page=None, count=None):
        _p = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.get_subsidy_account_detail(
            condition, page, count)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.get_charitable_subsidy_list')
    def __get_subsidy_list(condition, page=None, count=None):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.get_charitable_subsidy_list(permission_data,
                                                                       condition, page, count)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.recharg_charitable_account')
    def __recharg_subsidy_account(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.recharg_charitable_account(
            permission_data, condition)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.clearn_charitable_account')
    def __clearn_subsidy_account(condition):
        permission_data = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        condition['amount'] = 0
        res = subsidy_account_recharg_func.recharg_charitable_account(
            permission_data, condition)
        return res

    @jsonrpc.method('ISubsidyAccountRechargService.get_charitable_account_detail')
    def __get_charitable_account_detail(condition, page=None, count=None):
        _p = security_service.judge_permission_query(
            FunctionName.subsidy_account_recharg, PermissionName.query)
        res = subsidy_account_recharg_func.get_charitable_account_detail(
            condition, page, count)
        return res
