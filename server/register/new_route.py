import json
from flask import request, render_template
import time
import os
import base64

from server.pao_python.pao.service.data.mongo_db import N
from server.service.buss_mis.financial_manage import FinancialService
from server.service.security_module import SecurityModule, FunctionName, PermissionName


def register(app, jsonrpc, web_path, upload_file,  db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    financial_service = FinancialService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)
    security_service = SecurityModule(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @app.route('/service-record-detail/<path:month>/<path:orgId>/<path:orgName>/<path:socialWorkerId>/<path:socialWorkerName>/<path:buyType>', methods=['POST', 'GET'])
    def __print_record_detail(month, orgId, orgName, socialWorkerId, socialWorkerName, buyType):
        condition = {}
        if month and month != 'undefined':
            condition['month'] = month.replace('-', '/')
        if orgId and orgId != 'undefined':
            condition['org_id'] = orgId
        if socialWorkerId and socialWorkerId != 'undefined':
            condition['social_worker_id'] = socialWorkerId
        if buyType and buyType != 'undefined':
            condition['buy_type'] = buyType

        data = []
        permission_data = security_service.judge_permission_query(
            FunctionName.service_provider_settlement, PermissionName.query)
        res = financial_service.get_service_provider_record_settlement_yh(
            permission_data, condition)
        if len(res['result']) > 0:
            # for index in range(50):
            #     data.append(res['result'][0])
            # data.append(res['result'][1])
            data = res['result']
        print('len>>>>', len(data))
        return render_template('service-record-detail.html',
                               dataArray=data,
                               query_month=month.replace('-', '/'),
                               social_worker_name=socialWorkerName,
                               org_name=orgName,
                               buy_type=buyType
                               )
