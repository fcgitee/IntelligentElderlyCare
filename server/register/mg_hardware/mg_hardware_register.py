
from ...service.mg_hardware.mg_hardware_manage import MgHardwareService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    mg_hardware_service = MgHardwareService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IMgHardwareService.login_mg')
    def __login_mg():
        '''登录'''
        res = mg_hardware_service.login_mg()
        return res

    # @jsonrpc.method('IMgHardwareService.binding')
    # def __binding():
    #     '''绑定设备/解除绑定'''
    #     res = mg_hardware_service.binding()
    #     return res

    @jsonrpc.method('IMgHardwareService.get_position')
    def __get_position(sn_no):
        '''查看位置'''
        res = mg_hardware_service.get_position(sn_no)
        return res

    @jsonrpc.method('IMgHardwareService.setSos')
    def __setSos(sn_no, nums):
        '''设置SOS号码'''
        res = mg_hardware_service.setSos(sn_no, nums)
        return res

    # @jsonrpc.method('IMgHardwareService.notice_callback')
    # def __notice_callback():
    #     '''报警回调通知'''
    #     res = mg_hardware_service.notice_callback()
    #     return res
