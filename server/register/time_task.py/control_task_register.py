'''
版权：Copyright (c) 2019 China

创建日期：Wednesday November 13th 2019
创建者：ymq(ymq) - <<email>>

修改日期: Wednesday, 13th November 2019 10:53:36 pm
修改者: ymq(ymq) - <<email>>

说明
 1、
'''
from ...service.time_task.control_task import ControlTaskService

def register(jsonrpc, db_addr, db_port, db_name, session):
    control_task_service = ControlTaskService(
        db_addr, db_port, db_name, session)
   
    @jsonrpc.method('IControlTaskService.pause')
    def __pause():
        '''暂停任务'''
        res = control_task_service.pause('subsidy_elder_task')
        return res
    
    @jsonrpc.method('IControlTaskService.resume')
    def __resume():
        '''恢复任务'''
        res = control_task_service.resume('subsidy_elder_task')
        return res
