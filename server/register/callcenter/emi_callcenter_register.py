from ...service.callcenter.emi_callcenter import EmiCallCenterService


def register(jsonrpc, emi_callcenter_data, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    emi_service = EmiCallCenterService(
        emi_callcenter_data, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IEmiService.sign_in')
    def __sign_in():
        '''坐席签入'''
        res = emi_service.sign_in()
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '坐席签入')
        return res

    @jsonrpc.method('IEmiService.sign_off')
    def __sign_off():
        '''坐席签出'''
        res = emi_service.sign_off()
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '坐席签出')
        return res

    @jsonrpc.method('IEmiService.callout')
    def __callout(to):
        '''坐席呼出'''
        res = emi_service.callout(to)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '坐席呼出')
        return res

    @jsonrpc.method('IEmiService.call_cancel')
    def __call_cancel(call_id):
        '''挂断通话'''
        res = emi_service.call_cancel(call_id)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '挂断通话')
        return res

    @jsonrpc.method('IEmiService.get_call_record_url')
    def __get_call_record_url(call_id):
        '''获取通话语音下载url'''
        res = emi_service.get_call_record_url(call_id)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '通话语音下载')
        return res

    @jsonrpc.method('IEmiService.has_call_in')
    def __has_call_in():
        '''根据坐席获取呼入电话'''
        res = emi_service.has_call_in()
        return res

    @jsonrpc.method('IEmiService.get_call_record_list')
    def __get_call_record_list(condition, page=None, count=None):
        '''获取通话记录列表'''
        res = emi_service.get_call_record_list(condition, page, count)
        return res

    @jsonrpc.method('IEmiService.get_seat_operation_list')
    def __get_seat_operation_list(condition, page=None, count=None):
        '''获取坐席运营统计列表'''
        res = emi_service.get_seat_operation_list(condition, page, count)
        return res

    @jsonrpc.method('IEmiService.get_all_call_in')
    def __get_all_call_in():
        '''获取所有呼入电话'''
        res = emi_service.get_all_call_in()
        return res

    @jsonrpc.method('IEmiService.update_one_call_record')
    def __update_one_call_record(call_record):
        '''更新通话记录备注'''
        res = emi_service.update_one_call_record(call_record)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '更新通话记录备注')
        return res

    @jsonrpc.method('IEmiService.get_emergency_call_record_list')
    def __get_emergency_call_record_list(condition, page=None, count=None):
        '''获取紧急呼叫列表'''
        res = emi_service.get_emergency_call_record_list(
            condition, page, count)
        return res

    @jsonrpc.method('IEmiService.change_mode')
    def __change_mode(mode, phone=''):
        '''改变坐席模式，mode为1要传phone，0不用传'''
        res = emi_service.change_mode(mode, phone)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '改变坐席模式')
        return res

    @jsonrpc.method('IEmiService.add_call_note')
    def __add_call_note(call_type_code, call_id, note):
        '''添加通话备注'''
        res = emi_service.add_call_note(call_type_code, call_id, note)
        emi_service.bill_manage_server.insert_logs(
            '平台运营方', '呼叫中心', '添加通话备注')
        return res

    @jsonrpc.method('IEmiService.get_cs_org_location')
    def __get_cs_org_loaction():
        '''获取坐席人员所在机构的地址'''
        res = emi_service.get_cs_org_location()
        return res
