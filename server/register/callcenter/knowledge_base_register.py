from ...service.callcenter.knowledge_base import KnowledgeBaseService


def register(jsonrpc, db_addr, db_port, db_name, db_user, db_pwd, inital_password, session):
    knowledge_base_service = KnowledgeBaseService(
        db_addr, db_port, db_name, db_user, db_pwd, inital_password, session)

    @jsonrpc.method('IKnowledgeBaseService.del_knowledge')
    def __del_knowledge(id):
        '''删除知识'''
        res = knowledge_base_service.del_knowledge(id)
        return res

    @jsonrpc.method('IKnowledgeBaseService.update_knowledge')
    def __update_knowledge(knowledge):
        '''增加/更新知识'''
      # print(knowledge)
        res = knowledge_base_service.update_knowledge(knowledge)
        return res

    @jsonrpc.method('IKnowledgeBaseService.get_knowledge_list')
    def __get_knowledge_list(condition, page=None, count=None):
        '''查询知识'''
        res = knowledge_base_service.get_knowledge_list(condition, page, count)
        return res
