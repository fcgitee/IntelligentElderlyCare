import { createObject } from "pao-aop";
import { AppRouteObject } from "pao-aop-client";
import { LoginViewControl } from "src/business/views/login";
import { RegisterControl } from "src/business/views/register";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AboutUsViewControl } from "src/projects/views/buss-mis/mine/about-us";
import { ElderlyInfoViewControl } from "src/projects/views/buss-pub/elderly-info";
import { PersonalInfoInsertViewControl } from "src/projects/views/home/personal-info-insert";
import { blankID, headMainID, NoTabBarmainFormID } from "../index";
import { CODE_PARAM, KEY_PARAM } from './../../../business/util_tool';
import { RetrievePasswordControl } from "src/business/views/retrieve-password";
import { DownloatAppViewControl } from "src/projects/views/buss-pub/download-app";
import { ModifyPasswordPhoneControl } from "src/business/views/modify-password-phone";
import { FirstModifyControl } from "src/business/views/modify-password/fist_modify";
import { BindPhoneControl } from "src/business/views/bind-phone";
import { AppDeviceListControl } from "src/projects/views/buss-mis/device";
import { AppBindDeviceViewControl } from "src/projects/views/buss-mis/device/app-bind-device";
import { AppDeviceLocationViewControl } from "src/projects/views/buss-mis/device/app-device-location";
/**
 * 用户相关路由
 */
export const userRouter: AppRouteObject[] = [
    {
        path: `${ROUTE_PATH.login}/${KEY_PARAM}`,
        mainFormID: headMainID,
        targetObject: createObject(LoginViewControl, {
            home_path: ROUTE_PATH.home,
            infoInsert_path: ROUTE_PATH.personalInfoInsert,
            login_type: 'app',
            bind_phone_path: ROUTE_PATH.bindPhone,
            modify_password_path: ROUTE_PATH.modifyLoginPassword,
            retrieve_password: ROUTE_PATH.retrievePassword,
            register: ROUTE_PATH.register,
            login_service: AppServiceUtility.login_service,
            servicer_url: ROUTE_PATH.servicerHome
        })
    }, {
        path: `${ROUTE_PATH.login}`,
        mainFormID: headMainID,
        targetObject: createObject(LoginViewControl, {
            home_path: ROUTE_PATH.home,
            infoInsert_path: ROUTE_PATH.personalInfoInsert,
            login_type: 'app',
            bind_phone_path: ROUTE_PATH.bindPhone,
            modify_password_path: ROUTE_PATH.modifyfirstPsw,
            retrieve_password: ROUTE_PATH.retrievePassword,
            register: ROUTE_PATH.register,
            login_service: AppServiceUtility.login_service,
            servicer_url: ROUTE_PATH.servicerHome,
            modify_psw_and_phone_path: ROUTE_PATH.modifyPswAndPhone,
        })
    }, {
        path: ROUTE_PATH.bindPhone,
        mainFormID: headMainID,
        targetType: "secure",
        targetObject: createObject(BindPhoneControl, {
            link_url: ROUTE_PATH.login,
            login_service: AppServiceUtility.login_service,
            sms_service: AppServiceUtility.sms_manage_service
        })
    }, {
        path: ROUTE_PATH.modifyfirstPsw,
        mainFormID: headMainID,
        targetType: "secure",
        targetObject: createObject(FirstModifyControl, {
            link_url: ROUTE_PATH.login,
            login_service: AppServiceUtility.login_service,
        })
    }, {
        path: ROUTE_PATH.modifyPswAndPhone,
        mainFormID: headMainID,
        targetType: "secure",
        targetObject: createObject(ModifyPasswordPhoneControl, {
            link_url: ROUTE_PATH.login,
            login_service: AppServiceUtility.login_service,
            sms_service: AppServiceUtility.sms_manage_service
        })
    },
    {
        path: ROUTE_PATH.register,
        mainFormID: headMainID,
        targetObject: createObject(RegisterControl, {
            login_service: AppServiceUtility.login_service,
            admin_service: AppServiceUtility.business_area_service,
            sms_service: AppServiceUtility.sms_manage_service,
            back_url: ROUTE_PATH.login
        })
    },
    {
        path: `${ROUTE_PATH.downloadApp}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(DownloatAppViewControl)
    },
    {
        path: `${ROUTE_PATH.elderlyInfo}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ElderlyInfoViewControl)
    },
    {
        path: ROUTE_PATH.personalInfoInsert,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(PersonalInfoInsertViewControl)
    },
    {
        path: ROUTE_PATH.aboutUs,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(AboutUsViewControl)
    },
    {
        path: ROUTE_PATH.retrievePassword,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(RetrievePasswordControl, {
            sms_service: AppServiceUtility.sms_manage_service,
            login_service: AppServiceUtility.login_service,
            login_url: ROUTE_PATH.login,
            login_type: 'app',
        })
    },
    {
        path: ROUTE_PATH.appDeviceList,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(AppDeviceListControl, {})
    },
    {
        path: ROUTE_PATH.appBindDevice,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(AppBindDeviceViewControl, {})
    },
    {
        path: `${ROUTE_PATH.appDeviceLocation}/${KEY_PARAM}/${CODE_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetObject: createObject(AppDeviceLocationViewControl, {})
    },
];

// /**
//  * 首页链接
//  */
// export const homeLink = [
//     { key: "home", title: "首页", icon: "antd@home", link: `${ROUTE_PATH.home}` }
// ];