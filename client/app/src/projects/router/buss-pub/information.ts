
import { createObject } from "pao-aop";
import { AppRouteObject } from "pao-aop-client";
import { CODE_PARAM, KEY_PARAM } from 'src/business/util_tool';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { CommmentListViewControl } from "src/projects/views/buss-iec/commment-list";
import { ChangeCompetenceAssessmentViewControl } from "src/projects/views/buss-iec/competence-assessment/change-competence-assessment";
import { CompetenceAssessmentViewControl } from "src/projects/views/buss-iec/competence-assessment/index";
import { GiveThumbsUpViewControl } from "src/projects/views/buss-iec/give-thumbs-up";
import { PublishViewControl } from "src/projects/views/buss-iec/publish";
import { ActivityInformationViewControl } from "src/projects/views/buss-mis/activity-manage/activity-information";
import { ChangeActivityViewControl } from "src/projects/views/buss-mis/activity-manage/change-activity";
import { AppointmentApplicationViewControl } from "src/projects/views/buss-mis/appointment-application";
import { BindDeviceViewControl } from "src/projects/views/buss-mis/bind-device";
import { deviceDetailViewControl } from "src/projects/views/buss-mis/bind-device/device-detail";
import { DeviceListViewControl } from "src/projects/views/buss-mis/bind-device/device-list";
import { CharitableDonateViewControl } from "src/projects/views/buss-mis/charitable/charitable-donate";
import { CharitableDonateInfoViewControl } from "src/projects/views/buss-mis/charitable/charitable-donate/charitable-donate-info";
import { CharitableDonateListViewControl } from "src/projects/views/buss-mis/charitable/charitable-donate/charitable-donate-list";
import { CharitableInformationViewControl } from "src/projects/views/buss-mis/charitable/charitable-information";
import { CharitableListViewControl } from "src/projects/views/buss-mis/charitable/charitable-list";
import { CharitableOrganizationApplyViewControl } from "src/projects/views/buss-mis/charitable/charitable-organization-apply";
import { CharitableProjectListViewControl } from "src/projects/views/buss-mis/charitable/charitable-project";
import { CharitableProjectInfoViewControl } from "src/projects/views/buss-mis/charitable/charitable-project/charitable-project-info";
import { CharitableRecipientApplyViewControl } from "src/projects/views/buss-mis/charitable/charitable-recipient-apply";
import { CharitableRecipientApplyInfoViewControl } from "src/projects/views/buss-mis/charitable/charitable-recipient-apply/charitable-recipient-apply-info";
import { CharitableRecipientApplyListViewControl } from "src/projects/views/buss-mis/charitable/charitable-recipient-apply/charitable-recipient-apply-list";
import { CharitableTitleFundViewControl } from "src/projects/views/buss-mis/charitable/charitable-title-fund";
import { CharitableTitleFundDetailViewControl } from "src/projects/views/buss-mis/charitable/charitable-title-fund/charitable-title-fund-detail";
import { CharitableTitleFundInfoViewControl } from "src/projects/views/buss-mis/charitable/charitable-title-fund/charitable-title-fund-info";
import { CharitableTitleFundListViewControl } from "src/projects/views/buss-mis/charitable/charitable-title-fund/charitable-title-fund-list";
import { CharitableTitleFundRecordViewControl } from "src/projects/views/buss-mis/charitable/charitable-title-fund/charitable-title-fund-record";
import { CommunityListViewControl } from "src/projects/views/buss-mis/community";
import { DemandViewControl } from "src/projects/views/buss-mis/demand";
import { DemandDetailsViewControl } from "src/projects/views/buss-mis/demand-details";
import { DemandDetailsServicerViewControl } from "src/projects/views/buss-mis/demand-details-servicer";
import { DemandListViewControl } from "src/projects/views/buss-mis/demand-list";
import { DemandListServicerViewControl } from "src/projects/views/buss-mis/demand-list-servicer";
import { DemandReleaseViewControl } from "src/projects/views/buss-mis/demand-release";
import { FamilyListViewControl } from "src/projects/views/buss-mis/family-files/list";
import { FriendDialogueViewControl } from "src/projects/views/buss-mis/friend-dialogue";
import { FriendsListViewControl } from "src/projects/views/buss-mis/friends-list";
import { MessageRemindViewControl } from "src/projects/views/buss-mis/message-remind";
import { MyAcceptanceViewControl } from "src/projects/views/buss-mis/mine/acceptance";
import { changeNameViewControl } from "src/projects/views/buss-mis/mine/change-name";
import { EditInfoViewControl } from "src/projects/views/buss-mis/mine/edit_info";
import { MessageNoticeViewControl } from "src/projects/views/buss-mis/mine/message-notice";
import { MineViewControl } from "src/projects/views/buss-mis/mine/mine";
import { myDetailViewControl } from "src/projects/views/buss-mis/mine/my-detail";
import { MyReleaseViewControl } from "src/projects/views/buss-mis/mine/release";
import { MySigninViewControl } from "src/projects/views/buss-mis/mine/signin";
import { ResearchManageViewControl } from "src/projects/views/buss-mis/research-manage";
import { SearchViewControl } from "src/projects/views/buss-mis/search";
import { TaskCreateViewControl } from "src/projects/views/buss-mis/task/task-create";
import { TaskFinishViewControl } from "src/projects/views/buss-mis/task/task-finish";
import { TaskInfoViewControl } from "src/projects/views/buss-mis/task/task-info";
import { TaskListViewControl } from "src/projects/views/buss-mis/task/task-list";
import { TaskStatusViewControl } from "src/projects/views/buss-mis/task/task-status";
import { AnnouncementInfoViewControl } from "src/projects/views/buss-pub/announcement-info";
import { AnnouncementListViewControl } from "src/projects/views/buss-pub/announcement-list";
import { ChangeNewsViewControl } from "src/projects/views/buss-pub/change-news";
import { CommentViewControl } from "src/projects/views/buss-pub/comment";
import { FeedbackViewControl } from "src/projects/views/buss-pub/feedback";
import { FunctionIntroductionViewControl } from "src/projects/views/buss-pub/function-introduction";
import { MessageListViewControl } from "src/projects/views/buss-pub/message-manage";
import { MessageInfoViewControl } from "src/projects/views/buss-pub/message-manage/message-info";
import { NewsInfoViewControl } from "src/projects/views/buss-pub/news-info";
import { NewsListViewControl } from "src/projects/views/buss-pub/news-list";
import { ServiceAgreementViewControl } from "src/projects/views/buss-pub/service-agreement";
import { SetUpViewControl } from "src/projects/views/buss-pub/set-up";
import { blankID, mainFormID, mainFormNoIconID, NoTabBarmainFormID } from "../index";
import { CareSettingViewControl } from "src/projects/views/buss-mis/care-setting";
import { ElderLoveViewControl } from "src/projects/views/buss-mis/elder-love";
import { PrivacyPolicyViewControl } from "src/projects/views/buss-pub/privacy-policy";

/**
 * 信息相关路由
 */
export const informationRouter: AppRouteObject[] = [
    // 新闻列表
    {
        path: `${ROUTE_PATH.newsList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(NewsListViewControl)
    },
    {
        path: `${ROUTE_PATH.newsList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(NewsListViewControl)
    },
    // 编辑新闻
    {
        path: `${ROUTE_PATH.changeNews}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeNewsViewControl)
    },
    // 发布新闻
    {
        path: `${ROUTE_PATH.changeNews}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeNewsViewControl)
    },
    // 新闻详情
    {
        path: `${ROUTE_PATH.newsInfo}/${KEY_PARAM}/${CODE_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(NewsInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.newsInfo}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(NewsInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.newsInfo}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(NewsInfoViewControl)
    },
    // 公告列表
    {
        path: `${ROUTE_PATH.announcementList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AnnouncementListViewControl)
    },
    // 公告详情
    {
        path: `${ROUTE_PATH.announcementInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AnnouncementInfoViewControl)
    },
    // 公告详情
    {
        path: `${ROUTE_PATH.announcementInfo}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AnnouncementInfoViewControl)
    },
    // 评论
    {
        path: `${ROUTE_PATH.comment}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(CommentViewControl)
    },
    // 消息提醒
    {
        path: `${ROUTE_PATH.messageRemind}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MessageRemindViewControl)
    },
    // 好友列表
    {
        path: `${ROUTE_PATH.friendsList}`,
        mainFormID: mainFormID,
        targetType: 'secure',
        targetObject: createObject(FriendsListViewControl)
    },
    // 好友聊天
    {
        path: `${ROUTE_PATH.friendDialogue}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(FriendDialogueViewControl)
    },
    // 任务列表
    {
        path: `${ROUTE_PATH.taskList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(TaskListViewControl)
    },
    // 任务详情
    {
        path: `${ROUTE_PATH.taskInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(TaskInfoViewControl)
    },
    // 任务状态
    {
        path: `${ROUTE_PATH.taskStatus}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(TaskStatusViewControl)
    },
    // 任务完成
    {
        path: `${ROUTE_PATH.taskFinish}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(TaskFinishViewControl)
    },
    // 任务创建
    {
        path: `${ROUTE_PATH.taskCreate}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(TaskCreateViewControl)
    },
    // 我的
    {
        path: `${ROUTE_PATH.mine}`,
        mainFormID: mainFormNoIconID,
        targetType: 'secure',
        targetObject: createObject(MineViewControl)
    },
    // 我的-个人信息详情
    {
        path: `${ROUTE_PATH.myDetail}/${KEY_PARAM}`,
        mainFormID: mainFormID,
        targetType: 'secure',
        targetObject: createObject(myDetailViewControl)
    },
    // 我的-设置
    {
        path: `${ROUTE_PATH.setUp}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(SetUpViewControl)
    },
    // 我的-设置-功能介绍
    {
        path: `${ROUTE_PATH.functionIntroduction}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(FunctionIntroductionViewControl)
    },
    // 我的-设置-服务协议
    {
        path: `${ROUTE_PATH.serviceAgreement}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceAgreementViewControl)
    },
    // 我的-设置-隐私政策
    {
        path: `${ROUTE_PATH.privacyPolicy}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PrivacyPolicyViewControl)
    },
    // 我的-反馈
    {
        path: `${ROUTE_PATH.feedback}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FeedbackViewControl)
    },
    // 我的-修改个人资料
    {
        path: `${ROUTE_PATH.editInfo}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(EditInfoViewControl)
    },
    // 我的-更改用户名
    {
        path: `${ROUTE_PATH.changeName}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(changeNameViewControl)
    },
    // 调研数据录入
    {
        path: `${ROUTE_PATH.researchManage}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ResearchManageViewControl)
    },
    // 查看能力评估
    {
        path: `${ROUTE_PATH.changeCompetenceAssessment}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeCompetenceAssessmentViewControl)
    },

    // 创建能力评估
    {
        path: `${ROUTE_PATH.changeCompetenceAssessment}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeCompetenceAssessmentViewControl)
    },
    // 能力评估
    {
        path: `${ROUTE_PATH.competenceAssessment}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CompetenceAssessmentViewControl)
    },
    // 消息查看
    {
        path: `${ROUTE_PATH.messageInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MessageInfoViewControl)
    },
    // 消息列表
    {
        path: `${ROUTE_PATH.messageList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MessageListViewControl)
    },
    // 绑定设备
    {
        path: `${ROUTE_PATH.bindDevice}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BindDeviceViewControl)
    },
    // 获取绑定设备列表
    {
        path: `${ROUTE_PATH.deviceList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DeviceListViewControl)
    },
    // 绑定设备详情
    {
        path: `${ROUTE_PATH.deviceDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(deviceDetailViewControl)
    },
    // 绑定设备详情
    {
        path: `${ROUTE_PATH.deviceDetail}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(deviceDetailViewControl)
    },
    // 机构捐款申请/详情
    {
        path: `${ROUTE_PATH.charitableOrganizationApply}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableOrganizationApplyViewControl)
    },
    // 慈善信息汇总
    {
        path: `${ROUTE_PATH.charitableInformation}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableInformationViewControl)
    },
    // 慈善捐款申请/详情
    {
        path: `${ROUTE_PATH.charitableRecipientApplyList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableRecipientApplyListViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableRecipientApply}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableRecipientApplyViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableRecipientApplyInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableRecipientApplyInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableRecipientApply}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableRecipientApplyViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableProjectList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableProjectListViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableProjectInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableProjectInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableDonateList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableDonateListViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableDonateInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableDonateInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableDonate}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableDonateViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableDonate}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableDonateViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableDonateInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableDonateInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableTitleFundInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableTitleFundInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableTitleFundRecord}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableTitleFundRecordViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableTitleFundList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableTitleFundListViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableTitleFundDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableTitleFundDetailViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableTitleFund}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableTitleFundViewControl)
    },
    {
        path: `${ROUTE_PATH.charitableList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharitableListViewControl)
    },
    {
        path: `${ROUTE_PATH.communityList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CommunityListViewControl)
    },
    // APP端编辑活动
    {
        path: `${ROUTE_PATH.changeActivity}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeActivityViewControl)
    },
    // APP端发布活动
    {
        path: `${ROUTE_PATH.changeActivity}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChangeActivityViewControl)
    },
    // 活动信息汇总
    {
        path: `${ROUTE_PATH.activityInformation}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivityInformationViewControl)
    },
    // 服务记录预约申请
    {
        path: `${ROUTE_PATH.appointmentApplication}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AppointmentApplicationViewControl)
    },
    // 我的评论列表
    {
        path: `${ROUTE_PATH.commmentList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CommmentListViewControl)
    },
    // 我的点赞列表
    {
        path: `${ROUTE_PATH.giveThumbsUp}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(GiveThumbsUpViewControl)
    },
    // 我的评论列表
    {
        path: `${ROUTE_PATH.publish}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PublishViewControl)
    },
    // 家庭人员列表
    {
        path: `${ROUTE_PATH.familyList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FamilyListViewControl)
    },
    // 我的发布
    {
        path: `${ROUTE_PATH.myRelease}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyReleaseViewControl)
    },
    {
        path: `${ROUTE_PATH.myRelease}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyReleaseViewControl)
    },
    // 我的受理
    {
        path: `${ROUTE_PATH.myAcceptance}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyAcceptanceViewControl)
    },
    // 我的签到
    {
        path: `${ROUTE_PATH.mySignIn}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MySigninViewControl)
    },
    // 消息提醒
    {
        path: `${ROUTE_PATH.messageNotice}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MessageNoticeViewControl)
    },
    // 搜索
    {
        path: `${ROUTE_PATH.Search}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(SearchViewControl)
    },
    {
        path: `${ROUTE_PATH.Search}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(SearchViewControl)
    },

    // 需求发布
    {
        path: `${ROUTE_PATH.demandRelease}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandReleaseViewControl)
    },
    // 需求发布
    {
        path: `${ROUTE_PATH.demandRelease}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandReleaseViewControl)
    },
    // 需求详情
    {
        path: `${ROUTE_PATH.demandDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandDetailsViewControl)
    },
    // 我的需求列表
    {
        path: `${ROUTE_PATH.demandList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandListViewControl)
    },
    // 需求
    {
        path: `${ROUTE_PATH.serviceDemand}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandViewControl)
    },
    // 服务商竞单服务需求列表
    {
        path: `${ROUTE_PATH.demandListServicer}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandListServicerViewControl)
    },
    // 服务商竞单服务需求详情
    {
        path: `${ROUTE_PATH.demandDetailsServicer}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DemandDetailsServicerViewControl)
    },
    // 长者关怀设置
    {
        path: `${ROUTE_PATH.careSetting}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CareSettingViewControl)
    },
    // 长者关爱
    {
        path: `${ROUTE_PATH.elderLove}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ElderLoveViewControl)
    },
];