
/*
 * 版权：Copyright (c) 2019 中国
 * 
 * 创建日期：Thursday August 1st 2019
 * 创建者：胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Thursday, 1st August 2019 3:18:19 pm
 * 修改者: 胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 说明
 *    1、首页路由及链接配置
 */
import { createObject, PermissionState } from "pao-aop";
import { AppRouteObject } from "pao-aop-client";
import { KEY_PARAM } from "src/business/util_tool";
import { MessageInteractionViewControl } from "src/projects/views/buss-mis/message-interaction";
import { HomeViewControl } from "src/projects/views/home";
import { ROUTE_PATH } from "../app/util-tool";
import { CommonProblemViewControl } from "../views/buss-mis/message-interaction/common-problem";
import { ProblemDetailsViewControl } from "../views/buss-mis/message-interaction/problem-details";
import { mainFormNoIconID, NoTabBarmainFormID } from "./index";

/**
 * 首页路由
 * @param mainFormID 主窗体
 */
export const homeRouter: AppRouteObject[] = [
    {
        path: ROUTE_PATH.home,
        mainFormID: mainFormNoIconID,
        targetType: 'secure',
        targetObject: createObject(HomeViewControl)
    },
    {
        path: ROUTE_PATH.messageInteraction,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MessageInteractionViewControl)
    },
    {
        path: ROUTE_PATH.commonProblem,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CommonProblemViewControl)
    }
    ,
    {
        path: `${ROUTE_PATH.problemDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ProblemDetailsViewControl)
    }
];

/**
 * 首页链接
 */
export const homeLink = [
    { key: "home", title: "首页", icon: "antd@home", link: `${ROUTE_PATH.home}`, permission: { permission_state: PermissionState.grant } }
];