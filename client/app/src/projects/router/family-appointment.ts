import { ROUTE_PATH } from '../app/util-tool';
import { createObject } from 'pao-aop';
import { HomeViewControl } from '../views/home/index';
import { NoTabBarmainFormID } from "./index";
export const familyAppointment = {
    path: `/app${ROUTE_PATH.familyAppointment}`,
    mainFormID: NoTabBarmainFormID,
    targetType: 'secure',
    targetObject: createObject(HomeViewControl)
};