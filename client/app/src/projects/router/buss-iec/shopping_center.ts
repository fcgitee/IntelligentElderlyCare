/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-16 17:00:23
 * @LastEditTime : 2019-12-26 14:36:44
 * @LastEditors  : Please set LastEditors
 */

import { AppRouteObject } from "pao-aop-client";

import { ROUTE_PATH } from "src/projects/app/util-tool";

import { createObject } from "pao-aop";

import { KEY_PARAM } from "src/business/util_tool";
import { blankID, NoTabBarmainFormID } from "../index";
import { GoodsDetailsViewControl } from "src/projects/views/buss-iec/goods-detail";
import { NewSubmitOrderViewControl } from "src/projects/views/buss-iec/goods-detail/new_submit_order";
import { PayResultViewControl } from "src/projects/views/buss-iec/shopping-center/pay-result";
import { ConsigneeAddressEditViewControl } from "src/projects/views/buss-iec/goods-detail/consignee_address_edit";
import { ShoppingCenterViewControl } from "src/projects/views/buss-iec/shopping-center";
import { ShoppingSearchViewControl } from "src/projects/views/buss-iec/shopping-center/shopping-search";
import { ProductsViewControl } from "src/projects/views/buss-iec/shopping-center/products";
import { ProductDetailViewControl } from "src/projects/views/buss-iec/shopping-center/product-detail";
import { ConsigneeAddressListViewControl } from "src/projects/views/buss-iec/goods-detail/consignee_address_list";
import { ProductOrderListViewControl } from "src/projects/views/buss-iec/shopping-center/product-order-list";
import { ProductOrderDetailViewControl } from "src/projects/views/buss-iec/shopping-center/product-order-detail";
import { RouterAllViewControl } from "src/projects/views/buss-iec/shopping-center/router-all";
import { ApplyForRefundViewControl } from "src/projects/views/buss-iec/shopping-center/apply-for-refund";

/**
 * 机构相关路由
 */
export const shoppingCenterRouter: AppRouteObject[] = [
    // 可能被弃置的商品详情
    {
        path: `${ROUTE_PATH.goodsDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(GoodsDetailsViewControl)
    },
    // 创建新订单
    {
        path: `${ROUTE_PATH.newSubmitOrder}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(NewSubmitOrderViewControl)
    },
    // 支付结果
    {
        path: `${ROUTE_PATH.payResult}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PayResultViewControl)
    },
    // 选择收货地址
    {
        path: `${ROUTE_PATH.consigneeAddressList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConsigneeAddressListViewControl)
    },
    // 收货人地址列表
    {
        path: ROUTE_PATH.consigneeAddressList,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConsigneeAddressListViewControl)
    },
    // 编辑收货人地址
    {
        path: `${ROUTE_PATH.consigneeAddressEdit}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConsigneeAddressEditViewControl)
    },
    // 新增收货人地址
    {
        path: ROUTE_PATH.consigneeAddressEdit,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConsigneeAddressEditViewControl)
    },
    // 商城首页
    {
        path: ROUTE_PATH.shoppingCenter,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ShoppingCenterViewControl)
    },
    // 搜索
    {
        path: `${ROUTE_PATH.shoppingSearch}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ShoppingSearchViewControl)
    },
    // 搜索
    {
        path: `${ROUTE_PATH.shoppingSearch}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ShoppingSearchViewControl)
    },
    // 商品列表
    {
        path: `${ROUTE_PATH.products}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ProductsViewControl, {
            type: '商品',
        })
    },
    {
        path: ROUTE_PATH.products,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ProductsViewControl, {
            type: '商品',
        })
    },
    // 服务
    {
        path: `${ROUTE_PATH.services}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ProductsViewControl, {
            type: '服务',
        })
    },
    {
        path: ROUTE_PATH.services,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ProductsViewControl, {
            type: '服务',
        })
    },
    // 商品详情
    {
        path: `${ROUTE_PATH.productDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ProductDetailViewControl)
    },
    // 商品订单
    {
        path: `${ROUTE_PATH.productOrderList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ProductOrderListViewControl)
    },
    // 商品订单详情
    {
        path: `${ROUTE_PATH.productOrderDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ProductOrderDetailViewControl)
    },
    // 申请退款
    {
        path: `${ROUTE_PATH.applyForRefund}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplyForRefundViewControl)
    },
    // 测试路由
    {
        path: `${ROUTE_PATH.routerAll}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(RouterAllViewControl)
    },
];
