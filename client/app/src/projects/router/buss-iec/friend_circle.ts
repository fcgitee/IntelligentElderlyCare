/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-16 17:00:23
 * @LastEditTime : 2019-12-26 14:36:44
 * @LastEditors  : Please set LastEditors
 */

import { AppRouteObject } from "pao-aop-client";

import { ROUTE_PATH } from "src/projects/app/util-tool";

import { createObject } from "pao-aop";

import { FriendCircleViewControl } from "src/projects/views/buss-iec/friend-circle";
import { NoTabBarmainFormID, mainFormID } from "../index";
import { PublishFriendCircleViewControl } from "src/projects/views/buss-iec/publish-friend-circle";

/**
 * 机构相关路由
 */
export const friendCircleRouter: AppRouteObject[] = [
    {
        path: `${ROUTE_PATH.friendCircle}`,
        mainFormID: mainFormID,
        targetType: 'secure',
        targetObject: createObject(FriendCircleViewControl)
    },
    {
        path: `${ROUTE_PATH.publishFriendCircle}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PublishFriendCircleViewControl)
    },
];
