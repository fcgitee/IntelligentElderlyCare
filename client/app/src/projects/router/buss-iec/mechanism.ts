/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-16 17:00:23
 * @LastEditTime: 2019-09-25 11:05:45
 * @LastEditors: Please set LastEditors
 */

import { AppRouteObject } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { blankID, NoTabBarmainFormID, mainFormNoIconID } from "../index";
import { createObject } from "pao-aop";
import { BeadhouseListViewControl } from "src/projects/views/buss-iec/beadhouse-list";
import { HappinessHomeListViewControl } from "src/projects/views/buss-iec/happiness-home-list";
import { BeadhouseInfoViewControl } from "src/projects/views/buss-iec/beadhouse-info";
import { HappinessHomeInfoViewControl } from "src/projects/views/buss-iec/happiness-home-info";
import { ActivityListViewControl } from "src/projects/views/buss-mis/activity-manage";
import { ActivityDetailViewControl } from "src/projects/views/buss-mis/activity-manage/activity-detail";
import { KEY_PARAM, CODE_PARAM } from "src/business/util_tool";
import { ActivitySignInViewControl } from "src/projects/views/buss-mis/activity-manage/activity-signIn";
import { ActivityParticipateViewControl } from "src/projects/views/buss-mis/activity-manage/activity-participate";
import { MyActivityListViewControl } from "src/projects/views/buss-mis/activity-manage/my-activity-list";
import { MyActivityRoomListViewControl } from "src/projects/views/buss-mis/activity-manage/my-activity-room-list";
import { ActivityRoomListViewControl } from "src/projects/views/buss-mis/activity-manage/activity-room";
import { confirmationViewControl } from "src/projects/views/buss-iec/confirmation";
import { reservationlistViewControl } from "src/projects/views/buss-iec/reservationlist";
import { InteractionViewControl } from "src/projects/views/buss-mis/interaction";
import { BedReservationViewControl } from "src/projects/views/buss-iec/bed-reservation";

/**
 * 机构相关路由
 */
export const mechanismRouter: AppRouteObject[] = [
    {
        path: `${ROUTE_PATH.beadhouseList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BeadhouseListViewControl)
    },
    {
        path: `${ROUTE_PATH.happinessHomeList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HappinessHomeListViewControl)
    },
    {
        path: `${ROUTE_PATH.beadhouseInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BeadhouseInfoViewControl)
    },
    {
        path: ROUTE_PATH.reservationlist,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(reservationlistViewControl)
    },
    {
        path: `${ROUTE_PATH.confirmation}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(confirmationViewControl)
    },
    {
        path: `${ROUTE_PATH.happinessHomeInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HappinessHomeInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.activityList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivityListViewControl)
    },
    {
        path: `${ROUTE_PATH.activityList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivityListViewControl)
    },
    {
        path: `${ROUTE_PATH.activityDetail}/${KEY_PARAM}/${CODE_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ActivityDetailViewControl)
    },
    {
        path: `${ROUTE_PATH.activityDetail}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(ActivityDetailViewControl)
    },
    {
        path: `${ROUTE_PATH.activitySignIn}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivitySignInViewControl)
    },
    {
        path: `${ROUTE_PATH.activityParticipate}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivityParticipateViewControl)
    },
    {
        path: `${ROUTE_PATH.myActivityList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyActivityListViewControl)
    },
    {
        path: `${ROUTE_PATH.activityRoomList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ActivityRoomListViewControl)
    },
    {
        path: `${ROUTE_PATH.myActivityRoomList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyActivityRoomListViewControl)
    },
    {
        path: `${ROUTE_PATH.interaction}`,
        mainFormID: mainFormNoIconID,
        targetType: 'secure',
        targetObject: createObject(InteractionViewControl)
    },
    {
        path: `${ROUTE_PATH.bedReservation}`,
        mainFormID: mainFormNoIconID,
        targetType: 'secure',
        targetObject: createObject(BedReservationViewControl)
    },

];