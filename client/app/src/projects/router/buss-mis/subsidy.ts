import { AppRouteObject } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { NoTabBarmainFormID } from "../index";
import { createObject } from "pao-aop";
import { ApplicationSubsidiesViewControl } from "src/projects/views/buss-mis/application-subsidies";
import { ApplySuccessViewControl } from "src/projects/views/buss-mis/application-subsidies/apply_success";
import { ApplyStateViewControl } from "src/projects/views/buss-mis/application-subsidies/apply_state";
import { ApplicationDetailViewControl } from "src/projects/views/buss-mis/application-subsidies/application-detail";
import { ApplicationInsertViewControl } from "src/projects/views/buss-mis/application-subsidies/application-insert";
import { ApplyReadingClauseViewControl } from "src/projects/views/buss-mis/older-benefits/reading-clause";
import { OldApplyInfoInsertControl } from "src/projects/views/buss-mis/application-subsidies/old-allowance/info-insert";
import { OldApplyIdCardInsertControl } from "src/projects/views/buss-mis/application-subsidies/old-allowance/id_card_insert";
import { OldApplyCreditCardInsertControl } from "src/projects/views/buss-mis/application-subsidies/old-allowance/credit-card-insert";
import { OldApplyPersonelInfoInsertControl } from "src/projects/views/buss-mis/application-subsidies/old-allowance/personel-info-insert";
import { OldApplyStateViewControl } from "src/projects/views/buss-mis/older-benefits/apply-state";
import { OlderBenefitsApplyControl } from "src/projects/views/buss-mis/older-benefits/older-benefits-apply";
import { KEY_PARAM } from "src/business/util_tool";
import { UncertifiedListControl } from "src/projects/views/buss-mis/older-benefits/uncertified-list";
import { UncertifiedListDetailControl } from "src/projects/views/buss-mis/older-benefits/uncertified-list-detail";
import { WxApplyReadingClauseViewControl } from "src/projects/views/buss-mis/application-subsidies/old-allowance/reading-clause";

/**
 * 补贴相关路由
 */
export const subsidyRouter: AppRouteObject[] = [
    // 补贴申请列表
    {
        path: `${ROUTE_PATH.applicationSubsidies}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplicationSubsidiesViewControl)
    },
    // 补贴申请详情
    {
        path: `${ROUTE_PATH.applicationDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplicationDetailViewControl)
    },
     // 补贴申请详情
     {
        path: `${ROUTE_PATH.applicationDetail}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplicationDetailViewControl)
    },
    // 补贴申请填写
    {
        path: `${ROUTE_PATH.applicationInsert}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplicationInsertViewControl)
    },
    // 补贴申请填写
    {
        path: `${ROUTE_PATH.applicationInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplicationInsertViewControl)
    },
    // 补贴申请成功
    {
        path: `${ROUTE_PATH.applySuccess}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplySuccessViewControl)
    },
    // 补贴申请状态
    {
        path: `${ROUTE_PATH.applyState}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplyStateViewControl)
    },
    // 高龄补贴条款
    {
        path: `${ROUTE_PATH.applyReadinClause}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ApplyReadingClauseViewControl)
    },
    // 高龄补贴条款-wx
    {
        path: `${ROUTE_PATH.applyReadinClauseWx}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(WxApplyReadingClauseViewControl)
    },
    // 高龄补贴信息录入
    {
        path: `${ROUTE_PATH.applyInfoInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyInfoInsertControl)
    },
    // 高龄补贴身份证录入
    {
        path: `${ROUTE_PATH.applyIdcardInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyIdCardInsertControl)
    },
    // 高龄补贴银行卡录入
    {
        path: `${ROUTE_PATH.applyCreditcardInsert}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyCreditCardInsertControl)
    },
    // 高龄补贴人脸识别
    {
        path: `${ROUTE_PATH.applyPersonelInfoInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyPersonelInfoInsertControl)
    },
    // 高龄补贴状态查看
    {
        path: `${ROUTE_PATH.oldapplyState}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyStateViewControl)
    },
    {
        path: `${ROUTE_PATH.oldapplyState}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OldApplyStateViewControl)
    },
    // 高龄津贴申请
    {
        path: `${ROUTE_PATH.olderBenefitsApply}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OlderBenefitsApplyControl)
    },
    // 高龄津贴未认证列表
    {
        path: `${ROUTE_PATH.uncertifiedList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(UncertifiedListControl)
    },
    // 高龄津贴未认证详细列表
    {
        path: `${ROUTE_PATH.uncertifiedListDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(UncertifiedListDetailControl)
    },
];