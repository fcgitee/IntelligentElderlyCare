import { createObject } from "pao-aop";
/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-16 17:00:23
 * @LastEditTime: 2019-08-21 19:08:44
 * @LastEditors: Please set LastEditors
 */
import { AppRouteObject } from "pao-aop-client";
import { KEY_PARAM } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { ConfirmOrdersViewControl } from "src/projects/views/buss-mis/confirmation-orders";
import { elderCareViewControl } from "src/projects/views/buss-mis/elder-care";
import { elderlyCareTemplateViewControl } from "src/projects/views/buss-mis/elderly-care-template";
import { FamilyFilesViewControl } from "src/projects/views/buss-mis/family-files";
import { FamilyFilesEditSelfViewControl } from "src/projects/views/buss-mis/family-files/edit-self";
import { HealthFilesViewControl } from "src/projects/views/buss-mis/health-files";
import { HomeCareControl } from "src/projects/views/buss-mis/home-care";
import { AddProductViewControl } from "src/projects/views/buss-mis/home-care/add-product";
import { AddressViewControl } from "src/projects/views/buss-mis/home-care/Address";
import { CharacteristicServiceViewControl } from "src/projects/views/buss-mis/home-care/characteristic-service";
import { CommentViewControl } from "src/projects/views/buss-mis/home-care/comment";
import { CleanBusinessDetailsViewControl } from "src/projects/views/buss-mis/home-care/help-clean/business-details";
import { CleanBusinessViewControl } from "src/projects/views/buss-mis/home-care/help-clean/clean-business";
import { CleanListViewControl } from "src/projects/views/buss-mis/home-care/help-clean/clean-list";
import { CleanSubmitOrderViewControl } from "src/projects/views/buss-mis/home-care/help-clean/submit-order";
import { FoodBusinessViewControl } from "src/projects/views/buss-mis/home-care/help-food/food-business";
import { FoodBusinessDetailsViewControl } from "src/projects/views/buss-mis/home-care/help-food/food-business/business-details";
import { FoodDetailsViewControl } from "src/projects/views/buss-mis/home-care/help-food/food-details";
import { FoodListViewControl } from "src/projects/views/buss-mis/home-care/help-food/food-list";
import { FoodSubmitOrderViewControl } from "src/projects/views/buss-mis/home-care/help-food/submit-order";
import { ServicePackageListViewControl } from "src/projects/views/buss-mis/home-care/home-service-package/service-package-list";
import { ServiceProviderListViewControl } from "src/projects/views/buss-mis/home-care/home-service-package/service-provider-list";
import { PaymentViewControl } from "src/projects/views/buss-mis/home-care/payment";
import { PaymentFailViewControl } from "src/projects/views/buss-mis/home-care/payment/payment-fail";
import { PaymentSucceedViewControl } from "src/projects/views/buss-mis/home-care/payment/payment-succeed";
import { ServiceProductListViewControl } from 'src/projects/views/buss-mis/home-care/product-list';
import { RemarksViewControl } from "src/projects/views/buss-mis/home-care/Remarks";
import { ServiceDetailsViewControl } from "src/projects/views/buss-mis/home-care/service-product-details";
import { ServiceTypeViewControl } from "src/projects/views/buss-mis/home-care/service-type";
import { LeaveRecordViewControl } from "src/projects/views/buss-mis/leave-record";
import { BackInsertViewControl } from "src/projects/views/buss-mis/leave-record/back-insert";
import { LeaveDetailViewControl } from "src/projects/views/buss-mis/leave-record/leave-detail";
import { LeaveInsertViewControl } from "src/projects/views/buss-mis/leave-record/leave-insert";
import { LeaveListViewControl } from "src/projects/views/buss-mis/leave-record/leave-list";
import { MyOrderViewControl } from "src/projects/views/buss-mis/my-order";
import { ChargeBackViewControl } from "src/projects/views/buss-mis/my-order/charge-back";
import { OrderDetailViewControl } from "src/projects/views/buss-mis/my-order/order-detail";
import { nursingRecordViewControl } from "src/projects/views/buss-mis/nursing-record";
import { OrderPaymentViewControl } from "src/projects/views/buss-mis/order-payment";
import { SearchListViewControl } from "src/projects/views/buss-mis/search/search-list";
import { ServerDetailIntroduceViewControl } from "src/projects/views/buss-mis/server-detail/server-introduce";
import { ServiceListViewControl } from "src/projects/views/buss-mis/service";
import { ServiceCollectionViewControl } from "src/projects/views/buss-mis/service-collection";
import { ServiceCollectionDetailsViewControl } from "src/projects/views/buss-mis/service-collection/details";
import { ServiceConcernViewControl } from "src/projects/views/buss-mis/service-concern";
import { ServiceConcernDetailsViewControl } from "src/projects/views/buss-mis/service-concern/details";
import { ServiceInfoViewControl } from "src/projects/views/buss-mis/service-info";
import { ServicePaymentViewControl } from "src/projects/views/buss-mis/service-payment";
import { SerVicePersonalapplyViewControl } from "src/projects/views/buss-mis/service-personal-apply";
import { SerViceProviderapplyViewControl } from "src/projects/views/buss-mis/service-provider-apply";
import { CommentViewControl as ServiceCommentViewControl } from "src/projects/views/buss-pub/comment";
import { FamilyAppointmentViewControl } from "src/projects/views/buss-pub/family-appointment";
import { ServerDetailCredentialsViewControl } from '../../views/buss-mis/server-detail/server-credentials/index';
import { blankID, mainFormID, mainFormNoIconID, NoTabBarmainFormID } from "../index";
import { RepayViewControl } from './../../views/buss-mis/home-care/payment/repay/index';
import { OlderBenefitsControl } from './../../views/buss-mis/older-benefits/index';
import { ServerDetailViewControl } from './../../views/buss-mis/server-detail/index';
import { ServerDetailAggrementViewControl } from './../../views/buss-mis/server-detail/server-agreement/index';
import { PensionBenefitsControl } from './../../views/Pension-benefits/index';
import { PassCardViewControl } from "src/projects/views/buss-mis/epidemic-prevention/pass-card";
import { HealthFileViewControl } from "src/projects/views/buss-mis/epidemic-prevention/health-file";
import { DayUpdateViewControl } from "src/projects/views/buss-mis/epidemic-prevention/day-update";
import { OrgDayUpdateViewControl } from "src/projects/views/buss-mis/epidemic-prevention/org-day-update";
import { MyHomeViewControl } from "src/projects/views/buss-mis/my-home";
import { AddMyRoomViewControl } from "src/projects/views/buss-mis/my-home/add-room";
import { MyRoomListViewControl } from "src/projects/views/buss-mis/my-home/my-room-list";
import { MyHostListViewControl } from "src/projects/views/buss-mis/my-home/my-host-list";
import { AddHostViewControl } from "src/projects/views/buss-mis/my-home/add-host";
import { AddMyDeviceViewControl } from "src/projects/views/buss-mis/my-home/add-my-device";
import { MyRoomDetailViewControl } from "src/projects/views/buss-mis/my-home/my-room-detail";
import { MyHistoryViewControl } from "src/projects/views/buss-mis/my-home/my-device-history-warn";
import { DisabledPersonViewControl } from "src/projects/views/buss-mis/older-benefits/disabled-person";

/**
 * 服务行为相关路由
 */
export const serviceBehaviorRouter: AppRouteObject[] = [
    /** ======服务列表====== */
    {
        path: ROUTE_PATH.serviceList,
        mainFormID: mainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceListViewControl)
    },
    /** ======服务商列表====== */
    {
        path: ROUTE_PATH.ServiceProviderList,
        mainFormID: mainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceProviderListViewControl)
    },
    /** ======服务详情====== */
    {
        path: `${ROUTE_PATH.serviceInfo}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceInfoViewControl)
    },
    {
        path: `${ROUTE_PATH.confirmOrders}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConfirmOrdersViewControl)
    },
    {
        path: `${ROUTE_PATH.confirmOrders}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ConfirmOrdersViewControl)
    },
    {
        path: `${ROUTE_PATH.orderPayment}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OrderPaymentViewControl)
    },
    /** ======订单详情====== */
    {
        path: `${ROUTE_PATH.orderDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OrderDetailViewControl)
    },
    /** ======我的订单====== */
    {
        path: `${ROUTE_PATH.myOrder}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyOrderViewControl)
    },
    /** =========家属预约========= */
    {
        path: `${ROUTE_PATH.familyAppointment}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FamilyAppointmentViewControl)
    },
    /** =========家属预约========= */
    {
        path: `${ROUTE_PATH.familyAppointment}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FamilyAppointmentViewControl)
    },
    /** =========请假销假========= */
    {
        path: `${ROUTE_PATH.leaveRecord}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(LeaveRecordViewControl)
    },
    /** =========请假========= */
    {
        path: `${ROUTE_PATH.leaveInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(LeaveInsertViewControl)
    },
    /** =========请假列表========= */
    {
        path: `${ROUTE_PATH.leaveList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(LeaveListViewControl)
    },
    /** =========请假信息详情========= */
    {
        path: `${ROUTE_PATH.leaveDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(LeaveDetailViewControl)
    },
    /** =========请假信息详情========= */
    {
        path: `${ROUTE_PATH.leaveDetail}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(LeaveDetailViewControl)
    },
    /** =========销假========= */
    {
        path: `${ROUTE_PATH.backInsert}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BackInsertViewControl)
    },
    /** =========销假========= */
    {
        path: `${ROUTE_PATH.backInsert}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BackInsertViewControl)
    },
    /** =========居家养老首页========= */
    {
        path: `${ROUTE_PATH.homeCare}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HomeCareControl)
    },
    /** =========养老补助导航页========= */
    {
        path: `${ROUTE_PATH.pensionBenefitsNav}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PensionBenefitsControl)
    },
    /** =========高龄津贴========= */
    {
        path: `${ROUTE_PATH.olderBenefits}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OlderBenefitsControl)
    },
    /** =========高龄津贴========= */
    {
        path: `${ROUTE_PATH.olderBenefits}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OlderBenefitsControl)
    },
    /** =========残疾人认证========= */
    {
        path: `${ROUTE_PATH.disabledPerson}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DisabledPersonViewControl)
    },

    /** =========点餐商家列表页面========= */
    {
        path: `${ROUTE_PATH.foodBusiness}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodBusinessViewControl)
    },
    /** =========点餐商家详情页面========= */
    {
        path: `${ROUTE_PATH.foodBusinessDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodBusinessDetailsViewControl)
    },
    /** =========食品详情页面========= */
    {
        path: `${ROUTE_PATH.foodDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodDetailsViewControl)
    },
    /** =========食品列表页面========= */
    {
        path: `${ROUTE_PATH.foodList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodListViewControl)
    },
    /** =========食品列表页面========= */
    {
        path: `${ROUTE_PATH.foodList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodListViewControl)
    },
    /** =========点餐订单确认页面========= */
    {
        path: `${ROUTE_PATH.foodSubmitOrder}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodSubmitOrderViewControl)
    },
    {
        path: `${ROUTE_PATH.foodSubmitOrder}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FoodSubmitOrderViewControl)
    },
    /** =========备注页面========= */
    {
        path: `${ROUTE_PATH.remarks}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(RemarksViewControl)
    },
    /** =========评论页面========= */
    {
        path: `${ROUTE_PATH.comment2}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CommentViewControl)
    },
    /** =========地址页面========= */
    {
        path: `${ROUTE_PATH.address}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddressViewControl)
    },
    /** =========订单支付页面========= */
    {
        path: `${ROUTE_PATH.payment}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PaymentViewControl)
    },
    /** =========重新支付页面========= */
    {
        path: `${ROUTE_PATH.repayment}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(RepayViewControl)
    },
    /** =========申请退款界面========= */
    {
        path: `${ROUTE_PATH.chargeBack}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ChargeBackViewControl)
    },
    /** =========单个服务商详情页面========= */
    {
        path: `${ROUTE_PATH.serverDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServerDetailViewControl)
    },
    /** =========单个服务商详情介绍========= */
    {
        path: `${ROUTE_PATH.serverDetailIntroduce}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServerDetailIntroduceViewControl)
    },
    /** =========单个服务商服务商资质======== */
    {
        path: `${ROUTE_PATH.serverDetailCredentials}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServerDetailCredentialsViewControl)
    },
    /** =========单个服务商服务协议======== */
    {
        path: `${ROUTE_PATH.serverDetailAggrement}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServerDetailAggrementViewControl)
    },
    /** =========订单支付成功页面========= */
    {
        path: `${ROUTE_PATH.paymentSucceed}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PaymentSucceedViewControl)
    },
    /** =========订单支付成功页面========= */
    {
        path: `${ROUTE_PATH.paymentFail}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PaymentFailViewControl)
    },
    /** =========助洁商家列表页面========= */
    {
        path: `${ROUTE_PATH.cleanBusiness}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CleanBusinessViewControl)
    },
    /** =========助洁商家详情页面========= */
    {
        path: `${ROUTE_PATH.cleanBusinessDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CleanBusinessDetailsViewControl)
    },
    /** =========助洁列表页面========= */
    {
        path: `${ROUTE_PATH.cleanList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CleanListViewControl)
    },
    /** =========助洁订单确认页面========= */
    {
        path: `${ROUTE_PATH.cleanSubmitOrder}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CleanSubmitOrderViewControl)
    },
    {
        path: `${ROUTE_PATH.cleanSubmitOrder}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CleanSubmitOrderViewControl)
    },
    /** ============长者护理页面============ */
    {
        path: `${ROUTE_PATH.elderCare}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(elderCareViewControl)
    },
    /** ============护理记录列表页========== */
    {
        path: `${ROUTE_PATH.nursingRecord}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(nursingRecordViewControl)
    },
    /** 长者护理模板页面 */
    {
        path: `${ROUTE_PATH.elderlyCareTemplate}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(elderlyCareTemplateViewControl)
    },
    // 居家服务套餐
    {
        path: `${ROUTE_PATH.servicePackageList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicePackageListViewControl)
    },
    // 服务详情
    {
        path: `${ROUTE_PATH.serviceDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceDetailsViewControl)
    },
    // 服务人员申请
    {
        path: `${ROUTE_PATH.servicePersonalApply}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(SerVicePersonalapplyViewControl)
    },
    // 服务商申请
    {
        path: `${ROUTE_PATH.serviceProviderApply}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(SerViceProviderapplyViewControl)
    },
    // 服务评价
    {
        path: `${ROUTE_PATH.serviceComment}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceCommentViewControl)
    },
    // 首页服务菜单
    {
        path: `${ROUTE_PATH.homeServiceMenu}`,
        mainFormID: mainFormNoIconID,
        targetType: 'secure',
        targetObject: createObject(HomeCareControl)
    },
    // 家人档案
    {
        path: `${ROUTE_PATH.familyFiles}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FamilyFilesViewControl)
    },
    // 家人档案-完善本人信息
    {
        path: `${ROUTE_PATH.familyFilesEditSelf}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(FamilyFilesEditSelfViewControl)
    },
    // 健康档案
    {
        path: `${ROUTE_PATH.healthFiles}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HealthFilesViewControl)
    },
    // 服务支付
    {
        path: `${ROUTE_PATH.servicePayment}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicePaymentViewControl)
    },
    // 服务关注
    {
        path: `${ROUTE_PATH.serviceConcern}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceConcernViewControl)
    },
    // 服务关注-列表
    {
        path: `${ROUTE_PATH.serviceConcernDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceConcernDetailsViewControl)
    },
    // 服务收藏
    {
        path: `${ROUTE_PATH.serviceCollection}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceCollectionViewControl)
    },
    // 服务收藏-列表
    {
        path: `${ROUTE_PATH.serviceCollectionDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceCollectionDetailsViewControl)
    },
    // 服务类型
    {
        path: `${ROUTE_PATH.serviceType}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceTypeViewControl)
    },
    // 服务产品/套餐-列表页
    {
        path: `${ROUTE_PATH.serviceProductList}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceProductListViewControl)
    },
    // 特色服务
    {
        path: `${ROUTE_PATH.CharacteristicService}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(CharacteristicServiceViewControl)
    },
    // 新增服务产品
    {
        path: `${ROUTE_PATH.addProduct}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddProductViewControl)
    },
    // 搜索列表页
    {
        path: `${ROUTE_PATH.searchList}/${KEY_PARAM}`,
        mainFormID: blankID,
        targetType: 'secure',
        targetObject: createObject(SearchListViewControl)
    },
    // 防疫-许可证
    {
        path: `${ROUTE_PATH.passCard}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(PassCardViewControl)
    },
    // 防疫-健康档案
    {
        path: `${ROUTE_PATH.healthFile}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HealthFileViewControl)
    },
    {
        path: `${ROUTE_PATH.healthFile}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(HealthFileViewControl)
    },
    // 防疫-每日登记
    {
        path: `${ROUTE_PATH.DayUpdateHealth}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(DayUpdateViewControl)
    },
    // 防疫-机构每日登记
    {
        path: `${ROUTE_PATH.orgDayUpdateHealth}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OrgDayUpdateViewControl)
    },
    {
        path: `${ROUTE_PATH.orgDayUpdateHealth}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(OrgDayUpdateViewControl)
    },
    {
        path: `${ROUTE_PATH.myHome}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyHomeViewControl)
    },
    {
        path: `${ROUTE_PATH.addRoom}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddMyRoomViewControl)
    },
    {
        path: `${ROUTE_PATH.addRoom}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddMyRoomViewControl)
    },
    {
        path: `${ROUTE_PATH.myRoomList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyRoomListViewControl)
    },
    {
        path: `${ROUTE_PATH.myHostList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyHostListViewControl)
    },
    {
        path: `${ROUTE_PATH.addHost}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddHostViewControl)
    },
    {
        path: `${ROUTE_PATH.addHost}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddHostViewControl)
    },
    {
        path: `${ROUTE_PATH.addMyDevice}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddMyDeviceViewControl)
    },
    {
        path: `${ROUTE_PATH.addMyDevice}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AddMyDeviceViewControl)
    },
    {
        path: `${ROUTE_PATH.myRoomDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyRoomDetailViewControl)
    },
    {
        path: `${ROUTE_PATH.MyHistoryWarn}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyHistoryViewControl)
    }
];