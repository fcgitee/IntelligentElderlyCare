import { AppRouteObject } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { NoTabBarmainFormID } from "../index";
import { createObject } from "pao-aop";
import { SeeBileViewControl } from "src/projects/views/buss-mis/see-bill";
import { BalanceDetailsViewControl } from "src/projects/views/buss-mis/balance-details";
import { MyAccountViewControl } from "src/projects/views/buss-mis/my-account-manage/index";
import { KEY_PARAM } from "src/business/util_tool";
import { SeeBileDetailsViewControl } from "src/projects/views/buss-mis/see-bill-details";
import { AccountDetailViewControl } from "src/projects/views/buss-mis/my-account-manage/account_detail";
import { AccountRechargeViewControl } from "src/projects/views/buss-mis/my-account-manage/account_recharge";
/**
 * 财务相关路由
 */
export const financeRouter: AppRouteObject[] = [
    // 查看账单
    {
        path: `${ROUTE_PATH.seeBill}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(SeeBileViewControl)
    },
    /** 查看账单详情页 */
    {
        path: `${ROUTE_PATH.seeBillDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(SeeBileDetailsViewControl)
    },
    // 账户余额详情
    {
        path: `${ROUTE_PATH.balanceBetails}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(BalanceDetailsViewControl)
    },
    // 我的账户
    {
        path: `${ROUTE_PATH.myAccount}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(MyAccountViewControl)
    },
    // 账户详情
    {
        path: `${ROUTE_PATH.accountDetail}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AccountDetailViewControl)
    },
    {
        path: `${ROUTE_PATH.accountDetail}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AccountDetailViewControl)
    },// 账户充值
    {
        path: `${ROUTE_PATH.accountRecharge}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AccountRechargeViewControl)
    },
    {
        path: `${ROUTE_PATH.accountRecharge}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(AccountRechargeViewControl)
    },
    
];