/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-16 17:00:23
 * @LastEditTime: 2019-09-25 11:05:45
 * @LastEditors: Please set LastEditors
 */
import { AppRouteObject } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { NoTabBarmainFormID } from "../index";
import { createObject } from "pao-aop";
import { ServiceListViewControl } from "src/projects/views/buss-pub/servicer/service-list";
import { ServicerHomeViewControl } from "src/projects/views/buss-pub/servicer/home";
import { ServicerServiceDetailsViewControl } from "src/projects/views/buss-pub/servicer/servicer-service-details";
import { KEY_PARAM } from "src/business/util_tool";
import { ServicerMessageViewControl } from "src/projects/views/buss-pub/servicer/servicer-message";
import { ServicerSetViewControl } from "src/projects/views/buss-pub/servicer/servicer-set";
import { ServicerOrderDetailsViewControl } from "src/projects/views/buss-pub/servicer/servicer-order-details";
import { ServicerTaskImplementSituationViewControl } from "src/projects/views/buss-pub/servicer/servicer-task-implement-situation";
/**
 * 机构相关路由
 */
export const Servicers: AppRouteObject[] = [
    {
        path: `${ROUTE_PATH.servicerTaskList}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServiceListViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerHome}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerHomeViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerServiceDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerServiceDetailsViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerMessage}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerMessageViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerSet}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerSetViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerOrderDetails}/${KEY_PARAM}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerOrderDetailsViewControl)
    },
    {
        path: `${ROUTE_PATH.servicerTaskImplementSituation}`,
        mainFormID: NoTabBarmainFormID,
        targetType: 'secure',
        targetObject: createObject(ServicerTaskImplementSituationViewControl)
    },
];