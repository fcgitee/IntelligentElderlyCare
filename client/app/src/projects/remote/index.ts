/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-21 11:43:25
 * @LastEditTime: 2019-11-15 11:52:07
 * @LastEditors: Please set LastEditors
 */
/*
 * 版权：Copyright (c) 2019 红网
 * 
 * 创建日期：Monday August 19th 2019
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Monday, 19th August 2019 11:35:32 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、远程调用地址
 */

const path = 'http://localhost:3100';
export const remote = {
    url: path + '/remoteCall',
    login: 'ISecurityService',
    wxSign: 'https://zfcs.gdsinsing.com/getWxxcxSign',
    /** 文件上传路径 */
    upload_url: path + '/upload',
    /** 代表部署的正式环境地址 */
    path: path,

};
