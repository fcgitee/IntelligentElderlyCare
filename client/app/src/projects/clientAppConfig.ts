// import { AjaxJsonRpcLoadingFactory } from './../business/components/buss-components/ajax-loading/index';
/*
 * 版权：Copyright (c) 2019 红网
 * 
 * 创建日期：Tuesday April 23rd 2019
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Tuesday, 23rd April 2019 10:26:24 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、政务物联网运维APP配置
 */
import { ISecurityService } from 'pao-aop';
import { AjaxJsonRpcFactory, AppReactRouterControl, BlankMainFormControl, CookieUtil } from 'pao-aop-client';
import { AppMainFormNoIconControl } from 'src/business/mainForm/appMainFormNoIcon';
import { AppNoTabBarMainFormControl } from 'src/business/mainForm/appNoTabBarMainForm';
import { BackstageManageMainFormControl, COOKIE_KEY_CURRENT_USER, COOKIE_KEY_USER_ROLE } from 'src/business/mainForm/backstageManageMainForm';
import { HeadMainFormControl } from 'src/business/mainForm/headMainForm';
import { IUserService } from "src/projects/models/user";
import { IntelligentElderlyCareApplication } from './app';
import { ROUTE_PATH, selectedKeys } from './app/util-tool';
import { AppMainFormControl } from './mainForm/appMainForm';
import { remote } from './remote/index';
import { friendCircleRouter } from './router/buss-iec/friend_circle';
import { mechanismRouter } from './router/buss-iec/mechanism';
import { financeRouter } from './router/buss-mis/finance';
import { serviceBehaviorRouter } from './router/buss-mis/service_behavior';
import { subsidyRouter } from './router/buss-mis/subsidy';
import { informationRouter } from './router/buss-pub/information';
import { userRouter } from './router/buss-pub/user';
import { homeLink, homeRouter } from './router/home';
import { blankID, headMainID, layoutMain, mainFormID, mainFormNoIconID, NoTabBarmainFormID } from './router/index';
import { Servicers } from './router/servicer/index';
import './style/default.less';
import { getPlatformType, isInWeChat } from 'src/business/util_tool';
import { shoppingCenterRouter } from './router/buss-iec/shopping_center';

const blank = new BlankMainFormControl();
const manage_logo = require("../static/img/new_logo.png");
const backstage_nain_form_logo = require("../static/img/logo.png");

// 主窗体
const layoutMainForm = new BackstageManageMainFormControl(
    "智慧养老",
    [
    ],
    [
        { key: "scan", icon: "antd@scan", link: "/scan" }
    ],
    function () {
        CookieUtil.remove(COOKIE_KEY_CURRENT_USER);
        CookieUtil.remove(COOKIE_KEY_USER_ROLE);
    },
    '/login',
    undefined,
    (pathname: string) => selectedKeys(pathname),
    backstage_nain_form_logo
);
const mainForm = new AppMainFormNoIconControl(
    "智慧养老",
    [
        ...homeLink,
        { key: "friend", title: "老友圈", icon: "antd@usergroup-delete", link: ROUTE_PATH.friendCircle },
        // { key: "activity", title: "二维码", icon: "antd@qrcode", link: ROUTE_PATH.activityList },
        { key: "interaction", title: "互动", icon: "antd@phone", link: ROUTE_PATH.interaction },
        { key: "service", title: "搜索", icon: "antd@search", link: ROUTE_PATH.Search },
        { key: "mine", title: "我的", icon: "antd@user", link: ROUTE_PATH.mine },
        // { key: "联系客服", title: "客服", icon: "antd@bulb", link: ROUTE_PATH.mine }
    ],
    undefined,
    undefined,
    ROUTE_PATH.home
);

const NoTabBarmainForm = new AppNoTabBarMainFormControl(
    "智慧养老",
    [
        ...homeLink,
        // { key: "friend", title: "老友圈", icon: "antd@usergroup-delete", link: ROUTE_PATH.friendCircle },
        // { key: "activity", title: "首页", icon: "antd@smile", link: ROUTE_PATH.activityList },
        // { key: "activity", title: "二维码", icon: "antd@qrcode", link: ROUTE_PATH.activityList },
        { key: "interaction", title: "互动", icon: "antd@phone", link: ROUTE_PATH.interaction },
        { key: "service", title: "搜索", icon: "antd@search", link: ROUTE_PATH.Search },
        { key: "mine", title: "我的", icon: "antd@user", link: ROUTE_PATH.mine },
        // { key: "联系客服", title: "客服", icon: "antd@bulb", link: ROUTE_PATH.mine }
    ],
    undefined,
    undefined,
    ROUTE_PATH.home,
);

// let func_list: [] SON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);
// if (func_list === undefined) {
//     AppServiceUtility.login_service.get_function_list!()!
//         .then(e => {
//             func_list = e;
//         });
// }
// const userInfo = (IntelligentElderlyCareAppStorage.getCurrentUser() as any);
// const roleId = userInfo ? userInfo.role_id : '';

// const retArrClientConfig = filterPermissionByRole(func_list, ['老友圈', '互动', '搜索', '我的'], roleId);

const mainFormNoIcon = new AppMainFormControl(
    "智慧养老",
    [
        ...homeLink,
        // { key: "friend", title: "老友圈", icon: "antd@usergroup-delete", link: ROUTE_PATH.friendCircle, permission: { permission_state: getAppPermissonObject(retArrClientConfig, '老友圈', '查询') ? PermissionState.grant : PermissionState.forbid } },
        // { key: "interaction", title: "互动", icon: "antd@phone", link: ROUTE_PATH.interaction, permission: { permission_state: getAppPermissonObject(retArrClientConfig, '互动', '查询') ? PermissionState.grant : PermissionState.forbid } },
        // // { key: "activity", title: "首页", icon: "antd@smile", link: ROUTE_PATH.activityList },
        // // { key: "activity", title: "二维码", icon: "antd@qrcode", link: '' },
        // // { key: "service", title: "语音", icon: "antd@customer-service", link: ROUTE_PATH.serviceList },
        // { key: "service", title: "搜索", icon: "antd@search", link: ROUTE_PATH.Search, permission: { permission_state: getAppPermissonObject(retArrClientConfig, '搜索', '查询') ? PermissionState.grant : PermissionState.forbid } },
        // { key: "mine", title: "我的", icon: "antd@user", link: ROUTE_PATH.mine, permission: { permission_state: getAppPermissonObject(retArrClientConfig, '我的', '查询') ? PermissionState.grant : PermissionState.forbid } },
        { key: "friend", title: "老友圈", icon: "antd@usergroup-delete", link: ROUTE_PATH.friendCircle },
        { key: "interaction", title: "服务热线", icon: "antd@phone", link: ROUTE_PATH.interaction },
        { key: "friend", title: "资讯发布", icon: "antd@usergroup-delete", link: ROUTE_PATH.changeNews },
        { key: "interaction", title: "消息提醒", icon: "antd@bulb", link: ROUTE_PATH.messageNotice },
        { key: "service", title: "搜索", icon: "antd@search", link: ROUTE_PATH.Search },
        { key: "mine", title: "我的", icon: "antd@user", link: ROUTE_PATH.mine }
    ],
    [
        // { key: "scan", title: "扫码", icon: "antd@plus-circle", link: ROUTE_PATH.mine },
    ],
    undefined,
    ROUTE_PATH.home,
    true,
    [
        {
            icon: "home",
            text: '首页',
            url: ROUTE_PATH.home
        },
        {
            icon: "ordered-list",
            text: '我的订单',
            url: ROUTE_PATH.myOrder
        },
        {
            icon: "clock-circle",
            text: '家属预约',
            url: ROUTE_PATH.familyAppointment
        },
        {
            icon: "ordered-list",
            text: '服务列表',
            url: ROUTE_PATH.serviceList
        },
        {
            icon: "bank",
            text: '养老院',
            url: ROUTE_PATH.beadhouseList
        },
        {
            icon: "smile",
            text: '幸福院',
            url: ROUTE_PATH.happinessHomeList
        },
        {
            icon: "file-text",
            text: '查看账单',
            url: ROUTE_PATH.seeBill
        },
        {
            icon: "bank",
            text: '长者护理',
            url: ROUTE_PATH.elderCare
        },
        {
            icon: "bank",
            text: '护理记录',
            url: ROUTE_PATH.nursingRecord
        },
        {
            icon: "select",
            text: '补贴申请',
            url: ROUTE_PATH.applicationSubsidies
        },
        {
            icon: "global",
            text: '新闻中心',
            url: ROUTE_PATH.newsList
        },
        {
            icon: "shake",
            text: '活动',
            url: ROUTE_PATH.activityList
        },
        {
            icon: "notification",
            text: '活动报名',
            url: ROUTE_PATH.activityParticipate
        },
        {
            icon: "team",
            text: '我的活动',
            url: ROUTE_PATH.myActivityList
        },
        {
            icon: "audit",
            text: '任务列表',
            url: ROUTE_PATH.taskList
        },
        {
            icon: "table",
            text: '公告',
            url: ROUTE_PATH.announcementList
        },
        {
            icon: "home",
            text: '好友',
            url: ROUTE_PATH.friendsList
        },
        {
            icon: "home",
            text: '调研录入',
            url: ROUTE_PATH.researchManage
        },
        {
            icon: "home",
            text: '请假销假',
            url: ROUTE_PATH.leaveRecord
        }
    ]
);
/** 带头部窗体 */
const HeadMainForm = new HeadMainFormControl(
    manage_logo,
    '智慧养老服务平台',
    undefined,
    false
);

const router = new AppReactRouterControl(
    {
        [blankID]: blank,
        [layoutMain]: layoutMainForm,
        [mainFormID]: mainForm,
        [NoTabBarmainFormID]: NoTabBarmainForm,
        [headMainID]: HeadMainForm,
        [mainFormNoIconID]: mainFormNoIcon
    },
    [
        {
            path: '/app/',
            exact: true,
            redirect: '/app/login'
        },
        ...userRouter,
        ...homeRouter,
        ...serviceBehaviorRouter,
        ...financeRouter,
        ...informationRouter,
        ...subsidyRouter,
        ...mechanismRouter,
        // 服务人员路由
        ...Servicers,
        // 老友圈
        ...friendCircleRouter,
        ...shoppingCenterRouter,
    ]
);

export let defaultObject = new IntelligentElderlyCareApplication(
    router,
    new AjaxJsonRpcFactory(IUserService, remote.url, "IUserService"),
    new AjaxJsonRpcFactory(ISecurityService, remote.url, remote.login),
    remote.url
);

export default defaultObject;

// 适配APP端导航栏高度
(() => {
    // 非微信环境的IOS端需要特殊处理
    if (getPlatformType() === 'ios' && !isInWeChat() && document && document.getElementById('root')) {
        document.getElementById('root')!.className = "rootInIos";
    }
})();