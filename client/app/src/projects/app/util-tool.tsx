import { Toast } from "antd-mobile";
import axios from 'axios';
import { Permission, PermissionState } from "pao-aop";
import React from "react";
import { Redirect } from "react-router";
import { FORCE_LOGIN_KEY, TRUE_STR } from "src/business/mainForm/appNoTabBarMainForm";
import { LOCAL_FUNCTION_LIST } from "src/business/mainForm/backstageManageMainForm";
import { GO_BACK_OBJ, LOGIN_SUCCESS_OBJ } from "src/business/util_tool";
import { remote } from "../remote";
import { AppServiceUtility } from "./appService";
import { IntelligentElderlyCareAppStorage } from "./appStorage";

/** 路由URL */
export const ROUTE_PATH = {
    /** 安全路由 */
    rolePermission: '/role-permission',
    addRole: '/add-role',
    userManage: '/user-manage',
    userEditor: '/user-editor',
    login: '/app/login',
    register: '/app/register',
    bindPhone: '/bind-phone',
    modifyfirstPsw: '/modify_first_psw',
    modifyPswAndPhone: '/modify_psw_and_phone',
    modifyLoginPassword: '/modify-login-password',
    modifyMobile: '/modify-mobile',
    modifyEmail: '/modify-email',
    securitySettings: '/security-settings',
    Abnormity: '/abnormity',
    retrievePassword: '/retrieve-password',
    /** 首页 */
    home: '/app/home',
    /** 护理档案 */
    nursingArchives: '/app/nursing-archives',
    /** 档案编辑 */
    archivesEditor: '/app/archives-editor',
    /** 家属预约 */
    familyAppointment: '/app/family-appointment',
    /** 我的订单 */
    myOrder: '/app/my-order',
    /** 订单详情 */
    orderDetail: '/app/order-detail',
    /** 服务商列表 */
    ServiceProviderList: '/app/service-provider-list',
    /** 服务列表 */
    serviceList: '/app/service-list',
    /** 服务详情 */
    serviceInfo: '/app/service-info',
    /** 确认订单页面 */
    confirmOrders: '/app/confirm-orders',
    /** 订单支付页面 */
    orderPayment: '/app/order-payment',
    /** 长者信息 */
    elderlyInfo: '/app/elderly-info',
    /** 养老院列表页面 */
    beadhouseList: '/app/beadhouse-list',
    /** 养老院详情页面 */
    beadhouseInfo: '/app/beadhouse-info',
    /** 预约确认页面 */
    confirmation: '/app/confirmation',
    /** 已预约列表 */
    reservationlist: '/app/reservation',
    /** 幸福院列表页面 */
    happinessHomeList: '/app/happiness-home-list',
    /** 幸福院详情页面 */
    happinessHomeInfo: '/app/happiness-home-info',
    /** 查看账单 */
    seeBill: '/app/see-bill',
    /** 查看账单详情页 */
    seeBillDetails: '/app/see-bill-details',
    /** 消息提醒 */
    messageRemind: '/app/message-remind',
    /** 补贴申请列表 */
    applicationSubsidies: '/app/application-subsidies',
    /** 补贴申请详情 */
    applicationDetail: '/app/application-detail',
    /** 补贴申请填写 */
    applicationInsert: '/app/application-insert',
    /** 补贴申请成功页面 */
    applySuccess: '/app/apply-success',
    /** 补贴申请状态 */
    applyState: '/app/apply-state',
    /** 高龄补贴条款 */
    applyReadinClause: '/app/apply-reading-clause',
    /** 高龄补贴条款-wx */
    applyReadinClauseWx: '/app/apply-reading-clause-wx',
    /** 高龄补贴信息录入 */
    applyInfoInsert: '/app/apply-info-insert',
    /** 高龄补贴身份证录入 */
    applyIdcardInsert: '/app/apply-idCard-insert',
    /** 高龄补贴银行卡录入 */
    applyCreditcardInsert: '/app/apply-creditCard-insert',
    /** 高龄补贴人脸识别 */
    applyPersonelInfoInsert: '/app/apply-personelInfo-insert',
    /** 高龄补贴状态查看 */
    oldapplyState: '/app/old-apply-state',
    /** 新闻列表 */
    newsList: '/app/news-list',
    /** 新闻详情 */
    newsInfo: '/app/news-info',
    // 发布新闻
    changeNews: '/app/change-news',
    /** 好友列表 */
    friendsList: '/app/friends-list',
    /** 好友聊天页 */
    friendDialogue: '/app/friend-dialogue',
    /** 活动列表页面 */
    activityList: '/app/activity-list',
    /** 活动详情页面 */
    activityDetail: '/app/activity-detail',
    /** 活动参与报名页面(报名成功) */
    activityParticipate: '/app/activity-participate',
    /** 签到成功页面 */
    activitySignIn: '/app/activity-sign-in',
    /** 我的活动页面 */
    myActivityList: '/app/my-activity-list',
    /** 活动室页面 */
    activityRoomList: '/app/activity-room-list',
    /** 我的活动室预约页面 */
    myActivityRoomList: '/app/my-activity-room-list',
    /** 评论 */
    comment: '/app/comment',
    /** 任务列表 */
    taskList: '/app/task-list',
    /** 任务详情 */
    taskInfo: '/app/task-info',
    /** 任务状态 */
    taskStatus: '/app/task-status',
    /** 任务完成 */
    taskFinish: '/app/task-finish',
    /** 任务创建 */
    taskCreate: '/app/task-create',
    /** 我的页面 */
    mine: '/app/mine',
    /** 我的-个人信息详情页面 */
    myDetail: '/app/my-detail',
    /** 我的-修改个人资料 */
    editInfo: '/app/edit-info',
    /** 我的-更改用户名页面 */
    changeName: '/app/change-name',
    /** 调研管理页面 */
    researchManage: '/app/research-manage',
    /** 请假销假主面 */
    leaveRecord: '/app/leave-record',
    /** 请假页面 */
    leaveInsert: '/app/leave-insert',
    /** 请假列表页面 */
    leaveList: '/app/leave-list',
    /** 请假信息详情页面 */
    leaveDetail: '/app/leave-detail',
    /** 销假页面 */
    backInsert: '/app/back-insert',
    /** 公告列表 */
    announcementList: '/app/announcement-list',
    /** 公告详情 */
    announcementInfo: '/app/announcement-info',
    /** 子评论列表 */
    childCommmentList: '/app/child-comment-list',
    /** 评论列表 */
    commmentList: '/app/comment-list',
    /** 评论详情 */
    commmentInfo: '/app/comment-info',
    /** 能力评估列表 */
    competenceAssessment: '/app/competence-assessment-list',
    /** 创建/查看能力评估列表 */
    changeCompetenceAssessment: '/app/change-competence-assessment',
    /** 消息列表 */
    messageList: '/app/message-list',
    /** 消息列表 */
    messageInfo: '/app/message-info',
    /** 绑定设备 */
    bindDevice: '/app/bind-device',
    /** 绑定设备 */
    deviceList: '/app/device-list',
    /** 设备详情 */
    deviceDetail: '/app/device-detail',
    /** 居家养老首页 */
    homeCare: '/app/home-care',
    /** 点餐页面 */
    foodBusiness: '/app/food-business',
    /** 点餐商家详情页面 */
    foodBusinessDetails: '/app/food-business-details',
    /** 食物详情页面 */
    foodDetails: '/app/food-details',
    /** 食物列表页面 */
    foodList: '/app/food-list',
    /** 点餐提交页面 */
    foodSubmitOrder: '/app/food-submit-order',
    /** 备注页面 */
    remarks: '/app/remarks',
    /** 地址页面 */
    address: '/app/address',
    /** 评论页面 */
    comment2: '/app/comment2',
    /** 订单支付页面 */
    payment: '/app/payment',
    /** 重新支付页面 */
    repayment: '/app/repayment',
    /** 申请退款页面 */
    chargeBack: '/app/charge-back',
    /** 单个服务商详情页面 */
    serverDetail: '/app/server-detail',
    /** 单个服务商详情热门服务介绍页面 */
    serverDetailIntroduce: '/app/server-detail-introduce',
    /** 单个服务商详情服务商资质页面 */
    serverDetailCredentials: '/app/server-detail-credentials',
    /** 单个服务商详情服务协议页面 */
    serverDetailAggrement: '/app/server-detail-aggrement',
    /** 支付成功页面 */
    paymentSucceed: '/app/paymentSucceed',
    /** 支付失败 */
    paymentFail: '/app/paymentFail',
    /** 助洁商家列表页面 */
    cleanBusiness: '/app/clean-business',
    /** 助洁商家详情页面 */
    cleanBusinessDetails: '/app/clean-business-details',
    /** 助洁产品列表页面 */
    cleanList: '/app/clean-list',
    /** 助洁提交页面 */
    cleanSubmitOrder: '/app/clean-submit-order',
    // 慈善捐款申请
    charitableRecipientApply: '/app/charitable-recipient-apply',
    // 慈善机构捐款申请
    charitableOrganizationApply: '/app/charitable-organization-apply',
    // 慈善捐款查看
    charitableRecipientApplyInfo: '/app/charitable-recipient-apply-info',
    // 慈善捐款申请列表
    charitableRecipientApplyList: '/app/charitable-recipient-apply-list',
    // 慈善项目列表
    charitableProjectList: '/app/charitable-project-list',
    // 慈善项目详情
    charitableProjectInfo: '/app/charitable-project-info',
    // 冠名基金
    charitableTitleFund: '/app/charitable-title-fund',
    // 冠名基金申请记录
    charitableTitleFundRecord: '/app/charitable-title-fund-record',
    // 冠名基金列表
    charitableTitleFundList: '/app/charitable-title-fund-list',
    // 冠名基金详细页面
    charitableTitleFundDetail: '/app/charitable-title-fund-detail',
    // 冠名基金详情
    charitableTitleFundInfo: '/app/charitable-title-fund-info',
    // 慈善捐款
    charitableDonate: '/app/charitable-donate',
    // 慈善捐款列表
    charitableDonateList: '/app/charitable-donate-list',
    // 慈善捐款详情
    charitableDonateInfo: '/app/charitable-donate-info',
    // 慈善信息汇总
    charitableInformation: '/app/charitable-information',
    // 活动信息汇总
    activityInformation: '/app/activity-information',
    // 账户余额
    balanceBetails: '/app/balance-details',
    // 慈善机构菜单
    charitableList: '/app/charitable-list',
    // 社区养老菜单
    communityList: '/app/community-list',
    // 居家服务套餐 
    servicePackageList: '/app/service-package-list',
    // 服务详情
    serviceDetails: '/app/service-details',
    // 发布活动
    changeActivity: '/app/change-activity',
    /** 护理模板 */
    elderlyCareTemplate: '/app/elderly-care-template',
    /** 护理页面 */
    elderCare: '/app/elder-care',
    /** 护理记录 */
    nursingRecord: '/app/nursing-record',
    /** 我的账户 */
    myAccount: '/app/my-account',
    /** 账户详情 */
    accountDetail: '/app/account-detail',
    /** 账户充值 */
    accountRecharge: '/app/account-recharge',
    /** 资料补全 */
    personalInfoInsert: '/app/personal-info-insert',
    // 关于我们
    aboutUs: '/app/about-us',
    // 服务人员申请
    servicePersonalApply: '/app/service-personal-apply',
    // 服务商申请
    serviceProviderApply: '/app/service-provider-apply',
    // 服务评价
    serviceComment: '/app/service-comment',
    // 服务人员任务列表
    servicerTaskList: '/app/servicer-task-list',
    // 服务人员首页实视图
    servicerHome: '/app/servicer-home',
    // 服务人员服务任务详情
    servicerServiceDetails: '/app/servicer-service-details',
    // 服务人员服务消息
    servicerMessage: '/app/servicer-message',
    // 服务人员设置页面
    servicerSet: '/app/servicer-set',
    // 服务人员订单详情
    servicerOrderDetails: '/app/servicer-order-details',
    // 服务人员任务执行情况
    servicerTaskImplementSituation: '/app/servicer-task-implement-situation',
    // 幸福小站
    happinessStation: '/app/happiness-station',

    // 老友圈
    friendCircle: '/app/friend-circle',
    // 首页服务菜单链接
    homeServiceMenu: '/app/home-service-ment',
    // 互动
    interaction: '/app/interaction',
    // 消息互动
    messageInteraction: '/app/message-interaction',
    // 家人档案
    familyFiles: '/app/family-files',
    // 家人列表
    familyList: '/app/family-list',
    // 完善本人信息
    familyFilesEditSelf: '/app/family-files-edit-self',
    // 添加长者
    familyFilesAddOlder: '/app/family-files-add-elder',
    // 添加子女亲属
    familyFilesAddKids: '/app/family-files-add-kids',
    // 健康档案
    healthFiles: '/app/health-files',
    // 服务支付
    servicePayment: '/app/service-payment',
    // 常见问题
    commonProblem: '/app/common-problem',
    // 问题详情
    problemDetails: '/app/problem-details',
    // 预约申请
    appointmentApplication: '/app/appointment-application',
    // 服务关注
    serviceConcern: '/app/service-concern',
    // 服务关注-列表
    serviceConcernDetails: '/app/service-concern-details',
    // 服务收藏
    serviceCollection: '/app/service-collection',
    // 服务收藏-列表
    serviceCollectionDetails: '/app/service-collection-details',
    // 点赞
    giveThumbsUp: '/app/give-thumbs-up',
    // 服务类型
    serviceType: '/app/service-type',
    // 我的评论
    publish: '/app/publish',
    // 发布朋友圈
    publishFriendCircle: '/app/publish-friend-circle',
    /** 高龄津贴 */
    olderBenefits: '/app/older-benefits',
    /** 养老补助导航页 */
    pensionBenefitsNav: '/app/pension-benefits-nav',
    disabledPerson: '/app/disabled-person',
    // app设置页面
    setUp: '/app/set-up',
    // 功能介绍
    functionIntroduction: '/app/function-introduction',
    // 关于我们
    AboutUs: '/app/about-us',
    // 服务协议
    serviceAgreement: '/app/service-agreement',
    // 隐私政策
    privacyPolicy: '/app/privacy-policy',
    // 意见反馈
    feedback: '/app/feedback',
    // 服务产品/套餐-列表页
    serviceProductList: '/app/service-product-list',
    // 我的-发布
    myRelease: '/app/my-release',
    // 我的-签到
    mySignIn: '/app/my-sign-in',
    // 我的-受理
    myAcceptance: '/app/my-acceptance',
    // 消息提醒
    messageNotice: '/app/message-notice',
    // 搜索
    Search: '/app/search',
    // 特色服务
    CharacteristicService: '/app/characteristic-service',
    // 商家详情
    BusinessDetails: '/app/business-details',
    // 新增服务产品
    addProduct: '/app/add-product',
    // 床位预约
    bedReservation: '/app/bed-reservation',
    // 需求发布
    demandRelease: '/app/demand-release',
    // 需求详情
    demandDetails: '/app/demand-details',
    // 高龄津贴申请
    olderBenefitsApply: '/app/older-benefits-apply',
    // 需求列表
    demandList: '/app/demand-list',
    // 服务需求
    serviceDemand: '/app/service-demand',
    // 高龄津贴未认证列表
    uncertifiedList: '/app/uncertified-list',
    // 高龄津贴未认证详细列表
    uncertifiedListDetail: '/app/uncertified-list-detail',
    // 搜索列表
    searchList: '/app/search-list',
    // app下载
    downloadApp: '/app/download-app',
    // 服务商竞单服务需求列表
    demandListServicer: '/app/demand-list-servicer',
    // 服务商竞单服务需求详情
    demandDetailsServicer: '/app/demand-details-servicer',
    // 关怀设置
    careSetting: '/app/care-setting',
    // 长者关爱
    elderLove: '/app/elder-love',
    // 防疫-许可证
    passCard: '/app/pass-card',
    // 防疫-出入登记
    healthFile: '/app/health-file',
    // 防疫-每日登记
    DayUpdateHealth: '/app/day-update-health',
    // 防疫-机构每日登记
    orgDayUpdateHealth: '/app/org-day-update-health',
    // 我的家
    myHome: '/app/my-home',
    // 创建房间
    addRoom: '/app/add-my-room',
    // 我的房间列表
    myRoomList: '/app/my-room-list',
    // 主机管理
    myHostList: '/app/my-host-list',
    // 新增/编辑主机
    addHost: '/app/add-host',
    // 新增/编辑我的设备
    addMyDevice: '/app/add-my-device',
    // 我的设备列表
    myDeviceList: '/app/my-device-list',
    // 房间详情（警报信息）
    myRoomDetail: '/app/my-room-detail',
    // 历史警报信息
    MyHistoryWarn: '/app/my-history-warn',
    // 商品详情页面
    goodsDetail: '/app/goods-details',
    // 新的确认订单页面
    newSubmitOrder: '/app/new-submit-order',
    // 支付结果页面
    payResult: '/app/pay-result',
    // 订单详情页面
    goodsOrderDetail: '/app/goods-order-detail',
    // 收货人地址列表页面
    consigneeAddressList: '/app/consignee-address-list',
    // 收货人地址编辑页面
    consigneeAddressEdit: '/app/consignee-address-edit',
    // 商城首页
    shoppingCenter: '/app/shopping-center',
    // 商城搜索
    shoppingSearch: '/app/shopping-search',
    // 商城商品
    products: '/app/products',
    // 商城详情
    productDetail: '/app/product-detail',
    // 商城服务
    services: '/app/services',
    // 商品订单
    productOrderList: '/app/product-order-list',
    // 商品订单详情
    productOrderDetail: '/app/product-order-detail',
    // 商城路由
    routerAll: '/app/router-all',
    // 申请退款
    applyForRefund: '/app/apply-for-refund',
    // 设备列表
    appDeviceList: '/app/app-device-list',
    // 绑定设备列表
    appBindDevice: '/app/app-bind-device',
    // 查看设备定位
    appDeviceLocation: '/app/app-device-location',
};

/** 角色Match参数名称 */
export const ROLE_ID = 'role_id';
/** 角色路由参数名称 */
export const ROLE_ID_PARAM = `:${ROLE_ID}`;
/** 用户Match参数名称 */
export const USER_ID = 'user_id';
/** 用户路由参数名称 */
export const USER_ID_PARAM = `:${USER_ID}`;
export function selectedKeys(pathname: string) {
    console.info(pathname);
    switch (pathname) {
        // case ROUTE_PATH.companyTemplateDetail:
        // case companyTemplateDetail:
        // case changeCompanyTemplateView:
        //     return ROUTE_PATH.companyTemplateView;
        default:
            return pathname;
    }
}

/** 判断权限 */
export function isPermission(permission: Permission, company_id?: string) {
    let userRole = IntelligentElderlyCareAppStorage.getCurrentUserRoles();
    // 判断是否拥有权限
    for (let role in userRole) {
        if (role) {
            for (let permissions in userRole[role]['permission']) {
                if (permission) {
                    if (userRole[role]['permission'][permissions]['permission'] === permission['permission']
                        && userRole[role]['permission'][permissions]['permission_state'] === PermissionState.forbid) {
                        return <Redirect to={ROUTE_PATH.Abnormity} />;
                    } else if (userRole[role]['permission'][permissions]['permission'] === permission['permission']
                        && userRole[role]['permission'][permissions]['permission_state'] === PermissionState.default) {
                        return <Redirect to={ROUTE_PATH.Abnormity} />;
                    }
                }

            }
        }
    }
    return;
}
/**
 * 
 * @param that  this
 * @param ServiceName  服务.方法
 * @param params    参数
 * @param SuccessFun    成功回调
 * @param ErrorFun  失败回调
 */
export function request_func(that: any, ServiceName: any, params: any, SuccessFun?: Function, ErrorFun?: Function) {
    that.setState({ loading: true });
    ServiceName!(...params)!
        .then((data: any) => {
            SuccessFun ? SuccessFun!(data) : alert(data.msg);
        })
        .catch((error: any) => {
            if (ErrorFun) {
                ErrorFun(error);
            } else {
                if (error.message === '-36006') {
                    alert('会话已过期，请重新登录');
                    that.props.history!.push('/login');
                } else {
                    alert(error.message);
                }
            }
        });
    that.setState({ loading: false });
}

/** 上传前回调方法 */
export function beforeUpload(file: File) {
    const isImage = (file.type === 'image/jpeg' || file.type === 'image/png');
    if (!isImage) {
        Toast.info('你必须上传JPG/png格式的文件!');
    }
    const isLt1M = file.size / 1024 / 1024 < 2;
    if (!isLt1M) {
        Toast.info('图片大小必须小于2MB!');
    }
    return isLt1M && isImage;
}
/** 用身份证号码获取出生日期 */
export function getBirthday(id_card: string) {
    let len = (id_card + "").length;
    if (len === 0) {
        return;
    } else {
        // 身份证号码只能为15位或18位其它不合法
        if (len !== 15 && len !== 18) {
            return;
        }
    }
    let strBirthday = "";
    // 处理18位的身份证号码从号码中得到生日和性别代码
    if (len === 18) {
        strBirthday = id_card.substr(6, 4) + "-" + id_card.substr(10, 2) + "-" + id_card.substr(12, 2);
    }
    if (len === 15) {
        strBirthday = "19" + id_card.substr(6, 2) + "-" + id_card.substr(8, 2) + "-" + id_card.substr(10, 2);
    }
    // 时间字符串里，必须是“/”
    return strBirthday;
}

/** 用身份证获取年龄 */
export function getAge(id_card: string) {
    let birthday = new Date(getBirthday(id_card)!);
    let nowDateTime = new Date();
    let age = nowDateTime.getFullYear() - birthday!.getFullYear();
    // 再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
    if (nowDateTime.getMonth() < birthday!.getMonth() || (nowDateTime.getMonth() === birthday!.getMonth() && nowDateTime.getDate() < birthday!.getDate())) {
        age--;
    }
    return age;
}

/** 用身份证获取年龄2 */
export function getAge2(identityCard: string) {
    if (identityCard === '') {
        return identityCard;
    }
    identityCard = identityCard + '';
    var len = (identityCard + "").length;
    if (len === 0) {
        return 0;
    } else {
        // 身份证号码只能为15位或18位其它不合法
        if ((len !== 15) && (len !== 18)) {
            return 0;
        }
    }
    var strBirthday = "";
    // 处理18位的身份证号码从号码中得到生日和性别代码
    if (len === 18) {
        strBirthday = identityCard.substr(6, 4) + "/" + identityCard.substr(10, 2) + "/" + identityCard.substr(12, 2);
    }
    if (len === 15) {
        strBirthday = "19" + identityCard.substr(6, 2) + "/" + identityCard.substr(8, 2) + "/" + identityCard.substr(10, 2);
    }
    // 时间字符串里，必须是“/”
    var birthDate = new Date(strBirthday);
    var nowDateTime = new Date();
    var age = nowDateTime.getFullYear() - birthDate.getFullYear();
    // 再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
    if (nowDateTime.getMonth() < birthDate.getMonth() || (nowDateTime.getMonth() === birthDate.getMonth() && nowDateTime.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

/** 用身份证获取性别 */
export function getSex(id_card: string) {
    if (id_card.length === 15) {
        let sex = parseInt(id_card[14], undefined);
        if (sex % 2 === 0) {
            return '女';
        } else {
            return '男';
        }
    } else {
        let sex = parseInt(id_card[16], undefined);
        if (sex % 2 === 0) {
            return '女';
        } else {
            return '男';
        }
    }
}

export const PAGE_TYPE = {
    /** 确认支付界面是购买产品服务的页面 */
    BUY: 'buy',
    /** 确认支付界面是充值页面 */
    CHARGE: 'charge',
    /** 确认支付界面是充值页面 */
    DONATION: 'donation',
};

/** 对url参数进行编码 */
export function encodeUrlParam(str: string) {
    return window.btoa(encodeURI(str));
}

/** 对url参数进行解码 */
export function decodeUrlParam(str: string) {
    return decodeURI(window.atob(str));
}

export function filterPermissionByRole(funcList: any[], names: string[], roleId: string) {
    const retArr: any = [];

    funcList.map((e: any, i: number) => {
        for (let i = 0; i < names.length; i++) {
            const name = names[i];
            if (name === e.function) {
                if (roleId === e.role_id) {
                    retArr.push(e);
                }
            }
        }
    });
    return retArr;
}

/** 获取app权限 */
export function getAppPermisson(names: string[]) {
    return (constructor: Function) => {
        let func_list = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);
        if (!func_list) {
            AppServiceUtility.login_service.get_function_list!()!
                .then(e => {
                    func_list = e;
                    localStorage.setItem(LOCAL_FUNCTION_LIST, JSON.stringify(func_list));
                    constructor.prototype['AppPermission'] = e;
                });
        } else {
            const userInfo = (IntelligentElderlyCareAppStorage.getCurrentUser() as any);
            if (userInfo) {
                const roleId = userInfo.role_id;
                const retArr = filterPermissionByRole(func_list, names, roleId);
                constructor.prototype['AppPermission'] = retArr;
            } else {
                constructor.prototype['AppPermission'] = func_list;
            }
        }
    };
}

export function getAppPermissonObject(persissionObject: any[], permissionToCheck: string, permission: '查询' | '新增' | '编辑' | '删除') {
    let retBoolean = false;
    // 有权限列表就一定登录过 
    // const userInfo = (IntelligentElderlyCareAppStorage.getCurrentUser() as any);
    // let roleId = '';
    // if (userInfo) {
    //     roleId = userInfo.role_id;
    // } else {
    //     return retBoolean;
    // }

    persissionObject && persissionObject.map((e, i) => {
        if (e.function === permissionToCheck) {
            if (e.permission === permission) {
                // if (e.role_id === roleId) {
                retBoolean = true;
                return;
                // }
            }
        }
    });
    return retBoolean;
}

export function getAppPermissonJSXElement(persissionObject: any[], permissionToCheck: string, permission: '查询' | '新增' | '编辑' | '删除') {
    let retBoolean = getAppPermissonObject(persissionObject, permissionToCheck, permission);
    if (retBoolean === false) {
        return <></>;
    }
    return retBoolean;
}

// /**
//  * @description 强制检查是否登录
//  * 若已登录，该函数不带来任何副作用，直接跳转successPath。
//  * 若未登录，会跳转登录页面，
//  * 并且登录页面上若登录成功，则跳转successPath的路由
//  * 若不登录，点击登录页面上的返回，则跳转backPath的路由
//  */
// export function forceCheckIsLogin(props: Readonly<any>, successPath: string, backPath: string) {
//     // 判断是否登录
//     AppServiceUtility.personnel_service.get_user_session!()!
//         .then((data: any) => {
//             if (!data) {
//                 const param = {
//                     [LOGIN_SUCCESS_OBJ]: successPath,
//                     [GO_BACK_OBJ]: backPath
//                 };
//                 props.history!.push(ROUTE_PATH.login + "/" + encodeUrlParam(JSON.stringify(param)));
//                 return;
//             }
//             props.history!.push(successPath);
//             return;
//         });
// }

/**
 * @description 强制检查是否登录
 * 若已登录，该函数不带来任何副作用，直接跳转successPath。若successPath为空，则不做任何事情。
 * 若未登录，会跳转登录页面，
 * 并且登录页面上若登录成功，则跳转successPath的路由
 * 若不登录，点击登录页面上的返回，则跳转backPath的路由
 */
export function forceCheckIsLogin(props: Readonly<any>, successObj: { successPath?: string, params?: any }, backObj: { backPath: string, params?: any }) {
    // 判断是否登录
    AppServiceUtility.personnel_service.get_user_session!()!
        .then((data: any) => {
            if (!data) {
                const param = {
                    [LOGIN_SUCCESS_OBJ]: successObj,
                    [GO_BACK_OBJ]: backObj
                };
                props.history!.push(ROUTE_PATH.login, param);
                localStorage.setItem(FORCE_LOGIN_KEY, TRUE_STR);
                return;
            }
            if (successObj.successPath) {
                props.history!.push(successObj.successPath, successObj.params);
            }
            // return;
        });
}

async function getWxConfigInner() {

    let token = await axios.post(
        remote.wxSign,
        {
            url: window.location.href.split('#')[0],
        });

    return {
        // jsapi_ticket: token.data.jsapi_ticket,
        nonceStr: token.data.nonceStr,
        timestamp: token.data.timestamp,
        url: token.data.url,
        signature: token.data.signature,
        appId: 'wxc12e6e381af896c8',
        debug: token.data.debug,
        jsApiList: [
            'getLocation',
            'scanQRCode'
        ] // 必填，需要使用的JS接口列表
    };
}

let ifGetWxConfig = false;
let wxConfig: Object;
export async function getWxConfig() {
    if (!ifGetWxConfig) {
        let config = await getWxConfigInner();
        wxConfig = config;
        ifGetWxConfig = true;
        setTimeout(
            () => {
                ifGetWxConfig = false;
            },
            7200 * 1000
        );
        return wxConfig;
    } else {
        return wxConfig;
    }
}
export const funAddress = [
    {
        label: '北京',
        value: '01',
        children: [
            {
                label: '东城区',
                value: '01-1',
            },
            {
                label: '西城区',
                value: '01-2',
            },
            {
                label: '崇文区',
                value: '01-3',
            },
            {
                label: '宣武区',
                value: '01-4',
            },
        ],
    },
    {
        label: '广东省',
        value: '广东省',
        children: [
            {
                label: '广州市',
                value: '广州市',
                children: [
                    {
                        label: '海珠区',
                        value: '海珠区',
                    },
                    {
                        label: '越秀区',
                        value: '越秀区',
                    },
                    {
                        label: '荔湾区',
                        value: '荔湾区',
                    },
                    {
                        label: '天河区',
                        value: '天河区',
                    },
                ],
            },
            {
                label: '佛山市',
                value: '佛山市',
                children: [
                    {
                        label: '南海区',
                        value: '南海区',
                    },
                    {
                        label: '禅城区',
                        value: '禅城区',
                    },
                ],
            }
        ],
    },
];