/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-13 10:42:38
 * @LastEditTime: 2020-02-19 09:35:34
 * @LastEditors: Please set LastEditors
 */
import { getAddonTypeName, getObject, IAnyType, IType } from "pao-aop";
import { AjaxJsonRpcFactory } from "pao-aop-client";
import { IEmiService } from 'src/business/models/emi-account';
import { ICarouselService } from "src/models/carousel";
import { IActivityService } from "../models/activity";
import { IAllowanceService } from "../models/allowance-manage";
import { IAnnouncementDetailService, IAnnouncementListService } from "../models/announcement";
import { IAppPayService } from '../models/app-pay';
import { IAssessmentTemplateService } from "../models/assessment-template";
import { IBusinessAreaService } from "../models/business-area";
import { ICharitableService } from "../models/chartable";
import { CheckInService } from "../models/check-in";
import { IComfirmOrderService } from "../models/comfirm-order";
import { ICommentManageService } from "../models/comment";
import { ICompetenceAssessmentService } from "../models/competence-assessment";
import { IDeviceService } from "../models/dervice";
import { IFiancialService } from "../models/financial-manage";
import { IFriendsCircleService } from "../models/friends-circle";
import { IHotelZoneService, IHotelZoneTypeService } from "../models/hotel_zone";
import { ILeaveRecordService } from "../models/leave-record";
import { ILoginService } from "../models/login";
import { IMessageService } from "../models/message";
import { IMyAccountService } from "../models/my-account";
import { IArticleService, INewsDetailService, INewsListService } from "../models/news";
import { nursingRecordService } from "../models/nursing-record";
import { nursingTemplateService } from "../models/nursing-template";
import { IOpinionFeedbackService } from "../models/opinion_feedback";
import { IPersonOrgManageService } from "../models/person-org-manage";
import { IPersonnelOrganizationalService } from "../models/personnel-organizational";
import { IReservationRegistrationService } from "../models/reservation";
// import { IServiceOrderService } from "src/models/service-order";
import { IServicePackageListService, IServiceProductDetailService } from "../models/service";
import { IServiceFollowCollectionService } from '../models/service-follow-collection';
import { IServiceOperationService } from "../models/service-operation";
import { IServiceOrderService } from "../models/service-order";
import { IServiceRecordService } from '../models/service-record';
import { ITaskService } from "../models/task";
import { IUserService } from "../models/user";
import { IUserRelationShipService } from '../models/user-relationship';
import { IHomeCareService } from '../models/home-care';
import { remote } from "../remote";
import { IOperationRecordService } from "../models/operation-record";
import { IServiceItemPackage } from "../models/service-item-package";
import { IServicesProjectService } from "../models/services-project";
import { IServiceScopeService } from "../models/service_scope";
import { IDemandService } from "../models/demand";
import { ISmsManageService } from "../models/sms";
import { IAppPageConfigService } from "../models/app-page-config";
import { IMyHomeService } from "../models/my-home";
import { IShoppingMallManageService } from "../models/shopping-mall";
import { ILedgerAccountManageService } from "../models/ledger_account";

/**
 * 应用服务工具类
 */
export class AppServiceUtility {
    /** 远程请求url */
    static remote_url: string;
    /**
     * 服务对象
     * @param service_interface 接口
     * @type T 接口类型
     */
    static service<T>(service_interface: IType<T> | Object, remote_url?: string): T {
        if (typeof service_interface === 'function') {
            let destFunc = service_interface as IAnyType;
            service_interface = new destFunc();
        }
        let service_name = getAddonTypeName(service_interface as any);
        let factory = new AjaxJsonRpcFactory(service_interface, remote_url || AppServiceUtility.remote_url, service_name);
        let service = getObject(factory);
        return service as T;
    }
    /**
     * 服务对象
     * @param service_interface 接口
     * @type T 接口类型
     */
    static serviceNoLoading<T>(service_interface: IType<T> | Object): T {
        if (typeof service_interface === 'function') {
            let destFunc = service_interface as IAnyType;
            service_interface = new destFunc();
        }
        let service_name = getAddonTypeName(service_interface as any);
        let factory = new AjaxJsonRpcFactory(service_interface, remote.url, service_name);
        let service = getObject(factory);
        return service as T;
    }

    /** 家属预约页面 */
    static get reservation_service() {
        return AppServiceUtility.service(IReservationRegistrationService);
    }
    /** 我的订单 */
    static get service_order_service() {
        return AppServiceUtility.service(IServiceOrderService, remote.url);
    }
    /** 活动发布 */
    static get activity_service() {
        return AppServiceUtility.service(IActivityService);
    }
    /** 活动发布 */
    static get carousel_service() {
        return AppServiceUtility.service(ICarouselService);
    }
    /** 服务套餐服务 */
    static get service_package_service() {
        return AppServiceUtility.service(IServicePackageListService);
    }
    /** 服务详情服务 */
    static get service_detail_service() {
        return AppServiceUtility.service(IServiceProductDetailService);
    }
    /** 新闻详情服务 */
    static get news_detail_service() {
        return AppServiceUtility.service(INewsDetailService);
    }
    /** 新闻列表服务 */
    static get news_list_service() {
        return AppServiceUtility.service(INewsListService);
    }
    /** 新闻列表服务 */
    static get article_list_service() {
        return AppServiceUtility.service(IArticleService);
    }
    /** 任务管理服务 */
    static get task_service() {
        return AppServiceUtility.service(ITaskService);
    }
    /** 公告详情服务 */
    static get announcement_detail_service() {
        return AppServiceUtility.service(IAnnouncementDetailService);
    }
    /** 公告列表服务 */
    static get announcement_list_service() {
        return AppServiceUtility.service(IAnnouncementListService);
    }
    /** 我的订单服务 */
    static get my_order_service() {
        return AppServiceUtility.service(IServiceOrderService);
    }
    /** 确认订单服务 */
    static get comfirm_order_service() {
        return AppServiceUtility.service(IComfirmOrderService);
    }
    /** 评论服务 */
    static get comment_service() {
        return AppServiceUtility.service(ICommentManageService);
    }
    /** 请假销假服务 */
    static get leaveRecord_service() {
        return AppServiceUtility.service(ILeaveRecordService);
    }
    /** 住宿区域类型服务 */
    static get hotel_zone_type_service() {
        return AppServiceUtility.service(IHotelZoneTypeService);
    }
    /** 住宿区域服务 */
    static get hotel_zone_service() {
        return AppServiceUtility.service(IHotelZoneService);
    }
    /** 人员组织服务 */
    static get personnel_service() {
        return AppServiceUtility.service(IPersonnelOrganizationalService);
    }
    /** 权限人员组织机构管理 */
    static get person_org_manage_service() {
        return AppServiceUtility.service(IPersonOrgManageService);
    }
    /** 能力评估服务 */
    static get competence_assessment_service() {
        return AppServiceUtility.service(ICompetenceAssessmentService);
    }
    /** 信息管理服务 */
    static get message_service() {
        return AppServiceUtility.service(IMessageService);
    }
    /** 评估模板服务 */
    static get assessment_template_service() {
        return AppServiceUtility.service(IAssessmentTemplateService);
    }
    /** 补贴管理服务 */
    static get allowance_service() {
        return AppServiceUtility.service(IAllowanceService);
    }
    /** 设备服务 */
    static get device_service() {
        return AppServiceUtility.service(IDeviceService);
    }
    /** 用户服务 */
    static get user_service() {
        return AppServiceUtility.service(IUserService);
    }
    /** 入住登记服务 */
    static get check_in_service() {
        return AppServiceUtility.service(CheckInService);
    }
    /** 慈善服务 */
    static get charitable_service() {
        return AppServiceUtility.service(ICharitableService);
    }
    /** 业务区域管理服务 */
    static get business_area_service() {
        return AppServiceUtility.service(IBusinessAreaService, remote.url);
    }
    /** 服务运营服务 */
    static get service_operation_service() {
        return AppServiceUtility.service(IServiceOperationService);
    }
    /** 登录服务 */
    static get login_service() {
        return AppServiceUtility.service(ILoginService, remote.url);
    }

    /** 微信支付服务 */
    static get wechat_payment_service() {
        return AppServiceUtility.service(IServiceOrderService, remote.url);
    }

    /** 支付宝支付服务 */
    static get alipay_service() {
        return AppServiceUtility.service(IServiceOrderService, remote.url);
    }

    /** 护理模板列表 */
    static get nursing_template() {
        return AppServiceUtility.service(nursingTemplateService);
    }
    /** 护理记录列表 */
    static get nursing_record() {
        return AppServiceUtility.service(nursingRecordService);
    }
    /** 我的账户详情 */
    static get my_account_service() {
        return AppServiceUtility.service(IMyAccountService);
    }
    /** 支付服务 */
    static get app_pay_service() {
        return AppServiceUtility.service(IAppPayService);
    }

    /** 财务管理服务 */
    static get financial_service() {
        return AppServiceUtility.service(IFiancialService);
    }
    /** 服务记录服务 */
    static get service_record_service() {
        return AppServiceUtility.service(IServiceRecordService);
    }
    /** 易米云通服务 */
    static get emi_service() {
        return AppServiceUtility.service(IEmiService);
    }
    /** 易米云通服务 */
    static get emi_service_no_loading() {
        return AppServiceUtility.serviceNoLoading(IEmiService);
    }
    /** 家庭档案 */
    static get family_files_service() {
        return AppServiceUtility.service(IUserRelationShipService);
    }
    /** 服务关注/收藏 */
    static get service_follow_collection_service() {
        return AppServiceUtility.service(IServiceFollowCollectionService);
    }
    /** 家庭档案 */
    static get friends_circle() {
        return AppServiceUtility.service(IFriendsCircleService);
    }
    /** 意见反馈 */
    static get opinion_feedback() {
        return AppServiceUtility.service(IOpinionFeedbackService);
    }
    /** 意见反馈 */
    static get operation_record() {
        return AppServiceUtility.service(IOperationRecordService);
    }
    /** 服务包服务 */
    static get service_item_package() {
        return AppServiceUtility.service(IServiceItemPackage);
    }
    /** 服务项目服务 */
    static get services_project_service() {
        return AppServiceUtility.service(IServicesProjectService);
    }
    /** 服务适用范围服务 */
    static get service_scope_service() {
        return AppServiceUtility.service(IServiceScopeService);
    }
    /** 需求相关服务 */
    static get service_demand_service() {
        return AppServiceUtility.service(IDemandService);
    }
    /** 短信服务 */
    static get sms_manage_service() {
        return AppServiceUtility.service(ISmsManageService, remote.url);
    }
    /** 健康照护 */
    static get home_care_service() {
        return AppServiceUtility.service(IHomeCareService);
    }
    /** APP页面设置 */
    static get app_page_config_service() {
        return AppServiceUtility.service(IAppPageConfigService);
    }
    /** 我的家 */
    static get app_my_home_service() {
        return AppServiceUtility.service(IMyHomeService);
    }
    /** 查询商品详情 */
    static get shopping_mall_manage_service() {
        return AppServiceUtility.service(IShoppingMallManageService);
    }
    /** 分账登记管理服务 */
    static get ledger_account_manage_service() {
        return AppServiceUtility.service(ILedgerAccountManageService);
    }
}