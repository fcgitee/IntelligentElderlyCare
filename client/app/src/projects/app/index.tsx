/*
 * 版权: Copyright (c) 2018 red
 *
 * 文件: IntelligentElderlyCareApp.tsx
 * 创建日期: Sunday September 30th 2018
 * 作者: huyl
 * 说明:
 * 1、智慧养老系统应用
 */
import { addon, Ref, ISecurityService } from "pao-aop";

import { ReactApplication, AppReactRouterControl, updateTheme, Authentication } from "pao-aop-client";
import { theme_default } from "../style/theme/index";
import { IntelligentElderlyCareAppStorage } from "./appStorage";
import { IUserService } from "../models/user";
import { AppServiceUtility } from "./appService";

export let mainApplication: IntelligentElderlyCareApplication;

/**
 * 控件：智慧养老系统应用
 */
@addon('IntelligentElderlyCareApplication', '智慧养老系统应用', '智慧养老系统应用')
export class IntelligentElderlyCareApplication extends ReactApplication {
    /**
     * 物联网应用
     * @param mainForm 应用主窗体
     * @param userService_Fac 用户服务
     * @param securityService_Fac 安全服务
     */
    constructor(
        public router?: Ref<AppReactRouterControl>,
        public userService_Fac?: Ref<IUserService>,
        securityService_Fac?: Ref<ISecurityService>,
        public remote_url?: string) {
        super(router, securityService_Fac);
        AppServiceUtility.remote_url = this.remote_url!;
    }

    run?() {
        super.run!();
        mainApplication = this;
    }

    protected onAppBeforeStart?() {
        // 初始化全局数据事件方法
        Authentication.handleGlobalDataEvent = IntelligentElderlyCareAppStorage.handleGlobalDataEvent;
        IntelligentElderlyCareAppStorage.userService_Fac = this.userService_Fac;
        // 初始化主题
        updateTheme(theme_default, undefined, '/build/app/less.min.js');
    }
}