import Grid from "antd-mobile/lib/grid";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { ROUTE_PATH, getAppPermissonObject } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
import { LOCAL_FUNCTION_LIST } from "src/business/mainForm/backstageManageMainForm";
import { Modal } from 'antd';
import './index.less';
/**
 * 组件：养老补助导航页控件状态
 */
export interface PensionBenefitsState extends ReactViewState {
    /** 图标数据 */
    icon_data?: any[];
    show?: any;
}
/**
 * 组件：养老补助导航页控件
 */
export class PensionBenefits extends ReactView<PensionBenefitsControl, PensionBenefitsState> {
    constructor(props: any) {
        super(props);
        this.state = {
            icon_data: [
                { text: '居家补贴', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan39" /> },
                { text: '高龄津贴', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan37" /> },
                { text: '残疾人认证', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan37" /> },
                { text: '高龄津贴记录', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan37" /> },
                { text: '老人卡申请', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan38" /> },
            ],
            show: false
        };
    }
    componentDidMount = () => {
        const AppPermission = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);
        if (AppPermission && getAppPermissonObject(AppPermission, '高龄津贴未认证', '查询')) {
            let icon_data: any = this.state.icon_data;
            icon_data.push({ text: '高龄津贴未认证', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan33" /> });
            this.setState({
                icon_data
            });
        }
    }
    grid_click = (_el: any) => {
        switch (_el.text) {
            case '高龄津贴':
                this.props.history!.push(ROUTE_PATH.applyReadinClause);
                break;
            case '残疾人认证':
                this.props.history!.push(ROUTE_PATH.disabledPerson);
                break;
            case '高龄津贴未认证':
                this.props.history!.push(ROUTE_PATH.uncertifiedList);
                break;
            case '居家补贴':
                this.props.history!.push(ROUTE_PATH.applicationSubsidies);
                break;
            case '高龄津贴记录':
                this.props.history!.push(ROUTE_PATH.oldapplyState);
                break;
            case '老人卡申请':
                this.setState({ show: true });
                break;
            default:
                break;
        }
    }

    render() {
        setMainFormTitle("政策类申请");
        return (
            <div>
                <Grid data={this.state.icon_data} activeStyle={true} columnNum={3} onClick={_el => this.grid_click(_el)} />
                <Modal
                    title="提示"
                    visible={this.state.show}
                    okText='确认'
                    cancelText='取消'
                    onOk={() => { this.setState({ show: false }); }}
                    onCancel={() => { this.setState({ show: false }); }}
                >
                    <p style={{ textAlign: 'center' }}>待开发</p>
                </Modal>
            </div >
        );
    }
}

/**
 * 组件：养老补助导航页控件
 * 控制养老补助导航页控件
 */
@addon('PensionBenefits', '养老补助导航页控件', '控制养老补助导航页控件')
@reactControl(PensionBenefits, true)
export class PensionBenefitsControl extends ReactViewControl {
}