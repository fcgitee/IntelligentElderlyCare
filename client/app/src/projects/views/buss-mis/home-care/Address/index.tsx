import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Form } from "antd";
import { InputItem, Button } from 'antd-mobile';
// import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：地址首页状态
 */
export interface AddressViewState extends ReactViewState {
}

/**
 * 组件：地址首页
 * 描述
 */
export class AddressView extends ReactView<AddressViewControl, AddressViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onChange = () => {
        // console.log(11);
    }
    goBack = () => {
        // this.props.history!.push(ROUTE_PATH.submitOrder);
        history.back();
    }
    render() {
        return (
            <div>
                {/* <InputItem
            {...getFieldProps('autofocus')}
            clear
            placeholder="auto focus"
            ref={el => this.autoFocusInst = el}
            >标题</InputItem> */}
                <InputItem>姓名：</InputItem>
                <InputItem>联系电话：</InputItem>
                <InputItem>详细地址：</InputItem>
                <Button type="primary" onClick={this.goBack}>确认</Button>
            </div>
        );
    }
}

/**
 * 控件：地址首页控制器
 * 描述
 */
@addon('AddressView', '地址首页', '描述')
@reactControl(Form.create<any>()(AddressView), true)
export class AddressViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}