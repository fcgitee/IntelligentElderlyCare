import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List } from 'antd-mobile';
const Item = List.Item;
import { Modal } from 'antd';
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：政策法规状态
 */
export interface CharacteristicServiceViewState extends ReactViewState {
    visible?: any;
    modal_text?: any;
}

/**
 * 组件：政策法规
 * 描述
 */
export class CharacteristicServiceView extends ReactView<CharacteristicServiceViewControl, CharacteristicServiceViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            visible: false,
            modal_text: ''
        };
    }
    componentDidMount = () => {
        // console.log(this.props.match!.params.key);
    }
    goDatail = (text: any) => {
        // console.log(text);
        this.setState({
            visible: true
        });
        let modal_text = '';
        if (text === '学习强国') {
            modal_text = 'https://www.xuexi.cn/';
        }
        if (text === '法律法规') {
            modal_text = 'http://www.gov.cn/banshi/gm/flfg.htm';
        }
        if (text === '养老政策') {
            modal_text = 'http://www.nanhai.gov.cn/cms/html/71657/column_71657_1.html';
        }
        if (text === '中医养生') {
            modal_text = 'https://www.ys137.com/zyys/';
        }
        if (text === '护理康复') {
            modal_text = 'https://www.kfyx.cn/portal.php';
        }
        if (text === '心理健康') {
            modal_text = 'http://www.chinaxlbj.org/';
        }
        if (text === '营养攻略') {
            modal_text = 'http://www.ttmeishi.com/';
        }
        if (text === '食物供给') {
            modal_text = 'http://www.co.com.cn/category-42-b0.html';
        }
        if (text === '药膳推荐') {
            modal_text = 'https://jingyan.baidu.com/z/yaoshan/index.html';
        }
        if (text === '饮食检测') {
            modal_text = 'http://jiankang.cntv.cn/yeekang/index.shtml';
        }
        if (text === '神经系统检测') {
            modal_text = 'http://jiankang.cntv.cn/yeekang/qita/index.shtml';
        }
        if (text === '基因检测') {
            modal_text = 'http://gz.hejydna.com/';
        }
        if (text === '康旅攻略') {
            modal_text = 'http://travel.cnr.cn/';
        }
        if (text === '驴友召集') {
            modal_text = 'http://www.lvy.cn/';
        }
        if (text === '陪同帮助') {
            modal_text = 'https://fs.daojia.com/lvyou/';
        }
        if (text === '孝道历史') {
            modal_text = 'http://www.zhxdjsw.com/';
        }
        if (text === '文化传承') {
            modal_text = 'http://www.gxwhcc.com/';
        }
        if (text === '孝道教育') {
            modal_text = 'http://zhxwh.hbeu.cn/rcpy/jypx.htm';
        }
        Modal.info({
            title: '请复制以下内容到浏览器中打开',
            content: (
                <div>
                    <p>{modal_text}</p>
                </div>
            ),
            onOk() { },
        });
    }

    handleOk = (e: any) => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e: any) => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    }
    render() {
        return (
            <div>
                <div style={{ width: '100%', overflow: 'hidden  ' }}>
                    <div style={{ backgroundColor: 'white', width: '30%', height: '140px', float: 'left', borderRadius: '20px', margin: '20px 10px 30px 20px' }}><img style={{width: '100%', height: '100%'}} src='https://www.e-health100.com/api/attachment/iconmanagement_ServiceType/logo_78'/></div>
                    <div style={{ backgroundColor: 'white', textAlign: 'center', width: '54%', height: '140px', float: 'left', borderRadius: '20px', margin: '20px 20px 30px 10px' }}>文字简介</div>
                </div>
                {this.props.match!.params.key === '政策法规' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('学习强国'); }} arrow="horizontal">学习强国</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('法律法规'); }} arrow="horizontal">法律法规</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('养老政策'); }} arrow="horizontal">养老政策</Item>
                    </List>
                    :
                    ''
                }
                {this.props.match!.params.key === '长者教育' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('中医养生'); }} arrow="horizontal">中医养生</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('护理康复'); }} arrow="horizontal">护理康复</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('心理健康'); }} arrow="horizontal">心理健康</Item>
                    </List>
                    :
                    ''
                }
                {this.props.match!.params.key === '健康美食' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '每日一膳'); }} arrow="horizontal">每日一膳</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('营养攻略'); }} arrow="horizontal">营养攻略</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('食物供给'); }} arrow="horizontal">食物供给</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('药膳推荐'); }} arrow="horizontal">药膳推荐</Item>
                    </List>
                    :
                    ''
                }
                {this.props.match!.params.key === '预防检测' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('饮食检测'); }} arrow="horizontal">饮食检测</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('神经系统检测'); }} arrow="horizontal">神经系统检测</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('基因检测'); }} arrow="horizontal">基因检测</Item>
                    </List>
                    :
                    ''
                }
                {this.props.match!.params.key === '休闲旅游' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('康旅攻略'); }} arrow="horizontal">康旅攻略</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('驴友召集'); }} arrow="horizontal">驴友召集</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('陪同帮助'); }} arrow="horizontal">陪同帮助</Item>
                    </List>
                    :
                    ''
                }
                {this.props.match!.params.key === '孝道文化' ?
                    <List className="myList2">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('孝道历史'); }} arrow="horizontal">孝道历史</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('文化传承'); }} arrow="horizontal">文化传承</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} onClick={() => { this.goDatail('孝道教育'); }} arrow="horizontal">孝道教育</Item>
                    </List>
                    :
                    ''
                }
            </div>
        );
    }
}

/**
 * 控件：政策法规控制器
 * 描述
 */
@addon('CharacteristicServiceView', '政策法规', '描述')
@reactControl(CharacteristicServiceView, true)
export class CharacteristicServiceViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}