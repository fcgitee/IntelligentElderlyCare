import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Modal, } from "antd-mobile";
import { Row, Col, Card } from "antd";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
const alert = Modal.alert;
let indesx = 0;
/**
 * 组件：服务商页面视图控件状态
 */
export interface ServiceProviderListViewState extends ReactViewState {
    btnState?: any;
    food_list?: any;
    modal?: any;
    list?: any;
    menu_list?: any;
    tabs?: any;
    buy_list?: any;
    price?: any;
    tab_show_list?: any;
    food_data?: any;
    visible?: any;
    buy_info?: any;
    /** 区域列表 */
    areaList?: any;
    /** 服务商列表 */
    ServiceProviderList?: any;
    areaId?: any;
    modal1?: boolean;
    objList?: any;
}
/**
 * 组件：服务商页面视图控件
 */
export class ServiceProviderListView extends ReactView<ServiceProviderListViewControl, ServiceProviderListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            btnState: [],
            list: [{ 'id': 1, 'food_name': '打扫房间', 'food_detail': '...', 'tuijian': '6', 'surplus': '10', 'sell': '30', 'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1572521604937&di=b25d6c8984070b81e08bf353b1755dc5&imgtype=0&src=http%3A%2F%2Fpic.51yuansu.com%2Fpic3%2Fcover%2F01%2F99%2F64%2F5984bfbfa9e94_610.jpg', 'price': 50 },
            { 'id': 2, 'food_name': '清洗衣服', 'food_detail': '...', 'tuijian': '6', 'surplus': '10', 'sell': '30', 'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1572521639479&di=c913af65cf6bb261c50654534334d575&imgtype=0&src=http%3A%2F%2Fwww.tbw-xie.com%2FtuxieJDEwLmFsaWNkbi5jb20vaTQvMjI2MDI2MDQ0Ny9UQjJYOHFiY1BneV91SmpTWlNnWFhiejBYWGFfISEyMjYwMjYwNDQ3JDk.jpg', 'price': 100 },
            { 'id': 3, 'food_name': '剪发', 'food_detail': '...', 'tuijian': '6', 'surplus': '10', 'sell': '30', 'image': 'http://img4.imgtn.bdimg.com/it/u=2884420034,1776269664&fm=26&gp=0.jpg', 'price': 30 }],
            food_list: [],
            buy_list: [],
            modal: false,
            price: 0,
            tab_show_list: [],
            food_data: [],
            visible: false,
            buy_info: {},
            areaList: [],
            ServiceProviderList: [],
            modal1: true,
            objList: [],
        };
    }
    // areaChoose = () => {
    //     let list: any = [];
    //     this.state.areaList.map((item: any) => {
    //         list.push({
    //             text: item.name, onPress: () => {
    //                 // console.log(item.id);
    //                 this.selectServiceProvider(item.id);
    //             }
    //         });
    //     });
    //     // alert('服务区域', '请选择你的区域', list);
    // }
    // 这里的区域信息应该发送请求获取
    areaChoose = () => {
        let list: any = [];
        console.info('=========', this.state.areaList);
        this.state.areaList.map((item: any) => {
            list.push({
                text: item.name, onPress: () => {
                    // console.log(item.id);
                    let areaId = item.id;
                    this.setState({
                        areaId,
                    });
                    // console.log(this.state.areaId);
                    this.selectServiceProvider(item.id);
                }
            });
        });
        // console.log(list);
        alert('服务区域', <div style={{width:'240px', height:'100%'}}>请选择你的区域</div>, list);
    }
    selectServiceProvider = (id: any) => {
        this.setState({
            modal1: false,
        });
        request(this, AppServiceUtility.service_operation_service.get_service_provider_list_all!({ admin_area_id: id }))
            .then((data: any) => {
                // console.log("服务商信息>>>>>>>>>>>>>", data);

                this.setState({
                    list: data['result'],

                });
                this.listChange();
            });
    }
    componentDidMount() {
        // 为了处理tabBar重复渲染，用indexs来判断。
        indesx++;
        if (indesx > 1) {
            return;
        }
        request(this, AppServiceUtility.business_area_service.get_all_admin_division_list!({}))
            .then((data: any) => {
                let areaList = this.state.areaList;
                // let objList: any = [];
                data.result.map((item: any) => {
                    areaList.push({ 'name': item.name, 'id': item.id });
                });
                this.setState({
                    areaList
                });
                this.areaChoose();
                // areaList.map((value: any, index: number) => {
                //     objList.push((<div onClick={this.selectServiceProvider.bind(this, value.id!)} key={index} style={{ fontWeight: 600, background: "#fff", textAlign: "center", padding: '15px 0' }}>{value.name}</div>));
                // });
                // this.setState({
                //     objList
                // });
            });

    }
    componentWillUnmount() {
        indesx = 0;
    }
    onClose = () => {
        this.setState({
            modal: false,
        });
    }
    showModal = (e: any) => {
        e.preventDefault(); // 修复 Android 上点击穿透
        this.setState({
            modal: true,
        });
    }
    // 查找按钮的状态
    btnDisable = (id: any) => {
        if (this.state.btnState.length > 0) {
            let btnState = false;
            this.state.btnState.map((item: any) => {
                if (item.id === id) {
                    return btnState = item.state;
                }
            });
            // console.log(btnState);
            return btnState;
        }
        return false;
    }
    listChange = () => {
        let food_list: any = [];
        this.state.list.map((item: any, index: any) => {
            let list = (
                <div className="clean-list" key={index} onClick={() => this.goService(item.id)} style={{ margin: '0px' }}>
                    <Card className='list-conten'>
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={12}><img src={item.organization_info && item.organization_info.picture_list ? item.organization_info.picture_list[0] : ''} style={{ height: '72pt', width: '130px' }} /></Col>
                            <Col span={12} className='list-col'>
                                <Row><strong>{item.name}</strong></Row>
                                <Row>地址：{item.organization_info.address}</Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
            food_list.push(list);
        });
        this.setState({
            food_list
        });
    }
    // 跳转到详情页
    goService = (id: any) => {
        this.props.history!.push(ROUTE_PATH.servicePackageList + '/' + id);
    }
    // 显示日期框
    showDatePicker = (e: any, id: any, name: any, price: any, food_detail: any, image: any) => {
        e.stopPropagation();
        let list = { id, name, price, food_detail, image };
        // console.log(list);
        this.setState({
            visible: true,
            buy_info: list
        });
    }
    // 日期框确定回调
    DatePickerOK = () => {
        this.setState({
            visible: false
        });
    }
    //  日期框取消回调
    DatePickerCancel = () => {
        this.setState({
            visible: false
        });
    }
    // 处理时间
    formatDate = (date: any) => {
        const pad = (n: any) => n < 10 ? `0${n}` : n;
        const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
        const timeStr = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
        return `${dateStr} ${timeStr}`;
    }

    render() {
        return (
            <div style={{ paddingLeft: '6px', paddingRight: '6px', }}>
                {/* <div style={{ display: this.state.modal1 ? 'block' : 'none', height: '100%', overflowY: 'auto' }}>
                    <div style={{ fontWeight: 600, background: "#fff", textAlign: "center", padding: '15px 0' }}>请选择区域</div>
                    {this.state.objList}
                </div> */}
                <div className="clean-list-content">
                    {this.state.food_list.length > 0 ? this.state.food_list : <div style={{ textAlign: 'center' }}>暂无服务商数据</div>}
                </div>
            </div>
        );
    }
}

/**
 * 组件：服务商页面视图控件
 * 控制服务商页面视图控件
 */
@addon('ServiceProviderListView', '服务商页面视图控件', '控制服务商页面视图控件')
@reactControl(ServiceProviderListView, true)
export class ServiceProviderListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}