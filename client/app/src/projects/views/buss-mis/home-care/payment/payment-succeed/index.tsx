import { Icon } from 'antd';
import { Result } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';

// const wechatPicture = require('src/static/img/success_hook.jpg');
const MAX_COUNT = 5;
/**
 * 组件：支付成功状态
 */
export interface PaymentSucceedViewState extends ReactViewState {
    status?: any;
    count: number;
    loading: boolean;
    if_pay: boolean;
    data?: any;
}

/**
 * 组件：支付成功
 * 描述
 */
export class PaymentSucceedView extends ReactView<PaymentSucceedViewControl, PaymentSucceedViewState> {
    private INTERVAL: any;
    constructor(props: any) {
        super(props);
        this.state = {
            status: true,
            count: 0,
            if_pay: false,
            data: undefined,
            loading: true
        };
    }

    componentDidMount() {
        let { count } = this.state;
        this.INTERVAL = setInterval(
            () => {
                this.checkIfPay();
                if (count >= MAX_COUNT) {
                    this.setState({ loading: false });
                    clearInterval(this.INTERVAL);
                }
                this.setState({
                    count: ++count
                });
            },
            1000
        );

    }

    checkIfPay = () => {
        let locationState = this.props.location!.state;
        const out_trade_no = locationState && locationState['out_trade_no'] ? locationState['out_trade_no'] : '';

        AppServiceUtility.service_order_service.third_trade_query!({
            'out_trade_no': out_trade_no
        })!.then((data: any) => {
            this.setState({
                data
            });
            if (data.length === 0) {
                // Toast.fail('长度为0');
                return;
            } else if (data.length > 0) {
                // 已经付款，但是交易表查询结果未必是付款
                // Toast.fail('长度不为0：' + out_trade_no);
                if (data[0]['pay_status'] === '已支付') {
                    // Toast.fail('长度不为0：' + out_trade_no);
                    this.setState({
                        if_pay: true,
                        loading: false
                    });
                    clearInterval(this.INTERVAL);
                }
            }
        });
    }

    resultTitle = () => {
        const { if_pay, loading } = this.state;
        if (if_pay) {
            return '支付成功';
        } else {
            if (loading) {
                return '支付结果查询中' + ''.padEnd(this.state.count, '.');
            } else {
                return '支付失败';
            }
        }
    }

    resultImg = () => {
        const { if_pay, loading } = this.state;
        if (if_pay) {
            let locationState = this.props.location!.state;
            const pay_type = locationState && locationState['pay_way'] ? locationState['pay_way'] : '';
            switch (pay_type) {
                case '微信支付':
                    return <Icon type="wechat" style={{ color: "#44b549", fontSize: '3em' }} />;
                case '支付宝支付':
                    return <Icon type="alipay-circle" style={{ color: "#00abee", fontSize: '3em' }} />;
                default:
                    return <Icon type="loading" style={{ fontSize: '3em' }} />;
            }
        } else {
            if (loading) {
                return <Icon type="loading" spin={true} style={{ fontSize: '3em' }} />;
            } else {
                return <Icon type="exclamation-circle" style={{ color: "red", fontSize: '3em' }} />;
            }
        }
    }

    resultText = () => {
        const { if_pay } = this.state;
        if (if_pay) {
            return (this.state.data[0]['total_fee'] / 100) + '元';
        } else {
            return '';
        }
    }

    resultBtnText = () => {
        return '完成';
    }

    onResultBtnClick = (): (() => void) | undefined => {
        const { if_pay, loading } = this.state;
        if (if_pay) {
            return () => {
                this.props.history!.push(ROUTE_PATH.myOrder);
            };
        } else {
            if (loading) {
                return undefined;
            } else {
                return () => {
                    this.props.history!.push(ROUTE_PATH.myOrder);
                };
            }
        }
    }

    succeed = () => {
        this.props.history!.push(ROUTE_PATH.home);
    }

    render() {
        return (
            <div>
                <Result
                    img={this.resultImg()}
                    buttonText={this.resultBtnText()}
                    title={this.resultTitle()}
                    message={this.resultText()}
                    onButtonClick={this.onResultBtnClick()}
                />
            </div>
        );
    }
}

/**
 * 控件：支付成功控制器
 * 描述
 */
@addon('PaymentSucceedView', '支付成功', '描述')
@reactControl(PaymentSucceedView, true)
export class PaymentSucceedViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}