import { Col, Icon, message, Modal, Row, Spin } from 'antd';
import { Button, List, Radio, Toast, WhiteSpace } from 'antd-mobile';
import { addon, log } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { getPlatformType, getWebViewRetObjectType, setMainFormTitle, subscribeWebViewNotify } from 'src/business/util_tool';
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { decodeUrlParam, encodeUrlParam, PAGE_TYPE, ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
const RadioItem = Radio.RadioItem;
/**
 * 组件：订单支付首页状态
 */
export interface PaymentViewState extends ReactViewState {
    deadline?: any;
    pay_type?: any;
    value?: any;
    price?: any;
    /** 购买产品列表json信息字符串 */
    buy_list_json_str?: string;
    /** 请求支付信息接口返回的信息 */
    pay_mount_result?: any;
    /** 支付类型 */
    page_type?: string;
    /** 是否正在查询是否付款 */
    pay_modal?: boolean;
    loading: boolean;
    paying: boolean;
    check_pay: boolean;
    // loading_cookie: boolean;
}

/**
 * 组件：订单支付首页
 * 描述
 */
export class PaymentView extends ReactView<PaymentViewControl, PaymentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            deadline: Date.now() + 1000 * 60 * 15,
            pay_type: [
                // { value: 0, label: '银行卡支付', extra: 'money-collect' },
                { value: '微信支付', label: '微信支付', extra: 'wechat', disabled: false },
                { value: '支付宝支付', label: '支付宝支付', extra: 'alipay-circle', disabled: false },
                // { value: 3, label: '预存支付', extra: 'pay-circle' },
                // { value: 4, label: '扫脸支付', extra: 'smile' }
            ],
            value: '微信支付',
            price: 0,
            pay_modal: false,
            loading: true,
            paying: false,
            check_pay: false
            // loading_cookie: true
        };
    }

    componentWillMount() {
        const messageCb = (e: any) => {
            const retObject = JSON.parse(e.data);
            // if (getWebViewRetObjectType(retObject) === 'webView') {
            //     this.setState({
            //         pay_modal: true,
            //         paying: false
            //     });
            // }
            if (getWebViewRetObjectType(retObject) === 'payDone') {
                this.setState({
                    check_pay: true
                });
            }
            // if (getWebViewRetObjectType(retObject) === 'getCookie') {
            //     const cookie = JSON.parse(retObject.data);
            //     CookieUtil.save(COOKIE_KEY_CURRENT_USER, cookie[COOKIE_KEY_CURRENT_USER]);
            //     CookieUtil.save(CurrentTokenCookieName, cookie[CurrentTokenCookieName]);
            //     this.setState({ loading_cookie: false });
            // }
        };

        // if (CookieUtil.read(COOKIE_KEY_CURRENT_USER)) {
        //     this.setState({ loading_cookie: false });
        // }

        subscribeWebViewNotify(messageCb);
    }

    shouldComponentUpdate(nextProps: PaymentViewControl, nextState: PaymentViewState) {
        if (nextState.check_pay !== this.state.check_pay) {
            this.preHandle();
        }
        return true;
    }

    preHandle() {
        let buy_list_json = this.getBuyListJson();

        // this.setState({
        //     loading_cookie: false
        // });
        const user = IntelligentElderlyCareAppStorage.getCurrentUser();
        // if ((window as any).ReactNativeWebView) {
        //     if (getPlatformType() === 'android') {
        //         (window as any).ReactNativeWebView.postMessage(
        //             JSON.stringify({
        //                 function: 'setCookie',
        //                 params: JSON.stringify({ [COOKIE_KEY_CURRENT_USER]: CookieUtil.read(COOKIE_KEY_CURRENT_USER), [CurrentTokenCookieName]: CookieUtil.read(CurrentTokenCookieName) })
        //             })
        //         );
        //     }
        // }

        if (buy_list_json.page_type === PAGE_TYPE.BUY) {
            AppServiceUtility.app_pay_service.receive_pay_info!(
                {
                    // TODO: userid测试
                    payer_id: user.id,
                    info: buy_list_json.page_info
                }
            )!.then((data: any) => {
                if (this.state.check_pay) {
                    this.setState({
                        pay_modal: true,
                        paying: false
                    });
                }
                // if (buy_list_json['checkPay']) {
                //     this.setState({
                //         pay_modal: true,
                //         paying: false
                //     });
                // }
                this.setState(
                    {
                        page_type: PAGE_TYPE.BUY,
                        pay_mount_result: data,
                        loading: false
                    },
                    () => {
                        const amount = this.getRealPay();
                        if (amount <= 0) {
                            this.setState({
                                pay_type: this.state.pay_type.map((e: any, i: number) => {
                                    e.disabled = true;
                                    return e;
                                })
                            });
                        }
                    });
                // console.log(this.getRealPay());
            });
        } else if (buy_list_json.page_type === PAGE_TYPE.CHARGE) {
            this.setState({
                page_type: PAGE_TYPE.CHARGE,
                pay_mount_result: buy_list_json.page_info,
                loading: false
            });
        } else if (buy_list_json.page_type === PAGE_TYPE.DONATION) {
            this.setState({
                page_type: PAGE_TYPE.DONATION,
                pay_mount_result: buy_list_json.page_info,
                loading: false
            });
        }
    }

    componentDidMount() {
        this.preHandle();
    }

    getRealPay = () => {
        if (this.state.page_type === PAGE_TYPE.CHARGE) {
            return this.state.pay_mount_result;
        }
        if (this.state.page_type === PAGE_TYPE.DONATION) {
            return this.state.pay_mount_result;
        }
        if (this.state.page_type === PAGE_TYPE.BUY) {
            return this.state.pay_mount_result['real']['amount'];
        }
    }

    getBuyListJson = () => {
        let buy_list_json_str_b64 = this.props.match!.params.key;
        let buy_list_json = JSON.parse(decodeUrlParam(buy_list_json_str_b64));
        return buy_list_json;
    }

    goResult = () => {
        let buy_list_json = this.getBuyListJson();
        const user = IntelligentElderlyCareAppStorage.getCurrentUser();
        const amount = this.getRealPay();

        this.setState({
            paying: true
        });
        if (amount <= 0) {
            // 不调用第三方支付接口
            log('支付模块', '不调用第三方接口');
            AppServiceUtility.app_pay_service.confirm_pay!(
                undefined,
                {
                    payer_id: user.id,
                    info: buy_list_json.page_info
                }
            )!.then((data) => {
                if (data === true) {
                    const payment_info = {
                        /** 支付类型 */
                        type: this.state.value,
                        /** 金额 */
                        amount: this.getRealPay()
                    };
                    this.props.history!.push(ROUTE_PATH.paymentSucceed + '/' + encodeUrlParam(JSON.stringify(payment_info)));
                } else {
                    message.error('支付失败！');
                }
            });
        } else {
            // 调用第三方支付
            if (this.state.value === '微信支付') {
                let sendJson = {
                    page_title: document.title,
                    product_desc: buy_list_json['product_desc'],
                    // total_fee: this.getRealPay(),
                    total_fee: this.getRealPay(),
                    out_trade_no: buy_list_json['out_trade_no'],
                };
                if ((buy_list_json as Object).hasOwnProperty("attach")) {
                    sendJson['attach'] = buy_list_json['attach'];
                }
                // buy_list_json['checkPay'] = true;
                if (getPlatformType() === 'ios') {
                    sendJson['redirect_url'] = encodeURIComponent(window.location.host + ":/" + ROUTE_PATH.payment + '/' + encodeUrlParam(JSON.stringify(buy_list_json)));
                } else {
                    sendJson['redirect_url'] = encodeURIComponent(window.location.origin + ROUTE_PATH.payment + '/' + encodeUrlParam(JSON.stringify(buy_list_json)));
                }
                // 给财务模块调用的参数
                sendJson['pay_params'] = (() => {
                    if (buy_list_json['page_type'] === PAGE_TYPE.BUY) {
                        return {
                            payer_id: user.id,
                            info: buy_list_json.page_info
                        };
                    }
                    if (buy_list_json['page_type'] === PAGE_TYPE.CHARGE || buy_list_json['page_type'] === PAGE_TYPE.DONATION) {
                        return {
                            payer_id: user.id,
                            info: this.getRealPay(),
                            remarks: buy_list_json['remarks'],
                            abstract: buy_list_json['abstract']
                        };
                    }
                    return {};
                })();
                AppServiceUtility.my_order_service.wechat_payment!(
                    sendJson
                )!.then((data: any) => {
                    if (data['err_code']) {
                        if (data['err_code'] === 'ORDERPAID') {
                            Toast.success('该订单已支付成功！');
                            setTimeout(
                                () => {
                                    this.props.history!.push(ROUTE_PATH.paymentSucceed, { out_trade_no: buy_list_json['out_trade_no'], pay_way: this.state.value });
                                },
                                1000
                            );
                            return;
                        }
                    }
                    window.location.href = data.mweb_url;
                });
            }
            if (this.state.value === '支付宝支付') {
                log('支付模块', '支付宝支付');
                AppServiceUtility.app_pay_service.alipay!(

                )!.then(data => {

                });
            }
        }
    }

    radioChange = (value: any) => {
        this.setState({
            value
        });
    }

    render() {
        const { value } = this.state;
        let ele = <></>;
        console.warn(this.state.pay_mount_result);
        setMainFormTitle('支付');
        if (this.state.pay_mount_result) {
            ele = (
                <div className="main-content">
                    {/* <div className="countTime">
                        剩余时间：<Countdown className="time-out" value={this.state.deadline} format="mm:ss" />
                    </div>
                    <div className="price">
                        ￥{this.getRealPay()}
                    </div>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" /> */}
                    <List renderHeader={'请选择一下支付方式'}>
                        {this.state.pay_type.map((item: any, i: number) => {
                            return (<RadioItem key={item.value} checked={value === item.value} disabled={item.disabled} onChange={() => this.radioChange(item.value)}><Icon type={item.extra} />&nbsp;&nbsp;{item.label}</RadioItem>);
                        })}
                    </List>
                    <div className="submit-btn">

                        <Row>
                            <Col span={12}>
                                <Button className={"btn-amount"} style={{ pointerEvents: "none" }}  >
                                    {'￥' + this.getRealPay()}
                                </Button>
                            </Col>
                            <Col span={12}>
                                <Button type="primary" onClick={this.goResult}>确认支付</Button>
                            </Col>
                        </Row>
                    </div>
                    <Modal
                        visible={this.state.pay_modal}
                        closable={false}
                        footer={null}
                        title={"支付"}
                    >
                        <Button
                            type={'primary'}
                            onClick={
                                () => {
                                    let buy_list_json = this.getBuyListJson();
                                    this.props.history!.push(ROUTE_PATH.paymentSucceed, { out_trade_no: buy_list_json['out_trade_no'], pay_way: this.state.value });
                                }
                            }
                        >
                            已完成支付
                        </Button>
                        <WhiteSpace />
                        <Button type={"warning"} onClick={() => this.setState({ pay_modal: false, paying: false, check_pay: false })}>支付遇到问题，重新支付</Button>
                    </Modal>
                </div>
            );
        }

        return (
            <Spin spinning={this.state.loading || this.state.paying}>
                {ele}
            </Spin>
        );
    }
}

/**
 * 控件：订单支付首页控制器
 * 描述
 */
@addon('PaymentView', '订单支付首页', '描述')
@reactControl(PaymentView, true)
export class PaymentViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}