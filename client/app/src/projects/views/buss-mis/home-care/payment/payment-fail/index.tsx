import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Result, Button, Icon } from 'antd-mobile';
import './index.less';
/**
 * 组件：支付失败状态
 */
export interface PaymentFailViewState extends ReactViewState {
}

/**
 * 组件：支付失败
 * 描述
 */
export class PaymentFailView extends ReactView<PaymentFailViewControl, PaymentFailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    succeed = () => {
        this.props.history!.goBack();
    }
    render() {
        return (
            <div>
                <Result
                    img={<Icon type="cross-circle-o" className="spe" style={{ fill: '#F13642' }} />}
                    title="支付失败"
                />
                <div className="submit-btn"><Button type="primary" onClick={this.succeed}>确定</Button></div>
            </div>
        );
    }
}

/**
 * 控件：支付失败控制器
 * 描述
 */
@addon('PaymentFailView', '支付失败', '描述')
@reactControl(PaymentFailView, true)
export class PaymentFailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}