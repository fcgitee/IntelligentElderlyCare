import { Button, Card, Col, Form, Input, Row, Select, Radio, Checkbox, message } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { AppServiceUtility } from "src/projects/app/appService";
import TextArea from "antd/lib/input/TextArea";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { Multiple } from "src/business/components/buss-components/multiple";
// import { ModalSearch } from "src/business/components/buss-components/modal-search";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { ModalSearch } from "src/business/components/buss-components/modal-search";

/**
 * 组件：名称状态
 */
export interface AddProductViewState extends ReactViewState {
    /** 项目列表 */
    item_list?: any[];
    /** 项目列表集合 */
    all_item_list?: any;
    /** 服务产品列表 */
    service_product_list?: any[];
    /** 服务产品列表 */
    all_service_product_list?: any[];
    /** 基础数据 */
    base_data?: any;
    /** 适用范围 */
    application_scope?: any[];
    /** 组织机构 */
    organizational?: any[];
    /** 服务项目可视性 */
    is_service_item?: boolean;
    // 切换的下拉选择
    select_id?: any;
    catering_state?: any;
}
export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：名称
 * 描述
 */
export class AddProductView extends ReactView<AddProductViewControl, AddProductViewState> {
    /** 总项目列表 */
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            all_item_list: '',
            base_data: {},
            application_scope: [],
            organizational: [],
            is_service_item: true,
            service_product_list: [],
            all_service_product_list: [],
            select_id: '',
            catering_state: false
        };
    }
    /** 提交方法 */
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        let is_service_item = this.state.is_service_item;
        form!.validateFields((err: Error, values: ServiceItemPackageFormValues) => {
            if (err) {
                let errKeys = Object.keys(err);
                // 在服务套餐下，这里会导致一直报错
                if (errKeys.length !== 1 || errKeys[0] !== 'service_item___0') {
                    return;
                }
            }
            let formValue = {};
            let service_item: any = [];
            let service_product: any = [];
            let service_option = this.state.all_item_list.service_option;
            service_item.push({ service_options: [] });
            for (let key in values) {
                if (key.indexOf('___') !== -1) {
                    let keys = key.split('___');
                    if (keys[0] === 'service_type') {
                        service_item[parseInt(keys[1], undefined)][keys[0]] = values[key];
                    } else if (keys[0] === 'service_item') {
                        service_item[parseInt(keys[1], undefined)][keys[0]] = values[key];
                        formValue['service_item_id'] = values[key];
                    } else if (keys[0] === 'service_product') {
                        // 因为可以重复选，所以这里判断一下是否有，有的话就叠加解决，没有就新增
                        let isHave = false;
                        if (service_product.length > 0) {
                            for (let i in service_product) {
                                if (service_product[i]) {
                                    // 已经有，叠加次数
                                    if (service_product[i]['product_id'] === values[key]) {
                                        isHave = true;
                                        service_product[i]['remaining_times'] = String(Number(service_product[i]['remaining_times']) + Number(values['service_times___' + keys[1]]));
                                    }
                                    // 最后一次来判断
                                    if (Number(i) === service_product.length - 1 && isHave === false) {
                                        // 新增
                                        service_product.push({
                                            product_id: values[key],
                                            remaining_times: values['service_times___' + keys[1]]
                                        });
                                    }
                                }
                            }
                        } else {
                            // 新增
                            service_product.push({
                                product_id: values[key],
                                remaining_times: values['service_times___' + keys[1]]
                            });
                        }
                    } else if (keys[0] === 'valuation_formula') {
                        formValue['valuation_formula'] = values[key];
                    } else if (keys[0] === 'proportion') {
                        formValue['proportion'] = values[key];
                    } else {
                        is_service_item === true && service_option.forEach((item: any) => {
                            if (item['id'] === keys[0]) {
                                item['option_value'] = values[key];
                            }
                        });
                    }
                } else {
                    formValue[key] = values[key];
                }
            }
            if (is_service_item === true) {
                formValue['service_option'] = service_option;
            } else if (is_service_item === false) {
                formValue['service_product'] = service_product;
            }
            // formValue['service_item'] = service_item;
            // formValue['service_option'] = service_option;
            // formValue['service_product'] = service_product;
            // formValue['id'] = this.props.match!.params.key;
            // 如果是服务产品，就固定这两个参数为1
            if (!is_service_item) {
                // 是否可分派
                formValue['is_assignable'] = '1';
                // 服务次数
                formValue['remaining_times'] = '1';
            }
            AppServiceUtility.service_item_package.update_service_item_package!(formValue)!
                .then((data) => {
                    if (data) {
                        message.info('保存成功');
                        this.props.history!.goBack();
                    }
                });
        });
    }
    componentWillMount() {
        // 服务项目
        AppServiceUtility.services_project_service.get_services_project_list!({})!
            .then((data: any) => {
                if (data) {
                    this.setState({
                        item_list: data['result']
                    });
                }
            });
    }
    componentDidMount() {
        // 服务产品
        AppServiceUtility.service_item_package.get_service_item_package!({})!
            .then((data) => {
                this.setState({
                    service_product_list: data!.result,
                });
            });
        // 适用范围
        AppServiceUtility.service_scope_service.get_service_scope_list!({})!
            .then((data) => {
                let data_sourd: any[] = [];
                data!.result!.map((value) => {
                    data_sourd.push({ label: value.name, value: value.id });
                });
                this.setState({
                    application_scope: data_sourd
                });
            });
        // 组织机构
        AppServiceUtility.person_org_manage_service.get_organization_list_all!({id:this.props.match!.params.key})!
            .then((data) => {
                this.setState({
                    organizational: data.result,
                });
            });
        // if (this.props.match!.params.key) {
        //     AppServiceUtility.service_item_package.get_service_item_package!({ id: this.props.match!.params.key })!
        //         .then((data) => {
        //             // if (data!.result![0]['service_product_type']) {
        //             //     data!.result![0]['service_product_type'] = [data!.result![0]['service_product_type'], data!.result![0]['service_product_type_small']];
        //             // }
        //             this.setState({
        //                 base_data: data!.result![0],
        //                 all_item_list: data!.result![0]['item'][0] ? data!.result![0]['item'][0] : {},
        //                 all_service_product_list: data!.result![0]['service_product'],
        //                 is_service_item: data!.result![0]['is_service_item'] === 'true' || data!.result![0]['is_service_item'] === undefined,
        //             });
        //             if (data!.result![0]['service_item_id']) {
        //                 this.itemOnChange(data!.result![0]['service_item_id'], { key: "0" });
        //             }
        //         });
        // }
    }
    itemOnChange = (value: any, option: any) => {
        let list = this.state.all_item_list!;
        this.setState({
            select_id: value,
        });
        AppServiceUtility.services_project_service.get_services_project_list!({ id: value })!
            .then((data: any) => {
                if (list && data) {
                    list = data.result[0];
                    this.setState({
                        all_item_list: list
                    });
                }
            });
    }
    /** 返回按钮 */
    retBtn = () => {
        this.props.history!.goBack();
    }
    /** 添加项目按钮 */
    addItem = () => {
        let list = this.state.all_item_list!;
        if (!list) {
            list = {};
        }
        this.setState({
            all_item_list: list
        });
    }
    /** 删除项目按钮 */
    delItem = () => {
        this.setState({
            all_item_list: null
        });
    }
    addService = () => {
        let list: any[] | undefined = this.state.all_service_product_list;
        if (!list) {
            list = [];
        }
        list.push('');
        this.setState({
            all_service_product_list: list
        });
    }
    /** 服务项目与服务产品卡片之间的切换 */
    toggleCard = (value: any) => {
        if (value === 'true') {
            this.setState({
                is_service_item: true,
            });
        } else {
            this.setState({
                is_service_item: false,
            });
        }
    }
    typeChange = (value: any) => {
        if (value[0] === "餐饮服务") {
            this.setState({
                catering_state: true
            });
        } else {
            this.setState({
                catering_state: false
            });
        }
    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 8 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        // 如果服务项目不为空则生成
        let item_list;
        if (this.state.all_item_list) {
            const value = this.state.all_item_list;
            let keyId: string = this.state.base_data.service_item_id;
            if (keyId && keyId === this.state.select_id) {
                // 在编辑页面，如果选择的ID跟源ID一样，就赋值
                value.service_option = this.state.base_data.service_option;
                value.valuation_formula = this.state.base_data.valuation_formula;
            }
            item_list = (
                <Card>
                    <Row type="flex" justify="center">
                        <Col span={24}>
                            <Form.Item label='服务项目'>
                                {getFieldDecorator('service_item___' + 0, {
                                    initialValue: value.id,
                                    rules: [{
                                        required: true,
                                        message: '请选择服务项目'
                                    }]
                                })(
                                    <Select showSearch={true} onChange={this.itemOnChange} >
                                        {this.state.item_list!.map((key, index) => {
                                            return <Select.Option key={index} value={key['id']}>{key['name']}</Select.Option>;
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                            {
                                value.valuation_formula ? (
                                    <Form.Item label='计价方式'>
                                        {getFieldDecorator('valuation_formula___' + 0, {
                                            initialValue: value.valuation_formula ? value.valuation_formula : "",
                                            rules: [{
                                                required: false,
                                                message: '请选择计价方式'
                                            }],
                                            option: {
                                                disabled: true,
                                                placeholder: '请选择计价方式'
                                            }
                                        })(
                                            <Select>
                                                {typeof (value.valuation_formula) === 'string' ? <Select.Option key={0} value={value.valuation_formula}>{value.valuation_formula}</Select.Option> : value.valuation_formula.map((value: any, index: any) => {
                                                    return <Select.Option key={index} value={value}>{value.name}</Select.Option>;
                                                })}
                                            </Select>
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.proportion ? (
                                    <Form.Item label='分成比例'>
                                        {getFieldDecorator('proportion___' + 0, {
                                            initialValue: value.proportion ? value.proportion : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true,
                                            }
                                        })(
                                            <Multiple
                                                defaultFormat="{title}={contents}"
                                                btn_display={true}
                                                edit_form_items_props={[
                                                    {
                                                        type: 'input',
                                                        decorator_id: "title",
                                                        placeholder: "请输入经济主体",
                                                        required: true,
                                                        span: 7,
                                                        disabled: true
                                                    },
                                                    {
                                                        type: 'input',
                                                        decorator_id: "contents",
                                                        required: true,
                                                        placeholder: "请输入分成比例",
                                                        span: 15
                                                    }
                                                ]}
                                            />
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.type_name ? (
                                    <Form.Item label='服务类型'>
                                        {getFieldDecorator('service_type___' + 0, {
                                            initialValue: value.item_type ? value.item_type : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true
                                            }
                                        })(
                                            <Select>
                                                <Select.Option key={value.item_type} >{(typeof (value.type_name) === 'object' && value.type_name.length === 0) || value.type_name === '' ? '未定义服务类型' : value.type_name}</Select.Option>
                                            </Select>
                                            // 未定义服务类型是因为服务类型数据是不对的，指向不到类型
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.is_allowance ? (
                                    <Form.Item label='是否补贴'>
                                        {getFieldDecorator('is_allowance___' + 0, {
                                            initialValue: value.is_allowance ? value.is_allowance : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true
                                            }
                                        })(
                                            <Radio.Group>
                                                <Radio value={"1"}>是</Radio>
                                                <Radio value={"0"}>否</Radio>
                                            </Radio.Group>
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {(() => {
                                if (!value.service_option) {
                                    return null;
                                }
                                return value.service_option.map((service_option: any, indexs: any) => {
                                    if (service_option.option_type === 'service_after') {
                                        return null;
                                    }
                                    if (service_option.option_value_type === '1') {
                                        return (
                                            <Form.Item label={service_option.name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Radio.Group>
                                                        {
                                                            service_option.option_content!.map((item: any, content: any) => {
                                                                return <Radio key={content} value={item.option_value}>{item.option_content}</Radio>;
                                                            })
                                                        }
                                                    </Radio.Group>
                                                )}
                                            </Form.Item>
                                        );
                                    } else if (service_option.option_value_type === '2') {
                                        return (
                                            <Form.Item label={service_option.name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Checkbox.Group>
                                                        {
                                                            service_option.option_content!.map((item: any, idx: any) => {
                                                                return <Checkbox key={idx} value={item.option_value}>{item.option_content}</Checkbox>;
                                                            })
                                                        }
                                                    </Checkbox.Group>
                                                )}
                                            </Form.Item>
                                        );
                                    } else if (service_option.option_value_type === '3') {
                                        return (
                                            <Form.Item label={service_option.name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>
                                        );
                                    } else {
                                        return null;
                                    }
                                });
                            })()
                            }
                        </Col>
                    </Row>
                </Card>
            );

        }

        // 如果服务产品不为空则生成
        let product_list;
        if (this.state.all_service_product_list && this.state.all_service_product_list!.length > 0) {
            product_list = this.state.all_service_product_list!.map((value, index) => {
                return (
                    <Card key={index}>
                        <Row type="flex" justify="center">
                            <Col span={24}>
                                <Form.Item label='服务产品'>
                                    {getFieldDecorator('service_product___' + index, {
                                        initialValue: value.product_id,
                                        rules: [{
                                            required: true,
                                            message: '请选择服务产品'
                                        }]
                                    })(
                                        <Select showSearch={true} key={index}>
                                            {this.state.service_product_list!.map((key) => {
                                                return <Select.Option key={index} value={key['id']}>{key['name']}</Select.Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='服务次数'>
                                    {getFieldDecorator('service_times___' + index, {
                                        initialValue: value.remaining_times || '1',
                                        rules: [{
                                            required: true,
                                            message: '请选择服务次数'
                                        }]
                                    })(
                                        <Input autoComplete="off" />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                );
            });
        }
        const { Option } = Select;
        return (
            <div className='add-product'>
                <Form layout='vertical' onSubmit={this.handleSubmit} {...formItemLayout} labelAlign='right'>
                    <MainCard title='服务产品'>
                        <Form.Item label='服务产品名称'>
                            {getFieldDecorator('name', {
                                initialValue: this.state.base_data!['name'] ? this.state.base_data!['name'] : '',
                                rules: [{
                                    required: true,
                                    message: '请输入服务产品名称'
                                }],
                            })(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item label='简介'>
                            {getFieldDecorator('introduce', {
                                initialValue: this.state.base_data!['introduce'] ? this.state.base_data!['introduce'] : '',
                            })(
                                <TextArea />
                            )}
                        </Form.Item>
                        <Form.Item label='备注'>
                            {getFieldDecorator('remarks', {
                                initialValue: this.state.base_data!['remarks'] ? this.state.base_data!['remarks'] : '',
                            })(
                                <TextArea />
                            )}
                        </Form.Item>
                        <Form.Item label='是否启用'>
                            {getFieldDecorator('state', {
                                initialValue: this.state.base_data!['state'] ? this.state.base_data!['state'] : '',
                                rules: [{
                                    required: true,
                                    message: '请选择'
                                }],
                            })(
                                <Radio.Group>
                                    <Radio value={1}>启用</Radio>
                                    <Radio value={0}>停用</Radio>
                                </Radio.Group>
                            )}
                        </Form.Item>
                        {this.state.is_service_item ? <Form.Item label='是否需要分派'>
                            {getFieldDecorator('is_assignable', {
                                initialValue: this.state.base_data!['is_assignable'] ? this.state.base_data!['is_assignable'] : '',
                                rules: [{
                                    required: true,
                                    message: '请选择'
                                }],
                            })(
                                <Radio.Group>
                                    <Radio value="1">是</Radio>
                                    <Radio value="0">否 </Radio>
                                </Radio.Group>
                            )}
                        </Form.Item> : null}
                        {/* <Form.Item label='是否外部产品'>
                            {getFieldDecorator('is_external', {
                                initialValue: this.state.base_data!['is_external'] ? this.state.base_data!['is_external'] : '',
                                rules: [{
                                    required: true,
                                    message: '请选择'
                                }],
                            })(
                                <Radio.Group>
                                    <Radio value="1">是</Radio>
                                    <Radio value="0">否</Radio>
                                </Radio.Group>
                            )}
                        </Form.Item> */}
                        <Form.Item label='是否推荐产品'>
                            {getFieldDecorator('is_recommend', {
                                initialValue: this.state.base_data!['is_recommend'] ? this.state.base_data!['is_recommend'] : '',
                                rules: [{
                                    required: true,
                                    message: '请选择'
                                }],
                            })(
                                <Radio.Group>
                                    <Radio value={'是'}>是</Radio>
                                    <Radio value={'否'}>否</Radio>
                                </Radio.Group>
                            )}
                        </Form.Item>
                        <Form.Item label='所属组织机构'>
                            {getFieldDecorator('organization_id', {
                                initialValue: this.state.base_data!['organization_id'] ? this.state.base_data!['organization_id'] : '',
                                rules: [{
                                    required: false,
                                    message: '请选择所属组织机构'
                                }],
                            })(
                                <Select showSearch={true}>
                                    {
                                        this.state.organizational ? this.state.organizational!.map((option) => {
                                            return (<Select.Option key={option.id}>{option.name}</Select.Option>);
                                        }) : null
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        {/* <Form.Item label='产品类型'>
                                    {getFieldDecorator('service_product_type', {
                                        initialValue: this.state.base_data!['service_product_type'] ? this.state.base_data!['service_product_type'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择产品类型'
                                        }],
                                    })(
                                        <Cascader options={this.state.service_Type_list} onChange={(value: any) => this.typeChange(value)} placeholder="请选择产品类型" />
                                        // <Select showSearch={true} onChange={(value: any) => this.typeChange(value)}>
                                        //     <Select.Option key={'助餐服务'}>助餐服务</Select.Option>
                                        //     <Select.Option key={'助洁服务'}>助洁服务</Select.Option>
                                        //     <Select.Option key={'居家服务'}>居家服务</Select.Option>
                                        //     <Select.Option key={'幸福院'}>幸福院</Select.Option>
                                        // </Select>
                                    )}
                                </Form.Item> */}
                        {this.state.is_service_item ? <Form.Item label='服务次数'>
                            {getFieldDecorator('remaining_times', {
                                initialValue: this.state.base_data!['remaining_times'] ? this.state.base_data!['remaining_times'] : '',
                                rules: [{
                                    required: false,
                                    message: '请输入服务次数'
                                }],
                            })(
                                <Input />
                            )}
                        </Form.Item> : null}
                        {this.state.catering_state === true ?
                            <Col>
                                <Form.Item label='所属星期'>
                                    {getFieldDecorator('catering_week', {
                                        initialValue: this.state.base_data!['catering_week'] ? this.state.base_data!['catering_week'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择所属星期'
                                        }],
                                    })(
                                        <Select showSearch={true}>
                                            <Select.Option key={'星期一'}>星期一</Select.Option>
                                            <Select.Option key={'星期二'}>星期二</Select.Option>
                                            <Select.Option key={'星期三'}>星期三</Select.Option>
                                            <Select.Option key={'星期四'}>星期四</Select.Option>
                                            <Select.Option key={'星期五'}>星期五</Select.Option>
                                            <Select.Option key={'星期六'}>星期六</Select.Option>
                                            <Select.Option key={'星期日'}>星期日</Select.Option>
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='午餐/晚餐'>
                                    {getFieldDecorator('catering_time', {
                                        initialValue: this.state.base_data!['catering_time'] ? this.state.base_data!['catering_time'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择午餐/晚餐'
                                        }],
                                    })(
                                        <Radio.Group>
                                            <Radio value="午餐">午餐</Radio>
                                            <Radio value="晚餐">晚餐</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            </Col>
                            :
                            ''}
                        <Form.Item label='服务产品图片（大小小于2M，格式支持jpg/jpeg/png）'>
                            {getFieldDecorator('picture_collection', {
                                initialValue: this.state.base_data!['picture_collection'] ? this.state.base_data!['picture_collection'] : '',
                            })(
                                <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                            )}
                        </Form.Item>
                        <Form.Item label='附加合同条款'>
                            {getFieldDecorator('additonal_contract_terms', {
                                initialValue: this.state.base_data!['additonal_contract_terms'] ? this.state.base_data!['additonal_contract_terms'] : '',
                                rules: [{
                                    required: true,
                                    message: '请输入附加合同条款'
                                }],
                            })(
                                <TextArea />
                            )}
                        </Form.Item>
                        <Form.Item label='服务产品介绍'>
                            {getFieldDecorator('service_product_introduce', {
                                initialValue: this.state.base_data!['service_product_introduce'] ? this.state.base_data!['service_product_introduce'] : '',
                                rules: [{
                                    required: false,
                                    message: '请输入备注'
                                }],
                            })(
                                <TextArea />
                            )}
                        </Form.Item>
                        <Form.Item label='是否为服务项目'>
                            {getFieldDecorator('is_service_item', {
                                initialValue: this.state.base_data!['is_service_item'] ? this.state.base_data!['is_service_item'] : '',
                                rules: [{
                                    required: true,
                                    message: ''
                                }],
                            })(
                                <Select showSearch={true} onChange={this.toggleCard}>
                                    <Option value="true">是</Option>
                                    <Option value="false">否</Option>
                                </Select>
                            )}
                        </Form.Item>
                    </MainCard>
                    {this.state.is_service_item ?
                        <MainCard title='服务项目'>
                            {item_list}
                            <Button type='primary' onClick={this.addItem} disabled={this.state.all_item_list}>新增服务项目</Button>
                            <Button type='primary' onClick={this.delItem} disabled={this.state.all_item_list === null}>删除</Button>
                        </MainCard> :
                        <MainCard title='服务产品列表'>
                            {product_list}
                            <Button type='primary' onClick={this.addService}>新增服务产品</Button>
                        </MainCard>}
                    <Row type="flex" justify="center">
                        <Button htmlType='submit' type='primary'>保存</Button>
                        <Button type='ghost' name='返回' htmlType='button' onClick={this.retBtn}>返回</Button>
                    </Row>
                </Form>
            </div>
        );
    }
}

/**
 * 控件：名称控制器
 * 描述
 */
@addon('AddProductView', '名称', '描述')
@reactControl(Form.create<any>()(AddProductView), true)
export class AddProductViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}