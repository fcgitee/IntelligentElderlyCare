import { Carousel, Col, Rate, Row, Spin } from "antd";
// import Grid from "antd-mobile/lib/grid";
import { Button, Card, Grid, ListView, SearchBar, WhiteSpace, WingBlank } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
import './index.less';
// import { request } from "src/business/util_tool";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
// const Item = List.Item;
/**
 * 组件：居家养老首页状态
 */
export interface HomeCareState extends ReactViewState {
    icon_data?: any[];
    /*用户id */
    id?: any;
    /*账号名 */
    name?: any;
    ServiceProductList?: any;
    page?: any;
    dataSource?: any;
    xm_btn_type?: any;
    sj_btn_type?: any;
    pitrue_list?: any;
    animating?: any;
    show?: any;
    insert?: any;
}
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：居家养老首页
 * 描述
 */
export class HomeCare extends ReactView<HomeCareControl, HomeCareState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            icon_data: [
            ],
            ServiceProductList: [],
            page: 1,
            dataSource: ds,
            xm_btn_type: 'primary',
            sj_btn_type: 'ghost',
            pitrue_list: [],
            animating: false,
            show: false,
            insert: ''
        };
    }
    grid_click = (_el: any) => {
        if (_el.text === '更多') {
            this.props.history!.push(ROUTE_PATH.serviceType);
        } else if (_el.text === '服务套餐') {
            this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '服务套餐');
        } else {
            this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + _el.id);
        }
    }
    componentDidMount = () => {
        this.setState({
            animating: true
        });
        AppServiceUtility.service_detail_service.get_service_product_list!({ "sort": "销量", "num": -1 }, 1, 10)!
            .then((datas: any) => {
                // console.log(datas);
                let i = 0;
                let pitrue_list: any = [];
                datas.result.map((item: any) => {
                    if (i <= 4) {
                        pitrue_list.push({ picture: item.picture_collection[0], id: item.id });
                        i++;
                    } else {
                        return;
                    }
                });
                // console.log('图片？？？？？', pitrue_list);
                this.setState({
                    ServiceProductList: datas.result,
                    pitrue_list,
                    animating: false,
                    show: true
                });
            });
        // 服务类型
        AppServiceUtility.service_operation_service.get_service_type_list!({ 'is_show_app': true, 'is_top': true }, 1, 6)!
            .then((data: any) => {
                // console.log('服务类型？？？？？', data);
                let icon_data: any = [{ text: '服务套餐', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> }];
                if (data.result.length > 0) {
                    data.result.map((item: any) => {
                        let icon = { text: item.name, icon: <img style={{ width: '50px', height: '50px' }} src={item.logo_image} alt="" />, id: item.id };
                        icon_data.push(icon);
                    });
                }
                icon_data.push({ text: '更多', icon: <FontIcon type="iconziyuan11" style={{ fontSize: '40px' }} /> });
                this.setState({
                    icon_data
                });
            });
    }

    /** 下拉事件 */
    onEndReached = () => {
        // console.log(11111);
        let new_pape = this.state.page + 1;
        let ServiceProductList = this.state.ServiceProductList;
        AppServiceUtility.service_detail_service.get_service_product_list!({ "sort": "销量", "num": -1 }, new_pape, 10)!
            .then((data: any) => {
                // console.log(data);
                let list = [...ServiceProductList, ...data.result];
                this.setState({
                    ServiceProductList: list,
                    page: new_pape
                });
            });
    }

    xmClick = () => {
        this.setState({
            xm_btn_type: 'primary',
            sj_btn_type: 'ghost'
        });
    }

    sjClick = () => {
        this.setState({
            xm_btn_type: 'ghost',
            sj_btn_type: 'primary'
        });
        this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '服务商');
    }
    goProductDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
    }

    insert = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }

    search = () => {
        this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '服务项目' + '&' + '' + '&' + this.state.insert);
    }
    render() {
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <>
                    <WhiteSpace style={{ background: "rgb(245, 245, 245)" }} />
                    <Card.Body style={{ backgroundColor: 'white', marginRight: 3, marginLeft: 3, boxShadow: '0px 0px 12px 0px rgba(170,170,170,0.5)' }} key={owData} onClick={() => { this.goProductDetails(owData.id); }}>
                        <WingBlank>
                            <WhiteSpace />
                            <Row type={'flex'} justify="center">
                                <Col span={24}>
                                    <Row type={"flex"} justify={"space-between"}>
                                        <strong>{owData.name}</strong>
                                    </Row>
                                </Col>
                                <Col className='list-col' span={9} style={{ marginRight: '5pt' }}>
                                    <img src={owData.picture_collection} alt="" style={{ height: '72pt', width: '100%' }} />
                                </Col>
                                <Col span={14} className='list-col'>
                                    <Row style={{ fontSize: '14px', color: 'rgba(51,51,51,1)' }}>
                                        <Rate className='product-rate' value={owData.comment_avg} count={5} />
                                        <span >&nbsp;&nbsp;{owData.comment_avg}分</span>
                                    </Row>
                                    <Row style={{ fontSize: '14px' }}>
                                        <span
                                            style={{
                                                wordBreak: 'break-all',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: ' -webkit-box',
                                                WebkitLineClamp: 2,
                                                WebkitBoxOrient: 'vertical',
                                            }}
                                        >
                                            {owData.introduce}
                                        </span>
                                    </Row>
                                    <Row style={{ fontSize: '14px' }}>
                                        <span
                                            style={{
                                                marginTop: 5,
                                                wordBreak: 'break-all',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: ' -webkit-box',
                                                WebkitLineClamp: 1,
                                                WebkitBoxOrient: 'vertical',
                                            }}
                                        >
                                            {owData.org_info.length > 0 ? owData.org_info[0].name : ''}
                                        </span>
                                    </Row>
                                </Col>
                            </Row>
                            {/* <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '30%' }}>
                            </div>
                            <div style={{ float: 'left', width: '59%' }}>
                                <div style={{ overflow: 'hidden', width: '100%', marginBottom: '10px' }}><div style={{ float: 'left', width: '50%' }}><strong>{owData.name}</strong></div><div style={{ float: 'right' }}><Rate className='product-rate' value={owData.comment_avg} count={5} /></div></div>
                                <div className='content'>{owData.introduce}</div>
                                <div style={{ fontSize: '13px', marginBottom: '15px' }}>{}</div>
                                <div style={{ overflow: 'hidden', width: '100%' }}>
                                    <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{'￥' + (owData.total_price ? owData.total_price : 0)}</div>
                                    <div style={{ float: 'right' }}>{owData.pay_count + '人付款'}</div>
                                </div>
                            </div> */}
                            <WhiteSpace />
                        </WingBlank>
                    </Card.Body>
                </>
            );
        };
        return (
            <div className='home-care'>
                <SearchBar style={{ background: 'white' }} placeholder="请输入服务供应商、服务价格或服务名称" onChange={this.insert} showCancelButton={false} onSubmit={this.search} />
                <WhiteSpace />
                <WingBlank>
                    <div
                        style={{
                            borderRadius: 20,
                            overflow: 'hidden',
                            boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)',
                        }}
                    >
                        <Grid data={this.state.icon_data} activeStyle={true} hasLine={false} columnNum={4} onClick={_el => this.grid_click(_el)} />
                    </div>
                    <WhiteSpace />
                    <Carousel
                        style={{
                            borderRadius: 20,
                            overflow: 'hidden',
                            boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)'
                        }}
                        autoplay={true}
                        infinite={true}
                        autoplaySpeed={2000}
                    >
                        {this.state.pitrue_list.length > 0 ? this.state.pitrue_list!.map((item: any) => (
                            <a
                                className='carousel-a'
                                href="javascript:;"
                                key={item}
                            >
                                <img
                                    className='carousel-img'
                                    src={item.pitrue_list}
                                    alt=""
                                    onClick={() => { this.goProductDetails(item.id); }}
                                    onLoad={() => {
                                        window.dispatchEvent(new Event('resize'));
                                    }}
                                />
                            </a>
                        ))
                            :
                            (this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                                <div>
                                    <div style={{ textAlign: 'center', margin: '20px' }}>
                                        <Spin size="large" />
                                    </div>
                                </div>
                                :
                                ''
                            )
                        }
                    </Carousel>
                    <WhiteSpace />
                    <div className="header">
                        <span className="text">热门服务</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    {this.state.ServiceProductList && this.state.ServiceProductList.length > 0 ?
                        <ListView
                            ref={(el: any) => this['lv'] = el}
                            dataSource={this.state.dataSource.cloneWithRows(this.state.ServiceProductList)}
                            renderRow={renderRow}
                            initialListSize={10}
                            pageSize={10}
                            renderBodyComponent={() => <MyBody />}
                            style={{ height: document.documentElement.clientHeight }}
                            onEndReached={this.onEndReached}
                        />
                        :
                        (this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                            <div style={{ textAlign: 'center', margin: '20px' }}>
                                <Spin size="large" />
                            </div>
                            :
                            '')
                    }
                    {this.state.ServiceProductList.length === 0 && this.state.show === true ?
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                        </Row>
                        :
                        ''
                    }
                    <div style={{ position: 'fixed', bottom: '0px', overflow: 'hidden', width: '100%', left: '0px', zIndex: 99 }}>
                        <div style={{ width: '50%', float: 'left', backgroundColor: '#ffffff', borderRadius: 0 }}><Button type={this.state.xm_btn_type} onClick={this.xmClick}>服务项目</Button></div>
                        <div style={{ width: '50%', float: 'left', backgroundColor: '#ffffff', borderRadius: 0 }}><Button type={this.state.sj_btn_type} onClick={this.sjClick}>服务商家</Button></div>
                    </div>
                </WingBlank>
            </div>
        );
    }
}

/**
 * 控件：居家养老首页控制器
 * 描述
 */
@addon('HomeCare', '居家养老首页', '描述')
@reactControl(HomeCare, true)
export class HomeCareControl extends ReactViewControl {
    constructor() {
        super();
    }
}