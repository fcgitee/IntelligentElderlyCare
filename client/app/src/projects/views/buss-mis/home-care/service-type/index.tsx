import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, Grid } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
const Item = List.Item;
/**
 * 组件：服务类型状态
 */
export interface ServiceTypeViewState extends ReactViewState {
    icon_data?: any[];
    ts_icon_data?: any[];
}

/**
 * 组件：服务类型
 * 描述
 */
export class ServiceTypeView extends ReactView<ServiceTypeViewControl, ServiceTypeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            icon_data: [
                // { text: '服务套餐', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '助急服务', icon: <FontIcon type="iconziyuan7" style={{ fontSize: '40px' }} /> },
                // { text: '助洁服务', icon: <FontIcon type="iconziyuan31" style={{ fontSize: '40px' }} /> },
                // { text: '助聊服务', icon: <FontIcon type="iconziyuan4" style={{ fontSize: '40px' }} /> },
                // { text: '助餐服务', icon: <FontIcon type="iconziyuan26" style={{ fontSize: '40px' }} /> },
                // { text: '助医服务', icon: <FontIcon type="iconziyuan9" style={{ fontSize: '40px' }} /> },
                // { text: '助浴服务', icon: <img style={{width: '50px', height: '50px'}} src="http://i1.sinaimg.cn/ent/d/2008-06-04/U105P28T3D2048907F326DT20080604225106.jpg" alt=""/> },
                // { text: '助行服务', icon: <FontIcon type="iconziyuan13" style={{ fontSize: '40px' }} /> },
                // { text: '养生保健', icon: <FontIcon type="iconziyuan22" style={{ fontSize: '40px' }} /> },
                // { text: '陪护服务', icon: <FontIcon type="iconziyuan10" style={{ fontSize: '40px' }} /> },
                // { text: '360服务', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
                // { text: '陪护不打烊', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
                // { text: '服务人员申请', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
                // { text: '服务商申请', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
                // { text: '更多', icon: <FontIcon type="iconziyuan11" style={{ fontSize: '40px' }} /> }
            ],
            ts_icon_data: [
                { text: '政策法规', icon: <FontIcon type="iconziyuan8" style={{ fontSize: '40px' }} /> },
                { text: '长者教育', icon: <FontIcon type="iconziyuan13" style={{ fontSize: '40px' }} /> },
                { text: '健康美食', icon: <FontIcon type="iconziyuan22" style={{ fontSize: '40px' }} /> },
                { text: '预防检测', icon: <FontIcon type="iconziyuan10" style={{ fontSize: '40px' }} /> },
                { text: '休闲旅游', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
                { text: '孝道文化', icon: <FontIcon type="iconziyuan1" style={{ fontSize: '40px' }} /> },
            ]
        };
    }
    grid_click = (_el: any) => {
        // console.log(_el);
        this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + _el.id);
    }
    grid_click_ts = (_el: any) => {
        // console.log(_el);
        this.props.history!.push(ROUTE_PATH.CharacteristicService + '/' + _el.text);
    }
    componentDidMount = () => {
        AppServiceUtility.service_operation_service.get_service_type_list!({ 'is_show_app': true, 'is_top': true }, undefined, undefined)!
            .then((data: any) => {
                // console.log('服务类型？？？？？', data);
                let icon_data: any = [];
                if (data.result.length > 0) {
                    data.result.map((item: any) => {
                        if (item.name !== '适老化项目') {
                            icon_data.push({ text: item.name, icon: <img style={{ width: '50px', height: '50px' }} src={item.logo_image} alt="" />, id: item.id });
                        }
                    });
                }
                this.setState({
                    icon_data
                });
            });
    }

    goProductList = () => {
        this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '服务套餐');
    }
    render() {
        return (
            <div className='service-type'>
                <List>
                    <Item style={{ lineHeight: '50px', paddingLeft: '25px' }} arrow="horizontal" onClick={this.goProductList}>服务套餐</Item>
                    <Item style={{ lineHeight: '50px', paddingLeft: '25px' }} >服务类型</Item>
                </List>
                <Grid data={this.state.icon_data} activeStyle={true} hasLine={false} columnNum={3} onClick={_el => this.grid_click(_el)} />
                <List>
                    <Item style={{ lineHeight: '50px', paddingLeft: '25px' }} >特色服务（6）</Item>
                </List>
                <Grid data={this.state.ts_icon_data} activeStyle={true} hasLine={false} columnNum={3} onClick={_el => this.grid_click_ts(_el)} />
            </div>
        );
    }
}

/**
 * 控件：服务类型控制器
 * 描述
 */
@addon('ServiceTypeView', '服务类型', '描述')
@reactControl(ServiceTypeView, true)
export class ServiceTypeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}