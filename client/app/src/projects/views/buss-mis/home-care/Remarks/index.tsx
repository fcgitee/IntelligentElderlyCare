import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { TextareaItem, Button, Tag, WhiteSpace } from 'antd-mobile';
import { Form } from "antd";
import './index.less';
/**
 * 组件：备注首页状态
 */
export interface RemarksViewState extends ReactViewState {
}

/**
 * 组件：备注首页
 * 描述
 */
export class RemarksView extends ReactView<RemarksViewControl, RemarksViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onChange = () => {
        // console.log(11);
    }
    goBack = () => {
        history.back();
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div className="main-content">
                <TextareaItem
                    {...getFieldProps('remarks')}
                    placeholder='请输入口味、偏好等要求...'
                    rows={5}
                    count={50}
                />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <Tag onChange={this.onChange} className="tag">不要辣</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <Tag onChange={this.onChange} className="tag">不吃葱</Tag>
                <div className="submit-btn"><Button type="primary" onClick={this.goBack}>完成</Button></div>
            </div>
        );
    }
}

/**
 * 控件：备注首页控制器
 * 描述
 */
@addon('RemarksView', '备注首页', '描述')
@reactControl(Form.create<any>()(RemarksView), true)
export class RemarksViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}