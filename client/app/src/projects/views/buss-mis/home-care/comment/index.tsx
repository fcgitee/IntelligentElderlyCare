import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Form } from "antd";
import { TextareaItem, Button } from 'antd-mobile';
/**
 * 组件：评论首页状态
 */
export interface CommentViewState extends ReactViewState {
}

/**
 * 组件：评论首页
 * 描述
 */
export class CommentView extends ReactView<CommentViewControl, CommentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onChange = () => {
        // console.log(11);
    }
    goBack = () => {
        history.back();
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div>
                {/* <InputItem
            {...getFieldProps('autofocus')}
            clear
            placeholder="auto focus"
            ref={el => this.autoFocusInst = el}
            >标题</InputItem> */}
                <TextareaItem
                    {...getFieldProps('remarks')}
                    placeholder='请输入你对商家的评价...'
                    rows={5}
                    count={50}
                />
                <Button type="primary" onClick={this.goBack}>确认</Button>
            </div>
        );
    }
}

/**
 * 控件：评论首页控制器
 * 描述
 */
@addon('CommentView', '评论首页', '描述')
@reactControl(Form.create<any>()(CommentView), true)
export class CommentViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}