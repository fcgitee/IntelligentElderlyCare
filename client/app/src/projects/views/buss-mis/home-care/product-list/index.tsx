import { Col, Icon, Input, Modal, Rate, Row, Spin } from 'antd';
import { Button, Card, ListView, Menu, Tabs, WhiteSpace, WingBlank } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { LOCAL_FUNCTION_LIST } from 'src/business/mainForm/backstageManageMainForm';
import { getWebViewRetObjectType, setMainFormTitle, subscribeWebViewNotify } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { getAppPermissonObject, ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';

/**
 * 组件：服务产品/套餐-列表状态
 */
export interface ServiceProductListViewState extends ReactViewState {
    tabs?: any;
    iconType?: any;
    dataSource?: any;
    ServiceProductList?: any;
    page?: any;
    erji_data?: any;
    show?: any;
    erji_show?: any;
    btn_show?: any;
    modal_show?: any;
    insert?: any;
    item_list?: any;
    num?: any;
    sort?: any;
    servict_product_type?: any;
    tab_index?: any;
    organization_id?: any;
    location?: any;
    animating?: any;
}
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：服务产品/套餐-列表
 * 描述
 */
export class ServiceProductListView extends ReactView<ServiceProductListViewControl, ServiceProductListViewState> {
    xliconType: any = "up";
    jgiconType: any = "down";
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            tabs: [
                { title: '综合' },
                { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
            ],
            dataSource: ds,
            ServiceProductList: [],
            page: 1,
            erji_data: [],
            show: false,
            erji_show: true,
            btn_show: false,
            modal_show: false,
            insert: '',
            item_list: [],
            num: -1,
            sort: '',
            servict_product_type: '',
            tab_index: 0,
            organization_id: '',
            location: undefined,
            animating: false
        };
    }

    // 查询服务产品
    searchItem = (condition: any, page: any, old_list: any) => {
        AppServiceUtility.service_detail_service.get_service_product_list!(condition, page, 10)!
            .then((data: any) => {
                // console.log(data);
                let list = [...old_list, ...data.result];
                this.setState({
                    ServiceProductList: list,
                    animating: true
                });
            });
    }

    // 查询服务套餐
    searchPackage = (condition: any, page: any, old_list: any) => {
        AppServiceUtility.service_package_service.get_service_product_package_list!(condition, page, 10)!
            .then((data: any) => {
                // console.log(data);
                let list = [...old_list, ...data.result];
                this.setState({
                    ServiceProductList: list,
                    animating: true
                });
            });
    }

    // 查询服务商
    searchProvider = (condition: any, page: any, old_list: any) => {
        let param = { ...condition, 'personnel_category': '服务商', 'contract_status': '签约' };
        // AppServiceUtility.person_org_manage_service.get_all_organization_list!(param, page, 10)!
        AppServiceUtility.person_org_manage_service.get_organization_all_list!(param, page, 10)!
            .then((data: any) => {
                // console.log(data);
                let list = [...old_list, ...data.result];
                this.setState({
                    ServiceProductList: list,
                    animating: true
                });
            });
    }

    // 查询服务项目和服务套餐
    searchItemPackage = (condition: any, page: any, old_list: any) => {
        AppServiceUtility.service_detail_service.get_service_product_item_package_list!(condition, page, 10)!
            .then((data: any) => {
                // console.log(data);
                let list = [...old_list, ...data.result];
                this.setState({
                    ServiceProductList: list,
                    animating: true
                });
            });
    }
    componentDidMount = () => {
        // 服务套餐跳转过来
        if (this.props.match!.params.key === '服务套餐') {
            setMainFormTitle("服务套餐");
            this.setState({
                erji_show: false
            });
            this.searchPackage({}, 1, []);
        } else if (this.props.match!.params.key === '服务商') {
            // 服务商跳转过来
            setMainFormTitle("服务商");
            this.setState({
                erji_show: false,
                tabs: [
                    { title: '综合' },
                    { title: '距离' },
                    { title: '关注度' },
                ],
            });
            // 查询服务商列表
            this.searchProvider({}, 1, []);
        } else if (this.props.match!.params.key === '每日一膳') {
            setMainFormTitle("每日一膳");
            // 每日一膳
            this.setState({
                erji_show: false,
                tabs: [
                    { title: '好评优先' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>距离</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ],
            });
            // 查询膳食列表
            this.searchItem({ 'is_meals': true }, 1, []);
        } else if (this.props.match!.params.key.split('&')[0] === '服务项目') {
            setMainFormTitle("热门服务");
            // console.log(this.props.match!.params.key.split('&'));
            let id = '';
            let param = {};
            if (this.props.match!.params.key.split('&').length > 1) {
                if (this.props.match!.params.key.split('&').length === 3) {
                    param = { 'name': this.props.match!.params.key.split('&')[2] };
                } else {
                    id = this.props.match!.params.key.split('&')[1];
                    param = { 'organization_id': id };
                    const func_list_str = localStorage.getItem(LOCAL_FUNCTION_LIST);
                    if (getAppPermissonObject(JSON.parse(func_list_str!), '服务项目app发布', '新增')) {
                        this.setState({
                            btn_show: true,
                        });
                    }
                }
            }
            this.setState({
                erji_show: false,
                organization_id: id
            });
            // 查询服务商id下的服务产品/套餐
            this.searchItemPackage(param, 1, []);
        } else {
            let type_id = this.props.match!.params.key;
            AppServiceUtility.service_operation_service.get_service_type_list!({ 'is_show_app': true, 'is_top': true, "id": this.props.match!.params.key }, undefined, undefined)!
                .then((data: any) => {
                    if (data.result.length > 0) {
                        setMainFormTitle(data.result[0].name);
                    }
                });
            // 查询二级服务类型
            let item_list: any = [];
            AppServiceUtility.service_operation_service.get_service_type_list!({ 'is_show_app': true, 'parent_id': type_id }, undefined, undefined)!
                .then((data: any) => {
                    // console.log('二级服务类型？？？？？', data);
                    let erji_data: any = [];
                    if (data.result.length > 0) {
                        data.result.map((item: any) => {
                            let data = { value: item.id, label: item.name };
                            erji_data.push(data);
                            item_list.push(item.id);
                        });
                    }
                    // 查询服务产品
                    this.searchItem({ item_list }, 1, []);
                    // console.log(erji_data);
                    this.setState({
                        erji_data,
                        item_list
                    });
                });
        }
    }
    tabClick = (index: any) => {
        this.setState({
            tab_index: index,
            ServiceProductList: [],
            animating: false,
            page: 1
        });
        // console.log('this.state.tab_inde：', this.state.tab_index);

        if (this.props.match!.params.key === '服务商') {
            let param = {};
            if (this.state.insert !== '') {
                param = { 'name': this.state.insert };
            }
            if (index === 0) {
                this.searchProvider(param, 1, []);
            }
            if (index === 1) {
                if ((window as any).ReactNativeWebView) {
                    const messageCb = (e: any) => {
                        const retObject = JSON.parse(e.data);
                        if (getWebViewRetObjectType(retObject) === 'getPhonePosition') {
                            let location = retObject.data;
                            if (location === undefined) {
                                // Toast.fail('获取位置信息失败');
                            }
                            this.setState({
                                location: location
                            });
                            param = { ...param, "sortByDistance": true, ...location };
                            this.searchProvider(param, 1, []);
                            this.setState({
                                sort: { "sortByDistance": true, ...location }
                            });
                        }
                    };
                    subscribeWebViewNotify(messageCb);

                    this.getPhonePosition();
                } else {
                    this.searchProvider(param, 1, []);
                }
            }
            if (index === 2) {
                param = { ...param, 'sort': '关注度' };
                this.setState({
                    sort: { "sort": "关注度" }
                });
                this.searchProvider(param, 1, []);
            }
        } else if (this.props.match!.params.key === '每日一膳') {
            let param = {};
            param = { 'is_meals': true };
            if (this.state.insert !== '') {
                param = { 'name': this.state.insert };
            }
            if (index === 0) {
                this.searchItem(param, 1, []);
            }
            if (index === 1) {
                let num = -1;
                if (this.xliconType === 'down') {
                    this.xliconType = 'up';
                    num = 1;
                } else {
                    this.xliconType = 'down';
                    num = -1;
                }
                // console.log(this.xliconType);
                let tabs = [
                    { title: '好评优先' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>距离</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num,
                    sort: '销量'
                });
                param = { ...param, 'sort': '销量', 'num': num };
                this.searchItem(param, 1, []);
            }
            // 价格
            if (index === 2) {
                let num = -1;
                if (this.jgiconType === 'down') {
                    this.jgiconType = 'up';
                    num = 1;
                } else {
                    this.jgiconType = 'down';
                    num = -1;
                }
                // console.log(this.jgiconType);
                let tabs = [
                    { title: '好评优先' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>距离</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num,
                    sort: '销量'
                });
                param = { ...param, 'sort': '销量', 'num': num };
                this.searchItem(param, 1, []);
            }
        } else if (this.props.match!.params.key === '服务套餐') {
            let param = {};
            if (this.state.insert !== '') {
                param = { 'name': this.state.insert };
            }
            // console.log(index);
            if (index === 0) {
                // 查询服务套餐
                this.searchPackage(param, 1, []);
            }
            if (index === 1) {
                let num = -1;
                if (this.xliconType === 'down') {
                    this.xliconType = 'up';
                    num = 1;
                } else {
                    this.xliconType = 'down';
                    num = -1;
                }
                // console.log(this.xliconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num,
                    sort: '销量'
                });
                param = { ...param, 'sort': '销量', 'num': num };
                this.searchPackage(param, 1, []);
            }
            // 价格
            if (index === 2) {
                let num = -1;
                if (this.jgiconType === 'down') {
                    this.jgiconType = 'up';
                    num = 1;
                } else {
                    this.jgiconType = 'down';
                    num = -1;
                }
                // console.log(this.jgiconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num,
                    sort: '价格'
                });
                param = { ...param, 'sort': '价格', 'num': num };
                this.searchPackage(param, 1, []);
            }
        } else if (this.props.match!.params.key.split('&')[0] === '服务项目') {
            let param = {};
            if (this.props.match!.params.key.split('&').length > 1) {
                if (this.props.match!.params.key.split('&').length === 3) {
                    param = { 'name': this.props.match!.params.key.split('&')[2] };
                } else {
                    param = { 'organization_id': this.props.match!.params.key.split('&')[1] };
                }
            }
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            if (index === 0) {
                this.searchItemPackage(param, 1, []);
            }
            if (index === 1) {
                let num = 1;
                let prevNum = this.xliconType === 'down' ? 1 : -1;
                if (this.state.tab_index === index) {
                    if (this.xliconType === 'down') {
                        this.xliconType = 'up';
                        num = -1;
                    } else {
                        this.xliconType = 'down';
                        num = 1;
                    }
                }
                // console.log(this.xliconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num: this.state.tab_index !== index ? prevNum : num,
                    sort: '销量'
                });
                param = { ...param, 'sort': '销量', 'num': this.state.tab_index !== index ? prevNum : num };
                this.searchItemPackage(param, 1, []);
            }
            // 价格
            if (index === 2) {
                let num = -1;
                let prevNum = this.jgiconType === 'down' ? 1 : -1;
                if (this.state.tab_index === index) {
                    if (this.jgiconType === 'down') {
                        this.jgiconType = 'up';
                        num = -1;
                    } else {
                        this.jgiconType = 'down';
                        num = 1;
                    }
                }
                // console.log(this.jgiconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num: this.state.tab_index !== index ? prevNum : num,
                    sort: '价格'
                });
                param = { ...param, 'sort': '价格', 'num': this.state.tab_index !== index ? prevNum : num };
                this.searchItemPackage(param, 1, []);
            }
        } else {
            // console.log(index);
            let param = {};
            if (this.state.insert !== '') {
                param = { 'name': this.state.insert };
            }
            if (this.state.servict_product_type !== '') {
                param = { ...param, 'servict_product_type': this.state.servict_product_type };
            }
            if (this.state.servict_product_type === '') {
                param = { ...param, 'item_list': this.state.item_list };
            }
            // console.log(param);
            if (index === 0) {
                this.searchItem(param, 1, []);
            }
            if (index === 1) {
                let num = -1;
                let prevNum = this.xliconType === 'down' ? 1 : -1;
                if (this.state.tab_index === index) {
                    if (this.xliconType === 'down') {
                        this.xliconType = 'up';
                        num = -1;
                    } else {
                        this.xliconType = 'down';
                        num = 1;
                    }
                }
                // console.log(this.xliconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num: this.state.tab_index !== index ? prevNum : num,
                    sort: '销量'
                });
                param = { ...param, 'sort': '销量', 'num': this.state.tab_index !== index ? prevNum : num };
                this.searchItem(param, 1, []);
            }
            // 价格
            if (index === 2) {
                let num = -1;
                let prevNum = this.jgiconType === 'down' ? 1 : -1;

                if (this.state.tab_index === index) {
                    if (this.jgiconType === 'down') {
                        this.jgiconType = 'up';
                        num = -1;
                    } else {
                        this.jgiconType = 'down';
                        num = 1;
                    }
                }
                // console.log(this.jgiconType);
                let tabs = [
                    { title: '综合' },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>销量</span><Icon type={this.xliconType} style={{ verticalAlign: 'sub' }} /></div> },
                    { title: <div style={{ height: '43px', lineHeight: '43px' }}><span>价格</span><Icon type={this.jgiconType} style={{ verticalAlign: 'sub' }} /></div> },
                ];
                // console.log(tabs);
                this.setState({
                    tabs,
                    num: this.state.tab_index !== index ? prevNum : num,
                    sort: '价格'
                });
                param = { ...param, 'sort': '价格', 'num': this.state.tab_index !== index ? prevNum : num };
                this.searchItem(param, 1, []);
            }
        }
    }

    getPhonePosition() {
        // Toast.info('认证成功：' + e.data);
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'getPhonePosition',
                    params: undefined
                })
            );
        }
    }

    /** 下拉事件 */
    onEndReached = () => {
        // console.log(11111);
        let new_pape = this.state.page + 1;
        if (this.props.match!.params.key === '服务商') {
            let param = {};
            if (this.state.sort !== '') {
                param = { ...param, ...this.state.sort };
            }
            if (this.state.insert !== '') {
                param = { ...param, 'name': this.state.insert };
            }
            this.searchProvider(param, new_pape, this.state.ServiceProductList);
            this.setState({
                page: new_pape
            });
        } else if (this.props.match!.params.key.split('&')[0] === '服务项目') {
            let param = {};
            if (this.props.match!.params.key.split('&').length > 1) {
                if (this.props.match!.params.key.split('&').length === 3) {
                    param = { 'name': this.props.match!.params.key.split('&')[2] };
                } else {
                    param = { "organization_id": this.props.match!.params.key.split('&')[1] };
                }
            }
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            if (this.state.sort !== '') {
                param = { ...param, 'sort': this.state.sort, 'num': this.state.num };
            }
            // 查询服务商id下的服务产品/套餐
            this.searchItemPackage(param, new_pape, this.state.ServiceProductList);
            this.setState({
                page: new_pape
            });
        } else if (this.props.match!.params.key === '服务套餐') {
            let param = {};
            if (this.state.sort !== '') {
                param = { ...param, 'sort': this.state.sort, 'num': this.state.num };
            }
            if (this.state.insert !== '') {
                param = { ...param, 'name': this.state.insert };
            }
            this.searchPackage(param, new_pape, this.state.ServiceProductList);
            this.setState({
                page: new_pape
            });
        } else if (this.props.match!.params.key === '每日一膳') {
            // 查询膳食列表
            let param = {};
            param = { 'is_meals': true };
            if (this.state.sort !== '') {
                param = { ...param, 'sort': this.state.sort, 'num': this.state.num };
            }
            if (this.state.insert !== '') {
                param = { ...param, 'name': this.state.insert };
            }
            this.searchItem(param, new_pape, this.state.ServiceProductList);
            this.setState({
                page: new_pape
            });
        } else {
            let param = {};
            if (this.state.sort !== '') {
                param = { ...param, 'sort': this.state.sort, 'num': this.state.num };
            }
            if (this.state.insert !== '') {
                param = { ...param, 'name': this.state.insert };
            }
            if (this.state.servict_product_type !== '') {
                param = { ...param, 'servict_product_type': this.state.servict_product_type };
            }
            if (this.state.servict_product_type === '') {
                param = { ...param, 'item_list': this.state.item_list };
            }
            this.searchItem(param, new_pape, this.state.ServiceProductList);
            this.setState({
                page: new_pape
            });
        }
    }

    selectType = () => {
        this.setState({
            show: !this.state.show
        });
    }

    erjiTypeChange = (value: any) => {
        // console.log(value[0]);
        let param = {};
        this.setState({
            ServiceProductList: [],
            animating: false
        });
        param = { 'servict_product_type': value[0] };
        if (this.state.insert !== '') {
            param = { ...param, 'name': this.state.insert };
        }
        this.searchItem(param, 1, []);
        this.setState({
            servict_product_type: value[0],
            show: false,
            tab_index: 0,
        });
    }

    goProductDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
    }

    goBusinessDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.serverDetail + '/' + id);
    }

    // 查询搜索
    search = () => {
        this.setState({
            modal_show: true
        });
    }

    handleOk = (e: any) => {
        this.setState({
            ServiceProductList: [],
            animating: false,
            page: 1
        });
        // console.log(e);
        if (this.props.match!.params.key === '服务商') {
            this.searchProvider({ 'name': this.state.insert }, 1, []);
            this.setState({
                tab_index: 0,
                modal_show: false,
            });
        } else if (this.props.match!.params.key === '服务套餐') {
            this.setState({
                tab_index: 0,
                modal_show: false,
            });
            // 查询服务套餐
            let param = { 'name': this.state.insert };
            this.searchPackage(param, 1, []);
        } else if (this.props.match!.params.key === '每日一膳') {
            let param = {};
            param = { 'is_meals': true, 'name': this.state.insert };
            this.searchItem(param, 1, []);
            this.setState({
                modal_show: false,
                tab_index: 0,
            });
        } else if (this.props.match!.params.key.split('&')[0] === '服务项目') {
            let param = {};
            if (this.props.match!.params.key.split('&').length > 1) {
                if (this.props.match!.params.key.split('&').length === 3) {
                    param = { 'name': this.props.match!.params.key.split('&')[2] };
                } else {
                    param = { "organization_id": this.props.match!.params.key.split('&')[1] };
                }
            }
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            // 查询服务商id下的服务产品/套餐
            this.searchItemPackage(param, 1, []);
            this.setState({
                modal_show: false,
                tab_index: 0,
            });
        } else {
            let param = {};
            param = { 'name': this.state.insert };
            if (this.state.servict_product_type !== '') {
                param = { ...param, 'servict_product_type': this.state.servict_product_type };
            }
            if (this.state.servict_product_type === '') {
                param = { ...param, 'item_list': this.state.item_list };
            }
            this.searchItem(param, 1, []);
            this.setState({
                modal_show: false,
                tab_index: 0,
            });
        }
    }

    handleCancel = (e: any) => {
        // console.log(111);
        // console.log(e);
        this.setState({
            modal_show: false,
            insert: '',
            servict_product_type: '',
            tab_index: 0,
            sort: '',
            ServiceProductList: [],
            page: 1
        });
        if (this.props.match!.params.key === '服务套餐') {
            this.searchPackage({}, 1, []);
        } else if (this.props.match!.params.key === '服务商') {
            this.searchProvider({}, 1, []);
        } else if (this.props.match!.params.key === '每日一膳') {
            let param = {};
            param = { 'is_meals': true };
            this.searchItem(param, 1, []);
        } else if (this.props.match!.params.key.split('&')[0] === '服务项目') {
            let param = {};
            if (this.props.match!.params.key.split('&').length > 1) {
                if (this.props.match!.params.key.split('&').length === 3) {
                    param = { 'name': this.props.match!.params.key.split('&')[2] };
                } else {
                    param = { "organization_id": this.props.match!.params.key.split('&')[1] };
                }
            }
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            // 查询服务商id下的服务产品/套餐
            this.searchItemPackage(param, 1, []);
        } else {
            this.searchItem({ 'item_list': this.state.item_list }, 1, []);
        }
    }

    insertChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            insert: e.target.value
        });
    }

    render() {
        const { tabs } = this.state;
        let renderRow: any;

        if (this.props.match!.params.key === '服务商') {
            renderRow = (owData: any, sectionID: any, rowID: any) => {
                return (
                    <>
                        <WhiteSpace style={{ background: "rgb(245, 245, 245)" }} />
                        <Card.Body key={owData} style={{ borderRadius: 20, boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)', marginLeft: 3, marginRight: 3 }} onClick={() => { this.goBusinessDetails(owData.id); }}>
                            <WingBlank>
                                <WhiteSpace />
                                <Row type='flex' justify='center'>
                                    <Col span={24}>
                                        <Row type={"flex"} justify={"space-between"}>
                                            <strong>{owData.name} <Icon style={{ color: 'rgba(153,153,153,1)' }} type="right-circle" /></strong>
                                            <span>
                                                <Icon type="heart" theme={"filled"} style={{ marginRight: '5px', color: '#ff1466' }} />{owData.follow_count}
                                            </span>
                                        </Row>
                                    </Col>
                                    <Col className='list-col' span={9} style={{ marginRight: '5pt' }}>
                                        <img className='list-col' style={{ display: 'block', width: '100%' }} src={owData.organization_info.picture_list && owData.organization_info.picture_list.length > 0 ? owData.organization_info.picture_list[0] : require('../../../../../static/img/默认企业.jpg')} />
                                    </Col>
                                    <Col span={14} className='list-col'>
                                        <Row style={{ fontSize: '14px', color: 'rgba(51,51,51,1)' }}>
                                            <Rate character={<Icon style={{ fontSize: "24px" }} type="star" theme="filled" />} allowHalf={true} disabled={true} value={owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 0} />
                                            <span >&nbsp;&nbsp;{owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 4}星</span>
                                        </Row>
                                        <Row style={{ fontSize: '14px' }}>
                                            {owData.organization_info ? owData.organization_info.telephone : ''}
                                        </Row>
                                        <Row style={{ fontSize: '14px' }}>
                                            <span
                                                style={{
                                                    wordBreak: 'break-all',
                                                    textOverflow: 'ellipsis',
                                                    overflow: 'hidden',
                                                    display: ' -webkit-box',
                                                    WebkitLineClamp: 1,
                                                    WebkitBoxOrient: 'vertical',
                                                }}
                                            >
                                                {owData.organization_info ? owData.organization_info.address : ''}
                                            </span>
                                        </Row>
                                        <Row style={{ fontSize: '14px', color: "rgba(153,153,153,1)" }}>
                                            <Col span={12}>
                                                {owData.division_name}&nbsp;|&nbsp;{owData.distance ? '| ' + owData.distance.toFixed(2) + 'km' : ''}
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </WingBlank>
                        </Card.Body>
                    </>
                );
            };
        } else {
            renderRow = (owData: any, sectionID: any, rowID: any) => {
                return (
                    <>
                        <WhiteSpace style={{ background: "rgb(245, 245, 245)" }} />
                        <Card.Body key={owData} style={{ borderRadius: 20, boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)', marginLeft: 3, marginRight: 3 }} onClick={() => { this.goProductDetails(owData.id); }}>
                            <WingBlank>
                                <WhiteSpace />
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', margin: '0 2.5%', width: '35%', height: '100%' }}>
                                        <img src={owData.picture_collection} alt="" style={{ width: '100%', height: '100%' }} />
                                    </div>
                                    <div style={{ float: 'left', width: '58%' }}>
                                        <div style={{ overflow: 'hidden', width: '100%' }}>
                                            <div style={{ float: 'left', width: this.state.show === true ? '100%' : '100%' }}><strong>{owData.name}</strong></div>
                                            <div style={{ float: 'right' }}><Rate className='product-rate' value={owData.comment_avg ? owData.comment_avg : 0} count={5} /></div>
                                        </div>
                                        <br />
                                        <div className='content'>{owData.introduce}</div>
                                        <br />
                                        <div style={{ fontSize: '13px', marginBottom: '15px' }}>{owData.org_info.length > 0 ? owData.org_info[0].name : ''}</div>
                                        <div style={{ overflow: 'hidden', width: '100%' }}>
                                            <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{'￥' + (owData.total_price ? owData.total_price : 0)}</div>
                                            <div style={{ float: 'right', marginRight: 5 }}>{owData.pay_count + '人付款'}</div>
                                        </div>
                                    </div>
                                </div>
                            </WingBlank>
                        </Card.Body>
                    </>
                );
            };
        }

        return (
            <div className={'p-list product-list' + '' + (this.state.erji_show === false ? 'clear-padd' : '')}>
                <div style={{ position: 'fixed', top: '10px', right: '15px', zIndex: 999 }} onClick={this.search}><Icon type="search" /></div>
                {this.state.erji_show === true ?
                    <div className='twoType' onClick={this.selectType}>二级服务</div>
                    :
                    ''
                }
                <Tabs tabs={tabs} page={this.state.tab_index} initialPage={this.state.tab_index} onTabClick={(tab, index) => { this.tabClick(index); }}>
                    <div style={{ height: document.documentElement.clientHeight, width: '100%' }}>
                        {this.state.show === true ?
                            <div style={{ width: '30%', float: 'left' }}>
                                <Menu
                                    className="single-foo-menu"
                                    data={this.state.erji_data}
                                    level={1}
                                    onChange={this.erjiTypeChange}
                                    height={document.documentElement.clientHeight}
                                />
                            </div>
                            :
                            ''
                        }
                        {this.state.ServiceProductList && this.state.ServiceProductList.length > 0 ?
                            <div style={{ width: this.state.show === true ? '70%' : '100%', float: 'left' }}>
                                <WingBlank>
                                    <ListView
                                        ref={(el: any) => this['lv'] = el}
                                        dataSource={this.state.dataSource.cloneWithRows(this.state.ServiceProductList)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: document.documentElement.clientHeight }}
                                        onEndReached={this.onEndReached}
                                    />
                                </WingBlank>
                            </div>
                            :
                            (this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                                ''
                                :
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>)
                        }
                        {this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无数据
                            </Row>
                            :
                            ''
                        }
                    </div>
                    <div style={{ height: document.documentElement.clientHeight }}>
                        {this.state.show === true ?
                            <div style={{ width: '30%', float: 'left' }}>
                                <Menu
                                    className="single-foo-menu"
                                    data={this.state.erji_data}
                                    level={1}
                                    onChange={this.erjiTypeChange}
                                    height={document.documentElement.clientHeight}
                                />
                            </div>
                            :
                            ''
                        }
                        {this.state.ServiceProductList && this.state.ServiceProductList.length > 0 ?
                            <div style={{ width: this.state.show === true ? '70%' : '100%', float: 'left' }}>
                                <WingBlank>
                                    <ListView
                                        ref={(el: any) => this['lv'] = el}
                                        dataSource={this.state.dataSource.cloneWithRows(this.state.ServiceProductList)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: document.documentElement.clientHeight }}
                                        onEndReached={this.onEndReached}
                                    />
                                </WingBlank>
                            </div>
                            :
                            (this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                                ''
                                :
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>)
                        }
                        {this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无数据
                            </Row>
                            :
                            ''
                        }
                    </div>
                    <div style={{ height: document.documentElement.clientHeight }}>
                        {this.state.show === true ?
                            <div style={{ width: '30%', float: 'left' }}>
                                <Menu
                                    className="single-foo-menu"
                                    data={this.state.erji_data}
                                    level={1}
                                    onChange={this.erjiTypeChange}
                                    height={document.documentElement.clientHeight}
                                />
                            </div>
                            :
                            ''
                        }
                        {this.state.ServiceProductList && this.state.ServiceProductList.length > 0 ?
                            <div style={{ width: this.state.show === true ? '70%' : '100%', float: 'left' }}>
                                <WingBlank>
                                    <ListView
                                        ref={(el: any) => this['lv'] = el}
                                        dataSource={this.state.dataSource.cloneWithRows(this.state.ServiceProductList)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: document.documentElement.clientHeight }}
                                        onEndReached={this.onEndReached}
                                    />
                                </WingBlank>
                            </div>
                            :
                            (this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                                ''
                                :
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>)
                        }
                        {this.state.ServiceProductList.length === 0 && this.state.animating === true ?
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无数据
                            </Row>
                            :
                            ''
                        }
                    </div>
                </Tabs>
                {this.props.match!.params.key === '服务商' ?
                    <div style={{ position: 'fixed', bottom: '0px', overflow: 'hidden', width: '100%', left: '0px', zIndex: 99 }}>
                        <div style={{ width: '50%', float: 'left', backgroundColor: '#ffffff' }}><Button type='ghost' onClick={() => { history.back(); }}>服务项目</Button></div>
                        <div style={{ width: '50%', float: 'left', backgroundColor: '#ffffff' }}><Button type='primary' >服务商家</Button></div>
                    </div>
                    :
                    ''
                }
                {this.state.btn_show === true ?
                    <div style={{ position: 'fixed', bottom: '0px', overflow: 'hidden', width: '100%', left: '0px', zIndex: 99 }}>
                        <Button type='warning' onClick={() => { this.props.history!.push(ROUTE_PATH.addProduct + '/' + this.props.match!.params.key.split('&')[1]); }}>+新增</Button>
                    </div>
                    :
                    ''
                }
                <Modal
                    title="搜索"
                    visible={this.state.modal_show}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    cancelText='重置'
                    okText='搜索'
                >
                    <Input placeholder="请输入搜索关键字" value={this.state.insert} onChange={this.insertChange} />
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：服务产品/套餐-列表控制器
 * 描述
 */
@addon('ServiceProductListView', '服务产品/套餐-列表', '描述')
@reactControl(ServiceProductListView, true)
export class ServiceProductListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}