import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Carousel, Card, WhiteSpace, ListView, Button } from "antd-mobile";
import { Row, Icon, Col, Avatar, Divider } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { Activity } from "src/projects/models/activity";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import moment from "moment";
/**
 * 组件：商家详情视图控件状态
 */
export interface CleanBusinessDetailsViewState extends ReactViewState {
    /** 数据 */
    data: Activity;
    /** 图片高度 */
    imgHeight?: number | string;
    /** 评论列表 */
    comment_list?: any[];
    /** 长列表数据 */
    dataSource?: any;
    /** 根据id获取的商家详情信息 */
    businessDetail?: any;
    /** 评论内容 */
    comment_value?: any;
    state?: any;
    clean_data?: any;
    name?: any;
    address?: any;
    phone?: any;
    photo?: any;
    introduction?: any;
}
/**
 * 组件：商家详情视图控件
 */
export class CleanBusinessDetailsView extends ReactView<CleanBusinessDetailsViewControl, CleanBusinessDetailsViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            data: {},
            comment_list: [],
            imgHeight: 176,
            dataSource: ds,
            businessDetail: {},
            state: false,
            clean_data: [],
            comment_value: '',
            name: '',
            address: '',
            phone: '',
            photo: '',
            introduction: ''
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        // console.log(id);
        // 根据id获取服务商详情
        AppServiceUtility.service_operation_service.get_service_provider_byId!({ id }, 1, 1)!
            .then((data: any) => {
                // console.log("服务商详情", data);
                this.setState({
                    businessDetail: data.result[0],
                    name: data.result[0].name,
                    address: data.result[0].organization_info.address,
                    phone: data.result[0].organization_info.telephone,
                    photo: data.result[0].organization_info.picture_list,
                    introduction: data.result[0].organization_info.introduction ? data.result[0].organization_info.introduction : '暂无简介'
                });
            });
        // 根据服务商id查找当前服务商推荐的服务产品
        AppServiceUtility.service_package_service.get_service_product_package_list!({ org_id: id, service_product_type: "助洁服务", is_recommend: '是' }, undefined, undefined)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    clean_data: data.result
                });
            });
    }

    onClickRow = (id: string) => {
        console.info(id);
        this.props.history!.push(ROUTE_PATH.childCommmentList + '/' + id);
    }
    goCleanList = () => {
        let id = this.props.match!.params.key;
        this.props.history!.push(ROUTE_PATH.cleanList + '/' + id);
    }
    commentChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            comment_value: e.target.value
        });
    }
    textAreaChange = (value: any) => {
        this.setState({
            comment_value: value
        });
    }
    comment = () => {
        this.props.history!.push(ROUTE_PATH.comment2);
    }
    // 前往详情，到时候把1换成id
    goFoodDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.foodDetails + '/' + id);
    }
    render() {
        let { comment_list, dataSource, photo, name, address, phone, introduction } = this.state;
        // let id = this.props.match!.params.key;
        // let type = this.props.match!.params.code;
        const comment = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card>
                        <Row type='flex' justify='center' onClick={this.onClickRow.bind(this, owData.id)}>
                            <Col className='list-col' span={3}><WhiteSpace size='sm' /><Avatar size='large' icon="user" /></Col>
                            <Col span={20} className='list-col'>
                                <WhiteSpace size='sm' />
                                <Row>
                                    <Col span={12}><strong>{owData.comment_user}</strong></Col>
                                    <Col span={12}>{moment(owData.comment_date).format("YYYY-MM-DD hh:mm")}</Col>
                                </Row>
                                <Row>
                                    {owData.content}
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
        };

        return (
            <div>
                <Carousel
                    autoplay={true}
                    infinite={true}
                >
                    {/* {businessDetail!.photo ? businessDetail!.photo!.map((val: any) => (
                        <a
                            className='carousel-a'
                            key={val}
                            href="javascript:;"
                            style={{ height: this.state.imgHeight }}
                        >
                            <img
                                className='carousel-img'
                                src={"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3241691427,987726683&fm=15&gp=0.jpg"}
                                alt=""
                                onLoad={() => {
                                    window.dispatchEvent(new Event('resize'));
                                    this.setState({ imgHeight: 'auto' });
                                }}
                            />
                        </a>
                    )) :
                        <a
                            className='carousel-a'
                            key={1}
                            href="javascript:;"
                            style={{ height: this.state.imgHeight }}
                        >
                            <img
                                className='carousel-img'
                                src={"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3241691427,987726683&fm=15&gp=0.jpg"}
                                alt=""
                                onLoad={() => {
                                    window.dispatchEvent(new Event('resize'));
                                    this.setState({ imgHeight: 'auto' });
                                }}
                            />
                        </a>} */}
                    {photo ? <a
                        className='carousel-a'
                        key={1}
                        href="javascript:;"
                        style={{ height: this.state.imgHeight }}
                    >
                        <img
                            className='carousel-img'
                            src={photo}
                            alt=""
                            onLoad={() => {
                                window.dispatchEvent(new Event('resize'));
                                this.setState({ imgHeight: 'auto' });
                            }}
                        />
                    </a>
                        :
                        <a
                            className='carousel-a'
                            key={1}
                            href="javascript:;"
                            style={{ height: this.state.imgHeight }}
                        >
                            <img
                                className='carousel-img'
                                src={'http://e.hiphotos.baidu.com/nuomi/pic/item/cf1b9d16fdfaaf5146e966b5855494eef01f7ab6.jpg'}
                                alt=""
                                onLoad={() => {
                                    window.dispatchEvent(new Event('resize'));
                                    this.setState({ imgHeight: 'auto' });
                                }}
                            />
                        </a>}
                </Carousel>
                <Card>
                    <Card.Body>
                        <Row type='flex' justify='center'>
                            <Col span={4}> <Icon className='item-icon-size' type='shop' /></Col>
                            <Col span={18}> {name}</Col>
                        </Row>
                        <Divider />
                        <Row type='flex' justify='center'>
                            <Col span={4}> <Icon className='item-icon-size' type='environment' /></Col>
                            <Col span={18}> {address}</Col>
                        </Row>
                        {/* <Divider /> */}
                        {/* <Row type='flex' justify='center'>
                            <Col span={4}> <Icon className='item-icon-size' type='clock-circle' /></Col>
                            <Col span={18}>营业时间: 08:00 - 21:00</Col>
                        </Row> */}
                        <Divider />
                        <Row type='flex' justify='center'>
                            <Col span={4}> <Icon className='item-icon-size' type='phone' /></Col>
                            <Col span={18}> {phone}</Col>
                        </Row>
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title="服务推荐"
                    />
                    <Card.Body>
                        {this.state.clean_data.length > 0 ? this.state.clean_data.map((item: any) => {
                            return <Card className='list-conten' onClick={(e) => { this.goFoodDetails(item.id); }} key={item.id}>
                                <Row type='flex' justify='center'>
                                    <Col className='list-col' span={10}><img src={item.picture_collection[0]} style={{ height: '72pt', width: '140px' }} /></Col>
                                    <Col span={14} className='list-col'>
                                        <Row><strong>{item.name}</strong></Row>
                                        <Row>
                                            {/* <span>打扫卫生...</span> */}
                                            <Button type="warning" size="small" style={{ float: "right" }} onClick={(e) => { e.stopPropagation(); this.props.history!.push(ROUTE_PATH.foodList); }}>购买</Button>
                                        </Row>
                                        <Row>￥{item.total_price}</Row>
                                        {/* <Row>
                                            <Col span={12}>￥200</Col>
                                            <Col span={12}>已售:600</Col>
                                        </Row> */}
                                    </Col>
                                </Row>
                            </Card>;
                        })
                            :
                            <div style={{ textAlign: 'center' }}>暂无推荐菜品</div>
                        }
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title="服务简介"
                    />
                    {/* <Card.Body>
                        <div>注：仅限服务范围内购买</div>
                        <div>【服务范围】南海区大沥镇、狮山镇、西樵镇、里水镇、丹灶镇、九江镇、桂城街道</div>
                        <div>【服务人员】服务人员...</div>
                        <div>【服务流程】流程详细...</div>
                        <div>【服务保障、承诺】保障承诺...</div>
                        <div>【服务方式】方式...</div>
                        <div>【服务内容】内容...</div>
                    </Card.Body> */}
                    <Card.Body>
                        <div style={{ textAlign: 'center' }}>{introduction}</div>
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card className='list-conten'>
                    <Card.Header
                        title="评论"
                    />
                    <div className='tabs-content'>
                        {
                            comment_list && comment_list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(comment_list)}
                                    renderRow={comment}
                                    initialListSize={10}
                                    pageSize={10}
                                // renderBodyComponent={() => <MyBody />}
                                // style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    <Button type="primary" style={{ width: '60px' }} onClick={this.comment}>评论</Button>

                                </Row>
                        }
                    </div>
                </Card>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.goCleanList}>进店购买</Button>

            </div>
        );
    }
}

/**
 * 组件：商家详情视图控件
 * 控制商家详情视图控件
 */
@addon('CleanBusinessDetailsView', '商家详情视图控件', '控制商家详情视图控件')
@reactControl(CleanBusinessDetailsView, true)
export class CleanBusinessDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}