import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Row, Col } from "antd";
import { ListView, Card, SearchBar, NoticeBar, Modal } from "antd-mobile";
import './index.less';
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const alert = Modal.alert;
// const tabs = [
//     { title: '综合' },
//     { title: '距离' },
//     { title: '销量' },
//     { title: '价格' },
//     { title: '区域' },
// ];
// function MyBody(props: any) {
//     return (
//         <div className="am-list-body my-body" style={{ height: '100%' }}>
//             {props.children}
//         </div>
//     );
// }
/**
 * 组件：商家列表视图控件状态
 */
export interface CleanBusinessViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    /** 记录tab状态 */
    tab_status?: string;
    /** 区域列表 */
    areaList?: any;
    /** 服务商列表 */
    ServiceProviderList?: any;
    areaId?: any;
    area_name?: any;
    business_list?:any;
}
/**
 * 组件：商家列表视图控件
 */
export class CleanBusinessView extends ReactView<CleanBusinessViewControl, CleanBusinessViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'enroll',
            areaList: [],
            ServiceProviderList: [],
            areaId: '',
            area_name: '',
            business_list:[]
        };
    }
    componentDidMount() {
        // request(this, AppServiceUtility.activity_service.get_activity_list!({ 'status': this.state.tab_status }, this.state.page, 10))
        //     .then((data: any) => {
        //         // console.log('datas.result', data.result);
        //         this.setState({
        //             list: data.result,
        //         });
        //     });\
        // 获取区域列表
        request(this, AppServiceUtility.business_area_service.get_all_admin_division_list!({}))
            .then((data: any) => {
                // console.log("区域>>>>>>>>>>>", data);
                data.result.map((item: any) => {
                    // if (item.division_level === "4") {
                    let areaList = this.state.areaList;
                    areaList.push({ 'name': item.name, 'id': item.id });
                    this.setState({
                        areaList
                    });
                    // }
                });
                // console.log(this.state.areaList);
                this.areaChoose();
            });

    }
    /** 明细点击事件 */
    goDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.cleanBusinessDetails + '/' + id);
    }
    /** tab切换触发事件 */
    tab_click = (tab: any, index: number) => {
        let tab_status = 'other';
        switch (index) {
            case 0:
                tab_status = 'enroll';
                break;
            case 1:
                tab_status = 'progress';
                break;
            case 2:
                tab_status = 'finish';
                break;
            default:
                tab_status = 'other';
                break;
        }
        this.setState({ tab_status: tab_status, page: 1 });
        // request(this, AppServiceUtility.activity_service.get_activity_list!({ 'status': tab_status }, 1, 10))
        //     .then((data: any) => {
        //         // console.log('datas.result', data.result);
        //         this.setState({
        //             list: data.result,
        //         });
        //     });
    }
    /** 下拉事件 */
    onEndReached = () => {
        // console.log(11111);
        // console.log(this.state.areaId);
        let new_pape = this.state.page + 1;
        request(this, AppServiceUtility.service_operation_service.get_service_provider_list_all!({ admin_area_id: this.state.areaId }, new_pape, 10))
            .then((data: any) => {
                // console.log("服务商信息2>>>>>>>>>>>>>", data);
                let list = this.state.ServiceProviderList;
                data.result.map((item: any) => {
                    // let score = 0;
                    // item.qualification_info.map((item: any) => {
                    //     if (item.qualification_type_id === 'cbf6fe52-d07e-11e9-8be3-144f8aec0be6') {
                    //         score = item.qualification_level;
                    //     }
                    // });
                    list.push({ id: item.id, name: item.name, address: item.organization_info.address, img_url: item.organization_info.picture_list ? item.organization_info.picture_list[0] : '' });
                    this.setState({
                        ServiceProviderList: list,
                        page: new_pape
                    });
                });
            });
    }
    selectServiceProvider = (id: any) => {
        // console.log(id);
        request(this, AppServiceUtility.service_operation_service.get_service_provider_list_all!({ admin_area_id: id }, this.state.page, 10))
            .then((data: any) => {
                // console.log("服务商信息>>>>>>>>>>>>>", data);
                let list = this.state.ServiceProviderList;
                data.result.map((item: any) => {
                    // let score = 0;
                    // item.qualification_info.map((item: any) => {
                    //     if (item.qualification_type_id === 'cbf6fe52-d07e-11e9-8be3-144f8aec0be6') {
                    //         score = item.qualification_level;
                    //     }
                    // });
                    list.push({ id: item.id, name: item.name, address: item.organization_info.address, img_url: item.organization_info.picture_list ? item.organization_info.picture_list[0] : '' });
                    this.setState({
                        ServiceProviderList: list
                    });
                    this.listChange();
                });
            });
    }
    // 这里的区域信息应该发送请求获取
    areaChoose = () => {
        let list: any = [];
        console.info('=========', this.state.areaList);
        this.state.areaList.map((item: any) => {
            list.push({
                text: item.name, onPress: () => {
                    // console.log(item.id);
                    let areaId = item.id;
                    this.setState({
                        areaId,
                        area_name: item.name
                    });
                    // console.log(this.state.areaId);
                    this.selectServiceProvider(item.id);
                }
            });
        });
        // console.log(list);
        alert(<div>服务区域</div>, <div style={{ width: '240px', height: '100%' }} className='business-clean'>请选择你的区域</div>, list);
    }
    search = (value: any) => {
        // console.log(value);
        request(this, AppServiceUtility.service_operation_service.get_service_provider_list_all!({ business_name: value }, 1, 1))
            .then((data: any) => {
                // console.log(data);
                let list: any = [];
                data.result.map((item: any) => {
                    // let score = 0;
                    // item.qualification_info.map((item: any) => {
                    //     if (item.qualification_type_id === 'cbf6fe52-d07e-11e9-8be3-144f8aec0be6') {
                    //         score = item.qualification_level;
                    //     }
                    // });
                    list.push({ id: item.id, name: item.name, address: item.organization_info.address, img_url: item.organization_info.picture_list ? item.organization_info.picture_list[0] : '' });
                    this.setState({
                        ServiceProviderList: list
                    });
                });
            });
    }
    listChange = () => {
        let business_list: any = [];
        this.state.ServiceProviderList.map((item: any, index: any) => {
            let list = (
                <div className="clean-list" key={index} onClick={() => this.goDetails(item.id)} style={{ margin: '0px' }}>
                    <Card className='list-conten'>
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={12}><img src={item.img_url} style={{ height: '72pt', width: '130px' }} /></Col>
                            <Col span={12} className='list-col'>
                                <Row><strong>{item.name}</strong></Row>
                                <Row>地址：{item.address}</Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
            business_list.push(list);
        });
        this.setState({
            business_list
        });
    }
    render() {
        // const { ServiceProviderList, dataSource, } = this.state;
        // 获取item进行展示
        // const renderRow = (owData: any, sectionID: any, rowID: any) => {
        //     return (
        //         <div>
        //             <Card className='list-conten' onClick={() => this.goDetails(owData.id)}>
        //                 <Row type='flex' justify='center'>
        //                     <Col className='list-col' span={10} style={{ paddingTop: '17px' }}><img src={owData.img_url} style={{ height: '72pt' }} /></Col>
        //                     <Col span={14} className='list-col'>
        //                         <Row><strong>{owData.name}</strong></Row>
        //                         {/* <Row><Rate character={<Icon type="heart" theme="filled" />} allowHalf={true} disabled={true} value={owData.score} /></Row>
        //                         <Row>{owData.remarks}</Row>
        //                         <Row>
        //                             <Col span={12}>{owData.distance}</Col>
        //                             <Col span={12}>{owData.reservation_number}已售</Col>
        //                         </Row> */}
        //                         <Row>地址：{owData.address}</Row>
        //                     </Col>
        //                 </Row>
        //             </Card>
        //         </div>
        //     );
        // };
        return (
            <div className='clean-business'>
                <Row>
                    <NoticeBar marqueeProps={{ loop: true, style: { padding: '0 7.5px' }, text: '你当前所选的区域为' + this.state.area_name + '，这些商家只负责配送该区域内！' }} />
                    {/* </NoticeBar> */}
                    <SearchBar placeholder="Search" maxLength={8} onSubmit={(value: any) => { this.search(value); }} />
                    {/* <Tabs
                    tabs={tabs}
                    initialPage={0}
                // onTabClick={(tab, index) => this.tab_click(tab, index)}
                > */}
                    {/* <div className='tabs-content' style={{ padding: '6px' }}>
                        {
                            ServiceProviderList && ServiceProviderList.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(this.state.ServiceProviderList)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                    onEndReached={this.onEndReached}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div> */}
                    {/* </Tabs> */}
                    <div style={{ paddingLeft: '6px', paddingRight: '6px', }}>
                    {/* <div style={{ display: this.state.modal1 ? 'block' : 'none', height: '100%', overflowY: 'auto' }}>
                            <div style={{ fontWeight: 600, background: "#fff", textAlign: "center", padding: '15px 0' }}>请选择区域</div>
                            {this.state.objList}
                        </div> */}
                    <div className="CleanBusiness-list-content">
                        {this.state.business_list.length > 0 ? this.state.business_list : <div style={{ textAlign: 'center' }}>暂无服务商数据</div>}
                    </div>
                </div>
                </Row>
            </div>
        );
    }
}

/**
 * 组件：商家列表视图控件
 * 控制商家列表视图控件
 */
@addon('CleanBusinessView', '商家列表视图控件', '控制商家列表视图控件')
@reactControl(CleanBusinessView, true)
export class CleanBusinessViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}