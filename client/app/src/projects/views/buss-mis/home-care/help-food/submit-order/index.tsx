import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List } from 'antd-mobile';
import { Card, WhiteSpace, Button, Picker } from "antd-mobile";
import { Row, Col } from "antd";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const Item = List.Item;
import './index.less';
/**
 * 组件：提交订单首页状态
 */
export interface FoodSubmitOrderViewState extends ReactViewState {
    getTime?: any;
    distribution?: any;
    buy_list?: any;
    totalprice?: any;
}

/**
 * 组件：提交订单首页
 * 描述
 */
export class FoodSubmitOrderView extends ReactView<FoodSubmitOrderViewControl, FoodSubmitOrderViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            distribution: [{ label: "送货上门", value: 1 }, { label: "幸福院就餐", value: 2 }, { label: "幸福院自取", value: 3 }],
            getTime: [{ label: "半小时后", value: 1 }, { label: "一小时后", value: 2 }, { label: "一个半小时后", value: 3 }],
            buy_list: [],
            totalprice: 0,
        };
    }
    Changedistribution = (e: any) => {
        // console.log(11);
    }
    goPayment = () => {
        let price = this.state.totalprice + 2 + 4;
        this.props.history!.push(ROUTE_PATH.payment + '/' + price);
    }
    componentDidMount() {
        let buy_list = this.props.match!.params.key;
        buy_list = buy_list.split('___');
        let list: any = [];
        let totalprice = 0;
        buy_list.map((item: any) => {
            list.push(JSON.parse(item));
        });
        list.map((item: any) => {
            totalprice = totalprice + (item.price * item.num);
        });
        // console.log(totalprice);
        // console.log(list);
        this.setState({
            buy_list: list,
            totalprice
        });
        // console.log(this.state.totalprice);
    }
    goAddress = () => {
        this.props.history!.push(ROUTE_PATH.address);
    }
    render() {
        return (
            <div>
                <div className="main-content">
                    <List className="address-input">
                        <Item arrow="horizontal" onClick={this.goAddress}>
                            选择收货地址
                        </Item>
                        <Picker
                            data={this.state.distribution ? this.state.distribution : []}
                            value={this.state.distribution[0]}
                            cols={1}
                            onChange={(e) => this.Changedistribution(e)}
                        >
                            <List.Item arrow="horizontal">选择配送方式</List.Item>
                        </Picker>
                        <Picker
                            extra="预计14：51送达"
                            data={this.state.getTime ? this.state.getTime : []}
                            value={this.state.getTime[0]}
                            cols={1}
                            onChange={(e) => this.Changedistribution(e)}
                        >
                            <List.Item arrow="horizontal">预计送达时间</List.Item>
                        </Picker>
                    </List>
                    <WhiteSpace size="sm" />
                    <List>
                        <Item>
                            订单详情
                        </Item>
                    </List>
                    {this.state.buy_list.map((item: any, index: any) => {
                        return (<Card className='list-content' key={index}>
                            <Row type='flex' justify='center'>
                                <Col className='list-col' span={12}><img src={decodeURIComponent(item.image)} style={{ height: '72pt', width: '130px' }} /></Col>
                                <Col span={12} >
                                    <Row><strong>{item.name}</strong></Row>
                                    <Row style={{ paddingRight: '10px' }}>
                                        <Col span={12}>
                                            <span>排骨：20g</span>
                                            <span>饭：20g</span>
                                        </Col>
                                        <span className="list-col-span">￥{item.num * item.price}</span>
                                    </Row>
                                    <Row>
                                        <Col span={12}>x{item.num}</Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>);
                    })}
                    <WhiteSpace size="sm" />
                    <List>
                        <Item extra="￥2">餐盒费</Item>
                        <Item extra="￥4">配送费</Item>
                        <Item extra={'￥' + (this.state.totalprice + 2 + 4)}>小计</Item>
                    </List>
                    <WhiteSpace size="sm" />
                    <List>
                        <Item arrow="horizontal" onClick={(e) => { this.props.history!.push(ROUTE_PATH.remarks); }}>备注</Item>
                    </List>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                </div>
                <div className="submit-btn"><Button type="primary" onClick={this.goPayment}>{'￥' + (this.state.totalprice + 2 + 4)}  提交订单</Button></div>
            </div>
        );
    }
}

/**
 * 控件：提交订单首页控制器
 * 描述
 */
@addon('FoodSubmitOrderView', '提交订单首页', '描述')
@reactControl(FoodSubmitOrderView, true)
export class FoodSubmitOrderViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}