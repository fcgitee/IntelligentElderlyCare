import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import './index.less';
import { Carousel, Card, List, WhiteSpace } from "antd-mobile";
import { Row, Button } from "antd";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";

const Item = List.Item;
/**
 * 组件：食物详情视图控件状态
 */
export interface FoodDetailsViewState extends ReactViewState {
    /** 数据 */
    data?: any[];
    /** 图片高度 */
    imgHeight?: number | string;
    detail?: any;
}
/**
 * 组件：食物详情视图控件
 */
export class FoodDetailsView extends ReactView<FoodDetailsViewControl, FoodDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: ['1', '2', '3'],
            imgHeight: 176,
            detail: []
        };
    }
    componentDidMount() {
        // 根据传过来的id查询相应的服务产品信息
        let id = this.props.match!.params.key;
        // console.log(id);
        request(this, AppServiceUtility.service_package_service.get_service_product_package_list!({ id }, 1, 1))
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    detail: data.result[0]
                });
            });
    }
    goBuy = () => {
        this.props.history!.push(ROUTE_PATH.foodList);
    }
    goComment = () => {
        this.props.history!.push(ROUTE_PATH.comment2);
    }
    render() {
        const { detail } = this.state;
        return (
            <div>
                <Carousel
                    autoplay={true}
                    infinite={true}
                >
                    {detail.picture_collection.length > 0 ? detail.picture_collection!.map((item: any) => (
                        <a
                            className='carousel-a'
                            key={item}
                            href="javascript:;"
                            style={{ height: this.state.imgHeight }}
                        >
                            <img
                                className='carousel-img'
                                src={item}
                                alt=""
                                onLoad={() => {
                                    window.dispatchEvent(new Event('resize'));
                                    this.setState({ imgHeight: 'auto' });
                                }}
                            />
                        </a>
                    ))
                    :
                    <a
                        className='carousel-a'
                        href="javascript:;"
                        style={{ height: this.state.imgHeight }}
                    >
                        <img
                            className='carousel-img'
                            src={'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg'}
                            alt=""
                            onLoad={() => {
                                window.dispatchEvent(new Event('resize'));
                                this.setState({ imgHeight: 'auto' });
                            }}
                        />
                    </a>}
                </Carousel>
                <WhiteSpace size="sm" />
                <Card>
                    <Card.Header
                        title="食物规格"
                    />
                    <Card.Body className="food-details">
                        {detail.introduce}
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card className='list-conten'>
                    <Card.Header
                        title="食物评论"
                    />
                    <div className='tabs-content'>
                        {/* {
                            // comment_list && comment_list.length ?
                            //     <ListView
                            //         ref={el => this['lv'] = el}
                            //         dataSource={dataSource.cloneWithRows(comment_list)}
                            //         renderRow={comment}
                            //         initialListSize={10}
                            //         pageSize={10}
                            //     // renderBodyComponent={() => <MyBody />}
                            //     // style={{ height: this.state.height }}
                            //     />
                            //     :
                                  
                        } */}
                        <Row className='tabs-content' type='flex' justify='center'>
                            <Button type="primary" onClick={this.goComment} style={{ width: '70px' }}>评论</Button>
                        </Row>
                    </div>
                </Card>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <List className="confirmation-orders-buttom info-list">
                    <Item extra={<Button type='primary' onClick={this.goBuy}>购买</Button>}>
                        <Row type='flex' justify='center' className='info-buttom'>￥12</Row>
                    </Item>
                </List>
            </div>
        );
    }
}

/**
 * 组件：食物详情视图控件
 * 控制食物详情视图控件
 */
@addon('FoodDetailsView', '食物详情视图控件', '控制食物详情视图控件')
@reactControl(FoodDetailsView, true)
export class FoodDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}