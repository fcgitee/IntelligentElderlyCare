import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import './index.less';
import { Row } from "antd";
import { Card, List, Radio, Button } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
const RadioItem = Radio.RadioItem;
/**
 * 组件：订单支付控件状态
 */
export interface OrderPaymentViewState extends ReactViewState {
    /** 支付方式 */
    value?: number;
}
/**
 * 组件：订单支付控件
 */
export class OrderPaymentView extends ReactView<OrderPaymentViewControl, OrderPaymentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            value: 1
        };
    }
    onChange = (value: any) => {
        // console.log('checkbox');
        this.setState({
            value,
        });
    }

    onWechatPaymentSubmit = (obj: Object) => {
        AppServiceUtility.wechat_payment_service.wechat_payment!()!
            .then((data: any) => {
                // this.props.history!.push(data.mweb_url);
                window.location = data.mweb_url;
            });
    }

    onAliPaySubmit = (obj: Object) => {
        AppServiceUtility.alipay_service.alipay!({
            a: 1
        });
    }

    render() {
        const { value } = this.state;
        const data = [
            { value: 0, label: '银联' },
            { value: 1, label: '支付宝' },
            { value: 2, label: '微信' },
        ];
        return (
            <div>
                <Card>
                    <Row className='card-margin-top' type='flex' justify='center'>
                        支付剩余时间：29:00
                    </Row>
                    <Row className='card-margin-top' type='flex' justify='center'>
                        <strong>¥500</strong>
                    </Row>
                    <Row className='card-margin-top' type='flex' justify='center'>
                        服务项目名称
                    </Row>
                    <List className='card-margin-top'>
                        {data.map(i => (
                            <RadioItem key={i.value} checked={value === i.value} onChange={() => this.onChange(i.value)}>
                                {i.label}
                            </RadioItem>
                        ))}
                    </List>
                </Card>
                {/* <Row className='confirmation-orders-buttom'><Button type='primary' onClick={this.onWechatPaymentSubmit}>立即支付</Button></Row> */}
                <Row className=''><Button type='primary' onClick={this.onWechatPaymentSubmit}>微信支付</Button></Row>
                <Row className='confirmation-orders-buttom'><Button type='primary' onClick={this.onAliPaySubmit}>支付宝支付</Button></Row>
            </div>
        );
    }
}

/**
 * 组件：订单支付控件
 * 控制订单支付控件
 */
@addon('OrderPaymentView', '订单支付控件', '控制订单支付控件')
@reactControl(OrderPaymentView, true)
export class OrderPaymentViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}