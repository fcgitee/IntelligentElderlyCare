import { Button, Col, Icon, Rate, Row } from "antd";
import { Card, List, Modal, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { LoadingEffectView } from "src/business/views/loading-effect";
// var QRCode = require('qrcode.react');
const Item = List.Item;
/**
 * 组件：预约申请状态
 */
export interface AppointmentApplicationViewState extends ReactViewState {
    // 机构预约记录
    reservation_registration?: any;
    // 活动报名记录
    activity_participate?: any;
    // 补助申请记录
    allowance_manage?: any;
    // 慈善申请记录
    charitable_donate?: any;
    // 社区活动状态
    community_activities_state?: string;
    // 二维码弹窗
    qr_code_molde?: boolean;
    jgpage?: any;
    hdpage?: any;
    Bzpage?: any;
    Cspage?: any;
    upload?: boolean;
}

/**
 * 组件：预约申请
 * 描述
 */
export class AppointmentApplicationView extends ReactView<AppointmentApplicationViewControl, AppointmentApplicationViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            reservation_registration: [],
            activity_participate: [],
            allowance_manage: [],
            charitable_donate: [],
            community_activities_state: '',
            qr_code_molde: false,
            jgpage: 1,
            hdpage: 1,
            Bzpage: 1,
            Cspage: 1,
            upload: true,
        };
    }
    componentDidMount() {
    }

    getJgList = (param: any, page: any, oldlist: any) => {
        AppServiceUtility.person_org_manage_service!.get_current_user_reservation_registration!(param, page, 3)!
            .then((data: any) => {
                // console.log(data);
                let reservation_registration = [...oldlist, ...data.result];
                if (data.result && data.result.length > 0) {
                    this.setState({
                        reservation_registration,
                        upload: false,
                    });
                } else {
                    this.setState({ upload: false });
                }

            })
            .catch((err: any) => {
                this.setState({ upload: false });
                console.info(err);
            });
    }

    getHd = (page: any, old_list: any) => {
        AppServiceUtility.person_org_manage_service!.get_current_user_activity_participate!({}, page, 3)!
            .then(data => {
                if (data.result && data.result.length > 0) {
                    let time: number;
                    let list = data.result;
                    list.map((item: any, index: any) => {
                        if (item.activity) {
                            let start_date = new Date(item.activity[0].begin_date).getTime();
                            let now_date = new Date().getTime();
                            time = (now_date - start_date) / 1000 / 60;
                            if (30 > time && time > -30) {
                                list[index] = { ...list[index], 'state': '签到中' };
                            } else if (time < -30) {
                                list[index] = { ...list[index], 'state': '报名中' };
                            } else if (time > 30) {
                                list[index] = { ...list[index], 'state': '已结束' };
                            }
                        }
                    });
                    list = [...old_list, ...list];
                    this.setState({
                        activity_participate: list,
                        upload: false,
                    });
                } else {
                    this.setState({ upload: false });
                }
            })
            .catch(err => {
                this.setState({ upload: false });
                console.info(err);
            });
    }

    getYlbz = (page: any, old_list: any) => {
        AppServiceUtility.person_org_manage_service!.get_current_user_allowance_manage!({}, 1, 3)!
            .then(data => {
                if (data.result && data.result.length > 0) {
                    let list = [...old_list, ...data.result];
                    this.setState({
                        allowance_manage: list,
                        upload: false,
                    });
                } else {
                    this.setState({ upload: false });
                }
            })
            .catch(err => {
                this.setState({ upload: false });
                console.info(err);
            });
    }

    getCsjj = (page: any, old_list: any) => {
        AppServiceUtility.person_org_manage_service!.get_current_user_charitable_donate!({}, page, 3)!
            .then(data => {
                if (data.result && data.result.length > 0) {
                    let list = [...old_list, ...data.result];
                    this.setState({
                        upload: false,
                        charitable_donate: list,
                    });
                } else {
                    this.setState({ upload: false });
                }
            })
            .catch(err => {
                this.setState({ upload: false });
                console.info(err);
            });
    }
    componentWillMount() {
        this.getJgList({}, 1, []);
        this.getHd(1, []);
        this.getYlbz(1, []);
        this.getCsjj(1, []);
    }

    onClose = () => {
        this.setState({
            qr_code_molde: false,
        });
    }
    communityClick = (e: any) => {
        if (e === '签到中') {
            this.setState({
                qr_code_molde: true,
            });
        }

    }
    activityClick = (e: any) => {
        if (e) {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + e);
        }
    }
    organizationClick = (e: any) => {
        if (e) {
            this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + e);
        }
    }
    charitableDonateClick = (e: any) => {
        if (e) {
            this.props.history!.push(ROUTE_PATH.charitableProjectInfo + '/' + e);
        }
    }

    // 获取更多机构预约信息
    getMoreJG = () => {
        let page = this.state.jgpage + 1;
        this.setState({
            jgpage: page
        });
        let list = this.state.reservation_registration;
        this.getJgList({}, page, list);
    }

    // 获取更多活动信息
    getMoreHd = () => {
        let page = this.state.hdpage + 1;
        this.setState({
            hdpage: page
        });
        let list = this.state.activity_participate;
        this.getHd(page, list);
    }

    getMoreYlbz = () => {
        let page = this.state.hdpage + 1;
        this.setState({
            Bzpage: page
        });
        let list = this.state.allowance_manage;
        this.getYlbz(page, list);
    }

    getMoreCsjj = () => {
        let page = this.state.hdpage + 1;
        this.setState({
            Cspage: page
        });
        let list = this.state.allowance_manage;
        this.getCsjj(page, list);
    }
    render() {
        let { reservation_registration, activity_participate, allowance_manage, charitable_donate } = this.state;
        setMainFormTitle('预约申请');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.mine);
        });
        return (
            <div className='appointment-application'>
                {this.state.upload && <LoadingEffectView />}
                {
                    reservation_registration.length > 0 ?
                        <span>
                            <Card full={true}>
                                <Card.Header
                                    title="养老机构预约记录"
                                />
                                {reservation_registration.map((item: any) => {
                                    return <Card.Body key={item} onClick={this.organizationClick.bind(this, item && item.organization.length > 0 ? item.organization[0].id : '')}>
                                        <List>
                                            <Item extra={<Button>已预约</Button>}><span style={{ marginRight: '10px' }}>{item.elder_info.length > 0 ? item.elder_info[0].name : ''}</span><span style={{ fontWeight: 'bold', fontSize: "13px" }}>{'预约时间：' + (item ? item.reservation_date : '')}</span></Item>
                                        </List>
                                        <Row type='flex' justify='center' >

                                            <Col span={10}><img src={item && item.organization.length > 0 && item.organization[0].organization_info.picture_list.length > 0 ? item.organization[0].organization_info.picture_list[0] : ''} style={{ height: '72pt', width: '98%' }} /></Col>
                                            <Col span={14} className='list-col'>
                                                <h3>{item && item.organization.length > 0 ? item.organization[0].name : ''}</h3>
                                                <Row>
                                                    <Rate disabled={true} />
                                                </Row>
                                                {/* <Row>
                                入住率：95%
                            </Row> */}
                                                <Row>
                                                    <Col span={16}>
                                                        {item && item.organization.length > 0 ? item.organization[0].organization_info.telephone : ''}
                                                    </Col>
                                                    <Col span={8} style={{ textAlign: 'right', fontSize: '12px' }}>
                                                        {item && item.organization.length > 0 ? item.organization[0].organization_info.organization_nature : ""}
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={16}>
                                                        {item && item.organization.length > 0 ? item.organization[0].town : ''}
                                                    </Col>
                                                    <Col span={8} style={{ textAlign: 'right', fontSize: '12px' }}>
                                                        <Icon type="heart" theme="filled" style={{ marginRight: '5px' }} />0
                                </Col>
                                                </Row>
                                                <Row>
                                                    {item && item.organization.length > 0 ? item.organization[0].organization_info.address : ''}
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Card.Body>;
                                })}
                                <Card.Body onClick={this.getMoreJG}>
                                    <div style={{ overflow: 'hidden' }}>
                                        <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                        <div style={{ float: 'right' }}><Icon type="right" /></div>
                                    </div>
                                </Card.Body>
                            </Card>
                            <WhiteSpace />
                        </span>
                        : ''}
                {activity_participate.length > 0 ?
                    <span>
                        <Card full={true}>
                            <Card.Header
                                title="社区活动报名记录"
                            />
                            {activity_participate.map((item: any, index: any) => {
                                return <Card.Body key={index + index} onClick={this.activityClick.bind(this, item.activity.length > 0 ? item.activity[0].id : '')}>
                                    <List>
                                        <Item extra={<Button onClick={this.communityClick.bind(this, item.state)}>{item.state}</Button>}>报名时间：<span style={{ fontWeight: 'bold', fontSize: "13px" }}>{item ? item.date : ''}</span></Item>
                                    </List>
                                    <Row type='flex' justify='center' >

                                        <Col span={10}><img src={''} style={{ height: '72pt', width: '98%' }} /></Col>
                                        <Col span={14} className='list-col'>
                                            <h3>{allowance_manage ? allowance_manage.allowance_type : ''}</h3>
                                            <Row>
                                                申请服务：{allowance_manage ? allowance_manage.allowance_money : ''}
                                            </Row>
                                        </Col>
                                    </Row>
                                </Card.Body>;
                            })}
                            <Card.Body onClick={this.getMoreHd}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>
                        </Card>
                        <WhiteSpace />
                    </span> : ''
                }
                {allowance_manage.length > 0 ?
                    <span>
                        <Card full={true}>
                            <Card.Header
                                title="慈善基金申请记录"
                            />
                            {allowance_manage.map((item: any) => {
                                return <Card.Body key={item}>
                                    <List>
                                        <Item extra={<Button>{item ? item.status : ''}</Button>}>提交时间：<span style={{ fontWeight: 'bold', fontSize: "13px" }}>{item ? item.create_date : ''}</span></Item>
                                    </List>
                                    <Row type='flex' justify='center' >

                                        <Col span={10}><img src={''} style={{ height: '72pt', width: '98%' }} /></Col>
                                        <Col span={14} className='list-col'>
                                            <h3>{item ? item.allowance_type : ''}</h3>
                                            <Row>
                                                申请服务：{item ? item.allowance_money : ''}
                                            </Row>
                                        </Col>
                                    </Row>
                                </Card.Body>;
                            })}
                            <Card.Body onClick={this.getMoreYlbz}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>
                        </Card>
                        <WhiteSpace />
                    </span> : ""
                }
                {charitable_donate.length > 0 ?
                    <Card full={true}>
                        <Card.Header
                            title="慈善基金申请记录"
                        />
                        {charitable_donate.map((item: any) => {
                            return <Card.Body key={item} onClick={this.charitableDonateClick.bind(this, item ? item.id : '')}>
                                <List>
                                    <Item extra={<Button>{item ? item.status : ''}</Button>}>提交时间：<span style={{ fontWeight: 'bold', fontSize: "13px" }}>{item ? item.create_date : ''}</span></Item>
                                </List>
                                <Row type='flex' justify='center' >

                                    <Col span={10}><img src={''} style={{ height: '72pt', width: '98%' }} /></Col>
                                    <Col span={14} className='list-col'>
                                        <h3>{item ? item.donate_name : ''}</h3>
                                        <Row>
                                            符合类型：{item ? item.remark : ''}
                                        </Row>
                                        <Row>
                                            申请金额：{item ? item.donate_money : ''}
                                        </Row>
                                    </Col>
                                </Row>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={this.getMoreCsjj}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card> : ''
                }
                {
                    !charitable_donate && !allowance_manage && !activity_participate && !reservation_registration ?
                        <div style={{ textAlign: 'center' }}>
                            没有申请记录
                    </div> : ''
                }
                <Modal
                    visible={this.state.qr_code_molde!}
                    onClose={this.onClose}
                    closable={true}
                    style={{ textAlign: 'center' }}
                    className='c-modele'
                >
                    <div style={{ height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'flex-end' }}>
                        {/* <QRCode value='hello' size={200} /> */}
                    </div>

                </Modal>
            </div >
        );
    }
}

/**
 * 控件：预约申请控制器
 * 描述
 */
@addon('AppointmentApplicationView', '预约申请', '预约申请')
@reactControl(AppointmentApplicationView, true)
export class AppointmentApplicationViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}