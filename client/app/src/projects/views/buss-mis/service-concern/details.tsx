import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { SearchBar, Card, Toast, ListView } from 'antd-mobile';
import { AppServiceUtility } from "src/projects/app/appService";
import { Rate, Icon, Spin } from 'antd';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row } from "antd";

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：服务关注-列表状态
 */
export interface ServiceConcernDetailsViewState extends ReactViewState {
    card_list?: any;
    cancel_gz?: any;
    dataSource?: any;
    page?: any;
    insert?: any;
    animating?: any;
}

/**
 * 组件：服务关注-列表
 * 描述
 */
export class ServiceConcernDetailsView extends ReactView<ServiceConcernDetailsViewControl, ServiceConcernDetailsViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            card_list: [],
            cancel_gz: 1,
            page: 1,
            insert: '',
            animating: false
        };
    }

    selectList = (param: any, page: any, old_list: any) => {
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param, page, 10)!
            .then((data: any) => {
                let card_list = [];
                // console.log('shuju??????', old_list);
                if (data.result.length > 0) {
                    // console.log(data);
                    card_list = [...data.result, ...old_list];
                    this.setState(
                        {
                            card_list,
                            animating: false
                        }
                    );
                }
            });
    }

    selectAll = (card_list: any, param: any, page: any) => {
        let param1: any = { 'type': 'follow', 'object': '养老机构', ...param };
        let param2: any = { 'type': 'follow', 'object': '社区幸福院', ...param };
        let param3: any = { 'type': 'follow', 'object': '居家服务商', ...param };
        // 1
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param1, page, 10)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    // console.log(data);
                    card_list = [...data.result, ...card_list];
                }
                // 2
                AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param2, page, 10)!
                    .then((data: any) => {
                        if (data.result.length > 0) {
                            // console.log(data);
                            card_list = [...data.result, ...card_list];
                        }
                        // 3
                        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param3, page, 10)!
                            .then((data: any) => {
                                if (data.result.length > 0) {
                                    // console.log(data);
                                    card_list = [...data.result, ...card_list];
                                    // console.log(card_list);
                                    this.setState({
                                        card_list,
                                        animating: false
                                    });
                                }
                            });
                    });
            });
    }
    componentDidMount = () => {
        this.setState({
            animating: true
        });
        if (this.props.match!.params.key.split('&').length > 1) {
            this.selectAll([], { "name": this.props.match!.params.key.split('&')[0] }, 1);
        } else {
            this.selectList({ 'type': 'follow', 'object': this.props.match!.params.key }, 1, []);
        }
    }

    // 取消关注
    likeChange = (value: any, id: any) => {
        // console.log(value);
        // console.log(id);
        if (value === 0) {
            // console.log("取消关注了");
            AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(id)!
                .then((data: any) => {
                    // console.log(data);
                    if (data === 'Success') {
                        this.setState({
                            cancel_gz: 0
                        });
                        Toast.success('取消关注成功!!!', 1);
                        window.location.reload();
                    }
                });
        }
    }
    iconClick = (e: any) => {
        e.stopPropagation();
    }
    // 前往详情
    goDetails = (type: any, id: any) => {
        // console.log(id);
        // console.log(type);
        if (type === '养老机构') {
            this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + id);
        }
        if (type === '社区幸福院') {
            this.props.history!.push(ROUTE_PATH.happinessHomeInfo + '/' + id);
        }
        if (type === '居家服务商') {
            this.props.history!.push(ROUTE_PATH.foodBusinessDetails + '/' + id);
        }
    }
    // 下拉加载更多
    onEndReached = () => {
        // console.log(111);
        let new_pape = this.state.page + 1;
        this.setState({
            page: new_pape
        });
        if (this.props.match!.params.key.split('&').length > 1) {
            let card_list: any = this.state.card_list;
            let param = { "name": this.props.match!.params.key.split('&')[0] };
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            this.selectAll(card_list, param, new_pape);
        } else {
            let card_list = this.state.card_list;
            let param = {};
            param = { 'type': 'follow', 'object': this.props.match!.params.key };
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            this.selectList(param, new_pape, card_list);
        }
    }

    search = () => {
        this.setState({
            page: 1,
            animating: true,
            card_list: []
        });
        if (this.props.match!.params.key.split('&').length > 1) {
            let param = { "name": this.props.match!.params.key.split('&')[0] };
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            this.selectAll([], param, 1);
        } else {
            let param = {};
            param = { 'type': 'follow', 'object': this.props.match!.params.key };
            if (this.state.insert !== '') {
                param['name'] = this.state.insert;
            }
            this.selectList(param, 1, []);
        }
    }
    searchChange = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }
    render() {
        const { card_list, dataSource, } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            let show_data = owData.obj_list.length > 0 ? owData.obj_list[0] : '';
            let show_num = owData.sum_follow ? owData.sum_follow : 0;
            let show_area = owData.area ? owData.area : '';
            return (
                <Card style={{ marginBottom: '20px' }} key={owData}>
                    <Card.Body onClick={() => { this.goDetails(this.props.match!.params.key, show_data.id); }}>
                        <div style={{ overflow: 'hidden' }}>
                            <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                <img src={show_data.organization_info ? show_data.organization_info.picture_list ? show_data.organization_info.picture_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                            </div>
                            <div style={{ float: 'left', width: '59%' }}>
                                <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate character={<Icon type="heart" />} count={1} defaultValue={1} value={this.state.cancel_gz} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                <div><Rate disabled={true} value={show_data.organization_info ? show_data.organization_info.star_level : 0} /></div>
                                <div>{show_data.organization_info ? show_data.organization_info.telephone : ''}</div>
                                <div>{show_data.organization_info ? show_data.organization_info.organization_nature : ''}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '147px', overflow: 'hidden', display: 'inline-block' }}>{show_data.organization_info ? show_data.organization_info.address : ''}</span></div>
                                <div style={{ overflow: 'hidden', width: '100%' }}>
                                    <div style={{ float: 'left', width: '80%' }}>{show_area}&nbsp;|&nbsp;2km</div>
                                    <div style={{ float: 'right' }}><Icon type="heart" theme="filled" />&nbsp;{show_num}</div>
                                </div>
                            </div>
                        </div>
                    </Card.Body>
                </Card>
            );
        };
        return (
            <div>
                <SearchBar placeholder="可根据名称搜索" onChange={this.searchChange} maxLength={8} onSubmit={this.search} />
                <Row className='tabs-content'>
                    {
                        card_list && card_list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(card_list)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: document.documentElement!.clientHeight }}
                                onEndReached={this.onEndReached}
                            />
                            :
                            (card_list.length === 0 && this.state.animating === true ?
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>
                                :
                                '')

                    }
                    {card_list.length === 0 && this.state.animating === false ?
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                            </Row>
                        :
                        ''
                    }
                </Row>
            </div>
        );
    }
}

/**
 * 控件：服务关注-列表控制器
 * 描述
 */
@addon('ServiceConcernDetailsView', '服务关注-列表', '描述')
@reactControl(ServiceConcernDetailsView, true)
export class ServiceConcernDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}