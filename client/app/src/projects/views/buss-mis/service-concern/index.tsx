import { Icon, Rate, Spin } from 'antd';
import { Card, SearchBar, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { setMainFormTitle } from "src/business/util_tool";

/**
 * 组件：服务关注状态
 */
export interface ServiceConcernViewState extends ReactViewState {
    type?: any;
    card_list?: any;
    cancel_gz?: any;
    insert?: any;
    location?: any;
    show?: any;
}

/**
 * 组件：服务关注
 * 描述
 */
export class ServiceConcernView extends ReactView<ServiceConcernViewControl, ServiceConcernViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            type: ['养老机构', '社区幸福院', '居家服务商'],
            card_list: [],
            cancel_gz: 1,
            insert: '',
            location: undefined,
            show: false
        };
    }
    componentDidMount = () => {
        // // 监听app端通知
        // if ((window as any).ReactNativeWebView) {
        //     const messageCb = (e: any) => {
        //         const retObject = JSON.parse(e.data);
        //         if (getWebViewRetObjectType(retObject) === 'getPhonePosition') {
        //             const location = retObject.data;
        //             if (location === undefined) {
        //                 // Toast.fail('获取位置信息失败');
        //             }
        //             this.setState({
        //                 location: retObject.data
        //             });
        //             const { type } = this.state;
        //             let card_list: any = [];
        //             type.map((item: any) => {
        //                 AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ type: 'follow', object: item, ...location }, 1, 1)!
        //                     .then((data: any) => {
        //                         console.info('我在这里呀');
        //                         // console.log(item, data);
        //                         this.setState({
        //                             show: true
        //                         });
        //                         if (data.result.length > 0) {
        //                             let show_data = data.result[0].obj_list.length > 0 ? data.result[0].obj_list[0] : '';
        //                             let show_num = data.result[0].follow_count ? data.result[0].follow_count : 0;
        //                             let show_area = data.result[0].area ? data.result[0].area : '';
        //                             const card: JSX.Element = (
        //                                 <Card style={{ marginBottom: '20px' }} key={item}>
        //                                     <div onClick={(e) => { this.goList(e, item); }}>
        //                                         <Card.Header
        //                                             title={item}
        //                                             extra={<Icon type='right' />}
        //                                         />
        //                                     </div>
        //                                     <Card.Body onClick={() => { this.goDetails(item, show_data.id); }}>
        //                                         <div style={{ overflow: 'hidden' }}>
        //                                             <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
        //                                                 <img src={show_data.organization_info ? show_data.organization_info.picture_list ? show_data.organization_info.picture_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
        //                                             </div>
        //                                             <div style={{ float: 'left', width: '59%' }}>
        //                                                 <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate character={<Icon type="heart" />} defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, data.result[0].id); }} /></div></div>
        //                                                 <div><Rate disabled={true} value={show_data.organization_info ? show_data.organization_info.star_level : 0} /></div>
        //                                                 <div>{show_data.organization_info ? show_data.organization_info.telephone : ''}</div>
        //                                                 <div style={{width: '100%'}}>
        //                                                     <div style={{ float: 'left', width: '40%' }}>{show_data.organization_info ? show_data.organization_info.organization_nature : ''}</div>
        //                                                     <div style={{ float: 'right',width: '60%' }}><span style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '100%', overflow: 'hidden', display: 'inline-block' }}>{show_data.organization_info ? show_data.organization_info.address : ''}</span></div>
        //                                                 </div>
        //                                                 <div style={{ overflow: 'hidden', width: '100%' }}>
        //                                                     <div style={{ float: 'left', width: '80%' }}>{show_area}&nbsp;|&nbsp;{data.result[0].distance ? '| ' + data.result[0].distance.toFixed(2) + 'km' : ''}</div>
        //                                                     <div style={{ float: 'right' }}><Icon type="heart" theme="filled" />&nbsp;{show_num}</div>
        //                                                 </div>
        //                                             </div>
        //                                         </div>
        //                                     </Card.Body>
        //                                 </Card>
        //                             );
        //                             card_list.push(card);
        //                             // console.log(card_list.length);
        //                         }
        //                         this.setState(
        //                             {
        //                                 card_list
        //                             }
        //                         );
        //                     });
        //             });
        //         }
        //     };
        //     if ((window as any).ReactNativeWebView) {
        //         subscribeWebViewNotify(messageCb);
        //     }

        //     this.getPhonePosition();
        // }
        const { type } = this.state;
        let card_list: any = [];
        type.map((item: any) => {
            AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ type: 'follow', object: item, ...location }, 1, 1)!
                .then((data: any) => {
                    // console.log(item, data);
                    this.setState({
                        show: true
                    });
                    if (data.result.length > 0) {
                        let show_data = data.result[0].obj_list.length > 0 ? data.result[0].obj_list[0] : '';
                        let show_num = data.result[0].follow_count ? data.result[0].follow_count : 0;
                        let show_area = data.result[0].area ? data.result[0].area : '';
                        const card: JSX.Element = (
                            <Card style={{ marginBottom: '20px' }} key={item}>
                                <div onClick={(e) => { this.goList(e, item); }}>
                                    <Card.Header
                                        title={item}
                                        extra={<Icon type='right' />}
                                    />
                                </div>
                                <Card.Body onClick={() => { this.goDetails(item, show_data.id); }}>
                                    <div style={{ overflow: 'hidden' }}>
                                        <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                            <img src={show_data.organization_info ? show_data.organization_info.picture_list ? show_data.organization_info.picture_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                        </div>
                                        <div style={{ float: 'left', width: '59%' }}>
                                            <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate character={<Icon type="heart" />} defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, data.result[0].id); }} /></div></div>
                                            <div><Rate disabled={true} value={show_data.organization_info ? show_data.organization_info.star_level : 0} /></div>
                                            <div>{show_data.organization_info ? show_data.organization_info.telephone : ''}</div>
                                            <div style={{ width: '100%' }}>
                                                <div style={{ float: 'left', width: '40%' }}>{show_data.organization_info ? show_data.organization_info.organization_nature : ''}</div>
                                                <div style={{ float: 'right', width: '60%' }}><span style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '100%', overflow: 'hidden', display: 'inline-block' }}>{show_data.organization_info ? show_data.organization_info.address : ''}</span></div>
                                            </div>
                                            <div style={{ overflow: 'hidden', width: '100%' }}>
                                                <div style={{ float: 'left', width: '80%' }}>{show_area}&nbsp;|&nbsp;{data.result[0].distance ? '| ' + data.result[0].distance.toFixed(2) + 'km' : ''}</div>
                                                <div style={{ float: 'right' }}><Icon type="heart" theme="filled" />&nbsp;{show_num}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Card>
                        );
                        card_list.push(card);
                        // console.log(card_list.length);
                    }
                    this.setState(
                        {
                            card_list
                        }
                    );
                });
        });
    }

    getPhonePosition() {
        // Toast.info('认证成功：' + e.data);
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'getPhonePosition',
                    params: undefined
                })
            );
        }
    }

    // 前往列表页
    goList = (e: any, type: any) => {
        // console.log(type);
        this.props.history!.push(ROUTE_PATH.serviceConcernDetails + '/' + type);
    }

    // 取消关注
    likeChange = (value: any, id: any) => {
        // console.log(value);
        // console.log(id);
        if (value === 0) {
            // console.log("取消关注了");
            AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(id)!
                .then((data: any) => {
                    // console.log(data);
                    if (data === 'Success') {
                        this.setState({
                            cancel_gz: 0
                        });
                        Toast.success('取消关注成功!!!', 1);
                        window.location.reload();
                    }
                });
        }
    }
    iconClick = (e: any) => {
        e.stopPropagation();
    }
    // 前往详情
    goDetails = (type: any, id: any) => {
        // console.log(id);
        // console.log(type);
        if (type === '养老机构') {
            this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + id);
        }
        if (type === '社区幸福院') {
            this.props.history!.push(ROUTE_PATH.happinessHomeInfo + '/' + id);
        }
        if (type === '居家服务商') {
            this.props.history!.push(ROUTE_PATH.serverDetail + '/' + id);
        }
    }

    search = (value: any) => {
        // console.log(value);
        this.props.history!.push(ROUTE_PATH.serviceConcernDetails + '/' + this.state.insert + '&');
    }
    searchChange = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }
    render() {
        setMainFormTitle('服务关注');
        return (
            <div>
                <SearchBar placeholder="可根据名称搜索" onChange={this.searchChange} onSubmit={this.search} />
                {this.state.card_list.length > 0 && this.state.show === true ?
                    this.state.card_list.map((item: any) => {
                        return item;
                    })
                    :
                    ''
                }
                {this.state.show === false && this.state.card_list.length === 0 ?
                    <div style={{ textAlign: 'center', margin: '20px' }}>
                        <Spin size="large" />
                    </div>
                    :
                    <div style={{ width: '100%', textAlign: 'center' }}>暂无关注数据</div>
                }
            </div>
        );
    }
}

/**
 * 控件：服务关注控制器
 * 描述
 */
@addon('ServiceConcernView', '服务关注', '描述')
@reactControl(ServiceConcernView, true)
export class ServiceConcernViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}