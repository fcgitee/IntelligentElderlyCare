import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { List, InputItem, DatePicker, Button, WhiteSpace, } from "antd-mobile";
import { Radio, Form } from "antd";
import { Brief } from "antd-mobile/lib/list/ListItem";
import { Address } from "../../buss-pub/family-appointment/address";
const Item = List.Item;

/**
 * 组件：调研数据录入状态
 */
export interface ResearchManageViewState extends BaseReactElementState {
    /** 生日 */
    date: any;
    /** 家庭地址-省市区 */
    address?: any;
    /** 详细地址 */
    detail_address?: string;
    /** 家庭地址配置 */
    address_config: any;
}

/**
 * 组件：调研数据录入
 * 描述
 */
export class ResearchManageView extends BaseReactElement<ResearchManageViewControl, ResearchManageViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            date: '',
            address: ['请选择'],
            detail_address: '',
            address_config: {
                title: '家庭地址',
                row_name: '省-市-区-街道',
                address_modal: false
            },
        };
    }
    // 获取家庭地址
    getHomeAddress = (e: any) => {
        let config = this.state.address_config;
        config.address_modal = false;
        this.setState({ address: e.address, address_config: config });
    }
    showHomeAddress = (key: any) => {
        let config = this.state[key];
        config.address_modal = true;
        key = this.setState({ address_config: config });
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <List>
                    <InputItem {...getFieldProps('name')} placeholder="请输入姓名">姓名</InputItem>
                    <Item
                        extra={<Radio.Group defaultValue={'男'}>
                            <Radio value={'男'}>男</Radio>
                            <Radio value={'女'}>女</Radio>
                        </Radio.Group>}
                    >性别
                    </Item>
                    <Item
                        arrow='empty'
                    >证件类型
                    <WhiteSpace size='md' />
                        <Brief><Radio.Group defaultValue={'身份证'}>
                            <Radio value={'身份证'}>身份证</Radio>
                            <Radio value={'残疾证'}>残疾证</Radio>
                        </Radio.Group></Brief>
                    </Item>
                    <InputItem {...getFieldProps('id_card')} placeholder="证件号码">证件号码</InputItem>
                    <DatePicker
                        mode="date"
                        title="出生日期"
                        extra="请选择出生日期"
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">出生日期</List.Item>
                    </DatePicker>
                    <InputItem {...getFieldProps('telephone')} placeholder="请输入联系电话">联系电话</InputItem>
                    <InputItem {...getFieldProps('nation')} placeholder="请输入民族">民族</InputItem>
                    <InputItem {...getFieldProps('native_place')} placeholder="请输入籍贯">籍贯</InputItem>
                    <List.Item arrow="horizontal" onClick={() => { this.showHomeAddress('address_config'); }} extra={this.state.address}>家庭地址</List.Item>
                    <InputItem {...getFieldProps('family_members_name')} placeholder="请输入家属姓名">家属姓名</InputItem>
                    <InputItem {...getFieldProps('family_members_phone')} placeholder="请输入家属联系电话">家属联系电话</InputItem>
                    <Item
                        arrow='empty'
                    >家属关系
                    <WhiteSpace size='md' />
                        <Brief><Radio.Group>
                            <Radio value={'父子'}>父子</Radio>
                            <Radio value={'母子'}>母子</Radio>
                            <Radio value={'兄弟'}>兄弟</Radio>
                        </Radio.Group></Brief>
                    </Item>
                    <Button type="primary" >保存</Button>
                    <Address config={this.state.address_config} getAddress={this.getHomeAddress} />
                </List>
            </div>
        );
    }
}

/**
 * 控件：调研数据录入控制器
 * 描述
 */
@addon('ResearchManageView', '调研数据录入', '调研数据录入')
@reactControl(Form.create<any>()(ResearchManageView), true)
export class ResearchManageViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}