import { Grid } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { ROUTE_PATH, getAppPermissonObject } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
import { LOCAL_FUNCTION_LIST } from 'src/business/mainForm/backstageManageMainForm';
/**
 * 组件：社区养老首页状态
 */
export interface CommunityListViewState extends ReactViewState {
    icon_data?: any;
    /*用户id */
    id?: any;
    /*账号名 */
    name?: any;
}

/**
 * 组件：社区养老首页
 * 描述
 */
export class CommunityListView extends ReactView<CommunityListViewControl, CommunityListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            icon_data: []
        };
    }
    componentDidMount() {
        let icon_data = [
            // { text: '发布活动', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
            { text: '幸福院列表', icon: <FontIcon type="iconziyuan39" style={{ fontSize: '40px' }} /> },
            { text: '活动列表', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
            { text: '我的活动', icon: <FontIcon type="iconziyuan34" style={{ fontSize: '40px' }} /> },
        ];
        const AppPermission = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);
        if (AppPermission && getAppPermissonObject(AppPermission, '活动列表', '新增')) {
            icon_data.push({ text: '发布活动', icon: <FontIcon type="iconziyuan28" style={{ fontSize: '40px' }} /> });
        }
        if (AppPermission && getAppPermissonObject(AppPermission, '活动室列表', '查询')) {
            icon_data.push({ text: '活动室管理', icon: <FontIcon type="iconziyuan33" style={{ fontSize: '40px' }} /> });
        }
        this.setState({
            icon_data,
        });
    }
    grid_click = (_el: any) => {
        switch (_el.text) {
            case '发布活动':
                this.props.history!.push(ROUTE_PATH.changeActivity);
                break;
            case '活动列表':
                this.props.history!.push(ROUTE_PATH.activityList);
                break;
            case '我的活动':
                this.props.history!.push(ROUTE_PATH.myActivityList);
                break;
            case '活动信息汇总':
                this.props.history!.push(ROUTE_PATH.activityInformation);
                break;
            case '幸福院列表':
                this.props.history!.push(ROUTE_PATH.happinessHomeList);
                break;
            case '活动室管理':
                this.props.history!.push(ROUTE_PATH.activityRoomList);
                break;
            default:
                break;
        }
    }
    render() {
        setMainFormTitle('社区服务中心');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.home);
        });
        return (
            <div>
                <Grid data={this.state.icon_data} activeStyle={true} columnNum={3} onClick={_el => this.grid_click(_el)} />
            </div>
        );
    }
}

/**
 * 控件：社区养老首页控制器
 * 描述
 */
@addon('CommunityListView', '社区养老首页', '描述')
@reactControl(CommunityListView, true)
export class CommunityListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}