import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, WhiteSpace } from 'antd-mobile';
// import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Icon } from "antd";
// import { Row, Col } from "antd";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
const Item = List.Item;
/**
 * 组件：我的账户详情视图控件状态
 */
export interface MyAccountViewState extends ReactViewState {
    /** 我的账户数据 */
    account_data?: any;
    /** 账户数据list */
    list_data?: any;

}
/**
 * 组件：我的账户详情视图控件
 */
export class MyAccountView extends ReactView<MyAccountViewControl, MyAccountViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            account_data : [],

        };
    }
    componentDidMount() {
        // 通过id获取用户的详细信息
        request(this, AppServiceUtility.my_account_service.get_my_account!())
            .then((data: any) => {
                // console.log("我的账户", data);
                if (data.result.length > 0) {
                    this.setState({
                        account_data: data.result
                    });
                }
                // console.log("》》》》", this.state.account_data);
            });
    }
    onClickRow = (id: string) => {
        this.props.history!.push(ROUTE_PATH.accountDetail + '/' + id);
    }
    render() {
        return (
            <div style={{ height: document.body.clientHeight }}>
                <WhiteSpace size="lg" />
                <List>
                    {
                        this.state.account_data.length > 0 ? this.state.account_data.map((data: any, index: any) => {
                            if (data.account_type === 'APP储值账户') {
                                return <Item key={index} thumb={<Icon type="money-collect" />} onClick={() => { this.onClickRow(data.id); }} arrow="horizontal" style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra={'￥' + data.balance}>{data.account_type}</Item>;

                            } else {
                                return <Item key={index} thumb={<Icon type="money-collect" />}  arrow="horizontal" style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra={'￥' + data.balance}>{data.account_type}</Item>;
                            }
                            
                        }) : <Item  onClick={() => { }}>暂无账户</Item>
                    }
                </List>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                {/* <List >
                    <Item extra={<Row gutter={20}><Col span={12}><Button type='primary' onClick={() => this.state.account_data}>设置付款顺序</Button></Col></Row>}>

                    </Item>
                </List> */}
                <WhiteSpace size="lg" />
                <List className="buttom">
                    <Item arrow="horizontal" onClick={() => { }}>设置付款顺序</Item>
                    {/* <Item extra={<Row gutter={20}><Col span={12}><Button type='primary' onClick={() => {}}>充值</Button></Col><Col span={12}><Button type='primary' onClick={() => {}}>提现</Button></Col></Row>}> */}
                    {/* <Row type='flex' justify='center'>￥{0}</Row> */}
                    {/* </Item> */}
                </List>
            </div>
        );
    }
}

/**
 * 组件：我的账户详情视图控件
 * 控制我的账户详情视图控件
 */
@addon('MyAccountView', '我的账户详情视图控件', '控制我的账户详情视图控件')
@reactControl(MyAccountView, true)
export class MyAccountViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}