import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WhiteSpace, Result, Button } from 'antd-mobile';
// import { ROUTE_PATH } from "src/projects/app/util-tool";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { any } from "prop-types";
import { Icon } from "antd";
// import { Row, Col } from "antd";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
// const Item = List.Item;
/**
 * 组件：账户详情视图控件状态
 */
export interface AccountDetailViewState extends ReactViewState {
    /** 我的账户数据 */
    account_data?: any;
    /** 金额 */
    balance?: number;
    /** 账户类型 */
    account_type?: any;
    /** 账号id */
    user_id?: any;
}
/**
 * 组件：账户详情视图控件
 */
export class AccountDetailView extends ReactView<AccountDetailViewControl, AccountDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = { 
            account_data: any,

        };
    }
    componentDidMount() {
        // 通过id获取用户的详细信息
        console.info(this.props.match!.params.key);
        AppServiceUtility.my_account_service.get_account_detail!({ id: this.props.match!.params.key })!
            .then((data: any) => {
                console.info(data);
                this.setState({
                    account_data: data.result![0],
                    balance: data.result![0].balance ? data.result![0].balance : 0,
                    user_id: data.result![0].id,
                    account_type: data.result![0].account_type ? data.result![0].account_type : ""
                });
                // this.state.balance : this.state.account_data.balance;
            });
    }
    onClickRow = (id: string) => {
        this.props.history!.push(ROUTE_PATH.accountRecharge + '/' + id);
    }
    render() {
        return (
            <div style={{ height: document.body.clientHeight }}>
                <Result
                    img={<Icon type="pay-circle" theme="filled" style={{fontSize: '60px'}} />}
                    title={'￥' + this.state.balance}
                    message={this.state.account_type + '余额'}
                />
                {/* <List> */}
                    {
                        // this.state.account_data ? this.state.account_data.map((data: any, index: any) => {
                        //     return (
                        //         <Item key={index} onClick={this.onClickRow.bind(data.id)} arrow="horizontal" style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra={'￥' + data.balance}>{data.account_type}</Item>
                                
                        //         );
                        // }) : ''
                    }
                    {/* <Item extra={this.state.a} arrow="horizontal" onClick={() => { }}>头像</Item> */}
                    {/* 要传id 和 name */}
                    {/* <Item extra="zhangsan">账号</Item> */}
                    {/* <Item extra="张三" arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName); }}>账户名</Item>
                    <Item extra="zhangsan">账号</Item>
                    <Item arrow="horizontal" onClick={() => { }}>更多</Item> */}
                {/* </List> */}
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <Button type="primary" onClick={() => { this.onClickRow(this.state.user_id); }}>充值</Button><WhiteSpace />
                <Button onClick={() => { this.onClickRow(this.state.user_id); }}>提现</Button><WhiteSpace />
                <WhiteSpace size="lg" />

                {/* <List >
                    <Item extra={<Row gutter={20}><Col span={12}><Button type='primary' onClick={() => this.state.account_data}>设置付款顺序</Button></Col></Row>}>

                    </Item>
                </List> */}
                
                {/* <List  className="buttom">
                <Item arrow="horizontal" onClick={() => { }}>设置付款顺序</Item> */}
                    {/* <Item extra={<Row gutter={20}><Col span={12}><Button type='primary' onClick={() => {}}>充值</Button></Col><Col span={12}><Button type='primary' onClick={() => {}}>提现</Button></Col></Row>}> */}
                        {/* <Row type='flex' justify='center'>￥{0}</Row> */}
                    {/* </Item> */}
                {/* </List> */}
            </div>
        );
    }
}

/**
 * 组件：账户详情视图控件
 * 控制账户详情视图控件
 */
@addon('AccountDetailView', '账户详情视图控件', '控制账户详情视图控件')
@reactControl(AccountDetailView, true)
export class AccountDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}