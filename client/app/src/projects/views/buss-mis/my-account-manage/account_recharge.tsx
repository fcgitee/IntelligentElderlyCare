import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WhiteSpace, Button, InputItem, List } from 'antd-mobile';
import { ROUTE_PATH, PAGE_TYPE, encodeUrlParam } from "src/projects/app/util-tool";
import { any } from "prop-types";
import { getUuid } from "src/business/util_tool";

export interface ServiceItemPackageFormValues {
    id?: string;
}

/**
 * 组件：账户充值视图控件状态
 */
export interface AccountRechargeViewState extends ReactViewState {
    /** 我的账户数据 */
    account_data?: any;
    /** 充值金额 */
    money?: number;
    /** 账户类型 */
    account_type?: any;
    /** type */
    type?: any;
    /** e */
    e?: any;
}
/**
 * 组件：账户充值视图控件
 */
export class AccountRechargeView extends ReactView<AccountRechargeViewControl, AccountRechargeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            account_data: any,
            type: 'money',
        };
    }
    // onClickRow = (id: string) => {
    //     this.props.history!.push(ROUTE_PATH.accountRecharge + '/' + id);
    // }

    handleSubmit = (e: any) => {
        let buy_list_squence = '';
        let buy_list = {
            page_type: PAGE_TYPE.CHARGE,
            page_info: this.state.money,
            abstract: '充值',
            remarks: "充值",
            out_trade_no: getUuid(),
            product_desc: "充值",
            // 附加信息（可选），微信异步通知会原样返回
            attach: "充值"
        };
        buy_list_squence = JSON.stringify(buy_list);

        this.props.history!.push(ROUTE_PATH.payment + '/' + encodeUrlParam(buy_list_squence));
    }

    handlelecard = (e: string) => {
        this.setState({
            money: parseFloat(e)
        });
    }

    render() {
        return (
            <div style={{ height: document.body.clientHeight }}>
                <InputItem type="number" onChange={this.handlelecard} placeholder="请输入充值金额" >充值金额￥</InputItem>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <List.Item >
                    <Button type="primary" onClick={this.handleSubmit}>充值</Button>
                </List.Item>
                <WhiteSpace size="lg" />
            </div>
        );
    }
}

/**
 * 组件：账户充值视图控件
 * 控制账户充值视图控件
 */
@addon('AccountRechargeView', '账户充值视图控件', '控制账户充值视图控件')
@reactControl(AccountRechargeView, true)
export class AccountRechargeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}