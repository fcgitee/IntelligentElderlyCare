import React from "react";
import { reactControl, BaseReactElementControl, ReactView, ReactViewState } from "pao-aop-client";
import { addon } from "pao-aop";
import { Flex, WhiteSpace, Picker, List, DatePicker } from "antd-mobile";
import { Card, Row } from "antd";
import CardBody from "antd-mobile/lib/card/CardBody";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：查看账单状态
 */
export interface SeeBileViewState extends ReactViewState {
    /** 数据 */
    data?: any;
    /** 账户类型 */
    accountType?: Array<string>;
    /** 开始时间 */
    startime?: Date;
    /** 结束时间 */
    endtime?: Date;
}

/**
 * 组件：查看账单
 * 描述
 */
export class SeeBileView extends ReactView<SeeBileViewControl, SeeBileViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            accountType: [],
            startime: new Date(),
            endtime: new Date()
        };
    }

    componentDidMount() {
        AppServiceUtility.financial_service.billing_record!({}, 1, 10)!
            .then((res: any) => {
                this.setState({
                    data: res.result
                });
            });
    }

    handleinfo(id: number | string) {
        this.props.history!.push(ROUTE_PATH.seeBillDetails + '/' + id);
    }

    handletype(e: any) {
        this.setState({ accountType: e }, () => {
            if (this.state.accountType) {
                if (this.state.startime || this.state.endtime) {
                    if (this.state.endtime) {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    } else {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    }
                } else {
                    AppServiceUtility.financial_service.billing_record!({ account_type: this.state.accountType[0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            } else if (this.state.startime || this.state.endtime) {
                if (this.state.endtime) {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                } else {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            }
        });

    }

    startime(e: any) {
        // console.log(e);
        this.setState({ startime: e }, () => {
            if (this.state.accountType) {
                if (this.state.startime || this.state.endtime) {
                    if (this.state.endtime) {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    } else {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    }
                } else {
                    AppServiceUtility.financial_service.billing_record!({ account_type: this.state.accountType[0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            } else if (this.state.startime || this.state.endtime) {
                if (this.state.endtime) {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                } else {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            }
        });
    }

    endtime(e: any) {
        // console.log(e);
        this.setState({ endtime: e }, () => {
            if (this.state.accountType) {
                if (this.state.startime || this.state.endtime) {
                    if (this.state.endtime) {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    } else {
                        AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType[0] })!
                            .then((res: any) => {
                                this.setState({
                                    data: res.result
                                });
                            });
                    }
                } else {
                    AppServiceUtility.financial_service.billing_record!({ account_type: this.state.accountType[0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            } else if (this.state.startime || this.state.endtime) {
                if (this.state.endtime) {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), end_date: this.formatDate(this.state.endtime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                } else {
                    AppServiceUtility.financial_service.billing_record!({ start_date: this.formatDate(this.state.startime), account_type: this.state.accountType![0] })!
                        .then((res: any) => {
                            this.setState({
                                data: res.result
                            });
                        });
                }
            }
        });
    }

    formatTen(num: number) {
        return num > 9 ? (num + "") : ("0" + num);
    }
    formatDate(date: any) {
        let data = new Date(date);
        let year = data.getFullYear();
        let month = data.getMonth() + 1;
        let day = data.getDate();
        let hour = data.getHours();
        let minute = data.getMinutes();
        let second = data.getSeconds();
        return year + "-" + this.formatTen(month) + "-" + this.formatTen(day) + " " + this.formatTen(hour) + ":" + this.formatTen(minute) + ":" + this.formatTen(second);
    }

    render() {
        let district = [];
        district.push(
            {
                value: 'APP储值账户',
                label: 'APP储值账户'
            },
            {
                value: '机构储值账户',
                label: '机构储值账户'
            },
            {
                value: '补贴账户',
                label: '补贴账户'
            },
            {
                value: '慈善账户',
                label: '慈善账户'
            },
            {
                value: '真实账户',
                label: '真实账户'
            }
        );
        return (
            <div>
                <Picker data={district} title="请选择账户类型" value={this.state.accountType} cols={1} onOk={(e: any) => { this.handletype(e); }} >
                    <List.Item arrow="horizontal">请选择账户类型</List.Item>
                </Picker>
                <DatePicker onChange={(e) => { this.startime(e); }} value={this.state.startime} maxDate={new Date()} minDate={new Date(1970, 0, 1)}>
                    <List.Item arrow="horizontal">开始时间</List.Item>
                </DatePicker>
                <DatePicker onChange={(e) => { this.endtime(e); }} value={this.state.endtime} maxDate={new Date()} minDate={new Date(1970, 0, 1)}>
                    <List.Item arrow="horizontal">结束时间</List.Item>
                </DatePicker>
                <WhiteSpace size="lg" />
                {
                    this.state.data && this.state.data.length > 0 ?
                        this.state.data.map((data: any, index: number) => {
                            return (
                                <div key={index}>
                                    <Card key={index} onClick={() => { this.handleinfo(data.id); }}>
                                        <CardBody>
                                            <Flex>
                                                <Flex.Item>付款时间</Flex.Item>
                                                <Flex.Item>{data.date}</Flex.Item>
                                            </Flex>
                                            <Flex>
                                                <Flex.Item>付款金额</Flex.Item>
                                                <Flex.Item><strong>¥{data.amount}</strong></Flex.Item>
                                            </Flex>
                                            <Flex>
                                                <Flex.Item>付款账户</Flex.Item>
                                                <Flex.Item>{data.abstract}</Flex.Item>
                                            </Flex>
                                        </CardBody>
                                    </Card>
                                    <WhiteSpace size="lg" />
                                </div>
                            );
                        }) : <Row className='tabs-content' type='flex' justify='center'>暂无数据</Row>
                }
            </div>
        );
    }
}

/**
 * 控件：查看账单控制器
 * 描述
 */
@addon('SeeBileView', '查看账单', '查看账单')
@reactControl(SeeBileView, true)
export class SeeBileViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}