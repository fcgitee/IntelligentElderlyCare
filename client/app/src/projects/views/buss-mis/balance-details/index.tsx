import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { WhiteSpace, List } from "antd-mobile";
const Item = List.Item;
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
/**
 * 组件：账户余额详情状态
 */
export class BalanceDetailsSeeBileViewState implements BaseReactElementState {
}

/**
 * 组件：账户余额详情
 * 描述
 */
export class BalanceDetailsView extends BaseReactElement<BalanceDetailsViewControl, BalanceDetailsSeeBileViewState> {

    componentDidMount() {
        // 通过id获取用户的详细信息
        request(this, AppServiceUtility.my_account_service.get_my_account!())
            .then((data: any) => {
                // console.log("基金账户", data);
                this.setState({
                    account_data: data
                });
            });
    }

    render() {
        return (
            <div>
                <WhiteSpace size="lg" />
                <List className="myList">
                    <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra='￥888' arrow="horizontal">养老账户</Item>
                    <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra='￥888' arrow="horizontal">基金账户</Item>
                    <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} extra='￥888' arrow="horizontal">现金</Item>
                </List>
            </div>
        );
    }
}

/**
 * 控件：账户余额详情控制器
 * 描述
 */
@addon('BalanceDetailsView', '账户余额详情', '账户余额详情')
@reactControl(BalanceDetailsView, true)
export class BalanceDetailsViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}