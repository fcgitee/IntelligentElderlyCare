import React from "react";
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { List, TextareaItem, WhiteSpace, Button } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
const Item = List.Item;
/**
 * 组件：请假信息详情状态
 */
export interface LeaveDetailViewState extends ReactViewState {
    /** 数据 */
    data:{};
    /*传过来的id */
    id?:any;
}

/**
 * 组件：请假信息详情
 * 描述
 */
export class LeaveDetailView extends ReactView<LeaveDetailViewControl, LeaveDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            id:'',
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        // console.log(id);
        this.setState({
            id
        });
        request(this, AppServiceUtility.leaveRecord_service.get_leave_list!({ 'id': id },1, 1))
            .then((data: any) => {
                // console.log('datas.result', data.result);
                // data.result[0].checkout_date = data.result[0].checkout_date[0] + "-" + data.result[0].checkout_date[1] + "-" + data.result[0].checkout_date[2];
                // data.result[0].apply_date = data.result[0].apply_date[0] + "-" + data.result[0].apply_date[1] + "-" + data.result[0].apply_date[2];
                if (data) {
                    this.setState({
                        data: data.result[0], 
                    });
                }
            });
    }
    goBackInsert = (id:string) => {
        this.props.history!.push(ROUTE_PATH.backInsert + '/' + id);
    }
    render() {
        return (
            <div>
                <WhiteSpace size="lg" />
                <List>
                    <Item extra={this.state.data['elder_name']}>姓名</Item>
                </List>
                <List>
                    <Item extra={this.state.data['status']}>状态</Item>
                </List>
                <List>
                    <Item extra={this.state.data['record']}>记录原因</Item>
                </List>
                <List>
                    <Item extra={this.state.data['apply_date']}>申请时间</Item>
                </List>
                <List renderHeader={() => '记录说明'}>
                    <TextareaItem
                      value={this.state.data['record']}
                      rows={5}
                      disabled={true}
                    />
                </List>
                <List>
                    <Item extra={this.state.data['bed_id']}>床位编号</Item>
                </List>
                <List>
                    <Item extra={this.state.data['checkout_date']}>离开时间</Item>
                </List>
                <List>
                    <Item extra={this.state.data['pickout_name']}>接出人</Item>
                </List>
                <List>
                    <Item extra={this.state.data['operating_name']}>操作人员</Item>
                </List>
                <List.Item >
                        <Button onClick={() => this.goBackInsert(this.state.id)} type="primary">销假</Button>
                </List.Item>
            </div>
        );
    }
}
/**
 * 控件：请假信息详情控制器
 * 描述
 */
@addon('LeaveDetailView', '请假信息详情', '请假信息详情')
@reactControl(LeaveDetailView, true)
export class LeaveDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}