import React from "react";
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { List, Picker, DatePicker, WhiteSpace, Button, Toast, InputItem } from 'antd-mobile';
import { Form } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：销假状态
 */
export interface BackInsertViewState extends ReactViewState {
    bedListValue?: any[];
    personListValue?: any[];
    personListValue2?: any[];
    /** 请假日期 */
    date: any;
    /*床位编号 */
    bed_list?: any[];
    /*送入人列表 */
    person_list?: any[];
    /*传过来的id */
    id?: any;
    /*床位编号id */
    bed_list_value?: any;
    /*床位编号显示文字 */
    bed_list_show_value?: any;
    /*送入人id */
    person_list_value?: any;
    /*送入人人显示文字 */
    person_list_show_value?: any;
    /*操作人id */
    operationPerson_list_value?: any;
    /*操作人显示文字 */
    operationPerson_list_show_value?: any;
    height?: any;
    status?: any;
}

/**
 * 组件：销假
 * 描述
 */
export class BackInsertView extends ReactView<BackInsertViewControl, BackInsertViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            bedListValue: [],
            personListValue: [],
            personListValue2: [],
            date: '',
            bed_list: [],
            person_list: [],
            id: '',
            bed_list_value: '',
            bed_list_show_value: '',
            person_list_value: '',
            person_list_show_value: '',
            operationPerson_list_value: '',
            operationPerson_list_show_value: '',
            height: document.body.clientHeight,
            status: '通过',
        };
    }
    componentDidMount() {
        // 查询床位列表
        let id = this.props.match!.params.key;
        // console.log(id);
        this.setState({
            id
        });
        request(this, AppServiceUtility.leaveRecord_service.get_leave_list!({ 'id': id }, 1, 999))
            .then((datas: any) => {
                this.setState(
                    { bed_list_value: datas.result[0].bed_id }
                );
            }
            );
        request(this, AppServiceUtility.person_org_manage_service.get_personnel_elder!({}))
            .then((datas: any) => {
                let person_list: any[] = [];
                if (datas) {
                    person_list = datas.result.map((key: any, value: any) => {
                        let list = {};
                        list = { label: key['name'], value: key['id'] };
                        return list;
                    });
                    this.setState(
                        { person_list }
                    );
                }
            });
    }
    /*送入人 */
    ChangePerson = (e: any) => {
        this.setState({
            person_list_value: e[0],
            person_list_show_value: e,
        });
        // // console.log(this.state.person_list_show_value);
    }
    /*操作人 */
    ChangeOperationPerson = (e: any) => {
        this.setState({
            operationPerson_list_value: e[0],
            operationPerson_list_show_value: e,
        });
        // // console.log(this.state.person_list_show_value);
    }
    applyCreate = () => {
        if (!this.state.date) {
            Toast.fail('请选择入住时间', 1);
            return false;
        }
        if (!this.state.person_list_value) {
            Toast.fail('请选择送入人', 1);
            return false;
        }
        if (!this.state.operationPerson_list_value) {
            Toast.fail('请选择操作人员', 1);
            return false;
        }
        var param: any = {
            id: this.state.id,
            bed_id: this.state.bed_list_value,
            checkin_date: this.state.date,
            pickout_id: this.state.person_list_value,
            operating_personnel_id2: this.state.operationPerson_list_value,
            status: this.state.status,
        };
        AppServiceUtility.leaveRecord_service.update_leave_record!(param)!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.hide();
                    Toast.success('申请成功', 3);
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.leaveRecord);
                        },
                        1000
                    );
                } else {
                    Toast.fail('操作失败，请重试', 3);
                }
            });
        // console.log(param);
        return false;
    }
    render() {
        return (
            <div style={{ height: this.state.height }}>
                <Form>
                    <List renderHeader={() => '入住信息'}>
                        <InputItem value={this.state.bed_list_value} editable={false}>床位编号</InputItem>
                        <DatePicker
                            mode="date"
                            title="生日"
                            extra="请选择入住时间"
                            value={this.state.date}
                            onChange={date => this.setState({ date })}
                        >
                            <List.Item arrow="horizontal">入住时间</List.Item>
                        </DatePicker>
                        <Picker
                            data={this.state.person_list ? this.state.person_list : []}
                            value={this.state.person_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangePerson(e)}
                        >
                            <List.Item arrow="horizontal">送入人</List.Item>
                        </Picker>
                        <Picker
                            data={this.state.person_list ? this.state.person_list : []}
                            value={this.state.operationPerson_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangeOperationPerson(e)}
                        >
                            <List.Item arrow="horizontal">操作人员</List.Item>
                        </Picker>
                    </List>
                    <WhiteSpace />
                    <List.Item >
                        <Button type="primary" onClick={this.applyCreate}>提交</Button>
                    </List.Item>
                </Form>
            </div>
        );
    }
}
/**
 * 控件：销假控制器
 * 描述
 */
@addon('BackInsertView', '销假', '销假')
@reactControl(BackInsertView, true)
export class BackInsertViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}