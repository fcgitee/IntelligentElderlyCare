import React from "react";
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Button, WhiteSpace } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：请假销假主页状态
 */
export interface LeaveRecordViewState extends ReactViewState {
    height?:any;
}

/**
 * 组件：请假销假主页
 * 描述
 */
export class LeaveRecordView extends ReactView<LeaveRecordViewControl, LeaveRecordViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        height:document.body.clientHeight,
        };
    }
    goLeave = () => {
        this.props.history!.push(ROUTE_PATH.leaveInsert);
    }
    goLeaveList = () => {
        this.props.history!.push(ROUTE_PATH.leaveList);
    }
    render() {
        return (
            <div style={{height:this.state.height}}>
                <Button onClick={this.goLeave} style={{ margin: '150px 20px 0px 20px', height:'100px', textAlign:'center',lineHeight:'100px '}}>请假</Button><WhiteSpace />
                <Button onClick={this.goLeaveList} type="primary" style={{ margin: '10px 20px', height:'100px', textAlign:'center',lineHeight:'100px '}}>销假</Button><WhiteSpace />
            </div>
        );
    }
}
/**
 * 控件：请假销假主页控制器
 * 描述
 */
@addon('LeaveRecordView', '请假销假主页', '请假销假主页')
@reactControl(LeaveRecordView, true)
export class LeaveRecordViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}