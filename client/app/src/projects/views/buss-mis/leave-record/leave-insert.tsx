import React from "react";
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { List, Picker, DatePicker, TextareaItem, WhiteSpace, Button, Toast, InputItem } from 'antd-mobile';
import { Form } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：请假状态
 */
export interface LeaveInsertViewState extends ReactViewState {
    reason_list?: any;
    person_list?: any;
    bed_list?: any;
    /*申请人id */
    applyperson_list_value?: any;
    /*申请人人显示文字 */
    applyperson_list_show_value?: any;
    /*记录理由id */
    reason_list_value?: any;
    /*记录理由显示文字 */
    reason_list_show_value?: any;
    /*床位编号id */
    bed_list_value?: any;
    /*床位编号显示文字 */
    bed_list_show_value?: any;
    /*接出人id */
    person_list_value?: any;
    /*接出人显示文字 */
    person_list_show_value?: any;
    /*操作人id */
    operationPerson_list_value?: any;
    /*操作人显示文字 */
    operationPerson_list_show_value?: any;
    /*申请时间 */
    apply_date?: any;
    /*记录原因 */
    record?: any;
    /*离开时间 */
    checkout_date?: any;
    elder_list?: any;
}

/**
 * 组件：请假
 * 描述
 */
export class LeaveInsertView extends ReactView<LeaveInsertViewControl, LeaveInsertViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            reason_list: [],
            person_list: [],
            elder_list: [],
            reason_list_value: '',
            reason_list_show_value: '',
            bed_list_value: '',
            bed_list_show_value: '',
            person_list_value: '',
            person_list_show_value: '',
            applyperson_list_value: '',
            applyperson_list_show_value: '',
            operationPerson_list_value: '',
            operationPerson_list_show_value: '',
            apply_date: '',
            record: '',
            checkout_date: '',
        };
    }
    componentDidMount() {
        AppServiceUtility.leaveRecord_service.get_leave_reason_list!({}, 1, 999)!
            .then((datas: any) => {
                let reason_list: any[] = [];
                if (datas) {
                    reason_list = datas.result.map((key: any, value: any) => {
                        let list = {};
                        list = { label: key['name'], value: key['id'] };
                        return list;
                    });
                    // // console.log(reason_list);
                    this.setState(
                        { reason_list }
                    );
                }
            });
        // AppServiceUtility.hotel_zone_type_service.get_bed_list!({}, 1, 999)!
        //     .then((datas: any) => {
        //         let bed_list: any[] = [];
        //         if (datas) {
        //             bed_list = datas.result.map((key: any, value: any) => {
        //                 let list = {};
        //                 list = {label:key['bed_code'],value:key['id']};
        //                 return list;
        //             });
        //             // // console.log(bed_list);
        //             this.setState(
        //                 { bed_list }
        //             );}
        //     });
        AppServiceUtility.person_org_manage_service.get_personnel_elder!({})!
            .then((datas: any) => {
                // // console.log("旧的",datas);
                let person_list: any[] = [];
                if (datas) {
                    person_list = datas.result.map((key: any, value: any) => {
                        let list = {};
                        list = { label: key['name'], value: key['id'] };
                        return list;
                    });
                    // console.log(person_list);
                    this.setState(
                        { person_list }
                    );
                }
            });
        AppServiceUtility.check_in_service.get_check_in_list!({}, 1, 999)!
            .then((datas: any) => {
                // // console.log("入住用户",datas);
                let elder_list: any[] = [];
                if (datas) {
                    elder_list = datas.result.map((key: any, value: any) => {
                        let list = {};
                        list = { label: key['name'], value: key['user_id'] };
                        return list;
                    });
                    // console.log(elder_list);
                    this.setState({
                        elder_list
                    });
                }
            });
    }
    /*申请人*/
    ChangeApplyPerson = (e: any) => {
        this.setState({
            applyperson_list_value: e[0],
            applyperson_list_show_value: e,
        });
        let user_id = e[0];
        AppServiceUtility.check_in_service.get_check_in_list!({ user_id }, 1, 999)!
            .then((datas: any) => {
                // console.log("拿床位", datas);
                this.setState({
                    bed_list_value: datas.result[0].bed_number
                });
            });
        // console.log(this.state.applyperson_list_show_value);
    }
    /*申请原因 */
    changeRecord = (e: any) => {
        // console.log(e);
        this.setState({
            record: e,
        });
    }
    /*记录原因 */
    ChangeReason = (e: any) => {
        this.setState({
            reason_list_value: e[0],
            reason_list_show_value: e,
        });
    }
    /*床位编号 */
    ChangeBed = (e: any) => {
        this.setState({
            bed_list_value: e[0],
            bed_list_show_value: e,
        });
        // // console.log(this.state.bed_list_show_value);
    }
    /*接出人 */
    ChangePerson = (e: any) => {
        this.setState({
            person_list_value: e[0],
            person_list_show_value: e,
        });
        // // console.log(this.state.person_list_show_value);
    }
    /*操作人 */
    ChangeOperationPerson = (e: any) => {
        this.setState({
            operationPerson_list_value: e[0],
            operationPerson_list_show_value: e,
        });
        // // console.log(this.state.person_list_show_value);
    }
    applyCreate = () => {
        if (!this.state.applyperson_list_value) {
            Toast.fail('请选择申请人', 1);
            return false;
        }
        if (!this.state.reason_list_value) {
            Toast.fail('请选择记录原因', 1);
            return false;
        }
        if (!this.state.apply_date) {
            Toast.fail('请选择申请时间', 1);
            return false;
        }
        if (!this.state.record) {
            Toast.fail('请填写记录原因', 1);
            return false;
        }
        if (!this.state.checkout_date) {
            Toast.fail('请选择离开时间', 1);
            return false;
        }
        if (!this.state.person_list_value) {
            Toast.fail('请选择接出人', 1);
            return false;
        }
        if (!this.state.operationPerson_list_value) {
            Toast.fail('请选择操作人', 1);
            return false;
        }
        var param: any = {
            residents_id: this.state.applyperson_list_value,
            record_reason_id: this.state.reason_list_value,
            apply_date: this.state.apply_date,
            record: this.state.record,
            bed_id: this.state.bed_list_value,
            checkout_date: this.state.checkout_date,
            pickout_id: this.state.person_list_value,
            operating_personnel_id2: this.state.operationPerson_list_value,
        };
        AppServiceUtility.leaveRecord_service.update_leave_record!(param)!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.hide();
                    Toast.success('申请成功', 3);
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.leaveRecord);
                        },
                        1000
                    );
                } else {
                    Toast.fail('已有请假记录未消假', 3);
                }
            });
        // console.log(param);
        return false;
    }
    render() {
        return (
            <div>
                <Form>
                    <List renderHeader={() => '长者信息'}>
                        <Picker
                            data={this.state.elder_list ? this.state.elder_list : []}
                            value={this.state.applyperson_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangeApplyPerson(e)}
                        >
                            <List.Item arrow="horizontal">申请人</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => '记录登记'}>
                        <Picker
                            data={this.state.reason_list ? this.state.reason_list : []}
                            value={this.state.reason_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangeReason(e)}
                        >
                            <List.Item arrow="horizontal">记录原因</List.Item>
                        </Picker>
                        <DatePicker
                            mode="date"
                            title="申请时间"
                            extra="请选择申请时间"
                            value={this.state.apply_date}
                            onChange={apply_date => this.setState({ apply_date })}
                        >
                            <List.Item arrow="horizontal">申请时间</List.Item>
                        </DatePicker>
                        <TextareaItem
                            title="记录原因"
                            placeholder="输入原因"
                            rows={3}
                            value={this.state.record}
                            onChange={(e) => this.changeRecord(e)}
                        />
                    </List>
                    <List renderHeader={() => '离开信息'}>
                        {/* <Picker
                            data={this.state.bed_list ? this.state.bed_list : []}
                            value={this.state.bed_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangeBed(e)}
                        >
                        <List.Item arrow="horizontal">床位编号</List.Item>
                        </Picker> */}
                        <InputItem value={this.state.bed_list_value} editable={false}>床位编号</InputItem>
                        <DatePicker
                            mode="date"
                            title="离开时间"
                            extra="请选择离开时间"
                            value={this.state.checkout_date}
                            onChange={checkout_date => this.setState({ checkout_date })}
                        >
                            <List.Item arrow="horizontal">离开时间</List.Item>
                        </DatePicker>
                        <Picker
                            data={this.state.person_list ? this.state.person_list : []}
                            value={this.state.person_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangePerson(e)}
                        >
                            <List.Item arrow="horizontal">接出人</List.Item>
                        </Picker>
                        <Picker
                            data={this.state.person_list ? this.state.person_list : []}
                            value={this.state.operationPerson_list_show_value}
                            cols={1}
                            onChange={(e) => this.ChangeOperationPerson(e)}
                        >
                            <List.Item arrow="horizontal">操作人员</List.Item>
                        </Picker>
                    </List>
                    <WhiteSpace />
                    <List.Item >
                        <Button type="primary" onClick={this.applyCreate}>提交申请</Button>
                    </List.Item>
                </Form>
            </div>
        );
    }
}
/**
 * 控件：请假控制器
 * 描述
 */
@addon('LeaveInsertView', '请假', '请假')
@reactControl(LeaveInsertView, true)
export class LeaveInsertViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}