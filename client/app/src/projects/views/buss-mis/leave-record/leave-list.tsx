import React from "react";
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { ListView,Card, WingBlank, WhiteSpace } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row } from "antd";
/**
 * 组件：请假列表状态
 */
export interface LeaveListViewState extends ReactViewState {
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    /** 记录tab状态 */
    tab_status?: string;
    /*后台返回的列表数据*/
    recordDatas:{};
}

/**
 * 组件：请假列表
 * 描述
 */
export class LeaveListView extends ReactView<LeaveListViewControl, LeaveListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'participate',
            /*后台返回的列表数据*/
            recordDatas:{}
        };
    }
    componentDidMount() {
        AppServiceUtility.leaveRecord_service.get_leave_list!({}, 1, 10)!
            .then((data: any) => {
                // console.log(data);
                let list: any[] = [];
                if (data) {
                    list = data.result.map((key: any, value: any) => {
                        let recordDatas = {};
                        recordDatas['id'] = key['id'];
                        recordDatas['elder_name'] = key['elder_name'];
                        recordDatas['record'] = key['record'];
                        recordDatas['checkout_date'] = key['checkout_date'];
                        recordDatas['operating_name'] = key['operating_name'];
                        recordDatas['pickout_name'] = key['pickout_name'];
                        return recordDatas;
                    });
                    // console.log(list);
                    this.setState(
                        { list }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    onClickRow = (id: string) => {
        console.info();
        this.props.history!.push(ROUTE_PATH.leaveDetail + '/' + id);
    }
    render() {
        const { list, dataSource, } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <WingBlank size="lg">
                    <WhiteSpace size="lg" />
                        <Card style={{backgroundColor:'#9e9e9e30'}} onClick={this.onClickRow.bind(this, owData.id)}>
                          <Card.Header
                            title={owData.elder_name}
                            extra={<span>{owData.checkout_date}</span>}
                          />
                          <Card.Body>
                            <div>{owData.record}</div>
                          </Card.Body>
                          <Card.Footer content={"接出人:" + owData.pickout_name} extra={<div>{"操作人:" + owData.operating_name}</div>} />
                        </Card>
                    <WhiteSpace size="lg" />
                </WingBlank>
            );
        };
        return (
            <Row>
                <div className='tabs-content'>
                    {
                        <ListView
                            ref={el => this['lv'] = el}
                            dataSource={dataSource.cloneWithRows(list)}
                            renderRow={renderRow}
                            initialListSize={10}
                            pageSize={10}
                            style={{ height: this.state.height }}
                        />   
                    }
                </div>
            </Row>
        );
    }
}
/**
 * 控件：请假列表控制器
 * 描述
 */
@addon('LeaveListView', '请假列表', '请假列表')
@reactControl(LeaveListView, true)
export class LeaveListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}