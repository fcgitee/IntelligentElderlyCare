import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, WhiteSpace, Picker, TextareaItem, Button, Toast, Modal } from 'antd-mobile';
import { Form } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
// import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";

const alert = Modal.alert;

/**
 * 组件：服务人员申请页面视图控件状态
 */
export interface SerVicePersonalapplyViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 服务人员能力级别 */
    servicer_ability_level?: any;
    abilityValue?: any;
    /** 服务人员信用级别 */
    servicer_credit_level?: any[];
    creditValue?: any;
    /** 资质(资格证)照片 */
    servicer_qualifications_photos?: any;
    /**  */
    servicer_contract_photos?: any;
    servicer_name?: any;
    servicer_id_card?: any;
    servicer_phone?: any;
    servicer_email?: any;
    servicer_address?: any;
    servicer_contract_clauses?: any;
    servicer_remark?: any;
    organization_list?: any;
    servicer_organization_id?:any;
}
/**
 * 组件：服务人员申请页面视图控件
 */
export class SerVicePersonalapplyView extends ReactView<SerVicePersonalapplyViewControl, SerVicePersonalapplyViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            servicer_ability_level: [{ label: '一级', value: '一级' }, { label: '二级', value: '二级' }, { label: '三级', value: '三级' },],
            servicer_credit_level: [{ label: '一级', value: '一级' }, { label: '二级', value: '二级' }, { label: '三级', value: '三级' },],
            abilityValue: '',
            creditValue: '',
            servicer_qualifications_photos: '',
            servicer_contract_photos: '',
            servicer_name: '',
            servicer_id_card: '',
            servicer_phone: '',
            servicer_email: '',
            servicer_address: '',
            servicer_contract_clauses: '',
            servicer_remark: '',
            organization_list: [],
            servicer_organization_id:''
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_list_all!({ personnel_type: '2' })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let list:any = [];
                    data.result.map((item: any) => {
                        list.push({ label: item.name, value: item.id });
                    });
                    this.setState({
                        organization_list: list
                    });
                }
            });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { abilityValue, creditValue, servicer_name, servicer_id_card, servicer_phone, servicer_email, servicer_address, servicer_contract_clauses, servicer_remark, servicer_qualifications_photos, servicer_contract_photos, servicer_organization_id } = this.state;
        if (!servicer_name) {
            Toast.fail('请输入你的姓名', 1);
            return false;
        }
        if (!servicer_id_card) {
            Toast.fail('请输入你的身份证', 1);
            return false;
        }
        if (!servicer_phone) {
            Toast.fail('请输入你的手机号码', 1);
            return false;
        }
        if (!servicer_email) {
            Toast.fail('请输入你的邮箱', 1);
            return false;
        }
        if (!servicer_address) {
            Toast.fail('请输入你的家庭住址', 1);
            return false;
        }
        if (!servicer_organization_id) {
            Toast.fail('请选择所属组织机构', 1);
            return false;
        }
        if (!abilityValue) {
            Toast.fail('请选择服务人员能力级别', 1);
            return false;
        }
        if (!creditValue) {
            Toast.fail('请选择服务人员信用级别', 1);
            return false;
        }
        if (!servicer_qualifications_photos) {
            Toast.fail('请上传资质(资格证)照片', 1);
            return false;
        }
        if (!servicer_contract_photos) {
            Toast.fail('请上传合同照片', 1);
            return false;
        }
        if (!servicer_contract_clauses) {
            Toast.fail('请输入服务机构合同条款', 1);
            return false;
        }
        if (!servicer_remark) {
            Toast.fail('请输入备注', 1);
            return false;
        }
        var values = {
            abilityValue: abilityValue[0],
            creditValue: creditValue[0],
            servicer_name,
            servicer_id_card,
            servicer_phone,
            servicer_email,
            servicer_address,
            servicer_contract_clauses,
            servicer_remark,
            servicer_qualifications_photos,
            servicer_contract_photos,
            servicer_organization_id: servicer_organization_id[0]
        };
        // console.log(values);
        AppServiceUtility.service_operation_service.update_service_personal_apply!(values)!
            .then((data: any) => {
                if (data === '该用户已经申请过了，不可重复申请') {
                    alert('提示', '该用户已经申请过了，不可重复申请', [
                        { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                    ]);
                }
                if (data === '该用户不存在，请先注册') {
                    alert('提示', '该用户不存在，请先注册', [
                        { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                    ]);
                }
                if (data === 'Success') {
                    alert('成功', '申请成功，请耐心等待审核结果', [
                        { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                    ]);
                }
                // console.log(data);
            })
            .catch(err => {
                console.info(err);
            });
        return false;
    }
    // 姓名
    nameChange = (value: any) => {
        this.setState({
            servicer_name: value
        });
    }

    // 身份证
    idCardChange = (value: any) => {
        this.setState({
            servicer_id_card: value
        });
    }

    // 手机号码
    phoneChange = (value: any) => {
        this.setState({
            servicer_phone: value
        });
    }

    // e-mail
    emailChange = (value: any) => {
        this.setState({
            servicer_email: value
        });
    }

    // 地址
    addressChange = (value: any) => {
        this.setState({
            servicer_address: value
        });
    }
    // 组织机构
    organizationChange = (value: any) => {
        this.setState({
            servicer_organization_id: value
        });
    }

    // 能力级别
    abilityChange = (e: any) => {
        // // console.log(e);
        this.setState({
            abilityValue: e
        });
    }

    // 信用级别

    creditChange = (e: any) => {
        this.setState({
            creditValue: e
        });
    }
    // 资质(资格证)照片
    qualificationsChange = (e: any) => {
        // console.log(e);
        this.setState({
            servicer_qualifications_photos: e
        });
    }

    // 合同照片
    contractChange = (e: any) => {
        // console.log(e);
        this.setState({
            servicer_contract_photos: e
        });
    }

    // 条款
    clausesChange = (value: any) => {
        this.setState({
            servicer_contract_clauses: value
        });
    }

    // 备注
    remarkChange = (value: any) => {
        this.setState({
            servicer_remark: value
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { abilityValue, creditValue, servicer_name, servicer_id_card, servicer_phone, servicer_email, servicer_address, servicer_contract_clauses, servicer_remark, organization_list, servicer_organization_id } = this.state;
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <WhiteSpace />
                    <List renderHeader={() => '申请人信息'}>
                        <InputItem {...getFieldDecorator('servicer_name')} clear={true} placeholder="请填写你的姓名" onChange={this.nameChange} value={servicer_name}> 姓名 </InputItem>
                        <InputItem {...getFieldDecorator('servicer_id_card')} clear={true} placeholder="请填写你的身份证" onChange={this.idCardChange} value={servicer_id_card}> 身份证 </InputItem>
                        <InputItem {...getFieldDecorator('servicer_phone')} clear={true} placeholder="请填写你的手机号码" onChange={this.phoneChange} value={servicer_phone}> 手机号码 </InputItem>
                        <InputItem {...getFieldDecorator('servicer_email')} clear={true} placeholder="请填写你的邮箱" onChange={this.emailChange} value={servicer_email}> 邮箱 </InputItem>
                        <InputItem {...getFieldDecorator('servicer_address')} clear={true} placeholder="请填写你的家庭住址" onChange={this.addressChange} value={servicer_address}> 家庭住址 </InputItem>
                    </List>
                    <List renderHeader={() => '所属组织机构'}>
                        <Picker
                            {...getFieldDecorator('servicer_organization_id')}
                            data={organization_list ? organization_list : []}
                            cols={1}
                            onChange={this.organizationChange}
                            value={servicer_organization_id}
                        >
                            <List.Item arrow="horizontal">所属组织机构</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => '资质相关资料'}>
                        <Picker
                            {...getFieldDecorator('servicer_ability_level')}
                            data={this.state.servicer_ability_level ? this.state.servicer_ability_level : []}
                            cols={1}
                            onChange={this.abilityChange}
                            value={abilityValue}
                        >
                            <List.Item arrow="horizontal">服务人员能力级别</List.Item>
                        </Picker>
                        <Picker
                            {...getFieldDecorator('servicer_credit_level')}
                            data={this.state.servicer_credit_level ? this.state.servicer_credit_level : []}
                            cols={1}
                            onChange={this.creditChange}
                            value={creditValue}
                        >
                            <List.Item arrow="horizontal">服务人员信用级别</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => '资质(资格证)照片（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('servicer_qualifications_photos', {
                            initialValue: this.state.servicer_qualifications_photos,
                            rules: [{
                                required: false,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.qualificationsChange} />
                        )}
                    </List>
                    <List renderHeader={() => '合同照片（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('servicer_contract_photos', {
                            initialValue: this.state.servicer_contract_photos,
                            rules: [{
                                required: false,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.contractChange} />
                        )}
                    </List>
                    <List renderHeader={() => '服务机构合同条款'}>
                        <TextareaItem
                            {...getFieldDecorator('servicer_contract_clauses')}
                            title="合同条款"
                            placeholder="请输入服务机构合同条款"
                            rows={3}
                            onChange={this.clausesChange}
                            value={servicer_contract_clauses}
                        />
                    </List>
                    <List renderHeader={() => '备注'}>
                        <TextareaItem
                            {...getFieldDecorator('servicer_remark')}
                            title="备注"
                            placeholder="请输入备注"
                            rows={3}
                            onChange={this.remarkChange}
                            value={servicer_remark}
                        />
                    </List>
                    <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.handleSubmit}>提交申请</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 组件：服务人员申请页面视图控件
 * 控制服务人员申请页面视图控件
 */
@addon('SerVicePersonalapplyView', '服务人员申请页面视图控件', '控制服务人员申请页面视图控件')
@reactControl(Form.create<any>()(SerVicePersonalapplyView), true)
export class SerVicePersonalapplyViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}