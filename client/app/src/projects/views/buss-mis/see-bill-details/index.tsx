import React from "react";
import { BaseReactElementControl, ReactView, ReactViewState, reactControl } from "pao-aop-client";
import { AppServiceUtility } from "src/projects/app/appService";
import { addon } from "pao-aop";
import { Card } from "antd";
import CardBody from "antd-mobile/lib/card/CardBody";
import { Flex, WhiteSpace } from "antd-mobile";
/**
 * 组件：查看账单详情页状态
 */
export interface SeeBileDetailsViewState extends ReactViewState {
    /** 数据 */
    data?: any;
}

/**
 * 组件：查看账单详情页
 * 描述
 */
export class SeeBileDetailsView extends ReactView<SeeBileDetailsViewControl, SeeBileDetailsViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            data: {}
        };
    }

    componentDidMount() {
        if (this.props.match!.params.key) {
            AppServiceUtility.financial_service.billing_details_record!({ id: this.props.match!.params.key })!
                .then((res: any) => {
                    this.setState({
                        data: res.result[0]
                    });
                });
        }
    }

    render() {
        return (
            <div>
                <Card>
                    <CardBody>
                        <Flex>
                            <Flex.Item>账户类型</Flex.Item>
                            <Flex.Item>{this.state.data ? this.state.data.account_type : ''}</Flex.Item>
                        </Flex>
                        <WhiteSpace size='md' />
                        <Flex>
                            <Flex.Item>账户余额</Flex.Item>
                            <Flex.Item>{this.state.data ? this.state.data.balance : ''}</Flex.Item>
                        </Flex>
                        <WhiteSpace size='md' />
                        <Flex>
                            <Flex.Item>付款时间</Flex.Item>
                            <Flex.Item>{this.state.data ? this.state.data.date : ''}</Flex.Item>
                        </Flex>
                        <WhiteSpace size='md' />
                        <Flex>
                            <Flex.Item>付款金额</Flex.Item>
                            <Flex.Item><strong>¥{this.state.data ? this.state.data.amount : ''}</strong></Flex.Item>
                        </Flex>
                        <WhiteSpace size='md' />
                        <Flex>
                            <Flex.Item>收支</Flex.Item>
                            <Flex.Item><strong>{this.state.data ? this.state.data.type : ''}</strong></Flex.Item>
                        </Flex>
                        <WhiteSpace size='md' />
                        <Flex>
                            <Flex.Item>摘要</Flex.Item>
                            <Flex.Item><strong>{this.state.data ? this.state.data.abstract : ''}</strong></Flex.Item>
                        </Flex>

                    </CardBody>
                </Card>
                <WhiteSpace size="lg" />
            </div>
        );
    }
}

/**
 * 控件：查看账单详情页控制器
 * 描述
 */
@addon('SeeBileDetailsView', '查看账单详情页', '查看账单详情页')
@reactControl(SeeBileDetailsView, true)
export class SeeBileDetailsViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}