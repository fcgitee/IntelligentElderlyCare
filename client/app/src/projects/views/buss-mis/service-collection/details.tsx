import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { SearchBar, Card, Toast, ListView } from 'antd-mobile';
import { AppServiceUtility } from "src/projects/app/appService";
import { Rate, Icon, Row, Spin } from 'antd';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { filterHTMLTag } from "../../home";

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：服务收藏-列表状态
 */
export interface ServiceCollectionDetailsViewState extends ReactViewState {
    card_list?: any;
    cancel_gz?: any;
    insert?: any;
    dataSource?: any;
    page?: any;
    animating?: any;
}

/**
 * 组件：服务收藏-列表
 * 描述
 */
export class ServiceCollectionDetailsView extends ReactView<ServiceCollectionDetailsViewControl, ServiceCollectionDetailsViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            card_list: [],
            cancel_gz: 1,
            dataSource: ds,
            insert: '',
            page: 1,
            animating: false
        };
    }

    select = (param: any, page: any, old_list: any) => {
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param, page, 10)!
            .then((data: any) => {
                // console.log(data);
                let card_list = [];
                if (data.result.length > 0) {
                    card_list = [...data.result, ...old_list];
                    this.setState(
                        {
                            card_list,
                            animating: false
                        }
                    );
                }
            });
    }

    selectAll = (card_list: any, param: any, page: any) => {
        let param1: any = { "is_own": true, 'type': 'collection', 'object': '养老资讯', ...param };
        let param2: any = { "is_own": true, 'type': 'collection', 'object': '活动', ...param };
        let param3: any = { "is_own": true, 'type': 'collection', 'object': '居家服务', ...param };
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param1, page, 10)!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    card_list = [...data.result, ...card_list];
                    this.setState(
                        {
                            card_list
                        }
                    );
                }
                AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param2, page, 10)!
                    .then((data: any) => {
                        // console.log(data);
                        if (data.result.length > 0) {
                            card_list = [...data.result, ...card_list];
                            this.setState(
                                {
                                    card_list
                                }
                            );
                        }
                        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!(param3, page, 10)!
                            .then((data: any) => {
                                // console.log(data);
                                if (data.result.length > 0) {
                                    card_list = [...data.result, ...card_list];
                                    this.setState(
                                        {
                                            card_list,
                                            animating: false
                                        }
                                    );
                                }

                            });
                    });
            });
    }
    componentDidMount = () => {
        this.setState({
            animating: true
        });
        if (this.props.match!.params.key.split('&').length > 1) {
            this.selectAll([], { "name": this.props.match!.params.key.split('&')[0] }, 1);
        } else {
            this.select({ "is_own": true, 'type': 'collection', 'object': this.props.match!.params.key }, 1, []);

        }
    }
    // 取消关注
    likeChange = (value: any, id: any) => {
        // console.log(value);
        // console.log(id);
        if (value === 0) {
            // console.log("取消关注了");
            AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(id)!
                .then((data: any) => {
                    // console.log(data);
                    if (data === 'Success') {
                        this.setState({
                            cancel_gz: 0
                        });
                        Toast.success('取消关注成功!!!', 1);
                        window.location.reload();
                    }
                });
        }
    }
    iconClick = (e: any) => {
        e.stopPropagation();
    }
    // 前往详情
    goDetails = (type: any, id: any) => {
        // console.log(id);
        // console.log(type);
        if (type === '养老资讯') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
        if (type === '活动') {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id);
        }
        if (type === '居家服务') {
            this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
        }
    }

    searchChange = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }

    search = () => {
        this.setState({
            page: 1,
            card_list: [],
            animating: true
        });
        if (this.props.match!.params.key.split('&').length > 1) {
            this.selectAll([], { "name": this.state.insert }, 1);
        } else {
            this.select({ "is_own": true, 'type': 'collection', 'object': this.props.match!.params.key, "name": this.state.insert }, 1, []);
        }
    }
    onEndReached = () => {
        let new_pape = this.state.page + 1;
        this.setState({
            page: new_pape,
        });
        let card_list = this.state.card_list;
        let param = {};
        if (this.state.insert !== '') {
            param['name'] = this.state.insert;
        }
        if (this.props.match!.params.key.split('&').length > 1) {
            this.selectAll(card_list, { "name": this.props.match!.params.key.split('&')[0] }, new_pape);
        } else {
            param = { "is_own": true, 'type': 'collection', 'object': this.props.match!.params.key };
            this.select(param, new_pape, card_list);
        }
    }
    render() {
        const { card_list, dataSource, } = this.state;
        let renderRow: any;
        if (this.props.match!.params.key === '养老资讯') {
            renderRow = (owData: any) => {
                let show_data = owData.obj ? owData.obj : '';
                let show_org = owData.org ? owData.org.name : '';
                let start_time: Date = owData.obj ? new Date(owData.obj.create_date) : new Date();
                let diff = new Date().getTime() - start_time.getTime();
                var days = Math.floor(diff / (24 * 3600 * 1000));
                var leave1 = diff % (24 * 3600 * 1000);
                var hours = Math.floor(leave1 / (3600 * 1000));
                return (
                    <Card style={{ marginBottom: '20px' }} className='collection-card'>
                        <Card.Body onClick={() => { this.goDetails('养老资讯', show_data.id); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '35%' }}>
                                    <img src={show_data.app_img_list ? show_data.app_img_list.length > 0 ? show_data.app_img_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                </div>
                                <div style={{ float: 'left', width: '59%' }}>
                                    <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.title}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                    <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '147px', overflow: 'hidden', display: 'inline-block' }}>{show_data.description}</div>
                                    <div className='content'>{filterHTMLTag(show_data.content ? show_data.content : '')}</div>
                                    <div style={{ fontSize: '12px', marginTop: '10px' }}>{days + '天' + hours + '小时前'}</div>
                                    <div style={{ overflow: 'hidden', width: '100%' }}>
                                        <div style={{ float: 'left', width: '60%' }}>{show_org}</div>
                                        <div style={{ float: 'right' }}><Icon type="eye" />&nbsp;{0}&nbsp;&nbsp;<Icon type="heart" theme="filled" />&nbsp;{owData.follow_count}</div>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                );
            };
        } else if (this.props.match!.params.key === '活动') {
            renderRow = (owData: any) => {
                let show_data = owData.obj ? owData.obj : '';
                let org_name = owData.org ? owData.org.name : '';
                return (
                    <Card style={{ marginBottom: '20px' }} className='collection-card'>
                        <Card.Body onClick={() => { this.goDetails('活动', show_data.id); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '35%' }}>
                                    <img src={show_data.photo ? show_data.photo.length > 0 ? show_data.photo[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                </div>
                                <div style={{ float: 'left', width: '59%' }}>
                                    <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.activity_name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                    <br />
                                    <div>{org_name}</div>
                                    <div style={{ fontSize: '14px' }}>{'开始时间：' + (show_data.begin_date ? show_data.begin_date : '')}</div>
                                    <div style={{ fontSize: '14px' }}>{'结束时间：' + (show_data.end_date ? show_data.end_date : '')}</div>
                                    <br />
                                    <div style={{ overflow: 'hidden', width: '100%' }}>
                                        <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{!show_data.hasOwnProperty('amout') || show_data.amount === 0 || show_data.amount === '0' || show_data.amout === '' || show_data.amout === undefined ? '免费' : `￥${show_data['amount'] || 0}`}</div>
                                        <div style={{ float: 'right' }}>已报名：{owData.participate_count || 0}/{show_data.max_quantity}</div>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                );
            };
        } else if (this.props.match!.params.key === '居家服务') {
            renderRow = (owData: any) => {
                let show_data = owData.obj ? owData.obj : '';
                let org_name = owData.org ? owData.org.name : '';
                let price = owData.obj.service_package_price ? owData.obj.service_package_price : '';
                return (
                    <Card style={{ marginBottom: '20px' }} className='collection-card'>
                        <Card.Body onClick={() => { this.goDetails('居家服务', show_data.id); }} key={owData}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '35%' }}>
                                    <img src={show_data.picture_collection ? show_data.picture_collection.length > 0 ? show_data.picture_collection[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                </div>
                                <div style={{ float: 'left', width: '59%' }}>
                                    <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                    <br />
                                    <div className='content'>{show_data.introduce}</div>
                                    <br />
                                    <div>{org_name}</div>
                                    <div style={{ overflow: 'hidden', width: '100%' }}>
                                        <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{'￥' + price}</div>
                                        <div style={{ float: 'right' }}>{owData.pay_count + '人付款'}</div>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                );
            };
        } else {
            renderRow = (owData: any): any => {
                if (owData.object === '养老资讯') {
                    let show_data = owData.obj ? owData.obj : '';
                    let show_org = owData.org ? owData.org.name : '';
                    let start_time: Date = owData.obj ? new Date(owData.obj.create_date) : new Date();
                    let diff = new Date().getTime() - start_time.getTime();
                    var days = Math.floor(diff / (24 * 3600 * 1000));
                    var leave1 = diff % (24 * 3600 * 1000);
                    var hours = Math.floor(leave1 / (3600 * 1000));
                    return (
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <Card.Body onClick={() => { this.goDetails('养老资讯', show_data.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                        <img src={show_data.app_img_list ? show_data.app_img_list.length > 0 ? show_data.app_img_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                    </div>
                                    <div style={{ float: 'left', width: '59%' }}>
                                        <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.title}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                        <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '147px', overflow: 'hidden', display: 'inline-block' }}>{show_data.description}</div>
                                        <div className='content'>{show_data.content ? show_data.content : ''}</div>
                                        <div style={{ fontSize: '12px', marginTop: '10px' }}>{days + '天' + hours + '小时前'}</div>
                                        <div style={{ overflow: 'hidden', width: '100%' }}>
                                            <div style={{ float: 'left', width: '60%' }}>{show_org}</div>
                                            <div style={{ float: 'right' }}><Icon type="eye" />&nbsp;{0}&nbsp;&nbsp;<Icon type="heart" theme="filled" />&nbsp;{owData.follow_count}</div>
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    );
                }
                if (owData.object === '活动') {
                    let show_data = owData.obj ? owData.obj : '';
                    let org_name = owData.org ? owData.org.name : '';
                    return (
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <Card.Body onClick={() => { this.goDetails('活动', show_data.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                        <img src={show_data.photo ? show_data.photo.length > 0 ? show_data.photo[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                    </div>
                                    <div style={{ float: 'left', width: '59%' }}>
                                        <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.activity_name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                        <br />
                                        <div>{org_name}</div>
                                        <div style={{ fontSize: '14px' }}>{'开始时间：' + (show_data.begin_date ? show_data.begin_date : '')}</div>
                                        <div style={{ fontSize: '14px' }}>{'结束时间：' + (show_data.end_date ? show_data.end_date : '')}</div>
                                        <br />
                                        <div style={{ overflow: 'hidden', width: '100%' }}>
                                            <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{!show_data.hasOwnProperty('amout') || show_data.amount === 0 || show_data.amount === '0' || show_data.amout === '' || show_data.amout === undefined ? '免费' : `￥${show_data['amount'] || 0}`}</div>
                                            <div style={{ float: 'right' }}>已报名：{show_data.participate_count || 0}/{show_data.max_quantity}</div>
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    );
                }
                if (owData.object === '居家服务') {
                    let show_data = owData.obj ? owData.obj : '';
                    let org_name = owData.org ? owData.org.name : '';
                    let price = owData.obj.service_package_price ? owData.obj.service_package_price : '';
                    return (
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <Card.Body onClick={() => { this.goDetails('居家服务', show_data.id); }} key={owData}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                        <img src={show_data.picture_collection ? show_data.picture_collection.length > 0 ? show_data.picture_collection[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                    </div>
                                    <div style={{ float: 'left', width: '59%' }}>
                                        <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, owData.id); }} /></div></div>
                                        <br />
                                        <div className='content'>{show_data.introduce}</div>
                                        <br />
                                        <div>{org_name}</div>
                                        <div style={{ overflow: 'hidden', width: '100%' }}>
                                            <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{'￥' + price}</div>
                                            <div style={{ float: 'right' }}>{owData.pay_count + '人付款'}</div>
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    );
                }
            };
        }
        return (
            <div>
                <SearchBar placeholder="可根据名称搜索" onChange={this.searchChange} onSubmit={this.search} />
                <Row className='tabs-content'>
                    {
                        card_list && card_list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(card_list)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: document.documentElement!.clientHeight }}
                                onEndReached={this.onEndReached}
                            />
                            :
                            (card_list.length === 0 && this.state.animating === true ?
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>
                                :
                                '')
                    }
                    {card_list.length === 0 && this.state.animating === false ?
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                            </Row>
                        :
                        ''
                    }
                </Row>
            </div>
        );
    }
}

/**
 * 控件：服务收藏-列表控制器
 * 描述
 */
@addon('ServiceCollectionDetailsView', '服务收藏-列表', '描述')
@reactControl(ServiceCollectionDetailsView, true)
export class ServiceCollectionDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}