import { Icon, Rate, Spin } from 'antd';
import { Card, SearchBar, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
import { filterHTMLTag } from '../../home';

/**
 * 组件：服务收藏状态
 */
export interface ServiceCollectionViewState extends ReactViewState {
    type?: any;
    cancel_gz?: any;
    insert?: any;
    yl_data?: any;
    hd_data?: any;
    jj_data?: any;
    show?: any;
}

/**
 * 组件：服务收藏
 * 描述
 */
export class ServiceCollectionView extends ReactView<ServiceCollectionViewControl, ServiceCollectionViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            type: ['养老资讯', '活动', '居家服务'],
            cancel_gz: 1,
            insert: '',
            yl_data: [],
            hd_data: [],
            jj_data: [],
            show: false
        };
    }
    componentWillMount = () => {
        this.setState({
            show: true
        });
        const { type } = this.state;
        type.map((item: any) => {
            AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ type: 'collection', object: item, "is_own": true }, 1, 3)!
                .then((data: any) => {
                    // // console.log(item, data);
                    if (data.result.length > 0) {
                        if (item === '养老资讯') {
                            this.setState({
                                yl_data: data.result
                            });
                        }
                        if (item === '活动') {
                            this.setState({
                                hd_data: data.result
                            });
                        }
                        if (item === '居家服务') {
                            this.setState({
                                jj_data: data.result
                            });
                        }
                    }
                    this.setState({
                        show: false
                    });
                });
        });
    }
    // 前往列表页
    goList = (e: any, type: any) => {
        // // console.log(type);
        this.props.history!.push(ROUTE_PATH.serviceCollectionDetails + '/' + type);
    }

    // 取消关注
    likeChange = (value: any, id: any) => {
        // // console.log(value);
        // // console.log(id);
        if (value === 0) {
            // // console.log("取消关注了");
            AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(id)!
                .then((data: any) => {
                    // // console.log(data);
                    if (data === 'Success') {
                        this.setState({
                            cancel_gz: 0
                        });
                        Toast.success('取消关注成功!!!', 1);
                        window.location.reload();
                    }
                });
        }
    }
    iconClick = (e: any) => {
        e.stopPropagation();
    }
    // 前往详情
    goDetails = (type: any, id: any) => {
        // // console.log(id);
        // // console.log(type);
        if (type === '养老资讯') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
        if (type === '活动') {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id);
        }
        if (type === '居家服务') {
            this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
        }
    }

    searchChange = (value: any) => {
        // // console.log(value);
        this.setState({
            insert: value
        });
    }

    search = () => {
        this.props.history!.push(ROUTE_PATH.serviceCollectionDetails + '/' + this.state.insert + '&');
    }
    render() {
        setMainFormTitle('服务收藏');
        const { yl_data, hd_data, jj_data, show } = this.state;
        return (
            <div>
                <SearchBar placeholder="可根据名称搜索" onChange={this.searchChange} onSubmit={this.search} />
                {show && show === true ?
                    <div style={{ textAlign: 'center', margin: '20px' }}>
                        <Spin size="large" />
                    </div>
                    :
                    ''
                }
                {yl_data.length === 0 && hd_data.length === 0 && jj_data.length === 0 ?
                    <div style={{ width: '100%', textAlign: 'center' }}>暂无关注数据</div>
                    :
                    <div>
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <div onClick={(e) => { this.goList(e, '养老资讯'); }}>
                                <Card.Header
                                    title='养老资讯'
                                    extra={<Icon type='right' />}
                                />
                            </div>
                            {yl_data.length > 0 ? yl_data.map((items: any) => {
                                let show_data = items.obj ? items.obj : '';
                                let show_org = items.org ? items.org.name : '';
                                let start_time: Date = items.obj ? new Date(items.obj.create_date) : new Date();
                                let diff = new Date().getTime() - start_time.getTime();
                                var days = Math.floor(diff / (24 * 3600 * 1000));
                                var leave1 = diff % (24 * 3600 * 1000);
                                var hours = Math.floor(leave1 / (3600 * 1000));
                                return <Card.Body onClick={() => { this.goDetails('养老资讯', show_data.id); }} key={items}>
                                    <div style={{ overflow: 'hidden' }}>
                                        <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                            <img src={show_data.app_img_list ? show_data.app_img_list.length > 0 ? show_data.app_img_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                        </div>
                                        <div style={{ float: 'left', width: '59%' }}>
                                            <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.title}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, items.id); }} /></div></div>
                                            <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '147px', overflow: 'hidden', display: 'inline-block' }}>{show_data.description}</div>
                                            <div className='content'>{filterHTMLTag(show_data.content ? show_data.content : '')}</div>
                                            <div style={{ fontSize: '12px', marginTop: '10px' }}>{days + '天' + hours + '小时前'}</div>
                                            <div style={{ overflow: 'hidden', width: '100%' }}>
                                                <div style={{ float: 'left', width: '60%' }}>{show_org}</div>
                                                <div style={{ float: 'right' }}><Icon type="eye" />&nbsp;{0}&nbsp;&nbsp;<Icon type="heart" theme="filled" />&nbsp;{items.follow_count}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>;
                            })
                                :
                                ''
                            }
                        </Card>
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <div onClick={(e) => { this.goList(e, '活动'); }}>
                                <Card.Header
                                    title='活动'
                                    extra={<Icon type='right' />}
                                />
                            </div>
                            {hd_data.length > 0 ? hd_data.map((items: any) => {
                                let show_data = items.obj ? items.obj : '';
                                let org_name = items.org ? items.org.name : '';
                                return <Card.Body onClick={() => { this.goDetails('活动', show_data.id); }} key={items}>
                                    <div style={{ overflow: 'hidden' }}>
                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '35%' }}>
                                            <img src={show_data.photo ? show_data.photo.length > 0 ? show_data.photo[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                        </div>
                                        <div style={{ float: 'left', width: '59%' }}>
                                            <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.activity_name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, items.id); }} /></div></div>
                                            <br />
                                            <div>{org_name}</div>
                                            <div style={{ fontSize: '14px' }}>{'开始时间：' + (show_data.begin_date ? show_data.begin_date : '')}</div>
                                            <div style={{ fontSize: '14px' }}>{'结束时间：' + (show_data.end_date ? show_data.end_date : '')}</div>
                                            <br />
                                            <div style={{ overflow: 'hidden', width: '100%' }}>
                                                <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{!show_data.hasOwnProperty('amout') || show_data.amount === 0 || show_data.amount === '0' || show_data.amout === '' || show_data.amout === undefined ? '免费' : `￥${show_data['amount'] || 0}`}</div>
                                                <div style={{ float: 'right' }}>已报名：{items.participate_count || 0}/{show_data.max_quantity}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>;
                            })
                                : ''
                            }
                        </Card>
                        <Card style={{ marginBottom: '20px' }} className='collection-card'>
                            <div onClick={(e) => { this.goList(e, '居家服务'); }}>
                                <Card.Header
                                    title='居家服务'
                                    extra={<Icon type='right' />}
                                />
                            </div>
                            {jj_data.length > 0 ? jj_data.map((items: any) => {
                                let show_data = items.obj ? items.obj : '';
                                let org_name = items.org ? items.org.name : '';
                                let price = items.obj.service_package_price ? items.obj.service_package_price : '';
                                return <Card.Body onClick={() => { this.goDetails('居家服务', show_data.id); }} key={items}>
                                    <div style={{ overflow: 'hidden' }}>
                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid', width: '35%' }}>
                                            <img src={show_data.picture_collection ? show_data.picture_collection.length > 0 ? show_data.picture_collection[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                        </div>
                                        <div style={{ float: 'left', width: '59%' }}>
                                            <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.name}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, items.id); }} /></div></div>
                                            <br />
                                            <div className='content'>{show_data.introduce}</div>
                                            <br />
                                            <div>{org_name}</div>
                                            <div style={{ overflow: 'hidden', width: '100%' }}>
                                                <div style={{ float: 'left', width: '30%', color: '#ffb500' }}>{'￥' + price}</div>
                                                <div style={{ float: 'right' }}>{items.pay_count + '人付款'}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>;
                            })
                                : ''
                            }
                        </Card>
                    </div>
                }
            </div>
        );
    }
}

/**
 * 控件：服务收藏控制器
 * 描述
 */
@addon('ServiceCollectionView', '服务收藏', '描述')
@reactControl(ServiceCollectionView, true)
export class ServiceCollectionViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}