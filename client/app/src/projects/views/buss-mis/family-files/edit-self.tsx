import { Button, Form, Radio, TreeSelect } from "antd";
import { InputItem, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { getBirthday, getSex, decodeUrlParam, ROUTE_PATH, beforeUpload } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import './edit-self.less';
/**
 * 组件：家庭档案-完善本人信息状态
 */
export interface FamilyFilesEditSelfViewState extends ReactViewState {
    card_type?: any;
    area?: any;
    now_area?: any;
    user_id?: any;
    zz_show?: any;
    js_show?: any;
    id?: any;
    type_data?: any;
    relationship_type_id?: any;
    sex?: any;
    date_birth?: any;
    name?: any;
    id_card?: any;
    telephone?: any;
    is_address_type?: any;
    native_place?: any;
    address?: any;
    option?: any;
    administrative_division_list?: any;
    admin_area_id?: any;
    show?: any;
    show_value?: any;
    photo?:any;
}

/**
 * 组件：家庭档案-完善本人信息
 * 描述
 */
export class FamilyFilesEditSelfView extends ReactView<FamilyFilesEditSelfViewControl, FamilyFilesEditSelfViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            card_type: '中国大陆',
            area: '南海区',
            now_area: '与户籍地址一致',
            zz_show: false,
            js_show: false,
            id: '',
            user_id: '',
            type_data: [],
            relationship_type_id: '',
            sex: '',
            date_birth: '',
            name: '',
            id_card: '',
            telephone: '',
            is_address_type: '',
            native_place: '',
            address: '',
            administrative_division_list: [],
            admin_area_id: '',
            show_value: '',
            show: false,
            photo: [],
            option: { showSearch: true, treeNodeFilterProp: 'title', dropdownStyle: { maxHeight: 400, overflow: 'auto' }, allowClear: true, treeDefaultExpandAll: true, treeData: this.state.administrative_division_list, placeholder: "请选择行政区划", }
        };
    }
    getValue = (value: any) => {
        if (value.personnel_info && value.personnel_info.card_type ? value.personnel_info.card_type === '中国大陆' : false) {
            if (value.id_card.length === 18) {
                // console.log(111111111);
            }
            this.setState({
                sex: getSex(value.id_card),
                date_birth: getBirthday(value.id_card)
            });
        } else {
            this.setState({
                sex: value.personnel_info && value.personnel_info.sex ? value.personnel_info.sex : '',
                date_birth: value.personnel_info && value.personnel_info.date_birth ? value.personnel_info.date_birth.split(' ')[0] : '',
                show: true
            });
        }
        AppServiceUtility.business_area_service.get_all_admin_division_list!({ id: value.admin_area_id }, 1, 1)!
            .then((data: any) => {
                // console.log('行政区划', data);
                this.setState({
                    show_value: data.result[0]['name']
                });
            });
        this.setState({
            name: value.name,
            id_card: value.id_card,
            admin_area_id: value.admin_area_id,
            card_type: value.personnel_info ? value.personnel_info.card_type ? value.personnel_info.card_type : '中国大陆' : '中国大陆',
            telephone: value.personnel_info ? value.personnel_info.telephone : '',
            is_address_type: value.personnel_info ? value.personnel_info.is_address_type ? value.personnel_info.is_address_type : '南海区' : '南海区',
            native_place: value.personnel_info ? value.personnel_info.native_place ? value.personnel_info.native_place : '' : '',
            address: value.personnel_info ? value.personnel_info.address ? value.personnel_info.address : '' : '',
        });
    }
    componentDidMount = () => {
        AppServiceUtility.business_area_service.get_admin_division_list!({ name: '南海区' })!
            .then(data => {
                // console.log('行政区划', data);
                this.setState({
                    option: { showSearch: true, treeNodeFilterProp: 'title', dropdownStyle: { maxHeight: 250, overflow: 'auto' }, allowClear: true, treeDefaultExpandAll: true, treeData: data!.result, placeholder: "请选择行政区划", },
                    administrative_division_list: data!.result
                });
            });
        // console.log(this.props.match!.params.key);
        if (this.props.match!.params.key.split('&')[0] === '长者') {
            let user_info = {};
            let relationship_type_id = '';
            if (this.props.match!.params.key.split('&').length > 3) {
                user_info = JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[3]))[0];
                relationship_type_id = this.props.match!.params.key.split('&')[2];
                this.getValue(user_info);
            }
            // console.log(user_info);
            setMainFormTitle('添加长者');
            this.getType('长者');
            this.setState({
                zz_show: true,
                relationship_type_id
            });

        }
        if (this.props.match!.params.key.split('&')[0] === '家属') {
            let user_info = {};
            let relationship_type_id = '';
            if (this.props.match!.params.key.split('&').length > 3) {
                user_info = JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[3]))[0];
                relationship_type_id = this.props.match!.params.key.split('&')[2];
                this.getValue(user_info);
            }
            // console.log(user_info);
            setMainFormTitle('添加子女家属');
            this.getType('家属');
            this.setState({
                js_show: true,
                relationship_type_id
            });
        }
        if (this.props.match!.params.key.split('&')[0] === '本人') {
            setMainFormTitle('完善本人信息');
            let user_info = JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[1]));
            // console.log(user_info);
            this.getValue(user_info);
        }
    }

    // 获取人员关系类型信息
    getType = (type: any) => {
        AppServiceUtility.family_files_service.get_relationShip_type!({ "type": type })!
            .then((data: any) => {
                // console.log('人员类型关系表》》》》》》》》', data);
                this.setState({
                    type_data: data.result
                });
            });
    }

    // 身份证类型
    cardTypeChange = (e: any) => {
        // console.log(e.target.value);
        if (e.target.value === '港澳台' || e.target.value === '外籍') {
            this.setState({
                show: true
            });
        } else {
            this.setState({
                show: false
            });
        }
        this.setState({
            card_type: e.target.value
        });
    }

    // 户籍地址
    areaChange = (e: any) => {
        // console.log(e.target.value);
        if (e.target.value === '其他') {
            AppServiceUtility.business_area_service.get_admin_division_list!({})!
                .then(data => {
                    // console.log('行政区划', data);
                    this.setState({
                        option: { showSearch: true, treeNodeFilterProp: 'title', dropdownStyle: { maxHeight: 250, overflow: 'auto' }, allowClear: true, treeDefaultExpandAll: true, treeData: data!.result, placeholder: "请选择行政区划", },
                        administrative_division_list: data!.result
                    });
                });
        }
        this.setState({
            area: e.target.value
        });
    }

    // 常住地址
    nowareaChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            now_area: e.target.value
        });
    }

    // 关系
    relationTypeChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            relationship_type_id: e.target.value
        });
    }

    // 性别
    sexChange = (value: any) => {
        // console.log(value);
        this.setState({
            sex: value
        });
    }

    // 生日
    birthChange = (value: any) => {
        // console.log(value);
        this.setState({
            date_birth: value
        });
    }
    handleSubmit = (e: any) => {
        const { card_type, now_area, area, relationship_type_id, admin_area_id, photo } = this.state;
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (!err) {
                if (values['id_card'].length < 18 && this.state.card_type === '中国大陆') {
                    Toast.fail('身份证格式不正确，请重新填写');
                    return;
                } else {
                    if (this.checkIDCard(values['id_card']) === false && this.state.card_type === '中国大陆') {
                        Toast.fail('身份证格式不正确，请重新填写');
                        return;
                    }
                }
                if (values['telephone'].length < 11 && this.checkPhone(values['telephone']) === false) {
                    Toast.fail('手机号码格式不正确，请重新填写');
                    return;
                }
                let id = '';
                let user_id = '';
                if (this.props.match!.params.key.split('&').length > 2) {
                    id = this.props.match!.params.key.split('&')[1];
                    user_id = JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[3]))[0].id;
                } else {
                    // console.log(111111);
                    // console.log(JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[1])));
                    user_id = JSON.parse(decodeUrlParam(this.props.match!.params.key.split('&')[1])).id;
                }
                let sex = this.state.sex;
                let date_birth = this.state.date_birth;
                if (date_birth === undefined) {
                    Toast.fail('请填写生日');
                    return;
                }
                if (sex === undefined) {
                    Toast.fail('请填写性别');
                    return;
                }
                if (now_area === '与户籍地址一致') {
                    values['address'] = values['native_place'];
                }
                let params = {};
                params = { ...values, "card_type": card_type, "is_address_type": area, "sex": sex, "date_birth": date_birth };
                if (id !== '') {
                    params = { ...params, "id": id };
                }
                if (user_id !== '') {
                    params = { ...params, "user_id": user_id };
                }
                if (relationship_type_id !== '') {
                    params = { ...params, "relationship_type_id": relationship_type_id };
                }
                if (this.props.match!.params.key.split('&')[0] === '长者') {
                    params = { ...params, "operation_type": "长者" };
                }
                if (this.props.match!.params.key.split('&')[0] === '家属') {
                    params = { ...params, "operation_type": "家属" };
                }
                if (admin_area_id !== '') {
                    params = { ...params, "admin_area_id": admin_area_id };
                } else {
                    Toast.fail('请选择所属区划');
                    return;
                }
                if (photo.length > 0) {
                    params = { ...params, "picture_list": photo };
                } else {
                    params = { ...params, "picture_list": [
                        "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1385781871,3367469142&fm=26&gp=0.jpg"
                    ] };
                }
                // console.log(params);
                if (this.props.match!.params.key.split('&')[0] === '本人') {
                    // console.log(params);
                    AppServiceUtility.login_service.modify_user_info!({ key: 'id', value: params })!
                        .then((data: any) => {
                            console.info(data);
                            Toast.success('修改成功!!!', 1);
                            setTimeout(
                                () => {
                                    this.props.history!.push(ROUTE_PATH.familyFiles);
                                },
                                2000
                            );
                        })
                        .catch((err: any) => {
                            console.info(err);
                        });
                } else {
                    // 更新当前登陆人或家属信息
                    AppServiceUtility.family_files_service.update_user_relation_ship!(params)!
                        .then((data: any) => {
                            // console.log('增加成功了吗？？？？？？》》》》》', data);
                            if (data === 'Success') {
                                Toast.success('修改成功!!!', 1);
                                setTimeout(
                                    () => {
                                        this.props.history!.push(ROUTE_PATH.familyFiles);
                                    },
                                    2000
                                );
                            }
                        });
                }
            }
        });
    }

    nameChange = (value: any) => {
        // console.log(value);
        this.setState({
            name: value
        });
    }

    idCardChange = (value: any) => {
        // console.log(value);
        // console.log(this.checkIDCard(value));
        if (value.length === 18 && this.checkIDCard(value) === false && this.state.card_type === '中国大陆') {
            Toast.fail('身份证格式不正确，请重新填写');
        } else {
            if (this.state.card_type === '中国大陆') {
                this.setState({
                    sex: getSex(value),
                    date_birth: getBirthday(value),
                });
            }
        }
        this.setState({
            id_card: value,
        });
    }

    phoneChange = (value: any) => {
        if (value.length === 11 && this.checkPhone(value) === false) {
            Toast.fail('手机号码格式不正确，请重新填写');
        }
        this.setState({
            telephone: value
        });
    }

    qyChange = (value: any, title: any) => {
        // console.log(value, title);
        this.setState({
            admin_area_id: value,
            show_value: title[0]
        });
    }

    // 身份证校验
    checkIDCard = (idcode: any) => {
        // 加权因子
        var weight_factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        // 校验码
        var check_code = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];

        var code = idcode + "";
        var last = idcode[17]; // 最后一位

        var seventeen = code.substring(0, 17);

        // ISO 7064:1983.MOD 11-2
        // 判断最后一位校验码是否正确
        var arr: any = seventeen.split("");
        var len = arr.length;
        var num = 0;
        for (var i = 0; i < len; i++) {
            num = num + arr[i] * weight_factor[i];
        }

        // 获取余数
        var resisue = num % 11;
        var last_no = check_code[resisue];

        // 格式的正则
        // 正则思路
        /*
        第一位不可能是0
        第二位到第六位可以是0-9
        第七位到第十位是年份，所以七八位为19或者20
        十一位和十二位是月份，这两位是01-12之间的数值
        十三位和十四位是日期，是从01-31之间的数值
        十五，十六，十七都是数字0-9
        十八位可能是数字0-9，也可能是X
        */
        var idcard_patter = /^[1-9][0-9]{5}([1][9][0-9]{2}|[2][0][0|1][0-9])([0][1-9]|[1][0|1|2])([0][1-9]|[1|2][0-9]|[3][0|1])[0-9]{3}([0-9]|[X])$/;

        // 判断格式是否正确
        var format = idcard_patter.test(idcode);

        // 返回验证结果，校验码和格式同时正确才算是合法的身份证号码
        return last === last_no && format ? true : false;
    }

    // 手机号码校验
    checkPhone = (value: any) => {
        if (!(/^1[3456789]\d{9}$/.test(value))) {
            return false;
        } else {
            return true;
        }
    }

    photoChange = (value: any) => {
        // console.log(value);
        this.setState({
            photo: value
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className='family-edit'>
                <div className="am-list">
                    <div className="am-list-header">{
                        this.state.zz_show === true || this.state.js_show === true ? this.state.zz_show === true ? '完善长者信息' : '完善子女家属信息' : '完善本人信息'
                    }</div>
                </div>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item>
                        {getFieldDecorator('name', {
                            initialValue: this.state.name ? this.state.name : '',
                            rules: [{ required: true, message: '请输入你的姓名!' }],
                        })(
                            <div className='formItem-style'>
                                <div className='formItem-title'>姓名</div>
                                <InputItem value={this.state.name} onChange={this.nameChange} clear={true} placeholder="请输入姓名" />
                            </div>
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('card_type', {
                            initialValue: this.state.card_type ? this.state.card_type : '中国大陆',
                            rules: [{ required: true, message: '请输入你的身份证号码!' }],
                        })(
                            <div className='formItem-style' style={{ height: '120px' }}>
                                <div className='formItem-title' style={{ marginTop: '35px' }}>身份证号</div>
                                <div className='formItem-input'>
                                    <Radio.Group onChange={this.cardTypeChange} value={this.state.card_type}>
                                        <Radio value={'中国大陆'} defaultChecked={true}>中国大陆</Radio>
                                        <Radio value={'港澳台'}>港澳台</Radio>
                                        <Radio value={'外籍'}>外籍</Radio>
                                    </Radio.Group>
                                    <br />
                                    <Form.Item>
                                        {getFieldDecorator('id_card', {
                                            initialValue: this.state.id_card ? this.state.id_card : '',
                                            rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                        })(
                                            <div style={{ borderBottom: '1px #ccc solid' }}>
                                                <InputItem maxLength={18} clear={true} value={this.state.id_card} onChange={this.idCardChange} placeholder="请输入身份证号码" style={{ marginTop: '5px' }} />
                                            </div>
                                        )}
                                    </Form.Item>
                                </div>
                            </div>
                        )}
                    </Form.Item>
                    <Form.Item>
                        <div className='formItem-style'>
                            <div className='formItem-title'>性别</div>
                            <InputItem value={this.state.sex} onChange={this.sexChange} editable={this.state.show} />
                        </div>
                    </Form.Item>
                    <Form.Item>
                        <div className='formItem-style'>
                            <div className='formItem-title'>生日</div>
                            <InputItem value={this.state.date_birth} onChange={this.birthChange} editable={this.state.show} />
                        </div>
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('is_address_type', {
                            initialValue: this.state.is_address_type ? this.state.is_address_type : '',
                            rules: [{ required: false }],
                        })(
                            <div className='formItem-style' style={{ height: '170px' }}>
                                <div className='formItem-title' style={{ marginTop: '35px' }}>户籍地址</div>
                                <div className='formItem-input'>
                                    <Radio.Group onChange={this.areaChange} value={this.state.area}>
                                        <Radio value={'南海区'} defaultChecked={true}>南海区</Radio>
                                        <Radio value={'其他'}>其他</Radio>
                                    </Radio.Group>
                                    <br />
                                    <TreeSelect onChange={this.qyChange} value={this.state.show_value} style={{ width: '200px' }} {...this.state.option} />
                                    <Form.Item>
                                        {getFieldDecorator('native_place', {
                                            initialValue: this.state.native_place ? this.state.native_place : '',
                                            rules: [{ required: true, message: '请输入你的户籍地址!' }],
                                        })(
                                            <InputItem style={{ marginTop: '5px' }} value={this.state.is_address_type} clear={true} placeholder="请根据以下例子输入户籍地址" />
                                        )}
                                    </Form.Item>
                                    <span style={{ fontSize: '11px' }}>{this.state.area === '南海区' ? '例如：XX镇(街) XX路XX号XX房' : "例如：XX省XX市XX县(市区) XX镇(街) XX路XX号XX房"}</span>
                                </div>
                            </div>
                        )}
                    </Form.Item>
                    <Form.Item>
                        <div className='formItem-style' style={{ height: '120px' }}>
                            <div className='formItem-title' style={{ marginTop: '35px' }}>常住地址</div>
                            <div className='formItem-input'>
                                <Radio.Group onChange={this.nowareaChange} value={this.state.now_area}>
                                    <Radio value={'与户籍地址一致'} defaultChecked={true}>与户籍地址一致</Radio>
                                    <Radio value={'其他'}>其他</Radio>
                                </Radio.Group>
                                <br />

                                {this.state.now_area === '其他' ?
                                    <Form.Item>
                                        {getFieldDecorator('address', {
                                            initialValue: this.state.address ? this.state.address : '',
                                            rules: [{ required: false }],
                                        })(
                                            <div style={{ borderBottom: '1px #ccc solid' }}>
                                                <InputItem clear={true} value={this.state.address} placeholder="请输入常驻地址" style={{ marginTop: '5px' }} />
                                            </div>
                                        )}
                                    </Form.Item>
                                    :
                                    ''
                                }
                            </div>
                        </div>
                    </Form.Item>
                    {
                        this.state.zz_show === true ?
                            <Form.Item>
                                <div className='formItem-style' style={{ height: '100px' }}>
                                    <div className='formItem-title' style={{ marginTop: '10px' }}>与本人关系</div>
                                    <div className='formItem-input' style={{ marginTop: '10px' }}>
                                        <Radio.Group>
                                            {this.state.type_data.map((item: any) => {
                                                return <Radio key={item.id} onChange={this.relationTypeChange} value={item.id} style={{ marginRight: '30px' }}>{item.name}</Radio>;
                                            })}
                                        </Radio.Group>
                                    </div>
                                </div>
                            </Form.Item>
                            :
                            ''
                    }
                    {
                        this.state.js_show === true ?
                            <Form.Item>
                                <div className='formItem-style' style={{ height: '100px' }}>
                                    <div className='formItem-title' style={{ marginTop: '10px' }}>与本人关系</div>
                                    <div className='formItem-input' style={{ marginTop: '10px' }}>
                                        <Radio.Group>
                                            {this.state.type_data.map((item: any) => {
                                                return <Radio key={item.id} onChange={this.relationTypeChange} checked={this.state.relationship_type_id === item.id} value={item.id} style={{ marginRight: '10px' }}>{item.name}</Radio>;
                                            })}
                                        </Radio.Group>
                                    </div>
                                </div>
                            </Form.Item>
                            :
                            ''
                    }
                    < Form.Item >
                        {getFieldDecorator('telephone', {
                            initialValue: this.state.telephone ? this.state.telephone : '',
                            rules: [{ required: true, message: '请输入联系电话!' }],
                        })(
                            <div className='formItem-style'>
                                <div className='formItem-title'>联系电话</div>
                                <InputItem maxLength={11} value={this.state.telephone} onChange={this.phoneChange} clear={true} placeholder="请输入联系电话" />
                            </div>
                        )}
                    </Form.Item>
                    <Form.Item>
                        <div className='formItem-style' style={{ height: '120px' }}>
                            <div className='formItem-title' style={{ marginTop: '10px' }}>头像（大小小于2M，格式支持jpg/jpeg/png）</div>
                            <div className='formItem-input' style={{ marginTop: '10px' }}>
                                <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.photoChange} />
                            </div>
                        </div>
                    </Form.Item>
                    <Button type="primary" size='large' htmlType="submit" style={{ position: 'fixed', bottom: '0px', width: '100%' }}>确认添加</Button>
                </Form>
            </div>
        );
    }
}

/**
 * 控件：家庭档案-完善本人信息控制器
 * 描述
 */
@addon('FamilyFilesEditSelfView', '家庭档案-完善本人信息', '描述')
@reactControl(Form.create<any>()(FamilyFilesEditSelfView), true)
export class FamilyFilesEditSelfViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}