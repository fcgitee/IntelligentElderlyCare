import { Card } from 'antd';
import { Button, List, Modal, SwipeAction, WhiteSpace } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, encodeUrlParam } from "src/projects/app/util-tool";
const alert = Modal.alert;

/**
 * 组件：家人档案状态
 */
export interface FamilyFilesViewState extends ReactViewState {
    id?: any;
    family_data?: any;
    person_data?: any;
}

/**
 * 组件：家人档案
 * 描述
 */
export class FamilyFilesView extends ReactView<FamilyFilesViewControl, FamilyFilesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            family_data: [],
            person_data: []
        };
    }
    componentDidMount() {
        // 获取当前登陆人信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log('当前登陆人的信息》》》》》》》》》', data);
                // console.log(new Date().getFullYear());
                // console.log(data[0].personnel_info.id_card);
                // console.log(new Date().getFullYear() - data[0].id_card.slice(6, 10));
                this.setState({
                    id: data[0]['id'],
                    person_data: data[0]
                });
                // 获取当前登陆人的家属信息
                AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': data[0]['id'] })!
                    .then((data: any) => {
                        // console.log('所属亲属信息》》》》》', data);
                        this.setState({
                            family_data: data['result']
                        });
                    });
            });
    }
    // 添加
    add = () => {
        alert('请选择添加方式', <div />, [
            { text: '完善本人信息', onPress: () => { this.props.history!.push(ROUTE_PATH.familyFilesEditSelf + '/' + '本人' + '&' + encodeUrlParam(JSON.stringify(this.state.person_data))); } },
            { text: '添加长者', onPress: () => { this.props.history!.push(ROUTE_PATH.familyFilesEditSelf + '/' + '长者' + '&' + encodeUrlParam(JSON.stringify(this.state.person_data))); } },
            { text: '添加子女家属', onPress: () => { this.props.history!.push(ROUTE_PATH.familyFilesEditSelf + '/' + '家属' + '&' + encodeUrlParam(JSON.stringify(this.state.person_data))); } },
            { text: '取消', onPress: () => { return; } },
        ]);
    }
    // 解除
    jiechu = (id: any) => {
        // console.log(id);
        AppServiceUtility.family_files_service.delete_user_relation_ship!(id)!
            .then((data: any) => {
                // console.log("解除成功了吗？？？？？", data);
                if (data === 'Success') {
                    location.reload();
                }
            });
    }
    render() {
        setMainFormTitle('家人档案');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.mine);
        });
        const { person_data, family_data } = this.state;
        return (
            <div style={{ padding: '0 10 0 10', position: 'relative' }}>
                <Button size='small' style={{ position: 'absolute', top: '3px', right: '3px' }} type="primary" onClick={this.add}>添加</Button>
                <List renderHeader={() => '本人信息'}>
                    <Card size="default" title="本人信息" extra={<a href={ROUTE_PATH.familyFilesEditSelf + '/' + '本人' + '&' + encodeUrlParam(JSON.stringify(person_data))}>完善</a>}>
                        <div style={{ float: 'left', width: '37%' }}>
                            <img style={{ height: '100%', width: '100%' }} src={person_data.personnel_info ? person_data.personnel_info.picture_list : 'http://dmimg.5054399.com/allimg/pkm/pk/22.jpg'} alt="" />
                        </div>
                        <div style={{ float: 'left', marginLeft: '20px', width: '52%', padding: '10px' }}>
                            <p>姓名：{person_data.name ? person_data.name : '暂无相关数据,请前往完善'}</p>
                            <p>性别：{person_data.personnel_info ? person_data.personnel_info.sex : '暂无相关数据,请前往完善'}</p>
                            <p>年龄：{person_data.personnel_info ? (new Date().getFullYear() - person_data.id_card.slice(6, 10)) : '暂无相关数据,请前往完善'}</p>
                        </div>
                    </Card>
                </List>
                <WhiteSpace />
                <WhiteSpace />
                <List renderHeader={() => '家属列表'} />
                {family_data.length > 0 ? family_data.map((item: any) => {
                    return (<div key={item.id}>
                        <SwipeAction
                            style={{ backgroundColor: 'gray' }}
                            autoClose={true}
                            right={[
                                {
                                    text: '解除',
                                    onPress: () => { this.jiechu(item.id); },
                                    style: { backgroundColor: '#F4333C', color: 'white' },
                                },
                            ]}
                            onOpen={() => { }}
                            onClose={() => { }}
                        >
                            <Card size="default" title="家属信息" extra={<a href={ROUTE_PATH.familyFilesEditSelf + '/' + item.relationShip_type + '&' + item.id + '&' + item.relationship_type_id + '&' + encodeUrlParam(JSON.stringify(item.family_person))}>完善</a>}>
                                <div style={{ float: 'left', width: '37%' }}>
                                    <img style={{ height: '100%', width: '100%' }} src={item.family_person.length > 0 ? item.family_person[0].personnel_info.picture_list : 'http://dmimg.5054399.com/allimg/pkm/pk/22.jpg'} alt="" />
                                </div>
                                <div style={{ float: 'left', marginLeft: '20px', width: '52%', padding: '10px' }}>
                                    <p>姓名：{item.family_person.length > 0 ? item.family_person[0].name : '暂无相关数据'}</p>
                                    <p>与本人关系：{item.relation_type_name ? item.relation_type_name : '暂无相关数据'}</p>
                                    <p>常住地址： {item.family_person.length > 0 ? item.family_person[0].personnel_info.address : '暂无相关数据'}</p>
                                </div>
                            </Card>
                        </SwipeAction>
                        <WhiteSpace />
                        <WhiteSpace />
                    </div>);
                })
                    :
                    <div style={{ width: '100%', textAlign: 'center' }}>暂未绑定相关亲属</div>
                }
            </div>
        );
    }
}

/**
 * 控件：家人档案控制器
 * 描述
 */
@addon('FamilyFilesView', '家人档案', '描述')
@reactControl(FamilyFilesView, true)
export class FamilyFilesViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}