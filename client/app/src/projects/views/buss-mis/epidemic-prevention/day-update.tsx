import { Button, Form, Radio, InputNumber } from "antd";
import { InputItem, Toast } from 'antd-mobile';
import { List, Picker, DatePicker } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Address } from "src/business/components/buss-components/address";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
// import { beforeUpload } from "src/projects/app/util-tool";
import './health-file.less';
const Item = List.Item;
const Brief = Item.Brief;
/**
 * 组件：防疫-每日登记状态
 */
export interface DayUpdateViewState extends ReactViewState {
    current_user?: any;
    family_item_list?: any;
    show_item?: any;
    day_info?: any;
    update?: any;
    photo?: any;
}

/**
 * 组件：防疫-每日登记
 * 描述
 */
export class DayUpdateView extends ReactView<DayUpdateViewControl, DayUpdateViewState> {
    // private photo: any = undefined
    // private param: any = undefined;
    upload_btn: any;
    constructor(props: any) {
        super(props);
        this.state = {
            current_user: {},
            family_item_list: [],
            show_item: false,
            day_info: {},
            update: false,
            photo: []
        };
    }
    componentWillMount = () => {
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                if (data && data.length > 0) {
                    this.setState({
                        current_user: data[0]
                    });
                }
            });
        AppServiceUtility.friends_circle.get_epidemic_prevention!()!
            .then((data: any) => {
                if (data.length > 0) {
                    let show_item = false;
                    if (data[0]['is_out'] === '是') {
                        show_item = true;
                    }
                    this.setState({
                        show_item,
                        day_info: data[0],
                        photo: data[0].photo ? data[0].photo : []
                    });
                }
            });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        this.setState({
            update: true
        });
        this.upload_btn.upload();
        // setTimeout(
        //     () => {
        //         this.setState({
        //             update: false
        //         });
        //     },
        //     2000);
    }

    // 处理时间
    formatDate = (date: any) => {
        const pad = (n: any) => n < 10 ? `0${n}` : n;
        const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
        const timeStr = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
        return `${dateStr} ${timeStr}`;
    }

    isOutChange = (e: any) => {
        if (e.target.value === "是") {
            this.setState({
                show_item: true
            });
        } else {
            this.setState({
                show_item: false
            });
        }
    }

    endUpImg = (e: any) => {
        let { form } = this.props;
        let photos = e;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                if (photos.length > 0) {
                    values = { ...values, 'photo': photos };
                    let daily_info = {
                        user_id: this.state.current_user.id,
                        is_work: values['is_work'],
                        temperature: String(values['temperature']),
                        stay_disaster_area: values['stay_disaster_area'],
                        contact_disaster_area_people: values['contact_disaster_area_people'],
                        symptom: values['symptom'],
                        trip: values['trip'],
                        trip_history: values['trip_history'],
                        backWork_address: values['backWork_address'],
                        transportation: values['transportation'],
                        is_out: values['is_out'],
                        backWork_transportation: values['backWork_transportation'],
                        vehicle_num: values['vehicle_num'],
                        reach_vehicle_num: values['reach_vehicle_num'],
                        reach_transportation: values['reach_transportation'],
                        place_of_departure: values['place_of_departure'],
                        stay_city: values['stay_city'],
                        is_back: values['is_back'],
                        arrive_time: values['arrive_time'],
                        unarrive_time: values['unarrive_time'],
                        has_partner: values['has_partner'],
                        partner_num: values['partner_num'],
                        trip_description: values['trip_description'],
                        photo: values['photo'],
                        begin_date: values['begin_date']
                    };
                    Toast.loading('正在提交', 3);
                    AppServiceUtility.friends_circle.update_day_info!(daily_info)!
                        .then((data: any) => {
                            if (data === 'Success') {
                                Toast.info('提交成功', 2, () => {
                                    this.setState({
                                        update: false
                                    });
                                    this.props.history!.goBack();
                                });
                            } else {
                                this.setState({
                                    update: false
                                });
                                Toast.fail('提交失败', 3);
                            }
                        })
                        .catch((err: any) => {
                            this.setState({
                                update: false
                            });
                            Toast.fail(err.message, 3);
                            console.info('报错', err.message);
                        });
                } else {
                    Toast.fail('请上传粤康码图片', 1);
                    this.setState({
                        update: false
                    });
                    return;
                }
            } else {
                Toast.fail('请填写完整信息', 2);
                this.setState({
                    update: false
                });
                return;
            }
        });

        // this.handleSubmit;
        // console.log(e, 'woshiniba');
        // this.photo = e;
        this.setState(
            {
                photo: e
            }
        );
    }

    onRef = (ref: any) => {
        this.upload_btn = ref;
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { current_user, show_item, day_info } = this.state;
        const transportation = [
            {
                label: '公用交通工具',
                value: '公用交通工具'
            },
            {
                label: '步行/骑车',
                value: '步行/骑车'
            },
            {
                label: '自驾',
                value: '自驾'
            },
            {
                label: '其他',
                value: '其他'
            }
        ];
        const backWork_transportation = [
            {
                label: '航空',
                value: '航空'
            },
            {
                label: '铁路',
                value: '铁路'
            },
            {
                label: '汽车',
                value: '汽车'
            },
            {
                label: '轮船',
                value: '轮船'
            },
            {
                label: '自驾',
                value: '自驾'
            }
        ];
        return (
            <div className='family-edit' style={{ marginBottom: '40px' }}>
                <Form onSubmit={this.handleSubmit}>
                    <List renderHeader={() => '基本信息'} className="my-list">
                        <Item extra={current_user.name}>姓名</Item>
                        <Item extra={current_user.id_card}>身份证号码</Item>
                        <Item extra={current_user.personnel_info && current_user.personnel_info.telephone ? current_user.personnel_info.telephone : ''}>手机号码</Item>
                        <Item extra={JSON.stringify(day_info) !== '{}' && day_info.modify_date ? day_info.modify_date : this.formatDate(new Date())}>上报时间</Item>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('is_work', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.is_work ? day_info.is_work : '',
                                        rules: [{ required: true, message: '请选择当天是否提供服务!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            当天是否提供服务
                        </Item>
                    </List>
                    <List renderHeader={() => '个人情况'} className="my-list">
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('temperature', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.temperature ? day_info.temperature : '',
                                        rules: [{ required: true, message: '请输入体温!' }],
                                    })(
                                        <InputNumber min={33} max={42} step={0.1} placeholder="请输入体温" style={{ width: '100%', marginTop: '5px' }} />
                                    )}
                                </Form.Item>}
                        >
                            体温
                        </Item>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('stay_disaster_area', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.stay_disaster_area ? day_info.stay_disaster_area : '',
                                        rules: [{ required: true, message: '请选择是否逗留或途径疫情重灾区!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            是否逗留或途径疫情重灾区
                        </Item>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('contact_disaster_area_people', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.contact_disaster_area_people ? day_info.contact_disaster_area_people : '',
                                        rules: [{ required: true, message: '请选择是否接触过疫情重灾区人员!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            是否接触过疫情重灾区人员
                        </Item>
                    </List>
                    <List renderHeader={() => '出现症状'}>
                        <Form.Item>
                            {getFieldDecorator('symptom', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.symptom ? day_info.symptom : '',
                                rules: [{ required: true, message: '请选择出现症状!' }],
                            })(
                                <Radio.Group>
                                    <Item
                                        thumb={<Radio value={'没有出现症状'} />}
                                    >
                                        没有出现症状
                                    </Item>
                                    <Item
                                        wrap={true}
                                        thumb={<Radio value={'感冒样症状：乏力、精神差、咳嗽、发烧、肌肉...'} />}
                                    >
                                        感冒样症状：乏力、精神差、咳嗽、发烧、肌肉...
                                    </Item>
                                    <Item
                                        thumb={<Radio value={'喘憋、呼吸急促'} />}
                                    >
                                        喘憋、呼吸急促
                                    </Item>
                                    <Item
                                        thumb={<Radio value={'恶心呕吐、腹泻'} />}
                                    >
                                        恶心呕吐、腹泻
                                    </Item>
                                    <Item
                                        thumb={<Radio value={'心慌、胸闷'} />}
                                    >
                                        心慌、胸闷
                                    </Item>
                                    <Item
                                        wrap={true}
                                        thumb={<Radio value={'结膜炎（红眼病表现：眼睛涩、红、分泌物）'} />}
                                    >
                                        结膜炎（红眼病表现：眼睛涩、红、分泌物）
                                    </Item>
                                </Radio.Group>
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Form.Item>
                            {getFieldDecorator('trip', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.trip ? day_info.trip : '无外出行程',
                                rules: [{ required: false, message: '请输入外出期间行程!' }],
                            })(
                                <InputItem maxLength={20} clear={true} placeholder="请输入（养老机构工作人员必填）" style={{ marginTop: '5px', width: 'none' }} >外出期间行程</InputItem>
                            )}
                        </Form.Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('trip_history', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.trip_history ? day_info.trip_history : '',
                                        rules: [{ required: true, message: '请选择一月以来是否有工作地/常住地以外的旅行史!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            一月以来是否有工作地/常住地以外的旅行史
                        </Item>
                        <Item>
                            复工居住地：
                            <Brief>
                                <Form.Item>
                                    {getFieldDecorator('backWork_address', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.backWork_address ? day_info.backWork_address : '',
                                        rules: [{ required: true, message: '请输入复工居住地!' }],
                                    })(
                                        <Address />
                                    )}
                                </Form.Item>
                            </Brief>
                        </Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Form.Item>
                            {getFieldDecorator('transportation', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.transportation ? day_info.transportation : '',
                                rules: [{ required: true, message: '请选择上下班交通方式!' }],
                            })(
                                <Picker data={transportation} cols={1} {...getFieldDecorator('transportation')} className="forss">
                                    <List.Item arrow="horizontal">上下班交通方式</List.Item>
                                </Picker>
                            )}
                        </Form.Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('is_out', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.is_out ? day_info.is_out : '',
                                        rules: [{ required: true, message: '请选择近期是否出佛山市（14天内）!' }],
                                    })(
                                        <Radio.Group onChange={this.isOutChange}>
                                            <Radio value={'是'}>是</Radio>
                                            <Radio value={'否'} defaultChecked={true}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            近期是否出佛山市（14天内）
                        </Item>
                    </List>
                    {show_item ?
                        <List renderHeader={() => '返工情况'}>
                            <Form.Item>
                                {getFieldDecorator('begin_date', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.begin_date ? new Date(day_info.begin_date) : '',
                                    // initialValue: JSON.stringify(day_info) !== '{}' && day_info.begin_date ? day_info.begin_date : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <DatePicker
                                        format={val => `${this.formatDate(val)}`}
                                        title="出发时间"
                                    >
                                        <List.Item extra="出发时间" arrow="horizontal">出发时间</List.Item>
                                    </DatePicker>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('backWork_transportation', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.backWork_transportation ? day_info.backWork_transportation : [backWork_transportation[0].value],
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <Picker data={backWork_transportation} cols={1} {...getFieldDecorator('backWork_transportation')} className="forss" >
                                        <List.Item arrow="horizontal">返工交通工具</List.Item>
                                    </Picker>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('vehicle_num', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.vehicle_num ? day_info.vehicle_num : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <InputItem maxLength={30} clear={true} placeholder="请填写航班号/火车车次/车票号码/轮船号/车牌号码" style={{ marginTop: '5px' }} />
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('reach_transportation', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.reach_transportation ? day_info.reach_transportation : [backWork_transportation[0].value],
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <Picker data={backWork_transportation} cols={1} {...getFieldDecorator('reach_transportation')} className="forss">
                                        <List.Item arrow="horizontal">到达交通工具</List.Item>
                                    </Picker>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('reach_vehicle_num', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.reach_vehicle_num ? day_info.reach_vehicle_num : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <InputItem maxLength={30} clear={true} placeholder="请填写航班号/火车车次/车票号码/轮船号/车牌号码" style={{ marginTop: '5px' }} />
                                )}
                            </Form.Item>
                            <Item>
                                返工前出发地：
                            <Brief>
                                    <Form.Item>
                                        {getFieldDecorator('place_of_departure', {
                                            initialValue: JSON.stringify(day_info) !== '{}' && day_info.place_of_departure ? day_info.place_of_departure : '',
                                            // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                        })(
                                            <Address />
                                        )}
                                    </Form.Item>
                                </Brief>
                            </Item>
                            <Item>
                                停留城市：
                            <Brief>
                                    <Form.Item>
                                        {getFieldDecorator('stay_city', {
                                            initialValue: JSON.stringify(day_info) !== '{}' && day_info.stay_city ? day_info.stay_city : '',
                                            // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                        })(
                                            <Address />
                                        )}
                                    </Form.Item>
                                </Brief>
                            </Item>
                            <Item
                                wrap={true}
                                extra={
                                    <Form.Item>
                                        {getFieldDecorator('is_back', {
                                            initialValue: JSON.stringify(day_info) !== '{}' && day_info.is_back ? day_info.is_back : '',
                                            // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                        })(
                                            <Radio.Group>
                                                <Radio value={'是'} defaultChecked={true}>是</Radio>
                                                <Radio value={'否'}>否</Radio>
                                            </Radio.Group>
                                        )}
                                    </Form.Item>
                                }
                            >
                                是否已回本市
                            </Item>
                            <Form.Item>
                                {getFieldDecorator('arrive_time', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.arrive_time ? new Date(day_info.arrive_time) : '',
                                    // initialValue: JSON.stringify(day_info) !== '{}' && day_info.arrive_time ? day_info.arrive_time : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <DatePicker
                                        format={val => `${this.formatDate(val)}`}
                                        title="到达日期"
                                    >
                                        <List.Item extra="已到达本市请选择" arrow="horizontal" >到达日期</List.Item>
                                    </DatePicker>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('unarrive_time', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.unarrive_time ? new Date(day_info.unarrive_time) : '',
                                    // initialValue: JSON.stringify(day_info) !== '{}' && day_info.unarrive_time ? day_info.unarrive_time : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <DatePicker
                                        format={val => `${this.formatDate(val)}`}
                                        title="预计到达日期"
                                    >
                                        <List.Item extra="未到达本市请选择" arrow="horizontal">预计到达日期</List.Item>
                                    </DatePicker>
                                )}
                            </Form.Item>
                            <Item
                                wrap={true}
                                extra={
                                    <Form.Item>
                                        {getFieldDecorator('has_partner', {
                                            initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_partner ? day_info.has_partner : '',
                                            // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                        })(
                                            <Radio.Group>
                                                <Radio value={'是'} defaultChecked={true}>是</Radio>
                                                <Radio value={'否'}>否</Radio>
                                            </Radio.Group>
                                        )}
                                    </Form.Item>
                                }
                            >
                                是否有同行人
                            </Item>
                            <Form.Item>
                                {getFieldDecorator('partner_num', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.partner_num ? day_info.partner_num : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <InputItem maxLength={18} clear={true} placeholder="有同行人请填写" style={{ marginTop: '5px' }} >同行人数量</InputItem>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('trip_description', {
                                    initialValue: JSON.stringify(day_info) !== '{}' && day_info.trip_description ? day_info.trip_description : '',
                                    // rules: [{ required: true, message: '请输入你的身份证号码!' }],
                                })(
                                    <InputItem maxLength={18} clear={true} placeholder="请填写" style={{ marginTop: '5px' }} >行程描述</InputItem>
                                )}
                            </Form.Item>
                        </List>
                        : ''}
                    <br />
                    <List renderHeader={() => '上传粤康码照片（大小小于2M，格式支持jpg/jpeg/png）：'}>
                        <FileUploadBtn is_submit={true} onRef={this.onRef} upload_type='防疫上报' upload_amount={1} list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.endUpImg} value={this.state.photo} />
                    </List>
                    <Button disabled={this.state.update} type="primary" size='large' htmlType="submit" style={{ position: 'fixed', bottom: '0px', width: '100%' }}>提交</Button>
                </Form>
            </div>
        );
    }
}

/**
 * 控件：防疫-每日登记控制器
 * 描述
 */
@addon('DayUpdateView', '防疫-每日登记', '描述')
@reactControl(Form.create<any>()(DayUpdateView), true)
export class DayUpdateViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}