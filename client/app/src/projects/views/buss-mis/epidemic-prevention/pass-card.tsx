import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Button } from 'antd';
import './pass-card.less';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { setMainFormTitle } from "src/business/util_tool";
import { getAge } from "src/projects/app/util-tool";
import { Modal } from 'antd-mobile';
const alert = Modal.alert;
/**
 * 组件：服务人员许可证控件状态
 */
export interface PassCardViewState extends ReactViewState {
    info?: any;
    color?: any;
    cardTitle?: any;
    state?:any;
}

/**
 * 组件：服务人员许可证控件
 */
export class PassCardView extends ReactView<PassCardViewControl, PassCardViewState> {
    public lv: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            info: {},
            color: 'gray',
            cardTitle: '',
            state: true
        };
    }

    componentDidMount = () => {
        // 获取当前登陆用户的信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((dataa: any) => {
                console.log(dataa,9999);
                if (dataa && dataa.length > 0) {
                    AppServiceUtility.friends_circle.get_epidemic_prevention_all!({ user_id: dataa[0].id }, 1, 1)!
                        .then((data: any) => {
                            console.log('请假记录', data);
                            if (data.result.length > 0) {
                                let color = '';
                                let cardTitle = '';
                                AppServiceUtility.friends_circle.get_epidemic_prevention!()!
                                    .then((datas: any) => {
                                        if (datas.length > 0) {
                                            if (datas[0].status === '高风险') {
                                                color = 'red';
                                                cardTitle = '高风险健康问题';
                                            } else if (datas[0].status === '中风险') {
                                                color = 'orange';
                                                cardTitle = '中风险健康问题';
                                            } else if (datas[0].status === '低风险') {
                                                color = 'green';
                                                cardTitle = '未发现健康问题';
                                            } else {
                                                color = 'gray';
                                                cardTitle = '已更新，待审核';
                                            }
                                            this.setState({
                                                info: data.result[0],
                                                color,
                                                cardTitle,
                                                state: false
                                            });
                                        } else {
                                            color = 'gray';
                                            cardTitle = '未更新今天健康状况';
                                            alert('未更新今天健康状况', <div> 请先前往更新今天健康状况！</div >, [
                                                { text: '取消', onPress: () => history.back() },
                                                { text: '确定前往', onPress: () => this.props.history!.push(ROUTE_PATH.DayUpdateHealth) },
                                            ]);
                                        }
                                    });
                            } else {
                                alert('您还未建立健康档案', <div> 请先前往建立健康档案！</div >, [
                                    { text: '取消', onPress: () => history.back() },
                                    { text: '确定前往', onPress: () => this.props.history!.push(ROUTE_PATH.healthFile) },
                                ]);
                            }
                        })
                        .catch((err) => {
                            console.info(err);
                        });
                }
            });

    }

    goHealthFile = () => {
        if (JSON.stringify(this.state.info) !== '{}') {
            this.props.history!.push(ROUTE_PATH.healthFile + '/' + this.state.info['id']);
        } else {
            this.props.history!.push(ROUTE_PATH.healthFile);
        }
    }

    updateTodayHealth = () => {
        this.props.history!.push(ROUTE_PATH.DayUpdateHealth);
    }
    render() {
        setMainFormTitle('服务人员健康许可证');
        const { info, color, cardTitle } = this.state;
        return (
            <div className='pass-card-content'>
                <div className='pass-title'><h2>南海区智慧养老综合服务管理平台居家养老服务许可证</h2></div>
                <div className='pass-time'><h4 style={{ marginBottom: 0 }}>{info.create_time ? info.create_time : (info.modify_date ? info.modify_date : '')}</h4></div>
                <div className='pass-card' style={{ backgroundColor: color }}>
                    <div className='pass-touxiang'>
                        <img src={info.user_info && info.user_info.personnel_info.picture && info.user_info.personnel_info.picture.length ? info.user_info.personnel_info.picture[0] : require('../../../../static/img/default_nan.jpg')} alt="头像" />
                    </div>
                    <div className='pass-card-title'><h2 style={{ color: 'white' }}>{cardTitle}</h2></div>
                    <div className='pass-info'>
                        <p>姓名：{info.user_name ? info.user_name : ''}</p>
                        <p>年龄：{info.id_card ? getAge(info.id_card) : ''}</p>
                        <p>服务证件号：{info.id_card ? info.id_card : ''}</p>
                        <p>所属机构：{info.org_name ? info.org_name : ''}</p>
                        <p>证件有效期：2021年12月31号</p>
                    </div>
                </div>
                <p>发证机构：佛山市南海区智慧养老综合服务管理平台</p>
                <Button disabled={this.state.state} type="primary" style={{ width: '100%' }} onClick={this.goHealthFile}>查看我的健康档案</Button>
                <Button disabled={this.state.state} style={{ width: '100%' }} onClick={this.updateTodayHealth}>更新今日健康状况</Button>
            </div>
        );
    }
}

/**
 * 组件：服务人员许可证控件
 * 控制服务人员许可证控件
 */
@addon('PassCardView', '服务人员许可证控件', '控制服务人员许可证控件')
@reactControl(PassCardView, true)
export class PassCardViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
