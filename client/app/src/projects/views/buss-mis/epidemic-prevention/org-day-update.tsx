import { Button, Form, Radio } from "antd";
import { InputItem, TextareaItem } from 'antd-mobile';
import { List, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import './health-file.less';
const Item = List.Item;
/**
 * 组件：防疫-机构每日登记状态
 */
export interface OrgDayUpdateViewState extends ReactViewState {
    current_user?: any;
    day_info?: any;
}

/**
 * 组件：防疫-机构每日登记
 * 描述
 */
export class OrgDayUpdateView extends ReactView<OrgDayUpdateViewControl, OrgDayUpdateViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            current_user: {},
            day_info: {}
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                if (data && data.length > 0) {
                    AppServiceUtility.friends_circle.get_org_day_info!({ org_id: data[0]['organization_id'] })!
                        .then((datas: any) => {
                            if (datas.length > 0) {
                                this.setState({
                                    day_info: datas[0]
                                });
                            }
                        });
                    this.setState({
                        current_user: data[0]
                    });
                }
            });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                if (JSON.stringify(this.state.day_info) !== '{}') {
                    values['id'] = this.state.day_info.id;
                }
                Toast.loading('正在提交', 3);
                AppServiceUtility.friends_circle.update_org_day_info!(values)!
                    .then((data: any) => {
                        if (data === 'Success') {
                            Toast.info('提交成功', 2, () => {
                                this.props.history!.goBack();
                            });
                        }
                    })
                    .catch((err) => {
                        Toast.fail(err.message, 3);
                        console.info(err);
                    });
            } else {
                Toast.fail('请填写信息完整', 2);
            }
        });

    }

    // 处理时间
    formatDate = (date: any) => {
        const pad = (n: any) => n < 10 ? `0${n}` : n;
        const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
        const timeStr = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
        return `${dateStr} ${timeStr}`;
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { current_user, day_info } = this.state;
        return (
            <div className='family-edit' style={{ marginBottom: '40px' }}>
                <Form onSubmit={this.handleSubmit}>
                    <List renderHeader={() => '基本信息'} className="my-list">
                        <Item extra={current_user.org_name}>机构名称</Item>
                        <Item wrap={true} multipleLine={true} extra={JSON.stringify(day_info) !== '{}' && day_info.modify_date ? day_info.modify_date : this.formatDate(new Date())}>上报时间</Item>
                    </List>
                    <List renderHeader={() => '因特殊原因下班后需离开机构的工作人数'} className="my-list">
                        <Form.Item>
                            {getFieldDecorator('leave_work_num', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.leave_work_num ? day_info.leave_work_num : '',
                                rules: [{ required: true, message: '请输入人数!' }],
                            })(
                                <InputItem maxLength={18} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} />
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('has_work_fever', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_work_fever ? day_info.has_work_fever : '',
                                        rules: [{ required: true, message: '请选择是否有工作人员出现发热情况!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            是否有工作人员出现发热情况
                        </Item>
                        <Form.Item>
                            {getFieldDecorator('fever_people_detail', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.fever_people_detail ? day_info.fever_people_detail : '',
                                // rules: [{ required: true, message: '请列明详细!' }],
                            })(
                                <TextareaItem rows={5} count={300} clear={true} placeholder="如有请列明详细" style={{ marginTop: '5px' }} />
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Form.Item>
                            {getFieldDecorator('new_old_num', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.new_old_num ? day_info.new_old_num : '',
                                rules: [{ required: true, message: '请输入新收老人数!' }],
                            })(
                                <InputItem maxLength={18} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} >新收老人数</InputItem>
                            )}
                        </Form.Item>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('has_hesuan', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_hesuan ? day_info.has_hesuan : '',
                                        rules: [{ required: true, message: '请选择新收老人是否有做核酸测试!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            新收老人是否有做核酸测试
                        </Item>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('has_geli', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_geli ? day_info.has_geli : '',
                                        rules: [{ required: true, message: '请选择新收老人是否有做隔离!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            新收老人是否有做隔离
                        </Item>
                    </List>
                    <List renderHeader={() => '因发热送医院治疗的老人数并反馈医院诊断情况'} className="my-list">
                        <Form.Item>
                            {getFieldDecorator('hospital_elder_num', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.hospital_elder_num ? day_info.hospital_elder_num : '',
                                rules: [{ required: true, message: '请输入诊断情况!' }],
                            })(
                                <TextareaItem rows={5} count={300} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} />
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => '离开机构老人人数及情况（如因住院、退住等原因离开机构）'} className="my-list">
                        <Form.Item>
                            {getFieldDecorator('leave_elder_num', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.leave_elder_num ? day_info.leave_elder_num : '',
                                rules: [{ required: true, message: '请输入离开机构老人人数及情况!' }],
                            })(
                                <TextareaItem clear={true} rows={5} count={300} placeholder="请输入" style={{ marginTop: '5px' }} />
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('back_has_hesuan', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.back_has_hesuan ? day_info.back_has_hesuan : '',
                                        rules: [{ required: true, message: '请选择是否有做核酸测试!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            因外出、住院等原因回机构老人是否有做核酸测试
                        </Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('back_has_geli', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.back_has_geli ? day_info.back_has_geli : '',
                                        rules: [{ required: true, message: '请选择是否有做隔离!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            因外出、住院等原因回机构老人是否有做隔离
                        </Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('all_has_hesuan', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.all_has_hesuan ? day_info.all_has_hesuan : '',
                                        rules: [{ required: true, message: '请选择是否全部做过核酸检测!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            在机构的所有工作人员和老人是否全部做过核酸检测
                        </Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('unhesuan_num', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.unhesuan_num ? day_info.unhesuan_num : '',
                                        rules: [{ required: true, message: '请输入未做过核酸检测的人数!' }],
                                    })(
                                        <InputItem maxLength={18} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} />
                                    )}
                                </Form.Item>
                            }
                        >
                            未做过核酸检测的人数
                        </Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('unhesuan_reason', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.unhesuan_reason ? day_info.unhesuan_reason : '',
                                        rules: [{ required: true, message: '请输入未做过核酸检测的人的具体原因!' }],
                                    })(
                                        <InputItem maxLength={18} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} />
                                    )}
                                </Form.Item>
                            }
                        >
                            未做过核酸检测的人的具体原因
                        </Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Item
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('has_elder_dead', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_elder_dead ? day_info.has_elder_dead : '',
                                        rules: [{ required: true, message: '请选择是否有机构的老人离世!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            是否有机构的老人离世
                        </Item>
                        <Form.Item>
                            {getFieldDecorator('dead_detail', {
                                initialValue: JSON.stringify(day_info) !== '{}' && day_info.dead_detail ? day_info.dead_detail : '',
                                // rules: [{ required: false, message: '请列明详细!' }],
                            })(
                                <InputItem maxLength={18} clear={true} placeholder="请输入" style={{ marginTop: '5px' }} >如有请列明详细</InputItem>
                            )}
                        </Form.Item>
                    </List>
                    <List renderHeader={() => ''}>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('new_has_to_high', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_elder_dead ? day_info.has_elder_dead : '',
                                        rules: [{ required: true, message: '请选择当天新入住老人、工作人员、探访人员在14天内是否有到过香港、澳门、新疆等高风险地区!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            当天新入住老人、工作人员、探访人员在14天内是否有到过香港、澳门、新疆等高风险地区
                        </Item>
                        <Item
                            wrap={true}
                            extra={
                                <Form.Item>
                                    {getFieldDecorator('to_high_has_hesuan', {
                                        initialValue: JSON.stringify(day_info) !== '{}' && day_info.has_elder_dead ? day_info.has_elder_dead : '',
                                        rules: [{ required: true, message: '请选择!' }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'} defaultChecked={true}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            }
                        >
                            14天内有到过香港、澳门、新疆等高风险地区的人员是否有做核酸检测和三个一（做一次健康告知（问询）、扫一次健康码、指导在健康码上进行14天连续自主健康申报）
                        </Item>
                    </List>
                    <Button type="primary" size='large' htmlType="submit" style={{ position: 'fixed', bottom: '0px', width: '100%' }}>提交</Button>
                </Form>
            </div>
        );
    }
}

/**
 * 控件：防疫-机构每日登记控制器
 * 描述
 */
@addon('OrgDayUpdateView', '防疫-机构每日登记', '描述')
@reactControl(Form.create<any>()(OrgDayUpdateView), true)
export class OrgDayUpdateViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}