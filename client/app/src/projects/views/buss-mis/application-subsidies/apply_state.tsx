import React from "react";
import { BaseReactElementState, CookieUtil, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Result, Steps } from "antd-mobile";
import { AppServiceUtility } from 'src/projects/app/appService';
import { User } from "src/business/models/user";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
const Step = Steps.Step;
const myImg = (src: any) => <img src={src} className="spe am-icon am-icon-md" alt="" />;
/**
 * 组件：补贴申请状态视图状态
 */
export interface ApplyStateViewState extends BaseReactElementState {
    data?: any;
    content?: any;
}

/**
 * 组件：补贴申请状态视图
 * 补贴申请状态
 */
export class ApplyStateView extends BaseReactElement<ApplyStateViewControl, ApplyStateViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            content: ''
        };
    }
    componentDidMount() {
        let currentUserId = CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).id;
        // console.log(currentUserId);
        AppServiceUtility.allowance_service.get_currentUser_allowance_list!({ 'id': currentUserId, 'allowance_type': '养老补贴' })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        data: data.result[0]
                    });
                    if (data.result[0].step_no === 1) {
                        this.setState({
                            content: '你的资料已经提交,请耐心等待村（居）委会审核结果。'
                        });
                    }
                    if (data.result[0].step_no === 2) {
                        this.setState({
                            content: '请耐心等待镇（街道）审核结果。'
                        });
                    }
                    if (data.result[0].step_no === 3) {
                        this.setState({
                            content: '请耐心等待区民政局审核结果。'
                        });
                    }
                    if (data.result[0].step_no === -1) {
                        this.setState({
                            content: '审核完成，请等待补贴发放。'
                        });
                    }
                }
            });
    }
    render() {
        const { data, content } = this.state;
        return (
            <div>
                <Result
                    img={myImg('https://gw.alipayobjects.com/zos/rmsportal/HWuSTipkjJRfTWekgTUG.svg')}
                    title="等待处理"
                    message={content}
                />
                <div style={{ fontSize: '30px', paddingLeft: '20px', paddingTop: '10px' }}>
                    <Steps size="lg" current={data.step_no !== -1 ? data.step_no : 4} >
                        <Step title="已申请" description="您已提交申请" />
                        <Step title="村（居）委会审核" description="等待村（居）委会审核" />
                        <Step title="镇（街道）审核" description="等待镇（街道）审核" />
                        <Step title="区民政局审核" description="等待区民政局审核" />
                        <Step title="审核完成" description="审核完成" />
                    </Steps>
                </div>
            </div>
        );
    }
}

/**
 * 控件：补贴申请状态视图控制器
 * 补贴申请状态
 */
@addon('ApplySuccessView', '补贴申请状态视图', '补贴申请状态')
@reactControl(ApplyStateView)
export class ApplyStateViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}