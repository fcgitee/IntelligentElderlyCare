import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Form } from "antd";
import { List, Button, WhiteSpace, InputItem, Toast } from 'antd-mobile';
import { remote } from "src/projects/remote";
import { ROUTE_PATH, decodeUrlParam, encodeUrlParam } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
// import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";

// const alert = Modal.alert;
/**
 * 组件：高原补贴长者信息录入状态
 */
export interface OldApplyCreditCardInsertState extends BaseReactElementState {
    credit_card_img?: any;
    credit_card_num?: any;
    params?: any;
}
/**
 * 组件：高原补贴长者信息录入
 * 描述
 */
export class OldApplyCreditCardInsert extends BaseReactElement<OldApplyCreditCardInsertControl, OldApplyCreditCardInsertState> {
    constructor(props: any) {
        super(props);
        this.state = {
            credit_card_img: [],
            credit_card_num: '',
            params: {}
        };
    }
    componentDidMount() {
        // console.log(this.props.match!.params.key);
        let params = JSON.parse(decodeUrlParam(this.props.match!.params.key));
        // console.log(params);
        this.setState({
            params
        });
        // // 判断是否登录
        // AppServiceUtility.personnel_service.get_user_session!()!
        //     .then((data: any) => {
        //         if (!data) {
        //             this.props.history!.push(ROUTE_PATH.login);
        //             return;
        //         }
        //     });
    }

    submit = (): boolean => {
        const { params, credit_card_num, credit_card_img } = this.state;
        if (params.credit_card_num !== credit_card_num) {
            Toast.fail('你的银行卡号码前步骤填写的不一致', 1);
            return false;
        } else {
            let param = { ...params, credit_card_img };
            this.props.history!.push(ROUTE_PATH.applyPersonelInfoInsert + '/' + encodeUrlParam(JSON.stringify(param)));
        }
        return false;
    }

    imgChange = (e: any) => {
        // console.log(e);
        this.setState({
            credit_card_img: e
        });
    }

    CreditCardChange = (e: any) => {
        this.setState({
            credit_card_num: e
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <div>
                <Form>
                    <List renderHeader={() => '请上传银行卡照片（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('credit_card_img', {
                            initialValue: this.state.credit_card_img,
                            rules: [{
                                required: true,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.imgChange} />
                        )}
                    </List>
                    {/* <List renderHeader={'请上传银行卡照片'} className="my-list">
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.imgChange} />
                    </List> */}
                    <div>{
                        this.state.credit_card_img.length > 0 ?
                            this.state.credit_card_img.map((item: any) => {
                                <img src={item} alt="" style={{ width: '100%', height: '300px' }} />;
                            })
                            :
                            ''
                    }</div>
                    <List renderHeader={'确认银行卡号码'}>
                        <InputItem clear={true} placeholder="请输入银行卡号码" onChange={this.CreditCardChange} value={this.state.credit_card_num} type="bankCard">确认银行卡号码</InputItem>
                    </List>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.submit}>下一步</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 控件：高原补贴长者信息录入控制器
 * 描述
 */
@addon('OldApplyCreditCardInsert', '高原补贴长者信息录入', '高原补贴长者信息录入')
@reactControl(Form.create<any>()(OldApplyCreditCardInsert), true)
export class OldApplyCreditCardInsertControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}