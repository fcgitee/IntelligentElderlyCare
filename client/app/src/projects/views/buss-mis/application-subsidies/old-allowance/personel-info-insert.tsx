import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl, CookieUtil } from "pao-aop-client";
import { addon } from "pao-aop";
import { Form } from "antd";
import { List, Button, WhiteSpace, Toast, Modal } from 'antd-mobile';
import { remote } from "src/projects/remote";
// import { beforeUpload } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
// import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { AppServiceUtility } from 'src/projects/app/appService';
import { request } from "src/business/util_tool";
const alert = Modal.alert;

// const alert = Modal.alert;
/**
 * 组件：高原补贴长者信息录入状态
 */
export interface OldApplyPersonelInfoInsertState extends BaseReactElementState {
    photo?: any;
    id_card?: any;
    params?: any;
    user_info?: any;
    type?: any;
    disable?: any;
    is_btn_show?: boolean;
    is_send?: boolean;
}
/**
 * 组件：高原补贴长者信息录入
 * 描述
 */
export class OldApplyPersonelInfoInsert extends BaseReactElement<OldApplyPersonelInfoInsertControl, OldApplyPersonelInfoInsertState> {
    constructor(props: any) {
        super(props);
        this.state = {
            photo: [],
            id_card: '',
            params: {},
            user_info: {},
            type: '他人协助',
            disable: false,
            is_btn_show: false,
            is_send: false,
        };
    }
    componentDidMount() {
        // let params = JSON.parse(decodeUrlParam(this.props.match!.params.key));
        // // console.log(params);
        // this.setState({
        //     params
        // });
        // 判断是否登录
        // AppServiceUtility.personnel_service.get_user_session!()!
        //     .then((data: any) => {
        //         if (!data) {
        //             this.props.history!.push(ROUTE_PATH.login);
        //             return;
        //         }
        //     });
        // if (CookieUtil.read('user_info') !== undefined) {
        //     // cookies 读取数据
        //     let user_info: any = CookieUtil.read('user_info');
        //     this.setState({
        //         photo: user_info.photo
        //     });
        // }
    }

    submit = () => {
        const { photo, type, is_send } = this.state;
        if (is_send === true) {
            Toast.fail('请勿重复操作！');
            return;
        }
        // 从cookies读取数据
        let user_info: any = CookieUtil.read('user_info');
        if (!user_info || !user_info.name || !user_info.id_card_num) {
            Toast.fail('缺少长者数据，请返回上一步重新录入！');
            return;
        }
        if (photo.length === 0) {
            Toast.fail('请上传人脸照片！');
            return;
        }
        this.setState(
            {
                is_send: true,
                user_info: user_info,
                disable: true,
            },
            () => {
                user_info = { ...user_info, photo, type };
                Toast.loading('认证中...');
                request(this, AppServiceUtility.allowance_service.apply_old_age_allowance!(user_info))
                    .then((data: any) => {
                        Toast.hide();
                        this.setState({
                            is_send: false,
                        });
                        if (data[0].result === '认证成功') {
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                        this.props.history!.push(ROUTE_PATH.applyReadinClauseWx);
                                    }
                                },
                            ]);
                        } else if (data[0].result === '你本年度已完成认证，无需重复操作') {
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                        this.props.history!.push(ROUTE_PATH.applyReadinClauseWx);
                                    }
                                },
                            ]);
                        } else if (data[0].result !== '津贴名单中没有此人员的相关信息，认证失败') {
                            alert('认证结果', `${data[0].result}，你可以选择点击下方按钮进行一键认证`, [
                                {
                                    text: '确认', onPress: () => {
                                        this.setState({
                                            is_btn_show: true
                                        });
                                    }
                                },
                            ]);
                        } else {
                            // 津贴名单中没有此人员的相关信息，认证失败
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                        this.setState({
                                            disable: false,
                                        });
                                        CookieUtil.save('user_info', JSON.stringify(this.state.user_info));
                                        return;
                                    }
                                },
                            ]);
                        }
                    })
                    .catch(error => {
                        // Toast.hide();
                        Toast.info('认证服务不稳定，请稍候再试！');
                        this.setState({
                            is_send: false,
                            disable: false,
                        });
                        console.error(error.message);
                    });
                // finally(() => {
                //     Toast.hide();
                //     this.setState({
                //         is_send: false,
                //     });
                // });
                CookieUtil.remove("user_info");
            }
        );
    }

    imgChange = (e: any) => {
        // console.log(e);
        this.setState({
            photo: e
        });
        // // 从cookies读取数据
        // let user_info: any = CookieUtil.read('user_info');
        // let photo = e;
        // if (user_info.photo) {
        //     user_info.photo = photo;
        //     CookieUtil.save('user_info', JSON.stringify(user_info));
        // } else {
        //     let param = { ...user_info, photo };
        //     CookieUtil.save('user_info', JSON.stringify(param));
        // }
    }

    handMade = () => {
        const { user_info, photo, is_send } = this.state;
        if (is_send === true) {
            Toast.fail('请勿重复操作！');
            return;
        }
        if (!user_info || !user_info.name || !user_info.id_card_num) {
            Toast.fail('缺少长者数据，请返回上一步重新录入！');
            return;
        }
        if (photo.length === 0) {
            Toast.fail('请上传人脸照片！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let param: any = {
                    name: user_info.name,
                    id_card_num: user_info.id_card_num,
                    photo: photo,
                };
                Toast.loading('认证中...');
                request(this, AppServiceUtility.allowance_service.manual_authentication!(param))
                    .then((data: any) => {
                        Toast.hide();
                        if (data[0].result === '认证成功') {
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                        this.setState(
                                            {
                                                is_btn_show: false,
                                            },
                                            () => {
                                                this.props.history!.push(ROUTE_PATH.applyReadinClauseWx);
                                            }
                                        );
                                    }
                                },
                            ]);
                        } else {
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                        this.setState({
                                            is_send: false
                                        });
                                        // this.props.history!.push(ROUTE_PATH.applyReadinClauseWx);
                                    }
                                },
                            ]);
                        }
                    })
                    .catch(error => {
                        Toast.info('认证服务不稳定，请稍候再试！');
                        this.setState({
                            is_send: false
                        });
                        console.error(error.message);
                    });
            }
        );
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <div>
                <Form>
                    <List renderHeader={() => '请上传人脸照片或拍照（大小小于10M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('photo', {
                            initialValue: this.state.photo,
                            rules: [{
                                required: true,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.imgChange} upload_amount={1} />
                        )}
                    </List>
                    {/* <List renderHeader={'请上传人脸照片（10-20kb）'} className="my-list">
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.imgChange} />
                    </List> */}
                    {/* <div>{
                        this.state.photo.length > 0 ?
                            this.state.photo.map((item: any) => {
                                <img src={item} alt="" style={{ width: '100%', height: '300px' }} />;
                            })
                            :
                            ''
                    }</div> */}
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    {this.state.is_btn_show ? <Button type="primary" onClick={this.handMade}>一键认证</Button> : null}
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <Button type="primary" disabled={this.state.disable} style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.submit}>开始认证</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 控件：高原补贴长者信息录入控制器
 * 描述
 */
@addon('OldApplyPersonelInfoInsert', '高原补贴长者信息录入', '高原补贴长者信息录入')
@reactControl(Form.create<any>()(OldApplyPersonelInfoInsert), true)
export class OldApplyPersonelInfoInsertControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}