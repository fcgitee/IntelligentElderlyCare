import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl, CookieUtil } from "pao-aop-client";
import { addon } from "pao-aop";
import { Form } from "antd";
import { InputItem, List, Button, WhiteSpace, Toast } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
import { User } from "src/business/models/user";
import { AppServiceUtility } from 'src/projects/app/appService';
import { request } from "src/business/util_tool";

// const alert = Modal.alert;
/**
 * 组件：高原补贴长者信息录入状态
 */
export interface OldApplyInfoInsertState extends BaseReactElementState {
    name?: any;
    sex?: any;
    phone?: any;
    id_card_num?: any;
    age?: any;
    birth_day?: any;
    id_card_address?: any;
    house_address?: any;
    bank?: any;
    credit_card_num?: any;
    current_operator?: any;
    is_send?: boolean;
}
/**
 * 组件：高原补贴长者信息录入
 * 描述
 */
export class OldApplyInfoInsert extends BaseReactElement<OldApplyInfoInsertControl, OldApplyInfoInsertState> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            sex: '',
            phone: '',
            id_card_num: '',
            age: '',
            birth_day: '',
            id_card_address: '',
            house_address: '',
            bank: '',
            credit_card_num: '',
            current_operator: '',
            is_send: false,
        };
    }
    componentDidMount() {
        // 判断是否登录
        // AppServiceUtility.personnel_service.get_user_session!()!
        //     .then((data: any) => {
        //         if (!data) {
        //             this.props.history!.push(ROUTE_PATH.login);
        //             return;
        //         }
        //     });
        // 从cookies取出数据
        // console.log(CookieUtil.read('user_info'));
        if (CookieUtil.read('user_info') !== undefined) {
            let user_info: any = CookieUtil.read('user_info');
            this.setState({
                name: user_info.name,
                // sex: user_info.sex,
                id_card_num: user_info.id_card_num,
                age: user_info.age,
                birth_day: user_info.birth_day,
                current_operator: user_info.current_operator
            });
        }

    }

    submit = () => {
        let currentUserId = CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).id : '';
        const { name, age, birth_day, id_card_num, current_operator, is_send } = this.state;
        if (is_send) {
            return;
        }
        if (!name) {
            Toast.fail('请输入姓名', 1);
            return;
        }
        if (!id_card_num) {
            Toast.fail('请填写你的身份证', 1);
            return;
        }
        // if (!sex) {
        //     Toast.fail('请选择你的性别', 1);
        //     return false;
        // }
        // if (!bank) {
        //     Toast.fail('请填写收款银行', 1);
        //     return false;
        // }
        // if (!credit_card_num) {
        //     Toast.fail('请填写你的银行卡号码', 1);
        //     return false;
        // }
        // if (!phone) {
        //     Toast.fail('请填写你的联系电话', 1);
        //     return false;
        // }
        if (id_card_num.length < 18) {
            Toast.fail('你的身份证号码格式不正确', 2);
            return;
        }
        // if (Number(age) < 70) {
        //     Toast.fail('你的年龄未达到申请要求（70周岁及以上）', 2);
        //     return false;
        // }
        // if (id_card_address.indexOf('佛山市南海区') === -1) {
        //     Toast.fail('你的户籍不是佛山市南海区，不符合要求', 2);
        //     return false;
        // }

        // 将数据存到cookie
        let user_info: any = {};
        if (CookieUtil.read('user_info') !== undefined) {
            user_info = CookieUtil.read('user_info');
            user_info.name = name;
            user_info.id_card_num = id_card_num;
            user_info.age = age;
            user_info.birth_day = birth_day;
            user_info.current_operator = current_operator;
        } else {
            // 第一次进入页面
            user_info = {
                Operator: currentUserId,
                name,
                id_card_num,
                age,
                birth_day,
                current_operator
            };
        }
        Toast.loading('长者信息核查中...');
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.allowance_service.check_elder_exists!({ name, id_card_num }))
                    .then((data: any) => {
                        Toast.hide();
                        if (data === 'Success') {
                            CookieUtil.save('user_info', JSON.stringify(user_info));
                            this.props.history!.push(ROUTE_PATH.applyPersonelInfoInsert);
                        } else {
                            Toast.fail(data);
                            this.setState({
                                is_send: false
                            });
                        }
                    })
                    .catch(error => {
                        Toast.hide();
                        this.setState({
                            is_send: false
                        });
                        console.error(error.message);
                    });
            }
        );
        return;
    }
    nameChange = (value: any) => {
        this.setState({
            // 去除两边空格
            name: value.replace(/(^\s*)|(\s*$)/g, "")
        });
    }
    sexChange = (e: any) => {
        this.setState({
            sex: e.target.value
        });
    }
    // phoneChange = (value: any) => {
    //     // console.log(value);
    //     this.setState({
    //         phone: value
    //     });
    // }

    idCardChange = (value: any) => {
        // 年龄
        var date = new Date();
        let age = date.getFullYear() - value.slice(6, 10);
        // 生日
        let birth_day = value.slice(10, 14);
        // 去除两边空格
        value = value.replace(/(^\s*)|(\s*$)/g, "");
        this.setState({
            id_card_num: value,
            age,
            birth_day
        });
    }

    // idCardAddressChange = (value: any) => {
    //     // console.log(value);
    //     // console.log(value.indexOf('南海'));
    //     this.setState({
    //         id_card_address: value
    //     });
    // }

    // houseAddressChange = (value: any) => {
    //     // console.log(value);
    //     this.setState({
    //         house_address: value
    //     });
    // }

    // bankChange = (value: any) => {
    //     // console.log(value);
    //     this.setState({
    //         bank: value
    //     });
    // }
    // creditCardChange = (value: any) => {
    //     // console.log(value);
    //     this.setState({
    //         credit_card_num: value
    //     });
    // }
    operatorChange = (value: any) => {
        this.setState({
            current_operator: value
        });
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <Form>
                    <List>
                        <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入申请人姓名" onChange={this.nameChange} value={this.state.name}>长者姓名</InputItem>
                        <InputItem {...getFieldProps('id_card_num')} clear={true} placeholder="请输入身份证号码" onChange={this.idCardChange} value={this.state.id_card_num}>身份证号码</InputItem>
                        {/* <InputItem editable={false} labelNumber={10}>
                            性别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <Radio.Group defaultValue={'男'} value={this.state.sex} onChange={this.sexChange}>
                                <Radio value={'男'} defaultChecked={true}>男</Radio>
                                <Radio value={'女'}>女</Radio>
                            </Radio.Group>
                        </InputItem> */}
                        <InputItem {...getFieldProps('birth_day')} value={this.state.birth_day ? this.state.birth_day : ''} editable={false}>生日</InputItem>
                        <InputItem {...getFieldProps('age')} value={this.state.age ? this.state.age : ''} editable={false}>年龄</InputItem>
                        {/* <InputItem {...getFieldProps('id_card_address')} placeholder="例如（佛山市南海区xxx）" onChange={this.idCardAddressChange} value={this.state.id_card_address}>户籍地址</InputItem>
                        <InputItem {...getFieldProps('house_address')} clear={true} placeholder="请输入家庭地址" onChange={this.houseAddressChange} value={this.state.house_address}>家庭地址</InputItem> */}
                    </List>
                    <WhiteSpace size="lg" />
                    {/* <List>
                        <InputItem {...getFieldProps('bank')} clear={true} placeholder="请输入收款银行" onChange={this.bankChange} value={this.state.bank}>收款银行</InputItem>
                        <InputItem {...getFieldProps('credit_card_num')} clear={true} placeholder="请输入银行卡号码" onChange={this.creditCardChange} value={this.state.credit_card_num} type="bankCard">银行卡号码</InputItem>
                    </List> */}
                    {/* <List>
                        <InputItem {...getFieldProps('phone')} clear={true} placeholder="请输入联系电话" onChange={this.phoneChange} value={this.state.phone}>联系电话</InputItem>
                    </List> */}
                    <List renderHeader={() => '当前操作人（非必填）'}>
                        <InputItem {...getFieldProps('current_operator')} clear={true} placeholder="请输入当前操作人姓名" onChange={this.operatorChange} value={this.state.current_operator}>当前操作人</InputItem>
                    </List>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.submit}>下一步</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 控件：高原补贴长者信息录入控制器
 * 描述
 */
@addon('OldApplyInfoInsert', '高原补贴长者信息录入', '高原补贴长者信息录入')
@reactControl(Form.create<any>()(OldApplyInfoInsert), true)
export class OldApplyInfoInsertControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}