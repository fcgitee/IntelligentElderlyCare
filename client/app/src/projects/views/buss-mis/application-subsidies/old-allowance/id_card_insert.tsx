import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl, CookieUtil } from "pao-aop-client";
import { addon } from "pao-aop";
import { Form } from "antd";
import { List, Button, WhiteSpace, Toast, DatePicker, Flex, Checkbox } from 'antd-mobile';
import { remote } from "src/projects/remote";
// import { beforeUpload } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
const AgreeItem = Checkbox.AgreeItem;

// import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";

// const alert = Modal.alert;
/**
 * 组件：高原补贴长者信息录入状态
 */
export interface OldApplyIdCardInsertState extends BaseReactElementState {
    id_card_img?: any;
    id_card?: any;
    params?: any;
    card_start_date?: any;
    card_end_date?: any;
    start_show?: any;
    end_show?: any;
    Status?: any;
}
/**
 * 组件：高原补贴长者信息录入
 * 描述
 */
export class OldApplyIdCardInsert extends BaseReactElement<OldApplyIdCardInsertControl, OldApplyIdCardInsertState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id_card_img: [],
            id_card: '',
            params: {},
            card_start_date: '',
            card_end_date: '',
            start_show: '',
            end_show: '',
            Status: false
        };
    }
    componentDidMount() {
        // let params = JSON.parse(this.props.match!.params.key);
        // // console.log(this.props.match!.params.key);
        // this.setState({
        //     params
        // });
        // 判断是否登录
        // AppServiceUtility.personnel_service.get_user_session!()!
        //     .then((data: any) => {
        //         if (!data) {
        //             this.props.history!.push(ROUTE_PATH.login);
        //             return;
        //         }
        //     });
        // }
        // 从cookies取出数据
        // console.log(CookieUtil.read('user_info'));
        // user_info.sex = '性别';
        // CookieUtil.save('user_info', JSON.stringify(user_info));
        // let user_info2:any = CookieUtil.read('user_info');
        // let card_end_date = 
        if (CookieUtil.read('user_info') !== undefined) {
            let user_info: any = CookieUtil.read('user_info');
            if (user_info.card_end_date && user_info.card_start_date) {
                if (user_info.card_end_date === '00000000') {
                    // 开始时间
                    let start = user_info.card_start_date;
                    // console.log(start);
                    // console.log(start.slice(0, 4));
                    // console.log(start.slice(4, 6));
                    // console.log(start.slice(6, 8));
                    let start_day = start.slice(0, 4) + '-' + start.slice(4, 6) + '-' + start.slice(6, 8);
                    // console.log(start_day);
                    let start_show: any = new Date(start_day);
                    // console.log(start_show);
                    // console.log(111111111111);
                    this.setState(
                        {
                            id_card_img: user_info.id_card_img,
                            // id_card: user_info.id_card_num,
                            card_start_date: user_info.card_start_date,
                            card_end_date: user_info.card_end_date,
                            start_show: start_show,
                            Status: true
                        },
                        () => {
                            // console.log(this.state.Status);
                        }
                    );

                } else {
                    // 开始时间
                    let start = user_info.card_start_date;
                    // console.log(start);
                    // console.log(start.slice(0, 4));
                    // console.log(start.slice(4, 6));
                    // console.log(start.slice(6, 8));
                    let start_day = start.slice(0, 4) + '-' + start.slice(4, 6) + '-' + start.slice(6, 8);
                    // console.log(start_day);
                    let start_show: any = new Date(start_day);
                    // console.log(start_show);
                    // 结束时间
                    let end = user_info.card_end_date;
                    let end_day = end.slice(0, 4) + '-' + end.slice(4, 6) + '-' + end.slice(6, 8);
                    var end_show: any = new Date(end_day);
                    this.setState({
                        id_card_img: user_info.id_card_img,
                        // id_card: user_info.id_card_num,
                        card_start_date: user_info.card_start_date,
                        card_end_date: user_info.card_end_date,
                        start_show: start_show,
                        end_show: end_show,
                        Status: false
                    });
                }
            }
        }

    }

    submit = (): boolean => {
        const { card_start_date, card_end_date, id_card_img } = this.state;
        // console.log(id_card_img.length);
        // 从cookies读取数据
        let user_info: any = CookieUtil.read('user_info');
        // 判断
        if (id_card_img.length === 0) {
            Toast.fail('请上传身份证照片', 1);
            return false;
        }
        if (!card_start_date) {
            Toast.fail('请选择身份证有效期的开始时间', 1);
            return false;
        }
        if (!card_end_date) {
            Toast.fail('请选择身份证有效期的结束时间', 1);
            return false;
        }
        // if (user_info.id_card_num !== id_card) {
        //     Toast.fail('你的身份证号码与上一步填写的不一致', 1);
        //     return false;
        // }
        // 处理
        if (user_info.card_end_date || user_info.card_start_date || user_info.id_card_img) {
            user_info.card_end_date = card_end_date;
            user_info.card_start_date = card_start_date;
            user_info.id_card_img = id_card_img;
            CookieUtil.save('user_info', JSON.stringify(user_info));
            this.props.history!.push(ROUTE_PATH.applyPersonelInfoInsert);
        } else {
            let param = { ...user_info, card_end_date, card_start_date, id_card_img };
            // console.log("第一次进这个页面");
            CookieUtil.save('user_info', JSON.stringify(param));
            this.props.history!.push(ROUTE_PATH.applyPersonelInfoInsert);
        }
        // let param = { ...params, card_end_date, card_start_date, id_card_img };
        // // console.log(param);
        // // 将数据存到cookie
        // CookieUtil.save('user_info', JSON.stringify(param));
        // this.props.history!.push(ROUTE_PATH.applyPersonelInfoInsert + '/' + encodeUrlParam(JSON.stringify(param)));
        return false;
    }

    imgChange = (e: any) => {
        // console.log(e);
        this.setState({
            id_card_img: e
        });
    }

    // idCardChange = (e: any) => {
    //     this.setState({
    //         id_card: e
    //     });
    // }
    CardStartChange = (e: any) => {
        let dateArr = this.formatDate(e).split(' ')[0].split('-');
        let dateStr = dateArr[0] + dateArr[1] + dateArr[2];
        // console.log(dateStr);
        this.setState({
            card_start_date: dateStr,
            start_show: e
        });
    }
    CardEndChange = (e: any) => {
        // console.log(e);
        let dateArr = this.formatDate(e).split(' ')[0].split('-');
        let dateStr = dateArr[0] + dateArr[1] + dateArr[2];
        this.setState({
            card_end_date: dateStr,
            end_show: e
        });
    }
    formatDate = (date: Date) => {
        /* eslint no-confusing-arrow: 0 */
        const pad = (n: any) => n < 10 ? `0${n}` : n;
        const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
        const timeStr = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
        return `${dateStr} ${timeStr}`;
    }
    clickChange = (e: any) => {
        // // console.log(e.target.value);
        this.setState({
            card_end_date: '00000000'
        });
    }
    onClick() {
        this.setState({
            Status: !this.state.Status
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <div>
                <Form>
                    <List renderHeader={() => '请上传身份证照片（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('id_card_img', {
                            initialValue: this.state.id_card_img,
                            rules: [{
                                required: true,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.imgChange} />
                        )}
                    </List>
                    {/* <List renderHeader={'请上传身份证照片'} className="my-list">
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.imgChange} />
                    </List> */}
                    {/* <List renderHeader={'确认身份证号码'}> */}
                        {/* <InputItem clear={true} placeholder="请输入身份证号码" onChange={this.idCardChange} value={this.state.id_card} labelNumber={7} >身份证号码</InputItem> */}
                        {/* <InputItem clear={true} placeholder="请输入身份证开始日期" onChange={this.CardStartChange} value={this.state.card_start_date} labelNumber={7}>身份证开始日期</InputItem>
                        <InputItem clear={true} placeholder="请输入身份证结束日期" onChange={this.CardEndChange} value={this.state.card_end_date} labelNumber={7}>身份证结束日期</InputItem> */}
                    {/* </List> */}
                    <List renderHeader={'身份证有限期开始日期'}>
                        <DatePicker
                            mode="date"
                            title="选择时间"
                            value={this.state.start_show}
                            onChange={this.CardStartChange}
                            minDate={new Date(1900, 1, 1, 0, 0, 0)}
                            maxDate={new Date(2100, 1, 1, 0, 0, 0)}
                        >
                            <List.Item arrow="horizontal">开始时间</List.Item>
                        </DatePicker>
                    </List>
                    <List renderHeader={'身份证有限期结束日期(如为有限期为长期，请选择长期)'}>
                        <DatePicker
                            mode="date"
                            title="选择时间"
                            value={this.state.end_show}
                            onChange={this.CardEndChange}
                            minDate={new Date(1900, 1, 1, 0, 0, 0)}
                            maxDate={new Date(2100, 1, 1, 0, 0, 0)}
                        >
                            <List.Item arrow="horizontal">结束时间</List.Item>
                        </DatePicker>
                        <Flex>
                            <Flex.Item>
                                <AgreeItem onClick={() => this.onClick()} data-seed="logId" checked={this.state.Status} onChange={this.clickChange}>
                                    长期
                            </AgreeItem>
                            </Flex.Item>
                        </Flex>
                    </List>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.submit}>下一步</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 控件：高原补贴长者信息录入控制器
 * 描述
 */
@addon('OldApplyIdCardInsert', '高原补贴长者信息录入', '高原补贴长者信息录入')
@reactControl(Form.create<any>()(OldApplyIdCardInsert), true)
export class OldApplyIdCardInsertControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}