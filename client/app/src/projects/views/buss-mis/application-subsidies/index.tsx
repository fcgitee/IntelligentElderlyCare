import { Col, Form, Row } from "antd";
import { Card, ListView } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { LoadingEffectView } from 'src/business/views/loading-effect';
import './index.less';
// const alert = Modal.alert;
// const alert = Modal.alert;

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

/**
 * 组件：补贴申请状态
 */
export interface ApplicationSubsidiesViewState extends BaseReactElementState {
    data?: any;
    dataSource?: any;
    height?: any;
    isLoad?: boolean; // 是否显示加载效果
}
/**
 * 组件：补贴申请
 * 描述
 */
export class ApplicationSubsidiesView extends BaseReactElement<ApplicationSubsidiesViewControl, ApplicationSubsidiesViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            data: [],
            dataSource: ds,
            height: document.documentElement!.clientHeight,
            isLoad: true,
        };
    }
    goDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.applicationDetail + '/' + id);
    }

    // getList = () => {
    //     AppServiceUtility.allowance_service.get_allowance_project_list_all!({})!
    //         .then((data: any) => {
    //             // console.log(data.result);
    //             this.setState({
    //                 data: data.result
    //             });
    //         });
    // }
    componentDidMount() {
        // alert('你是要认证哪种补贴', <div />, [
        //     { text: '高龄津贴', onPress: () => this.oldapply() },
        //     // { text: '长者补贴', onPress: () => this.apply() },
        //     // { text: '返回', onPress: () => history.back() },
        // ]);
        AppServiceUtility.allowance_service.get_allowance_project_list_all!({})!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    data: data.result,
                    isLoad: false,
                });
            });
    }
    // apply = () => {
    //     alert('你是要认证/查看认证结果', <div />, [
    //         { text: '申请补贴', onPress: () => this.getList() },
    //         { text: '查看审核结果', onPress: () => this.props.history!.push(ROUTE_PATH.applyState) },
    //         // { text: '返回', onPress: () => history.back() },
    //     ]);
    // }
    // oldapply = () => {
    //     alert('你是要认证/查看是否发放', <div />, [
    //         { text: '认证', onPress: () => this.props.history!.push(ROUTE_PATH.applyReadinClause)},
    //         { text: '查看是否发放', onPress: () => this.props.history!.push(ROUTE_PATH.oldapplyState) },
    //         { text: '返回', onPress: () =>  this.props.history!.push(ROUTE_PATH.applicationSubsidies)},
    //     ]);
    // }
    render() {
        const { data, dataSource } = this.state;
        setMainFormTitle("居家养老补助");
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.home);
        });
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten' onClick={() => this.goDetails(owData.id)} key={owData.id}>
                        <Row type='flex' justify='center'>
                            <img src={owData.allowance_project_photo[0]} alt="" style={{ height: '180px', width: '100%' }} />
                        </Row>
                        <Row type='flex' justify='center' style={{ fontSize: '20px', fontWeight: 'bold' }}>
                            <Col span={24}>{owData.allowance_project_name + '(' + owData.allowance_project_type + ')'}</Col>
                        </Row>
                        <Row type='flex' justify='center'>
                            <Col span={24}><span style={{ color: 'red', fontSize: '25px', fontWeight: 'bold' }}>￥{owData.allowance_project_money}</span>&nbsp;&nbsp;元每月&nbsp;&nbsp;&nbsp;已有{owData.sq_num ? owData.sq_num : 0}人申请</Col>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <div className='ylbz-list'>
                {this.state.isLoad && <LoadingEffectView />}
                {data.length > 0 ?
                    <ListView
                        ref={el => this['lv'] = el}
                        dataSource={dataSource.cloneWithRows(data)}
                        renderRow={renderRow}
                        initialListSize={10}
                        pageSize={10}
                        renderBodyComponent={() => <MyBody />}
                        style={{ height: this.state.height }}
                    />
                    :
                    <Row className='tabs-content' type='flex' justify='center'>
                        暂无数据
                </Row>
                }
            </div>
        );
    }
}

/**
 * 控件：补贴申请控制器
 * 描述
 */
@addon('ApplicationSubsidiesView', '补贴申请', '补贴申请')
@reactControl(Form.create<any>()(ApplicationSubsidiesView), true)
export class ApplicationSubsidiesViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}