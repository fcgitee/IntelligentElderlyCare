import { Avatar, Col, Form, Modal, Row } from "antd";
import { Button, Card, Carousel, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH, encodeUrlParam } from "src/projects/app/util-tool";

/**
 * 组件：补贴申请详情状态
 */
export interface ApplicationDetailViewState extends BaseReactElementState {
    imgHeight?: any;
    data?: any;
    show?: any;
    person_list?: any;
    check?: any;
    index?: any;
    check_person?: any;
}
/**
 * 组件：补贴申请详情
 * 描述
 */
export class ApplicationDetailView extends BaseReactElement<ApplicationDetailViewControl, ApplicationDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            imgHeight: 176,
            data: [],
            show: false,
            person_list: [],
            check: false,
            index: 0,
            check_person: {}
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        AppServiceUtility.allowance_service.get_allowance_project_list_all!({ "id": id })!
            .then((data: any) => {
                // console.log(data.result);
                this.setState({
                    data: data.result[0]
                });
            });
    }
    goInsert = () => {
        // 判断是否登录
        AppServiceUtility.personnel_service.get_user_session!()!
            .then((data: any) => {
                // console.log("登录？？？？？？", data);
                if (!data) {
                    this.props.history!.push(ROUTE_PATH.login);
                    return;
                }
            });
        // 获取当前登陆人信息
        let person_list: any = [];
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log('当前登陆人的信息》》》》》》》》》', data);
                person_list.push(data[0]);
                // 获取当前登陆人的家属信息
                AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': data[0]['id'] })!
                    .then((data: any) => {
                        // console.log('所属亲属信息》》》》》', data);
                        // this.setState({
                        //     family_data: data['result']
                        // });
                        if (data.result.length > 0) {
                            data.result.map((item: any) => {
                                if (item.relationShip_type === '长者') {
                                    person_list.push(item.family_person[0]);
                                    this.setState({
                                        person_list
                                    });
                                }
                            });
                            this.setState({
                                show: true
                            });
                        }
                        // else if (data.result.length === 0) {
                        //     this.props.history!.push(ROUTE_PATH.familyFilesEditSelf + '/' + '长者&');
                        // }
                    });
            });

    }

    handleOk = (e: any) => {
        // console.log(this.state.check_person);
        if (JSON.stringify(this.state.check_person) !== '{}' && this.state.check_person['personnel_info']['personnel_category'] !== '长者') {
            Toast.fail('你选择的这个用户类别非长者!!!', 3);
        } else {
            let id = this.props.match!.params.key;
            this.props.history!.push(ROUTE_PATH.applicationInsert + '/' + id + '&' + encodeUrlParam(JSON.stringify(this.state.check_person)));
            this.setState({
                show: false,
            });
        }
    }

    handleCancel = (e: any) => {
        // console.log(e);
        this.setState({
            show: false,
        });
    }

    check = (index: any, info: any) => {
        // console.log(info);
        if (index === this.state.index && this.state.check === true) {
            this.setState({
                check: false,
                index
            });
        } else {
            this.setState({
                check: true,
                index
            });
        }
        this.setState({
            check_person: info
        });
    }
    render() {
        const { data } = this.state;
        setMainFormTitle("详情");
        return (
            <div>
                <Carousel
                    autoplay={true}
                    infinite={true}
                >
                    <a
                        className='carousel-a'
                        href="javascript:;"
                        style={{ height: this.state.imgHeight }}
                    >
                        <img
                            className='carousel-img'
                            src={data.allowance_project_photo}
                            alt=""
                            onLoad={() => {
                                window.dispatchEvent(new Event('resize'));
                                this.setState({ imgHeight: 'auto' });
                            }}
                        />
                    </a>
                </Carousel>
                <Card className='list-conten'>
                    <Row type='flex' justify='center' style={{ fontSize: '20px', fontWeight: 'bold' }}>
                        <Col span={24}>{data.allowance_project_name + '(' + data.allowance_project_type + ')'}</Col>
                    </Row>
                    <Row type='flex' justify='center'>
                        <Col span={24}><span style={{ color: 'red', fontSize: '25px', fontWeight: 'bold' }}>￥{data.allowance_project_money}</span>&nbsp;&nbsp;元每月&nbsp;&nbsp;&nbsp;已有{data.sq_num ? data.sq_num : 0}人申请</Col>
                    </Row>
                </Card>
                <Card>
                    <Card.Header
                        title="补助对象"
                    />
                    <Card.Body>
                        {data.allowance_project_people}
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title="详情介绍"
                    />
                    <Card.Body>
                        {data.allowance_project_details}
                    </Card.Body>
                </Card>
                <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.goInsert}>立即申请</Button>
                <Modal title="选择长者" visible={this.state.show} cancelText='取消' okText='确认' onOk={this.handleOk} onCancel={this.handleCancel}>
                    <div style={{ overflowX: 'scroll', whiteSpace: 'nowrap' }}>
                        <div style={{ display: 'flex' }}>
                            {this.state.person_list.map((item: any, index: any) => {
                                return (<div key={item} style={{ textAlign: 'center', float: 'left' }}>
                                    <div onClick={() => { this.check(index, item); }} style={{ border: this.state.index === index && this.state.check === true ? '1px solid green' : '0px' }}>
                                        <Avatar size={64} src={item.personnel_info ? item.personnel_info.picture : "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"} style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                                        <p style={{ marginBottom: '0px' }}>{item.name}</p>
                                    </div>
                                </div>);
                            })}
                        </div>
                    </div>
                </Modal>
            </div >
        );
    }
}

/**
 * 控件：补贴申请详情控制器
 * 描述
 */
@addon('ApplicationDetailView', '补贴申请详情', '补贴申请详情')
@reactControl(Form.create<any>()(ApplicationDetailView), true)
export class ApplicationDetailViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}