import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Result, Icon } from "antd-mobile";

/**
 * 组件：补贴申请成功视图状态
 */
export interface ApplySuccessViewState extends BaseReactElementState {
}

/**
 * 组件：补贴申请成功视图
 * 补贴申请成功
 */
export class ApplySuccessView extends BaseReactElement<ApplySuccessViewControl, ApplySuccessViewState> {
    render() {
        return (
            <div>
                <Result
                    img={<Icon type="check-circle" style={{ fill: '#1F90E6', width: '60px', height: '60px' }} />}
                    title="补贴申请成功"
                    message=""
                />
            </div>
        );
    }
}

/**
 * 控件：补贴申请成功视图控制器
 * 补贴申请成功
 */
@addon('ApplySuccessView', '补贴申请成功视图', '补贴申请成功')
@reactControl(ApplySuccessView)
export class ApplySuccessViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}