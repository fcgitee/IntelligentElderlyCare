import { Icon, message } from "antd";
import { Button, Card, List, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
const Item = List.Item;
const Brief = Item.Brief;

/**
 * 组件：确认订单控件状态
 */
export interface ConfirmOrdersViewState extends ReactViewState {
    /** 提交表单 */
    formValues?: {};
    /** 订单详情 */
    orderIfno?: {};
    /** 总金额 */
    total_amount?: number;
    /** 用户信息 */
    user_info?: {};
    /** 订单列表 */
    order_list?: any[];
}
/**
 * 组件：确认订单控件
 */
export class ConfirmOrdersView extends ReactView<ConfirmOrdersViewControl, ConfirmOrdersViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            formValues: {},
            orderIfno: {},
            total_amount: 0,
            user_info: {},
            order_list: []
        };
    }
    componentDidMount() {
        /** 判断是否立即购买 */
        if (this.props.match!.params.key) {
            console.info(JSON.parse(this.props.match!.params.key));
            let formValues = JSON.parse(this.props.match!.params.key);
            let total_amount = 0;
            console.info(formValues);
            formValues.item_list.map((value: any, index: number) => {
                total_amount += value.item_price;
            });
            total_amount = total_amount * formValues.count;
            AppServiceUtility.comfirm_order_service.get_purchaser_detail!()!
                .then((data) => {
                    // console.info(data);
                    this.setState({
                        order_list: [formValues],
                        total_amount,
                        user_info: data['personnel_info']
                    });
                })
                .catch((err) => {
                    console.info(err);
                });
        } else {
            let storage = window.localStorage;
            let shop = storage.getItem('shop');
            let shop_list: any[] = [];
            let shop_obj_list: any[] = [];
            let total_amount = 0;
            if (shop) {
                shop_list = shop.split('___');
                shop_list.map(value => {
                    shop_obj_list.push(JSON.parse(value));
                });
                shop_obj_list.map(order => {
                    total_amount += order['total_price'];
                });
                AppServiceUtility.comfirm_order_service.get_purchaser_detail!()!
                    .then((data) => {
                        // console.info(data);
                        this.setState({
                            order_list: shop_obj_list,
                            total_amount,
                            user_info: data['personnel_info']
                        });
                    })
                    .catch((err) => {
                        console.info(err);
                    });
            }
        }

    }
    /** 提交订单 */
    onSubmit = () => {
        AppServiceUtility.comfirm_order_service.create_product_order!(this.state.order_list)!
            .then((data) => {
                console.info(data);
                if (data === 'Success') {
                    message.info('提交成功');
                    // window.localStorage.clear();
                    this.props.history!.push(ROUTE_PATH.myOrder);
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    /** 订单删除 */
    deleteOrder = (index: number) => {
        let { order_list, total_amount } = this.state;
        let order_str_list: any[] = [];
        console.info(order_list![index]);
        total_amount = 0;
        order_list!.splice(index, 1);
        order_list!.map(order => {
            total_amount += order['total_price'];
            order_str_list.push(JSON.stringify(order));
        });
        let shop = order_str_list.join('___');
        window.localStorage.setItem('shop', shop);
        this.setState({
            order_list,
            total_amount
        });
    }
    render() {
        return (
            <div className='order-body'>
                {
                    this.state.order_list!.map((formValues, index) => {
                        return (
                            <Card key={index} className='confirmation-orders-card'>
                                <Icon className='delete-btn' type="delete" onClick={this.deleteOrder.bind(this, index)} />
                                <List className="my-list">
                                    <Item
                                        thumb="https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg"
                                        multipleLine={true}
                                    >
                                        {formValues!['name']}
                                        {/* <Brief>总计服务次数<h4>20</h4></Brief> */}
                                        <WhiteSpace size="lg" />
                                        <Brief>{formValues!['remarks']}</Brief>
                                    </Item>
                                    <Item extra={formValues!['count']}>数量</Item>
                                    <Item extra={<span>￥{formValues!['total_price']}</span>}>小计</Item>

                                </List>
                            </Card>
                        );
                    })
                }
                <Card className='confirmation-orders-card'>
                    <List className="personnel-info-list">
                        <Item extra={<span>{this.state.user_info!['name']}<br />{this.state.user_info!['telephone']}</span>}>被服务人员</Item>
                        <Item extra={<span>{this.state.user_info!['address']}</span>}>服务地址</Item>
                    </List>
                </Card>
                <List className="confirmation-orders-buttom">
                    <Item extra={<Button type='primary' onClick={this.onSubmit}>提交订单</Button>}><strong>￥{this.state.total_amount}</strong></Item>
                </List>
            </div>
        );
    }
}

/**
 * 组件：确认订单控件
 * 控制确认订单控件
 */
@addon('ConfirmOrdersView', '确认订单控件', '控制确认订单控件')
@reactControl(ConfirmOrdersView, true)
export class ConfirmOrdersViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}