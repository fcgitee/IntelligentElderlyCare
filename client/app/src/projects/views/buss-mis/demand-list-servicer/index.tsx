import { Input, Row, Icon, Col, } from "antd";
import { Tabs, Card } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { getWebViewRetObjectType, setMainFormTitle, subscribeWebViewNotify } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
const { Search } = Input;
const tabs = [
    { title: '距离' },
    { title: '价格' },
    { title: '时间' },
];
/**
 * 组件：服务商需求列表列表视图控件状态
 */
export interface DemandListServicerViewState extends ReactViewState {
    /** 请求的数据 */
    list?: any;
    /** 长列表容器高度 */
    height?: any;
    location?: any;
    animating?: any;
    notice?: string;
}
/**
 * 组件：服务商需求列表列表视图控件
 */
export class DemandListServicerView extends ReactView<DemandListServicerViewControl, DemandListServicerViewState> {
    first_in: boolean;

    constructor(props: any) {
        super(props);
        this.first_in = true;
        this.state = {
            list: [],
            height: document.documentElement!.clientHeight,
            location: undefined,
            animating: false,
            notice: '正在加载',
        };
    }

    componentDidMount() {
        // 监听app端通知
        if ((window as any).ReactNativeWebView) {
            const messageCb = (e: any) => {
                const retObject = JSON.parse(e.data);
                if (getWebViewRetObjectType(retObject) === 'getPhonePosition') {
                    const location = retObject.data;
                    if (location === undefined) {
                        // Toast.fail('获取位置信息失败');
                    }
                    this.setState({
                        location: retObject.data
                    });
                    if (this.first_in) {
                        AppServiceUtility.service_demand_service.get_demand!({ sortByDistance: true, ...location })!
                            .then((data: any) => {
                                if (data.result.length === 0) {
                                    this.setState({
                                        notice: '暂无数据',
                                    });
                                }
                                this.setState({
                                    list: data.result,
                                    animating: true
                                });
                            });
                        this.first_in = false;
                    }
                }
            };
            if ((window as any).ReactNativeWebView) {
                subscribeWebViewNotify(messageCb);
            }

            this.getPhonePosition();
        } else {
            this.handletab({ title: '距离' });
        }
    }

    getPhonePosition() {
        // Toast.info('认证成功：' + e.data);
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'getPhonePosition',
                    params: undefined
                })
            );
        }
    }

    onstop = (id: any) => {
        this.props.history!.push(ROUTE_PATH.demandDetailsServicer + '/' + id);
    }

    handletab(e: any) {
        let condition: Object = {};

        if (e.title === '距离') {
            condition['sortByDistance'] = true;
        }
        if (e.title === '价格') {
            condition['sort'] = '价格';
        }
        if (e.title === '时间') {
            condition['sort'] = '时间';
        }
        let se = Object.assign(condition, location);
        AppServiceUtility.service_demand_service.get_demand!(se)!
            .then((data: any) => {
                if (data.result.length === 0) {
                    this.setState({
                        notice: '暂无数据',
                    });
                }
                this.setState({
                    list: data.result,
                    animating: true
                });
            });
    }

    search = (value: any) => {
        AppServiceUtility.service_demand_service.get_demand!({ 'demand_title': value, ...location })!
            .then((data: any) => {
                if (data.result.length === 0) {
                    this.setState({
                        notice: '暂无数据',
                    });
                }
                this.setState({
                    list: data.result,
                    animating: true
                });
            });
    }
    create_dow(value: any) {
        return (
            <Card key={value.id} className='list-conten' onClick={() => this.onstop(value.id)}>
                <Row type='flex' justify='center'>
                    <Col className='list-col' span={10}><img src={value.demand_picture && value.demand_picture.length > 0 ? value.demand_picture[0] : ''} style={{ height: '72pt', width: '100pt' }} /></Col>
                    <Col span={14} className='list-col'>
                        <Row>
                            <Col span={16}>
                                <strong>{value.demand_title}</strong>
                            </Col>
                            <Col span={8}>
                                <div className='waiting-orders'>{value.state}</div>
                            </Col>

                        </Row>
                        <Row>
                            <span
                                style={{
                                    wordBreak: 'break-all',
                                    textOverflow: 'ellipsis',
                                    overflow: 'hidden',
                                    display: ' -webkit-box',
                                    WebkitLineClamp: 2,
                                    WebkitBoxOrient: 'vertical',
                                }}
                            >
                                {value.demand_explain}
                            </span>
                        </Row>
                        <Row style={{ fontSize: '10px' }}>
                            <Col span={12}>{
                                value.service_type_list.map((value: any, index: number) => {
                                    if (value.service_type && value.value === value.service_type[0]) {
                                        return value.label;
                                    }
                                })
                            }</Col>
                            <Col span={12}>{value.create_date}</Col>
                        </Row>
                        <Row>
                            {value.price_type}<span style={{ color: 'orange' }}>￥{value.price}</span>
                        </Row>
                    </Col>
                </Row>
            </Card>
        );
    }
    get_have_type_dow = (data: any[]) => {
        let dom: any[] = [];
        data.map((value: any, index: number) => {
            if (value.state === '有人竞单' || value.state === '等待接单' && value.service_type) {
                dom.push(this.create_dow(value));
            }
        });
        return dom;
    }
    get_not_have_type_dow = (data: any[]) => {
        let dom: any[] = [];
        data.forEach((value: any, index: number) => {
            if ((value.state === '有人竞单' || value.state === '等待接单') && !value.service_type) {
                dom.push(this.create_dow(value));
            }
        });
        return dom;

    }
    bidding_success = (data: any[]) => {
        let dom;
        data.forEach((value: any, index: number) => {
            if (value.state && value.state === '竞单成功') {
                dom = this.create_dow(value);
                return;
            }
        });
        return dom;
    }
    done = (data: any[]) => {
        let dom;
        data.forEach((value: any, index: number) => {
            if (value.state && value.state === '已完成') {
                dom = this.create_dow(value);
                return;
            }
        });
        return dom;
    }
    render() {
        setMainFormTitle('服务需求');
        let { list } = this.state;
        return (
            <Row>
                <div style={{ height: '50px', }}>
                    <Search
                        placeholder="搜索需求"
                        onSearch={this.search}
                        style={{ width: '100%', height: '40px', marginTop: '3px' }}
                    />
                </div>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                    onChange={this.handletab = this.handletab.bind(this)}
                >
                    {
                        tabs.map((value: any, index: number) => {
                            return (
                                <div className='tabs-content demand-servicer-card' key={index}>
                                    <Card full={true}>
                                        <Card.Header
                                            title="定服务类型"
                                            extra={<Icon type="right" />}
                                        />
                                        <Card.Body>
                                            {
                                                list && list.length > 0 ?
                                                    this.get_have_type_dow(list) : <div style={{ textAlign: 'center' }}>暂无数据</div>
                                            }
                                        </Card.Body>
                                    </Card>
                                    <Card full={true} style={{ marginTop: '20px' }}>
                                        <Card.Header
                                            title="未定服务类型"
                                            extra={<Icon type="right" />}
                                        />
                                        <Card.Body>
                                            {
                                                list && list.length > 0 ?
                                                    this.get_not_have_type_dow(list) : <div style={{ textAlign: 'center' }}>暂无数据</div>
                                            }
                                        </Card.Body>
                                    </Card>
                                    <Card full={true} style={{ marginTop: '20px' }}>
                                        <Card.Header
                                            title="竞单成功"
                                            extra={<Icon type="right" />}
                                        />
                                        <Card.Body>
                                            {
                                                list && list.length > 0 ?
                                                    this.bidding_success(list) : <div style={{ textAlign: 'center' }}>暂无数据</div>
                                            }
                                        </Card.Body>
                                    </Card>
                                    <Card full={true} style={{ marginTop: '20px' }}>
                                        <Card.Header
                                            title="已完成"
                                            extra={<Icon type="right" />}
                                        />
                                        <Card.Body>
                                            {
                                                list && list.length > 0 ?
                                                    this.done(list) : <div style={{ textAlign: 'center' }}>暂无数据</div>
                                            }
                                        </Card.Body>
                                    </Card>
                                </div>
                            );
                        })
                    }

                </Tabs>
            </Row>
        );
    }
}

/**
 * 组件：服务商需求列表列表视图控件
 * 控制服务商需求列表列表视图控件
 */
@addon('DemandListServicerView', '服务商需求列表列表视图控件', '控制服务商需求列表列表视图控件')
@reactControl(DemandListServicerView, true)
export class DemandListServicerViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}