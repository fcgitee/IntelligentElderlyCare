import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { SearchBar } from "antd-mobile";
import { ListView } from "antd-mobile";
import { Row } from "antd";

/**
 * 组件：护理记录列表页控件状态
 */
export interface nursingRecordViewState extends ReactViewState {
    /** 基础数据 */
    elderdata?: any;
    /** 长者输入框value */
    eldersearchvalue?: string;
    /** 护理员输入框value */
    workersearchvalue?: string;
    /** 数据 */
    dataList?: any;
    /** 当前页数 */
    pagesize?: number;

}

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

/**
 * 组件：护理记录列表页控件
 */
export class nursingRecordView extends ReactView<nursingRecordViewControl, nursingRecordViewState> {
    public lv: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            elderdata: ds,
            eldersearchvalue: '',
            workersearchvalue: '',
            dataList: [],
            pagesize: 1
        };
    }
    componentDidMount() {
        AppServiceUtility.nursing_record.get_nursing_recode_list!({}, this.state.pagesize, 10)!
            .then((data: any) => {
                // console.log(data);
                const dataBlob = getDataBlob(data.result);
                this.setState({
                    elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                    dataList: data.result
                });
            });
    }
    componentDidUpdate() {
        let navheaderheight;
        if (document.getElementsByClassName('searchinput').length) {
            navheaderheight = document.getElementsByClassName('searchinput')[0].clientHeight;
        }
        let widthheight = document.body.clientHeight;
        let headerheight = document.getElementsByClassName('am-navbar am-navbar-dark')[0].clientHeight;
        if (document.getElementsByClassName('am-list-view-scrollview').length) {
            if (navheaderheight) {
                document.getElementsByClassName('am-list-view-scrollview')[0]['style']!.height = (widthheight - headerheight - navheaderheight) + 'px';
            } else {
                document.getElementsByClassName('am-list-view-scrollview')[0]['style']!.height = (widthheight - headerheight) + 'px';
            }
        }
    }
    handleelder() {
        AppServiceUtility.nursing_record.get_nursing_recode_list!({ name: this.state.eldersearchvalue })!
            .then((data: any) => {
                const dataBlob = getDataBlob(data.result);
                this.setState({
                    elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                    dataList: data.result
                });
            });
    }
    handleelders(e:any) {
        this.setState({
            eldersearchvalue: e
        });
    }
    handleworker() {
        AppServiceUtility.nursing_record.get_nursing_recode_list!({ worker_name: this.state.workersearchvalue })!
            .then((data: any) => {
                const dataBlob = getDataBlob(data.result);
                this.setState({
                    elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                    dataList: data.result
                });
            });
    }
    handleworkers(e:any) {
        this.setState({
            workersearchvalue: e
        });
    }
    onEndReached() {
        this.setState({ pagesize: this.state.pagesize! + 1 }, () => {
            AppServiceUtility.nursing_record.get_nursing_recode_list!({}, this.state.pagesize, 10)!
                .then((data: any) => {
                    let item = this.state.dataList;
                    data.result.forEach((items: any, index: number) => {
                        item.push(items);
                    });
                    const dataBlob = getDataBlob(item);
                    this.setState({
                        elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                        dataList: item
                    });
                });
        });
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}><span>姓名：{obj.user[0].name}</span><span style={{ textAlign: 'right' }}>护理内容：{obj.obj.project_list[0].ass_info.project_name}</span></div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={{ fontWeight: 'bold', flex: '0 1 50%', maxWidth: '50%' }}>性别：{obj.user[0].personnel_info.sex}</div>
                        <div style={{ flex: '0 1 50%', maxWidth: '50%', textAlign: 'right' }}>护理时间：{obj.create_date}</div>
                    </div>
                </div>
            );
        };
        return (
            <div className="elder-care">
                <div className="search-header" style={{display: 'flex'}}>
                    <div className="search-heder-left" style={{flex: '0 1 48%', maxWidth: '48%'}}>
                        <SearchBar placeholder="请输入长者姓名" value={this.state.eldersearchvalue} onChange={this.handleelders = this.handleelders.bind(this)} onSubmit={this.handleelder = this.handleelder.bind(this)} className="searchinput" />
                    </div>
                    <div className="search-header-right" style={{flex: '0 1 48%', maxWidth: '48%'}}>
                        <SearchBar placeholder="请输入护理员姓名" value={this.state.workersearchvalue} onChange={this.handleworkers = this.handleworkers.bind(this)} onSubmit={this.handleworker = this.handleworker.bind(this)} className="searchinput" />
                    </div>
                </div>
                {
                    this.state.dataList && this.state.dataList.length ?
                        <ListView
                            ref={el => this.lv = el}
                            dataSource={this.state.elderdata}
                            renderRow={row}
                            renderSeparator={separator}
                            pageSize={10}
                            initialListSize={10}
                            renderBodyComponent={() => <MyBody />}
                            onEndReached={this.onEndReached = this.onEndReached.bind(this)}
                        />
                        :
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                        </Row>
                }
            </div>
        );
    }
}

/**
 * 组件：护理记录列表页控件
 * 控制护理记录列表页控件
 */
@addon('nursingRecordView', '护理记录列表页控件', '控制护理记录列表页控件')
@reactControl(nursingRecordView, true)
export class nursingRecordViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
