import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import "./index.less";
import { version_no } from "src/business/security/security-setting";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：关于我们状态
 */
export interface AboutUsViewState extends ReactViewState {
    // 公司名
    company_name?: string;
    // 公司描述
    company_description?: string;
    // 版本号
    version?: string;
    // 版权所有
    owner?: string;
    // 备案号
    copyright?: string;
}

/**
 * 组件：关于我们
 * 描述
 */
export class AboutUsView extends ReactView<AboutUsViewControl, AboutUsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            company_name: '南海区“一网三层”普惠型养老体系建设简介',
            company_description: '',
            version: version_no,
            owner: '版权所有',
            copyright: 'Copyright © 2018 - 2019 All Rights Reserved.',
        };
    }
    componentDidMount() {
        const company_name = this.state.company_name;
        if (document.getElementsByClassName('am-navbar-title') && document.getElementsByClassName('am-navbar-title')[0] && this.state.company_name! !== '') {
            document.getElementsByClassName('am-navbar-title')[0].innerHTML = company_name!;
        }
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: 'about_us' })!
            .then((data: any) => {
                if (data && data.result.length && data.result[0] && data.result[0]['data'] && data.result[0]['data']['content']) {
                    this.setState({
                        company_description: data.result[0]['data']['content']
                    });
                }
            })).catch((err) => {
                console.info(err);
            });
    }
    render() {
        const { company_name, company_description, owner, copyright } = this.state;
        return (
            <div className="about-us">
                <div className="compnay-title">
                    {company_name}
                </div>
                {/* <div className="system-version">
                    Version：{version}
                </div> */}
                <div className="company-description" dangerouslySetInnerHTML={{ __html: company_description ? company_description : '' }} />
                <div className="system-owner">
                    {owner}
                </div>
                <div className="system-copyright" style={{ marginBottom: '10px' }}>
                    {copyright}
                </div>
            </div>
        );
    }
}

/**
 * 控件：关于我们控制器
 * 描述
 */
@addon('AboutUsView', '关于我们', '描述')
@reactControl(AboutUsView, true)
export class AboutUsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}