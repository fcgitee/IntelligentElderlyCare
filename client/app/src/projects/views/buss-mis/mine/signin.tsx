import { Col, Row } from "antd";
import { Card, ListView, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { getAge2, ROUTE_PATH } from "src/projects/app/util-tool";
import '../../buss-pub/news-list/index.less';
import { LoadingEffectView } from "src/business/views/loading-effect";

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：我的签到列表视图控件状态
 */
export interface MySigninViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;

    empty?: any;
    animating?: any;
    pageCount?: number;
}
/**
 * 组件：我的签到列表视图控件
 */
export class MySigninView extends ReactView<MySigninViewControl, MySigninViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            empty: false,
            animating: true,
            pageCount: 10,
        };
    }
    componentDidMount() {
        this.getMySignin(this.state.page, false);
    }
    /** 明细点击事件 */
    on_click_detail = (type: string, id: string) => {
        if (type === 'activity') {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id + '/enroll');
        } else if (type === 'article') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
    }
    /** 下拉事件 */
    onEndReached = () => {
        let new_pape = this.state.page + 1;
        this.getMySignin(new_pape, true);
    }
    getMySignin(page: number, isConcact: boolean) {
        let { list, pageCount } = this.state;
        this.setState(
            {
                animating: true,
            },
            () => {
                request(this, AppServiceUtility.activity_service.get_activity_sign_in_list_all!({ 'type': 'my_activity' }, page, pageCount))
                    .then((data: any) => {
                        if (data && data.result.length > 0) {
                            this.setState({
                                list: list!.concat(data.result),
                                page,
                                animating: false,
                            });
                        } else {
                            this.setState({
                                animating: false,
                                empty: true,
                            });
                        }
                    }).catch((e) => {
                        this.setState({
                            animating: false,
                            empty: true,
                        });
                    });
            }
        );
    }
    render() {
        const { list, dataSource, animating, empty } = this.state;
        // 获取item进行展示
        setMainFormTitle('签到');
        const activityRow = (owData: any, sectionID: any, rowID: any) => {
            if (owData['user'].length === 0 || !owData['user'][0]['personnel_info']) {
                return (
                    <div />
                );
            }
            let user_info = owData['user'][0]['personnel_info'];
            return (
                <Card className='list-conten' onClick={() => this.on_click_detail('activity', owData.id)}>
                    <Row type='flex' justify='center'>
                        <Col className='list-col' span={10}><img className="activity-img" src={owData['activity'] && owData['activity'][0] && owData['activity'][0]['photo'] && owData['activity'][0]['photo'][0] ? owData['activity'][0]['photo'] && owData['activity'][0]['photo'][0] : 'https://www.e-health100.com/api/attachment/activityphoto/8336'} /></Col>
                        <Col span={14} className='list-col'>
                            <Row>{owData.activity_name}</Row>
                            <Row>姓名：{owData.user_name}</Row>
                            <Row>性别：{user_info.sex}</Row>
                            <Row>年龄：{getAge2(owData['user'][0].id_card)}</Row>
                            <Row>联系方式：{user_info.telephone}</Row>
                            <Row>签到时间：{owData.create_date}</Row>
                        </Col>
                    </Row>
                </Card>
            );
        };
        return (
            <Row className='tabs-content'>
                {
                    list && list.length ?
                        <ListView
                            ref={el => this['lv'] = el}
                            dataSource={dataSource.cloneWithRows(list)}
                            renderRow={activityRow}
                            initialListSize={10}
                            pageSize={10}
                            renderBodyComponent={() => <MyBody />}
                            style={{ height: this.state.height }}
                            onEndReached={this.onEndReached}
                        /> : null
                }
                {animating ? <Row>
                    <WhiteSpace size="lg" />
                    <LoadingEffectView />
                    <WhiteSpace size="lg" />
                </Row> : null}
                {empty ? <Row>
                    <WhiteSpace size="lg" />
                    <Row className='tabs-content' type='flex' justify='center'>{list && list.length ? '已经是最后一条了' : '无数据'}</Row>
                    <WhiteSpace size="lg" />
                </Row> : null}
            </Row>
        );
    }
}

/**
 * 组件：我的签到列表视图控件
 * 控制我的签到列表视图控件
 */
@addon('MySigninView', '我的签到列表视图控件', '控制我的签到列表视图控件')
@reactControl(MySigninView, true)
export class MySigninViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}