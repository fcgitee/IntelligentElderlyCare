import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List } from 'antd-mobile';
// import { Form, Button } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
const Item = List.Item;

/**
 * 组件：更改个人资料视图控件状态
 */
export interface EditInfoViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 真实姓名 */
    user_name?:any;
    /** 账户名 */
    account_name?:any;
    /** 手机号码 */
    phone?:any;
    /** 家庭住址 */
    address?:any;
}
/**
 * 组件：更改个人资料视图控件
 */
export class EditInfoView extends ReactView<EditInfoViewControl, EditInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            user_name:'',
            account_name:'',
            phone:'',
            address:''
        };
    }
    componentDidMount() {
        // 通过id获取用户的详细信息
        request(this, AppServiceUtility.person_org_manage_service.get_current_user_info!())
            .then((data: any) => {
                // console.log("用户", data);
                this.setState({
                    user_name: data[0].name,
                    account_name: data[0].login_info[0].login_check.account_name,
                    phone: data[0].personnel_info.telephone,
                    address: data[0].personnel_info.address,
                });
            });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form, } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (values.name) {
                AppServiceUtility.device_service.update_device!({ 'id': '', 'name': values.name })!
                    .then(data => {
                        console.info('绑定成功！');
                        this.props.history!.push(ROUTE_PATH.myDetail);
                    })
                    .catch(err => {
                        console.info(err);
                    });
            }
        });
    }

    render() {
        // const { getFieldProps } = this.props.form;
        const { user_name, account_name, phone, address } = this.state;
        return (
            <div>
                {/* <Form onSubmit={this.handleSubmit}>
                    <WhiteSpace />
                    <List>
                        <InputItem  {...getFieldProps('name')} placeholder="请输入新的用户名">用户名</InputItem>
                        <List.Item >
                            <Button style={{ width: '100%' }} type="primary" htmlType='submit' onClick={this.onClick}>保存</Button>
                        </List.Item>
                        <WhiteSpace />
                    </List>
                </Form> */}
                <List>
                    <Item extra={user_name} arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "真实姓名&name" ); }}>真实姓名</Item>
                    <Item extra={account_name} arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "账户名&account_name"); }}>账户名</Item>
                    <Item extra={phone} arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "手机号码&telephone"); }}>手机号码</Item>
                    <Item extra={address} arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "家庭住址&address"); }}>家庭住址</Item>
                    <Item arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "登录密码&password"); }}>登录密码</Item>
                    <Item arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.familyFiles);}}>绑定亲属关系</Item>
                </List>
            </div >
        );
    }
}

/**
 * 组件：更改个人资料视图控件
 * 控制更改个人资料视图控件
 */
@addon('EditInfoView', '更改个人资料视图控件', '控制更改个人资料视图控件')
@reactControl(EditInfoView, true)
export class EditInfoViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}