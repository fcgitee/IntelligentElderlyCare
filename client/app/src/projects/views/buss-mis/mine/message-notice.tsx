import { Col, Row, Spin } from "antd";
import { Card, ListView, Tabs, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { getAge2, ROUTE_PATH } from "src/projects/app/util-tool";
import '../../buss-pub/news-list/index.less';
import moment from "moment";
import { getGoodLookTimeShow } from "../activity-manage";
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：消息提醒列表视图控件状态
 */
export interface MessageNoticeViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: any;
    /** 记录tab状态 */
    tab_status?: string;
    tab_no?: number;
    tabs?: any;
    empty?: any;
    animating?: any;
    defaultImg?: any;
}
/**
 * 组件：消息提醒列表视图控件
 */
export class MessageNoticeView extends ReactView<MessageNoticeViewControl, MessageNoticeViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            defaultImg: 'https://www.e-health100.com/api/attachment/activityphoto/8336',
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: {},
            tab_status: 'activity',
            tabs: [],
            tab_no: 0,
            empty: false,
            animating: true,
        };
    }
    componentDidMount() {
        this.getMessageNotice();
    }
    /** 明细点击事件 */
    on_click_detail = (type: string, message_id: string, url_id: string) => {
        // 资讯和新闻不需要
        if (type !== 'news' && type !== 'activity') {
            this.setIsRead(message_id);
        }
        setTimeout(
            () => {
                if (type === 'activityP') {
                    // this.props.history!.push(ROUTE_PATH.activityDetail + '/' + url_id);
                } else if (type === 'bed') {
                    this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + url_id);
                } else if (type === 'product') {
                    this.props.history!.push(ROUTE_PATH.orderDetail + '/' + url_id);
                } else if (type === 'old_age') {
                    this.props.history!.push(ROUTE_PATH.applicationDetail + '/' + url_id);
                } else if (type === 'news') {
                    this.props.history!.push(ROUTE_PATH.newsInfo + '/' + url_id + '/SH');
                } else if (type === 'activity') {
                    this.props.history!.push(ROUTE_PATH.activityDetail + '/' + url_id + '/SH');
                }
            },
            300
        );
    }
    setIsRead(id: string) {
        request(this, AppServiceUtility.message_service.set_message_already_read!({ id }))
            .then((data: any) => {
                // console.log(`设置已读：${data}`);
            });
    }
    /** tab切换触发事件 */
    tab_click = (tab: any, index: number) => {
        this.setState({
            tab_status: tab['title'],
            tab_no: index,
        });
    }
    /** 下拉事件 */
    onEndReached = () => {
    }
    getMessageNotice() {
        this.setState(
            {
                empty: false,
                animating: true,
            },
            () => {
                request(this, AppServiceUtility.person_org_manage_service.get_current_user_message_notice!({}, 1, 999))
                    .then((data: any) => {
                        if (data && data.length > 0) {
                            this.setState({
                                list: data,
                                animating: false,
                            });
                        } else {
                            this.setState({
                                animating: false,
                                empty: true,
                            });
                        }
                    });
            }
        );
    }
    setRow(name: string) {
        switch (name) {
            case '床位预约':
                return (owData: any, sectionID: any, rowID: any) => {
                    if (owData['elder_info'].length === 0 || !owData['elder_info']['personnel_info']) {
                        return (
                            <div />
                        );
                    }
                    let personnel_info = owData['elder_info']['personnel_info'];
                    return (
                        <Card className='list-conten'>
                            <Row type='flex' justify='center' onClick={() => this.on_click_detail('bed', owData.id, owData['business_info'].organization_id)}>
                                <Col className='list-col' span={10}><img className="activity-img" src={personnel_info['photo'] && personnel_info['photo'][0] ? owData['elder_info']['photo'][0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>姓名：{owData['elder_info']['name'] || ''}</Row>
                                    <Row>性别：{personnel_info['sex'] || ''}</Row>
                                    <Row>年龄：{getAge2(personnel_info['id_card'] || '')}</Row>
                                    <Row>联系方式：{personnel_info['telephone'] || ''}</Row>
                                    <Row>受理方式：{owData['business_info']['state']}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '高龄津贴':
                return (owData: any, sectionID: any, rowID: any) => {
                    if (owData['elder_info'].length === 0 || !owData['elder_info']['personnel_info']) {
                        return (
                            <div />
                        );
                    }
                    let personnel_info = owData['elder_info']['personnel_info'];
                    return (
                        <Card className='list-conten'>
                            <Row type='flex' justify='center' onClick={() => this.on_click_detail('old_age', owData.id, owData['business_info'].id)}>
                                <Col className='list-col' span={10}><img className="activity-img" src={personnel_info['photo'] && personnel_info['photo'][0] ? owData['elder_info']['photo'][0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>姓名：{owData['elder_info']['name'] || ''}</Row>
                                    <Row>性别：{personnel_info['sex'] || ''}</Row>
                                    <Row>年龄：{getAge2(personnel_info['id_card'] || '')}</Row>
                                    <Row>联系方式：{personnel_info['telephone'] || ''}</Row>
                                    <Row>社区：{owData['business_info']['village']}</Row>
                                    <Row>镇街：{owData['business_info']['town_street']}</Row>
                                    <Row>金额：{owData['business_info']['total_amount']}</Row>
                                    <Row>受理结果：{owData['business_info']['apply_status']}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '活动报名':
                return (owData: any, sectionID: any, rowID: any) => {
                    return (
                        <Card className='list-conten'>
                            <Row type='flex' justify='center' onClick={() => this.on_click_detail('activityP', owData.id, owData['activity_info'].id)}>
                                <Col className='list-col' span={10}><img className="activity-img" src={owData.activity_info['photo'] && owData.activity_info['photo'][0] ? owData.activity_info['photo'][0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>{owData.activity_info['activity_name']}</Row>
                                    <Row>姓名：{owData.elder_info['name'] || ''}</Row>
                                    <Row>性别：{owData.elder_info['personnel_info'] && owData.elder_info['personnel_info']['sex'] ? owData.elder_info['personnel_info']['sex'] : ''}</Row>
                                    <Row>年龄：{getAge2(owData.elder_info['personnel_info'] && owData.elder_info['personnel_info']['id_card'] ? owData.elder_info['personnel_info']['id_card'] : '')}</Row>
                                    <Row>联系方式：{owData.elder_info['personnel_info'] && owData.elder_info['personnel_info']['telephone'] ? owData.elder_info['personnel_info']['telephone'] : ''}</Row>
                                    <Row>报名时间：{owData.create_date}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '养老资讯':
                return (owData: any, sectionID: any, rowID: any) => {
                    return (
                        <Card className='list-conten'>
                            <Row type='flex' justify='center' onClick={() => this.on_click_detail('news', owData.id, owData['business_info'].id)}>
                                <Col className='list-col' span={10}><img src={owData.hasOwnProperty('app_img_list') && owData.app_img_list.length > 0 ? owData.app_img_list[0] : this.state.defaultImg} style={{ height: '72pt', width: '110pt' }} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row className="maxLine2" style={{ WebkitBoxOrient: 'vertical' }}><strong>{owData['business_info'].title}</strong></Row>
                                    <Row>作者：{owData['business_info']['author'] || ''}</Row>
                                    <Row className='list-t'>{moment(owData.create_date).format("YYYY-MM-DD hh:mm")}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '活动信息':
                return (owData: any, sectionID: any, rowID: any) => {
                    return (
                        <Card className='list-conten' onClick={() => this.on_click_detail('activity', owData.id, owData['business_info'].id)}>
                            <Row type='flex' justify='center'>
                                <Col className='list-col' span={10}><img className="activity-img" src={owData.business_info.photo && owData.business_info.photo[0] ? owData.business_info.photo[0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>
                                        <Col span={24} className="happiness-info-row-title" style={{ WebkitBoxOrient: 'vertical' }}>{owData.business_info.activity_name}</Col>
                                    </Row>
                                    <Row className='list-row'>{owData.org_info.name}</Row>
                                    <Row>{getGoodLookTimeShow(owData.business_info.begin_date, owData.business_info.end_date)}</Row>
                                    <Row>
                                        <Col span={4} className="font-orange">{!owData.business_info.hasOwnProperty('amout') || owData.business_info.amount === 0 || owData.business_info.amount === '0' || owData.business_info.amount === undefined ? '免费' : `￥${owData['amount'] || 0}`}</Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '基金申请':
                return (owData: any, sectionID: any, rowID: any) => {
                    return (
                        <Card className='list-conten'>
                            <Row type='flex' justify='center' onClick={() => this.on_click_detail('fund', owData.id, owData['business_info'].id)}>
                                <Col className='list-col' span={10}><img className="activity-img" src={owData.app_img_list && owData.app_img_list[0] ? owData.app_img_list[0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>{owData['business_info']['donate_name']}</Row>
                                    <Row>基金金额：{owData['business_info']['donate_money']}</Row>
                                    <Row>基金意向：{owData['business_info']['donate_intention']}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            case '服务订单':
                return (owData: any, sectionID: any, rowID: any) => {
                    return (
                        <Card className='list-conten' style={{ padding: 8 }}>
                            <Row type='flex' justify='space-between' onClick={() => this.on_click_detail('product', owData.id, owData['business_info'].id)}>
                                <Col className='list-col' span={9}><img className="activity-img" src={owData.picture_collection && owData.picture_collection[0] ? owData.picture_collection[0] : this.state.defaultImg} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>产品名称：{owData['product_info'].name}</Row>
                                    <Row
                                        className="maxLine5"
                                        style={{
                                            WebkitBoxOrient: 'vertical',
                                        }}
                                    >
                                        产品描述：{owData['product_info'].introduce}
                                    </Row>
                                    <Row>{owData.org_info && owData.org_info.length > 0 && owData.org_info['name'] ? owData.org_info['name'] : ''}</Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                };
                break;
            default:
                return () => {
                    return (
                        <div />
                    );
                };
                break;
        }
    }
    render() {
        const { list, dataSource, animating, empty } = this.state;
        setMainFormTitle('消息提醒');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.mine);
        });
        return (
            <Row>
                {(() => {
                    if (list.length > 0) {
                        let tabs: any = [];
                        let divs: any = [];
                        list.map((item: any, index: number) => {
                            tabs.push({ 'title': item['name'] });
                            divs.push(<div className='tabs-content'>
                                {
                                    item.data && item.data.result && item.data.result.length ?
                                        <ListView
                                            ref={el => this['lv'] = el}
                                            dataSource={dataSource.cloneWithRows(item.data.result)}
                                            renderRow={this.setRow(item['name'])}
                                            initialListSize={10}
                                            pageSize={10}
                                            renderBodyComponent={() => <MyBody />}
                                            style={{ height: this.state.height }}
                                            onEndReached={this.onEndReached}
                                        /> : <Row>
                                            <WhiteSpace size="lg" />
                                            <Row className='tabs-content' type='flex' justify='center'>无数据</Row>
                                            <WhiteSpace size="lg" />
                                        </Row>
                                }
                            </div>);
                        });
                        return (<Tabs
                            tabs={tabs}
                            initialPage={0}
                            onTabClick={(tab, index) => this.tab_click(tab, index)}
                        >
                            {divs}
                        </Tabs>);
                    }
                    return (
                        <Row />
                    );
                })()}
                {animating ? <Row>
                    <WhiteSpace size="lg" />
                    <Row style={{ textAlign: 'center' }}>
                        <Spin size="large" />
                    </Row>
                    <WhiteSpace size="lg" />
                </Row> : null}
                {empty && list.length === 0 ? <Row>
                    <WhiteSpace size="lg" />
                    <Row className='tabs-content' type='flex' justify='center'>无数据</Row>
                    <WhiteSpace size="lg" />
                </Row> : null}
            </Row>
        );
    }
}

/**
 * 组件：消息提醒列表视图控件
 * 控制消息提醒列表视图控件
 */
@addon('MessageNoticeView', '消息提醒列表视图控件', '控制消息提醒列表视图控件')
@reactControl(MessageNoticeView, true)
export class MessageNoticeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}