import { Button, Col, Icon, Row } from "antd";
// import Grid from "antd-mobile/lib/grid";
import { Card, List, Modal, WhiteSpace, WingBlank } from 'antd-mobile';
import { addon } from "pao-aop";
import { Authentication, reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { LOCAL_FUNCTION_LIST } from "src/business/mainForm/backstageManageMainForm";
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { forceCheckIsLogin, getAppPermissonObject, ROUTE_PATH } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
const Item = List.Item;
const alert = Modal.alert;
/**
 * 组件：名称状态
 */
export interface MineViewState extends ReactViewState {
    icon_data?: any[];
    /*用户id */
    id?: any;
    /*账号名 */
    name?: any;
    showState?: boolean;
    account?: any;
    touxiang?: any;
    is_loading?: any;
}

/**
 * 组件：名称
 * 描述
 */
export class MineView extends ReactView<MineViewControl, MineViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            name: '',
            showState: false,
            is_loading: false
        };
    }
    componentDidMount() {
        this.setState({
            is_loading: true
        });
        // 判断是否登录
        AppServiceUtility.personnel_service.get_user_session!()!
            .then((data: any) => {
                this.setState({ showState: data, is_loading: false });
            });
        // // 获取当前用户，拿到用户id和头像、账户名
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                if (data && data.length > 0) {
                    this.setState({
                        id: data[0].id,
                        name: data[0].name,
                        account: data[0].login_info[0].login_check.account_name,
                        // 防止报错
                        touxiang: data[0].personnel_info && data[0].personnel_info.picture_list && data[0].personnel_info.picture_list[0] ? data[0].personnel_info.picture_list[0] : "",
                    });
                }
            });
    }
    // 退出登录
    exit = () => {
        alert('退出登录', '你确认退出吗???', [
            { text: '取消', onPress: () => history.back() },
            { text: '确定', onPress: () => this.handleExit() },
        ]);
    }

    // 确定回调
    handleExit = () => {
        Authentication.logout()
            .then(data => {
                // 清除cookies
                IntelligentElderlyCareAppStorage.clearCookies();
                let remember_user = localStorage.getItem('remember_user');
                if (remember_user) {
                    localStorage.clear();
                    localStorage.setItem('remember_user', remember_user);
                } else {
                    localStorage.clear();
                }
                this.props.history!.push(ROUTE_PATH.login, { 'go_back_obj': { 'backPath': ROUTE_PATH.home } });
            })
            .catch((error) => { });
    }
    render() {
        const { showState, touxiang } = this.state;
        const AppPermission = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);

        const getCardItem = (text: string, icon: string, onClick: any) => {
            return (
                <Col
                    style={{ textAlign: "center" }}
                    onClick={onClick}
                >
                    <FontIcon style={{ fontSize: 36 }} type={icon} />
                    <div style={{ fontSize: 12 }}>
                        {text}
                    </div>
                </Col>
            );
        };

        return (
            // 修改滚动不到最底部的样式，原样式在上面
            <WingBlank style={{ display: this.state.is_loading ? 'none' : 'block' }}>
                <div style={{ backgroundColor: '#ffffff', borderRadius: '10px', boxShadow: '0px 0px 12px 0px rgba(170,170,170,0.5)' }}>
                    {showState === true ?
                        <>
                            <Row type={'flex'} >
                                <Row type={'flex'} style={{ flex: 1 }} onClick={() => this.props.history!.push(ROUTE_PATH.myDetail + '/' + this.state.id)}>
                                    <Col style={{ width: 100 }}>
                                        <WhiteSpace />
                                        <WingBlank>
                                            <div style={{ height: 80 }}>
                                                <img
                                                    src={touxiang ? touxiang : 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg'}
                                                    alt=""
                                                    style={{ width: '80px', height: '80px', borderRadius: '10%' }}
                                                />
                                            </div>
                                        </WingBlank>
                                    </Col>
                                    <Col style={{ flex: 1 }}>
                                        <WhiteSpace />
                                        <WingBlank>
                                            <p style={{ fontSize: '20px', fontWeight: 'bold', marginBottom: '11px' }}>{this.state.name ? this.state.name : ''}</p>
                                            <p style={{ fontSize: '15px', fontWeight: 'bold' }}>{"账号：" + (this.state.account ? this.state.account : '')}</p>
                                        </WingBlank>
                                    </Col>
                                </Row>
                                <Row>
                                    <WhiteSpace />
                                    <Button
                                        onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.familyFiles }, { backPath: ROUTE_PATH.mine }); }}
                                        style={{
                                            fontWeight: 'bold',
                                            width: '50%',
                                            height: '100%',
                                            fontSize: 18,
                                            borderRadius: '0 0 0 10px',
                                            padding: 13,
                                            borderLeft: 0,
                                        }}
                                    >
                                        <Icon type="wallet" />
                                        家人档案
                                    </Button>
                                    <Button
                                        onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.healthFiles }, { backPath: ROUTE_PATH.mine }); }}
                                        style={{
                                            fontWeight: 'bold',
                                            width: '50%',
                                            height: '100%',
                                            fontSize: 18,
                                            borderLeft: 0,
                                            borderRight: 0,
                                            padding: 13,
                                            borderRadius: '0 0 10px 0',
                                        }}
                                    >
                                        <Icon type="wallet" />
                                        健康档案
                                    </Button>
                                </Row>
                            </Row>
                        </>
                        :
                        <div style={{ height: '75px', padding: '0 20px', textAlign: 'center', fontSize: '20px', boxShadow: '0px 0px 12px 0px rgba(170,170,170,0.5)' }} onClick={() => this.props.history!.push(ROUTE_PATH.login, { 'go_back_obj': { 'backPath': ROUTE_PATH.mine } })}><span style={{ lineHeight: '75px' }}>暂未登录,请登录</span></div>
                    }
                </div>
                <WhiteSpace />
                <WhiteSpace />
                <Card
                    style={{
                        boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)',
                        borderRadius: 10
                    }}
                >
                    <Card.Header
                        title="我的服务"
                    // extra={<span>查看更多 <Icon type="right-circle" /></span>}
                    />
                    <Card.Body>
                        <Row type={'flex'} justify={'space-between'} style={{ flexWrap: 'nowrap' }} gutter={10}>
                            {

                                AppPermission && getAppPermissonObject(AppPermission, '服务人员订单', '查询') ?
                                    getCardItem(
                                        '服务订单',
                                        'icondingdan',
                                        () => {
                                            forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.servicerTaskList }, { backPath: ROUTE_PATH.mine });
                                        },
                                    )
                                    :
                                    getCardItem(
                                        '我的订单',
                                        'icondingdan',
                                        () => {
                                            forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.myOrder }, { backPath: ROUTE_PATH.mine });
                                        },
                                    )
                            }
                            {
                                getCardItem(
                                    '服务支付',
                                    'iconzhifufangshi',
                                    () => {
                                        forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.servicePayment }, { backPath: ROUTE_PATH.mine });
                                    }
                                )
                            }
                            {
                                getCardItem(
                                    '预约申请',
                                    'iconyu_yue',
                                    () => {
                                        forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.appointmentApplication }, { backPath: ROUTE_PATH.mine });
                                    }
                                )
                            }
                            {
                                getCardItem(
                                    '服务关注',
                                    'iconfuwuguanzhu',
                                    () => {
                                        forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.serviceConcern }, { backPath: ROUTE_PATH.mine });
                                    }
                                )
                            }
                            {
                                getCardItem(
                                    '服务收藏',
                                    'iconshoucang',
                                    () => {
                                        forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.serviceCollection }, { backPath: ROUTE_PATH.mine });
                                    }
                                )
                            }
                        </Row>
                    </Card.Body>
                </Card>
                <WhiteSpace />
                {
                    AppPermission && (getAppPermissonObject(AppPermission, '发帖', '查询') || getAppPermissonObject(AppPermission, '评论', '查询') || getAppPermissonObject(AppPermission, '点赞', '查询')) ?
                        <Card
                            style={{
                                boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)',
                                borderRadius: 10
                            }}
                        >
                            <Card.Header
                                title="我的老友圈"
                                extra={<span>查看更多 <Icon type="right-circle" /></span>}
                            />
                            <Card.Body>
                                <Row type={'flex'} justify={'space-between'} style={{ flexWrap: 'nowrap' }} gutter={10}>
                                    {
                                        <>
                                            <>
                                                {
                                                    getAppPermissonObject(AppPermission, '发帖', '查询') ?
                                                        getCardItem(
                                                            '发帖',
                                                            'wallet',
                                                            () => { this.props.history!.push(ROUTE_PATH.publish); forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.familyList, params: { url: ROUTE_PATH.demandRelease, key: this.props.match!.params.key } }, { backPath: ROUTE_PATH.mine }); },
                                                        )
                                                        :
                                                        null
                                                }
                                            </>
                                            <>
                                                {
                                                    getAppPermissonObject(AppPermission, '评论', '查询') ?
                                                        getCardItem(
                                                            '评论',
                                                            'wallet',
                                                            () => { this.props.history!.push(ROUTE_PATH.commmentList); }
                                                        )
                                                        :
                                                        null
                                                }
                                            </>
                                            <>
                                                {
                                                    getAppPermissonObject(AppPermission, '点赞', '查询') ?
                                                        getCardItem(
                                                            '点赞',
                                                            'wallet',
                                                            () => { this.props.history!.push(ROUTE_PATH.giveThumbsUp); }
                                                        )
                                                        :
                                                        null
                                                }
                                            </>
                                        </>
                                    }
                                </Row>
                            </Card.Body>
                        </Card>
                        :
                        null
                }
                <div style={{ marginTop: '10px' }}>
                    <List className="myList">
                        {AppPermission && getAppPermissonObject(AppPermission, '防疫上报', '查询') ?
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.passCard }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">防疫上报</Item> : ''}
                        {AppPermission && getAppPermissonObject(AppPermission, '防疫上报(机构)', '查询') ?
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.orgDayUpdateHealth }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">防疫上报（机构）</Item> : ''}
                        {AppPermission && getAppPermissonObject(AppPermission, '我的家', '查询') ?
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.myHome }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">我的家</Item> : ''}
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.familyList, params: { url: ROUTE_PATH.demandRelease, key: this.props.match!.params.key } }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">需求发布</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.demandList }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">我的需求</Item>
                        {/* {
                            AppPermission && getAppPermissonObject(AppPermission, '服务人员订单', '查询') ?
                                <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.servicerTaskList }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">我的订单(服务人员)</Item>
                                :
                                <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.myOrder }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">我的订单</Item>
                        } */}
                        {/* <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.servicePayment }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">服务支付</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.appointmentApplication }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">预约申请</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.serviceConcern }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">服务关注</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.serviceCollection }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">服务收藏</Item> */}
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.careSetting }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">关怀设置</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.elderLove }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">长者关爱</Item>
                        {AppPermission && getAppPermissonObject(AppPermission, '智能监护管理', '查询') ?
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.appDeviceList }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">智能监护</Item> : ''}
                    </List>
                </div>
                {
                    AppPermission && (getAppPermissonObject(AppPermission, '发布', '查询') || getAppPermissonObject(AppPermission, '签到', '查询') || getAppPermissonObject(AppPermission, '受理', '查询')) ?
                        <div style={{ marginTop: '10px' }}>
                            <List className="myList">
                                {
                                    getAppPermissonObject(AppPermission, '发布', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.myRelease); }} arrow="horizontal">发布</Item>
                                        :
                                        null
                                }
                                {
                                    getAppPermissonObject(AppPermission, '签到', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.mySignIn); }} arrow="horizontal">签到</Item>
                                        :
                                        null
                                }
                                {
                                    getAppPermissonObject(AppPermission, '受理', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.myAcceptance); }} arrow="horizontal">受理</Item>
                                        :
                                        null
                                }

                            </List>
                        </div>
                        :
                        null
                }
                {/* {
                    AppPermission && (getAppPermissonObject(AppPermission, '发帖', '查询') || getAppPermissonObject(AppPermission, '评论', '查询') || getAppPermissonObject(AppPermission, '点赞', '查询')) ?
                        <div style={{ marginTop: '10px' }}>
                            <List className="myList">
                                {
                                    getAppPermissonObject(AppPermission, '发帖', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.publish); }} arrow="horizontal">发帖</Item>
                                        :
                                        null
                                }
                                {
                                    getAppPermissonObject(AppPermission, '评论', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.commmentList); }} arrow="horizontal">评论</Item>
                                        :
                                        null
                                }
                                {
                                    getAppPermissonObject(AppPermission, '点赞', '查询') ?
                                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.giveThumbsUp); }} arrow="horizontal">点赞</Item>
                                        :
                                        null
                                }
                            </List>
                        </div>
                        :
                        null
                } */}
                <div style={{ marginTop: '10px' }}>
                    <List className="myList">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.setUp); }} arrow="horizontal">设置</Item>
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.feedback }, { backPath: ROUTE_PATH.mine }); }} arrow="horizontal">反馈</Item>
                    </List>
                </div>
                <div style={{ marginTop: '10px' }}>
                    <List className="myList">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold' }} thumb={<Icon type="wallet" />} onClick={() => { this.props.history!.push(ROUTE_PATH.interaction); }} arrow="horizontal">在线咨询</Item>
                    </List>
                </div>
                {showState && <div style={{ marginTop: '10px' }}>
                    <List className="myList">
                        <Item style={{ lineHeight: '50px', paddingLeft: '15px', fontWeight: 'bold', color: 'red' }} thumb={<Icon type="wallet" />} arrow="horizontal" onClick={this.exit}>退出登录</Item>
                    </List>
                </div>}
            </WingBlank>
        );
    }
}

/**
 * 控件：名称控制器
 * 描述
 */
@addon('MineView', '名称', '描述')
@reactControl(MineView, true)
export class MineViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}