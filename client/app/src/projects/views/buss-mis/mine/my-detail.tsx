import { List, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Modal } from 'antd';
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
const Item = List.Item;
/**
 * 组件：个人详情视图控件状态
 */
export interface myDetailViewState extends ReactViewState {
    a?: any;
    user_info?: any;
    user_name?: any;
    account_name?: any;
    phone_num?: any;
    touxiang?: any;
    visible?: any;
    photo?: any;
    show?: any;
}
/**
 * 组件：个人详情视图控件
 */
export class myDetailView extends ReactView<myDetailViewControl, myDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            visible: false,
            photo: [],
            show: false
        };
    }
    componentDidMount() {
        // 通过id获取用户的详细信息
        request(this, AppServiceUtility.person_org_manage_service.get_current_user_info!())
            .then((data: any) => {
                // console.log("用户", data);
                this.setState({
                    user_name: data[0].name,
                    account_name: data[0].login_info[0].login_check.account_name,
                    phone_num: data[0].personnel_info.telephone,
                    // 解决报错的问题
                    touxiang: <img src={data[0].personnel_info && data[0].personnel_info.picture_list && data[0].personnel_info.picture_list[0] ? data[0].personnel_info.picture_list[0] : ""} style={{ height: '50px', width: '50px' }} />
                });
            });
    }

    showModal = () => {
        this.setState({
            visible: true
        });
    }

    handleOk = (e: any) => {
        // console.log(e);

        AppServiceUtility.login_service.modify_user_info!({ key: 'photo', value: this.state.photo })!
            .then((data: any) => {
                console.info(data);
                Toast.success('修改成功!!!', 1);
                setTimeout(
                    () => {
                        this.props.history!.push(ROUTE_PATH.mine);
                    },
                    2000
                );
            })
            .catch((err: any) => {
                console.info(err);
            });
        this.setState({
            visible: false,
            show: false,
            photo: []
        });
    }

    handleCancel = (e: any) => {
        // console.log(e);
        this.setState({
            visible: false,
            show: false,
            photo: []
        });
    }

    imgChange = (e: any) => {
        // console.log(e);
        this.setState({
            photo: e,
            show: true
        });
    }
    render() {
        setMainFormTitle('我的资料');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.mine);
        });
        const { user_name, account_name, phone_num, touxiang } = this.state;
        return (
            <div style={{ height: document.body.clientHeight }}>
                <div>
                    <List>
                        <Item extra={touxiang} style={{ height: '80px' }} arrow="horizontal" onClick={() => { this.showModal(); }}>我的头像</Item>
                        {/* 要传id 和 name */}
                        <Item extra={account_name ? account_name : '无'}>昵称</Item>
                    </List>
                </div>
                <div style={{ marginTop: '20px' }}>
                    <List className="myList">
                        <Item extra={user_name ? user_name : '无'}>姓名</Item>
                        <Item extra={phone_num ? phone_num : '无'}>联系电话</Item>
                    </List>
                </div>
                <div style={{ marginTop: '20px' }}>
                    <List className="myList">
                        <Item arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.changeName + "/" + "登录密码&password"); }}>密码修改</Item>
                        <Item arrow="horizontal" onClick={() => { this.props.history!.push(ROUTE_PATH.editInfo); }}>更多</Item>
                    </List>
                </div>
                <Modal
                    title="上传新的头像（大小小于2M，格式支持jpg/jpeg/png）"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    cancelText='取消'
                    okText='提交'
                >
                    {this.state.show === true ?
                        <div style={{ width: '100px', height: '100px', margin: '0px auto' }}>
                            <img src={this.state.photo} style={{ width: '100px', height: '100px' }} />
                        </div>
                        :
                        ''
                    }
                    <div style={{ width: '100px', height: '100px', margin: '0px auto' }}>
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.imgChange} />
                    </div>
                </Modal>
            </div>
        );
    }
}

/**
 * 组件：个人详情视图控件
 * 控制个人详情视图控件
 */
@addon('myDetailView', '个人详情视图控件', '控制个人详情视图控件')
@reactControl(myDetailView, true)
export class myDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}