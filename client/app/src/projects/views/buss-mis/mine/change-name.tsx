import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, WhiteSpace, Toast } from 'antd-mobile';
import { Form, Button } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：更改用户名视图控件状态
 */
export interface changeNameViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    name?: any;
    key?: any;
    first_inset?: any;
    second_inset?: any;
}
/**
 * 组件：更改用户名视图控件
 */
export class changeNameView extends ReactView<changeNameViewControl, changeNameViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            key: '',
            first_inset: '',
            second_inset: ''
        };
    }
    componentDidMount() {
        let params = this.props.match!.params.key;
        // console.log(params);
        params = params.split("&");
        this.setState({
            name: params[0],
            key: params[1]
        });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { first_inset, second_inset, key } = this.state;
        if (first_inset !== second_inset) {
            Toast.fail('两次输入的内容不一致!!!', 1);
        }
        if (first_inset === second_inset) {
            AppServiceUtility.login_service.modify_user_info!({ key, value: second_inset })!
                .then((data: any) => {
                    console.info(data);
                    Toast.success('修改成功!!!', 1);
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.editInfo);
                        },
                        2000
                    );
                })
                .catch((err: any) => {
                    console.info(err);
                });
        }
    }
    fChange = (value: any) => {
        // console.log(value);
        this.setState({
            first_inset: value
        });
    }
    SChange = (value: any) => {
        // console.log(value);
        this.setState({
            second_inset: value
        });
    }
    render() {
        const { getFieldProps } = this.props.form;
        const { name, first_inset, second_inset } = this.state;
        return (
            <div>
                <Form>
                    <WhiteSpace />
                    <List>
                        <InputItem  {...getFieldProps('value1')} placeholder={"请输入新的" + name} labelNumber={7} type="password" value={first_inset} onChange={this.fChange}>{name}</InputItem>
                        <InputItem  {...getFieldProps('value2')} placeholder={"请确认新的" + name} labelNumber={7} type="password" value={second_inset} onChange={this.SChange}>确认新{name}</InputItem>
                        <List.Item >
                            <Button style={{ width: '100%' }} type="primary" htmlType='submit' onClick={this.handleSubmit}>保存</Button>
                        </List.Item>
                        <WhiteSpace />
                    </List>
                </Form>
            </div >
        );
    }
}

/**
 * 组件：更改用户名视图控件
 * 控制更改用户名视图控件
 */
@addon('changeNameView', '更改用户名视图控件', '控制更改用户名视图控件')
@reactControl(Form.create<any>()(changeNameView), true)
export class changeNameViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}