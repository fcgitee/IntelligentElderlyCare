import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：互动页控件状态
 */
export interface InteractionViewState extends ReactViewState {
}

/**
 * 组件：互动页控件
 */
export class InteractionView extends ReactView<InteractionViewControl, InteractionViewState> {
    public lv: any = null;
    constructor(props: any) {
        super(props);
    }
    componentDidMount() {

    }
    leavingMessage = () => {
        this.props.history!.push(ROUTE_PATH.messageInteraction);
    }
    render() {
        return (
            <div
                style={{
                    position: 'fixed',
                    top: '0',
                    bottom: '0',
                    width: '100%',
                    background: 'rgba(249,247,243,1)'
                }}
            >
                <div
                    style={{
                        display: 'flex',
                        height: '100%',
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <div>
                        <div
                            style={{
                                marginTop: '-60px',
                                marginBottom: '60px',
                            }}
                        >
                            <div
                                onClick={this.leavingMessage}
                            >
                                <img src={require('src/static/img/icon_message.png')} width={150} height={150} alt="" />
                            </div>
                            <p style={{ textAlign: 'center', fontWeight: 'bold', marginTop: '10px', }}>在线留言</p>
                        </div>
                        <div>
                            <a href="tel:4000123495">
                                <div>
                                    <img src={require('src/static/img/icon_phone.png')} width={150} height={150} alt="" />
                                </div>
                            </a>
                            <span style={{ textAlign: 'center', marginTop: '10px', fontWeight: 'bold', display: 'block' }}>电话咨询</span>
                            <span style={{ textAlign: 'center', fontWeight: 'bold', display: 'block' }}>0757-66886188</span>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

/**
 * 组件：互动页控件
 * 控制互动页控件
 */
@addon('InteractionView', '互动页控件', '控制互动页控件')
@reactControl(InteractionView, true)
export class InteractionViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
