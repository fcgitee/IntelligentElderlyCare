import { List, Picker, InputItem, Modal, NavBar, Icon, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { request_func, ROUTE_PATH } from "src/projects/app/util-tool";
import { Form, Button, Row } from 'antd';
/**
 * 组件：创建设备视图控件状态
 */
export interface AddMyDeviceViewState extends ReactViewState {
    room_list: any; // 设备类型列表
    device_info: any; // 当前设备信息
    del_status: boolean; // 是否删除状态
    device_type_list: any; // 设备类型列表
    host_list: any; // 主机列表
}
/**
 * 组件：创建设备视图控件
 */
export class AddMyDeviceView extends ReactView<AddMyDeviceViewControl, AddMyDeviceViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_list: [],
            device_info: {},
            del_status: false,
            device_type_list: [],
            host_list: [],
        };
    }
    componentDidMount() {
        // 获取设备位置列表
        request_func(this, AppServiceUtility.app_my_home_service.get_all_my_room_list, [{}], (data: any) => {
            const room_list = data.map((val: any) => {
                return { label: val.name, value: val.id };
            });
            this.setState({ room_list });
        });
        // 获取设备类型列表
        request_func(this, AppServiceUtility.app_my_home_service.get_device_type_list, [{}], (data: any) => {
            const device_type_list = data.map((val: any) => {
                return { label: val.name, value: val.id };
            });
            this.setState({ device_type_list });
        });
        // 获取主机列表
        request_func(this, AppServiceUtility.app_my_home_service.get_my_host_list, [{}], (data: any) => {
            const host_list = data.map((val: any) => {
                return { label: val.name, value: val.host_id };
            });
            this.setState({ host_list });
        });
        if (this.props.match!.params.key) { // 编辑
            request_func(this, AppServiceUtility.app_my_home_service.get_my_device_detail, [{ id: this.props.match!.params.key }], (data: any) => {
                data.length ? this.setState({ device_info: data[0] }) : Toast.fail('暂无数据', 3);
            });
        }
    }
    // 新建/编辑设备
    handleSubmit = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            let param = {
                device_type: values.device_type[0],
                name: values.name,
                device_id: values.device_id,
                host_id: values.host_id[0],
                room_id: values.room_id[0]
            };
            this.props.match!.params.key && Object.assign(param, { id: this.props.match!.params.key });
            request_func(this, AppServiceUtility.app_my_home_service.add_my_device, [param], (data: any) => {
                data.code === 200 ? Toast.success(data.msg, 3, () => {
                    this.goBack();
                }) : Toast.fail(data.msg, 3,);
            });
        });
    }
    // 删除设备
    delRoom = () => {
        this.setDelStatus(false);
        request_func(this, AppServiceUtility.app_my_home_service.del_my_device, [this.props.match!.params.key], (data: any) => {
            data.code === 200 ? Toast.success(data.msg, 3, () => {
                this.goBack();
            }) : Toast.fail(data.msg, 3,);
        });
    }
    goBack = () => {
        history.back();
    }
    goToOtherNav = (url: string) => {
        this.props!.history!.push(url);
    }
    setDelStatus = (isDel: boolean = true) => {
        this.setState({ del_status: isDel });
    }
    // 切换设备类型
    selectDeviceType = (val: any) => {
        this.state.device_type_list.map((item: any) => {
            if (item.value === val[0]) {
                let device_info = this.state.device_info;
                device_info.room_type = val[0];
                device_info.name = item.label;
                this.setState({ device_info });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                <NavBar
                    icon={<Icon type={"left"} onClick={() => { this.goBack(); }} />}
                    rightContent={this.props.match!.params.key && <Row onClick={this.handleSubmit} type="flex" justify="end">更改</Row>}
                >
                    {
                        this.props.match!.params.key ? '设备信息' : '新增设备'
                    }
                </NavBar>
                <Form onSubmit={this.handleSubmit}>
                    <List style={{ backgroundColor: 'white' }} className="picker-list">
                        {getFieldDecorator('device_type', {
                            initialValue: this.state.device_info ? [this.state.device_info.device_type] : [this.state.device_type_list[0].value],
                            rules: [{ required: true, message: '请选择设备类型!' }],
                        })(
                            <Picker data={this.state.device_type_list} cols={1} className="forss" onOk={this.selectDeviceType}>
                                <List.Item arrow="horizontal">设备类型</List.Item>
                            </Picker>
                        )}
                        {getFieldDecorator('name', {
                            initialValue: this.state.device_info ? this.state.device_info.name : this.state.device_type_list[0].label,
                            rules: [{ required: true, message: '请输入设备名称!' }],
                        })(
                            <InputItem placeholder="请输入设备名称" style={{ textAlign: 'right' }}>设备名称</InputItem>
                        )}
                        {getFieldDecorator('device_id', {
                            initialValue: this.state.device_info ? this.state.device_info.device_id : '',
                            rules: [{ required: true, message: '请输入设备ID!' }],
                        })(
                            <InputItem placeholder="请输入房间名称" style={{ textAlign: 'right' }}>设备ID</InputItem>
                        )}
                        {getFieldDecorator('host_id', {
                            initialValue: this.state.device_info ? [this.state.device_info.host_id] : [],
                            rules: [{ required: true, message: '请选择绑定的主机!' }],
                        })(
                            <Picker data={this.state.host_list} cols={1} className="forss" onOk={this.selectDeviceType}>
                                <List.Item arrow="horizontal">绑定主机</List.Item>
                            </Picker>
                        )}
                        {getFieldDecorator('room_id', {
                            initialValue: this.state.device_info ? [this.state.device_info.room_id] : [],
                            rules: [{ required: true, message: '请选择设备位置!' }],
                        })(
                            <Picker data={this.state.room_list} cols={1} className="forss" onOk={this.selectDeviceType}>
                                <List.Item arrow="horizontal">设备位置</List.Item>
                            </Picker>
                        )}
                    </List>
                    {this.props.match!.params.key ? (<List style={{ borderTop: '10px solid #dcdcdc' }}>
                        <InputItem style={{ textAlign: 'right' }} placeholder="未知" value={this.state.device_info ? this.state.device_info.power : '未知'}>剩余电量</InputItem>
                        <List.Item style={{ borderTop: '10px solid #dcdcdc' }} arrow="horizontal" onClick={() => this.goToOtherNav(ROUTE_PATH.MyHistoryWarn + '/' + this.props.match!.params.key)}>历史报警信息</List.Item>
                        <Button type="primary" size='large' onClick={() => { this.setDelStatus(); }} style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>删除设备</Button>
                    </List>) :
                        <Button type="primary" size='large' htmlType="submit" style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>绑定</Button>}
                </Form>
                <Modal
                    visible={this.state.del_status}
                    transparent={true}
                    maskClosable={false}
                    onClose={() => { this.setDelStatus(false); }}
                    title="确定删除设备"
                    footer={[
                        { text: '取消', onPress: () => { this.setDelStatus(false); } },
                        { text: '确定', onPress: () => { this.delRoom(); } }]}
                />
            </div>
        );
    }
}

/**
 * 组件：创建设备视图控件
 * 控制我的家视图控件
 */
@addon('AddMyDeviceView', '创建设备视图控件', '控制创建设备视图控件')
@reactControl(Form.create<any>()(AddMyDeviceView), true)
export class AddMyDeviceViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}