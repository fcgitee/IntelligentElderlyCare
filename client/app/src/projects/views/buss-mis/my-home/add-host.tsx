import { List, Picker, InputItem, Modal, NavBar, Icon, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { request_func, ROUTE_PATH } from "src/projects/app/util-tool";
import { Form, Button, Row } from 'antd';
/**
 * 组件：新建主机视图控件状态
 */
export interface AddHostViewState extends ReactViewState {
    room_list: any; // 房间列表
    host_info: any;
    del_status: boolean;
    device_room_list: any; // 设备位置信息
}
/**
 * 组件：新建主机视图控件
 */
export class AddHostView extends ReactView<AddHostViewControl, AddHostViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_list: [],
            host_info: { name: '主机' },
            del_status: false,
            device_room_list: []
        };
    }
    componentDidMount() {
        request_func(this, AppServiceUtility.app_my_home_service.get_all_my_room_list, [{}], (data: any) => {
            const room_list = data.map((val: any) => {
                return { label: val.name, value: val.id };
            });
            this.setState({ room_list });
        });
        if (this.props.match!.params.key) { // 编辑
            // setMainFormTitle('房间信息');
            request_func(this, AppServiceUtility.app_my_home_service.get_my_host_detail, [{ id: this.props.match!.params.key }], (data: any) => {
                if (data.length) {
                    const device_room_info = data[0].device_room_info;
                    let device_list = data[0].device;
                    device_list.map((device_val: any) => {
                        device_room_info.map((val: any) => {
                            if (device_val.room_id === val.id) {
                                device_val.room_name = val.name;
                            }
                        });
                    });
                    data[0].device = device_list;
                    console.info('最终的设备数据>>>>>>>', data[0].device);
                    this.setState({ host_info: data[0] });
                } else {
                    Toast.fail('暂无数据', 3);
                }
            });
        }
    }
    // 新建/编辑主机
    handleSubmit = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            let param = { name: values.name, room_id: values.room_id[0], host_id: values.host_id };
            this.props.match!.params.key && Object.assign(param, { id: this.props.match!.params.key });
            request_func(this, AppServiceUtility.app_my_home_service.add_my_host, [param], (data: any) => {
                data.code === 200 ? Toast.success(data.msg, 3, () => {
                    this.goBackHostList();
                }) : Toast.fail(data.msg, 3,);
            });
        });
    }
    // 删除房间
    delMyHost = () => {
        this.setDelStatus(false);
        request_func(this, AppServiceUtility.app_my_home_service.del_my_host, [this.props.match!.params.key], (data: any) => {
            data.code === 200 ? Toast.success(data.msg, 3, () => {
                this.goBackHostList();
            }) : Toast.fail(data.msg, 3,);
        });
    }
    goBackHostList = (url: string = ROUTE_PATH.myHostList) => {
        this.props.history!.push(url);
    }
    setDelStatus = (isDel: boolean = true) => {
        if (isDel && this.state.host_info.device.length > 0) {
            Toast.fail('删除主机前，请移除该主机所绑定的设备！', 5);
            return;
        }
        this.setState({ del_status: isDel });
    }
    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                <NavBar
                    icon={<Icon type={"left"} onClick={() => { this.goBackHostList(); }} />}
                    rightContent={this.props.match!.params.key && <Row onClick={this.handleSubmit} type="flex" justify="end">更改</Row>}
                >
                    {
                        this.props.match!.params.key ? '主机信息' : '新建主机'
                    }
                </NavBar>
                <Form onSubmit={this.handleSubmit}>
                    <List style={{ backgroundColor: 'white' }} className="picker-list">
                        {getFieldDecorator('name', {
                            initialValue: this.state.host_info ? this.state.host_info.name : this.state.room_list[0].label,
                            rules: [{ required: true, message: '请输入主机名称!' }],
                        })(
                            <InputItem placeholder="请输入主机名称" style={{ textAlign: 'right' }}>主机名称</InputItem>
                        )}
                        {getFieldDecorator('host_id', {
                            initialValue: this.state.host_info ? this.state.host_info.host_id : '',
                            rules: [{ required: true, message: '请输入主机ID!' }],
                        })(
                            <InputItem placeholder="请输入主机ID" style={{ textAlign: 'right' }}>主机名称</InputItem>
                        )}
                        {getFieldDecorator('room_id', {
                            initialValue: this.state.host_info ? [this.state.host_info.room_id] : [this.state.room_list[0].value],
                            rules: [{ required: true, message: '请选择主机位置!' }],
                        })(
                            <Picker data={this.state.room_list} cols={1} className="forss">
                                <List.Item arrow="horizontal">主机位置</List.Item>
                            </Picker>
                        )}

                    </List>
                    {this.props.match!.params.key && this.state.host_info ? (
                        <List>
                            <List.Item style={{ fontSize: '14px', background: '#dcdcdc' }}>已绑定设备</List.Item>
                            {this.state.host_info && this.state.host_info!.device && this.state.host_info!.device.length ? (
                                this.state.host_info.device.map((val: any) => {
                                    return <List.Item arrow="horizontal" key={val.id} extra={val.room_name} onClick={() => this.goBackHostList(ROUTE_PATH.addMyDevice + '/' + val.id)}>{val.name}</List.Item>;
                                })
                            ) : <List.Item >暂无数据</List.Item>}
                            <List.Item style={{ borderTop: '10px solid #dcdcdc', borderBottom: 'none' }} arrow="horizontal" onClick={() => this.goBackHostList(ROUTE_PATH.addMyDevice)}>新增设备</List.Item>
                            <Button type="primary" size='large' htmlType="button" onClick={() => { this.setDelStatus(); }} style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>删除主机</Button></List>
                    ) : <Button type="primary" size='large' htmlType="submit" style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>绑定</Button>
                    }
                </Form>
                <Modal
                    visible={this.state.del_status}
                    transparent={true}
                    maskClosable={false}
                    onClose={() => { this.setDelStatus(false); }}
                    title="确定删除主机"
                    footer={[
                        { text: '取消', onPress: () => { this.setDelStatus(false); } },
                        { text: '确定', onPress: () => { this.delMyHost(); } }]}
                />
            </div>
        );
    }
}

/**
 * 组件：新建主机视图控件
 * 控制我的家视图控件
 */
@addon('AddHostView', '新建主机视图控件', '控制新建主机视图控件')
@reactControl(Form.create<any>()(AddHostView), true)
export class AddHostViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}