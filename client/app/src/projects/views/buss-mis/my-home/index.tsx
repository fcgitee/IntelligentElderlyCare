import { Card, NavBar, Drawer, Icon, List } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, request_func } from "src/projects/app/util-tool";
// import room_Pic from '../../../../static/img/房间.jpg';
/**
 * 组件：我的家视图控件状态
 */
export interface MyHomeViewState extends ReactViewState {
    room_list_label: any; // 房间列表
    open: boolean; // 是否打开侧边栏
}
/**
 * 组件：我的家视图控件
 */
export class MyHomeView extends ReactView<MyHomeViewControl, MyHomeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_list_label: [],
            open: false,
        };
    }
    componentDidMount() {
        const picture_list = {
            bedroom: require('../../../../static/img/卧室.jpg'),
            living: require('../../../../static/img/客厅.jpg'),
            entrance: require('../../../../static/img/玄关.jpg'),
            bathroom: require('../../../../static/img/卫生间.jpg'),
            kitchen: require('../../../../static/img/厨房.jpg'),
            balcony: require('../../../../static/img/阳台.jpg'),
            stairs: require('../../../../static/img/楼梯.jpg'),
            corridor: require('../../../../static/img/走廊.jpg'),
        };
        request_func(this, AppServiceUtility.app_my_home_service.get_my_room_list, [], (data: any) => {
            const room_list_label = data.map((val: any) => {
                return (
                    <Card style={{ height: '180px', cursor: 'pointer', margin: '10px', borderRadius: '20px' }} key={val.id} onClick={() => { this.goToOtherNav(ROUTE_PATH.myRoomDetail + '/' + val.id); }}>
                        <Card.Body>
                            <img src={`${picture_list[val.room_type]}`} style={{ display: 'block', width: '100%', height: '100%' }} />
                        </Card.Body>
                        <Card.Footer content={val.name} extra={<div>设备数量:&nbsp;{val.device_num}</div>} />
                    </Card>);
            });
            this.setState({ room_list_label });

        });

    }
    goToOtherNav = (url: string) => {
        this.props!.history!.push(url);
    }
    onOpenChange = () => {
        this.setState({ open: !this.state.open });
    }
    render() {
        setMainFormTitle('我的家');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.mine);
        });
        let nav = [{
            name: '主机管理',
            url: ROUTE_PATH.myHostList
        }, {
            name: '房间管理',
            url: ROUTE_PATH.myRoomList
        }, {
            name: '新增设备',
            url: ROUTE_PATH.addMyDevice
        }];
        // 侧边栏
        let sidebar = (
            <List style={{ marginTop: '40px' }}>
                {nav.map((val: any) => {
                    return <List.Item key={val.name} arrow='horizontal' onClick={() => { this.goToOtherNav(val.url); }}>{val.name}</List.Item>;
                })}</List>);
        return (
            <div>
                <NavBar
                    onClick={this.onOpenChange}
                    rightContent={<Icon type="ellipsis" />}
                    icon={<Icon type={"left"} />}
                    onLeftClick={() => { this.goToOtherNav(ROUTE_PATH.mine); }}
                >我的家
                </NavBar>
                <Drawer
                    className="my-drawer"
                    style={{ minHeight: window.innerHeight }}
                    enableDragHandle={true}
                    position='right'
                    contentStyle={{ color: '#A6A6A6', textAlign: 'left' }}
                    sidebar={sidebar}
                    open={this.state.open}
                    onOpenChange={this.onOpenChange}
                />
                {this.state.room_list_label}
            </div>
        );
    }
}

/**
 * 组件：我的家视图控件
 * 控制我的家视图控件
 */
@addon('MyHomeView', '我的家视图控件', '控制我的家视图控件')
@reactControl(MyHomeView, true)
export class MyHomeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}