import { NavBar, Icon, Toast, List } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, request_func } from "src/projects/app/util-tool";
/**
 * 组件：我的家视图控件状态
 */
export interface MyRoomDetailViewState extends ReactViewState {
    room_list_label: any; // 房间列表
    open: boolean; // 是否打开侧边栏
    room_info: any; // 房间详情
}
/**
 * 组件：我的家视图控件
 */
export class MyRoomDetailView extends ReactView<MyRoomDetailViewControl, MyRoomDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_list_label: [],
            open: false,
            room_info: {},
        };
    }
    componentDidMount() {

        // 获取房间详情
        request_func(this, AppServiceUtility.app_my_home_service.get_my_room_warning_detail, [{ id: this.props.match!.params.key }], (data: any) => {
            if (data.length) {
                let room_info = data[0];
                let warn_info_date: any = [];
                let warn_info_list: any = [];
                if (room_info.warn_info.length) { // 有警报信息
                    room_info.warn_info.map((val: any) => {
                        const info = val.time.split(' ');
                        let date = info[0];
                        let time = info[1];
                        let index: any = [warn_info_date.indexOf(date)][0];
                        let recode = {
                            date: date,
                            time: time,
                            value: val.warn_type
                        };
                        if (index > -1) {
                            warn_info_list[index].push(recode);
                            console.info(warn_info_list);
                        } else {
                            warn_info_list.push([recode]);
                            console.info(warn_info_list);
                            warn_info_date.push(date);
                        }
                    });
                    room_info.warn_info_list = warn_info_list;
                }
                console.info('最终时间>>>>>>>>>>>', room_info);
                console.info('设置了头部', room_info.name);
                this.setState({ room_info });
            } else {
                Toast.fail('暂无数据', 3,);
            }
        });

    }
    goToOtherNav = (url: string) => {
        this.props!.history!.push(url);
    }
    onOpenChange = () => {
        this.setState({ open: !this.state.open });
    }
    render() {
        const picture_list = {
            bedroom: require('../../../../static/img/卧室.jpg'),
            living: require('../../../../static/img/客厅.jpg'),
            entrance: require('../../../../static/img/玄关.jpg'),
            bathroom: require('../../../../static/img/卫生间.jpg'),
            kitchen: require('../../../../static/img/厨房.jpg'),
            balcony: require('../../../../static/img/阳台.jpg'),
            stairs: require('../../../../static/img/楼梯.jpg'),
            corridor: require('../../../../static/img/走廊.jpg'),
        };
        return (
            <div>
                <NavBar
                    onClick={this.onOpenChange}
                    rightContent={<Icon type="ellipsis" onClick={() => { this.goToOtherNav(ROUTE_PATH.addRoom + '/' + this.props!.match!.params.key); }} />}
                    icon={<Icon type={"left"} />}
                    onLeftClick={() => { this.goToOtherNav(ROUTE_PATH.myHome); }}
                >
                    {this.state.room_info.name}
                </NavBar>
                <img src={`${picture_list[this.state.room_info.room_type]}`} style={{ display: 'block', width: '100%', height: '200px' }} />
                {this.props.match!.params.key && this.state.room_info ? (
                    <List style={{ borderTop: '10px solid #dcdcdc' }}>
                        {this.state.room_info && this.state.room_info!.device && this.state.room_info!.device.length ? (
                            this.state.room_info.device.map((val: any) => {
                                return (
                                    <List key={val.id} style={{ borderTop: 'none' }}>
                                        <List.Item arrow="horizontal" key={val.id} onClick={() => this.goToOtherNav(ROUTE_PATH.addMyDevice + '/' + val.id)}>{val.name}</List.Item>
                                        {this.state.room_info.warn_info_list.length && this.state.room_info.warn_info_list.map((item: any) => {
                                            return (
                                                <List key={item.value} style={{ borderTop: 'none' }}>
                                                    <List.Item style={{ height: '-10px', background: '#dcdcdc', fontSize: '10px' }}>{item[0].date}</List.Item>
                                                    {item.map((info: any) => {
                                                        return <List.Item key={info.value} extra={info.value} style={{ borderBottom: 'none' }}>{info.time}</List.Item>;
                                                    })}
                                                </List>
                                            );
                                        })}
                                    </List>);
                            })
                        ) : <List.Item >暂无数据</List.Item>}
                    </List>
                ) : null
                }
            </div>
        );
    }
}

/**
 * 组件：我的家视图控件
 * 控制我的家视图控件
 */
@addon('MyRoomDetailView', '我的家视图控件', '控制我的家视图控件')
@reactControl(MyRoomDetailView, true)
export class MyRoomDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}