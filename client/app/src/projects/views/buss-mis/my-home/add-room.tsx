import { List, Picker, InputItem, Modal, NavBar, Icon, Toast } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { request_func, ROUTE_PATH } from "src/projects/app/util-tool";
import { Form, Button, Row } from 'antd';
/**
 * 组件：创建房间视图控件状态
 */
export interface AddMyRoomViewState extends ReactViewState {
    room_type_list: any; // 房间类型列表
    room_info: any;
    del_status: boolean;
}
/**
 * 组件：创建房间视图控件
 */
export class AddMyRoomView extends ReactView<AddMyRoomViewControl, AddMyRoomViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_type_list: [],
            room_info: {},
            del_status: false,
        };
    }
    componentDidMount() {
        request_func(this, AppServiceUtility.app_my_home_service.get_room_type_list, [], (data: any) => {
            const room_type_list = data.map((val: any) => {
                return { label: val.name, value: val.id };
            });
            this.setState({ room_type_list });
        });
        if (this.props.match!.params.key) { // 编辑
            // setMainFormTitle('房间信息');
            request_func(this, AppServiceUtility.app_my_home_service.get_my_room_detail, [{ id: this.props.match!.params.key }], (data: any) => {
                data.length ? this.setState({ room_info: data[0] }) : Toast.fail('暂无数据', 3);
            });
        }
    }
    // 新建/编辑房间
    handleSubmit = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            let param = { name: values.name, room_type: values.room_type[0] };
            this.props.match!.params.key && Object.assign(param, { id: this.props.match!.params.key });
            request_func(this, AppServiceUtility.app_my_home_service.update_room, [param], (data: any) => {
                data.code === 200 ? Toast.success(data.msg, 3, () => {
                    this.goBackRoomList();
                }) : Toast.fail(data.msg, 3,);
            });
        });
    }
    // 删除房间
    delRoom = () => {
        this.setDelStatus(false);
        request_func(this, AppServiceUtility.app_my_home_service.del_room, [this.props.match!.params.key], (data: any) => {
            data.code === 200 ? Toast.success(data.msg, 3, () => {
                this.goBackRoomList();
            }) : Toast.fail(data.msg, 3,);
        });
    }
    goBackRoomList = (url: string = ROUTE_PATH.myRoomList) => {
        this.props.history!.push(url);
    }
    setDelStatus = (isDel: boolean = true) => {
        if (isDel) {
            this.state.room_info.device.length && Toast.fail('删除房间前，请移除该房间所绑定的设备！', 5);
            this.state.room_info.host.length && Toast.fail('删除房间前，请移除该房间所绑定的主机！', 5);
            return;
        }
        this.setState({ del_status: isDel });
    }
    // 切换房间类型
    selectRoomType = (val: any) => {
        this.state.room_type_list.map((item: any) => {
            if (item.value === val[0]) {
                let room_info = this.state.room_info;
                room_info.room_type = val[0];
                room_info.name = item.label;
                this.setState({ room_info });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                <NavBar
                    icon={<Icon type={"left"} onClick={() => { history.back(); }} />}
                    rightContent={this.props.match!.params.key && <Row onClick={this.handleSubmit} type="flex" justify="end">更改</Row>}
                >
                    {
                        this.props.match!.params.key ? '房间信息' : '创建房间'
                    }
                </NavBar>
                <Form onSubmit={this.handleSubmit}>
                    <List style={{ backgroundColor: 'white' }} className="picker-list">
                        {getFieldDecorator('room_type', {
                            initialValue: this.state.room_info ? this.state.room_info.room_type : [this.state.room_type_list[0].value],
                            rules: [{ required: true, message: '请选择房间类型!' }],
                        })(
                            <Picker data={this.state.room_type_list} cols={1} className="forss" onOk={this.selectRoomType}>
                                <List.Item arrow="horizontal">房间类型</List.Item>
                            </Picker>
                        )}
                        {getFieldDecorator('name', {
                            initialValue: this.state.room_info ? this.state.room_info.name : this.state.room_type_list[0].label,
                            rules: [{ required: true, message: '请输入房间名称!' }],
                        })(
                            <InputItem placeholder="请输入房间名称" style={{ textAlign: 'right' }}>房间名称</InputItem>
                        )}

                    </List>
                    {this.props.match!.params.key && this.state.room_info ? (
                        <List>
                            <List.Item style={{ fontSize: '14px', background: '#dcdcdc' }}>已绑定设备</List.Item>
                            {this.state.room_info && this.state.room_info!.device && this.state.room_info!.device.length ? (
                                this.state.room_info.device.map((val: any) => {
                                    return <List.Item arrow="horizontal" key={val.id} onClick={() => this.goBackRoomList(ROUTE_PATH.addMyDevice + '/' + val.id)}>{val.name}</List.Item>;
                                })
                            ) : <List.Item >暂无数据</List.Item>}
                            <List.Item style={{ borderTop: '10px solid #dcdcdc', borderBottom: 'none' }} arrow="horizontal" onClick={() => this.goBackRoomList(ROUTE_PATH.addMyDevice)}>新增设备</List.Item>
                            <Button type="primary" size='large' htmlType="button" onClick={() => { this.setDelStatus(); }} style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>删除房间</Button></List>
                    ) : <Button type="primary" size='large' htmlType="submit" style={{ width: '90%', margin: '20px 5%', borderRadius: '50px' }}>确认</Button>
                    }
                </Form>
                <Modal
                    visible={this.state.del_status}
                    transparent={true}
                    maskClosable={false}
                    onClose={() => { this.setDelStatus(false); }}
                    title="确定删除房间"
                    footer={[
                        { text: '取消', onPress: () => { this.setDelStatus(false); } },
                        { text: '确定', onPress: () => { this.delRoom(); } }]}
                />
            </div>
        );
    }
}

/**
 * 组件：创建房间视图控件
 * 控制我的家视图控件
 */
@addon('AddMyRoomView', '创建房间视图控件', '控制创建房间视图控件')
@reactControl(Form.create<any>()(AddMyRoomView), true)
export class AddMyRoomViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}