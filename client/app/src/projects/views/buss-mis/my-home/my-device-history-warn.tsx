import { NavBar, Icon, List, DatePicker } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, request_func } from "src/projects/app/util-tool";
/**
 * 组件：历史报警信息视图控件状态
 */
export interface MyHistoryViewState extends ReactViewState {
    room_list_label: any; // 房间列表
    open: boolean; // 是否打开侧边栏
    room_info: any; // 房间详情
    date: any; // 日期
}
/**
 * 组件：历史报警信息视图控件
 */
export class MyHistoryView extends ReactView<MyHistoryViewControl, MyHistoryViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_list_label: [],
            open: false,
            room_info: {},
            date: null,
        };
    }
    componentDidMount() {
        this.getRoomWarn({ id: this.props.match!.params.key });
    }
    getRoomWarn = (param: object) => {
        // 获取房间详情
        request_func(this, AppServiceUtility.app_my_home_service.get_my_history_warn, [param], (data: any) => {
            if (data.length) {
                let room_info = data[0];
                let warn_info_date: any = [];
                let warn_info_list: any = [];
                if (room_info.warn_info.length) { // 有警报信息
                    room_info.warn_info.map((val: any) => {
                        const info = val.time.split(' ');
                        let date = info[0];
                        let time = info[1];
                        let index: any = [warn_info_date.indexOf(date)][0];
                        let recode = {
                            date: date,
                            time: time,
                            value: val.warn_type
                        };
                        if (index > -1) {
                            warn_info_list[index].push(recode);
                            console.info(warn_info_list);
                        } else {
                            warn_info_list.push([recode]);
                            console.info(warn_info_list);
                            warn_info_date.push(date);
                        }
                    });
                    room_info.warn_info_list = warn_info_list;
                }
                setMainFormTitle(room_info.name); // 设置头部标题
                this.setState({ room_info });
            } else {
                this.setState({ room_info: {} }); // 置空
                // Toast.fail('暂无数据', 3,);

            }
        });

    }
    goToOtherNav = (url: string) => {
        this.props!.history!.push(url);
    }
    // 切换日期
    changeDate = (date: any) => {
        this.getRoomWarn({ id: this.props.match!.params.key, date: date });
        this.setState({ date: date });
        console.info('日期', date);
    }
    render() {
        return (
            <div>
                <NavBar
                    icon={<Icon type={"left"} />}
                    onLeftClick={() => { this.goToOtherNav(ROUTE_PATH.addMyDevice + '/' + this.props!.match!.params.key); }}
                >历史报警信息
                </NavBar>
                <DatePicker
                    mode="date"
                    title="请选择报警日期"
                    onChange={date => this.changeDate(date)}
                    value={this.state.date}
                >
                    <List.Item arrow="horizontal">日期</List.Item>
                </DatePicker>
                {
                    this.state.room_info.warn_info_list ? (
                        <List>
                            {this.state.room_info.warn_info_list.map((item: any) => {
                                return (
                                    <List key={item.value} style={{ border: 'none' }}>
                                        <List.Item style={{ height: '-10px !important', background: '#dcdcdc', fontSize: '10px' }}>{item[0].date}</List.Item>
                                        {item.map((info: any) => {
                                            return <List.Item key={info.value} extra={info.value} style={{ borderBottom: 'none' }}>{info.time}</List.Item>;
                                        })}
                                    </List>
                                );
                            })}
                        </List>
                    ) : <List.Item >暂无数据</List.Item>
                }
            </div >
        );
    }
}

/**
 * 组件：历史报警信息视图控件
 * 控制历史报警信息视图控件
 */
@addon('MyHistoryView', '历史报警信息视图控件', '控制历史报警信息视图控件')
@reactControl(MyHistoryView, true)
export class MyHistoryViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}