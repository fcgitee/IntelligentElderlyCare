import { List } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, request_func } from "src/projects/app/util-tool";
/**
 * 组件：主机管理视图控件状态
 */
export interface MyHostListViewState extends ReactViewState {
    // 我的房间列表
    host_list: Array<object>;
}
/**
 * 组件：主机管理视图控件
 */
export class MyHostListView extends ReactView<MyHostListViewControl, MyHostListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            host_list: [],
        };
    }
    componentDidMount() {
        request_func(this, AppServiceUtility.app_my_home_service.get_my_host_list, [{}], (data: any) => {
            this.setState({ host_list: data });
        });
    }
    goToRoomDetail = (host_id: string = '') => {
        const url = host_id === '' ? ROUTE_PATH.addHost : ROUTE_PATH.addHost + '/' + host_id;
        this.props!.history!.push(url);
    }
    render() {
        setMainFormTitle('主机管理');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.myHome);
        });
        return (
            <div>
                <List style={{ backgroundColor: 'white' }} className="picker-list">
                    {this.state.host_list.length ? (
                        this.state.host_list.map((val: any) => {
                            return <List.Item arrow="horizontal" key={val.id} onClick={() => { this.goToRoomDetail(val.id); }}>{val.name}</List.Item>;
                        })
                    ) : <List.Item >暂无数据</List.Item>}
                </List>
                <List>
                    <List.Item arrow="horizontal" style={{ borderTop: '10px solid #dcdcdc', borderBottom: 'none' }} onClick={() => { this.goToRoomDetail(); }}>新增主机</List.Item>
                </List>
            </div >
        );
    }
}

/**
 * 组件：主机管理视图控件
 * 控制我的家视图控件
 */
@addon('MyHostListView', '主机管理视图控件', '控制主机管理视图控件')
@reactControl(MyHostListView, true)
export class MyHostListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}