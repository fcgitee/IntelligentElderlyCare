import { List } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, request_func } from "src/projects/app/util-tool";
/**
 * 组件：房间管理视图控件状态
 */
export interface MyRoomListViewState extends ReactViewState {
    // 我的房间列表
    room_info: Array<object>;
}
/**
 * 组件：房间管理视图控件
 */
export class MyRoomListView extends ReactView<MyRoomListViewControl, MyRoomListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            room_info: [],
        };
    }
    componentDidMount() {
        request_func(this, AppServiceUtility.app_my_home_service.get_all_my_room_list, [{}], (data: any) => {
            this.setState({ room_info: data });
        });
    }
    goToRoomDetail = (room_id: string = '') => {
        const url = room_id === '' ? ROUTE_PATH.addRoom : ROUTE_PATH.addRoom + '/' + room_id;
        this.props!.history!.push(url);
    }
    render() {
        setMainFormTitle('房间管理');
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.myHome);
        });
        return (
            <div>
                <List style={{ backgroundColor: 'white' }} className="picker-list">
                    {this.state.room_info.length ? (
                        this.state.room_info.map((val: any) => {
                            return <List.Item arrow="horizontal" key={val.id} onClick={() => { this.goToRoomDetail(val.id); }}>{val.name}</List.Item>;
                        })
                    ) : <List.Item >暂无数据</List.Item>}
                </List>
                <List>
                    <List.Item arrow="horizontal" style={{ borderTop: '10px solid #dcdcdc', borderBottom: 'none' }} onClick={() => { this.goToRoomDetail(); }}>创建房间</List.Item>
                </List>
            </div >
        );
    }
}

/**
 * 组件：房间管理视图控件
 * 控制我的家视图控件
 */
@addon('MyRoomListView', '房间管理视图控件', '控制房间管理视图控件')
@reactControl(MyRoomListView, true)
export class MyRoomListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}