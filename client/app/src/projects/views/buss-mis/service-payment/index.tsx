import { Avatar, Input, Rate } from 'antd';
import { Button, Card, Icon, List, SearchBar, SegmentedControl } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import './index.less';
const Item = List.Item;
const { Search } = Input;
/**
 * 组件：服务支付状态
 */
export interface ServicePaymentViewState extends ReactViewState {
    title?: any;
    state?: any;
}

/**
 * 组件：服务支付
 * 描述
 */
export class ServicePaymentView extends ReactView<ServicePaymentViewControl, ServicePaymentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            title: '',
            state: '默认'
        };
    }
    componentDidMount() {
        setMainFormTitle('服务支付');

        this.setState({
            title: '补贴账户'
        });
    }
    titleChange = (e: any) => {
        // console.log(e.nativeEvent.value);
        this.setState({
            title: e.nativeEvent.value
        });
    }
    // 充值/提现
    goCZorTX = () => {
        setMainFormTitle('充值/提现');
        this.setState({
            title: '充值'
        });
    }
    // 设置付款顺序
    goSetSX = () => {
        setMainFormTitle('设置付款顺序');
        this.setState({
            state: '设置'
        });
    }
    render() {
        const { title, state } = this.state;
        return (
            <div style={{ backgroundColor: 'rgba(217, 217, 217, 0.5)', width: '100%', height: document.body.clientHeight, overflow: 'hidden' }}>
                <SearchBar placeholder="Search" maxLength={8} />
                <div className='touxiang'>
                    <div style={{ marginTop: '20px', display: 'flex' }}>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1578916184&di=81366179664167a50c22cf0c4215710c&src=http://k.zol-img.com.cn/dcbbs/24956/a24955103_01000.jpg" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>林霞</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="http://img4.imgtn.bdimg.com/it/u=1239732185,3510771801&fm=26&gp=0.jpg" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>廖琴仙</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="http://img1.imgtn.bdimg.com/it/u=2952257403,2955814340&fm=26&gp=0.jpg" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>潘达海</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="http://img2.imgtn.bdimg.com/it/u=3896305893,3298977345&fm=26&gp=0.jpg" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>李沁</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                    </div>
                </div>
                {state === '默认' ?
                    <div style={{ height: '100%', overflow: 'scroll' }}>
                        <List>
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px' }} arrow="horizontal" onClick={this.goSetSX}>设置付款顺序</Item>
                        </List>
                        <SegmentedControl
                            style={{ margin: '10px 10px 15px 10px' }}
                            values={['补贴账户', '受捐账户', '自费账户']}
                            onChange={this.titleChange}
                        />
                        <div style={{ height: '100%', overflow: 'scroll' }}>
                            <div style={{ width: '100%', height: '80px' }}>
                                {title === '自费账户' ?
                                    <div style={{ float: 'left', marginLeft: '10px', marginTop: '10px', width: '120px' }}>
                                        <Button type="primary" onClick={this.goCZorTX}>充值/提现</Button>
                                    </div>
                                    : ''
                                }
                                <div style={{ float: 'right', textAlign: 'right', marginRight: '10px' }}>
                                    <p>账户余额：￥500</p>
                                    <p>其中：养老账户余额：￥300</p>
                                </div>
                            </div>
                            <div style={{ margin: '10px', overflow: 'scroll' }}>
                                {/* 补贴 */}
                                {title === '补贴账户' ?
                                    <Card style={{ marginBottom: '10px' }}>
                                        <Card.Header
                                            title="2月电子老友券"
                                            extra={<span>有效期至2020.2.5</span>}
                                        />
                                        <Card.Body>
                                            <div>面值：  ￥500</div>
                                            <div>已用金额：    ￥200</div>
                                        </Card.Body>
                                        <Card.Footer extra={<div>剩余：￥300&nbsp;&nbsp;点击用券</div>} />
                                    </Card>
                                    : ''
                                }
                                {title === '受捐账户' ?
                                    <Card style={{ marginBottom: '10px' }}>
                                        <Card.Header
                                            title="冠名基金"
                                            extra={<span>捐款时间：2020.2.5</span>}
                                        />
                                        <Card.Body>
                                            <div>你捐款超过300元，已成为该基金的捐款达人</div>
                                        </Card.Body>
                                        <Card.Footer extra={<div>捐款金额：￥300</div>} />
                                    </Card>
                                    : ''
                                }
                                {title === '自费账户' ?
                                    <div style={{ height: '1000px', overflow: 'scroll' }}>
                                        <Card style={{ marginBottom: '10px' }}>
                                            <Card.Header
                                                title="账户充值"
                                                extra={<span>捐款时间：2020.2.5</span>}
                                            />
                                            <Card.Body>
                                                <div>你的儿子（廖凡）为你充值了100元，请查收</div>
                                            </Card.Body>
                                            <Card.Footer extra={<div>充值金额：￥100&nbsp;&nbsp;点击使用</div>} />
                                        </Card>
                                        <Card style={{ marginBottom: '10px' }}>
                                            <Card.Header
                                                title="服务商"
                                                extra={<span>购买时间：2020.1.5</span>}
                                            />
                                            <Card.Body>
                                                <div style={{ overflow: 'hidden' }}>
                                                    <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                        <img src="https://www.e-health100.com/api/attachment/agencyphoto/1088_logoPhoto" alt="" style={{ width: '130px', height: '130px' }} />
                                                    </div>
                                                    <div style={{ float: 'left' }}>
                                                        <p><strong>助洁服务</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥150</p>
                                                        <p>服务清单</p>
                                                        <p>已完成&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;未评价</p>
                                                    </div>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer content="共一项服务" extra={<div>合计：￥150</div>} />
                                        </Card>
                                        <Card style={{ marginBottom: '10px' }}>
                                            <Card.Header
                                                title="服务商"
                                                extra={<span>购买时间：2020.2.5</span>}
                                            />
                                            <Card.Body>
                                                <div style={{ overflow: 'hidden' }}>
                                                    <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                        <img src="http://dmimg.5054399.com/allimg/pkm/pk/22.jpg" alt="" style={{ width: '130px', height: '130px' }} />
                                                    </div>
                                                    <div style={{ float: 'left' }}>
                                                        <p><strong>服务套餐名称</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥XX</p>
                                                        <p>服务清单</p>
                                                        <p style={{ textAlign: 'right' }}>预约时间：2020.0.10 15：00</p>
                                                        <p style={{ textAlign: 'right' }}>2020.0.10 15：00</p>
                                                        <p style={{ textAlign: 'right' }}>2020.0.10 15：00</p>
                                                    </div>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer content="共一项服务" extra={<div>合计：￥XX</div>} />
                                        </Card>
                                        <Card style={{ marginBottom: '10px' }}>
                                            <Card.Header
                                                title="服务商"
                                                extra={<span>购买时间：2020.2.5</span>}
                                            />
                                            <Card.Body>
                                                <div>
                                                    <div style={{ overflow: 'hidden' }}>
                                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                            <img src="http://dmimg.5054399.com/allimg/pkm/pk/22.jpg" alt="" style={{ width: '130px', height: '130px' }} />
                                                        </div>
                                                        <div style={{ float: 'left' }}>
                                                            <p><strong>服务套餐名称</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥XX</p>
                                                            <p>服务清单</p>
                                                            <p>预约时间：2020.1.9</p>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div style={{ overflow: 'hidden' }}>
                                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                            <img src="http://dmimg.5054399.com/allimg/pkm/pk/22.jpg" alt="" style={{ width: '130px', height: '130px' }} />
                                                        </div>
                                                        <div style={{ float: 'left' }}>
                                                            <p><strong>服务套餐名称</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥XX</p>
                                                            <p>服务清单</p>
                                                            <p>预约时间：2020.1.9</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer content="共两项服务" extra={<div>合计：￥XX</div>} />
                                        </Card>
                                        <Card style={{ marginBottom: '10px' }}>
                                            <Card.Header
                                                title="服务商"
                                                extra={<span>购买时间：2020.2.5</span>}
                                            />
                                            <Card.Body>
                                                <div>
                                                    <div style={{ overflow: 'hidden' }}>
                                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                            <img src="http://dmimg.5054399.com/allimg/pkm/pk/22.jpg" alt="" style={{ width: '130px', height: '130px' }} />
                                                        </div>
                                                        <div style={{ float: 'left' }}>
                                                            <p><strong>服务套餐名称</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥XX</p>
                                                            <p>服务清单</p>
                                                            <p>预约时间：<Rate /></p>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div style={{ overflow: 'hidden' }}>
                                                        <div style={{ float: 'left', marginRight: '20px', border: '1px solid' }}>
                                                            <img src="http://dmimg.5054399.com/allimg/pkm/pk/22.jpg" alt="" style={{ width: '130px', height: '130px' }} />
                                                        </div>
                                                        <div style={{ float: 'left' }}>
                                                            <p><strong>服务套餐名称</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;￥XX</p>
                                                            <p>服务清单</p>
                                                            <p>预约时间：<Rate /></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer content="共一项服务" extra={<div>合计：￥XX</div>} />
                                        </Card>
                                    </div>
                                    : ''
                                }
                                {title === '充值' ?
                                    <div style={{ margin: '10px' }}>
                                        充值金额：<Search
                                            placeholder="请输入充值金额"
                                            enterButton="充值"
                                            size="large"
                                            onSearch={value => { }}
                                        />
                                        <br />
                                        <br />
                                        提现金额：<Search
                                            placeholder="请输入提现金额"
                                            enterButton="提现"
                                            size="large"
                                            onSearch={value => { }}
                                        />
                                    </div>
                                    :
                                    ''
                                }
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        <List>
                            <Item style={{ lineHeight: '50px', paddingLeft: '15px' }}>设置付款顺序</Item>
                        </List>
                        <table style={{ backgroundColor: '#ffffff', width: '100%' }}>
                            <tr style={{ width: '100%', height: '45px', textAlign: 'center' }}>
                                <td style={{ border: '1px solid', width: '10%' }}>1</td>
                                <td style={{ border: '1px solid', width: '40%' }}>补贴账户</td>
                                <td style={{ border: '1px solid', width: '10%' }} />
                                <td style={{ border: '1px solid', width: '10%' }}><Icon type='down' /></td>
                            </tr>
                            <tr style={{ width: '100%', height: '45px', textAlign: 'center' }}>
                                <td style={{ border: '1px solid', width: '10%' }}>2</td>
                                <td style={{ border: '1px solid', width: '40%' }}>受捐账户</td>
                                <td style={{ border: '1px solid', width: '10%' }}><Icon type='up' /></td>
                                <td style={{ border: '1px solid', width: '10%' }}><Icon type='down' /></td>
                            </tr>
                            <tr style={{ width: '100%', height: '45px', textAlign: 'center' }}>
                                <td style={{ border: '1px solid', width: '10%' }} rowSpan={3}>3</td>
                                <td style={{ border: '1px solid', width: '40%' }}>自费账户</td>
                                <td style={{ border: '1px solid', width: '10%' }}><Icon type='up' /></td>
                                <td style={{ border: '1px solid', width: '10%' }} />
                            </tr>
                            <tr style={{ width: '100%', height: '45px', textAlign: 'center' }}>
                                <td>充值余额</td>
                                <td />
                                <td><Icon type='down' /></td>
                            </tr>
                            <tr style={{ width: '100%', height: '45px', textAlign: 'center', borderBottom: '1px solid', }}>
                                <td>现金支付</td>
                                <td><Icon type='up' /></td>
                                <td />
                            </tr>
                        </table>
                    </div>
                }
            </div>
        );
    }
}

/**
 * 控件：服务支付控制器
 * 描述
 */
@addon('ServicePaymentView', '服务支付', '描述')
@reactControl(ServicePaymentView, true)
export class ServicePaymentViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}