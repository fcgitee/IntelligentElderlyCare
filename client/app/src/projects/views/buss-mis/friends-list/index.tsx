import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { List, WhiteSpace } from "antd-mobile";
import { Avatar } from "antd";
const Item = List.Item;

/**
 * 组件：好友列表状态
 */
export interface FriendsListViewState extends BaseReactElementState {
}

/**
 * 组件：好友列表
 * 描述
 */
export class FriendsListView extends BaseReactElement<FriendsListViewControl, FriendsListViewState> {
    render() {
        return (
            <div>
                <List>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                    <Item
                        thumb={<Avatar size='large' icon="user" />}
                        arrow='empty'
                    >
                        成默默
                    </Item>
                    <WhiteSpace size="lg" />
                </List>
            </div>
        );
    }
}

/**
 * 控件：好友列表控制器
 * 描述
 */
@addon('FriendsListView', '好友列表', '描述')
@reactControl(FriendsListView)
export class FriendsListViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}