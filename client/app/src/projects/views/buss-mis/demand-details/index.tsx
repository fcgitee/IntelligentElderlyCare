import { Col, Icon, Rate, Row } from "antd";
import { Card, Carousel, List, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const Item = List.Item;
const Brief = Item.Brief;
/**
 * 组件：需求详情视图控件状态
 */
export interface DemandDetailsViewState extends ReactViewState {
    /** 图片高度 */
    imgHeight?: number | string;
    /** 数据对象 */
    elderdata?: any;
    /** 需求对象 */
    demand?: any;
}
/**
 * 组件：需求详情视图控件
 */
export class DemandDetailsView extends ReactView<DemandDetailsViewControl, DemandDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            imgHeight: 176,
            elderdata: {
                organization_info: {
                    organization_nature: '',
                    address: '',
                    telephone: '',
                    comment: '',
                    picture_list: [],
                }
            },
            demand: {},
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            AppServiceUtility.service_demand_service.get_demand_details!({ 'id': this.props.match!.params.key })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            demand: data.result[0]
                        });
                    }

                });
        }
    }
    getNow(date: any) {
        if (!date) {
            return '';
        }
        date = date.substring(0, 19);
        // 必须把日期'-'转为'/'
        date = date.replace(/-/g, '/');
        let timestamp = new Date(date).getTime();
        let timestamps = new Date().getTime();
        return Math.floor(Math.abs(timestamps - timestamp) / (24 * 3600 * 1000 * 360));
    }
    getTime(data: string) {
        let day_list = data.split(' ');
        let day = day_list[0].split('-');
        let time = day[1] + '月' + day[2] + '日 ';
        let s = day_list[1].split(':');
        time = time + s[0] + ":" + s[1];
        return time;
    }
    servicerClick = (id: string) => {
        this.props.history!.push(ROUTE_PATH.serverDetail + '/' + id);
    }
    create_dow(outer_layer: any, inner_layer: any) {
        let dom: any[] = [];
        outer_layer.map((value: any, index: number) => {
            for (let i in inner_layer) {
                if (inner_layer[i].bidders_organization_id === value.id) {
                    let v = inner_layer[i];
                    dom.push(
                        <Card className='list-conten' key={index} onClick={this.servicerClick.bind(this, value.id)}>
                            <Row type='flex' justify='center'>
                                <Col className='list-col' span={10}><img src={value.picture_list && value.picture_list.length > 0 ? value.picture_list[0] : ''} style={{ height: '72pt', width: '100pt' }} /></Col>
                                <Col span={14} className='list-col'>
                                    <Row>
                                        <Col span={16}><strong>{value.name}</strong></Col>
                                        <Col span={8}><strong style={{ color: 'orange' }}>￥{v.bidding_price}</strong></Col>
                                    </Row>
                                    <Row style={{ fontSize: '12px' }}>
                                        <Rate character={<Icon style={{ fontSize: "14px" }} type="star" theme="filled" />} allowHalf={true} disabled={true} value={value.star_level} />
                                    </Row>
                                    <Row>
                                        <Col span={8} style={{ fontSize: '12px' }}>{value.organization_info.telephone}</Col>
                                        <Col span={16} style={{ fontSize: '12px' }}>竞单时间：{v.create_date ? this.getTime(v.create_date) : ''}</Col>
                                    </Row>
                                    <Row style={{ fontSize: '14px' }}>

                                        <span
                                            style={{
                                                wordBreak: 'break-all',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: ' -webkit-box',
                                                WebkitLineClamp: 1,
                                                WebkitBoxOrient: 'vertical',
                                            }}
                                        >
                                            {value.organization_info.address}
                                        </span>
                                    </Row>
                                    <Row>
                                        <Col span={12}>{value.town} | 1.4km</Col>
                                        <Col span={12} style={{ textAlign: 'right' }}>
                                            <span style={{ marginRight: '10px' }}>
                                                <Icon type="heart" theme="filled" style={{ marginRight: '5px' }} />0
                                            </span>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    );
                    return;
                }
            }
        });
        return dom;
    }
    cancel_demand = () => {
        AppServiceUtility.service_demand_service.cancel_demand!({ 'id': this.props.match!.params.key })!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.info('取消需求成功');
                    this.props.history!.go(-1);
                } else {
                    Toast.info('取消需求失败');
                }

            });
    }
    edit_demand = () => {
        this.props.history!.push(ROUTE_PATH.demandRelease + '/' + this.props.match!.params.key);
    }
    render() {
        setMainFormTitle('需求详情');
        let info = this.state.demand;
        let plclist = info.demand_picture;
        // plclist = data.organization_info.picture_list;
        let dom = (
            <div>
                {
                    plclist && plclist.length > 0 ?
                        <Carousel
                            autoplay={true}
                            infinite={true}
                            className="acasdasdasd"
                        >
                            {plclist!.map((data: any, index: number) => {
                                return (
                                    <a
                                        className='carousel-a'
                                        key={index}
                                        href="javascript:;"
                                        style={{ height: this.state.imgHeight }}
                                    >
                                        <img
                                            className='carousel-img'
                                            src={data}
                                            alt=""
                                            onLoad={() => {
                                                window.dispatchEvent(new Event('resize'));
                                                this.setState({ imgHeight: 'auto' });
                                            }}
                                        />
                                    </a>
                                );
                            })}
                        </Carousel>
                        :
                        ''
                }

                <WhiteSpace />
                <List>
                    <Item
                        thumb={<img src='https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1385781871,3367469142&fm=26&gp=0.jpg' style={{ height: "50px", width: '50px', borderRadius: '50%' }} />}
                        onClick={() => { }}
                        extra={<Icon className='item-icon-size' type="compass" theme="filled" />}
                    >
                        <span>{info.elder_name}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{info.sex}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{this.getNow(info.date_birth)}周岁</span>

                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<span>紧急联系人</span>}
                        multipleLine={true}
                        extra={<a href={info.emergency_contact ? "tel:" + info.emergency_contact[0].personnel_info.telephone : ""}><Icon className='item-icon-size' type="phone" theme="filled" /></a>}
                    >
                        <Brief>
                            {
                                info.emergency_contact ? info.emergency_contact[0].name : ''
                            }
                        </Brief>
                    </Item>
                </List>

                <WhiteSpace />
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title={info.demand_title}
                        extra={<span>{info.price_type}<span style={{ color: 'orange' }}>￥{info.price}</span></span>}
                    />
                    <Card.Body>
                        <List>
                            <Item>
                                <Brief>服务类型：{info.service_type && info.service_type.length > 0 ? info.service_type[0].name : ''}</Brief>
                            </Item>
                            <Item
                                thumb={<span>预约时间：</span>}
                            >
                                <Brief>{info.start_date}</Brief>
                                <Brief>{info.end_date}</Brief>
                            </Item>
                        </List>
                        <Row
                            type='flex'
                            justify='center'
                        >
                            <span style={{ fontSize: '16px' }}>需求说明</span>
                        </Row>
                        <div style={{ padding: '20px' }}>
                            {info.demand_explain}
                        </div>
                    </Card.Body>
                </Card>
                {
                    info.state && info.state === '有人竞单' && info.state !== '取消需求' ?
                        <div className="confirmation-orders-buttom info-list" onClick={this.cancel_demand}>
                            取消
                        </div>
                        :
                        info.state === '等待接单' ?
                            <div className="confirmation-orders-buttom info-list">
                                <Row style={{ height: '100%' }}>
                                    <Col span={12} style={{ height: '100%', textAlign: 'center', lineHeight: '50px', borderRight: '1px solid #fff' }} onClick={this.edit_demand}>
                                        编辑
                        </Col>
                                    <Col span={12} style={{ height: '100%', textAlign: 'center', lineHeight: '50px' }} onClick={this.cancel_demand}>
                                        取消
                        </Col>
                                </Row>
                            </div> : ''
                }
                <Card full={true} className='list-contents'>
                    <Card.Body>
                        {
                            info.bidding_organization && info.bidding_organization.length > 0 ?
                                this.create_dow(info.bidding_organization, info.bidding_servicer) : ''
                        }
                    </Card.Body>
                </Card>
            </div>
        );
        return (dom);
    }
}

/**
 * 组件：需求详情视图控件
 * 控制需求详情视图控件
 */
@addon('DemandDetailsView', '需求详情视图控件', '控制需求详情视图控件')
@reactControl(DemandDetailsView, true)
export class DemandDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}