import { Collapse } from 'antd';
import { DatePicker, List, Modal, Result, Tabs, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH } from "src/projects/app/util-tool";
// import './index.less';
const Item = List.Item;
const prompt = Modal.prompt;
const alert = Modal.alert;
const { Panel } = Collapse;
const myImg = (src: any) => <img src={src} className="spe am-icon am-icon-md" alt="" />;
/**
 * 组件：补贴申请状态视图状态
 */
export interface OldApplyStateViewState extends BaseReactElementState {
    data?: any;
    content?: any;
    id_card?: any;
    id_card_address?: any;
    time?: any;
    showState?: any;
    select_data?: any;
    year_data?: any;
    year?: any;
    month?: any;
    show_year?: any;
}

/**
 * 组件：补贴申请状态视图
 * 补贴申请状态
 */
export class OldApplyStateView extends BaseReactElement<OldApplyStateViewControl, OldApplyStateViewState> {
    constructor(props: any) {
        super(props);
        const date = new Date();
        this.state = {
            data: {},
            id_card: '',
            id_card_address: '',
            time: '',
            showState: false,
            select_data: {},
            year_data: [],
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            show_year: ''
        };
    }
    componentDidMount() {
        // let a = "?certToken=92426f5b-be50-44c0-9bb1-61b051331915&retCode=0&errorMsg=%E6%88%90%E5%8A%9F";
        // // console.log(window.location.search === '');
        // console.log(this.props.match!.params.key);
        if (!this.props.match!.params.key) {
            prompt('身份证' + window.location.search, '请输入身份证号码', [
                { text: '返回' },
                { text: '确认', onPress: value => this.selectRecord(value) },
            ]);
        } else {
            if (window.location.search === '') {
                this.selectRecord(this.props.match!.params.key);
            } else {
                // 拿到返回的certToken
                let State = window.location.search.split('?')[1].split('&')[1].split('=')[1];
                if (State === '0') {
                    let certToken = window.location.search.split('?')[1].split('&')[0].split('=')[1];
                    AppServiceUtility.allowance_service.get_rz_result!({ "certToken": certToken })!
                        .then((data: any) => {
                            alert('认证结果', data[0].result, [
                                {
                                    text: '确认', onPress: () => {
                                    }
                                },
                            ]);
                            let id_card_num = data[1].id_card_num;
                            this.selectRecord(id_card_num);
                        });
                }
            }
        }
        // // console.log(a.split('?')[1].split('&')[1].split('=')[1] === '0');
    }

    selectRecord = (id_card_num: any) => {
        // console.log(id_card_num);
        // const date = new Date();
        AppServiceUtility.allowance_service.get_old_age_allowance_status!({ id_card_num, 'month': new Date().getMonth() })!
            .then((data: any) => {
                console.log(data, 'niubi');
                if (data === '你本月还未有认证记录') {
                    // console.log(111111111111111111111111111111111);
                    alert('查询结果', '你本月还未进行认证,请先前往认证', [
                        { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.applyReadinClause) },
                    ]);
                } else if (data === '该长者不在名单表中') {
                    Toast.info('该长者不在名单表中');
                } else {
                    this.setState(
                        {
                            data: data,
                        },
                        () => {
                            this.recordChange('');
                        }
                    );
                    return;
                }
            });
    }
    dateChange = (value: any) => {
        // console.log(value);
        this.setState({
            time: value
        });
        AppServiceUtility.allowance_service.get_old_age_allowance_status!({ 'id_card_num': this.state.data.id_card_num ? this.state.data.id_card_num : '', "time": value })!
            .then((data: any) => {
                // console.log(data);
                if (data === '你本月还未有发放记录') {
                    this.setState(
                        {
                            showState: false,
                            time: ''
                        });
                } else {
                    this.setState({
                        showState: true,
                        select_data: data
                    });
                }
            });
    }
    recordChange = (value: any) => {
        // console.log(value);
        const date = new Date();
        let year: any = Number;
        if (value !== '') {
            year = value;
            // console.log(year);
        } else {
            year = date.getFullYear();
            // console.log(year);
        }
        AppServiceUtility.allowance_service.get_old_age_allowance_status!({ 'id_card_num': this.state.data.id_card_num ? this.state.data.id_card_num : '', 'year': year })!
            .then((data: any) => {
                // console.log("一整年的数据》》》》》》》》》》", data);
                if (data === []) {
                    alert('查询结果', '你本月还未进行认证,请先前往认证', [
                        { text: '确认', onPress: () => { return; } },
                    ]);
                }
                this.setState({
                    year_data: data
                });
            });
    }
    yearChange = (value: any) => {
        // console.log(value.getFullYear());
        this.setState({
            year: value.getFullYear(),
            show_year: value
        });
        this.recordChange(value.getFullYear());
    }
    formatDate = (date: any) => {
        /* eslint no-confusing-arrow: 0 */
        const pad = (n: any) => n < 10 ? `0${n}` : n;
        const monthStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}`;
        const yearStr = `${date.getFullYear()}`;
        return `${monthStr} ${yearStr}`;
    }

    render() {
        setMainFormTitle("发放详情");
        const tabs = [
            { title: '发放记录' },
            { title: '详情' },
        ];
        const { data, showState, select_data, year_data } = this.state;
        let day: any = [];
        let one: any = {};
        let two: any = {};
        let three: any = {};
        let four: any = {};
        let five: any = {};
        let six: any = {};
        let seven: any = {};
        let eight: any = {};
        let night: any = {};
        let ten: any = {};
        let eleven: any = {};
        let twelve: any = {};
        year_data.map((item: any) => {
            const date = new Date(item.create_date);
            day.push(date.getMonth() + 1);
            if (date.getMonth() + 1 === 1) {
                one = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 2) {
                two = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 3) {
                three = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 4) {
                four = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 5) {
                five = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 6) {
                six = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 7) {
                seven = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 8) {
                eight = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 9) {
                night = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 10) {
                ten = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 11) {
                eleven = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
            if (date.getMonth() + 1 === 12) {
                twelve = { "state": item.apply_status === '认证成功' ? '本月已认证，暂未发放津贴' : '本月已发放津贴', "money": item.amount };
            }
        });
        return (
            <div>
                <Tabs tabs={tabs} initialPage={1} animated={false} useOnPan={false}>
                    <div style={{ fontSize: '30px' }}>
                        <div style={{ paddingLeft: '20px', width: '100%', paddingRight: '20px' }}>
                            <WhiteSpace size="lg" />
                            <h4>{this.state.year + '年'}发放记录</h4>
                            <WhiteSpace size="lg" />
                            <DatePicker
                                mode="year"
                                title="选择年份"
                                format={val => `${this.formatDate(val).split(' ')[1]}`}
                                onChange={this.yearChange}
                                value={this.state.show_year ? this.state.show_year : new Date()}
                                minDate={new Date(2000, 1, 1, 0, 0, 0)}
                                maxDate={new Date(2100, 1, 1, 0, 0, 0)}
                            >
                                <List.Item arrow="horizontal">选择年份</List.Item>
                            </DatePicker>
                            <WhiteSpace size="lg" />
                            <Collapse onChange={this.recordChange}>
                                <Panel header="一月" key="1">
                                    {day.indexOf(1) !== -1 ?
                                        <div>
                                            <p>{"状态：" + one.state}</p>
                                            <p>{"发放金额：" + one.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="二月" key="2">
                                    {day.indexOf(2) !== -1 ?
                                        <div>
                                            <p>{"状态：" + two.state}</p>
                                            <p>{"发放金额：" + two.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="三月" key="3">
                                    {day.indexOf(3) !== -1 ?
                                        <div>
                                            <p>{"状态：" + three.state}</p>
                                            <p>{"发放金额：" + three.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="四月" key="4">
                                    {day.indexOf(4) !== -1 ?
                                        <div>
                                            <p>{"状态：" + four.state}</p>
                                            <p>{"发放金额：" + four.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="五月" key="5">
                                    {day.indexOf(5) !== -1 ?
                                        <div>
                                            <p>{"状态：" + five.state}</p>
                                            <p>{"发放金额：" + five.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="六月" key="6">
                                    {day.indexOf(6) !== -1 ?
                                        <div>
                                            <p>{"状态：" + six.state}</p>
                                            <p>{"发放金额：" + six.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="七月" key="7">
                                    {day.indexOf(7) !== -1 ?
                                        <div>
                                            <p>{"状态：" + seven.state}</p>
                                            <p>{"发放金额：" + seven.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="八月" key="8">
                                    {day.indexOf(8) !== -1 ?
                                        <div>
                                            <p>{"状态：" + eight.state}</p>
                                            <p>{"发放金额：" + eight.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="九月" key="9">
                                    {day.indexOf(9) !== -1 ?
                                        <div>
                                            <p>{"状态：" + night.state}</p>
                                            <p>{"发放金额：" + night.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="十月" key="10">
                                    {day.indexOf(10) !== -1 ?
                                        <div>
                                            <p>{"状态：" + ten.state}</p>
                                            <p>{"发放金额：" + ten.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="十一月" key="11">
                                    {day.indexOf(11) !== -1 ?
                                        <div>
                                            <p>{"状态：" + eleven.state}</p>
                                            <p>{"发放金额：" + eleven.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                                <Panel header="十二月" key="12">
                                    {day.indexOf(12) !== -1 ?
                                        <div>
                                            <p>{"状态：" + twelve.state}</p>
                                            <p>{"发放金额：" + twelve.money}</p>
                                        </div>
                                        :
                                        <p>本月未认证</p>
                                    }
                                </Panel>
                            </Collapse>
                        </div>
                    </div>
                    <div>
                        <div style={{ paddingLeft: '20px', paddingRight: '20px' }}>
                            <WhiteSpace size="lg" />
                            <h2>当前状态</h2>
                            <Result
                                style={{ padding: '0px', margin: '0px' }}
                                img={myImg('https://gw.alipayobjects.com/zos/rmsportal/HWuSTipkjJRfTWekgTUG.svg')}
                                title={data.apply_status === '认证成功' ? '已认证未发放' : (data.apply_status === '已生成台账' ? '已认证已发放' : '未认证')}
                                message={data.apply_status === '认证成功' ? "等待发放" : (data.apply_status === '已生成台账' ? '已发放' : '未认证')}
                            />
                        </div>
                        <WhiteSpace size="lg" />
                        <hr style={{ margin: '20px' }} />
                        <WhiteSpace size="lg" />
                        <div style={{ paddingLeft: '20px', paddingRight: '20px' }}>
                            <h2>&nbsp;申请人基本情况</h2>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <List>
                                <Item extra={data.name}>姓名</Item>
                                <Item extra={data.id_card_num}>身份证号码</Item>
                                <Item extra={new Date().getFullYear() + '年' + this.state.month + '月'}>月份</Item>
                                <Item extra={data.amount ? data.amount + '元' : '还未发放'}>发放金额</Item>
                            </List>
                        </div>
                        <div style={{ paddingLeft: '20px', paddingRight: '20px' }}>
                            <List renderHeader={'查看历史的发放记录'}>
                                <DatePicker
                                    mode="month"
                                    title="选择时间"
                                    format={val => `${this.formatDate(val).split(' ')[0]}`}
                                    onChange={this.dateChange}
                                    value={this.state.time}
                                    minDate={new Date(2018, 1, 1, 0, 0, 0)}
                                    maxDate={new Date(2100, 1, 1, 0, 0, 0)}
                                >
                                    <List.Item arrow="horizontal">选择年份、月份</List.Item>
                                </DatePicker>
                            </List>
                            {showState === true ?
                                (
                                    select_data.apply_status === '已生成台账' ?
                                        <Item wrap={true} extra={'本月已发放津贴' + select_data.amount ? select_data.amount + '元' : '' + '元'}>查询结果：</Item>
                                        :
                                        <Item wrap={true} extra={'本月已认证，暂未发放津贴'}>查询结果：</Item>
                                )
                                :
                                <Item wrap={true} extra={"本月未认证"}>查询结果：</Item>
                            }
                        </div>
                    </div>
                </Tabs>
            </div >
        );
    }
}

/**
 * 控件：补贴申请状态视图控制器
 * 补贴申请状态
 */
@addon('ApplySuccessView', '补贴申请状态视图', '补贴申请状态')
@reactControl(OldApplyStateView, true)
export class OldApplyStateViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}