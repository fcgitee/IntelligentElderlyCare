import { Form, Spin } from "antd";
import { Button, InputItem, List, Toast, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { idCardCheck, nameCheck, setMainFormTitle, subscribeWebViewNotify } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { decodeUrlParam } from "src/projects/app/util-tool";
import { forceCheckIsLogin, ROUTE_PATH } from "src/projects/app/util-tool";
// import VConsole from 'vconsole';
/**
 * 组件：残疾人身份认证状态
 */
export interface DisabledPersonViewState extends ReactViewState {
    name_has_error: boolean;
    id_card_has_error: boolean;
    name: string;
    id_card: string;
    loading: boolean;
}
/**
 * 组件：残疾人身份认证
 * 描述
 */
export class DisabledPersonView extends ReactView<DisabledPersonViewControl, DisabledPersonViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id_card_has_error: true,
            name_has_error: true,
            name: '',
            id_card: '',
            loading: false
        };
    }

    componentDidMount() {
        // new VConsole();
        AppServiceUtility.personnel_service.get_user_session!()!
            .then((data: any) => {
                if (!data) {
                    forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.disabledPerson }, { backPath: ROUTE_PATH.disabledPerson });
                    return;
                } else {
                    // console.log
                    (this.props.match!.params.key);
                    if (this.props.match!.params.key !== undefined) {
                        let person_info = JSON.parse(decodeUrlParam(this.props.match!.params.key));
                        // let person_info = {
                        //     'name': '陈旭霖',
                        //     id_card: "441423199009108035"
                        // };
                        // // console.log(person_info);
                        if (person_info) {
                            this.setState({
                                name: person_info.name,
                                id_card: person_info.id_card,
                            });
                            this.onIdCardChange(person_info.id_card);
                            this.onNameChange(person_info.name);
                        }
                    }
                    const messageCb = (e: any) => {
                        console.log('认证结果1：' + e);
                        const retData = JSON.parse(e.data);
                        console.log(retData,'结果2');
                        if (retData.type === 'WiiAuthPage') {
                            if (retData.data.result === true) {
                                AppServiceUtility.allowance_service.apply_disabled_person_allowance!({
                                    name: this.state.name,
                                    id_card_num: this.state.id_card,
                                    status: 0
                                })!.then((e: any) => {
                                    Toast.info(e[0].result, 5);
                                });
                            } else {
                                Toast.fail('认证失败，请检查身份证与姓名是否正确或明确是否有认证资格！', 5);
                                AppServiceUtility.allowance_service.apply_disabled_person_allowance!({
                                    name: this.state.name,
                                    id_card_num: this.state.id_card,
                                    status: -1
                                });
                            }
                        }
                    };

                    subscribeWebViewNotify(messageCb);
                }
            });
    }

    onNameErrorClick = () => {
        if (this.state.name_has_error) {
            Toast.info('请输入正确的姓名');
        }
    }

    onIdCardErrorClick = () => {
        if (this.state.id_card_has_error) {
            Toast.info('请输入正确的身份证号码');
        }
    }

    onNameChange = (value: string) => {
        if (!nameCheck(value)) {
            this.setState({
                name_has_error: true,
            });
        } else {
            this.setState({
                name_has_error: false,
            });
        }
        this.setState({
            name: value,
        });
    }

    onIdCardChange = (value: string) => {
        if (!idCardCheck(value)) {
            this.setState({
                id_card_has_error: true,
            });
        } else {
            this.setState({
                id_card_has_error: false,
            });
        }
        this.setState({
            id_card: value,
        });
    }

    onSubmit = () => {
        const { id_card_has_error, name_has_error } = this.state;
        this.setState({
            loading: true
        });
        AppServiceUtility.allowance_service.get_disabled_person_list!({ name: this.state.name, id_card_num: this.state.id_card })!
            .then((data: any) => {
                this.setState({ loading: false });
                if (data.result.length > 0) {
                    if (!id_card_has_error && !name_has_error) {
                        if ((window as any).ReactNativeWebView) {
                            (window as any).ReactNativeWebView.postMessage(
                                JSON.stringify({
                                    function: 'WiiAuthPage',
                                    params: {
                                        name: this.state.name,
                                        idCard: this.state.id_card
                                    }
                                })
                            );
                        }
                    }
                } else {
                    Toast.fail('请核对长者信息是否正确，再点击验证');
                }
            });
    }

    render() {
        setMainFormTitle('残疾人认证');

        return (
            <Spin spinning={this.state.loading}>
                <div id={"older-benefits"}>
                    <List renderHeader={() => '残疾人身份认证'}>
                        <InputItem
                            type={'text'}
                            placeholder="请输入姓名"
                            error={this.state.name_has_error}
                            onErrorClick={this.onNameErrorClick}
                            onChange={this.onNameChange}
                            value={this.state.name}
                        >
                            姓名
                        </InputItem>
                        <InputItem
                            placeholder="请输入身份证"
                            error={this.state.id_card_has_error}
                            onErrorClick={this.onIdCardErrorClick}
                            onChange={this.onIdCardChange}
                            value={this.state.id_card}
                        >
                            身份证
                        </InputItem>
                    </List>
                    <WhiteSpace />
                    <Button type="primary" onClick={this.onSubmit}>验证</Button>
                </div>
            </Spin>
        );
    }
}

/**
 * 控件：残疾人身份认证控制器
 * 描述
 */
@addon('DisabledPersonView', '残疾人身份认证', '残疾人身份认证')
@reactControl(Form.create<any>()(DisabledPersonView), true)
export class DisabledPersonViewControl extends ReactViewControl {
}