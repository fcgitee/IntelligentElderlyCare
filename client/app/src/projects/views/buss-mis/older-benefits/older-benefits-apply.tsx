import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Modal, Avatar, Form, Button } from "antd";
import { InputItem, Toast } from 'antd-mobile';
import './index.less';

/**
 * 组件：高龄津贴申请控件状态
 */
export interface OlderBenefitsApplyState extends ReactViewState {
    state?: any;
    show?: any;
    person_list?: any;
    check_person?: any;
    check?: any;
    index?: any;
    input_show?: any;
    age?:any;
    date_birth?:any;
    sex?:any;
    id_card?:any;
    address?:any;
    phone?:any;
    allowance_money?:any;
}
/**
 * 组件：高龄津贴申请控件
 */
export class OlderBenefitsApply extends ReactView<OlderBenefitsApplyControl, OlderBenefitsApplyState> {
    constructor(props: any) {
        super(props);
        this.state = {
            state: true,
            show: false,
            person_list: [],
            check_person: {},
            check: false,
            index: 0,
            input_show: false,
            age: 0,
            date_birth: '',
            sex: '',
            id_card: '',
            address: '',
            phone: '',
            allowance_money: 0
        };
    }

    componentDidMount = () => {
        // 获取当前登陆人信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log('当前登陆人的信息》》》》》》》》》', data);
                // 获取当前登陆人的家属信息
                AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': data[0]['id'] })!
                    .then((data: any) => {
                        // console.log('所属亲属信息》》》》》', data);
                        // this.setState({
                        //     family_data: data['result']
                        // });
                        if (data.result.length > 0) {
                            let person_list: any = [];
                            data.result.map((item: any) => {
                                if (item.relationShip_type === '长者') {
                                    person_list.push(item.family_person[0]);
                                    this.setState({
                                        person_list
                                    });
                                }
                            });
                            this.setState({
                                show: true
                            });
                            if (person_list.length === 0) {
                                this.props.history!.push(ROUTE_PATH.familyFilesEditSelf + '/' + '长者&');
                            }
                        }
                    });
            });
    }

    handleOk = (e: any) => {
        this.setState({
            show: false,
            input_show: true
        });
    }

    handleCancel = (e: any) => {
        // console.log(e);
        this.setState({
            show: false,
        });
    }

    check = (index: any, info: any) => {
        // console.log(info);
        if (index === this.state.index && this.state.check === true) {
            this.setState({
                check: false,
                index
            });
        } else {
            this.setState({
                check: true,
                index
            });
        }
        // console.log(new Date().getFullYear() - Number(info.id_card.slice(6, 10)));
        if ((new Date().getFullYear() - Number(info.id_card.slice(6, 10))) >= 70 && (new Date().getFullYear() - Number(info.id_card.slice(6, 10))) <= 79) {
            this.setState({
                age: '70~79周岁',
                allowance_money: 30
            });
        }
        if ((new Date().getFullYear() - Number(info.id_card.slice(6, 10))) >= 80 && (new Date().getFullYear() - Number(info.id_card.slice(6, 10))) <= 89) {
            this.setState({
                age: '80~89周岁',
                allowance_money: 100
            });
        }
        if ((new Date().getFullYear() - Number(info.id_card.slice(6, 10))) >= 90 && (new Date().getFullYear() - Number(info.id_card.slice(6, 10))) <= 99) {
            this.setState({
                age: '90~99周岁',
                allowance_money: 150
            });
        }
        if ((new Date().getFullYear() - Number(info.id_card.slice(6, 10))) >= 100) {
            this.setState({
                age: '100周岁以上',
                allowance_money: 500
            });
        }
        this.setState({
            check_person: info,
            date_birth: info.id_card.slice(6, 14),
            sex: (parseInt(info.id_card.substr(16, 1),0) % 2 === 1) ? '男' : '女',
            id_card: info.id_card,
            address: info.personnel_info ? info.personnel_info.address : '',
            phone: info.personnel_info ? info.personnel_info.telephone : '',
        });
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            // console.log(values);
            AppServiceUtility.allowance_service.apply_old_allowance!(values)!
                    .then((data: any) => {
                        // console.log('增加成功了吗？？？？？？》》》》》', data);
                        if (data === '申请成功') {
                            Toast.success('申请成功!!!', 1);
                            // this.props.history!.push(ROUTE_PATH.familyFiles);
                        } else {
                            Toast.success('已申请，不可重复申请!!!', 1);
                        }
                    });
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className='older-apply'>
                {this.state.input_show ?
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item>
                            {getFieldDecorator('name', {
                                initialValue: this.state.check_person.name ? this.state.check_person.name : '',
                                rules: [{ required: true, message: '请输入你的姓名!' }],
                            })(
                                <InputItem value={this.state.check_person.name} clear={true} placeholder="请输入姓名">姓名</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('sex', {
                                initialValue: this.state.sex ? this.state.sex : '',
                                rules: [{ required: true, message: '请输入你的性别!' }],
                            })(
                                <InputItem editable={false} placeholder="根据身份证自动识别">性别</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('date_birth', {
                                initialValue: this.state.date_birth ? this.state.date_birth : '',
                                rules: [{ required: true, message: '请输入你的出生年月!' }],
                            })(
                                <InputItem editable={false} placeholder="根据身份证自动识别">出生年月</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('id_card', {
                                initialValue: this.state.id_card ? this.state.id_card : '',
                                rules: [{ required: true, message: '请输入你的身份证号!' }],
                            })(
                                <InputItem clear={true} placeholder="请输入身份证号">身份证号</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('address', {
                                initialValue: this.state.address ? this.state.address : '',
                                rules: [{ required: true, message: '请输入你的常住地址!' }],
                            })(
                                <InputItem clear={true} placeholder="请输入常住地址">常住地址</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('zhenjie', {
                                initialValue: '',
                                rules: [{ required: true, message: '请输入你的所属镇街!' }],
                            })(
                                <InputItem clear={true} placeholder="请输入所属镇街">所属镇街</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('shequ', {
                                initialValue: '',
                                rules: [{ required: true, message: '请输入你的所属社区!' }],
                            })(
                                <InputItem clear={true} placeholder="请输入所属社区">所属社区</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('telephone', {
                                initialValue: this.state.phone ? this.state.phone : '',
                                rules: [{ required: true, message: '请输入你的联系电话!' }],
                            })(
                                <InputItem clear={true} placeholder="请输入联系电话">联系电话</InputItem>
                            )}
                        </Form.Item>
                        <br/>
                        <Form.Item>
                            {getFieldDecorator('age', {
                                initialValue: this.state.age ? this.state.age : '',
                            })(
                                <InputItem editable={false} placeholder="根据身份证自动识别">年龄阶段</InputItem>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('allowance_money', {
                                initialValue: this.state.allowance_money ? this.state.allowance_money : '',
                            })(
                                <InputItem editable={false} placeholder="根据年龄段自动识别">补贴金额</InputItem>
                            )}
                        </Form.Item>
                        <Button type="primary" size='large' htmlType="submit" style={{ position: 'fixed', bottom: '0px', width: '100%' }}>提交申请</Button>
                    </Form>
                    :
                    ''
                }
                <Modal title="选择长者" visible={this.state.show} onOk={this.handleOk} onCancel={this.handleCancel}>
                    <div style={{ overflowX: 'scroll', whiteSpace: 'nowrap' }}>
                        <div style={{ display: 'flex' }}>
                            {this.state.person_list.map((item: any, index: any) => {
                                return (<div key={item} style={{ textAlign: 'center', float: 'left' }}>
                                    <div onClick={() => { this.check(index, item); }} style={{ border: this.state.index === index && this.state.check === true ? '1px solid green' : '0px' }}>
                                        <Avatar size={64} src={item.personnel_info ? item.personnel_info.picture : "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"} style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                                        <p style={{ marginBottom: '0px' }}>{item.name}</p>
                                    </div>
                                </div>);
                            })}
                        </div>
                    </div>
                </Modal>
            </div >
        );
    }
}

/**
 * 组件：高龄津贴申请控件
 * 控制高龄津贴申请控件
 */
@addon('OlderBenefitsApply', '高龄津贴申请控件', '控制高龄津贴申请控件')
@reactControl(Form.create<any>()(OlderBenefitsApply), true)
export class OlderBenefitsApplyControl extends ReactViewControl {
}