import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { SearchBar, Card, Toast } from 'antd-mobile';
import { Icon } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, encodeUrlParam } from "src/projects/app/util-tool";
import { LoadingEffectView } from 'src/business/views/loading-effect';
/**
 * 组件：长者身份认证状态
 */
export interface UncertifiedListState extends ReactViewState {
    data1?: any;
    data2?: any;
    insert?: any;
    isLoad?: boolean;
    isLoadOther?: boolean;
}
/**
 * 组件：长者身份认证
 * 描述
 */
export class UncertifiedList extends ReactView<UncertifiedListControl, UncertifiedListState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data1: [],
            data2: [],
            insert: '',
            isLoad: true,
            isLoadOther: true,
        };
    }

    componentDidMount = () => {
        // 判断是否登陆
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log(data);
                if (data.length === 0) {
                    Toast.info('你还未登录，请先前往登录!!!', 2, () => {
                        this.props.history!.push(ROUTE_PATH.login);
                    });
                }
            });
        AppServiceUtility.allowance_service.get_uncertified_person_list!({ "time": "61-90" }, 1, 3)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    data1: data.result,
                    isLoadOther: false,
                });
            });
        AppServiceUtility.allowance_service.get_uncertified_person_list!({ "time": ">90" }, 1, 3)!
            .then((data: any) => {
                this.setState({
                    data2: data.result,
                    isLoad: false
                });
            });
    }
    goDetail = (param: any) => {
        this.props.history!.push(ROUTE_PATH.uncertifiedListDetail + '/' + param + '&');
    }
    goRZ = (name: any, id_card: any) => {
        this.props.history!.push(ROUTE_PATH.olderBenefits + '/' + encodeUrlParam(JSON.stringify({ 'name': name, 'id_card': id_card })));
    }

    insert = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }

    search = () => {
        this.props.history!.push(ROUTE_PATH.uncertifiedListDetail + '/' + 'all' + '&' + this.state.insert);
    }
    render() {
        return (
            <div>
                <SearchBar placeholder="可根据姓名、身份证搜索" onChange={this.insert} onSubmit={this.search} />
                <Card style={{ marginBottom: '20px' }}>
                    <div onClick={() => { this.goDetail('>90'); }}>
                        <Card.Header
                            title={'90天以上未认证'}
                            extra={<Icon type='right' />}
                        />
                    </div>
                    {this.state.isLoad && <LoadingEffectView />}
                    {this.state.data2.length > 0 ? this.state.data2.map((item: any) => {
                        return <Card.Body key={item} onClick={() => { this.goRZ(item.name, item.id_card_num); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div>
                                    <span style={{ marginRight: '20px' }}>{item.name}</span>
                                    <span style={{ marginRight: '20px' }}>{item.sex}</span>
                                    <span style={{ marginRight: '20px' }}>{new Date().getFullYear() - Number(item.id_card_num.substring(6, 10)) + '岁'}</span>
                                    <span>{item.id_card_num}</span>
                                </div>
                                <div>{item.town_street + '-' + item.village}</div>
                            </div>
                        </Card.Body>;
                    })
                        :
                        <div style={{ textAlign: 'center' }}>暂无相关数据</div>}
                </Card>
                <br />
                <Card style={{ marginBottom: '20px' }}>
                    <div onClick={() => { this.goDetail('61-90'); }}>
                        <Card.Header
                            title={'61-90天之间认证过的'}
                            extra={<Icon type='right' />}
                        />
                    </div>
                    {this.state.isLoadOther && <LoadingEffectView />}
                    {this.state.data1.length > 0 ? this.state.data1.map((item: any) => {
                        return <Card.Body key={item} onClick={() => { this.goRZ(item.name, item.id_card_num); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div>
                                    <span style={{ marginRight: '20px' }}>{item.name}</span>
                                    <span style={{ marginRight: '20px' }}>{item.sex}</span>
                                    <span style={{ marginRight: '20px' }}>{new Date().getFullYear() - Number(item.id_card_num.substring(6, 10)) + '岁'}</span>
                                    <span>{item.id_card_num}</span>
                                </div>
                                <div>{item.town_street + '-' + item.village}</div>
                            </div>
                        </Card.Body>;
                    })
                        :
                        <div style={{ textAlign: 'center' }}>暂无相关数据</div>}
                </Card>
            </div>
        );
    }
}

/**
 * 控件：长者身份认证控制器
 * 描述
 */
@addon('UncertifiedList', '长者身份认证', '长者身份认证')
@reactControl(UncertifiedList, true)
export class UncertifiedListControl extends ReactViewControl {
}