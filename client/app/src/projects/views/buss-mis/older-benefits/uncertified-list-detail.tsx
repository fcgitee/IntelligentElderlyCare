import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { SearchBar, Card, ListView } from 'antd-mobile';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, encodeUrlParam } from "src/projects/app/util-tool";
import { Row, Spin } from "antd";

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：高龄津贴未认证列表状态
 */
export interface UncertifiedListDetailState extends ReactViewState {
    data1?: any;
    insert?: any;
    dataSource?: any;
    animating?: any;
    page?: any;
}
/**
 * 组件：高龄津贴未认证列表
 * 描述
 */
export class UncertifiedListDetail extends ReactView<UncertifiedListDetailControl, UncertifiedListDetailState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            data1: [],
            insert: '',
            dataSource: ds,
            animating: false,
            page: 1
        };
    }

    componentDidMount = () => {
        this.setState({
            animating: true
        });
        let param = {};
        if (this.props.match!.params.key.split('&')[0] === 'all') {
            param = { "name": this.props.match!.params.key.split('&')[1] };
        } else {
            param = { "time": this.props.match!.params.key.split('&')[0] };
        }
        AppServiceUtility.allowance_service.get_uncertified_person_list!(param, 1, 15)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    data1: data.result,
                    animating: false
                });
            });
    }

    goRZ = (name: any, id_card: any) => {
        this.props.history!.push(ROUTE_PATH.olderBenefits + '/' + encodeUrlParam(JSON.stringify({ 'name': name, 'id_card': id_card })));
    }

    insert = (value: any) => {
        // console.log(value);
        this.setState({
            insert: value
        });
    }

    search = () => {
        this.setState({
            animating: true
        });
        let param = {};
        if (this.props.match!.params.key.split('&')[0] === 'all') {
            param = { "name": this.props.match!.params.key.split('&')[1] };
        } else {
            param = { "time": this.props.match!.params.key.split('&')[0] };
        }
        if (this.state.insert !== '') {
            param['name'] = this.state.insert;
        }
        AppServiceUtility.allowance_service.get_uncertified_person_list!(param, 1, 1)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    data1: data.result,
                    animating: false
                });
            });
    }
    onEndReached = () => {
        let page = this.state.page;
        let data1 = this.state.data1;
        page = page + 1;
        this.setState({
            page
        });
        let param = {};
        if (this.props.match!.params.key.split('&')[0] === 'all') {
            param = { "name": this.props.match!.params.key.split('&')[1] };
        } else {
            param = { "time": this.props.match!.params.key.split('&')[0] };
        }
        if (this.state.insert !== '') {
            param['name'] = this.state.insert;
        }
        AppServiceUtility.allowance_service.get_uncertified_person_list!(param, page, 10)!
            .then((data: any) => {
                // console.log(data);
                data1 = [...data1,...data.result];
                this.setState({
                    data1,
                    animating: false
                });
            });
    }
    render() {
        const { data1, dataSource } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <Card.Body key={owData} onClick={() => { this.goRZ(owData.name, owData.id_card_num); }}>
                    <div style={{ overflow: 'hidden' }}>
                        <div>
                            <span style={{ marginRight: '20px', marginLeft: '20px' }}>{owData.name}</span>
                            <span style={{ marginRight: '20px' }}>{owData.sex}</span>
                            <span style={{ marginRight: '20px' }}>{new Date().getFullYear() - Number(owData.id_card_num.substring(6, 10)) + '岁'}</span>
                            <span>{owData.id_card_num}</span>
                        </div>
                        <div style={{ marginLeft: '20px' }}>{owData.town_street + '-' + owData.village}</div>
                    </div>
                </Card.Body>
            );
        };
        return (
            <div>
                <SearchBar placeholder="可根据姓名、身份证搜索" onChange={this.insert} onSubmit={this.search} />
                <Row className='tabs-content'>
                    {
                        data1 && data1.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(data1)}
                                renderRow={renderRow}
                                initialListSize={15}
                                pageSize={15}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: document.documentElement!.clientHeight }}
                                onEndReached={this.onEndReached}
                            />
                            :
                            (data1.length === 0 && this.state.animating === true ?
                                <div style={{ textAlign: 'center', margin: '20px' }}>
                                    <Spin size="large" />
                                </div>
                                :
                                '')

                    }
                    {data1.length === 0 && this.state.animating === false ?
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                            </Row>
                        :
                        ''
                    }
                </Row>

            </div >
        );
    }
}

/**
 * 控件：高龄津贴未认证列表控制器
 * 描述
 */
@addon('UncertifiedListDetail', '高龄津贴未认证列表', '高龄津贴未认证列表')
@reactControl(UncertifiedListDetail, true)
export class UncertifiedListDetailControl extends ReactViewControl {
}