import { Avatar, Modal } from "antd";
import { Button, Checkbox, Flex } from 'antd-mobile';
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, CookieUtil, reactControl } from "pao-aop-client";
import React from "react";
import { encodeUrlParam, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { forceCheckIsLogin, ROUTE_PATH } from "src/projects/app/util-tool";

const AgreeItem = Checkbox.AgreeItem;
/**
 * 组件：高龄补贴条款视图状态
 */
export interface ApplyReadingClauseViewState extends BaseReactElementState {
    state?: any;
    show?: any;
    person_list?: any;
    check_person?: any;
    check?: any;
    index?: any;
    person_data?: any;
}

/**
 * 组件：高龄补贴条款视图
 * 高龄补贴条款
 */
export class ApplyReadingClauseView extends BaseReactElement<ApplyReadingClauseViewControl, ApplyReadingClauseViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            state: true,
            show: false,
            person_list: [],
            check_person: {},
            check: false,
            index: 0,
            person_data: []
        };
    }
    componentDidMount = () => {
        CookieUtil.remove("user_info");
    }
    clickChange = (e: any) => {
        // // console.log(e.target.value);
        let state = this.state.state;
        this.setState({
            state: !state
        });
    }
    // self = () => {
    //     AppServiceUtility.allowance_service.get_certToken!({ "url": "http://192.168.95.140:3000/app/old-apply-state" })!
    //         .then((data: any) => {
    //             // console.log(data);
    //             window.location.href = "https://rz.weijing.gov.cn/authgzh/auth?certToken=" + data['tokenInfo']['certToken'];
    //         });
    // }
    // other = () => {
    //     this.props.history!.push(ROUTE_PATH.applyInfoInsert);
    // }
    goInsert = () => {
        // alert('请选择认证方式', <div />, [
        //     { text: '本人', onPress: () => this.self() },
        //     { text: '他人协助', onPress: () => this.other() },
        //     { text: '返回', onPress: () => history.back() },
        // ]);
        // this.props.history!.push(ROUTE_PATH.olderBenefits);
        // 获取当前登陆人信息
        AppServiceUtility.personnel_service.get_user_session!()!
            .then((data: any) => {
                if (!data) {
                    forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.applyReadinClause }, { backPath: ROUTE_PATH.applyReadinClause });
                    return;
                } else {
                    let person_list: any = [];
                    AppServiceUtility.person_org_manage_service.get_current_user_info!()!
                        .then((data: any) => {
                            person_list.push(data[0]);
                            // console.log('当前登陆人的信息》》》》》》》》》', data);
                            this.setState({
                                person_data: data[0]
                            });
                            // 获取当前登陆人的家属信息
                            AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': data[0]['id'] })!
                                .then((data: any) => {
                                    // console.log('所属亲属信息》》》》》', data);
                                    // this.setState({
                                    //     family_data: data['result']
                                    // });
                                    if (data.result.length > 0) {
                                        data.result.map((item: any) => {
                                            if (item.relationShip_type === '长者') {
                                                person_list.push(item.family_person[0]);
                                                this.setState({
                                                    person_list
                                                });
                                            }
                                        });
                                        this.setState({
                                            show: true
                                        });
                                    } else {
                                        this.props.history!.push(ROUTE_PATH.olderBenefits);
                                    }
                                });
                        });
                }
                // return;
            });
    }
    handleOk = (e: any) => {
        // console.log(e);
        // console.log(this.state.check_person);
        // console.log(JSON.stringify(this.state.check_person) === '{}');
        if (JSON.stringify(this.state.check_person) === '{}') {
            this.props.history!.push(ROUTE_PATH.olderBenefits);
        } else {
            AppServiceUtility.allowance_service.get_old_age_allowance_status!({ rz_status: true, id_card_num: this.state.check_person.id_card })!
                .then((data: any) => {
                    // console.log(data);
                    if (data === '未认证') {
                        this.props.history!.push(ROUTE_PATH.olderBenefits + '/' + encodeUrlParam(JSON.stringify(this.state.check_person)));
                        this.setState({
                            show: false,
                        });
                    } else if (data === '该长者不在名单表中') {
                        this.props.history!.push(ROUTE_PATH.olderBenefits + '/' + encodeUrlParam(JSON.stringify(this.state.check_person)));
                        this.setState({
                            show: false,
                        });
                    } else if (data === '60天内已认证') {
                        this.props.history!.push(ROUTE_PATH.oldapplyState + '/' + this.state.check_person.id_card);
                    }
                });
        }
    }

    handleCancel = (e: any) => {
        // console.log(e);
        this.setState({
            show: false,
        });
    }

    check = (index: any, info: any) => {
        // console.log(info);
        // console.log(index);
        if (index === this.state.index && this.state.check === true) {
            this.setState({
                check: false,
                index,
                check_person: {}
            });
        } else {
            this.setState({
                check: true,
                index,
                check_person: info
            });
        }
    }
    render() {
        const { state } = this.state;
        setMainFormTitle("高龄津贴发放对象及标准");
        return (
            <div>
                <div style={{ width: '96%', backgroundColor: '#fffffff2', margin: '2% 2% 2% 2%', borderRadius: '4px', padding: '7px' }}>
                    <h2>高龄津贴发放对象及标准</h2>
                    <div>一、定义</div>
                    <div>高龄津贴是指我区向年满70周岁（含）以上老人发放的生活照料津贴。</div>
                    <br />
                    <div>二、发放对象</div>
                    <div>具有佛山市南海区户籍、年满70周岁的老人。</div>
                    <br />
                    <div>三、发放标准</div>
                    <div>高龄津贴发放按年龄阶段划分为4个档次：</div>
                    <div>&nbsp;&nbsp;（一）70-79周岁每人每月发放30元；</div>
                    <div>&nbsp;&nbsp;（二）80-89周岁每人每月发放100元；</div>
                    <div>&nbsp;&nbsp;（三）90-99周岁每人每月发放150元；</div>
                    <div>&nbsp;&nbsp;（四）100周岁以上每人每月发放500元。</div>
                    <br />
                    <div>[1]根据《佛山市南海区人民政府关于调整高龄津贴发放标准和范围的批复》（南府复〔2013〕543号）</div>
                </div>
                <Flex>
                    <Flex.Item>
                        <AgreeItem data-seed="logId" onChange={this.clickChange}>
                            我已认真阅读上述内容
                            </AgreeItem>
                    </Flex.Item>
                </Flex>
                <Button type="primary" disabled={state} style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.goInsert}>高龄津贴认证</Button>
                <Modal title="请选择长者" visible={this.state.show} okText={'确定'} cancelText={'取消'} onOk={this.handleOk} onCancel={this.handleCancel}>
                    <div style={{ overflowX: 'scroll', whiteSpace: 'nowrap' }}>
                        <div style={{ display: 'flex' }}>
                            {this.state.person_list.map((item: any, index: any) => {
                                return (<div key={item} style={{ textAlign: 'center', float: 'left' }}>
                                    <div onClick={() => { this.check(index, item); }} style={{ border: this.state.index === index && this.state.check === true ? '1px solid green' : '0px' }}>
                                        <Avatar size={64} src={item.personnel_info ? item.personnel_info.picture : "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"} style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                                        <p style={{ marginBottom: '0px' }}>{item.name}</p>
                                    </div>
                                </div>);
                            })}
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：高龄补贴条款视图控制器
 * 高龄补贴条款
 */
@addon('ApplyReadingClauseView', '高龄补贴条款视图', '高龄补贴条款')
@reactControl(ApplyReadingClauseView, true)
export class ApplyReadingClauseViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}