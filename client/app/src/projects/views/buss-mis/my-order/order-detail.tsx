import { Button, Col, Row, Spin } from "antd";
import { List, WhiteSpace } from "antd-mobile";
import { Brief } from "antd-mobile/lib/list/ListItem";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import "./oreder-detal.less";

const Item = List.Item;
// TODO:图片写死
const url = 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg';

/**
 * 组件：订单详情控件状态
 */
export interface OrderDetailViewState extends ReactViewState {
    service_order_info: any;
    loading: boolean;
}
/**
 * 组件：订单详情控件
 */
export class OrderDetailView extends ReactView<OrderDetailViewControl, OrderDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            service_order_info: {},
            loading: true
        };
    }

    componentDidMount() {
        console.info(this.props.match!.params.key);
        if (this.props.match!.params.key) {
            AppServiceUtility.my_order_service.my_order_list!({ id: this.props.match!.params.key, 'no_user': true })!
                .then((data) => {
                    console.log('记录', data);
                    if (data.result!.length > 0) {
                        this.setState({
                            service_order_info: data.result![0]

                        });
                    }
                    this.setState({
                        loading: false
                    });
                })
                .catch((err) => {
                    console.info(err);
                });
        }
    }

    repay = (id: string) => {
        this.props.history!.push(ROUTE_PATH.repayment + '/' + id);
    }

    getOperation = (owData: any) => {
        if (owData.pay_status === '未付款') {
            return (
                <Row gutter={20}>
                    <WhiteSpace size='md' />
                    <Col style={{ textAlign: 'right' }}>
                        <Button style={{ marginRight: '1em' }}>取消订单</Button>
                        <Button type='primary' onClick={() => this.repay(owData.id)}>付款</Button>
                    </Col>
                </Row>
            );
        }

        if (owData.status === '服务中') {
            return (
                <Row gutter={20}>
                    <WhiteSpace size='md' />
                    <Col style={{ textAlign: 'right' }}>
                        <Button>联系服务人员</Button>
                    </Col>
                </Row>
            );
        }

        if (owData.status === '未服务') {
            return (
                <Row gutter={20}>
                    <WhiteSpace size='md' />
                    <Col style={{ textAlign: 'right' }}>
                        {/* TODO：未根据长者或者服务人员来显示：长者--待派工  工作人员--开始执行 */}
                        <Button>派工</Button>
                    </Col>
                </Row>
            );
        }

        return (
            <Row gutter={20}>
                <WhiteSpace size='md' />
                <Col style={{ textAlign: 'right' }}>
                    <Button type='primary' >评价</Button>
                </Col>
            </Row>
        );
    }

    /** 获取单个订单渲染元素 */
    getOrderEle(serviceOrder: any) {
        console.info(serviceOrder);
        let ele: JSX.Element[];

        ele = [(
            <List key={1}>
                <div className={"list-item-block"}>
                    <Item
                        extra={serviceOrder['pay_status'] ? serviceOrder['pay_status'] : ''}
                    >
                        {serviceOrder.orgnization_name ? serviceOrder.orgnization_name : ""}
                    </Item>
                    {
                        serviceOrder.detail ? serviceOrder.detail.map((value: any, key: number) => {
                            return (
                                <div key={key}>
                                    <Item
                                        thumb={serviceOrder.product_img && serviceOrder.product_img[key] && serviceOrder.product_img[key][0] ? serviceOrder.product_img[key][0] : url}
                                        multipleLine={true}
                                    >
                                        {
                                            <p>
                                                {value.product_name}
                                            </p>
                                        }
                                        {/* {
                                            value.service_option!.map((option: any, index: number) => {
                                                return (
                                                    <p key={index}>
                                                        {option.name}
                                                    </p>
                                                );
                                            })
                                        } */}
                                        {/* <Brief>剩余服务次数：{value.count * value.all_times}</Brief> */}
                                        <Brief>

                                            {
                                                (() => {
                                                    let price = 0;
                                                    serviceOrder.detail.map((e: any, i: number) => {
                                                        for (let index = 0; index < serviceOrder.price_info["id"].length; index++) {
                                                            const element = serviceOrder.price_info["id"][index];
                                                            if (e.product_id === element) {
                                                                price = serviceOrder.price_info["price"][index];
                                                            }
                                                        }
                                                    });
                                                    price *= value.count;
                                                    return isNaN(price) ? '' : `￥${price}`;
                                                })()
                                            }
                                        </Brief>
                                        <Brief>×{value.count}</Brief>
                                    </Item>
                                    {/* <Item
                                        extra={"李护工"}
                                    >
                                        服务人员
                                    </Item> */}
                                    {/* <Item
                                        extra={serviceOrder.service_start_date[key].format("YYYY-MM-DD hh:mm")}
                                    >
                                        服务开始时间
                                    </Item>
                                    <Item
                                        extra={serviceOrder.service_end_date[key].format("YYYY-MM-DD hh:mm")}
                                    >
                                        服务结束时间
                                    </Item> */}
                                    <WhiteSpace />
                                </div>
                            );
                        }) : ''
                    }
                </div>
            </List>
        ),
        // (
        //     this.getOperation(serviceOrder)
        // ),
        <WhiteSpace key={2} />,
        (
            <List key={3}>
                <div className={"list-item-block"}>
                    <Item
                        extra={serviceOrder.order_code}
                    >
                        {"订单编号"}
                    </Item>
                    <Item
                        extra={serviceOrder.order_date ? serviceOrder.order_date.format("YYYY-MM-DD hh:mm") : ''}
                    >
                        创建时间
                    </Item>
                    <Item
                        extra={(() => {
                            console.warn(serviceOrder);

                            if (serviceOrder.detail && serviceOrder.detail.length > 0) {
                                return serviceOrder.detail[0].count;
                            } else {
                                return 0;
                            }
                        })()}
                    >
                        购买数量
                    </Item>
                    <Item
                        extra={serviceOrder.amout}
                    >
                        订单总价
                    </Item>
                    <Item
                        extra={serviceOrder.orgnization_name ? serviceOrder.orgnization_name : ''}
                    >
                        服务机构
                    </Item>
                    <Item
                        extra={serviceOrder.origin_product ? serviceOrder.origin_product.product_name : ''}
                    >
                        服务项目
                    </Item>
                </div>
            </List>),
        <WhiteSpace key={4} />
        ];

        return ele!;
    }

    render() {
        setMainFormTitle("订单详情");
        return (
            <div
                className={"order-detail"}
            >
                <Spin spinning={this.state.loading}>
                    {JSON.stringify(this.state.service_order_info) !== '{}' && this.state.loading === false ? this.getOrderEle(this.state.service_order_info) : '暂无明细'}
                </Spin>
            </div>
        );
    }
}

/**
 * 组件：订单详情控件
 * 控制订单详情控件
 */
@addon('OrderDetailView', '订单详情控件', '控制订单详情控件')
@reactControl(OrderDetailView, true)
export class OrderDetailViewControl extends ReactViewControl {
}