import { Col, Modal, Row } from "antd";
import { Button, List, Radio, TextareaItem, WhiteSpace } from "antd-mobile";
import { Brief } from "antd-mobile/lib/list/ListItem";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import "./index.less";

const RadioItem = Radio.RadioItem;
const Item = List.Item;
// TODO:图片写死
const url = 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg';

/**
 * 组件：申请退单视图状态
 */
export interface ChargeBackViewState extends ReactViewState {
    service_order_info: any;
    charge_back_reason: Array<Object>;
    modal_open: boolean;
    select_reason: string;
    select_reason_to_show: string;
    slelect_reasion_key: number;
}
/**
 * 组件：申请退单视图
 */
export class ChargeBackView extends ReactView<ChargeBackViewControl, ChargeBackViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            service_order_info: {},
            charge_back_reason: [
                { key: "下错订单", val: 1 },
                { key: "信息错误", val: 2 },
                { key: "想换服务", val: 3 },
                { key: "不想要了", val: 4 },
            ],
            modal_open: false,
            select_reason: '',
            slelect_reasion_key: 1,
            select_reason_to_show: ''
        };
    }

    componentDidMount() {
        console.info(this.props.match!.params.key);
        AppServiceUtility.my_order_service.my_order_list!({ id: this.props.match!.params.key })!
            .then((data) => {
                console.info(data);
                this.setState({
                    service_order_info: data.result![0]
                });
            })
            .catch((err) => {
                console.info(err);
            });
    }

    onRadioChcek = (val: string, index: number) => {
        this.setState({ select_reason: val, slelect_reasion_key: index });
    }

    /** 获取单个订单渲染元素 */
    getOrderEle(serviceOrder: any) {
        console.info(serviceOrder);
        let ele: JSX.Element[];

        ele = [(
            <List key={1}>
                <div className={"list-item-block"}>
                    <Item
                        extra={serviceOrder.pay_status}
                    >
                        {serviceOrder.orgnization_name ? serviceOrder.orgnization_name[0] : ""}
                    </Item>
                    {
                        this.state.service_order_info.detail ? this.state.service_order_info.detail.map((value: any, key: number) => {
                            return (
                                <div key={key}>
                                    <Item
                                        thumb={url}
                                        multipleLine={true}
                                    >
                                        {
                                            value.service_option!.map((option: any, index: number) => {
                                                return (
                                                    <p key={index}>
                                                        {option.name + ':' + option.default_value}
                                                    </p>
                                                );
                                            })
                                        }

                                        <Brief>￥{value.amount}</Brief>
                                        <Brief>×1</Brief>
                                    </Item>
                                    {/* <Item
                                        extra={"李护工"}
                                    >
                                        服务人员
                                    </Item> */}
                                    {/* <Item
                                        extra={serviceOrder.service_start_date[key].format("YYYY-MM-DD hh:mm")}
                                    >
                                        服务开始时间
                                    </Item>
                                    <Item
                                        extra={serviceOrder.service_end_date[key].format("YYYY-MM-DD hh:mm")}
                                    >
                                        服务结束时间
                                    </Item> */}
                                    <WhiteSpace />
                                </div>
                            );
                        }) : ''
                    }
                </div>
            </List>
        ),
        <WhiteSpace key={2} />,
        (
            <List key={3}>
                <div className={"list-item-block"}>
                    <Item
                        onClick={() => this.setState({ modal_open: true })}
                        extra={<span > {this.state.select_reason_to_show === '' ? '请选择 >' : this.state.select_reason_to_show}</span>}
                    >
                        退款原因
                    </Item>
                    <Item
                        // TODO:待获取到订单总价再决定
                        extra={serviceOrder.pay_date ? serviceOrder.order_date.format("YYYY-MM-DD hh:mm") : ''}
                    >
                        退款金额
                    </Item>
                    <TextareaItem
                        title="退款说明"
                        autoHeight={true}
                        placeholder={"请填写退款说明"}
                        labelNumber={5}
                    />
                </div>
            </List>),
        <WhiteSpace key={4} />
        ];

        return ele!;
    }

    render() {
        setMainFormTitle("申请退单");
        return (
            <div
                className={"order-detail"}
            >
                {this.getOrderEle(this.state.service_order_info)}
                <div className="submit-btn">
                    <Row>
                        <Col span={24}>
                            <Button className={"btn-amount"}   >提交</Button>
                        </Col>
                    </Row>
                </div>
                <Modal
                    // popup={true}
                    visible={this.state.modal_open}
                    // onClose={this.onClose('modal2')}
                    // afterClose={() => { alert('afterClose'); }}
                    closable={false}
                    // footer={[{ text: 'Ok', onPress: () => { // console.log('ok'); } }]}
                    okText={"确定"}
                    title={"用户协议"}
                    onOk={() => this.setState({ modal_open: false, select_reason_to_show: this.state.select_reason })}
                    onCancel={() => {
                        this.setState({ modal_open: false });
                        this.setState({ select_reason: '', slelect_reasion_key: -1 });
                    }}
                    cancelText={"关闭"}

                // footer={[{ text: '确定', onPress: () => { // console.log('ok'); } }, { text: '取消', onPress: () => { // console.log('ok'); } }]}
                >
                    {
                        this.state.charge_back_reason.map((e: any, i: number) => {
                            return (<RadioItem key={e.val} onClick={() => this.onRadioChcek(e.key, e.val)} checked={this.state.slelect_reasion_key === e.val} defaultChecked={e.val === this.state.charge_back_reason[0]['val'] ? true : false} >{e.key}</RadioItem>);
                        })
                    }
                </Modal>
            </div>
        );
    }
}

/**
 * 组件：申请退单视图
 * 控制申请退单视图
 */
@addon('ChargeBackView', '申请退单视图', '控制申请退单视图')
@reactControl(ChargeBackView, true)
export class ChargeBackViewControl extends ReactViewControl {
}