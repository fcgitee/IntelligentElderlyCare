import { Col, Row, Spin } from "antd";
import { Card, ListView, Tabs, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { getGoodLookTimeShow } from "./index";
import './index.less';
const tabs = [
    { title: '已报名' },
    { title: '已完成' },
];
function MyBody(props: any) {
    return (
        <Row className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </Row>
    );
}
/**
 * 组件：活动列表视图控件状态
 */
export interface MyActivityListViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    /** 记录tab状态 */
    tab_status?: string;

    empty?: any;
    animating?: any;
    pageCount?: number;
}
/**
 * 组件：活动列表视图控件
 */
export class MyActivityListView extends ReactView<MyActivityListViewControl, MyActivityListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'participate',
            empty: false,
            animating: true,
            pageCount: 10,
        };
    }
    componentDidMount() {
        this.getActivityList(this.state.tab_status);
    }
    /** 明细点击事件 */
    on_click_detail = (id: string) => {
        this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id + '/' + this.state.tab_status);
    }
    /** tab切换触发事件 */
    tab_click = (tab: any, index: number) => {
        let tab_status = 'participate';
        switch (index) {
            case 0:
                tab_status = 'participate';
                break;
            case 1:
                tab_status = 'complete';
                break;
            default:
                tab_status = 'participate';
                break;
        }
        this.setState(
            {
                tab_status: tab_status,
                page: 1,
                list: [],
            },
            () => {
                this.getActivityList(tab_status);
            }
        );
    }
    getActivityList(tab_status: any, page: any = 1) {
        this.setState(
            {
                animating: true,
                empty: false,
            },
            () => {
                request(this, AppServiceUtility.activity_service.get_activity_participate_list!({ 'status': tab_status, 'user_id': '' }, page, this.state.pageCount))
                    .then((data: any) => {
                        if (data && data.result.length > 0) {
                            this.setState({
                                list: this.state.list!.concat(data.result),
                                page,
                                animating: false,
                            });
                        } else {
                            this.setState({
                                animating: false,
                                empty: true,
                            });
                        }
                    }).catch((error: Error) => {
                        console.info(error);
                    });
            }
        );
    }
    /** 下拉事件 */
    onEndReached = () => {
        let new_pape = this.state.page + 1;
        this.getActivityList({ 'status': this.state.tab_status, 'user_id': '' }, new_pape);
    }
    render() {
        const { list, dataSource, animating, empty } = this.state;
        setMainFormTitle('我的活动');
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <Card className='list-conten' onClick={() => this.on_click_detail(owData.activity_id)}>
                    <Row type='flex' justify='center'>
                        <Col className='list-col' span={10} style={{ marginRight: '5pt' }}><img className="activity-img" src={owData.photo && owData.photo[0] ? owData.photo[0] : 'https://www.e-health100.com/api/attachment/activityphoto/8336'} /></Col>
                        <Col span={13} className='list-col'>
                            <Row>
                                <Col span={17}><strong>{owData.activity_name}</strong></Col>
                            </Row>
                            <Row className='list-row'>{owData.organization_name}</Row>
                            <Row>{getGoodLookTimeShow(owData.begin_date, owData.end_date)}</Row>
                            <Row>
                                <Col span={6} className="font-orange">{owData.amount === 0 || owData.amount === '0' ? '免费' : `￥${owData['amount'] || 0}`}</Col>
                                <Col span={18}>已报名：{owData.participate_count || 0}/{owData.max_quantity}</Col>
                            </Row>
                        </Col>
                    </Row>
                </Card>
            );
        };
        return (
            <Row>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                    onTabClick={(tab, index) => this.tab_click(tab, index)}
                />
                <Row className='tabs-content'>
                    {
                        list && list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(list)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: this.state.height }}
                                onEndReached={this.onEndReached}
                            /> : null
                    }
                    {animating ? <Row>
                        <WhiteSpace size="lg" />
                        <Row style={{ textAlign: 'center' }}>
                            <Spin size="large" />
                        </Row>
                        <WhiteSpace size="lg" />
                    </Row> : null}
                    {empty ? <Row>
                        <WhiteSpace size="lg" />
                        <Row className='tabs-content' type='flex' justify='center'>{list && list.length ? '已经是最后一条了' : '无数据'}</Row>
                        <WhiteSpace size="lg" />
                    </Row> : null}
                </Row>
            </Row>
        );
    }
}

/**
 * 组件：活动列表视图控件
 * 控制活动列表视图控件
 */
@addon('MyActivityListView', '活动列表视图控件', '控制活动列表视图控件')
@reactControl(MyActivityListView, true)
export class MyActivityListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}