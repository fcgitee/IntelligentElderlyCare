import { Col, Row } from "antd";
import { Button } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：活动报名结果页面状态
 */
export interface ActivityParticipateViewState extends ReactViewState {
}

/**
 * 组件：活动报名结果页面
 * 描述
 */
export class ActivityParticipateView extends ReactView<ActivityParticipateViewControl, ActivityParticipateViewState> {
    componentDidMount() {
    }
    /** 完成按钮事件 */
    btn() {
        // history.back();
        this.props.history!.push(ROUTE_PATH.activityList);
    }
    toHome() {
        // history.back();
        this.props.history!.push(ROUTE_PATH.home);
    }
    render() {
        let key = this.props.match!.params.key;
        setMainFormTitle("活动报名");
        setMainFormBack('', () => {
            this.toHome();
        });
        return (
            <Row className='list-row' >
                <Col span={24} >
                    <Row type='flex' justify='center'>
                        {(() => {
                            if (key === 'Success') {
                                return (
                                    <img src={require('../../../../static/img/success_hook.jpg')} style={{ height: '100pt' }} />
                                );
                            } else if (key === 'Fail') {
                                return (
                                    <img src={require('../../../../static/img/fail_hook.jpg')} style={{ height: '100pt' }} />
                                );
                            }
                            return null;
                        })()}
                    </Row>
                    {(() => {
                        if (key === 'Success') {
                            return (
                                <Row>
                                    <Row type='flex' justify='center'>{'活动报名成功'}</Row>
                                    <Row type='flex' justify='center'>{'可在我的活动中查看详情'}</Row>
                                </Row>
                            );
                        } else if (key === 'Fail') {
                            return (
                                <Row type='flex' justify='center'>{'活动报名失败'}</Row>
                            );
                        }
                        return null;
                    })()}
                    <Row className='button'>
                        <Button onClick={() => this.btn()}>完成</Button>
                    </Row>
                </Col>
            </Row>
        );
    }
}

/**
 * 控件：活动报名结果页面控制器
 * 描述
 */
@addon('ActivityParticipateView', '活动报名结果页面', '描述')
@reactControl(ActivityParticipateView, true)
export class ActivityParticipateViewControl extends ReactViewControl {
}