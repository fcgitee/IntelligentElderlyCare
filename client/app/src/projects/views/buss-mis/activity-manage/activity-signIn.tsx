import { Col, Row } from "antd";
import { Button } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：活动签到结果页面状态
 */
export interface ActivitySignInViewState extends ReactViewState {
    key: any;
    errMsg: string;
}

/**
 * 组件：活动签到结果页面
 * 描述
 */
export class ActivitySignInView extends ReactView<ActivitySignInViewControl, ActivitySignInViewState> {
    constructor(props: ActivitySignInViewControl) {
        super(props);
        this.state = {
            key: undefined,
            errMsg: '',
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        request(this, AppServiceUtility.activity_service.sign_in_activity!(id))
            .then((data: any) => {
                if (data === 'Success') {
                    this.setState({ key: 'Success' });
                } else {
                    this.setState({ key: 'Fail', errMsg: data });
                }
            });
    }
    /** 完成按钮事件 */
    btn() {
        this.props.history!.push(ROUTE_PATH.activityList);
        // this.props.history!.push(ROUTE_PATH.activityList);
    }
    render() {
        let { key } = this.state;
        setMainFormTitle("活动签到");
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.home);
        });
        return (
            <Row className='list-row'>
                <Col span={24} className='list-col'>
                    <Row type='flex' justify='center'>
                        {(() => {
                            if (key === 'Success') {
                                return (
                                    <img src={require('../../../../static/img/success_hook.jpg')} style={{ height: '100pt' }} />
                                );
                            } else if (key === 'Fail') {
                                return (
                                    <img src={require('../../../../static/img/fail_hook.jpg')} style={{ height: '100pt' }} />
                                );
                            }
                            return null;
                        })()}
                    </Row>
                    <Row type='flex' justify='center'>{key === 'Success' ? '活动签到成功' : this.state.errMsg}</Row>
                    <Row className='button'>
                        <Button onClick={() => this.btn()}>完成</Button>
                    </Row>
                </Col>
            </Row>
        );
    }
}

/**
 * 控件：活动签到结果页面控制器
 * 描述
 */
@addon('ActivitySignInView', '活动签到结果页面', '描述')
@reactControl(ActivitySignInView, true)
export class ActivitySignInViewControl extends ReactViewControl {
}