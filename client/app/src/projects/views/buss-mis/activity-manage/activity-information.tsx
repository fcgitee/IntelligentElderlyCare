import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import { addon } from "pao-aop";
// import { ListView, Card, WingBlank, WhiteSpace, Flex, DatePicker, List, Button, Toast } from "antd-mobile";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { ROUTE_PATH } from "src/projects/app/util-tool";
// import { request } from "src/business/util_tool";
import "./index.less";
// import { MainContent } from "src/business/components/style-components/main-content";
import React from "react";
import { Table } from "antd";
// import { Row } from "antd";

/**
 * 组件：活动信息汇总状态
 */
export interface ActivityInformationViewState extends ReactViewState {
    // 活动列表
    activity_list?: any;
    // 活动列表数量
    activity_list_count?: number;
    // 当前月份
    now_month?: any;
    now_showing_month?: any;
    now_month_obj?: any;
    // 报名总人数
    participate_sum?: number;
    // 签到数量
    signin_sum?: number;
    // 评论数量
    comment_sum?: number;
    // 是否发送
    isSend?: boolean;
    activityDataSource?: any;
}

// const getDataBlob = (datas: any) => {
//     const dataBlob = {};
//     datas.map((item: any, index: number) => {
//         dataBlob[`${index}`] = `row - ${index}`;
//     });
//     return dataBlob;
// };

// const ds = new ListView.DataSource({
//     rowHasChanged: (row1: any, row2: any) => row1 !== row2,
// });

/**
 * 组件：活动信息汇总
 * 活动信息汇总
 */
export class ActivityInformationView extends ReactView<ActivityInformationViewControl, ActivityInformationViewState> {

    public lv1: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            activity_list: [],
            activity_list_count: 0,
            now_month: '',
            now_showing_month: '',
            now_month_obj: new Date(Date.now()),
            participate_sum: 0,
            signin_sum: 0,
            comment_sum: 0,
            // activityDataSource: ds,
        };
    }
    // 月份变更
    // changeMonth(e: any) {
    //     this.setState({
    //         now_month_obj: e,
    //         now_month: this.formatDate(e),
    //     });
    // }
    // componentDidMount() {
    //     this.getMonthData();
    // }
    // btnClick() {
    //     let { now_showing_month, now_month, isSend } = this.state;
    //     if (isSend) {
    //         Toast.fail('请勿操作过快！');
    //         return;
    //     }

    //     if (now_showing_month !== '' && now_showing_month === now_month) {
    //         return;
    //     }
    //     Toast.loading('查询中...', 11);
    //     setTimeout(
    //         () => {
    //             this.setState({
    //                 isSend: false,
    //             });
    //         },
    //         10000,
    //     );
    //     this.setState(
    //         {
    //             isSend: true,
    //         },
    //         () => {
    //             this.getMonthData();
    //         }
    //     );
    // }
    // getMonthData() {
    //     let { now_month, now_month_obj } = this.state;
    //     // 给个默认值
    //     now_month = now_month || this.formatDate(now_month_obj);
    //     let param: any = {
    //         month: now_month,
    //     };
    //     // 汇总信息
    //     request(this, AppServiceUtility.activity_service.get_activity_information_list_all!(param, 1, 10))
    //         .then((datas: any) => {

    //             Toast.hide();

    //             let activity_list = datas['activity_list'] && datas['activity_list']['result'] ? datas['activity_list']['result'] : [];

    //             const activityDataBlob = getDataBlob(activity_list);

    //             this.setState({
    //                 // 
    //                 activity_list_count: datas['activity_list'] && datas['activity_list']['total'] ? datas['activity_list']['total'] : 0,
    //                 // 
    //                 activity_list,
    //                 activityDataSource: this.state.activityDataSource.cloneWithRows(activityDataBlob),
    //                 // 
    //                 signin_sum: datas['signin_list'] && datas['signin_list']['total'] ? datas['signin_list']['total'] : 0,
    //                 // 
    //                 participate_sum: datas['participate_list'] && datas['participate_list']['total'] ? datas['participate_list']['total'] : 0,
    //                 // 
    //                 comment_sum: datas['comment_list'] && datas['comment_list']['total'] ? datas['comment_list']['total'] : 0,

    //                 // 
    //                 now_month,
    //                 now_showing_month: now_month,
    //                 // 
    //                 isSend: false,
    //             });
    //         }).catch(error => {
    //             Toast.hide();
    //             this.setState(
    //                 {
    //                     isSend: false,
    //                 },
    //                 () => {
    //                     Toast.fail(error.message);
    //                 }
    //             );
    //         });
    // }
    // formatDate(date: any) {
    //     const pad = (n: any) => n < 10 ? `0${n}` : n;
    //     const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}`;
    //     return `${dateStr}`;
    // }
    // render() {
    //     const separator = (sectionID: any, rowID: any) => (
    //         <div
    //             key={`${sectionID}-${rowID}`}
    //             style={{
    //                 backgroundColor: '#F5F5F9',
    //                 height: 8,
    //                 // borderTop: '1px solid #ECECED',
    //                 // borderBottom: '1px solid #ECECED',
    //             }}
    //         />
    //     );
    //     const { activity_list_count, activityDataSource, participate_sum, signin_sum, comment_sum, activity_list, now_month_obj } = this.state;

    //     let index1 = activity_list.length - 1;
    //     const row1 = (rowData: any, sectionID: any, rowID: any) => {
    //         if (index1 < 0) {
    //             index1 = activity_list.length - 1;
    //         }
    //         const obj = activity_list[index1--];
    //         return (
    //             <div key={rowID} style={{ padding: '0 15px' }}>
    //                 <div style={{ display: 'flex', padding: '8px 0' }}>
    //                     <div>活动名字：{obj.activity_name}</div>
    //                 </div>
    //                 <div style={{ display: 'flex', padding: '8px 0' }}>
    //                     <div>活动场地：{obj.address}</div>
    //                 </div>
    //                 <div style={{ display: 'flex', padding: '8px 0' }}>
    //                     <div>活动时间：{obj.begin_date}-{obj.end_date}</div>
    //                 </div>
    //             </div>
    //         );
    //     };
    //     return (
    //         <Card className="app-charitable-information">
    //             <WingBlank size="sm">
    //                 <DatePicker
    //                     mode="month"
    //                     title="选择月份"
    //                     extra="Optional"
    //                     value={now_month_obj}
    //                     onChange={(e) => this.changeMonth(e)}
    //                     format={val => `${this.formatDate(val)}`}
    //                 >
    //                     <List.Item arrow="horizontal">选择月份</List.Item>
    //                 </DatePicker>
    //                 <Button type="primary" onClick={() => this.btnClick()}>查询</Button>
    //                 <WhiteSpace size="xl" />
    //                 <div className="aci-title aci-title1">
    //                     <strong>当月活动信息汇总</strong>
    //                 </div>
    //                 <Flex justify="center">
    //                     <Flex.Item>
    //                         <div>当月活动数量</div>
    //                         <div className="aci-num">{activity_list_count}</div>
    //                     </Flex.Item>
    //                     <Flex.Item>
    //                         <div>当月报名人数</div>
    //                         <div className="aci-num">{participate_sum}</div>
    //                     </Flex.Item>
    //                     <Flex.Item>
    //                         <div>当月签到数量</div>
    //                         <div className="aci-num">{signin_sum}</div>
    //                     </Flex.Item>
    //                     <Flex.Item>
    //                         <div>当月评论人数</div>
    //                         <div className="aci-num">{comment_sum}</div>
    //                     </Flex.Item>
    //                 </Flex>
    //                 <WhiteSpace size="lg" />
    //                 <WhiteSpace size="xs" />
    //                 <div className="aci-title aci-title1">
    //                     <strong>当月活动信息</strong>
    //                 </div>
    //                 <WhiteSpace size="xs" />
    //                 {(() => {
    //                     if (activity_list.length > 0) {
    //                         return (
    //                             <ListView
    //                                 ref={el => this.lv1 = el}
    //                                 dataSource={activityDataSource}
    //                                 renderRow={row1}
    //                                 renderSeparator={separator}
    //                                 pageSize={4}
    //                                 useBodyScroll={true}
    //                                 scrollRenderAheadDistance={500}
    //                             />
    //                         );
    //                     } else {
    //                         return (
    //                             <div>
    //                                 <strong>暂无</strong>
    //                             </div>
    //                         );
    //                     }
    //                 })()}
    //                 <WhiteSpace size="xs" />
    //             </WingBlank>
    //         </Card>
    //     );
    // }
    get_number(num: number, flag = false) {
        if (flag === true) {
            return this.randomFrom(4, 80);
        }
        return this.randomFrom(4, 80) + '%';
    }
    randomFrom(lowerValue: any, upperValue: any) {
        return Math.floor(Math.random() * (upperValue - lowerValue + 1) + lowerValue);
    }
    format() {
        var myDate = new Date();
        let Y: any = myDate.getFullYear() + '-';
        let M: any = (myDate.getMonth() + 1 < 10 ? '0' + (myDate.getMonth() + 1) : myDate.getMonth() + 1) + '-';
        let D: any = myDate.getDate() + ' ';
        let h: any = myDate.getHours() + ':';
        let m: any = myDate.getMinutes() + ':';
        let s: any = myDate.getSeconds();
        return Y + M + D + h + m + s;
    }
    render() {
        const columns: any = [
            {
                title: '类型',
                dataIndex: 'type',
                key: 'type',
                align: 'left'
            },
            {
                title: '项目',
                dataIndex: 'name',
                key: 'name',
                align: 'left'
            },
            {
                title: '数值',
                dataIndex: 'shuzhi',
                key: 'shuzhi',
                align: 'center'
            },
            {
                title: '上月同比变化率',
                dataIndex: 'sytbbhl',
                key: 'sytbbhl',
                align: 'center'
            },
            {
                title: '上半年同比变化率',
                dataIndex: 'sbntbbhl',
                key: 'sbntbbhl',
                align: 'center'
            },
            {
                title: '上年同比变化率',
                dataIndex: 'sntbbhl',
                key: 'sntbbhl',
                align: 'center'
            },
            {
                title: '统计时间',
                dataIndex: 'tjsj',
                key: 'tjsj',
                align: 'center',
            }
        ];

        let Zh: any = [{
            'type': '基础',
            'childs': [
                '幸福院挂牌总数',
                '幸福院挂牌通过验收总数',
                '自运营幸福院总数',
                '社工机构总数',
                'APP流量总数',
                '幸福院覆盖率',
                '志愿者工作站总数',
                '幸福小站总数',
                '社区长者饭堂总数',
            ]
        }, {
            'type': '评比',
            'childs': [
                '幸福院挂牌参与评比总数',
                '幸福院评比达到优良总数',
            ]
        }, {
            'type': '运营',
            'childs': [
                '幸福院设备使用总人次',
                '幸福院工作人员总数',
                '持证工作人员总数',
                '幸福院工作人员中专以上学历总数',
                '投诉总次数',
                '幸福院活动组织总数',
                '幸福院参加活动总人数',
                '幸福院APP活动发布总数',
                '志愿者总数',
                '志愿者总时长',
                '幸福小站商品类别总数',
                '社区长者饭堂用餐总数',
            ]
        }, {
            'type': '收支',
            'childs': [
                '幸福院建设资助总额',
                '幸福院评比奖励总额',
                '平台捐赠总额',
                '平台受赠总额',
                '交卖商城成交总数',
                '幸福小站交易总额',
                '长者饭堂补贴总额',
            ]
        }];

        let data: any = [];

        Zh.map((item: any, index: number) => {
            var type = false;
            item.childs.map((itm: any, idx: number) => {
                data.push({
                    type: type === false ? item.type : '',
                    name: itm,
                    shuzhi: this.get_number(10000, true),
                    sytbbhl: this.get_number(10000),
                    sbntbbhl: this.get_number(10000),
                    sntbbhl: this.get_number(10000),
                    tjsj: this.format(),
                });
                if (type === false) {
                    type = true;
                }
            });
        });
        // console.log(data);
        return (
            <Table
                columns={columns}
                dataSource={data}
                bordered={true}
                size="middle"
                rowKey="name"
                pagination={false}
                className="nowraptb"
            />
        );
    }
}

/**
 * 控件：活动信息汇总控制器
 * 活动信息汇总
 */
@addon('ActivityInformationView', '活动信息汇总', '活动信息汇总')
@reactControl(ActivityInformationView, true)
export class ActivityInformationViewControl extends ReactViewControl {

}