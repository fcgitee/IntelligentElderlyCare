import { List } from "antd-mobile";
import { Brief } from "antd-mobile/lib/list/ListItem";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';

/**
 * 组件：单个服务供应商服务商资质控件状态
 */
export interface ServerDetailCredentialsViewState extends ReactViewState {
    /** 数据 */
    server_info?: any;
}

/**
 * 组件：单个服务供应商服务商资质控件
 */
export class ServerDetailCredentialsView extends ReactView<ServerDetailCredentialsViewControl, ServerDetailCredentialsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            server_info: undefined,
        };
    }

    componentWillMount() {
        const serverId = this.props.match!.params.key;

        AppServiceUtility.service_detail_service.get_server_list!({ id: serverId }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    const result = data.result[0];
                    this.setState({ server_info: result });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }

    render() {
        const { server_info } = this.state;
        if (server_info) {
            setMainFormTitle(server_info.name + '资质');
        }
        return (
            server_info ?
                (
                    <div className={'server-detail'}>
                        <List>
                            <List.Item>
                                统一社会信用代码证
                                <Brief style={{ display: "flex", justifyContent: "center" }}>
                                    {
                                        server_info.organization_info.unified_social_credit ?
                                            server_info.organization_info.unified_social_credit.map((e: any, i: number) => {
                                                return <img key={i} style={{ width: 300, height: 300 }} src={e} alt="" />;
                                            })
                                            :
                                            '无'
                                    }
                                </Brief>
                            </List.Item>
                            <List.Item>
                                平台备案承诺书
                                <Brief style={{ display: "flex", justifyContent: "center" }}>
                                    {
                                        server_info.organization_info.platform_filing_commitment ?
                                            server_info.organization_info.platform_filing_commitment.map((e: any, i: number) => {
                                                return <img key={i} style={{ width: 300, height: 300 }} src={e} alt="" />;
                                            })
                                            :
                                            '无'
                                    }
                                </Brief>
                            </List.Item>
                        </List>
                    </div>
                )
                :
                <></>
        );
    }
}

/**
 * 组件：单个服务供应商服务商资质控件
 * 控制单个服务供应商服务商资质控件
 */
@addon('ServerDetailCredentialsView', '单个服务供应商服务商资质控件', '控制单个服务供应商服务商资质控件')
@reactControl(ServerDetailCredentialsView, true)
export class ServerDetailCredentialsViewControl extends ReactViewControl {
}