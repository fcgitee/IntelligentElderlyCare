import { Rate } from "antd";
import { List } from "antd-mobile";
import { Brief } from "antd-mobile/lib/list/ListItem";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';

/**
 * 组件：单个服务供应商详情介绍控件状态
 */
export interface ServerDetailIntroduceViewState extends ReactViewState {
    /** 数据 */
    data: any[];
    server_info?: any;
    server_worker_info?: any;
    imgHeight: number | string;
}

/**
 * 组件：单个服务供应商详情介绍控件
 */
export class ServerDetailIntroduceView extends ReactView<ServerDetailIntroduceViewControl, ServerDetailIntroduceViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [1, 2, 3],
            server_info: undefined,
            server_worker_info: undefined,
            imgHeight: 176,
        };
    }

    componentDidMount() {
        // 105df5ac-0301-11ea-9fd1-005056882303 谷丰id
        const serverId = this.props.match!.params.key;
        AppServiceUtility.service_detail_service.get_server_list!({ id: serverId }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    const result = data.result[0];
                    this.setState({ server_info: result });

                    AppServiceUtility.service_detail_service.get_server_worker_list!({ server_id: serverId })!
                        .then((data: any) => {
                            const worker_result = data;
                            // console.log(data);
                            this.setState({ server_worker_info: worker_result });
                        })
                        .catch((err) => {
                            // console.log(err);
                        });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }

    render() {
        const { server_info, server_worker_info } = this.state;
        // let personnel_info = undefined;
        // if (server_worker_info) {
        //     personnel_info = server_worker_info.personnel_info;
        // }
        return (
            server_info ?
                (
                    <div className={'server-detail'}>
                        <List>
                            <List.Item>
                                简介
                                <Brief>
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: server_info.organization_info.description ?
                                                server_info.organization_info.description :
                                                ''
                                        }}
                                    />
                                </Brief>
                            </List.Item>
                            <List.Item>
                                服务类型
                                <Brief>
                                    {/* TODO：复杂，先跳过 */}
                                    {/* {
                                        server_info.organization_info.introduction? 
                                        server_info.organization_info.introduction:
                                        ''
                                    } */}
                                    服务类型1：
                                   <br />
                                    服务类型2：
                                   <br />
                                    服务类型3：
                                </Brief>
                            </List.Item>
                            <List.Item>
                                从业人员
                                {
                                    server_worker_info ?
                                        server_worker_info.map((info: any, i: number) => {
                                            let data = info[0];
                                            return (
                                                <List key={i}>
                                                    <List.Item
                                                        thumb={
                                                            <img
                                                                // style={{
                                                                //     width: 100
                                                                // }}
                                                                src={
                                                                    (() => {
                                                                        if (data.personnel_info.picture_list) {
                                                                            if (data.personnel_info.picture_list.length > 0) {
                                                                                return data.personnel_info.picture_list.length;
                                                                            }
                                                                        }
                                                                        // TODO：默认图片
                                                                        return require('../../../../../static/img/head_logo.png');
                                                                    })()
                                                                }
                                                            />
                                                        }
                                                    >
                                                        {data.personnel_info.name}   {data.personnel_info.sex}  {data.personnel_info.age} 岁
                                                        <br />
                                                        <Rate />
                                                        <br />
                                                        资格职称：
                                                        <br />
                                                        服务范围：
                                                        <br />
                                                        工作时间段：
                                                    </List.Item>
                                                </List>
                                            );
                                        })
                                        :
                                        <></>
                                }
                                {/* <List>
                                     <List.Item
                                         thumb={
                                             <img
                                                 style={{
                                                     width: 100
                                                 }}
                                                 src={'https:gss1.bdstatic.com/9vo3dSag_xI4khGkpoWK1HF6hhy/baike/crop%3D0%2C115%2C1029%2C679%3Bc0%3Dbaike116%2C5%2C5%2C116%2C38/sign=107516f6db1373f0e17035df993f67ca/b8389b504fc2d56244035e60ed1190ef76c66c30.jpg'}
                                             />
                                         }
                                     >
                                         姓名   女  xx岁
                                         <br />
                                         <Rate />
                                         <br />
                                         资格职称：
                                         <br />
                                         服务范围：
                                         <br />
                                         工作时间段：
                                     </List.Item>
                                 </List> */}
                            </List.Item>
                        </List>
                    </div>
                )
                :
                <></>
        );
    }
}

/**
 * 组件：单个服务供应商详情介绍控件
 * 控制单个服务供应商详情介绍控件
 */
@addon('ServerDetailIntroduceView', '单个服务供应商详情介绍控件', '控制单个服务供应商详情介绍控件')
@reactControl(ServerDetailIntroduceView, true)
export class ServerDetailIntroduceViewControl extends ReactViewControl {
}