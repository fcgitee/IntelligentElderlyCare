import { Icon, Rate, Spin } from "antd";
import { Carousel, List, Toast, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { forceCheckIsLogin, ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';

/**
 * 组件：单个服务供应商详情控件状态
 */
export interface ServerDetailViewState extends ReactViewState {
    /** 数据 */
    data: any[];
    server_info?: any;
    imgHeight: number | string;
    follow: boolean;
    loading: boolean;
}

/**
 * 组件：单个服务供应商详情控件
 */
export class ServerDetailView extends ReactView<ServerDetailViewControl, ServerDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [1, 2, 3],
            server_info: undefined,
            imgHeight: 176,
            follow: false,
            loading: true
        };
    }

    componentDidMount() {
        // 105df5ac-0301-11ea-9fd1-005056882303 谷丰id
        // this.props.match!.params.key
        const serverId = this.props.match!.params.key;
        AppServiceUtility.service_detail_service.get_server_list!({ id: serverId }, 1, 1)!
            .then((data: any) => {
                // console.log(data,'不是吧啊');
                if (data.result.length > 0) {
                    const result = data.result[0];
                    this.setState({ server_info: result, loading: false });
                    if (result.follow_list.length > 0) {
                        this.setState({ follow: true });
                    }
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }

    follow = (id: string) => {
        forceCheckIsLogin(
            this.props,
            {
            },
            {
                backPath: window.location.href
            }
        );
        AppServiceUtility.service_follow_collection_service.update_service_follow_collection!(id, 'follow', '居家服务商')!
            .then((e) => {
                if (e === "Success") {
                    this.setState({ follow: true });
                    Toast.info('关注成功');
                }
                console.warn(e);

            }).catch((e) => {
                console.error(e);
                Toast.fail('关注失败');
            });
    }

    unfollow = (id: string) => {
        AppServiceUtility.service_follow_collection_service.delete_service_follow_collection_by_business_id!({ business_id: id, type: 'follow', object: '居家服务商' })!
            .then((e) => {
                console.warn(e);
                if (e === "Success") {
                    Toast.info('取消关注成功');
                    this.setState({ follow: false });
                }

            }).catch((e) => {
                Toast.fail('取消关注失败');
                console.error(e);
            });
    }
    // 通知react-native打开外部页面
    onpenUrl = (url: string) => {
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'openurl',
                    params: url
                })
            );
        }
    }
    render() {
        const background = { background: 'whitesmoke' };
        const { server_info } = this.state;
        let map_url = "";
        if (server_info) {
            setMainFormTitle(server_info.name);
            map_url = "http://api.map.baidu.com/marker?location=" + server_info.organization_info.lat + "," + server_info.organization_info.lon + "&title=" + server_info.name + "&output=html";
        }

        return (
            <Spin spinning={this.state.loading}>
                {
                    server_info ?
                        (
                            <div className={'server-detail'}>
                                <div style={{ display: 'flex', justifyContent: "flex-end" }}>
                                    {
                                        this.state.follow ?
                                            <Icon type={"heart"} theme="twoTone" twoToneColor="red" onClick={() => this.unfollow(server_info.id)} />
                                            :
                                            <Icon type={"heart"} onClick={() => this.follow(server_info.id)} />
                                    }
                                </div>
                                <WhiteSpace style={{ marginTop: '1em' }} />
                                <Carousel
                                    autoplay={true}
                                    autoplayInterval={5000}
                                    infinite={true}
                                >
                                    {server_info.organization_info.picture_list ?
                                        server_info.organization_info.picture_list.map((val: any, i: number) => (
                                            <img
                                                src={val}
                                                alt=""
                                                style={{ width: '100%', verticalAlign: 'top' }}
                                                onLoad={() => {
                                                    // fire window resize event to change height
                                                    window.dispatchEvent(new Event('resize'));
                                                    this.setState({ imgHeight: 'auto' });
                                                }}
                                            />
                                        ))
                                        :
                                        <img
                                            src={require("src/static/img/no_picture.jpg")}
                                            alt=""
                                            style={{ width: '100%', verticalAlign: 'top' }}
                                        />
                                    }
                                </Carousel>
                                <List>
                                    <List.Item>
                                        <div>
                                            {server_info.name}
                                        </div>
                                        {/* TODO:默认4星 */}
                                        <Rate disabled={true} value={server_info['organization_info'] && server_info['organization_info']['star_level'] ? Number(server_info['organization_info']['star_level']) : 0} />
                                    </List.Item>
                                    <List.Item
                                        wrap={true}
                                        extra={<Icon className='item-icon-size' type="compass" theme="filled" onClick={() => this.onpenUrl(map_url)} />}
                                        thumb={<Icon className='item-icon-size' type="shop" />}
                                        multipleLine={true}
                                        onClick={() => { }}
                                    >
                                        {server_info.organization_info.address}
                                    </List.Item>
                                    <List.Item
                                        thumb={<a href={"tel:" + server_info.organization_info.telephone}> <Icon type="phone" /></a>}
                                    >
                                        {server_info.organization_info.telephone}
                                    </List.Item>
                                    <WhiteSpace style={background} />
                                    <List.Item
                                        arrow="horizontal"
                                        multipleLine={true}
                                        onClick={() => {
                                            this.props.history!.push(ROUTE_PATH.serverDetailIntroduce + '/' + server_info.id);
                                        }}
                                    >
                                        <p> 详情介绍</p>
                                        <div dangerouslySetInnerHTML={{ __html: server_info.organization_info.description }} />
                                    </List.Item>
                                    <WhiteSpace style={background} />
                                    <List.Item
                                        arrow="horizontal"
                                        onClick={() => { this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + '服务项目' + '&' + this.props.match!.params.key); }}
                                    >
                                        热门服务
                                    </List.Item>
                                    <WhiteSpace style={background} />
                                    <List.Item
                                        arrow="horizontal"
                                        onClick={() => {
                                            this.props.history!.push(ROUTE_PATH.serverDetailAggrement + '/' + (server_info && server_info.organization_info && server_info.organization_info.service_agreement ? server_info.organization_info.service_agreement : '暂无数据'));
                                        }}
                                    >
                                        服务协议
                                    </List.Item>
                                    <WhiteSpace style={background} />
                                    <List.Item
                                        arrow="horizontal"
                                        onClick={() => {
                                            this.props.history!.push(ROUTE_PATH.serverDetailCredentials + '/' + server_info.id);
                                        }}
                                    >
                                        服务商资质
                                    </List.Item>
                                </List>
                            </div>
                        )
                        :
                        <></>
                }
            </Spin>
        );
    }
}

/**
 * 组件：单个服务供应商详情控件
 * 控制单个服务供应商详情控件
 */
@addon('ServerDetailView', '单个服务供应商详情控件', '控制单个服务供应商详情控件')
@reactControl(ServerDetailView, true)
export class ServerDetailViewControl extends ReactViewControl {
}