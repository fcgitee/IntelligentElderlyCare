import { WingBlank } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
// import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import './index.less';

/**
 * 组件：单个服务供应商服务协议控件状态
 */
export interface ServerDetailAggrementViewState extends ReactViewState {
    // current_user_info?: any;
}

/**
 * 组件：单个服务供应商服务协议控件
 */
export class ServerDetailAggrementView extends ReactView<ServerDetailAggrementViewControl, ServerDetailAggrementViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            // current_user_info: undefined,
        };
    }

    componentWillMount() {
        // const user = IntelligentElderlyCareAppStorage.getCurrentUser();
        // // console.log(this.props,'啥东西');
        setMainFormTitle("服务协议");
        // this.setState({
        //     current_user_info: user
        // });
    }

    render() {
        // const { current_user_info } = this.state;
        // const service_aggrement = current_user_info && current_user_info.personnel_info && current_user_info.personnel_info.service_aggrement ? current_user_info.personnel_info.service_aggrement : '暂无数据';
        return (
            <div className={'server-detail'}>
                <WingBlank>
                    {this.props.match!.params.key}
                </WingBlank>
            </div>
        );
    }
}

/**
 * 组件：单个服务供应商服务协议控件
 * 控制单个服务供应商服务协议控件
 */
@addon('ServerDetailAggrementView', '单个服务供应商服务协议控件', '控制单个服务供应商服务协议控件')
@reactControl(ServerDetailAggrementView, true)
export class ServerDetailAggrementViewControl extends ReactViewControl {
}