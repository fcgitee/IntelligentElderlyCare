import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Button, Toast, TextareaItem, Steps, Result, Icon } from "antd-mobile";
import { Row } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
// import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
const Step = Steps.Step;
const myImg = (src: any) => <img src={src} className="spe am-icon am-icon-md" alt="" />;
/**
 * 组件：募捐详情页面状态
 */
export interface CharitableRecipientApplyInfoViewState extends BaseReactElementState {
    // 募捐详情
    apply_info: any;
    // 反馈信息
    feedback: string;
    // 募捐步骤
    apply_step: any;
    // 当前步骤
    step_no?: number;
    // 当前状态
    status?: string;
    // 已经成功添加的反馈信息
    success_feedback?: string;
}

/**
 * 组件：募捐详情页面
 * 募捐详情
 */
export class CharitableRecipientApplyInfoView extends BaseReactElement<CharitableRecipientApplyInfoViewControl, CharitableRecipientApplyInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            apply_info: [],
            feedback: '',
            apply_step: [],
            step_no: -2,
            status: '未知',
            success_feedback: '',
        };
    }
    componentDidMount() {
        Toast.loading('Loading...', 1);
        this.reloadFunc();

        // 获取募捐步骤
        request(this, AppServiceUtility.charitable_service.get_recipient_step!({ 'step_type': '个人' }, 1, 1))
            .then((data: any) => {
                if (data) {
                    this.setState({
                        apply_step: data,
                    });
                } else {
                    Toast.fail(`错误信息：${data}`);
                }
            })
            .catch(error => {
                Toast.fail(error.message);
            });
    }
    backList = () => {
        history.back();
    }
    reloadFunc = () => {
        request(this, AppServiceUtility.charitable_service.get_charitable_recipient_apply_list!({ all: true, id: this.props.match!.params.key }, 1, 1))
            .then((datas: any) => {
                Toast.hide();
                let step_no = (datas.result && datas.result[0] && datas.result[0]['new_field'] && datas.result[0]['new_field'][0] && datas.result[0]['new_field'][0]['step_no']) ? datas.result[0]['new_field'][0]['step_no'] : ((datas.result && datas.result[0] && datas.result[0]['step_no']) ? datas.result[0]['step_no'] : -1);
                let status = (datas.result && datas.result[0] && datas.result[0]['new_field'] && datas.result[0]['new_field'][0] && datas.result[0]['new_field'][0]['status']) ? datas.result[0]['new_field'][0]['status'] : ((datas.result && datas.result[0] && datas.result[0]['status']) ? datas.result[0]['status'] : '未知');
                this.setState({
                    step_no,
                    status,
                    apply_info: (datas.result && datas.result[0]) || [],
                });
            });
    }
    changeValue = (e: any) => {
        // console.log(e);
        this.setState({
            'feedback': e
        });
    }
    submit() {
        let { feedback, apply_info } = this.state;
        if (!feedback) {
            Toast.info('请输入反馈信息！');
            return;
        }
        let param = {
            feedback: feedback,
            apply_type: apply_info.apply_type,
            id: this.props.match!.params.key,
        };
        Toast.loading('提交中...');
        request(this, AppServiceUtility.charitable_service.update_charitable_recipient_apply!(param))
            .then((datas: any) => {
                if (datas === 'Success') {
                    Toast.info('提交成功！');
                    apply_info['feedback'] = feedback;
                    this.setState({
                        apply_info,
                    });
                }
            });
        return;
    }
    render() {
        let { step_no, status, apply_info, apply_step } = this.state;
        step_no = step_no === -1 ? apply_step.length - 1 : step_no;
        let errStatus = status === '不通过' ? { status: "error" } : {};
        return (
            <Card>
                <WingBlank size="md">
                    <WhiteSpace size="md" />
                    <Row type='flex' justify='center'>
                        <strong style={{ fontSize: '18px' }}>募捐流程</strong>
                    </Row>
                    {(() => {
                        if (status === '通过') {
                            return (
                                <Result
                                    img={<Icon type="check-circle" className="spe" style={{ fill: '#52C41A' }} />}
                                    title="通过"
                                />
                            );
                        } else if (status === '不通过') {
                            return (
                                <Result
                                    img={<Icon type="cross-circle-o" className="spe" style={{ fill: '#F13642' }} />}
                                    title="不通过"
                                    message={`原因：${apply_info.sp_msg}`}
                                />
                            );
                        } else {
                            return (
                                <Result
                                    img={myImg('https://gw.alipayobjects.com/zos/rmsportal/HWuSTipkjJRfTWekgTUG.svg')}
                                    title="待审批"
                                />
                            );
                        }
                    })()}
                    <div style={{ fontSize: '30px', paddingTop: '10px' }}>
                        <Steps size="lg" {...errStatus} current={step_no} >
                            {apply_step.map((item: any, index: number) => {
                                return (
                                    <Step key={index} title={item.step_name} />
                                );
                            })}
                        </Steps>
                    </div>
                    <Row type='flex' justify='center'>
                        <strong style={{ fontSize: '18px' }}>募捐申请信息</strong>
                    </Row>
                    {apply_info.hasOwnProperty('project_info') && apply_info['project_info'].length > 0 ? <Row><WhiteSpace size="xl" />
                        <Row type='flex' justify='start'>
                            <strong>申请基金名称</strong>
                        </Row>
                        <WhiteSpace size="xs" />
                        <Row>
                            {apply_info.project_info[0]['donate_name']}
                        </Row></Row> : null}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>{apply_info.apply_type === '个人' ? '申请人名字' : '申请机构'}</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.apply_name}
                    </Row>
                    {apply_info.apply_type === '个人' ? <div>
                        <WhiteSpace size="xl" />
                        <Row type='flex' justify='start'>
                            <strong>身份证号码</strong>
                        </Row>
                        <WhiteSpace size="xs" />
                        <Row>
                            {apply_info.id_card_no}
                        </Row>
                    </div> : null}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>联系方式</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.contacts}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>申请金额</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.apply_money}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>银行卡帐号</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.account_number}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>详细地址</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.address}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>申请原因</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.description}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>申请时间</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.create_date}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>申请状态</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {apply_info.status}
                    </Row>
                    {apply_info.beianhao ? <Row>
                        <WhiteSpace size="xl" />
                        <Row type='flex' justify='start'>
                            <strong>备案号</strong>
                        </Row>
                        <WhiteSpace size="xs" />
                        <Row>
                            {apply_info.beianhao}
                        </Row>
                    </Row> : null}
                    {(() => {
                        if (apply_info.project_list && apply_info.project_list.length > 0) {
                            return apply_info.project_list.map((item: any, index: number) => {
                                if (item.ass_info.selete_type === "6") {
                                    return (
                                        <Row key={index}>
                                            <WhiteSpace size="xl" />
                                            <Row type='flex' justify='start'>
                                                <strong>{item.ass_info.project_name}</strong>
                                            </Row>
                                            <Row>
                                                <img
                                                    alt={item.ass_info.project_name}
                                                    src={item.ass_info.value[0]}
                                                />
                                            </Row>
                                        </Row>
                                    );
                                } else {
                                    return (
                                        <Row key={index}>
                                            <WhiteSpace size="xl" />
                                            <Row type='flex' justify='start'>
                                                <strong>{item.ass_info.project_name}</strong>
                                            </Row>
                                            <Row>
                                                {item.ass_info.value || ''}
                                            </Row>
                                        </Row>
                                    );
                                }
                            });
                        }
                        return null;
                    })()}
                    {(() => {
                        if (apply_info.status === '通过') {
                            if (!apply_info['feedback']) {
                                return (
                                    <Row>
                                        <WhiteSpace size="xl" />
                                        <Row type='flex' justify='start'>
                                            <strong>款项反馈</strong>
                                        </Row>
                                        <Row>
                                            <TextareaItem placeholder="请输入款项反馈" rows={5} autoHeight={true} onChange={(e) => this.changeValue(e)} />
                                            <Button type="primary" onClick={() => this.submit()}>提交</Button>
                                        </Row>
                                    </Row>
                                );
                            } else if (apply_info['feedback']) {
                                return (
                                    <Row>
                                        <WhiteSpace size="xl" />
                                        <Row type='flex' justify='start'>
                                            <strong>使用反馈</strong>
                                        </Row>
                                        <WhiteSpace size="xs" />
                                        <Row>
                                            {apply_info.feedback}
                                        </Row>
                                    </Row>
                                );
                            }
                        }
                        return null;
                    })()}
                    <WhiteSpace size='md' />
                    <Button onClick={() => this.backList()}>返回</Button>
                    <WhiteSpace size='md' />
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：募捐详情页面控制器
 * 募捐详情
 */
@addon('CharitableRecipientApplyInfoView', '募捐详情页面', '募捐详情')
@reactControl(CharitableRecipientApplyInfoView, true)
export class CharitableRecipientApplyInfoViewControl extends BaseReactElementControl {

}