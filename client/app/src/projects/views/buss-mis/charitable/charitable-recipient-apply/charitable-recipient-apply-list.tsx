import { Badge, ListView, Tabs, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：个人募捐申请状态
 */
export interface CharitableRecipientApplyListViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
    // 是否加载中
    animating?: boolean;
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：个人募捐申请
 * 个人募捐申请
 */
export class CharitableRecipientApplyListView extends ReactView<CharitableRecipientApplyListViewControl, CharitableRecipientApplyListViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
            animating: true,
        };
    }
    componentDidMount() {
        this.getRecipientData({ all: true });
    }
    getRecipientData(param: any) {
        setTimeout(
            () => {
                this.setState({
                    animating: false,
                });
                Toast.hide();
            },
            10000
        );
        param['is_mine'] = true;
        request(this, AppServiceUtility.charitable_service.get_charitable_recipient_apply_list!(param, 1, 99))
            .then((datas: any) => {
                Toast.hide();
                // 跟web排序不符，要反转一次
                let reverseData = datas.result ? datas.result.reverse() : [];
                const dataBlob = getDataBlob(reverseData);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    animating: false,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            })
            .catch((error: Error) => {
                Toast.hide();
                Toast.fail(error.message);
            });
    }
    tabChange = (tab: any, index: number) => {
        Toast.loading('加载中...', 30);
        this.setState({
            rData: getDataBlob([]),
            dataList: [],
            dataSource: ds,
        });
        let param;
        switch (index) {
            case 1:
                param = { status: '待审批', all: true };
                break;
            case 2:
                param = { status: '通过', all: true };
                break;
            case 3:
                param = { status: '不通过', all: true };
                break;
            default:
                param = { all: true };
                break;
        }
        this.getRecipientData(param);
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.charitableRecipientApplyInfo + '/' + obj.id);
    }
    render() {
        setMainFormTitle("慈善申请记录");
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}><span>{obj.apply_name}</span><span>{obj.status}</span></div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={{ fontWeight: 'bold' }}>{obj.description}</div>
                    </div>
                    <div style={{ textAlign: 'right', paddingBottom: '8px' }}>
                        <div>申请时间：{obj.create_date}</div>
                    </div>
                </div>
            );
        };
        const tabs = [
            { title: <Badge>全部</Badge> },
            { title: <Badge>待审批</Badge> },
            { title: <Badge>通过</Badge> },
            { title: <Badge>不通过</Badge> },
        ];
        let { animating } = this.state;
        return (
            <div>
                <Tabs tabs={tabs} initialPage={0} onChange={this.tabChange} >
                    {this.state.dataList.length > 0 ? <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    /> : <p style={{ marginTop: '20px', textAlign: 'center' }}>{animating === true ? '' : '无数据'}</p>}
                    {this.state.dataList.length > 0 ? <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    /> : <p style={{ marginTop: '20px', textAlign: 'center' }}>{animating === true ? '' : '无数据'}</p>}
                    {this.state.dataList.length > 0 ? <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    /> : <p style={{ marginTop: '20px', textAlign: 'center' }}>{animating === true ? '' : '无数据'}</p>}
                    {this.state.dataList.length > 0 ? <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    /> : <p style={{ marginTop: '20px', textAlign: 'center' }}>{animating === true ? '' : '无数据'}</p>}
                </Tabs>
            </div>
        );
    }
}

/**
 * 控件：个人募捐申请控制器
 * 个人募捐申请
 */
@addon('CharitableRecipientApplyListView', '个人募捐申请', '个人募捐申请')
@reactControl(CharitableRecipientApplyListView, true)
export class CharitableRecipientApplyListViewControl extends ReactViewControl {

}