import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, WingBlank, WhiteSpace } from "antd-mobile";
import { Row } from "antd";
/**
 * 组件：慈善会信息状态
 */
export class SocietyInformationState {
}

/**
 * 组件：慈善会信息
 * 慈善会信息
 */
export class SocietyInformation extends React.Component<SocietyInformationControl, SocietyInformationState> {

    componentWillMount() {
        this.setState({
            data_info: this.props.data_info || [],
        });
    }
    render() {
        let { data_info } = this.props;

        // 慈善会
        // 初始化，抑制报错
        if (!data_info) {
            data_info = [];
        }
        // 名字
        data_info!['name'] = data_info!['name'] || "";
        // 描述
        data_info!['description'] = data_info!['description'] || "";
        // 图片
        data_info!['picture'] = data_info!['picture'] || "";
        // 证书吧
        data_info!['certificate'] = data_info!['certificate'] || "";
        const picCls = {
            maxWidth: '100%',
        };
        return (
            <Card>
                <WingBlank>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>慈善会名字</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {data_info['name']}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>慈善会描述</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {data_info['description']}
                    </Row>
                    {
                        data_info['picture'] && data_info['picture'].length > 0 ? data_info['picture'].map((item: any, index: number) => {
                            return (
                                <div key={index}>
                                    <WhiteSpace size="xl" />
                                    <Row type='flex' justify='start'>
                                        <strong>慈善会图片</strong>
                                    </Row>
                                    <WhiteSpace size="xs" />
                                    <Row>
                                        <img src={item} alt={item} style={picCls} />
                                    </Row>
                                </div>
                            );
                        }) : ''
                    }
                    {
                        data_info['certificate'] && data_info['certificate'].length > 0 ? data_info['certificate'].map((item: any, index: number) => {
                            return (
                                <div key={index}>
                                    <WhiteSpace size="xl" />
                                    <Row type='flex' justify='start'>
                                        <strong>慈善会募捐证书</strong>
                                    </Row>
                                    <WhiteSpace size="xs" />
                                    <Row>
                                        <img src={item} alt={item} style={picCls} />
                                    </Row>
                                </div>
                            );
                        }) : ''
                    }
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：慈善会信息控制器
 * 慈善会信息
 */
@addon('SocietyInformation', '慈善会信息', '慈善会信息')
@reactControl(SocietyInformation, true)
export class SocietyInformationControl extends BaseReactElementControl {
    /** 基础数据 */
    data_info?: any;
}