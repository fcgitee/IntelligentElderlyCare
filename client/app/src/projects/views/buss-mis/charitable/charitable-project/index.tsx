import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Button } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";

/**
 * 组件：慈善项目列表状态
 */
export interface CharitableProjectListViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：慈善项目列表
 * 慈善项目列表
 */
export class CharitableProjectListView extends ReactView<CharitableProjectListViewControl, CharitableProjectListViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_project_list_all!({}, 1, 99))
            .then((datas: any) => {
                const dataBlob = getDataBlob(datas.result);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.charitableProjectInfo + '/' + obj.id);
    }
    toDonate = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.charitableDonate + '/' + obj.id);
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }}>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <img src={obj.project_picture[0]} style={{ display: 'block', maxWidth: '100%' }} />
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>项目名称：{obj.project_name}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>项目状态：{obj.project_status}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>项目意向：{obj.project_intention}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>已接收捐款：{obj.donate_money_sum}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0', paddingBottom: '8px' }}>
                        <div>项目时间：{obj.begin_date} - {obj.end_date}</div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: "space-between", padding: '8px 0', paddingBottom: '8px' }}>
                        <Button style={{ padding: '0 15px' }} size="large" onClick={(e: any) => this.viewInfo(e, obj)}>查看详情</Button>
                        <Button style={{ padding: '0 15px' }} size="large" type="primary" onClick={(e: any) => this.toDonate(e, obj)}>我要捐助</Button>
                    </div>
                </div>
            );
        };
        return (
            <div>
                <ListView
                    ref={el => this.lv = el}
                    dataSource={this.state.dataSource}
                    renderRow={row}
                    renderSeparator={separator}
                    pageSize={4}
                    useBodyScroll={true}
                    onScroll={() => { }}
                    scrollRenderAheadDistance={500}
                />
            </div>
        );
    }
}

/**
 * 控件：慈善项目列表控制器
 * 慈善项目列表
 */
@addon('CharitableProjectListView', '慈善项目列表', '慈善项目列表')
@reactControl(CharitableProjectListView, true)
export class CharitableProjectListViewControl extends ReactViewControl {

}