import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Button, Toast } from "antd-mobile";
import { Row } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";

/**
 * 组件：慈善项目详情状态
 */
export interface CharitableProjectInfoViewState extends BaseReactElementState {
    // 项目详情
    project_info: any;
}

/**
 * 组件：慈善项目详情
 * 项目详情
 */
export class CharitableProjectInfoView extends BaseReactElement<CharitableProjectInfoViewControl, CharitableProjectInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            project_info: [],
        };
    }
    componentDidMount() {
        Toast.loading('Loading...', 1);
        this.reloadFunc();
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.charitableProjectList);
    }
    reloadFunc = () => {
        request(this, AppServiceUtility.charitable_service.get_charitable_project_list_all!({ id: this.props.match!.params.key }, 1, 1))
            .then((datas: any) => {
                Toast.hide();
                this.setState({
                    project_info: (datas.result && datas.result[0]) || [],
                });
            });
    }
    notDo() {
        // console.log('没事做');
    }
    render() {
        const project_info = this.state.project_info;
        return (
            <Card>
                <WingBlank size="sm">
                    {(() => {
                        if (project_info.project_picture && project_info.project_picture[0]) {
                            return (
                                <Row>
                                    <WhiteSpace size="xl" />
                                    <Row>
                                        <img src={project_info.project_picture[0]} style={{ display: 'block', maxWidth: '100%' }} />
                                    </Row>
                                </Row>
                            );
                        }
                        return null;
                    })()}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目名称</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.project_name}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>已接收捐款</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.donate_money_sum}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目发起机构</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.organization_name}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目意向</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.project_intention}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目描述</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.remark}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目状态</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.project_status}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>项目时间</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {project_info.create_date} - {project_info.end_date}
                    </Row>
                    <WhiteSpace size='md' />
                    <Button onClick={this.backList}>返回</Button>
                    <WhiteSpace size='md' />
                </WingBlank>
            </Card >
        );
    }
}

/**
 * 控件：慈善项目详情控制器
 * 项目详情
 */
@addon('CharitableProjectInfoView', '慈善项目详情', '项目详情')
@reactControl(CharitableProjectInfoView, true)
export class CharitableProjectInfoViewControl extends BaseReactElementControl {

}