import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Card, Button, Result, Icon } from "antd-mobile";
import { Row } from "antd";
// import { ROUTE_PATH } from "src/projects/app/util-tool";
const myImg = (src: any) => <img src={src} className="spe am-icon am-icon-md" alt="" />;
/**
 * 组件：结果页状态
 */
export interface CharitableResultState extends ReactViewState {
    // 返回链接
    return_url: string;
    // 状态
    status?: string;
    // 标题
    title?: string;
    // 内容
    desc?: string;
}

/**
 * 组件：结果页
 * 结果页
 */
export class CharitableResult extends ReactView<CharitableResultControl, CharitableResultState> {
    constructor(props: any) {
        super(props);
        this.state = {
            return_url: '',
            status: '',
            title: '',
            desc: '',
        };
    }
    componentDidMount() {
        let { return_url, status, title, desc } = this.props;

        this.setState({
            return_url: return_url || '',
            status: status || 'wait',
            title: title || '等待处理',
            desc: desc || '',
        });
    }
    backUrl() {
        this.props.pprops.history!.push(this.state.return_url);
    }
    render() {
        const { status, title, desc, return_url } = this.state;
        return (
            <Card>
                {(() => {
                    if (status === 'wait') {
                        return (
                            <Result
                                img={myImg('https://gw.alipayobjects.com/zos/rmsportal/HWuSTipkjJRfTWekgTUG.svg')}
                                title={title}
                                message={desc}
                            />
                        );
                    } else if (status === 'success') {
                        return (
                            <Result
                                img={<Icon type="check-circle" className="spe" style={{ fill: '#1F90E6' }} />}
                                title={title}
                                message={desc}
                            />
                        );
                    }
                    return null;
                })()}
                {return_url ? <Row>
                    <Button onClick={() => this.backUrl()}>返回</Button>
                </Row> : null}
            </Card>
        );
    }
}

/**
 * 控件：结果页控制器
 * 结果页
 */
@addon('CharitableResult', '结果页', '结果页')
@reactControl(CharitableResult, true)
export class CharitableResultControl extends ReactViewControl {
    // 返回链接
    return_url: string;
    // 状态
    status?: string;
    // 标题
    title?: string;
    // 内容
    desc?: string;
    // 
    pprops?: any;
}