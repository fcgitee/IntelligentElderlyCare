import { Grid } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
/**
 * 组件：慈善机构首页状态
 */
export interface CharitableListViewState extends ReactViewState {
    icon_data?: any[];
    /*用户id */
    id?: any;
    /*账号名 */
    name?: any;
}

/**
 * 组件：慈善机构首页
 * 描述
 */
export class CharitableListView extends ReactView<CharitableListViewControl, CharitableListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            icon_data: [
                // { text: '我要捐赠', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '慈善捐赠记录', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '申请冠名基金', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '基金申请记录', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                { text: '慈善基金列表', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '个人申请募捐', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '机构申请募捐', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '募捐申请记录', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '慈善项目列表', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
                // { text: '慈善信息汇总', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> },
            ]
        };
    }
    grid_click = (_el: any) => {
        switch (_el.text) {
            // case '慈善捐赠记录':
            //     this.props.history!.push(ROUTE_PATH.charitableDonateList);
            //     break;
            // case '慈善项目列表':
            //     this.props.history!.push(ROUTE_PATH.charitableProjectList);
            //     break;
            // case '个人申请募捐':
            //     this.props.history!.push(ROUTE_PATH.charitableRecipientApply);
            //     break;
            // case '机构申请募捐':
            //     this.props.history!.push(ROUTE_PATH.charitableOrganizationApply);
            //     break;
            // case '募捐申请记录':
            //     this.props.history!.push(ROUTE_PATH.charitableRecipientApplyList);
            //     break;
            // case '我要捐赠':
            //     this.props.history!.push(ROUTE_PATH.charitableDonate);
            //     break;
            // case '慈善信息汇总':
            //     this.props.history!.push(ROUTE_PATH.charitableInformation);
            //     break;
            // case '申请冠名基金':
            //     this.props.history!.push(ROUTE_PATH.charitableTitleFund);
            //     break;
            // case '基金申请记录':
            //     this.props.history!.push(ROUTE_PATH.charitableTitleFundRecord);
            //     break;
            case '慈善基金列表':
                this.props.history!.push(ROUTE_PATH.charitableTitleFundList);
                break;
            default:
                break;
        }
    }
    render() {
        setMainFormTitle("慈善基金");
        return (
            <div>
                <Grid data={this.state.icon_data} activeStyle={true} columnNum={3} onClick={_el => this.grid_click(_el)} />
            </div>
        );
    }
}

/**
 * 控件：慈善机构首页控制器
 * 描述
 */
@addon('CharitableListView', '慈善机构首页', '描述')
@reactControl(CharitableListView, true)
export class CharitableListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}