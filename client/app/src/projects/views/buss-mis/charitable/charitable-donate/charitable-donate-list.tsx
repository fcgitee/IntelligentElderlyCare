import { ListView, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：慈善捐款列表状态
 */
export interface CharitableDonateListViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：慈善捐款列表
 * 慈善捐款列表
 */
export class CharitableDonateListView extends ReactView<CharitableDonateListViewControl, CharitableDonateListViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({}, 1, 99))
            .then((datas: any) => {
                // 跟web排序不符，要反转一次
                let reverseData = datas.result ? datas.result.reverse() : [];
                const dataBlob = getDataBlob(reverseData);
                this.setState({
                    rData: dataBlob,
                    dataList: reverseData,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            })
            .catch((error: Error) => {
                Toast.fail(error.message);
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.charitableDonateInfo + '/' + obj.id);
    }
    render() {
        setMainFormTitle("捐赠记录");
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>捐款名义：{obj.donate_name || ''}</div>
                    </div>
                    {/* <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>捐款项目：{obj.donate_project_name || ''}</div>
                    </div> */}
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>捐款金额：{obj.donate_money || 0}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>已用金额：{obj.payed_money || 0}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>冻结金额：{obj.freeze_money || 0}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>联系方式：{obj.contacts || ''}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>支付状态：{obj.pay_status || ''}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0' }}>
                        <div>捐款时间：{obj.create_date || ''}</div>
                    </div>
                    <div style={{ display: 'flex', padding: '8px 0', paddingBottom: '8px' }}>
                        <div>捐款留言：{obj.remark || ''}</div>
                    </div>
                </div>
            );
        };
        return (
            <div>
                {this.state.dataList.length > 0 ? <ListView
                    ref={el => this.lv = el}
                    dataSource={this.state.dataSource}
                    renderRow={row}
                    renderSeparator={separator}
                    pageSize={4}
                    useBodyScroll={true}
                    onScroll={() => { }}
                    scrollRenderAheadDistance={500}
                /> : <p style={{ marginTop: '20px', textAlign: 'center' }}>暂无数据</p>}
            </div>
        );
    }
}

/**
 * 控件：慈善捐款列表控制器
 * 慈善捐款列表
 */
@addon('CharitableDonateListView', '慈善捐款列表', '慈善捐款列表')
@reactControl(CharitableDonateListView, true)
export class CharitableDonateListViewControl extends ReactViewControl {

}