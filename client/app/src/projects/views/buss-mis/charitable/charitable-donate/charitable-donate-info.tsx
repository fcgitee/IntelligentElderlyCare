import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Button, Toast } from "antd-mobile";
import { Row } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
/**
 * 组件：慈善捐款详情状态
 */
export interface CharitableDonateInfoViewState extends BaseReactElementState {
    // 募捐详情
    donate_info: any;
    // 重新获取的开关，只让拿一次
    reGet: boolean;
}

/**
 * 组件：慈善捐款详情
 * 募捐详情
 */
export class CharitableDonateInfoView extends BaseReactElement<CharitableDonateInfoViewControl, CharitableDonateInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            donate_info: [],
            reGet: true,
        };
    }
    componentDidMount() {
        Toast.loading('Loading...', 1);
        this.reloadFunc();
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.charitableDonateList);
    }
    reloadFunc = () => {
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({ id: this.props.match!.params.key }, 1, 1))
            .then((datas: any) => {
                Toast.hide();
                let donate_info = (datas.result && datas.result[0]) || [];
                this.setState({
                    donate_info,
                });
                if (this.state.reGet === true && (donate_info['pay_status'] !== '已付款' || donate_info['status'] !== '通过')) {
                    this.checkPayStatus(donate_info['out_trade_no']);
                }
            }).catch((error: Error) => {
                Toast.fail(error.message);
            });
    }
    checkPayStatus(out_trade_no: string) {
        request(this, AppServiceUtility.charitable_service.checkPayStatus!({ out_trade_no: out_trade_no, checkType: 'donate' }))
            .then((datas: any) => {
                // 已付款重新获取数据
                if (datas === '已付款') {
                    this.setState(
                        {
                            reGet: false,
                        },
                        () => {
                            this.reloadFunc();
                        }
                    );
                }
            }).catch((error: Error) => {
                Toast.fail(error.message);
            });
    }
    notDo() {
        // console.log('没事做');
    }
    render() {
        const donate_info = this.state.donate_info;
        return (
            <Card>
                <WingBlank size="sm">
                    <WhiteSpace size="md" />
                    <Row type='flex' justify='center'>
                        <strong style={{ fontSize: '18px' }}>捐款信息</strong>
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>捐款名义</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.donate_name || '无'}
                    </Row>
                    {/* <WhiteSpace size="xl" />
                        <Row type='flex' justify='start'>
                            <strong>捐款项目</strong>
                        </Row>
                        <WhiteSpace size="xs" />
                        <Row>
                            {donate_info.donate_project_name || '无'}
                        </Row> */}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>捐款金额</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.donate_money || 0}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>已用金额</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.payed_money || 0}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>冻结金额</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.freeze_money || 0}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>剩余金额</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.surplus_money || 0}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>联系方式</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.contacts || '无'}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>支付状态</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.pay_status || '无'}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>捐款留言</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.remark || '无'}
                    </Row>
                    <WhiteSpace size='xl' />
                    <Row type='flex' justify='start'>
                        <strong>捐赠时间</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {donate_info.create_date || '无'}
                    </Row>
                    <WhiteSpace size="lg" />
                    {donate_info['pay_status'] === '已付款' && donate_info['recipient_info'] ?
                        <Card>
                            <Card.Header
                                title="款项去向"
                            />
                            {donate_info['recipient_info'].length > 0 ? donate_info['recipient_info'].map((item: any, index: number) => {
                                return (
                                    <WingBlank key={index} size="lg">
                                        <WhiteSpace size="lg" />
                                        <Card.Body>
                                            <Row type='flex' justify='start'>
                                                <strong>募捐者</strong>
                                            </Row>
                                            <WhiteSpace size="xs" />
                                            <Row>
                                                {item.apply_name || '无'}
                                            </Row>
                                            <WhiteSpace size="xl" />
                                            <Row type='flex' justify='start'>
                                                <strong>使用金额</strong>
                                            </Row>
                                            <WhiteSpace size="xs" />
                                            <Row>
                                                {item.give_money || 0}
                                            </Row>
                                            <WhiteSpace size="xl" />
                                            <Row type='flex' justify='start'>
                                                <strong>募捐反馈</strong>
                                            </Row>
                                            <WhiteSpace size="xs" />
                                            <Row>
                                                {item.feedback || '暂无'}
                                            </Row>
                                            <WhiteSpace size='md' />
                                        </Card.Body>
                                    </WingBlank>
                                );
                            }) : <Row type="flex" justify="center">暂无使用记录</Row>}
                        </Card> : null
                    }
                    <WhiteSpace size="lg" />
                    <Button onClick={this.backList}>返回</Button>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：慈善捐款详情控制器
 * 募捐详情
 */
@addon('CharitableDonateInfoView', '慈善捐款详情', '募捐详情')
@reactControl(CharitableDonateInfoView, true)
export class CharitableDonateInfoViewControl extends BaseReactElementControl {

}