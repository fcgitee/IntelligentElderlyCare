import { Row, Spin } from "antd";
import { Card, ListView, WhiteSpace, SearchBar } from "antd-mobile";
import moment from "moment";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle, setMainFormBack } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import '../../../buss-pub/news-list/index.less';
import './charitableTitleFund.less';
function MyBody(props: any) {
    return (
        <Row className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </Row>
    );
}

/**
 * 组件：慈善基金列表状态
 */
export interface CharitableTitleFundListViewState extends BaseReactElementState {
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    empty?: boolean;
    animating?: any;
    // 每页数量
    pageCount: number;
    page?: number;
    keyword?: string;
}

/**
 * 组件：慈善基金列表
 * 慈善基金列表
 */
export class CharitableTitleFundListView extends BaseReactElement<CharitableTitleFundListViewControl, CharitableTitleFundListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: true,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            empty: false,
            animating: true,
            pageCount: 10,
            keyword: '',
        };
    }
    componentDidMount() {
        this.getCharitableTitleFundList();
    }
    getCharitableTitleFundList(param: any = {}, page: number = 1) {
        let { list, keyword } = this.state;
        if (keyword !== '') {
            param['donate_name'] = keyword;
        }
        param['all'] = true;
        param['status'] = '通过';
        this.setState(
            {
                animating: true,
            },
            () => {
                request(this, AppServiceUtility.charitable_service.get_charitable_title_fund_list!(param))
                    .then((datas: any) => {
                        if (datas && datas.result.length > 0) {
                            this.setState({
                                list: list!.concat(datas.result),
                                page,
                                animating: false,
                            });
                        } else {
                            this.setState({
                                animating: false,
                                empty: true,
                            });
                        }
                    })
                    .catch((error: Error) => {
                        console.info(error);
                    });
            }
        );
    }
    /** 下拉事件 */
    onEndReached = () => {
        const { animating, page } = this.state;
        if (animating === true) {
            return;
        }
        this.getCharitableTitleFundList({}, page! + 1);
    }
    searchKey(e: any) {
        this.setState(
            {
                empty: false,
                keyword: e,
                list: [],
            },
            () => {
                this.getCharitableTitleFundList({});
            }
        );
    }
    onClickRow = (id: string) => {
        this.props.history!.push(ROUTE_PATH.charitableTitleFundDetail + '/' + id);
    }
    endOrIng(begin_date: any, end_date: any) {
        const begin_date_time = new Date(begin_date).getTime();
        const end_date_time = new Date(end_date).getTime();
        const now_date_time = new Date().getTime();
        if (now_date_time > end_date_time) {
            return (
                <Row className="tag end">已结束</Row>
            );
        } else if (now_date_time <= end_date_time && now_date_time >= begin_date_time) {
            return (
                <Row className="tag ing">进行中</Row>
            );
        } else if (now_date_time < begin_date_time) {
            return (
                <Row className="tag end">未开始</Row>
            );
        } else {
            return null;
        }
    }
    render() {
        const { list, dataSource, animating, empty } = this.state;
        setMainFormTitle("慈善基金列表");
        setMainFormBack('', () => {
            this.props.history!.push(ROUTE_PATH.home);
        });
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <Card className='list-conten title-fund-list'>
                    <Row type='flex' justify='center' onClick={this.onClickRow.bind(this, owData.id)}>
                        <Row>
                            <img className="title-fund-img" src={owData.photo && owData.photo[0] ? owData.photo[0] : ''} />
                        </Row>
                        <Row className="title-fund-content">
                            {this.endOrIng(owData['begin_date'], owData['end_date'])}
                            <Row><strong>{owData.donate_name}</strong></Row>
                            <Row>{owData.donate_intention}</Row>
                            <Row>开始时间：{moment(owData.begin_date).format("YYYY-MM-DD hh:mm")}</Row>
                            <Row>结束时间：{moment(owData.end_date).format("YYYY-MM-DD hh:mm")}</Row>
                        </Row>
                    </Row>
                </Card>
            );
        };
        return (
            <Row>
                <SearchBar placeholder="搜索" maxLength={8} onSubmit={(e: any) => this.searchKey(e)} />
                <Row className='tabs-content'>
                    {
                        list && list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(list)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: this.state.height }}
                            /> : null
                    }
                    {animating ? <Row>
                        <WhiteSpace size="lg" />
                        <Row style={{ textAlign: 'center' }}>
                            <Spin size="large" />
                        </Row>
                        <WhiteSpace size="lg" />
                    </Row> : null}
                    {empty ? <Row>
                        <WhiteSpace size="lg" />
                        <Row className='tabs-content' type='flex' justify='center'>{list && list.length ? '已经是最后一条了' : '无数据'}</Row>
                        <WhiteSpace size="lg" />
                    </Row> : null}
                </Row>
            </Row>
        );
    }
}

/**
 * 控件：慈善基金列表控制器
 * 慈善基金列表
 */
@addon('CharitableTitleFundListView', '慈善基金列表', '慈善基金列表')
@reactControl(CharitableTitleFundListView, true)
export class CharitableTitleFundListViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}