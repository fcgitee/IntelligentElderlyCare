import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Card, Button, Toast } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row } from "antd";
import moment from "moment";
import '../../../buss-pub/news-list/index.less';
import './charitableTitleFund.less';
import { request } from "src/business/util_tool";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";

/**
 * 组件：慈善基金详细状态
 */
export interface CharitableTitleFundDetailViewState extends BaseReactElementState {
    /** 请求的数据 */
    list: any;
    /** 长列表容器高度 */
    height?: any;
}

/**
 * 组件：慈善基金详细
 * 慈善基金详细
 */
export class CharitableTitleFundDetailView extends BaseReactElement<CharitableTitleFundDetailViewControl, CharitableTitleFundDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            list: [],
            height: document.documentElement!.clientHeight,
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_title_fund_list!({ id: this.props.match!.params.key, all: true, 'status': '通过' }))
            .then((datas: any) => {
                if (datas.result.length > 0) {
                    this.setState({
                        list: datas.result[0]
                    });
                }
            })
            .catch((error: Error) => {
                console.info(error);
            });
    }
    endOrIng(begin_date: any, end_date: any) {
        const begin_date_time = new Date(begin_date).getTime();
        const end_date_time = new Date(end_date).getTime();
        const now_date_time = new Date().getTime();
        if (now_date_time > end_date_time) {
            return (
                <Row className="tag end">已结束</Row>
            );
        } else if (now_date_time <= end_date_time && now_date_time >= begin_date_time) {
            return (
                <Row className="tag ing">进行中</Row>
            );
        } else if (now_date_time < begin_date_time) {
            return (
                <Row className="tag end">未开始</Row>
            );
        } else {
            return null;
        }
    }
    applyNow() {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        // console.log(user);
        if (!user) {
            Toast.fail('请先登录！');
            return;
        }
        this.props.history!.push(ROUTE_PATH.charitableRecipientApply + '/' + this.state.list['id']);
    }
    render() {
        const { list } = this.state;
        return (
            <Row>
                <Card className='list-conten title-fund-list'>
                    <Row type='flex' justify='center'>
                        <Row>
                            <img className="title-fund-img" src={list.photo && list.photo[0] ? list.photo[0] : ''} />
                        </Row>
                        <Row className="title-fund-content">
                            {this.endOrIng(list['begin_date'], list['end_date'])}
                            <Row><strong>{list.donate_name}</strong></Row>
                            <Row>{list.donate_intention}</Row>
                            <Row>{moment(list.begin_date).format("YYYY-MM-DD hh:mm")} - {moment(list.end_date).format("YYYY-MM-DD hh:mm")}</Row>
                            <Row><span className="red-color font-big">{list.surplus_money}</span>元&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span className="red-color">{list.apply_count || 0}</span>人已申请</Row>
                        </Row>
                    </Row>
                </Card>
                <Card className='list-conten title-fund-list'>
                    <Row type='flex' justify='center'>
                        <Row className="title-fund-object red-color">
                            救助对象
                        </Row>
                        <Row className="title-fund-content">
                            {list.donate_object}
                        </Row>
                    </Row>
                </Card>
                <Card className='list-conten title-fund-list'>
                    <Row type='flex' justify='center'>
                        <Row className="title-fund-object red-color">
                            基金描述
                        </Row>
                        <Row className="title-fund-content">
                            {list.donate_description}
                        </Row>
                    </Row>
                </Card>
                <Row className="fix-bot-btn">
                    <Button type="primary" onClick={() => this.applyNow()}>马上申请</Button>
                </Row>
            </Row>
        );
    }
}

/**
 * 控件：慈善基金详细控制器
 * 慈善基金详细
 */
@addon('CharitableTitleFundDetailView', '慈善基金详细', '慈善基金详细')
@reactControl(CharitableTitleFundDetailView, true)
export class CharitableTitleFundDetailViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}