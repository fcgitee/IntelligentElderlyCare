import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import './index.less';
import { List } from 'antd-mobile';
import { Icon } from "antd";
const Item = List.Item;
/**
 * 组件：问题详情状态
 */
export interface ProblemDetailsViewState extends ReactViewState {
    // 数据
    data?: any;
    // 详情
    detail?: any;
}

/**
 * 组件：问题详情
 * 描述
 */
export class ProblemDetailsView extends ReactView<ProblemDetailsViewControl, ProblemDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [
                {
                    id: '1',
                    content: '如何注册智慧康养APP？'
                },
                {
                    id: '2',
                    content: '智慧康养APP绑定小壹助手步骤？'
                },
                {
                    id: '3',
                    content: '购买服务后工作人员什么时候上门服务？'
                }
            ],
            detail: {},
        };
    }
    componentDidMount() {
    }
    componentWillMount() {
        let id = this.props.match!.params.key;
        let detail = {};
        switch (id) {
            case '1':
                detail = {
                    id: '1',
                    content: '如何注册智慧康养APP？',
                    answer: '<p>1.安卓手机需先下载应用宝，在应用宝中搜索智慧康养，进行下载。</p>\
                    <p>2.苹果手机在App Stroe上搜索智慧康养，进行下载。</p>\
                    <p>3.使用手机号进行注册，设置密码，填写手机收到的验证码即可完成注册。</p>'
                };
                break;
            case '2':
                detail = {
                    id: '1',
                    content: '智慧康养APP绑定小壹助手步骤？',
                    answer: '<p>1. 手机打开智慧康养APP，进入到【首页】界面。</p>\
                    <p>2. 点击【首页】界面右上角的【+】，进入到设备绑定界面。</p>\
                    <p>3. 点击【小壹助手】【立即扫描】扫描小壹助手背后的条形码绑定；【手动输入IMEI号】也可进行绑定。</p>\
                    <p>4. 绑定手机号：手机号为小壹助手内SIM卡的号码。</p>\
                    <p>5. 绑定成功，小壹助手会显示在首页界面，云平台也会自动同步绑定信息。</p>\
                    <p>6. 小壹助手【定位】功能：在【首页】界面点击【小壹助手】进入功能界面，会半屏显示小壹助手位置信息，点击【全屏】可全屏显示小壹助手位置信息，可以对地图进行放大缩小。</p>\
                    <p>7. 小壹助手【留言】功能：长按中间【话筒】样式的按键，录入语音信息，屏幕出现3秒倒数字符，倒数结束留言保存成功，即时下发到小壹助手。</p>\
                    <p>8. 小壹助手会发出“您有未读的消息”语音提醒，轻触右侧留言按键即可播放留言内容。</p>\
                    <p>9. 小壹助手【提醒】功能，点击【提醒】进入到提醒功能界面、点击右上角的【+】，新增提醒事项。</p>\
                    <p>10. 点击【+】，新增提醒、点击中间的录制按钮录制6秒的语音，点击【完成】。</p>\
                    <p>11.设置提醒时间：输入【标题】、设置3三个【提醒时间】选择【提醒日期】。</p>\
                    <p>12. 设置完成后、在设定时间小壹助手会自动播放提醒信息，无须按键。</p>'
                };
                break;
            case '3':
                detail = {
                    id: '1',
                    content: '购买服务后工作人员什么时候上门服务？',
                    answer: '1.支付完成即可预约服务，预约时间早：8:00-18:00，请提前两个小时预约。'
                };
                break;
            default:
                break;
        }
        this.setState({ detail });
    }
    render() {
        return (
            <div>
                <List>

                    <Item
                        thumb={<Icon type="question-circle" style={{ fontSize: '20px', color: '#0c82c7' }} />}
                        multipleLine={true}
                        wrap={true}
                    >
                        {this.state.detail['content']}
                    </Item>
                    <Item
                        // thumb={<Icon type="check-circle" style={{ fontSize: '20px', color: '#0cc78c', float: 'left' }} />}
                        multipleLine={true}
                        wrap={true}
                    >
                        <div dangerouslySetInnerHTML={{ __html: this.state.detail['answer'] }} />
                    </Item>
                </List>
            </div>
        );
    }
}

/**
 * 控件：问题详情控制器
 * 描述
 */
@addon('CommonProblemView', '问题详情', '问题详情')
@reactControl(ProblemDetailsView, true)
export class ProblemDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}