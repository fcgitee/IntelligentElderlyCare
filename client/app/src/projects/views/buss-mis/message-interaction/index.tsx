import { Button, Input } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState, CookieUtil } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
/**
 * 组件：消息互动状态
 */
export interface MessageInteractionViewState extends ReactViewState {
    // 屏幕宽度
    PWidth?: number;
    // 聊天内容
    chatContent?: any;
    // 聊天框输入
    chatIn?: string;
    // 记录
    session_id?: any;
    data_list?: any;
}

/**
 * 组件：消息互动
 * 描述
 */
export class MessageInteractionView extends ReactView<MessageInteractionViewControl, MessageInteractionViewState> {
    private stoTimer: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            PWidth: 0,
            chatContent: [
                {
                    content: '欢迎使用智慧养老！您可以在这里与我们对话，我会帮您解决养老服务问题。',
                    type: 'response',
                }
            ],
            chatIn: '',
            session_id: '',
            data_list: [
                {
                    content: '欢迎使用智慧养老！您可以在这里与我们对话，我会帮您解决养老服务问题。',
                    type: 'response',
                }
            ],
        };
    }
    componentDidMount() {
        const online_session_id = CookieUtil.read('online_session_id');
        if (online_session_id) {
            // 获取消息
            this.setState(
                {
                    session_id: online_session_id,
                },
                () => {
                    this.getList();
                }
            );
        }
    }
    sto(times: number) {
        this.stoTimer = window.setTimeout(
            () => {
                if (this.state.session_id) {
                    this.getList();
                }
            },
            times
        );
    }
    componentWillUnmount() {
        if (this.stoTimer) {
            window.clearTimeout(this.stoTimer);
        }
    }
    getList() {
        AppServiceUtility.message_service.get_app_online_message_list!({ user_id: this.state.session_id }, 1, 5)!
            .then((data: any) => {
                if (data && data.result && data.result.length) {
                    this.setState(
                        {
                            data_list: this.state.chatContent.concat(data.result.reverse()),
                        },
                        () => {
                            this.sto(8000);
                            this.autoScroll(99999);
                        }
                    );
                }
            });
    }
    componentWillMount() {
        this.setState({
            PWidth: document.body.clientWidth - 100,
        });
    }
    sendMsg = () => {
        if (this.state.chatIn) {
            let param: any = { content: this.state.chatIn };
            // 发送消息
            request(this, AppServiceUtility.message_service.update_app_online_message!(param)!)
                .then((data: any) => {
                    if (data && data.code && data.code === 'Success') {
                        CookieUtil.save('online_session_id', data.session_id);
                        this.setState(
                            {
                                session_id: data.session_id,
                                chatIn: '',
                            },
                            () => {
                                this.getList();
                            }
                        );
                    }
                });
        }

    }
    msgChange = (e: any) => {
        this.setState({
            chatIn: e.target.value,
        });
    }
    commonProblem = () => {
        this.props.history!.push(ROUTE_PATH.commonProblem);
    }
    autoScroll(height: number) {
        if (document.querySelector('#appMainFormNavBar') && document.querySelector('#appMainFormNavBar')!.parentElement) {
            document.querySelector('#appMainFormNavBar')!.parentElement!.scrollTop = height;
        }
    }
    render() {
        setMainFormTitle('消息互动');
        return (
            <div style={{ background: '#f7f4f8' }}>
                <div style={{ height: '200px', position: 'relative', background: '#f7f4f8' }}>
                    <span
                        style={{
                            position: 'absolute',
                            right: '15px',
                            top: '15px',
                            fontSize: '18px',
                        }}
                        onClick={this.commonProblem}
                    >
                        常见问题
                    </span>
                    <div style={{ height: '150px', textAlign: 'center', background: "white" }}>
                        <span style={{ display: 'inline-block', marginTop: '20px', height: '180px', width: '180px', }}>
                            <img src={require('./people.png')} style={{ height: '100%', width: '100%', borderRadius: '50%' }} />
                        </span>
                    </div>
                </div>
                <div style={{ marginTop: '20px', padding: '10px' }}>
                    {
                        this.state.data_list.map((value: any, index: number) => {
                            return (
                                <div key={index} style={{ marginBottom: '10px' }}>
                                    <img src={require('./people.png')} style={{ float: value.type === 'response' ? 'left' : 'right', height: '80px', width: '80px', borderRadius: '50%' }} />
                                    <div className='chat' style={{ minHeight: '80px', width: this.state.PWidth + 'px', textAlign: value.type === 'response' ? 'left' : 'right' }}>
                                        <div
                                            style={{
                                                display: 'inline-block',
                                                margin: '10px',
                                                borderRadius: '20px',
                                                padding: '10px',
                                                background: value.type === 'response' ? '#C2EB97' : '#ffffff',
                                                textAlign: 'left',
                                            }}
                                        >
                                            {value.content}
                                        </div>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
                <div
                    style={{
                        position: 'fixed',
                        bottom: '0',
                        left: '0',
                        right: '0',
                        height: '50px',
                        borderTop: '1px #f0f0f0 solid'
                    }}
                >
                    <div
                        style={{
                            position: 'absolute',
                            top: '10px',
                            left: '10px',
                            bottom: '7px',
                            right: '80px',
                        }}
                    >
                        <Input
                            value={this.state.chatIn}
                            onChange={this.msgChange}
                            style={{
                                height: '100%',
                                width: '100%',
                                boxShadow: '0px 0px 5px 0px rgba(170,170,170,0.5) inset',
                                borderRadius: 25,
                            }}
                        />
                    </div>
                    <div
                        style={{
                            position: 'absolute',
                            top: '10px',
                            bottom: '10px',
                            right: '10px',
                        }}
                    >
                        <Button
                            style={{
                                background: 'rgba(255,255,255,0.3)',
                                borderRadius: 25,
                                boxShadow: '0px 0px 4px 0px rgba(170,170,170,0.5)'
                            }}
                            onClick={this.sendMsg}
                        >
                            发送
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * 控件：消息互动控制器
 * 描述
 */
@addon('MessageInteractionView', '消息互动', '消息互动')
@reactControl(MessageInteractionView, true)
export class MessageInteractionViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}