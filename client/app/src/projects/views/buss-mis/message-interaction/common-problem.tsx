import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import './index.less';
import { List } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
const Item = List.Item;
/**
 * 组件：常见问题状态
 */
export interface CommonProblemViewState extends ReactViewState {
    // 数据
    data?: any;
}

/**
 * 组件：常见问题
 * 描述
 */
export class CommonProblemView extends ReactView<CommonProblemViewControl, CommonProblemViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [
                {
                    id: '1',
                    content: '如何注册智慧康养APP？'
                },
                {
                    id: '2',
                    content: '智慧康养APP绑定小壹助手步骤？'
                },
                {
                    id: '3',
                    content: '购买服务后工作人员什么时候上门服务？'
                }
            ]
        };
    }
    componentDidMount() {
    }
    componentWillMount() {
    }
    problem = (value: any) => {
        this.props.history!.push(ROUTE_PATH.problemDetails + '/' + value);
    }
    render() {
        return (
            <div>
                <List renderHeader={() => { return (<span style={{ color: '#000000' }}>以下是常见问题回复，您也可以直接提问客服。</span>); }}>

                    {
                        this.state.data.map((value: any) => {
                            return (
                                <Item arrow="horizontal" onClick={this.problem.bind(this, value.id)} key={value.id}>{value.content}</Item>
                            );
                        })
                    }
                </List>
            </div>
        );
    }
}

/**
 * 控件：常见问题控制器
 * 描述
 */
@addon('CommonProblemView', '常见问题', '常见问题')
@reactControl(CommonProblemView, true)
export class CommonProblemViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}