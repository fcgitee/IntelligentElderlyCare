import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Row, Avatar, Button, } from "antd";
import { WhiteSpace } from "antd-mobile";
import Input from "antd-mobile/lib/input-item/Input";
/**
 * 组件：好友对话视图状态
 */
export interface FriendDialogueViewState extends BaseReactElementState {
}

/**
 * 组件：好友对话视图
 * 描述
 */
export class FriendDialogueView extends BaseReactElement<FriendDialogueViewControl, FriendDialogueViewState> {
    render() {
        return (
            <div style={{ position: 'relative', height: '100%', width: '100%' }}>
                <Row style={{ height: '50px', width: '100%', lineHeight: '50px', background: '#fff' }} type='flex' justify='center'>成默默</Row>
                <div style={{ height: '100%', width: '100%', padding: '0px 10px' }}>
                    <WhiteSpace size='lg' />
                    <div>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginLeft: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'left' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div style={{ textAlign: 'right' }}>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginRight: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'right' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginLeft: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'left' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div style={{ textAlign: 'right' }}>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginRight: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'right' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginLeft: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'left' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div style={{ textAlign: 'right' }}>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginRight: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'right' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginLeft: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'left' }} />
                    </div>
                    <WhiteSpace size='sm' />
                    <div style={{ textAlign: 'right' }}>
                        <div style={{ display: 'inline-block', padding: '6px', background: '#fff', border: '1px #ccc solid', borderRadius: '5px', marginRight: '5px' }}>
                            你好啊
                        </div>
                        <Avatar size='large' icon="user" style={{ float: 'right' }} />
                    </div>
                </div>
                <div style={{ position: 'fixed', bottom: '0px', width: '100%', height: '50px', background: '#fff' }}>
                    <div style={{ position: 'absolute', left: '10px', right: '85px', height: '50px', lineHeight: '50px' }}>
                        <Input style={{ height: '32px', width: '100%', border: '1px #ccc solid', borderRadius: '5px' }} />
                    </div>
                    <div style={{ position: 'absolute', right: '10px', height: '50px', lineHeight: '50px' }}><Button type='primary'>发送</Button></div>
                </div>
            </div>
        );
    }
}

/**
 * 控件：好友对话视图控制器
 * 描述
 */
@addon('FriendDialogueView', '好友对话视图', '描述')
@reactControl(FriendDialogueView)
export class FriendDialogueViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}