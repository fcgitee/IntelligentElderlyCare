import { Col, Input, Row } from "antd";
import { Card, ListView } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { LoadingEffectView } from 'src/business/views/loading-effect';
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
const { Search } = Input;
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：需求列表视图控件状态
 */
export interface DemandListViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 长者列表 */
    elderList?: any[];
    /** 家人信息 */
    family_data?: any[];
}
/**
 * 组件：需求列表视图控件
 */
export class DemandListView extends ReactView<DemandListViewControl, DemandListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            elderList: [],
            upLoading: true,
            pullLoading: false,
            family_data: [],
            height: document.documentElement!.clientHeight,
        };
    }
    componentDidMount() {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (!user) {
            return;
        }
        // let user = {
        //     'id': '23b3630a-d92c-11e9-8b9a-983b8f0bcd67'
        // };
        AppServiceUtility.service_demand_service.get_demand!({ 'publisher_id': user.id })!
            .then((data: any) => {
                this.setState({
                    list: data.result
                });
            });
        // 获取当前登陆人的家属信息
        AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': user.id })!
            .then((data: any) => {
                this.setState({ upLoading: false });
                if (data['result']) {
                    let old: any[] = [];
                    data.result.map((value: any, index: number) => {
                        if (value.relation_type_name === '父亲' || value.relation_type_name === '母亲') {
                            old.push(value);
                        }
                    });
                    this.setState({
                        elderList: old,
                    });
                }
                this.setState({
                    family_data: data['result']
                });
            });
    }
    onstop = (id: any) => {
        this.props.history!.push(ROUTE_PATH.demandDetails + '/' + id);
    }
    search = (value: any) => {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (!user) {
            return;
        }
        AppServiceUtility.service_demand_service.get_demand!({ 'publisher_id': user.id, 'demand_title': value })!
            .then((data: any) => {
                this.setState({
                    list: data.result
                });
            });
    }
    eldSelect = (e: any) => {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (!user) {
            return;
        }
        AppServiceUtility.service_demand_service.get_demand!({ 'publisher_id': user.id, 'elder_id': e })!
            .then((data: any) => {
                this.setState({
                    list: data.result
                });
            });

    }
    render() {
        const { list, dataSource, } = this.state;
        setMainFormTitle('我的需求');
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten' onClick={() => this.onstop(owData.id)}>
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={10}><img src={owData.demand_picture && owData.demand_picture.length > 0 ? owData.demand_picture[0] : ''} style={{ height: '72pt', width: '100pt' }} /></Col>
                            <Col span={14} className='list-col'>
                                <Row>
                                    <Col span={16}>
                                        <strong>{owData.demand_title}</strong>
                                    </Col>
                                    <Col span={8}>
                                        <div className='waiting-orders'>{owData.state}</div>
                                    </Col>

                                </Row>
                                <Row>
                                    <span
                                        style={{
                                            wordBreak: 'break-all',
                                            textOverflow: 'ellipsis',
                                            overflow: 'hidden',
                                            display: ' -webkit-box',
                                            WebkitLineClamp: 2,
                                            WebkitBoxOrient: 'vertical',
                                        }}
                                    >
                                        {owData.demand_explain}
                                    </span>
                                </Row>
                                <Row style={{ fontSize: '10px' }}>
                                    <Col span={12}>{
                                        owData.service_type_list.map((value: any, index: number) => {
                                            if (owData.service_type && value.value === owData.service_type[0]) {
                                                return value.label;
                                            }
                                        })
                                    }</Col>
                                    <Col span={12}>{owData.create_date}</Col>
                                </Row>
                                <Row>
                                    {owData.price_type}<span style={{ color: 'orange' }}>￥{owData.price}</span>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                <div style={{ height: '50px', }}>
                    <Search
                        placeholder="搜索"
                        onSearch={this.search}
                        style={{ width: '100%', height: '40px', marginTop: '3px' }}
                    />
                </div>
                {this.state.upLoading && <LoadingEffectView />}
                {
                    this.state.elderList!.length > 0 ?
                        <div className='my-needs-header'>
                            {
                                this.state.elderList!.map((value: any, index: number) => {
                                    return (
                                        <div
                                            key={index}
                                            onClick={this.eldSelect.bind(this, value.family_person[0].id)}
                                            style={{
                                                display: 'inline-block',
                                                height: '80px',
                                                width: '80px',
                                                borderRadius: '50%',
                                                border: '1px blue solid',
                                                marginLeft: '10px',
                                                paddingTop: '20px',
                                            }}
                                        >
                                            <div style={{ textAlign: 'center', fontSize: '14px' }}>{value.family_person[0].name}</div>
                                            <div style={{ textAlign: 'center', fontSize: '12px' }}>{value.relation_type_name}</div>
                                        </div>
                                    );
                                })
                            }
                        </div> : ''
                }
                <div className='tabs-content'>
                    {
                        list && list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(list)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: this.state.height }}
                            />
                            :
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无数据
                            </Row>

                    }
                </div>
            </Row>
        );
    }
}

/**
 * 组件：需求列表视图控件
 * 控制需求列表视图控件
 */
@addon('DemandListView', '需求列表视图控件', '控制需求列表视图控件')
@reactControl(DemandListView, true)
export class DemandListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}