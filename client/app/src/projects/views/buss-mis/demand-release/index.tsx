import { Radio, Button } from "antd";
import { DatePicker, InputItem, List, Picker, TextareaItem, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { setMainFormTitle } from "src/business/util_tool";
import { remote } from "src/projects/remote";
import { AppServiceUtility } from "src/projects/app/appService";
// import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";

const Item = List.Item;
/**
 * 组件：需求发布视图状态
 */
export interface DemandReleaseViewState extends ReactViewState {
    // 数据
    data?: any;
    // 紧急联系人id
    emergency_contact_id?: any;
    // 长者id
    elder_id?: string;
    // 需求标题
    demand_title?: string;
    // 服务类型
    service_type?: any;
    // 需求说明
    demand_explain?: string;
    // 开始日期
    start_date?: any;
    // 结束日期
    end_date?: any;
    // 出价类型
    price_type?: string;
    // 价格
    price?: string;
    // 需求照片
    demand_picture?: string[];
    // 长者姓名
    elder_name?: string;
    // 服务地址
    service_address?: string;
    // 性别
    sex?: string;
    // 生日
    date_birth?: string;
    // 联系电话
    phone?: string;
    // 联系人列表
    emergency_contact?: any;
    // 服务类型列表
    service_type_list?: any;
    // 数据id
    id?: string;
    // 发布者id
    publisher_id?: string;
}

/**
 * 组件：需求发布视图
 * 需求发布视图
 */
export class DemandReleaseView extends ReactView<DemandReleaseViewControl, DemandReleaseViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            elder_name: '',
            emergency_contact: [],
            service_type_list: [],
        };
    }
    componentDidMount() {
        console.info(this.props.match!.params.key);
        if (this.props.match!.params.key) {
            AppServiceUtility.service_demand_service.get_demand!({ 'id': this.props.match!.params.key })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            id: this.props.match!.params.key,
                            elder_name: data.result[0].elder_name,
                            elder_id: data.result[0].elder_id,
                            service_address: data.result[0].service_address,
                            sex: data.result[0].sex,
                            date_birth: data.result[0].date_birth,
                            phone: data.result[0].phone,
                            emergency_contact_id: data.result[0].emergency_contact_id,
                            service_type: data.result[0].service_type,
                            price_type: data.result[0].price_type,
                            price: data.result[0].price,
                            demand_picture: data.result[0].demand_picture,
                            demand_title: data.result[0].demand_title,
                            demand_explain: data.result[0].demand_explain,
                            start_date: new Date(data.result[0].start_date),
                        });
                    }

                });
        } else if (this.props.location!.state.select_family_data!) {
            let info = this.props.location!.state.select_family_data[0];
            this.setState({
                elder_name: info.family_person[0].name,
                elder_id: info.family_person[0].id,
                service_address: info.family_person[0].personnel_info.address,
                sex: info.family_person[0].personnel_info.sex,
                date_birth: info.family_person[0].personnel_info.date_birth,
                phone: info.family_person[0].personnel_info.telephone,

            });
        }
        // 获取当前登陆人的家属信息
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (user) {
            AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': user.id })!
                .then((data: any) => {
                    if (data['result']) {
                        let per: any[] = [];
                        data.result.map((value: any, index: number) => {
                            if (value.family_person) {
                                let p = {
                                    value: value.family_person[0].id,
                                    label: value.family_person[0].name,
                                };
                                per.push(p);
                            }
                        });
                        this.setState({
                            emergency_contact: per
                        });
                    }
                });
        }
        // 获取服务项目类型
        AppServiceUtility.service_operation_service.get_service_type_list!({ 'is_show_app': true, 'is_top': true }, undefined, undefined)!
            .then((data: any) => {
                let icon_data: any = [];
                if (data.result) {
                    data.result.map((item: any) => {
                        let icon = { label: item.name, value: item.id };
                        icon_data.push(icon);
                    });
                }
                this.setState({
                    service_type_list: icon_data,
                });
            });
        this.setState({
            publisher_id: user.id,
        });

    }

    getNow() {
        if (!this.state.date_birth) {
            return;
        }
        let date = this.state.date_birth!;
        date = date.substring(0, 19);
        // 必须把日期'-'转为'/'
        date = date.replace(/-/g, '/');
        let timestamp = new Date(date).getTime();
        let timestamps = new Date().getTime();
        return Math.floor(Math.abs(timestamps - timestamp) / (24 * 3600 * 1000 * 360));
    }
    emergencyContactChange = (e: any) => {
        this.setState({
            emergency_contact_id: e,
        });

    }

    demandTitleChange = (e: any) => {
        this.setState({
            demand_title: e,
        });
    }
    demandExplainChange = (e: any) => {
        this.setState({
            demand_explain: e,
        });
    }
    startTime = (e: any) => {
        this.setState({
            start_date: e
        });
    }
    endTime = (e: any) => {
        this.setState({
            end_date: e,
        });
    }
    servictTypeChange = (e: any) => {
        this.setState({
            service_type: e,
        });
    }
    upImg = (e: any) => {
        this.setState({
            demand_picture: e,
        });
    }
    priceTypeChange = (e: any) => {
        console.info(e);
        this.setState({
            price_type: e.target.value
        });
    }
    priceChange = (e: any) => {
        this.setState({
            price: e
        });
    }
    submit = () => {
        let obj = this.state;
        if (!obj.emergency_contact_id) {
            Toast.info('请选择紧急联系人');
            return;
        } else if (!obj.demand_title) {
            Toast.info('请输入需求标题');
            return;
        } else if (!obj.start_date) {
            Toast.info('请选择开始时间');
            return;
        } else if (!obj.price_type) {
            Toast.info('请选择价格类型');
            return;
        } else if (!obj.price) {
            Toast.info('请输入价格');
            return;
        }
        AppServiceUtility.service_demand_service.update_demand!(obj)!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.info('提交成功');
                    this.props.history!.go(-1);
                }
            });
    }
    phoneChange = (e: any) => {
        this.setState({
            phone: e,
        });
    }
    add_family_files = () => {
        this.props.history!.push(ROUTE_PATH.familyFiles);
    }

    render() {
        setMainFormTitle("需求发布");
        return (
            <div>
                <List renderHeader={() => '请填写如下信息'}>
                    <Item
                        thumb={<img src='https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1385781871,3367469142&fm=26&gp=0.jpg' style={{ height: "50px", width: '50px', borderRadius: '50%' }} />}
                        onClick={() => { }}
                    >
                        <span>{this.state.elder_name}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{this.state.sex}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{this.getNow()}周岁</span>
                    </Item>
                </List>
                <WhiteSpace />
                <List>
                    <InputItem
                        onChange={this.phoneChange}
                        value={this.state.phone}
                        placeholder={this.state.phone}
                    >
                        联系电话
                    </InputItem>
                    <Item extra={this.state.service_address}> 服务地址</Item>
                    {
                        this.state.emergency_contact.length > 0 ?
                            <Picker onChange={this.emergencyContactChange} value={this.state.emergency_contact_id} data={this.state.emergency_contact} cols={1} className="forss" extra='请选择紧急联系人'>
                                <List.Item arrow="horizontal">紧急联系人</List.Item>
                            </Picker> :
                            <Item extra={<Button type='primary' onClick={this.add_family_files}>添加</Button>}> 紧急联系人</Item>
                    }
                </List>
                <WhiteSpace />
                <List>
                    <InputItem onChange={this.demandTitleChange} value={this.state.demand_title} placeholder='请输入需求标题'>
                        需求标题
                    </InputItem>
                    <Picker data={this.state.service_type_list} value={this.state.service_type} onChange={this.servictTypeChange} cols={1} className="forss" extra='请选择服务类型'>
                        <List.Item arrow="horizontal">服务类型</List.Item>
                    </Picker>
                    <TextareaItem onChange={this.demandExplainChange} value={this.state.demand_explain} placeholder="请输入需求说明" count={1000} rows={3} autoHeight={true} />
                </List>
                <WhiteSpace />
                <List renderHeader={() => '预约时间'}>
                    <DatePicker onChange={this.startTime} value={this.state.start_date!}>
                        <List.Item arrow="horizontal">请选择开始日期</List.Item>
                    </DatePicker>
                    <DatePicker onChange={this.endTime} value={this.state.end_date!}>
                        <List.Item arrow="horizontal">请选择结束日期</List.Item>
                    </DatePicker>
                </List>
                <WhiteSpace />
                <List renderHeader={() => '出价'}>
                    <Item>
                        <Radio.Group onChange={this.priceTypeChange} value={this.state.price_type}>
                            <Radio value={'最高价格'} defaultChecked={true}>最高价格</Radio>
                            <Radio value={'固定价格'}>固定价格</Radio>
                        </Radio.Group>
                    </Item>
                    <InputItem
                        onChange={this.priceChange}
                        placeholder="0.00"
                        type='money'
                        extra="元"
                        value={this.state.price!}
                    >
                        价格
                    </InputItem>
                </List>
                <List renderHeader={() => '照片（大小小于2M，格式支持jpg/jpeg/png）'}>
                    <FileUploadBtn value={this.state.demand_picture} onChange={this.upImg} list_type={"picture-card"} contents={"plus"} action={remote.upload_url} />
                </List>
                <div onClick={this.submit} className="confirmation-orders-buttom info-list">
                    确认发布
                </div>
            </div>
        );
    }
}

/**
 * 控件：需求发布视图控制器
 * 需求发布视图
 */
@addon('DemandReleaseView', '需求发布视图', '需求发布视图')
@reactControl(DemandReleaseView, true)
export class DemandReleaseViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}