// import { Grid } from 'antd-mobile';
import { Avatar, Checkbox, message } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
import { Card } from 'antd-mobile';
/**
 * 组件：关怀设置状态
 */
export interface CareSettingViewState extends ReactViewState {
    current_user_id?: any;
    family_data?: any;
    person_data?: any;
    options?: any;
    type_list?: any;
    show?: any;
    checked?: any;
    checked_id?: any;
    record_list?: any;
    index?:any;
}

/**
 * 组件：关怀设置
 * 描述
 */
export class CareSettingView extends ReactView<CareSettingViewControl, CareSettingViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            current_user_id: undefined,
            family_data: [],
            person_data: [],
            type_list: [],
            record_list: [],
            checked_id: undefined,
            show: false,
            index: '',
            checked: false,
            options: [
                { label: 'Apple', value: 'Apple' },
                { label: 'Pear', value: 'Pear' },
                { label: 'Orange', value: 'Orange' },
            ]
        };
    }
    componentDidMount() {
        // 获取当前登陆人信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                this.setState({
                    current_user_id: data[0]['id'],
                    person_data: data[0]
                });
                // 获取当前登陆人的家属信息
                AppServiceUtility.family_files_service.get_user_relation_ship_list!({ 'main_relation_people': data[0]['id'] })!
                    .then((data: any) => {
                        // console.log('所属亲属信息》》》》》', data);
                        this.setState({
                            family_data: data['result']
                        });
                        // this.selectRecord(data[0]['id'], data['result'][0].family_person[0].id);
                    });
            });
        // 查询关怀类型
        AppServiceUtility.family_files_service.get_care_type_list!({})!
            .then((data: any) => {
                // console.log('类型', data);
                this.setState({
                    type_list: data.result
                });
            });
    }
    selectRecord = (main_concern_people: any, concerned_people: any) => {
        // 查询关怀记录
        AppServiceUtility.family_files_service.get_care_record!({ main_concern_people, concerned_people })!
            .then((data: any) => {
                // console.log('记录', data);
                this.setState({
                    record_list: data.result
                });
                let type_list: any = [];
                type_list = this.state.type_list;
                if (data.result.length > 0) {
                    this.state.type_list.map((items: any, index: any) => {
                        let care_type: any = [];
                        let id_list: any = [];
                        data.result.map((item: any) => {
                            care_type.push(item.care_type);
                            id_list.push(
                                { 'id': item.id, 'care_type': item.care_type }
                            );
                            // if (item.care_type === items.id) {
                            //     type_list[index]['care_status'] = true;
                            //     type_list[index]['record_id'] = item.id;
                            // }
                        });
                        if (care_type.indexOf(items.id) !== -1) {
                            type_list[index]['care_status'] = true;
                            id_list.map((item_x: any) => {
                                if (item_x.care_type === items.id) {
                                    type_list[index]['record_id'] = item_x.id;
                                }
                            });
                        } else {
                            type_list[index]['care_status'] = false;
                            type_list[index]['record_id'] = '';
                        }
                    });
                } else {
                    this.state.type_list.map((items: any, index: any) => {
                        type_list = this.state.type_list;
                        type_list[index]['care_status'] = false;
                        type_list[index]['record_id'] = '';
                    });
                }
                this.setState({
                    type_list,
                    show: true,
                });
                // console.log(type_list);

            });
    }
    check = (id: any,index:any) => {
        // console.log(id);
        this.setState({
            checked_id: id,
            index
        });
        this.selectRecord(this.state.current_user_id, id);
    }

    update = (e: any, id: any, record_id: any) => {
        this.setState({
            show: false
        });
        // console.log(e.target.checked);
        this.setState({
            checked: e.target.checked
        });
        if (e.target.checked) {
            if (this.state.checked_id === undefined) {
                message.error('请先选择关怀的人员',1);
                return;
            }
            let data = {
                main_concern_people: this.state.current_user_id,
                concerned_people: this.state.checked_id,
                care_type: id,
            };
            // 新增关怀记录
            AppServiceUtility.family_files_service.update_care_record!(data)!
                .then((data: any) => {
                    message.success('设置成功',1);
                    // console.log('新增', data);
                    this.selectRecord(this.state.current_user_id, this.state.checked_id);
                });
        } else {
            // 删除关怀记录
            AppServiceUtility.family_files_service.del_care_record!(record_id)!
                .then((data: any) => {
                    message.success('取消成功',1);
                    // console.log('删除', data);
                    this.selectRecord(this.state.current_user_id, this.state.checked_id);
                });
        }

    }
    render() {
        setMainFormTitle('关怀设置');
        return (
            <div>
                <div className='touxiang'>
                    <div style={{ marginTop: '10px', display: 'flex' }}>
                        <div style={{ textAlign: 'center', display: 'flex' }}>
                            {this.state.family_data.map((item: any,index:any) => {
                                return <div key={item.id} onClick={() => { this.check(item.family_person[0].id,index); }} className={index === this.state.index ? 'checked' : ''}>
                                    <Avatar size={64} src={"https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"} style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                                    <p>{item.family_person[0].name}</p>
                                </div>;
                            })}
                        </div>
                    </div>
                </div>
                <Card full={true}>
                    <Card.Header
                        title="关怀内容"
                    />
                    <Card.Body style={{ display: 'flex', justifyContent: 'space-around' }}>
                        {this.state.type_list.map((item: any) => {
                            return <div key={item.id}>
                                <Checkbox checked={item.care_status ? item.care_status : false} onChange={(e) => { this.update(e, item.id, item.record_id); }}>{item.type}</Checkbox>
                            </div>;
                        })}
                    </Card.Body>
                </Card>
                <Card full={true}>
                    <Card.Header
                        title="关怀记录"
                    />
                    {this.state.record_list.length > 0 ? this.state.record_list.map((item: any) => {
                        return <Card.Body key={item} style={{ display: 'flex', justifyContent: 'space-around' }}>
                            <div>
                                <span style={{ marginRight: '10px' }}>
                                    {item.current_year + '年' + item.current_month + '月' + item.current_day + '日'}
                                </span>
                                <span>
                                    {'您关注了' + item.concerned_name + '的' + item.care_type_name + '情况'}
                                </span>
                            </div>
                        </Card.Body>;
                    })
                        :
                        <Card.Body style={{ display: 'flex', justifyContent: 'space-around' }}>
                            <div>
                                你对此用户暂无关怀记录
                            </div>
                        </Card.Body>
                    }
                </Card>
            </div>
        );
    }
}

/**
 * 控件：关怀设置控制器
 * 描述
 */
@addon('CareSettingView', '关怀设置', '描述')
@reactControl(CareSettingView, true)
export class CareSettingViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}