import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Row, Col, } from "antd";
import { Tabs, ListView, Card } from "antd-mobile";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const tabs = [
    { title: '综合' },
    { title: '距离' },
    { title: '预约' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：服务列表视图控件状态
 */
export interface ServiceListViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
}
/**
 * 组件：服务列表视图控件
 */
export class ServiceListView extends ReactView<ServiceListViewControl, ServiceListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
        };
    }
    componentDidMount() {
        AppServiceUtility.service_package_service.get_service_product_package_list!({}, 1, 10)!
            .then((data: any) => {
                let list: any[] = [];
                if (data.result) {
                    list = data.result.map((key: any, value: any) => {
                        let service = {};
                        service['id'] = key['id'];
                        service['name'] = key['name'];
                        // service['service_org_name'] = key['org'][0]['name'];
                        service['price'] = key['total_price'];
                        /** 缺少统计和图片url */
                        service['purchase_number'] = '500';
                        service['img_url'] = key['picture_collection'] ? key['picture_collection'][0] : 'https://www.e-health100.com/api/attachment/servicetype/typePicture2_225?1516587849871';
                        return service;
                    });
                    console.info(list);
                    this.setState(
                        { list }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    onClickRow = (id: string) => {
        console.info(id);
        this.props.history!.push(ROUTE_PATH.serviceInfo + '/' + id);
    }
    render() {
        const { list, dataSource, } = this.state;
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten'>
                        <Row type='flex' justify='center' onClick={this.onClickRow.bind(this, owData.id)}>
                            <Col className='list-col' span={10}><img src={owData.img_url} style={{ height: '72pt', width: '100pt' }} /></Col>
                            <Col span={14} className='list-col'>
                                <Row><strong>{owData.name}</strong></Row>
                                <Row>{owData.service_org_name}</Row>
                                <Row>
                                    <Col span={12}>{owData.purchase_number}已购买</Col>
                                    <Col span={12}><strong>¥{owData.price}</strong></Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                >
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div>
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div>
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>
                        }
                    </div>
                </Tabs>
            </Row>
        );
    }
}

/**
 * 组件：服务列表视图控件
 * 控制服务列表视图控件
 */
@addon('ServiceListView', '服务列表视图控件', '控制服务列表视图控件')
@reactControl(ServiceListView, true)
export class ServiceListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}