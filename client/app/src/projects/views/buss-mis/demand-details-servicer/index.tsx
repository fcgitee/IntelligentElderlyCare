import { Icon, Row, Input, Button } from "antd";
import { Card, Carousel, List, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
const Item = List.Item;
const Brief = Item.Brief;
/**
 * 组件：服务商需求详情视图控件状态
 */
export interface DemandDetailsServicerViewState extends ReactViewState {
    /** 图片高度 */
    imgHeight?: number | string;
    /** 数据对象 */
    elderdata?: any;
    /** 需求对象 */
    demand?: any;
    /** 竞单价格 */
    bidding_price?: number;
}
/**
 * 组件：服务商需求详情视图控件
 */
export class DemandDetailsServicerView extends ReactView<DemandDetailsServicerViewControl, DemandDetailsServicerViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            imgHeight: 176,
            elderdata: {
                organization_info: {
                    organization_nature: '',
                    address: '',
                    telephone: '',
                    comment: '',
                    picture_list: [],
                }
            },
            demand: {},
            bidding_price: 0,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            AppServiceUtility.service_demand_service.get_demand_details!({ 'id': this.props.match!.params.key })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            demand: data.result[0]
                        });
                    }

                });
        }
    }
    getNow(date: any) {
        if (!date) {
            return '';
        }
        date = date.substring(0, 19);
        // 必须把日期'-'转为'/'
        date = date.replace(/-/g, '/');
        let timestamp = new Date(date).getTime();
        let timestamps = new Date().getTime();
        return Math.floor(Math.abs(timestamps - timestamp) / (24 * 3600 * 1000 * 360));
    }
    commontChage = (value: any) => {
        value.persist();
        this.setState({
            bidding_price: value.target.value,
        });
    }
    sub = () => {
        let { bidding_price } = this.state;
        if (!bidding_price) {
            Toast.info('请输入价格');
            return;
        }
        let obj = {
            bidding_price: bidding_price,
            demand_id: this.props.match!.params.key,
        };
        if (this.props.match!.params.key) {
            AppServiceUtility.service_demand_service.update_demand_bidding!(obj)!
                .then((data: any) => {
                    if (data === 'Success') {
                        Toast.info('竞单成功');
                        this.props.history!.go(-1);
                    } else {
                        Toast.info(data);
                    }

                });
        }

    }
    render() {
        setMainFormTitle('需求详情');
        let info = this.state.demand;
        let plclist = info.demand_picture;
        // plclist = data.organization_info.picture_list;
        let dom = (
            <div className='demand-details-servicer'>
                {
                    plclist && plclist.length > 0 ?
                        <Carousel
                            autoplay={true}
                            infinite={true}
                            className="acasdasdasd"
                        >
                            {plclist!.map((data: any, index: number) => {
                                return (
                                    <a
                                        className='carousel-a'
                                        key={index}
                                        href="javascript:;"
                                        style={{ height: this.state.imgHeight }}
                                    >
                                        <img
                                            className='carousel-img'
                                            src={data}
                                            alt=""
                                            onLoad={() => {
                                                window.dispatchEvent(new Event('resize'));
                                                this.setState({ imgHeight: 'auto' });
                                            }}
                                        />
                                    </a>
                                );
                            })}
                        </Carousel>
                        :
                        ''
                }

                <WhiteSpace />
                <List>
                    <Item
                        thumb={<img src='https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1385781871,3367469142&fm=26&gp=0.jpg' style={{ height: "50px", width: '50px', borderRadius: '50%' }} />}
                        onClick={() => { }}
                        extra={<Icon className='item-icon-size' type="compass" theme="filled" />}
                    >
                        <span>{info.elder_name}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{info.sex}</span>
                        <span style={{ marginLeft: '10px', fontSize: '14px' }}>{this.getNow(info.date_birth)}周岁</span>

                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<span>紧急联系人</span>}
                        multipleLine={true}
                        extra={<a href={info.emergency_contact ? "tel:" + info.emergency_contact[0].personnel_info.telephone : ""}><Icon className='item-icon-size' type="phone" theme="filled" /></a>}
                    >
                        <Brief>
                            {
                                info.emergency_contact ? info.emergency_contact[0].name : ''
                            }
                        </Brief>
                    </Item>
                </List>

                <WhiteSpace />
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title={info.demand_title}
                        extra={<span>{info.price_type}<span style={{ color: 'orange' }}>￥{info.price}</span></span>}
                    />
                    <Card.Body>
                        <List>
                            <Item>
                                <Brief>服务类型：{info.service_type && info.service_type.length > 0 ? info.service_type[0].name : ''}</Brief>
                            </Item>
                            <Item
                                thumb={<span>预约时间：</span>}
                            >
                                <Brief>{info.start_date}</Brief>
                                <Brief>{info.end_date}</Brief>
                            </Item>
                        </List>
                        <Row
                            type='flex'
                            justify='center'
                        >
                            <span style={{ fontSize: '16px' }}>需求说明</span>
                        </Row>
                        <div style={{ padding: '20px' }}>
                            {info.demand_explain}
                        </div>
                    </Card.Body>
                </Card>
                <div className="confirmation-orders-buttom info-list">
                    <div
                        style={{
                            position: 'absolute',
                            height: '50px',
                            left: '0px',
                            right: '80px',
                            paddingLeft: '10px',
                        }}
                    >
                        <Input onChange={this.commontChage} value={this.state.bidding_price} type='number' placeholder='请输入竞单价格' style={{ width: '100%' }} />
                    </div>
                    <Button onClick={this.sub} style={{ float: 'right', marginTop: '10px', marginRight: "10px" }}>竞单</Button>
                </div>
            </div>
        );
        return (dom);
    }
}

/**
 * 组件：服务商需求详情视图控件
 * 控制服务商需求详情视图控件
 */
@addon('DemandDetailsView', '服务商需求详情视图控件', '控制服务商需求详情视图控件')
@reactControl(DemandDetailsServicerView, true)
export class DemandDetailsServicerViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}