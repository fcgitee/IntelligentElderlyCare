import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List } from 'antd-mobile';
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
const Item = List.Item;
/**
 * 组件：设备详情视图控件状态
 */
export interface deviceDetailViewState extends ReactViewState {
    a?:any;
}
/**
 * 组件：设备详情视图控件
 */ 
export class deviceDetailView extends ReactView<deviceDetailViewControl, deviceDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
        // console.log(this.props.match!.params.key);
        // // 通过id获取用户的详细信息
        // request(this, AppServiceUtility.user_service.get_user_by_id!(this.props.match!.params.key))
        // .then((data: any) => {
        //     // console.log("用户",data);
        // });
    }
    render() {
        return (
            <div>
                <List>
                    <Item extra="8000866114020516482" arrow="horizontal" onClick={() => {}}>设备编号</Item>
                    <Item extra="2019-09-21" onClick={() => {}}>初次修改时间</Item>
                    <Item extra="2019-09-24" onClick={() => {}}>最近修改时间</Item>
                </List>
            </div>
        );
    }
}

/**
 * 组件：设备详情视图控件
 * 控制设备详情视图控件
 */
@addon('deviceDetailView', '设备详情视图控件', '控制设备详情视图控件')
@reactControl(deviceDetailView, true)
export class deviceDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}