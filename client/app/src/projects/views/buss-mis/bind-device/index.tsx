import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, WhiteSpace, } from 'antd-mobile';
import { Form, Button } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：绑定设备视图控件状态
 */
export interface BindDeviceViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;

}
/**
 * 组件：绑定设备视图控件
 */
export class BindDeviceView extends ReactView<BindDeviceViewControl, BindDeviceViewState> {
    constructor(props: any) {
        super(props);
    }
    componentDidMount() {
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form, } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (values.imei) {
                AppServiceUtility.person_org_manage_service.get_current_user_info!()!
                    .then(data => {
                        AppServiceUtility.device_service.update_device!({ 'user_id': data![0].id, 'imei': values.imei })!
                            .then(datas => {
                                this.props.history!.push(ROUTE_PATH.mine);
                            })
                            .catch(err => {
                                console.info(err);
                            });
                    })
                    .catch(err => {
                        console.info(err);
                    });

            }
        });
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <WhiteSpace />
                    <List>
                        <InputItem  {...getFieldProps('imei')} placeholder="请输入设备编号">设备编号</InputItem>
                        <List.Item >
                            <Button style={{ width: '100%' }} type="primary" htmlType='submit'>保存</Button>
                        </List.Item>
                        <WhiteSpace />
                    </List>
                </Form>
            </div >
        );
    }
}

/**
 * 组件：绑定设备视图控件
 * 控制绑定设备视图控件
 */
@addon('BindDeviceView', '绑定设备视图控件', '控制绑定设备视图控件')
@reactControl(Form.create<any>()(BindDeviceView), true)
export class BindDeviceViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}