import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { SwipeAction, List, Button } from 'antd-mobile';
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：设备列表视图控件状态
 */
export interface DeviceListViewState extends ReactViewState {
    /*设备编号 */
    imei?: any;
    /*设备 */
    device_list?: any;
}
/**
 * 组件：设备列表视图控件
 */
export class DeviceListView extends ReactView<DeviceListViewControl, DeviceListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            imei: '',
            device_list: []
        };
    }
    componentDidMount() {
        // // 获取当前用户，拿到用户id，然后根据id查找设备数据
        // request(this, AppServiceUtility.person_org_manage_service.get_current_user_info!())
        // .then((data: any) => {
        //     // console.log("用户",data);
        // });
        request(this, AppServiceUtility.device_service.get_device!({ user_id: '0ab6b488-cfd9-11e9-a909-144f8aec0be5' }, 1, 999))
            .then((data: any) => {
                // console.log("设备", data);
                if (data.result.length > 0) {
                    this.setState({
                        device_list: data.result
                    });
                }
            });
    }
    delectImei = (id: any) => {
        // console.log(id);
        request(this, AppServiceUtility.device_service.delete_device!([id]))
            .then((data: any) => {
                // console.log(data);
            });
    }
    render() {
        let device = this.state.device_list!.map((key: any) => {
            return (
                <SwipeAction
                    key={key.id}
                    style={{ backgroundColor: 'gray' }}
                    autoClose={true}
                    right={[
                        {
                            text: '取消',
                            style: { backgroundColor: '#ddd', color: 'white' },
                        },
                        {
                            text: '删除',
                            onPress: () => this.delectImei(key.id),
                            style: { backgroundColor: '#F4333C', color: 'white' },
                        },
                    ]}
                >
                    <List.Item
                        arrow="horizontal"
                        onClick={() => { this.props.history!.push(ROUTE_PATH.deviceDetail + '/' + key.id); }}
                    >
                        {key.imei}
                    </List.Item>
                </SwipeAction>);
        });
        return (
            <div style={{ height: document.body.clientHeight }}>
                <List renderHeader={() => '最近设备列表：你可以删除列表中的设备，删除后再次使用该设备需要重新绑定。'}>
                    {/* 拿到的数据，遍历这一块的SwipeAction，把id传给onPress*/}
                    {device}
                    <List.Item style={{ position: "fixed", bottom: 0, width: "100%" }}>
                        <Button type="primary" onClick={() => { this.props.history!.push(ROUTE_PATH.bindDevice); }}>新增设备</Button>
                    </List.Item>
                </List>
            </div>
        );
    }
}

/**
 * 组件：设备列表视图控件
 * 控制设备列表视图控件
 */
@addon('DeviceListView', '设备列表视图控件', '控制设备列表视图控件')
@reactControl(DeviceListView, true)
export class DeviceListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}