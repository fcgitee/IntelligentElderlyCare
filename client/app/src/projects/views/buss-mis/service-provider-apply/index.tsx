import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, WhiteSpace, Picker, TextareaItem, Button, Toast } from 'antd-mobile';
import { Form } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
// import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from "src/projects/app/util-tool";

// const alert = Modal.alert;

/**
 * 组件：服务人员申请页面视图控件状态
 */
export interface SerViceProviderapplyViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 服务人员能力级别 */
    servicer_ability_level?: any;
    sp_ability_level?: any;
    /** 服务人员信用级别 */
    servicer_credit_level?: any[];
    sp_credit_level?: any;
    /** 资质(资格证)照片 */
    business_photo?: any;
    /**  */
    business_license_url?: any;
    name?: any;
    address?: any;
    telephone?: any;
    lon?: any;
    lat?: any;
    sp_contract_clauses?: any;
    introduction?: any;
    organization_list?: any;
    super_org_id?: any;
    legal_person?: any;
    organization_nature?: any;
    organization_nature_list?: any;
    areaList?: any;
    admin_area_id?: any;
    sp_qualifications?: any;
    remark?: any;
}
/**
 * 组件：服务人员申请页面视图控件
 */
export class SerViceProviderapplyView extends ReactView<SerViceProviderapplyViewControl, SerViceProviderapplyViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            servicer_ability_level: [{ label: '一级', value: '一级' }, { label: '二级', value: '二级' }, { label: '三级', value: '三级' },],
            servicer_credit_level: [{ label: '一级', value: '一级' }, { label: '二级', value: '二级' }, { label: '三级', value: '三级' },],
            organization_nature_list: [{ label: '公办', value: '公办' }, { label: '公办民营', value: '公办民营' }, { label: '民办企业', value: '民办企业' }, { label: '民办非营利', value: '民办非营利' }],
            organization_nature: '',
            sp_ability_level: '',
            sp_credit_level: '',
            business_photo: '',
            business_license_url: '',
            name: '',
            address: '',
            legal_person: '',
            telephone: '',
            lon: '',
            lat: '',
            sp_contract_clauses: '',
            introduction: '',
            organization_list: [],
            super_org_id: '',
            areaList: [],
            admin_area_id: '',
            sp_qualifications: '',
            remark: ''
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_list_all!({ personnel_type: '2' })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let list: any = [];
                    data.result.map((item: any) => {
                        list.push({ label: item.name, value: item.id });
                    });
                    this.setState({
                        organization_list: list
                    });
                }
            });
        AppServiceUtility.business_area_service.get_all_admin_division_list!({})!
            .then((data: any) => {
                // console.log("区域>>>>>>>>>>>", data);
                data.result.map((item: any) => {
                    let areaList = this.state.areaList;
                    areaList.push({ label: item.name, value: item.id });
                    this.setState({
                        areaList
                    });
                });
                // console.log(this.state.areaList);
            });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { sp_ability_level, sp_credit_level, organization_nature, legal_person, name, address, telephone, admin_area_id, sp_qualifications,
            remark, lon, lat, sp_contract_clauses, introduction, business_photo, business_license_url, super_org_id } = this.state;
        if (!name) {
            Toast.fail('请输入你的名称', 1);
            return false;
        }
        if (!address) {
            Toast.fail('请输入你的地址', 1);
            return false;
        }
        if (!introduction) {
            Toast.fail('请输入服务商简介', 1);
            return false;
        }
        if (!legal_person) {
            Toast.fail('请输入法人', 1);
            return false;
        }
        if (!telephone) {
            Toast.fail('请输入你的联系电话', 1);
            return false;
        }
        if (!organization_nature) {
            Toast.fail('请选择组织机构性质', 1);
            return false;
        }
        if (!admin_area_id) {
            Toast.fail('请选择所属行政区划', 1);
            return false;
        }
        if (!super_org_id) {
            Toast.fail('请选择上级组织机构', 1);
            return false;
        }
        if (!sp_ability_level) {
            Toast.fail('请选择服务商能力级别', 1);
            return false;
        }
        if (!sp_credit_level) {
            Toast.fail('请选择服务商信用级别', 1);
            return false;
        }
        if (!business_photo) {
            Toast.fail('请上传服务商相关图片', 1);
            return false;
        }
        if (!business_license_url) {
            Toast.fail('请上传营业执照', 1);
            return false;
        }
        if (!sp_qualifications) {
            Toast.fail('请上传服务商资质相关图片', 1);
            return false;
        }
        if (!sp_contract_clauses) {
            Toast.fail('请输入服务机构合同条款', 1);
            return false;
        }
        var values = {
            sp_ability_level: sp_ability_level[0],
            sp_credit_level: sp_credit_level[0],
            name,
            address,
            telephone,
            lon,
            lat,
            remark,
            organization_nature,
            sp_qualifications,
            admin_area_id,
            personnel_category: '服务商',
            legal_person,
            sp_contract_clauses,
            introduction,
            business_photo,
            business_license_url,
            super_org_id: super_org_id[0]
        };
        // console.log(values);
        AppServiceUtility.service_operation_service.update_service_provider!(values)!
            .then((data: any) => {
                // if (data === '该用户已经申请过了，不可重复申请') {
                //     alert('提示', '该用户已经申请过了，不可重复申请', [
                //         { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                //     ]);
                // }
                // if (data === '该用户不存在，请先注册') {
                //     alert('提示', '该用户不存在，请先注册', [
                //         { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                //     ]);
                // }
                // if (data === 'Success') {
                //     alert('成功', '申请成功，请耐心等待审核结果', [
                //         { text: '确认', onPress: () => this.props.history!.push(ROUTE_PATH.homeCare) },
                //     ]);
                // }
                // console.log(data);
            })
            .catch(err => {
                console.info(err);
            });
        return false;
    }
    // 名称
    nameChange = (value: any) => {
        this.setState({
            name: value
        });
    }

    // 法人
    legalChange = (value: any) => {
        this.setState({
            legal_person: value
        });
    }
    // 身份证
    addressChange = (value: any) => {
        this.setState({
            address: value
        });
    }

    // 手机号码
    phoneChange = (value: any) => {
        this.setState({
            telephone: value
        });
    }

    // 经度
    lonChange = (value: any) => {
        this.setState({
            lon: value
        });
    }

    // 纬度
    latChange = (value: any) => {
        this.setState({
            lat: value
        });
    }

    // 上级组织机构
    organizationChange = (value: any) => {
        this.setState({
            super_org_id: value
        });
    }

    // 组织机构性质
    organizationNatureChange = (value: any) => {
        this.setState({
            organization_nature: value
        });
    }

    // 能力级别
    abilityChange = (e: any) => {
        // // console.log(e);
        this.setState({
            sp_ability_level: e
        });
    }

    // 信用级别

    creditChange = (e: any) => {
        this.setState({
            sp_credit_level: e
        });
    }
    // 服务商资质照片
    qualificationsChange = (e: any) => {
        // console.log(e);
        this.setState({
            sp_qualifications: e
        });
    }

    // 营业执照
    contractChange = (e: any) => {
        // console.log(e);
        this.setState({
            business_license_url: e
        });
    }

    // 条款
    clausesChange = (value: any) => {
        this.setState({
            sp_contract_clauses: value
        });
    }

    // 简介
    introductionChange = (value: any) => {
        this.setState({
            introduction: value
        });
    }

    // 备注
    remarkChange = (value: any) => {
        this.setState({
            remark: value
        });
    }

    // 上级行政区划
    areaChange = (value: any) => {
        this.setState({
            admin_area_id: value
        });
    }

    // 服务商相关图片
    photoChange = (value: any) => {
        this.setState({
            business_photo: value
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { sp_ability_level, sp_credit_level, name, remark, admin_area_id, address, areaList, organization_nature_list, organization_nature, telephone, lon, lat, sp_contract_clauses, introduction, organization_list, super_org_id, legal_person } = this.state;
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <WhiteSpace />
                    <List renderHeader={() => '申请人信息'}>
                        <InputItem {...getFieldDecorator('name')} clear={true} placeholder="请填服务商名称" onChange={this.nameChange} value={name}> 名称 </InputItem>
                        <InputItem {...getFieldDecorator('address')} clear={true} placeholder="请填写服务商的地址" onChange={this.addressChange} value={address}> 地址 </InputItem>
                        <TextareaItem
                            {...getFieldDecorator('introduction')}
                            title="简介"
                            placeholder="请填写服务简介"
                            rows={3}
                            onChange={this.introductionChange}
                            value={introduction}
                        />
                        <InputItem {...getFieldDecorator('lon')} clear={true} placeholder="请填写经度" onChange={this.lonChange} value={lon}> 经度 </InputItem>
                        <InputItem {...getFieldDecorator('lat')} clear={true} placeholder="请填写纬度" onChange={this.latChange} value={lat}> 纬度 </InputItem>
                        <InputItem {...getFieldDecorator('legal_person')} clear={true} placeholder="请填写法人" onChange={this.legalChange} value={legal_person}> 法人 </InputItem>
                        <InputItem {...getFieldDecorator('telephone')} clear={true} placeholder="请填写联系电话" onChange={this.phoneChange} value={telephone}> 联系电话 </InputItem>
                    </List>
                    <List renderHeader={() => '行政区划'}>
                        <Picker
                            {...getFieldDecorator('organization_nature')}
                            data={organization_nature_list ? organization_nature_list : []}
                            cols={1}
                            onChange={this.organizationNatureChange}
                            value={organization_nature}
                        >
                            <List.Item arrow="horizontal">组织机构性质</List.Item>
                        </Picker>
                        <Picker
                            {...getFieldDecorator('admin_area_id')}
                            data={areaList ? areaList : []}
                            cols={1}
                            onChange={this.areaChange}
                            value={admin_area_id}
                        >
                            <List.Item arrow="horizontal">行政区划</List.Item>
                        </Picker>
                        <Picker
                            {...getFieldDecorator('super_org_id')}
                            data={organization_list ? organization_list : []}
                            cols={1}
                            onChange={this.organizationChange}
                            value={super_org_id}
                        >
                            <List.Item arrow="horizontal">上级组织机构</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => '服务商级别'}>
                        <Picker
                            {...getFieldDecorator('sp_ability_level')}
                            data={this.state.servicer_ability_level ? this.state.servicer_ability_level : []}
                            cols={1}
                            onChange={this.abilityChange}
                            value={sp_ability_level}
                        >
                            <List.Item arrow="horizontal">服务商能力级别</List.Item>
                        </Picker>
                        <Picker
                            {...getFieldDecorator('sp_credit_level')}
                            data={this.state.servicer_credit_level ? this.state.servicer_credit_level : []}
                            cols={1}
                            onChange={this.creditChange}
                            value={sp_credit_level}
                        >
                            <List.Item arrow="horizontal">服务商信用级别</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => '服务商相关图片（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('business_photo', {
                            initialValue: this.state.business_photo,
                            rules: [{
                                required: false,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.photoChange} />
                        )}
                    </List>
                    <List renderHeader={() => '营业执照（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('business_license_url', {
                            initialValue: this.state.business_license_url,
                            rules: [{
                                required: false,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.contractChange} />
                        )}
                    </List>
                    <List renderHeader={() => '服务商资质信息（大小小于2M，格式支持jpg/jpeg/png）'}>
                        {getFieldDecorator('sp_qualifications', {
                            initialValue: this.state.sp_qualifications,
                            rules: [{
                                required: false,
                            }],
                        })(
                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} onChange={this.qualificationsChange} />
                        )}
                    </List>
                    <List renderHeader={() => '服务机构合同条款'}>
                        <TextareaItem
                            {...getFieldDecorator('sp_contract_clauses')}
                            title="合同条款"
                            placeholder="请输入服务机构合同条款"
                            rows={3}
                            onChange={this.clausesChange}
                            value={sp_contract_clauses}
                        />
                    </List>
                    <List renderHeader={() => '备注'}>
                        <TextareaItem
                            {...getFieldDecorator('introduction')}
                            title="备注"
                            placeholder="请输入备注"
                            rows={3}
                            onChange={this.remarkChange}
                            value={remark}
                        />
                    </List>
                    <Button type="primary" style={{ position: 'fixed', bottom: '0px', width: '100%' }} onClick={this.handleSubmit}>提交申请</Button>
                </Form>
            </div >
        );
    }
}

/**
 * 组件：服务人员申请页面视图控件
 * 控制服务人员申请页面视图控件
 */
@addon('SerViceProviderapplyView', '服务人员申请页面视图控件', '控制服务人员申请页面视图控件')
@reactControl(Form.create<any>()(SerViceProviderapplyView), true)
export class SerViceProviderapplyViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}