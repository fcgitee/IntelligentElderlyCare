import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { FontIcon } from "src/projects/components/icon";
import { ROUTE_PATH, forceCheckIsLogin, getAppPermissonObject } from "src/projects/app/util-tool";
import { Grid } from "antd-mobile";
import { LOCAL_FUNCTION_LIST } from "src/business/mainForm/backstageManageMainForm";
/**
 * 组件：服务需求视图状态
 */
export interface DemandViewState extends ReactViewState {
    icon_data?: any[];

}

/**
 * 组件：服务需求视图
 * 服务需求视图
 */
export class DemandView extends ReactView<DemandViewControl, DemandViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            icon_data: [],
        };
    }
    componentWillMount() {
        let icon: any[] = [];
        const func_list_str = localStorage.getItem(LOCAL_FUNCTION_LIST);
        if (getAppPermissonObject(JSON.parse(func_list_str!), '需求发布', '查询')) {
            icon.push({ text: '需求发布', icon: <FontIcon type="iconziyuan32" style={{ fontSize: '40px' }} /> });
        }
        if (getAppPermissonObject(JSON.parse(func_list_str!), '我的需求', '查询')) {
            icon.push({ text: '我的需求', icon: <FontIcon type="iconziyuan7" style={{ fontSize: '40px' }} /> });
        }
        if (getAppPermissonObject(JSON.parse(func_list_str!), '服务需求', '查询')) {
            icon.push({ text: '服务需求', icon: <FontIcon type="iconziyuan31" style={{ fontSize: '40px' }} /> });
        }
        this.setState({
            icon_data: icon,
        });
    }
    grid_click = (_el: any) => {
        switch (_el.text) {
            case '需求发布':
                forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.familyList, params: { url: ROUTE_PATH.demandRelease, key: this.props.match!.params.key } }, { backPath: ROUTE_PATH.mine });
                break;
            case '我的需求':
                forceCheckIsLogin(this.props, { successPath: ROUTE_PATH.demandList }, { backPath: ROUTE_PATH.mine });
                break;
            case '服务需求':
                this.props.history!.push(ROUTE_PATH.demandListServicer); 
                break;
            default:
                break;
        }
        // this.props.history!.push(ROUTE_PATH.serviceProductList + '/' + _el.id);
    }
    render() {
        setMainFormTitle("服务需求");
        return (
            <div>
                <Grid data={this.state.icon_data} activeStyle={true} hasLine={false} columnNum={3} onClick={_el => this.grid_click(_el)} />
            </div>
        );
    }
}

/**
 * 控件：服务需求视图控制器
 * 服务需求视图
 */
@addon('DemandView', '服务需求视图', '服务需求视图')
@reactControl(DemandView, true)
export class DemandViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}