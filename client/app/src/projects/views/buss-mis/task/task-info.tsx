import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Button, Modal, Toast, TextareaItem, ImagePicker } from "antd-mobile";
import { Row } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

const prompt = Modal.prompt;

const taskState = {
    to_be_processed: '待处理',
    failed_examine: '审核未通过',
    adopt_examine: '审核通过',// 待分派
    to_be_receive: '待接收',
    ongoing: '进行中',
    rejected: '已拒绝',
    completed: '已完成',
    evaluated: '已评价',
};

/**
 * 组件：任务详情页面状态
 */
export interface TaskInfoViewState extends BaseReactElementState {
    // 任务详情
    task_info: any;
    // 防止重复发送
    isSend: boolean;
    // 文件列表？
    files?: any;
    // 是否多选
    multiple?: boolean;
    // 文字记录
    task_text_record?: string;
    // 图片记录
    task_image_record?: string;
}

/**
 * 组件：任务详情页面
 * 任务详情
 */
export class TaskInfoView extends BaseReactElement<TaskInfoViewControl, TaskInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            isSend: false,
            files: [],
            multiple: true,
            task_info: {
                // 任务名字
                task_name: '',
                // 任务内容
                task_content: '',
                // 紧急程度
                task_urgent: '',
                // 任务类型
                task_type: '',
                // 任务地址
                task_address: '',
                // 任务状态
                task_state: '',
                // 地理位置
                task_location: '',
                // 备注
                remark: '',
                // 创建时间
                create_date: '',
                // 分派人员
                task_receive_person: '',
                // 审核不通过或者拒绝的原因
                task_fail_reason: '',
            }
        };
    }
    // 拒绝任务
    cancelReason = () => {
        prompt(
            '请输入拒绝原因',
            '',
            [
                { text: '取消' },
                {
                    text: '确认', onPress: (value) => {
                        if (value === '') {
                            Toast.fail('请输入拒绝原因', 2);
                            return;
                        }
                        Toast.loading('Loading...', 10);
                        AppServiceUtility.task_service.task_receive_failed!({ id: this.props.match!.params.key, task_fail_reason: value })!
                            .then((datas: any) => {
                                Toast.hide();
                                if (datas === 'Success') {
                                    Toast.success('操作成功', 3);
                                    setTimeout(
                                        () => {
                                            this.backList();
                                        },
                                        3000,
                                    );
                                } else {
                                    Toast.fail('操作失败', 3);
                                }
                            });
                    }
                },
            ],
        );
    }
    // 接受任务
    accept = () => {
        Toast.loading('Loading...', 10);
        AppServiceUtility.task_service.task_receive_passed!({ id: this.props.match!.params.key })!
            .then((datas: any) => {
                Toast.hide();
                if (datas === 'Success') {
                    Toast.success('操作成功', 3);
                    this.setState({
                        isSend: false,
                    });
                    setTimeout(
                        () => {
                            Toast.loading('Loading...', 1);
                            this.reloadFunc();
                        },
                        3000,
                    );
                } else {
                    Toast.fail('操作失败', 3);
                }
            });
    }
    // 完成任务
    complete = () => {
        Toast.loading('Loading...', 10);
        AppServiceUtility.task_service.task_complete!({ id: this.props.match!.params.key, task_text_record: this.state.task_text_record })!
            .then((datas: any) => {
                Toast.hide();
                if (datas === 'Success') {
                    Toast.success('操作成功', 3);
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.taskFinish);
                        },
                        3000,
                    );
                } else {
                    Toast.fail('操作失败', 3);
                }
            });
    }
    componentDidMount() {
        if (this.state.isSend) {
            return;
        }
        Toast.loading('Loading...', 1);
        this.reloadFunc();
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.taskList);
    }
    reloadFunc = () => {
        // // console.log(this.props);
        AppServiceUtility.task_service.get_all_task_list!({ id: this.props.match!.params.key }, 1, 1)!
            .then((datas: any) => {
                Toast.hide();
                let result = datas.result[0];
                if (result.task_type_info.length > 0) {
                    result.task_type = result.task_type_info[0]['name'];
                }
                if (result.receiver_info && result.receiver_info.length > 0) {
                    result.task_receive_person = result.receiver_info[0]['name'];
                }
                if (result.task_state === taskState.failed_examine) {
                    result.task_fail_reason = result.task_fail_reason || '暂无';
                }
                if (!result.remark) {
                    result.remark = '无';
                }
                // console.log(result);
                this.setState({
                    isSend: true,
                    task_info: result,
                });
            });
    }
    changeValue = (e: any, inputName: string) => {
        this.setState({
            task_text_record: e
        });
    }
    onChange = (files: any, type: any, index: any) => {
        // console.log(files);
        this.setState({
            files,
        });
    }
    onSegChange = (e: any) => {
        const index = e.nativeEvent.selectedSegmentIndex;
        this.setState({
            multiple: index === 1,
        });
    }
    render() {
        const { files } = this.state;
        return (
            <Card>
                <WingBlank size="sm">
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务名称</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_name}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务内容</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_content}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务类型</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_type}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务紧急程度</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_urgent}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务地址</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_address}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务地理位置</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_location}
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务备注</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.remark}
                    </Row>
                    {(() => {
                        if (this.state.task_info.task_receive_person) {
                            return <div>
                                <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>分派人员</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <Row>
                                    {this.state.task_info.task_receive_person}
                                </Row>
                            </div>;
                        }
                        return null;
                    })()}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务状态</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.task_state}
                    </Row>
                    {(() => {
                        if (this.state.task_info.task_state === taskState.failed_examine) {
                            return <div>
                                <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>不通过原因</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <Row>
                                    {this.state.task_info.task_fail_reason}
                                </Row>
                            </div>;
                        }
                        return null;
                    })()}
                    {(() => {
                        if (this.state.task_info.task_state === taskState.rejected) {
                            return <div>
                                <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>拒绝原因</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <Row>
                                    {this.state.task_info.task_fail_reason}
                                </Row>
                            </div>;
                        }
                        return null;
                    })()}
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务创建时间</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        {this.state.task_info.create_date}
                    </Row>
                    {(() => {
                        if (this.state.task_info.task_state === taskState.to_be_receive) {
                            return <div>
                                <WhiteSpace size="xl" />
                                <Button type="primary" onClick={this.accept}>接受</Button>
                                <WhiteSpace size='md' />
                                <Button type="warning" onClick={this.cancelReason}>拒绝</Button>
                                <WhiteSpace size='md' />
                            </div>;
                        }
                        return null;
                    })()}
                    {(() => {
                        if (this.state.task_info.task_state === taskState.ongoing) {
                            return <div>
                                <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>文字记录</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <TextareaItem
                                    placeholder="请输入文字记录"
                                    data-seed="logId"
                                    autoHeight={true}
                                    rows={3}
                                    onChange={(e) => this.changeValue(e, 'task_text_record')}
                                />
                                <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>上传图片记录</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <ImagePicker
                                    files={files}
                                    onChange={this.onChange}
                                    onImageClick={(index, fs) => { }}
                                    selectable={this.state.files.length < 7}
                                    multiple={this.state.multiple}
                                />
                                {/* <WhiteSpace size="xl" />
                                <Row type='flex' justify='start'>
                                    <strong>上传视频记录</strong>
                                </Row>
                                <WhiteSpace size="xs" />
                                <ImagePicker
                                    files={files}
                                    onChange={this.onChange}
                                    onImageClick={(index, fs) => // console.log(index, fs)}
                                    multiple={this.state.multiple}
                                    selectable={this.state.files.length < 5}
                                    accept="image/gif,image/jpeg,image/jpg,image/png"
                                /> */}
                                <WhiteSpace size="xl" />
                                <Button type="primary" onClick={this.complete}>完成任务</Button>
                                <WhiteSpace size='md' />
                            </div>;
                        }
                        return null;
                    })()}
                    <WhiteSpace size="xl" />
                    <Button onClick={this.backList}>返回</Button>
                    <WhiteSpace size='md' />
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：任务详情页面控制器
 * 任务详情
 */
@addon('TaskInfoView', '任务详情页面', '任务详情')
@reactControl(TaskInfoView, true)
export class TaskInfoViewControl extends BaseReactElementControl {

}