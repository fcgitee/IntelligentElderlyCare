import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Result, Icon, Button } from "antd-mobile";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：任务完成页面状态
 */
export interface TaskFinishViewState extends BaseReactElementState {
}

/**
 * 组件：任务完成页面
 * 任务完成
 */
export class TaskFinishView extends BaseReactElement<TaskFinishViewControl, TaskFinishViewState> {
    backList = () => {
        this.props.history!.push(ROUTE_PATH.taskList);
    }
    render() {
        const cssObj = {
            width: '60px', height: '60px', fill: '#1F90E6'
        };
        return (
            <Card>
                <WingBlank size="sm">
                    <Result
                        img={<Icon
                            type="check-circle"
                            style={cssObj}
                        />}
                        title="任务完成"
                        message="任务内容已经提交完成"
                    />
                    <Button icon="left" onClick={this.backList}>返回列表</Button>
                    <WhiteSpace size="xl" />
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：任务完成页面控制器
 * 任务完成
 */
@addon('TaskFinishView', '任务完成页面', '任务完成')
@reactControl(TaskFinishView, true)
export class TaskFinishViewControl extends BaseReactElementControl {

}