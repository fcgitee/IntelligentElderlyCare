import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, WhiteSpace, Card, Button, TextareaItem, ImagePicker } from "antd-mobile";
import { Row } from "antd";

/**
 * 组件：任务状态页面状态
 */
export interface TaskStatusViewState extends BaseReactElementState {
    files?: any;

    multiple?: boolean;
}

/**
 * 组件：任务状态页面
 * 任务状态
 */
export class TaskStatusView extends BaseReactElement<TaskStatusViewControl, TaskStatusViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            files: [],
            multiple: true
        };
    }
    onChange = (files: any, type: any, index: any) => {
        this.setState({
            files,
        });
    }
    onSegChange = (e: any) => {
        const index = e.nativeEvent.selectedSegmentIndex;
        this.setState({
            multiple: index === 1,
        });
    }

    render() {
        const { files } = this.state;
        return (
            <Card>
                <WingBlank size="sm">
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务名称</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        清理ABC房间
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务内容</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        在2020年12月之前清理ABC房间，在2020年12月之前清理ABC房间，在2020年12月之前清理ABC房间，在2020年12月之前清理ABC房间，在2020年12月之前清理ABC房间。
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务地址</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        广东省佛山市xx路11号
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务地理位置</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        广东省佛山市xx路11号
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>任务备注</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        任务备注
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>分派时间</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <Row>
                        2019-08-17 12:00:00
                    </Row>
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>文字记录</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <TextareaItem
                        placeholder="请输入文字记录"
                        data-seed="logId"
                        autoHeight={true}
                        rows={3}
                    />
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>上传图片记录</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <ImagePicker
                        files={files}
                        onChange={this.onChange}
                        onImageClick={(index, fs) => { }}
                        selectable={this.state.files.length < 7}
                        multiple={this.state.multiple}
                    />
                    <WhiteSpace size="xl" />
                    <Row type='flex' justify='start'>
                        <strong>上传视频记录</strong>
                    </Row>
                    <WhiteSpace size="xs" />
                    <ImagePicker
                        files={files}
                        onChange={this.onChange}
                        onImageClick={(index, fs) => { }}
                        multiple={this.state.multiple}
                        selectable={this.state.files.length < 5}
                        accept="image/gif,image/jpeg,image/jpg,image/png"
                    />
                    <WhiteSpace size="xl" />
                    <Button type="primary">完成任务</Button>
                    <WhiteSpace size='md' />
                </WingBlank>
            </Card>
        );
    }
}

/**
 * 控件：任务状态页面控制器
 * 任务状态
 */
@addon('TaskStatusView', '任务状态页面', '任务状态')
@reactControl(TaskStatusView)
export class TaskStatusViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}