import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Badge, Tabs, Toast } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：任务列表状态
 */
export interface TaskListViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：任务列表
 * 任务列表
 */
export class TaskListView extends ReactView<TaskListViewControl, TaskListViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.task_service.get_all_task_list!({}, 1, 1)!
            .then((datas: any) => {
                const dataBlob = getDataBlob(datas.result);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            });
    }
    tabChange = (tab: any, index: number) => {
        Toast.loading('Loading...', 30);
        this.setState({
            rData: getDataBlob([]),
            dataList: [],
            dataSource: ds,
        });
        var api;
        switch (index) {
            case 1:
                api = 'get_to_be_processed_task_list';
                break;
            case 2:
                api = 'get_to_be_send_task_list';
                break;
            case 3:
                api = 'get_ongoing_task_list';
                break;
            case 4:
                api = 'get_completed_task_list';
                break;
            default:
                api = 'get_all_task_list';
                break;
        }
        AppServiceUtility.task_service[api]({}, 1, 1)!
            .then((datas: any) => {
                setTimeout(
                    () => {
                        Toast.hide();
                    },
                    500
                );
                const dataBlob = getDataBlob(datas.result);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.taskInfo + '/' + obj.id);
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}><span>{obj.task_name}</span><span>{obj.task_state}</span></div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={{ fontWeight: 'bold' }}>{obj.task_content}</div>
                    </div>
                    <div style={{ textAlign: 'right', paddingBottom: '8px' }}>
                        <div>{obj.create_date}</div>
                    </div>
                </div>
            );
        };
        const tabs = [
            { title: <Badge>全部</Badge> },
            { title: <Badge>待处理</Badge> },
            { title: <Badge>待分派</Badge> },
            { title: <Badge>进行中</Badge> },
            { title: <Badge>待评价</Badge> },
        ];
        return (
            <div>
                <Tabs tabs={tabs} initialPage={0} onChange={this.tabChange} >
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    />
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    />
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    />
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    />
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.dataSource}
                        renderRow={row}
                        renderSeparator={separator}
                        pageSize={4}
                        useBodyScroll={true}
                        onScroll={() => { }}
                        scrollRenderAheadDistance={500}
                    />
                </Tabs>
            </div>
        );
    }
}

/**
 * 控件：任务列表控制器
 * 任务列表
 */
@addon('TaskListView', '任务列表', '任务列表')
@reactControl(TaskListView, true)
export class TaskListViewControl extends ReactViewControl {

}