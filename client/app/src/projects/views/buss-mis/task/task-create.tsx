import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, List, WhiteSpace, InputItem, TextareaItem, Picker, Button, Toast } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：任务创建页面状态
 */
export interface TaskCreateViewState extends BaseReactElementState {
    // 任务名称
    task_name?: string;
    // 任务内容
    task_content?: string;
    // 任务备注
    remark?: string;
    // 任务地址
    task_address?: string;
    // 任务地理位置
    task_location?: string;
    // 任务类型
    task_type_value?: string;
    // 默认任务类型
    task_urgent_value?: string;
    /** 任务类型 */
    task_type?: any;
    /** 任务紧急程度 */
    task_urgent?: any;
    /** 任务类型选择框的显示文字 */
    task_type_show_value?: any;
    /** 任务紧急选择框的显示文字 */
    task_urgent_show_value?: any;
}

/**
 * 组件：任务创建页面
 * 任务创建
 */
export class TaskCreateView extends BaseReactElement<TaskCreateViewControl, TaskCreateViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            task_type_value: '',
            task_urgent_value: '',
            task_name: '',
            task_content: '',
            task_address: '',
            task_location: '',
            remark: '',
            task_type_show_value: '',
            task_urgent_show_value: '',
            task_type: [],
            task_urgent: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.task_service.input_task_type!()!
            .then((datas: any) => {
                this.setState({
                    task_type: datas,
                });
            });
        AppServiceUtility.task_service.input_task_urgent!()!
            .then((datas: any) => {
                this.setState({
                    task_urgent: datas,
                });
            });
    }
    changeValue = (e: any, inputName: string) => {
        this.setState({
            [inputName]: e
        });
    }
    changeUrgent = (e: any) => {
        this.setState({
            task_urgent_value: e[0],
            task_urgent_show_value: e,
        });
    }
    changeType = (e: any) => {
        this.setState({
            task_type_value: e[0],
            task_type_show_value: e,
        });
    }
    taskCreate = (): boolean => {
        if (!this.state.task_name) {
            Toast.fail('请输入任务名称', 1);
            return false;
        }
        if (!this.state.task_content) {
            Toast.fail('请输入任务内容', 1);
            return false;
        }
        if (!this.state.task_type_value) {
            Toast.fail('请选择任务类型', 1);
            return false;
        }
        if (!this.state.task_urgent_value) {
            Toast.fail('请选择任务紧急类型', 1);
            return false;
        }
        if (!this.state.task_address) {
            Toast.fail('请输入任务地址', 1);
            return false;
        }
        if (!this.state.task_location) {
            Toast.fail('请输入任务地理位置', 1);
            return false;
        }

        Toast.loading('Loading...', 3, () => {
            // console.log('Load complete !!!');
        });

        var param: any = {
            task_name: this.state.task_name,
            task_content: this.state.task_content,
            task_type: this.state.task_type_value,
            task_urgent: this.state.task_urgent_value,
            task_address: this.state.task_address,
            task_location: this.state.task_location,
        };

        if (this.state.remark) {
            param.remark = this.state.remark;
        }
        Toast.loading('Loading...', 11, () => {
        });
        let st = setTimeout(
            () => {
                Toast.hide();
                Toast.fail('操作失败，请稍候再试', 3);
            },
            10000
        );
        AppServiceUtility.task_service.add_new_process_task!(param)!
            .then((data: any) => {
                if (data === 'Success') {
                    clearTimeout(st);
                    Toast.hide();
                    Toast.success('操作成功', 3);
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.taskList);
                        },
                        3000
                    );
                } else {
                    Toast.fail('操作失败，请稍候再试', 3);
                }
            });

        // console.log(param);
        return false;
    }
    render() {
        let type = this.state.task_type;
        let task_type: any[] = [];
        type!.map((item: any) => {
            task_type.push({
                label: item.name,
                value: item.id,
            });
        });
        let urgent = this.state.task_urgent;
        let task_urgent: any[] = [];
        urgent!.map((item: any) => {
            task_urgent.push({
                label: item,
                value: item,
            });
        });
        const
            borderBoth = {
                borderTop: '1px solid #ddd',
                borderBottom: '1px solid #ddd',
            },
            borderBottom = {
                borderBottom: '1px solid #ddd',
            };
        return (
            <div>
                <List renderHeader={() => '任务名称'}>
                    <InputItem clear={true} placeholder="请输入任务名称" onChange={(e) => this.changeValue(e, 'task_name')} />
                </List>
                <List renderHeader={() => '任务内容'}>
                    <TextareaItem placeholder="请输入任务内容" rows={3} autoHeight={true} onChange={(e) => this.changeValue(e, 'task_content')} />
                </List>
                <List renderHeader={() => '任务类型'}>
                    <Picker data={task_type} cols={1} value={this.state.task_type_show_value} onChange={(e) => this.changeType(e)}>
                        <List.Item arrow="horizontal" style={borderBoth}>任务类型</List.Item>
                    </Picker>
                </List>
                <List renderHeader={() => '紧急程度'}>
                    <Picker data={task_urgent} cols={1} value={this.state.task_urgent_show_value} onChange={(e) => this.changeUrgent(e)}>
                        <List.Item arrow="horizontal" style={borderBottom}>紧急程度</List.Item>
                    </Picker>
                </List>
                <List renderHeader={() => '任务地址'}>
                    <InputItem clear={true} placeholder="请输入任务地址" onChange={(e) => this.changeValue(e, 'task_address')} />
                </List>
                <List renderHeader={() => '地理位置'}>
                    <InputItem clear={true} placeholder="请输入地理位置" onChange={(e) => this.changeValue(e, 'task_location')} />
                </List>
                <List renderHeader={() => '任务备注'}>
                    <TextareaItem placeholder="请输入任务备注" rows={3} autoHeight={true} onChange={(e) => this.changeValue(e, 'remark')} />
                </List>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WingBlank size="lg">
                    <Button type="primary" onClick={this.taskCreate}>创建任务</Button>
                </WingBlank>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
            </div>
        );
    }
}

/**
 * 控件：任务创建页面控制器
 * 任务创建
 */
@addon('TaskCreateView', '任务创建页面', '任务创建')
@reactControl(TaskCreateView, true)
export class TaskCreateViewControl extends BaseReactElementControl {

}