import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { WhiteSpace, Button, Modal } from "antd-mobile";
const alert = Modal.alert;
const showAlert = () => {
    alert('温馨提醒', '你有一个新消息，请查看。', [
        { text: '取消', onPress: () => { } },
        { text: '确定', onPress: () => { } },
    ]);
    // setTimeout(() => {
    //     // 可以调用close方法以在外部close
    //     // console.log('auto close');
    //     alertInstance.close();
    // }, 500000);
};
/**
 * 组件：消息提醒状态
 */
export class MessageRemindViewState implements BaseReactElementState {
}

/**
 * 组件：消息提醒
 * 描述
 */
export class MessageRemindView extends BaseReactElement<MessageRemindViewControl, MessageRemindViewState> {
    render() {
        return (
            <div>
                <WhiteSpace size="lg" />
                <Button onClick={showAlert}>提醒</Button>
            </div>
        );
    }
}

/**
 * 控件：消息提醒控制器
 * 描述
 */
@addon('MessageRemindView', '消息提醒', '描述')
@reactControl(MessageRemindView)
export class MessageRemindViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}