import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Icon, Card, SearchBar } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row, Spin } from "antd";

// const Item = List.Item;
// const Brief = Item.Brief;

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

/**
 * 组件：搜索列表状态
 */
export interface SearchListViewState extends BaseReactElementState {
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    type?: any;
    insert?: any;
    show?: any;
}

/**
 * 组件：搜索列表
 * 搜索列表
 */
export class SearchListView extends BaseReactElement<SearchListViewControl, SearchListViewState> {
    autoFocusInst: SearchBar | any;
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            type: '',
            height: document.documentElement.clientHeight,
            page: 1,
            insert: '',
            show: false
        };
    }

    yljg = (name: any, insert: any, page: any, old_list: any) => {
        // 养老机构
        AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: name, name: insert }, page, 11)!
            .then((data: any) => {
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list
                    });
                    // console.log('机构', data);
                }
            });
    }

    xfyhd = (insert: any, page: any, old_list: any) => {
        AppServiceUtility.activity_service.get_activity_list!({ activity_name: insert }, page, 11)!
            .then((data: any) => {
                // console.log('活动', data);
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list
                    });
                }
            });
    }

    fwcp = (insert: any, page: any, old_list: any) => {
        AppServiceUtility.service_detail_service.get_service_product_item_package_list!({ name: insert }, page, 11)!
            .then((data: any) => {
                // console.log('服务产品', data);
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list
                    });
                }
            });
    }

    zx = (insert: any, page: any, old_list: any) => {
        AppServiceUtility.article_list_service.get_news_list!({ title: insert }, page, 11)!
            .then((data: any) => {
                // console.log('新闻？？？？？？？？', data);
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list
                    });
                }
            });
    }

    ylbz = (insert: any, page: any, old_list: any) => {
        AppServiceUtility.allowance_service.get_allowance_project_list_all!({ name: insert }, page, 11)!
            .then((data: any) => {
                // console.log('养老补助', data);
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list
                    });
                }
            });
    }

    csjj = (insert: any, page: any, old_list: any) => {
        AppServiceUtility.charitable_service.get_charitable_title_fund_list_all!({ all: true, 'status': '通过', donate_name: insert }, page, 11)!
            .then((data: any) => {
                // console.log('慈善基金', data);
                let list = [...old_list, ...data.result];
                this.setState({
                    show: false
                });
                if (data.result.length > 0) {
                    this.setState({
                        list,
                    });
                }
            });
    }
    componentDidMount() {
        this.autoFocusInst.focus();
        let type = this.props.match!.params.key.split('&')[0];
        let insert = this.props.match!.params.key.split('&')[1];
        this.setState({
            type: this.props.match!.params.key.split('&')[0],
            insert: this.props.match!.params.key.split('&')[1],
            show: true
        });
        if (type === '养老机构') {
            this.yljg('福利院', insert, 1, []);
        }
        if (type === '社区幸福院') {
            this.yljg('幸福院', insert, 1, []);
        }
        if (type === '幸福院活动') {
            this.xfyhd(insert, 1, []);
        }
        if (type === '居家服务商') {
            this.yljg('服务商', insert, 1, []);
        }
        if (type === '服务产品') {
            this.fwcp(insert, 1, []);
        }
        if (type === '资讯') {
            this.zx(insert, 1, []);
        }
        if (type === '养老补助') {
            this.ylbz(insert, 1, []);
        }
        if (type === '慈善基金') {
            this.csjj(insert, 1, []);
        }
    }

    /** 下拉事件 */
    onEndReached = () => {
        const { type, page, insert, list } = this.state;
        let new_pape = page + 1;
        this.setState({
            page: new_pape
        });
        if (type === '养老机构') {
            this.yljg('福利院', insert, new_pape, list);
        }
        if (type === '社区幸福院') {
            this.yljg('幸福院', insert, new_pape, list);
        }
        if (type === '幸福院活动') {
            this.xfyhd(insert, new_pape, list);
        }
        if (type === '居家服务商') {
            this.yljg('服务商', insert, new_pape, list);
        }
        if (type === '服务产品') {
            this.fwcp(insert, new_pape, list);
        }
        if (type === '资讯') {
            this.zx(insert, new_pape, list);
        }
        if (type === '养老补助') {
            this.ylbz(insert, new_pape, list);
        }
        if (type === '慈善基金') {
            this.csjj(insert, new_pape, list);
        }
    }

    goDetails = (type: any, id: any) => {
        if (type === '养老机构') {
            this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + id);
        }
        if (type === '社区幸福院') {
            this.props!.history!.push(ROUTE_PATH.happinessHomeInfo + '/' + id);
        }
        if (type === '幸福院活动') {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id);
        }
        if (type === '居家服务商') {
            this.props.history!.push(ROUTE_PATH.serverDetail + '/' + id);
        }
        if (type === '服务产品') {
            this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
        }
        if (type === '资讯') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
        if (type === '养老补助') {
            this.props.history!.push(ROUTE_PATH.applicationDetail + '/' + id);
        }
        if (type === '慈善基金') {
            this.props.history!.push(ROUTE_PATH.charitableTitleFundDetail + '/' + id);
        }

    }

    goBack = () => {
        this.props.history!.push(ROUTE_PATH.Search + '/' + this.props.match!.params.key.split('&')[1]);
    }

    insert = (value: any) => {
        this.setState({
            insert: value
        });
    }

    submit = () => {
        let type = this.props.match!.params.key.split('&')[0];
        let insert = this.state.insert;
        this.setState({
            list: [],
            show: true,
            page: 1
        });
        if (insert !== '') {
            if (type === '养老机构') {
                this.yljg('福利院', insert, 1, []);
            }
            if (type === '社区幸福院') {
                this.yljg('幸福院', insert, 1, []);
            }
            if (type === '幸福院活动') {
                this.xfyhd(insert, 1, []);
            }
            if (type === '居家服务商') {
                this.yljg('服务商', insert, 1, []);
            }
            if (type === '服务产品') {
                this.fwcp(insert, 1, []);
            }
            if (type === '资讯') {
                this.zx(insert, 1, []);
            }
            if (type === '养老补助') {
                this.ylbz(insert, 1, []);
            }
            if (type === '慈善基金') {
                this.csjj(insert, 1, []);
            }
        } else {
            this.setState({
                show: false
            });
        }
    }
    render() {
        const { list, dataSource, type, show } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            let name = '';
            let src = '';
            if (type === '养老机构') {
                name = owData.name;
                src = owData.organization_info ? owData.organization_info.picture_list[0] : '';
            }
            if (type === '社区幸福院') {
                name = owData.name;
                src = owData.organization_info ? owData.organization_info.picture_list[0] : '';
            }
            if (type === '幸福院活动') {
                name = owData.activity_name;
                src = owData.photo ? owData.photo[0] : '';
            }
            if (type === '居家服务商') {
                name = owData.name;
                src = owData.organization_info ? owData.organization_info.picture_list[0] : '';
            }
            if (type === '服务产品') {
                name = owData.name;
                src = owData.picture_collection ? owData.picture_collection[0] : '';
            }
            if (type === '资讯') {
                name = owData.title;
                src = owData.app_img_list ? owData.app_img_list[0] : '';
            }
            if (type === '养老补助') {
                name = owData.allowance_project_name;
                src = owData.allowance_project_photo ? owData.allowance_project_photo[0] : '';
            }
            if (type === '慈善基金') {
                name = owData.donate_name;
                src = owData.photo ? owData.photo[0] : '';
            }
            return (
                <Card.Body key={owData} onClick={() => { this.goDetails(type, owData.id); }}>
                    <div style={{ overflow: 'hidden' }}>
                        <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={src} /></div>
                        <div style={{ float: 'left', width: '75%' }}>{name}</div>
                        <div style={{ float: 'right' }}><Icon type="right" /></div>
                    </div>
                </Card.Body>
            );
        };
        return (
            <div>
                <div style={{ overflow: 'hidden', height: '44px', marginBottom: '10px' }}>
                    <div onClick={this.goBack} style={{ float: 'left', width: '10%', backgroundColor: '#efeff4', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='left' />
                    </div>
                    <div style={{ float: 'right', width: '90%', height: '100%' }}>
                        <SearchBar value={this.state.insert} onChange={this.insert} onSubmit={this.submit} ref={ref => this.autoFocusInst = ref} />
                    </div>
                </div>
                {show && show === true ?
                    <div style={{ textAlign: 'center', marginTop: '30px' }}>
                        <Spin size="large" />
                    </div>
                    :
                    ''
                }
                <Row className='tabs-content' >
                    {
                        list && list.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(list)}
                                renderRow={renderRow}
                                initialListSize={11}
                                pageSize={11}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: this.state.height, overflow: 'auto' }}
                                onEndReached={this.onEndReached}
                            />
                            :
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无相关数据
                        </Row>
                    }
                </Row >
            </div>
        );
    }
}

/**
 * 控件：搜索列表控制器
 * 搜索列表
 */
@addon('SearchListView', '搜索列表', '搜索列表')
@reactControl(SearchListView, true)
export class SearchListViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}