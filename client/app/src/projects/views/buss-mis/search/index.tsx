import { Spin } from 'antd';
import { Card, Icon, SearchBar } from 'antd-mobile';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
/**
 * 组件：搜索状态
 */
export interface SearchViewState extends ReactViewState {
    insert_info?: any;
    jg_list?: any;
    jg_show?: any;
    xfy_show?: any;
    xfy_list?: any;
    ylbz_show?: any;
    ylbz_list?: any;
    csjj_show?: any;
    csjj_list?: any;
    hd_show?: any;
    hd_list?: any;
    fws_show?: any;
    fws_list?: any;
    fwcp_show?: any;
    fwcp_list?: any;
    zx_show?: any;
    zx_list?: any;
    first?: any;
    show?: any;
}

/**
 * 组件：搜索
 * 描述
 */
export class SearchView extends ReactView<SearchViewControl, SearchViewState> {
    autoFocusInst: SearchBar | any;
    constructor(props: any) {
        super(props);
        this.state = {
            insert_info: '',
            jg_list: [],
            jg_show: false,
            xfy_show: false,
            xfy_list: [],
            ylbz_show: false,
            ylbz_list: [],
            csjj_show: false,
            csjj_list: [],
            hd_show: false,
            hd_list: [],
            fws_show: false,
            fws_list: [],
            fwcp_show: false,
            fwcp_list: [],
            zx_show: false,
            zx_list: [],
            first: false,
            show: false
        };
    }
    componentDidMount() {
        this.autoFocusInst.focus();
        if (this.props.match!.params.key && this.state.first === false) {
            this.submit(this.props.match!.params.key);
            this.setState({
                insert_info: this.props.match!.params.key
            });
        }
    }
    insert = (value: any) => {
        // console.log(value);
        this.setState({
            insert_info: value,
        });
    }

    submit = (name: any) => {
        // console.log(name);
        this.setState({
            first: true,
            show: true,
            jg_list: [],
            xfy_list: [],
            ylbz_list: [],
            csjj_list: [],
            hd_list: [],
            fws_list: [],
            fwcp_list: [],
            zx_list: [],
            jg_show: false,
            xfy_show: false,
            ylbz_show: false,
            csjj_show: false,
            hd_show: false,
            fws_show: false,
            fwcp_show: false,
            zx_show: false,
        });
        if (name !== '') {
            // 养老机构
            AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '福利院', name }, 1, 3)!
                .then((data: any) => {
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            jg_show: true,
                            jg_list: data.result
                        });
                        // console.log('机构', data);
                    }
                });
            // 社区幸福院
            AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '幸福院', name }, 1, 3)!
                .then((data: any) => {
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            xfy_show: true,
                            xfy_list: data.result,

                        });
                        // console.log('幸福院', data);
                    }
                });
            // 活动
            AppServiceUtility.activity_service.get_activity_list!({ activity_name: name }, 1, 3)!
                .then((data: any) => {
                    // console.log('活动', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            hd_show: true,
                            hd_list: data.result,

                        });
                    }
                });
            // 服务商
            AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '服务商', name }, 1, 3)!
                .then((data: any) => {
                    // console.log('服务商', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            fws_show: true,
                            fws_list: data.result,

                        });
                    }
                });
            // 服务产品
            AppServiceUtility.service_detail_service.get_service_product_item_package_list!({ name }, 1, 3)!
                .then((data: any) => {
                    // console.log('服务产品', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            fwcp_show: true,
                            fwcp_list: data.result,

                        });
                    }
                });
            // 资讯
            AppServiceUtility.article_list_service.get_news_list!({ title: name }, 1, 3)!
                .then((data: any) => {
                    // console.log('新闻？？？？？？？？', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            zx_show: true,
                            zx_list: data.result,

                        });
                    }
                });
            // 养老补助
            AppServiceUtility.allowance_service.get_allowance_project_list_all!({ name }, 1, 3)!
                .then((data: any) => {
                    // console.log('养老补助', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            ylbz_show: true,
                            ylbz_list: data.result,

                        });
                    }
                });
            // 慈善基金
            AppServiceUtility.charitable_service.get_charitable_title_fund_list_all!({ all: true, 'status': '通过', donate_name: name }, 1, 3)!
                .then((data: any) => {
                    // console.log('慈善基金', data);
                    this.setState({
                        show: false
                    });
                    if (data.result.length > 0) {
                        this.setState({
                            csjj_show: true,
                            csjj_list: data.result,

                        });
                    }
                });
        } else {
            this.setState({
                show: false
            });
        }
    }
    goList = (type: any) => {
        this.props.history!.push(ROUTE_PATH.searchList + '/' + type + '&' + this.state.insert_info);
    }

    goDetails = (type: any, id: any) => {
        if (type === '养老机构') {
            this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + id);
        }
        if (type === '社区幸福院') {
            this.props!.history!.push(ROUTE_PATH.happinessHomeInfo + '/' + id);
        }
        if (type === '幸福院活动') {
            this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id);
        }
        if (type === '居家服务商') {
            this.props.history!.push(ROUTE_PATH.serverDetail + '/' + id);
        }
        if (type === '服务产品') {
            this.props.history!.push(ROUTE_PATH.serviceDetails + '/' + id);
        }
        if (type === '资讯') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
        if (type === '养老补助') {
            this.props.history!.push(ROUTE_PATH.applicationDetail + '/' + id);
        }
        if (type === '慈善基金') {
            this.props.history!.push(ROUTE_PATH.charitableTitleFundDetail + '/' + id);
        }

    }
    goBack = () => {
        this.props.history!.push(ROUTE_PATH.home);
    }
    render() {
        const { show, jg_list, xfy_list, ylbz_list, csjj_list, hd_list, fws_list, fwcp_list, zx_list, first } = this.state;
        return (
            <div className={"mainSearch"}>
                <div style={{ overflow: 'hidden', height: '44px', marginBottom: '10px' }}>
                    <div onClick={this.goBack} style={{ float: 'left', width: '10%', backgroundColor: '#efeff4', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='left' />
                    </div>
                    <div style={{ float: 'right', width: '90%', height: '100%' }}>
                        <SearchBar onChange={this.insert} value={this.state.insert_info} onSubmit={() => { this.submit(this.state.insert_info); }} placeholder="机构/幸福院/活动/服务商/产品/资讯/补助/慈善" ref={ref => this.autoFocusInst = ref} />
                    </div>
                </div>
                {show && show === true ?
                    <div style={{ textAlign: 'center', marginTop: '30px' }}>
                        <Spin size="large" />
                    </div>
                    :
                    ''
                }
                {this.state.jg_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'养老机构'}
                        />
                        {this.state.jg_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('养老机构', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.organization_info ? item.organization_info.picture_list[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('养老机构'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.xfy_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'社区幸福院'}
                        />
                        {this.state.xfy_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('社区幸福院', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.organization_info ? item.organization_info.picture_list[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('社区幸福院'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.hd_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'幸福院活动'}
                        />
                        {this.state.hd_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('幸福院活动', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.photo ? item.photo[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.activity_name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('幸福院活动'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.fws_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'居家服务商'}
                        />
                        {this.state.fws_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('居家服务商', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.organization_info ? item.organization_info.picture_list[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('居家服务商'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.fwcp_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'服务产品'}
                        />
                        {this.state.fwcp_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('服务产品', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.picture_collection ? item.picture_collection[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('服务产品'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.zx_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'资讯'}
                        />
                        {this.state.zx_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('资讯', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.app_img_list ? item.app_img_list[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.title}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('资讯'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.ylbz_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'养老补助'}
                        />
                        {this.state.ylbz_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('养老补助', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.allowance_project_photo ? item.allowance_project_photo[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.allowance_project_name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('养老补助'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {this.state.csjj_show === true ?
                    <Card style={{ marginBottom: '20px' }}>
                        <Card.Header
                            title={'慈善基金'}
                        />
                        {this.state.csjj_list.map((item: any) => {
                            return <Card.Body key={item} onClick={() => { this.goDetails('慈善基金', item.id); }}>
                                <div style={{ overflow: 'hidden' }}>
                                    <div style={{ float: 'left', marginRight: '10px', width: '40px', height: '40px' }}><img style={{ width: '40px', height: '40px' }} src={item.photo ? item.photo[0] : ''} /></div>
                                    <div style={{ float: 'left', width: '75%' }}>{item.donate_name}</div>
                                    <div style={{ float: 'right' }}><Icon type="right" /></div>
                                </div>
                            </Card.Body>;
                        })}
                        <Card.Body onClick={() => { this.goList('慈善基金'); }}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', color: 'blue' }}>更多</div>
                                <div style={{ float: 'right' }}><Icon type="right" /></div>
                            </div>
                        </Card.Body>
                    </Card>
                    : ''
                }
                {first === true && jg_list.length === 0 && xfy_list.length === 0 && ylbz_list.length === 0 && csjj_list.length === 0 && hd_list.length === 0 && fws_list.length === 0 && fwcp_list.length === 0 && zx_list.length === 0 ?
                    <div style={{ width: '100%', textAlign: 'center' }}>暂无关注数据</div>
                    :
                    ''
                }
            </div>
        );
    }
}

/**
 * 控件：搜索控制器
 * 描述
 */
@addon('SearchView', '搜索', '描述')
@reactControl(SearchView, true)
export class SearchViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}