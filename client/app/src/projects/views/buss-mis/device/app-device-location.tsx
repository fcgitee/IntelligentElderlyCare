import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Form, Spin, Row } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { request, setMainFormTitle } from "src/business/util_tool";
import LocationMap from "./locationMap";
import { Modal, Toast } from "antd-mobile";
import "./index.less";
import ElectronicFenceMap from "./ElectronicFenceMap";
const prompt = Modal.prompt;
/**
 * 组件：绑定设备定位控件状态
 */
export interface AppDeviceLocationViewState extends ReactViewState {
    /** 基础数据 */
    data_info: any;
    is_loading: boolean;
    map_type: string;
    center_lat: number;
    center_lng: number;
    overlay: any;
}
/**
 * 组件：绑定设备定位控件
 */
export class AppDeviceLocationView extends ReactView<AppDeviceLocationViewControl, AppDeviceLocationViewState> {
    private mapObject: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: false,
            data_info: {
                lng: 113.14958,
                lat: 23.035191,
                address: '智慧养老平台',
                points: [],
            },
            map_type: 'location',
            center_lng: 113.14958,
            center_lat: 23.035191,
            overlay: [],
        };
    }
    componentDidMount() {
        this.ctrl('getLocation');

        $('.BMapGLLib_Drawing_panel a').on('click', () => {
            this.setState({
                overlay: {}
            });
        });
    }
    ctrl = (type: string) => {
        if (type === 'setLocation') {
            this.setState(
                {
                    map_type: 'location',
                    is_loading: true
                },
                () => {
                    this.ctrl('getLocation');
                }
            );
        } else if (type === 'setFootprint') {
            this.setState(
                {
                    map_type: 'footprint',
                    is_loading: true
                },
                () => {
                    this.ctrl('getFootprint');
                }
            );
        } else if (type === 'setElectronicFence') {
            this.setState(
                {
                    map_type: 'setElectronicFence',
                    is_loading: true
                },
                () => {
                    this.getDeviceElectronicFence();
                }
            );
        } else if (type === 'getLocation') {
            setMainFormTitle('查看设备定位');
            request(this, AppServiceUtility.device_service.get_device_location!({ id: this.props.match!.params.key, from: 'relation' }))
                .then((datas: any) => {
                    if (datas && datas.code === 0) {
                        this.setState({
                            data_info: datas.msg
                        });
                    } else {
                        Toast.fail(datas.msg);
                    }
                })
                .catch((err) => {
                    console.error(err);
                }).finally(() => {
                    this.setState({
                        is_loading: false,
                    });
                });
        } else if (type === 'getFootprint') {
            setMainFormTitle('查看设备轨迹');
            request(this, AppServiceUtility.device_service.get_device_footprint!({ id: this.props.match!.params.key, from: 'relation' }))
                .then((datas: any) => {
                    if (datas && datas.code === 0) {
                        if (datas.msg && datas.msg.length) {
                            this.setState({
                                data_info: {
                                    lat: datas.msg[0]['lat'],
                                    lng: datas.msg[0]['lng'],
                                    points: datas.msg,
                                },
                            });
                        }
                    } else {
                        Toast.fail(datas.msg);
                    }
                })
                .catch((err) => {
                    console.error(err);
                }).finally(() => {
                    this.setState({
                        is_loading: false,
                    });
                });
        } else if (type === 'clear') {
            let data_info = this.state.data_info;
            data_info['points'] = [];
            this.setState(
                {
                    overlay: [],
                    data_info,
                },
                () => {
                    this.mapObject && this.mapObject.map && this.mapObject.map.clearOverlays();
                    $('.BMapGLLib_Drawing').fadeIn();
                }
            );
        } else if (type === 'save') {
            const { overlay } = this.state;
            if (!Array.isArray(overlay) || overlay.length === 0) {
                Toast.fail('请选择电子围栏范围！');
                return;
            }
            this.setState(
                {
                    is_loading: true,
                },
                () => {
                    let param: any = {};
                    param['id'] = this.props.match!.params.code;
                    param['overlay'] = {};
                    param['overlay']['drawing_mode'] = 'rectangle';
                    param['overlay']['points'] = overlay;
                    // 保存围栏坐标
                    request(this, AppServiceUtility.device_service.update_device_weilan!(param)!)
                        .then((datas: any) => {
                            if (datas === 'Success') {
                                Toast.success('保存成功！', 1, () => {
                                    history.back();
                                });
                            } else {
                                Toast.fail(datas);
                            }
                        }).catch((error: Error) => {
                            Toast.fail(error.message);
                        })
                        .finally(() => {
                            this.setState({
                                is_loading: false,
                            });
                        });
                }
            );
        }
    }

    getDeviceElectronicFence = () => {
        request(this, AppServiceUtility.device_service.get_device_all!({ id: this.props.match!.params.code })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length > 0) {

                    let data_info = datas['result'][0];

                    let setStateParam: any = {};
                    // 数据源
                    setStateParam['data_info'] = data_info;
                    // 赋值原有的数据
                    if (data_info.overlay) {
                        setStateParam['overlay'] = data_info.overlay;
                    }

                    // 设置中心点，好看点
                    if (data_info.overlay) {
                        if (!Array.isArray(data_info.overlay.points) || data_info.overlay.points.length === 0) {
                            return;
                        }
                        if (data_info.overlay.drawing_mode === 'rectangle') {
                            setStateParam['center_lng'] = (data_info.overlay.points[0].lng + data_info.overlay.points[1].lng) / 2;
                            setStateParam['center_lat'] = (data_info.overlay.points[0].lat + data_info.overlay.points[2].lat) / 2;
                        } else if (data_info.overlay.drawing_mode === 'circle') {
                            setStateParam['center_lng'] = data_info.overlay.points[0].lng;
                            setStateParam['center_lat'] = data_info.overlay.points[0].lat;
                        }
                    }
                    this.setState(setStateParam);
                }
            })
            .catch((err) => {
                console.error(err);
            }).finally(() => {
                this.setState({
                    is_loading: false,
                });
            });
    }

    deg2rad(lat: number) {
        return (lat / 180) * Math.PI;
    }
    rad2deg(radius: number) {
        return radius * 180 / Math.PI;
    }

    returnSquarePoint = (lng: number, lat: number, $distance = 0.5) => {
        let dlng = 2 * Math.asin(Math.sin($distance / (2 * 6371)) / Math.cos(this.deg2rad(lat)));
        dlng = this.rad2deg(dlng);

        let dlat = $distance / 6371;
        dlat = this.rad2deg(dlat);

        let returnArr = [];
        // rightBottom
        returnArr.push({
            lat: lat - dlat,
            lng: lng + dlng,
        });
        // leftBottom
        returnArr.push({
            lat: lat - dlat,
            lng: lng - dlng,
        });
        // leftTop
        returnArr.push({
            lat: lat + dlat,
            lng: lng - dlng,
        });
        // rightTop
        returnArr.push({
            lat: lat + dlat,
            lng: lng + dlng,
        });
        return returnArr;
    }

    onComplete = (e: any, info: any) => {
        if (info.drawingMode) {
            let overlay: any = {
                points: [],
                drawing_mode: info.drawingMode,
            };
            if (info.drawingMode === 'rectangle') {
                // 矩形
                for (let i = 0; i < info.overlay.points.length; i++) {
                    if (info.overlay.points[i].latLng) {
                        overlay.points.push({
                            lat: info.overlay.points[i].latLng['lat'],
                            lng: info.overlay.points[i].latLng['lng'],
                        });
                    }
                }
            } else if (info.drawingMode === 'circle') {
                // 圆形
                overlay.points.push({
                    lat: info.overlay.point.latLng['lat'],
                    lng: info.overlay.point.latLng['lng'],
                });
                // 半径
                overlay.radius = info.overlay.radius;
            } else if (info.drawingMode === 'marker') {
                let allOverlay: any = this.mapObject && this.mapObject.map && this.mapObject.map.getOverlays ? this.mapObject.map.getOverlays() : [];
                for (let i = 0; i < allOverlay.length - 1; i++) {
                    if (i !== allOverlay.length - 2) {
                        this.mapObject && this.mapObject.map && this.mapObject.map.removeOverlay && this.mapObject.map.removeOverlay(allOverlay[i]);
                    }
                }
                prompt(
                    '请输入围栏边长', '单位：（KM）',
                    [
                        { text: '取消' },
                        {
                            text: '确认',
                            onPress: (value) => {
                                if (isNaN(value)) {
                                    Toast.fail('边长必须输入数字！');
                                    return;
                                }
                                value = Number(value);
                                if (value < 1) {
                                    Toast.fail('边长最小为1！');
                                    return;
                                }
                                if (parseInt(value, 0) !== value) {
                                    Toast.fail('边长必须为整数！');
                                    return;
                                }
                                // 获取半径
                                let sqrt: number = Number(Math.sqrt((Math.pow(value, 2) * 2)).toFixed(2));
                                overlay = this.returnSquarePoint(info.overlay.latLng['lng'], info.overlay.latLng['lat'], Number((sqrt / 2.8).toFixed(2)));
                                let data_info = this.state.data_info;
                                data_info['overlay'] = {
                                    drawing_mode: 'rectangle',
                                    points: overlay,
                                };
                                this.setState(
                                    {
                                        overlay,
                                        center_lng: info.overlay.latLng['lng'],
                                        center_lat: info.overlay.latLng['lat'],
                                        data_info: data_info,
                                    },
                                    () => {
                                        $('.BMapGLLib_Drawing').fadeOut();
                                    }
                                );
                            }
                        },
                    ],
                    'default',
                    '1'
                );
                // prompt(
                //     '请输入围栏长宽',
                //     '',
                //     (login, password) => {
                //         console.log($('.am-modal-wrap').find('input[type="text"]').eq(0).val());
                //         console.log($('.am-modal-wrap').find('input[type="text"]').eq(1).val());
                //     },
                //     'login-password',
                //     undefined,
                //     ['请输入边长（单位：KM）', '请输入宽（单位：KM）'],
                // );
                // setTimeout(
                //     () => {
                //         $('.am-modal-wrap').find('input[type="password"]').prop({
                //             type: 'text'
                //         });
                //     },
                //     0
                // );
                return;
            }
            this.setState(
                {
                    overlay
                },
                () => {
                    $('.BMapGLLib_Drawing').fadeOut();
                }
            );
        }
    }

    cb = (e: any) => {
        if (e) {
            this.mapObject = e;
        }
    }

    render() {
        const { is_loading, data_info, map_type, center_lng, center_lat } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row>
                    {map_type === 'setElectronicFence' ?
                        <ElectronicFenceMap center_lng={center_lng} center_lat={center_lat} cb={this.cb} onComplete={this.onComplete} defaultOverlay={data_info.overlay} /> : (data_info.lng && data_info.lat ? <LocationMap lng={data_info.lng} lat={data_info.lat} address={data_info.address} user_name={data_info.user_name} map_type={map_type} points={data_info.points} /> : null)}
                    {map_type === 'setElectronicFence' ? <div className="ctrl-bnts">
                        <span onClick={() => this.ctrl('clear')}>清空</span>
                        <span onClick={() => this.ctrl('save')}>保存</span>
                    </div> : null}
                    <div className="toggle-btns">
                        <span onClick={() => this.ctrl('setLocation')} className={`location${map_type === 'location' ? ' active' : ''}`}>实时定位</span>
                        <span onClick={() => this.ctrl('setFootprint')} className={`footprint${map_type === 'footprint' ? ' active' : ''}`}>历史轨迹</span>
                        <span onClick={() => this.ctrl('setElectronicFence')} className={`electronicfence${map_type === 'setElectronicFence' ? ' active' : ''}`}>电子围栏</span>
                    </div>
                </Row>
            </Spin >
        );
    }
}

/**
 * 组件：绑定设备定位控件
 * 控制绑定设备定位控件
 */
@addon('AppDeviceLocationView', '绑定设备定位控件', '控制绑定设备定位控件')
@reactControl(Form.create<any>()(AppDeviceLocationView), true)
export class AppDeviceLocationViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}