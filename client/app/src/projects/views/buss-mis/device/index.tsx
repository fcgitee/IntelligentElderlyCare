import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Modal, NavBar, Toast, WhiteSpace, WingBlank } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { request, setMainFormTitle } from "src/business/util_tool";
import { Form, Icon, Row, Spin } from "antd";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const alert = Modal.alert;

/**
 * 组件：绑定设备列表状态
 */
export interface AppDeviceListState extends BaseReactElementState {
    is_loading: boolean;
    device_list: any[];
}

/**
 * 组件：绑定设备列表
 * 描述
 */
export class AppDeviceList extends BaseReactElement<AppDeviceListControl, AppDeviceListState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: true,
            device_list: [],
        };
    }
    componentDidMount() {
        setMainFormTitle('我的绑定设备');
        this.getDataList();
    }
    getDataList = () => {
        this.setState(
            {
                is_loading: true,
            },
            () => {
                request(this, AppServiceUtility.device_service.get_device_self!({}))
                    .then((datas: any) => {
                        if (datas && datas.result && datas.result.length > 0) {
                            this.setState({
                                device_list: datas.result,
                            });
                        }
                    })
                    .catch((err) => {
                        console.error(err);
                    }).finally(() => {
                        this.setState({
                            is_loading: false,
                        });
                    });
            }
        );
    }
    ctrl = (type: string, record: any = {}, event: any = {}) => {
        if (type === 'toAdd') {
            this.props!.history!.push(ROUTE_PATH.appBindDevice);
        } else if (type === 'toLocation') {
            this.props!.history!.push(ROUTE_PATH.appDeviceLocation + '/' + record.id + '/' + record.device_id);
        } else if (type === 'unbind') {
            event && event.stopPropagation && event.stopPropagation();
            event && event.preventDefault && event.preventDefault();
            alert('解绑此设备', '', [
                { text: '取消', onPress: () => console.log('cancel') },
                {
                    text: '确认', onPress: () => {
                        this.setState(
                            {
                                is_loading: true,
                            },
                            () => {
                                request(this, AppServiceUtility.device_service.unbind_device!({ imei: record.imei }))
                                    .then((datas: any) => {
                                        if (datas === 'Success') {
                                            Toast.success('解绑成功！', 1, () => {
                                                this.getDataList();
                                            });
                                        } else {
                                            Toast.fail(`解绑失败！${datas}`);
                                        }
                                    })
                                    .catch((err) => {
                                        console.error(err);
                                    }).finally(() => {
                                        this.setState({
                                            is_loading: false,
                                        });
                                    });
                            }
                        );
                    }
                },
            ]);
        }
    }
    render() {
        const { is_loading, device_list } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row className="consignee-address-list-layout">
                    <NavBar
                        icon={<Icon type={"left"} onClick={() => history.back()} />}
                        rightContent={<Icon type={"plus"} onClick={() => this.ctrl('toAdd')} />}
                    >
                        {'我的绑定设备'}
                    </NavBar>
                    <Row>
                        <WhiteSpace size="lg" />
                        <WingBlank>
                            {device_list && device_list.length ? device_list.map((item: any, index: number) => {
                                return (
                                    <Row key={index}>
                                        <Row className="consignee-address-item" onClick={() => this.ctrl('toLocation', item)}>
                                            <Row type="flex" justify="start" align="bottom" className="consignee-address-item-line">
                                                <span className="consignee-address-name">{item.elder_name}</span>
                                                <span>{item.device_name}</span>
                                            </Row>
                                            <WhiteSpace size="md" />
                                            <Row className="consignee-address-item-line maxLine2" style={{ WebkitBoxOrient: 'vertical' }}>
                                                <span>绑定时间：{item.create_date}</span>
                                            </Row>
                                            <span className="btn-delete" onClick={(e) => this.ctrl('unbind', item, e)}>删除</span>
                                        </Row>
                                        <WhiteSpace size="lg" />
                                    </Row>
                                );
                            }) : null}
                        </WingBlank>
                    </Row>
                </Row>
            </Spin>
        );
    }
}

/**
 * 控件：绑定设备列表控制器
 * 描述
 */
@addon('AppDeviceList', '绑定设备列表', '描述')
@reactControl(Form.create<any>()(AppDeviceList), true)
export class AppDeviceListControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}