import React from 'react';
import Map from 'react-bmapgl/Map';
import DrawingManager from 'react-bmapgl/Library/DrawingManager';
import Polygon from 'react-bmapgl/Overlay/Polygon';
import Circle from 'react-bmapgl/Overlay/Circle';
import { Row } from 'antd';

class ElectronicFenceMap extends React.Component {
    render() {
        let drawingToolOptions = {
            drawingModes: ['marker'],
        }
        const { center_lng, center_lat, cb, onComplete, defaultOverlay } = this.props;
        // 默认矩形
        let drawingMode = defaultOverlay && defaultOverlay.drawing_mode ? defaultOverlay.drawing_mode : drawingToolOptions.drawingModes[0];
        return (
            <Row>
                <Map
                    zoom={17}
                    amapkey={"jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji"}
                    center={{ lng: center_lng, lat: center_lat }}
                    enableDragging={true}
                    enableScrollWheelZoom={true}
                    ref={ref => { cb && cb(ref) }}
                    style={{ height: `calc(100vh - ${$('.layout-conten').css('marginTop')})` }}>
                    <DrawingManager
                        enableLimit={true}
                        enableCalculate={true}
                        drawingToolOptions={drawingToolOptions}
                        drawingMode={drawingMode}
                        onOverlaycomplete={(e, info) => { onComplete && onComplete(e, info) }}
                    />
                    {(() => {
                        if (defaultOverlay && defaultOverlay.drawing_mode) {
                            if (defaultOverlay.drawing_mode === 'rectangle') {
                                return (
                                    <Polygon
                                        path={[
                                            new BMapGL.Point(defaultOverlay.points[0].lng, defaultOverlay.points[0].lat),
                                            new BMapGL.Point(defaultOverlay.points[1].lng, defaultOverlay.points[1].lat),
                                            new BMapGL.Point(defaultOverlay.points[2].lng, defaultOverlay.points[2].lat),
                                            new BMapGL.Point(defaultOverlay.points[3].lng, defaultOverlay.points[3].lat),
                                        ]}
                                        key={Math.random()}
                                        strokeColor="#f00"
                                        strokeWeight={2}
                                        fillColor="#ff0"
                                        fillOpacity={0.3}
                                        onMouseover={e => { console.log(e) }}
                                    />
                                );
                            } else if (defaultOverlay.drawing_mode === 'circle') {
                                return (
                                    <Circle
                                        center={new BMapGL.Point(defaultOverlay.points[0].lng, defaultOverlay.points[0].lat)}
                                        radius={defaultOverlay.radius}
                                        strokeColor="#f00"
                                        strokeWeight={2}
                                        fillColor="#ff0"
                                        fillOpacity={0.3}
                                        key={Math.random()}
                                    />
                                );
                            }
                        }
                        return null;
                    })()}
                </Map>
            </Row>
        )
    }
}

export default ElectronicFenceMap;