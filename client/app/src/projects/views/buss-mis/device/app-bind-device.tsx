import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, WhiteSpace, Toast, } from 'antd-mobile';
import { Form, Button, Spin } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { request, setMainFormTitle } from "src/business/util_tool";
/**
 * 组件：绑定设备视图控件状态
 */
export interface AppBindDeviceViewState extends ReactViewState {
    /** 基础数据 */
    is_loading: boolean;
}
/**
 * 组件：绑定设备视图控件
 */
export class AppBindDeviceView extends ReactView<AppBindDeviceViewControl, AppBindDeviceViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: false,
        };
    }
    componentDidMount() {
        setMainFormTitle('绑定设备');
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form, } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (values.imei) {
                this.setState({
                    is_loading: true,
                });
                request(this, AppServiceUtility.device_service.bind_device_self!({ imei: values.imei }))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            Toast.success('绑定成功！', 1, () => {
                                this.props!.history!.push(ROUTE_PATH.appDeviceList);
                            });
                        } else {
                            Toast.fail(`绑定失败！${datas}`);
                        }
                    })
                    .catch((err) => {
                        console.error(err);
                    }).finally(() => {
                        this.setState({
                            is_loading: false,
                        });
                    });
            }
        });
    }
    render() {
        const { getFieldProps } = this.props.form;
        const { is_loading } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Form onSubmit={this.handleSubmit}>
                    <WhiteSpace />
                    <List>
                        <InputItem  {...getFieldProps('imei')} placeholder="请输入设备编号">设备编号</InputItem>
                        <List.Item >
                            <Button style={{ width: '100%' }} type="primary" htmlType='submit'>保存</Button>
                        </List.Item>
                        <WhiteSpace />
                    </List>
                </Form>
            </Spin >
        );
    }
}

/**
 * 组件：绑定设备视图控件
 * 控制绑定设备视图控件
 */
@addon('AppBindDeviceView', '绑定设备视图控件', '控制绑定设备视图控件')
@reactControl(Form.create<any>()(AppBindDeviceView), true)
export class AppBindDeviceViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}