import React from 'react';
import Map from 'react-bmapgl/Map';
import Marker from 'react-bmapgl/Overlay/Marker';
import InfoWindow from 'react-bmapgl/Overlay/InfoWindow';
import Polyline from 'react-bmapgl/Overlay/Polyline';

class LocationMap extends React.Component {
    render() {
        const { lng, lat, address, map_type, points, user_name } = this.props;
        return (
            map_type ?
                <Map
                    // zoom={17}
                    amapkey={"jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji"}
                    center={{ lng: lng, lat: lat }}
                    enableDragging={true}
                    enableScrollWheelZoom={true}
                    style={{ height: `calc(100vh - ${$('.layout-conten').css('marginTop')})` }}>
                    {
                        map_type === 'location' ? <Marker position={{ lng: lng, lat: lat }} /> : null
                    }
                    {
                        map_type === 'location' ? <InfoWindow
                            position={{ lng: lng, lat: lat }}
                        >
                            <div style={{ fontSize: 14 }}>{user_name}<br />{address}</div>
                        </InfoWindow> : null
                    }
                    {
                        map_type === 'footprint' && points && points.length && points.map ?
                            <Polyline
                                path={
                                    points.map((item, index) => {
                                        return (
                                            new BMapGL.Point(item.lng, item.lat)
                                        )
                                    })
                                }
                                key={Math.random()}
                                strokeColor="#f00"
                                strokeWeight={3}
                            /> : null
                    }
                    {
                        map_type === 'footprint' && points && points.length && points.map ?
                            points.map((item, index) => {
                                return (
                                    <Marker key={index} position={{ lng: item.lng, lat: item.lat }} />
                                );
                            }) : null
                    }
                </Map>
                : null
        )
    }
}

export default LocationMap;