// import { Grid } from 'antd-mobile';
import { Avatar, Tabs } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import './index.less';
const { TabPane } = Tabs;
/**
 * 组件：健康档案状态
 */
export interface HealthFilesViewState extends ReactViewState {
}

/**
 * 组件：健康档案
 * 描述
 */
export class HealthFilesView extends ReactView<HealthFilesViewControl, HealthFilesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    render() {
        setMainFormTitle('我的健康');
        // const data = Array.from(new Array(9)).map((_val, i) => ({
        //     icon: 'http://i1.sinaimg.cn/ent/d/2008-06-04/U105P28T3D2048907F326DT20080604225106.jpg',
        //     text: `4111`,
        // }));
        return (
            <div>
                <div className='touxiang'>
                    <div style={{ marginTop: '20px', display: 'flex' }}>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                        <div style={{ textAlign: 'center', float: 'left' }}>
                            <Avatar size={64} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{ backgroundColor: '#333333', marginLeft: '20px', marginRight: '30px' }} />
                            <p>zhangsan</p>
                        </div>
                    </div>
                </div>
                <div>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="健康报告" key="1">
                            报告内容
                    </TabPane>
                    </Tabs>
                </div>
            </div>
        );
    }
}

/**
 * 控件：健康档案控制器
 * 描述
 */
@addon('HealthFilesView', '健康档案', '描述')
@reactControl(HealthFilesView, true)
export class HealthFilesViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}