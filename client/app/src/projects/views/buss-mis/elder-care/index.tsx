import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { SearchBar } from "antd-mobile";
import { ListView } from "antd-mobile";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row } from "antd";

/**
 * 组件：长者护理列表页控件状态
 */
export interface elderCareViewState extends ReactViewState {
    /** 基础数据 */
    elderdata?: any;
    /** 输入框value */
    searchvalue?: string;
    /** 数据 */
    dataList?: any;
    /** 当前页数 */
    pagesize?: number;

}

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

/**
 * 组件：长者护理列表页控件
 */
export class elderCareView extends ReactView<elderCareViewControl, elderCareViewState> {
    public lv: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            elderdata: ds,
            searchvalue: '',
            dataList: [],
            pagesize: 1
        };
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_personnel_elder!({}, this.state.pagesize, 10)!
            .then((data) => {
                const dataBlob = getDataBlob(data.result);
                this.setState({
                    elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                    dataList: data.result
                });
            });
    }
    handlesearch(e: any) {
        this.setState({
            searchvalue: e
        });
    }
    componentDidUpdate() {
        let navheaderheight;
        if (document.getElementsByClassName('searchinput').length) {
            navheaderheight = document.getElementsByClassName('searchinput')[0].clientHeight;
        }
        let widthheight = document.body.clientHeight;
        let headerheight = document.getElementsByClassName('am-navbar am-navbar-dark')[0].clientHeight;
        if (document.getElementsByClassName('am-list-view-scrollview').length) {
            if (navheaderheight) {
                document.getElementsByClassName('am-list-view-scrollview')[0]['style']!.height = (widthheight - headerheight - navheaderheight) + 'px';
            } else {
                document.getElementsByClassName('am-list-view-scrollview')[0]['style']!.height = (widthheight - headerheight) + 'px';
            }
        }
    }
    handlesubmit() {
        AppServiceUtility.person_org_manage_service.get_personnel_elder!({ name: this.state.searchvalue })!
            .then((data: any) => {
                const dataBlob = getDataBlob(data.result);
                this.setState({
                    elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                    dataList: data.result
                });
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.elderlyCareTemplate + '/' + obj.id);
    }
    onEndReached() {
        this.setState({ pagesize: this.state.pagesize! + 1 }, () => {
            AppServiceUtility.person_org_manage_service.get_personnel_elder!({}, this.state.pagesize, 10)!
                .then((data: any) => {
                    let item = this.state.dataList;
                    data.result.forEach((items: any, index: number) => {
                        item.push(items);
                    });
                    const dataBlob = getDataBlob(item);
                    this.setState({
                        elderdata: this.state.elderdata.cloneWithRows(dataBlob),
                        dataList: item
                    });
                });
        });
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        function idcard(idCard: string) {
            let uw = idCard.replace(/(\w)/g, function (a, b, c, d) {
                return ((c > 1 && c < 6) || c > (idCard.length - 5)) ? '*' : a;
            });
            return uw;
        }
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}><span>{obj.name}</span><span>{obj.nation}</span></div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={{ fontWeight: 'bold', flex: '0 1 50%', maxWidth: '50%' }}>{obj.sex}</div>
                        <div style={{ flex: '0 1 50%', maxWidth: '50%' }}>{idcard(obj.id_card)}</div>
                    </div>
                    <div style={{ textAlign: 'right', paddingBottom: '8px', display: 'flex' }}>
                        <div style={{ flex: '0 1 50%', maxWidth: '50%' }}>{obj.native_place}</div>
                        <div style={{ flex: '0 1 50%', maxWidth: '50%' }}>{obj.telephone}</div>
                    </div>
                </div>
            );
        };
        return (
            <div className="elder-care">
                <SearchBar placeholder="请输入长者姓名" value={this.state.searchvalue} onChange={this.handlesearch = this.handlesearch.bind(this)} onSubmit={this.handlesubmit = this.handlesubmit.bind(this)} className="searchinput" />
                {
                    this.state.dataList && this.state.dataList.length ?
                        <ListView
                            ref={el => this.lv = el}
                            dataSource={this.state.elderdata}
                            renderRow={row}
                            renderSeparator={separator}
                            pageSize={10}
                            initialListSize={10}
                            renderBodyComponent={() => <MyBody />}
                            onEndReached={this.onEndReached = this.onEndReached.bind(this)}
                        />
                        :
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                        </Row>
                }
            </div>
        );
    }
}

/**
 * 组件：长者护理列表页控件
 * 控制长者护理列表页控件
 */
@addon('elderCareView', '长者护理列表页控件', '控制长者护理列表页控件')
@reactControl(elderCareView, true)
export class elderCareViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
