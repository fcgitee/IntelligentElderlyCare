import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { Card, Toast } from 'antd-mobile';
import { Icon, Rate } from 'antd';
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { filterHTMLTag } from "../../home";

/**
 * 组件：关怀设置状态
 */
export interface ElderLoveViewState extends ReactViewState {
    yl_data?: any;
    cancel_gz?: any;
    record_list?: any;
}

/**
 * 组件：关怀设置
 * 描述
 */
export class ElderLoveView extends ReactView<ElderLoveViewControl, ElderLoveViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            yl_data: [],
            cancel_gz: 1,
            record_list: []
        };
    }

    componentWillMount = () => {
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ type: 'collection', object: '养老资讯', "is_own": true }, 1, 3)!
            .then((data: any) => {
                this.setState({
                    yl_data: data.result
                });
            });
        // 获取当前登陆人信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log('当前登陆', data);
                // 查询关怀记录
                AppServiceUtility.family_files_service.get_care_record!({ concerned_people: data[0]['id'] }, 1, 3)!
                    .then((data: any) => {
                        // console.log('记录', data);
                        this.setState({
                            record_list: data.result
                        });
                    });
            });
    }

    // 前往列表页
    goList = (e: any, type: any) => {
        // console.log(type);
        this.props.history!.push(ROUTE_PATH.serviceCollectionDetails + '/' + type);
    }

    // 前往详情
    goDetails = (type: any, id: any) => {
        // console.log(id);
        // console.log(type);
        if (type === '养老资讯') {
            this.props.history!.push(ROUTE_PATH.newsInfo + '/' + id);
        }
    }

    iconClick = (e: any) => {
        e.stopPropagation();
    }

    // 取消关注
    likeChange = (value: any, id: any) => {
        // console.log(value);
        // console.log(id);
        if (value === 0) {
            // console.log("取消关注了");
            AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(id)!
                .then((data: any) => {
                    // console.log(data);
                    if (data === 'Success') {
                        this.setState({
                            cancel_gz: 0
                        });
                        Toast.success('取消关注成功!!!', 1);
                        window.location.reload();
                    }
                });
        }
    }

    render() {
        setMainFormTitle('长者关爱');
        const { yl_data } = this.state;
        return (
            <div>
                <Card style={{ marginBottom: '20px' }} className='collection-card'>
                    <div onClick={(e) => { this.goList(e, '养老资讯'); }}>
                        <Card.Header
                            title='养老资讯'
                            extra={<Icon type='right' />}
                        />
                    </div>
                    {yl_data.length > 0 ? yl_data.map((items: any) => {
                        let show_data = items.obj ? items.obj : '';
                        let show_org = items.org ? items.org.name : '';
                        let start_time: Date = items.obj ? new Date(items.obj.create_date) : new Date();
                        let diff = new Date().getTime() - start_time.getTime();
                        var days = Math.floor(diff / (24 * 3600 * 1000));
                        var leave1 = diff % (24 * 3600 * 1000);
                        var hours = Math.floor(leave1 / (3600 * 1000));
                        return <Card.Body onClick={() => { this.goDetails('养老资讯', show_data.id); }} key={items}>
                            <div style={{ overflow: 'hidden' }}>
                                <div style={{ float: 'left', marginRight: '15px', border: '1px solid', width: '35%' }}>
                                    <img src={show_data.app_img_list ? show_data.app_img_list.length > 0 ? show_data.app_img_list[0] : '' : ''} alt="" style={{ width: '100%', height: '120px' }} />
                                </div>
                                <div style={{ float: 'left', width: '59%' }}>
                                    <div style={{ overflow: 'hidden', width: '100%' }}><div style={{ float: 'left', width: '80%' }}><strong>{show_data.title}</strong></div><div style={{ float: 'right' }} onClick={(e) => { this.iconClick(e); }}><Rate defaultValue={1} value={this.state.cancel_gz} count={1} onChange={(value: any) => { this.likeChange(value, items.id); }} /></div></div>
                                    <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '147px', overflow: 'hidden', display: 'inline-block' }}>{show_data.description}</div>
                                    <div className='content'>{filterHTMLTag(show_data.content ? show_data.content : '')}</div>
                                    <div style={{ fontSize: '12px', marginTop: '10px' }}>{days + '天' + hours + '小时前'}</div>
                                    <div style={{ overflow: 'hidden', width: '100%' }}>
                                        <div style={{ float: 'left', width: '60%' }}>{show_org}</div>
                                        <div style={{ float: 'right' }}><Icon type="eye" />&nbsp;{0}&nbsp;&nbsp;<Icon type="heart" theme="filled" />&nbsp;{items.follow_count}</div>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>;
                    })
                        :
                        ''
                    }
                </Card>
                <Card full={true}>
                    <Card.Header
                        title="关怀记录"
                        extra={<Icon type='right' />}
                    />
                    {this.state.record_list.length > 0 ? this.state.record_list.map((item: any) => {
                        return <Card.Body key={item} style={{ display: 'flex', justifyContent: 'space-around' }}>
                            <div>
                                <span style={{ marginRight: '10px' }}>
                                    {item.current_year + '年' + item.current_month + '月' + item.current_day + '日'}
                                </span>
                                <span>
                                    {item.main_name + '关注了您的' + item.care_type_name + '情况'}
                                </span>
                            </div>
                        </Card.Body>;
                    })
                        :
                        <Card.Body style={{ display: 'flex', justifyContent: 'space-around' }}>
                            <div>
                                你对此用户暂无关怀记录
                            </div>
                        </Card.Body>
                    }
                </Card>
            </div>
        );
    }
}

/**
 * 控件：关怀设置控制器
 * 描述
 */
@addon('ElderLoveView', '关怀设置', '描述')
@reactControl(ElderLoveView, true)
export class ElderLoveViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}