import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { InputItem } from 'antd-mobile';
import { Form, Radio } from "antd";
/**
 * 组件：资料补全页面状态
 */
export interface PersonalInfoInsertViewState extends BaseReactElementState {
}

/**
 * 组件：资料补全页面
 * 资料补全
 */
export class PersonalInfoInsertView extends BaseReactElement<PersonalInfoInsertViewControl, PersonalInfoInsertViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    render() {
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <Form>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入姓名">姓名</InputItem>
                    <InputItem editable={false} labelNumber={10}>
                        性别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <Radio.Group>
                            <Radio value={'男'} defaultChecked={true}>男</Radio>
                            <Radio value={'女'}>女</Radio>
                        </Radio.Group>
                    </InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">证件号码</InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">籍贯</InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">民族</InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">家庭住址</InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">行政区划</InputItem>
                    <InputItem {...getFieldProps('name')} clear={true} placeholder="请输入证件号码">组织机构</InputItem>
                </Form>
            </div>
        );
    }
}

/**
 * 控件：资料补全页面控制器
 * 资料补全
 */
@addon('(PersonalInfoInsertView', '资料补全页面', '资料补全')
@reactControl(Form.create<any>()(PersonalInfoInsertView),true)
export class PersonalInfoInsertViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}