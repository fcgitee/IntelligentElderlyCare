import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：服务协议状态
 */
export interface ServiceAgreementViewState extends BaseReactElementState {
    data_string?: string;
}
/**
 * 组件：服务协议
 * 服务协议
 */
export class ServiceAgreementView extends BaseReactElement<ServiceAgreementViewControl, ServiceAgreementViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_string: '',
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: 'service_agreement' })!
            .then((data: any) => {
                if (data && data.result.length && data.result[0] && data.result[0]['data'] && data.result[0]['data']['content']) {
                    this.setState({
                        data_string: data.result[0]['data']['content']
                    });
                }
            })).catch((err) => {
                console.info(err);
            });
    }
    render() {
        return (
            <div className="about-us">
                <div className="compnay-title">
                    欢迎使用智慧康养APP及小壹助手！
                </div>
                <div className="company-description" dangerouslySetInnerHTML={{ __html: this.state.data_string ? this.state.data_string : '' }} />
            </div>
        );
    }
}

/**
 * 控件：服务协议控制器
 * 服务协议
 */
@addon('ServiceAgreementView', '服务协议', '服务协议')
@reactControl(ServiceAgreementView, true)
export class ServiceAgreementViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}