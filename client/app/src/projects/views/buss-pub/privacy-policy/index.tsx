import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：隐私政策状态
 */
export interface PrivacyPolicyViewState extends BaseReactElementState {
    data_string?: string;
}
/**
 * 组件：隐私政策
 * 隐私政策
 */
export class PrivacyPolicyView extends BaseReactElement<PrivacyPolicyViewControl, PrivacyPolicyViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_string: '',
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: 'privacy_policy' })!
            .then((data: any) => {
                if (data && data.result.length && data.result[0] && data.result[0]['data'] && data.result[0]['data']['content']) {
                    this.setState({
                        data_string: data.result[0]['data']['content']
                    });
                }
            })).catch((err) => {
                console.info(err);
            });
    }
    render() {
        return (
            <div className="about-us">
                <div className="compnay-title">
                    隐私政策
                </div>
                <div className="company-description" dangerouslySetInnerHTML={{ __html: this.state.data_string ? this.state.data_string : '' }} />
            </div>
        );
    }
}

/**
 * 控件：隐私政策控制器
 * 隐私政策
 */
@addon('PrivacyPolicyView', '隐私政策', '隐私政策')
@reactControl(PrivacyPolicyView, true)
export class PrivacyPolicyViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}