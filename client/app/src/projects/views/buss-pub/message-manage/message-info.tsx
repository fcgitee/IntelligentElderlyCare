import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WhiteSpace, Card, Button } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Col, Row } from "antd";
import moment from "moment";
/**
 * 组件：消息详情状态
 */
export interface MessageInfoViewState extends BaseReactElementState {
    // 是否已经发送
    isSend: boolean;
    // 消息主体
    message: any;
    // id
    id?: string | undefined;
}

enum MessageState {
    Already_read = '已读',
    Unread = '未读',
}

/**
 * 组件：消息详情
 * 消息详情
 */
export class MessageInfoView extends BaseReactElement<MessageInfoViewControl, MessageInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            isSend: false,
            message: [],
            // 当前id
            id: this.props.match!.params.key || undefined,
        };
    }
    componentDidMount() {
        // 获取单条数据
        if (this.state.id) {
            request(this, AppServiceUtility.message_service.get_all_message_list!({ 'id': this.state.id }, 1, 1))
                .then((datas: any) => {
                    if (datas.result[0].message_state === MessageState.Unread) {
                        request(this, AppServiceUtility.message_service.set_message_already_read!({ 'id': this.state.id }))
                            .then((datas2: any) => {
                                // console.log(datas2);
                            });
                    }
                    // 暂存
                    this.setState({
                        message: datas.result[0],
                    });
                });
        }
    }
    render() {
        let message = this.state.message;
        let info = {
            'font-size': '14px',
            'padding': '9px 15px',
        };
        return (
            <div>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title={message.message_name}
                    />
                    <Col style={info}>
                        <Row>发送人：{message.message_creator}</Row>
                        <WhiteSpace size="xs" />
                        <Row>发送时间：{moment(message.create_date).format("YYYY-MM-DD hh:mm:ss")}</Row>
                        <WhiteSpace size="xs" />
                    </Col>

                    <Card.Body>
                        <p dangerouslySetInnerHTML={{ __html: message.message_content! }} />
                    </Card.Body>

                </Card>
                <WhiteSpace size="xl" />
                <Button onClick={() => { this.props.history!.push(ROUTE_PATH.messageList); }}>返回列表</Button>
                <WhiteSpace size="xl" />
            </div>
        );
    }
}

/**
 * 控件：消息详情控制器
 * 消息详情
 */
@addon('MessageInfoView', '消息详情', '消息详情')
@reactControl(MessageInfoView, true)
export class MessageInfoViewControl extends BaseReactElementControl {

}