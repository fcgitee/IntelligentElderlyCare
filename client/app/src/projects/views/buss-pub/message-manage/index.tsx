import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Badge } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { LoadingEffectView } from "src/business/views/loading-effect";

/**
 * 组件：消息列表状态
 */
export interface MessageListViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
    upload?: boolean;
}

enum MessageState {
    Already_read = '已读',
    Unread = '未读',
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：消息列表
 * 消息列表
 */
export class MessageListView extends ReactView<MessageListViewControl, MessageListViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
            upload: true,
        };
    }
    componentDidMount() {
        AppServiceUtility.message_service.get_all_message_list!({}, 1, 50)!
            .then((datas: any) => {
                const dataBlob = getDataBlob(datas.result);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                    upload: false,
                });
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.messageInfo + '/' + obj.id);
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let multi = {
            'display': '-webkit-box',
            '-webkit-box-orient': 'vertical',
            '-webkit-line-clamp': '3',
            'overflow': 'hidden',
        };
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}>
                        <span style={{ maxWidth: '85%', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', fontWeight: 'bold' }}>{obj.message_name}</span><span>{obj.message_state}
                            {(() => {
                                if (obj.message_state === MessageState.Unread) {
                                    return <Badge dot={true} />;
                                }
                                return null;
                            })()}</span>
                    </div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={multi}>{obj.message_content}</div>
                    </div>
                    <div style={{ textAlign: 'right', paddingBottom: '8px' }}>
                        <div>{obj.create_date}</div>
                    </div>
                </div >
            );
        };
        return (
            <div>
                {this.state.upload && <LoadingEffectView />}
                <ListView
                    ref={el => this.lv = el}
                    dataSource={this.state.dataSource}
                    renderRow={row}
                    renderSeparator={separator}
                    pageSize={4}
                    useBodyScroll={true}
                    scrollRenderAheadDistance={500}
                />
            </div >
        );
    }
}

/**
 * 控件：消息列表控制器
 * 消息列表
 */
@addon('MessageListView', '消息列表', '消息列表')
@reactControl(MessageListView, true)
export class MessageListViewControl extends ReactViewControl {

}