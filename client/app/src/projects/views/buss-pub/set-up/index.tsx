import { List } from 'antd-mobile';
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { message } from 'antd';
const Item = List.Item;

/**
 * 组件：设置状态
 */
export interface SetUpViewState extends BaseReactElementState {
}
/**
 * 组件：设置
 * 设置
 */
export class SetUpView extends BaseReactElement<SetUpViewControl, SetUpViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        setMainFormTitle('设置');
        return (
            <div>
                <List>
                    <Item onClick={() => { this.props.history!.push(ROUTE_PATH.functionIntroduction); }} arrow='horizontal'>功能介绍</Item>
                    <Item onClick={() => { this.props.history!.push(ROUTE_PATH.AboutUs); }} arrow='horizontal'>关于我们</Item>
                </List>
                <div style={{ marginTop: '10px' }}>
                    <List>
                        <Item onClick={() => { this.props.history!.push(ROUTE_PATH.serviceAgreement); }} arrow='horizontal'>服务协议</Item>
                        <Item onClick={() => { this.props.history!.push(ROUTE_PATH.privacyPolicy); }} arrow='horizontal'>隐私政策</Item>
                        <Item onClick={() => { message.info('暂无开放'); }} arrow='horizontal'>为我们打分</Item>
                    </List>
                </div>
                <div style={{ marginTop: '10px', display: 'flex', justifyContent: 'center' }}>
                    <img className='logo-img' src={require('../../../../static/img/南海安卓下载.png')} />
                    <img className='logo-img' src={require('../../../../static/img/南海IOS下载.png')} />
                </div>
            </div >
        );
    }
}

/**
 * 控件：设置控制器
 * 设置
 */
@addon('SetUpView', '设置', '设置')
@reactControl(SetUpView, true)
export class SetUpViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}