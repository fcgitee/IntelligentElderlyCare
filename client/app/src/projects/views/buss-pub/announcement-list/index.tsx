import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Card  } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row, Col, } from "antd";
import moment from "moment";
// const Item = List.Item;
// const Brief = Item.Brief;

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

/**
 * 组件：公告列表状态
 */
export interface AnnouncementListViewState extends BaseReactElementState {
     /** 长列表数据 */
     dataSource?: any;
     /** 请求的数据 */
     list?: any[];
     /** 上拉加载 */
     upLoading?: boolean;
     /** 下拉刷新 */
     pullLoading?: boolean;
     /** 长列表容器高度 */
     height?: any;
     /** 当前第几页 */
     page: number;
     /** 记录tab状态 */
     tab_status?: string;
}

/**
 * 组件：公告列表
 * 公告列表
 */
export class AnnouncementListView extends BaseReactElement<AnnouncementListViewControl, AnnouncementListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'participate',
        };
    }
    componentDidMount() {
        AppServiceUtility.announcement_list_service.get_announcement_list!()!
            .then((data: any) => {
                let list: any[] = [];
                if (data) {
                    list = data.map((key: any, value: any) => {
                        let announcement = {};
                        announcement['id'] = key['id'];
                        announcement['title'] = key['title'];
                        announcement['content'] = key['content'];
                        // announcement['app_img_list_first'] = key['app_img_list'][0];
                        // announcement['app_img_list'] = key['app_img_list'];
                        announcement['issue_date'] = key['issue_date'];
                        return announcement;
                    });
                    console.info(list);
                    this.setState(
                        { list }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    onClickRow = (id: string) => {
        console.info(id);
        this.props.history!.push(ROUTE_PATH.announcementInfo + '/' + id);
    }
    render() {
        const { list, dataSource, } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten'>
                        <Row type='flex' justify='center' onClick={this.onClickRow.bind(this, owData.id)}>
                            {/* <Col className='list-col' span={10}><img src={owData.app_img_list} style={{ height: '72pt' }} /></Col> */}
                            <Col span={14} className='list-col'>
                                <Row><strong>{owData.title}</strong></Row>
                                <Row><p dangerouslySetInnerHTML={{ __html: owData.content! }} /></Row>
                                <Row>{moment(owData.issue_date).format("YYYY-MM-DD hh:mm")}</Row>
                                {/* <Row>
                                    <Col span={12}>{owData.purchase_number}已购买</Col>
                                    <Col span={12}><strong>¥{owData.price}</strong></Col>
                                </Row> */}
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                {/* <Tabs
                    // tabs={tabs}
                    initialPage={0}
                > */}
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无公告
                                </Row>

                        }
                    </div>
                    {/* <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div>
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>
                        }
                    </div> */}
                {/* </Tabs> */}
            </Row>
        );
    }
}

/**
 * 控件：公告列表控制器
 * 公告列表
 */
@addon('AnnouncementListView', '公告列表', '公告列表')
@reactControl(AnnouncementListView, true)
export class AnnouncementListViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}