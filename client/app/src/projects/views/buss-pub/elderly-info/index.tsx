import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, InputItem, DatePicker, Picker, Button, } from 'antd-mobile';
import { Form, Radio, message } from "antd";
import { Address } from '../family-appointment/address';
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
const Item = List.Item;
/**
 * 组件：长者信息控件状态
 */
export interface ElderlyInfoViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 生日 */
    date: any;
    /** 家庭地址-省市区 */
    address?: any;
    /** 详细地址 */
    detail_address?: string;
    /** 户籍地址 */
    residence_address: any;
    /** 户籍地址详情 */
    residence_address_detail: string;
    /** 家庭地址配置 */
    address_config: any;
    /** 户籍地址配置 */
    residence_config: any;
}

export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：长者信息控件
 */
export class ElderlyInfoView extends ReactView<ElderlyInfoViewControl, ElderlyInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            date: '',
            address: ['请选择'],
            detail_address: '',
            residence_address: ['请选择'],
            residence_address_detail: '',
            address_config: {
                title: '家庭地址',
                row_name: '省-市-区-街道',
                address_modal: false
            },
            residence_config: {
                title: '户籍地址',
                row_name: '省-市-区-街道',
                address_modal: false
            },
            base_data: {}
        };
    }
    componentDidMount() {
        request_func(this, AppServiceUtility.reservation_service.get_reservation_registration_list, [{}, 1, 1], (data: any) => {
            // console.log('查询结果：', data.result[0].basic_information[0]);
            // const result: any = data.result;
            this.setState({ base_data: data.result[0].basic_information[0] });
        });

    }
    // 获取家庭地址
    getHomeAddress = (e: any) => {
        let config = this.state.address_config;
        config.address_modal = false;
        this.setState({ address: e.address, address_config: config });
    }
    // 获取户籍地址
    getResidenceAddress = (e: any) => {
        let config = this.state.residence_config;
        config.address_modal = false;
        this.setState({ residence_address: e.address, residence_config: config });
    }
    showHomeAddress = (key: any) => {
        let config = this.state[key];
        config.address_modal = true;
        key === 'address_config' ? this.setState({ address_config: config }) : this.setState({ residence_config: config });
    }
    saveElderInfo = () => {
        const { form } = this.props;
        form!.validateFields((err: Error, values: ServiceItemPackageFormValues) => {
            console.info('新参数：', values);
            const condition = {
                basic_information: values,
                preliminary_assessment: {},
                residence_demand: {}
            };
            request_func(this, AppServiceUtility.reservation_service.update_reservation_registration, [condition], (data: any) => {
                const mes = data.result === 'Success' ? '保存成功' : '保存失败';
                message.info(mes);
            });
        });
        // console.info('参数：', this.props.form.getFieldsValue());
        // request_func(this, AppServiceUtility.reservation_service, [this.props.form.getFieldsValue()], (data: any) => {

        // })
    }
    render() {
        // 宗教信仰
        const faith_list = [
            { label: '基督教', value: '基督教' }, { label: '伊斯兰教', value: '伊斯兰教' }, { label: '佛教', value: '佛教' }];
        // 爱好特长
        const hobbies_list = [
            { label: '唱歌', value: '唱歌' }, { label: '粤剧', value: '粤剧' }, { label: '书法', value: '书法' }, { label: '阅读', value: '阅读' }, { label: '棋牌', value: '棋牌' },];
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <List renderHeader={() => '长者档案'}>
                    <InputItem {...getFieldProps('name')} placeholder="请输入长者姓名" value={this.state.base_data.name}>长者姓名</InputItem>
                    <InputItem {...getFieldProps('id_card')} placeholder="请输入身份证号码" value={this.state.base_data.id_card}>身份证号码</InputItem>
                    <Item
                        {...getFieldProps('sex')}
                        extra={<Radio.Group defaultValue={this.state.base_data.sex}>
                            <Radio value={'男'}>男</Radio>
                            <Radio value={'女'}>女</Radio>
                        </Radio.Group>}
                    >性别
                    </Item>
                    <DatePicker
                        mode="date"
                        title="生日"
                        extra="请选择生日"
                        {...getFieldProps('date')}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">生日</List.Item>
                    </DatePicker>
                    <InputItem {...getFieldProps('telephone')} placeholder="请输入联系电话" value={this.state.base_data.telephone}>联系电话</InputItem>
                    <List.Item arrow="horizontal" onClick={() => { this.showHomeAddress('residence_config'); }} extra={this.state.residence_address}>户籍地址</List.Item>
                    <List.Item arrow="horizontal" onClick={() => { this.showHomeAddress('address_config'); }} extra={this.state.address}>家庭地址
                        </List.Item>
                    <Picker data={faith_list} cols={1} {...getFieldProps('faith')} className="forss">
                        <List.Item arrow="horizontal">宗教信仰</List.Item>
                    </Picker>
                    <Picker data={hobbies_list} cols={1} {...getFieldProps('hobby')} className="forss">
                        <List.Item arrow="horizontal">爱好特长</List.Item>
                    </Picker>
                    <Button type="primary" onClick={this.saveElderInfo}> 保存</Button>
                    <Address config={this.state.residence_config} getAddress={this.getResidenceAddress} />
                    <Address config={this.state.address_config} getAddress={this.getHomeAddress} />
                </List>
            </div >
        );
    }
}

/**
 * 组件：长者信息控件
 * 控制长者信息控件
 */
@addon('ElderlyInfoViewControl', '长者信息控件', '控制长者信息控件')
@reactControl(Form.create<any>()(ElderlyInfoView), true)
export class ElderlyInfoViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}