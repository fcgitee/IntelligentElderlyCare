import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WhiteSpace, Card } from "antd-mobile";
// import { Row, Col, Avatar } from "antd";
import { Announcement } from "src/projects/models/announcement";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import moment from "moment";
// const Item = List.Item;
// const Brief = Item.Brief;
/**
 * 组件：公告详情页面状态
 */
export interface AnnouncementInfoViewState extends BaseReactElementState {
    /** 数据 */
    data: Announcement;
    /** 图片高度 */
    imgHeight?: number | string;
}

/**
 * 组件：公告详情页面
 * 公告详情
 */
export class AnnouncementInfoView extends BaseReactElement<AnnouncementInfoViewControl, AnnouncementInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            imgHeight: 176,
        };
    }

    componentDidMount() {
        // console.warn(this.props);
        let id = this.props.match!.params.key;
        console.warn(id);
        request(this, AppServiceUtility.announcement_detail_service.get_announcement_detail!({ 'id': id }))
            .then((data: any) => {
                // console.log('datas.result', data.result);
                if (data) {
                    this.setState({ data: data.result[0] });
                }
            });
    }

    render() {
        let { data } = this.state;
        return (
            <div>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title={data.title}
                    />
                    <Card.Body>
                        <p dangerouslySetInnerHTML={{ __html: data.content! }} />
                        {moment(data.issue_date).format("YYYY-MM-DD hh:mm")}
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                {/* {
                    this.btn_row(id, type)
                } */}
            </div>
        );
    }
}

/**
 * 控件：公告详情页面控制器
 * 公告详情
 */
@addon('AnnouncementInfoView', '公告详情页面', '公告详情')
@reactControl(AnnouncementInfoView,true)
export class AnnouncementInfoViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}