import { Button, List, TextareaItem, Toast, WhiteSpace, WingBlank } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：意见反馈状态
 */
export interface FeedbackViewState extends BaseReactElementState {

    // 捐款留言
    content?: string;
}

/**
 * 组件：意见反馈
 * 意见反馈
 */
export class FeedbackView extends BaseReactElement<FeedbackViewControl, FeedbackViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            content: '',
        };
    }
    componentDidMount() {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (!user) {
            this.props.history!.push(ROUTE_PATH.login);
            return;
        }
    }
    changeValue = (e: any, inputName: any) => {
        this.setState({
            [inputName]: e
        });
    }
    CharitableTitleFund = () => {
        let { content } = this.state;
        if (!content) {
            Toast.fail('请输入内容', 1);
            return;
        }
        AppServiceUtility.opinion_feedback.update_opinion_feedback!({ content: this.state.content! })!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.info('提交成功');
                    this.props.history!.goBack();
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        setMainFormTitle('意见反馈');
        return (
            <div>
                <List renderHeader={() => '意见反馈'}>
                    <TextareaItem placeholder="请输入内容" rows={3} autoHeight={true} onChange={(e) => this.changeValue(e, 'content')} />
                </List>
                <WhiteSpace size="lg" />
                <WingBlank size="lg">
                    <Button type="primary" onClick={this.CharitableTitleFund}>提交</Button>
                </WingBlank>
            </div>
        );
    }
}

/**
 * 控件：意见反馈控制器
 * 意见反馈
 */
@addon('FeedbackView', '意见反馈', '意见反馈')
@reactControl(FeedbackView, true)
export class FeedbackViewControl extends BaseReactElementControl {

}