import { Icon, Rate } from "antd";
import { Button, List, TextareaItem, Toast, WhiteSpace } from 'antd-mobile';
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";

const Item = List.Item;
const Brief = Item.Brief;

/**
 * 组件：评论状态
 */
export interface CommentViewState extends BaseReactElementState {
    order_info?: any;
    // 评论内容
    // 态度
    service_attitude?: any;
    // 质量
    service_quality?: any;
    // 内容
    opinion_remarks?: any;
    // 是否已发送
    isSend?: boolean;
    // 是否都评论了
    isComment?: boolean;
    // 服务记录的评价记录
    product_comment?: any;
}
/**
 * 组件：评论
 * 评论
 */
export class CommentView extends BaseReactElement<CommentViewControl, CommentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            order_info: [],
            product_comment: [],
            service_attitude: '',
            service_quality: '',
            opinion_remarks: '',
            isSend: false,
            isComment: undefined,
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        AppServiceUtility.my_order_service.my_order_list!({ id })!
            .then((data) => {
                if (data && data.result && data.result[0]) {
                    let product_comment: any = [];
                    let isComment = true;
                    if (data.result[0].hasOwnProperty('record_info') && data.result[0]['record_info'].length > 0) {
                        let newRecordInfo: any = [];
                        data.result[0]['record_info'].map((item: any, index: number) => {
                            product_comment[item['id']] = {
                                service_attitude: '',
                                service_quality: '',
                                opinion_remarks: '',
                                disabled: false,
                            };
                            if (item.hasOwnProperty('comment_info')) {
                                // 已评价的，放后面
                                if (item.comment_info.length > 0) {
                                    newRecordInfo.push(item);
                                    product_comment[item['id']] = {
                                        service_attitude: item.comment_info[0]['service_attitude'],
                                        service_quality: item.comment_info[0]['service_quality'],
                                        opinion_remarks: item.comment_info[0]['opinion_remarks'],
                                        disabled: true,
                                    };
                                } else {
                                    // 有未评价的
                                    isComment = false;
                                    newRecordInfo.unshift(item);
                                }
                            }
                        });
                        // 重新赋值
                        data.result[0]['record_info'] = newRecordInfo;
                    }
                    // 主数据
                    product_comment[data.result[0]['id']] = {
                        service_attitude: '',
                        service_quality: '',
                        opinion_remarks: '',
                        disabled: false,
                    };
                    // 看主数据是否评价了
                    if (data.result[0].hasOwnProperty('comment_info')) {
                        if (data.result[0]['comment_info'].length === 0) {
                            isComment = false;
                        } else {
                            data.result[0]['comment_info'].map((item: any) => {
                                // 没有record_id的那条才是主数据评论
                                if (item.order_id === data.result![0]['id'] && !item.hasOwnProperty('record_id')) {
                                    product_comment[data.result![0]['id']] = {
                                        service_attitude: data.result![0]['comment_info'][0]['service_attitude'],
                                        service_quality: data.result![0]['comment_info'][0]['service_quality'],
                                        opinion_remarks: data.result![0]['comment_info'][0]['opinion_remarks'],
                                        disabled: true,
                                    };
                                }
                            });
                        }
                    }
                    this.setState({
                        order_info: data.result[0],
                        product_comment,
                        isComment,
                    });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    // 服务内容评价
    changeValue = (e: any, inputName: any, pid: any) => {
        let product_comment = this.state.product_comment;
        // console.log(product_comment, pid, e);
        product_comment[pid][inputName] = e;
        this.setState({
            product_comment,
        });
    }
    // 提交评论啦
    commentSubmit() {
        const order_info = this.state.order_info!;
        const { isSend, product_comment } = this.state;
        if (isSend === true) {
            Toast.info('请勿重复提交！');
            return;
        }
        if (order_info.length < 1 || !order_info['id'] || !order_info['origin_product'] || !order_info['origin_product']['product_id']) {
            Toast.info('数据出错，请刷新重试！');
            return;
        }
        if (!product_comment[order_info['id']]['service_attitude']) {
            Toast.info('请对订单服务态度评价！');
            return;
        }
        if (!product_comment[order_info['id']]['service_quality']) {
            Toast.info('请对订单服务质量评价！');
            return;
        }
        // if (service_attitude === '') {
        //     Toast.info('请对服务态度评价！');
        //     return;
        // }
        // if (service_quality === '') {
        //     Toast.info('请对服务质量评价！');
        //     return;
        // }
        let param: any = [];
        let needSend = false;
        for (let i in product_comment) {
            if (product_comment.hasOwnProperty(i)) {
                if (i === order_info['id']) {
                    // 订单主数据
                    if (product_comment[i]['disabled'] === false) {
                        needSend = true;
                        param.push({
                            opinion_remarks: product_comment[i]['opinion_remarks'],
                            service_attitude: product_comment[i]['service_attitude'],
                            service_quality: product_comment[i]['service_quality'],
                            order_id: order_info['id'],
                            product_id: order_info['origin_product']['product_id'],
                        });
                    }
                } else {
                    // 记录多传一个字段
                    if (product_comment[i]['service_attitude'] !== '' && product_comment[i]['service_quality'] !== '' && product_comment[i]['disabled'] === false) {
                        needSend = true;
                        param.push({
                            opinion_remarks: product_comment[i]['opinion_remarks'],
                            service_attitude: product_comment[i]['service_attitude'],
                            service_quality: product_comment[i]['service_quality'],
                            order_id: order_info['id'],
                            record_id: i,
                            product_id: order_info['origin_product']['product_id'],
                        });
                    }
                }
            }
        }
        if (needSend !== true) {
            Toast.info('请至少进行一项评价！');
            return;
        }
        this.setState(
            {
                isSend: true,
            },
            () => {
                // 由于后端会循环，所以这里传一个数组对象
                request(this, AppServiceUtility.service_operation_service.add_service_order_comment!(param))
                    .then((data: any) => {
                        if (data === 'Success') {
                            Toast.success('发布评价成功！', 3, () => {
                                window.location.reload();
                            });
                        } else {
                            this.setState(
                                {
                                    isSend: false,
                                },
                                () => {
                                    Toast.fail(data);
                                }
                            );
                        }
                    }).catch((e) => {
                        this.setState({
                            isSend: false,
                        });
                        console.error(e);
                    });
            }
        );
    }
    render() {
        const order_info = this.state.order_info!;
        const { product_comment, isComment } = this.state;
        // console.log('product_comment', product_comment);
        setMainFormTitle("发表评价");
        return (
            <div>
                <List>
                    <WhiteSpace size="lg" />
                    <Item>{order_info.hasOwnProperty('orgnization_name') > 0 ? order_info.orgnization_name : ''}</Item>
                    <Item thumb={<span>服务项目</span>}><Brief>{order_info.hasOwnProperty('origin_product') && order_info.origin_product.hasOwnProperty('product_name') ? order_info.origin_product['product_name'] : ''}</Brief></Item>
                    <Item thumb={<span>服务态度</span>}><Rate character={<Icon type="heart" theme="filled" />} onChange={(e) => this.changeValue(e, 'service_attitude', order_info.id)} disabled={product_comment[order_info.id] && product_comment[order_info.id]['disabled'] ? product_comment[order_info.id]['disabled'] : false} allowHalf={true} value={product_comment[order_info.id] && product_comment[order_info.id]['service_attitude'] ? product_comment[order_info.id]['service_attitude'] + 0 : 0} /></Item>
                    <Item thumb={<span>服务质量</span>}><Rate character={<Icon type="heart" theme="filled" />} onChange={(e) => this.changeValue(e, 'service_quality', order_info.id)} disabled={product_comment[order_info.id] && product_comment[order_info.id]['disabled'] ? product_comment[order_info.id]['disabled'] : false} allowHalf={true} value={product_comment[order_info.id] && product_comment[order_info.id]['service_quality'] ? product_comment[order_info.id]['service_quality'] + 0 : 0} /></Item>
                    {(() => {
                        if (product_comment[order_info.id] && product_comment[order_info.id]['disabled'] && product_comment[order_info.id]['disabled'] === true) {
                            return (
                                <Item thumb={<span>评价意见</span>}><Brief>{product_comment[order_info.id] && product_comment[order_info.id]['opinion_remarks'] ? product_comment[order_info.id]['opinion_remarks'] : ''}</Brief></Item>
                            );
                        } else {
                            return (
                                <TextareaItem
                                    rows={5}
                                    placeholder='您对这次服务满意吗？'
                                    onChange={(e) => this.changeValue(e, 'opinion_remarks', order_info.id)}
                                />
                            );
                        }
                    })()}
                    <WhiteSpace />
                    <WhiteSpace />
                    {order_info.hasOwnProperty('record_info') && order_info.record_info.length > 0 ? order_info.record_info.map((item: any, index: number) => {
                        return (
                            <List key={index}>
                                <Item thumb={<span>服务项目</span>}><Brief>{item.hasOwnProperty('product_name') ? item.product_name : ''}</Brief></Item>
                                <Item thumb={<span>服务态度</span>}><Rate character={<Icon type="heart" theme="filled" />} onChange={(e) => this.changeValue(e, 'service_attitude', item.id)} disabled={product_comment[item.id] && product_comment[item.id]['disabled'] ? product_comment[item.id]['disabled'] : false} allowHalf={true} value={product_comment[item.id] && product_comment[item.id]['service_attitude'] ? product_comment[item.id]['service_attitude'] + 0 : 0} /></Item>
                                <Item thumb={<span>服务质量</span>}><Rate character={<Icon type="heart" theme="filled" />} onChange={(e) => this.changeValue(e, 'service_quality', item.id)} disabled={product_comment[item.id] && product_comment[item.id]['disabled'] ? product_comment[item.id]['disabled'] : false} allowHalf={true} value={product_comment[item.id] && product_comment[item.id]['service_quality'] ? product_comment[item.id]['service_quality'] + 0 : 0} /></Item>
                                {(() => {
                                    if (product_comment[item.id] && product_comment[item.id]['disabled'] && product_comment[item.id]['disabled'] === true) {
                                        return (
                                            <Item thumb={<span>评价意见</span>}><Brief>{product_comment[item.id] && product_comment[item.id]['opinion_remarks'] ? product_comment[item.id]['opinion_remarks'] : ''}</Brief></Item>
                                        );
                                    } else {
                                        return (
                                            <TextareaItem
                                                rows={5}
                                                placeholder='您对这次服务满意吗？'
                                                onChange={(e) => this.changeValue(e, 'opinion_remarks', item.id)}
                                            />
                                        );
                                    }
                                })()}
                            </List>
                        );
                    }) : null}
                    <WhiteSpace />
                    <WhiteSpace />
                    {(() => {
                        if (isComment === true) {
                            return (
                                <Button type='primary' disabled={true}>已评价</Button>
                            );
                        } else if (isComment === false) {
                            return (
                                <Button type='warning' onClick={() => this.commentSubmit()}>发布</Button>
                            );
                        }
                        return null;
                    })()}
                </List>
            </div>
        );
    }
}

/**
 * 控件：评论控制器
 * 评论
 */
@addon('CommentView', '评论', '评论')
@reactControl(CommentView, true)
export class CommentViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}