import { Button, Col, Icon, Row, message, Avatar } from "antd";
import { Card, ListView, Modal, NavBar, Toast, WhiteSpace, TextareaItem } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { LOCAL_FUNCTION_LIST } from "src/business/mainForm/backstageManageMainForm";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { getAppPermissonObject, ROUTE_PATH } from "src/projects/app/util-tool";
import { filterHTMLTag } from "../../home";
import CopyToClipboard from 'react-copy-to-clipboard';
import baguetteBox from "./baguetteBox.min.js";
import './index.less';
import './baguetteBox.min.css';

function MyBody(props: any) {
    return (
        <Row className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </Row>
    );
}

const prompt = Modal.prompt;
/**
 * 组件：新闻详情页面状态
 */
export interface NewsInfoViewState extends BaseReactElementState {
    /** 数据 */
    data: any;
    /** 长列表数据 */
    dataSource?: any;
    /** 图片高度 */
    imgHeight?: number | string;
    /** 评论列表 */
    comment_list?: any[];
    /** 长列表容器高度 */
    height?: any;
    /** 评论内容 */
    comment_value?: any;
    focusState?: any;
    // user信息
    user?: any;

    // 
    code?: any;
    // 是否登录
    isLogin?: boolean;
    // 是否拥有审核权限
    isAudit?: boolean;
    isSend?: boolean;
}

/**
 * 组件：新闻详情页面
 * 新闻详情
 */
export class NewsInfoView extends BaseReactElement<NewsInfoViewControl, NewsInfoViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            data: {},
            imgHeight: 176,
            comment_list: [],
            dataSource: ds,
            height: document.documentElement!.clientHeight / 2,
            comment_value: '',
            focusState: true,
            user: [],
            code: '',
            isAudit: false,
            isSend: false,
            isLogin: false,
        };
    }
    componentWillMount() {
        // 判断是否登录
        request(this, AppServiceUtility.personnel_service.get_user_session!())
            .then((data: any) => {
                this.setState({
                    isLogin: data,
                });
            });

        // 获取登录信息
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();

        this.setState({ user });

        // 检查是否有审批权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission!({ method: 'audit', type: 'article' }))
            .then((data: any) => {
                if (data.code === 'Success') {
                    this.setState({
                        isAudit: data.isAudit,
                    });
                }
            });
        // 当前资讯ID
        let id = this.props.match!.params.key;

        // 获取权限信息
        const AppPermission = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);

        request(this, AppServiceUtility.article_list_service.get_news_pure_list!({ id: this.props.match!.params.key }))
            .then((data: any) => {
                if (data && data.result && data.result.length > 0) {
                    // 点击放大图片处理
                    data.result[0].content.replace(/<img [^>]*src=[‘"]([^‘"]+)[^>]*>/g, function (match: any, capture: any) {
                        data.result[0].content = data.result[0].content.replace(match, `<div class='gallery'><a data-caption='Image caption' href=${capture}>${match}</a></div>`);
                    });
                    // 通过的直接显示
                    if (data.result[0].status === '通过'
                        // 是作者就直接显示
                        || (user && data.result[0]['promulgator_id'] && data.result[0]['promulgator_id'] === user.id)
                        // 有编辑权限的
                        || (AppPermission && getAppPermissonObject(AppPermission, '新闻管理', '编辑'))
                        // 有审批权限的
                        || (AppPermission && getAppPermissonObject(AppPermission, '新闻审核', '编辑'))
                    ) {
                        this.setState({
                            data: data.result[0],
                        });
                    }
                }
            });
        AppServiceUtility.comment_service.get_comment_list_all!({ comment_object_id: id, audit_status: '通过' }, 1, 10)!
            .then((data: any) => {
                if (data && data.result && data.result.length > 0) {
                    this.setState(
                        { comment_list: data.result }
                    );
                }
            });
    }

    componentDidUpdate() {
        baguetteBox.run('.gallery');
    }

    onClickRow = (id: string) => {
        this.props.history!.push(ROUTE_PATH.childCommmentList + '/' + id);
    }
    commentChange = (e: any) => {
        this.setState({
            comment_value: e.target.value
        });
    }
    textAreaChange = (value: any) => {
        this.setState({
            comment_value: value
        });
    }
    commentsubmit = () => {
        let { data, isSend, isLogin, comment_value } = this.state;
        if (!isLogin) {
            Toast.fail('请先登录后进行操作！');
            return;
        }
        if (comment_value === '') {
            Toast.fail('请输入评论内容！');
            return;
        }
        if (isSend === true || !data.hasOwnProperty('id')) {
            return;
        }
        let param: any = {
            comment_object_id: data['id'],
            content: comment_value,
            type_id: data.type_id
        };
        Toast.loading('');
        let st = setTimeout(
            () => {
                Toast.hide();
                Toast.fail('发送失败，请稍候再试', 1);
            },
            19999
        );
        this.setState(
            {
                isSend: true,
            },
            () => {
                request(this, AppServiceUtility.comment_service.update_comment!(param))
                    .then((data: any) => {
                        clearTimeout(st);
                        Toast.hide();
                        if (data === 'Success') {
                            Toast.success('发送成功！');
                            this.setState({
                                isSend: false,
                                comment_value: '',
                            });
                        } else {
                            Toast.fail(data);
                            this.setState({
                                isSend: false,
                            });
                        }
                    }).catch((error: Error) => {
                        clearTimeout(st);
                        Toast.hide();
                        this.setState({
                            isSend: false,
                        });
                        console.error(error.message);
                    });
            }
        );
    }
    ctrlThis(type: string) {
        let { isSend, isLogin, data } = this.state;
        if (!isLogin) {
            Toast.fail('请先登录后进行操作！');
            return;
        }
        let param: any = { id: data['id'], type: type };
        let is_cancel = false;
        if (isSend === true) {
            return;
        }
        if ((data.is_likes === true && type === 'likes') || (data.is_collection === true && type === 'collection')) {
            is_cancel = true;
        }
        Toast.loading('');
        let st = setTimeout(
            () => {
                Toast.hide();
                Toast.fail('操作失败，请稍候再试', 1);
            },
            19999
        );
        this.setState(
            {
                isSend: true,
            },
            () => {
                if (data && data.hasOwnProperty('id')) {
                    request(this, AppServiceUtility.article_list_service.update_ctrl!(param))
                        .then((result: any) => {
                            clearTimeout(st);
                            Toast.hide();
                            if (result === 'Success') {
                                if (type === 'likes') {
                                    if (is_cancel) {
                                        Toast.success('取消成功 ！');
                                        data['is_likes'] = false;
                                        data['likes_count'] = data['likes_count'] - 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    } else {
                                        Toast.success('点赞成功 ！');
                                        data['is_likes'] = true;
                                        data['likes_count'] = data['likes_count'] + 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    }
                                } else if (type === 'share') {
                                    if (is_cancel) {
                                        Toast.success('取消成功 ！');
                                        data['is_share'] = false;
                                        data['share_count'] = data['share_count'] - 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    } else {
                                        Toast.success('分享成功 ！');
                                        data['is_share'] = false;
                                        data['share_count'] = data['share_count'] + 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    }
                                } else if (type === 'collection') {
                                    if (is_cancel) {
                                        Toast.success('取消成功 ！');
                                        data['is_collection'] = false;
                                        data['collection_count'] = data['collection_count'] - 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    } else {
                                        Toast.success('收藏成功 ！');
                                        data['is_collection'] = false;
                                        data['collection_count'] = data['collection_count'] + 1;
                                        this.setState({
                                            isSend: false,
                                            data,
                                        });
                                    }
                                }
                            } else {
                                this.setState({
                                    isSend: false,
                                });
                                Toast.fail(result);
                            }
                        }).catch((error: Error) => {
                            clearTimeout(st);
                            Toast.hide();
                            this.setState({
                                isSend: false,
                            });
                            console.error(error.message);
                        });
                }
            }
        );
    }
    auditOpen() {
        prompt('资讯审核', '', [
            {
                text: '不通过',
                onPress: value => {
                    this.auditThis('2', value);
                },
            },
            {
                text: '通过',
                onPress: value => {
                    this.auditThis('1', value);
                },
            }]);
    }
    componentWillUnmount = () => {
        // if (window.SpeechSynthesisUtterance) {
        //     speechSynthesis.pause();
        // }
        if (document && document.getElementById('ny_audio_id')) {
            document.getElementById('ny_audio_id')!['pause']();
        }
    }
    auditThis(type: string, value: string = '') {
        let { data } = this.state;
        if (type !== '1' && value === '') {
            Toast.info('不通过请填写原因！');
            return;
        }
        let param: any = {
            // 审批标识
            'action': 'sh',
            // 审批状态
            'action_value': type,
            // 审批信息
            'reason': value,
            // 主键ID
            'id': data['id'],
        };
        const isSend = this.state.isSend;
        const that = this;
        // 正在发送请求
        if (isSend === true) {
            Toast.fail('请勿操作过快！');
            return;
        }
        this.setState(
            {
                isSend: true,
            },
            () => {
                request(that, AppServiceUtility.article_list_service.update_news!(param))
                    .then((data: any) => {
                        if (data === 'Success') {
                            Toast.success('操作成功！', 3, () => history.back());
                        } else {
                            Toast.fail(data);
                        }
                    }).catch((error: Error) => {
                        Toast.fail(error.message);
                    });
            }
        );
        return;
    }
    readNews() {
        let data = this.state.data;
        if (!data || !data.hasOwnProperty('content')) {
            message.info('暂无可朗读内容');
            return;
        }
        let content: any = filterHTMLTag(data.content);
        if (content.length === 0) {
            message.info('暂无可朗读内容');
            return;
        }
        // if (window.SpeechSynthesisUtterance) {
        //     const msg = new SpeechSynthesisUtterance();
        //     msg.text = content;
        //     speechSynthesis.speak(msg);
        // }
        if (document && document.getElementById('ny_audio_id')) {
            document.getElementById('ny_audio_id')!['play']();
        } else {
            message.info('语音朗读失败');
            return;
        }
    }

    getimgsrc(html: any) {
        let reg = /<img.+?src=('|")?([^'"]+)('|")?(?:\s+|>)/gim;
        // console.log(reg.exec(html), '拿到的图片列表');
        let arr = [];
        let tem: any = [];
        while (tem = reg.exec(html)) {
            arr.push(tem[2]);
        }
        // console.log(arr, '拿到的图片列表');
        return arr;
    }
    render() {
        let { data, user, isAudit, isLogin, comment_list, dataSource } = this.state;
        const AppPermission = JSON.parse(localStorage.getItem(LOCAL_FUNCTION_LIST)!);
        const comment = (owData: any, sectionID: any, rowID: any) => {
            return (
                <Card>
                    <Row type='flex' justify='center'>
                        <Col className='list-col' span={3}><WhiteSpace size='sm' /><Avatar size='large' icon="user" /></Col>
                        <Col span={20} className='list-col'>
                            <WhiteSpace size='sm' />
                            <Row>
                                <Col span={12}><strong>{owData.comment_user}</strong></Col>
                                <Col span={12}>{owData.comment_date}</Col>
                            </Row>
                            <Row>
                                {owData.content}
                            </Row>
                        </Col>
                    </Row>
                </Card>
            );
        };
        return (
            <Row>
                <NavBar
                    className={data && data['title'] && data['title'].length > 11 ? "ny-news-info start" : "ny-news-info"}
                    icon={<Icon type={"left"} onClick={() => history.back()} />}
                    rightContent={isLogin && ((user && user['id'] && data && data['promulgator_id'] && user['id'] === data['promulgator_id']) || (AppPermission && getAppPermissonObject(AppPermission, '新闻管理', '编辑'))) ? <Row type="flex" justify="end" onClick={() => this.props.history!.push(ROUTE_PATH.changeNews + '/' + this.props.match!.params.key)}>编辑</Row> : null}
                >
                    {
                        data && data['title'] ? data['title'] : ''
                    }
                </NavBar>
                <Card>
                    <Card.Body className="newsInfoBox">
                        <Row style={{ textAlign: 'center', fontSize: '20px', fontWeight: 'bold', marginBottom: '20px', }}>
                            {data.title}
                        </Row>
                        {
                            data.subhead
                            &&
                            (
                                <Row style={{ textAlign: 'center', fontSize: '16px', color: "#999999" }}>
                                    {data.subhead}
                                </Row>
                            )
                        }
                        {data && data.hasOwnProperty('content') ?
                            <Row type="flex" justify="end">
                                <Button type="primary" onClick={() => this.readNews()}>语音朗读</Button>
                                <audio id="ny_audio_id">
                                    <source
                                        src={`http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=5&per=1&text=${filterHTMLTag(data.content)}`}
                                    />
                                </audio>
                            </Row> : null}
                        <hr style={{ border: 'none', height: 1, backgroundColor: "#979797" }} />
                        <Row style={{ fontSize: 14 }} type={'flex'} justify={'space-between'}>
                            <Col>
                                {data.org_name}
                            </Col>
                            <Col style={{ color: '#999999' }}>
                                {data.issue_date && data.issue_date.substring(0, 10)}
                            </Col>
                        </Row>
                        <WhiteSpace size="lg" />
                        <Row className="ny-htmlbody" dangerouslySetInnerHTML={{ __html: data.content! }} />
                    </Card.Body>
                </Card>
                <Card className='list-conten' id="pl">
                    <Card.Header
                        title="评论"
                    />
                    <Row className='tabs-content'>
                        {
                            comment_list && comment_list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(comment_list)}
                                    renderRow={comment}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: comment_list!.length * 100 }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无评论
                                    </Row>
                        }
                    </Row>
                </Card>
                {data.status === '通过' ? <Card className='list-conten send-card'>
                    <Card.Header
                        title="发表评论"
                    />
                    <Row>
                        <Row>
                            <TextareaItem
                                autoHeight={true}
                                rows={4}
                                labelNumber={5}
                                placeholder={isLogin ? "" : '请先登录后进行评论'}
                                onChange={this.textAreaChange}
                                value={this.state.comment_value}
                            />
                        </Row>
                        <WhiteSpace size="xs" />
                        <Row type="flex" justify="center" className='send' onClick={this.commentsubmit}>
                            <Button className="btnW950" type="primary">发送</Button>
                        </Row>
                        <WhiteSpace size="xs" />
                    </Row>
                </Card> : null}
                <Row
                    style={{
                        position: 'fixed',
                        bottom: '0',
                        width: '100%',
                        height: '50px',
                        background: '#ffffff',
                    }}
                >
                    {isAudit && data.status === '待审批' ? <Row style={{ textAlign: 'center', lineHeight: '50px', borderRight: '1px solid #ddd' }}>
                        <Button className="btnW950" type="primary" onClick={() => this.auditOpen()}>审核</Button>
                    </Row> : data.status === '通过' ? <Row>
                        <Col span={8} style={{ textAlign: 'center', lineHeight: '50px', borderRight: '1px solid #ddd' }}>
                            <span className={data.is_likes ? 'font-red' : ''} style={{ marginRight: '10px', fontSize: '20px' }} onClick={() => this.ctrlThis('likes')}>
                                <Icon type="like" style={{ marginRight: '5px' }} />点赞：{data.likes_count}
                            </span>
                        </Col>
                        <Col span={8} style={{ textAlign: 'center', lineHeight: '50px', borderRight: '1px solid #ddd' }}>
                            <span className={data.is_collection ? 'font-red' : ''} style={{ marginRight: '10px', fontSize: '20px' }} onClick={() => this.ctrlThis('collection')}>
                                <Icon type="file" style={{ marginRight: '5px' }} />收藏：{data.collection_count}
                            </span>
                        </Col>
                        <Col span={8} style={{ textAlign: 'center', lineHeight: '50px' }}>
                            {/* <span className={data.is_share ? 'font-red' : ''} style={{ marginRight: '10px', fontSize: '20px' }} onClick={() => this.ctrlThis('share')}> */}
                            <CopyToClipboard text={window.location.href} onCopy={() => { Toast.info("已复制分享链接"); }}>
                                <Row style={{ fontSize: '20px' }}>
                                    <Icon type="branches" style={{ marginRight: '5px' }} />分享
                                    </Row>
                            </CopyToClipboard>
                        </Col>
                    </Row> : null}
                </Row>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
            </Row >
        );
    }
}

/**
 * 控件：新闻详情页面控制器
 * 新闻详情
 */
@addon('NewsInfoView', '新闻详情页面', '新闻详情')
@reactControl(NewsInfoView, true)
export class NewsInfoViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}