import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { Row, Button, Icon } from "antd";
import { remote } from "src/projects/remote";
/**
 * 组件：app下载状态
 */
export interface DownloatAppState extends BaseReactElementState {
    /** 是否微信浏览器 */
    is_weixin?: boolean;
    /** 是否安卓系统 */
    is_android?: boolean;
    /** 是否ios系统 */
    is_ios?: boolean;
}
/**
 * 组件：app下载
 * app下载
 */
export class DownloatAppView extends BaseReactElement<DownloatAppViewControl, DownloatAppState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_android: false,
            is_weixin: false,
            is_ios: false,
        };
    }
    // 判断是否微信浏览
    isWeiXin() {
        let ua = window.navigator.userAgent.toLowerCase();
        // @ts-ignore
        if (ua.indexOf('micromessenger') > 0) {
            return true;

        } else {
            return false;
        }
    }
    componentDidMount() {
        let is_weixin = this.isWeiXin();
        if (is_weixin) {
            this.setState({
                is_weixin: true,
            });
        }

        let u = navigator.userAgent;
        let isXiaomi = u.indexOf('XiaoMi') > -1;
        let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
        let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
        if (isAndroid) {
            this.setState({
                is_android: true,
                is_ios: false,
            });
        } else if (isIOS) {
            if (isXiaomi) {
                this.setState({
                    is_android: true,
                    is_ios: false,
                });
            } else {
                this.setState({
                    is_android: false,
                    is_ios: true,
                });
            }
        }
    }
    render() {
        return (
            <div style={{ height: '100%' }}>
                <Row
                    type='flex'
                    justify='center'
                    align='middle'
                >
                    <img src={require("../../../../static/img/download.png")} />
                </Row>
                <Row
                    type='flex'
                    justify='center'
                    align='middle'
                >
                    <div style={{ marginTop: '20px', fontSize: '20px', fontWeight: 'bold' }}>智慧康养</div>
                </Row>
                <Row
                    type='flex'
                    justify='center'
                    align='middle'
                    style={{
                        paddingTop: '20px',
                        display: this.state.is_android ? 'flex' : 'none'
                    }}
                >
                    <a href={remote.path + '/build/upload/zhyl.apk'}><Button type='primary'>安卓下载</Button></a>
                </Row>
                <Row

                    type='flex'
                    justify='center'
                    align='middle'
                    style={{
                        paddingTop: '20px',
                        display: this.state.is_ios ? 'flex' : 'none'

                    }}
                >
                    <a href={remote.path + '/build/index.html'}><Button type='primary'>苹果下载</Button></a>
                </Row>
                <div
                    style={{
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                        top: 0,
                        bottom: 0,
                        background: 'rgba(0,0,0,0.5)',
                        color: '#ffffff',
                        display: this.state.is_weixin ? 'block' : 'none',
                        // display: 'block'
                    }}
                >
                    <span style={{ fontSize: "24px", width: '80%', display: 'inline-block', textAlign: 'center', marginTop: "20px", }}>请点击右上角</span>
                    <span style={{ fontSize: "24px", width: '80%', display: 'inline-block', textAlign: 'center', }}>选择在浏览器打开</span>
                    <span style={{ fontSize: "18px", width: '80%', display: 'inline-block', textAlign: 'center', }}>（苹果用户请选择safari浏览器）</span>
                    <span style={{ display: "inline-block", position: 'absolute', right: '15px', top: '0' }}>
                        <Icon style={{ fontSize: '28px' }} type="arrow-up" />
                    </span>
                </div>
            </div>

        );
    }
}

/**
 * 控件：app下载控制器
 * app下载
 */
@addon('DownloatAppView', 'app下载', 'app下载')
@reactControl(DownloatAppView, true)
export class DownloatAppViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}