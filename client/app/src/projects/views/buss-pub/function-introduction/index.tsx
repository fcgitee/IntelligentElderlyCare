import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Carousel } from "antd-mobile";
import './index.less';

/**
 * 组件：功能介绍状态
 */
export interface FunctionIntroductionViewState extends BaseReactElementState {
}
/**
 * 组件：功能介绍
 * 功能介绍
 */
export class FunctionIntroductionView extends BaseReactElement<FunctionIntroductionViewControl, FunctionIntroductionViewState> {
    constructor(props: any) {
        super(props);
    }
    componentDidMount() {

    }
    render() {
        return (
            <div className='function-introduction' style={{ height: '100%' }}>
                <Carousel
                    autoplay={false}
                >
                    <div style={{ height: '100%' }}>
                        <img style={{ width: '100%', height: '100%' }} src={require('./fn1.png')} />
                    </div>
                    <div style={{ height: '100%' }}>
                        <img style={{ width: '100%', height: '100%' }} src={require('./fn2.png')} />
                    </div>
                    <div style={{ height: '100%' }}>
                        <img style={{ width: '100%', height: '100%' }} src={require('./fn3.png')} />
                    </div>
                    <div style={{ height: '100%' }}>
                        <img style={{ width: '100%', height: '100%' }} src={require('./fn4.png')} />
                        <div style={{ position: 'absolute', bottom: '20px', width: '100%', textAlign: 'center', padding: "0 20px" }}>
                            <div onClick={() => this.props.history!.goBack()} style={{ background: '#ff9900', color: '#fff', height: '40px', lineHeight: '40px', borderRadius: '5px' }}>完成</div>
                        </div>

                    </div>
                </Carousel>
            </div>
        );
    }
}

/**
 * 控件：功能介绍控制器
 * 功能介绍
 */
@addon('FunctionIntroductionView', '功能介绍', '功能介绍')
@reactControl(FunctionIntroductionView, true)
export class FunctionIntroductionViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}