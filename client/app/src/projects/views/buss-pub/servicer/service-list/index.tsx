import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView, Card, Tabs, DatePicker, List, WhiteSpace } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row, Col, Button, Spin, } from "antd";
import './index.less';
import { request } from "src/business/util_tool";
import Search from "antd/lib/input/Search";
import moment from "moment";
// const Item = List.Item;
// const Brief = Item.Brief;

const tabs = [
    { title: '待接收' },
    { title: '服务中' },
    { title: '已完成' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

/**
 * 组件：服务列表状态
 */
export interface ServiceListViewState extends BaseReactElementState {
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    /** 记录tab状态 */
    tab_status?: string;
    /** 已完成 */
    do_list?: any[];
    /** 日期 */
    date?: any;
    /** 姓名或身份证 */
    name_or_id_card?: string;
    empty: any;
    animating: any;
    tab: any;
    /** 弹窗显示 */
    is_back_visible?: boolean;
    /** 撤回开始的服务记录 */
    back_record?: any;
}

/**
 * 组件：服务列表
 * 服务列表
 */
export class ServiceListView extends BaseReactElement<ServiceListViewControl, ServiceListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            do_list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'participate',
            name_or_id_card: '',
            empty: false,
            animating: true,
            tab: tabs[0]['title'],
            is_back_visible: false,
            back_record: {}
        };
    }
    componentDidMount() {
        this.getData();
    }
    onClickRow = (id: string) => {
        this.props.history!.push(ROUTE_PATH.servicerServiceDetails + '/' + id);
    }
    onBackClick = (back_record: any) => {
        // console.log('back_record>>', back_record);
        this.setState({
            back_record: back_record,
            // is_back_visible: true,
        });
    }
    handleCancel = () => {
        this.setState({
            is_back_visible: false,
        });
    }
    startBack = (e: any) => {
        // 调用接口
    }
    onSeeDetiles = (id: string) => {
        this.props.history!.push(ROUTE_PATH.servicerOrderDetails + '/' + id);
    }
    dateChange = (date: any) => {
        this.setState(
            {
                date,
            },
            () => {
                this.getData();
            }
        );
    }
    tabClick(e: any) {
        this.setState(
            {
                tab: e['title'],
            },
            () => {
                this.getData();
            }
        );
    }
    search = (value: any) => {
        this.setState(
            {
                name_or_id_card: value
            },
            () => {
                this.getData();
            }
        );
    }
    getData() {
        let { date, tab, name_or_id_card } = this.state;
        this.setState(
            {
                animating: true,
                list: [],
                empty: false,
            },
            () => {
                let param = {};
                if (name_or_id_card) {
                    param['name_or_id_card'] = name_or_id_card;
                }
                if (date) {
                    param['order_date'] = date;
                }
                if (tab) {
                    // 待接收对应服务订单的未服务，服务任务的待接收
                    if (tab === '待接收') {
                        tab = '未服务';
                    }
                    param['status'] = tab;
                }
                request(this, AppServiceUtility.service_record_service.get_service_record_list_all_app!(param))
                    .then((data: any) => {
                        if (data && data.result.length > 0) {
                            this.setState({
                                list: data!.result.reverse(),
                                animating: false,
                            });
                        } else {
                            this.setState({
                                animating: false,
                                empty: true,
                            });
                        }
                    })
                    .catch((err) => {
                        console.info(err);
                    });
            }
        );
    }
    render() {
        const { list, dataSource, animating, empty, tab } = this.state;
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card full={false} className='list-conten' style={{ padding: '10px 10px', margin: '10px 0', }}>
                        <Card.Header title={owData.service_provider_name} />
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={9} style={{ marginRight: '5pt' }}><img src={owData.service_product_img && owData.service_product_img.length > 0 ? owData.service_product_img[0] : require("src/static/img/no_picture.jpg")} style={{ height: '72pt', width: '98%' }} /></Col>
                            <Col span={14} className='list-col'>
                                <Row style={{ fontSize: '12px' }}>{owData.service_order_code}</Row>
                                <Row><strong>{owData.service_project_name}</strong></Row>
                                <Row>
                                    {owData.purchaser_name}
                                    <span style={{ float: 'right', }}>
                                        {
                                            moment(owData.create_date).month() === moment(owData.now_date).month() ? owData.task_state : '已过期'
                                        }
                                    </span>
                                </Row>
                                <Row style={{ fontSize: '12px' }}>订单时间：{owData.create_date}</Row>
                            </Col>
                        </Row>
                        <Row>
                            {/* 以后需要限制当前月时候，再放开这段{moment(owData.create_date).month() === moment(owData.now_date).month() ? <Button style={{ float: 'right', }} onClick={this.onClickRow.bind(this, owData.id)} type='primary'>开始执行</Button> : null} */}
                            <Button style={{ float: 'right', }} onClick={this.onClickRow.bind(this, owData.id)} type='primary'>开始执行</Button>

                        </Row>
                        {/* <Card.Footer extra={<Button onClick={this.onClickRow.bind(this, owData.id)} type='primary'>开始执行</Button>} /> */}
                    </Card>
                </div>
            );
        };
        const renderRow2 = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card full={false} className='list-conten' style={{ padding: '10px 10px', margin: '10px 0', }}>
                        <Card.Header title={owData.service_provider_name} />
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={10}><img src={owData.service_product_img && owData.service_product_img.length > 0 ? owData.service_product_img[0] : require("src/static/img/no_picture.jpg")} style={{ height: '72pt', width: '98%' }} /></Col>
                            <Col span={14} className='list-col'>
                                <Row style={{ fontSize: '12px' }}>{owData.service_order_code}</Row>
                                <Row><strong>{owData.service_project_name}</strong></Row>
                                <Row>
                                    {owData.purchaser_name}<span style={{ float: 'right', }}>{owData.status}</span>
                                </Row>
                                <Row style={{ fontSize: '12px' }}>订单时间：{owData.create_date}</Row>
                                <Row style={{ fontSize: '12px' }}>服务开始时间：{owData.start_date}</Row>
                                <Row style={{ fontSize: '12px' }}>服务结束时间：{owData.end_date}</Row>
                            </Col>
                        </Row>
                        <Row>
                            <Button style={{ float: 'right', }} onClick={this.onClickRow.bind(this, owData.id)} type='primary'>继续执行</Button>
                        </Row>
                    </Card>
                </div>
            );
        };
        const renderRow3 = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card full={false} className='list-conten' style={{ padding: '10px 10px', margin: '10px 0', }}>
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={10}><img src={owData.service_product_img && owData.service_product_img.length > 0 ? owData.service_product_img[0] : require("src/static/img/no_picture.jpg")} style={{ height: '72pt', width: '98%' }} /></Col>
                            <Col span={14} className='list-col'>
                                <Row style={{ fontSize: '12px' }}>{owData.service_order_code}</Row>
                                <Row><strong>{owData.service_project_name}</strong></Row>
                                <Row>
                                    {owData.purchaser_name}<span style={{ float: 'right', }}>{owData.task_state}</span>
                                </Row>
                                <Row style={{ fontSize: '12px' }}>订单时间：{owData.create_date}</Row>
                                <Row style={{ fontSize: '12px' }}>服务开始时间：{owData.start_date}</Row>
                                <Row style={{ fontSize: '12px' }}>服务结束时间：{owData.end_date}</Row>
                            </Col>
                        </Row>
                        <Row>
                            {owData.service_attitude && owData.service_quality ? <Button style={{ float: 'right', }} type='primary' onClick={this.onSeeDetiles.bind(this, owData.id)}>查看评价</Button> : <Button style={{ float: 'right', }} disabled={true}>待用户评价</Button>}
                            <Button style={{ float: 'right', }} onClick={this.onClickRow.bind(this, owData.id)} type='primary'>查看详情</Button>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                <div style={{ height: '50px', }}>
                    <Search
                        placeholder="姓名/身份证"
                        onSearch={this.search}
                        style={{ width: '100%', height: '40px', marginTop: '3px' }}
                    />
                </div>
                <List>
                    <DatePicker
                        mode="date"
                        title="日期"
                        extra="选择日期"
                        value={this.state.date}
                        onChange={this.dateChange}
                    >
                        <List.Item arrow="horizontal">日期</List.Item>
                    </DatePicker>
                </List>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                    onTabClick={(e) => this.tabClick(e)}
                >
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list.reverse())}
                                    renderRow={tab === '待接收' ? renderRow : tab === '服务中' ? renderRow2 : renderRow3}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                : null
                        }
                        {animating ? <Row>
                            <WhiteSpace size="lg" />
                            <Row style={{ textAlign: 'center' }}>
                                <Spin size="large" />
                            </Row>
                            <WhiteSpace size="lg" />
                        </Row> : null}
                        {empty ? <Row>
                            <WhiteSpace size="lg" />
                            <Row className='tabs-content' type='flex' justify='center'>{list && list.length ? '已经是最后一条了' : '无数据'}</Row>
                            <WhiteSpace size="lg" />
                        </Row> : null}
                    </div>
                </Tabs>
            </Row>
        );
    }
}

/**
 * 控件：服务列表控制器
 * 服务列表
 */
@addon('ServiceListView', '服务列表', '服务列表')
@reactControl(ServiceListView, true)
export class ServiceListViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}