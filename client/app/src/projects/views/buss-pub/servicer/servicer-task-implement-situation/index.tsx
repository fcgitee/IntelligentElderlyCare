import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, DatePicker, InputItem, TextareaItem } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Row, Col, } from "antd";
import './index.less';
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { request } from "src/business/util_tool";
import { remote } from "src/projects/remote";
/**
 * 组件：服务人员任务执行情况状态
 */
export interface ServicerTaskImplementSituationViewState extends BaseReactElementState {
    /** 请求的数据 */
    list?: any[];
}

/**
 * 组件：服务人员任务执行情况
 * 服务人员任务执行情况
 */
export class ServicerTaskImplementSituationView extends BaseReactElement<ServicerTaskImplementSituationViewControl, ServicerTaskImplementSituationViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            list: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.service_record_service.get_service_record_list_all!({ user_filter: true }))
            .then((data: any) => {
                this.setState({
                    list: data!.result.reverse()
                });
            })
            .catch((err) => {
                console.info(err);
            });
    }
    onClickRow = (id: string) => {
        // console.info(id);
        this.props.history!.push(ROUTE_PATH.servicerServiceDetails + '/' + id);
    }
    render() {
        return (
            <Row>
                <List className="my-list">

                    <DatePicker
                        mode="datetime"
                        title="服务开始时间："
                    >
                        <List.Item arrow="horizontal">服务开始时间：</List.Item>
                    </DatePicker>
                    <DatePicker
                        mode="datetime"
                        title="服务结束时间："
                    >
                        <List.Item arrow="horizontal">服务结束时间：</List.Item>
                    </DatePicker>
                    <InputItem
                        defaultValue={''}
                        moneyKeyboardAlign="left"
                    >
                        服务前位置：
                    </InputItem>
                    <InputItem
                        defaultValue={''}
                        moneyKeyboardAlign="left"
                    >
                        服务后位置：
                    </InputItem>
                </List>
                <Row style={{ paddingLeft: '20px' }}>
                    <Col span={12}>
                        <span style={{ marginBottom: '10px' }}>服务开始照片（大小小于2M，格式支持jpg/jpeg/png）</span>
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} />
                    </Col>
                    <Col span={12}>
                        <span style={{ marginBottom: '10px' }}>服务结束照片（大小小于2M，格式支持jpg/jpeg/png）</span>
                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} />
                    </Col>
                </Row>
                <List renderHeader={() => '执行情况'}>
                    <TextareaItem placeholder="请输入内容" rows={3} autoHeight={true} />
                </List>
                <div
                    style={{
                        position: 'fixed',
                        bottom: '0',
                        height: '50px',
                        left: '0',
                        right: '0',
                        textAlign: 'center',
                        lineHeight: '50px',
                        background: 'blue',
                        color: '#fff',
                        fontSize: '18px',
                        zIndex: 99,
                    }}
                >
                    确定
                </div>
            </Row>
        );
    }
}

/**
 * 控件：服务人员任务执行情况控制器
 * 服务人员任务执行情况
 */
@addon('ServicerTaskImplementSituationView', '服务人员任务执行情况', '服务人员任务执行情况')
@reactControl(ServicerTaskImplementSituationView, true)
export class ServicerTaskImplementSituationViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}