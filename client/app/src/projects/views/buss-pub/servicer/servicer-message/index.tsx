import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Row, } from "antd";
import { Tabs, ListView, Card, } from "antd-mobile";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
const tabs = [
    { title: '消息通知' },
    { title: '离床消息' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：服务消息视图控件状态
 */
export interface ServicerMessageViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
}
/**
 * 组件：服务消息视图控件
 */
export class ServicerMessageView extends ReactView<ServicerMessageViewControl, ServicerMessageViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
        };
    }
    componentDidMount() {
        AppServiceUtility.my_order_service.my_order_list!({})!
            .then((data) => {
                this.setState({
                    list: data.result
                });
            })
            .catch((err) => {
                console.info(err);
            });
    }
    onClickRow = (id: string) => {
        // this.props.history!.push(ROUTE_PATH.orderDetail + '/' + id);
    }
    render() {
        const { list, dataSource, } = this.state;
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten'>
                        <Card.Header
                            title="服务待执行"
                            extra={<span>2019-11-02 00：00</span>}
                        />
                        <Card.Body>
                            <div>您有一个助浴服务的订单待执行</div>
                        </Card.Body>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                >
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div>
                    <div className='tabs-content'>
                        {/* {
                            will_service && will_service.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(will_service)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        } */}
                        <Row className='tabs-content' type='flex' justify='center'>
                            暂无数据
                        </Row>
                    </div>
                </Tabs>
            </Row>
        );
    }
}

/**
 * 组件：服务消息视图控件
 * 控制服务消息视图控件
 */
@addon('ServicerMessageView', '服务消息视图控件', '控制服务消息视图控件')
@reactControl(ServicerMessageView, true)
export class ServicerMessageViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}