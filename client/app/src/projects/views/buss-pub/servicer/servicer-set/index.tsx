import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { List, Switch, Button } from "antd-mobile";
import './index.less';
import { ROUTE_PATH } from "src/projects/app/util-tool";
const Item = List.Item;
/**
 * 组件：服务人员设置视图控件状态
 */
export interface ServicerSetViewState extends ReactViewState {

}
/**
 * 组件：服务人员设置视图控件
 */
export class ServicerSetView extends ReactView<ServicerSetViewControl, ServicerSetViewState> {
    constructor(props: any) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }
    login = () => {
        this.props.history!.push(ROUTE_PATH.login);
    }
    render() {
        return (
            <div>
                <List className='servicer-set'>
                    <div style={{ margin: '10px 20px', }}>
                        视频功能
                    </div>

                    <Item
                        extra={<Switch />}
                    >
                        接受视频通话
                    </Item>
                    <div style={{ margin: '10px 20px' }}>
                        同步功能
                    </div>

                    <Item
                        extra={<Switch />}
                    >
                        自动同步
                    </Item>
                    <Item
                        extra={<Switch />}
                    >
                        仅在WIFI下同步
                    </Item>
                </List>
                <div style={{ position: 'fixed', bottom: '0', width: '100%', height: '50px' }}>
                    <Button type='primary' onClick={this.login}>切换账号</Button>
                </div>
            </div>
        );
    }
}

/**
 * 组件：服务人员设置视图控件
 * 控制服务人员设置视图控件
 */
@addon('ServicerSetView', '服务人员设置视图控件', '控制服务人员设置视图控件')
@reactControl(ServicerSetView, true)
export class ServicerSetViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}