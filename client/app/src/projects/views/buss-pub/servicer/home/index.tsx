import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Avatar } from "antd";
import Grid from "antd-mobile/lib/grid";
import './index.less';
import { CarouselData } from "src/models/carousel";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { FontIcon } from "src/projects/components/icon";
import { getLocation } from "src/business/models/location";
/**
 * 组件：服务人员首页视图控件状态
 */
export interface ServicerHomeViewState extends ReactViewState {
    /** 轮播数据 */
    data?: CarouselData;
    /** 图标数据 */
    icon_data?: any[];
    /** 请求的数据 */
    list?: any[];
    /** 长列表数据 */
    dataSource?: any;
}
/**
 * 组件：服务人员首页视图控件
 */
export class ServicerHomeView extends ReactView<ServicerHomeViewControl, ServicerHomeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            list: [],
            icon_data: [{ text: '上门服务', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan18" /> },
            { text: '消息通知', icon: <FontIcon style={{ fontSize: '40px' }} type="iconziyuan19" /> },]
        };
    }
    componentDidMount() {

    }
    grid_click = (_el: any) => {
        switch (_el.text) {
            case '上门服务':
                this.props.history!.push(ROUTE_PATH.servicerTaskList);
                break;
            case '消息通知':
                this.props.history!.push(ROUTE_PATH.servicerMessage);
                break;
            default:
                break;
        }
    }
    // 获取经纬度
    location = (position: any) => {
        // alert(position.coords.longitude);
    }
    setInfo = () => {
        this.props.history!.push(ROUTE_PATH.servicerSet);
    }
    render() {
        getLocation(this.location);
        return (
            <div>
                <div style={{ display: 'flex', height: '200px', alignItems: 'center', justifyContent: 'center', }}>
                    <div style={{ textAlign: 'center' }} onClick={this.setInfo}>
                        <Avatar size={64} icon="user" />
                        <p style={{ fontSize: '20px', fontWeight: 'bold' }}>admin</p>
                    </div>

                </div>
                <Grid data={this.state.icon_data} activeStyle={true} columnNum={2} onClick={_el => this.grid_click(_el)} />
            </div >
        );
    }
}

/**
 * 组件：服务人员首页视图控件
 * 控制服务人员首页视图控件
 */
@addon('ServicerHomeView', '服务人员首页视图控件', '控制服务人员首页视图控件')
@reactControl(ServicerHomeView, true)
export class ServicerHomeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}