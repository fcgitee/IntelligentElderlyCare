import { Button, Col, Row, Modal } from "antd";
import { Card, DatePicker, InputItem, List, TextareaItem, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { request, beforUploadCompress } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from "src/projects/remote";
import './index.less';

// let moment = require('moment');
/**
 * 组件：服务详情状态
 */
export interface ServicerServiceDetailsViewState extends BaseReactElementState {
    /** 请求的数据 */
    data_info?: any;
    // 开始的图片
    begin_photo?: any;
    end_photo?: any;
    // 步骤状态标识
    stepStatus?: any;
    // 开始时间
    start_date?: any;
    // 结束时间
    end_date?: any;
    // 
    start_date_str?: any;
    end_date_str?: any;
    // 执行情况说明
    remarks?: string;
    // 服务前位置
    start_position?: string;
    // 服务后位置
    end_position?: string;
    // 
    is_send?: boolean;
    // 时间戳
    start_time_stamp?: number;
    end_time_stamp?: number;
    isStart?: boolean;
    // 控制确认弹框是否显示
    is_visible?: boolean;
    // 操作的方法类型
    flag?: string;
}
/**
 * 组件：服务详情
 * 服务详情
 */
export class ServicerServiceDetailsView extends BaseReactElement<ServicerServiceDetailsViewControl, ServicerServiceDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: [],
            begin_photo: [],
            end_photo: [],
            stepStatus: 0,
            start_date: '',
            end_date: '',
            start_date_str: '',
            end_date_str: '',
            remarks: '',
            start_position: '',
            end_position: '',
            is_send: false,
            start_time_stamp: 0,
            end_time_stamp: 0,
            isStart: true,
            is_visible: false
        };
    }
    componentDidMount() {
        this.get_data();
    }
    get_data() {
        let id = this.props.match!.params.key;
        request(this, AppServiceUtility.service_record_service.get_service_record_list_all_app!({ user_filter: true, id: id }, 1, 1))
            .then((datas: any) => {
                let data_info = datas.result && datas.result[0] ? datas.result[0] : [];
                let stepStatus = 0;
                if (data_info.hasOwnProperty('task_state')) {
                    if (data_info['task_state'] && data_info['task_state'] === '待接收') {
                        // 待接收的时候，开始必填，完成选填
                        stepStatus = 1;
                    } else if (data_info['task_state'] && data_info['task_state'] === '服务中') {
                        // 进行中的时候，必须填完成
                        stepStatus = 2;
                    }
                }
                let end_date_str = '';
                if (data_info.hasOwnProperty('end_date')) {
                    end_date_str = data_info['end_date'];
                }
                let start_date_str = '';
                if (data_info.hasOwnProperty('start_date')) {
                    start_date_str = data_info['start_date'];
                }
                this.setState({
                    stepStatus,
                    data_info,
                    start_time_stamp: data_info['start_date'] ? new Date(data_info['start_date']).getTime() : 0,
                    begin_photo: data_info['begin_photo'] ? data_info['begin_photo'] : [],
                    end_photo: data_info['end_photo'] ? data_info['end_photo'] : [],
                    start_date_str,
                    end_date_str,
                    start_date: data_info['start_date'],
                    end_date: data_info['end_date'],
                    start_position: data_info['start_position'],
                    end_position: data_info['end_position']
                });
            })
            .catch((error) => {
                console.info(error);
            });
    }
    startUpImg = (e: any) => {
        this.setState({
            begin_photo: e
        });
    }

    endUpImg = (e: any) => {
        this.setState({
            end_photo: e
        });
    }
    onBackClick = () => {
        if (this.state.is_send === true) {
            Toast.fail('请勿重复操作！');
            return;
        }
        Toast.info('正在提交中...', 2);
        let { data_info } = this.state;
        let record_id = data_info['id'];
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.task_service.back_start_service_order_record!(record_id))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            Toast.success('操作成功', 1, () => {
                                history.back();
                            });
                        } else {
                            Toast.fail(datas);
                            this.setState({
                                is_send: false,
                                is_visible: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                            is_visible: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    // 服务开始/结束方法
    saveTime = (type: string) => {
        if (this.state.is_send === true) {
            Toast.fail('请勿重复操作！');
            return;
        }
        Toast.info('正在提交中...', 2);
        let { data_info } = this.state;
        let param: any = {
            // 这里发过去要任务的ID
            id: data_info['task_id'] && data_info['task_id'] ? data_info['task_id'] : '',
            order_id: data_info['order_id'],
            type: type
        };
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.task_service.save_time_service_order!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            Toast.success('操作成功', 1, () => {
                                this.get_data();
                                this.setState({
                                    is_send: false,
                                    is_visible: false,
                                });
                            });
                        } else {
                            Toast.fail(datas);
                            this.setState({
                                is_send: false,
                                is_visible: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                            is_visible: false,
                        });
                        // console.log(error);
                    });
            });

    }
    // 提交照片
    sumUP = () => {
        Toast.info('正在提交中...', 2);
        let { data_info, start_date, start_date_str, end_date, end_date_str, begin_photo, end_photo, is_send, stepStatus, remarks, start_position, end_position } = this.state;
        if (is_send === true) {
            Toast.fail('请勿重复操作！');
            return;
        }
        // 待接收/服务中
        if (stepStatus === 1) {
            if (!start_date) {
                Toast.fail('请选择服务开始时间');
                return;
            }
            if (begin_photo.length === 0) {
                Toast.fail('至少上传服务开始图片！');
                return;
            }
        }
        // 服务中/服务中
        if (stepStatus === 2) {
            if (!end_date) {
                Toast.fail('请选择服务完成时间');
                return;
            }
            if (end_photo.length === 0) {
                Toast.fail('请上传服务结束图片！');
                return;
            }
        }
        if (begin_photo.length === 0) {
            Toast.fail('请上传服务开始图片');
            return;
        }
        if (!data_info['task_id']) {
            Toast.fail('未知错误，请刷新重试！');
            return;
        }
        let param: any = {
            // 这里发过去要任务的ID
            id: data_info['task_id'] && data_info['task_id'] ? data_info['task_id'] : '',
            order_id: data_info['order_id'],
            begin_photo: begin_photo.length > 0 ? begin_photo : '',
            end_photo: end_photo.length > 0 ? end_photo : '',
            start_date: start_date_str || '',
            end_date: end_date_str || '',
            remarks: remarks || '',
            start_position: start_position || '',
            end_position: end_position || '',
            is_app: true
        };
        // console.log(param);
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.task_service.change_task_service_order!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            Toast.success('保存成功', 1, () => {
                                history.back();
                            });
                        } else {
                            Toast.fail(datas);
                            this.setState({
                                is_send: false,
                                is_visible: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                            is_visible: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    selectStartDate = (e: any) => {
        let data_info = this.state.data_info;
        data_info.start_date = this.dateFormat(e);
        this.setState({
            start_date_str: this.dateFormat(e),
            start_date: e,
            start_time_stamp: e.getTime(),
            data_info: data_info
        });
    }
    selectEndDate = (e: any) => {
        let start_time_stamp = this.state.start_time_stamp!;
        if (start_time_stamp === 0) {
            Toast.fail('请先选择服务开始时间！');
            return;
        }
        if (e.getTime() <= start_time_stamp) {
            Toast.fail('服务结束时间必须在开始时间之后！');
            return;
        }
        let data_info = this.state.data_info;
        data_info.end_date = this.dateFormat(e);
        this.setState({
            end_date_str: this.dateFormat(e),
            end_date: e,
            end_time_stamp: e.getTime(),
            data_info: data_info
        });
    }
    dateFormat(d: any) {
        var date = (d.getFullYear()) + "-" +
            ((d.getMonth() + 1) + '').padStart(2, '0') + "-" +
            (d.getDate() + '').padStart(2, '0') + " " +
            (d.getHours() + '').padStart(2, '0') + ":" +
            (d.getMinutes() + '').padStart(2, '0') + ":" +
            (d.getSeconds() + '').padStart(2, '0');
        return date;
    }
    remarks = (e: any) => {
        this.setState({
            remarks: e,
        });
    }
    startPosition = (e: any) => {
        this.setState({
            start_position: e,
        });
    }
    endPosition = (e: any) => {
        this.setState({
            end_position: e,
        });
    }
    handleCancel = () => {
        this.setState({
            is_visible: false,
            flag: ''
        });
    }
    openModal = (flag: string) => {
        this.setState({
            is_visible: true,
            flag: flag
        });
    }
    handleSubmit = () => {
        if (this.state.flag === '开始') {
            this.saveTime('start');
        }
        if (this.state.flag === '撤回开始') {
            this.onBackClick();
        }
        if (this.state.flag === '结束') {
            this.saveTime('end');
        }
        if (this.state.flag === '提交') {
            this.sumUP();
        }
    }
    render() {
        let { stepStatus, data_info, begin_photo, end_photo, remarks } = this.state;
        // console.log(stepStatus);
        return (
            <Row>
                <Modal
                    visible={this.state.is_visible}
                    onCancel={this.handleCancel}
                    cancelText='取消'
                    onOk={this.handleSubmit}
                    okText='确认'
                    width={300}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <p>是否确认操作</p>
                </Modal>
                <Card className='list-conten' style={{ padding: '10px 10px', margin: '10px 0', }}>
                    <Row type='flex' justify='center'>
                        <Col className='list-col' span={10}><img src={data_info.service_product_img && data_info.service_product_img.length > 0 ? data_info.service_product_img[0] : require("src/static/img/no_picture.jpg")} style={{ height: '72pt', width: '98%' }} /></Col>
                        <Col span={14} className='list-col'>
                            <Row><strong>{data_info.service_project_name}</strong></Row>
                            <Row>
                                {data_info.purchaser_name}<span style={{ float: 'right', }}>{data_info.status === '服务中' ? data_info.status : data_info.task_state}</span>
                            </Row>
                            <Row>{data_info.create_date}</Row>
                            <Row>{data_info.service_provider_name}</Row>
                        </Col>
                    </Row>
                </Card>
                <Card>
                    <Card.Header
                        title="任务信息"
                    />
                    <Card.Body>
                        <Row>
                            服务工单编号：{data_info.record_code ? data_info.record_code : ''}
                        </Row>
                        <Row>
                            任务地址：{data_info.task_address ? data_info.task_address : ''}
                        </Row>
                        <Row>
                            备注：{data_info.task_remark ? data_info.task_remark : ''}
                        </Row>
                    </Card.Body>
                </Card>
                <Card>
                    <Card.Header
                        title="长者信息"
                    />
                    <Card.Body>
                        <Row>
                            长者姓名：{data_info.purchaser_name ? data_info.purchaser_name : ''}
                        </Row>
                        {/* <Row>
                            联系方式：{data_info.purchaser_telephone ? data_info.purchaser_telephone : ''}
                        </Row>
                        <Row>
                            长者类型：{data_info.classification_name ? data_info.classification_name : ''}
                        </Row> */}
                    </Card.Body>
                </Card>
                <List className="my-list">
                    {/* {data_info.start_date ?
                        <Row style={{ padding: '15px' }}>
                            服务开始时间：<div style={{ float: 'right' }}>{data_info.start_date}</div>
                        </Row> : <DatePicker
                            mode="datetime"
                            title="选择服务开始时间"
                            onChange={this.selectStartDate}
                        >
                            <List.Item>服务开始时间</List.Item>
                        </DatePicker>} */}
                    <DatePicker
                        mode="datetime"
                        title="选择服务开始时间"
                        onChange={this.selectStartDate}
                        value={data_info.start_date ? new Date(Date.parse(data_info.start_date.replace(/-/g, '/'))) : undefined}
                    >
                        <List.Item>服务开始时间</List.Item>
                    </DatePicker>
                    {<DatePicker
                        mode="datetime"
                        title="选择服务结束时间"
                        minDate={new Date(new Date().getFullYear(), new Date().getMonth(), 1, 0, 0)}
                        maxDate={new Date(new Date().getFullYear(), new Date().getMonth(), new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate(), 23, 59)}
                        onChange={this.selectEndDate}
                        value={data_info.end_date ? new Date(Date.parse(data_info.end_date.replace(/-/g, '/'))) : undefined}
                    >
                        <List.Item className='datePickerItem'>服务结束时间</List.Item>
                    </DatePicker>}
                    <InputItem
                        value={this.state.start_position ? this.state.start_position : data_info.task_address}
                        moneyKeyboardAlign="left"
                        onChange={this.startPosition}
                    >
                        服务前位置：
                    </InputItem>
                    <InputItem
                        value={this.state.end_position ? this.state.end_position : data_info.task_address}
                        moneyKeyboardAlign="left"
                        onChange={this.endPosition}
                    >
                        服务后位置：
                    </InputItem>
                </List>
                <Row style={{ paddingLeft: '20px' }}>
                    <Col span={12}>
                        <span style={{ marginBottom: '10px' }}>服务开始照片（大小小于2M，格式支持jpg/jpeg/png）</span>
                        <FileUploadBtn upload_amount={stepStatus === 0 ? begin_photo.length : 5} value={begin_photo} list_type={"picture-card"} contents={"plus"} beforeUpload={beforUploadCompress} action={remote.upload_url} onChange={this.startUpImg} disabled={stepStatus === 0 ? true : false} />
                    </Col>
                    <Col span={12}>
                        <span style={{ marginBottom: '10px' }}>服务结束照片（大小小于2M，格式支持jpg/jpeg/png）</span>
                        <FileUploadBtn upload_amount={stepStatus === 0 ? begin_photo.length : 5} value={end_photo} list_type={"picture-card"} contents={"plus"} beforeUpload={beforUploadCompress} action={remote.upload_url} onChange={this.endUpImg} disabled={stepStatus === 0 ? true : false} />
                    </Col>
                </Row>
                {/* <Card>
                    <Card.Header
                        title="服务开始"
                        extra={begin_photo.length > 0 ? <span>已上传</span> : <span>请拍照并上传</span>}
                    />
                    <Card.Body>
                        {this.state.data_info.start_date ?
                            <Row>
                                服务开始时间：{this.state.data_info.start_date}
                            </Row> : <DatePicker
                                mode="datetime"
                                title="选择服务开始时间"
                                onChange={this.selectStartDate}
                            >
                                <List.Item>{this.state.start_date_str ? this.state.start_date_str : '选择服务开始时间'}</List.Item>
                            </DatePicker>}
                        <br />
                        <FileUploadBtn value={begin_photo} list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.startUpImg} disabled={stepStatus === 2 || stepStatus === 0 ? true : false} />
                    </Card.Body>
                </Card> */}
                {/* <Card>
                    <Card.Header
                        title="服务结束"
                        extra={end_photo.length > 0 ? <span>已上传</span> : <span>请拍照并上传</span>}
                    />
                    <Card.Body>
                        {this.state.data_info.end_date ?
                            <Row>
                                服务结束时间：{this.state.data_info.end_date}
                            </Row> : <DatePicker
                                mode="datetime"
                                title="选择服务结束时间"
                                onChange={this.selectEndDate}
                            >
                                <List.Item>{this.state.end_date_str ? this.state.end_date_str : '选择服务结束时间'}</List.Item>
                            </DatePicker>}
                        <br />
                        <FileUploadBtn value={end_photo} list_type={"picture-card"} contents={"plus"} action={remote.upload_url} onChange={this.endUpImg} disabled={stepStatus === 0 ? true : false} />
                    </Card.Body>
                </Card> */}
                {stepStatus === 0 ? <Row style={{ padding: '15px' }}>执行情况：{data_info.remarks}</Row> : <List renderHeader={() => '执行情况'}><TextareaItem onChange={this.remarks} placeholder="请输入内容" rows={3} autoHeight={true} value={remarks || data_info.remarks} /></List>}
                <div style={{ position: 'fixed', bottom: '0', width: '100%', height: '50px' }}>
                    {
                        data_info.start_date ?
                            (data_info.end_date ? <Button style={{ width: '100%' }} disabled={stepStatus === 0 ? true : false} type='primary' onClick={() => { this.openModal('提交'); }}>{stepStatus === 0 ? '已提交' : '提交'}</Button> : <div><Button style={{ width: '46%', margin: '2%' }} disabled={stepStatus === 0 ? true : false} type='primary' onClick={() => { this.openModal('撤回开始'); }}>撤回开始</Button><Button style={{ width: '46%', margin: '2%' }} disabled={stepStatus === 0 ? true : false} type='primary' onClick={() => { this.openModal('结束'); }}>服务结束</Button></div>) : <Button style={{ width: '100%' }} disabled={stepStatus === 0 ? true : false} type='primary' onClick={() => { this.openModal('开始'); }}>服务开始</Button>

                    }

                </div>
            </Row >
        );
    }
}

/**
 * 控件：服务详情控制器
 * 服务详情
 */
@addon('ServicerServiceDetailsView', '服务详情', '服务详情')
@reactControl(ServicerServiceDetailsView, true)
export class ServicerServiceDetailsViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}