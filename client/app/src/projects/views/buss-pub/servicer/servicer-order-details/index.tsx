import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Card, WhiteSpace, List } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { Row, Col, Rate, Icon, } from "antd";
import './index.less';
import { request } from "src/business/util_tool";
const Item = List.Item;
/**
 * 组件：服务详情状态
 */
export interface ServicerOrderDetailsViewState extends BaseReactElementState {
    /** 请求的数据 */
    list?: any;
}

/**
 * 组件：服务详情
 * 服务详情
 */
export class ServicerOrderDetailsView extends BaseReactElement<ServicerOrderDetailsViewControl, ServicerOrderDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            list: {},
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        request(this, AppServiceUtility.service_record_service.get_service_record_list_all_app!({ user_filter: true, id: id }, 1, 1))
            .then((data: any) => {
                this.setState({
                    list: data!.result[0]
                });
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        let { list } = this.state;
        return (
            <Row>
                <Card full={false} className='list-conten' style={{ padding: '10px 10px', margin: '10px 0', }}>
                    <Card.Header title={list.service_provider_name} />
                    <Row type='flex' justify='center'>
                        <Col className='list-col' span={10}><img style={{ maxWidth: '100%' }} src={list.service_product_img && list.service_product_img[0] ? list.service_product_img[0] : ""} /></Col>
                        <Col span={14} className='list-col'>
                            <Row>{list.service_project_name}</Row>
                            {/* <Row>
                                剩余服务次数：0
                            </Row> */}
                            {/* <Row>￥ 1.00</Row> */}
                            {/* <Row>x1</Row> */}
                        </Col>
                    </Row>
                    <Card.Footer extra={list.task_state} />
                </Card>
                <WhiteSpace />
                <List className="my-list">
                    <Item>服务机构：{list.service_provider_name}</Item>
                    <Item>服务项目：{list.service_project_name}</Item>
                    <Item>订单编号：{list.order_id}</Item>
                    <Item>创建时间：{list.create_date}</Item>
                    <Item>开始时间：{list.start_date}</Item>
                    <Item>结束时间：{list.end_date}</Item>
                    {/* <Item>购买数量：1</Item> */}
                    {/* <Item>订单总价：￥1</Item> */}
                    {list.service_attitude ? <Item>服务态度：<Rate value={list.service_attitude} character={<Icon type="heart" theme="filled" style={{ color: 'red' }} />} /></Item> : null}
                    {list.service_quality ? <Item>服务质量：<Rate value={list.service_quality} character={<Icon type="heart" theme="filled" style={{ color: 'red' }} />} /></Item> : null}
                    {list.opinion_remarks ? <Item>评价内容：{list.opinion_remarks}</Item> : null}
                    {!list.service_attitude && !list.service_quality ? <Item>购买者未作出评价</Item> : null}
                </List>
            </Row>
        );
    }
}

/**
 * 控件：服务详情控制器
 * 服务详情
 */
@addon('ServicerOrderDetailsView', '服务详情', '服务详情')
@reactControl(ServicerOrderDetailsView, true)
export class ServicerOrderDetailsViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}