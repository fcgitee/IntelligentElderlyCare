import { Col, Icon as AdIcon, Row } from "antd";
import { Card, Icon, ListView, WhiteSpace } from "antd-mobile";
import moment from "moment";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { LoadingEffectView } from "src/business/views/loading-effect";
import { filterHTMLTag } from "../../home";

/**
 * 组件：我的点赞视图状态
 */
export interface GiveThumbsUpViewState extends ReactViewState {
    /** 点赞数据 */
    data?: any;
    /** 养老咨询数据 */
    pensionData?: any;
    /** 长列表数据 */
    dataSource?: any;
    /** 养老长列表 */
    dataSourcePension?: any;
    /** 长列表容器高度 */
    height?: any;
    // dat
    notice?: string;
    upload?: boolean; // 加载效果
}

/**
 * 组件：我的点赞视图
 * 我的点赞视图
 */
export class GiveThumbsUpView extends ReactView<GiveThumbsUpViewControl, GiveThumbsUpViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        const dsp = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            dataSourcePension: dsp,
            pensionData: [],
            data: [],
            height: document.documentElement!.clientHeight,
            notice: '正在加载',
            upload: true,
        };
    }
    componentDidMount() {
        AppServiceUtility.friends_circle.get_like_list!({ 'is_own': true }, 1, 3)!
            .then((data: any) => {
                if (data.result) {
                    if (data.result.length === 0) {
                        this.setState({
                            notice: '暂无数据',
                        });
                    }
                    this.setState(
                        {
                            data: data.result,
                        }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
        AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ 'object': '养老资讯', 'type': 'like', 'is_own': true }, 1, 3)!
            .then((data: any) => {
                this.setState({ upload: false });
                if (data.result) {
                    if (data.result.length === 0) {
                        this.setState({
                            notice: '暂无数据',
                        });
                    }
                    this.setState(
                        {
                            pensionData: data.result,
                        }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        const { data, pensionData, } = this.state;
        setMainFormTitle('点赞');
        return (
            <div>
                <Card full={true}>
                    <Card.Header
                        title="老友圈"
                        extra={<Icon type='right' />}
                    />
                    <Card.Body>
                        {this.state.upload && <LoadingEffectView />}
                        {
                            data && data.length ?
                                data.map((owData: any, index: number) => {
                                    return (
                                        <div key={index}>
                                            <AdIcon type="like" theme="filled" style={{ float: 'right', color: "#EEAD0E", marginTop: '20px', }} />
                                            <div style={{ display: 'flex', padding: '15px 15px', borderBottom: '1px solid #ddd' }}>
                                                <div style={{ height: '64px', }}>
                                                    <img style={{ height: '64px', width: '64px', borderRadius: '50%', marginRight: "15px" }} src={owData.circle_user_picture ? owData.circle_user_picture[0] : ''} alt="" />
                                                </div>
                                                <div>
                                                    <div style={{ marginBottom: '8px', fontWeight: 'bold' }}>{owData.circle_user_name}</div>
                                                    <div>{owData.create_date.split(' ')[0]}</div>
                                                    <div>{owData.circle_content}</div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    {this.state.notice}
                                </Row>
                        }
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <Card full={true}>
                    <Card.Header
                        title="养老资讯"
                        extra={<Icon type='right' />}
                    />
                    <Card.Body>
                        {
                            pensionData && pensionData.length ?
                                pensionData.map((owData: any, index: number) => {
                                    console.info(owData);
                                    return (
                                        <div key={index}>
                                            <Card className='list-conten'>
                                                <Row type='flex' justify='center'>
                                                    <Col className='list-col' span={10}><img src={owData.obj && owData.obj.app_img_list ? owData.obj.app_img_list[0] : ''} className="activity-img" /></Col>
                                                    <Col span={14} className='list-col'>
                                                        <AdIcon type="like" theme="filled" style={{ float: 'right', color: "#EEAD0E", marginTop: '20px', }} />
                                                        <Row><strong>{owData.obj ? owData.obj.title : ''}</strong></Row>
                                                        <Row>
                                                            <div
                                                                style={{
                                                                    wordBreak: 'break-all',
                                                                    textOverflow: 'ellipsis',
                                                                    overflow: 'hidden',
                                                                    display: ' -webkit-box',
                                                                    WebkitLineClamp: 2,
                                                                    WebkitBoxOrient: 'vertical',
                                                                }}
                                                            >
                                                                {filterHTMLTag(owData.obj ? owData.obj.content : '')}
                                                            </div>
                                                        </Row>
                                                        {/* <Row>{moment(owData.issue_date).format("YYYY-MM-DD hh:mm")}</Row> */}
                                                        <Row>
                                                            <Col span={12}>{moment(owData.obj ? owData.obj.create_date : '').format("MM-DD hh:mm")}</Col>
                                                            <Col span={12} style={{ textAlign: 'right' }}>
                                                                <span style={{ marginRight: '10px' }}>
                                                                    <Icon type="like" style={{ marginRight: '5px' }} />0
                                                                </span>
                                                                <span style={{ marginRight: '10px' }}>
                                                                    <Icon type="chrome" style={{ marginRight: '5px' }} />0
                                                                </span>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {owData.org ? owData.org.name : ''}
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </div>
                                    );

                                })
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    {this.state.notice}
                                </Row>
                        }
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

/**
 * 控件：我的点赞视图控制器
 * 我的点赞视图
 */
@addon('GiveThumbsUpView', '我的点赞视图', '我的点赞视图')
@reactControl(GiveThumbsUpView, true)
export class GiveThumbsUpViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}