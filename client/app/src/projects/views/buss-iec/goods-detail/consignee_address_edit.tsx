import { Spin, List } from "antd";
import { WhiteSpace, InputItem, Toast, Switch, Picker, Button } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
import { funAddress } from "src/projects/app/util-tool";

/**
 * 组件：收货人编辑视图控件状态
 */
export interface ConsigneeAddressEditViewState extends ReactViewState {

    /** 根据id获取的收货地址信息 */
    consignee_address_data?: any;
    /** 加载圈 */
    loading: boolean;
}
/**
 * 组件：收货人编辑视图控件
 */
export class ConsigneeAddressEditView extends ReactView<ConsigneeAddressEditViewControl, ConsigneeAddressEditViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            consignee_address_data: {},
            loading: false
        };
    }
    componentDidMount() {
        setMainFormTitle("收货地址");
        let id = this.props.match!.params.key;
        if (id) {
            this.setState(
                {
                    loading: true
                },
                () => {
                    // 根据id获取收货人地址信息
                    AppServiceUtility.shopping_mall_manage_service.get_consignee_address_detail_app!({ 'id': id })!
                        .then((data: any) => {
                            if (data.result.length > 0) {
                                this.setState({
                                    consignee_address_data: data.result[0],
                                    loading: false
                                });
                            }
                        });
                });
        }

    }
    /** 改变名称触发事件 */
    changeName = (value: any) => {
        let { consignee_address_data } = this.state;
        consignee_address_data['name'] = value;
        this.setState({
            consignee_address_data
        });
    }
    /** 改变手机号码触发事件 */
    changeMobile = (value: any) => {
        let { consignee_address_data } = this.state;
        consignee_address_data['mobile'] = value;
        this.setState({
            consignee_address_data
        });
    }
    /** 改变地址触发事件 */
    changeAddress = (value: any) => {
        let { consignee_address_data } = this.state;
        consignee_address_data['address'] = value;
        this.setState({
            consignee_address_data
        });
    }
    /** 改变详细地址触发事件 */
    changeFunAddress = (value: any) => {
        let { consignee_address_data } = this.state;
        consignee_address_data['fun_address'] = value;
        this.setState({
            consignee_address_data
        });
    }
    /** 改变是否默认触发事件 */
    changeIsDefault = (value: any) => {
        let { consignee_address_data } = this.state;
        consignee_address_data['is_default'] = value;
        this.setState({
            consignee_address_data
        });
    }
    /** 保存方法 */
    save = () => {
        let { consignee_address_data } = this.state;
        this.setState(
            {
                loading: true
            },
            () => {
                setTimeout(
                    () => {
                    },
                    5000
                );
                AppServiceUtility.shopping_mall_manage_service.save_consignee_address_app!(consignee_address_data)!
                    .then((data: any) => {
                        this.setState(
                            {
                                loading: false
                            },
                            () => {
                                if (data === 'Success') {
                                    Toast.success('保存成功', 2, () => { history.back(); });
                                } else {
                                    Toast.fail('保存失败' + data, 3);
                                }
                            });

                    })
                    .catch((error: Error) => {
                        this.setState(
                            {
                                loading: false
                            }
                        );
                    });
            }
        );
    }
    render() {
        let { consignee_address_data, loading } = this.state;
        return (
            <Spin spinning={loading}>
                <List className="my-list input-float-right">
                    <InputItem
                        placeholder='请输入'
                        value={consignee_address_data.name ? consignee_address_data.name : ''}
                        moneyKeyboardAlign="left"
                        onChange={this.changeName}
                    >
                        收货人
                    </InputItem>
                    <InputItem
                        placeholder='请输入'
                        value={consignee_address_data.mobile ? consignee_address_data.mobile : undefined}
                        moneyKeyboardAlign="left"
                        onChange={this.changeMobile}
                    >
                        手机号码
                    </InputItem>
                    <Picker
                        data={funAddress}
                        title="选择区域"
                        cascade={true}
                        value={consignee_address_data.fun_address ? consignee_address_data.fun_address : ''}
                        // onChange={v => this.setState({ sValue: v })}
                        onOk={this.changeFunAddress}
                    >
                        <List.Item
                            className="am-list-item-middle"
                            style={{
                                padding: '12px 15px',
                                fontSize: '17px',
                                color: '#000',
                                borderBottom: '1px #ccc solid',
                                backgroundColor: '#fff',
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}
                        >
                            <div>所在区域</div>
                        </List.Item>
                    </Picker>
                    <InputItem
                        placeholder='请输入'
                        value={consignee_address_data.address ? consignee_address_data.address : ''}
                        moneyKeyboardAlign="left"
                        onChange={this.changeAddress}
                    >
                        详细地址
                    </InputItem>
                    <List.Item
                        className="am-list-item-middle"
                        style={{
                            padding: '12px 0 12px 15px',
                            fontSize: '17px',
                            color: '#000',
                            borderBottom: '1px solid #d9d9d9',
                            backgroundColor: '#fff'
                        }}
                        extra={<Switch
                            checked={consignee_address_data.is_default ? consignee_address_data.is_default : false}
                            onChange={this.changeIsDefault}
                        />}
                    >设置默认地址
                    </List.Item>
                </List>
                <WhiteSpace size="lg" />
                <div className="app-shop-btn-layout">
                    <Button className="app-shop-btn" style={{ width: '100%' }} disabled={loading} type='primary' onClick={() => { this.save(); }}>保存地址</Button>
                </div>
            </Spin>
        );
    }
}

/**
 * 组件：收货人编辑视图控件
 * 控制收货人编辑视图控件
 */
@addon('ConsigneeAddressEditView', '收货人编辑视图控件', '控制收货人编辑视图控件')
@reactControl(ConsigneeAddressEditView, true)
export class ConsigneeAddressEditViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}