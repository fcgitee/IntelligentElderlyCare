import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row, Icon } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { NavBar, WhiteSpace, WingBlank } from "antd-mobile";

/**
 * 组件：商城商品控件状态
 */
export interface ConsigneeAddressListViewState extends ReactViewState {
    address_list: any;
    is_loading: boolean;
}
/**
 * 组件：商城商品控件
 */
export class ConsigneeAddressListView extends ReactView<ConsigneeAddressListViewControl, ConsigneeAddressListViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            address_list: [],
            is_loading: true,
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_consignee_address_detail_app!({}))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        address_list: datas.result,
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    toEdit = (record: any) => {
        if (this.props.location!.state && this.props.location!.state.id) {
            // 选择地址状态
            this.props.history!.push(ROUTE_PATH.newSubmitOrder + '/' + this.props.location!.state.id, {
                consignee_data: record,
                ...(this.props.location!.state && this.props.location!.state.type ? { type: this.props.location!.state.type } : {}),
            });
        } else {
            this.props!.history!.push(ROUTE_PATH.consigneeAddressEdit + '/' + record.id);
        }
    }
    toAdd = () => {
        this.props!.history!.push(ROUTE_PATH.consigneeAddressEdit);
    }
    render() {
        setMainFormTitle('我的收货地址');
        const { address_list, is_loading } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row className="consignee-address-list-layout">
                    <NavBar
                        icon={<Icon type={"left"} onClick={() => history.back()} />}
                        rightContent={<Icon type={"plus"} onClick={() => this.toAdd()} />}
                    >
                        {'我的收货地址'}
                    </NavBar>
                    <Row>
                        <WhiteSpace size="lg" />
                        <WingBlank>
                            {address_list && address_list.length ? address_list.map((item: any, index: number) => {
                                return (
                                    <Row key={index} onClick={() => this.toEdit(item)}>
                                        <Row className="consignee-address-item">
                                            <Row type="flex" justify="start" align="bottom" className="consignee-address-item-line">
                                                <span className="consignee-address-name">{item.name}</span>
                                                <span>{item.mobile}</span>
                                            </Row>
                                            <WhiteSpace size="md" />
                                            <Row className="consignee-address-item-line maxLine2" style={{ WebkitBoxOrient: 'vertical' }}>
                                                {item.fun_address && item.fun_address.map ? item.fun_address.map((itm: any, idx: number) => {
                                                    return (
                                                        <span key={idx}>{itm}</span>
                                                    );
                                                }) : null}
                                                <span>{item.address}</span>
                                            </Row>
                                        </Row>
                                        <WhiteSpace size="lg" />
                                    </Row>
                                );
                            }) : null}
                        </WingBlank>
                    </Row>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：商城商品控件
 * 控制商城商品控件
 */
@addon('ConsigneeAddressListView', '商城商品控件', '控制商城商品控件')
@reactControl(ConsigneeAddressListView, true)
export class ConsigneeAddressListViewControl extends ReactViewControl {
}