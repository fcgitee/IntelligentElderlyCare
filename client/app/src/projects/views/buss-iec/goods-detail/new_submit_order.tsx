import { Col, message, Row, Spin } from "antd";
import { Button, Calendar, Icon, Modal, Toast, WhiteSpace, WingBlank } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { ProductOrder } from "../shopping-center/blocks/product-order";
import './index.less';
const prompt = Modal.prompt;
import zhCN from 'antd-mobile/lib/calendar/locale/zh_CN';
import moment from "moment";
import { pay } from "../../buss-mis/home-care/service-product-details";

/**
 * 组件：商家详情视图控件状态
 */
export interface NewSubmitOrderViewState extends ReactViewState {

    /** 根据id获取的确认订单信息 */
    data_info?: any;
    /** 加载圈 */
    is_loading: boolean;
    /** 收货人信息 */
    consignee_data?: any;
    /** 备注的值 */
    remark?: string;
    // 预约的服务时间
    selected_date?: any;
    // 选择时间弹窗是否开启
    date_modal?: any;
    // 弹窗配置
    date_config?: any;
    // 商品详情
    businessDetail?: any;
}
/**
 * 组件：商家详情视图控件
 */
export class NewSubmitOrderView extends ReactView<NewSubmitOrderViewControl, NewSubmitOrderViewState> {
    originbodyScrollY = document.getElementsByTagName('body')[0].style.overflowY;
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: {},
            is_loading: true,
            consignee_data: {},
            remark: '',
            selected_date: undefined,
            date_modal: false,
            date_config: {
                locale: zhCN,
                type: 'one',
                pickTime: true,
            },
            businessDetail: {},
        };
    }
    componentDidMount() {
        setMainFormTitle("确认订单");
        // 根据id获取商品信息
        if (this.props.location!.state && this.props.location!.state.type) {
            if (this.props.location!.state.type === '商品') {
                this.getProduct();
            } else if (this.props.location!.state.type === '居家服务') {
                this.getServices();
            }
        } else {
            this.getProduct();
        }
        // request(this, AppServiceUtility.shopping_mall_manage_service.get_goods_detail_list_app!({ id: this.props.match!.params.key }))
        //     .then((datas: any) => {
        //         this.setState({
        //             is_loading: false,
        //         });
        //         if (datas.result.length > 0) {
        //             this.setState({
        //                 data_info: datas.result[0],
        //             });
        //         }
        //     })
        //     .catch(err => {
        //         this.setState({
        //             is_loading: false,
        //         });
        //     });
        // 如果有选择地址，就用选择的地址
        if (this.props.location!.state && this.props.location!.state.consignee_data) {
            this.setState({
                consignee_data: this.props.location!.state.consignee_data,
            });
        } else {
            // 没有的话就获取默认地址
            request(this, AppServiceUtility.shopping_mall_manage_service.get_default_consignee_data!())
                .then((datas: any) => {
                    if (datas.result.length > 0) {
                        this.setState({
                            consignee_data: datas.result[0],
                        });
                    }
                });
        }
    }
    getProduct() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_goods_list!({ status: 'PASSED', id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    getServices() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_services_list!({ status: '通过', state: '启用', id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    /** 支付方法 */
    pay = () => {
        const { is_loading, consignee_data, data_info, remark, selected_date } = this.state;
        if (is_loading) {
            return;
        }
        if (this.props.location!.state && this.props.location!.state.type === '居家服务') {
            if (!selected_date) {
                message.error('请选择预约时间！');
                return;
            }
            request(this, AppServiceUtility.service_detail_service.get_service_product_item_package_list!({ id: data_info.id }, 1, 1))!
                .then((data: any) => {
                    if (data.result.length > 0) {
                        // 使用旧流程的购买
                        this.setState(
                            {
                                businessDetail: data.result[0],
                            },
                            () => {
                                pay(this);
                            }
                        );
                    }
                });
        } else {
            this.setState(
                {
                    is_loading: true,
                },
                () => {
                    // 下单参数
                    const param = {
                        consignee_data,
                        product_data: [
                            {
                                product_id: data_info.id,
                                number: 1,
                            }
                        ],
                        remark: remark
                    };
                    request(this, AppServiceUtility.shopping_mall_manage_service.create_order!(param))
                        .then((datas: any) => {
                            this.setState({
                                is_loading: false,
                            });
                            if (datas === 'Success') {
                                Toast.success('下单成功！', 1, () => {
                                    this.props.history!.push(ROUTE_PATH.productOrderList);
                                });
                            } else {
                                Toast.fail(`下单失败，${datas}！`);
                            }
                        })
                        .catch(err => {
                            Toast.fail('下单失败！');
                            this.setState({
                                is_loading: false,
                            });
                        });
                }
            );
        }
    }
    ctrl = (type: string, record: any) => {
        if (type === 'insertRemark') {
            prompt(
                '请输入备注',
                '',
                [
                    { text: '取消' },
                    {
                        text: '确定', onPress: (value: any) => {
                            this.setState(
                                {
                                    remark: value,
                                },
                            );
                        }
                    },
                ],
                'default',
                ''
            );
        } else if (type === 'chooseConsignee') {
            this.props.history!.push(ROUTE_PATH.consigneeAddressList, {
                id: this.props.match!.params.key,
                ...(this.props.location!.state && this.props.location!.state.type ? { type: this.props.location!.state.type } : {}),
            });
        } else if (type === 'selectDate') {
            this.setState(
                {
                    date_modal: true,
                },
            );
        }
    }

    onConfirm = (selected_date: any) => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            date_modal: false,
            selected_date,
        });
    }

    onCancel = () => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            date_modal: false,
            // selected_date: undefined,
        });
    }
    render() {
        let { data_info, consignee_data, is_loading, remark, selected_date, date_config, date_modal } = this.state;
        const product_order_info: any = {
            organization_name: data_info.org_name,
            line_info: [
                {
                    number: 1,
                    unit_price: data_info.price,
                    product_title: data_info.title,
                    product_image_urls: data_info.image_urls,
                    product_content: data_info.content,
                }
            ],
            final_amount: (data_info.price || 0) * 1 + (data_info.postage_price || 0),
            postage_amount: data_info.postage_price || 0,
            status: '',
        };
        return (
            <Spin spinning={is_loading}>
                <Row className="shopping-center-layout shopping-center-product-order-list">
                    <WhiteSpace size="lg" />
                    <WingBlank>
                        <Row className="product-order-section product-order-detail-status-address">
                            <Row className="product-order-detail-address">
                                <WingBlank>
                                    <Row type="flex" justify="center" align="middle" onClick={() => this.ctrl('chooseConsignee', {})}>
                                        <Col span={22}>
                                            <WhiteSpace size="md" />
                                            <Row>{this.props.location!.state && this.props.location!.state.type === '居家服务' ? '待服务人' : '收货人'}：{consignee_data.name}</Row>
                                            <WhiteSpace size="md" />
                                            <Row>{this.props.location!.state && this.props.location!.state.type === '居家服务' ? '服务地址' : '收货地址'}：{consignee_data.address}</Row>
                                            <WhiteSpace size="md" />
                                        </Col>
                                        <Col span={2} style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'middle' }}>
                                            <Icon type={'right'} />
                                        </Col>
                                    </Row>
                                </WingBlank>
                            </Row>
                        </Row>
                        <WhiteSpace size="md" />
                        <ProductOrder product_order_info={product_order_info} inDetail={true} inOrder={true} />
                        <WhiteSpace size="md" />
                        {this.props.location!.state && this.props.location!.state.type === '居家服务' ? <>
                            <Row className="product-order-section">
                                <WhiteSpace size="md" />
                                <WingBlank>
                                    <Row type="flex" justify="space-between" align="middle">
                                        <span className="font-important">预约时间</span>
                                        <span onClick={() => this.ctrl('selectDate', data_info)}>{selected_date ? moment(selected_date).format('YYYY-MM-DD hh:mm:ss') : '请选择预约时间'}</span>
                                    </Row>
                                </WingBlank>
                                <WhiteSpace size="md" />
                            </Row>
                            <WhiteSpace size="md" />
                        </> : <>
                                <Row className="product-order-section">
                                    <WhiteSpace size="md" />
                                    <WingBlank>
                                        <Row type="flex" justify="space-between" align="middle">
                                            <span className="font-important">配送方式</span>
                                            <span>快递 ￥{product_order_info.postage_amount}</span>
                                        </Row>
                                    </WingBlank>
                                    <WhiteSpace size="md" />
                                </Row>
                                <WhiteSpace size="md" />
                            </>}
                        <Row className="product-order-section">
                            <WhiteSpace size="md" />
                            <WingBlank>
                                <Row type="flex" justify="space-between" align="top">
                                    <span className="font-important">订单备注</span>
                                    <span style={{ maxWidth: '60%' }} onClick={() => this.ctrl('insertRemark', data_info)}>{remark || '选填，建议先和商家沟通一致'}</span>
                                </Row>
                            </WingBlank>
                            <WhiteSpace size="md" />
                        </Row>
                    </WingBlank>
                    {(() => {
                        let wp: any = [];
                        for (let i = 0; i < 6; i++) {
                            wp.push(<WhiteSpace size="lg" key={i} />);
                        }
                        return wp;
                    })()}
                    <div className="app-shop-btn-layout app-shop-btn-layout-fix app-shop-btn-layout-white product-order-btns">
                        <Row type="flex" justify="space-between" align="middle">
                            <Col span={11}>
                                合计：￥<span className="font-important" style={{ fontSize: 20 }}>{isNaN(product_order_info.final_amount) ? '-' : product_order_info.final_amount}</span>
                            </Col>
                            <Col span={12}>
                                <Button className="app-shop-btn product-order-btn gray" style={{ width: '100%', marginLeft: 0 }} disabled={is_loading} onClick={() => { this.pay(); }}>立即购买</Button>
                            </Col>
                        </Row>
                    </div>
                </Row>
                <Calendar
                    {...date_config}
                    visible={date_modal}
                    onCancel={this.onCancel}
                    onConfirm={this.onConfirm}
                    defaultValue={data_info.services_date ? [new Date(data_info.services_date)] : null}
                    defaultDate={new Date()}
                    minDate={new Date()}
                />
            </Spin>
        );
    }
}

/**
 * 组件：确认订单视图控件
 * 控制确认订单视图控件
 */
@addon('NewSubmitOrderView', '确认订单视图控件', '控制确认订单视图控件')
@reactControl(NewSubmitOrderView, true)
export class NewSubmitOrderViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}