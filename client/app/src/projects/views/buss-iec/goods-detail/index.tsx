import { Col, Rate, Row, Spin, Icon } from "antd";
import { Card, Carousel, Flex, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle, encodeUrlParam } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { forceCheckIsLogin, ROUTE_PATH } from "src/projects/app/util-tool";
import { Activity } from "src/projects/models/activity";
import './index.less';

/**
 * 组件：商家详情视图控件状态
 */
export interface GoodsDetailsViewState extends ReactViewState {
    /** 数据 */
    data: Activity;
    /** 图片高度 */
    imgHeight?: number | string;
    /** 根据id获取的商品详情信息 */
    goodsDetail?: any;
    /** 服务介绍 */
    introduce?: string;
    /** imgUrl */
    imgUrl?: any;
    /** 预约时间 */
    appointment_data?: any;
    loading: boolean;
    is_collection?: any;
}
/**
 * 组件：商家详情视图控件
 */
export class GoodsDetailsView extends ReactView<GoodsDetailsViewControl, GoodsDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            imgHeight: 176,
            goodsDetail: {},
            introduce: '',
            imgUrl: [],
            appointment_data: '',
            loading: true,
            is_collection: 0
        };
    }
    componentDidMount() {
        setMainFormTitle("商品详情");
        let id = this.props.match!.params.key;
        // // 查询是否收藏
        // AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ business_id: id, type: 'collection', object: '居家服务', "is_own": true }, 1, 1)!
        //     .then((data: any) => {
        //         // console.log(data);
        //         if (data.result.length > 0) {
        //             this.setState({
        //                 is_collection: 1
        //             });
        //         }
        //     });
        // 根据id获取商品详情
        AppServiceUtility.shopping_mall_manage_service.get_goods_detail_list_app!({ 'id': id })!
            .then((data: any) => {
                // console.log(data);
                let service_ulr: any = [];
                if (data.result.length > 0) {
                    data.result[0].image_urls ? data.result[0].image_urls.map((value: any, index: number) => {
                        let obj = (
                            <a
                                className='carousel-a'
                                key={index}
                                href="javascript:;"
                                style={{ height: this.state.imgHeight }}
                            >
                                <img
                                    className='carousel-img'
                                    src={value}
                                    alt=""
                                    onLoad={() => {
                                        window.dispatchEvent(new Event('resize'));
                                        this.setState({ imgHeight: 'auto' });
                                    }}
                                />
                            </a>
                        );
                        service_ulr.push(obj);
                    })
                        :
                        '';
                    this.setState({
                        goodsDetail: data.result[0],
                        loading: false,
                        imgUrl: service_ulr,
                        introduce: data.result[0].content,
                    });
                }
            });
    }
    pay = (date?: any) => {
        // 商品详情
        // let { businessDetail } = this.state;
        // let j = businessDetail.total_price ? businessDetail.total_price : 0;
        // // 数量为1
        // let divide = {};
        // if (businessDetail['proportion']) {
        //     businessDetail['proportion'].map((e: { title: string, contents: number }, i: number) => {
        //         switch (e.title) {
        //             case "平台":
        //                 divide['平台'] = {
        //                     id: divideTitleMap['平台'],
        //                     percent: e.contents
        //                 };
        //                 break;
        //             case "服务商":
        //                 divide["服务商"] = {
        //                     id: divideTitleMap["服务商"],
        //                     percent: e.contents
        //                 };
        //                 break;
        //             case "慈善":
        //                 divide['慈善'] = {
        //                     id: divideTitleMap['慈善'],
        //                     percent: e.contents
        //                 };
        //                 break;
        //             default:
        //                 break;
        //         }
        //     });
        // } else {
        //     divide['平台'] = {
        //         id: divideTitleMap['平台'],
        //         percent: 0
        //     };
        //     divide["服务商"] = {
        //         id: divideTitleMap["服务商"],
        //         percent: 1
        //     };
        //     divide['慈善'] = {
        //         id: divideTitleMap['慈善'],
        //         percent: 0
        //     };
        // }
        // businessDetail['num'] = 1;
        // 服务订单列表所需数据
        // let buy_list = [];
        // let uuid = getUuid();
        // if (j) {
        // buy_list.push({
        //     // 服务套餐id（必要）
        //     id: businessDetail['id'],
        //     // 单价（必要）
        //     price: j,
        //     // 数量（必要）
        //     num: 1,
        //     // 总价（必要）
        //     amount_total: businessDetail['num'] * j,
        //     picture_collection: businessDetail['picture_collection'],
        //     // 是否补贴账户（必要）
        //     subsidy: businessDetail['is_allowance'] === "1" ? businessDetail['num'] * j : 0,
        //     // 摘要（必要）
        //     abstract: businessDetail['name'],
        //     // 平台id（必要）
        //     pt_id: divide['平台']['id'],
        //     // 平台分成（必要）
        //     pt_percent: divide['平台']['percent'],
        //     // 服务商id（必要）
        //     fw_id: businessDetail['organization_id'],
        //     // 服务商分成（必要）
        //     fw_percent: divide['服务商']['percent'],
        //     // 慈善id（必要）
        //     cs_id: divide['慈善']['id'],
        //     // 慈善分成（必要）
        //     cs_percent: divide['慈善']['percent'],
        //     // 备注（必要）
        //     remarks: '居家服务商品',
        //     // appointment_data: date ? date : '',
        // });
        // }
        const paramToSend = {
            // 产品Id
            id: this.props.match!.params.key
        };
        const paramToSendStr = JSON.stringify(paramToSend);
        forceCheckIsLogin(
            this.props,
            {
                // 改为新的确认订单页面
                // successPath: ROUTE_PATH.cleanSubmitOrder + '/' + encodeUrlParam(paramToSendStr)
                successPath: ROUTE_PATH.newSubmitOrder + '/' + encodeUrlParam(paramToSendStr)
            },
            {
                backPath: window.location.pathname
            }
        );
    }

    render() {
        let { goodsDetail } = this.state;
        return (
            <Spin spinning={this.state.loading}>
                <div>
                    <div className='fucp-details'>
                        <Icon type="shopping" theme="twoTone" style={{}} />
                    </div>
                    <Carousel
                        autoplay={true}
                        infinite={true}
                    >
                        {this.state.imgUrl.length > 0 ? this.state.imgUrl : <div style={{ textAlign: 'center' }}>没有图片数据</div>}
                    </Carousel>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col span={14} style={{ fontSize: "16px" }}>{goodsDetail.title}</Col>
                            </Row>
                            <WhiteSpace size='sm' />
                            <Row>
                                <Col span={20}>
                                    <Row>
                                        ￥&nbsp;<strong style={{ fontSize: '20px' }}>{goodsDetail.price ? goodsDetail.price : 0}</strong>
                                    </Row>
                                    <Row>
                                        快递：<span style={{ fontSize: '10px' }}>{goodsDetail.postage_price ? goodsDetail.postage_price : 0}</span>
                                    </Row>
                                </Col>
                                <Col span={4} style={{ marginTop: '15px' }}>
                                    <Row style={{ fontSize: '10px' }}>
                                        销量&nbsp;<span style={{}}>{goodsDetail.sales_volume ? goodsDetail.sales_volume : 2000}</span>
                                    </Row>
                                    <Row style={{ fontSize: '10px' }}>
                                        库存&nbsp;<span style={{}}>{goodsDetail.stock_num ? goodsDetail.stock_num : 0}</span>
                                    </Row>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                    <WhiteSpace size="lg" />
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col span={6}><img src={goodsDetail.org_picture ? goodsDetail.org_picture : ''} style={{ height: '50pt', width: '50pt' }} /></Col>
                                <Col span={14}>
                                    <Row style={{ fontSize: '18px' }}>
                                        <span
                                            style={{
                                                wordBreak: 'break-all',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: ' -webkit-box',
                                                WebkitLineClamp: 1,
                                                WebkitBoxOrient: 'vertical',
                                            }}
                                        >
                                            {goodsDetail.org_name ? goodsDetail.org_name : '商家的名称'}
                                        </span>
                                    </Row>
                                    <Row
                                        style={{
                                            paddingTop: '20px'
                                        }}
                                    >
                                        <Rate className='product-rate' count={5} value={goodsDetail.comment_avg ? goodsDetail.comment_avg : 4.5} />{goodsDetail.comment_avg ? goodsDetail.comment_avg : 4.5}分
                                    </Row>
                                </Col>
                                <Col span={4}><Icon type="phone" theme="twoTone" twoToneColor="#eb2f96" style={{ height: '30pt', width: '30pt' }} /></Col>
                            </Row>
                        </Card.Body>
                    </Card>
                    <WhiteSpace size="lg" />
                    <Card>
                        <Card.Header
                            className='card-hearder-center'
                            title="详情介绍"
                        />
                        <Card.Body>
                            {<div dangerouslySetInnerHTML={{ __html: this.state.introduce ? this.state.introduce : '' }} />}
                        </Card.Body>
                    </Card>
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <WhiteSpace size="lg" />
                    <div style={{ position: 'fixed', bottom: '0px', right: "0px", height: '50px', width: '100%', background: '#fff' }}>
                        <Flex>
                            <Flex.Item><div style={{ width: '100%', height: '100%', textAlign: 'center', lineHeight: '50px', background: 'red', color: '#fff', fontWeight: 'bold' }} onClick={this.pay}>立即购买</div></Flex.Item>
                        </Flex>
                    </div>
                </div>
            </Spin>
        );
    }
}

/**
 * 组件：商品详情视图控件
 * 控制商品详情视图控件
 */
@addon('GoodsDetailsView', '商品详情视图控件', '控制商品详情视图控件')
@reactControl(GoodsDetailsView, true)
export class GoodsDetailsViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}