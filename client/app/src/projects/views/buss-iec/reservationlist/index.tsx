import React from "react";
import { reactControl, BaseReactElementControl, ReactView, ReactViewState } from "pao-aop-client";
import { addon } from "pao-aop";
import { Tabs, ListView, Card } from "antd-mobile";
import { request } from "src/business/util_tool";
import { Row, Col } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
const tabs = [
    { title: '已预约' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}

/**
 * 组件：预约养老院列表状态
 */
export interface reservationlistViewState extends ReactViewState {
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    /** 当前第几页 */
    page: number;
    /** 记录tab状态 */
    tab_status?: string;
}

/**
 * 组件：预约养老院列表
 * 描述
 */
export class reservationlistView extends ReactView<reservationlistViewControl, reservationlistViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            page: 1,
            tab_status: 'participate',
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.reservation_service.get_reservation_registration_list_all!({}, 1, 10))
            .then((data: any) => {
                this.setState({
                    list: data.result,
                });
            });
    }
    // /** 明细点击事件 */
    // on_click_detail = (id: string) => {
    //     this.props.history!.push(ROUTE_PATH.activityDetail + '/' + id + '/' + this.state.tab_status);
    // }
    // /** tab切换触发事件 */
    // tab_click = (tab: any, index: number) => {
    //     let tab_status = 'other';
    //     switch (index) {
    //         case 0:
    //             tab_status = 'participate';
    //             break;
    //         case 1:
    //             tab_status = 'complete';
    //             break;
    //         default:
    //             tab_status = 'other';
    //             break;
    //     }
    //     this.setState({ tab_status: tab_status, page: 1 });
    //     request(this, AppServiceUtility.activity_service.get_activity_participate_list!({ 'status': tab_status, 'user_id': 'true' }, 1, 10))
    //         .then((data: any) => {
    //             this.setState({
    //                 list: data.result,
    //             });
    //         });
    // }
    /** 下拉事件 */
    onEndReached = () => {
        let new_pape = this.state.page + 1;
        request(this, AppServiceUtility.reservation_service.get_reservation_registration_list!({}, new_pape, 10))
            .then((data: any) => {
                this.setState({
                    list: data.result,
                    page: new_pape
                });
            });
    }
    render() {
        const { list, dataSource, } = this.state;
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div>
                    <Card className='list-conten'>
                        <Row type='flex' justify='center'>
                            <Col className='list-col' span={10}><img src={owData.organization_info[0] && owData.organization_info.length > 0 ? owData.organization_info[0].organization_info.picture_list[0] : ''} style={{ width: '100pt', height: '100%' }} /></Col>
                            <Col span={14} className='list-col'>
                                {}
                                <Row><strong>{owData.activity_room_name}</strong></Row>
                                <Row className='list-row'>预约时间：{owData.modify_date}</Row>
                                <Row className='list-row'>所属机构：{owData.organization_name}</Row>
                                <Row className='list-row'>地址：{owData.organization_address}</Row>
                            </Col>
                        </Row>
                    </Card>
                </div>
            );
        };
        return (
            <Row>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                // onTabClick={(tab, index) => this.tab_click(tab, index)}
                >
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                    onEndReached={this.onEndReached}
                                />
                                :
                                <Row className='tabs-content' type='flex' justify='center'>
                                    暂无数据
                                </Row>

                        }
                    </div>
                </Tabs>
            </Row>
        );
    }
}

/**
 * 控件：预约养老院列表
 * 描述
 */
@addon('reservationlistView', '预约养老院列表', '预约养老院列表')
@reactControl(reservationlistView)
export class reservationlistViewControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}