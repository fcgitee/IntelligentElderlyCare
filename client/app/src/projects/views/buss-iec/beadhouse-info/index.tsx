import { Icon, Rate, Row } from "antd";
import { Card, Carousel, List, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { forceCheckIsLogin, ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
const Item = List.Item;
const Brief = Item.Brief;
/**
 * 组件：养老院详情视图控件状态
 */
export interface BeadhouseInfoViewState extends ReactViewState {
    /** 图片高度 */
    imgHeight?: number | string;
    /** 数据对象 */
    elderdata?: any;
    /** 是否关注 */
    is_follow?: boolean;
    /** 关注id */
    follow_id?: string;
    state?: any;
}
/**
 * 组件：养老院详情视图控件
 */
export class BeadhouseInfoView extends ReactView<BeadhouseInfoViewControl, BeadhouseInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            imgHeight: 176,
            is_follow: false,
            elderdata: {
                service_product: [],
                organization_info: {
                    organization_nature: '',
                    address: '',
                    telephone: '',
                    comment: '',
                    picture_list: [],
                }
            },
            state: '床位预约'
        };
    }
    order() {
        // this.props.history!.push(ROUTE_PATH.familyList, {
        //     url: ROUTE_PATH.familyAppointment,
        //     key: this.props.match!.params.key
        // });
        forceCheckIsLogin(
            this.props,
            {
                successPath: ROUTE_PATH.familyList,
                params: {
                    url: ROUTE_PATH.familyAppointment,
                    key: this.props.match!.params.key
                }
            },
            {
                backPath: ROUTE_PATH.beadhouseInfo
            }
        );
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            let id = this.props.match!.params.key;
            let user = IntelligentElderlyCareAppStorage.getCurrentUser();
            AppServiceUtility.person_org_manage_service.get_organization_fly_list!({ id }, 1, 1)!
                .then((data: any) => {
                    this.setState({
                        elderdata: data.result[0]
                    });
                });
            if (user) {
                // 预约情况
                AppServiceUtility.person_org_manage_service!.get_current_user_reservation_registration!({ id: user.id, organization_id: this.props.match!.params.key }, 1, 1)!
                    .then((data: any) => {
                        // console.log(data);
                        if (data.result && data.result.length > 0) {
                            if (data.result[0].state === "网上预约") {
                                this.setState({
                                    state: '已预约'
                                });
                            }
                        }
                    });

                // 关注
                AppServiceUtility.service_follow_collection_service.get_service_follow_collection_list!({ 'business_id': id, 'user_id': user.id, 'type': 'follow' }, 1, 1)!
                    .then((data: any) => {
                        // console.log(data);
                        if (data.result.length > 0) {
                            this.setState({
                                is_follow: true,
                                follow_id: data.result[0].id,
                            });
                        }
                    });

            }
        }
    }
    unfollow = () => {
        AppServiceUtility.service_follow_collection_service.delete_service_follow_collection!(this.state.follow_id)!
            .then((data: any) => {
                if (data === 'Success') {
                    this.setState({
                        is_follow: false
                    });
                }
            });
    }
    follow = () => {
        AppServiceUtility.service_follow_collection_service.update_service_follow_collection!(this.props.match!.params.key, 'follow', '养老机构')!
            .then((data: any) => {
                if (data === 'Success') {
                    this.setState({
                        is_follow: true
                    });
                }
            });
    }
    // 通知react-native打开外部页面
    onpenUrl = (url: string) => {
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'openurl',
                    params: url
                })
            );
        }
    }
    render() {
        let data = this.state.elderdata;
        setMainFormTitle(data.name);
        let plclist = ['https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg', 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg', 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg'];
        plclist = data.organization_info.picture_list;
        let map_url = "http://api.map.baidu.com/marker?location=" + data.organization_info.lat + "," + data.organization_info.lon + "&title=" + data.name + "&output=html";
        let dom = (
            <div>
                <Carousel
                    autoplay={true}
                    infinite={true}
                    className="acasdasdasd"
                >
                    {plclist && plclist.map && plclist.map((data: any, index: number) => {
                        return (
                            <a
                                className='carousel-a'
                                key={index}
                                href="javascript:;"
                                style={{ height: this.state.imgHeight }}
                            >
                                <img
                                    className='carousel-img'
                                    src={data}
                                    alt=""
                                    onLoad={() => {
                                        window.dispatchEvent(new Event('resize'));
                                        this.setState({ imgHeight: 'auto' });
                                    }}
                                />
                            </a>
                        );
                    })}
                </Carousel>
                <Card className='info-card'>
                    <Row style={{ paddingRight: '150xp' }}>
                        <strong>{data.name}</strong>
                        <span style={{ float: 'right' }}>
                            {this.state.is_follow ? <Icon onClick={this.unfollow} type="heart" theme="filled" style={{ color: "red", marginRight: "10px" }} /> : <Icon onClick={this.follow} type="heart" theme="filled" style={{ marginRight: "10px" }} />}
                            {this.state.is_follow ? '已关注' : '关注'}
                        </span>
                    </Row>
                    <Row>
                        <Rate character={<Icon type="star" theme="filled" />} allowHalf={true} disabled={true} value={data.organization_info.star_level} />
                        <span style={{ float: 'right' }}>入住率：{data.organization_info.chenk_in_probability}%</span>
                    </Row>
                </Card>
                <WhiteSpace size="lg" />
                <List className="my-list">
                    <Item

                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="shop" />}
                        multipleLine={true}
                    >
                        <Brief>{data.organization_info.organization_nature}</Brief>
                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="shop" />}
                        multipleLine={true}
                        extra={<Icon className='item-icon-size' type="compass" theme="filled" onClick={() => this.onpenUrl(map_url)} />}
                        wrap={true}
                    >
                        {data.organization_info.address}
                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="phone" theme="filled" />}
                        multipleLine={true}
                        extra={<a href={"tel:" + data.organization_info.telephone}><Icon className='item-icon-size' type="phone" theme="filled" /></a>}
                    >
                        <Brief>{data.organization_info.telephone}</Brief>
                    </Item>
                    <Item
                        arrow={undefined}
                        multipleLine={true}
                        thumb={<><Icon type="heart" theme={"filled"} style={{ marginRight: '5px', color: '#ff1466' }} />{data.follow_count}</>}
                    />
                </List>
                <WhiteSpace size="lg" />
                <Card>
                    <Card.Header
                        title="机构介绍"
                    />
                    <Card.Body>
                        <div dangerouslySetInnerHTML={{ __html: data.organization_info.description ? data.organization_info.description : '' }} />
                        <div dangerouslySetInnerHTML={{ __html: data.organization_info.medical_introducte ? data.organization_info.medical_introducte : '' }} />
                    </Card.Body>
                </Card>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <div className="confirmation-orders-buttom info-list" onClick={() => this.order()}>
                    {this.state.state}
                </div>
                <Card full={true} className='list-contents'>
                    <Card.Header
                        title="费用标准"
                    />
                    <Card.Body style={{ paddingTop: '0' }}>
                        <div style={{ padding: '0 15px' }} dangerouslySetInnerHTML={{ __html: data.organization_info.service_standard && data.organization_info.service_standard.bed_price_describe ? data.organization_info.service_standard.bed_price_describe : '' }} />
                        <div style={{ padding: '15px 15px 6px' }} dangerouslySetInnerHTML={{ __html: data.organization_info.service_standard && data.organization_info.service_standard.nurse_price_describe ? data.organization_info.service_standard.nurse_price_describe : '' }} />
                        <div style={{ padding: '15px 15px 6px' }} dangerouslySetInnerHTML={{ __html: data.organization_info.service_standard && data.organization_info.service_standard.meals_price_describe ? data.organization_info.service_standard.meals_price_describe : '' }} />
                    </Card.Body>
                </Card>
            </div>
        );
        return (dom);
    }
}

/**
 * 组件：养老院详情视图控件
 * 控制养老院详情视图控件
 */
@addon('BeadhouseInfoView', '养老院详情视图控件', '控制养老院详情视图控件')
@reactControl(BeadhouseInfoView, true)
export class BeadhouseInfoViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}