import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { ListView } from "antd-mobile";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：能力评估列表状态
 */
export interface CompetenceAssessmentViewState extends ReactViewState {
    // 数据
    dataSource?: any;
    // 是否还有更多
    hasMore?: boolean;
    // 
    rData?: any;
    // 
    dataList?: any;
}

const getDataBlob = (datas: any) => {
    const dataBlob = {};
    datas.map((item: any, index: number) => {
        dataBlob[`${index}`] = `row - ${index}`;
    });
    return dataBlob;
};

const ds = new ListView.DataSource({
    rowHasChanged: (row1: any, row2: any) => row1 !== row2,
});

/**
 * 组件：能力评估列表
 * 能力评估列表
 */
export class CompetenceAssessmentView extends ReactView<CompetenceAssessmentViewControl, CompetenceAssessmentViewState> {

    public lv: any = null;

    constructor(props: any) {
        super(props);

        this.state = {
            dataSource: ds,
            dataList: [],
            hasMore: true,
            rData: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.competence_assessment_service.get_competence_assessment_list!({}, 1, 50)!
            .then((datas: any) => {
                const dataBlob = getDataBlob(datas.result);
                this.setState({
                    rData: dataBlob,
                    dataList: datas.result,
                    dataSource: this.state.dataSource.cloneWithRows(dataBlob),
                });
            });
    }
    viewInfo = (e: any, obj: any) => {
        this.props.history!.push(ROUTE_PATH.changeCompetenceAssessment + '/' + obj.id);
    }
    render() {
        const separator = (sectionID: any, rowID: any) => (
            <div
                key={`${sectionID}-${rowID}`}
                style={{
                    backgroundColor: '#F5F5F9',
                    height: 8,
                    borderTop: '1px solid #ECECED',
                    borderBottom: '1px solid #ECECED',
                }}
            />
        );
        let index = this.state.dataList.length - 1;
        const row = (rowData: any, sectionID: any, rowID: any) => {
            if (index < 0) {
                index = this.state.dataList.length - 1;
            }
            const obj = this.state.dataList[index--];
            return (
                <div key={rowID} style={{ padding: '0 15px' }} onClick={(e: any) => this.viewInfo(e, obj)}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', lineHeight: '50px', color: '#888', fontSize: 18, borderBottom: '1px solid #F6F6F6' }}><span>{obj.user_name}</span><span>{obj.task_state}</span></div>
                    <div style={{ display: 'flex', padding: '15px 0' }}>
                        <div style={{ fontWeight: 'bold' }}>{obj.template_name}</div>
                    </div>
                    <div style={{ textAlign: 'right', paddingBottom: '8px' }}>
                        <div>{obj.create_date}</div>
                    </div>
                </div>
            );
        };
        return (
            <div>
                <ListView
                    ref={el => this.lv = el}
                    dataSource={this.state.dataSource}
                    renderRow={row}
                    renderSeparator={separator}
                    pageSize={4}
                    useBodyScroll={true}
                    onScroll={() => { }}
                    scrollRenderAheadDistance={500}
                />
            </div>
        );
    }
}

/**
 * 控件：能力评估列表控制器
 * 能力评估列表
 */
@addon('CompetenceAssessmentView', '能力评估列表', '能力评估列表')
@reactControl(CompetenceAssessmentView, true)
export class CompetenceAssessmentViewControl extends ReactViewControl {

}