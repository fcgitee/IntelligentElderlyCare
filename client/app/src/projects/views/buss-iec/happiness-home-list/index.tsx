import { Col, Icon, Input, Rate, Row, Spin } from "antd";
import { Card, ListView, Tabs, WhiteSpace, WingBlank } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle, getWebViewRetObjectType, subscribeWebViewNotify } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import './index.less';
const { Search } = Input;

const tabs = [
    { title: '活动数' },
    { title: '报名数' },
    { title: '关注度' },
    { title: '距离' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：幸福院列表视图控件状态
 */
export interface HappinessHomeListViewState extends ReactViewState {

    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any[];
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    // 当前的tab
    tab?: any;
    location?: any;

    empty: any;
    animating: any;
    pageCount: number;
    page: any;
    keyword?: string;
    tabs?: any;
    sort?: any; // 当前tab
}
/**
 * 组件：幸福院列表视图控件
 */
export class HappinessHomeListView extends ReactView<HappinessHomeListViewControl, HappinessHomeListViewState> {
    first_in: boolean;

    constructor(props: any) {
        super(props);
        this.first_in = true;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            tab: tabs[0]['title'],
            location: undefined,
            list: [],
            empty: false,
            animating: true,
            pageCount: 10,
            page: 1,
            keyword: "",
            sort: '活动数',
        };
    }
    componentDidMount() {
        this.get_organization_xfy_list(true);
    }

    get_organization_xfy_list = (isFirst: boolean = false, sortByDistanceParam?: {}) => {
        let { pageCount, keyword, list } = this.state;
        // 获取幸福院信息
        let param: Object = {};
        if (keyword !== '') {
            param['name'] = keyword;
        }
        param['personnel_category'] = '幸福院';
        param['sort'] = this.state.sort;
        if (sortByDistanceParam) {
            param = { ...param, ...sortByDistanceParam };
        }
        request(this, AppServiceUtility.person_org_manage_service.get_organization_xfy_list!(param, this.state.page, pageCount))
            .then((data: any) => {
                isFirst && setMainFormTitle('社区幸福院 (' + data.total + ')');
                if (data && data.result.length > 0) {
                    this.setState({
                        list: list!.concat(data.result),
                        animating: false,
                    });
                } else {
                    this.setState({
                        animating: false,
                        empty: true,
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });

    }
    // getHappinessListData(type: string = '', newPage: number = 1) {
    //     let { list, location, pageCount, keyword } = this.state;
    //     let param: any = { personnel_category: '幸福院' };
    //     if (type !== '') {
    //         if (type === '距离') {
    //             param['sortByDistance'] = true;
    //         } else {
    //             param['sort'] = type;
    //         }
    //     }
    //     if (keyword !== '') {
    //         param['name'] = keyword;
    //     }
    //     param = { ...param, ...location };
    //     this.setState(
    //         {
    //             animating: true,
    //         },
    //         () => {
    //             // 获取幸福院信息
    //             request(this, AppServiceUtility.person_org_manage_service.get_organization_xfy_list!(param, this.state.page, pageCount))
    //                 .then((data: any) => {
    //                     if (data && data.result.length > 0) {
    //                         this.setState({
    //                             list: list!.concat(data.result),
    //                             animating: false,
    //                             tabs: [
    //                                 { title: <Badge text={data.total}>活动数</Badge>, },
    //                                 { title: <Badge text={data.total}>报名数</Badge>, },
    //                                 { title: <Badge text={data.total}>关注度</Badge>, }
    //                             ]
    //                         });
    //                     } else {
    //                         this.setState({
    //                             animating: false,
    //                             empty: true,
    //                         });
    //                     }
    //                 })
    //                 .catch(error => {
    //                     console.error(error);
    //                 });
    //         }
    //     );
    // }
    toInfo(id: string) {
        this.props!.history!.push(ROUTE_PATH.happinessHomeInfo + '/' + id);
    }
    tabClick(e: any) {
        this.setState(
            {
                sort: e.title,
                list: [],
                empty: false,
            },
            () => {
                if (e.title === '距离') {
                    if ((window as any).ReactNativeWebView) {
                        const messageCb = (e: any) => {
                            const retObject = JSON.parse(e.data);
                            if (getWebViewRetObjectType(retObject) === 'getPhonePosition') {
                                let location = retObject.data;
                                if (location === undefined) {
                                    // Toast.fail('获取位置信息失败');
                                }
                                this.setState({
                                    location: location
                                });
                                let param = { "sortByDistance": true, ...location };
                                this.get_organization_xfy_list(false, param);
                            }
                        };
                        subscribeWebViewNotify(messageCb);

                        this.getPhonePosition();
                    } else {
                        this.get_organization_xfy_list();
                    }
                } else {
                    this.get_organization_xfy_list();
                }
            }
        );
    }
    getPhonePosition() {
        // Toast.info('认证成功：' + e.data);
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'getPhonePosition',
                    params: undefined
                })
            );
        }
    }
    searchKey(e: any) {
        this.setState(
            {
                keyword: e,
                empty: false,
                list: [],
                page: 1,
            },
            () => {
                this.get_organization_xfy_list();
            }
        );
    }
    /** 下拉事件 */
    onEndReached = () => {
        let page = this.state.page ? this.state.page + 1 : 1;
        this.setState({ page }, () => {
            !this.state.animating && this.get_organization_xfy_list();
        });
    }
    render() {
        const { list, dataSource, animating, empty } = this.state;
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <>
                    <WhiteSpace />
                    <div style={{}}>
                        <Card className='list-content' style={{ borderRadius: 20, boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)', marginRight: 3, marginLeft: 3 }}>
                            <WingBlank>
                                <WhiteSpace />
                                <Row type='flex' justify='center' onClick={() => this.toInfo(owData.id)}>
                                    <Col span={24}>
                                        <Row type={"flex"} justify={"space-between"}>
                                            <strong>{owData.name} <Icon style={{ color: 'rgba(153,153,153,1)' }} type="right-circle" /></strong>
                                            <span>
                                                <Icon type="heart" theme={"filled"} style={{ marginRight: '5px', color: '#ff1466' }} />{owData.follow_count}
                                            </span>
                                        </Row>
                                    </Col>
                                    <Col className='list-col' style={{ paddingRight: 20 }} span={10}><img src={owData.organization_info.picture_list && owData.organization_info.picture_list.length > 0 ? owData.organization_info.picture_list[0] : ''} style={{ height: '100%', width: '100%', objectFit: 'cover', display: 'block' }} /></Col>
                                    <Col span={14} className='list-col'>
                                        <Row style={{ fontSize: '14px', color: 'rgba(51,51,51,1)' }}>
                                            <Rate character={<Icon style={{ fontSize: "24px" }} type="star" theme="filled" />} allowHalf={true} disabled={true} value={owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 4} />
                                            <span >&nbsp;&nbsp;{owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 4}星</span>
                                        </Row>
                                        <Row><span className="font-orange">{owData.activity_count || 0}</span><span style={{ color: 'rgba(255,153,12,1)' }}>次活动</span>&nbsp;&nbsp;&nbsp;&nbsp;<span className="font-orange">{owData.participate_count || 0}</span><span style={{ color: 'rgba(255,153,12,1)' }}>条报名</span></Row>
                                        <Row>{owData['organization_info'] && owData['organization_info']['telephone'] ? owData['organization_info']['telephone'] : ''}</Row>
                                        <Row className="happiness-address" style={{ fontSize: 14, "WebkitBoxOrient": "vertical" }}>{owData['organization_info'] && owData['organization_info']['address'] ? owData['organization_info']['address'] : ''}</Row>
                                        <Row type="flex" justify="space-between" style={{ fontSize: '14px', color: "rgba(153,153,153,1)" }}>
                                            <Col span={12}>{owData.division_name} {owData.distance ? '| ' + owData.distance.toFixed(2) + 'km' : ''}</Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <WhiteSpace />
                            </WingBlank>
                        </Card>
                    </div>
                </>
            );
        };
        return (
            <Row className='happinessHomeList'>
                <div style={{ height: '50px', backgroundColor: 'white', borderRadius: '15px 15px 0 0' }}>
                    <WingBlank>
                        {/* <s placeholder="搜索" maxLength={8} onSubmit={(e: any) => this.searchKey(e)} /> */}
                        <Search
                            placeholder="搜索机构"
                            onSearch={(e: any) => this.searchKey(e)}
                            style={{ width: '100%', height: '40px', marginTop: '3px', borderRadius: 50 }}
                        />
                    </WingBlank>
                </div>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                    onTabClick={(e) => this.tabClick(e)}
                />
                <WingBlank>
                    <div className='tabs-content'>
                        {
                            list && list.length ?
                                <ListView
                                    ref={el => this['lv'] = el}
                                    dataSource={dataSource.cloneWithRows(list)}
                                    renderRow={renderRow}
                                    initialListSize={10}
                                    pageSize={10}
                                    onEndReached={() => this.onEndReached()}
                                    renderBodyComponent={() => <MyBody />}
                                    style={{ height: this.state.height }}
                                /> : null
                        }
                        {animating ? <Row>
                            <WhiteSpace size="lg" />
                            <Row style={{ textAlign: 'center' }}>
                                <Spin size="large" />
                            </Row>
                            <WhiteSpace size="lg" />
                        </Row> : null}
                        {empty ? <Row>
                            <WhiteSpace size="lg" />
                            <Row className='tabs-content' type='flex' justify='center'>{list && list.length ? '已经是最后一条了' : '无数据'}</Row>
                            <WhiteSpace size="lg" />
                        </Row> : null}
                    </div>
                </WingBlank>
            </Row>
        );
    }
}

/**
 * 组件：幸福院列表视图控件
 * 控制幸福院列表视图控件
 */
@addon('HappinessHomeListView', '幸福院列表视图控件', '控制幸福院列表视图控件')
@reactControl(HappinessHomeListView, true)
export class HappinessHomeListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}