import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import './index.less';
import { DatePicker, Card, List, Radio, Button, Toast } from "antd-mobile";
import { Form, Input } from "antd";
/**
 * 组件：预约确认视图控件状态
 */
export interface confirmationViewState extends ReactViewState {
    /** 数据对象 */
    elderdata?: any;
    /** 申请人 */
    username?: string;
    /** 身份证号码 */
    userldcard?: number;
    /** 性别 男是0  女是1 */
    usergender?: number;
    /** 出生日期 */
    userbirthday?: Date;
    /** 联系电话 */
    userphone?: number;
    /** 户籍地址 */
    userdiress?: string;
    /** 家庭住址 */
    userhomedress?: string;
}
/**
 * 组件：预约确认视图控件
 */
export class confirmationView extends ReactView<confirmationViewControl, confirmationViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            elderdata: {
                organization_info: {
                    organization_nature: '',
                    address: '',
                    telephone: '',
                    comment: ''
                }
            },
            username: '',
            userldcard: undefined,
            usergender: undefined,
            userbirthday: undefined,
            userphone: undefined,
            userdiress: '',
            userhomedress: '',
        };
    }

    componentDidMount() {
        let data = JSON.parse(this.props.match!.params.key);
        this.setState({
            elderdata: data
        });
    }
    onusername = (event: any) => {
        this.setState({
            username: event.target.value
        });
    }
    onuserldcard = (event: any) => {
        this.setState({
            userldcard: event.target.value
        });
    }
    onusergender = (event: any) => {
        this.setState({
            usergender: event.target.value
        });
    }
    onuserbirthday = (event: any) => {
        this.setState({
            userbirthday: event
        });
    }
    onuserphone = (event: any) => {
        this.setState({
            userphone: event.target.value
        });
    }
    onuserdiress = (event: any) => {
        this.setState({
            userdiress: event.target.value
        });
    }
    onuserhomedress = (event: any) => {
        this.setState({
            userhomedress: event.target.value
        });
    }
    subfrom() {
        /** 处理提交事件 */
        /** 弹出成功提示框 */
        Toast.success(`预约${this.state.elderdata.name}成功！请稍后等待养老院的通知！`);
    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const RadioItem = Radio.RadioItem;
        const age = [
            {
                label: '男',
                value: 0
            }, {
                label: '女',
                value: 1
            }
        ];
        let dom = (
            <div className="confirmation">
                <Card.Header title="填写预约信息" />
                <Card.Body>
                    <List renderHeader={() => this.state.elderdata.name}>
                        <List.Item>
                            <Form layout="inline" {...formItemLayout} className="from">
                                <Form.Item label="申请人">
                                    {(<Input value={this.state.username} onChange={(e) => this.onusername(e)} />)}
                                </Form.Item>
                                <Form.Item label="身份证号码">
                                    {(<Input value={this.state.userldcard} onChange={(e) => this.onuserldcard(e)} />)}
                                </Form.Item>
                                <Form.Item label="性别">
                                    {
                                        age.map((item: any, index: number) => {
                                            <RadioItem key={index} checked={this.state.usergender === item.value} onChange={(e: any) => this.onusergender(e)}>
                                                {item.label}
                                            </RadioItem>;
                                        })
                                    }
                                </Form.Item>
                                <Form.Item label="出生日期">
                                    <DatePicker
                                        mode="date"
                                        title="出生日期"
                                        value={this.state.userbirthday}
                                        onChange={(e) => { this.onuserbirthday(e); }}
                                    >
                                        <List.Item>出生日期</List.Item>
                                    </DatePicker>
                                </Form.Item>
                                <Form.Item label="联系电话">
                                    {(<Input value={this.state.userphone} onChange={(e) => this.onuserphone(e)} type="number" />)}
                                </Form.Item>
                                <Form.Item label="户籍地址">
                                    {(<Input value={this.state.userdiress} onChange={(e) => this.onuserdiress(e)} />)}
                                </Form.Item>
                                <Form.Item label="家庭住址">
                                    {(<Input value={this.state.userhomedress} onChange={(e) => this.onuserhomedress(e)} />)}
                                </Form.Item>
                            </Form>
                        </List.Item>
                    </List>
                    <Button onClick={() => this.subfrom()} type="primary">提交</Button>
                </Card.Body>
            </div>
        );
        return (dom);
    }
}

/**
 * 组件：预约确认视图控件
 * 控制预约确认视图控件
 */
@addon('confirmationView', '预约确认视图控件', '控制预约确认视图控件')
@reactControl(confirmationView, true)
export class confirmationViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}