
import { Row, Rate, Icon, Col, Spin } from "antd";
import { Carousel, Card, WhiteSpace, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import './index.less';
import { getGoodLookTimeShow } from "../../buss-mis/activity-manage";
// const Item = List.Item;
// const Brief = Item.Brief;
/**
 * 组件：幸福院详情视图控件状态
 */
export interface HappinessHomeInfoViewState extends ReactViewState {
    /** 数据 */
    data?: any[];
    /** 图片高度 */
    imgHeight?: number | string;
    // 幸福院详情数据
    happiness_info?: any;
    // 活动列表
    organization_activity?: any;
    // 资讯列表
    organization_article?: any;
    animating_activity: any;
    animating_article: any;

    isLogin?: boolean;
}
/**
 * 组件：幸福院详情视图控件
 */
export class HappinessHomeInfoView extends ReactView<HappinessHomeInfoViewControl, HappinessHomeInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: ['1', '2', '3'],
            imgHeight: 176,
            happiness_info: [],
            organization_activity: [],
            organization_article: [],
            animating_activity: true,
            animating_article: true,
            isLogin: false,
        };
    }

    componentDidMount() {
        // 判断是否登录
        request(this, AppServiceUtility.personnel_service.get_user_session!())
            .then((data: any) => {
                this.setState({
                    isLogin: data,
                });
            });
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.person_org_manage_service.get_organization_xfy_list!({ id: this.props.match!.params.key }))
                .then((datas: any) => {
                    if (datas.result && datas.result.length > 0) {
                        this.setState({
                            happiness_info: datas.result[0],
                        });
                    }
                }).catch((error: Error) => {
                    console.error(error.message);
                });
            request(this, AppServiceUtility.activity_service.get_activity_list!({ organization_id: this.props.match!.params.key }, 1, 3))
                .then((data: any) => {
                    this.setState({
                        organization_activity: data.result,
                        animating_activity: false,

                    });
                }).catch((error: Error) => {
                    console.error(error.message);
                });
            request(this, AppServiceUtility.article_list_service.get_news_list!({ organization_id: this.props.match!.params.key }, 1, 3))
                .then((data: any) => {
                    this.setState({
                        organization_article: data.result,
                        animating_article: false,
                    });
                }).catch((error: Error) => {
                    console.error(error.message);
                });
        }
    }
    // 查看机构的全部活动
    toAllActivity() {
        const happiness_info = this.state.happiness_info;
        if (!happiness_info || !happiness_info['id']) {
            return;
        }
        this.props!.history!.push(ROUTE_PATH.activityList + '/' + happiness_info['id']);
    }
    // 查看机构的全部资讯
    toAllArticle() {
        const happiness_info = this.state.happiness_info;
        if (!happiness_info || !happiness_info['id']) {
            return;
        }
        this.props!.history!.push(ROUTE_PATH.newsList + '/' + happiness_info['id']);
    }
    toArticle(id: string) {
        this.props!.history!.push(ROUTE_PATH.newsInfo + '/' + id);
    }
    toActivity(id: string) {
        this.props!.history!.push(ROUTE_PATH.activityDetail + '/' + id);
    }
    toFollow(type: string, id: string) {
        let { happiness_info, isLogin } = this.state;
        if (!isLogin) {
            Toast.fail('请先登录后进行操作！');
            return;
        }
        if (!happiness_info || !happiness_info['id']) {
            Toast.info('幸福院加载中...');
            return;
        }
        if (type === 'add') {
            request(this, AppServiceUtility.service_follow_collection_service.update_service_follow_collection!(id, 'follow', '社区幸福院'))
                .then((datas: any) => {
                    if (datas === 'Success') {
                        happiness_info['follow_count'] = happiness_info['follow_count'] + 1;
                        happiness_info['is_follow'] = 1;
                        this.setState(
                            {
                                happiness_info,
                            },
                            () => {
                                Toast.success('关注成功！');
                            }
                        );
                    }
                }).catch((error: Error) => {
                    console.error(error.message);
                });
        } else if (type === 'del') {
            request(this, AppServiceUtility.service_follow_collection_service.delete_service_follow_collection_by_business_id!({ busindess: id, type: 'follow', object: '社区幸福院' }))
                .then((datas: any) => {
                    if (datas === 'Success') {
                        happiness_info['follow_count'] = happiness_info['follow_count'] - 1;
                        if (happiness_info['follow_count'] < 0) {
                            happiness_info['follow_count'] = 0;
                        }
                        happiness_info['is_follow'] = 0;
                        this.setState(
                            {
                                happiness_info,
                            },
                            () => {
                                Toast.success('取消关注成功！');
                            }
                        );
                    }
                }).catch((error: Error) => {
                    console.error(error.message);
                });
        }
    }
    // 通知react-native打开外部页面
    onpenUrl = (url: string) => {
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'openurl',
                    params: url
                })
            );
        }
    }
    render() {
        const { happiness_info, organization_activity, organization_article, animating_article, animating_activity } = this.state;
        setMainFormTitle(happiness_info['name']);
        // 百度地图url
        let lon = happiness_info.hasOwnProperty('organization_info') && happiness_info['organization_info'].hasOwnProperty('lon') ? happiness_info['organization_info']['lon'] : "";
        let lat = happiness_info.hasOwnProperty('organization_info') && happiness_info['organization_info'].hasOwnProperty('lat') ? happiness_info['organization_info']['lat'] : "";
        let map_url = "http://api.map.baidu.com/marker?location=" + lat + "," + lon + "&title=" + happiness_info.name + "&output=html";
        return (
            <div>
                {(() => {
                    if (happiness_info['organization_info'] && happiness_info['organization_info']['picture_list'] && happiness_info['organization_info']['picture_list'].length > 0) {
                        return (
                            <Carousel
                                autoplay={true}
                                infinite={true}
                            >
                                {happiness_info['organization_info']['picture_list'].map((item: any, index: number) => (
                                    <a
                                        className='carousel-a'
                                        key={index}
                                        href="javascript:;"
                                        style={{ height: this.state.imgHeight }}
                                    >
                                        <img
                                            className='carousel-img'
                                            src={item || 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1564975937&di=cac390b27275e6a039d3a1eeb2b338bf&src=http://img.mp.itc.cn/upload/20170816/ede70cf3a497447fa423391b154fe7b0_th.jpg'}
                                            alt={happiness_info['name'] || ''}
                                            onLoad={() => {
                                                window.dispatchEvent(new Event('resize'));
                                                this.setState({ imgHeight: 'auto' });
                                            }}
                                        />
                                    </a>
                                ))}
                            </Carousel>
                        );
                    }
                    return null;
                })()}
                <Card className='info-card'>
                    {}
                    <Row type="flex" justify="space-between"><strong>{happiness_info['name'] || ''}</strong> {happiness_info['is_follow'] > 0 ? <span onClick={() => this.toFollow('del', happiness_info['id'])}><Icon className='item-icon-size font-red' type="heart" theme="filled" />已关注</span> : <span onClick={() => this.toFollow('add', happiness_info['id'])}><Icon className='item-icon-size' type="heart" theme="filled" />未关注</span>}</Row>
                    <Row type="flex" justify="space-between"><Rate allowHalf={true} disabled={true} value={happiness_info['organization_info'] && happiness_info['organization_info']['star_level'] ? Number(happiness_info['organization_info']['star_level']) : 4} /><Col><span className="font-orange">{happiness_info['activity_count'] || 0}</span>次活动&nbsp;&nbsp;&nbsp;&nbsp;<span className="font-orange">{happiness_info['participate_count'] || 0}</span>条报名</Col></Row>
                    {happiness_info['organization_info'] && happiness_info['organization_info']['address'] ? <Row>{happiness_info['organization_info']['address']}&nbsp;&nbsp;&nbsp;&nbsp;<Icon className='item-icon-size' type="compass" theme="filled" onClick={() => this.onpenUrl(map_url)} /></Row> : null}
                    {happiness_info['organization_info'] && happiness_info['organization_info']['telephone'] ? <Row>{happiness_info['organization_info']['telephone']}&nbsp;&nbsp;&nbsp;&nbsp;<a href={"tel:" + happiness_info['organization_info']['telephone']}><Icon className='item-icon-size' type="phone" theme="filled" /></a></Row> : null}
                    <Row type="flex" justify="end" style={{ padding: '10px' }}>
                        {/* <Icon className='item-icon-size' type="heart" theme="filled" /> */}
                        关注度：{happiness_info['follow_count'] || 0}
                    </Row>
                </Card>
                <WhiteSpace size="lg" />
                {/* <List className="my-list">
                    <Item
                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="shop" />}
                        multipleLine={true}
                    >
                        <Brief>{happiness_info.organization_nature || '无'}</Brief>
                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="shop" />}
                        multipleLine={true}
                        wrap={true}
                        extra={<a href={map_url}><Icon className='item-icon-size' type="compass" theme="filled" /></a>}
                    >
                        <Brief> {happiness_info['organization_info'] && happiness_info['organization_info']['address'] ? happiness_info['organization_info']['address'] : '无'}</Brief>
                    </Item>
                    <Item
                        arrow={undefined}
                        thumb={<Icon className='item-icon-size' type="phone" theme="filled" />}
                        extra={<a href={"tel:" + (happiness_info['organization_info'] && happiness_info['organization_info']['telephone'] ? happiness_info['organization_info']['telephone'] : '')}><Icon className='item-icon-size' type="phone" theme="filled" /></a>}
                        multipleLine={true}
                    >
                        <Brief> {happiness_info['organization_info'] && happiness_info['organization_info']['telephone'] ? happiness_info['organization_info']['telephone'] : '无'}</Brief>
                    </Item>
                    <Row type="flex" justify="end" style={{ padding: '10px' }}>
                        <Icon className='item-icon-size' type="heart" theme="filled" />
                        关注度：{happiness_info['follow_count'] || 0}
                    </Row>
                </List>
                <WhiteSpace size="lg" /> */}
                <Card>
                    <Card.Header
                        title="机构介绍"
                    />
                    <Card.Body>
                        <div dangerouslySetInnerHTML={{ __html: (happiness_info['organization_info'] && happiness_info['organization_info']['description'] ? happiness_info['organization_info']['description'] : '待补充') }} />
                    </Card.Body>
                </Card>
                <Card className='list-conten'>
                    <Card.Header
                        title="活动"
                        extra={<span onClick={() => this.toAllActivity()}>查看全部活动</span>}
                    />
                    {(() => {
                        if (organization_activity && organization_activity.length > 0) {
                            return organization_activity.map((item: any, index: number) => {
                                return (
                                    <Row className="happiness-info-row" key={index} type='flex' justify='center' onClick={() => this.toActivity(item['id'])}>
                                        <Col className='list-col' span={10}><WhiteSpace size='sm' /><img src={item['photo'] && item['photo'][0] ? item['photo'][0] : 'https://www.e-health100.com/api/attachment/activityphoto/8336'} style={{ height: '72pt' }} /></Col>
                                        <Col span={14} className='list-col'>
                                            <WhiteSpace size='sm' />
                                            <Row className="happiness-info-row-title" style={{ WebkitBoxOrient: 'vertical' }}>{item['activity_name']}</Row>
                                            <Row>{getGoodLookTimeShow(item['begin_date'], item['end_date'])}</Row>
                                            <Row>
                                                <Col span={8}><span className="font-orange">{!item.hasOwnProperty('amount') || item['amount'] === 0 || item['amount'] === '0' || item['amount'] === undefined ? '免费' : `￥${item['amount']}`}</span></Col>
                                                <Col span={8}>{item['is_participate'] > 0 ? '已报名' : '未报名'}</Col>
                                                <Col span={8}>{item['participate_count'] || 0}/{item['max_quantity']}</Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                );
                            });
                        }
                        if (animating_activity === true) {
                            return (
                                <Row>
                                    <WhiteSpace size="lg" />
                                    <Row style={{ textAlign: 'center' }}>
                                        <Spin size="large" />
                                    </Row>
                                    <WhiteSpace size="lg" />
                                </Row>
                            );
                        }
                        return (
                            <Row type="flex" justify="center">
                                无数据
                            </Row>
                        );
                    })()}
                    <WhiteSpace size="lg" />
                </Card>
                <WhiteSpace size="lg" />
                <Card className='list-conten'>
                    <Card.Header
                        title="养老资讯"
                        extra={<span onClick={() => this.toAllArticle()}>查看全部资讯</span>}
                    />
                    {(() => {
                        if (organization_article && organization_article.length > 0) {
                            return organization_article.map((item: any, index: number) => {
                                return (
                                    <Row key={index} type='flex' justify='center' onClick={() => this.toArticle(item['id'])}>
                                        <Col className='list-col' span={10}><WhiteSpace size='sm' /><img src={item['app_img_list'] && item['app_img_list'][0] ? item['app_img_list'][0] : 'https://www.e-health100.com/api/attachment/activityphoto/8336'} style={{ height: '72pt' }} /></Col>
                                        <Col span={14} className='list-col'>
                                            <WhiteSpace size='sm' />
                                            <Row><strong>{item['title']}</strong></Row>
                                            <Row>{item['author']}</Row>
                                            <Row>{item['create_date']}</Row>
                                        </Col>
                                    </Row>
                                );
                            });
                        }
                        if (animating_article === true) {
                            return (
                                <Row>
                                    <WhiteSpace size="lg" />
                                    <Row style={{ textAlign: 'center' }}>
                                        <Spin size="large" />
                                    </Row>
                                    <WhiteSpace size="lg" />
                                </Row>
                            );
                        }
                        return (
                            <Row type="flex" justify="center">
                                无数据
                            </Row>
                        );
                    })()}
                    <WhiteSpace size="lg" />
                </Card>
                <WhiteSpace size="lg" />
                <Card className='list-conten'>
                    <Card.Header
                        title="活动小组"
                    />
                    <Row type="flex" justify="center">
                        无数据
                    </Row>
                    <WhiteSpace size="lg" />
                </Card>
            </div >
        );
    }
}

/**
 * 组件：幸福院详情视图控件
 * 控制幸福院详情视图控件
 */
@addon('HappinessHomeInfoView', '幸福院详情视图控件', '控制幸福院详情视图控件')
@reactControl(HappinessHomeInfoView, true)
export class HappinessHomeInfoViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}