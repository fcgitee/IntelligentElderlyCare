import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { Row, Button } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { WingBlank } from "antd-mobile";

/**
 * 组件：商城路由状态
 */
export interface RouterAllViewState extends ReactViewState {
    router_list: any[];
}
/**
 * 组件：商城路由
 */
export class RouterAllView extends ReactView<RouterAllViewControl, RouterAllViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            router_list: [
                {
                    label: '商城首页',
                    router: ROUTE_PATH.shoppingCenter,
                },
                {
                    label: '搜索页',
                    router: ROUTE_PATH.shoppingSearch,
                },
                {
                    label: '商品列表',
                    router: ROUTE_PATH.products,
                },
                {
                    label: '收货地址列表',
                    router: ROUTE_PATH.consigneeAddressList,
                },
                {
                    label: '商城订单列表',
                    router: ROUTE_PATH.productOrderList,
                },
                // {
                //     label: '申请退款-退款中',
                //     router: ROUTE_PATH.applyForRefund + '/1/退款中',
                // },
                // {
                //     label: '申请退款-已退款',
                //     router: ROUTE_PATH.applyForRefund + '/2/已退款',
                // },
            ]
        };
    }
    toRoute = (record: any) => {
        record.router && this.props!.history!.push(record.router);
    }
    render() {
        setMainFormTitle('商城路由');
        return (
            <WingBlank>
                {this.state.router_list && this.state.router_list.map && this.state.router_list.map((item: any, index: number) => {
                    return (
                        <Row type="flex" justify="center" key={index} style={{ marginTop: 20 }}>
                            <Button type="primary" onClick={() => this.toRoute(item)}>{item.label}</Button>
                        </Row>
                    );
                })}
            </WingBlank>
        );
    }
}

/**
 * 组件：商城路由
 * 控制商城路由
 */
@addon('RouterAllView', '商城路由', '控制商城路由')
@reactControl(RouterAllView, true)
export class RouterAllViewControl extends ReactViewControl {
}