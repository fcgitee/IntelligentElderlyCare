import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row, Col, Select } from "antd";
import { SearchBar, Toast, WingBlank } from "antd-mobile";
import "./index.less";
import { Product } from "./blocks/product";
import { ROUTE_PATH } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from "src/projects/app/util-tool";

/**
 * 组件：购物中心控件状态
 */
export interface ShoppingSearchViewState extends ReactViewState {
    is_loading: boolean;
    is_submited: boolean;
    keyword: string;
    type: string;
    sort: string;
    product_list: any;
    org_data_list: any;
    selected_sort: string;
    is_show_history: boolean;
    selected_price_direction: string;
}
/**
 * 组件：购物中心控件
 */
export class ShoppingSearchView extends ReactView<ShoppingSearchViewControl, ShoppingSearchViewState> {
    autoFocusInst: SearchBar | any;
    private condition_list = [
        '综合',
        '销量',
        '价格',
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: false,
            is_submited: false,
            is_show_history: true,
            keyword: '',
            sort: '',
            type: '商品',
            product_list: [],
            org_data_list: {},
            selected_sort: this.condition_list[0],
            selected_price_direction: 'reverse',
        };
    }
    componentDidMount() {
        this.autoFocusInst.focus();

        if (this.props.match!.params.key) {
            this.setState({
                type: this.props.match!.params.key
            });
        }
    }
    setSort = (selected_sort: string) => {
        let sort = '';
        let selected_price_direction = '';
        if (selected_sort === '销量') {
            sort = 'sales_num';
        } else if (selected_sort === '价格') {
            sort = 'price';
            selected_price_direction = this.state.selected_price_direction === 'reverse' ? 'unreverse' : 'reverse';
        } else {
            sort = '';
        }
        this.setState(
            {
                selected_sort,
                sort,
                selected_price_direction,
            },
            () => {
                this.submit();
            }
        );
    }
    insert = (value: any) => {
        this.setState({
            keyword: value,
        });
    }
    submit = () => {
        const { keyword, type, is_submited, sort, selected_price_direction } = this.state;
        if (is_submited) {
            return;
        }
        if (!type || (type !== '商家' && type !== '商品')) {
            Toast.info('请选择要搜索的类型！', 1);
            return;
        }
        if (!keyword) {
            Toast.info('请输入要搜索的关键词！', 1);
            return;
        }
        let searchHistory: string | null = localStorage.getItem('SEARCH_HISTORY');
        let newSearchHistoryArray: any[] = new Array();
        newSearchHistoryArray.push({
            type,
            keyword,
        });
        if (searchHistory) {
            let searchHistoryArray: any[] = JSON.parse(searchHistory);
            for (let i = 0; i < searchHistoryArray.length; i++) {
                if (searchHistoryArray[i]['keyword'] !== keyword && newSearchHistoryArray.length < 10) {
                    newSearchHistoryArray.push({
                        type: searchHistoryArray[i]['type'],
                        keyword: searchHistoryArray[i]['keyword'],
                    });
                }
            }
        }
        localStorage.setItem('SEARCH_HISTORY', JSON.stringify(newSearchHistoryArray));
        this.setState(
            {
                is_loading: true,
                is_submited: true,
            },
            () => {
                if (type === '商品') {
                    let searchParam: any = {
                        status: 'PASSED', active_status: '上架', sort: sort, title: keyword
                    };
                    if (sort !== '') {
                        searchParam['sort'] = sort;
                    }
                    if (selected_price_direction !== '') {
                        searchParam['selected_price_direction'] = selected_price_direction;
                    }
                    request(this, AppServiceUtility.shopping_mall_manage_service.get_goods_list!(searchParam))
                        .then((datas: any) => {
                            this.setState({
                                is_loading: false,
                                is_submited: false,
                                is_show_history: false,
                            });
                            if (datas && datas.result && datas.result.length > 0) {
                                this.setState({
                                    product_list: datas.result,
                                });
                            }
                        });
                } else if (type === '商家') {
                    let searchParam: any = {
                        title: keyword
                    };
                    if (sort !== '') {
                        searchParam['sort'] = sort;
                    }
                    if (selected_price_direction !== '') {
                        searchParam['selected_price_direction'] = selected_price_direction;
                    }
                    request(this, AppServiceUtility.shopping_mall_manage_service.get_products_list_by_organization!(searchParam))
                        .then((datas: any) => {
                            this.setState({
                                is_loading: false,
                                is_submited: false,
                                is_show_history: false,
                            });
                            if (datas) {
                                this.setState({
                                    org_data_list: datas,
                                });
                            }
                        });
                }
            }
        );
    }
    changeType = (e: any) => {
        this.setState({
            type: e,
        });
    }
    historySearch = (record: any) => {
        this.setState(
            {
                type: this.state.type || record.type,
                keyword: record.keyword,
            },
            () => {
                this.submit();
            }
        );
    }

    back = () => {
        history.back();
        // this.props!.history!.push(ROUTE_PATH.shoppingCenter);
    }

    ctrl = (record: any, extra: any = {}) => {
        if (!record || !record.id) {
            return;
        }
        this.props.history!.push(ROUTE_PATH.productDetail + '/' + record.id, {
            type: record.type || extra.type,
        });
    }

    render() {
        setMainFormTitle('商城搜索');
        const { is_loading, keyword, product_list, type, org_data_list, selected_sort, is_show_history } = this.state;

        let searchHistory: string | null = localStorage.getItem('SEARCH_HISTORY');
        let searchHistoryArray: any[] = [];
        if (searchHistory) {
            searchHistoryArray = JSON.parse(searchHistory);
        }
        return (
            <Spin spinning={is_loading}>
                <Row className="shopping-search-layout">
                    <Row>
                        {this.props.match!.params.key ? null : <Col span={6}>
                            <Select className="shopping-search-select" onChange={this.changeType} value={type}>
                                {['商家', '商品'].map((item: any, index: number) => {
                                    return <Select.Option key={item} >{item}</Select.Option>;
                                })}
                            </Select>
                        </Col>}
                        <Col span={this.props.match!.params.key ? 24 : 18}>
                            <SearchBar onChange={this.insert} value={keyword} onSubmit={() => { this.submit(); }} placeholder="搜索..." ref={ref => this.autoFocusInst = ref} />
                        </Col>
                    </Row>
                    {this.props.match!.params.key || !is_show_history ? null : <Row className="shopping-search-history">
                        <WingBlank>
                            <Row className="shopping-search-history-title">最近搜索</Row>
                            <Row className="shopping-search-history-list" type="flex" justify="start">
                                {searchHistoryArray.map((item: any, index: number) => {
                                    return (
                                        <span className="shopping-search-history-item" onClick={() => this.historySearch(item)} key={index}>{item.keyword}</span>
                                    );
                                })}
                            </Row>
                        </WingBlank>
                    </Row>}
                    {
                        type === '商品' && product_list && product_list.length ? (
                            <Row className="shopping-center-layout shopping-center-products">
                                <Row type="flex" justify="space-around" align="middle" className="products-search-condition">
                                    {this.condition_list.map((item: any, index: number) => {
                                        let className: any = [];
                                        if (item === '价格') {
                                            className.push('toggle');
                                            if (item === selected_sort) {
                                                className.push(this.state.selected_price_direction);
                                            }
                                        }
                                        if (item === selected_sort) {
                                            className.push('active');
                                        }
                                        return (
                                            <span className={className.join(' ')} key={index} onClick={() => this.setSort(item)}>{item}</span>
                                        );
                                    })}
                                </Row>
                                <WingBlank>
                                    <Row className="s_1_2_box">
                                        <Row className="s_1_2_list" type="flex">
                                            {
                                                product_list.map((item: any, idx: number) => {
                                                    return (
                                                        <Product product_info={item} key={idx} onClick={() => this.ctrl(item, { type: '商品' })} />
                                                    );
                                                })
                                            }
                                        </Row>
                                    </Row>
                                </WingBlank>
                            </Row>
                        ) : null
                    }
                    {
                        type === '商家' && org_data_list.hasOwnProperty('products') ? (
                            <Row className="shopping-center-layout shopping-center-products">
                                <Row type="flex" justify="space-around" align="middle" className="products-search-condition">
                                    {this.condition_list.map((item: any, index: number) => {
                                        let className: any = [];
                                        if (item === '价格') {
                                            className.push('toggle');
                                            if (item === selected_sort) {
                                                className.push(this.state.selected_price_direction);
                                            }
                                        }
                                        if (item === selected_sort) {
                                            className.push('active');
                                        }
                                        return (
                                            <span className={className.join(' ')} key={index} onClick={() => this.setSort(item)}>{item}</span>
                                        );
                                    })}
                                </Row>
                                <WingBlank>
                                    <Row className="s_1_2_box">
                                        <Row type="flex" justify="center" align="middle" className="s_module_name s_1_2_module_name"><span><i className="before" />服务<i className="after" /></span></Row>
                                        <Row className="s_1_2_list" type="flex">
                                            {
                                                org_data_list.services && org_data_list.services.result && org_data_list.services.result.length ? org_data_list.services.result.map((item: any, idx: number) => {
                                                    return (
                                                        <Product product_info={item} key={idx} onClick={() => this.ctrl(item, { type: '居家服务' })} />
                                                    );
                                                }) : null
                                            }
                                        </Row>
                                    </Row>
                                    <Row className="s_1_2_box">
                                        <Row type="flex" justify="center" align="middle" className="s_module_name s_1_2_module_name"><span><i className="before" />商品<i className="after" /></span></Row>
                                        <Row className="s_1_2_list" type="flex">
                                            {
                                                org_data_list.products && org_data_list.products.result && org_data_list.products.result.length ? org_data_list.products.result.map((item: any, idx: number) => {
                                                    return (
                                                        <Product product_info={item} key={idx} onClick={() => this.ctrl(item, { type: '商品' })} />
                                                    );
                                                }) : null
                                            }
                                        </Row>
                                    </Row>
                                </WingBlank>
                            </Row>
                        ) : null
                    }
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：购物中心控件
 * 控制购物中心控件
 */
@addon('ShoppingSearchView', '购物中心控件', '控制购物中心控件')
@reactControl(ShoppingSearchView, true)
export class ShoppingSearchViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}