import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Icon, Spin, Row } from "antd";
import { Toast, WhiteSpace, WingBlank } from "antd-mobile";
import "./index.less";
import { ProductOrder } from "./blocks/product-order";
import CopyToClipboard from "react-copy-to-clipboard";
import { OrderAction, switchAction } from "./order-action";
// const prompt = Modal.prompt;
/**
 * 组件：订单详情控件状态
 */
export interface ProductOrderDetailViewState extends ReactViewState {
    data_info: any;
    is_loading: boolean;
}
/**
 * 组件：订单详情控件
 */
export class ProductOrderDetailView extends ReactView<ProductOrderDetailViewControl, ProductOrderDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: [],
            is_loading: true,
        };
    }
    componentWillMount() {
        this.getData();
    }
    getData = () => {
        this.setState(
            {
                is_loading: true,
            },
            () => {
                if (this.props.match!.params.key) {
                    request(this, AppServiceUtility.shopping_mall_manage_service.get_product_orders_list_all!({ id: this.props.match!.params.key }))
                        .then((datas: any) => {
                            if (datas && datas.result && datas.result[0]) {
                                this.setState({
                                    data_info: datas.result[0],
                                });
                            }
                        })
                        .catch(err => {
                            console.error(err);
                        })
                        .finally(() => {
                            this.setState({
                                is_loading: false,
                            });
                        });
                }
            }
        );
    }
    ctrl = (e: any, type: string, record: any) => {
        switch (type) {
            // case 'insertRemark':
            //     prompt(
            //         '请输入备注',
            //         '',
            //         [
            //             { text: '取消' },
            //             {
            //                 text: '提交', onPress: (value: any) => {
            //                     this.setState(
            //                         {
            //                             is_loading: true,
            //                         },
            //                         () => {
            //                             request(this, AppServiceUtility.shopping_mall_manage_service.update_order_remark!({ id: this.props.match!.params.key, remark: value }))
            //                                 .then((datas: any) => {
            //                                     this.setState({
            //                                         is_loading: false,
            //                                     });
            //                                     if (datas === 'Success') {
            //                                         Toast.fail('修改成功！');
            //                                         let data_info = this.state.data_info;
            //                                         data_info['remark'] = value;
            //                                         this.setState({
            //                                             data_info,
            //                                         });
            //                                     } else {
            //                                         Toast.fail('修改失败！');
            //                                     }
            //                                 })
            //                                 .catch(err => {
            //                                     Toast.fail('修改失败！');
            //                                     this.setState({
            //                                         is_loading: false,
            //                                     });
            //                                 });
            //                         }
            //                     );
            //                 }
            //             },
            //         ],
            //         'default',
            //         record.remark || ''
            //     );
            //     break;
            default:
                switchAction(type, record.id, this, () => { this.getData(); });
                break;
        }
    }
    render() {
        setMainFormTitle('订单详情');
        const { data_info, is_loading } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row className="shopping-center-layout shopping-center-product-order-list">
                    <WhiteSpace size="lg" />
                    <WingBlank>
                        <Row className="product-order-section product-order-detail-status-address">
                            <Row className="product-order-detail-status">
                                <img src={data_info.product_image_urls && data_info.product_image_urls[0] ? data_info.product_image_urls[0] : null} alt="" />
                            </Row>
                            <Row className="product-order-detail-address">
                                <WingBlank>
                                    <WhiteSpace size="md" />
                                    <Row>收货人：{data_info.create_user_name}</Row>
                                    <WhiteSpace size="md" />
                                    <Row>收货地址：{data_info.address}</Row>
                                    <WhiteSpace size="md" />
                                </WingBlank>
                            </Row>
                        </Row>
                        <WhiteSpace size="md" />
                        {data_info && data_info.line_info && data_info.line_info[0] && data_info.line_info[0]['logistics_name'] && data_info.line_info[0]['logistics_number'] ? <><Row className="product-order-section">
                            <WhiteSpace size="md" />
                            <WingBlank>
                                <Row type="flex" justify="space-between" align="middle">
                                    <span className="font-important">物流信息</span>
                                    <span>{data_info.line_info[0]['logistics_name']} {data_info.line_info[0]['logistics_number']} <CopyToClipboard text={data_info.line_info[0]['logistics_number']} onCopy={() => { Toast.info("已复制到粘贴板"); }}>
                                        <Icon type="copy" />
                                    </CopyToClipboard></span>
                                </Row>
                            </WingBlank>
                            <WhiteSpace size="md" />
                        </Row>
                            <WhiteSpace size="md" /></> : null}
                        <ProductOrder product_order_info={data_info} inDetail={true} />
                        <WhiteSpace size="md" />
                        <Row className="product-order-section">
                            <WhiteSpace size="md" />
                            <WingBlank>
                                <Row type="flex" justify="space-between" align="middle">
                                    <span className="font-important">订单备注</span>
                                    <span>{data_info.remark || '-'}</span>
                                </Row>
                            </WingBlank>
                            <WhiteSpace size="md" />
                        </Row>
                        <WhiteSpace size="md" />
                        <Row className="product-order-section" type="flex" justify="space-between" align="middle">
                            <WingBlank>
                                <WhiteSpace size="md" />
                                <Row className="font-important">订单信息</Row>
                                <WhiteSpace size="lg" />
                                <Row>订单编号：{data_info.order_number}</Row>
                                <WhiteSpace size="md" />
                                <Row>下单时间：{data_info.create_date}</Row>
                                <WhiteSpace size="md" />
                                {data_info.pay_date ? <Row>
                                    <Row>付款时间：{data_info.pay_date}</Row>
                                    <WhiteSpace size="md" />
                                </Row> : null}
                                {data_info && data_info.line_info && data_info.line_info[0] && data_info.line_info[0]['delivery_date'] ? <Row>
                                    <Row>发货时间：{data_info.line_info[0]['delivery_date']}</Row>
                                    <WhiteSpace size="md" />
                                </Row> : null}
                            </WingBlank>
                        </Row>
                    </WingBlank>
                    {(() => {
                        let wp: any = [];
                        for (let i = 0; i < 6; i++) {
                            wp.push(<WhiteSpace size="lg" key={i} />);
                        }
                        return wp;
                    })()}
                    <div className="app-shop-btn-layout app-shop-btn-layout-fix app-shop-btn-layout-white product-order-btns">
                        <Row type="flex" justify="end">
                            <OrderAction order_info={data_info} on_click={this.ctrl} />
                        </Row>
                    </div>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：订单详情控件
 * 控制订单详情控件
 */
@addon('ProductOrderDetailView', '订单详情控件', '控制订单详情控件')
@reactControl(ProductOrderDetailView, true)
export class ProductOrderDetailViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}