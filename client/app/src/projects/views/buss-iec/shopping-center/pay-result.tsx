import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Row, Button, Icon, Spin } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { WhiteSpace, WingBlank } from "antd-mobile";

/**
 * 组件：支付结果状态
 */
export interface PayResultViewState extends ReactViewState {
    is_loading: boolean;
    data_info: any;
}
/**
 * 组件：支付结果
 */
export class PayResultView extends ReactView<PayResultViewControl, PayResultViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: true,
            data_info: [],
        };
    }
    componentWillMount() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_product_orders_list_all!({ id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    ctrl = (type: string, record: any) => {
        if (!record.id) {
            return;
        }
        if (type === 'orderDetail') {
            this.props!.history!.push(ROUTE_PATH.productOrderDetail + '/' + record.id);
        } else if (type === 'home') {
            this.props!.history!.push(ROUTE_PATH.shoppingCenter);
        }
    }
    render() {
        setMainFormTitle('支付结果');
        const { is_loading, data_info } = this.state;
        return (
            <Spin spinning={is_loading}>
                <WingBlank>
                    <Row type="flex" align="middle" className="order-status-layout">
                        {data_info.pay_status === 'PAID' ? <Row>
                            <Row type="flex" justify="center" align="middle">
                                <Icon type="check-circle" className="icon-style" />
                            </Row>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <Row type="flex" justify="center" align="middle" className="pay-status">支付成功</Row>
                        </Row> : null}
                        {data_info.pay_status === 'WAIT_PAY' ? <Row>
                            <Row type="flex" justify="center" align="middle">
                                <Icon type="close-circle" className="icon-style" />
                            </Row>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <Row type="flex" justify="center" align="middle" className="pay-status">支付未成功</Row>
                        </Row> : null}
                        <WhiteSpace size="lg" />
                        <WhiteSpace size="lg" />
                        <WhiteSpace size="lg" />
                        <Row type="flex" justify="space-between" align="middle" className="order-status-btn-row">
                            <Button onClick={() => this.ctrl('orderDetail', data_info)}>查看订单</Button>
                            <Button onClick={() => this.ctrl('home', data_info)}>返回首页</Button>
                        </Row>
                    </Row>
                </WingBlank>
            </Spin>
        );
    }
}

/**
 * 组件：支付结果
 * 控制支付结果
 */
@addon('PayResultView', '支付结果', '控制支付结果')
@reactControl(PayResultView, true)
export class PayResultViewControl extends ReactViewControl {
}