import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row } from "antd";
import { Carousel, SearchBar, WingBlank } from "antd-mobile";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Product } from "./blocks/product";

/**
 * 组件：购物中心控件状态
 */
export interface ShoppingCenterViewState extends ReactViewState {
    block_list: any;
    is_loading: boolean;
}
/**
 * 组件：购物中心控件
 */
export class ShoppingCenterView extends ReactView<ShoppingCenterViewControl, ShoppingCenterViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            block_list: [],
            is_loading: true,
        };
    }
    componentWillMount() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_app_blocks_list!({ recommend_list_condition: { recommend_status: true, product_status: 'PASSED', active_status: '上架', is_show_app: true } }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.length > 0) {
                    this.setState({
                        block_list: datas,
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    renderBlock = (block_list: any) => {
        return block_list && block_list.map && block_list.length ? block_list.map((item: any, index: number) => {
            switch (item.show_type) {
                // 轮播
                case '1x1':
                    return (
                        <Row className="s_1_1_box" key={index}>
                            <Carousel
                                autoplay={true}
                                infinite={true}
                                autoplayInterval={3000}
                                className="s_1_1_carousel"
                            >
                                {item.details ? item.details!.map((itm: any, idx: number) => (
                                    <span
                                        key={idx}
                                        onClick={() => this.toDetail(itm)}
                                    >
                                        {itm.image_url && itm.image_url[0] ? <img
                                            src={itm.image_url[0]}
                                            onLoad={() => {
                                                window.dispatchEvent(new Event('resize'));
                                            }}
                                        /> : <span className="empty-img" />}
                                        <Row style={{ background: 'rgba(0,0,0,0.1)', position: 'absolute', bottom: '0', width: '100%', color: '#fff', paddingLeft: '10px' }}><Row style={{ background: 'none', wordBreak: 'break-all', textOverflow: 'ellipsis', overflow: 'hidden', display: ' -webkit-box', WebkitLineClamp: 1, WebkitBoxOrient: 'vertical', }}>{itm.title}</Row></Row>
                                    </span>
                                )) : null}
                            </Carousel>
                        </Row>
                    );
                    break;
                case '1x2':
                    return (
                        <Row className="s_1_2_box" key={index}>
                            <Row type="flex" justify="center" align="middle" className="s_module_name s_1_2_module_name"><span><i className="before" />{item.name}<i className="after" /></span></Row>
                            {item.details ? (<Row className="s_1_2_list" type="flex">
                                {item.details!.map((itm: any, idx: number) => (
                                    <Product product_info={itm} key={idx} onClick={this.toDetail} />
                                ))}
                            </Row>) : null}
                            {item.all ? (<Row type="flex" justify="center" align="middle" className="s_1_2_all" onClick={() => this.toProducts(item.id)}>查看全部</Row>) : null}
                        </Row>
                    );
                    break;
                case '2x5':
                    // 转换为二维数组
                    let details_array: any = [];
                    let current_index: number = 0;
                    if (item.details && item.details.length) {
                        for (let i = 0; i < item.details.length; i++) {
                            // 没有这个索引的数组就创建这个索引的数组
                            if (!details_array[current_index]) {
                                details_array[current_index] = [];
                            }
                            // push进这个索引的数组
                            details_array[current_index].push(item.details[i]);
                            if (details_array[current_index].length === 10) {
                                // 索引+1
                                ++current_index;
                            }
                        }
                    }
                    return (
                        <Row className="s_2_5_box" key={index}>
                            {details_array.length ? (<Carousel
                                className="s_2_5_carousel"
                                dots={false}
                                // 这里要做个判断，antd有毛病，只有一格的时候给间距会导致样式紊乱
                                cellSpacing={item.details.length > 10 ? 20 : 0}
                            >
                                {details_array!.map((itm: any, idx: number) => (
                                    <Row className="s_2_5_list" type="flex" key={idx}>
                                        {itm.map((im: any, ix: number) => {
                                            return (
                                                <Row className="s_2_5_section" key={ix} onClick={() => this.toServices(im.product_id)}>
                                                    <Row className="s_2_5_picture">
                                                        {im.image_url && im.image_url[0] ? <img src={im.image_url[0]} /> : <span className="empty-img" />}
                                                    </Row>
                                                    <Row className="s_2_5_title">
                                                        {im.title}
                                                    </Row>
                                                </Row>
                                            );
                                        })}
                                    </Row>
                                ))}
                            </Carousel>) : null}
                        </Row>
                    );
                    break;
                default:
                    return null;
                    break;
            }
        }) : null;
    }
    toSearch = () => {
        this.props!.history!.push(ROUTE_PATH.shoppingSearch);
    }
    toProducts = (id: string) => {
        this.props!.history!.push(ROUTE_PATH.products + '/' + id);
    }
    toServices = (id: string) => {
        this.props!.history!.push(ROUTE_PATH.services + '/' + id);
    }
    toDetail = (record: any) => {
        if (!record || !record.product_id) {
            return;
        }
        this.props.history!.push(ROUTE_PATH.productDetail + '/' + record.product_id, {
            type: record.type,
        });
    }
    render() {
        setMainFormTitle('商城中心');
        const { block_list, is_loading } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row className="shopping-center-layout">
                    <WingBlank>
                        <SearchBar onFocus={() => this.toSearch()} placeholder="搜索..." />
                        {this.renderBlock(block_list)}
                    </WingBlank>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：购物中心控件
 * 控制购物中心控件
 */
@addon('ShoppingCenterView', '购物中心控件', '控制购物中心控件')
@reactControl(ShoppingCenterView, true)
export class ShoppingCenterViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}