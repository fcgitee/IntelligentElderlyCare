import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Row, Button, Icon, Spin } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { WhiteSpace, WingBlank } from "antd-mobile";

/**
 * 组件：申请退款状态
 */
export interface ApplyForRefundViewState extends ReactViewState {
    is_loading: boolean;
    data_info: any;
}
/**
 * 组件：申请退款
 */
export class ApplyForRefundView extends ReactView<ApplyForRefundViewControl, ApplyForRefundViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_loading: true,
            data_info: [],
        };
    }
    componentWillMount() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_product_orders_list_all!({ id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    ctrl = (type: string, record: any) => {
        if (!record.id) {
            return;
        }
        if (type === 'orderDetail') {
            this.props!.history!.push(ROUTE_PATH.productOrderDetail + '/' + record.id);
        } else if (type === 'home') {
            this.props!.history!.push(ROUTE_PATH.shoppingCenter);
        }
    }
    render() {
        setMainFormTitle('申请退款');
        const { is_loading, data_info } = this.state;
        return (
            <Spin spinning={is_loading}>
                <WingBlank>
                    <Row type="flex" align="middle" className="order-status-layout">
                        {data_info.pay_status === 'REFUNDING' ? <Row>
                            <Row type="flex" justify="center" align="middle">
                                <Icon type="exclamation-circle" className="icon-style" />
                            </Row>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <Row type="flex" justify="center" align="middle">已提交退款申请，请等待商家联系</Row>
                        </Row> : null}
                        {data_info.pay_status === 'REFUNDED' ? <Row>
                            <Row type="flex" justify="center" align="middle">
                                <Icon type="check-circle" className="icon-style" />
                            </Row>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <Row type="flex" justify="center" align="middle">退款成功，支付金额已原路返回</Row>
                        </Row> : null}
                        {data_info.pay_status === 'REFUND_REFUSED' ? <Row>
                            <Row type="flex" justify="center" align="middle">
                                <Icon type="close-circle" className="icon-style" />
                            </Row>
                            <WhiteSpace size="lg" />
                            <WhiteSpace size="lg" />
                            <Row type="flex" justify="center" align="middle">退款被拒绝</Row>
                        </Row> : null}
                        <WhiteSpace size="lg" />
                        <WhiteSpace size="lg" />
                        <WhiteSpace size="lg" />
                        <Row type="flex" justify="space-between" align="middle" className="order-status-btn-row">
                            <Button onClick={() => this.ctrl('orderDetail', data_info)}>查看订单</Button>
                            <Button onClick={() => this.ctrl('home', data_info)}>返回首页</Button>
                        </Row>
                    </Row>
                </WingBlank>
            </Spin>
        );
    }
}

/**
 * 组件：申请退款
 * 控制申请退款
 */
@addon('ApplyForRefundView', '申请退款', '控制申请退款')
@reactControl(ApplyForRefundView, true)
export class ApplyForRefundViewControl extends ReactViewControl {
}