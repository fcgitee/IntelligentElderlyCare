import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Row } from "antd";
import { WhiteSpace, WingBlank } from "antd-mobile";
import { filterHTMLTag } from "src/projects/views/home";
import { OrderAction } from "../order-action";

/**
 * 组件：商品订单详情状态
 */
export class ProductOrderState {
}

/**
 * 组件：商品订单详情
 * 商品订单详情
 */
export class ProductOrder extends React.Component<ProductOrderControl, ProductOrderState> {

    componentWillMount() {
    }
    onClick = (e: any, type: string, record: any) => {
        e.stopPropagation && e.stopPropagation();
        e.preventDefault && e.preventDefault();
        this.props.onClick && record && this.props.onClick(type, record);
    }
    render() {
        let { product_order_info } = this.props;

        // 初始化，抑制报错
        if (!product_order_info) {
            product_order_info = {};
        }

        return (
            <Row className="product-order-section" onClick={(e) => this.onClick(e, 'detail', product_order_info)}>
                <WhiteSpace size="md" />
                <WingBlank>
                    <Row className="product-order-type-status" type="flex" justify="space-between">
                        <span className="product-order-type">{product_order_info.organization_name}</span>
                        {this.props.inDetail ? null : <span className="product-order-status">{product_order_info.order_status_name}</span>}
                    </Row>
                    <WhiteSpace size="md" />
                    {product_order_info.line_info && product_order_info.line_info.map ? product_order_info.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index}>
                                <WhiteSpace size="md" />
                                <Row className="product-order-picture-name-price" type="flex" justify="space-between">
                                    <div className="product-order-picture">
                                        <img src={item.product_image_urls && item.product_image_urls[0] ? item.product_image_urls[0] : null} alt="" />
                                    </div>
                                    <div className="product-order-name-desc">
                                        <div className="product-order-name maxLine2" style={{ WebkitBoxOrient: 'vertical' }}>
                                            {item.product_title}
                                        </div>
                                        <div className="product-order-desc maxLine2" style={{ WebkitBoxOrient: 'vertical' }}>
                                            {filterHTMLTag(item.product_content)}
                                        </div>
                                    </div>
                                    <div className="product-order-price-num">
                                        <div className="product-order-price">
                                            ￥{item.unit_price || 0}
                                        </div>
                                        <div className="product-order-num">
                                            x{item.number}
                                        </div>
                                    </div>
                                </Row>
                            </Row>
                        );
                    }) : null}
                    <WhiteSpace size="md" />
                    {this.props.inOrder || !product_order_info.line_info ? null : <>
                        <Row className="product-order-amount" type="flex" justify="end">
                            实付：￥{isNaN(product_order_info.final_amount) ? null : (product_order_info.final_amount)}（{product_order_info.postage_amount === 0 ? '包邮' : `含运费 ￥${product_order_info.postage_amount}`}）
                        </Row>
                        <WhiteSpace size="md" />
                    </>}
                    {this.props.inDetail ? null : <>
                        <Row className="product-order-btns" type="flex" justify="end">
                            <OrderAction order_info={product_order_info} on_click={this.onClick} />
                        </Row>
                        <WhiteSpace size="md" />
                    </>}
                </WingBlank>
            </Row>
        );
    }
}

/**
 * 控件：商品订单详情控制器
 * 商品订单详情
 */
@addon('ProductOrder', '商品订单详情', '商品订单详情')
@reactControl(ProductOrder, true)
export class ProductOrderControl extends BaseReactElementControl {
    /** 基础数据 */
    product_order_info?: any;
    onClick?: Function;
    // 在订单详情里，需要隐藏一些数据
    inDetail?: boolean;
    // 在订单提交页面，需要隐藏一些数据
    inOrder?: boolean;
}