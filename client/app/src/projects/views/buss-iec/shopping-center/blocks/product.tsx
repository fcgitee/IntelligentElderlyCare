import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Row } from "antd";
/**
 * 组件：商品状态
 */
export class ProductState {
}

/**
 * 组件：商品
 * 商品
 */
export class Product extends React.Component<ProductControl, ProductState> {

    componentWillMount() {
    }
    onClick = (record: any) => {
        this.props.onClick && record && this.props.onClick(record);
    }
    render() {
        let { product_info } = this.props;

        // 初始化，抑制报错
        if (!product_info) {
            product_info = {};
        }

        let picture: string = '';
        if (product_info.image_urls && product_info.image_urls[0]) {
            picture = product_info.image_urls[0];
        }
        if (product_info.image_url && product_info.image_url[0]) {
            picture = product_info.image_url[0];
        }

        return (
            <Row className="s_1_2_section" onClick={() => this.onClick(product_info)}>
                <Row className="s_1_2_picture">
                    {product_info.type && product_info.type === '居家服务' ? <span className="icon-puzzle" /> : null}
                    {picture ? <img src={picture} /> : <span className="empty-img" />}
                </Row>
                <Row className="s_1_2_title maxLine2" style={{ WebkitBoxOrient: 'vertical' }}>
                    {product_info.title}
                </Row>
                <Row className="s_1_2_data" type="flex" justify="space-between" align="bottom">
                    <span>{product_info.price ? `￥${product_info.price}` : null}</span>
                    <span>销量 {product_info.sales_num || 0}</span>
                </Row>
                <Row className="s_1_2_service_name" type="flex" justify="start" align="middle">
                    {product_info.organization_name}
                </Row>
            </Row>
        );
    }
}

/**
 * 控件：商品控制器
 * 商品
 */
@addon('Product', '商品', '商品')
@reactControl(Product, true)
export class ProductControl extends BaseReactElementControl {
    /** 基础数据 */
    product_info?: any;
    onClick?: Function;
}