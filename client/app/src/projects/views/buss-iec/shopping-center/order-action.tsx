import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Toast, Modal, Button } from "antd-mobile";
import React from "react";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/app/util-tool";
const alert = Modal.alert;

// 通用接口
export function commonUpdateOrder(id: string, obj: any, condition: any = {}, cb?: Function, action?: string) {
    const is_loading = obj.state.is_loading;
    // 设为已完成
    if (is_loading || !id) {
        return;
    }
    obj.setState(
        {
            is_loading: true,
        },
        () => {
            request(obj, AppServiceUtility.shopping_mall_manage_service.update_order!({ id, ...condition }))
                .then((datas: any) => {
                    if (datas === 'Success') {
                        Toast.success(`${action || '操作'}成功！`, 1, () => {
                            cb && cb();
                        });
                    } else {
                        Toast.fail(`${action || '操作'}失败，${datas}！`);
                    }
                })
                .catch(err => {
                    Toast.fail(`${action || '操作'}失败！`);
                }).finally(() => {
                    obj.setState({
                        is_loading: false,
                    });
                });
        }
    );
}

// 根据对订单的操作类型展示弹窗
export function switchAction(action_type: string, primary_id: string, obj: any, cb?: Function) {
    switch (action_type) {
        // 取消订单
        case 'setCacneled':
            alert('取消订单', '这个宝贝好抢手的，真的要取消吗？', [
                { text: '再想一下', onPress: () => { } },
                {
                    text: '确认取消', onPress: () => {
                        commonUpdateOrder(primary_id, obj, { order_status: 'CANCELED' }, cb, '取消');
                    }
                },
            ]);
            break;
        // 删除订单
        case 'setDeleted':
            alert('删除订单', '删除后无法找回，请谨慎操作！', [
                {
                    text: '再想一下', onPress: () => { }
                },
                {
                    text: '确认删除', onPress: () => {
                        commonUpdateOrder(primary_id, obj, { order_status: 'DELETED' }, cb, '删除');
                    }
                },
            ]);
            break;
        // 确认收货
        case 'setCompleted':
            alert('确认收货', '确认收到货了吗？', [
                {
                    text: '还没有', onPress: () => { }
                },
                {
                    text: '确认收货', onPress: () => {
                        commonUpdateOrder(primary_id, obj, { order_status: 'COMPLETED' }, cb, '确认收货');
                    }
                },
            ]);
            break;
        // 退款
        case 'setRefunding':
            alert('退款', '您确定要申请退款吗？退款后订单支付金额将原路退回！', [
                {
                    text: '再想一下', onPress: () => { }
                },
                {
                    text: '确认申请', onPress: () => {
                        commonUpdateOrder(primary_id, obj, { pay_status: 'REFUNDING' }, cb, '申请退款');
                    }
                },
            ]);
            break;
        // 设为已付款（测试）
        case 'setPaid':
            alert('设为已付款（测试）', '此操作会设为已付款（测试）', [
                {
                    text: '再想一下', onPress: () => { }
                },
                {
                    text: '确认付款', onPress: () => {
                        commonUpdateOrder(primary_id, obj, { order_status: 'PAID', pay_status: 'PAID' }, () => { obj.props!.history!.push(ROUTE_PATH.payResult + '/' + primary_id); }, '付款');
                    }
                },
            ]);
            break;
        // 查看退款详情
        case 'checkRefund':
            obj.props!.history!.push(ROUTE_PATH.applyForRefund + '/' + primary_id);
            break;
        default: break;
    }
}

export class OrderAction extends React.Component<OrderActionControl, {}> {

    onClick = (events: any, type: string, record: any) => {
        this.props.on_click && record && this.props.on_click(events, type, record);
    }
    renderActionButtons(order_info: any) {
        if (order_info.order_status === 'WAIT_PAY') {
            return (
                <>
                    <Button className='app-shop-btn product-order-btn' size="small" onClick={(e) => this.onClick(e, 'setCacneled', order_info)}>取消订单</Button>
                    <Button className='app-shop-btn product-order-btn' size="small" onClick={(e) => this.onClick(e, 'setPaid', order_info,)}>付款</Button>
                </>
            );
        } else if (order_info.order_status === 'PAID') {
            if (order_info.pay_status === 'REFUNDED' || order_info.pay_status === 'REFUNDING' || order_info.pay_status === 'REFUNDED') {
                return (
                    <Button className="app-shop-btn product-order-btn gray" size="small" onClick={(e) => this.onClick(e, 'checkRefund', order_info)}>查看退款详情</Button>
                );
            } else {
                return (
                    <>
                        <Button className="app-shop-btn product-order-btn gray" size="small" onClick={(e) => this.onClick(e, 'setRefunding', order_info)}>退款</Button>
                    </>
                );
            }
        } else if (order_info.order_status === 'DELIVERED') {
            return (
                <>
                    <Button className="app-shop-btn product-order-btn gray" size="small" onClick={(e) => this.onClick(e, 'setCompleted', order_info)}>确认收货</Button>
                </>
            );
        } else if (order_info.order_status === 'CANCELED' || order_info.order_status === 'COMPLETED' || order_info.order_status === 'REFUNDED' || order_info.order_status === 'REFUND_REFUSED') {
            return (
                <>
                    <Button className="app-shop-btn product-order-btn" size="small" onClick={(e) => this.onClick(e, 'setDeleted', order_info)}>删除订单</Button>
                    {order_info.pay_status === 'REFUNDING' ? <Button className="app-shop-btn product-order-btn gray" size="small" onClick={(e) => this.onClick(e, 'checkRefund', order_info)}>查看退款进度</Button> : null}
                    {order_info.pay_status === 'REFUNDED' || order_info.pay_status === 'REFUND_REFUSED' ? <Button className="app-shop-btn product-order-btn gray" size="small" onClick={(e) => this.onClick(e, 'checkRefund', order_info)}>查看退款详情</Button> : null}
                </>
            );
        }
        return null;
    }
    render() {
        let { order_info } = this.props;

        return (
            this.renderActionButtons(order_info)
        );
    }
}

@addon('OrderAction', '商品', '商品')
@reactControl(OrderAction, true)
export class OrderActionControl extends BaseReactElementControl {
    /** 基础数据 */
    order_info?: any;
    on_click?: Function;
}