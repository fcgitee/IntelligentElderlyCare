import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row, Icon } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { NavBar, WhiteSpace, WingBlank } from "antd-mobile";
import { ProductOrder } from "./blocks/product-order";
import { switchAction } from "./order-action";

/**
 * 组件：商城商品订单状态
 */
export interface ProductOrderListViewState extends ReactViewState {
    products_order_list: any;
    is_loading: boolean;
    selected_status: string;
    selected_condition: any;
}
/**
 * 组件：商城商品订单
 */
export class ProductOrderListView extends ReactView<ProductOrderListViewControl, ProductOrderListViewState> {
    private condition_list = [
        '全部',
        '待发货',
        '待收货',
        '已完成',
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            products_order_list: [],
            is_loading: true,
            selected_status: this.condition_list[0],
            selected_condition: {},
        };
    }
    componentWillMount() {
        this.getDataList();
    }
    getDataList = () => {
        this.setState(
            {
                is_loading: true,
            },
            () => {
                request(this, AppServiceUtility.shopping_mall_manage_service.get_product_orders_list_all!(this.state.selected_condition, 1, 10))
                    .then((datas: any) => {
                        this.setState({
                            is_loading: false,
                        });
                        if (datas && datas.result) {
                            this.setState({
                                products_order_list: datas.result,
                            });
                        }
                    })
                    .catch(err => {
                        this.setState({
                            is_loading: false,
                        });
                    });
            }
        );
    }
    ctrl = (type: string, record: any) => {
        if (!record || !record.id) {
            return;
        }
        switch (type) {
            case 'detail':
                this.props!.history!.push(ROUTE_PATH.productOrderDetail + '/' + record.id);
                break;
            default:
                switchAction(type, record.id, this, () => { this.getDataList(); });
                break;
        }
    }
    renderProductOrder = (product_list: any) => {
        return product_list && product_list.length ? (
            <Row className="product-order-list">
                {
                    product_list.map((item: any, idx: number) => {
                        return (
                            <Row key={idx}>
                                <WhiteSpace size="lg" />
                                <ProductOrder product_order_info={item} key={idx} onClick={this.ctrl} />
                            </Row>
                        );
                    })
                }
            </Row>
        ) : null;
    }
    setStatus = (status: string) => {
        let param: any = {};
        if (status === '全部') {
            delete param['order_status'];
        } else if (status === '待发货') {
            param['order_status'] = 'PAID';
        } else if (status === '待收货') {
            param['order_status'] = 'DELIVERED';
        } else if (status === '已完成') {
            param['order_status'] = 'COMPLETED';
        }
        this.setState(
            {
                selected_status: status,
                selected_condition: param,
            },
            () => {
                this.getDataList();
            }
        );
    }
    toSearch = () => {
        this.props!.history!.push(ROUTE_PATH.shoppingSearch + '/订单');
    }
    render() {
        setMainFormTitle('我的订单');
        const { products_order_list, is_loading, selected_status } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row>
                    <NavBar
                        icon={<Icon type={"left"} onClick={() => history.back()} />}
                        rightContent={<Icon type={"search"} onClick={() => this.toSearch()} />}
                    >
                        {'我的订单'}
                    </NavBar>
                    <Row className="shopping-center-layout shopping-center-product-order-list">
                        <Row type="flex" justify="space-around" align="middle" className="product-order-condition">
                            {this.condition_list.map((item: any, index: number) => {
                                return (
                                    <span className={item === selected_status ? 'active' : ''} key={index} onClick={() => this.setStatus(item)}>{item}</span>
                                );
                            })}
                        </Row>
                        <WingBlank>
                            {this.renderProductOrder(products_order_list)}
                        </WingBlank>
                    </Row>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：商城商品订单
 * 控制商城商品订单
 */
@addon('ProductOrderListView', '商城商品订单', '控制商城商品订单')
@reactControl(ProductOrderListView, true)
export class ProductOrderListViewControl extends ReactViewControl {
}