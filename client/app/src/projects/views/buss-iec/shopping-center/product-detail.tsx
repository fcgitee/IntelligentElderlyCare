import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row } from "antd";
import { Button, Carousel, Toast, WhiteSpace, WingBlank } from "antd-mobile";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
/**
 * 组件：商品/服务详情控件状态
 */
export interface ProductDetailViewState extends ReactViewState {
    data_info: any;
    is_loading: boolean;
    rate: number;
    is_submited: boolean;
}
/**
 * 组件：商品/服务详情控件
 */
export class ProductDetailView extends ReactView<ProductDetailViewControl, ProductDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: [],
            is_loading: true,
            is_submited: false,
            rate: 0,
        };
    }
    componentWillMount() {
        if (this.props.location!.state && this.props.location!.state.type) {
            if (this.props.location!.state.type === '商品') {
                this.getProduct();
            } else if (this.props.location!.state.type === '居家服务') {
                this.getServices();
            }
        } else {
            this.getProduct();
        }
    }
    getProduct() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_goods_list!({ status: 'PASSED', id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    getServices() {
        request(this, AppServiceUtility.shopping_mall_manage_service.get_services_list!({ status: '通过', state: '启用', id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    ctrl = (type: string, record: any) => {
        console.log(type, record);
    }
    changeRate = (e: any) => {
        this.setState({
            rate: e,
        });
    }
    submit = () => {
        this.props!.history!.push(ROUTE_PATH.newSubmitOrder + '/' + this.props.match!.params.key, {
            type: this.props.location!.state && this.props.location!.state.type ? this.props.location!.state.type : '商品',
        });
    }
    noTelephone = () => {
        Toast.info('暂无客服热线！');
        return;
    }
    render() {
        setMainFormTitle('商品详情');
        const { data_info, is_loading, is_submited } = this.state;
        return (
            <Spin spinning={is_loading}>
                <Row className="shopping-center-layout">
                    <WhiteSpace size="lg" />
                    <WingBlank>
                        <Row className="product-detail-section">
                            {data_info && data_info.image_urls && data_info.image_urls.map ? <Row className="product-detail-carousel">
                                {this.props.location!.state && this.props.location!.state.type === '居家服务' ? <span className="icon-puzzle" /> : null}
                                <Carousel
                                    autoplay={true}
                                    infinite={true}
                                    autoplayInterval={3000}
                                    className="product-detail-carousel-main"
                                >
                                    {data_info.image_urls.map((itm: any, idx: number) => (
                                        <span
                                            key={idx}
                                        >
                                            <img
                                                src={itm}
                                                onLoad={() => {
                                                    window.dispatchEvent(new Event('resize'));
                                                }}
                                            />
                                        </span>
                                    ))}
                                </Carousel>
                            </Row> : null}
                            <WhiteSpace size="sm" />
                            <Row className="product-detail-title-price-sales">
                                <WingBlank>
                                    <Row>{data_info.title}</Row>
                                    <WhiteSpace size="sm" />
                                    <Row type="flex" justify="space-between" align="bottom">
                                        <span className="product-detail-price">{data_info.price ? `￥${data_info.price}` : null}</span>
                                        <span>销量 {data_info.sales_num || 0}</span>
                                    </Row>
                                    <WhiteSpace size="sm" />
                                    {this.props.match!.params.key === '商品' ? <Row>
                                        <Row type="flex" justify="space-between" align="bottom">
                                            <span>{`快递：${data_info.postage_price || 0}`}</span>
                                            <span>{`库存 ${data_info.stock_num > 999 ? '999+' : (data_info.stock_num ? data_info.stock_num : 0)}`}</span>
                                        </Row>
                                        <WhiteSpace size="sm" />
                                    </Row> : null}
                                </WingBlank>
                            </Row>
                        </Row>
                        <WhiteSpace size="lg" />
                        {/* <Row className="product-detail-section product-detail-organization" >
                            <WingBlank>
                                <Row className="product-oraganization-main">
                                    <Row className="product-oraganization-picture">
                                        {data_info.organization_picture && data_info.organization_picture[0] ? <img src={data_info.organization_picture[0]} /> : null}
                                    </Row>
                                    <Row className="product-oraganization-name-rate" type="flex">
                                        <Row>{data_info.organization_name}</Row>
                                        <Row>
                                            <Rate character={<Icon type="heart" />} onChange={this.changeRate} style={{ fontSize: '12px' }} value={rate} allowHalf={true} />
                                            {rate ? <span className="ant-rate-text" style={{ fontSize: '12px' }}>{rate}分</span> : null}
                                        </Row>
                                    </Row>
                                    <Row className="product-oraganization-hotline">
                                        {data_info.organization_telephone ? <a href={`tel:` + data_info.organization_telephone}>客服热线</a> : <a onClick={this.noTelephone}>客服热线</a>}
                                    </Row>
                                </Row>
                            </WingBlank>
                        </Row>
                        <WhiteSpace size="lg" /> */}
                        <Row className="product-detail-section">
                            <WingBlank>
                                <WhiteSpace size="sm" />
                                <Row type="flex" justify="center" align="middle" className="s_module_name"><span><i className="before" />详情介绍<i className="after" /></span></Row>
                                <WhiteSpace size="sm" />
                                <Row className="reach-text">
                                    <div dangerouslySetInnerHTML={{ __html: data_info.content || '' }} />
                                </Row>
                                <WhiteSpace size="sm" />
                            </WingBlank>
                        </Row>
                        {(() => {
                            let wp: any = [];
                            for (let i = 0; i < 6; i++) {
                                wp.push(<WhiteSpace size="lg" key={i} />);
                            }
                            return wp;
                        })()}
                        <div className="app-shop-btn-layout app-shop-btn-layout-fix app-shop-btn-layout-white">
                            <Button className="app-shop-btn" style={{ width: '100%' }} disabled={is_submited} type='primary' onClick={() => { this.submit(); }}>立即购买</Button>
                        </div>
                    </WingBlank>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：商品/服务详情控件
 * 控制商品/服务详情控件
 */
@addon('ProductDetailView', '商品/服务详情控件', '控制商品/服务详情控件')
@reactControl(ProductDetailView, true)
export class ProductDetailViewControl extends ReactViewControl {
    // constructor() {
    //     super();
    // }
}