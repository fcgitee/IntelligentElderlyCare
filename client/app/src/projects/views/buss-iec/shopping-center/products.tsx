import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { request, setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { Spin, Row, Icon } from "antd";
import "./index.less";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { Product } from "./blocks/product";
import { NavBar, WingBlank } from "antd-mobile";

/**
 * 组件：商城商品控件状态
 */
export interface ProductsViewState extends ReactViewState {
    products_list: any;
    service_type_list: any;
    is_loading: boolean;
    selected_sort: string;
    selected_type: string;
    selected_condition: any;
    main_title: string;
    parent_info: any;
    selected_price_direction: string;
}
/**
 * 组件：商城商品控件
 */
export class ProductsView extends ReactView<ProductsViewControl, ProductsViewState> {
    private condition_list = [
        '综合',
        '销量',
        '价格',
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            products_list: [],
            is_loading: true,
            service_type_list: [],
            selected_condition: { recommend_status: true, product_status: 'PASSED', active_status: '上架', is_show_app: true },
            selected_sort: this.condition_list[0],
            selected_type: '全部',
            main_title: '',
            parent_info: {},
            selected_price_direction: 'reverse',
        };
    }
    componentWillMount() {
        let selected_condition = this.state.selected_condition;
        selected_condition['product_category_id'] = this.props.match!.params.key;
        if (this.props.type && this.props.type === '服务') {
            let condition: any = {};
            if (this.props.match!.params.key) {
                condition['parent_id'] = this.props.match!.params.key;
            }
            request(this, AppServiceUtility.shopping_mall_manage_service.get_service_type_list!(condition))
                .then((datas: any) => {
                    if (datas && datas.result && datas.result.length > 0) {
                        this.setState(
                            {
                                selected_type: this.props.match!.params.key,
                                service_type_list: datas.result,
                                parent_info: datas.parent_info,
                                main_title: (datas.parent_info && datas.parent_info.name ? datas.parent_info.name : ''),
                                selected_condition,
                            },
                            () => {
                                this.getDataList(this.state.selected_condition);
                            }
                        );
                    }
                });
        } else {
            this.getDataList(this.state.selected_condition);
        }
    }
    getDataList = (condition: any = {}) => {
        this.setState(
            {
                is_loading: true,
            },
            () => {
                if (this.props.match!.params.key && this.props.type !== '服务') {
                    // 有id并且不是服务类型，说明是从首页模块的全部进的
                    request(this, AppServiceUtility.shopping_mall_manage_service.get_app_blocks_list!({ id: this.props.match!.params.key, recommend_list_condition: condition }))
                        .then((datas: any) => {
                            this.setState({
                                is_loading: false,
                            });
                            if (datas && datas.length > 0) {
                                this.setState({
                                    products_list: datas[0].details,
                                    main_title: datas[0].name,
                                });
                            }
                        })
                        .catch(err => {
                            this.setState({
                                is_loading: false,
                            });
                        });
                } else {
                    // 有ID并且是服务，或者没有ID，都进商品筛选
                    if (this.props.type === '服务') {
                        request(this, AppServiceUtility.shopping_mall_manage_service.get_service_product_list!(condition))
                            .then((datas: any) => {
                                this.setState({
                                    is_loading: false,
                                });
                                if (datas && datas.result) {
                                    this.setState({
                                        products_list: datas.result,
                                    });
                                }
                            })
                            .catch((err) => {
                                this.setState({
                                    is_loading: false,
                                });
                            });
                    } else {
                        request(this, AppServiceUtility.shopping_mall_manage_service.get_goods_list!(condition))
                            .then((datas: any) => {
                                this.setState({
                                    is_loading: false,
                                });
                                if (datas && datas.result) {
                                    this.setState({
                                        products_list: datas.result,
                                    });
                                }
                            })
                            .catch((err) => {
                                this.setState({
                                    is_loading: false,
                                });
                            });
                    }
                }
            }
        );
    }
    toProductDetail = (record: any) => {
        if (!record) {
            return;
        }
        if (this.props.match!.params.key) {
            record.product_id && this.props!.history!.push(ROUTE_PATH.productDetail + '/' + record.product_id, {
                type: record.type,
            });
        } else {
            record.id && this.props!.history!.push(ROUTE_PATH.productDetail + '/' + record.id, {
                type: record.type,
            });
        }
    }
    renderProducts = (product_list: any) => {
        return product_list && product_list.length ? (
            <Row className="s_1_2_box">
                <Row className="s_1_2_list" type="flex">
                    {
                        product_list.map((item: any, idx: number) => {
                            return (
                                <Product product_info={item} key={idx} onClick={() => this.toProductDetail(item)} />
                            );
                        })
                    }
                </Row>
            </Row>
        ) : null;
    }
    setSort = (sort: string) => {
        let { selected_condition, selected_price_direction } = this.state;
        if (sort === '销量') {
            selected_condition['sort'] = 'sales_num';
            selected_condition['selected_price_direction'] && delete selected_condition['selected_price_direction'];
        } else if (sort === '价格') {
            selected_condition['sort'] = 'price';
            selected_condition['selected_price_direction'] = selected_price_direction === 'reverse' ? 'unreverse' : 'reverse';
        } else {
            selected_condition['sort'] && delete selected_condition['sort'];
            selected_condition['selected_price_direction'] && delete selected_condition['selected_price_direction'];
        }
        this.setState(
            {
                selected_sort: sort,
                selected_condition,
                selected_price_direction: selected_condition['selected_price_direction'],
            },
            () => {
                this.getDataList(selected_condition);
            }
        );
    }
    setType = (type: string) => {
        let selected_condition = this.state.selected_condition;
        if (type && type !== '全部') {
            selected_condition['product_category_id'] = type;
        } else {
            delete selected_condition['product_category_id'];
        }
        this.setState(
            {
                selected_type: type,
            },
            () => {
                this.getDataList(selected_condition);
            }
        );
    }
    toSearch = () => {
        this.props!.history!.push(ROUTE_PATH.shoppingSearch + '/' + this.props.type);
    }
    render() {
        const { products_list, is_loading, selected_sort, selected_type, service_type_list, main_title } = this.state;
        let title: string = main_title || (this.props.type && this.props.type === '服务' ? '服务' : '商品列表');
        setMainFormTitle(title);
        return (
            <Spin spinning={is_loading}>
                <Row>
                    <NavBar
                        icon={<Icon type={"left"} onClick={() => history.back()} />}
                        rightContent={<Icon type={"search"} onClick={() => this.toSearch()} />}
                    >
                        {title}
                    </NavBar>
                    <Row className="shopping-center-layout shopping-center-products">
                        {this.props.type && this.props.type === '服务' ?
                            <Row className="products-type">
                                <Row type="flex" justify="start" align="middle" className="scroll-x">
                                    <span className={'全部' === selected_type ? 'active' : ''} key={0} onClick={() => this.setType('全部')}>{'全部'}</span>
                                    {service_type_list.length ? service_type_list.map((item: any, index: number) => {
                                        return (
                                            <span className={item.id === selected_type ? 'active' : ''} key={index} onClick={() => this.setType(item.id)}>{item.name}</span>
                                        );
                                    }) : null}
                                </Row>
                            </Row> : null}
                        <Row type="flex" justify="space-around" align="middle" className="products-search-condition">
                            {this.condition_list.map((item: any, index: number) => {
                                let className: any = [];
                                if (item === '价格') {
                                    className.push('toggle');
                                    if (item === selected_sort) {
                                        className.push(this.state.selected_price_direction);
                                    }
                                }
                                if (item === selected_sort) {
                                    className.push('active');
                                }
                                return (
                                    <span className={className.join(' ')} key={index} onClick={() => this.setSort(item)}>{item}</span>
                                );
                            })}
                        </Row>
                        <WingBlank>
                            {this.renderProducts(products_list)}
                        </WingBlank>
                    </Row>
                </Row>
            </Spin>
        );
    }
}

/**
 * 组件：商城商品控件
 * 控制商城商品控件
 */
@addon('ProductsView', '商城商品控件', '控制商城商品控件')
@reactControl(ProductsView, true)
export class ProductsViewControl extends ReactViewControl {
    // constructor() {
    //     super();
    // }
    // 类型
    type?: any;
}