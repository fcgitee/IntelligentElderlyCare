import { Col, Icon, Row } from "antd";
import { Grid, ListView, WhiteSpace } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { setMainFormTitle } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from "src/projects/remote";

function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：发帖视图状态
 */
export interface PublishViewState extends ReactViewState {
    // 数据
    data?: any;
    /** 长列表数据 */
    dataSource?: any;
    /** 长列表容器高度 */
    height?: any;
}

/**
 * 组件：发帖视图
 * 发帖视图
 */
export class PublishView extends ReactView<PublishViewControl, PublishViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            data: [],
            height: document.documentElement!.clientHeight,
        };
    }
    componentDidMount() {
        AppServiceUtility.friends_circle.get_friends_circle_list!({ is_own: true })!
            .then((data: any) => {
                if (data) {
                    this.setState({
                        data: data.result
                    });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        let { data, dataSource, } = this.state;
        setMainFormTitle('发帖');
        // 获取服务器地址
        let fileUrl = remote.upload_url.split('/')[0];
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            let pct_list: any[] = [];
            if (owData.picture.length > 0) {
                owData.picture.map((value: any) => {
                    pct_list.push({ url: fileUrl + value });
                });
            }
            return (
                <div>
                    <div style={{ display: 'flex', padding: '15px 15px', borderBottom: '1px solid #ddd' }}>
                        <div style={{ height: '64px', }}>
                            <img style={{ height: '64px', width: '64px', borderRadius: '50%', marginRight: "15px" }} src={owData.user_picture ? owData.user_picture[0] : ''} alt="" />
                        </div>

                        <div style={{ width: '100%' }}>
                            <div style={{ marginBottom: '8px', fontWeight: 'bold' }}>{owData.name}</div>
                            <div>{owData.content}</div>
                            <div>
                                <Grid
                                    data={pct_list}
                                    columnNum={3}
                                    activeStyle={false}
                                    renderItem={dataItem => (
                                        <div>
                                            <img src={dataItem!.url} style={{ width: '75px', height: '75px' }} alt="" />
                                        </div>
                                    )}
                                />
                            </div>
                            <WhiteSpace size="sm" />
                            <Row>
                                <Col span={12}>{owData.create_date.split(' ')[0]}</Col>
                                <Col span={12} style={{ textAlign: 'right' }}>
                                    <span style={{ marginRight: '10px' }}>
                                        <Icon type="like" style={{ marginRight: '5px' }} />{owData.like_list.length}
                                    </span>
                                    <span style={{ marginRight: '10px' }}>
                                        <Icon type="chrome" style={{ marginRight: '5px' }} />{owData.comment.length}
                                    </span>
                                </Col>
                            </Row>
                        </div>
                    </div>

                </div>
            );
        };
        return (
            <div style={{ paddingBottom: '50px' }}>

                <div>
                    {
                        data && data.length ?
                            <ListView
                                ref={el => this['lv'] = el}
                                dataSource={dataSource.cloneWithRows(data)}
                                renderRow={renderRow}
                                initialListSize={10}
                                pageSize={10}
                                renderBodyComponent={() => <MyBody />}
                                style={{ height: this.state.height }}
                            />
                            :
                            <Row className='tabs-content' type='flex' justify='center'>
                                暂无数据
                            </Row>

                    }

                </div>
            </div>
        );
    }
}

/**
 * 控件：发帖视图控制器
 * 发帖视图
 */
@addon('PublishView', '发帖视图', '发帖视图')
@reactControl(PublishView, true)
export class PublishViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}