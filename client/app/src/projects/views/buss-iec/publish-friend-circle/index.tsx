import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { WingBlank, List, WhiteSpace, TextareaItem, Toast } from "antd-mobile";
import { ROUTE_PATH } from "src/projects/app/util-tool";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { AppServiceUtility } from "src/projects/app/appService";
import { setMainFormTitle } from "src/business/util_tool";
import { Button } from "antd";
import { beforeUpload } from "src/projects/app/util-tool";
/**
 * 组件：朋友圈发布状态
 */
export interface PublishFriendCircleViewState extends BaseReactElementState {

    // 捐款留言
    content?: string;
    // 图片
    photo?: any;
}

/**
 * 组件：朋友圈发布
 * 朋友圈发布
 */
export class PublishFriendCircleView extends BaseReactElement<PublishFriendCircleViewControl, PublishFriendCircleViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            content: '',
            photo: [],
        };
    }
    componentDidMount() {
        let user = IntelligentElderlyCareAppStorage.getCurrentUser();
        if (!user) {
            this.props.history!.push(ROUTE_PATH.login);
            return;
        }
    }
    changeValue = (e: any, inputName: any) => {
        this.setState({
            [inputName]: e
        });
    }
    endUpImg = (e: any) => {
        this.setState({
            photo: e
        });
    }
    CharitableTitleFund = () => {
        let { content } = this.state;
        if (!content) {
            Toast.fail('请输入内容', 1);
            return;
        }
        AppServiceUtility.friends_circle.update_friends_circle!(this.state.content, this.state.photo)!
            .then((data: any) => {
                if (data === 'Success') {
                    Toast.info('提交成功');
                    this.props.history!.goBack();
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        setMainFormTitle('发表帖子');
        return (
            <div>
                <List renderHeader={() => '内容'}>
                    <TextareaItem placeholder="请输入内容" rows={3} autoHeight={true} onChange={(e) => this.changeValue(e, 'content')} />
                </List>
                <List renderHeader={() => '图片（大小小于2M，格式支持jpg/jpeg/png）'}>
                    <FileUploadBtn list_type={"picture-card"} value={this.state.photo} contents={"plus"} action={remote.upload_url} onChange={this.endUpImg} beforeUpload={beforeUpload} />
                </List>
                <WhiteSpace size="lg" />
                <WingBlank size="lg">
                    <Button style={{ width: '100%', height: '45px' }} type="primary" onClick={this.CharitableTitleFund}>提交</Button>
                </WingBlank>
            </div>
        );
    }
}

/**
 * 控件：朋友圈发布控制器
 * 朋友圈发布
 */
@addon('PublishFriendCircleView', '朋友圈发布', '朋友圈发布')
@reactControl(PublishFriendCircleView, true)
export class PublishFriendCircleViewControl extends BaseReactElementControl {

}