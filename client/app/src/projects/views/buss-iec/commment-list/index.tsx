import { reactControl, ReactViewState, ReactView, ReactViewControl } from "pao-aop-client";

import React from "react";

import { addon } from "pao-aop";
import { ListView } from "antd-mobile";
import { Row, } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：我的评论视图状态
 */
export interface CommmentListViewState extends ReactViewState {
    // 数据
    data?: any;
    /** 长列表数据 */
    dataSource?: any;
    /** 长列表容器高度 */
    height?: any;
    notice?: string;
}

/**
 * 组件：我的评论视图
 * 我的评论视图
 */
export class CommmentListView extends ReactView<CommmentListViewControl, CommmentListViewState> {
    constructor(props: any) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            data: [],
            height: document.documentElement!.clientHeight,
            notice: '正在加载',
        };
    }
    componentDidMount() {
        AppServiceUtility.friends_circle.get_commont_list!({ 'is_own': true })!
            .then((data: any) => {
                if (data.result) {
                    if (data.result.length === 0) {
                        this.setState({
                            notice: '暂无数据',
                        });
                    }
                    this.setState(
                        {
                            data: data.result,
                        }
                    );
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {

        let { data, dataSource, } = this.state;
        console.info(data);
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <div style={{ borderBottom: '10px solid #ddd' }}>
                    <div style={{ display: 'flex', padding: '15px 15px', }}>
                        <div style={{ height: '64px', }}>
                            <img style={{ height: '64px', width: '64px', borderRadius: '50%', marginRight: "15px" }} src={owData.user_picture ? owData.user_picture[0] : ''} alt="" />
                        </div>
                        <div>
                            <div style={{ marginBottom: '8px', fontWeight: 'bold' }} >{owData.name}</div>
                            <div>{owData.create_date.split(' ')[0]}</div>
                            <div>{owData.content}</div>
                        </div>
                    </div>
                    <div style={{ margin: '10px', background: '#F2F2F2', display: 'flex', padding: '15px 15px', borderBottom: '1px solid #ddd' }}>
                        <div style={{ height: '64px', }}>
                            <img style={{ height: '64px', width: '64px', borderRadius: '50%', marginRight: "15px" }} src={owData.circle_user_picture ? owData.circle_user_picture[0] : ''} alt="" />
                        </div>

                        <div>
                            <div style={{ marginBottom: '8px', fontWeight: 'bold' }}>{owData.circle_user_name}</div>
                            <div>{owData.release_time}</div>
                            <div
                                style={{
                                    wordBreak: 'break-all',
                                    textOverflow: 'ellipsis',
                                    overflow: 'hidden',
                                    display: ' -webkit-box',
                                    WebkitLineClamp: 1,
                                    WebkitBoxOrient: 'vertical',
                                }}
                            >
                                {owData.circle_content}
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
        return (
            <div>
                {
                    data && data.length ?
                        <ListView
                            ref={el => this['lv'] = el}
                            dataSource={dataSource.cloneWithRows(data)}
                            renderRow={renderRow}
                            initialListSize={10}
                            pageSize={10}
                            renderBodyComponent={() => <MyBody />}
                            style={{ height: this.state.height }}
                        />
                        :
                        <Row className='tabs-content' type='flex' justify='center'>
                            {this.state.notice}
                        </Row>

                }
            </div>
        );
    }
}

/**
 * 控件：我的评论视图控制器
 * 我的评论视图
 */
@addon('CommmentListView', '我的评论视图', '我的评论视图')
@reactControl(CommmentListView, true)
export class CommmentListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}