import { Col, Icon, Input, Rate, Row, Spin } from "antd";
import { Card, ListView, Tabs, WhiteSpace, WingBlank, Toast } from "antd-mobile";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH, getWxConfig } from "src/projects/app/util-tool";
import { callWxMiniApp, getWebViewRetObjectType, setMainFormTitle, subscribeWebViewNotify } from "src/business/util_tool";
// import VConsole from 'vconsole';
import './index.less';

const { Search } = Input;
const tabs = [
    { title: '距离' },
    { title: '入住率' },
    { title: '关注度' },
];
function MyBody(props: any) {
    return (
        <div className="am-list-body my-body" style={{ height: '100%' }}>
            {props.children}
        </div>
    );
}
/**
 * 组件：养老院列表视图控件状态
 */
export interface BeadhouseListViewState extends ReactViewState {
    err?: any;
    /** 长列表数据 */
    dataSource?: any;
    /** 请求的数据 */
    list?: any;
    /** 上拉加载 */
    upLoading?: boolean;
    /** 下拉刷新 */
    pullLoading?: boolean;
    /** 长列表容器高度 */
    height?: any;
    location?: any;
    animating?: any;
    notice?: string;
    searchValue?: string;
    index?: any; // 当前tab位置
    page?: any;
}
/**
 * 组件：养老院列表视图控件
 */
export class BeadhouseListView extends ReactView<BeadhouseListViewControl, BeadhouseListViewState> {
    first_in: boolean;

    constructor(props: any) {
        super(props);
        this.first_in = true;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1: any, r2: any) => r1 !== r2
        });
        this.state = {
            dataSource: ds,
            list: [],
            upLoading: false,
            pullLoading: false,
            height: document.documentElement!.clientHeight,
            location: undefined,
            animating: false,
            notice: '正在加载...',
            index: 0,
            page: 1,
        };
    }
    get_organization_fly_list = (condition: any = {}, isFirst: boolean = false) => {
        const tab_name = ['距离', '入住率', '关注度'];
        const sort = tab_name[this.state.index];
        this.setState({ animating: true });
        AppServiceUtility.person_org_manage_service.get_organization_fly_list!({ personnel_category: '福利院', ...condition, sort: sort }, this.state.page, 10)!
            .then((data: any) => {
                // let list = this.state.list;
                isFirst && setMainFormTitle('养老机构 (' + data.total + ')');
                console.log('这里看看》》', data.result.length);
                data.result.length === 0 ? this.setState({
                    notice: '暂无数据',
                }) : this.setState({
                    list: data.result,
                    animating: false,
                });
            }).catch(e => {
                console.error(e);
            });
    }

    componentDidMount() {
        // 监听app端通知
        let location = {};
        if ((window as any).ReactNativeWebView) {
            const messageCb = (e: any) => {
                const retObject = JSON.parse(e.data);
                if (getWebViewRetObjectType(retObject) === 'getPhonePosition') {
                    location = retObject.data || {};
                    if (location === {}) {
                        Toast.fail('获取位置信息失败');
                    }
                    this.setState({
                        location
                    });
                    this.get_organization_fly_list({ sortByDistance: true, ...location }, true);
                }
            };
            if ((window as any).ReactNativeWebView) {
                subscribeWebViewNotify(messageCb);
            }

            this.getPhonePosition();
        } else {
            this.get_organization_fly_list({}, true);
        }
        // 初始化查询机构列表

        callWxMiniApp((wx: any) => {
            const that = this;
            setTimeout(
                () => {
                    getWxConfig()
                        .then(config => {
                            wx.config(config);
                            wx.ready(function () {
                                // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
                                wx.getLocation({
                                    success: (res: any) => {
                                        const latitude = res.latitude;
                                        const longitude = res.longitude;
                                        that.setState({
                                            location: { lat: latitude, lon: longitude }
                                        });
                                        that.get_organization_fly_list({ lat: latitude, lon: longitude, sortByDistance: true }, true);
                                    },
                                    fail: (e: any) => {
                                        // that.setState({ err: JSON.stringify(e) });
                                        console.error(e);
                                    }
                                });
                            });
                            wx.error(function (res: any) {
                                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
                                console.error(res);
                            });
                        });

                },
                0
            );
        });
    }

    getPhonePosition() {
        // Toast.info('认证成功：' + e.data);
        if ((window as any).ReactNativeWebView) {
            (window as any).ReactNativeWebView.postMessage(
                JSON.stringify({
                    function: 'getPhonePosition',
                    params: undefined
                })
            );
        }
    }

    onstop = (id: any) => {
        this.props.history!.push(ROUTE_PATH.beadhouseInfo + '/' + id);
    }

    handletab(e: any, index: number) {
        this.setState({
            list: [],
            page: 1
        });
        let condition: Object = {};
        this.setState({ index }, () => {
            if (this.state.searchValue) {
                condition['name'] = this.state.searchValue;
            }
            this.get_organization_fly_list(condition);
        });
    }
    /** 下拉事件 */
    onEndReached = () => {
        let page = this.state.page ? this.state.page + 1 : 1;
        this.setState({ page }, () => {
            this.get_organization_fly_list();
        });
    }
    search = (value: any) => {
        this.setState(
            {
                searchValue: value,
            },
            () => {
                this.get_organization_fly_list({ name: value });
            }
        );
    }
    render() {
        const { list, dataSource } = this.state;
        // 获取item进行展示
        const renderRow = (owData: any, sectionID: any, rowID: any) => {
            return (
                <>
                    <WhiteSpace />
                    <Card style={{ borderRadius: 20, boxShadow: '0px 0px 6px 0px rgba(170,170,170,0.5)', marginLeft: 3, marginRight: 3 }} className='list-content' onClick={() => this.onstop(owData.id)}>
                        <WingBlank>
                            <WhiteSpace />
                            <Row type='flex' justify='center'>
                                <Col span={24}>
                                    <Row type={"flex"} justify={"space-between"}>
                                        <strong >{owData.name} <Icon style={{ color: 'rgba(153,153,153,1)' }} type="right-circle" /></strong>
                                        <span>
                                            <Icon type="heart" theme={"filled"} style={{ marginRight: '5px', color: '#ff1466' }} />{owData.follow_count}
                                        </span>
                                    </Row>
                                </Col>
                                <Col className='list-col' span={10}>
                                    <img src={owData.organization_info.picture_list && owData.organization_info.picture_list.length > 0 ? owData.organization_info.picture_list[0] : ''} style={{ height: '60pt', width: '88pt' }} />
                                </Col>
                                <Col span={14} className='list-col'>
                                    <Row style={{ fontSize: '14px', color: 'rgba(51,51,51,1)' }}>
                                        <Rate character={<Icon style={{ fontSize: "20px" }} type="star" theme="filled" />} allowHalf={true} disabled={true} value={owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 1} />
                                        <span >&nbsp;&nbsp;{owData['organization_info'] && owData['organization_info']['star_level'] ? Number(owData['organization_info']['star_level']) : 1}星</span>
                                    </Row>
                                    <Row style={{ fontSize: '14px' }}>
                                        {owData.organization_info.organization_nature}
                                        <span style={{ marginLeft: '20px' }}>{owData.organization_info.chenk_in_probability}% 入住率</span>
                                    </Row>
                                    <Row>
                                        {owData.organization_info.telephone}
                                    </Row>
                                    <Row style={{ fontSize: '14px' }}>
                                        <span
                                            style={{
                                                wordBreak: 'break-all',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: ' -webkit-box',
                                                WebkitLineClamp: 1,
                                                WebkitBoxOrient: 'vertical',
                                            }}
                                        >
                                            {owData.organization_info.address}
                                        </span>
                                    </Row>
                                    <Row style={{ fontSize: '14px', color: "rgba(153,153,153,1)" }}>
                                        <Col span={24}>{owData.town} {owData.distance ? '| ' + owData.distance.toFixed(2) + 'km' : ''}</Col>
                                    </Row>
                                </Col>
                            </Row>
                            <WhiteSpace />
                        </WingBlank>
                    </Card>
                </>
            );
        };
        return (
            <Row className={"beadhouseListTab"}>
                <div>
                    {this.state.err}
                </div>
                <div style={{ height: '50px', backgroundColor: 'white', borderRadius: '15px 15px 0 0' }}>
                    <WingBlank>
                        <Search
                            placeholder="搜索机构"
                            onSearch={this.search}
                            style={{ width: '100%', height: '40px', marginTop: '3px', borderRadius: 50 }}
                        />
                    </WingBlank>
                </div>
                <Tabs
                    tabs={tabs}
                    initialPage={0}
                    onChange={this.handletab = this.handletab.bind(this)}
                >
                    <WingBlank>
                        <div className='tabs-content'>
                            {
                                list && list.length ?
                                    <ListView
                                        ref={el => this['lv'] = el}
                                        dataSource={dataSource.cloneWithRows(list)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        onEndReached={() => this.onEndReached()}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: this.state.height }}
                                    />
                                    :
                                    <div style={{ textAlign: 'center', margin: '20px' }}>
                                        <Spin size="large" />
                                    </div>
                            }
                            {this.state.list.length === 0 && this.state.animating === true ?
                                <Row className='tabs-content' type='flex' justify='center'>
                                    {this.state.notice}
                                </Row>
                                :
                                ''
                            }
                        </div>
                    </WingBlank>
                    <WingBlank>
                        <div className='tabs-content'>
                            {
                                list && list.length ?
                                    <ListView
                                        ref={el => this['lv'] = el}
                                        dataSource={dataSource.cloneWithRows(list)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        onEndReached={() => this.onEndReached()}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: this.state.height }}
                                    />
                                    :
                                    <div style={{ textAlign: 'center', margin: '20px' }}>
                                        <Spin size="large" />
                                    </div>
                            }
                            {this.state.list.length === 0 && this.state.animating === true ?
                                <Row className='tabs-content' type='flex' justify='center'>
                                    {this.state.notice}
                                </Row>
                                :
                                ''
                            }
                        </div>
                    </WingBlank>
                    <WingBlank>
                        <div className='tabs-content'>
                            {
                                list && list.length ?
                                    <ListView
                                        ref={el => this['lv'] = el}
                                        dataSource={dataSource.cloneWithRows(list)}
                                        renderRow={renderRow}
                                        initialListSize={10}
                                        onEndReached={() => this.onEndReached()}
                                        pageSize={10}
                                        renderBodyComponent={() => <MyBody />}
                                        style={{ height: this.state.height }}
                                    />
                                    :
                                    <div style={{ textAlign: 'center', margin: '20px' }}>
                                        <Spin size="large" />
                                    </div>
                            }
                            {this.state.list.length === 0 && this.state.animating === true ?
                                <Row className='tabs-content' type='flex' justify='center'>
                                    {this.state.notice}
                                </Row>
                                :
                                ''
                            }
                        </div>
                    </WingBlank>
                </Tabs>
            </Row>
        );
    }
}

/**
 * 组件：养老院列表视图控件
 * 控制养老院列表视图控件
 */
@addon('BeadhouseListView', '养老院列表视图控件', '控制养老院列表视图控件')
@reactControl(BeadhouseListView, true)
export class BeadhouseListViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}