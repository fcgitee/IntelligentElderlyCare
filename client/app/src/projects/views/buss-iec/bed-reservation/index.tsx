import { reactControl, ReactViewState, ReactView, ReactViewControl } from "pao-aop-client";

import React from "react";

import { addon } from "pao-aop";
import { List, InputItem, TextareaItem } from "antd-mobile";
import { Radio } from "antd";
const Item = List.Item;
/**
 * 组件：床位预约视图状态
 */
export interface BedReservationViewState extends ReactViewState {
    // 数据
    data?: any;
}

/**
 * 组件：床位预约视图
 * 床位预约视图
 */
export class BedReservationView extends ReactView<BedReservationViewControl, BedReservationViewState> {
    constructor(props: any) {
        super(props);
    }
    componentDidMount() {
    }
    render() {
        return (
            <div>
                <List>
                    <InputItem>
                        姓名
                    </InputItem>
                    <Item>
                        性别
                        <Radio.Group style={{ margin: '0 20px' }}>
                            <Radio value={'男'} defaultChecked={true}>男</Radio>
                            <Radio value={'女'}>女</Radio>
                        </Radio.Group>
                    </Item>

                    <InputItem>
                        户籍地址
                    </InputItem>
                    <InputItem>
                        所属社区
                    </InputItem>
                    <InputItem>联系电话</InputItem>
                </List>
                <List renderHeader={() => '预约说明'}>
                    <TextareaItem placeholder="请输入内容" rows={3} autoHeight={true} />
                </List>
            </div>
        );
    }
}

/**
 * 控件：床位预约视图控制器
 * 床位预约视图
 */
@addon('BedReservationView', '床位预约视图', '床位预约视图')
@reactControl(BedReservationView, true)
export class BedReservationViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}