import { Icon } from "antd";

export const FontIcon = Icon.createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1482776_50xwnkddrr.js', // 在 iconfont.cn 上生成
});