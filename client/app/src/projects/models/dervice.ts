import { NullablePromise, addon } from "pao-aop";

/**
 *  设备
 */
export interface Device {

}
/**  设备 */
@addon('IDeviceService', '设备', '设备')
export class IDeviceService {
    /** 绑定设备 */
    update_device?(condition?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取设备列表 */
    get_device?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 解除绑定设备 */
    delete_device?(ids: string[]): NullablePromise<boolean> {
        return undefined;
    }
    // 获取这个人绑定的设备
    get_device_self?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    // 获取设备定位
    get_device_location?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    // 获取设备足迹
    get_device_footprint?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    // 绑定设备
    bind_device_self?(condition?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 更新设备信息 */
    update_device_weilan?(condition?: any): NullablePromise<any> {
        return undefined;
    }
    /** 解绑设备 */
    unbind_device?(condition?: any): NullablePromise<any> {
        return undefined;
    }
    // 获取设备信息
    get_device_all?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
}