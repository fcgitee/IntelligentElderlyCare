import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 入住登记管理
 */
export interface checkIn {
    /** id */
    id: string;
    /** 长者用户id */
    user_id: string;
    /** 入住时间 */
    /** 入住人 */
    name: string;

}

@addon('CheckInService', '入住登记服务', '入住登记服务')
export class CheckInService {

    /** 
     * 入住登记管理 
     */

    /** 列表 */
    get_check_in_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }

    /** 删除 */
    delete_check_in?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 新增入住 */
    add_check_in?(elder?: {}, evaluation_info?: {}, service_item?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 新增入住 */
    update_check_in?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}