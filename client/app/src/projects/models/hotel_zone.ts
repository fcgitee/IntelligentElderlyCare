import { NullablePromise, DataList, addon } from "pao-aop";

export interface HotelZoneType {
    /** id */
    id: string;
    /** 名称 */
    name: string;
    /** 编号 */
    number: string;
    /** 备注 */
    remark: string;
}

export interface HotelZone {
    /** id */
    id: string;
    /** 区域名称 */
    zone_name: string;
    /** 区域编号 */
    zone_number: string;
    /** 组织机构id */
    organization_id: string;
    /** 床位id */
    bed_id: string;
    /** 区域类别id */
    hotel_zone_type_id: string;
    /** 上级区域id */
    upper_hotel_zone_id: string;
    /** 备注 */
    remark: string;
}
export interface Bed {
    /** id */
    id: string;
    /** 区域 */
    area_id: string;
    /** 区域 */
    area_name: string;
    /** 床位名称 */
    name: string;
    /** 床位编号 */
    bed_code: string;
    /** 入住人id */
    residents_id: string;
    /** 入住人 */
    residents: string;
    /** 服务包 */
    service_item_package_id: string;
    /** 服务包 */
    service_item_package: string;
    /** 备注 */
    remark: string;
}

@addon('IHotelZoneTypeService', '住宿区域服务接口', '住宿区域服务接口')
export class IHotelZoneTypeService {
    /**
     * 获取住宿区域类型列表
     */
    get_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 删除住宿区域类型
     */
    delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改住宿区域类型
     */
    update?(serviceItem: HotelZoneType): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 查询床位列表
     */
    get_bed_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Bed> | undefined> {
        return undefined;
    }

    /**
     * 查询床位列表通过id
     */
    get_bed_list_byId?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Bed> | undefined> {
        return undefined;
    }

    /**
     * 获取床位位置
     */
    get_bed_position?(condition?: {}): NullablePromise<DataList<Bed> | undefined> {
        return undefined;
    }

    /**
     * 根据床位获取相应的长者信息
     * TODO: 人员没有接口对应，所以先返回any
     */
    get_residents_of_bed?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 查询未入住的床位
     */
    get_bed_list_no_user?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Bed> | undefined> {
        return undefined;
    }

    /** 
     * 新增/修改床位
     */
    update_bed?(bed: Bed): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 删除床位
     */
    delete_bed?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}
@addon('IHotelZoneService', '住宿区域服务接口', '住宿区域服务接口')
export class IHotelZoneService {
    /**
     * 获取住宿区域列表
     */
    get_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 获取房态图列表
     */
    get_room_status_list?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 更新房态图
     */
    update_room_status?(resident_source_id?: string, bed_source_id?: string, bed_dest_id?: string): NullablePromise<string | undefined> {
        return undefined;
    }

    /**
     * 删除住宿区域
     */
    delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改住宿区域
     */
    update?(serviceItem: HotelZone): NullablePromise<boolean | undefined> {
        return undefined;
    }
}