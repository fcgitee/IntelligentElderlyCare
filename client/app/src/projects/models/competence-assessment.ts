import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  能力评估
 */
export interface CompetenceAssessment {
    /** id */
    id: string;
    /** 长者id */
    elder?: string;
    /** 模板 */
    template?: string;
    /** 项目列表 */
    project_list?: any[];
    /** 总分 */
    total_score?: any[];

}
/**  能力评估服务 */
@addon('ICompetenceAssessmentService', '能力评估服务', '能力评估服务')
export class ICompetenceAssessmentService {
    /**
     * 获取 能力评估列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_competence_assessment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /** 修改/新增能力评估 */
    update_competence_assessment?(competence_assessment: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除能力评估 */
    delete_competence_assessment?(competence_assessment_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}