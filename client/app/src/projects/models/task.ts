import { NullablePromise, DataList, addon } from "pao-aop";
export interface task {

}
@addon('ITaskService', '任务管理服务', '任务管理服务')
export class ITaskService {
    /**
     * 获取所有分派任务列表
     */
    get_task_service_order_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取所有任务列表
     */
    get_all_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取等待处理的任务
     */
    get_to_be_processed_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取等待分派的任务
     */
    get_to_be_send_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取进行中的任务
     */
    get_ongoing_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取待评价的任务
     */
    get_completed_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /** 新增需要审核的任务 */
    add_new_process_task?(nursig: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 获取人员下拉列表 */
    input_person?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务类型 */
    input_task_type?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务紧急类型 */
    input_task_urgent?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 拒接任务 */
    task_receive_failed?(nursing: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 接收任务 */
    task_receive_passed?(nursing: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 完成任务 */
    task_complete?(nursing: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 服务订单分派的接口
    change_task_service_order?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 服务订单分派更新时间接口
    save_time_service_order?(data: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 服务记录撤回开始接口
    back_start_service_order_record?(record_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
}