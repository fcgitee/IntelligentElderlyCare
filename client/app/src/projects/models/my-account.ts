import { NullablePromise, DataList, addon } from "pao-aop";
export interface IMyAccount {

}
@addon('IMyAccountService', '我的账户服务', '我的账户服务')
export class IMyAccountService {
    /**
     * 获取我的所有账户信息
     */
    get_my_account?(): NullablePromise<DataList<IMyAccount> | undefined> {
        return undefined;
    }

    /**
     * 获取账户详情
     */
    get_account_detail?(account_data?: {}): NullablePromise<DataList<IMyAccount> | undefined> {
        return undefined;
    }

    /**
     * 账户充值
     */
    account_recharge?(recharge_data?: {}): NullablePromise<DataList<IMyAccount> | undefined> {
        return undefined;
    }
}