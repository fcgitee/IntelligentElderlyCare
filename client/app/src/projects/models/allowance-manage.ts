/*
 * @Description: In User Settings Edit
 * @Author: 杨子毅
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-08-13 17:14:00
 * @LastEditors: Please set LastEditors
 */
import { addon, DataList, NullablePromise } from "pao-aop";

export interface AllowanceInsert {
    /** 姓名 */
    name: string;
    /** 性别 */
    sex: string;
    /** 出生日期 */
    date_birth: string;
    /** 证件类型 */
    id_card_type: string;
    /** 证件号码 */
    id_card_number: string;
    /** 电话 */
    telephone: string;
    /** 民族 */
    nation: string;
    /** 籍贯 */
    native_place: string;
    /** 家庭住址 */
    address: string;
    /** 家属姓名 */
    contacts_name: string;
    /** 家属电话 */
    contacts_telephone: string;
    /** 家属关系 */
    contacts_type: string;
    /** 申请理由 */
    apply_reasons: string;
    /** 申请材料 */
    apply_material: any;
    /** 申请状态 */
    status?: string;
    /** 通过/不通过理由 */
    check_reason?: string;
    /** id */
    id?: string;
    /** 补贴金额 */
    money?: string;

}

export interface AllowanceDefine {
    /** 定义名称 */
    name: string;
    /** 评估模板id */
    template_id: string;
    /** 创建时间 */
    create_time: Date;
    /** 备注 */
    remark: string;
}

export interface AllowanceProject {
}
@addon('IAllowanceService', '补贴管理', '补贴管理')
export class IAllowanceService {
    /**
     * 查询申请列表
     */
    get_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询已申请列表
     */
    get_allowance_applied_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 新增申请
     */
    update_allowance?(allowance: AllowanceInsert): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除申请
     */
    del_allowance?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取评估模板
     */
    get_assessment_template?(): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 更新补贴定义
     * @param define 
     */
    add_allowance_define?(allowance: AllowanceDefine): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 查询补贴项目列表
     */
    get_allowance_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceProject> | undefined> {
        return undefined;
    }
    /**
     * 查询补贴项目列表
     */
    get_allowance_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceProject> | undefined> {
        return undefined;
    }
    /**
     * 查询补贴定义列表
     */
    get_allowance_define_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询当前用户的申请
     */
    get_currentUser_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询审核意见
     */
    get_reviewed_idea?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 申请高龄津贴
     */
    apply_old_age_allowance?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 高龄补贴状态查看
     */
    get_old_age_allowance_status?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }

    /**
     * 从大白获取凭证
     */
    get_certToken?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }

    /**
     * 从大白获取验证结果
     */
    get_rz_result?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }

    /**
     * 高龄津贴申请
     */
    apply_old_allowance?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }

    /**
     * 查询高龄津贴未认证列表
     */
    get_uncertified_person_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }

    /** 获取认证列表 */
    get_old_age_list?(condition?: {}, page?: number, count?: number): NullablePromise<any | undefined> {
        return undefined;
    }
    /** 获取残疾人认证列表 */
    get_disabled_person_list?(condition?: {}, page?: number, count?: number): NullablePromise<any | undefined> {
        return undefined;
    }
    /**
     * 残疾人认证
     */
    apply_disabled_person_allowance?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 高龄紧贴手动认证
     */
    manual_authentication?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 检测是否存在名单里
    check_elder_exists?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}