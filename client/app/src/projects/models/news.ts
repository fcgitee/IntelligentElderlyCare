
import { NullablePromise, DataList, addon } from "pao-aop";

export interface News {
    /** id */
    id?: string;
    /** 文章标题 */
    title?: string;
    /** 文章副标题 */
    subhead?: string;
    /** 文类型 */
    type_id?: string;
    /** 文章描述 */
    description?: string;
    /** 文章内容 */
    content?: string;
    /** 文章web图片列表 */
    web_img_list?: any[];
    /** 文章app图片列表 */
    app_img_list?: any[];
    /** 文章附件清单 */
    accessory_list?: any[];
    /** 文章作者 */
    author?: string;
    /** 文章来源 */
    source?: string;
    /** 文章发布者 */
    promulgator_id?: string;
    /** 发布时间 */
    issue_date?: string;
    /** 文章审核者 */
    auditor_id?: string;
    /** 审核时间 */
    audit_date?: string;
    /** 文章状态 */
    status?: string;
    /** 文章显示状态 */
    showing_status?: string;
    /** 备注 */
    remark?: string;
}

@addon('INewsListService', '新闻列表服务', '新闻列表服务')
export class INewsListService {
    /**
     * 获取服务列表
     */
    get_news_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<News>> {
        return undefined;
    }

}

@addon('INewsDetailService', '新闻详情', '新闻详情')
export class INewsDetailService {
    /**
     * 获取服务详情
     */
    get_news_detail?(condition?: {}): NullablePromise<DataList<News>> {
        return undefined;
    }
}

@addon('IArticleService', '新闻列表', '新闻列表')
export class IArticleService {

    /**
     * 获取新闻图片列表
     */
    get_news_list_picture?(condition?: {}): NullablePromise<DataList<News> | undefined> {
        return undefined;
    }
    /**
     * 获取新闻列表
     */
    get_news_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<News> | undefined> {
        return undefined;
    }
    get_news_pure_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<News> | undefined> {
        return undefined;
    }
    /** 新增/修改新闻 */
    update_news?(new_data: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 点赞分享
    update_ctrl?(new_data: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 检测账户
    check_app_role_type?(condition?: {}): NullablePromise<DataList<News> | undefined> {
        return undefined;
    }
    /**
     * app首页查询新闻列表
     */
    get_app_home_page_news_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<News> | undefined> {
        return undefined;
    }
}