import { NullablePromise, DataList, addon, } from "pao-aop";
/** 登录服务接口 */
@addon('ILoginService', '活动服务', '活动服务')
export class ILoginService {
    /** 登录 */
    login?(login_data?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取角色列表 */
    get_role_list(): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 设置角色id和区域id服务 */
    set_role_org_session(role_id?: string, org_id?: string): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取功能列表 */
    get_function_list(): NullablePromise<any> {
        return undefined;
    }
    /** 注册 */
    register(data?: any): NullablePromise<any> {
        return undefined;
    }
    /** 修改个人信息 */
    modify_user_info?(new_info?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 修改密码 */
    retrieve_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 修改手机号码 */
    modify_mobile?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 重置密码的操作 */
    modify_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 修改初始密码的操作 */
    first_modify_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
}