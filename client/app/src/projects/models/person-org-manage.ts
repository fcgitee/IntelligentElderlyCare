/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-30 18:16:42
 * @LastEditTime: 2019-11-15 10:38:00
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, DataList, addon } from "pao-aop";

/** 人员组织结构服务 */
@addon('IPersonOrgManageService', '人员组织结构服务', '人员组织结构服务')
export class IPersonOrgManageService {
    /**
     * 获取当前登录用户信息
     */
    get_current_user_info?(): NullablePromise<any[]> {
        return undefined;
    }
    /**
     * 获取全部用户列表
     */
    get_user_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表 */
    get_organization_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_organization_all_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_organization_fly_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_organization_xfy_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_user_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_user_list_look?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取工作人员用户列表 */
    get_user_list_worker?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取长者列表 */
    get_personnel_elder?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取工作人员列表 */
    get_personnel_worker?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表 */
    get_organization_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取所有组织机构列表 */
    get_all_organization_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取人员 */
    get_personnel_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取当前用户机构预约记录 */
    get_current_user_reservation_registration?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取当前用户活动报名记录 */
    get_current_user_activity_participate?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取当前用户补贴申请记录 */
    get_current_user_allowance_manage?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取当前用户慈善申请记录 */
    get_current_user_charitable_donate?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取当前用户权限及受理记录
    get_current_user_acceptance?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取当前用户消息提醒
    get_current_user_message_notice?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 用于判断资讯和活动的权限
    check_permission?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_organ_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    is_alert_yinsizhengce?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    agree_yinsizhengce?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
}