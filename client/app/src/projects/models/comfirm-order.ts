import { addon, DataList, NullablePromise } from "pao-aop";

export interface order {

}

@addon('IComfirmOrderService', '确认订单服务', '确认订单服务')
export class IComfirmOrderService {
    /**
     * 获取购买者信息
     */
    get_purchaser_detail?(condition?: {}): NullablePromise<DataList<order>> {
        return undefined;
    }
    /** 创建订单 */
    create_product_order?(condition?: any, is_pay_first?: any): NullablePromise<DataList<order>> {
        return undefined;
    }

    /** 修改交易状态 */
    change_trade_status?(id?: string, status?: '已付款' | '未付款'): NullablePromise<DataList<order>> {
        return undefined;
    }
}
