import { NullablePromise, DataList, addon } from "pao-aop";

@addon('IShoppingMallManageService', '商城管理', '商城管理')
export class IShoppingMallManageService {
    /**
     * 查询商品详情
     */
    get_goods_detail_list_app?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 查询收货人地址详情
     */
    get_consignee_address_detail_app?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 保存收货人地址
     */
    save_consignee_address_app?(data: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 获取所有板块
    get_app_blocks_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取所有商品
    get_goods_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取居家服务商品
    get_services_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取服务类型
    get_service_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取服务产品
    get_service_product_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取机构下的服务和商品
    get_products_list_by_organization?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 查询商品订单
    get_product_orders_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 更新商品订单备注
    update_order_remark?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 更新商品订单状态（取消，删除）
    update_order_status?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取默认的收货地址
    get_default_consignee_data?(): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 创建订单
    create_order?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 更新商品订单信息
    update_order?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取推荐明细
    get_recommend_product_service_list?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
}