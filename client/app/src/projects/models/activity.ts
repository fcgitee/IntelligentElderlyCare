/*
 * @Description: In User Settings Edit
 * @Author: 杨子毅
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-09-26 16:30:46
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, DataList, addon } from "pao-aop";

export interface Activity {
    /** id */
    id?: string;
    /** 活动名称 */
    activity_name?: string;
    /** 图片 */
    photo?: string[];
    /** 商家 */
    organization?: string;
    /** 商家名称 */
    organization_name?: string;
    /** 商家地址 */
    organization_address?: string;
    /** 最大报名人数 */
    max_quantity?: number;
    /** 开始时间 */
    begin_date?: string;
    /** 结束时间 */
    end_date?: string;
    /** 地址 */
    address?: string;
    /** 价格 */
    amount?: string;
    /** 介绍 */
    introduce?: string;
    /** 是否启用 */
    is_enable?: boolean;
    /** 是否签到 */
    is_sign_in?: boolean;
    /** 报名人数 */
    acount?: number;
    /** 联系人 */
    contacts?: string;
    /** 电话 */
    phone?: string;
    // 报名状态
    per_status?: string;
}
export interface ActivitySignIn {

}
export interface ActivityParticipate {

}
/**
 * 活动室
 */
export interface ActivityRoom {
    /** id */
    id?: string;
    /** 活动室名称 */
    activity_room_name?: string;
    /** 最大参与人数 */
    max_quantity?: string;
    /** 开放时间 */
    open_date?: any;
    /** 所属机构 */
    organization_name?: string;
    /** 地址 */
    address?: string;
    /** 图片 */
    photo?: string[];
    /** 备注 */
    remarks?: string;
}
/**
 * 活动室预约
 */
export interface ActivityRoomReservation {
    /** 活动室名称 */
    activity_room_name?: string;
    /** 参与人 */
    user_name?: string;
    /** 预约时间 */
    reservate_date?: string;
}
@addon('IActivityService', '活动服务', '活动服务')
export class IActivityService {
    /**
     * 查询活动信息
     */
    get_activity_information_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    /**
     * 查询活动列表
     */
    get_activity_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    /**
     * 查询活动列表
     */
    get_activity_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    /**
     * 查询活动签到列表
     */
    get_activity_sign_in_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    /**
     * 查询活动签到列表
     */
    get_activity_sign_in_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    /**
     * 查询活动参与列表
     */
    get_activity_participate_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    /**
     * 查询活动类型列表
     */
    get_activity_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    /**
     * 新增活动
     */
    update_activity?(activity: Activity): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // /**
    //  * 删除活动
    //  */
    // del_activity?(ids: string[]): NullablePromise<boolean | undefined> {
    //     return undefined;
    // }
    /**
     * 参与活动
     */
    participate_activity?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 取消活动 */
    cancel_participate_activity?(activity_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 活动签到
     */
    sign_in_activity?(activity_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询活动室列表 */
    get_activity_room_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoom> | undefined> {
        return undefined;
    }
    /** 查询活动室列表 */
    get_activity_room_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoom> | undefined> {
        return undefined;
    }
    /** 查询活动室预约列表 */
    get_activity_room_reservation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoomReservation> | undefined> {
        return undefined;
    }
    /** 预约活动室 */
    reservate_activity_room?(activity_room_id: string, reservate_date?: Date): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 活动室预约
    update_activity_room_reservation?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
}