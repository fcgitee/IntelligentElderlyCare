import { NullablePromise, DataList, addon } from "pao-aop";

export interface order {

}

@addon('IServiceOrderService', '我的订单', '我的订单')
export class IServiceOrderService {
    /**
     * 获取服务列表
     */
    my_order_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<order>> {
        return undefined;
    }
    /**
     * 服务商获取订单
     * @param condition 条件
     * @param page 页数
     * @param count 条数
     */
    service_order?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 服务商上传图片
     * @param condition 图片url
     */
    upload_service_img?(condition?: {}): NullablePromise<any> {
        return undefined;
    }
    /**
     * 微信支付
     */
    wechat_payment?(condition?: {
        /** 网页标题 */
        page_title: string,
        /** 商品描述，描述+网页标题至多128个字符串 */
        product_desc: string,
        /** 商品唯一订单号，32位字符串 */
        out_trade_no: string,
        /** 金额 */
        total_fee: number
    }): NullablePromise<DataList<order>> {
        return undefined;
    }

    /**
     * 微信支付主动查询
     */
    third_trade_query?(condition?: {
        /** 唯一id */
        id?: string,
        /** 商户唯一订单号 */
        out_trade_no?: string,
    }): NullablePromise<DataList<order>> {
        return undefined;
    }

    /**
     * 支付宝支付
     */
    alipay?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<order>> {
        return undefined;
    }

    /** 获取用户id */
    get_user_id?(): NullablePromise<DataList<order>> {
        return undefined;
    }
}
