import { NullablePromise, DataList, addon } from "pao-aop";

export interface IAppPay {

}

@addon('IAppPayService', '支付服务', '支付服务')
export class IAppPayService {
    /**
     * 接收支付信息
     */
    receive_pay_info?(condition?: {}): NullablePromise<DataList<IAppPay>> {
        return undefined;
    }

    /**
     * 财务模块接口
     */
    confirm_pay?(pay_type?: '微信支付' | '支付宝支付', pay_info?: {}): NullablePromise<DataList<IAppPay>> {
        return undefined;
    }

    /**
     * 微信支付接口
     */
    wechat_pay?(
        condition?: {
            /** 网页标题 */
            page_title: string,
            /** 商品描述，描述+网页标题至多128个字符串 */
            product_desc: string,
            /** 商品唯一订单号，32位字符串 */
            out_trade_no: string,
            /** 金额 */
            total_fee: number
        }
    ): NullablePromise<DataList<IAppPay>> {
        return undefined;
    }

    /**
     * 支付宝支付
     */
    alipay?(
        condition?: {
            /** 网页标题 */
            page_title: string,
            /** 商品描述，描述+网页标题至多128个字符串 */
            product_desc: string,
            /** 商品唯一订单号，32位字符串 */
            out_trade_no: string,
            /** 金额 */
            total_fee: number
        }
    ): NullablePromise<DataList<IAppPay>> {
        return undefined;
    }
}
