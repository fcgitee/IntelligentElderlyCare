import { NullablePromise, DataList, addon } from "pao-aop";

export interface Announcement {
    /** id */
    id?: string;
    /** 文章标题 */
    title?: string;
    /** 文章副标题 */
    subhead?: string;
    /** 文类型 */
    type_id?: string;
    /** 文章描述 */
    description?: string;
    /** 文章内容 */
    content?: string;
    /** 文章web图片列表 */
    web_img_list?: any[];
    /** 文章app图片列表 */
    app_img_list?: any[];
    /** 文章附件清单 */
    accessory_list?: any[];
    /** 文章作者 */
    author?: string;
    /** 文章来源 */
    source?: string;
    /** 文章发布者 */
    promulgator_id?: string;
    /** 发布时间 */
    issue_date?: string;
    /** 文章审核者 */
    auditor_id?: string;
    /** 审核时间 */
    audit_date?: string;
    /** 文章状态 */
    status?: string;
    /** 文章显示状态 */
    showing_status?: string;
    /** 备注 */
    remark?: string;
}

@addon('IAnnouncementListService', '公告列表服务', '公告列表服务')
export class IAnnouncementListService {
    /**
     * 获取公告列表
     */
    get_announcement_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Announcement>> {
        return undefined;
    }
}

@addon('IAnnouncementDetailService', '公告详情', '公告详情')
export class IAnnouncementDetailService {
    /**
     * 获取公告详情
     */
    get_announcement_detail?(condition?: {}): NullablePromise<DataList<Announcement>> {
        return undefined;
    }
}