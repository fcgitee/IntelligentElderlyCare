import { NullablePromise, DataList, addon } from "pao-aop";

export interface Comment {
    /** id */
    id: string;
    /** 评论对象 */
    comment_object_id?: string;
    /** 父级评论 */
    father_comment_id?: string;
    /** 评论类型 */
    type?: string;
    /** 评论用户 */
    comment_user_id?: string;
    /** 评论内容 */
    content?: string;
    /** 评论等级 */
    level?: string;
    /** 评论时间 */
    comment_date?: string;
    /** 评论审核时间 */
    audit_date?: string;
    /** 评论审核者 */
    audit_user_id?: string;
    /** 评论状态 */
    audit_status?: string;
    /** 点赞清单 */
    like_list?: any[];
    /** 审核意见 */
    audit_idea?: string;
}

@addon('ICommentManageService', '评论表服务', '评论服务')
export class ICommentManageService {
    /**
     * 获取评论列表
     */
    get_comment_list_all?(condition?: {},page?: number, count?: number): NullablePromise<DataList<Comment>> {
        return undefined;
    }
    /**
     * 获取子评论列表
     */
    get_child_comment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment>> {
        return undefined;
    }
    /**
     * 获取子评论列表
     */
    get_my_comment?(): NullablePromise<DataList<Comment>> {
        return undefined;
    }
    /**
     * 获取评论详情
     */
    get_comment_detail?(condition?: {}): NullablePromise<DataList<Comment>> {
        return undefined;
    }
    /**
     * 新增评论
     */
    update_comment?(comment?: {}): NullablePromise<DataList<Comment>> {
        return undefined;
    }
    /**
     * 点赞
     */
    give_a_like?(condition?: {}): NullablePromise<DataList<Comment>> {
        return undefined;
    }
}