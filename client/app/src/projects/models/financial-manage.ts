/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Wednesday July 24th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Wednesday, 24th July 2019 11:47:10 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、财务管理接口
 */
import { NullablePromise, DataList, addon } from "pao-aop";
export interface Fiancialaccount {
    /** 开始时间 */
    start_date?: string;
    /** 结束时间 */
    end_date?: string;
    /** 账户类型 */
    account?: string;
    /** id */
    id?: string | number;
}
@addon('IFiancialService', '财务管理接口', '财务管理接口')
export class IFiancialService {

    /** 收付款记录服务 */
    billing_record?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }

    /** 收付款详情记录 */
    billing_details_record?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }
}