import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  护理模板
 */
export interface nursingTemplate {
    /** 名称 */
    name?: string;
    /** 创建时间 */
    create_date?: string;
    /** 护理描述 */
    describe?: string;

}
/**  护理模板服务 */
@addon('INursingService', '护理模板服务', '护理模板服务')
export class nursingTemplateService {
    /**
     * 获取 护理模板列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_nursing_template_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<nursingTemplate> | undefined> {
        return undefined;
    }
    /** 修改/新增护理模板列表 */
    update_nursing_template?(assessment_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除护理模板列表 */
    delete_nursing_template?(assessment_project_ids: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

}