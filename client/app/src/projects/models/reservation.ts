import { NullablePromise, addon, DataList } from "pao-aop";

export interface reservation {
    /** 名字 */
    name?: any;
    /** 身份证号码 */
    id_card?: any;
    /** 生日 */
    date_birth?: any;
    /** 备注 */
    comment?: any;
    /** 个人信息 */
    personnel_info?: any;
    personnel_type?: any;
    telephone?: any;
    /* 性别 */
    sex?: any;
    /** 状态 */
    state?: any;
    /** 证件类型 */
    id_card_type?: any;
}
@addon('IReservationRegistrationService', '预约入住服务接口', '预约入住服务接口')
export class IReservationRegistrationService {
    /** 新增家属预约 */
    update_reservation_registration?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<reservation> | undefined> {
        return undefined;
    }
    app_update_reservation_registration?(reservation_registration: reservation): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_reservation_registration_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<reservation> | undefined> {
        return undefined;
    }
    /** 删除预约登记 */
    delete_reservation_registration?(reservation_registration_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_reservation_registration_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<reservation>> {
        return undefined;
    }
}