import { NullablePromise, DataList, addon } from "pao-aop";

@addon('ILedgerAccountManageService', '分账登记管理', '分账登记管理')
export class ILedgerAccountManageService {
    /**
     * 获取商品订单信息
     */
    confirm_payment?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // /**
    //  * 查询收货人地址详情
    //  */
    // get_consignee_address_detail_app?(condition?: {}): NullablePromise<DataList<any> | undefined> {
    //     return undefined;
    // }
    // /**
    //  * 保存收货人地址
    //  */
    // save_consignee_address_app?(data: {}): NullablePromise<boolean | undefined> {
    //     return undefined;
    // }
    // // 获取所有板块
    // get_app_blocks_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
    //     return undefined;
    // }
    // // 获取所有商品
    // get_goods_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
    //     return undefined;
    // }
    // // 获取所有服务类型
    // get_service_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
    //     return undefined;
    // }
    // // 获取机构下的服务和商品
    // get_products_list_by_organization?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
    //     return undefined;
    // }
}