import { NullablePromise, DataList, addon } from "pao-aop";
export interface Charitable {

}
@addon('ICharitableService', '慈善管理服务', '慈善管理服务')
export class ICharitableService {
    /**
     * 检查支付状态
     */
    checkPayStatus?(condition?: {}): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取募捐申请步骤
     */
    get_recipient_step?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取冠名基金
     */
    get_charitable_title_fund_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取冠名基金
     */
    get_charitable_title_fund_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 编辑冠名基金
     */
    update_charitable_title_fund?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善信息
     */
    get_charitable_information_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善会列表
     */
    get_charitable_society_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善会列表
     */
    get_charitable_society_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善捐款列表
     */
    get_charitable_donate_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增慈善捐款
     */
    update_charitable_donate?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善项目列表
     */
    get_charitable_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增编辑慈善项目
     */
    update_charitable_project?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取个人募捐申请列表
     */
    get_charitable_recipient_apply_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取个人募捐申请列表
     */
    get_charitable_recipient_apply_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增个人募捐申请
     */
    update_charitable_recipient_apply?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
}