import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 人员资料
 */
export interface Personnel {
    /** id */
    id: string;

    /** 证件号码 */
    id_card?: string;

    /** 证件类型 */
    id_card_type?: string;

    /** 人员类别 */
    personnel_category?: string;

    /** 姓名 */
    name?: string;

    /** 性别 */
    sex?: string;

    /** 出生日期 */
    date_birth?: string;

    /** 民族 */
    nation?: string;

    /** 备注 */
    comment?: string;

    /** 电话 */
    telephone?: string;

    /** 联系人 */
    contacts?: string;

    /** 联系人电话 */
    contacts_telephone?: string;
    /** 身份证地址 */
    id_card_address?: string;

    /** 家庭住址 */
    address?: string;

    /** 人员类型 */
    personnel_type?: string;

    /** 语言 */
    language?: string;

    /** 籍贯 */
    native_place?: string;

    /** 卡号 */
    card_number?: string;

}

export interface Organizational {
    /** id */
    id: string;

    /** 名称 */
    name?: string;

    /** 级别 */
    level?: string;

    /** 行政区划 */
    administrative_division?: string;

    /** 组织类型 */
    organization_type?: string;

    /** 备注 */
    comment?: string;

}
/** 人员组织结构服务 */
@addon('IPersonnelOrganizationalService', '人员组织结构服务', '人员组织结构服务')
export class IPersonnelOrganizationalService {
    /**
     * 获取人员列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    get_personnel_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Personnel>> {
        return undefined;
    }

    /** 修改/新增人员 */
    update_personnel?(personnel: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除人员 */
    delete_personnel?(personnelIDs: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 获取组织机构列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    get_organizational_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Personnel>> {
        return undefined;
    }

    /** 修改/新增组织机构 */
    update_organizational?(Organizational: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除组织机构 */
    delete_organizational?(Organizational: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取当前登录信息 */
    get_user_session?(): NullablePromise<boolean> {
        return undefined;
    }
}