import { addon, DataList, NullablePromise } from "pao-aop";

@addon('IServiceFollowCollectionService', '服务关注/收藏', '服务关注/收藏')
export class IServiceFollowCollectionService {
    /**
     * 获取收藏/关注列表
     */
    get_service_follow_collection_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 取消收藏/关注
     */
    delete_service_follow_collection?(ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 根据业务ID取消收藏/关注
     */
    delete_service_follow_collection_by_business_id?(condition: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 收藏/关注 */
    update_service_follow_collection?(id: string, type: string, object: string): NullablePromise<DataList<any>> {
        return undefined;
    }
}