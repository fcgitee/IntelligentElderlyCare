import { NullablePromise, addon } from "pao-aop";

/**
 *  需求
 */
export interface Demand {

}
/**  需求 */
@addon('IDemandService', '需求', '需求')
export class IDemandService {

    /** 编辑/新增需求 */
    update_demand?(condition?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取需求列表 */
    get_demand?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取需求详情 */
    get_demand_details?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 新增/编辑需求竞单 */
    update_demand_bidding?(condition?: {}, ): NullablePromise<boolean> {
        return undefined;
    }
    /** 取消需求 */
    cancel_demand?(condition?: {}, ): NullablePromise<boolean> {
        return undefined;
    }
}