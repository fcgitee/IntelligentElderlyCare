
import { NullablePromise, addon, DataList } from "pao-aop";

/**
 * 业务区域
 */
export interface BusinessArea {
    /** id */
    id?: string;
    /** 组织机构 */
    user_name?: string;
    /** logo */
    logo?: string;
}
/**
 * 业务区域配置项
 */
export interface BusinessAreaDispose {
    /** id */
    id?: string;
    /** 名称 */
    name?: string;
    /** 配置页面 */
    dispose_view?: string;
    /** 配置类别 */
    dispose_type?: string;
    /** 程序模块 */
    program_module?: string;
}
/**
 * 行政区划对象
 */
export interface AdministratorDivision {
    /** id */
    id?: string;
    /** 名称 */
    name?: string;
    /** 英文名称 */
    english_name?: string;
    /** 简称 */
    short_name?: string;
    /** 首府 */
    capital?: string;
    /** 地理位置 */
    location?: {
        /** 经度 */
        lon?: string;
        /** 纬度 */
        lat?: string;
    };
    /** 行政区划级别 */
    division_level?: string;
    /** 备注 */
    remarks?: string;
    /** 行政代码编号 */
    code?: string;
    /** 上级行政区划ID */
    parent_id?: string;
}
/**  业务区域管理服务 */
@addon('IBusinessAreaService', '业务区域管理服务', '业务区域管理服务')
export class IBusinessAreaService {
    /**
     * 查询业务区域
     */
    get_business_area_list?(condition?: {}, page?: number, count?: number): NullablePromise<BusinessArea | undefined> {
        return undefined;
    }
    get_business_area_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<BusinessArea | undefined> {
        return undefined;
    }
    get_business_area_list_look_delete?(condition?: {}, page?: number, count?: number): NullablePromise<BusinessArea | undefined> {
        return undefined;
    }
    get_admin_division_list_no_login?(condition?: {}, page?: number, count?: number): NullablePromise<BusinessArea | undefined> {
        return undefined;
    }
    /**
     * 新增业务区域
     */
    add_business_area?(businessArea: BusinessArea): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除业务区域
     */
    del_business_area?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 修改业务区域配置
     */
    update_business_area_dispose?(businessAreaDispose: BusinessAreaDispose): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 复制业务区域数据
     */
    copy_business_area_dispose?(id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询业务区域配置项列表 */
    get_business_area_item?(condition?: {}, page?: number, count?: number): NullablePromise<BusinessAreaDispose | undefined> {
        return undefined;
    }
    /** 查询行政区划列表 */
    get_all_admin_division_list?(condition?: {}, page?: number, count?: number): NullablePromise<AdministratorDivision | undefined> {
        return undefined;
    }
    get_admin_division_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AdministratorDivision> | undefined> {
        return undefined;
    }
    get_admin_division_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AdministratorDivision> | undefined> {
        return undefined;
    }
    get_admin_division_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AdministratorDivision> | undefined> {
        return undefined;
    }
    /** 新增修改行政区划 */
    update_admin_division?(adminDivision: AdministratorDivision): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除行政区划 */
    del_admin_division?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}