import { addon, DataList, NullablePromise } from "pao-aop";

export interface service {

}

@addon('IServicePackageListService', '服务套餐服务', '服务套餐服务')
export class IServicePackageListService {
    /**
     * 获取服务列表
     */
    get_service_package_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 获取服务套餐列表
     */
    get_service_product_package_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
}

@addon('IServiceProductDetailService', '服务详情', '服务详情')
export class IServiceProductDetailService {
    /** 获取服务商服务人员信息 */
    get_server_worker_list?(condition?: Object, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }

    /** 获取服务商信息 */
    get_server_list?(condition?: Object, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取服务详情
     */
    get_service_package_detail?(condition?: string): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 获取服务套餐详情
     */
    get_service_product_package_detail(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 获取服务产品列表 */
    get_service_product_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 获取服务产品/套餐列表 */
    get_service_product_item_package_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
}