
import { NullablePromise, DataList, addon } from "pao-aop";

export interface MyHome {

}

@addon('IMyHomeService', '新闻列表服务', '新闻列表服务')
export class IMyHomeService {
    /**
     * 获取我的房间列表
     */
    get_my_room_list?(): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取房间类型列表
    get_room_type_list?(): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 新建/编辑房间
    update_room?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取我的房间详情
    get_my_room_detail?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 删除房间
    del_room?(id: string): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取我的所有房间（不涉及设备）
    get_all_my_room_list?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }

    // 新建我的主机
    add_my_host?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取我的主机列表
    get_my_host_list?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 删除我的主机
    del_my_host?(id: string): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 我的主机详情
    get_my_host_detail?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取我的设备列表
    get_my_device_list?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 删除我的设备
    del_my_device?(id: string): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 我的设备详情
    get_my_device_detail?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 新建/编辑我的设备
    add_my_device?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 获取设备类型
    get_device_type_list?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 我的房间详情（警报信息）
    get_my_room_warning_detail?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
    // 我的历史报警信息
    get_my_history_warn?(condition: any): NullablePromise<DataList<MyHome>> {
        return undefined;
    }
}