import { NullablePromise, DataList, addon } from "pao-aop";

@addon('IFriendsCircleService', '老友圈', '老友圈')
export class IFriendsCircleService {
    /**
     * 获取老友圈广场列表
     */
    get_friends_circle_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 发布老友圈
     */
    update_friends_circle?(content: any, picture: any, location?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除老友圈接口
     */
    delete_friends_circle?(friends_circle_ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 发布评论
     */
    update_commont?(fcm_id: string, common: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除评论
     */
    del_commont?(comment_ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 点赞
     */
    update_like?(fcm_id: string): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取老友圈评论列表
     */
    get_commont_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取老友圈点赞列表
     */
    get_like_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 新增/修改工作人员出入档案
     */
    update_epidemic_prevention?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 查询当前工作人员出入档案
     */
    get_epidemic_prevention?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 查询工作人员出入档案
     */
    get_epidemic_prevention_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 更新每日登记信息
     */
    update_day_info?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 新增机构每日登记信息
     */
    update_org_day_info?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 查询机构每日登记信息
     */
    get_org_day_info?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
}