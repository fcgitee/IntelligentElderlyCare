import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  能力评估模板
 */
export interface AssessmentTemplate {
    /** id */
    id: string;
    /** 状态 */
    state?: string;
    /** 模板名称 */
    template_name?: string;
    /** 描述 */
    describe?: string;
    /** 创建日期 */
    creation_date?: string;
    /** 评估内容 */
    assessment_content?: any[];

}
/**  能力评估模板服务 */
@addon('IAssessmentTemplateService', '能力评估模板服务', '能力评估模板服务')
export class IAssessmentTemplateService {
    /**
     * 获取 能力评估模板列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_assessment_template_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentTemplate> | undefined> {
        return undefined;
    }
    get_assessment_template_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentTemplate> | undefined> {
        return undefined;
    }
    get_assessment_template_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentTemplate> | undefined> {
        return undefined;
    }
    /**
     * 获取 单条能力评估模板
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_assessment_template?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentTemplate> | undefined> {
        return undefined;
    }
    /** 修改/新增能力模板 */
    update_assessment_template?(assessment_template: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除能力模板 */
    delete_assessment_template?(assessment_template_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_user_list_worker_all?(assessment_template_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_user_list_worker_look?(assessment_template_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}