
import { NullablePromise, DataList, addon } from "pao-aop";
export type RecordStatus = "全部" | "未服务" | "服务中" | "已完成";
export type ServerList = "综合" | "销量" | "价格";

/**
 * 服务订单
 */
export interface ServiceOrder {
    /** id */
    id: string;
    /** 服务包名称 */
    name: string;
    /** 金额 */
    valuation_amount: string;
    /** 订单状态 */
    record_status: string;
    /** 组织机构名称 */
    orgnization_name: string;
    /** 服务名称 */
    service_name: string;
    /** 选项列表 */
    option_list: any[];
    /** 价格 */
    total_price: number;
}

/**  服务订单服务 */
@addon('IServiceOrderService', '服务订单服务', '服务订单服务')
export class IServiceOrderService {
    /**
     * 获取我的订单
     * @param condition 条件 关键字{record_status 待付款/待服务/服务中/待评价}
     * 
     */
    my_order_record?(condition?: { record_status?: RecordStatus }): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /**
     * 获取服务列表
     */
    get_service_package_list?(): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }
}