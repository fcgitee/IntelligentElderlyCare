/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-19 14:54:42
 * @LastEditTime : 2020-01-08 10:06:50
 * @LastEditors  : Please set LastEditors
 */
import { NullablePromise, addon } from "pao-aop";

/**
 * 轮播
 */
export interface CarouselData {
    /** id */
    id: string;
    /** 名称 */
    name: string;
    /** 图片列表 */
    urls: string;
}
/**  轮播服务 */
@addon('ICarouselService', '轮播服务', '轮播服务')
export class ICarouselService {
    /**
     * 获取轮播图片列表
     * @param condition 条件 
     * 
     */
    get_carousel_list?(condition?: any): NullablePromise<CarouselData | undefined> {
        return undefined;
    }
}