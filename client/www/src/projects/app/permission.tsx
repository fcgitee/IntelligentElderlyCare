import { Permission, PermissionState } from "pao-aop";
import React from "react";
import { Redirect } from "react-router";
import { ROUTE_PATH } from "src/projects/router";
import { IntelligentElderlyCareAppStorage } from "./appStorage";
/**
 * 权限列表
 */
export class PermissionList {
    /** 权限：用户管理查看 */
    static UserManage_Select = { 'permission': 'UserManage_Select', 'per_name': '用户增删改查权限', 'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': PermissionState.default };
    /** 权限：角色权限查看 */
    static RolePermission_Select = { 'permission': 'RolePermission_Select', 'per_name': '角色增删改查权限', 'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': PermissionState.default };
    /** 权限：安全设置查看 */
    static SecuritySettings_Select = { 'permission': 'SecuritySettings_Select', 'per_name': '安全设置增删改查权限', 'module': 'SystemManage', 'module_name': '系统管理', 'permission_state': PermissionState.default };
}

/** 判断权限 */
export function isPermission(permission: Permission, company_id?: string) {
    let userRole = IntelligentElderlyCareAppStorage.getCurrentUserRoles();
    // 判断是否拥有权限
    if (!userRole) {
        return;
    }
    for (let role of userRole) {
        if (role) {
            for (let permissions in role['permission']) {
                if (permission) {
                    if (role['permission'][permissions]['permission'] === permission['permission']
                        && role['permission'][permissions]['permission_state'] === PermissionState.forbid) {
                        return <Redirect to={ROUTE_PATH.Abnormity} />;
                    } else if (role['permission'][permissions]['permission'] === permission['permission']
                        && role['permission'][permissions]['permission_state'] === PermissionState.default) {
                        return <Redirect to={ROUTE_PATH.Abnormity} />;
                    }
                }

            }
        }
    }
    return;
}