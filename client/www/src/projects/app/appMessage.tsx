// import React from "react";
import { notification, Button } from 'antd';
import { AppServiceUtility } from './appService';
import { AjaxJsonRpcFactory } from 'pao-aop-client';
import { remote } from 'src/projects/remote';
import { IMessageService } from '../models/message';
import { getObject } from 'pao-aop';
import React from 'react';

/**
 * 组件：全局消息推送
 * 全局消息推送
 */
export class IntelligentElderlyCareAppMessage {

    // 正在展示的数组
    static megShowing: any = [];
    // 显示时间，单位秒
    static defaultDuration: number = 60;
    // 轮询时间，单位秒
    static defaultInterval: number = 3000;
    // 服务
    static service: any = null;
    // 定时器标识
    static stv: any = null;
    // 定时器持续标识
    static stvWorking: boolean = false;

    /**
     * 推送一条消息
     * @param message 标题 
     * @param description 内容
     * @param duration 显示时间，单位秒，传null则一直不消失
     */
    static messageShow(message: string, description: string, key: string = '', duration: number | null = this.defaultDuration) {
        // 没有key就默认一个
        const notificationKey = key || `nk${Date.now()}`;
        // 已经显示过的
        this.megShowing.push(notificationKey);
        // 创建一个警告提示框
        notification.open({
            message,
            description,
            duration,
            key: notificationKey,
            btn: <Button type="primary" size="small" onClick={() => this.messageClose(notificationKey)}>确定</Button>,
            onClose: () => {
                this.messageClose(notificationKey);
            },
        });
    }

    // 关闭操作，暂时可以视为已读
    static messageClose(key: string) {
        notification.close(key);
        this.NService()!.set_message_already_read!({ id: key })!.then((result: any) => {
            // console.log(result);
        });
    }

    // 轮询初始化
    static messagePollingInit() {
        // console.log('message_polling_init...');
        this.stv = setInterval(
            () => {
                if (!this.stvWorking) {
                    return;
                }
                this.NService()!.get_unread_message_list!({}, 1, 1)!.then((unread_message_list: any) => {
                    unread_message_list.result!.map((message_item: any) => {
                        if (this.inArray(message_item.id, this.megShowing) === false) {
                            this.messageShow(message_item.message_name, message_item.message_content, message_item.id);
                        }
                    });
                });
            },
            this.defaultInterval,
        );
    }

    // 停止轮询
    static messagePollingPause() {
        this.stvWorking = false;
    }

    // 继续轮询
    static messagePollingContinue() {
        this.stvWorking = true;
    }

    // 清除轮询
    static messagePollingClear() {
        clearInterval(this.stv);
    }

    // 创建一个服务
    static NService(): any {
        if (this.service !== null) {
            return this.service;
        }
        let factory = new AjaxJsonRpcFactory(IMessageService, remote.url, `IMessageService`);
        return getObject(factory);
    }

    // in_array
    static inArray(search: string, array: any) {
        for (var i in array) {
            if (array[i] === search) {
                return true;
            }
        }
        return false;
    }

    // 新增一个消息类型，测试用
    static messageTypeSave(typeName: string) {
        // 新增
        return AppServiceUtility.message_service.add_new_message_type!({ name: typeName })!
            .then((datas: any) => {
                return datas.result === 'Success' ? true : false;
            });
    }
}