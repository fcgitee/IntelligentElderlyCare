import { FormProps } from "antd/lib/form";
// import { Bed } from "../models/hotel_zone";
import { message } from "antd";
import { numCheck } from "src/business/util_tool";
import { isPhoneAvailable } from "pao-aop";

export function changeNameforStatus(code: string) {
    switch (code) {
        case 'valid':
            return '生效';
        case 'invalid':
            return '失效';
        case 'new':
            return '新建';
        default:
            return code;
    }
}

/** table公用的参数 */
export let table_param = {
    other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
    showHeader: true,
    bordered: false,
    show_footer: true,
    rowKey: 'id',
};

export let edit_props_info = {
    form_item_layout: {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    },  // 表单项大小配置
    form_props: {
        hideRequiredMark: true
    } as FormProps,
    row_btn_props: {
        style: {
            justifyContent: " center"
        }
    }
};

/** 上传前回调方法 */
export function beforeUpload(file: File) {
    const isImage = (file.type === 'image/jpeg' || file.type === 'image/png');
    if (!isImage) {
        message.error('你必须上传JPG/png格式的文件!');
    }
    const isLt1M = file.size / 1024 / 1024 < 2;
    if (!isLt1M) {
        message.error('图片大小必须小于2MB!');
    }
    return isLt1M && isImage;
}
/** 上传前回调方法 */
export function beforeUpload2(file: File, typelist: any) {
    const filename = file.name;
    const index = filename.lastIndexOf(".");
    const suffix = filename.substr(index + 1);
    let ifSuffix = true;
    if (typelist.indexOf(suffix.toLocaleLowerCase()) < 0) {
        ifSuffix = false;
        message.error(`你必须上传${typelist.join(',')}格式的文件!`);
    }
    const isLt1M = file.size / 1024 / 1024 < 2;
    if (!isLt1M) {
        message.error('文件大小必须小于2MB!');
    }
    return isLt1M && ifSuffix;
}
/**
 * 人员类别枚举值
 */
export const personnelCategory = {
    /** 用户 */
    user: '1',
    /** 长者 */
    elder: '长者',
    /** 员工 */
    staff: '2'
};
/** 服务结算状态 */
export const serviceSettlementState = {
    /** 已结算 */
    settled: 'settled',
    /** 未结算 */
    unliquidated: 'unliquidated'
};
// 任务状态
export const taskState = {
    to_be_processed: '待处理',
    failed_examine: '审核未通过',
    adopt_examine: '审核通过',
    to_be_receive: '待接收',
    ongoing: '进行中',
    rejected: '已拒绝',
    completed: '已完成',
    evaluated: '已评价',
};
export const personnel_type = {
    /** 组织 */
    /** 人员 */
    person: '1'
};

/** 电话以及手机输入框判空规则 */
export function checkContact(rule: any, value: { title: string; contents: string }[], callback: any) {
    if (Array.isArray(value)) {
        let empty: boolean = false;
        let isNotNum: boolean = false;
        let index = 0;
        for (const iterator of value) {
            index++;
            /** 判空检查 */
            if (index !== 1 && (iterator.title === "" || iterator.contents === "")) {
                empty = true;
            } else {
                /** 不为空，进行内容数字校验 */
                if (rule.type === "mobile") {
                    if (!isPhoneAvailable(iterator.contents)) {
                        isNotNum = true;
                    }
                } else {
                    if (!numCheck(iterator.contents)) {
                        isNotNum = true;
                    }
                }
            }
        }
        /** 有非数字 */
        if (isNotNum && value.length > 0 && value[0].contents) {
            if (rule.type === "mobile") {
                callback("请输入有效的手机号码");
            } else {
                callback("请输入有效的电话号码");
            }
        } else if (empty) {
            /** 自己为空 */
            callback("内容不能为空");
        } else {
            callback();
        }
    } else {
        callback();
    }
}

/** 用身份证号码获取出生日期 */
export function getBirthday(id_card: string) {
    let len = (id_card + "").length;
    // console.info(len);
    if (len === 0) {
        return;
    } else {
        // 身份证号码只能为15位或18位其它不合法
        if (len !== 15 && len !== 18) {
            return;
        }
    }
    let strBirthday = "";
    // 处理18位的身份证号码从号码中得到生日和性别代码
    if (len === 18) {
        strBirthday = id_card.substr(6, 4) + "/" + id_card.substr(10, 2) + "/" + id_card.substr(12, 2);
    }
    if (len === 15) {
        strBirthday = "19" + id_card.substr(6, 2) + "/" + id_card.substr(8, 2) + "/" + id_card.substr(10, 2);
    }
    // 时间字符串里，必须是“/”
    return strBirthday;
}

/** 用身份证获取年龄 */
export function getAge(id_card: string) {
    if (!id_card) {
        return '';
    }
    let birthday = new Date(getBirthday(id_card)!);
    let nowDateTime = new Date();
    let age = nowDateTime.getFullYear() - birthday!.getFullYear();
    // 再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
    if (nowDateTime.getMonth() < birthday!.getMonth() || (nowDateTime.getMonth() === birthday!.getMonth() && nowDateTime.getDate() < birthday!.getDate())) {
        age--;
    }
    // 解决
    if (isNaN(age)) {
        return '';
    }
    return age;
}

/** 用身份证获取年龄2 */
export function getAge2(identityCard: string) {
    if (identityCard === '') {
        return identityCard;
    }
    identityCard = identityCard + '';
    var len = (identityCard + "").length;
    if (len === 0) {
        return '';
    } else {
        // 身份证号码只能为15位或18位其它不合法
        if ((len !== 15) && (len !== 18)) {
            return '';
        }
    }
    var strBirthday = "";
    // 处理18位的身份证号码从号码中得到生日和性别代码
    if (len === 18) {
        strBirthday = identityCard.substr(6, 4) + "/" + identityCard.substr(10, 2) + "/" + identityCard.substr(12, 2);
    }
    if (len === 15) {
        strBirthday = "19" + identityCard.substr(6, 2) + "/" + identityCard.substr(8, 2) + "/" + identityCard.substr(10, 2);
    }
    // 时间字符串里，必须是“/”
    var birthDate = new Date(strBirthday);
    var nowDateTime = new Date();
    var age = nowDateTime.getFullYear() - birthDate.getFullYear();
    // 再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
    if (nowDateTime.getMonth() < birthDate.getMonth() || (nowDateTime.getMonth() === birthDate.getMonth() && nowDateTime.getDate() < birthDate.getDate())) {
        age--;
    }
    if (isNaN(age)) {
        return '';
    }
    return age;
}

/** 用身份证获取性别 */
export function getSex(id_card: string) {
    if (id_card.length === 15) {
        let sex = parseInt(id_card[14], undefined);
        if (sex % 2 === 0) {
            return '女';
        } else {
            return '男';
        }
    } else {
        let sex = parseInt(id_card[16], undefined);
        if (sex % 2 === 0) {
            return '女';
        } else {
            return '男';
        }
    }
}

/** 对url参数进行编码 */
export function encodeUrlParam(str: string) {
    return window.btoa(encodeURI(str));
}

/** 对url参数进行解码 */
export function decodeUrlParam(str: string) {
    return decodeURI(window.atob(str));
}