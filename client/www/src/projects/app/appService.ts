/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-08 09:54:26
 * @LastEditTime: 2020-03-02 13:59:26
 * @LastEditors: Please set LastEditors
 */
import { getAddonTypeName, getObject, IAnyType, IType } from "pao-aop";
import { AjaxJsonRpcFactory } from 'pao-aop-client';
import { AjaxJsonRpcLoadingFactory } from "src/business/components/buss-components/ajax-loading";
import { IPermissionService } from "src/business/models/role";
import { remote } from "src/projects/remote";
import { Test } from 'src/projects/views/buss_mis/data-import/data_import';
import { IPersonnelOrganizationalService as iPersonnelOrganizationalServices } from "../../business/models/personnel-organizational";
import { IAccommodationProcessService } from "../models/accommodation-process";
import { IActivityService } from "../models/activity";
import { IAllowanceService } from "../models/allowance-manage";
import { IArticleService } from '../models/article-manage';
import { IArticleTypeService } from "../models/article-type";
import { IAssessmentProjectService } from "../models/assessment-project";
import { IAssessmentTemplateService } from "../models/assessment-template";
import { IBehavioralCompetenceAssessmentService } from "../models/behavioral-competence-assessment";
import { IBillManageService } from "../models/bill-manage";
import { IBusinessAreaService } from "../models/business-area";
import { ICharitableService } from "../models/charitable";
import { CheckInService } from "../models/check-in";
import { ICommentManageService } from '../models/comment-manage';
import { ICommentTypeService } from '../models/comment-type';
import { ICompetenceAssessmentService } from "../models/competence-assessment";
import { CostManageService } from "../models/cost-manage";
import { ICostAccountService } from '../models/cost_accounting';
import { IDeviceService } from "../models/dervice";
import { IFiancialAccountService } from "../models/financial-account";
import { IFiancialService } from "../models/financial-manage";
import { IHospitalService } from "../models/hospital";
import { IHotelZoneService, IHotelZoneTypeService } from "../models/hotel_zone";
import { IHydropowerService } from "../models/hydropower";
import { IExcelService } from "../models/iexcel-service";
import { IKnowledgeBaseService } from "../models/knowledge-base";
import { ILeaveRecordService } from "../models/leave-record";
import { ILoginService } from "../models/login";
import { IMessageService } from "../models/message";
import { IMonitorService } from "../models/monitor";
import { INursingItemService, INursingService } from "../models/nursing";
import { nursingContentService } from "../models/nursing-content";
import { nursingRecordService } from "../models/nursing-record";
import { nursingTemplateService } from "../models/nursing-template";
import { IPersonOrgManageService } from "../models/person-org-manage";
import { IPersonnelOrganizationalService } from "../models/personnel-organizational";
import { ReportManage } from "../models/report-management";
import { IRequirementOptionService, IRequirementProjectService, IRequirementTypeService } from "../models/requirement";
import { IRequirementRecordService } from "../models/requirement-record";
import { IRequirementTemplateService } from "../models/requirement-template";
import { IResearchService } from "../models/research-manage";
import { IReservationService } from "../models/reservation";
import { IReservationRegistrationService } from "../models/reservation_registration";
import { IRoomService } from "../models/room";
import { IComfirmOrderService, IServiceItemPackage, IServicePackageListService, IServiceProductDetailService } from "../models/service-item-package";
import { IServicesLedgerService } from "../models/service-ledger";
import { IServiceOperationService } from "../models/service-operation";
import { IServiceOrderService } from "../models/service-order";
import { IServiceOrderTaskService } from "../models/service-order-task";
import { IServiceRecordService } from "../models/service-record";
import { IServicesProjectService } from "../models/services-project";
import { IServicesItemCategoryService } from "../models/services_item_category";
import { IServicrOptionService } from "../models/services_option";
import { IServiceScopeService } from "../models/service_scope";
import { ISocialGroupsTypeService } from "../models/social-groups-type";
import { ISubsidyAccountRechargService } from "../models/subsidy-account-recharg-service";
import { subsidyAutoRecharge } from "../models/subsidy-auto-recharge";
import { ITaskService } from "../models/task";
import { ITransactionService } from "../models/transaction-manage";
import { IUserService } from "../models/user";
import { IEmiService } from './../../business/models/emi-account';
import { IBaseSituationService } from './../models/base-situation';
import { ISmsManageService } from "../models/sms";
import { IOfficialFollowService } from "../models/official-follow";
import { IAnnoutmentService } from "../models/annoutment";
import { ICostService } from "../models/other-cost";
import { IHealthCareService } from "../models/health-care";
import { IUserRelationShipService } from "../models/user-relationship";
import { IAppPageConfigService } from "../models/app-config";
import { IOpinionFeedbackService } from "../models/opinion-feedback";
import { IFriendsCircleService } from "../models/friends-circle";
import { IMaterielService } from "../models/materiel";
import { ICallCenterNhManageService } from "../models/call_center_nh";
import { IShoppingMallManageService } from "../models/shopping-mall";

/**
 * 应用服务工具类
 */
export class AppServiceUtility {
    /**
     * 服务对象
     * @param service_interface 接口
     * @type T 接口类型
     */
    static service<T>(service_interface: IType<T> | Object): T {
        if (typeof service_interface === 'function') {
            let destFunc = service_interface as IAnyType;
            service_interface = new destFunc();
        }
        let service_name = getAddonTypeName(service_interface as any);
        let factory = new AjaxJsonRpcLoadingFactory(service_interface, remote.url, service_name);
        let service = getObject(factory);
        return service as T;
    }

    /**
     * 服务对象
     * @param service_interface 接口
     * @type T 接口类型
     */
    static serviceNoLoading<T>(service_interface: IType<T> | Object): T {
        if (typeof service_interface === 'function') {
            let destFunc = service_interface as IAnyType;
            service_interface = new destFunc();
        }
        let service_name = getAddonTypeName(service_interface as any);
        let factory = new AjaxJsonRpcFactory(service_interface, remote.url, service_name);
        let service = getObject(factory);
        return service as T;
    }

    /** 角色服务 */
    static get role_service() {
        return AppServiceUtility.service(IPermissionService);
    }
    /** 人员组织服务 */
    static get ipersonnel_service() {
        return AppServiceUtility.service(iPersonnelOrganizationalServices);
    }
    /** 人员组织服务 */
    static get personnel_service() {
        return AppServiceUtility.service(IPersonnelOrganizationalService);
    }
    /** 评估模板服务 */
    static get assessment_template_service() {
        return AppServiceUtility.service(IAssessmentTemplateService);
    }
    /** 评估项目服务 */
    static get assessment_project_service() {
        return AppServiceUtility.service(IAssessmentProjectService);
    }
    /** 单据服务 */
    static get bill_manage_service() {
        return AppServiceUtility.service(IBillManageService);
    }
    /** 用户服务 */
    static get user_service() {
        return AppServiceUtility.service(IUserService);
    }
    /** 护理档案服务 */
    static get nursing_service() {
        return AppServiceUtility.service(INursingService);
    }
    /** 护理项目档案服务 */
    static get nursing_item_service() {
        return AppServiceUtility.service(INursingItemService);
    }
    /** 确认订单服务 */
    static get comfirm_order_service() {
        return AppServiceUtility.service(IComfirmOrderService);
    }
    /** 预约登记服务 */
    static get reservation_service() {
        return AppServiceUtility.service(IReservationService);
    }
    /** 入住登记服务 */
    static get check_in_service() {
        return AppServiceUtility.service(CheckInService);
    }
    /** 房间管理服务 */
    static get room_service() {
        return AppServiceUtility.service(IRoomService);
    }
    /** 业务区域管理服务 */
    static get business_area_service() {
        return AppServiceUtility.service(IBusinessAreaService);
    }
    /** 服务运营服务 */
    static get service_operation_service() {
        return AppServiceUtility.service(IServiceOperationService);
    }
    /** 交易服务 */
    static get transaction_service() {
        return AppServiceUtility.service(ITransactionService);
    }
    /** 行为能力评估服务 */
    static get behavioral_competence_assessment_service() {
        return AppServiceUtility.service(IBehavioralCompetenceAssessmentService);
    }
    /** 财务管理服务 */
    static get financial_service() {
        return AppServiceUtility.service(IFiancialService);
    }
    /** 能力评估服务 */
    static get competence_assessment_service() {
        return AppServiceUtility.service(ICompetenceAssessmentService);
    }
    /** 服务项目服务 */
    static get services_project_service() {
        return AppServiceUtility.service(IServicesProjectService);
    }
    /** 住宿区域类型服务 */
    static get hotel_zone_type_service() {
        return AppServiceUtility.service(IHotelZoneTypeService);
    }
    /** 费用管理服务 */
    static get cost_manage_service() {
        return AppServiceUtility.service(CostManageService);
    }
    /** 住宿区域服务 */
    static get hotel_zone_service() {
        return AppServiceUtility.service(IHotelZoneService);
    }
    /** 需求类型服务 */
    static get requirement_type_service() {
        return AppServiceUtility.service(IRequirementTypeService);
    }
    /** 需求项目服务 */
    static get requirement_project_service() {
        return AppServiceUtility.service(IRequirementProjectService);
    }
    /** 需求项目服务 */
    static get requirement_option_service() {
        return AppServiceUtility.service(IRequirementOptionService);
    }
    /** 服务类别服务 */
    static get services_item_category_service() {
        return AppServiceUtility.service(IServicesItemCategoryService);
    }
    /** 服务适用范围服务 */
    static get service_scope_service() {
        return AppServiceUtility.service(IServiceScopeService);
    }
    /** 服务选项服务 */
    static get service_option_service() {
        return AppServiceUtility.service(IServicrOptionService);
    }
    /** 服务包服务 */
    static get service_item_package() {
        return AppServiceUtility.service(IServiceItemPackage);
    }
    /** 服务包服务 */
    static get service_item_service() {
        return AppServiceUtility.service(IServicePackageListService);
    }
    /** 服务包详情服务 */
    static get service_item_serviceinfo() {
        return AppServiceUtility.service(IServiceProductDetailService);
    }
    /** 服务订单任务 */
    static get service_order_task_service() {
        return AppServiceUtility.service(IServiceOrderTaskService);
    }
    /** 服务记录服务 */
    static get service_record_service() {
        return AppServiceUtility.service(IServiceRecordService);
    }
    /** 预约登记服务 */
    static get reservation_registration_service() {
        return AppServiceUtility.service(IReservationRegistrationService);
    }
    /** 服务订单服务 */
    static get service_order_service() {
        return AppServiceUtility.service(IServiceOrderService);
    }
    /** 需求模板服务 */
    static get requirement_template_service() {
        return AppServiceUtility.service(IRequirementTemplateService);
    }
    /** 需求模板服务 */
    static get requirement_record_service() {
        return AppServiceUtility.service(IRequirementRecordService);
    }
    /** 台账服务 */
    static get service_ledger_service() {
        return AppServiceUtility.service(IServicesLedgerService);
    }
    /** 文章服务 */
    static get article_service() {
        return AppServiceUtility.service(IArticleService);
    }
    /** 评论管理服务 */
    static get comment_manage_service() {
        return AppServiceUtility.service(ICommentManageService);
    }
    /** 活动发布 */
    static get activity_service() {
        return AppServiceUtility.service(IActivityService);
    }
    /** 文章类型 */
    static get article_type_service() {
        return AppServiceUtility.service(IArticleTypeService);
    }
    /** 任务管理服务 */
    static get task_service() {
        return AppServiceUtility.service(ITaskService);
    }
    /** 信息管理服务 */
    static get message_service() {
        return AppServiceUtility.service(IMessageService);
    }
    static get message_service2() {
        return AppServiceUtility.serviceNoLoading(IMessageService);
    }
    /** 补贴管理服务 */
    static get allowance_service() {
        return AppServiceUtility.service(IAllowanceService);
    }
    /** 调研管理服务 */
    static get research_service() {
        return AppServiceUtility.service(IResearchService);
    }
    /** 请假销假服务 */
    static get leaveRecord_service() {
        return AppServiceUtility.service(ILeaveRecordService);
    }
    /** 医院管理 */
    static get hospital_manage() {
        return AppServiceUtility.service(IHospitalService);
    }
    /** 水电抄表 */
    static get hydropower() {
        return AppServiceUtility.service(IHydropowerService);
    }
    /** 评论类型 */
    static get comment_type_service() {
        return AppServiceUtility.service(ICommentTypeService);
    }
    /** 账务管理 */
    static get financial_account_service() {
        return AppServiceUtility.service(IFiancialAccountService);
    }
    /** 费用核算 */
    static get cost_accounting_service() {
        return AppServiceUtility.service(ICostAccountService);
    }
    /** 部分住宿过程服务 */
    static get accommodation_process_service() {
        return AppServiceUtility.service(IAccommodationProcessService);
    }
    /** 权限人员组织机构管理 */
    static get person_org_manage_service() {
        return AppServiceUtility.service(IPersonOrgManageService);
    }
    /** 登录服务 */
    static get login_service() {
        return AppServiceUtility.service(ILoginService);
    }
    /** 设备服务 */
    static get device_service() {
        return AppServiceUtility.service(IDeviceService);
    }
    /** 社会群体服务服务 */
    static get social_groups_type_service() {
        return AppServiceUtility.service(ISocialGroupsTypeService);
    }
    /** 监控服务 */
    static get monitor_service() {
        return AppServiceUtility.service(IMonitorService);
    }
    /** 护理内容 */
    static get nursing_content() {
        return AppServiceUtility.service(nursingContentService);
    }
    /** 护理模板列表 */
    static get nursing_template() {
        return AppServiceUtility.service(nursingTemplateService);
    }
    /** 护理记录列表 */
    static get nursing_record() {
        return AppServiceUtility.service(nursingRecordService);
    }
    /** 慈善管理 */
    static get charitable_service() {
        return AppServiceUtility.service(ICharitableService);
    }
    /** 报表管理 */
    static get report_management() {
        return AppServiceUtility.service(ReportManage);
    }
    /** 补贴自动充值控制管理 */
    static get subsidy_auto_recharge() {
        return AppServiceUtility.service(subsidyAutoRecharge);
    }
    /** 基础档案服务 */
    static get iexcel_service() {
        return AppServiceUtility.service(IExcelService);
    }
    /** 易米云通服务 */
    static get emi_service_no_loading() {
        return AppServiceUtility.serviceNoLoading(IEmiService);
    }
    /** 易米云通服务 */
    static get emi_service_with_loading() {
        return AppServiceUtility.service(IEmiService);
    }
    /** 知识库服务 */
    static get knowledge_service() {
        return AppServiceUtility.service(IKnowledgeBaseService);
    }
    /** 账户充值服务 */
    static get subsidy_account_recharg_service() {
        return AppServiceUtility.service(ISubsidyAccountRechargService);
    }
    /** 基本情况 */
    static get service_base_situation() {
        return AppServiceUtility.service(IBaseSituationService);
    }
    static get data_import() {
        return AppServiceUtility.service(Test);
    }
    /** 短信服务 */
    static get sms_manage_service() {
        return AppServiceUtility.service(ISmsManageService);
    }
    /** 南海呼叫中心 */
    static get call_center_nh_service() {
        return AppServiceUtility.service(ICallCenterNhManageService);
    }
    static get call_center_nh_no_loading_service() {
        return AppServiceUtility.serviceNoLoading(ICallCenterNhManageService);
    }
    /** 商城管理 */
    static get shopping_mall_service() {
        return AppServiceUtility.service(IShoppingMallManageService);
    }
    /** 公众号关注服务 */
    static get official_follow_service() {
        return AppServiceUtility.service(IOfficialFollowService);
    }
    /** 系统公告 */
    static get annoutment_service() {
        return AppServiceUtility.service(IAnnoutmentService);
    }
    /** 其他费用 */
    static get cost_service() {
        return AppServiceUtility.service(ICostService);
    }
    /** 健康照护 */
    static get health_care_service() {
        return AppServiceUtility.service(IHealthCareService);
    }
    /** 家庭档案 */
    static get family_files_service() {
        return AppServiceUtility.service(IUserRelationShipService);
    }
    /** APP页面设置 */
    static get app_page_config_service() {
        return AppServiceUtility.service(IAppPageConfigService);
    }
    /** APP意见反馈 */
    static get opinion_feedback_service() {
        return AppServiceUtility.service(IOpinionFeedbackService);
    }
    /** APP老友圈 */
    static get friends_circle_service() {
        return AppServiceUtility.service(IFriendsCircleService);
    }
    static get materiel_service() {
        return AppServiceUtility.service(IMaterielService);
    }
}