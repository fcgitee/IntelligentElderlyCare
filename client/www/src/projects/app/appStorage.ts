/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-06-20 17:38:17
 * @LastEditTime: 2019-10-25 11:04:41
 * @LastEditors: Please set LastEditors
 */

import { IUserService, User } from "src/business/models/user";
import { Ref, getObject, log, Role } from "pao-aop";
import { CookieUtil } from "pao-aop-client";
import { COOKIE_KEY_CURRENT_USER, COOKIE_KEY_USER_ROLE } from "src/business/mainForm/backstageManageMainForm";
import { AppServiceUtility } from "./appService";

/**
 * 工业互联网应用存储
 */
export class IntelligentElderlyCareAppStorage {

    /** 用户服务 */
    static userService_Fac?: Ref<IUserService>;
    /** 用户服务 */
    static get userService() {
        return getObject(IntelligentElderlyCareAppStorage.userService_Fac);
    }
    /** 处理全局数据事件 */
    static async handleGlobalDataEvent() {
        try {
            const currentUser = await AppServiceUtility.person_org_manage_service.get_current_user_info!()!;
            const user = { id: '', name: '', org_id: '' };
            if (currentUser && currentUser.length > 0) {
                user.id = currentUser[0].id;
                user.name = currentUser[0].name;
                user.org_id = currentUser[0].org_id;
            }
            CookieUtil.save(COOKIE_KEY_CURRENT_USER, user!);
            // const roles = await IntelligentElderlyCareAppStorage
            //     .userService!
            //     .get_current_role!()!;
            // CookieUtil.save(COOKIE_KEY_USER_ROLE, roles!);
            // console.log('roles', roles!);
        } catch (error) {
            log('IntelligentElderlyCareAppStorage', `${error.message}`);
        }
    }
    /** 获取当前用户角色 */
    static getCurrentUserRoles() {
        return CookieUtil.read<Role[]>(COOKIE_KEY_USER_ROLE);
    }

    /** 获取用户信息 */
    static getCurrentUser() {
        return CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER);
    }
}