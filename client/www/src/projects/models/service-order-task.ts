import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  服务订单
 */
export interface ServiceOrderTask {
}
/**  服务订单任务分派 */
@addon('IServiceOrderTaskService', '服务订单任务分派', '服务订单任务分派')
export class IServiceOrderTaskService {
    /**
     * 获取 服务订单列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_order_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrderTask> | undefined> {
        return undefined;
    }
    get_service_order_task_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrderTask> | undefined> {
        return undefined;
    }
    get_service_order_task_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrderTask> | undefined> {
        return undefined;
    }
    // 获取服务项目名称列表
    get_services_item_category_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrderTask> | undefined> {
        return undefined;
    }
    /** 修改/新增服务订单 */
    update_service_order_task_manage?(service_order: any[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 服务订单取消派工 */
    cancel_assign_service_order_task_manage?(order_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
}