import { NullablePromise, addon, DataList } from "pao-aop";
@addon('ISubsidyAccountRechargService', '账户充值', '账户充值')
export class ISubsidyAccountRechargService {
    /**
     * 查询列表
     */
    get_subsidy_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 充值接口
     */
    recharg_subsidy_account?(obj?: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 清零接口
     */
    clearn_subsidy_account?(obj?: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 账户明细列表
     * @param condition 条件
     * @param page 叶码
     * @param count 条数
     */
    get_subsidy_account_detail?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 慈善账户查询
    get_charitable_subsidy_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 慈善账户充值接口
     */
    recharg_charitable_account?(obj?: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 慈善账户清零接口
     */
    clearn_charitable_account?(obj?: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 慈善账户明细列表
     * @param condition 条件
     * @param page 叶码
     * @param count 条数
     */
    get_charitable_account_detail?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取余额
    get_account_balance?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

}