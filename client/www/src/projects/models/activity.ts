/*
 * @Description: In User Settings Edit
 * @Author: 杨子毅
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-09-25 11:27:10
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, DataList, addon } from "pao-aop";

export interface Activity {

}
export interface ActivitySignIn {

}
export interface ActivityParticipate {

}
/**
 * 活动类型
 */
export interface ActivityType {
    /** id */
    id?: string;
    /** 名称 */
    name?: string;
    /** 编号 */
    num?: string;
    /** 备注 */
    remarks?: string;
}
/**
 * 活动室
 */
export interface ActivityRoom {
    /** id */
    id?: string;
    /** 活动室名称 */
    activity_room_name?: string;
    /** 最大参与人数 */
    max_quantity?: string;
    /** 开放时间 */
    open_date?: any;
    /** 所属机构 */
    organization_name?: string;
    /** 地址 */
    address?: string;
    /** 图片 */
    photo?: string[];
    /** 备注 */
    remarks?: string;
}
/**
 * 活动室预约
 */
export interface ActivityRoomReservation {
    /** 活动室名称 */
    activity_room_name?: string;
    /** 参与人 */
    user_name?: string;
    /** 预约时间 */
    // reservate_date?: string;
}

export interface ActivityNull {

}
@addon('IActivityService', '活动服务', '活动服务')
export class IActivityService {
    /**
     * 查询活动信息
     */
    get_activity_information_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    /**
     * 查询活动列表
     */
    get_activity_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    get_activity_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    get_activity_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    // 这个去掉很多参数，提升查询效率
    get_activity_pure_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    get_activity_pure_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Activity> | undefined> {
        return undefined;
    }
    /**
     * 查询活动签到列表
     */
    get_activity_sign_in_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    get_activity_sign_in_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    get_activity_sign_in_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    get_activity_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    get_activity_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivitySignIn> | undefined> {
        return undefined;
    }
    /**
     * 查询活动参与列表
     */
    get_activity_participate_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    get_activity_participate_pure_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    get_activity_participate_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    get_activity_participate_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityParticipate> | undefined> {
        return undefined;
    }
    /**
     * 新增活动
     */
    update_activity?(activity: Activity): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除活动
     */
    del_activity?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 参与活动
     */
    participate_activity?(activity_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * PC参与活动
     */
    participate_activity_pc?(activity_id: string, is_pc?: boolean): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 活动签到
     */
    sign_in_activity?(activity_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 删除活动签到信息
     */
    delete_activity_participate?(activity_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 删除活动签到信息
     */
    delete_sign_in_activity?(ids: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 删除活动签到信息
     */
    delete_activity_signin?(ids: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询活动类型列表 */
    get_activity_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityType> | undefined> {
        return undefined;
    }
    /** 新增/修改活动类型 */
    update_activity_type?(activity_type: ActivityType): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除活动类型 */
    del_activity_type?(activity_type_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询活动室列表 */
    get_activity_room_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoom> | undefined> {
        return undefined;
    }
    get_activity_room_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoom> | undefined> {
        return undefined;
    }
    get_activity_room_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoom> | undefined> {
        return undefined;
    }
    /** 新增/修改活动室 */
    update_activity_room?(activity_room: ActivityRoom): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除活动室 */
    del_activity_room?(activity_room_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询活动室预约列表 */
    get_activity_room_reservation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoomReservation> | undefined> {
        return undefined;
    }
    get_activity_room_reservation_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoomReservation> | undefined> {
        return undefined;
    }
    // 活动室预约
    update_activity_room_reservation?(nursig: ActivityNull): NullablePromise<DataList<ActivityNull> | undefined> {
        return undefined;
    }
    /** 删除活动室预约 */
    del_activity_room_reservation?(activity_room_reservate_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 获取调查问卷列表
    get_questionnaire_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ActivityRoomReservation> | undefined> {
        return undefined;
    }
    // 新增调查问卷
    update_questionnaire?(new_data: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 导入活动报名列表
     */
    upload_sign_in_list?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}