import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 护理级别档案
 */
export interface OtherCost {
    /** 档案id */
    id: string;
    /** 序号 */
    name: string;
    /** 护理类别 */
    remark: string;
}
/** 其他费用服务 */
@addon('ICostService', '其他费用服务', '其他费用服务')
export class ICostService {
    /**
     * 获取其他费用列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    get_other_cost?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<OtherCost> | undefined> {
        return undefined;
    }
    /**
     * 修改/新增其他费用
     * @param condition
     */
    update_other_cost?(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除其他费用
     * @param ids 
     */
    delete_other_cost?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 获取费用类型列表
     * @param condition 
     * @param page 
     * @param count 
     */
    get_cost_type?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<OtherCost> | undefined> {
        return undefined;
    }
    /**
     * 新建/编辑费用类型
     * @param condition 
     */
    update_cost_type?(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除费用类型
     * @param ids 
     */
    delete_cost_type?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}