import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  服务项目类别
 */
export interface ServicesItemCategory {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 编号 */
    number?: string;
    /** 备注 */
    remarks?: string;

}
/**  服务项目类别服务 */
@addon('IServicesItemCategoryService', '服务项目类别服务', '服务项目类别服务')
export class IServicesItemCategoryService {
    /**
     * 获取 服务项目类别列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_services_item_category_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesItemCategory> | undefined> {
        return undefined;
    }
    get_services_item_category_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesItemCategory> | undefined> {
        return undefined;
    }
    get_services_item_category_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesItemCategory> | undefined> {
        return undefined;
    }
    /** 修改/新增服务项目类别 */
    update_services_item_category?(services_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务项目类别 */
    delete_services_item_category?(services_project_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}