import { addon, NullablePromise, DataList } from "pao-aop";

export interface KnowledgeList {
}
export interface knowledge {
    title: string;
    keyword:string;
    type:string;
    content:string;
}
@addon('IKnowledgeBaseService', '知识库', '知识库')
export class IKnowledgeBaseService {
    /**
     * 获取知识库列表
     */
    get_knowledge_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<KnowledgeList> | undefined> {
        return undefined;
    }
    /**
     * 更新知识库
     */
    update_knowledge?(knowledge:knowledge): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除某条数据
     */
    del_knowledge?(id:any): NullablePromise<boolean | undefined> {
        return undefined;
    }
}
