import { NullablePromise, DataList, addon } from "pao-aop";

@addon('CostManageService', '费用管理服务接口', '费用管理服务接口')
export class CostManageService {
    /**
     * 
     * @param condition 获取杂费列表
     * @param page 
     * @param count 
     */
    get_incidental_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_incidental_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 修改/新增杂费 */
    update_incidental?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除杂费
     */
    delete_incidental?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 
     * @param condition 获取餐费列表
     * @param page 
     * @param count 
     */
    get_food_cost_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_food_cost_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 修改/新增餐费 */
    update_food_cost?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除餐费
     */
    delete_food_cost?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 
     * @param condition 获取医疗费用列表
     * @param page 
     * @param count 
     */
    get_medical_cost_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 修改/新增医疗费用 */
    update_medical_cost?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除餐费
     */
    delete_medical_cost?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 
     * @param condition 获取尿片费用列表
     * @param page 
     * @param count 
     */
    get_diaper_cost_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 修改/新增尿片费用 */
    update_diaper_cost?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除餐费
     */
    delete_diaper_cost?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    upload_incidental?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    upload_food_cost?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    export_template?(): NullablePromise<boolean | undefined> {
        return undefined;
    }

}