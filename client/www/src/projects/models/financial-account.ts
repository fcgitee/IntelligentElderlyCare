/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Sunday August 18th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Sunday, 18th August 2019 11:22:09 am
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、账务管理系统
 */
import { NullablePromise, DataList, addon } from "pao-aop";
export interface Unpayaccount {
    /** 项目 */
    item?: string;
    /** 金额 */
    amount?: number;
}
@addon('IFiancialAccountService', '账务管理接口', '账务管理接口')
export class IFiancialAccountService {
    /**
     * 收费员获取需要收款的账单
     */
    get_unpay_account_list_manage?(condition?: {}): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    get_charge_record_list_all?(condition?: {}): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    get_bank_unreconciliation_list_look_export?(condition?: {}): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    get_bank_unreconciliation_list_manage?(condition?: {}): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /**
     * 获取收费记录（收费员）
     */
    get_charge_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /**
     * 获取银行扣款单（未划扣）
     */
    get_bank_unreconciliation_list?(page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    get_bank_reconciliation_list_all?(page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /**
     * 获取银行扣款结果（已划扣）
     */
    get_bank_reconciliation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /**
     * 收费员完成收款
     */
    complete_pay_account?(data?: {}, ): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /**
     * 收费员获取需要付款的账单
     */
    get_need_pay_account_list?(data?: {}, ): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /** 导出 */
    exprot_excel_manage?(): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    exprot_excel_look?(): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /** 导入 */
    upload_excel_manage?(data?: any): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    upload_excel_look?(data?: any): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /** 非银行扣款单管理查询 */
    get_unbank_reconciliation_list_manage(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    get_unbank_reconciliation_list_Look(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /** 修改非银行扣款单状态 */
    update_unbank_reconciliation?(data?: any): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    upload_excel?(data?: any): NullablePromise<DataList<Unpayaccount>> {
        return undefined;
    }
    /** 导入 */
    upload_excel_band_record?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }

}