/*
 * @Description: In User Settings Edit
 * @Author: 杨子毅
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-12-11 15:37:10
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, DataList, addon } from "pao-aop";

export interface AllowanceInsert {
    /** 姓名 */
    name: string;
    /** 性别 */
    sex: string;
    /** 出生日期 */
    date_birth: string;
    /** 证件类型 */
    id_card_type: string;
    /** 证件号码 */
    id_card_number: string;
    /** 电话 */
    telephone: string;
    /** 民族 */
    nation: string;
    /** 籍贯 */
    native_place: string;
    /** 家庭住址 */
    address: string;
    /** 家属姓名 */
    contacts_name: string;
    /** 家属电话 */
    contacts_telephone: string;
    /** 家属关系 */
    contacts_type: string;
    /** 申请理由 */
    apply_reasons: string;
    /** 申请材料 */
    apply_material: any;
    /** 申请状态 */
    status?: string;
    /** 通过/不通过理由 */
    check_reason?: string;
    /** id */
    id?: string;
    /** 补贴金额 */
    money?: string;

}

export interface AllowanceDefine {
    /** 定义名称 */
    name: string;
    /** 评估模板id */
    template_id: string;
    /** 创建时间 */
    create_time: Date;
    /** 备注 */
    remark: string;
}
export interface oldAllowance {
    /** 姓名 */
    name: any;
}
export interface AllowanceProject {
    id: string;
    allowance_project_name: string;
    allowance_project_type: string;
    create_date: Date;
}
export interface subsidyApproval {
    /** id */
    id: string;
    money: string;
    status: string;
    check_reason: string;
}
export interface OldAllowanceStatistics {
    /** 镇（街) */
    town_street?: string;
    /** 村（居委） */
    village?: string;
    /** 70-79周岁长者人数 */
    s_elder_quantity?: number;
    /** 70-79周岁认证人数 */
    s_elder_authentication_quantity?: number;
    /** 80-89周岁长者人数 */
    e_elder_quantity?: number;
    /** 80-89周岁认证人数 */
    e_elder_authentication_quantity?: number;
    /** 90-99周岁长者人数 */
    n_elder_quantity?: number;
    /** 90-99周岁认证人数 */
    n_elder_authentication_quantity?: number;
    /** 100周岁以上长者人数 */
    h_elder_quantity?: number;
    /** 100周岁以上认证人数 */
    h_elder_authentication_quantity?: number;
    /** 长者人数合计 */
    elder_total_quantity?: number;
    /** 认证人数合计 */
    elder_authentication_total_quantity?: number;
    /** 认证完成率合计 */
    elder_complete_percent_total_quantity?: number;
    /** 未认证人数合计 */
    elder_uncertified_total_quantity?: number;
}
@addon('IAllowanceService', '补贴管理', '补贴管理')
export class IAllowanceService {
    /**
     * 查询申请列表
     */
    get_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    get_allowance_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    get_currentUser_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询已申请列表
     */
    get_allowance_applied_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    get_allowance_applied_list_look_edit?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 新增申请
     */
    update_allowance?(allowance: AllowanceInsert): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除申请
     */
    del_allowance?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取评估模板
     */
    get_assessment_template?(): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 更新补贴定义
     * @param define 
     */
    add_allowance_define?(allowance: AllowanceDefine): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 更新补贴项目
     * @param define 
     */
    add_allowance_project?(allowance: AllowanceProject): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 查询补贴项目列表
     */
    get_allowance_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceProject> | undefined> {
        return undefined;
    }
    /**
     * 查询补贴项目列表
     */
    get_allowance_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceProject> | undefined> {
        return undefined;
    }
    /**
     * 查询补贴定义列表
     */
    get_allowance_define_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    get_allowance_define_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 删除补贴定义
     */
    del_allowance_define?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除补贴项目
     */
    del_allowance_project?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /*审核申请 */
    subsidy_approval?(subsidy_data: subsidyApproval): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_allowance_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询审核意见
     */
    get_reviewed_idea?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 活体认证
     */
    user_identification?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 查询高龄补贴审核列表
     */
    get_audit_old_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<oldAllowance> | undefined> {
        return undefined;
    }

    get_audit_old_allowance_no_check_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<oldAllowance> | undefined> {
        return undefined;
    }
    /** 高龄津贴认证删除 */
    del_old_age_allowance?(condition?: {}): NullablePromise<DataList<AllowanceInsert> | undefined> {
        return undefined;
    }
    /**
     * 高龄补贴审核
     */
    audit_old_allowance?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 高龄补贴名单列表
     */
    get_old_age_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<oldAllowance> | undefined> {
        return undefined;
    }
    /**
     * 导入高龄津贴名单
     */
    upload_old_age_list?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 高龄津贴统计报表查询
     */
    get_old_age_statistics?(condition?: {}): NullablePromise<DataList<OldAllowanceStatistics> | undefined> {
        return undefined;
    }
    /**
     * 残疾人名单 
     */
    get_disabled_person_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<oldAllowance> | undefined> {
        return undefined;
    }
    upload_disabled_person_list?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_audit_disabled_person_allowance_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<oldAllowance> | undefined> {
        return undefined;
    }
    get_disabled_person_statistics?(condition?: {}): NullablePromise<DataList<OldAllowanceStatistics> | undefined> {
        return undefined;
    }
    update_disabled_info?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    shengchengtaizhang?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}