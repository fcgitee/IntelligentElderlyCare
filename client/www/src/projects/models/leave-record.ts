import { addon, DataList, NullablePromise } from "pao-aop";

export interface LeaveRecord {
    id?: string;
    /** 入住人id */
    residents_id?: string;
    /** 床位id */
    bed_id?: string;
    /** 申请时间 */
    apply_date?: string;
    /** 离开时间 */
    checkout_date?: any;
    /** 接出人id */
    pickout_id?: string;
    /** 离开原因id */
    record_reason_id?: string;
    /** 记录说明 */
    record?: string;
    /** 操作人员id */
    operating_personnel_id2?: string;
    /** 状态 */
    status?: string;
    reason_type?: string;
}
export interface LeaveReason {
    /** id */
    id: string;
    /** 名称 */
    name: string;
    /** 编号 */
    code: string;
    /** 备注 */
    remark: string;
}
@addon('ILeaveRecordService', '离开原因服务接口', '离开原因服务接口')
export class ILeaveRecordService {
    /**
     * 新增/修改住宿记录信息
     */
    update_leave_record?(LeaveRecord: LeaveRecord): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取住宿记录列表
     */
    get_leave_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveRecord> | undefined> {
        return undefined;
    }
    get_leave_reason_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveRecord> | undefined> {
        return undefined;
    }
    /**
     * 获取住宿记录列表
     */
    get_leave_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveRecord> | undefined> {
        return undefined;
    }
    get_leave_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveRecord> | undefined> {
        return undefined;
    }
    get_leave_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveRecord> | undefined> {
        return undefined;
    }
    /**
     * 删除住宿记录
     */
    del_leave_record(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取离开原因列表
     */
    get_leave_reason_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<LeaveReason> | undefined> {
        return undefined;
    }
    /**
     * 新增/修改离开原因信息
     */
    update_leave_reason?(leaveReason: LeaveReason): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 删除离开原因
     */
    del_leave_reason?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

}