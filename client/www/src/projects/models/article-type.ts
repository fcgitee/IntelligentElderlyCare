import { NullablePromise, DataList, addon } from "pao-aop";

export interface ArticleType {
}
@addon('IArticleTypeService', '文章类型服务接口', '文章类型服务接口')
export class IArticleTypeService {
    /**
     * 获取文章类型列表
     */
    get_article_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_article_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_article_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除文章类型
     */
    delete_article_type?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 新增/修改文章类型
     */
    update_article_type?(article_type: ArticleType): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 新增/修改文章类型权限
     */
    check_article_type_permission?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
}