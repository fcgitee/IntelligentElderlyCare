import { NullablePromise, DataList, addon } from "pao-aop";
export interface NNN {

}
@addon('ICallCenterNhManageService', '南海呼叫中心', '南海呼叫中心')
export class ICallCenterNhManageService {
    /**
     * 接口
     */
    api?(type: string, condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 获取记录
     */
    get_record_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 编辑录音
     */
    update_record?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 查询移动呼叫中心帐号
     */
    get_call_center_zh_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 查询移动呼叫中心帐号
     */
    get_call_center_zh_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 更新移动呼叫中心帐号
     */
    update_call_center_zh?(data?: any): NullablePromise<DataList<NNN>> {
        return undefined;
    }
    /**
     * 删除移动呼叫中心帐号
     */
    delete_call_center_zh?(data?: any): NullablePromise<DataList<NNN>> {
        return undefined;
    }
    /**
     * 查询爱关怀
     */
    get_agh_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    /**
     * 更新爱关怀
     */
    update_agh?(data?: any): NullablePromise<DataList<NNN>> {
        return undefined;
    }
    /**
     * 删除爱关怀
     */
    delete_agh?(data?: any): NullablePromise<DataList<NNN>> {
        return undefined;
    }
    // 适老化设备警报
    get_device_warning_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_device_warning_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 适老化设备管理
    get_device_manage_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_device_manage_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取长者的设备列表
    get_edler_device_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取设备警报列表
    get_device_warn_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取呼叫中心日志列表
    get_call_center_log_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 坐席运营统计
    get_seat_operation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 记录点击接听操作的时间
    update_yingda?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
}