import { addon, DataList, NullablePromise } from "pao-aop";
/** 登录服务接口 */
@addon('ILoginService', '活动服务', '活动服务')
export class ILoginService {
    /** 登录 */
    login?(login_data?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取角色列表 */
    get_role_list(): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 设置角色id和区域id服务 */
    set_role_org_session(role_id?: string, org_id?: string, org_name?: string): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取功能列表 */
    get_function_list(): NullablePromise<any> {
        return undefined;
    }
    /** 获取消息数量 */
    get_message_list_new(condition?: any, page?: number, count?: number): NullablePromise<any> {
        return undefined;
    }
    /** 获取待审批的提醒数据 */
    get_notice_list(): NullablePromise<any> {
        return undefined;
    }
    /** 修改密码 */
    retrieve_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 修改手机号码 */
    modify_mobile?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 重置密码的操作 */
    modify_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 修改初始密码的操作 */
    first_modify_password?(data?: {}): NullablePromise<any> {
        return undefined;
    }
}