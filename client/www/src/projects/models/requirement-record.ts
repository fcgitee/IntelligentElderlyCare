import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  需求记录
 */
export interface RequirementRecord {
    /** id */
    id: string;
    /** 长者id */
    elder?: string;
    /** 需求模板 */
    template?: string;
    /** 项目列表 */
    project_list?: any[];
    /** 总分 */
    // total_score?: any[];

}
/**  需求记录服务 */
@addon('IRequirementRecordService', '需求记录服务', '需求记录服务')
export class IRequirementRecordService {
    /**
     * 获取 需求记录服务列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_requirement_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<RequirementRecord> | undefined> {
        return undefined;
    }
    /** 修改/新增需求记录 */
    update_requirement_record?(requirement_record: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除需求记录 */
    delete_requirement_record?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}