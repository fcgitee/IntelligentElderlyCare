import { NullablePromise, DataList, addon } from "pao-aop";

export interface Annoutment {
    /** id */
    id: string;
    content: string;
    operator_id: string;
}
@addon('IAnnoutmentService', '系统公告接口', '系统公告接口')
export class IAnnoutmentService {
    /**
     * 获取文章类型列表
     */
    get_annoutment_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_annoutment_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除文章类型
     */
    delete_annoutment?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改系统公告
     */
    update_annoutment?(condition: Annoutment): NullablePromise<boolean | undefined> {
        return undefined;
    }
}