import { NullablePromise, DataList, addon } from "pao-aop";
export interface task {

}
@addon('ITaskService', '任务管理服务', '任务管理服务')
export class ITaskService {
    /**
     * 获取所有任务列表
     */
    get_all_task_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_all_task_list_manage?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_all_task_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_to_be_send_task_list_look_edit?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_to_be_processed_task_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_task_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    get_task_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取未分派的列表
     */
    get_to_be_send_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取待接收的任务
     */
    get_to_be_receive_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取任务类型列表
     */
    get_task_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取进行中的任务
     */
    get_ongoing_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 新增一个任务类型
     */
    update_task_type?(nursig: task): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /**
     * 获取未审核的列表
     */
    get_to_be_processed_task_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    /** 直接待分派 */
    add_new_receive_task?(nursig: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务审核通过 */
    task_examine_passed?(nursig: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务审核不通过 */
    task_examine_failed?(nursig: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除任务 */
    del_task?(nursig: task): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 修改/新增任务 */
    get_task_state?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 获取人员下拉列表 */
    input_person?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务类型 */
    input_task_type?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 任务紧急类型 */
    input_task_urgent?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除任务 */
    delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 指派任务 */
    task_appoint?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // 获取服务订单任务
    get_task_service_order_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<task> | undefined> {
        return undefined;
    }
    // 服务订单分派的接口
    change_task_service_order?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}