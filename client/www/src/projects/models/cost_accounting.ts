import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  费用结算
 */
export interface CostAccount {

}
/**  费用结算 */
@addon('ICostAccountService', '费用结算', '费用结算')
export class ICostAccountService {
    /**
     * 获取 费用结算列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_cost_account_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    get_cost_account_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    get_cost_account_list_manage?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 修改/新增费用结算 */
    update_cost_account?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除费用结算 */
    delete_cost_account?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 发送记录，生成账单 */
    cost_account_manage?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 应收款账单 */
    get_receivables_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 预收核算 */
    add_receivables_data?(user_id?: [], date?: []): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 发送账单 */
    send_receivables_data?(): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 审核账单 */
    check_bill?(condition?: [], param?: {}): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 实收核算 */
    add_actual_receivables_data?(user_id?: [], date?: []): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    get_actual_receivables_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    check_bill_actual?(condition?: [], param?: {}): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }
    /** 差额明细表 */
    get_difference_detail_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }

    /** 导出预收款 */
    bill_down?(condition?: {}): NullablePromise<DataList<CostAccount> | undefined> {
        return undefined;
    }

}