/*
 * @Author: your name
 * @Date: 2019-09-25 09:48:29
 * @LastEditTime : 2019-12-30 15:23:31
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \IntelligentElderlyCare\client\www\src\projects\models\service-operation.ts
 */

import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 服务商登记
 */
export interface ServiceProvider {
    /** id */
    id: string;
    /** 服务商Id */
    service_provider_id: string;
    /** 服务商类别Id */
    sp_type_id: string;
    /** 服务商能力级别 */
    sp_ability_level: string;
    /** 服务商信用级别 */
    sp_credit_level: string;
    /** 服务商资质信息 */
    sp_qualifications: string;
}
export interface ServiceType {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 服务描述 */
    service_des?: string;
    /** 服务类别 */
    service_type?: string;
    /** 服务预订页面 */
    service_book_view?: string;
    /** 服务确认页面 */
    service_confirm_view?: string;
    /** 服务登记页面 */
    service_register_view?: string;
    /** 服务计价页面 */
    service_valuation_view?: string;
    /** 服务人员页面 */
    service_person_view?: string;
    /** 服务接受人页面 */
    service_receiver_view?: string;
    /** 服务评价页面 */
    service_evaluate_view?: string;
    /** 服务计价公式 */
    service_valuation_formula?: string;
    /** 服务绩效计算公式 */
    service_achievements_formula?: string;
    /** 备注 */
    remark: string;
}
export interface ServiceItem {
    /** id */
    id: string;
    /** 服务商ID */
    service_provider_id: string;
    /** 服务类型Id */
    service_type_id: string;
    /** 服务项目参数 */
    service_item_param: string;
}
export interface ServiceOrder {
    /** id */
    id: string;
    /** 采购人 */
    purchaser: string;
    /** 服务商 */
    service_provider: string;
    /** 订单时间 */
    order_date: string;
    /** 服务时间 */
    service_date?: string;
    /** 付款时间 */
    pay_date?: string;
    /** 交易编号 */
    transation_code: string;
    /** 状态 */
    status?: string;
    /** 备注 */
    remark?: string;
}
export interface ServiceEvalute {
    /** id */
    id: string;
    /** 服务商Id */
    service_item_id?: string;
    /** 评分等级 */
    score_level?: string;
    /** 内容 */
    evaluate_content?: string;
}

export interface ServiceWoker {
    /** id */
    id: string;
    /** 服务人员id */
    user_id?: string;
    /** 服务项目清单 */
    aera_dispose?: any;
}

/*服务人员申请 */
export interface ServicePersonalApply {
    /** 姓名 */
    servicer_name: string;
    /** 性别 */
    servicer_sex: string;
    /** 年龄 */
    servicer_age: string;
    /** 家庭住址 */
    servicer_address: string;
    /** 手机号码 */
    servicer_phone: string;
    /** 服务人员能力级别 */
    servicer_ability_level: string;
    /* 服务人员信用级别 */
    servicer_credit_level: string;
    /* 资质(资格证)照片 */
    servicer_qualifications_photos: any;
    /* 合同照片 */
    servicer_contract_photos: any;
    /* 服务机构合同条款 */
    servicer_contract_clauses: string;
    /* 备注 */
    servicer_remark: string;
}
/**
 * 服务人员
 */
export interface ServicePersonal {

}
@addon('IServiceOperationService', '服务商服务接口', '服务商服务接口')
export class IServiceOperationService {

    /**
     * 获取服务商列表
     */
    get_service_provider_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 获取当前用户服务商列表
     */
    get_current_service_provider?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 新增/修改服务商信息
     */
    update_service_provider?(serviceProvider: ServiceProvider): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 删除服务商
     */
    del_service_provider?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 查询服务类型列表
     */
    get_service_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceType> | undefined> {
        return undefined;
    }

    /**
     * 查询服务类型树形结构列表
     */
    get_service_type_list_tree?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 新增/修改服务类型
     */
    update_service_type?(serviceType: ServiceType): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 删除服务类型
     */
    del_service_type?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 查询服务项目列表
     */
    get_service_item_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceItem> | undefined> {
        return undefined;
    }
    /**
     * 新增/修改服务项目
     */
    update_service_item?(serviceItem: ServiceItem): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 删除服务项目
     */
    del_service_item?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 查询服务订单列表
     */
    get_service_order_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /**
     * 查询服务订单列表--平台运营服务订单
     */
    get_service_order_fwyy_fwdd_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /** 查询服务订单分派列表接口 */
    get_service_order_fwddfp_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /**
     * 查询服务订单列表
     */
    get_service_order_list_al?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /**
     * 新增/修改服务订单
     */
    update_service_order?(serviceOrder: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 删除服务订单 */
    delete_service_order?(service_order_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 查询服务评价列表
     */
    get_service_evaluate_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceEvalute> | undefined> {
        return undefined;
    }
    /**
     * 查询服务回访列表
     */
    get_service_return_visit_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceEvalute> | undefined> {
        return undefined;
    }
    /**
     * 更新服务回访
     */
    update_service_return_visit?(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 更新服务回访
     */
    update_service_return_visit_record?(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 查询服务包列表
     */
    get_service_item_package?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除服务包
     */
    del_service_item_package?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 服务提供者登记信息
     */
    get_service_woker_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceWoker> | undefined> {
        return undefined;
    }
    service_woker_update?(ServiceWoker: ServiceWoker): NullablePromise<boolean | undefined> {
        return undefined;
    }

    service_woker_delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 生成服务记录接口
     */
    add_service_record?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 查询服务人员登记列表
     */
    get_service_personal_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicePersonal> | undefined> {
        return undefined;
    }
    /**
     * 服务人员申请
     */
    update_service_personal_apply?(service_personal_apply: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 审核服务人员申请
     */
    subsidy_service_personal_apply?(reviewed_result: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 新增/修改服务人员
     */
    update_service_personal?(service_personal: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 删除服务人员 */
    del_service_personal?(service_personal_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 服务结算 */
    update_service_settlement(service_settlement: any): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 审核服务商申请 */
    update_service_provider_record(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取待审核服务商列表
     */
    get_Subsidy_service_provider_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 工作人员统计
     */
    get_work_people_statistics?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 总体统计
     */
    get_all_statistics?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 查询服务计划列表
     */
    get_service_plan_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }
    /**
     * 查询服务计划列表
     */
    get_service_plan_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }

    /**
     * 新增/修改服务计划
     */
    update_service_plan?(serviceOrder: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 删除服务订单 */
    delete_service_plan?(service_order_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}