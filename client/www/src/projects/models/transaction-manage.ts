import { NullablePromise, DataList, addon } from "pao-aop";
export interface TransactionManage {
    /** 交易编号 */
    transaction_code?: string;
    /** 交易甲方 */
    party_a?: string;
    /** 交易乙方 */
    party_b?: string;
    /** 交易内容 */
    transaction_content?: string;
    /** 交易状态 */
    transaction_state?: string;
    /** 交易评价 */
    transaction_evaluate?: string;
    /** 备注 */
    transaction_remark?: string;
}

export interface TransactionComment {
    /** 经济主体 */
    user_name?: string;
    /** 交易编号 */
    transaction_id?: string;
    /** 交易内容 */
    transaction_comment?: string;
    /** 信用评分 */
    comment_score?: number;
}

export interface CommentList {
    /** 交易编号 */
    transaction_id?: string;
    /** 评论人 */
    comment_person?: string;
    /** 评价分数 */
    comment_score?: number;
    /** 评价内容 */
    comment_content?: string;
}
@addon('ITransactionService', '交易服务接口', '交易服务接口')
export class ITransactionService {
    /**
     * 获取需要发起的交易列表
     */
    get_transaction_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<TransactionManage>> {
        return undefined;
    }
    /**
     * 取消交易
     */
    cancel_transaction_by_id?(transaction_id?: string): NullablePromise<string> {
        return undefined;
    }
    /**
     * 恢复交易
     */
    return_transaction_by_id?(transaction_id?: any): NullablePromise<string> {
        return undefined;
    }
    /**
     * 删除交易
     */
    del_transaction_by_id?(transaction_id?: string): NullablePromise<string> {
        return undefined;
    }
    /**
     * 交易清单评论查询
     */
    query_transaction_comment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<TransactionComment>> {
        return undefined;
    }
    /**
     * 评论详情查询
     */
    query_comment_details_list?(tcondition?: {}, page?: number, count?: number): NullablePromise<DataList<CommentList>> {
        return undefined;
    }

}