import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  护理管理
 */
export interface nursingContent {
    /** 名称 */
    name?: string;
    /** 创建时间 */
    create_date?: string;
    /** 护理描述 */
    describe?: string;

}
/**  护理管理服务 */
@addon('INursingService', '护理管理服务', '护理管理服务')
export class nursingContentService {
    /**
     * 获取 护理内容列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_nursing_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<nursingContent> | undefined> {
        return undefined;
    }
    /** 修改/新增护理内容 */
    update_nursing_project?(assessment_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除护理内容 */
    delete_nursing_project?(assessment_project_ids: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

}