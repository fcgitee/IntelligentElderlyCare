import { NullablePromise, DataList, addon } from "pao-aop";

export interface service {

}

/**
 *  服务包
 */
export interface ServiceItemPackage {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 适用范围 */
    application_scope?: any[];
    /** 附属合同条款 */
    subsidiary_contract_clauses?: string;
    /** 备注 */
    remarks?: string;
    /** 服务项目清单 */
    service_items?: any[];
    /** 所属组织机构 */
    organization?: string;

}
/**  服务包服务 */
@addon('IServiceItemPackage', '服务包服务', '服务包服务')
export class IServiceItemPackage {
    /**
     * 获取 服务包列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_item_package_pure?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_service_item_package?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_service_item_package_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_service_item_package_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /** 修改/新增服务包 */
    update_service_item_package?(service_item_package: {}): NullablePromise<any | undefined> {
        return undefined;
    }
    /** 删除服务包 */
    delete_service_item_package?(service_item_package_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_service_product_list?(service_item_package_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

}
@addon('IServicePackageListService', '服务包服务', '服务包服务')
export class IServicePackageListService {
    /**
     * 获取服务套餐列表
     */
    get_service_product_package_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    get_service_product_package_all?(condition?: {}): NullablePromise<DataList<service>> {
        return undefined;
    }
}

@addon('IServiceProductDetailService', '服务详情', '服务详情')
export class IServiceProductDetailService {
    /**
     * 获取服务详情
     */
    get_service_package_detail?(condition?: string): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 获取服务套餐详情
     */
    get_service_product_package_detail(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 获取产品列表
     */
    get_service_product_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 获取服务产品/套餐列表 */
    get_service_product_item_package_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 获取服务产品/套餐列表 */
    get_product_package_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
}

@addon('IComfirmOrderService', '确认订单服务', '确认订单服务')
export class IComfirmOrderService {
    /**
     * 获取购买者信息
     */
    get_purchaser_detail?(condition?: {}): NullablePromise<DataList<service>> {
        return undefined;
    }
    // /** 创建订单 */
    // add_comfirm_order?(condition?: {}): NullablePromise<DataList<service>> {
    //     return undefined;
    // }
    /** 创建订单 */
    create_product_order?(condition?: {}, is_paying_first?: string): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 创建订单 */
    batch_create_order?(condition?: {}, price?: string): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 编辑预定服务
     * @param condition 条件
     */
    update_reserve_service?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询预定服务
     * @param condition 查询条件
     */
    get_reserve_service?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 删除预定服务 */
    delete_reserve_service?(condition: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 一键下单 */
    one_key_order?(item_list: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取日托订单列表
     */
    get_rt_order_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<service>> {
        return undefined;
    }
    /**
     * 编辑日托订单
     */
    update_rt_order(condition?: {}): NullablePromise<DataList<service>> {
        return undefined;
    }
    /** 全部长者下单 */
    all_buy_order?(): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 重置服务记录 */
    reset_record?(item_list: any): NullablePromise<DataList<any>> {
        return undefined;
    }
}