import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  服务选项
 */
export interface ServiceOption {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 选项值类型 */
    option_value_type?: string;
    /** 默认值 */
    default_value?: string;
    /** 是否允许修改 */
    is_modification_mermissible?: string;
    /** 备注 */
    remarks?: string;
    /** 选项内容 */
    option_content?: any[];

}
/**  服务选项服务 */
@addon('IServicrOptionService', '服务选项服务', '服务选项服务')
export class IServicrOptionService {
    /**
     * 获取 服务选项列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_servicre_option_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOption> | undefined> {
        return undefined;
    }
    get_servicre_option_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOption> | undefined> {
        return undefined;
    }
    get_servicre_option_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOption> | undefined> {
        return undefined;
    }
    /** 修改/新增服务选项 */
    update_servicre_option?(service_option: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务选项 */
    delete_servicre_option?(service_option_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}