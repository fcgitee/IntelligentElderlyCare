import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  水电抄表
 */
export interface Hydropower {
    /** id */
    id: string;
    /** 长者id */
    elder?: string;

}
/**  医院管理 */
@addon('IHydropowerService', '水电抄表', '水电抄表')
export class IHydropowerService {
    /**
     * 获取 水电抄表列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_hydropower_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    get_hydropower_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    get_hydropower_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    /** 修改/新增水电抄表 */
    update_hydropower?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除需求记录 */
    delete_hydropower?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 九江颐养院 电费查询 */
    jiujiang_update_hydropower?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    get_jiujiang_hydropower_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    get_jiujiang_hydropower_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    delete_jiujiang_hydropower?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    upload_hydropower?(condition: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    export_template?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    update_water_electricity(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_water_electricity?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hydropower> | undefined> {
        return undefined;
    }
    delete_water_electricity?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}