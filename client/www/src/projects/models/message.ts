import { NullablePromise, DataList, addon } from "pao-aop";
export interface IMessage {

}
@addon('IMessageService', '信息管理服务', '信息管理服务')
export class IMessageService {
    /**
     * 获取所有信息
     */
    get_all_message_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    /**
     * 获取未读的信息
     */
    get_unread_message_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    /**
     * 获取信息类型列表
     */
    get_message_type_list(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    /**
     * 保存一个信息
     */
    add_new_message?(data: IMessage): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    /**
     * 设置信息为已读
     */
    set_message_already_read?(data: IMessage): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    /**
     * 保存一个信息类型
     */
    add_new_message_type?(data: IMessage): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    // 在线咨询消息
    get_app_online_message_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    // 在线咨询消息
    update_app_online_message?(condition?: {}): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
    // 更新备注
    update_app_online_message_remark?(condition?: {}): NullablePromise<DataList<IMessage> | undefined> {
        return undefined;
    }
}