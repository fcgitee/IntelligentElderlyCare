/** 模拟所有权限 */
export let permission = [
    { id: 'see-regional-user-management', name: '区域用户管理查看权限' },
    { id: 'edit-regional-user-management', name: '区域用户管理编辑权限' },
    { id: 'see-this-user-management', name: '本组织机构查看权限' },
    { id: 'edit-this-user-management', name: '本组织机构编辑权限' },
];