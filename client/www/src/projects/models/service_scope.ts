import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  服务适用范围
 */
export interface ServiceScope {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 编号 */
    number?: string;
    /** 备注 */
    remarks?: string;

}
/**  服务适用范围服务 */
@addon('IServiceScopeService', '服务适用范围服务', '服务适用范围服务')
export class IServiceScopeService {
    /**
     * 获取 服务适用范围列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_scope_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceScope> | undefined> {
        return undefined;
    }
    get_service_scope_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceScope> | undefined> {
        return undefined;
    }
    get_service_scope_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceScope> | undefined> {
        return undefined;
    }
    /** 修改/新增服务适用范围 */
    update_service_scope?(service_scope: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务适用范围 */
    delete_service_scope?(service_scope_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}