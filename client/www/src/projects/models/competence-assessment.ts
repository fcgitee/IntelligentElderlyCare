import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  能力评估
 */
export interface CompetenceAssessment {
    [x: string]: any;
    /** id */
    id: string;
    /** 长者id */
    elder?: string;
    /** 模板 */
    template?: string;
    /** 项目列表 */
    project_list?: any[];
    /** 总分 */
    total_score?: any[];
    /** 用户名 */
    user_name?: any[];

}
/**  能力评估服务 */
@addon('ICompetenceAssessmentService', '能力评估服务', '能力评估服务')
export class ICompetenceAssessmentService {
    /**
     * 获取 能力评估列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_competence_assessment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    get_competence_assessment_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    get_competence_assessment_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    get_assessment_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    get_assessment_project_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /** 修改/新增能力评估 */
    update_competence_assessment?(competence_assessment: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除能力评估 */
    delete_competence_assessment?(competence_assessment_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 护理等级设置
     */
    /**
     * 
     * @param condition 
     * @param page 
     * @param count 
     */
    get_nursing_grade_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 新增/编辑护理等级
     * @param condition 
     */
    update_nursing_grade(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 
     * @param ids 
     */
    delete_nursing_grade?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取护理内容列表
     */
    get_nursing_content_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /**
     * 新增/编辑护理内容列表
     */
    update_nursing_content?(condition?: {}): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /**
     * 删除护理内容列表
     */
    delete_nursing_content?(condition?: {}): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /**
     * 新增/编辑护理记录
     */
    update_nursing_record?(condition?: {}): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /**
     * 获取护理记录
     */
    get_nursing_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
    /**
     * 删除护理记录
     */
    delete_nursing_record?(condition?: {}): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }

    /* 导入 */
    import_pinggu_record?(condition?: {}): NullablePromise<DataList<CompetenceAssessment> | undefined> {
        return undefined;
    }
}