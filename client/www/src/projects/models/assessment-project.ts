import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  能力评估模板
 */
export interface AssessmentProject {
    /** id */
    id: string;
    /** 状态 */
    state?: string;
    /** 项目名称 */
    project_name?: string;
    /** 评估类型 */
    assessment_type?: string;
    /** 选择框类型 */
    selete_type?: string;
    /** 记分方式 */
    scoring_method?: string;
    /** 描述 */
    describe?: string;
    /** 创建日期 */
    creation_date?: string;
    /** 选项内容 */
    select_content?: any[];

}
/**  能力评估模板服务 */
@addon('IAssessmentProjectService', '能力评估模板服务', '能力评估模板服务')
export class IAssessmentProjectService {
    /**
     * 获取 评估项目列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_assessment_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    get_assessment_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    get_assessment_template?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    get_assessment_project_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    get_assessment_project_list_all_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    /** 修改/新增评估项目 */
    update_assessment_project?(assessment_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除评估项目 */
    delete_assessment_project?(assessment_project_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 护理项目 */
    get_nursing_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    get_nursing_project_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<AssessmentProject> | undefined> {
        return undefined;
    }
    delete_nursing_project?(assessment_project_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

}