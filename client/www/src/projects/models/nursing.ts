import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 护理级别档案
 */
export interface Nursing {
    /** 档案id */
    id: string;
    /** 序号 */
    number: string;
    /** 护理类别 */
    nursing_type: string;
    /** 说明 */
    Description: string;
    /** 费用 */
    fee: string;
    /** 状态 */
    state: string;

}
/** 护理管理服务 */
@addon('INursingService', '护理管理服务', '护理管理服务')
export class INursingService {
    /**
     * 获取护理档案列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    get_nursing_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Nursing> | undefined> {
        return undefined;
    }
    /** 修改/新增护理档案 */
    update?(nursig: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除护理档案 */
    delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 护理类型服务
     */
    /** 护理类型列表 */
    get_nursing_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<INursingType> | undefined> {
        return undefined;
    }
    /** 修改/新增护理类型 */
    nursing_type_update?(nursig: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除护理类型 */
    nursing_type_delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 护理关系
     */

    /** 护理类型列表 */
    get_nursing_relationship_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<INursingType> | undefined> {
        return undefined;
    }
    /** 删除护理关系 */
    nursing_relationship_delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 新建护理关系 */
    nursing_relationship_update?(nursig: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}

/**
 * 护理项目档案
 */
export interface INursingItem {
    /** 档案id */
    id: string;
    /** 序号 */
    number: string;
    /** 护理类别 */
    nursing_type: string;
    /** 说明 */
    Description: string;
    /** 费用 */
    fee: string;
    /** 状态 */
    state: string;

}

/** 护理项目服务 */
@addon('INursingItemService', '护理项目服务', '护理项目服务')
export class INursingItemService {
    /**
     * 获取护理档案列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 椰树
     */
    get_nursing_item_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<INursingItem> | undefined> {
        return undefined;
    }
    /** 修改/新增用户 */
    update?(nursig: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除用户 */
    delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}

/**
 * 护理类型
 */
export interface INursingType {
    /** 档案id */
    id: string;
    /** 编号 */
    number: string;
    /** 护理名称 */
    name: string;
    /** 备注 */
    Description: string;
    /** 费用 */
    Option: string;

}
