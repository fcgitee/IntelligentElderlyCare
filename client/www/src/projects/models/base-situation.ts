
import { addon, DataList, NullablePromise } from "pao-aop";

/**
 *  基本情况统计
 */
export interface IBaseSituation {
    /** id */
    id: string;
    /** 镇街 */
    stree?: string;
    /** 幸福院名称 */
    hapiness_name?: string;
    /** 村（局）幸福院负责人 */
    manager?: string;
    /** 设立时间 */
    build_time?: string;
    /** 星级 */
    start?: string;
    /** 服务机构 */
    ser_org?: string;
    /** 占地面积（ */
    cover_area?: string;
    /** 建筑面积 */
    build_area?: string;
    /** 覆盖社区 */
    cover_zoom?: string;
    /** 建设方式 */
    build_type?: string;
    /** 办公电话 */
    tel?: string;
    /** 地址 */
    address?: string;
    /** 功能室 */
    fn_room?: string;
}
/**  基本情况统计服务 */
@addon('IBaseSituationService', '基本情况统计服务', '基本情况统计服务')
export class IBaseSituationService {
    /**
     * 获取 基本情况统计列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_base_situation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_base_situation_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IBaseSituation>> {
        return undefined;
    }
    get_base_situation_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<IBaseSituation>> {
        return undefined;
    }
}