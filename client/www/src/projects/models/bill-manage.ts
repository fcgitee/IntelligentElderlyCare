import { NullablePromise, DataList, addon } from "pao-aop";
export interface BillManage {
    /** 单据编号 */
    bill_code?: string;
    /** 单据类型 */
    bill_type?: string;
    /** 发起人 */
    initiator?: string;
    /** 关联方 */
    related_parties?: string;
    /** 更改内容 */
    body_mes?: any;
}
export interface signatureContent {
    id?: string;
    remark?: string;
}
@addon('IBillManageService', '单据服务接口', '单据服务接口')
export class IBillManageService {

    /**
     * 获取需要发起的单据列表
     */
    get_signature_bill_by_personId?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<BillManage> | undefined> {
        return undefined;
    }
    /**
     * 获取需要发起的单据列表
     */
    get_signature_bill_by_initiatorId?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<BillManage> | undefined> {
        return undefined;
    }
    /**
     * 审核通过
     */
    signature_bill_by_personId?(signatureContent?: signatureContent): NullablePromise<string | undefined> {
        return undefined;
    }
    /**
     * 拒审
     */
    refuse_bill_by_personId?(signatureContent?: signatureContent): NullablePromise<string | undefined> {
        return undefined;
    }
    get_signature_bill_by_personId_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<BillManage> | undefined> {
        return undefined;
    }
    get_signature_bill_by_personId_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<BillManage> | undefined> {
        return undefined;
    }
}