import { NullablePromise, DataList, addon } from "pao-aop";

export interface Annoutment {
}
@addon('IAppPageConfigService', 'APP页面设置接口', 'APP页面设置接口')
export class IAppPageConfigService {
    // 获取app页面设置列表
    get_app_page_config_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取app页面设置列表
    get_app_page_config_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑app页面设置
    update_app_page_config?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除app页面设置
    delete_app_page_config?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取app设置列表
    get_app_config_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑app设置
    update_app_config?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 查询同意 */
    get_app_privacy_agree_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 删除查询同意 */
    delete_app_privacy_agree?(condition: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}