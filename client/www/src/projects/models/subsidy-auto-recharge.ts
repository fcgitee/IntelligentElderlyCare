
import { NullablePromise, addon, DataList } from "pao-aop";

/**
 * 报表数据
 */
export interface Personnel {

}

/** 补贴自动充值控制服务 */
@addon('subsidyAutoRecharge', '补贴自动充值控制服务', '补贴自动充值控制服务')
export class subsidyAutoRecharge {
    /**
     * 获取补贴自动充值控制
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    /** 获取日志列表 */
    get_journal_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 启动/暂停服务 */
    update_service?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}