import { NullablePromise, DataList, addon } from "pao-aop";
export interface reservation {

}
@addon('IReservationService', '护理档案服务', '护理档案服务')
export class IReservationService {
    /**
     * 获取护理档案列表
     */
    get_nursing_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<reservation> | undefined> {
        return undefined;
    }
    /** 修改/新增用户 */
    update?(nursig: reservation): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除用户 */
    delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

}