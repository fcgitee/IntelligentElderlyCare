import { NullablePromise, DataList, addon } from "pao-aop";

@addon('IUserRelationShipService', '家庭档案', '家庭档案')
export class IUserRelationShipService {
    /**
     * 获取人员关系
     */
    get_user_relation_ship_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 解除人员关系
     */
    delete_user_relation_ship?(ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善人员信息
     */
    update_user_relation_ship?(data?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取人员关系类型
     */
    get_relationShip_type?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善关怀记录
     */
    update_care_record?(data?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取关怀记录
     */
    get_care_record?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取关怀类型
     */
    get_care_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善关怀类型
     */
    update_care_type?(data?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除关怀记录
     */
    del_care_record?(ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除关怀类型
     */
    del_care_type?(ids: any): NullablePromise<DataList<any>> {
        return undefined;
    }
}