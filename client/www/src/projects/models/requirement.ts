import { NullablePromise, DataList, addon } from "pao-aop";

export interface RequirementType {
    /** id */
    id: string;
    /** 名称 */
    name: string;
    /** 编号 */
    number: string;
    /** 备注 */
    remark: string;
}

export interface RequirementProject {
    /** id */
    id: string;
    /** 项目名称 */
    project_name: string;
    /** 需求类别id */
    requirement_type_id: string;
    /** 需求描述 */
    requirement_desc: string;
    /** 需求匹配公式 */
    requirement_match_func: string;
    /** 需求选项 */
    requirement_option_ids: string[];
    /** 所属组织机构id */
    organization_id: string;
}

/**
 * 需求选项
 */
export interface RequirementOption {
    /** id */
    id: string;
    /** 选项名称 */
    name: string;
    /** 选项内容 */
    option_content_ids: string[];
}

/**
 * 需求选项内容
 */
export interface RequirementOptionContent {
    /** 选项名称 */
    name: string;
    /** 选项内容 */
    option_content: any[];
    /** 权重 */
    weight: string;
}

@addon('IRequirementTypeService', '需求类型服务接口', '需求类型服务接口')
export class IRequirementTypeService {
    /**
     * 获取需求类型列表
     */
    get_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 删除需求类型
     */
    delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改需求类型
     */
    update?(serviceItem: RequirementType): NullablePromise<boolean | undefined> {
        return undefined;
    }
}

@addon('IRequirementProjectService', '需求项目服务接口', '需求项目服务接口')
export class IRequirementProjectService {
    /**
     * 获取需求项目列表
     */
    get_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 删除需求项目
     */
    delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改需求项目
     */
    update?(serviceItem: RequirementProject): NullablePromise<boolean | undefined> {
        return undefined;
    }
}

@addon('IRequirementOptionService', '需求项目选项服务接口', '需求项目选项服务接口')
export class IRequirementOptionService {
    /**
     * 获取需求项目选项列表
     */
    get_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /**
     * 删除需求项目选项
     */
    delete?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改需求项目选项
     */
    update?(serviceItem: RequirementProject): NullablePromise<boolean | undefined> {
        return undefined;
    }
}