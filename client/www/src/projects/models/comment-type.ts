import { NullablePromise, DataList, addon } from "pao-aop";

export interface CommentType {
    /** id */
    id: string;
    /** 名称 */
    name: string;
    /** 编号 */
    code: string;
    /** 备注 */
    remark: string;
}
@addon('ICommentTypeService', '评论类型服务接口', '评论类型服务接口')
export class ICommentTypeService {
    /**
     * 获取评论类型列表
     */
    get_comment_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_comment_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    get_comment_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除评论类型
     */
    delete_comment_type?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 新增/修改评论类型
     */
    update_comment_type?(comment_type: CommentType): NullablePromise<boolean | undefined> {
        return undefined;
    }
}