import { NullablePromise, DataList, addon } from "pao-aop";

export interface ServiceLedger {
    /** id */
    id: string;
    /** 台账人员 */
    user_id?: string;
    /** 台账日期 */
    date?: string;
    /** 服务项目 */
    service_item?: { id: string, amount: number }[];
    /** 总金额 */
    total_amount?: number;
}
/**  台账服务 */
@addon('IServicesLedgerService', '台账服务', '台账服务')
export class IServicesLedgerService {
    /**
     * 获取 服务台账列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_ledger_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceLedger> | undefined> {
        return undefined;
    }
    /** 修改/新增服务台账 */
    update_services_ledger?(services_ledger: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务台账 */
    delete_services_ledger?(services_ledger_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}