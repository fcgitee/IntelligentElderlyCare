import { NullablePromise, addon } from "pao-aop";

/**
 * 用户
 */
export interface Room {
    /** id */
    id: string;
    /** 楼宇 */
    building: string;
    /** 房间号 */
    room_number: string;
    /** 费用 */
    fee: string;
    /** 入住人数 */
    living_population: string;
    /** 房间类型 */
    room_type: string;
    /** 备注 */
    remark: string;

}
export class DataList<T> {
    result?: T[];
}

/** 通用服务 */
@addon('IRoomService', '房间服务接口', '房间服务接口')
export class IRoomService {
    /**
     * 查询
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    query?(col_name: String, condition?: {}, page?: number, count?: number): NullablePromise<DataList<Room> | undefined> {
        return undefined;
    }

    /**
     *  新增房间
     * @param col_name  数据库表名
     */
    insert?(col_name: String, condition?: {}): NullablePromise<object | undefined> {
        return undefined;
    }
    delete?(col_name: String, id?: []): NullablePromise<object | undefined> {
        return undefined;
    }
}