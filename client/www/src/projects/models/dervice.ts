import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  设备
 */
export interface Device {

}
/**  设备 */
@addon('IDeviceService', '设备', '设备')
export class IDeviceService {
    /**
     * 获取 设备列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_device?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Device>> {
        return undefined;
    }
    get_device_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Device>> {
        return undefined;
    }
    get_device_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Device>> {
        return undefined;
    }
    /** 绑定设备 */
    update_device?(condition?: any): NullablePromise<any> {
        return undefined;
    }
    /** 更新设备信息 */
    update_device_weilan?(condition?: any): NullablePromise<any> {
        return undefined;
    }
    /** 解除绑定设备 */
    delete_device?(dervice_id: string): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取设备日志 */
    get_device_log?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取设备警报 */
    get_device_message_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取设备期限 */
    get_device_term_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 设备导入 */
    import_device?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取设备定位
    get_device_location?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    // 获取设备足迹
    get_device_footprint?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
}