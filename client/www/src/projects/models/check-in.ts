import { addon, DataList, NullablePromise } from "pao-aop";

/**
 * 入住登记管理
 */
export interface checkIn {
    /** id */
    id: string;
    /** 长者用户id */
    user_id: string;
    /** 入住时间 */
    /** 入住人 */
    name: string;

}

@addon('CheckInService', '入住登记服务', '入住登记服务')
export class CheckInService {

    /** 
     * 入住登记管理 
     */

    /** 列表 */
    get_check_in_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    get_check_in_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    check_if_pay_done?(condition?: {}): NullablePromise<any> {
        return undefined;
    }
    get_check_in_list_room_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    get_check_in_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    /** 删除 */
    delete_check_in?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 新增入住 */
    add_check_in?(elder?: {}, evaluation_info?: {}, service_item?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 新增入住 */
    update_check_in?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 导出 */
    export_excel?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 待入住列表 */
    get_wait_checkin_elder_list?(): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    /** 入住办理 */
    add_check_in_special?(service_item?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 入住办理列表 */
    check_in_alone_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    /** 入住办理列表 */
    get_check_in_list_details?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
    check_in_alone_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<checkIn> | undefined> {
        return undefined;
    }
     /** 修改入住时间 */
     update_create_date?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}