/*
 * @Description: In User Settings Edit
 * @Author: 陈旭霖
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-09-29 22:54:15
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, addon } from "pao-aop";

@addon('IAccommodationProcessService', '住宿过程', '住宿过程')
export class IAccommodationProcessService {
    /**
     * 调房
     */
    change_room_process?(data?: {}): NullablePromise<undefined> {
        return undefined;
    }
    /**
     * 一键生成服务记录
     */
    auto_creat_service_record?(): NullablePromise<undefined> {
        return undefined;
    }
    /** 退住 */
    check_out_process?(user_id?: string): NullablePromise<undefined> {
        return undefined;
    }
    /** 生成服务记录 */
    auto_creat_service_record_manage?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
}