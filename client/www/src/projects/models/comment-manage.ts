import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  评论管理
 */
export interface Comment {
    /** id */
    id: string;
    /** 评论对象 */
    comment_object_id?: string;
    /** 父级评论 */
    father_comment_id?: string;
    /** 评论类型 */
    type?: string;
    /** 评论用户 */
    comment_user_id?: string;
    /** 评论内容 */
    content?: string;
    /** 评论等级 */
    level?: string;
    /** 评论时间 */
    comment_date?: string;
    /** 评论审核时间 */
    audit_date?: string;
    /** 评论审核者 */
    audit_user_id?: string;
    /** 评论状态 */
    audit_status?: string;
    /** 点赞清单 */
    like_list?: any[];
    /** 审核意见 */
    audit_idea?: string;
}
export interface commentContent {
    id?: string;
    remark?: string;
}
/**  评论管理服务 */
@addon('ICommentManageService', '评论管理服务', '评论管理服务')
export class ICommentManageService {
    /**
     * 获取 评论列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_comment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_comment_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_comment_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    /** 新增/修改评论 */
    update_comment?(comment: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除评论 */
    delete_comment?(comment_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /**
     * 获取需要审核的评论列表
     */
    get_comment_audit_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_comment_audit_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_comment_audit_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    // /**
    //  * 获取需要审核的评论列表
    //  */
    // get_comment_audit_by_initiatorId?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
    //     return undefined;
    // }
    /**
     * 评论审核通过
     */
    comment_audit_success?(commentContent?: commentContent): NullablePromise<string | undefined> {
        return undefined;
    }
    /**
     * 审核不通过
     */
    comment_audit_fail?(commentContent?: commentContent): NullablePromise<string | undefined> {
        return undefined;
    }

}