import { NullablePromise, DataList, addon } from "pao-aop";

@addon('IHealthCareService', '家庭档案', '家庭档案')
export class IHealthCareService {
    /**
     * 新增/完善药品档案
     */
    update_medicine_file?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取药品档案列表
     */
    get_medicine_file_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除药品档案
     */
    del_medicine_file?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善疾病档案
     */
    update_disease_file?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取疾病档案列表
     */
    get_disease_file_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除疾病档案
     */
    del_disease_file?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善过敏档案
     */
    update_allergy_file?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 获取过敏档案列表
     */
    get_allergy_file_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除疾病档案
     */
    del_allergy_file?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善意向回访
     */
    update_visit_question?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询意向回访
     */
    get_visit_question_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除意向回访
     */
    del_visit_question?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增/完善押金结算设置
     */
    update_deposit_setting?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询押金结算设置
     */
    get_deposit_setting_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除押金结算设置
     */
    del_deposit_setting?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
}