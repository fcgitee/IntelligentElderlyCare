
import { addon, DataList, NullablePromise } from "pao-aop";

/**
 *  服务记录
 */
export interface ServiceRecord {
    /** id */
    id: string;
    /** 服务订单 */
    service_order_id?: string;
    /** 服务项目 */
    service_project_id?: string;
    /** 服务开始时间 */
    service_begin_date?: string;
    /** 服务完成时间 */
    service_finish_date?: string;
    /** 服务人员 */
    service_worker_id?: string;
    /** 服务内容 */
    service_content?: string;
    /** 现场照片 */
    scene_photo?: string;
    /** 文字记录 */
    character_record?: string;
    /** 当前状态 */
    status?: string;
    /** 备注 */
    remark?: string;
    /** 选项清单 */
    option_list?: any[];
    /** 评价列表 */
    evaluate_list?: any[];

}
/**  服务记录服务 */
@addon('IServiceRecordService', '服务记录服务', '服务记录服务')
export class IServiceRecordService {
    /**
     * 获取 服务记录列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    get_service_record_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    get_elder_service_record_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    get_service_record_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    /** 查询服务记录的服务评价列表 */
    get_record_evaluate_list?(services_record?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    /** 查询服务记录的选项列表 */
    get_record_option_list?(services_record?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord>> {
        return undefined;
    }
    /** 修改/新增服务记录 */
    update_service_record?(services_record: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务记录 */
    delete_services_reocrd?(services_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 发送费用记录 */
    send_record?(services_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 服务商结算 */
    provider_settlement?(service_provider_settlement: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 服务人员结算 */
    personal_settlement?(service_personal_settlement: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 居家养老服务对象月报表 */
    get_servicer_report_month?(condition?: {}): NullablePromise<any> {
        return undefined;
    }
    /** 订单统计报表 */
    get_order_report?(condition?: {}, page?: number, count?: number): NullablePromise<any> {
        return undefined;
    }
    /** 订单统计 */
    get_order_statistics?(condition?: {}, page?: number, count?: number): NullablePromise<any> {
        return undefined;
    }
    // app工单核算
    get_app_order_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord> | undefined> {
        return undefined;
    }
    // 费用统计
    get_cost_statistics?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceRecord> | undefined> {
        return undefined;
    }
}