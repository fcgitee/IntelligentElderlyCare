import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  行为能力评估项目
 */
export interface Project {
    /** id */
    id: string;
    /** 项目名称 */
    project_name?: string;
    /** 一级类别 */
    category_i?: string;
    /** 二级类别 */
    category_ii?: string;
}
/**  行为能力评估项目服务 */
@addon('IBehavioralCompetenceAssessmentService', '行为能力评估项目服务', '行为能力评估项目服务')
export class IBehavioralCompetenceAssessmentService {
    /**
     * 获取 行为能力评估项目列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Project> | undefined> {
        return undefined;
    }
    /** 修改/新增用户 */
    update_project?(Project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除用户 */
    delete_project?(ProjectIDs: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}