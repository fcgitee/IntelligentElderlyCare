import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  预约登记
 */
export interface ReservationRegistration {
    /** id */
    id: string;
    /** 名称 */
    name?: string;

}
/**  预约登记服务 */
@addon('IReservationRegistrationService', '预约登记服务', '预约登记服务')
export class IReservationRegistrationService {
    /**
     * 获取 预约登记列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_reservation_registration_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ReservationRegistration>> {
        return undefined;
    }
    get_reservation_registration_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ReservationRegistration>> {
        return undefined;
    }
    get_reservation_registration_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ReservationRegistration>> {
        return undefined;
    }
    /** 修改/新增预约登记 */
    update_reservation_registration?(reservation_registration: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除预约登记 */
    delete_reservation_registration?(reservation_registration_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}