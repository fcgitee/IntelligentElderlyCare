import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  设备
 */
export interface Monitor {

}
/**  设备 */
@addon('IMonitorService', '监控', '监控')
export class IMonitorService {
    /**
     * 获取 监控列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_monitor_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Monitor>> {
        return undefined;
    }
    get_monitor_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Monitor>> {
        return undefined;
    }
    get_monitor_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Monitor>> {
        return undefined;
    }
    /** 新增/修改监控 */
    update_monitor?(condition?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除监控 */
    delete_monitor?(monitor_id: string): NullablePromise<boolean> {
        return undefined;
    }
}