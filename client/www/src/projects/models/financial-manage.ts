/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Wednesday July 24th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Wednesday, 24th July 2019 11:47:10 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、财务管理接口
 */
import { NullablePromise, DataList, addon } from "pao-aop";
export interface Fiancialaccount {
    /** 开始时间 */
    start_date?: string;
    /** 结束时间 */
    end_date?: string;
    /** 摘要 */
    abstract?: string;
    /** 账号 */
    account?: string;
}

export interface Fiancialaccountbook {
    /** 经济主体名称 */
    user_name?: string;
    /** 账簿类型 */
    type_book?: string;
    /** 设立日期 */
    set_date?: string;
    /** 上次结账日期 */
    settle_account_date?: string;
}

export interface Fiancialaccount {
    /** 账簿名称 */
    account_book_name?: string;
    /** 账户名称 */
    account_name?: string;
    /** 账户类型 */
    account_type?: string;
    /** 卡号 */
    card_number?: string;
    /** 开户单位 */
    account_open_unit?: string;
    /** 借/贷 */
    borrow_loan?: string;
    /** 余额 */
    balance?: string;
}
export interface Fiancialvoucher {
    /** 时间 */
    date?: string;
    /** 摘要 */
    abstract?: string;
    /** 凭证类型 */
    voucher_type?: string;
    /** 凭证编号 */
    voucher_id?: string;
    /** 借方科目 */
    borrow_subject?: string;
    /** 贷方科目 */
    loan_subject?: string;
    /** 金额 */
    amount?: string;
    /** 借/贷 */
    borrow_loan?: string;
    /** 余额 */
    balance?: string;
}
export interface Accountvoucher {
    /** 时间 */
    date?: string;
    /** 分录摘要 */
    entry_abstract?: string;
    /** 分录科目 */
    entry_subject?: string;
    /** 借/贷 */
    borrow_loan?: string;
    /** 金额 */
    amount?: string;
}

export interface returnOperartor {
    id?: string;
    reason?: string;
}

export interface inputData {
    key?: string;
    text?: string;
}

export interface divideRecord {
    organization_name?: string;
    account_type?: string;
    is_distributed?: string;
    divide_percent?: string;
    amount?: string;
    date?: string;
    financial_id?: string;
    abstract?: string;
}

export interface subsidyServiceProvider {
    organization_name?: string;
    amount?: string;
    date?: string;
    financial_id?: string;
    abstract?: string;
}

@addon('IFiancialService', '财务管理接口', '财务管理接口')
export class IFiancialService {
    /**
     * 获取账簿名称列表
     */
    input_account_book_list?(condition?: {}): NullablePromise<DataList<inputData>> {
        return undefined;
    }
    /**
     * 获取账户名称列表
     */
    input_account_list?(condition?: {}): NullablePromise<DataList<inputData>> {
        return undefined;
    }
    /**
     * 获取经济主体名称列表
     */
    input_user_name_list?(condition?: {}): NullablePromise<DataList<inputData>> {
        return undefined;
    }
    /**
     * 获取会计科目列表
     */
    input_subject_list?(condition?: {}): NullablePromise<DataList<inputData>> {
        return undefined;
    }
    /**
     * 获取账户流水列表
     */
    get_account_flow_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }
    get_account_flow_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }
    /**
     * 保存账户收款数据
     */
    save_account_receive_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 保存账户付款数据
     */
    save_account_payment_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 保存账户转账数据
     */
    save_account_transfer_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 红冲账户数据
     */
    return_account_data?(returnOperartor?: returnOperartor): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 获取账簿列表
     */
    get_account_book_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccountbook>> {
        return undefined;
    }
    get_account_book_list_manage?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccountbook>> {
        return undefined;
    }
    /**
     * 新增/修改账簿信息
     */
    update_account_book_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 获取账户列表
     */
    get_account_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }
    /**
     * 获取用户下的账户列表
     */
    get_user_account_sequence_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialaccount>> {
        return undefined;
    }
    /**
     * 修改多个账户信息
     */
    update_account_data_list?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 新增/修改账户信息
     */
    update_account_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 分类明细账（记账凭证）列表查询
     */
    get_voucher_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialvoucher>> {
        return undefined;
    }
    /**
     * 查看记账凭证
     */
    query_account_voucher_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Accountvoucher>> {
        return undefined;
    }
    /**
     * 新增记账凭证
     */
    add_account_voucher?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 红冲记账凭证
     */
    return_account_voucher?(returnOperartor?: returnOperartor): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 科目列表查询
     */
    get_subject_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Fiancialvoucher>> {
        return undefined;
    }
    /**
     * 删除科目
     */
    del_subject_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 新增/编辑会计科目
     */
    update_subject_data?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 查询收入分成记录列表
     * @param condition 
     */
    get_divide_record_list(condition?: {}): NullablePromise<DataList<divideRecord>> {
        return undefined;
    }

    /**
     * 获取服务商补贴账户账单
     * @param condition 
     */
    get_subsidy_service_provider_list(condition?: {}): NullablePromise<DataList<subsidyServiceProvider>> {
        return undefined;
    }
}