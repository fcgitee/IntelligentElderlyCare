import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  文章管理
 */
export interface Article {
    /** id */
    id: string;
    /** 文章标题 */
    title?: string;
    /** 文章副标题 */
    subhead?: string;
    /** 文类型 */
    type_id?: string;
    /** 文章描述 */
    description?: string;
    /** 文章内容 */
    content?: string;
    /** 文章web图片列表 */
    web_img_list?: any[];
    /** 文章app图片列表 */
    app_img_list?: any[];
    /** 文章附件清单 */
    accessory_list?: any[];
    /** 文章作者 */
    author?: string;
    /** 文章来源 */
    source?: string;
    /** 文章发布者 */
    promulgator_id?: any[];
    /** 发布时间 */
    issue_date?: string;
    /** 文章审核者 */
    auditor_id?: string;
    /** 审核时间 */
    audit_date?: string;
    /** 文章状态 */
    status?: string;
    /** 文章显示状态 */
    showing_status?: string;
    /** 备注 */
    remark?: string;

}
export interface articleContent {
    id?: string;
    remark?: string;
}
/**  文章服务 */
@addon('IArticleService', '文章服务', '文章服务')
export class IArticleService {
    /**
     * 获取 文章列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    /** 获取公告列表 */
    get_announcement_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_announcement_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_announcement_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    /** 新增/修改公告 */
    update_announcement?(announcement: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除公告 */
    delete_announcement?(announcement_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    // /** 获取草稿 */
    // get_article_draft?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
    //     return undefined;
    // }
    /** 获取新闻列表 */
    get_news_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_news_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_news_pure_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_news_pure_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    get_news_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Article> | undefined> {
        return undefined;
    }
    /** 新增/修改新闻 */
    update_news?(new_data: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除新闻 */
    delete_news?(new_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 获取需要审核的文章列表
     */
    get_article_audit_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_article_audit_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    get_article_audit_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Comment> | undefined> {
        return undefined;
    }
    /**
     * 文章审核通过
     */
    article_audit_success?(articleContent?: articleContent): NullablePromise<string | undefined> {
        return undefined;
    }
    /**
     * 审核不通过
     */
    article_audit_fail?(articleContent?: articleContent): NullablePromise<string | undefined> {
        return undefined;
    }

}