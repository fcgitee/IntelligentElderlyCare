/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-30 18:16:42
 * @LastEditTime: 2020-03-04 17:10:29
 * @LastEditors: Please set LastEditors
 */
import { addon, DataList, NullablePromise } from "pao-aop";

export interface PersonnelClassification {

}
/** 人员组织结构服务 */
@addon('IPersonOrgManageService', '人员组织结构服务', '人员组织结构服务')
export class IPersonOrgManageService {
    /**
     * 获取当前登录用户信息
     */
    get_current_user_info?(): NullablePromise<any[]> {
        return undefined;
    }
    /** 长者信息 */
    get_elder_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_all_elder_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 长者生日信息 */
    get_user_elder_birth_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 更新月收入统计 */
    update_month_salary?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 待入住的长者列表 */
    get_wait_checkin_elder_list(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_elder_list_by_org?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_elder_list_look?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 修改/新增长者 */
    update_elder_info?(personnel: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除长者 */
    delete_elder_info?(personnelIDs: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取人员 */
    get_personnel_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 新增/修改人员 */
    update_personnel_info?(personnel: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除人员 */
    delete_personnel_info?(personnelIDs: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /**
     * 获取全部用户列表
     */
    get_user_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_user_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_user_list_look?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取工作人员用户列表 */
    get_user_list_worker?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 月份统计 */
    get_month_salary_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_worker_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 新增/修改工作人员 */
    update_worker_info?(personnel: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除工作人员 */
    delete_worker_info?(personnelIDs: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取长者列表 */
    get_personnel_elder?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取长者列表，单纯长者表数据 */
    get_elder_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取工作人员列表 */
    get_personnel_worker?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 单纯获取工作人员列表 */
    get_worker_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表 */
    get_organization_list_all?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表（无权限过滤，只过滤范围） */
    get_organization_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_org_list?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取机构信息档案列表（无权限过滤，只过滤范围） */
    get_org_info_record_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 更新机构信息档案 */
    update_org_info_record?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取收费类型 */
    get_charge_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 更新收费类型 */
    update_charge_list?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 删除收费类型 */
    delete_charge_list?(condition?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取社区养老运营情况统计 */
    get_operate_sitation_statistic?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表（无权限过滤，只过滤范围） */
    get_current_org_info?(): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构树形列表 */
    get_organization_tree_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构树形列表 */
    get_organization_tree_list_forName?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表-新 */
    get_organization_list_new?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 删除组织机构-新 */
    del_organization?(ids?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取验收幸福院列表 */
    get_ys_xfy_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取幸福院组织机构树形列表 */
    get_organization_xfy_tree_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表 */
    get_organization_list_look?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取组织机构列表(不判断范围) */
    get_organization_all_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 新增/修改组织机构 */
    update_organization?(organization: {}): NullablePromise<any> {
        return undefined;
    }
    /** 新增/修改运营情况 */
    update_business_income?(organization: {}): NullablePromise<any> {
        return undefined;
    }
    /** 获取运营情况 */
    get_business_income?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 删除运营情况 */
    delete_business_income_byid?(organization: {}): NullablePromise<any> {
        return undefined;
    }
    /** 验收幸福院 */
    update_ys_xfy?(data: {}): NullablePromise<any> {
        return undefined;
    }
    /** 新增幸福院评比指标 */
    update_xfy_target_setting?(data: {}): NullablePromise<any> {
        return undefined;
    }
    /** 新增幸福院评比详细指标 */
    update_xfy_target_setting_details?(data: {}): NullablePromise<any> {
        return undefined;
    }
    /*  */
    /** 上架/下架机构 */
    update_org_show_app?(data: {}): NullablePromise<any> {
        return undefined;
    }
    /** 设置机构启用/停用 */
    update_org_start_stop?(data: {}): NullablePromise<any> {
        return undefined;
    }
    /** 删除幸福院评比指标 */
    del_xfy_target_setting?(ids: any): NullablePromise<any> {
        return undefined;
    }
    /** 删除幸福院评比详细指标 */
    del_xfy_target_setting_details?(ids: any): NullablePromise<any> {
        return undefined;
    }
    /** 幸福院评比详细指标excel导入 */
    upload_index_excel?(condition: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 获取幸福院评比指标列表 */
    get_xfy_target_setting_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取幸福院评比详细指标列表 */
    get_xfy_target_setting_details_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 新增/修改组织机构 */
    delete_organization?(ids: string[]): NullablePromise<any> {
        return undefined;
    }
    /** 获取当前组织机构信息 */
    get_cur_org_info(): NullablePromise<any> {
        return undefined;
    }
    // 获取人员类别
    get_personnel_classification_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 修改/新增人员类别 */
    update_personnel_classification?(PersonnelClassification: {}): NullablePromise<boolean> {
        return undefined;
    }
    // 删除人员类别
    delete_personnel_classification?(ids: string[]): NullablePromise<boolean> {
        return undefined;
    }
    /** 从excel导入 */
    import_excel_manage?(condition: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取组织机构 */
    get_all_organization_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 查询服务商下的订单 */
    getOrderByBussiness?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 创建草稿 */
    create_draft?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取草稿列表
    get_draft_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取长者健康列表
    get_elder_healthy_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取长者健康列表
    get_elder_healthy_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取长者用餐列表
    get_elder_food_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取长者用餐列表
    get_elder_food_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑长者用餐
    update_elder_food?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除用餐
    delete_elder_food?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取收费减免列表
    get_deduction_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取收费减免列表
    get_deduction_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑收费减免
    update_deduction?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除收费减免
    delete_deduction?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取排班列表
    get_set_class_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取排班类型列表
    get_set_class_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑排班类型
    update_set_class?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除排班类型
    delete_set_class?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取排班类型列表
    get_set_class_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取排班类型列表
    get_set_class_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑排班类型
    update_set_class_type?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除排班类型
    delete_set_class_type?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取护理收费列表
    get_nursing_pay_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取护理收费列表
    get_nursing_pay_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑护理收费
    update_nursing_pay?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除护理收费
    delete_nursing_pay?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }

    // 获取评比方案列表
    get_rating_plan_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取评比方案列表
    get_rating_plan_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑评比方案
    update_rating_plan?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除评比方案
    delete_rating_plan?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取评比列表
    get_rating_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取评比列表
    get_rating_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑评比
    update_rating?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除评比
    delete_rating?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取团体列表
    get_groups_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取团体列表
    get_groups_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取团队相关的志愿者列表
    get_groups_volunteer_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑团体
    update_groups?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除团体
    delete_groups?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取幸福院运营情况
    get_happiness_operation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取服务商情况
    get_servicer_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取机构情况
    get_organ_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取社区运营情况
    get_area_operation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取APP注册用户
    get_app_user_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 编辑app注册用户
    change_app_user?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 检查权限
    check_permission2?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 新增/修改个案服务情况 */
    update_service_situation?(organization: {}): NullablePromise<any> {
        return undefined;
    }
    /** 新增/修改探访服务情况 */
    update_visit_situation?(organization: {}): NullablePromise<any> {
        return undefined;
    }
    /** 获取个案服务情况列表 */
    get_service_situation_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 获取探访服务情况列表 */
    get_visit_situation_list?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 个案服务情况
    delete_service_situation_byid?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增情况总结
    update_situation_summary?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除长者
    delete_elder_info_byid?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除探访服务情况
    delete_visit_situation_byid?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增幸福院运营机构信息
    update_xfy_org_info?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 查询幸福院运营机构信息
    get_xfy_org_info?(condition?: any, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除幸福院运营机构信息
    delete_xfy_org_info?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 总体统计
    get_all_tj_info?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 综合统计-养老机构
    yljg_tj?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 综合统计-居家服务
    jjfws_tj?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取项目协议与报告
    get_deal_and_report_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取项目协议与报告列表
    get_deal_and_report_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑项目协议与报告
    update_deal_and_report?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除项目协议与报告
    delete_deal_and_report?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取年度计划与总结
    get_yearplan_and_summary_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取年度计划与总结列表
    get_yearplan_and_summary_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑年度计划与总结
    update_yearplan_and_summary?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除年度计划与总结
    delete_yearplan_and_summary?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取月度计划
    get_month_plan_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取月度计划列表
    get_month_plan_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑月度计划
    update_month_plan?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除月度计划
    delete_month_plan?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 民办非营机构运营资助
    get_org_operation_fund_list?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 民办非营机构床位资助
    get_org_bed_fund_list?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 新增机构运营情况记录
     */
    update_operation_situation?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询机构运营情况记录
     */
    get_operation_situation_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 删除机构运营情况记录
     */
    del_operation_situation?(id?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询长者饭堂记录
     */
    get_elder_meals_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 查询长者饭堂记录
     */
    get_elder_meals_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 更新长者饭堂
     */
    update_elder_meals?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询幸福小站
     */
    get_happiness_station_product_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 查询幸福小站
     */
    get_happiness_station_product_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 更新幸福小站
     */
    update_happiness_station_product?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除幸福小站
     */
    delete_happiness_station_product?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 查询年度自评
     */
    get_happiness_year_self_assessment_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 查询年度自评
     */
    get_happiness_year_self_assessment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    /**
     * 更新年度自评
     */
    update_happiness_year_self_assessment?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    /**
     * 删除年度自评
     */
    delete_happiness_year_self_assessment?(data?: any): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取消防设施
    get_fire_equipment_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取消防设施
    get_fire_equipment_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑消防设施
    update_fire_equipment?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除消防设施
    delete_fire_equipment?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 导入长者档案 */
    import_elder?(data?: {}): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取长者入住统计 */
    get_rzzzqktj_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取按平台统计数据 */
    get_apttj_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取按区统计数据 */
    get_aqtj_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取按镇街统计数据 */
    get_azjtj_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 获取工作人员情况统计 */
    get_cyryqktj_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    // 判断是否平台超管
    is_ptcg?(): NullablePromise<boolean> {
        return undefined;
    }
    /** 新增用户和当前账号关联的机构id记录 */
    user_org_link?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 查询用户关联机构数据 */
    user_org_link_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean> {
        return undefined;
    }
    /** 删除用户关联机构数据 */
    delete_user_org_link?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
}