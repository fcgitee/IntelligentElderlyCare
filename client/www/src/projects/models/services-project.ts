import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  住宿服务项目
 */
export interface ServicesProject {
    /** id */
    id: string;
    /** 名称 */
    name?: string;
    /** 是否标准服务 */
    is_standard_service?: string;
    /** 适用范围 */
    application_scope?: any[];
    /** 项目类别 */
    item_type?: string;
    /** 计算公式 */
    valuation_formula?: string;
    /** 服务介绍页面 */
    service_project_website?: string;
    /** 合同条款 */
    contract_clauses?: string;
    /** 所属组织机构 */
    organization?: string;
    /** 服务选项清单 */
    services_option?: any[];

}
/**  服务项目服务 */
@addon('IServicesProjectService', '服务项目服务', '服务项目服务')
export class IServicesProjectService {
    /**
     * 获取 服务项目列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_services_project_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    /**
     * 获取所有服务产品下所有的服务项目
     * @param condition 
     * @param page 
     * @param count 
     */
    get_all_item_from_product?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    get_services_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    get_services_project_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    /** 修改/新增服务项目 */
    update_services_project?(services_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务项目 */
    delete_services_project?(services_project_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 获取周期列表 */
    get_services_periodicity_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    /** 这个接口是特殊使用的，用来在后端把name转换成option_name的 */
    get_services_project_list2?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
    get_services_project_list_by_scope?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServicesProject> | undefined> {
        return undefined;
    }
}