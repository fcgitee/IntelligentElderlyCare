import { NullablePromise, DataList, addon } from "pao-aop";
export interface Charitable {

}
@addon('ICharitableService', '慈善管理服务', '慈善管理服务')
export class ICharitableService {
    /**
     * 获取募捐申请步骤
     */
    get_recipient_step?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善申请模板
     */
    get_charitable_apply_template_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善会资料
     */
    get_charitable_society_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善会资料
     */
    get_charitable_society_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 编辑慈善会资料
     */
    update_charitable_society?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取冠名基金
     */
    get_charitable_title_fund_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 编辑冠名基金
     */
    update_charitable_title_fund?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善汇总信息
     */
    get_charitable_information_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善捐款列表
     */
    get_charitable_donate_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增慈善捐款
     */
    update_charitable_donate?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善项目列表
     */
    get_charitable_project_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增编辑慈善项目
     */
    update_charitable_project?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取个人募捐申请列表
     */
    get_charitable_recipient_apply_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增个人募捐申请
     */
    update_charitable_recipient_apply?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取志愿者申请列表
     */
    get_volunteer_apply_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增编辑志愿者申请
     */
    update_volunteer_apply?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取志愿者服务类别列表
     */
    get_volunteer_service_category_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增编辑志愿者服务类别
     */
    update_volunteer_service_category?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善拨款列表
     */
    get_charitable_appropriation_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增慈善拨款
     */
    update_charitable_appropriation?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    get_all_Charitable_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    get_to_be_send_Charitable_list_look_edit?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    get_to_be_processed_Charitable_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    get_Charitable_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    get_Charitable_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取未分派的列表
     */
    get_to_be_send_Charitable_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取慈善类型列表
     */
    get_Charitable_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取进行中的慈善
     */
    get_ongoing_Charitable_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 新增一个慈善类型
     */
    update_Charitable_type?(nursig: Charitable): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /**
     * 获取未审核的列表
     */
    get_to_be_processed_Charitable_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Charitable> | undefined> {
        return undefined;
    }
    /** 直接待分派 */
    add_new_receive_Charitable?(nursig: Charitable): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 慈善审核通过 */
    Charitable_examine_passed?(nursig: Charitable): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 慈善审核不通过 */
    Charitable_examine_failed?(nursig: Charitable): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除慈善 */
    del_Charitable?(nursig: Charitable): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 修改/新增慈善 */
    get_Charitable_state?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 获取人员下拉列表 */
    input_person?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 慈善类型 */
    input_Charitable_type?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 慈善紧急类型 */
    input_Charitable_urgent?(): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除慈善 */
    delete?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 指派慈善 */
    Charitable_appoint?(nursingids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }

}