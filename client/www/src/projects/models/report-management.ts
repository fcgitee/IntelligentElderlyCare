/*
 * @Author: your name
 * @Date: 2019-11-08 09:21:19
 * @LastEditTime: 2020-02-21 14:57:13
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \IntelligentElderlyCare\client\www\src\projects\models\report-management.ts
 */

import { NullablePromise, addon, DataList } from "pao-aop";

/**
 * 报表数据
 */
export interface Personnel {

}

/** 报表数据查询服务 */
@addon('ReportManage', '报表数据查询服务', '报表数据查询服务')
export class ReportManage {
    /**
     * 获取报表列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 页数
     */
    /** 幸福院工作人员统计列表 */
    get_happiness_worker_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }

    /** 福利院工作人员统计列表 */
    get_welfare_worker_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 福利院长者入住统计 */
    get_check_in_elder_list?(condition?: {}, page?: number, count?: number): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 镇街结算当前账号服务商 */
    get_service_provider_settlement?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 获取服务商结算汇总 */
    get_service_provider_settlement_total?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 获取服务商服务记录明细统计 */
    get_service_provider_record_settlement?(condition?: {}, page?: number, count?: number): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 用于发送记录给服务商确认是否结算 */
    confirm_send?(service_provider_id: string, year_month: string, buy_type: string, service_product_id: string, social_worker_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }

    /** 服务商结算确认 */
    accounting_confirm?(service_provider_id: string, year_month: string, buy_type: string, service_product_id: string, social_worker_id: string): NullablePromise<boolean | undefined> {
        return undefined;
    }
}