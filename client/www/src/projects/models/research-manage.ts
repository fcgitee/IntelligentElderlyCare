/*
 * @Description: In User Settings Edit
 * @Author: 杨子毅
 * @Date: 2019-08-13 14:48:05
 * @LastEditTime: 2019-08-13 17:14:00
 * @LastEditors: Please set LastEditors
 */
import { NullablePromise, DataList, addon } from "pao-aop";

export interface ResearchEnter {
    /** 任务名称 */
    research_name: string;
    /** 任务创建人 */
    creater_name: string;
    /** 任务类型 */
    research_type: string;
    /** 开始日期 */
    date_begin: string;
    /** 结束日期 */
    date_end: string;
    /** 任务详情 */
    task_details: string;
    /** 任务指派 */
    task_assign: string; 
}
export interface ResearchInsert {
    /** 姓名 */
    ask_name: string; 
    /** 残疾证号 */
    card_number: string;
    /** 年龄 */
    age: string;
    /** 采集状态 */
    collection_state: string;
    /** 电话 */
    phone: string;
    /** 地址 */
    address: string;
    /** 调查方式 */
    research_type: string;
    /** 调查结果 */
    research_result: string;
    /** 是否国家建档立卡 */
    has_card: string;
    /** 是否低保 */
    subsistence_allowances: string;
    /** 是否大学生 */
    college_student: string;
    /** 是否危房改造 */
    dangerous_house: string;
    /** 是否就业 */
    has_work: string;
    /** 就业形式 */
    work_type: string;
    /** 就业形式 */
    unwork_reason: string;
    /** 职业技能培训 */
    vocational_skills_training: string;
    /** 农村实用技能培训 */
    practical_skills_training: string;
    /** 其他帮扶 */
    other_help: string;
    /** 困难残疾人生活补 */
    difficulty_help: string;
    /** 重度残疾人护理补 */
    seriously_help: string;
    /** 是否享受托养服务 */
    support_help: string;
    /** 动态更新年度是 */
    update_date: string;
    /** 五年内是否有过家 */
    has_newLife: string;
    /** 行政规划区名称 */
    administrative_region: string;
}
@addon('IResearchService', '补贴管理', '补贴管理')
export class IResearchService {
    /**
     * 查询调研任务列表
     */
    get_research_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ResearchEnter> | undefined> {
        return undefined;
    }
    /**
     * 新增申请
     */
    update_research?(research: ResearchEnter): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 新增调研数据
     */
    update_research_data?(research_data: ResearchInsert): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除调研任务
     */
    del_research?(ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}