import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  护理记录
 */
export interface nursingRecord {
    /** 名称 */
    name?: string;
    /** 创建时间 */
    create_date?: string;
    /** 护理描述 */
    describe?: string;

}
/**  护理记录服务 */
@addon('INursingService', '护理记录服务', '护理记录服务')
export class nursingRecordService {
    /**
     * 获取 护理记录列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_nursing_recode_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<nursingRecord> | undefined> {
        return undefined;
    }
    get_nursing_recode_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<nursingRecord> | undefined> {
        return undefined;
    }
    /** 修改/新增护理记录列表 */
    update_nursing_recode?(assessment_project: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除护理记录列表 */
    delete_nursing_recode?(assessment_project_ids: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }

}