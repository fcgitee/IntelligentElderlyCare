import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  医疗欠费
 */
export interface Hospital {
    /** id */
    id: string;
    /** 长者id */
    elder?: string;

}
/**  医院管理 */
@addon('IHospitalService', '医院管理', '医院管理')
export class IHospitalService {
    /**
     * 获取 医疗欠费列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_hospital_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<Hospital> | undefined> {
        return undefined;
    }
    /** 修改/新增医疗欠费 */
    update_hospital?(condition?: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除需求记录 */
    delete_hospital?(requirement_record_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}