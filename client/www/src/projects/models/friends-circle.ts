import { NullablePromise, DataList, addon } from "pao-aop";
/**  意见反馈 */
@addon('IFriendsCircleService', '老友圈', '老友圈')
export class IFriendsCircleService {
    /** 老友圈 */
    get_friends_circle_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    get_circle_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 新增/修改圈子
     */
    update_circle?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 新增/修改圈子
     */
    delete_circle?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 新增/修改工作人员出入档案
     */
    update_epidemic_prevention?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    get_epidemic_prevention_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 判定
     */
    decide_epidemic_prevention?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 查询某个人每天的出入登记记录
     */
    get_day_epidemic_prevention_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 查询机构所有每日登记信息
     */
    get_org_day_info_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 
     * 新增机构每日登记信息
     */
    update_org_day_info?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 判定
     */
    decide_by_once?(condition: any, recode?: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 
     * 导入机构每日登记信息
     */
    upload_day_info_excl?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
}