import { NullablePromise, DataList, addon } from "pao-aop";
export interface NNN {

}
@addon('IShoppingMallManageService', '商城管理', '商城管理')
export class IShoppingMallManageService {
    /**
     * 接口
     */
    get_data_dictionary_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    update_data_dictionary?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    delete_data_dictionary?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_data_dictionary_childs_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_data_dictionary_childs_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 商品信息
    get_goods_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_goods_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 新增编辑商品信息
    update_goods?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 审核商品
    update_goods_sh?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 删除商品信息
    delete_goods?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取商品订单
    get_product_orders_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    get_product_orders_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 服务商财务总览
    get_sp_financial_overview_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 服务商入账记录
    get_sp_entry_record_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 平台财务总览
    get_pt_financial_overview_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 平台入账记录
    get_pt_entry_record_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取所有板块
    get_blocks_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 更新板块
    update_block?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 删除板块信息
    delete_block?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取所有板块推荐
    get_recommend_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 更新板块推荐
    update_recommend?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 删除板块推荐
    delete_recommend?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取服务类型
    get_service_type_list?(condition?: {}): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取服务产品
    get_service_product_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 获取资金详情列表
    get_capital_detail_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 获取分账审核列表
    get_separate_accounts_sh_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<NNN> | undefined> {
        return undefined;
    }
    // 更新商品订单状态（取消，删除）
    update_order_status?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 更新商品订单信息
    update_order?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 复制一个商品
    copy_goods?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
    // 复制一个商品
    change_goods?(condition?: {}): NullablePromise<DataList<any> | undefined> {
        return undefined;
    }
}