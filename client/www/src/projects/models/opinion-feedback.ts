import { NullablePromise, DataList, addon } from "pao-aop";
/**  意见反馈 */
@addon('IOpinionFeedbackService', '意见反馈', '意见反馈')
export class IOpinionFeedbackService {
    /** 查询意见反馈 */
    get_app_feedback_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    /** 更新意见反馈 */
    update_opinion_feedback?(condition: any): NullablePromise<boolean | undefined> {
        return undefined;
    }
}