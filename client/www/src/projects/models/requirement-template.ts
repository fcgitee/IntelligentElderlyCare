import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  需求模板
 */
export interface RequirementTemplate {
    /** id */
    id: string;
    /** 模板名称 */
    name?: string;
    /** 需求类别id */
    requirement_type_id?: string;
    /** 需求模板明细 */
    requirement_template_detail?: string;
    /** 备注 */
    remark?: string;
    /** 所属组织机构id */
    organization_is?: string;

}
/**  需求模板服务 */
@addon('IRequirementTemplateService', '需求模板服务', '需求模板服务')
export class IRequirementTemplateService {
    /**
     * 获取 需求模板列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_requirement_template_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<RequirementTemplate> | undefined> {
        return undefined;
    }
    /** 修改/新增需求模板 */
    update_requirement_template?(requirement_template: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除需求模板 */
    delete_requirement_template?(requirement_template_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}