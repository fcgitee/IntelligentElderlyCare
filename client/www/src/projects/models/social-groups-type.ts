/*
 * @Description: In User Settings Edit
 * @Author: yzy
 * @Date: 2019-09-20 10:56:29
 * @LastEditTime: 2019-09-20 11:09:33
 * @LastEditors: Please set LastEditors
 */

import { NullablePromise, addon, DataList } from "pao-aop";

export interface SocialGroupsType {
    /** ID */
    id?: string;
    /** 类型名称 */
    name?: string;
    /** 描述 */
    describe?: string;
    /** 类型 */
    type?: string;
}
@addon('ISocialGroupsTypeService', '社会群体类型', '社会群体类型')
export class ISocialGroupsTypeService {
    /**
     * 查询列表
     */
    get_social_groups_type_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<SocialGroupsType> | undefined> {
        return undefined;
    }
    get_social_groups_type_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<SocialGroupsType> | undefined> {
        return undefined;
    }
    get_social_groups_type_list_look?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<SocialGroupsType> | undefined> {
        return undefined;
    }
    /**
     * 新增/修改
     */
    update_social_groups_type?(socialGroupsType?: SocialGroupsType): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /**
     * 删除
     */
    del_social_groups_type?(ids?: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}