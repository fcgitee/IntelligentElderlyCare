/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-30 18:16:42
 * @LastEditTime: 2020-03-04 17:10:29
 * @LastEditors: Please set LastEditors
 */
import { addon, DataList, NullablePromise } from "pao-aop";

export interface Materiel {

}
/** 物料 */
@addon('IMaterielService', '物料', '物料')
export class IMaterielService {
    // 获取计量单位
    get_units_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取计量单位列表
    get_units_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑计量单位
    update_units?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除计量单位
    delete_units?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取物料分类
    get_thing_sort_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取计量单位列表
    get_thing_sort_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑计量单位
    update_thing_sort?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除计量单位
    delete_thing_sort?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取物料档案
    get_thing_archives_list_all?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 获取物料档案列表
    get_thing_archives_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 新增/编辑物料档案
    update_thing_archives?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
    // 删除计量物料档案
    delete_thing_archives?(condition?: {}): NullablePromise<DataList<any>> {
        return undefined;
    }
}