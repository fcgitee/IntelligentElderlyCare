import { NullablePromise, DataList, addon } from "pao-aop";

/**
 *  服务订单
 */
export interface ServiceOrder {
    /** id */
    id: string;
    /** 采购人id */
    purchaser_id?: string;
    /** 服务商id */
    service_provider_id?: string;
    /** 订单时间 */
    order_date?: string;
    /** 订单时间 */
    service_date?: string;
    /** 订单时间 */
    pay_date?: string;
    /** 交易id */
    transation_id?: string;
    /** 状态 */
    status?: string;
    /** 合同条款 */
    contract_clauses?: string;
    /** 服务项目清单 */
    service_items?: any[];
}
/**  服务订单服务 */
@addon('IServiceOrderService', '服务订单服务', '服务订单服务')
export class IServiceOrderService {
    /**
     * 获取 服务订单列表
     * @param condition 查询条件
     * @param page 页码
     * @param count 条数
     */
    get_service_order_list?(condition?: {}, page?: number, count?: number): NullablePromise<DataList<ServiceOrder> | undefined> {
        return undefined;
    }
    /** 修改/新增服务订单 */
    update_service_order?(service_order: {}): NullablePromise<boolean | undefined> {
        return undefined;
    }
    /** 删除服务订单 */
    delete_service_order?(service_order_ids: string[]): NullablePromise<boolean | undefined> {
        return undefined;
    }
}