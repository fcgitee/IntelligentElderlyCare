import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { Select } from "antd";
let { Option } = Select;
/**
 * 组件：长者类型详情
 */
export interface ChangePersonnelClassificationViewState extends ReactViewState {
    /** 数据id */
    id?: string;
}

/**
 * 组件：长者类型详情
 */
export default class ChangePersonnelClassificationView extends ReactView<ChangePersonnelClassificationViewControl, ChangePersonnelClassificationViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        history.back();
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        const classification_type = ['政府补贴', '自费'];
        const classification_type_list: JSX.Element[] = classification_type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        var edit_props = {
            form_items_props: [
                {
                    title: '添加长者类型',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "长者类型名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入长者类型名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入长者类型名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.select,
                            label: "类型",
                            decorator_id: "type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择类型",
                                childrens: classification_type_list
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "长者类型描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入长者类型描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入长者类型描述",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        history.back();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_personnel_classification"
                    },
                    query: {
                        func_name: "get_personnel_classification_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：长者类型详情
 * @description 长者类型详情
 * @author
 */
@addon('ChangePersonnelClassificationView', '长者类型详情', '长者类型详情')
@reactControl(ChangePersonnelClassificationView, true)
export class ChangePersonnelClassificationViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}