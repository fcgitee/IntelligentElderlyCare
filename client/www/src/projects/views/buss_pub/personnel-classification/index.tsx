import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
/**
 * 组件：长者类型状态
 */
export interface PersonnelClassificationViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：长者类型
 * 长者类型列表
 */
export class PersonnelClassificationView extends ReactView<PersonnelClassificationViewControl, PersonnelClassificationViewState> {
    private columns_data_source = [{
        title: '长者类型名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '类别描述',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: '类型',
        dataIndex: 'type',
        key: 'type',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            request_url: '',
        };
    }

    // 新增长者类型
    add = () => {
        this.props.history!.push(ROUTE_PATH.changePersonnelClassification);
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changePersonnelClassification + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url,
        });
    }
    render() {
        // let donate_project_list: any[] = [];
        // this.state.donate_project_list!.map((item, idx) => {
        //     donate_project_list.push(<Option key={item['id']}>{item['project_name']}</Option>);
        // });
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "长者类型名称",
                decorator_id: "name"
            }],
            btn_props: [{
                label: '新增长者类型',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_personnel_classification'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let config_list = Object.assign(option, table_param);
        return (
            <Row>
                <SignFrameLayout {...config_list} />
            </Row>
        );
    }
}

/**
 * 控件：长者类型
 * 长者类型列表
 */
@addon('PersonnelClassificationView', '长者类型', '长者类型列表')
@reactControl(PersonnelClassificationView, true)
export class PersonnelClassificationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}