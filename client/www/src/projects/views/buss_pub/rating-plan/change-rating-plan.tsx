import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Select } from "antd";
let Option = Select.Option;
/**
 * 状态：评比方案编辑页面
 */
export interface ChangeRatingPlanViewState extends ReactViewState {
    happiness_list?: any;
}

/**
 * 组件：评比方案编辑页面视图
 */
export class ChangeRatingPlanView extends ReactView<ChangeRatingPlanViewControl, ChangeRatingPlanViewState> {
    constructor(props: ChangeRatingPlanViewControl) {
        super(props);
        this.state = {
            happiness_list: [],
        };
    }
    componentDidMount() {
        // 获取幸福院
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ personnel_category: '幸福院' })!)
            .then((datas: any) => {
                this.setState({
                    happiness_list: datas.result,
                });
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let happiness_list: JSX.Element[] = this.state.happiness_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        let edit_props = {
            form_items_props: [{
                title: "评比方案",
                need_card: true,
                input_props: [
                    {
                        type: InputType.select,
                        label: "幸福院",
                        decorator_id: "org_id",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择幸福院" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择幸福院",
                            childrens: happiness_list,
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.antd_input,
                        label: "方案名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入方案名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入方案名称",
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remark",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入备注" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            autocomplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_rating_plan_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_rating_plan"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：评比方案编辑页面控件
 * @description 评比方案编辑页面控件
 * @author
 */
@addon('ChangeRatingPlanViewControl', '评比方案编辑页面控件', '评比方案编辑页面控件')
@reactControl(ChangeRatingPlanView, true)
export class ChangeRatingPlanViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}