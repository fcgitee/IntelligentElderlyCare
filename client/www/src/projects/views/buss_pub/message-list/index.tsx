import { Row, Select } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from 'src/projects/app/util-tool';

let { Option } = Select;
const funcMap = {
    "article": "新闻管理",
    "activity": "活动列表",
    "activity_participate": "活动签到",
    "old_age_allowance": "高龄津贴名单",
    "service_order": "服务订单",
    "rservation_registration": "床位管理"
};

/**
 * 组件：消息列表（新需求）列表状态
 */
export interface MessageListNewViewState extends ReactViewState {
}

/**
 * 组件：消息列表（新需求）列表
 * 描述
 */
export class MessageListNewView extends ReactView<MessageListNewViewControl, MessageListNewViewState> {
    private columns_data_source = [
        {
            title: '消息主题',
            dataIndex: 'message_name',
            key: 'message_name',
        },
        {
            title: '消息内容',
            dataIndex: 'message_content',
            key: 'message_content',
        }, {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
        }, {
            title: '消息类型',
            dataIndex: 'business_type',
            key: 'business_type',
            render: (text: string, record: any) => {
                return funcMap[record['business_type']];
            }
        }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onIconClick = (type: string, contents: any) => {
    }
    componentDidMount() {
    }
    render() {
        const messageType = [
            { name: '新闻管理', key: 'article' },
            { name: '活动列表', key: 'activity' },
            { name: '活动签到', key: 'activity_participate' },
            { name: '高龄津贴名单', key: 'old_age_allowance' },
            { name: '服务订单', key: 'service_order' },
            { name: '床位管理', key: 'rservation_registration' }
        ];
        const messageType_list: JSX.Element[] = messageType!.map((item) => <Option key={item.key} value={item.key}>{item.name}</Option>);

        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "消息类型",
                    decorator_id: "business_type",
                    option: {
                        placeholder: "请选择消息类型",
                        // autoComplete: 'off',
                        childrens: messageType_list,
                    }
                }
            ],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.login_service,
            service_option: {
                select: {
                    service_func: 'get_message_list_new',
                    service_condition: [{}, 1, 10]
                }
            },
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：消息列表（新需求）列表
 * 描述
 */
@addon('MessageListNewView', '消息列表（新需求）列表', '描述')
@reactControl(MessageListNewView, true)
export class MessageListNewViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}