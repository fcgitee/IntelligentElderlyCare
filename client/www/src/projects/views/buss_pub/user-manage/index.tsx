import { addon, getObject, IPermissionService, IRoleService, log, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { IUserService } from "src/projects/models/user";
// import zh_CN from 'antd/lib/locale-provider/zh_CN';

/**
 * 组件：用户管理状态
 */
export interface UserManageViewState extends ReactViewState {
    /** 用户角色选择框内容 */
    user_role?: string[];
    /** 用户账号输入框内容 */
    account?: string;
    /** 数据总条数 */
    total_data?: number;
    /** 总页数 */
    total_pages?: number;
    /** 当前页数 */
    current_page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 数据 */
    data_source?: any[];
    /** 所有角色 */
    role_list?: any[];
    /** 所有角色 */
    role_list_id?: any[];
    /** 用户名称 */
    user_name?: string;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
}
/**
 * 组件：安全设备登记管理
 */
export class UserManageView extends ReactView<UserManageViewControl, UserManageViewState> {
    private columns_data_source = [{
        title: '用户名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '登录账号',
        dataIndex: 'account',
        key: 'account',
    }, {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    constructor(props: UserManageViewControl) {
        super(props);
        this.state = {
            role_list: [],
            loading: false,
            condition: {
                role_ids: [],
                user_name: "",
                account: ""
            }
        };
    }
    /** 用户角色分类选择框回调 */
    userTypeButtonSelect = (value: any) => {
        console.info(value);
        this.setState({
            user_role: value,
            loading: true
        });
        let condition = {
            role_ids: value,
            user_name: "",
            account: ""
        };
        this.userService!()!.get_user_list!(condition, 1, 10)!
            .then((data: any) => {
                this.setState({
                    data_source: data.result,
                    total_data: data.total,
                    loading: false
                });
            })
            .catch(error => {
                this.setState({
                    loading: false
                });
                log("userManageViewControl", error.message);
            });
    }
    usenameChange = (event: any) => {
        this.setState({
            user_name: event.target.value
        });
    }
    /** 用户账号输入框回调 */
    loginAccountChange = (event: any) => {
        this.setState({
            account: event.target.value
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push('/user-editor/' + contents.user_id);

        }
    }
    /** 删除按钮回调事件 */
    // onClickDel = (contents: any) => {
    //     // console.log("删除按钮返回值", contents);
    //     this.userService!()!.delete!([contents.user_id])!
    //         .then((data: any) => {
    //             // console.log(data);
    //             if (data === 'Success') {
    //                 message.info('删除成功');
    //                 this.handleDelete(contents.user_id);
    //             }

    //         })
    //         .catch(error => {
    //             // debugger;
    //             // console.log(error);
    //         });

    // }
    handleDelete = (key: any) => {
        const dataSource = [...this.state.data_source!];
        this.setState({ data_source: dataSource.filter(item => item.user_id !== key) });
    }
    /** 行选择回调事件 */
    onRowSelection = (contents: string[]) => {
        // console.log('selectedRow changed: ', contents);
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            current_page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    queryBtn = (e: any, val: any) => {
        this.setState({
            loading: true
        });
        let str: string[] = [];
        let condition = {
            role_ids: str,
            user_name: "",
            account: ""
        };
        condition.role_ids = this.state.user_role ? this.state.user_role : this.state.role_list_id!;
        condition.account = val.accout ? val.accout : '';
        condition.user_name = val.user_name ? val.user_name : '';
        this.userService!()!.get_user_list!(condition, 1, 10)!
            .then((data: any) => {
                this.setState({
                    data_source: data.result,
                    total_data: data.total,
                    loading: false
                });
            })
            .catch(error => {
                // debugger;
                this.setState({
                    loading: false
                });
                log("userManageViewControl", error.message);
            });
    }
    resetBtn = () => {
        alert('重置操作');
    }
    /** 初始化服务 */
    userService?() {
        return getObject(this.props.userService_Fac!);
    }
    /** 权限服务 */
    permissionService?() {
        return getObject(this.props.permissionService_Fac!);
    }

    roleService?() {
        return getObject(this.props.roleService_Fac!);
    }
    componentWillMount() {

    }
    componentDidMount() {
        this.setState({
            loading: true
        });
        let condition = {
            role_ids: [],
            user_name: "",
            account: ""
        };
        request(this, this.roleService!()!.get_role_list!({}, 1, 10))
            .then((data: any) => {
                let list = data.result.map((item: any) => {
                    return item.id;
                });

                condition.role_ids = list;
                this.setState({
                    role_list_id: list,
                    role_list: data.result,
                    condition: condition
                });
            });
    }

    addUser = () => {
        this.props.history!.push('/user-editor');
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { role_list } = this.state;
        let datas = role_list!.map((item: any) => {
            let obj = {
                key: item.id,
                text: item.role_name,
                value: item.id
            };
            return obj;
        });
        let user_manege = {
            type_show: true,
            type_method: this.userTypeButtonSelect,
            type_props: {
                label: '用户角色',
                data: datas,
            },
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "用户名称",
                    decorator_id: "user_name",
                    option: {
                        placeholder: "请输入用户名称"
                    }
                }, {
                    type: InputType.input,
                    label: "登录账号",
                    decorator_id: "accout",
                    option: {
                        placeholder: "请输入登录账号"
                    }
                },
            ],
            btn_props: [{
                label: '新增用户',
                btn_method: this.addUser,
                icon: 'plus'
            }],
            // table配置
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            page_on_click: this.pageOnCick,
            show_size_change: this.showSizeChange,
            rowKey: 'user_id',
            search_cb: this.queryBtn,
            service_object: AppServiceUtility.user_service,
            service_option: {
                select: {
                    service_func: 'get_user_list',
                    service_condition: [{}, 1, 10]
                }
            }

        };
        return (
            <SignFrameLayout {...user_manege} />
        );
    }
}

/**
 * 控件：用户管理
 * @description 控制用户管理
 * @author
 */
@addon('UserManageView', '用户管理管理', '用户管理')
@reactControl(UserManageView, true)
export class UserManageViewControl extends ReactViewControl {
    /** 用户服务 */
    public userService_Fac?: Ref<IUserService>;
    /**
     * 权限服务接口
     */
    public permissionService_Fac?: Ref<IPermissionService>;
    /** 角色服务 */
    public roleService_Fac?: Ref<IRoleService>;
    /** 视图权限 */
    public permission?: Permission;
    constructor() {
        super();
    }
}