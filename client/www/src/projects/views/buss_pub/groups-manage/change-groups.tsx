import { AppServiceUtility } from "src/projects/app/appService";
import { beforeUpload } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import Form from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Card, message, Button, Row, Col, Input, DatePicker, Table, Modal, Select } from "antd";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import "./index.less";
import { ROUTE_PATH } from "src/projects/router";
let Option = Select.Option;
/**
 * 状态：团体详情页面
 */
export interface CbangeGroupsViewState extends ReactViewState {
    // 团体详情
    group_info?: any;
    // 队长详情
    captain_info?: any;
    // 成员详情
    member_info?: any;
    // 成员列表
    member_list?: any;
    // 是否已发送
    is_send?: boolean;
    // 成员表单是否显示
    is_member_box_show?: boolean;
    // 是否完成数据获取
    is_get_data?: boolean;
    // 当前存在状态
    page_status?: string;
    // 删除框
    is_del_member_show?: boolean;
    // 按钮是否开启
    is_button_disabled?: boolean;
    // user加载模态框
    is_user_box_show?: boolean;
    // 找到的user数据
    user_info?: any;
    // 当前触发的表单，队长或者成员
    form_type?: string;
}

/**
 * 组件：团体详情页面视图
 */
export class CbangeGroupsView extends ReactView<ChangeGroupsViewControl, CbangeGroupsViewState> {
    public groupForm: any = null;
    public captainForm: any = null;
    public memberForm: any = null;
    public stv: any = null;
    private $member_base: any = ['name', 'id_card', 'sex', 'age', 'telephone', 'address', 'job', 'photo'];
    // 队长属性多一个描述
    private $captain_param: any = this.$member_base.concat('description');
    // 成员属性多一个职责
    private $member_param: any = this.$member_base.concat('duty');
    // 团队参数
    private $group_param: any = ['name', 'description', 'photo', 'category', 'founded_date'];
    constructor(props: ChangeGroupsViewControl) {
        super(props);
        this.state = {
            group_info: [],
            captain_info: [],
            member_info: [],
            user_info: [],
            form_type: "",
            is_send: false,
            is_member_box_show: false,
            is_get_data: false,
            is_del_member_show: false,
            is_button_disabled: true,
            is_user_box_show: false,
            page_status: this.props.match!.params.key ? 'edit' : 'add',
        };
    }
    componentDidMount() {
        // 获取团体详情
        if (this.props.match!.params.key) {
            this.getTeamAllData(this.props.match!.params.key);
        } else {
            this.setState({
                is_button_disabled: true,
            });
        }
    }
    getTeamAllData(id: string) {
        const { is_send } = this.state;
        if (!id || is_send === true) {
            return;
        }
        this.setState(
            {
                is_send: true
            },
            () => {
                request(this, AppServiceUtility.person_org_manage_service.get_groups_list_all!({ id })!)
                    .then((datas: any) => {
                        if (datas && datas.result && datas.result[0]) {
                            // 判断队长
                            let captain_info: any = [];
                            let member_list: any = [];
                            if (datas.result[0].hasOwnProperty('member_list')) {
                                datas.result[0]['member_list'].map((item: any) => {
                                    if (item.hasOwnProperty('type')) {
                                        if (item['type'] === 'captain') {
                                            captain_info = item;
                                        } else {
                                            member_list.push(item);
                                        }
                                    }
                                });
                            }
                            this.setState({
                                group_info: datas.result[0] || [],
                                member_list,
                                captain_info,
                                is_get_data: true,
                                is_send: false,
                                is_button_disabled: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    handleFormSubmit = (e: any, type: any) => {
        e.preventDefault();
        const { form } = this.props;
        const { group_info, is_get_data, page_status, member_info } = this.state;
        // 成员和队长表保存，必须要有团体ID，如果没有，提示新增
        if (type === 'captain' || type === 'member') {
            // 没有团体ID
            if (!group_info.hasOwnProperty('id')) {
                // 编辑
                if (page_status === 'edit') {
                    if (is_get_data === false) {
                        // 应该是还没加载完
                        return;
                    }
                } else if (page_status === 'add') {
                    // 新增界面
                    message.info('请先新增团体！');
                    return;
                }
            }
        }
        let form_param = this.formTypeGetFormParam(type);
        let new_form_param = form_param.map((item: any) => `${type}_${item}`);
        // 只验证对应的域
        form!.validateFields(new_form_param, (err: Error, values: any) => {
            if (!err) {
                let formValue: any = {};
                let form_param = this.formTypeGetFormParam(type);
                // 属性保存
                for (let i = 0; i < form_param.length; i++) {
                    // 时间需要格式化，不然时区是错的
                    formValue[form_param[i]] = values[`${type}_${form_param[i]}`];
                    if (form_param[i] === 'founded_date') {
                        formValue[form_param[i]] = formValue[form_param[i]].format("YYYY-MM-DD HH:mm:ss");
                    }
                }
                formValue['type'] = type;
                if (type === 'group') {
                    if (group_info.hasOwnProperty('id')) {
                        formValue['id'] = group_info['id'];
                    } else if (this.props.match!.params.key) {
                        formValue['id'] = this.props.match!.params.key;
                    }
                } else if (type === 'captain' || type === 'member') {
                    // 人员需要加上团体的ID
                    formValue['group_id'] = group_info['id'];
                    if (type === 'member') {
                        formValue['id'] = member_info['id'];
                    }
                }
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.person_org_manage_service.update_groups!(formValue))
                            .then((datas: any) => {
                                this.setState({
                                    is_send: false,
                                });
                                if (type === 'group' && datas !== 'Fail' && datas.split('-').length === 5) {
                                    this.setState(
                                        {
                                            page_status: 'edit',
                                        },
                                        () => {
                                            message.info('新增团体成功！');
                                            this.getTeamAllData(datas);
                                        }
                                    );
                                } else {
                                    if (datas === 'Success') {
                                        if (type === 'member') {
                                            this.setState({
                                                is_member_box_show: false,
                                            });
                                        }
                                        message.info('保存成功！');
                                        this.getTeamAllData(this.props.match!.params.key);
                                    } else {
                                        message.info(datas);
                                    }
                                }
                            })
                            .catch((error: any) => {
                                message.info('保存失败！');
                                this.setState({
                                    is_send: false,
                                });
                                // console.log(error);
                            });
                    }
                );
            }
        });
    }
    showMember(e: any, contents: any) {
        if (contents === 'add') {
            this.setState({
                member_info: [],
                is_member_box_show: true,
            });
        } else if (contents.hasOwnProperty('id')) {
            this.setState({
                member_info: contents,
                is_member_box_show: true,
            });
        }
    }
    delMember(e: any, contents: any) {
        this.setState({
            member_info: contents,
            is_del_member_show: true,
        });
    }
    hideMember() {
        this.setState({
            is_member_box_show: false,
        });
    }
    formTypeGetFormParam(type?: string) {
        if (type === 'captain') {
            return this.$captain_param;
        } else if (type === 'member') {
            return this.$member_param;
        } else if (type === 'group') {
            return this.$group_param;
        }
    }
    formReset(e: any, type: string) {
        // let param: any = {};
        // let form_param = this.formTypeGetFormParam(type);
        // for (let i = 0; i < form_param.length; i++) {
        //     param[`${type}_${form_param[i]}`] = '';
        // }
        // this.props.form.setFieldsValue(param);
        if (type === 'group') {
            this.setState({
                group_info: [],
            });
        } else if (type === 'captain') {
            this.setState({
                captain_info: [],
            });
            this.props.form.setFieldsValue({
                captain_id_card: '',
            });
        } else if (type === 'member') {
            this.setState({
                member_info: [],
            });
            this.props.form.setFieldsValue({
                member_id_card: '',
            });
        }
    }
    setRef(item: any, type: string) {
        if (type === 'captain') {
            this.captainForm = item;
        } else {
            this.memberForm = item;
        }
    }
    getPersonForm(type: string) {
        const memberLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const { is_button_disabled } = this.state;
        const { getFieldDecorator } = this.props.form!;
        let zh = type === 'captain' ? '队长' : "成员";
        let prev = type === 'captain' ? 'captain_' : "member_";
        let member_info = type === 'captain' ? this.state.captain_info : this.state.member_info;
        return (
            <Form {...memberLayout} onSubmit={(e) => this.handleFormSubmit(e, type)} ref={(item) => this.setRef(item, type)}>
                <Card title={`${zh}信息`} extra={type === 'member' ? <Button onClick={() => this.hideMember()}>取消</Button> : null}>
                    <Row type="flex" justify="center">
                        <Col span={18}>
                            <Row>
                                <Col span={8}>
                                    <Form.Item label='身份证号'>
                                        {getFieldDecorator(`${prev}id_card`, {
                                            initialValue: member_info['id_card'] || '',
                                            rules: [{
                                                required: true,
                                                message: '请输入身份证号'
                                            }],
                                        })(
                                            <Input onChange={(e) => this.slowCheck(e, type)} autoComplete='off' placeholder="请输入身份证号" />
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item label='姓名'>
                                        {getFieldDecorator(`${prev}name`, {
                                            initialValue: member_info['name'] || '',
                                            rules: [{
                                                required: true,
                                                message: '请输入姓名'
                                            }],
                                        })(
                                            <Input autoComplete='off' placeholder="请输入姓名" />
                                        )}
                                    </Form.Item>
                                </Col>
                                {type === 'captain' ? <Col span={8}>
                                    <Form.Item label='从事职业'>
                                        {getFieldDecorator(`${prev}job`, {
                                            initialValue: member_info['job'] || '',
                                            rules: [{
                                                required: true,
                                                message: '请输入当前/以往从事职业'
                                            }],
                                        })(
                                            <Input autoComplete='off' placeholder="请输入当前/以往从事职业" />
                                        )}
                                    </Form.Item>
                                </Col> : <Col span={8}>
                                        <Form.Item label='团体内职责'>
                                            {getFieldDecorator(`${prev}duty`, {
                                                initialValue: member_info['duty'] || '',
                                                rules: [{
                                                    required: true,
                                                    message: '请输入团体内职责'
                                                }],
                                            })(
                                                <Input autoComplete='off' placeholder="请输入团体内职责" />
                                            )}
                                        </Form.Item>
                                    </Col>}
                            </Row>
                            <Row>
                                <Col span={16}>
                                    <Row>
                                        <Col span={12}>
                                            <Form.Item label='性别'>
                                                {getFieldDecorator(`${prev}sex`, {
                                                    initialValue: member_info['sex'] || '',
                                                    rules: [{
                                                        required: true,
                                                        message: '请输入性别'
                                                    }],
                                                })(
                                                    <Input autoComplete='off' placeholder="请输入性别" />
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                            <Form.Item label='周岁'>
                                                {getFieldDecorator(`${prev}age`, {
                                                    initialValue: member_info['age'] || '',
                                                    rules: [{
                                                        required: true,
                                                        message: '请输入周岁'
                                                    }],
                                                })(
                                                    <Input autoComplete='off' placeholder="请输入周岁" />
                                                )}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12}>
                                            <Form.Item label='常住地址'>
                                                {getFieldDecorator(`${prev}address`, {
                                                    initialValue: member_info['address'] || '',
                                                    rules: [{
                                                        required: true,
                                                        message: '请输入常住地址'
                                                    }],
                                                })(
                                                    <Input autoComplete='off' placeholder="请输入常住地址" />
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                            <Form.Item label='联系电话'>
                                                {getFieldDecorator(`${prev}telephone`, {
                                                    initialValue: member_info['telephone'] || '',
                                                    rules: [{
                                                        required: true,
                                                        message: '请输入联系电话'
                                                    }],
                                                })(
                                                    <Input autoComplete='off' placeholder="联系电话" />
                                                )}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Col>
                                {type === 'captain' ? <Col span={8}>
                                    <Form.Item label='个人简介'>
                                        {getFieldDecorator(`${prev}description`, {
                                            initialValue: member_info['description'] || '',
                                            rules: [{
                                                required: true,
                                                message: '请输入个人简介'
                                            }],
                                        })(
                                            <TextArea autoComplete="off" rows={4} placeholder="请输入个人简介" />
                                        )}
                                    </Form.Item>
                                </Col> : <Col span={8}>
                                        <Form.Item label='从事职业'>
                                            {getFieldDecorator(`${prev}job`, {
                                                initialValue: member_info['job'] || '',
                                                rules: [{
                                                    required: true,
                                                    message: '请输入当前/以往从事职业'
                                                }],
                                            })(
                                                <Input autoComplete='off' placeholder="请输入当前/以往从事职业" />
                                            )}
                                        </Form.Item>
                                    </Col>}
                            </Row>
                        </Col>
                        <Col span={6}>
                            <Form.Item label={`${zh}图片（大小小于2M，格式支持jpg/jpeg/png）`}>
                                {getFieldDecorator(`${prev}photo`, {
                                    initialValue: member_info['photo'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请上传队长图片'
                                    }],
                                })(
                                    <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" className="ctrl-btns">
                        <Button htmlType='submit' type='primary' disabled={is_button_disabled}>保存</Button>
                        <Button onClick={(e) => this.formReset(e, type)}>重置</Button>
                    </Row>
                </Card>
            </Form>
        );
    }
    backList() {
        this.props.history!.push(ROUTE_PATH.groups);
    }
    handleDelMemberOk(e: any) {
        const { is_send, member_info, member_list } = this.state;
        if (is_send || !member_info.hasOwnProperty('id')) {
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.person_org_manage_service.delete_groups!({ type: 'member', id: member_info.id })!)
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('删除成功！');
                            let new_member_list: any = [];
                            member_list.map((item: any) => {
                                if (item.id !== member_info.id) {
                                    new_member_list.push(item);
                                }
                            });
                            this.setState({
                                is_send: false,
                                is_del_member_show: false,
                                member_list: new_member_list,
                            });
                        } else {
                            message.info(datas);
                        }
                    })
                    .catch((error: any) => {
                        message.info('删除失败！');
                        this.setState({
                            is_send: false,
                            is_del_member_show: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    getAge2(identityCard: string) {
        if (identityCard === '') {
            return identityCard;
        }
        identityCard = identityCard + '';
        var len = (identityCard + "").length;
        if (len === 0) {
            return 0;
        } else {
            // 身份证号码只能为15位或18位其它不合法
            if ((len !== 15) && (len !== 18)) {
                return 0;
            }
        }
        var strBirthday = "";
        // 处理18位的身份证号码从号码中得到生日和性别代码
        if (len === 18) {
            strBirthday = identityCard.substr(6, 4) + "/" + identityCard.substr(10, 2) + "/" + identityCard.substr(12, 2);
        }
        if (len === 15) {
            strBirthday = "19" + identityCard.substr(6, 2) + "/" + identityCard.substr(8, 2) + "/" + identityCard.substr(10, 2);
        }
        // 时间字符串里，必须是“/”
        var birthDate = new Date(strBirthday);
        var nowDateTime = new Date();
        var age = nowDateTime.getFullYear() - birthDate.getFullYear();
        // 再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
        if (nowDateTime.getMonth() < birthDate.getMonth() || (nowDateTime.getMonth() === birthDate.getMonth() && nowDateTime.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    handleUserBoxOk(e: any) {
        const { form_type, user_info } = this.state;
        if (form_type === 'captain') {
            let captain_info = this.state.captain_info;
            // 名字
            if (user_info.hasOwnProperty('name') && user_info.name) {
                captain_info['name'] = user_info.name;
            }
            // 性别
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('sex') && user_info.personnel_info.sex) {
                captain_info['sex'] = user_info.personnel_info.sex;
            }
            // 联系电话
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('telephone') && user_info.personnel_info.telephone) {
                captain_info['telephone'] = user_info.personnel_info.telephone;
            }
            // 地址
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('address') && user_info.personnel_info.address) {
                captain_info['address'] = user_info.personnel_info.address;
            }
            this.setState({
                captain_info,
                is_user_box_show: false,
            });
        } else if (form_type === 'member') {
            let member_info = this.state.member_info;
            // 名字
            if (user_info.hasOwnProperty('name') && user_info.name) {
                member_info['name'] = user_info.name;
            }
            // 性别
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('sex') && user_info.personnel_info.sex) {
                member_info['sex'] = user_info.personnel_info.sex;
            }
            // 联系电话
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('telephone') && user_info.personnel_info.telephone) {
                member_info['telephone'] = user_info.personnel_info.telephone;
            }
            // 地址
            if (user_info.hasOwnProperty('personnel_info') && user_info['personnel_info'].hasOwnProperty('address') && user_info.personnel_info.address) {
                member_info['address'] = user_info.personnel_info.address;
            }
            this.setState({
                member_info,
                is_user_box_show: false,
            });
        } else {
            message.info('导入失败！');
            return;
        }
    }
    handleUserBoxCancel(e: any) {
        this.setState({
            is_user_box_show: false,
        });
    }
    handleDelMemberCancel(e: any) {
        this.setState({
            is_del_member_show: false,
        });
    }
    slowCheck(e: any, type: string) {
        clearTimeout(this.stv);
        let svalue: any = e.target.value;
        this.stv = setTimeout(
            () => {
                this.setState(
                    {
                        form_type: type,
                    },
                    () => {
                        let age: any = this.getAge2(svalue);
                        if (age !== 0 || age !== '') {
                            if (type === 'captain') {
                                let captain_info = this.state.captain_info;
                                captain_info['age'] = age;
                                this.setState({
                                    captain_info,
                                });
                            } else if (type === 'member') {
                                let member_info = this.state.member_info;
                                member_info['age'] = age;
                                this.setState({
                                    member_info,
                                });
                            }
                        }
                        this.checkUserExists(svalue);
                    }
                );
            },
            1000,
        );
    }
    checkUserExists(id_card?: any) {
        if (!id_card) {
            return;
        }
        request(this, AppServiceUtility.personnel_service.get_personnel_list!({ id_card })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        user_info: datas.result[0],
                        is_user_box_show: true,
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const groupLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const memberTableColumns: any = [
            {
                title: '风貌',
                dataIndex: 'photo',
                key: 'photo',
                render: (text: any, record: any, index: number) => {
                    return (
                        <div>
                            <img src={text} alt={record.name} className="imgMw100px" />
                        </div>
                    );
                }
            },
            {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '职责',
                dataIndex: 'duty',
                key: 'duty',
            },
            {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            },
            {
                title: '性别',
                dataIndex: 'sex',
                key: 'sex',
            },
            {
                title: '周岁',
                dataIndex: 'age',
                key: 'age',
            },
            {
                title: '常住地址',
                dataIndex: 'address',
                key: 'address',
            },
            {
                title: '联系电话',
                dataIndex: 'telephone',
                key: 'telephone',
            },
            {
                title: '当前/以往从事职业',
                dataIndex: 'job',
                key: 'job',
            },
            {
                title: '操作',
                dataIndex: 'action',
                key: 'action',
                render: (text: any, record: any, index: number) => {
                    return (
                        <div>
                            <div className="ny-action-modify-date">最近编辑时间<br />{record.modify_date}</div>
                            <div className="ny-action-btns">
                                <Button type="primary" size="small" onClick={(e) => this.showMember(e, record)} className="btns1">编辑</Button>
                                <Button onClick={(e) => this.delMember(e, record)} type="danger" size="small">删除</Button>
                            </div>
                        </div>
                    );
                }
            },
        ];
        const { getFieldDecorator } = this.props.form!;
        let { group_info, is_member_box_show, member_list, is_del_member_show, user_info, is_user_box_show } = this.state;
        return (
            <MainContent>
                <Form {...groupLayout} onSubmit={(e) => this.handleFormSubmit(e, 'group')} ref={(item) => this.groupForm = item}>
                    <Card title='团体基本信息'>
                        <Row type="flex" justify="center">
                            <Col span={16}>
                                <Row>
                                    <Col span={8}>
                                        <Form.Item label='团体名称'>
                                            {getFieldDecorator('group_name', {
                                                initialValue: group_info['name'] || '',
                                                rules: [{
                                                    required: true,
                                                    message: '请输入团体名称'
                                                }],
                                            })(
                                                <Input autoComplete='off' placeholder="请输入团体名称" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='成立时间'>
                                            {getFieldDecorator('group_founded_date', {
                                                initialValue: group_info['founded_date'] ? moment(group_info['founded_date'], 'YYYY-MM-DD') : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择成立时间'
                                                }],
                                            })(
                                                <DatePicker placeholder="请选择成立时间" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='团体类型'>
                                            {getFieldDecorator('group_category', {
                                                initialValue: group_info['group_category'],
                                                rules: [{
                                                    required: true,
                                                    message: '请选择团体类型'
                                                }],
                                            })(
                                                <Select
                                                    showSearch={true}
                                                >
                                                    {['志愿者队伍', '慢病小组', '其他'].map((item, index) => {
                                                        return (
                                                            <Option key={item}>{item}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={24}>
                                        <Form.Item label='团体介绍' labelCol={{ sm: 3 }} wrapperCol={{ sm: 21 }}>
                                            {getFieldDecorator('group_description', {
                                                initialValue: group_info['description'] || '',
                                                rules: [{
                                                    required: true,
                                                    message: '请输入团体介绍'
                                                }],
                                            })(
                                                <TextArea autoComplete="off" rows={4} placeholder="请输入团体介绍" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={8}>
                                <Form.Item label='团体图片（大小小于2M，格式支持jpg/jpeg/png）'>
                                    {getFieldDecorator('group_photo', {
                                        initialValue: group_info['photo'] || '',
                                        rules: [{
                                            required: true,
                                            message: '请上传团体图片'
                                        }],
                                    })(
                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button htmlType='submit' type='primary'>保存</Button>
                            <Button onClick={(e) => this.formReset(e, 'group')}>重置</Button>
                        </Row>
                    </Card>
                </Form>
                {this.getPersonForm('captain')}
                {is_member_box_show ? this.getPersonForm('member') : null}
                {is_member_box_show ? null : <Row type="flex" justify="end" className="ny-groups-add">
                    <Button onClick={(e) => this.showMember(e, 'add')} type="primary">新增</Button>
                </Row>}
                <Card title="团员信息">
                    <Table
                        columns={memberTableColumns}
                        dataSource={member_list}
                        size="middle"
                        pagination={false}
                    />
                </Card>
                <Row type="flex" justify="center" className="ctrl-btns">
                    <Button onClick={() => this.backList()}>返回</Button>
                </Row>
                <Modal
                    title="删除团体成员"
                    okText="确认"
                    cancelText="取消"
                    visible={is_del_member_show}
                    onOk={(e) => this.handleDelMemberOk(e)}
                    onCancel={(e) => this.handleDelMemberCancel(e)}
                >
                    <p>是否确认删除该成员？删除后不可恢复！</p>
                </Modal>
                <Modal
                    title="信息"
                    okText="是"
                    cancelText="否"
                    visible={is_user_box_show}
                    onOk={(e) => this.handleUserBoxOk(e)}
                    onCancel={(e) => this.handleUserBoxCancel(e)}
                >
                    <p>您输入的身份证号码：{user_info.id_card}</p>
                    <p>后台已有数据，是否导入？</p>
                </Modal>
            </MainContent >
        );
    }
}

/**
 * 控件：团体详情页面控件
 * @description 团体详情页面控件
 * @author
 */
@addon('ChangeGroupsViewControl', '团体详情页面控件', '团体详情页面控件')
@reactControl(Form.create<any>()(CbangeGroupsView), true)
export class ChangeGroupsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}