import { Row } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';
import moment from 'moment';

/**
 * 组件：团体列表状态
 */
export interface GroupsListViewState extends ReactViewState {
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：团体列表
 * 描述
 */
export class GroupsListView extends ReactView<GroupsListViewControl, GroupsListViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '团体人数',
        dataIndex: 'member_num',
        key: 'member_num',
    }, {
        title: '成立时间',
        dataIndex: 'founded_date',
        key: 'founded_date',
        render: (text: any, record: any, index: number) => {
            return moment(text).format('YYYY-MM-DD');
        }
    }, {
        title: '队长姓名',
        dataIndex: 'captain_name',
        key: 'captain_name',
    }, {
        title: '队长电话',
        dataIndex: 'captain_telephone',
        key: 'captain_telephone',
    }, {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: '团体简介',
        dataIndex: 'description',
        key: 'description',
        width: 600,
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            org_list: []
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeGroups);
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeGroups + '/' + contents.id);
        }
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入名称",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.input,
                    label: "队长姓名",
                    decorator_id: "captain_name",
                    option: {
                        placeholder: "请输入队长姓名",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_groups_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_groups'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：团体列表
 * 描述
 */
@addon('GroupsListView', '团体列表', '描述')
@reactControl(GroupsListView, true)
export class GroupsListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}