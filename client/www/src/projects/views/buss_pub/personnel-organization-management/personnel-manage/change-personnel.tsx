import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { IDCard } from "src/business/device/id_card";
import { isPermission } from "src/projects/app/permission";
import { UserEntry } from 'src/projects/components/user-entry';
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { message } from "antd";
import moment from "moment";

/**
 * 组件：人员清单状态
 */
export interface ChangePersonnelViewState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** 用户数组 */
    base_user?: [];
    /** 身份证 */
    id_card?: IDCard;
}

/**
 * 组件：人员清单视图
 */
export default class ChangePersonnelView extends ReactView<ChangePersonnelViewControl, ChangePersonnelViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            base_user: [],
            id: '',
            id_card: {}
        };
    }
    sum_cb = (err: any, value: any) => {
        let { personnel_category } = this.props;
        let id = this.props.match!.params.key;
        let per_info: any = {
            "id": id,
            "name": undefined,
            "town": undefined,
            'personnel_type': '1',
            "admin_area_id": undefined,
            "id_card_type": undefined,
            "id_card": undefined,
            'organization_id': undefined,
            "login_info": [
                {
                    "login_type": "account",
                    "login_check": {
                        "account_name": undefined,
                        "password": undefined
                    }
                }
            ],
            "personnel_info": {
                "name": undefined,
                "personnel_category": personnel_category ? personnel_category : undefined,
                "personnel_classification": undefined,
                "id_card": undefined,
                "sex": undefined,
                "telephone": undefined,
                "date_birth": undefined,
                'nation': undefined,
                'native_place': undefined,
                'address': undefined,
                'card_number': undefined,
                'remarks': undefined,
                'role_id': undefined,
                'family_name': undefined,
                'fixed_telephone': undefined,
                'card_name': undefined,
                'id_card_address': undefined,
                "guardian_name": undefined,
                "guardian_id_card": undefined,
                "guardian_date_birth": undefined,
                "guardian_sex": undefined,
                "guardian_age": undefined,
                "guardian_dress": undefined,
                "guardian_telephone": undefined,
                "guardian_remarks": undefined,
                'picture': undefined,
                'die_state': undefined,
                'is_member': undefined,
                'marriage_state': undefined,
            }
        };

        for (let obj in per_info) {
            if (obj === 'login_info') {
                if (Object.prototype.toString.call(value['account_name']) === '[object Array]') {
                    per_info[obj][0]['login_check']['account_name'] = value['account_name'][0];
                } else {
                    per_info[obj][0]['login_check']['account_name'] = value['account_name'];
                }
            } else if (obj === 'personnel_info') {
                for (let per in per_info[obj]) {
                    if (per) {
                        per_info[obj][per] = value[per];
                    }
                }
            } else if (obj === "id") {
                if (value.hasOwnProperty(obj)) {
                    per_info[obj] = value[obj];
                } else {
                    continue;
                }
            } else {
                if (value[obj]) {
                    per_info[obj] = value[obj];
                }
            }
        }
        // 处理日期不对的问题
        if (per_info.personnel_info && per_info.personnel_info.date_birth) {
            per_info['personnel_info']['date_birth'] = moment(per_info.personnel_info.date_birth).format('YYYY-MM-DD HH:mm:ss');
        }
        let savePromise = AppServiceUtility.person_org_manage_service[this.props.submit_face!](per_info) as Promise<any>;
        request(this, savePromise)
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    history!.back();
                    // this.props.history!.push(this.props.back_url!);
                } else {
                    message.error(data);
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let sum_btn = {
            text: "保存",
            cb: this.sum_cb
        };
        return (
            <MainContent>
                <UserEntry
                    is_detailed_info={true}
                    personnel_category={this.props.personnel_category}
                    base_info_submit_btn_propps={sum_btn}
                    select_face={this.props.select_face}
                    detailed_info_succ_func={() => { history!.back(); }}
                    id={this.props.match!.params.key}
                    detailed_info_other_btn_propps={
                        [
                            {
                                text: "返回",
                                cb: () => {
                                    history!.back();
                                    // this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.personnel);
                                }
                            }
                        ]
                    }
                />
            </MainContent>
        );
    }
}

/**
 * 控件：人员清单编辑控件
 * @description 人员清单编辑控件
 * @author
 */
@addon('ChangePersonnelView', '人员清单编辑控件', '人员清单编辑控件')
@reactControl(ChangePersonnelView, true)
export class ChangePersonnelViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 上一页url */
    public back_url?: string;
    /** 提交接口 */
    public submit_face?: string;
    /** 查询接口 */
    public select_face?: string;
    /** 人员类型 */
    public personnel_category?: string;
}