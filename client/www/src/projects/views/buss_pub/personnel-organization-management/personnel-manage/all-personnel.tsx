import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { message } from "antd";
// import { remote } from "src/projects/remote";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { Select } from 'antd';
let { Option } = Select;

/** 状态：人员清单视图 */
export interface AllPersonnelViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 接口名 */
    request_url?: string;
    select_type?: any;
    /** 对话框显示状态 */
    visible?: boolean;
    /** 导入参数 */
    config: any;
    /** 组织机构列表 */
    organization_list?: any;
}
/** 组件：人员清单视图 */
export class AllPersonnelView extends React.Component<AllPersonnelViewControl, AllPersonnelViewState> {
    private columns_data_source = [{
        title: '证件号码',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '证件类型',
        dataIndex: 'id_card_type',
        key: 'id_card_type',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '性别',
        dataIndex: 'personnel_info.sex',
        key: 'personnel_info.sex',
    },
    {
        title: '年龄',
        dataIndex: 'personnel_info.age',
        key: 'personnel_info.age',
    }, {
        title: '出生日期',
        dataIndex: 'personnel_info.date_birth',
        key: 'personnel_info.date_birth',
    },
    {
        title: '家庭住址',
        dataIndex: 'personnel_info.address',
        key: 'personnel_info.address',
    },
    {
        title: '人员类别',
        dataIndex: 'personnel_info.personnel_category',
        key: 'personnel_info.personnel_category',
    },
    {
        title: '所属组织机构',
        dataIndex: 'org.name',
        key: 'org.name',
    },
    {
        title: '创建人',
        dataIndex: 'founder',
        key: 'founder',
    },
    {
        title: '创建时间',
        dataIndex: 'create_time',
        key: 'create_time',
    },
    {
        title: '最后修改人',
        dataIndex: '',
        key: '',
    },
    {
        title: '最后修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },
    ];
    constructor(props: AllPersonnelViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10],
            request_url: '',
            select_type: {},
            visible: false,
            config: {
                isFormat: true,
                upload: false,
                func: AppServiceUtility.person_org_manage_service.import_excel_manage,
                title: '导入人员'
            }
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changePersonnel);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(this.props.change_url ? this.props.change_url + '/' + contents.id : ROUTE_PATH.changePersonnel + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        let select_type = this.props.select_type;
        this.setState({
            request_url,
            select_type
        });
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_list!({})!
            .then((data) => {
                if (data.result) {
                    this.setState({
                        organization_list: data.result
                    });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    /** 导入接口 */
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    updateOnchange = (data?: any) => {
        if (data.file.response && data.file.response.code && data.file.response.code === 200) {
            let item = data.file.response.url;
            AppServiceUtility!.iexcel_service!.import_excel_user_nh!({ url: item })!
                .then((res: any) => {
                    if (res === 'Success') {
                        message.info('保存成功');
                        this.setState({
                            visible: !this.state.visible
                        });
                        history!.back();
                    } else {
                        message.error(res);
                    }
                });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let type = ['长者', '工作人员'];
        let type_dow: JSX.Element[] = type.map((item, idx) => <Option key={item}>{item}</Option>);
        let marriage_state: any[] = ['未婚', '已婚', '再婚', '离异', '丧偶'];
        let marriage_state_list: JSX.Element[] = marriage_state.map((item, idx) => <Option key={item}>{item}</Option>);
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "证件号码",
                    decorator_id: "id_card"
                }, {
                    type: InputType.input,
                    label: "组织机构",
                    decorator_id: "organization_name"
                },
                {
                    type: InputType.input,
                    label: "户籍地址",
                    decorator_id: "address"
                },
                {
                    type: InputType.radioGroup,
                    label: "在世状态",
                    decorator_id: "die_state",
                    option: {
                        options: [
                            {
                                label: '健在',
                                value: '健在'
                            },
                            {
                                label: '已故',
                                value: '已故'
                            },
                        ],
                    }
                },
                {
                    type: InputType.radioGroup,
                    label: "是否会员",
                    decorator_id: "is_member",
                    option: {
                        options: [
                            {
                                label: '是',
                                value: '是'
                            },
                            {
                                label: '否',
                                value: '否'
                            },
                        ],
                    }
                },
                {
                    type: InputType.select,
                    label: "婚姻状态",
                    decorator_id: "marriage_state",
                    option: {
                        placeholder: "请选择婚姻状态",
                        childrens: marriage_state_list,
                    }
                },
                {
                    type: InputType.select,
                    label: "人员类型",
                    decorator_id: "type",
                    option: {
                        placeholder: "请选择人员类型",
                        childrens: type_dow,
                    }
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            },
                // excel导入
                // {
                //     label: '导入',
                //     btn_method: this.upload,
                //     icon: 'plus'
                // }
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.state.select_type ? this.state.select_type : {}, 1, 10]
                },
                delete: {
                    service_func: this.props.delete_url
                }
            },
            searchExtraParam: this.state.select_type,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            select_type: this.props.select_type
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...personnel_list} />
                    <UploadFile {...this.state.config} />
                </div>
            )
        );
    }
}

/**
 * 控件：人员清单视图控制器
 * @description 人员清单视图
 * @author
 */
@addon('AllPersonnelView', '人员清单视图', '人员清单视图')
@reactControl(AllPersonnelView, true)
export class AllPersonnelViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public delete_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 查询条件 */
    public select_type?: any;
}