import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { message } from "antd";
// import { remote } from "src/projects/remote";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { Select } from 'antd';
import moment from "moment";
// import { getAgeFromDateBirth } from "src/projects/views/buss_mis/older-birth-notify";
import { exprot_excel } from "src/business/util_tool";
import { remote } from "src/projects/remote";
import { getAge } from "src/projects/app/util-tool";
let { Option } = Select;

/** 状态：人员清单视图 */
export interface PersonnelViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 接口名 */
    request_url?: string;
    select_type?: any;
    /** 对话框显示状态 */
    visible?: boolean;
    /** 导入参数 */
    config: any;
    old_type_name?: any;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：人员清单视图 */
export class PersonnelView extends React.Component<PersonnelViewControl, PersonnelViewState> {
    private formCreator: any = null;
    constructor(props: PersonnelViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10],
            request_url: '',
            select_type: {},
            visible: false,
            config: {
                isFormat: true,
                upload: false,
                func: AppServiceUtility.person_org_manage_service.import_elder,
                title: '导入人员'
            },
            old_type_name: [],
            org_list: [],
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changePersonnel);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(this.props.change_url ? this.props.change_url + '/' + contents.id : ROUTE_PATH.changePersonnel + '/' + contents.id);
        }
    }
    componentWillMount() {
        // 长者类型列表
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({})!
            .then((data: any) => {
                this.setState({
                    old_type_name: data.result
                });
            });
        let request_url = this.props.request_url;
        let select_type = this.props.select_type;
        this.setState({
            request_url,
            select_type
        });
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 导入接口 */
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    updateOnchange = (data?: any) => {
        if (data.file.response && data.file.response.code && data.file.response.code === 200) {
            let item = data.file.response.url;
            AppServiceUtility!.iexcel_service!.import_excel_user_nh!({ url: item })!
                .then((res: any) => {
                    if (res === 'Success') {
                        message.info('保存成功');
                        this.setState({
                            visible: !this.state.visible
                        });
                        history!.back();
                    } else {
                        message.error(res);
                    }
                });
        }
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    // 下载
    download = () => {
        let _this = this.formCreator;
        AppServiceUtility.person_org_manage_service.get_elder_list_all!(_this.state.condition)!
            .then((data: any) => {
                if (data.result.length) {
                    let new_data: any = [];
                    let file_name = '长者档案';
                    if (this.props.type) {
                        data.result.map((item: any) => {
                            new_data.push({
                                "社区": item.admin_name,
                                "长者姓名": item.name,
                                "身份证": item.id_card,
                                "性别": item.personnel_info.sex,
                                "出生日期": item.personnel_info.date_birth,
                                "年龄": item.id_card.length === 18 ? getAge(item.id_card) : item.personnel_info.age,
                                "地址": item.personnel_info.address,
                                "养老类型": item.old_type,
                                "长者类型": item.old_type_name,
                                "在世状态": item.personnel_info.die_state,
                            });
                        });
                    } else {
                        data.result.map((item: any) => {
                            new_data.push({
                                "社区": item.admin_name,
                                "证件号码": item.id_card,
                                "证件类型": item.id_card_type,
                                "姓名": item.name,
                                "性别": item.personnel_info.sex,
                                "年龄": item.id_card.length === 18 ? getAge(item.id_card) : item.personnel_info.age,
                                "出生日期": item.personnel_info.date_birth,
                                "家庭住址": item.personnel_info.address,
                                "人员类别": item.old_type_name,
                                "所属组织机构": item.org.name,
                                "创建人": item.founders,
                                "创建时间": item.create_time,
                                "最后修改人": item.last_modifier,
                                "最后修改时间": item.modify_date,
                            });
                        });
                    }
                    exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
                } else {
                    message.info('暂无数据可导', 2);
                }
            });
    }
    // 下载导入模板
    downloadTemp = () => {
        window.location.href = remote.path + "/build/导入长者模板.xls";
    }
    render() {
        let columns_data_source: any = [];
        let edit_form_items_props: any = [];
        let marriage_state: any[] = ['未婚', '已婚', '再婚', '离异', '丧偶'];
        let old_type: any = ['政府补贴', '自费'];
        let die_state: any = ['健在', '已故'];
        let marriage_state_list: JSX.Element[] = marriage_state.map((item, idx) => <Option key={item}>{item}</Option>);
        let old_type_list: JSX.Element[] = old_type.map((item: any, idx: any) => <Option key={item}>{item}</Option>);
        let old_type_name_list: JSX.Element[] = this.state.old_type_name.map((item: any, idx: any) => <Option key={idx} value={item.name}>{item.name}</Option>);
        let die_state_list: JSX.Element[] = die_state.map((item: any, idx: any) => <Option key={item}>{item}</Option>);
        if (this.props.type) {
            columns_data_source = [{
                title: '社区',
                dataIndex: 'admin_name',
                key: 'admin_name',
            }, {
                title: '长者姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证',
                dataIndex: 'id_card',
                key: 'id_card',
            }, {
                title: '性别',
                dataIndex: 'personnel_info.sex',
                key: 'personnel_info.sex',
            },
            {
                title: '出生日期',
                dataIndex: 'personnel_info.date_birth',
                key: 'personnel_info.date_birth',
                render: (text: any, record: any) => {
                    if (text) {
                        return moment(new Date(text)).format('YYYY-MM-DD');
                    }
                    return '';
                }
            }, {
                title: '年龄',
                dataIndex: 'personnel_info.age',
                key: 'personnel_info.age',
                render: (text: any, record: any) => {
                    if (record.id_card && record.id_card.length === 18) {
                        return getAge(record.id_card);
                    } else if (text) {
                        return text;
                    }
                    return '';
                }
            },
            {
                title: '地址',
                dataIndex: 'personnel_info.address',
                key: 'personnel_info.address',
            },
            {
                title: '养老类型',
                dataIndex: 'old_type',
                key: 'old_type',
            },
            {
                title: '长者类型',
                dataIndex: 'old_type_name',
                key: 'old_type_name',
            },
            {
                title: '在世状态',
                dataIndex: 'personnel_info.die_state',
                key: 'personnel_info.die_state',
            },
            ];
            edit_form_items_props = [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入长者姓名",
                    }
                },
                {
                    type: InputType.input,
                    label: "长者身份证",
                    decorator_id: "id_card",
                    option: {
                        placeholder: "请输入长者身份证",
                    }
                },
                {
                    type: InputType.select,
                    label: "养老类型",
                    decorator_id: "old_type",
                    option: {
                        placeholder: "请选择养老类型",
                        childrens: old_type_list,
                    }
                },
                {
                    type: InputType.select,
                    label: "长者类型",
                    decorator_id: "old_type_name",
                    option: {
                        placeholder: "请选择长者类型",
                        childrens: old_type_name_list,
                    }
                },
                {
                    type: InputType.select,
                    label: "在世状态",
                    decorator_id: "die_state",
                    option: {
                        placeholder: "请选择在世状态",
                        childrens: die_state_list,
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "organization_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },

            ];
        } else {
            columns_data_source = [{
                title: '社区',
                dataIndex: 'admin_name',
                key: 'admin_name',
            }, {
                title: '证件号码',
                dataIndex: 'id_card',
                key: 'id_card',
            }, {
                title: '证件类型',
                dataIndex: 'id_card_type',
                key: 'id_card_type',
            }, {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '性别',
                dataIndex: 'personnel_info.sex',
                key: 'personnel_info.sex',
            },
            {
                title: '年龄',
                dataIndex: 'personnel_info.age',
                key: 'personnel_info.age',
                render: (text: any, record: any) => {
                    if (record.id_card && record.id_card.length === 18) {
                        return getAge(record.id_card);
                    } else {
                        return '暂无数据';
                    }
                }
            }, {
                title: '出生日期',
                dataIndex: 'personnel_info.date_birth',
                key: 'personnel_info.date_birth',
                render: (text: any, record: any) => {
                    if (record.personnel_info.date_birth) {
                        return record.personnel_info.date_birth.split(' ')[0];
                    } else {
                        return '暂无数据';
                    }
                }
            },
            {
                title: '家庭住址',
                dataIndex: 'personnel_info.address',
                key: 'personnel_info.address',
            },
            {
                title: '长者类型',
                dataIndex: 'old_type_name',
                key: 'old_type_name',
            },
            {
                title: '所属组织机构',
                dataIndex: 'org.name',
                key: 'org.name',
            },
            {
                title: '创建人',
                dataIndex: 'founders',
                key: 'founders',
                render: (text: any, record: any) => {
                    if (record.founders) {
                        return record.founders;
                    } else {
                        return 'admin';
                    }
                }
            },
            {
                title: '创建时间',
                dataIndex: 'create_date',
                key: 'create_date',
                render: (text: any, record: any) => {
                    if (record.create_time) {
                        return record.create_time;
                    } else if (record.create_date) {
                        return record.create_date;
                    }
                }
            },
            {
                title: '最后修改人',
                dataIndex: 'last_modifier',
                key: 'last_modifier',
                render: (text: any, record: any) => {
                    if (record.last_modifier) {
                        return record.last_modifier;
                    } else {
                        return 'admin';
                    }
                }
            },
            {
                title: '最后修改时间',
                dataIndex: 'modify_date',
                key: 'modify_date',
            },
            ];
            edit_form_items_props = [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "证件号码",
                    decorator_id: "id_card"
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "organization_name"
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "organization_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.input,
                    label: "家庭住址",
                    decorator_id: "address"
                },
                {
                    type: InputType.radioGroup,
                    label: "在世状态",
                    decorator_id: "die_state",
                    option: {
                        options: [
                            {
                                label: '健在',
                                value: '健在'
                            },
                            {
                                label: '已故',
                                value: '已故'
                            },
                        ],
                    }
                },
                {
                    type: InputType.radioGroup,
                    label: "是否会员",
                    decorator_id: "is_member",
                    option: {
                        options: [
                            {
                                label: '是',
                                value: '是'
                            },
                            {
                                label: '否',
                                value: '否'
                            },
                        ],
                    }
                },
                {
                    type: InputType.select,
                    label: "婚姻状态",
                    decorator_id: "marriage_state",
                    option: {
                        placeholder: "请选择婚姻状态",
                        childrens: marriage_state_list,
                    }
                },
                {
                    type: InputType.input,
                    label: "社区",
                    decorator_id: "admin_name",
                    option: {
                        placeholder: "请输入社区",
                    }
                },
                {
                    type: InputType.select,
                    label: "长者类型",
                    decorator_id: "old_type_name",
                    option: {
                        placeholder: "请选择长者类型",
                        childrens: old_type_name_list,
                    }
                },
            ];
        }
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: edit_form_items_props,
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            },
            {
                label: '导出',
                btn_method: this.download,
                icon: 'download'
            },
            // excel导入
            {
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            },
            // 下载导入模板
            {
                label: '下载模板',
                btn_method: this.downloadTemp,
                icon: 'download'
            }
            ],
            onRef: this.onRef,
            columns_data_source: columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.state.select_type ? this.state.select_type : {}, 1, 10]
                },
                delete: {
                    service_func: this.props.delete_url
                }
            },
            searchExtraParam: this.state.select_type,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            select_type: this.props.select_type
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...personnel_list} />
                    <UploadFile {...this.state.config} />
                </div>
            )
        );
    }
}

/**
 * 控件：人员清单视图控制器
 * @description 人员清单视图
 * @author
 */
@addon('PersonnelView', '人员清单视图', '人员清单视图')
@reactControl(PersonnelView, true)
export class PersonnelViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public delete_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 查询条件 */
    public select_type?: any;
    /** 入口 */
    public type?: any;
}