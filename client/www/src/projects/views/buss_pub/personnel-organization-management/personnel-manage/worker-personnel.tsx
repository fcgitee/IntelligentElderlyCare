import { Button, message, Select } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { remote } from "src/projects/remote";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;
/** 状态：工作人员视图 */
export interface WorkerPersonnelViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 接口名 */
    request_url?: string;
    select_type?: any;
    /** 对话框显示状态 */
    visible?: boolean;
    /** 导入参数 */
    config: any;
    /** 组织机构列表 */
    // organization_list?: any;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：工作人员视图 */
export class WorkerPersonnelView extends React.Component<WorkerPersonnelViewControl, WorkerPersonnelViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '证件号码',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '证件类型',
        dataIndex: 'id_card_type',
        key: 'id_card_type',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '性别',
        dataIndex: 'personnel_info.sex',
        key: 'personnel_info.sex',
    },
    {
        title: '年龄',
        dataIndex: 'personnel_info.age',
        key: 'personnel_info.age',
    }, {
        title: '出生日期',
        dataIndex: 'personnel_info.date_birth',
        key: 'personnel_info.date_birth',
    },
    {
        title: '家庭住址',
        dataIndex: 'personnel_info.address',
        key: 'personnel_info.address',
    },
    // {
    //     title: '人员类别',
    //     dataIndex: 'personnel_info.personnel_category',
    //     key: 'personnel_info.personnel_category',
    // },
    {
        title: '所属组织机构',
        dataIndex: 'org.name',
        key: 'org.name',
    },
    // {
    //     title: '角色类型',
    //     dataIndex: 'role_name',
    //     key: 'role_name',
    // },
    {
        title: '在职状态',
        dataIndex: 'personnel_info.work_state',
        key: 'personnel_info.work_state',
    },
    {
        title: '职务',
        dataIndex: 'personnel_info.post',
        key: 'personnel_info.post',
    },
    {
        title: '创建人',
        dataIndex: 'founders',
        key: 'founders',
    },
    {
        title: '创建时间',
        dataIndex: 'create_time',
        key: 'create_time',
    },
    {
        title: '最后修改人',
        dataIndex: 'last_modifier',
        key: 'last_modifier',
    },
    {
        title: '最后修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },
    {
        title: '启用/停用',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            record['account_status'] && record['account_status'] === '停用' ?
                <div style={{ marginRight: '5px' }}>已停用<Button type="primary" onClick={() => { this.start(record.id); }}>启用</Button></div>
                :
                <div style={{ marginRight: '5px' }}>已启用<Button type="danger" onClick={() => { this.stop(record.id); }}>停用</Button></div>)
    }];
    constructor(props: WorkerPersonnelViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10],
            request_url: '',
            select_type: {},
            visible: false,
            config: {
                isFormat: true,
                upload: false,
                func: AppServiceUtility.person_org_manage_service.import_excel_manage,
                title: '导入人员'
            },
            org_list: [],
        };
    }
    /** 启用 */
    start = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_start_stop!({ id, "show": "启用" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('启用成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 停用 */
    stop = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_start_stop!({ id, "show": "停用" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('冻结成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changePersonnel);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(this.props.change_url ? this.props.change_url + '/' + contents.id : ROUTE_PATH.changePersonnel + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        let select_type = this.props.select_type;
        this.setState({
            request_url,
            select_type
        });
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // AppServiceUtility.person_org_manage_service.get_organization_list!({})!
        //     .then((data) => {
        //         if (data.result) {
        //             this.setState({
        //                 organization_list: data.result
        //             });
        //         }
        //     })
        //     .catch((err) => {
        //         console.info(err);
        //     });
    }
    /** 导入接口 */
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    updateOnchange = (data?: any) => {
        if (data.file.response && data.file.response.code && data.file.response.code === 200) {
            let item = data.file.response.url;
            AppServiceUtility!.iexcel_service!.import_excel_user_nh!({ url: item })!
                .then((res: any) => {
                    if (res === 'Success') {
                        message.info('保存成功');
                        this.setState({
                            visible: !this.state.visible
                        });
                        history!.back();
                    } else {
                        message.error(res);
                    }
                });
        }
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const work_state = ['在职', '离职'];
        const work_state_list: JSX.Element[] = work_state!.map((item: any) => <Option key={item} value={item}>{item}</Option>);
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "证件号码",
                    decorator_id: "id_card"
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "organization_name"
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "organization_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: "状态",
                    decorator_id: "work_state",
                    option: {
                        placeholder: "请选择状态",
                        childrens: work_state_list
                    }
                },
                {
                    type: InputType.input,
                    label: "账号",
                    decorator_id: "account_name"
                },
                // {
                //     type: InputType.input,
                //     label: "角色类型",
                //     decorator_id: "role_name"
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            },
                // excel导入
                // {
                //     label: '导入',
                //     btn_method: this.upload,
                //     icon: 'plus'
                // }
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.state.select_type ? this.state.select_type : { type: this.props.type }, 1, 10]
                },
                delete: {
                    service_func: this.props.delete_url
                }
            },
            searchExtraParam: this.state.select_type,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            select_type: this.props.select_type
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...personnel_list} />
                    <UploadFile {...this.state.config} />
                </div>
            )
        );
    }
}

/**
 * 控件：工作人员视图控制器
 * @description 工作人员视图
 * @author
 */
@addon('PersonnelView', '工作人员视图', '工作人员视图')
@reactControl(WorkerPersonnelView, true)
export class WorkerPersonnelViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public delete_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 查询条件 */
    public select_type?: any;
}