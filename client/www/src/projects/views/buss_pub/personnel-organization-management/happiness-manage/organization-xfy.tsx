
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { Tree, Button, message } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { request } from "src/business/util_tool";

/** 状态：幸福院组织机构视图 */
export interface OrganizationXFYViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
    /** 接口名 */
    request_url?: string;
    /** 树形结构 */
    tree?: any;
}
/** 组件：幸福院组织机构视图 */
export class OrganizationXFYView extends React.Component<OrganizationXFYViewControl, OrganizationXFYViewState> {
    constructor(props: OrganizationXFYViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {},
            request_url: '',
            tree: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeOrganization);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: any, contents: any) => {
        // 根据selectedNodes去确定是行政区划还是组织机构
        let is_division = contents && contents.hasOwnProperty('selectedNodes') && contents['selectedNodes'][0] && contents['selectedNodes'][0].hasOwnProperty('props') && contents['selectedNodes'][0]['props'].hasOwnProperty('is_division') ? contents['selectedNodes'][0]['props']['is_division'] : '';
        // console.log(type[0], is_division);
        if (is_division === false) {
            this.props.history!.push(ROUTE_PATH.changeOrganization + '/' + type[0]);
        } else if (is_division === true) {
            this.props.history!.push(ROUTE_PATH.changeAdministrationDivision + '/' + type[0]);
        } else {
            message.error('未知类型！');
        }
    }
    componentWillMount() {
        let { request_url, select_param } = this.props;
        request(this, AppServiceUtility.person_org_manage_service[request_url!](select_param || {}))
            .then((data: any) => {
                // console.log("树形结构", data);
                if (data.result.length > 0) {
                    this.setState({
                        tree: data.result,
                    });
                }
            }).catch((error: Error) => {
                message.error(error.message);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        return (
            <MainContent>
                <MainCard title={'幸福院'}>
                    <Button type={'primary'} onClick={this.add}>新增幸福院</Button>
                    <MainContent>
                        <Tree
                            defaultExpandAll={true}
                            showIcon={true}
                            treeData={this.state.tree}
                            onSelect={this.onIconClick}
                        />
                    </MainContent>
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：幸福院组织机构视图控制器
 * @description 幸福院组织机构视图
 * @author
 */
@addon('OrganizationXFYView', '幸福院组织机构视图', '幸福院组织机构视图')
@reactControl(OrganizationXFYView, true)
export class OrganizationXFYViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public select_param?: any;

}