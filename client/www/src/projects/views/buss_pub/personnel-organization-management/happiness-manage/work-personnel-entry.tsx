import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, message, Modal, Card } from "antd";
import { MainContent } from "src/business/components/style-components/main-content";
const { confirm } = Modal;
/**
 * 组件：编辑组织机构状态
 */
export interface SheQuWorkerListViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    visible?: any;
    photo?: any;
}

/**
 * 组件：编辑组织机构视图
 */
export class SheQuWorkerListView extends ReactView<SheQuWorkerListViewControl, SheQuWorkerListViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '序号',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '风貌',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <img onClick={() => { this.photoClick(record); }} style={{ width: '70px', height: '70px' }} src={record.personnel_info.picture} alt="" />
        ),
    },
    {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '职务',
        dataIndex: 'personnel_info.post',
        key: 'personnel_info.post',
    },
    {
        title: '联系电话',
        dataIndex: 'personnel_info.telephone',
        key: 'personnel_info.telephone',
    },
    {
        title: '从业年限',
        dataIndex: 'personnel_info.work_year',
        key: 'personnel_info.remarks',
    },
    {
        title: '个人介绍',
        dataIndex: 'personnel_info.personnel_introduce',
        key: 'personnel_info.personnel_introduce',
    },
    {
        title: '操作',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <div>最近编辑时间：</div>
                    <div>{record.modify_date}</div>
                </div>
                <div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>编辑</Button>
                        <Button type="danger" onClick={() => { this.del(record.id); }}>删除</Button>
                    </div>
                </div>
            </div>
        ),
    }];
    constructor(props: SheQuWorkerListViewControl) {
        super(props);
        this.state = {
            visible: false,
            photo: ''
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changewokerPersonnel);
    }
    /** 编辑 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.changewokerPersonnel + '/' + record.id);
    }

    onRef = (ref: any) => {
        this.formCreator = ref;
    }

    // 删除
    del = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                AppServiceUtility.person_org_manage_service.delete_worker_info!([ids])!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功', 1, () => {
                                if (this.formCreator && this.formCreator!.reflash) {
                                    this.formCreator!.reflash();
                                }
                            });
                        }
                    })
                    .catch(error => {
                        message.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
    }

    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }

    photoClick = (record: any) => {
        this.setState({
            visible: true,
            photo: record.personnel_info.picture
        });
    }

    handleOk = (e: any) => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e: any) => {
        this.setState({
            visible: false,
        });
    }
    render() {
        let service_person = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "员工名称",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入要搜索的员工名称",
                        autoComplete: 'off',
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 1]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let service_person_list = Object.assign(service_person, table_param);
        return (
            <MainContent>
                <Card title="社区人员信息">
                    <SignFrameLayout {...service_person_list} />
                    <Modal
                        title="人员信息"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        <div style={{ height: '100%', width: '100%', textAlign: 'center' }}>
                            <img style={{ width: '400px', height: '400px' }} src={this.state.photo} alt="" />
                        </div>
                    </Modal>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：社区服务人员控件
 * @description 社区服务人员控件
 * @author
 */
@addon('SheQuWorkerListView', '社区服务人员控件', '社区服务人员控件')
@reactControl(SheQuWorkerListView, true)
export class SheQuWorkerListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}