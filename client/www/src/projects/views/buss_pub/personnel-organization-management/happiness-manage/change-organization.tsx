import { message, Tabs, Button, Form, Row, Col, Icon, Input, DatePicker, Select, Modal } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import moment from 'moment';
// import { HappinessEntry } from "./entry";
import { MainCard } from "src/business/components/style-components/main-card";
import { OrganizationManageNewView } from "../../../buss_mis/organization-manage/index";
const { TabPane } = Tabs;
const { Option } = Select;
const { MonthPicker } = DatePicker;
const { confirm } = Modal;
import './index.less';
/**
 * 组件：编辑组织机构状态
 */
export interface ChangeHappinessViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** 对方话框 */
    visible?: boolean;
    /** 树形结构 */
    tree?: any;
    show?: any;
    param?: any;
    editShow?: any;
    income_list?: any;
    pageIndex?: any;
}

/**
 * 组件：编辑组织机构视图
 */
export class ChangeHappinessView extends ReactView<ChangeHappinessViewControl, ChangeHappinessViewState> {
    private columns_data_source = [{
        title: '年份',
        dataIndex: 'year',
        key: 'year',
        render: (text: any, record: any) => {
            return record.year.split(' ')[0].split('-')[0];
        },
    }, {
        title: '年度建设总经费',
        dataIndex: 'year_all_amout',
        key: 'year_all_amout',
    }, {
        title: '是否获得评比奖励',
        dataIndex: 'is_reward',
        key: 'is_reward',
    }, {
        title: '评比奖励',
        dataIndex: 'reward_amout',
        key: 'reward_amout',
    }, {
        title: '其他投入',
        dataIndex: 'other_input',
        key: 'other_input',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    },
    {
        title: '操作',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <div>最近编辑时间：</div>
                    <div>{record.modify_date}</div>
                </div>
                <div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.editYunYing(record); }}>编辑</Button>
                        <Button type="danger" onClick={() => { this.showDeleteConfirm(record.id); }}>删除</Button>
                    </div>
                </div>
            </div>

        ),
    }];
    private Table: any;
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
            visible: false,
            tree: [],
            show: false,
            param: undefined,
            editShow: false,
            income_list: {},
            pageIndex: '1'
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
        let id = this.state.param;
        let org_info: any = {};
        if (id) {
            org_info = { "id": id };
        }
        if (values) {
            org_info = {
                ...org_info,
                "name": undefined,
                "personnel_type": '2',
                "town": undefined,
                'admin_area_id': undefined,
                "reg_date": undefined,
                "organization_info": {
                    "describe": undefined,
                    "picture_list": [],
                    "telephone": undefined,
                    "organization_nature": undefined,
                    "lon": undefined,
                    street: undefined,
                    rusticate: undefined,
                    bulid_type: undefined,
                    build_area: undefined,
                    cover_area: undefined,
                    day_bed_num: undefined,
                    post_code: undefined,
                    open_days: undefined,
                    start_business_time: undefined,
                    end_business_time: undefined,
                    introduce: undefined,
                    land_deed_file: undefined,
                    property_file: undefined,
                    health_license_no: undefined,
                    fire_safety_permit_file: undefined,
                    house_check_report: undefined,
                    env_protect: undefined,
                    project_report: undefined,
                    buy_dev_bill: undefined,
                    legal_person_id_img: undefined,
                    build_location_img: undefined,
                    build_outer_img: undefined,
                    fire_safety_img: undefined,
                    "lat": undefined,
                    'super_org_id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67',
                    "personnel_category": "平台",
                    'comment': undefined,
                    'address': undefined,
                    'legal_person': undefined,
                    'business_license_url': [],
                    'unified_social_credit': [],
                    'platform_filing_commitment': [],
                    'service_agreement': undefined,
                    'star_level': undefined,
                    'contract_status': undefined,
                    'book_num': undefined,
                    'service_organization': undefined,
                },
                // "qualification_info": [],
            };
            for (let org in org_info) {
                if (org === 'organization_info') {
                    for (let info in org_info[org]) {
                        if (info) {
                            org_info[org][info] = values[info];
                        }
                    }
                } else {
                    if (values[org]) {
                        org_info[org] = values[org];
                    }
                }
            }
            org_info.organization_info.personnel_category = '幸福院';
            org_info.organization_info.super_org_id = '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67';
            AppServiceUtility.person_org_manage_service.update_organization!(org_info)!
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('保存成功');
                        // this.props.history!.push(ROUTE_PATH.organization);
                        this.setState({
                            show: false
                        });
                        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type: '幸福院' })!
                            .then((data: any) => {
                                if (data.result.length > 0) {
                                    this.setState({
                                        tree: data.result,
                                    });
                                }
                            });
                    } else {
                        message.error(data);
                    }
                })
                .catch(error => {
                    message.error(error.message);
                });
        }
    }
    handleOk = () => {
        AppServiceUtility.person_org_manage_service.delete_organization!([this.props.match!.params.key!])!
            .then(data => {
                if (data === 'Success') {
                    message.info('删除成功');
                    this.props.history!.push(ROUTE_PATH.organization);

                } else {
                    message.info(data);
                }
                this.setState({
                    visible: false,
                });
            });
    }
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    /** 新增按钮 */
    add = () => {
        this.setState({
            show: true,
            param: undefined
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: any, contents: any) => {
        this.setState({
            param: contents.selectedNodes[0].props.id,
            show: true
        });
    }

    onRef = (ref: any) => {
        this.Table = ref;
    }

    componentDidMount() {
        // if (this.props.onRef) {
        //     this.props.onRef(this);
        // }
        // let request_url = this.props.request_url;
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type: '幸福院' })!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        tree: data.result,
                    });
                }
            });
        if (this.props.match!.params.key) {
            this.setState({
                param: this.props.match!.params.key,
                show: true
            });
        }
    }

    // onIconClick2 = (type: string, contents: any) => {
    //     if ('icon_edit' === type) {
    //         // console.log('自定义按钮edit返回值：', contents);
    //         this.props.history!.push(ROUTE_PATH.changeMonitor + '/' + contents.id);
    //     }
    // }

    // 新增运营情况
    addYunying = () => {
        this.setState({
            editShow: true,
            income_list: []
        });
    }

    // 表单取消按钮
    cancalEdit = () => {
        this.setState({
            editShow: false
        });
    }

    // 表单提交
    submit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (err) {
                return;
            }
            if (JSON.stringify(this.state.income_list) !== '{}') {
                values['id'] = this.state.income_list.id;
            }
            // 获取当前登陆人信息
            AppServiceUtility.person_org_manage_service.get_current_user_info!()!
                .then((data: any) => {
                    values['organization_id'] = data[0].organization_id;
                    AppServiceUtility.person_org_manage_service.update_business_income!(values)!
                        .then((data: any) => {
                            if (data === 'Success') {
                                message.info('保存成功');
                                values = {};
                                // this.props.history!.push(ROUTE_PATH.organization);
                                this.setState({
                                    editShow: false,
                                    income_list: []
                                });
                                this.Table.reflash();
                                this.props.form.resetFields();
                                // this.props.history!.push(ROUTE_PATH.HappinessEntryControl + '/' + '2');
                            } else {
                                message.error(data);
                            }
                        })
                        .catch(error => {
                            message.error(error.message);
                        });
                });
        });
    }

    // 编辑
    editYunYing = (data: any) => {
        AppServiceUtility.person_org_manage_service.get_business_income!({ id: data.id })!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        income_list: data.result[0],
                        editShow: true
                    });
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }

    // 删除
    showDeleteConfirm = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk() {
                AppServiceUtility.person_org_manage_service.delete_business_income_byid!({ id: ids })!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功');
                        }
                    })
                    .catch(error => {
                        message.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
        this.Table.reflash();
    }

    tabChange = (key: any) => {
        this.setState({
            pageIndex: key
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let base_data = this.state.base_data;
        // let submit_btn_propps = {
        //     text: "保存",
        //     cb: this.handleSubmit
        // };
        let other_btn_propps = [
            {
                text: "返回",
                cb: () => {
                    // history.back();
                    this.setState({
                        show: false,
                        param: undefined
                    });
                    // this.props.history!.push(ROUTE_PATH.organization);
                }
            },
        ];
        if (this.props.match!.params.key) {
            let del_btn = {
                text: "删除",
                cb: () => {
                    this.setState({
                        visible: true,
                    });
                }
            };
            other_btn_propps.push(del_btn);
        }
        // // 新增幸福院
        // let service_option = {
        //     service_object: AppServiceUtility.person_org_manage_service,
        //     operation_option: {
        //         save: {
        //             func_name: "update_organization"
        //         },
        //         query: {
        //             func_name: "",
        //             arguments: [{ id: this.state.param }, 1, 1]
        //         }
        //     },
        // };
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 10 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 14 },
                sm: { span: 16 },
            },
        };
        // 运营情况
        let service_item = {
            type_show: false,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_business_income',
                    service_condition: [{}]
                },
                delete: {
                    service_func: ''
                }
            },
            onRef: this.onRef,
        };
        let service_item_list = Object.assign(service_item, table_param);
        // let succ_func = () => { this.props.history!.push(ROUTE_PATH.organization); }
        return (
            (
                <MainContent>
                    <Tabs type="card" activeKey={this.state.pageIndex} onChange={this.tabChange}>
                        <TabPane tab="基本信息" key="1">
                            {/* {this.state.show ?
                                <div>
                                    <Button type={'primary'} onClick={() => { this.setState({ show: false, param: undefined }); }}>返回</Button>
                                    <HappinessEntry
                                        base_data={base_data}
                                        id={this.state.param}
                                        submit_btn_propps={submit_btn_propps}
                                        other_btn_propps={other_btn_propps}
                                        service_option={service_option}
                                    />
                                </div>
                                :
                                // <MainCard title={'组织机构'}>
                                //     <Button type={'primary'} onClick={this.add}>新增组织机构</Button>
                                //     <MainContent>
                                //         <Tree
                                //             defaultExpandAll={true}
                                //             showIcon={true}
                                //             treeData={this.state.tree}
                                //             onSelect={this.onIconClick}
                                //         />
                                //     </MainContent>
                                // </MainCard>
                            } */}
                            <MainCard>
                                <MainContent>
                                    <OrganizationManageNewView
                                        permission={{ 'permission': '组织机构查询' }}
                                        permission_class={AppServiceUtility.login_service}
                                        get_permission_name='get_function_list'
                                        request_url='get_organization_list_new'
                                        select_permission='组织机构查询'
                                        edit_permission='组织机构编辑'
                                        add_permission='组织机构新增'
                                        org_type='幸福院'
                                    />
                                </MainContent>
                            </MainCard>
                        </TabPane>
                        <TabPane tab="运营收入" key="2">
                            <div className='yytj'>
                                <Form {...formItemLayout} className="ant-advanced-search-form" style={{ display: this.state.editShow ? 'block' : 'none' }} onSubmit={this.submit}>
                                    <Row gutter={24}>
                                        <Col span={7} offset={3}>
                                            <Form.Item label='年度'>
                                                {getFieldDecorator('year', {
                                                    initialValue: this.state.income_list.year ? moment(this.state.income_list.year, 'YYYY') : moment(new Date(), 'YYYY'),
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: '请选择年度!',
                                                        },
                                                    ],
                                                })(<MonthPicker placeholder="请选择年份" defaultValue={moment('2015', 'YYYY')} format={'YYYY'} />)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={7} offset={3} style={{ display: 'block' }}>
                                            <Form.Item label='年度建设总经费'>
                                                {getFieldDecorator('year_all_amout', {
                                                    initialValue: this.state.income_list.year_all_amout,
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: '请输入年度建设总经费!',
                                                        },
                                                    ],
                                                })(<Input placeholder="请输入年度建设总经费" />)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={7} offset={3} style={{ display: 'block' }}>
                                            <Form.Item label='是否获得评比奖励'>
                                                {getFieldDecorator('is_reward', {
                                                    initialValue: this.state.income_list.is_reward,
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: '请选择是否获得评比奖励!',
                                                        },
                                                    ],
                                                })(<Select>
                                                    <Option value="是">是</Option>
                                                    <Option value="否">否</Option>
                                                </Select>)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={7} offset={3} style={{ display: 'block' }}>
                                            <Form.Item label='评比奖励'>
                                                {getFieldDecorator('reward_amout', {
                                                    initialValue: this.state.income_list.reward_amout,
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: '请输入评比奖励!',
                                                        },
                                                    ],
                                                })(<Input placeholder="请输入评比奖励" />)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={7} offset={3} style={{ display: 'block' }}>
                                            <Form.Item label='其他投入'>
                                                {getFieldDecorator('other_input', {
                                                    initialValue: this.state.income_list.other_input,
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: '请输入其他投入!',
                                                        },
                                                    ],
                                                })(<Input placeholder="请输入其他投入" />)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={7} offset={3} style={{ display: 'block' }}>
                                            <Form.Item label='备注'>
                                                {getFieldDecorator('remark', {
                                                    initialValue: this.state.income_list.remark,
                                                    rules: [
                                                        {
                                                            required: false,
                                                            message: '请输入备注!',
                                                        },
                                                    ],
                                                })(<Input placeholder="请输入备注" />)}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={6} offset={10} style={{ textAlign: 'center' }}>
                                            <Button type="primary" htmlType="submit">
                                                保存
                                            </Button>
                                            <Button style={{ marginLeft: 8 }} onClick={this.cancalEdit}>
                                                取消
                                             </Button>
                                        </Col>
                                        <Col span={8} style={{ textAlign: 'right' }} onClick={this.cancalEdit}>
                                            <a style={{ marginLeft: 8, fontSize: 12 }}>
                                                取消编辑<Icon type={this.state.editShow ? 'up' : 'down'} />
                                            </a>
                                        </Col>
                                    </Row>
                                </Form>
                                <br /><br /><br />
                                <Button type="primary" htmlType="submit" onClick={this.addYunying}>
                                    新增
                                </Button>
                                <SignFrameLayout {...service_item_list} />
                            </div>
                        </TabPane>
                    </Tabs>
                </MainContent >
            )
        );
    }
}

/**
 * 控件：编辑组织机构编辑控件
 * @description 编辑组织机构编辑控件
 * @author
 */
@addon('ChangeHappinessView', '编辑组织机构编辑控件', '编辑组织机构编辑控件')
@reactControl(Form.create()(ChangeHappinessView), true)
export class ChangeHappinessViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public select_param?: any;

}