import { Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";

let { Option } = Select;
/**
 * 组件：组织机构输入组件状态
 */
export interface HappinessEntryState extends BaseReactElementState {
    /** 行政区划列表 */
    administrative_division_list?: any;
    /** 基础数据 */
    base_data?: any;
    /** 组织机构列表 */
    org_list?: any[];
}

/**
 * 组件：组织机构输入组件
 * 组织机构输入组件
 */
export class HappinessEntry extends BaseReactElement<HappinessEntryControl, HappinessEntryState> {
    constructor(props: any) {
        super(props);
        this.state = {
            administrative_division_list: [],
            base_data: { 'qualification_info': [] },
        };
    }
    // 校验手机号码
    checkPhone = (rule: any, value: any, callback: any) => {
        var phone = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if (value === '') {
            callback('请输入手机号码');
        }
        if (value.length !== 11) {
            callback('格式不正确');
            if (!phone.test(value)) {
                callback('格式不正确');
            }
        }
    }
    // 校验座机号码
    checkLinePhone = (rule: any, value: any, callback: any) => {
        var phone = /^0\d{2,3}-\d{7,8}$/;
        if (value === '') {
            callback('请输入固定电话号码');
        }
        if (value.length !== 11 && value.length !== 12) {
            callback('格式不正确');
            if (!phone.test(value)) {
                callback('格式不正确');
            }
        }
    }
    componentDidMount() {
        // AppServiceUtility.business_area_service.get_business_area_list!({})!
        //     .then((data: any) => {
        //         if (data.result) {
        //             this.setState({
        //                 administrative_division_list: data.result
        //             });
        //         }
        //     });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        let { id } = this.props;
        if (id) {
            let { service_option } = this.props;
            let operation_option = service_option.operation_option;
            AppServiceUtility.person_org_manage_service.get_organization_list_all!(...operation_option.query.arguments)!
                .then((data: any) => {
                    // console.log(data);
                    this.setState({
                        base_data: data.result[0]
                    });
                });
        }
    }

    render() {
        const build_type = ['新建', '改建', '扩建'];
        const build_type_list: JSX.Element[] = build_type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        const open_days = ['周一', '周二', '周三', '周四', '周五', '周六', '周日'];
        const open_days_list: JSX.Element[] = open_days!.map((item) => <Option key={item} value={item}>{item}</Option>);
        const star_level = [
            {
                value: '3',
                label: '三星级',
            },
            {
                value: '4',
                label: '四星级',
            },
            {
                value: '5',
                label: '五星级',
            },
        ];
        const star_level_list: JSX.Element[] = star_level!.map((item) => <Option key={item.value} >{item.label}</Option>);
        let { base_data } = this.state;
        let edit_props = {
            form_items_props: [
                {
                    title: "组织机构",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入名称" }],
                                initialValue: base_data && base_data.name ? base_data.name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入名称",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "详细地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入地址" }],
                                initialValue: base_data.organization_info && base_data.organization_info.address ? base_data.organization_info.address : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入地址",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.nt_rich_text,
                            label: "机构介绍",
                            decorator_id: "describe",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入机构介绍" }],
                                initialValue: base_data.organization_info && base_data.organization_info.describe ? base_data.organization_info.describe : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构介绍",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "经度",
                            decorator_id: "lon",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入经度" }],
                                initialValue: base_data.organization_info && base_data.organization_info.lon ? base_data.organization_info.lon : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入经度",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "纬度",
                            decorator_id: "lat",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入纬度" }],
                                initialValue: base_data.organization_info && base_data.organization_info.lat ? base_data.organization_info.lat : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入纬度",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "法人或主要负责人",
                            decorator_id: "legal_person",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入法人或主要负责人" }],
                                initialValue: base_data.organization_info && base_data.organization_info.legal_person ? base_data.organization_info.legal_person : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入法人或主要负责人",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "办公电话",
                            decorator_id: "telephone",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入办公电话" }],
                                initialValue: base_data.organization_info && base_data.organization_info.telephone ? base_data.organization_info.telephone : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入办公电话",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.tree_select,
                            label: "所属镇街",
                            decorator_id: "street",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择所属镇街" }],
                                initialValue: base_data.organization_info && base_data.organization_info.street ? base_data.organization_info.street : ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择所属镇街",
                                autocomplete: 'off',
                            },
                        }, {
                            type: InputType.tree_select,
                            label: "所属村居",
                            decorator_id: "rusticate",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择所属村居" }],
                                initialValue: base_data.organization_info && base_data.organization_info.rusticate ? base_data.organization_info.rusticate : ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择所属村居",
                                autocomplete: 'off',
                            },
                        }, {
                            type: InputType.select,
                            label: "建设档次",
                            decorator_id: "star_level",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择建设档次" }],
                                initialValue: base_data.organization_info && base_data.organization_info.star_level ? base_data.organization_info.star_level : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择建设档次",
                                childrens: star_level_list,
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.select,
                            label: "建设方式",
                            decorator_id: "bulid_type",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入建设方式" }],
                                initialValue: base_data.organization_info && base_data.organization_info.bulid_type ? base_data.organization_info.bulid_type : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入建设方式",
                                childrens: build_type_list,
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.date,
                            label: "设立时间",
                            decorator_id: "reg_date",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择设立时间" }],
                                initialValue: base_data && base_data.reg_date ? base_data.reg_date : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择设立时间",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "服务机构",
                            decorator_id: "service_organization",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入服务机构" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_organization ? base_data.organization_info.service_organization : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务机构",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "图书数量",
                            decorator_id: "book_num",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入图书数量" }],
                                initialValue: base_data.organization_info && base_data.organization_info.book_num ? base_data.organization_info.book_num : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入图书数量",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "建筑面积（㎡）",
                            decorator_id: "build_area",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入建筑面积" }],
                                initialValue: base_data.organization_info && base_data.organization_info.build_area ? base_data.organization_info.build_area : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入建筑面积",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "占地面积（㎡）",
                            decorator_id: "cover_area",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入占地面积" }],
                                initialValue: base_data.organization_info && base_data.organization_info.cover_area ? base_data.organization_info.cover_area : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入占地面积",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.input_number,
                            label: "日托床位数（张）",
                            decorator_id: "day_bed_num",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入日托床位数（张）" }],
                                initialValue: base_data.organization_info && base_data.organization_info.day_bed_num ? base_data.organization_info.day_bed_num : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入日托床位数",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.input_number,
                            label: "邮政编码",
                            decorator_id: "post_code",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入邮政编码" }],
                                initialValue: base_data.organization_info && base_data.organization_info.post_code ? base_data.organization_info.post_code : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入邮政编码",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.select,
                            label: "开放时间",
                            decorator_id: "open_days",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择开放时间" }],
                                initialValue: base_data.organization_info && base_data.organization_info.open_days ? base_data.organization_info.open_days : undefined,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择开放时间",
                                mode: "multiple",
                                childrens: open_days_list,
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.timePicker,
                            label: "营业开始时间",
                            decorator_id: "start_business_time",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择营业开始时间" }],
                                initialValue: base_data.organization_info && base_data.organization_info.start_business_time ? base_data.organization_info.start_business_time : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择营业开始时间",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.timePicker,
                            label: "营业结束时间",
                            decorator_id: "end_business_time",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择营业结束时间" }],
                                initialValue: base_data.organization_info && base_data.organization_info.end_business_time ? base_data.organization_info.end_business_time : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择营业结束时间",
                                autocomplete: 'off',
                            }
                        },
                        //  {
                        //     type: InputType.text_area,
                        //     label: "机构介绍",
                        //     decorator_id: "introduce",
                        //     field_decorator_option: {
                        //         initialValue: base_data.organization_info ? base_data.organization_info.introduce : '',
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入备注",
                        //         row: 3,
                        //     }
                        // },
                        {
                            type: InputType.upload,
                            label: "土地使用证明（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "land_deed_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传土地使用证明" }],
                                initialValue: base_data.organization_info && base_data.organization_info.land_deed_file ? base_data.organization_info.land_deed_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传土地使用证明',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "房产来源证明（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "property_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传房产来源证明" }],
                                initialValue: base_data.organization_info && base_data.organization_info.property_file ? base_data.organization_info.property_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传房产来源证明',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "卫生许可证号",
                            decorator_id: "health_license_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入卫生许可证号" }],
                                initialValue: base_data.organization_info && base_data.organization_info.health_license_no ? base_data.organization_info.health_license_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入卫生许可证号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "消防合格证明（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "fire_safety_permit_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传消防合格证明" }],
                                initialValue: base_data.organization_info && base_data.organization_info.fire_safety_permit_file ? base_data.organization_info.fire_safety_permit_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传消防合格证明',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "房屋检测报告（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "house_check_report",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传房屋检测报告" }],
                                initialValue: base_data.organization_info && base_data.organization_info.house_check_report ? base_data.organization_info.house_check_report : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传房屋检测报告',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "环保合格报告（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "env_protect",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传环保合格报告" }],
                                initialValue: base_data.organization_info && base_data.organization_info.env_protect ? base_data.organization_info.env_protect : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传环保合格报告',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "工程结算报告（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "project_report",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传工程结算报告" }],
                                initialValue: base_data.organization_info && base_data.organization_info.project_report ? base_data.organization_info.project_report : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传工程结算报告',
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "购置设备发票（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "buy_dev_bill",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传购置设备发票" }],
                                initialValue: base_data.organization_info && base_data.organization_info.buy_dev_bill ? base_data.organization_info.buy_dev_bill : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传购置设备发票',
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "法人身份证（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "legal_person_id_img",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传法人身份证" }],
                                initialValue: base_data.organization_info && base_data.organization_info.legal_person_id_img ? base_data.organization_info.legal_person_id_img : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传法人身份证',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "建筑位置标示图（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "build_location_img",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传建筑位置标示图" }],
                                initialValue: base_data.organization_info && base_data.organization_info.build_location_img ? base_data.organization_info.build_location_img : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传建筑位置标示图',
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "建筑外部照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "build_outer_img",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传建筑外部照片" }],
                                initialValue: base_data.organization_info && base_data.organization_info.build_outer_img ? base_data.organization_info.build_outer_img : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传建筑外部照片',
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "消防设施照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "fire_safety_img",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传消防设施照片" }],
                                initialValue: base_data.organization_info && base_data.organization_info.fire_safety_img ? base_data.organization_info.fire_safety_img : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传消防设施照片',
                                autocomplete: 'off',
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                ...this.props.other_btn_propps!
            ],
            submit_btn_propps: this.props.submit_btn_propps,
            service_option: this.props.service_option,
            id: this.props.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <div>
                <FormCreator {...edit_props_list} />
            </div>
        );
    }
}

/**
 * 控件：组织机构输入组件控制器
 * 组织机构输入组件
 */
@addon('HappinessEntry', '组织机构输入组件', '组织机构输入组件')
@reactControl(HappinessEntry)
export class HappinessEntryControl extends BaseReactElementControl {
    /** 组织机构id */
    id?: string;
    /** 基本数据 */
    public base_data?: any;
    /** 提交按钮 */
    public submit_btn_propps?: any;
    /** 其他按钮 */
    public other_btn_propps?: any[];
    /** 服务配置 */
    public service_option?: any;
}