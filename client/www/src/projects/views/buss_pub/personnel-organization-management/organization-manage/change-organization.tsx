import { message, Modal } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { OrganizationEntry } from "src/projects/components/organization-entry";
// import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：编辑组织机构状态
 */
export interface ChangeOrganizationViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** 对方话框 */
    visible?: boolean;
}

/**
 * 组件：编辑组织机构视图
 */
export class ChangeOrganizationView extends ReactView<ChangeOrganizationViewControl, ChangeOrganizationViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
            visible: false

        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
        let id = this.props.match!.params.key;
        if (values) {
            let org_info = {
                id: id,
                name: undefined,
                personnel_type: '2',
                town: undefined,
                admin_area_id: undefined,
                reg_date: undefined,
                organization_info: {
                    description: undefined,
                    picture_list: [],
                    telephone: undefined,
                    organization_nature: undefined,
                    lon: undefined,
                    lat: undefined,
                    build_area: undefined,
                    cover_area: undefined,
                    super_org_id: undefined,
                    personnel_category: "平台",
                    comment: undefined,
                    if_build_fire_spray_sys: undefined,
                    if_build_fire_spray_sys_file: undefined,
                    if_build_smoke_sense_sys: undefined,
                    if_build_smoke_sense_sys_file: undefined,
                    older_org_permission_no: undefined,
                    older_org_permission_file: undefined,
                    configured_medical: undefined,
                    cooperative_medical: undefined,
                    land_deed_no: undefined,
                    land_deed_file: undefined,
                    property_no: undefined,
                    property_file: undefined,
                    health_license_no: undefined,
                    health_license_file: undefined,
                    fire_safety_permit_no: undefined,
                    fire_safety_permit_file: undefined,
                    address: undefined,
                    legal_person: undefined,
                    business_license_url: [],
                    unified_social_credit: [],
                    platform_filing_commitment: [],
                    service_agreement: undefined,
                    star_level: undefined,
                    contract_status: undefined,
                    unified_social: undefined,
                    taxes_level: undefined,
                    taxes_level_certificate: [],
                    organization_logo: [],
                    medical_introducte: undefined,
                    service_standard: {
                        bed_price_describe: undefined,
                        bed_price: undefined,
                        nurse_price_describe: undefined,
                        nurse_price: undefined,
                        meals_price_describe: undefined,
                        meals_price: undefined,
                    }
                },
                // "qualification_info": [],
            };
            for (let org in org_info) {
                if (org === 'organization_info') {
                    for (let info in org_info[org]) {
                        if (info) {
                            if (info === 'service_standard') {
                                for (let ifo in org_info[org][info]) {
                                    if (ifo) {
                                        org_info[org][info][ifo] = values[ifo];
                                    }
                                }
                            } else {
                                org_info[org][info] = values[info];
                            }
                        }
                    }
                } else {
                    if (values[org]) {
                        org_info[org] = values[org];
                    }
                }
            }
            AppServiceUtility.person_org_manage_service.update_organization!(org_info)!
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('保存成功');
                        // this.props.history!.push(ROUTE_PATH.organization);
                        history.back();
                    } else {
                        message.error(data);
                    }
                })
                .catch(error => {
                    message.error(error.message);
                });
        }
    }
    handleOk = () => {
        AppServiceUtility.person_org_manage_service.delete_organization!([this.props.match!.params.key!])!
            .then(data => {
                if (data === 'Success') {
                    message.info('删除成功');
                    history.back();
                    // this.props.history!.push(ROUTE_PATH.organization);

                } else {
                    message.info(data);
                }
                this.setState({
                    visible: false,
                });
            });
    }
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let base_data = this.state.base_data;
        let submit_btn_propps = {
            text: "保存",
            cb: this.handleSubmit
        };
        let other_btn_propps = [
            {
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                    // this.props.history!.push(ROUTE_PATH.organization);
                }
            },
        ];
        if (this.props.match!.params.key) {
            let del_btn = {
                text: "删除",
                cb: () => {
                    this.setState({
                        visible: true,
                    });
                }
            };
            other_btn_propps.push(del_btn);
        }
        let service_option = {
            service_object: AppServiceUtility.person_org_manage_service,
            operation_option: {
                save: {
                    func_name: "update_organization"
                },
                query: {
                    func_name: "",
                    arguments: [{ id: this.props.match!.params.key }, 1, 1]
                }
            },
        };
        // let succ_func = () => { this.props.history!.push(ROUTE_PATH.organization); }
        return (
            (
                <MainContent>
                    <OrganizationEntry
                        base_data={base_data}
                        id={this.props.match!.params.key}
                        submit_btn_propps={submit_btn_propps}
                        other_btn_propps={other_btn_propps}
                        service_option={service_option}
                    />
                    <Modal
                        title="温馨提示"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        确定要删除该组织机构吗？
                    </Modal>
                </MainContent>
            )
        );
    }
}

/**
 * 控件：编辑组织机构编辑控件
 * @description 编辑组织机构编辑控件
 * @author
 */
@addon('ChangeOrganizationView', '编辑组织机构编辑控件', '编辑组织机构编辑控件')
@reactControl(ChangeOrganizationView, true)
export class ChangeOrganizationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}