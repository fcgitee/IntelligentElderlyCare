
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { Tree, Button } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";

/** 状态：组织机构视图 */
export interface OrganizationViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
    /** 接口名 */
    request_url?: string;
    /** 树形结构 */
    tree?: any;
}
/** 组件：组织机构视图 */
export class OrganizationView extends React.Component<OrganizationViewControl, OrganizationViewState> {
    constructor(props: OrganizationViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {},
            request_url: '',
            tree: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeOrganization);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: any, contents: any) => {
        if (this.props.organization_type) {
            if (this.props.organization_type === '服务商') {
                this.props.history!.push(ROUTE_PATH.changeOrganizationServicer + '/' + contents.selectedNodes[0].props.id);
                return;
            }
        }
        this.props.history!.push(ROUTE_PATH.changeOrganization + '/' + contents.selectedNodes[0].props.id);
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        let param = {};
        if (this.props.organization_type) {
            param = { type: this.props.organization_type };
        }
        AppServiceUtility.person_org_manage_service[request_url!](param)!
            .then((data: any) => {
                // console.log("树形结构", data);
                if (data.result.length > 0) {
                    this.setState({
                        tree: data.result,
                    });
                }
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const name = this.props.organization_type ? this.props.organization_type : '组织机构';
        return (
            <MainContent>
                <MainCard title={name}>
                    <Button type={'primary'} onClick={this.add}>新增{name}</Button>
                    <MainContent>
                        <Tree
                            defaultExpandAll={true}
                            showIcon={true}
                            treeData={this.state.tree}
                            onSelect={this.onIconClick}
                        />
                    </MainContent>
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：组织机构视图控制器
 * @description 组织机构视图
 * @author
 */
@addon('OrganizationView', '组织机构视图', '组织机构视图')
@reactControl(OrganizationView, true)
export class OrganizationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public organization_type?: string;
}