import { Row } from "antd";
import { addon, Permission, } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { isPermission } from "src/projects/app/permission";

// const Option = Select.Option;
/**
 * 组件：角色列表页面状态
 */
export interface RoleViewState extends ReactViewState {
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：角色列表页面
 */
export class RoleView extends ReactView<RoleViewControl, RoleViewState> {

    private columns_data_source = [{
        title: '角色名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '所属组织',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeRole);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRole + '/' + contents.id);
        }
    }

    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "角色姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入角色名称'
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: '请输入机构名称'
                //     }
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.role_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_roles'
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：角色列表页面
 * 描述
 */
@addon('RoleView', '角色列表页面', '描述')
@reactControl(RoleView, true)
export class RoleViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}