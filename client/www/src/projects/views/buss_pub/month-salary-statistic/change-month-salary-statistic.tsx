import { Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;
/**
 * 组件：月收入统计页面状态
 */
export interface ChangeMonthSalaryStatisticViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    man?: any;
}

/**
 * 组件：月收入统计页面
 * 描述
 */
export class ChangeMonthSalaryStatisticView extends ReactView<ChangeMonthSalaryStatisticViewControl, ChangeMonthSalaryStatisticViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            man: []
        };
    }
    componentWillMount() {
        // let id = this.props.match!.params.key;

        AppServiceUtility.person_org_manage_service.get_worker_list_all!({ current_org_id: {} })!.
            then(e => {
                const data = e.result;
                this.setState({ man: data });
            });
    }

    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
    }
    render() {
        const that = this;
        function otherSubmitData() {
            if (that.props.match!.params.key) {
                return {
                    'user_id': that.props.match!.params.key
                };
            } else {
                return undefined;
            }
        }
        // console.log(this.state.man);

        const other_submit_data = otherSubmitData();
        let edit_props = {
            form_items_props: [
                {
                    title: "月收入统计",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "人员姓名",
                            decorator_id: "worker_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输人员姓名" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输人员姓名",
                                disabled: this.props.match!.params.key ? true : false,
                                childrens: this.state.man.map((e: any, i: number) => {
                                    return <Option key={e.id} value={e.id}>{e.name}</Option>;
                                })
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "月收入",
                            decorator_id: "month_salary",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入月收入" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入月收入"
                            }
                        },
                        {
                            type: InputType.monthPicker,
                            label: "月份",
                            decorator_id: "month",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入月份" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入月份",
                                month: true,
                                // format: 'YYYY-MM'
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.monthSalaryStatistic);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            other_submit_data: {
                ...other_submit_data
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_month_salary_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_month_salary"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.monthSalaryStatistic); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：月收入统计页面
 * 描述
 */
@addon('ChangeMonthSalaryStatisticView', '月收入统计页面', '描述')
@reactControl(ChangeMonthSalaryStatisticView, true)
export class ChangeMonthSalaryStatisticViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}