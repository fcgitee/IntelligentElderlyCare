import moment from 'moment';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：月收入统计视图 */
export interface MonthSalaryStatisticViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 接口名 */
    request_url?: string;
    select_type?: any;
    /** 对话框显示状态 */
    visible?: boolean;
    /** 组织机构列表 */
    organization_list?: any;
    org_info: any;
}
/** 组件：月收入统计视图 */
export class MonthSalaryStatisticView extends React.Component<MonthSalaryStatisticViewControl, MonthSalaryStatisticViewState> {
    private columns_data_source = [{
        title: '证件号码',
        dataIndex: 'user.id_card',
        key: 'user.id_card',
    }, {
        title: '证件类型',
        dataIndex: 'user.id_card_type',
        key: 'user.id_card_type',
    }, {
        title: '姓名',
        dataIndex: 'user.name',
        key: 'user.name',
    }, {
        title: '性别',
        dataIndex: 'user.personnel_info.sex',
        key: 'user.personnel_info.sex',
    },
    {
        title: '年龄',
        dataIndex: 'user.personnel_info.age',
        key: 'user.personnel_info.age',
    }, {
        title: '出生日期',
        dataIndex: 'user.personnel_info.date_birth',
        key: 'user.personnel_info.date_birth',
    },
    {
        title: '家庭住址',
        dataIndex: 'user.personnel_info.address',
        key: 'user.personnel_info.address',
    },
    {
        title: '人员类别',
        dataIndex: 'user.personnel_info.personnel_category',
        key: 'user.personnel_info.personnel_category',
    },
    {
        title: '所属组织机构',
        dataIndex: 'user.org.name',
        key: 'user.org.name',
    },
    {
        title: '月收入',
        dataIndex: 'month_salary',
        key: 'month_salary',
        render: (text: string, record: any) => {
            return record.month_salary;
        }
    },
    {
        title: '月份',
        dataIndex: 'month',
        key: 'month',
        render: (text: string, record: any) => {
            if (record.user_income) {
                return undefined;
            }
            return moment(record.month).format('YYYY-MM');
        }
    }
    ];
    constructor(props: MonthSalaryStatisticViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10],
            request_url: '',
            select_type: {},
            org_info: {},
            visible: false,
        };
    }

    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changeMonthSalaryStatistic);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(this.props.change_url ? this.props.change_url + '/' + contents.id : ROUTE_PATH.changeMonthSalaryStatistic + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        let select_type = this.props.select_type;
        this.setState({
            request_url,
            select_type
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.state.select_type ? this.state.select_type : { current_org_id: {} }, 1, 10]
                },
                delete: {
                    service_func: this.props.delete_url
                }
            },
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            searchExtraParam: this.state.select_type,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            select_type: this.props.select_type
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...personnel_list} />
                </div>
            )
        );
    }
}

/**
 * 控件：月收入统计视图控制器
 * @description 月收入统计视图
 * @author
 */
@addon('MonthSalaryStatisticView', '月收入统计视图', '月收入统计视图')
@reactControl(MonthSalaryStatisticView, true)
export class MonthSalaryStatisticViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public delete_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 查询条件 */
    public select_type?: any;
    /** 人员类型 */
    public personnel_category?: string;
}