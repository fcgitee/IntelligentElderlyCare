import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { message, Select } from "antd";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { edit_props_info } from "src/projects/app/util-tool";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;
message.config({
    top: 350,
});

/**
 * 组件：帮长者预定购买服务状态
 */
export interface ReserveBuyServiceEditViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 服务商列表 */
    service_p_list?: any[];
    /** 服务列表 */
    service?: any[];
    /** 价格 */
    total_price?: number;
    /** 人员类别 */
    personnel_classification_list?: any;
    /** 长者姓名 */
    user_name?: string;
    /** 长者类型 */
    personnel_classification?: string;
    // /** 长者电话  */
    // elder_telephone?: string;
    // /** 长者地址  */
    // service_address?: string;
    is_send?: boolean;
}

/**
 * 组件：帮长者预定购买服务视图
 */
export class ReserveBuyServiceEditView extends ReactView<ReserveBuyServiceEditViewControl, ReserveBuyServiceEditViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            service_p_list: [],
            service: [],
            total_price: 0,
            personnel_classification_list: [],
            is_send: false,
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
        // let id = this.props.match!.params.key;
        // const isTrue: boolean = (!values.personnel_classification && !values.name) || (values.personnel_classification && values.name);
        // if (isTrue) {
        //     message.info('只能选择长者类型或长者其中一个');
        //     return;
        // }
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                if (values) {
                    // // console.log('value>>>>', values);
                    let key = this.props.match!.params.key;
                    AppServiceUtility.comfirm_order_service.update_reserve_service!({
                        'id': key,
                        'personnel_classification': this.state.personnel_classification,
                        // 'service_address': this.state.service_address,
                        // 'elder_telephone': this.state.elder_telephone,
                        'service_product_id': values.service_product_id.key,
                        'product_name': values.service_product_id.label,
                        'name': values.name,
                        'user_name': this.state.user_name,
                        'service_provider_id': values.service_provider_id.key,
                        'provider_name': values.service_provider_id.label,
                        'comment': values.comment,
                        'organization_id': values.service_provider_id.key,
                        'count': values.count,
                        'buy_type': '补贴账户',
                    })!
                        .then((data: any) => {
                            if (data === 'Success') {
                                message.info('预约成功！', 1, () => {
                                    this.props.history!.push(ROUTE_PATH.reserveBuyService);
                                });
                            } else if (data === 'Exists') {
                                message.error('该长者已预订服务！');
                                this.setState({
                                    is_send: false
                                });
                            } else {
                                message.error(data);
                                this.setState({
                                    is_send: false
                                });
                            }
                        })
                        .catch(error => {
                            message.error(error.message);
                            this.setState({
                                is_send: false
                            });
                        });
                }
            }
        );
    }
    older_change = (value: any) => {
        AppServiceUtility.person_org_manage_service.get_all_elder_list!({ id: value }, 1, 1)!
            .then(data => {
                if (data.result!.length > 0) {
                    this.setState({
                        user_name: data.result![0]['name'],
                        personnel_classification: data.result![0]['personnel_info']['personnel_category'],
                        // service_address: data.result![0]['personnel_info']['address'],
                        // elder_telephone: data.result![0]['personnel_info']['telephone']
                    });
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    componentWillMount() {
        let key = this.props.match!.params.key;
        if (key) {
            AppServiceUtility.comfirm_order_service.get_reserve_service!({ 'id': key })!
                .then(data => {
                    AppServiceUtility.service_item_service.get_service_product_package_list!({ "organization_id": data.result![0].provider_id })!
                        .then(data => {
                            this.setState({
                                service: data!.result
                            });
                        })
                        .catch(err => {
                            console.info(err);
                        });
                    this.setState({
                        base_data: data.result![0],
                        total_price: data.result![0].count
                    });
                })
                .catch(err => {
                    console.info(err);
                });
        }
        AppServiceUtility.person_org_manage_service.get_org_list!({ "personnel_category": "服务商" })!
            .then((data: any) => {
                this.setState({
                    service_p_list: data
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 人员类别列表
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({})!
            .then(data => {
                this.setState({
                    personnel_classification_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    service_providerChange = (value: any) => {
        AppServiceUtility.service_item_service.get_service_product_package_all!({ "organization_id": value.key })!
            .then((data: any) => {
                this.setState({
                    service: data
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    service_change = (value: any) => {
        AppServiceUtility.service_item_service.get_service_product_package_all!({ "id": value.key })!
            .then(data => {
                this.setState({
                    total_price: data![0]['total_price'],
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let { service_p_list, service } = this.state;
        let org_type_list: any[] = [];
        let service_list: any[] = [];
        let base_date = this.state.base_data;
        service_p_list!.map((item) => {
            org_type_list.push(<Option key={item.id}>{item.name}</Option>);
        });
        // 防止重复
        let uniqueArray: any = [];
        service!.map((item) => {
            if (!uniqueArray[item.id]) {
                uniqueArray[item.id] = true;
                service_list.push(<Option key={item.id}>{item.name}</Option>);
            }
        });
        // let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
        //     return (
        //         <Option key={value.id}>{value.name}</Option>
        //     );
        // });
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_all_elder_list',
            service_option: base_date.name ? [{ id: base_date.name }, 1, 1] : '',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let service_option = {
            service_object: AppServiceUtility.comfirm_order_service,
            operation_option: {
                save: {
                    func_name: ""
                },
                query: {
                    func_name: "get_reserve_service",
                    arguments: [{ id: this.props.match!.params.key }, 1, 1]
                }
            },
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "预定服务购买",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者姓名",
                            decorator_id: 'name',
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入名称" }],
                                initialValue: base_date.name,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者姓名",
                                modal_search_items_props: modal_search_items_props,
                                onChange: this.older_change
                            },
                        },
                        // {
                        //     type: InputType.select,
                        //     label: "长者类型",
                        //     decorator_id: "personnel_classification",
                        //     field_decorator_option: {
                        //         rules: [{ message: "请选择长者类型" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         childrens: personnel_classification_list,
                        //         allowClear: true,
                        //         placeholder: "请选择长者类型",
                        //     },
                        // },
                        {
                            type: InputType.select,
                            label: "服务商",
                            decorator_id: "service_provider_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务商" }],
                                initialValue: { key: base_date.service_provider_id, label: base_date.provider_name },
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务商",
                                childrens: org_type_list,
                                onChange: this.service_providerChange,
                                labelInValue: true,
                                showSearch: true,
                                optionFilterProp: "children",
                                filterOption: (input: any, option: any) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        }, {
                            type: InputType.select,
                            label: "服务产品",
                            decorator_id: "service_product_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务产品" }],
                                initialValue: { key: base_date.service_product_id, label: base_date.product_name },
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务产品",
                                childrens: service_list,
                                onChange: this.service_change,
                                labelInValue: true,
                                showSearch: true,
                                optionFilterProp: "children",
                                filterOption: (input: any, option: any) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "金额",
                            decorator_id: "count",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                                initialValue: this.state.total_price,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "金额",
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "comment",
                            field_decorator_option: {
                                initialValue: base_date.comment,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                row: 3,
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    this.props.history!.push(ROUTE_PATH.reserveBuyService);
                },
                btn_other_props: { disabled: this.state.is_send }
            }],
            submit_btn_propps: {
                text: "确定",
                cb: this.handleSubmit,
                btn_other_props: { disabled: this.state.is_send }
            },
            service_option: service_option,
            // id: this.props.id,
        };
        // let succ_func = () => { this.props.history!.push(ROUTE_PATH.organization); }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}

/**
 * 控件：帮长者预定购买服务编辑控件
 * @description 帮长者预定购买服务编辑控件
 * @author
 */
@addon('ReserveBuyServiceEditView', '帮长者预定购买服务编辑控件', '帮长者预定购买服务编辑控件')
@reactControl(ReserveBuyServiceEditView, true)
export class ReserveBuyServiceEditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}