
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, message, Select, Button } from "antd";
import './index.less';
let { Option } = Select;
/** 状态：预定服务列表视图 */
export interface ReserveBuyServiceViewState extends ReactViewState {
    /** 接口名 */
    request_ulr?: string;
    /** 对话框 */
    model_show?: boolean;
    /** 长者类型 */
    personnel_classification_list?: any;
    // 多选项目行
    item_list?: any[];
    // 镇街
    administrative_division_list?: any;
    is_send?: boolean;
    /** 全部下单提示框 */
    model_all_show?: boolean;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：预定服务列表视图 */
export class ReserveBuyServiceView extends React.Component<ReserveBuyServiceViewControl, ReserveBuyServiceViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '长者身份证号',
        dataIndex: 'user_info[0].id_card',
        key: 'user_info[0].id_card',
    }, {
        title: '长者所在镇街',
        dataIndex: 'admin_area_name',
        key: 'admin_area_name',
    }, {
        title: '服务商',
        dataIndex: 'provider_name',
        key: 'provider_name',
    }, {
        title: '服务项目',
        dataIndex: 'product_name',
        key: 'product_name',
    }, {
        title: '金额',
        dataIndex: 'count',
        key: 'count',
    }, {
        title: '备注',
        dataIndex: 'comment',
        key: 'comment',
    }, {
        title: '修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }, {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '执行结果',
        dataIndex: 'month',
        key: 'month',
        render: (text: string, record: any, index: any) => {
            if (record.status === '失败') {
                return <Button type='danger'>{record.month + '月:' + record.status}</Button>;
            } else if (record.status === '成功') {
                return <Button style={{ color: 'white', backgroundColor: 'green' }}>{record.month + '月:' + record.status}</Button>;
            } else {
                return <Button style={{ color: 'white', backgroundColor: 'gray' }}>新增</Button>;
            }
        }
    }];
    constructor(props: ReserveBuyServiceViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            model_show: false,
            model_all_show: false,
            personnel_classification_list: [],
            administrative_division_list: [],
            is_send: false,
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.reserveBuyServiceEdit);
    }
    /** 一键下单 */
    buyService = () => {
        let { item_list } = this.state;
        // console.log('item_list>>>', item_list);
        if (!item_list || item_list!.length === 0) {
            message.error('先选中所需下单的长者！');
        } else {
            this.setState({
                model_show: true,
            });
        }
    }
    /** 全部下单 */
    allBuyService = () => {
        this.setState({
            model_all_show: true,
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.reserveBuyServiceEdit + '/' + contents.id);
        }
    }

    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 人员类别列表
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({})!
            .then(data => {
                this.setState({
                    personnel_classification_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 镇街
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result!
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    handleCancel = () => {
        this.setState({
            model_show: false
        });
    }
    handleAllCancel = () => {
        this.setState({
            model_all_show: false
        });
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    handleOk = () => {
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let { item_list } = this.state;
                AppServiceUtility.comfirm_order_service.one_key_order!(item_list)!
                    .then(data => {
                        if (data === 'Success') {
                            message.info('下单成功');
                            this.formCreator && this.formCreator.reflash();
                        } else {
                            alert(data);
                            this.formCreator && this.formCreator.reflash();
                        }
                        this.setState({
                            model_show: false,
                            is_send: false
                        });
                    })
                    .catch(err => {
                        console.info(err);
                        this.setState({
                            is_send: false
                        });
                    });
            }
        );
    }
    handleAllOk = () => {
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                AppServiceUtility.comfirm_order_service.all_buy_order!()!
                    .then(data => {
                        if (data === 'Success') {
                            message.info('下单成功');
                            this.formCreator && this.formCreator.reflash();
                        } else {
                            alert(data);
                            this.formCreator && this.formCreator.reflash();
                        }
                        this.setState({
                            model_all_show: false,
                            is_send: false
                        });
                    })
                    .catch(err => {
                        console.info(err);
                        this.setState({
                            is_send: false
                        });
                    });
            }
        );
    }
    /** 行选中事件 */
    onRowSelection = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({
            item_list: selectedRowKeys
        });
    }
    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name",
                    option: {
                        placeholder: "请输入长者姓名",
                    }
                },
                {
                    type: InputType.input,
                    label: "长者身份证号",
                    decorator_id: "user_id_card",
                    option: {
                        placeholder: "请输入长者身份证号",
                    }
                },
                // {
                //     type: InputType.input,
                //     label: "服务商",
                //     decorator_id: 'provider_name',
                //     option: {
                //         placeholder: "请输入服务商",
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "provider_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.input,
                    label: "服务项目",
                    decorator_id: 'product_name',
                    option: {
                        placeholder: "请输入服务项目",
                    }
                },
                {
                    type: InputType.select,
                    label: "长者类型",
                    decorator_id: "personnel_classification_id",
                    option: {
                        placeholder: "请选择长者类型",
                        childrens: personnel_classification_list,
                        allowClear: true
                    }
                },
                {
                    type: InputType.tree_select,
                    col_span: 6,
                    label: "镇街",
                    decorator_id: "admin_area_id",
                    option: {
                        placeholder: "请选择镇街",
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        allowClear: true,
                        treeDefaultExpandAll: true,
                        treeData: this.state.administrative_division_list,
                    },
                },
            ],
            btn_props: [{
                label: '新增服务预定',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }, {
                label: '一键下单(勾选)',
                btn_method: this.buyService,
                icon: 'upload',
            }, {
                label: '全部下单',
                btn_method: this.allBuyService,
                icon: 'upload',
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            on_row_selection: this.onRowSelection,
            service_object: AppServiceUtility.comfirm_order_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_reserve_service'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let table_param = {
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="温馨提示"
                    visible={this.state.model_show}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <div style={{ textAlign: 'center' }}>确定要把勾选的长者一键下单吗？</div>
                </Modal>
                <Modal
                    title="温馨提示"
                    visible={this.state.model_all_show}
                    onOk={this.handleAllOk}
                    onCancel={this.handleAllCancel}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <div style={{ textAlign: 'center' }}>确定要给所有服务预约的长者下单吗？</div>
                </Modal>
            </div>

        );
    }
}

/**
 * 控件：预定服务列表视图控制器
 * @description 预定服务列表视图
 * @author
 */
@addon(' ReserveBuyServiceView', '预定服务列表视图', '预定服务列表视图')
@reactControl(ReserveBuyServiceView, true)
export class ReserveBuyServiceViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}