
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
/** 状态：服务结算报表视图 */
export interface ServiceSettlementStatementViewState extends ReactViewState {
    /** 接口名 */
    request_ulr?: string;
}
/** 组件：服务结算报表视图 */
export class ServiceSettlementStatementView extends React.Component<ServiceSettlementStatementViewControl, ServiceSettlementStatementViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '联系地址',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '长者类别',
        dataIndex: 'user_type',
        key: 'user_type',
    }, {
        title: '服务商',
        dataIndex: 'provider_name',
        key: 'provider_name',
    }, {
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    }, {
        title: '服务项目',
        dataIndex: 'product_name',
        key: 'product_name',
    }, {
        title: '服务开始时间',
        dataIndex: 'service_start',
        key: 'service_start',
    }, {
        title: '服务结束时间',
        dataIndex: 'service_end',
        key: 'service_end',
    }, {
        title: '金额(元)',
        dataIndex: 'count',
        key: 'count',
    },];
    constructor(props: ServiceSettlementStatementViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.reserveBuyServiceEdit + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name"
                },
                {
                    type: InputType.input,
                    label: "服务商",
                    decorator_id: 'provider_name'
                },
                {
                    type: InputType.input,
                    label: "服务项目",
                    decorator_id: 'product_name'
                },
            ],
            btn_props: [],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
            </div>

        );
    }
}

/**
 * 控件：服务结算报表视图控制器
 * @description 服务结算报表视图
 * @author
 */
@addon(' ReserveBuyServiceView', '服务结算报表视图', '服务结算报表视图')
@reactControl(ServiceSettlementStatementView, true)
export class ServiceSettlementStatementViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}