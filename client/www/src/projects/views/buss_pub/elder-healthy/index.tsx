import { Select, Row } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param, getAge } from 'src/projects/app/util-tool';

let { Option } = Select;
/**
 * 组件：长者健康信息列表状态
 */
export interface ElderHealthyListViewState extends ReactViewState {
}

/**
 * 组件：长者健康信息列表
 * 描述
 */
export class ElderHealthyListView extends ReactView<ElderHealthyListViewControl, ElderHealthyListViewState> {
    private columns_data_source = [
        {
            title: '序号',
            dataIndex: 'pk_phy_exam_report',
            key: 'pk_phy_exam_report',
        },
        {
            title: '姓名',
            dataIndex: 'elder_name',
            key: 'elder_name',
        }, {
            title: '性别',
            dataIndex: 'elder_sex',
            key: 'elder_sex',
        }, {
            title: '周岁',
            dataIndex: 'pk_member',
            key: 'pk_member',
            render: (text: string, record: any) => {
                return text ? getAge(text) : '';
            }
        }, {
            title: '联系电话',
            dataIndex: 'elder_telephone',
            key: 'elder_telephone',
        }, {
            title: '血糖',
            dataIndex: 'blood_sugar',
            key: 'blood_sugar',
            render: (text: string, record: any) => {
                let number: any = ['6', '19', '20', '21', '22', '23', '24', '25'];
                let string = '';
                number.map((item: any) => {
                    if (record.hasOwnProperty(`healthy_${item}`)) {
                        string += record[`healthy_${item}`] + "\r\n";
                    }
                });
                return (
                    <span>{string}</span>
                );
            }
        }, {
            title: '体温',
            dataIndex: 'healthy_9',
            key: 'healthy_9',
        }, {
            title: '血氧',
            dataIndex: 'healthy_1',
            key: 'healthy_1',
        }, {
            title: 'BMI体重指数',
            dataIndex: 'healthy_30',
            key: 'healthy_30',
        }, {
            title: '体水份率',
            dataIndex: 'healthy_41',
            key: 'healthy_41',
        }, {
            title: '体脂肪率',
            dataIndex: 'healthy_39',
            key: 'healthy_39',
        }, {
            title: '基础代谢',
            dataIndex: 'healthy_53',
            key: 'healthy_53',
        }, {
            title: '检测时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onIconClick = (type: string, contents: any) => {
    }
    componentDidMount() {
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "elder_name",
                    option: {
                        placeholder: "姓名",
                        autoComplete: 'off',
                    }
                }, {
                    type: InputType.input,
                    label: "身份证号",
                    decorator_id: "pk_member",
                    option: {
                        placeholder: "请输入身份证号",
                        autoComplete: 'off',
                    }
                }, {
                    type: InputType.select,
                    label: "性别",
                    decorator_id: "elder_sex",
                    option: {
                        placeholder: "请选择性别",
                        autoComplete: 'off',
                        childrens: ['男', '女'].map((item, idx) => {
                            return (
                                <Option key={item}>{item}</Option>
                            );
                        }),
                    }
                },
                // {
                //     type: InputType.select,
                //     label: "血糖状况",
                //     decorator_id: "blood_sugar",
                //     option: {
                //         placeholder: "请选择血糖状况",
                //         childrens: ['正常', '偏高'].map((item, idx) => {
                //             return (
                //                 <Option key={item}>{item}</Option>
                //             );
                //         }),
                //     }
                // }, {
                //     type: InputType.select,
                //     label: "体水份率",
                //     decorator_id: "body_water_rate",
                //     option: {
                //         placeholder: "请选择体水份率",
                //         childrens: ['正常', '偏高'].map((item, idx) => {
                //             return (
                //                 <Option key={item}>{item}</Option>
                //             );
                //         }),
                //     }
                // }, {
                //     type: InputType.select,
                //     label: "体脂肪率",
                //     decorator_id: "body_fat_rate",
                //     option: {
                //         placeholder: "请选择体脂肪率",
                //         childrens: ['正常', '偏高'].map((item, idx) => {
                //             return (
                //                 <Option key={item}>{item}</Option>
                //             );
                //         }),
                //     }
                // }, 
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "date_range"
                },
            ],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_elder_healthy_list_all',
                    service_condition: [{}, 1, 10]
                }
            },
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：长者健康信息列表
 * 描述
 */
@addon('ElderHealthyListView', '长者健康信息列表', '描述')
@reactControl(ElderHealthyListView, true)
export class ElderHealthyListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}