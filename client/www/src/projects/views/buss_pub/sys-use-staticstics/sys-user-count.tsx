import { Row, Select } from "antd";
import { addon, Permission, } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { isPermission } from "src/projects/app/permission";
let { Option } = Select;

// const Option = Select.Option;
/**
 * 组件：系统使用情况统计页面状态
 */
export interface SysUseStaticsticsUserViewState extends ReactViewState {
    org_list?: any;
}

/**
 * 组件：系统使用情况统计页面
 */
export class SysUseStaticsticsUserView extends ReactView<SysUseStaticsticsUserViewControl, SysUseStaticsticsUserViewState> {
    private columns_data_source = [
        {
            title: '在线人数',
            dataIndex: 'count',
            key: 'user_name',
        },
    ];

    constructor(props: any) {
        super(props);
        this.state = {
            org_list: []
        };
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let org_type_list: JSX.Element[] = ['福利院', '幸福院', '服务商', '民政', '平台'].map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: '请输入组织机构名称'
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.input,
                    label: "功能名称",
                    decorator_id: "module_name",
                    option: {
                        placeholder: '请输入功能名称'
                    }
                },
                {
                    type: InputType.select,
                    label: "机构类型",
                    decorator_id: "org_type",
                    option: {
                        placeholder: '请选择机构类型',
                        childrens: org_type_list
                    }
                },
                {
                    type: InputType.date,
                    label: "操作时间",
                    decorator_id: "create_date",
                },
            ],
            columns_data_source: this.columns_data_source,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            service_object: AppServiceUtility.role_service,
            service_option: {
                select: {
                    service_func: 'get_staticstics_user',
                    service_condition: [{}, 1, 10]
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：系统使用情况统计页面
 * 描述
 */
@addon('SysUseStaticsticsUserView', '系统使用情况统计页面', '描述')
@reactControl(SysUseStaticsticsUserView, true)
export class SysUseStaticsticsUserViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 导出权限 */
    public add_permission?: string;
}