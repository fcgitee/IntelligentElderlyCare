import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：消息类型状态
 */
export interface MessageTypeListViewState extends ReactViewState {
    // 信息总数总条数
    total?: number;
}

/**
 * 组件：消息类型
 * 描述
 */
export class MessageTypeListView extends ReactView<MessageTypeListViewControl, MessageTypeListViewState> {
    private columns_data_source = [{
        title: '消息类型名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    // 创建消息类型
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeMessageType);
    }

    // 查看信息详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeMessageType + '/' + contents.id);
        }
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let message_option = {
            edit_form_items_props: [{
                type: InputType.input,
                label: "消息类型名称",
                decorator_id: "name"
            }, {
                type: InputType.input,
                label: "备注",
                decorator_id: "name"
            }],
            btn_props: [{
                label: '添加消息类型',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.message_service,
            service_option: {
                select: {
                    service_func: 'get_message_type_list',
                    service_condition: [{}, 1, 10]
                },
            },
        };
        let message_list = Object.assign(message_option, table_param);
        return (
            <Row>
                <SignFrameLayout {...message_list} />
            </Row>
        );
    }
}

/**
 * 控件：消息类型
 * 描述
 */
@addon('MessageTypeListView', '消息类型', '描述')
@reactControl(MessageTypeListView, true)
export class MessageTypeListViewControl extends ReactViewControl {
}