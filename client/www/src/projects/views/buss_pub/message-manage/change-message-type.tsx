import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
/**
 * 组件：编辑消息类型
 */
export interface ChangeMessageTypeViewState extends ReactViewState {
}

/**
 * 组件：编辑消息类型
 */
export default class ChangeMessageTypeView extends ReactView<ChangeMessageTypeViewControl, ChangeMessageTypeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.messageList);
    }

    componentDidMount() {
    }

    render() {
        var edit_props = {
            form_items_props: [
                {
                    title: '编辑消息类型',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "消息类型名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入消息类型名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消息类型名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入信息备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.messageTypeList);
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.message_service,
                operation_option: {
                    save: {
                        func_name: "add_new_message_type"
                    },
                    query: {
                        func_name: "get_message_type_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.messageTypeList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑消息类型
 * @description 编辑消息类型
 * @author
 */
@addon('ChangeMessageTypeView', '编辑消息类型', '编辑消息类型')
@reactControl(ChangeMessageTypeView, true)
export class ChangeMessageTypeViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}