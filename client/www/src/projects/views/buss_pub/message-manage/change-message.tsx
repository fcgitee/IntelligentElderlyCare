import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import { Select } from 'antd';
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
const { Option } = Select;
/**
 * 组件：创建消息
 */
export interface ChangeMessageViewState extends ReactViewState {
    // id
    id?: string;
    // 信息数组
    message_data: any[];
    // 信息类型
    message_type: any[];
    // 接收信息的人
    message_receive_person?: any[];
}

/**
 * 组件：创建消息
 */
export default class ChangeMessageView extends ReactView<ChangeMessageViewControl, ChangeMessageViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            message_data: [],
            message_type: [],
            message_receive_person: [],
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.messageList);
    }

    componentDidMount() {
        // 获取信息类型
        request(this, AppServiceUtility.message_service.get_message_type_list!({}, 1, 1)!)
            .then((datas: any) => {
                this.setState({
                    message_type: datas.result,
                });
            });
        // 获取接收提示的人
        request(this, AppServiceUtility.task_service.input_person!()!)
            .then((datas: any) => {
                this.setState({
                    message_receive_person: datas,
                });
            });
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "人员名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: [{
                title: '编号',
                dataIndex: 'code',
                key: 'code',
            }, {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            }, {
                title: '性别',
                dataIndex: 'sex',
                key: 'sex',
            }, {
                title: '出生年月',
                dataIndex: 'birth_date',
                key: 'birth_date',
            }],
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_personnel_list',
            title: '人员查询',
            select_option: {
                placeholder: "请选择人员",
            }
        };
        let type = this.state.message_type;
        let message_type: any[] = [];
        type!.map((item: any) => {
            message_type.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let pesron = this.state.message_receive_person;
        let message_receive_person: any[] = [];
        pesron!.map((item: any) => {
            message_receive_person.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        var edit_props = {
            form_items_props: [
                {
                    title: '创建消息',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "消息名称",
                            decorator_id: "message_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入信息消息名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消息名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "消息内容",
                            decorator_id: "message_content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入消息内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消息内容",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.modal_search,
                            label: "接收人",
                            decorator_id: 'receive_info',
                            field_decorator_option: {
                                rules: [{ required: true, message: "接收人" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择接收人",
                                modal_search_items_props: modal_search_items_props
                            },
                        },
                        {
                            type: InputType.select,
                            label: "消息类型",
                            decorator_id: 'message_type',
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择消息类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择任务类型",
                                childrens: message_type,
                            },
                        },
                        {
                            type: InputType.date,
                            label: "提醒时间",
                            decorator_id: "push_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "信息提醒时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入信息提醒时间",
                                format: "YYYY-MM-DD HH:mm:ss",
                                locale: 'zh-cn',
                                showTime: true,
                            },
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入信息备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.messageList);
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.message_service,
                operation_option: {
                    save: {
                        func_name: "add_new_message"
                    },
                    query: {
                        func_name: "get_message_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.messageList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：创建消息
 * @description 创建消息
 * @author
 */
@addon('ChangeMessageView', '创建消息', '创建消息')
@reactControl(ChangeMessageView, true)
export class ChangeMessageViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}