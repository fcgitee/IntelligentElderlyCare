import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：消息类型状态
 */
export interface MessageListViewState extends ReactViewState {
    // 信息总数总条数
    total?: number;
    // 信息列表
    message_list?: any[];
    // 信息类型
    messsage_type?: any[];
}

/**
 * 组件：消息类型
 * 描述
 */
export class MessageListView extends ReactView<MessageListViewControl, MessageListViewState> {
    private columns_data_source = [{
        title: '消息名称',
        dataIndex: 'message_name',
        key: 'message_name',
    }, {
        title: '消息内容',
        dataIndex: 'message_content',
        key: 'message_content',
    }, {
        title: '消息类型',
        dataIndex: 'message_type_name',
        key: 'message_type_name',
    }, {
        title: '消息状态',
        dataIndex: 'message_state',
        key: 'message_state',
    }, {
        title: '推送时间',
        dataIndex: 'push_date',
        key: 'push_date',
    }, {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];

    // 创建消息类型
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeMessage);
    }

    // 查看信息详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeMessage + '/' + contents.id);
        }
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let message_option = {
            edit_form_items_props: [{
                type: InputType.input,
                label: "消息名称",
                decorator_id: "name"
            }],
            btn_props: [{
                label: '添加消息',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.message_service,
            service_option: {
                select: {
                    service_func: 'get_all_message_list',
                    service_condition: [{}, 1, 10]
                },
            },
        };
        let message_list = Object.assign(message_option, table_param);
        return (
            <Row>
                <SignFrameLayout {...message_list} />
            </Row>
        );
    }
}

/**
 * 控件：消息类型
 * 描述
 */
@addon('MessageListView', '消息类型', '描述')
@reactControl(MessageListView, true)
export class MessageListViewControl extends ReactViewControl {
}