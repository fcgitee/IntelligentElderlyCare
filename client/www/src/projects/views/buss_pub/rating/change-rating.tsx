import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Select } from "antd";
let Option = Select.Option;
/**
 * 状态：评比编辑页面
 */
export interface CbangeRatingViewState extends ReactViewState {
    happiness_list?: any;
    plan_list?: any;
    isDisabled?: boolean;
    tips?: string;
}

/**
 * 组件：评比编辑页面视图
 */
export class CbangeRatingView extends ReactView<ChangeRatingViewControl, CbangeRatingViewState> {
    constructor(props: ChangeRatingViewControl) {
        super(props);
        this.state = {
            happiness_list: [],
            plan_list: [],
            isDisabled: true,
            tips: '请先选择要评分的幸福院！',
        };
    }
    componentDidMount() {
        // 获取幸福院
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ personnel_category: '幸福院' })!)
            .then((datas: any) => {
                this.setState({
                    happiness_list: datas.result,
                });
            });
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.person_org_manage_service.get_rating_list!({ id: this.props.match!.params.key })!)
                .then((datas: any) => {
                    if (datas && datas.result && datas.result.length > 0 && datas.result[0].hasOwnProperty('org_id')) {
                        this.getRatingPlanList({ org_id: datas.result[0]['org_id'] });
                    }
                });
        }
    }
    getRatingPlanList(param: any) {
        // 获取评比列表
        request(this, AppServiceUtility.person_org_manage_service.get_rating_plan_list!(param)!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    if (datas.result.length > 0) {
                        this.setState({
                            plan_list: datas.result,
                            isDisabled: false,
                            tips: '请选择评比方案！',
                        });
                    } else {
                        this.setState({
                            plan_list: datas.result,
                            isDisabled: false,
                            tips: '该幸福院下没有评比方案！',
                        });
                    }
                }
            });
    }
    happinessListChange = (value: any) => {
        // console.log(value);
        this.getRatingPlanList({ org_id: value });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let haappiness_list: JSX.Element[] = this.state.happiness_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        let plan_list: JSX.Element[] = this.state.plan_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        let edit_props = {
            form_items_props: [{
                title: "评比",
                need_card: true,
                input_props: [
                    {
                        type: InputType.select,
                        label: "幸福院",
                        decorator_id: "org_id",
                        field_decorator_option: {
                            rules: [{ required: true, message: '请选择幸福院' }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: '请选择幸福院',
                            childrens: haappiness_list,
                            autocomplete: 'off',
                            onChange: this.happinessListChange,
                        }
                    },
                    {
                        type: InputType.select,
                        label: "评比方案",
                        decorator_id: "plan_id",
                        field_decorator_option: {
                            rules: [{ required: true, message: this.state.tips }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            disabled: this.state.isDisabled,
                            placeholder: this.state.tips,
                            childrens: plan_list,
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "评比得分",
                        decorator_id: "score",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入评比得分" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入评比得分",
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remark",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入备注" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            autocomplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_rating_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_rating"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：评比编辑页面控件
 * @description 评比编辑页面控件
 * @author
 */
@addon('ChangeRatingViewControl', '评比编辑页面控件', '评比编辑页面控件')
@reactControl(CbangeRatingView, true)
export class ChangeRatingViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}