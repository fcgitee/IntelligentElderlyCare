import { Row } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：评比列表状态
 */
export interface RatingListViewState extends ReactViewState {
}

/**
 * 组件：评比列表
 * 描述
 */
export class RatingListView extends ReactView<RatingListViewControl, RatingListViewState> {
    private columns_data_source = [{
        title: '幸福院名称',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: '评比结果',
        dataIndex: 'score',
        key: 'score',
    }, {
        title: '行政区划',
        dataIndex: 'admin_area_name',
        key: 'admin_area_name',
    }, {
        title: '组织机构类型',
        dataIndex: 'org_type',
        key: 'org_type',
    }, {
        title: '上级组织机构',
        dataIndex: 'parent_org_name',
        key: 'parent_org_name',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeRating);
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeRating + '/' + contents.id);
        }
    }
    componentDidMount() {
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "幸福院名称",
                    decorator_id: "org_name",
                    option: {
                        placeholder: "请输入幸福院名称",
                        autoComplete: 'off',
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_rating_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_rating'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：评比列表
 * 描述
 */
@addon('RatingListView', '评比列表', '描述')
@reactControl(RatingListView, true)
export class RatingListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}