import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, Descriptions, Row, Button, message, Select, Col, Skeleton, } from "antd";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：商城商品审核状态
 */
export class ChangeProductShState {
    product_info?: any;
    status_list?: any;
    reason?: any;
    is_send?: boolean;
    status?: string;
}

/**
 * 组件：商城商品审核
 * 商城商品审核
 */
export class ChangeProductSh extends React.Component<ChangeProductShControl, ChangeProductShState> {
    constructor(props: ChangeProductShControl) {
        super(props);
        this.state = {
            status: '',
            reason: '',
            product_info: [],
            status_list: [],
            is_send: false,
        };
    }
    componentWillMount() {
        if (this.props.match!.params.key) {
            // 商城商品
            request(this, AppServiceUtility.shopping_mall_service.get_goods_list_all!({ id: this.props.match!.params.key }))
                .then((data: any) => {
                    if (data && data.result && data.result.length > 0) {
                        this.setState({
                            product_info: data.result[0],
                        });
                    }
                });
            request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_code: 'PRODUCT_STATUS' })!)
                .then((datas: any) => {
                    if (datas && datas.result) {
                        this.setState({
                            status_list: datas.result,
                        });
                    }
                }).catch((error: Error) => {
                    console.error(error);
                });
        }
    }
    back() {
        history.back();
    }
    change(e: any) {
        this.setState({
            status: e,
        });
    }
    save() {
        let { product_info, reason, is_send, status } = this.state;
        if (!product_info.hasOwnProperty('id')) {
            message.info('没有找到可操作的数据！');
            return;
        }
        if (!status) {
            message.info('请选择审核结果！');
            return;
        }
        if (status !== 'PASSED' && reason === '') {
            message.info('请填写不通过的原因！');
            return;
        }
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.shopping_mall_service.update_goods_sh!({ id: product_info.id, status: status, reason: reason }))
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('操作成功！', 1, () => {
                                this.props.history!.push(ROUTE_PATH.productsSh);
                            });
                        } else {
                            this.setState({
                                is_send: false
                            });
                            message.info('操作失败！');
                        }
                    });
            }
        );
    }
    setRejectReason(e: any) {
        this.setState({
            reason: e.target.value,
        });
    }
    render() {
        let { product_info } = this.state;

        // 初始化，抑制报错
        if (!product_info) {
            product_info = [];
        }

        const { Option } = Select;

        let status_node_list = this.state.status_list.map((item: any) => {
            if (item.code !== 'PENDING_APPROVAL') {
                return (
                    <Option key={item.code} value={item.code}>{item.name}</Option>
                );
            }
            return null;
        });
        return (
            <MainContent>
                <Card className="product-sh">
                    {product_info.hasOwnProperty('id') ? <Row>
                        <Descriptions title="商城商品审核" bordered={true}>
                            <Descriptions.Item label="名称">{product_info['title'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="所属机构">{product_info['organization_name'] && product_info['organization_name'] ? product_info['organization_name'] : ''}</Descriptions.Item>
                            <Descriptions.Item label="价格">{product_info['price'] ? product_info['price'] : ''}</Descriptions.Item>
                            <Descriptions.Item label="库存">{product_info['stock_num'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="配送方式">{product_info['postage_type'] ? product_info['postage_type'].join(',') : ''}</Descriptions.Item>
                            <Descriptions.Item label="快递费用">{product_info['express_fee'] === '买家付费' ? product_info['postage_price'] : product_info['express_fee']}</Descriptions.Item>
                            <Descriptions.Item label="商品图片" span={1}>
                                {product_info['image_urls'] && product_info['image_urls'].length > 0 ? product_info['image_urls'].map((item: any, index: number) => {
                                    return (
                                        <Row key={index} style={{ marginBottom: 10 }}>
                                            <img style={{ maxWidth: '200px', display: 'block' }} src={item} />
                                        </Row>
                                    );
                                }) : null}
                            </Descriptions.Item>
                            <Descriptions.Item label="商品详情" span={2}><div dangerouslySetInnerHTML={{ __html: product_info['content'] || '' }} /></Descriptions.Item>
                            <Descriptions.Item label="提交时间">{product_info['create_date'] ? product_info['create_date'] : ''}</Descriptions.Item>
                        </Descriptions>
                        <MainCard>
                            <Row>
                                <Col span={3}>
                                    <Select style={{ width: '90%' }} placeholder="请选择审核结果" onChange={(e: any) => this.change(e)}>
                                        {status_node_list}
                                    </Select>
                                </Col>
                                <Col span={21}>
                                    <TextArea placeholder="原因" rows={4} onChange={(e) => { this.setRejectReason(e); }} />
                                </Col>
                            </Row>
                            <Row type="flex" justify="center" className="ctrl-btns">
                                <Button disabled={this.state.is_send} type='primary' onClick={() => this.save()}>保存</Button>
                                <Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                            </Row>
                        </MainCard>
                    </Row> : <Skeleton paragraph={{ rows: 8 }} />}
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：商城商品审核控制器
 * 商城商品审核
 */
@addon('ChangeProductSh', '商城商品审核', '商城商品审核')
@reactControl(ChangeProductSh, true)
export class ChangeProductShControl extends BaseReactElementControl {
}