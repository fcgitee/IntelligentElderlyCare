
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Table, Row, Button, Card, Tabs } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { request, toThousands } from "src/business/util_tool";
import "./index.less";
import { ROUTE_PATH } from "src/projects/router";
const { TabPane } = Tabs;
/** 状态：对账单详情 */
export interface FinancialDetailState extends ReactViewState {
    datas_list?: any;
}

/** 组件：对账单详情 */
export class FinancialDetail extends React.Component<FinancialDetailControl, FinancialDetailState> {
    constructor(props: FinancialDetailControl) {
        super(props);
        this.state = {
            datas_list: [],
        };
    }
    componentDidMount = () => {
        let condition: any = {};
        if (this.props.match!.params.key) {
            condition['date'] = this.props.match!.params.key;
        }
        request(this, AppServiceUtility.shopping_mall_service.get_sp_entry_record_list_all!(condition)!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        datas_list: datas.result,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.SPFinancialOverview);
    }
    toOrderDetail = (id: string) => {
        console.log(id);
    }
    onTabsChange = () => {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { datas_list } = this.state;
        return (
            <MainContent>
                <Card>
                    <Tabs onChange={this.onTabsChange} type="card" animated={true}>
                        <TabPane tab="入账" key="入账">
                            <Row type="flex" justify="start" align="middle" className="dzdrow">
                                <span className="mr20">入账</span>
                            </Row>
                            <Table
                                columns={[
                                    {
                                        title: '类型',
                                        dataIndex: 'date',
                                        key: 'date',
                                    },
                                    {
                                        title: '金额（元）',
                                        dataIndex: 'money',
                                        key: 'money',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Row>
                                                    ￥{toThousands(text)}
                                                </Row>
                                            );
                                        }
                                    }, {
                                        title: '入账时间',
                                        dataIndex: 'date',
                                        key: 'date',
                                    }, {
                                        title: '名称',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }, {
                                        title: '订单号',
                                        dataIndex: 'order_number',
                                        key: 'order_number',
                                    }, {
                                        title: '操作',
                                        dataIndex: 'action',
                                        key: 'payment_date',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Button type="link" onClick={() => this.toOrderDetail(record.order_id)}>查看订单详情</Button>
                                            );
                                        }
                                    },
                                ]}
                                dataSource={datas_list}
                                rowKey="id"
                                pagination={false}
                                className="mt20"
                            />
                        </TabPane>
                        <TabPane tab="出账" key="出账">
                            <Row type="flex" justify="start" align="middle" className="dzdrow">
                                <span className="mr20">出账</span>
                            </Row>
                            <Table
                                columns={[
                                    {
                                        title: '类型',
                                        dataIndex: 'date',
                                        key: 'date',
                                    },
                                    {
                                        title: '金额（元）',
                                        dataIndex: 'money',
                                        key: 'money',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Row>
                                                    ￥{toThousands(text)}
                                                </Row>
                                            );
                                        }
                                    }, {
                                        title: '出账时间',
                                        dataIndex: 'date',
                                        key: 'date',
                                    }, {
                                        title: '名称',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }, {
                                        title: '订单号',
                                        dataIndex: 'order_number',
                                        key: 'order_number',
                                    }
                                ]}
                                dataSource={datas_list}
                                rowKey="id"
                                pagination={false}
                                className="mt20"
                            />
                        </TabPane>
                    </Tabs>
                    <Row className="ctrl-btns" type="flex" justify="center" align="middle">
                        <Button onClick={this.backList}>返回</Button>
                    </Row>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：对账单详情控制器
 * @description 对账单详情
 * @author
 */
@addon('FinancialDetail', '对账单详情', '对账单详情')
@reactControl(Form.create<any>()(FinancialDetail), true)
export class FinancialDetailControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
}