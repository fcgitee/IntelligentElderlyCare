
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Button, message, Modal } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import "./index.less";
import { checkPermission } from "src/business/util_tool";
// const Option = Select.Option;
/** 状态：板块列表 */
export interface BlocksViewState extends ReactViewState {
    status_list?: any;
    edit_permission?: boolean;
    delete_permission?: boolean;
}

/** 组件：板块列表 */
export class BlocksView extends React.Component<BlocksViewControl, BlocksViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '板块名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '板块类型',
            dataIndex: 'type_name',
            key: 'type_name',
        }, {
            title: '呈现方式',
            dataIndex: 'show_type',
            key: 'show_type',
        },
        {
            title: '排序',
            dataIndex: 'sort',
            key: 'sort',
            render: (text: any, record: any) => {
                return (
                    <div>
                        <div className="tbfixed">
                            <Row>创建时间：{record.create_date}</Row>
                            <Row>最后修改时间：{record.modify_date}</Row>
                            <Row type="flex" align="middle" justify="end">
                                {record.status === true ? <div>
                                    <span className="status-text success mr20">已开放</span>
                                    <Button onClick={() => this.ctrl('setFalse', record)}>关闭</Button>
                                </div> : <div>
                                        <span className="status-text danger mr20">已关闭</span>
                                        <Button onClick={() => this.ctrl('setTrue', record)}>开放</Button>
                                    </div>}
                            </Row>
                        </div>
                        {text || 0}
                    </div>
                );
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    <Row className="shangcheng-table-action" type="flex" align="middle" justify="end">
                        {this.state.edit_permission ? <Button type="link" size="small" onClick={() => this.ctrl('edit', record)}>编辑</Button> : null}
                        {this.state.delete_permission ? <Button type="link" size="small" onClick={() => this.ctrl('delete', record)}>删除</Button> : null}
                        <Button type="link" size="small" onClick={() => this.ctrl('detail', record)}>明细列表</Button>
                        {this.state.edit_permission ? <Button type="link" size="small" onClick={() => this.ctrl('add', record)}>添加板块推荐</Button> : null}
                    </Row>
                );
            }
        },
    ];
    ctrl = (type: string, record: any) => {
        if (type === 'detail') {
            this.props.history!.push(ROUTE_PATH.blockDetails + '/' + record.id);
        } else if (type === 'add') {
            this.props.history!.push(ROUTE_PATH.changeBlockDetails + '/parent/' + record.id);
        } else if (type === 'edit') {
            this.props.history!.push(ROUTE_PATH.changeBlock + '/' + record.id);
        } else if (type === 'delete') {
            Modal.confirm({
                title: '删除',
                content: '此操作不可撤销，请谨慎操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    request(this, AppServiceUtility.shopping_mall_service.delete_block!({ ids: [record.id] })!)
                        .then((datas: any) => {
                            if (datas && datas === 'Success') {
                                message.success('删除成功！', 1, () => {
                                    this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                });
                            }
                        }).catch((error: Error) => {
                            console.error(error);
                        });
                }
            });
        } else if (type === 'setFalse') {
            Modal.confirm({
                title: '关闭',
                content: '请确认此操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    this.updateBlock({ id: record.id, status: false });
                }
            });
        } else if (type === 'setTrue') {
            Modal.confirm({
                title: '开启',
                content: '请确认此操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    this.updateBlock({ id: record.id, status: true });
                }
            });
        }
    }
    updateBlock = (condition: any) => {
        request(this, AppServiceUtility.shopping_mall_service.update_block!(condition)!)
            .then((datas: any) => {
                if (datas && datas === 'Success') {
                    message.success('操作成功！', 1, () => {
                        this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    constructor(props: BlocksViewControl) {
        super(props);
        this.state = {
            status_list: [],
            edit_permission: false,
            delete_permission: false,
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeBlock + '/' + contents.id);
        }
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeBlock);
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    componentDidMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
            delete_permission: checkPermission(this.props.delete_permission!),
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let blocks = {
            type_show: false,
            btn_props: [{
                label: '添加板块',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            onRef: this.onRef,
        };
        let blocks_list = Object.assign(blocks, table_param);
        return (
            <Row className="shangcheng-table blocks-list-table">
                <SignFrameLayout {...blocks_list} />
            </Row>
        );
    }
}

/**
 * 控件：板块列表控制器
 * @description 板块列表
 * @author
 */
@addon('BlocksView', '板块列表', '板块列表')
@reactControl(Form.create<any>()(BlocksView), true)
export class BlocksViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}