import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Select } from "antd";
const { Option } = Select;
/**
 * 状态：数据字典编辑页面
 */
export interface ChangeDataDictionaryViewState extends ReactViewState {
    // 一级数据字典
    l1_list: any;
}

/**
 * 组件：数据字典编辑页面视图
 */
export class ChangeDataDictionaryView extends ReactView<ChangeDataDictionaryViewControl, ChangeDataDictionaryViewState> {
    constructor(props: ChangeDataDictionaryViewControl) {
        super(props);
        this.state = {
            l1_list: [],
        };
    }
    componentDidMount() {
        if (this.props.type) {
            // 获取信息类型
            request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_list_all!({ in_select: true })!)
                .then((datas: any) => {
                    this.setState({
                        l1_list: datas,
                    });
                });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let l1_option_list: any = this.state.l1_list!.map((item: any) => {
            return (
                <Option key={item['id']}>{item['name']}</Option>
            );
        });
        let title = '';
        let list_url = '';
        if (!this.props.type) {
            // 一级接口
            list_url = 'get_data_dictionary_list_all';
            if (this.props.match!.params.key) {
                title = '编辑一级字典';
            } else {
                title = '新增一级字典';
            }
        } else if (this.props.type === 'changeChild') {
            // 二级接口
            list_url = 'get_data_dictionary_childs_list_all';
            if (this.props.match!.params.key === 'parent') {
                title = '新增二级字典';
            } else {
                title = '编辑二级字典';
            }
        }
        let edit_props = {
            form_items_props: [{
                title: title,
                need_card: true,
                input_props: [
                    {
                        ...(this.props.type && this.props.type === 'changeChild' ? {
                            type: InputType.select,
                            label: "父级字典",
                            decorator_id: 'dictionary_id',
                            field_decorator_option: {
                                initialValue: this.props.match!.params.code,
                                rules: [{ required: true, message: "请选择父级字典" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择父级字典",
                                childrens: l1_option_list,
                                autoComplete: 'off',
                            },
                        } : {})
                    },
                    {
                        type: InputType.antd_input,
                        label: "字典名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入字典名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入字典名称",
                            autoComplete: 'off',
                        }
                    },
                    {
                        type: InputType.antd_input,
                        label: "字典编号",
                        decorator_id: "code",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入字典编号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入字典编号",
                            autoComplete: 'off',
                        }
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "排序号",
                        decorator_id: "index_no",
                        field_decorator_option: {
                            initialValue: 0,
                            rules: [{ required: true, message: "请输入排序号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入排序号",
                            autoComplete: 'off',
                        }
                    },
                    {
                        type: InputType.select,
                        label: "是否系统字典",
                        decorator_id: 'is_sys',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择是否系统字典" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择是否系统字典",
                            childrens: [{
                                label: '是',
                                value: true
                            }, {
                                label: '否',
                                value: false
                            }].map((item: any) => {
                                return (
                                    <Option key={item.value} value={item.value}>{item.label}</Option>
                                );
                            }),
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.select,
                        label: "是否启用",
                        decorator_id: 'state',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择是否启用" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择是否启用",
                            childrens: [{
                                label: '启用',
                                value: '启用'
                            }, {
                                label: '停用',
                                value: '停用'
                            }].map((item: any) => {
                                return (
                                    <Option key={item['value']}>{item['label']}</Option>
                                );
                            }),
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remark",
                        field_decorator_option: {
                            rules: [{ required: false, message: "请输入备注" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            autoComplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.shopping_mall_service,
                operation_option: {
                    query: {
                        func_name: list_url,
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: 'update_data_dictionary'
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：数据字典编辑页面控件
 * @description 数据字典编辑页面控件
 * @author
 */
@addon('ChangeDataDictionaryViewControl', '数据字典编辑页面控件', '数据字典编辑页面控件')
@reactControl(ChangeDataDictionaryView, true)
export class ChangeDataDictionaryViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    public type?: string;
}