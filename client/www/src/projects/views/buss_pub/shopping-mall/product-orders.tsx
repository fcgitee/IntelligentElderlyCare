
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Select, Button, Modal, Icon, Col, Input, Card, DatePicker, message } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { request, checkPermission } from "src/business/util_tool";
import "./index.less";
import { MainContent } from "src/business/components/style-components/main-content";
const Option = Select.Option;
const { RangePicker } = DatePicker;
import locale from 'antd/es/date-picker/locale/zh_CN';
import moment, { Moment } from "moment";
/** 状态：商品订单列表 */

// 修改通用接口
export function commonUpdateOrder(id: string, that: any, condition: any = {}, cb?: Function, action?: string) {
    const is_send = that.state.is_send;
    // 设为已完成
    if (is_send || !id) {
        return;
    }
    that.setState(
        {
            is_send: true,
        },
        () => {
            request(that, AppServiceUtility.shopping_mall_service.update_order!({ id, ...condition }))
                .then((datas: any) => {
                    that.setState({
                        is_send: false,
                        delivery_goods_visible: false,
                    });
                    if (datas === 'Success') {
                        message.success(`${action || '操作'}成功！`, 1, () => {
                            cb && cb();
                        });
                    } else {
                        message.error(`${action || '操作'}失败，${datas}！`);
                    }
                })
                .catch(err => {
                    message.error(`${action || '操作'}失败！`);
                    that.setState({
                        is_send: false,
                    });
                });
        }
    );
}

// 根据对订单的操作类型展示弹窗
export function updateOrder(type: string, id: string, that: any, condition: any, cb?: Function, action?: string) {
    switch (type) {
        // 发货
        case 'setDelivered':
        // 设为已付款（测试）
        case 'setPaid':
            commonUpdateOrder(id, that, condition, cb, action);
            break;
        // 取消订单
        case 'setCanceled':
            Modal.confirm({
                title: '取消',
                content: '您确定要取消该订单吗？取消后订单支付金额将原路退回！',
                okText: '确定',
                cancelText: '关闭',
                icon: (<Icon type="info-circle" />),
                onOk: () => {
                    commonUpdateOrder(id, that, condition, cb, action);
                },
                onCancel: () => {
                    console.log('取消');
                },
            });
            break;
        // 设为已完成
        case 'setCompleted':
            Modal.confirm({
                title: '设为已完成',
                content: '订单完成后不可取消，您确定要将该订单设为已完成状态吗？',
                okText: '确定',
                cancelText: '关闭',
                icon: (<Icon type="info-circle" />),
                onOk: () => {
                    commonUpdateOrder(id, that, condition, cb, action);
                },
                onCancel: () => {
                    console.log('设为已完成取消');
                },
            });
            break;
        // 退款
        case 'setRefunded':
            Modal.confirm({
                title: '退款',
                content: '您确定要退款吗？退款后订单支付金额将原路退回！',
                okText: '确定',
                cancelText: '关闭',
                icon: (<Icon type="info-circle" />),
                onOk: () => {
                    commonUpdateOrder(id, that, condition, cb, action);
                },
                onCancel: () => {
                    console.log('退款取消');
                },
            });
            break;
        // 拒绝退款
        case 'setRefundedRefused':
            Modal.confirm({
                title: '拒绝退款',
                content: '请确认此操作！',
                okText: '确定',
                cancelText: '关闭',
                icon: (<Icon type="info-circle" />),
                onOk: () => {
                    commonUpdateOrder(id, that, condition, cb, action);
                },
                onCancel: () => {
                    console.log('拒绝退款取消');
                },
            });
            break;
        default: break;
    }
}

// 根据订单状态显示不同的操作按钮
export function showActionByOrderStatus(record: any, item: any, that: any) {
    switch (record.order_status) {
        case 'PAID':
            // 已支付可以发货可以退款
            if (record.pay_status === 'REFUNDING') {
                return (
                    <>
                        <Button type="link" size="small" onClick={() => that.ctrl('setRefundedRefused', record)}>拒绝退款</Button>
                        <Button type="link" size="small" onClick={() => that.ctrl('setRefunded', record)}>确认退款</Button>
                    </>
                );
            } else {
                return (
                    <>
                        <Button type="link" size="small" onClick={() => { that.setState({ delivery_goods_visible: true, selected_id: item.id }); }}>发货</Button>
                        <Button type="link" size="small" onClick={() => that.ctrl('setRefunded', record)}>退款</Button>
                    </>
                );
            }
            break;
        case 'WAIT_PAY':
            // 未付款可以取消
            return (
                <>
                    <Button type="link" size="small" onClick={() => that.ctrl('setCanceled', record)}>取消</Button>
                    <Button type="link" size="small" onClick={() => that.ctrl('setPaid', record)}>设为已付款（测）</Button>
                </>
            );
            break;
        case 'DELIVERED':
            // 已发货可以完成
            return (
                <>
                    <Button type="link" size="small" onClick={() => that.ctrl('setCompleted', record)}>设为已完成</Button>
                </>
            );
            break;
        case 'COMPLETED':
            // 已完成可以判断是否退款
            if (record.pay_status === 'REFUNDING') {
                return (
                    <>
                        <Button type="link" size="small" onClick={() => that.ctrl('setRefundedRefused', record)}>拒绝退款</Button>
                        <Button type="link" size="small" onClick={() => that.ctrl('setRefunded', record)}>确认退款</Button>
                    </>
                );
            }
            return null;
            break;
        default:
            return null;
            break;
    }
}

export function showDeliverGoodsModal(that: any, logistics_list: any) {
    return (
        <Modal
            title={''}
            visible={that.state.delivery_goods_visible}
            onOk={() => that.ctrl('setDelivered')}
            onCancel={() => { that.setState({ delivery_goods_visible: false, logistics_number: '' }); }}
            cancelText="关闭"
            okText="发货"
            closable={false}
            width={400}
            className={'fadeConfirm'}
        >
            <Row>
                <Row type="flex" align="middle">
                    <Icon type="info-circle" style={{ color: '#faad14', fontSize: 20, marginRight: 16 }} />
                    <span style={{ fontSize: 16, color: "#202020" }}>发货</span>
                </Row>
                <Row className="mt20" type="flex" align="middle">
                    <Col span={6}>物流公司</Col>
                    <Col span={18}>
                        <Select style={{ width: '100%' }} placeholder="请选择物流公司" onChange={(e) => that.ctrl('setValue', { key: 'logistics_id', value: e })}>
                            {logistics_list.map((item: any) => <Option key={item.code} >{item.name}</Option>)}
                        </Select>
                    </Col>
                </Row>
                <Row className="mt20" type="flex" align="middle">
                    <Col span={6}>物流单号</Col>
                    <Col span={18}><Input placeholder="请输入物流单号" onChange={(e) => that.ctrl('setValue', { key: 'logistics_number', value: e.target.value })} /></Col>
                </Row>
            </Row>
        </Modal>
    );
}

export interface ProductOrdersViewState extends ReactViewState {
    status_list?: any;
    logistics_list?: any;
    service_type_list?: any;
    order_status_list?: any;
    delivery_list?: any;
    edit_permission?: boolean;
    c1_key_value?: any;
    selected_range_value?: any;
    range_date_value?: any;
    data_source?: any;
    is_send?: boolean;
    delivery_goods_visible?: boolean;
    logistics_id?: string;
    logistics_number?: string;
    selected_id?: any;
    extraParam?: any;
}

/** 组件：商品订单列表 */
export class ProductOrdersView extends React.Component<ProductOrdersViewControl, ProductOrdersViewState> {
    private formCreator: any = null;
    private format: any = 'YYYY-MM-DD';
    private c1_key_arr: any = [
        { label: '订单号', key: 'order_number' },
        { label: '收货人姓名', key: 'create_user_name' },
        { label: '收货人手机号', key: 'create_user_mobile' },
        { label: '收货人地址', key: 'address' },
        { label: '买家姓名', key: 'buyer_name' },
        { label: '买家手机号', key: 'buyer_mobile' },
    ];
    private columns_data_source = [
        {
            title: '商品',
            dataIndex: 'product_name',
            key: 'product_name',
            render: (text: any, record: any) => {
                return (
                    record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} type="flex" justify="start" align="middle" style={{ flexFlow: 'unset', justifyContent: 'left', alignItems: 'center' }} className="product-orders-table-row">
                                {item.product_image_urls && item.product_image_urls[0] ? <Row className="product-picture">
                                    <img src={item.product_image_urls[0]} />
                                </Row> : null}
                                <Row className="maxLine3" style={{ WebkitBoxOrient: 'vertical' }}>{item.product_title}</Row>
                            </Row>
                        );
                    })
                );
            }
        },
        {
            title: '价格（元）/数量',
            dataIndex: 'unit_price',
            key: 'unit_price',
            render: (text: any, record: any) => {
                return (
                    record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} className="product-orders-table-row" type="flex" align="middle">
                                <Row>
                                    {item.unit_price}
                                </Row>
                                <Row>
                                    x{item.number}
                                </Row>
                            </Row>
                        );
                    })
                );
            }
        }, {
            title: '买家/收货人',
            dataIndex: 'buyer_name',
            key: 'buyer_name',
            render: (text: any, record: any) => {
                return (
                    record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} className="product-orders-table-row" type="flex" align="middle">
                                <Row>
                                    {record.buyer_name}
                                </Row>
                                <Row>
                                    {record.create_user_name}
                                </Row>
                            </Row>
                        );
                    })
                );
            }
        },
        {
            title: '配送方式',
            dataIndex: 'product_postage_type',
            key: 'product_postage_type',
            render: (text: any, record: any) => {
                return (
                    record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} className="product-orders-table-row" type="flex" align="middle">
                                {(() => {
                                    if (item.product_postage_type) {
                                        if (Array.isArray(item.product_postage_type)) {
                                            let delivery_arr: any = [];
                                            this.state.delivery_list.map((itm: any, idx: number) => {
                                                delivery_arr[itm['code']] = itm['name'];
                                            });
                                            let delivery_name_arr: any = [];
                                            item.product_postage_type.map((itm: any, index: number) => {
                                                if (delivery_arr[itm]) {
                                                    delivery_name_arr.push(delivery_arr[itm]);
                                                }
                                            });
                                            return delivery_name_arr.join('，');
                                        }
                                        return item.product_postage_type;
                                    }
                                })()}
                            </Row>
                        );
                    })
                );
            }
        },
        {
            title: '小计',
            dataIndex: 'true_money',
            key: 'true_money',
            render: (text: any, record: any) => {
                return (
                    <div>
                        <div className="tbfixed">
                            <Row>订单编号{record.order_number}</Row>
                            <Row>
                                <Row className="one-row-two-line">下单时间：{record.create_date}</Row>
                                <Row className="one-row-two-line">结算时间：{record.settlement_date || '-'}</Row>
                            </Row>
                            <Row className="order-detail-link">
                                <Button type="link" onClick={(e) => this.toOrderDetail(e, record)}>详情</Button>
                            </Row>
                        </div>
                        {record.line_info.map((item: any, index: number) => {
                            return (
                                <Row key={index} className="product-orders-table-row" type="flex" align="middle">
                                    {`￥${record.product_price}`}
                                </Row>
                            );
                        })}
                    </div>
                );
            }
        },
        {
            title: '订单状态',
            dataIndex: 'order_status_name',
            key: 'order_status_name',
            render: (text: any, record: any) => {
                return (
                    record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} className="product-orders-table-row" type="flex" align="middle">
                                {record.order_status_name}
                            </Row>
                        );
                    })
                );
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    this.state.edit_permission ? record.line_info.map((item: any, index: number) => {
                        return (
                            <Row key={index} className="product-orders-table-row" type="flex" align="middle" justify="end">
                                {showActionByOrderStatus(record, item, this)}
                            </Row>
                        );
                    }) : null
                );
            }
        },
    ];
    constructor(props: ProductOrdersViewControl) {
        super(props);
        this.state = {
            status_list: [],
            logistics_list: [],
            order_status_list: [],
            service_type_list: [],
            delivery_list: [],
            edit_permission: false,
            c1_key_value: '',
            selected_range_value: '',
            range_date_value: [],
            data_source: [],
            is_send: false,
            delivery_goods_visible: false,
            logistics_id: '',
            logistics_number: '',
            selected_id: '',
            extraParam: {},
        };
    }
    toOrderDetail = (e: any, record: any) => {
        if (record.id) {
            this.props.history!.push(ROUTE_PATH.changeProductOrder + '/' + record.id);
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeProductOrder + '/' + contents.id);
        }
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeProduct);
    }
    componentDidMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
        });
        // 获取审批字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_codes: ['PRODUCT_STATUS', 'LOGISTICS_TYPE', 'ORDER_STATUS', 'DELIVERY_TYPE'], in_select: true })!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    let status_list: any = [];
                    let logistics_list: any = [];
                    let order_status_list: any = [];
                    let delivery_list: any = [];
                    for (let i = 0; i < datas.result.length; i++) {
                        if (datas.result[i]['parent_code'] === 'PRODUCT_STATUS') {
                            status_list.push(datas.result[i]);
                        } else if (datas.result[i]['parent_code'] === 'LOGISTICS_TYPE') {
                            logistics_list.push(datas.result[i]);
                        } else if (datas.result[i]['parent_code'] === 'ORDER_STATUS') {
                            order_status_list.push(datas.result[i]);
                        } else if (datas.result[i]['parent_code'] === 'DELIVERY_TYPE') {
                            delivery_list.push(datas.result[i]);
                        }
                    }
                    this.setState({
                        status_list,
                        logistics_list,
                        order_status_list,
                        delivery_list,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        // 获取商品类型
        request(this, AppServiceUtility.shopping_mall_service.get_service_type_list!({})!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        service_type_list: datas.result
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    onsucess_func = (data: any) => {
        this.setState({
            data_source: data.result,
        });
    }
    headFunc = (data: any) => {
        data = data || [];
        let all_amout = 0;
        if (data.length) {
            for (let i = 0; i < data.length; i++) {
                if (data[i]['pay_status'] === 'PAID' || data[i]['pay_status'] === 'REFUND_REFUSED') {
                    if (data[i].line_info.length) {
                        for (let j = 0; j < data[i].line_info.length; j++) {
                            all_amout += data[i].line_info[j].final_price + data[i].line_info[j].postage_price;
                        }
                    }
                }
            }
        }
        return (
            <Row className="order-all">
                共{data.length}个订单，小计金额￥{all_amout.toFixed(2)}
            </Row>
        );
    }

    reflash = () => {
        this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
    }

    ctrl = (type: string, record?: any) => {
        const { selected_id, logistics_id, logistics_number } = this.state;
        switch (type) {
            case 'setC1Key':
                for (let i = 0; i < this.c1_key_arr.length; i++) {
                    if (record === this.c1_key_arr[i]['key']) {
                        this.setState({
                            c1_key_value: this.c1_key_arr[i]['label']
                        });
                        break;
                    }
                }
                break;
            case 'selectRangeType':
                let begin_date: Moment | boolean = false;
                let end_date: Moment | boolean = false;
                switch (record) {
                    case '今天':
                        begin_date = moment();
                        end_date = begin_date;
                        break;
                    case '昨天':
                        begin_date = moment().day(-1);
                        end_date = begin_date;
                        break;
                    case '近7天':
                        begin_date = moment().days(-7);
                        end_date = moment();
                        break;
                    case '近30天':
                        begin_date = moment().days(-30);
                        end_date = moment();
                        break;
                    default: break;
                }
                if (begin_date && end_date) {
                    this.setState({
                        selected_range_value: record,
                        range_date_value: [begin_date, end_date],
                    });
                }
                break;
            case 'reset':
                record.preventDefault();
                this.setState({
                    selected_range_value: '',
                    range_date_value: '',
                });
                this.props.form.resetFields();
                break;
            case 'setValue':
                this.setState(
                    {
                        [record.key]: record.value,
                    }
                );
                break;
            case 'select':
                this.reflash();
                break;
            case 'setCanceled':
                updateOrder('setCanceled', record.id, this, { order_status: 'CANCELED' }, () => { this.reflash(); }, '取消');
                break;
            case 'setDelivered':
                // 注意此时selected_id是Order_Line的主键id
                if (!logistics_id) {
                    message.error('请选择物流公司！');
                    return;
                }
                if (!logistics_number) {
                    message.error('请输入物流单号！');
                    return;
                }
                if (!/^[0-9a-zA-Z]+$/.test(logistics_number)) {
                    message.error('物流单号只能为英文+数字组合');
                    return;
                }
                updateOrder('setDelivered', selected_id, this, { logistics_id, logistics_number, order_status: 'DELIVERED' }, () => { this.reflash(); }, '发货');
                break;
            // 设为已完成
            case 'setCompleted':
                updateOrder('setCompleted', record.id, this, { order_status: 'COMPLETED' }, () => { this.reflash(); }, '设为已完成');
                break;
            // 设为已付款
            case 'setPaid':
                updateOrder('setPaid', record.id, this, { order_status: 'PAID', pay_status: 'PAID' }, () => { this.reflash(); }, '设为已付款');
                break;
            // 退款
            case 'setRefunded':
                updateOrder('setRefunded', record.id, this, { order_status: 'REFUNDED', pay_status: 'REFUNDED' }, () => { this.reflash(); }, '退款');
                break;
            case 'setRefundedRefused':
                updateOrder('setRefundedRefused', record.id, this, { pay_status: 'REFUND_REFUSED' }, () => { this.reflash(); }, '拒绝退款');
                break;
            default: break;
        }
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            // console.log(values);
            let param: any = {};
            if (values.order_status) {
                param['order_status'] = values.order_status;
            }
            if (values.product_category_id) {
                param['product_category_id'] = values.product_category_id;
            }
            if (values.postage_type) {
                param['postage_type'] = values.postage_type;
            }
            if (values.settlement_status) {
                param['settlement_status'] = values.settlement_status;
            }
            if (values.title) {
                param['title'] = values.title;
            }
            if (values.c1_value && values.c1_key) {
                param[values.c1_key] = values.c1_value;
            }
            if (values.c2_value && values.c2_value[0] && values.c2_key) {
                param[values.c2_key] = values.c2_value;
            }
            this.setState(
                {
                    extraParam: param,
                },
                () => {
                    this.reflash();
                }
            );
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let products = {
            type_show: false,
            btn_props: [{
                label: '发布商品',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: 'get_product_orders_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            searchExtraParam: this.state.extraParam,
            onsucess_func: this.onsucess_func,
            onRef: this.onRef,
        };
        let products_list = Object.assign(products, table_param);
        let { delivery_list, order_status_list, data_source, logistics_list, service_type_list, c1_key_value, selected_range_value, range_date_value } = this.state;
        const { getFieldDecorator } = this.props.form!;
        return (
            <Row className="shangcheng-table product-orders-list-table">
                <MainContent>
                    <Card bordered={false} className="conditions-card">
                        <Form onSubmit={this.handleSubmit}>
                            <Row type="flex" align="middle">
                                <Col span={1} className="mr20">订单搜索</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('c1_key', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部" onChange={(e) => this.ctrl('setC1Key', e)}>
                                                {this.c1_key_arr.map((item: any) => <Option key={item.key} >{item.label}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('c1_value', {
                                        })(
                                            <Input placeholder={c1_key_value ? `请输入要搜索的${c1_key_value}` : `请选择要搜索的条件`} />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row type="flex" align="middle" className="mt20">
                                <Col span={1} className="mr20">订单搜索</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('c2_key', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部">
                                                {[
                                                    { label: '下单日期', key: 'create_date' },
                                                    { label: '结算日期', key: 'settlement_date' },
                                                ].map((item: any) => <Option key={item.key} >{item.label}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('c2_value', {
                                            initialValue: range_date_value,
                                        })(
                                            <RangePicker
                                                format={this.format}
                                                locale={locale}
                                            // onChange={(e: any) => {
                                            //     this.setFormCondition(e, 'c2_value');
                                            // }}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    {['今天', '昨天', '近7天', '近30天'].map((item: any, index: number) => {
                                        return (
                                            <Button onClick={() => this.ctrl('selectRangeType', item)} style={{ marginRight: 10 }} key={index} {...(selected_range_value === item ? { type: 'primary' } : {})}>{item}</Button>
                                        );
                                    })}
                                </Col>
                            </Row>
                            <Row type="flex" align="middle" className="mt20">
                                <Col span={1} className="mr20">商品名称</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('title', {
                                        })(
                                            <Input placeholder="请输入要搜索的商品名称" />
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={1} className="mr20">商品类型</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('product_category_id', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部">
                                                {service_type_list.map((item: any) => <Option key={item.id} >{item.name}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={1} className="mr20">订单状态</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('order_status', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部">
                                                {order_status_list.map((item: any) => <Option key={item.code} >{item.name}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={1} className="mr20">配送方式</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('postage_type', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部" >
                                                {delivery_list.map((item: any) => <Option key={item.code} >{item.name}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row type="flex" align="middle" className="mt20">
                                <Col span={1} className="mr20">结算状态</Col>
                                <Col span={4} className="mr20">
                                    <Form.Item className="mb0">
                                        {getFieldDecorator('settlement_status', {
                                        })(
                                            <Select style={{ width: '100%' }} placeholder="全部" >
                                                {['已结算', '未结算'].map((item: any) => <Option key={item} >{item}</Option>)}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle" className="mt20">
                                {this.headFunc(data_source)}
                                <Row>
                                    <Button type="primary" className="mr20" htmlType="submit">
                                        查询
                                </Button>
                                    <Button onClick={(e) => this.ctrl('reset', e)}>
                                        重置
                                </Button>
                                </Row>
                            </Row>
                        </Form>
                    </Card>
                </MainContent>
                <SignFrameLayout {...products_list} />
                {showDeliverGoodsModal(this, logistics_list)}
            </Row>
        );
    }
}

/**
 * 控件：商品订单列表控制器
 * @description 商品订单列表
 * @author
 */
@addon('ProductOrdersView', '商品订单列表', '商品订单列表')
@reactControl(Form.create<any>()(ProductOrdersView), true)
export class ProductOrdersViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}