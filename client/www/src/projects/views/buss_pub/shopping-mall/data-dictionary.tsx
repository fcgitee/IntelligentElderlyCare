
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Table, Modal, Card, Col, Input, Button, message } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
/** 状态：数据字典 */
export interface DataDictionaryViewState extends ReactViewState {
    // 页码配置
    pagination?: any;
    // 删除弹框是否显示
    delete_visible?: boolean;
    // 搜索参数
    condition?: any;
    // 数据列表
    data_list?: any;
    // 要删除的数据
    delete_data?: any;
    // 发送状态
    is_send?: boolean;
}

/** 组件：数据字典 */
export class DataDictionaryView extends React.Component<DataDictionaryViewControl, DataDictionaryViewState> {
    private columns_data_source = [
        {
            title: '字典名称',
            dataIndex: 'name',
            key: 'name',
            width: 100,
        }, {
            title: '字典代码',
            dataIndex: 'code',
            key: 'code',
            width: 100,
        }, {
            title: '是否系统字典',
            dataIndex: 'is_sys',
            key: 'is_sys',
            width: 100,
            render: (text: any, record: any) => {
                return text === true ? '是' : '否';
            }
        },
        {
            title: '状态',
            dataIndex: 'state',
            key: 'state',
            width: 100,
        }, {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
            width: 100,
        }, {
            title: '最后修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
            width: 150,
        }, {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
            width: 150,
        }, {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            width: 150,
            render: (text: string, record: any) => (
                <div>
                    {record.hasOwnProperty('dictionary_id') ? null : <span style={{ marginRight: '10px' }}>
                        <Button size="small" onClick={() => { this.onIconClick('icon_add', record); }} >
                            新增
                        </Button>
                    </span>}
                    <span style={{ marginRight: '10px' }}>
                        <Button size="small" onClick={() => { this.onIconClick('icon_edit', record); }} >
                            编辑
                        </Button>
                    </span>
                    <span>
                        <Button type="danger" size="small" onClick={() => { this.onClickDel(record); }} >
                            删除
                        </Button>
                    </span>
                </div>
            ),
        }
    ];
    constructor(props: DataDictionaryViewControl) {
        super(props);
        this.state = {
            pagination: {},
            condition: {},
            delete_visible: false,
            is_send: false,
            data_list: [],
            delete_data: {},
        };
    }
    componentDidMount = () => {
        this.get_data_list();
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            if (contents.hasOwnProperty('dictionary_id')) {
                this.props.history!.push(ROUTE_PATH.changeChildDataDictionary + '/' + contents.id);
            } else {
                this.props.history!.push(ROUTE_PATH.changeDataDictionary + '/' + contents.id);
            }
        } else if ('icon_add' === type) {
            this.props.history!.push(ROUTE_PATH.changeChildDataDictionary + '/parent/' + contents.id);
        }
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeDataDictionary);
    }
    handleTableChange = (pagination: any, filters: any, sorter: any) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
    }
    onClickDel = (content: any) => {
        this.setState({
            delete_visible: true,
            delete_data: content
        });
    }
    handleDelete = () => {
        let { is_send, delete_data } = this.state;
        if (is_send || !delete_data.hasOwnProperty('id')) {
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let condition: any = {};
                if (delete_data.hasOwnProperty('dictionary_id')) {
                    condition['type'] = '2';
                } else {
                    condition['type'] = '1';
                }
                condition['ids'] = [delete_data['id']];
                request(this, AppServiceUtility.shopping_mall_service.delete_data_dictionary!(condition)!)
                    .then((datas: any) => {
                        if (datas && datas === 'Success') {
                            message.success('删除成功！');
                            this.setState(
                                {
                                    is_send: false,
                                    delete_visible: false,
                                }
                            );
                            this.get_data_list();
                        } else {
                            this.setState(
                                {
                                    is_send: false,
                                    delete_visible: false,
                                }
                            );
                            message.error(`删除失败【${datas}】`);
                        }
                    }).catch((error: Error) => {
                        message.error(`删除失败！`);
                        console.error(error);
                        this.setState(
                            {
                                is_send: false,
                                delete_visible: false,
                            }
                        );
                    });
            }
        );
    }
    handleCancelDelete = () => {
        this.setState({
            delete_visible: false,
        });
    }
    handleSearch = (e: any) => {
        e.preventDefault();
        let { props } = this;
        props.form!.validateFields((err: any, values: any) => {
            this.get_data_list(values, 1);
        });
    }
    get_data_list = (condition: any = {}, page: number = 1, count: number = 10) => {
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_list_all!(condition, page, count)!)
            .then((datas: any) => {
                if (datas && datas['result']) {
                    this.setState({
                        data_list: datas['result'],
                    });
                }
            }).catch((error: Error) => {
                message.error(error.message);
            });
    }
    setSearchValue(e: any, value: any) {
        let condition = this.state.condition;
        condition[value.decorator_id] = e;
        this.setState({
            condition,
        });
    }
    reset() {
        let { form } = this.props;
        form.resetFields();
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "字典名称",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入要搜索的字典名称',
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.input,
                    label: "字典代码",
                    decorator_id: "code",
                    option: {
                        placeholder: '请输入要搜索的字典代码',
                        autoComplete: 'off',
                    }
                },
            ],
            btn_props: [
                {
                    label: '新增',
                    btn_method: this.add,
                    icon: 'plus'
                },
            ],
        };
        const { getFieldDecorator } = this.props.form!;

        const expandedRowRender = (item: any) => {
            // const data_list: any = this.state.data_list;
            const columns = this.columns_data_source;

            const childs_data: any = [];
            for (let i = 0; i < item['childs'].length; i++) {
                item['childs'][i]['key'] = item['childs'][i]['id'];
                childs_data.push(item['childs'][i]);
            }
            return <Table columns={columns} dataSource={childs_data} pagination={false} />;
        };
        return (
            <MainContent>
                <Card>
                    <div className='form-bottom'>
                        <Form
                            className="nt-sign-manage-layout"
                            onSubmit={this.handleSearch}
                        >
                            <Row gutter={24}>
                                {
                                    json_info.edit_form_items_props ? json_info.edit_form_items_props.map((item, i) =>
                                        (
                                            <Col span={8} key={i}>
                                                <Form.Item
                                                    label={item.label}
                                                >
                                                    {getFieldDecorator(item.decorator_id, {
                                                        initialValue: '',
                                                    })(
                                                        <Input onChange={(e) => this.setSearchValue(e, item)} {...item.option} />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        )
                                    )
                                        :
                                        null
                                }
                                <Col span={24}>
                                    <Row type="flex" style={{ flexDirection: "row-reverse" }}>
                                        <Button type='ghost' htmlType='button' onClick={() => this.reset()} >重置</Button>
                                        <Button htmlType="submit" type='primary' className='res-btn'>查询</Button>
                                    </Row>
                                </Col>
                            </Row >
                            <Row type="flex" >
                                {
                                    json_info.btn_props ?
                                        json_info.btn_props !== undefined && json_info.btn_props.map((item, idx) => {
                                            return <Button type='primary' onClick={item.btn_method} key={idx} icon={item.icon!} style={{ marginLeft: '10px' }}>{item.label}</Button>;
                                        }) : <span />

                                }
                            </Row>
                        </Form>
                    </div>
                    <Table
                        className="components-table-demo-nested"
                        columns={this.columns_data_source}
                        rowKey='id'
                        defaultExpandAllRows={true}
                        expandedRowRender={expandedRowRender}
                        dataSource={this.state.data_list}
                        pagination={this.state.pagination}
                        onChange={this.handleTableChange}
                    />
                </Card>
                <Modal
                    title="注意！"
                    visible={this.state.delete_visible}
                    onOk={this.handleDelete}
                    onCancel={this.handleCancelDelete}
                    okText="确认删除"
                    cancelText="取消"
                >
                    <p>是否删除该行信息</p>
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：数据字典控制器
 * @description 数据字典
 * @author
 */
@addon('DataDictionaryView', '数据字典', '数据字典')
@reactControl(Form.create<any>()(DataDictionaryView), true)
export class DataDictionaryViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}