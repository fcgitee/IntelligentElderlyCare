import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Select } from "antd";
const { Option } = Select;
/**
 * 状态：编辑板块页面
 */
export interface ChangeBlockViewState extends ReactViewState {
    block_type_list?: any;
}

/**
 * 组件：编辑板块页面视图
 */
export class ChangeBlockView extends ReactView<ChangeBlockViewControl, ChangeBlockViewState> {
    constructor(props: ChangeBlockViewControl) {
        super(props);
        this.state = {
            block_type_list: [],
        };
    }
    componentDidMount() {
        // 获取审批字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_code: 'BLOCK_TYPE' })!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        block_type_list: datas.result,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let title: string = '';
        if (this.props.match!.params.key) {
            title = '编辑板块';
        } else {
            title = '新增板块';
        }
        let edit_props = {
            form_items_props: [{
                title: title,
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "板块名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入板块名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入板块名称",
                            autoComplete: 'off',
                        }
                    },
                    {
                        type: InputType.select,
                        label: "板块类型",
                        decorator_id: 'type',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择板块类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择板块类型",
                            childrens: this.state.block_type_list.map((item: any) => {
                                return (
                                    <Option key={item.code}>{item.name}</Option>
                                );
                            }),
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.select,
                        label: "展示方式",
                        decorator_id: 'show_type',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择展示方式" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择展示方式",
                            childrens: [
                                {
                                    label: '1 x 1',
                                    value: '1x1'
                                }, {
                                    label: '1 x 2',
                                    value: '1x2'
                                }, {
                                    label: '2 x 5',
                                    value: '2x5'
                                }
                            ].map((item: any) => {
                                return (
                                    <Option key={item.value} >{item.label}</Option>
                                );
                            }),
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.radioGroup,
                        label: "是否启用",
                        decorator_id: 'status',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择是否启用" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            options: [{
                                label: '是',
                                value: true,
                            }, {
                                label: '否',
                                value: false,
                            }],
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "序号",
                        decorator_id: "sort",
                        field_decorator_option: {
                            initialValue: 0,
                            rules: [{ required: true, message: "请输入序号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "值越大，越靠前",
                            autoComplete: 'off',
                        }
                    },
                    {
                        type: InputType.text_area,
                        label: "板块描述",
                        decorator_id: "desc",
                        field_decorator_option: {
                            rules: [{ required: false, message: "请输入板块描述" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入板块描述",
                            autoComplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.shopping_mall_service,
                operation_option: {
                    query: {
                        func_name: "get_blocks_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: 'update_block'
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑板块页面控件
 * @description 编辑板块页面控件
 * @author
 */
@addon('ChangeBlockViewControl', '编辑板块页面控件', '编辑板块页面控件')
@reactControl(ChangeBlockView, true)
export class ChangeBlockViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}