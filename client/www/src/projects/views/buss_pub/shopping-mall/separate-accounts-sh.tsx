import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Button, message, Modal } from 'antd';
// import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { toThousands, checkPermission } from "src/business/util_tool";
import "./index.less";
/** 状态：资金明细 */
export interface SeparateAccountsSHState extends ReactViewState {
    edit_permission?: boolean;
    selected_list?: any;
}

/** 组件：资金明细 */
export class SeparateAccountsSH extends React.Component<SeparateAccountsSHControl, SeparateAccountsSHState> {
    private columns_data_source = [
        {
            title: '服务商',
            dataIndex: 'service_provider_name',
            key: 'service_provider_name',
        },
        {
            title: '日期',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: '收入订单金额',
            dataIndex: 'income_money',
            key: 'income_money',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '收入订单数',
            dataIndex: 'income_order_count',
            key: 'income_order_count',
        },
        {
            title: '支出订单金额',
            dataIndex: 'output_money',
            key: 'output_money',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '支出订单数',
            dataIndex: 'output_order_count',
            key: 'output_order_count',
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    this.state.edit_permission ? (
                        <Row className="shangcheng-table-action" type="flex" align="middle" justify="end">
                            <Button type="link" size="small" onClick={() => this.ctrl('不通过', record)}>不通过</Button>
                            <Button type="link" size="small" onClick={() => this.ctrl('通过', record)}>通过</Button>
                        </Row>
                    ) : null
                );
            }
        },
    ];
    constructor(props: SeparateAccountsSHControl) {
        super(props);
        this.state = {
            edit_permission: false,
            selected_list: [],
        };
    }
    componentDidMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
        });
    }
    ctrl = (action: string, record: any) => {
        console.log(action, record);
    }
    access = () => {
        if (this.state.selected_list.length === 0) {
            message.error('请选择要操作的数据！');
            return;
        }
        Modal.confirm({
            title: '审核通过',
            content: '请确认此操作！',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                console.log('ok');
            }
        });
    }
    onRowSelection = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({
            selected_list: selectedRowKeys
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let separate = {
            type_show: false,
            btn_props: [{
                label: '通过审核',
                btn_method: this.access,
            }],
            on_row_selection: this.onRowSelection,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: 'get_separate_accounts_sh_list_all',
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
        };
        let separate_list = Object.assign(separate, table_param);
        return (
            <Row>
                <SignFrameLayout {...separate_list} />
            </Row>
        );
    }
}

/**
 * 控件：资金明细控制器
 * @description 资金明细
 * @author
 */
@addon('SeparateAccountsSH', '资金明细', '资金明细')
@reactControl(Form.create<any>()(SeparateAccountsSH), true)
export class SeparateAccountsSHControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}