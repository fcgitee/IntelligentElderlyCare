import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Form, Spin, Row } from 'antd';
import "./index.less";
import { request, setMainFormTitle } from "src/business/util_tool";
import { Button, Carousel, WhiteSpace, WingBlank } from "antd-mobile";
/** 状态：商品预览 */
export interface ProductPreviewState extends ReactViewState {
    data_info: any;
    is_loading: boolean;
    rate: number;
    is_submited: boolean;
}

/** 组件：商品预览 */
export class ProductPreview extends React.Component<ProductPreviewControl, ProductPreviewState> {
    constructor(props: ProductPreviewControl) {
        super(props);
        this.state = {
            data_info: [],
            is_loading: true,
            is_submited: false,
            rate: 0,
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.shopping_mall_service.get_goods_list!({ id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    is_loading: false,
                });
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    is_loading: false,
                });
            });
    }
    ctrl = (action: string, record: any) => {
        console.log(action, record);
    }
    render() {
        setMainFormTitle('商品详情');
        const { data_info, is_loading, is_submited } = this.state;
        const cssStyle: Object = {
            width: 375,
            // background: '#fff',
            margin: '10px auto 0',
            boxShadow: '0 0 10px rgba(0,0,0,.3)',
            borderRadius: 15,
            overflow: 'hidden',
            fontFamily: '宋体',
            minHeight: 'calc(100vh - 115px)',
        };
        return (
            <Row style={cssStyle}>
                <Spin spinning={is_loading}>
                    <Row className="shopping-center-layout">
                        <WhiteSpace size="lg" />
                        <WingBlank>
                            <Row className="product-detail-section">
                                {data_info && data_info.image_urls && data_info.image_urls.map ? <Row className="product-detail-carousel">
                                    <Carousel
                                        autoplay={true}
                                        infinite={true}
                                        autoplayInterval={3000}
                                        className="product-detail-carousel-main"
                                    >
                                        {data_info.image_urls.map((itm: any, idx: number) => (
                                            <span
                                                key={idx}
                                            >
                                                <img
                                                    src={itm}
                                                    onLoad={() => {
                                                        window.dispatchEvent(new Event('resize'));
                                                    }}
                                                />
                                            </span>
                                        ))}
                                    </Carousel>
                                </Row> : null}
                                <WhiteSpace size="sm" />
                                <Row className="product-detail-title-price-sales">
                                    <WingBlank>
                                        <Row>{data_info.title}</Row>
                                        <WhiteSpace size="sm" />
                                        <Row type="flex" justify="space-between" align="bottom">
                                            <span className="product-detail-price">{data_info.price ? `￥${data_info.price}` : null}</span>
                                            <span>销量 {data_info.sales_num || 0}</span>
                                        </Row>
                                        <WhiteSpace size="sm" />
                                        {this.props.match!.params.key === '商品' ? <Row>
                                            <Row type="flex" justify="space-between" align="bottom">
                                                <span>{`快递：${data_info.postage_price || 0}`}</span>
                                                <span>{`库存 ${data_info.stock_num > 999 ? '999+' : (data_info.stock_num ? data_info.stock_num : 0)}`}</span>
                                            </Row>
                                            <WhiteSpace size="sm" />
                                        </Row> : null}
                                    </WingBlank>
                                </Row>
                            </Row>
                            <WhiteSpace size="lg" />
                            {/* <Row className="product-detail-section product-detail-organization" >
                            <WingBlank>
                                <Row className="product-oraganization-main">
                                    <Row className="product-oraganization-picture">
                                        {data_info.organization_picture && data_info.organization_picture[0] ? <img src={data_info.organization_picture[0]} /> : null}
                                    </Row>
                                    <Row className="product-oraganization-name-rate" type="flex">
                                        <Row>{data_info.organization_name}</Row>
                                        <Row>
                                            <Rate character={<Icon type="heart" />} onChange={this.changeRate} style={{ fontSize: '12px' }} value={rate} allowHalf={true} />
                                            {rate ? <span className="ant-rate-text" style={{ fontSize: '12px' }}>{rate}分</span> : null}
                                        </Row>
                                    </Row>
                                    <Row className="product-oraganization-hotline">
                                        {data_info.organization_telephone ? <a href={`tel:` + data_info.organization_telephone}>客服热线</a> : <a onClick={this.noTelephone}>客服热线</a>}
                                    </Row>
                                </Row>
                            </WingBlank>
                        </Row>
                        <WhiteSpace size="lg" /> */}
                            <Row className="product-detail-section">
                                <WingBlank>
                                    <WhiteSpace size="sm" />
                                    <Row type="flex" justify="center" align="middle" className="s_module_name"><span><i className="before" />详情介绍<i className="after" /></span></Row>
                                    <WhiteSpace size="sm" />
                                    <Row className="reach-text">
                                        <div dangerouslySetInnerHTML={{ __html: data_info.content || '' }} />
                                    </Row>
                                    <WhiteSpace size="sm" />
                                </WingBlank>
                            </Row>
                            {(() => {
                                let wp: any = [];
                                for (let i = 0; i < 6; i++) {
                                    wp.push(<WhiteSpace size="lg" key={i} />);
                                }
                                return wp;
                            })()}
                            <div className="app-fade-pr">
                                <div className="app-shop-btn-layout app-shop-btn-layout-fix app-shop-btn-layout-white">
                                    <Button className="app-shop-btn" style={{ width: '100%' }} disabled={is_submited} type='primary'>立即购买</Button>
                                </div>
                            </div>
                        </WingBlank>
                    </Row>
                </Spin>
            </Row>
        );
    }
}

/**
 * 控件：商品预览控制器
 * @description 商品预览
 * @author
 */
@addon('ProductPreview', '商品预览', '商品预览')
@reactControl(Form.create<any>()(ProductPreview), true)
export class ProductPreviewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}