
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Button, Modal, message } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import "./index.less";
import { checkPermission, request } from "src/business/util_tool";
/** 状态：板块详情列表 */
export interface BlockDetailsViewState extends ReactViewState {
    status_list?: any;
    edit_permission?: boolean;
    delete_permission?: boolean;
}

/** 组件：板块详情列表 */
export class BlockDetailsView extends React.Component<BlockDetailsViewControl, BlockDetailsViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '宣传图',
            dataIndex: 'image_url',
            key: 'image_url',
            render: (text: any, record: any) => {
                return (
                    text && text[0] ? <img src={text[0]} style={{ maxWidth: 100, maxHeight: 100 }} /> : null
                );
            }
        },
        {
            title: '标题',
            dataIndex: 'title',
            key: 'title',
            render: (text: any, record: any) => {
                return (
                    <Row className="maxLine3" style={{ WebkitBoxOrient: 'vertical' }}>{text}</Row>
                );
            }
        },
        {
            title: '关联项',
            dataIndex: 'product_name',
            key: 'product_name',
            render: (text: any, record: any) => {
                return (
                    record.product_id ? <div className="abutton" onClick={() => this.ctrl('toDetail', record)} ><Row className="maxLine3" style={{ WebkitBoxOrient: 'vertical' }}>{text}</Row></div> : (record.service_name ? record.service_name : null)
                );
            }
        },
        {
            title: '排序',
            dataIndex: 'sort',
            key: 'sort',
            render: (text: any, record: any) => {
                return (
                    <div>
                        <div className="tbfixed">
                            <Row>创建时间：{record.create_date}</Row>
                            <Row>最后修改时间：{record.modify_date}</Row>
                            <Row type="flex" align="middle" justify="end">
                                {record.status === true ? <div>
                                    <span className="status-text success mr20">已开放</span>
                                    <Button onClick={() => this.ctrl('setFalse', record)}>关闭</Button>
                                </div> : <div>
                                        <span className="status-text danger mr20">已关闭</span>
                                        <Button onClick={() => this.ctrl('setTrue', record)}>开放</Button>
                                    </div>}
                            </Row>
                        </div>
                        {text || 0}
                    </div>
                );
            }
        }, {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render: (text: any) => {
                return text === true ? '启用' : '非启用';
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    <Row className="shangcheng-table-action" type="flex" align="middle" justify="end">
                        {this.state.edit_permission ? <Button type="link" size="small" onClick={() => this.ctrl('edit', record)}>编辑</Button> : null}
                        {this.state.delete_permission ? <Button type="link" size="small" onClick={() => this.ctrl('delete', record)}>删除</Button> : null}
                    </Row>
                );
            }
        },
    ];
    ctrl = (type: string, record: any) => {
        if (type === 'edit') {
            this.props.history!.push(ROUTE_PATH.changeBlockDetails + '/' + record.id + '/' + this.props.match!.params.key);
        } else if (type === 'delete') {
            Modal.confirm({
                title: '删除',
                content: '此操作不可撤销，请谨慎操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    request(this, AppServiceUtility.shopping_mall_service.delete_recommend!({ ids: [record.id] })!)
                        .then((datas: any) => {
                            if (datas && datas === 'Success') {
                                message.success('删除成功！', 1, () => {
                                    this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                });
                            }
                        }).catch((error: Error) => {
                            console.error(error);
                        });
                }
            });
        } else if (type === 'toDetail') {
            if (record.type === '商品') {
                this.props.history!.push(ROUTE_PATH.changeProduct + '/' + record.product_id);
            } else if (record.type === '居家服务') {
                this.props.history!.push(ROUTE_PATH.changeServiceItemPackage + '/' + record.product_id);
            } else if (record.type === '服务') {
                this.props.history!.push(ROUTE_PATH.changeServicesItemCategory + '/' + record.product_id);
            }
        } else if (type === 'setFalse') {
            Modal.confirm({
                title: '关闭',
                content: '请确认此操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    this.updateRecommend({ id: record.id, status: false });
                }
            });
        } else if (type === 'setTrue') {
            Modal.confirm({
                title: '开启',
                content: '请确认此操作！',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                    this.updateRecommend({ id: record.id, status: true });
                }
            });
        }
    }
    updateRecommend = (condition: any) => {
        request(this, AppServiceUtility.shopping_mall_service.update_recommend!(condition)!)
            .then((datas: any) => {
                if (datas && datas === 'Success') {
                    message.success('操作成功！', 1, () => {
                        this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    constructor(props: BlockDetailsViewControl) {
        super(props);
        this.state = {
            status_list: [],
            edit_permission: false,
            delete_permission: false,
        };
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeBlockDetails + '/parent/' + this.props.match!.params.key);
    }
    componentDidMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
            delete_permission: checkPermission(this.props.delete_permission!),
        });
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        if (this.props.match!.params.key) {
            param['recommend_module_id'] = this.props.match!.params.key;
        }
        let block_details = {
            type_show: false,
            btn_props: [{
                label: '添加推荐',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'delete_recommend'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            searchExtraParam: param,
            onRef: this.onRef,
        };
        let block_details_list = Object.assign(block_details, table_param);
        return (
            <Row className="shangcheng-table blocks-list-table">
                <SignFrameLayout {...block_details_list} />
            </Row>
        );
    }
}

/**
 * 控件：板块详情列表控制器
 * @description 板块详情列表
 * @author
 */
@addon('BlockDetailsView', '板块详情列表', '板块详情列表')
@reactControl(Form.create<any>()(BlockDetailsView), true)
export class BlockDetailsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}