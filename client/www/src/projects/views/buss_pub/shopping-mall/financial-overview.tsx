
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Table, Row, Button, Card, Select, DatePicker, message } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { request, toThousands } from "src/business/util_tool";
import "./index.less";
import { ROUTE_PATH } from "src/projects/router";

const Option = Select.Option;
const { MonthPicker } = DatePicker;
/** 状态：财务总览 */
export interface FinancialOverviewViewState extends ReactViewState {
    datas_list?: any;
    statistics?: any;
    date_type?: string;
    selected_date: any;
}

/** 组件：财务总览 */
export class FinancialOverviewView extends React.Component<FinancialOverviewViewControl, FinancialOverviewViewState> {
    constructor(props: FinancialOverviewViewControl) {
        super(props);
        this.state = {
            statistics: [],
            datas_list: [],
            date_type: '日汇总',
            selected_date: '',
        };
    }
    componentDidMount = () => {
        this.getDatas();
    }
    getDatas = (condition: any = {}) => {
        if (this.props.request_url) {
            request(this, AppServiceUtility.shopping_mall_service[this.props.request_url]!(condition)!)
                .then((datas: any) => {
                    if (datas && datas.result) {
                        this.setState({
                            datas_list: datas.result,
                            statistics: datas.statistics,
                        });
                    }
                }).catch((error: Error) => {
                    console.error(error);
                });
        }
    }
    toDetail = (date: string) => {
        this.props.history!.push(ROUTE_PATH.SPFinancialDetail + '/' + date);
    }
    changeDateType = (e: any) => {
        this.setState({
            date_type: e,
            selected_date: '',
        });
    }
    onDateChange = (e: any) => {
        this.setState({
            selected_date: e,
        });
    }
    toEntryRecord = () => {
        this.props.history!.push(ROUTE_PATH.SPEntryRecord);
    }
    toFundDetail = () => {
        this.props.history!.push(ROUTE_PATH.capitalDetail);
    }
    select = () => {
        const { date_type, selected_date } = this.state;
        if (date_type !== '' && selected_date === '') {
            message.warning(`请选择${date_type === '日汇总' ? '日期' : '月份'}`);
            return;
        }
        this.getDatas({ date_type: date_type, date_value: selected_date });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { datas_list, statistics, date_type } = this.state;
        return (
            <MainContent>
                <Card>
                    <Table
                        columns={[
                            {
                                title: '待财务审核金额（审核通过后结算至银行卡）',
                                dataIndex: 'waiting',
                                key: 'waiting',
                                width: 400,
                                render: (text: any, record: any) => {
                                    return (
                                        <Row>
                                            ￥{toThousands(text)}
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '已完成订单总金额',
                                dataIndex: 'finished',
                                key: 'finished',
                                width: 400,
                                render: (text: any, record: any) => {
                                    return (
                                        <Row>
                                            ￥{toThousands(text)}
                                        </Row>
                                    );
                                }
                            }, {
                                title: '已付款至银行卡总金额',
                                dataIndex: 'paid',
                                key: 'paid',
                                width: 400,
                                render: (text: any, record: any) => {
                                    return (
                                        <Row>
                                            ￥{toThousands(text)}
                                        </Row>
                                    );
                                }
                            }
                        ]}
                        dataSource={statistics}
                        rowKey="id"
                        pagination={false}
                        className="statistics-table"
                    />
                    {this.props.select_permission === '服务商对账查询' ? <Row type="flex" justify="start" align="middle" className="mt20">
                        <Row className="mr20"><Button onClick={this.toEntryRecord}>入账记录</Button></Row>
                        <Row className="font-red">订单完成后1-5个工作日，商品收入会结算至您的银行卡</Row>
                    </Row> : null}
                    {/* {this.props.select_permission === '平台对账查询' ? <Row type="flex" justify="start" align="middle" className="mt20">
                        <Row className="mr20"><Button type="primary" onClick={this.toFundDetail}>资金明细</Button></Row>
                    </Row> : null} */}
                    <Row type="flex" justify="start" align="middle" className="dzdrow">
                        <span className="mr20">对账单</span>订单完成后进行统计，如有出入，请您在“商品订单”模块，查看相关订单的结算时间
                    </Row>
                    <Row type="flex" justify="start" align="middle" className="mt20">
                        <Row className="mr20">汇总类型</Row>
                        <Row className="mr20">
                            <Select onChange={this.changeDateType} style={{ width: 150 }} placeholder="请选择汇总周期" defaultValue="日汇总">
                                <Option key={'日汇总'} >日汇总</Option>
                                <Option key={'月汇总'} >月汇总</Option>
                            </Select>
                        </Row>
                        <Row className="mr20">
                            {date_type === '月汇总' ? <MonthPicker onChange={this.onDateChange} placeholder="请选择月份" /> : <DatePicker onChange={this.onDateChange} placeholder="请选择日期" />}
                        </Row>
                        <Row>
                            <Button type='primary' onClick={this.select}>查询</Button>
                        </Row>
                    </Row>
                    <Table
                        columns={[
                            {
                                title: '日期',
                                dataIndex: 'date',
                                key: 'date',
                            },
                            {
                                title: '收入',
                                dataIndex: 'income',
                                key: 'income',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="font-green">
                                            +{toThousands(text)}
                                        </Row>
                                    );
                                }
                            }, {
                                title: '支出',
                                dataIndex: 'outpaid',
                                key: 'outpaid',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row>
                                            -{toThousands(text)}
                                        </Row>
                                    );
                                }
                            }, {
                                title: '余额',
                                dataIndex: 'account',
                                key: 'account',
                            }, {
                                title: '操作',
                                dataIndex: 'action',
                                key: 'action',
                                render: (text: any, record: any) => {
                                    return (
                                        <Button type="link" onClick={() => this.toDetail(record.date)}>
                                            明细
                                        </Button>
                                    );
                                }
                            },
                        ]}
                        dataSource={datas_list}
                        rowKey="id"
                        pagination={false}
                        className="mt20"
                    />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：财务总览控制器
 * @description 财务总览
 * @author
 */
@addon('FinancialOverviewView', '财务总览', '财务总览')
@reactControl(Form.create<any>()(FinancialOverviewView), true)
export class FinancialOverviewViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    // 查询接口
    public request_url?: string;
}