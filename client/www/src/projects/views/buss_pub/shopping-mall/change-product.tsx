import { AppServiceUtility } from "src/projects/app/appService";
import { beforeUpload, beforeUpload2, edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import Form, { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { remote } from "src/projects/remote";
import "./index.less";
import { Row, Col, Button, message, Select } from "antd";
import { NTBraftEditor } from "src/business/components/buss-components/rich-text-editor";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;
/**
 * 状态：商品信息编辑页面
 */
export interface ChangeProductViewState extends ReactViewState {
    step?: number;
    express_fee?: string;
    postage_type?: any[];
    service_type_list?: any[];
    delivery_list?: any[];
    step1_data?: any;
    content?: string;
    is_send: boolean;
}

/**
 * 组件：商品信息编辑页面视图
 */
export class ChangeProductView extends ReactView<ChangeProductViewControl, ChangeProductViewState> {
    constructor(props: ChangeProductViewControl) {
        super(props);
        this.state = {
            step: 1,
            express_fee: '',
            postage_type: [],
            service_type_list: [],
            delivery_list: [],
            step1_data: {},
            content: '',
            is_send: false,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.shopping_mall_service.get_goods_list!({ id: this.props.match!.params.key }))
                .then((datas: any) => {
                    if (datas && datas.result && datas.result.length) {
                        this.setState({
                            step1_data: datas.result[0],
                            content: datas.result[0].content
                        });
                    }
                })
                .catch((error: any) => {
                    message.error(error);
                });
        }
        request(this, AppServiceUtility.shopping_mall_service.get_service_type_list!({})!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        service_type_list: datas.result
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        // 获取审批字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_codes: ['DELIVERY_TYPE'], in_select: true })!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        delivery_list: datas.result,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    preview = () => {
        return (
            <Row>
                <Row>
                    <Col span={12} className="preview-col">
                        <Row className="preview-box">
                            <Row type="flex" justify="center" align="middle" className="preview-title">商品详情效果预览</Row>
                            <div className="preview-html" dangerouslySetInnerHTML={{ __html: this.state.content || '' }} />
                        </Row>
                    </Col>
                    <Col span={12}>
                        <NTBraftEditor value={this.state.content} onChange={(e) => this.onChange(e, 'content')} remoteUrl={remote.upload_url} />
                    </Col>
                </Row>
                <Row type="flex" justify="center" className="ctrl-btns">
                    <Button type='ghost' name='返回' htmlType='button' onClick={() => { this.setState({ step: 1 }); }}>返回</Button>
                    <Button onClick={this.handleSubmit2} type='primary'>保存并提交审核</Button>
                </Row>
            </Row>
        );
    }
    onChange = (e: any, type: string) => {
        if (type === 'express_fee') {
            this.setState({
                express_fee: e.target.value,
            });
        } else if (type === 'postage_type') {
            this.setState({
                postage_type: e,
            });
        } else if (type === 'content') {
            this.setState({
                content: e,
            });
        }
    }
    handleSubmit1 = (err: Error, values: any) => {
        this.setState({
            step: 2,
            step1_data: values,
        });
    }
    handleSubmit2 = () => {
        let { content, step1_data, is_send } = this.state;
        if (content === '') {
            message.error('商品详情不可为空！');
            return;
        }
        if (is_send) {
            return;
        }
        const params: any = {
            content,
            ...step1_data
        };
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.shopping_mall_service.update_goods!(params))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                this.props.history!.push(ROUTE_PATH.products);
                            });
                        } else {
                            this.setState({
                                is_send: false,
                            });
                            message.error(datas);
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        console.error(error);
                    });
            }
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { step1_data, service_type_list, postage_type, express_fee, step, delivery_list } = this.state;
        console.log(step);
        let delivery_type_list: any = [];
        delivery_list!.map((item: any, index: number) => {
            delivery_type_list.push({
                id: item.code,
                name: item.name
            });
        });
        let edit_props = {
            form_items_props: [{
                title: '基本信息',
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "商品名称",
                        decorator_id: 'title',
                        field_decorator_option: {
                            initialValue: step1_data['title'],
                            rules: [{
                                required: true,
                                message: '请输入商品名称',
                            }, {
                                max: 200,
                                message: '商品名称不能超过200字符',
                            }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入商品名称",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.select,
                        label: "商品类型",
                        decorator_id: 'product_category_id',
                        field_decorator_option: {
                            initialValue: step1_data['product_category_id'],
                            rules: [{ required: true, message: "请选择商品类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择商品类型",
                            childrens: service_type_list!.map((item: any) => {
                                return (
                                    <Option key={item.id}>{item.name}</Option>
                                );
                            }),
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.upload,
                        label: "商品图片",
                        decorator_id: "image_urls",
                        field_decorator_option: {
                            initialValue: step1_data['image_urls'],
                            rules: [{ required: true, message: "请上传商品图片" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请上传商品图片",
                            autoComplete: 'off',
                            action: remote.upload_url,
                            beforeUpload: beforeUpload,
                        }
                    },
                    {
                        type: InputType.upload,
                        label: "视频封面图片",
                        decorator_id: "video_cover_image_url",
                        field_decorator_option: {
                            initialValue: step1_data['video_cover_image_url'],
                            rules: [{ required: false, message: "请上传视频封面图片" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请上传视频封面图片",
                            autoComplete: 'off',
                            action: remote.upload_url,
                            beforeUpload: beforeUpload,
                            tips: (
                                <Row>图片最多上传15张，图片建议尺寸 800像素800像素，仅支持 .jpg .jpeg .png .gif 格式，大小不超过 10M</Row>
                            ),
                        }
                    },
                    {
                        type: InputType.upload,
                        label: "商品视频（选填）",
                        decorator_id: "video_url",
                        field_decorator_option: {
                            initialValue: step1_data['video_url'],
                            rules: [{ required: false, message: "请上传商品视频" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请上传商品视频",
                            autoComplete: 'off',
                            fileType: 'video',
                            action: remote.upload_url,
                            beforeUpload: (e: any) => beforeUpload2(e, ['mp4', 'ogg']),
                            tips: (
                                <Row>视频建议宽高比 16:9，时长 9-30 秒，仅支持 .mp4 格式，大小不超过 2M</Row>
                            ),
                        }
                    },
                    // {
                    //     type: InputType.antd_input_number,
                    //     label: "排序",
                    //     decorator_id: 'sort',
                    //     field_decorator_option: {
                    //         initialValue: step1_data['sort'] || 50,
                    //         rules: [{ required: true, message: "请输入排序" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请输入排序",
                    //         autoComplete: 'off',
                    //     },
                    // },
                ]
            }, {
                title: `价格库存`,
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input_number,
                        label: "价格",
                        decorator_id: 'price',
                        field_decorator_option: {
                            initialValue: step1_data['price'],
                            rules: [{ required: true, message: "请输入价格" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            initialValue: 0,
                            placeholder: "请输入价格",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "库存",
                        decorator_id: 'stock_num',
                        field_decorator_option: {
                            initialValue: step1_data['stock_num'] || 0,
                            rules: [{ required: true, message: "请输入库存" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入库存",
                            autoComplete: 'off',
                        },
                    },
                ]
            }, {
                title: '物流信息',
                need_card: true,
                input_props: [
                    {
                        type: InputType.checkbox_group,
                        label: "配送方式",
                        decorator_id: 'postage_type',
                        field_decorator_option: {
                            initialValue: step1_data['postage_type'] || [],
                            rules: [{ required: true, message: "请选择配送方式" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择配送方式",
                            list: delivery_type_list,
                            onChange: (e: any) => {
                                this.onChange(e, 'postage_type');
                            }
                        },
                    },
                    {
                        ...((postage_type && postage_type.includes('DELIVERY_TYPE_EXPRESS')) || (step1_data['postage_type'] && step1_data['postage_type'].includes('DELIVERY_TYPE_EXPRESS')) ? {
                            type: InputType.radioGroup,
                            label: "快递费用",
                            decorator_id: 'express_fee',
                            field_decorator_option: {
                                initialValue: step1_data['express_fee'],
                                rules: [{ required: true, message: "请选择快递费用" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择快递费用",
                                options: [{
                                    label: '包邮',
                                    value: '包邮',
                                }, {
                                    label: '买家付费',
                                    value: '买家付费',
                                }],
                                onChange: (e: any) => {
                                    this.onChange(e, 'express_fee');
                                }
                            },
                        } : {})
                    },
                    {
                        ...(((postage_type && postage_type.includes('DELIVERY_TYPE_EXPRESS')) || (step1_data['postage_type'] && step1_data['postage_type'].includes('DELIVERY_TYPE_EXPRESS'))) && (express_fee === '买家付费' || step1_data['express_fee'] === '买家付费') ? {
                            type: InputType.antd_input_number,
                            label: "请输入费用",
                            decorator_id: 'postage_price',
                            field_decorator_option: {
                                initialValue: step1_data['postage_price'] || 0,
                                rules: [{ required: true, message: "请输入费用" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入费用",
                            },
                        } : {})
                    }
                ]
            }, {
                title: '其他信息',
                need_card: true,
                input_props: [
                    {
                        type: InputType.checkbox_group,
                        label: "上架时间",
                        decorator_id: 'active_date',
                        field_decorator_option: {
                            initialValue: step1_data['active_date'] || 0,
                            rules: [{ required: false, message: "请选择上架时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择上架时间",
                            list: [{
                                id: '保存后立即上架',
                                name: '保存后立即上架'
                            }]
                        },
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    history.back();
                }
            }],
            next_btn_propps: {
                text: "下一步",
                cb: this.handleSubmit1,
                btn_other_props: {
                    type: 'primary'
                }
            },
            service_option: {
                operation_option: {}
            },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <Row className="change-product-form-creator">
                    {step === 1 ? <FormCreator {...edit_props_list} /> : this.preview()}
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：商品信息编辑页面控件
 * @description 商品信息编辑页面控件
 * @author
 */
@addon('ChangeProductViewControl', '商品信息编辑页面控件', '商品信息编辑页面控件')
@reactControl(Form.create<any>()(ChangeProductView), true)
export class ChangeProductViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}