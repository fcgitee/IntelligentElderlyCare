import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Button } from 'antd';
import { table_param } from "src/projects/app/util-tool";
import { toThousands } from "src/business/util_tool";
/** 状态：入账记录 */
export interface EntryRecordViewState extends ReactViewState {
}

/** 组件：入账记录 */
export class EntryRecordView extends React.Component<EntryRecordViewControl, EntryRecordViewState> {
    private columns_data_source = [
        {
            title: '打款时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
        {
            title: '金额',
            dataIndex: 'in_amount',
            key: 'in_amount',
            render: (text: any, record: any) => {
                return (
                    <Row>
                        ￥{toThousands(text)}
                    </Row>
                );
            }
        }, {
            title: '到账银行',
            dataIndex: 'bank_name',
            key: 'bank_name',
        }, {
            title: '银行处理状态',
            dataIndex: 'status',
            key: 'status',
        }, {
            title: '到账时间',
            dataIndex: 'payment_date',
            key: 'payment_date',
            render: (text: any, record: any) => {
                return text ? text : "--";
            }
        },
    ];
    private edit_form_items_props: any = [];
    constructor(props: EntryRecordViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount = () => {
        if (this.props.select_permission === '平台对账查询') {
            this.columns_data_source.unshift({
                title: '服务商',
                dataIndex: 'service_provider_name',
                key: 'service_provider_name',
            });
            this.edit_form_items_props.push({
                type: InputType.input,
                label: '服务商名称',
                decorator_id: "service_provider_name",
                option: {
                    placeholder: "请输入要搜索的服务商名称",
                    autoComplete: 'off',
                }
            });
        }
    }
    save = () => {
        console.log('保存');
    }
    back = () => {
        history.back();
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let entry = {
            type_show: false,
            edit_form_items_props: this.edit_form_items_props,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
        };
        let entry_list = Object.assign(entry, table_param);
        return (
            <Row>
                <SignFrameLayout {...entry_list} />
                <Row type="flex" justify="center" className="ctrl-btns sc-btns">
                    <Button name='返回' onClick={this.back}>返回</Button>
                </Row>
            </Row>
        );
    }
}

/**
 * 控件：入账记录控制器
 * @description 入账记录
 * @author
 */
@addon('EntryRecordView', '入账记录', '入账记录')
@reactControl(Form.create<any>()(EntryRecordView), true)
export class EntryRecordViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
}