import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Select, Button } from 'antd';
// import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { toThousands } from "src/business/util_tool";
import "./index.less";
const Option = Select.Option;
/** 状态：资金明细 */
export interface CapitalDetailViewState extends ReactViewState {
}

/** 组件：资金明细 */
export class CapitalDetailView extends React.Component<CapitalDetailViewControl, CapitalDetailViewState> {
    private columns_data_source = [
        {
            title: '检验结果',
            dataIndex: 'status',
            key: 'status',
            render: (text: any, record: any) => {
                if (text === '通过') {
                    return (
                        <span className="font-green">√通过</span>
                    );
                } else {
                    return (
                        <span className="font-red">！{text}</span>
                    );
                }
            }
        },
        {
            title: '服务商',
            dataIndex: 'service_provider_name',
            key: 'service_provider_name',
        }, {
            title: '已结算',
            dataIndex: 'settled',
            key: 'settled',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '待结算',
            dataIndex: 'to_be_settled',
            key: 'to_be_settled',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '入账',
            dataIndex: 'entry',
            key: 'entry',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '出账',
            dataIndex: 'out_of_account',
            key: 'out_of_account',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
        {
            title: '盈余',
            dataIndex: 'surplus',
            key: 'surplus',
            render: (text: any, record: any) => {
                return (
                    <span>￥{toThousands(text)}</span>
                );
            }
        },
    ];
    constructor(props: CapitalDetailViewControl) {
        super(props);
        this.state = {
            // status_list: [],
        };
    }
    componentDidMount() {
        // 获取审批字典
        // request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_code: 'PRODUCT_STATUS' })!)
        //     .then((datas: any) => {
        //         if (datas && datas.result) {
        //             this.setState({
        //                 status_list: datas.result,
        //             });
        //         }
        //     }).catch((error: Error) => {
        //         console.error(error);
        //     });
    }
    back = () => {
        history.back();
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let capitals = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '服务商名称',
                    decorator_id: "service_provider_name",
                    option: {
                        placeholder: "请输入要搜索的服务商名称",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.select,
                    label: '检验结果',
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择检验结果",
                        childrens: ['通过', '有误'].map((item: any) => <Option key={item} >{item}</Option>),
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: 'get_capital_detail_list_all',
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
        };
        let capitals_list = Object.assign(capitals, table_param);
        return (
            <Row>
                <SignFrameLayout {...capitals_list} />
                <Row type="flex" justify="center" className="ctrl-btns sc-btns">
                    <Button name='返回' onClick={this.back}>返回</Button>
                </Row>
            </Row>
        );
    }
}

/**
 * 控件：资金明细控制器
 * @description 资金明细
 * @author
 */
@addon('CapitalDetailView', '资金明细', '资金明细')
@reactControl(Form.create<any>()(CapitalDetailView), true)
export class CapitalDetailViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}