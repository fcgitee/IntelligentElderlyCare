
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Button, Form, Icon, message, Modal, Row, Select } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { checkPermission, request } from "src/business/util_tool";
import "./index.less";
const Option = Select.Option;
/** 状态：商品列表 */
export interface ProductsViewState extends ReactViewState {
    status_list?: any;
    service_type_list?: any;
    // 选择的行
    selected_items?: any[];
    edit_permission?: boolean;
    is_send: boolean;
}

/** 组件：商品列表 */
export class ProductsView extends React.Component<ProductsViewControl, ProductsViewState> {
    private formCreator: any = null;
    private columns_data_source: any = [
        {
            title: '商品',
            dataIndex: 'title',
            key: 'title',
            render: (text: any, record: any) => {
                return (
                    <Row type="flex" justify="start" align="middle" style={{ flexFlow: 'unset', width: '100%' }}>
                        {record.image_urls && record.image_urls[0] ? <Row className="product-picture">
                            {(() => {
                                if (record.status === 'PENDING_APPROVAL') {
                                    return (
                                        <Row type="flex" justify="center" align="middle" className="hover-status">审核中</Row>
                                    );
                                } else if (record.status === 'APPROVAL_FAILED') {
                                    return (
                                        <Row type="flex" justify="center" align="middle" className="hover-status" style={{ cursor: 'pointer' }} onClick={() => this.ctrl('reason', record)}>不通过</Row>
                                    );
                                } else if (record.active_status === '下架') {
                                    return (
                                        <Row type="flex" justify="center" align="middle" className="hover-status">已下架</Row>
                                    );
                                }
                                return null;
                            })()}
                            <img src={record.image_urls[0]} />
                        </Row> : null}
                        <Row className="maxLine3 abutton" style={{ WebkitBoxOrient: 'vertical' }} onClick={() => this.ctrl('preview', record)}>{text}</Row>
                    </Row>
                );
            }
        },
        {
            title: '商品类型',
            dataIndex: 'category_name',
            key: 'category_name',
        },
        {
            title: '价格（元）',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: '库存',
            dataIndex: 'stock_num',
            key: 'stock_num',
        },
        {
            title: '销量',
            dataIndex: 'sales_num',
            key: 'sales_num',
            render: (text: any, record: any) => {
                return (
                    <div>
                        <div className="tbfixed">
                            <Row>创建时间：{record.create_date}</Row>
                            <Row>佣金：{record.commission_ratio || 0}%
                            {/* &nbsp;&nbsp;&nbsp;&nbsp;慈善基金：{record.charitable_commission_ratio || 0}%&nbsp;&nbsp;&nbsp;&nbsp;交易费率：{record.transaction_rate || 0}% */}
                            </Row>
                        </div>
                        {text || 0}
                    </div>
                );
            }
        },
        {
            title: '上架状态',
            dataIndex: 'active_status',
            key: 'active_status',
            render: (text: any, record: any) => {
                return (
                    record.status === 'PASSED' ? text : '下架'
                );
            }
        },
    ];
    private btn_props: any = [];
    constructor(props: ProductsViewControl) {
        super(props);
        this.state = {
            status_list: [],
            service_type_list: [],
            selected_items: [],
            edit_permission: false,
            is_send: false,
        };
    }
    ctrl = (type: string, record?: any) => {
        const { is_send, selected_items } = this.state;
        switch (type) {
            // 新增
            case 'add':
                this.props.history!.push(ROUTE_PATH.changeProduct);
                break;
            // 编辑
            case 'edit':
                this.props.history!.push(ROUTE_PATH.changeProduct + '/' + record.id);
                break;
            // 预览
            case 'preview':
                window.open(ROUTE_PATH.productPreview + '/' + record.id);
                break;
            // 显示不通过原因
            case 'reason':
                Modal.info({
                    title: '不通过原因',
                    content: (
                        <div>
                            <p>{record.reason}</p>
                        </div>
                    ),
                    onOk() { },
                });
                break;
            // 复制
            case 'copy':
                Modal.confirm({
                    title: '复制',
                    content: '您确定要复制吗？',
                    okText: '确定',
                    cancelText: '关闭',
                    icon: (<Icon type="info-circle" />),
                    onOk: () => {
                        if (is_send || !record.id) {
                            return;
                        }
                        this.setState(
                            {
                                is_send: true,
                            },
                            () => {
                                request(this, AppServiceUtility.shopping_mall_service.copy_goods!({ id: record.id })!)
                                    .then((data: any) => {
                                        if (data && data === 'Success') {
                                            message.success('复制成功！', 1, () => {
                                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                            });
                                        } else {
                                            message.error(`复制失败，${data}！`);
                                        }
                                    }).catch((error: Error) => {
                                        console.error(error);
                                    }).finally(() => {
                                        this.setState({
                                            is_send: false,
                                        });
                                    });
                            }
                        );
                    },
                    onCancel: () => {
                        console.log('退款取消');
                    },
                });
                break;
            case 'on':
                if (is_send) {
                    return;
                }
                if (selected_items!.length === 0) {
                    message.error('请选择要上架的商品！');
                    return;
                }
                let on_param: any = [];
                for (let i = 0; i < selected_items!.length; i++) {
                    if (selected_items![i]['status'] !== 'PASSED') {
                        message.error(`商品：${selected_items![i]['title']}未审核通过，不允许操作！`);
                        return;
                    }
                    if (selected_items![i]['active_status'] === '上架') {
                        message.error(`商品：${selected_items![i]['title']}已经是上架状态，无需重复操作！`);
                        return;
                    }
                    on_param.push({
                        id: selected_items![i]['id'],
                        active_status: '上架',
                    });
                }
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.shopping_mall_service.change_goods!(on_param)!)
                            .then((data: any) => {
                                if (data && data === 'Success') {
                                    message.success('上架成功！', 1, () => {
                                        this.setState({
                                            selected_items: [],
                                        });
                                        this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                        this.formCreator && this.formCreator.clearTableSelectedRow && this.formCreator.clearTableSelectedRow();
                                    });
                                } else {
                                    message.error(`上架失败，${data}！`);
                                }
                            }).catch((error: Error) => {
                                console.error(error);
                            }).finally(() => {
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
                break;
            case 'off':
                if (is_send) {
                    return;
                }
                if (selected_items!.length === 0) {
                    message.error('请选择要下架的商品！');
                    return;
                }
                let off_param: any = [];
                for (let i = 0; i < selected_items!.length; i++) {
                    if (selected_items![i]['status'] !== 'PASSED') {
                        message.error(`商品：${selected_items![i]['title']}未审核通过，不允许操作！`);
                        return;
                    }
                    if (selected_items![i]['active_status'] === '下架') {
                        message.error(`商品：${selected_items![i]['title']}已经是下架状态，无需重复操作！`);
                        return;
                    }
                    off_param.push({
                        id: selected_items![i]['id'],
                        active_status: '下架',
                    });
                }
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.shopping_mall_service.change_goods!(off_param)!)
                            .then((data: any) => {
                                if (data && data === 'Success') {
                                    message.success('下架成功！', 1, () => {
                                        this.setState({
                                            selected_items: [],
                                        });
                                        this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                        this.formCreator && this.formCreator.clearTableSelectedRow && this.formCreator.clearTableSelectedRow();
                                    });
                                } else {
                                    message.error(`下架失败，${data}！`);
                                }
                            }).catch((error: Error) => {
                                console.error(error);
                            }).finally(() => {
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
                break;
            case 'delete':
                Modal.confirm({
                    title: '删除',
                    content: '您确定要删除吗？删除后将不可恢复！',
                    okText: '确定',
                    cancelText: '关闭',
                    icon: (<Icon type="info-circle" />),
                    onOk: () => {
                        if (is_send) {
                            return;
                        }
                        let delete_param: any = [];
                        for (let i = 0; i < selected_items!.length; i++) {
                            delete_param.push(selected_items![i]['id']);
                        }
                        this.setState(
                            {
                                is_send: true,
                            },
                            () => {
                                request(this, AppServiceUtility.shopping_mall_service.delete_goods!({ ids: delete_param })!)
                                    .then((data: any) => {
                                        if (data && data === 'Success') {
                                            message.success('删除成功！', 1, () => {
                                                this.setState({
                                                    selected_items: [],
                                                });
                                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash(1);
                                                this.formCreator && this.formCreator.clearTableSelectedRow && this.formCreator.clearTableSelectedRow();
                                            });
                                        } else {
                                            message.error(`删除失败，${data}！`);
                                        }
                                    }).catch((error: Error) => {
                                        console.error(error);
                                    }).finally(() => {
                                        this.setState({
                                            is_send: false,
                                        });
                                    });
                            }
                        );
                    },
                    onCancel: () => {
                        console.log('退款取消');
                    },
                });
                break;
            default:
                break;
        }
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        this.setState({
            selected_items: selectedRows
        });
    }

    componentDidMount() {
        // 获取审批字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_code: 'PRODUCT_STATUS' })!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        status_list: datas.result,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        // 获取商品类型
        request(this, AppServiceUtility.shopping_mall_service.get_service_type_list!({})!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        service_type_list: datas.result
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        let edit_permission = checkPermission(this.props.edit_permission!);
        let add_permission = checkPermission(this.props.add_permission!);
        // 固定按钮
        this.btn_props.push({
            label: '发布商品',
            btn_method: () => this.ctrl('add'),
            icon: 'plus'
        });
        // 编辑权限
        if (edit_permission || add_permission) {
            // 编辑权限加编辑操作
            this.columns_data_source.push(
                {
                    title: '操作',
                    dataIndex: 'action',
                    key: 'action',
                    render: (text: any, record: any) => {
                        return (
                            <Row className="shangcheng-table-action" type="flex" align="middle" justify="end">
                                {edit_permission ? <Button type="link" size="small" onClick={() => this.ctrl('edit', record)}>编辑</Button> : null}
                                {add_permission ? <Button type="link" size="small" onClick={() => this.ctrl('copy', record)}>复制</Button> : null}
                            </Row>
                        );
                    }
                }
            );
        }
        if (edit_permission) {
            this.setState({
                edit_permission
            });
            // 编辑权限加编辑操作
            this.btn_props.push({
                label: '上架',
                btn_method: () => this.ctrl('on'),
            });
            this.btn_props.push({
                label: '下架',
                btn_method: () => this.ctrl('off'),
            });
            this.btn_props.push({
                label: '删除',
                btn_method: () => this.ctrl('delete'),
            });
        }
    }
    validator = (rule: any, val: any, callback: Function) => {
        if (val) {
            for (let item in val) {
                if (item) {
                    if (!/^\d+(\.\d{1,2})?$/.test(val[item])) {
                        callback('必须输入数字类型，最多两位小数！');
                    }
                }
            }
        }
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let products = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '商品名称',
                    decorator_id: "title",
                    option: {
                        placeholder: "请输入要搜索的商品名称",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.select,
                    label: '商品类型',
                    decorator_id: "product_category_id",
                    option: {
                        placeholder: "请选择要搜索的商品类型",
                        autoComplete: 'off',
                        childrens: this.state.service_type_list.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>),
                    }
                },
                {
                    type: InputType.select,
                    label: '审核状态',
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择审核状态",
                        childrens: this.state.status_list.map((item: any) => <Option key={item.code} value={item.code}>{item.name}</Option>),
                    }
                },
                {
                    type: InputType.rangeInput,
                    label: '价格',
                    decorator_id: "price",
                    option: {
                        before_option: {
                            addonAfter: "元",
                        },
                        after_option: {
                            addonAfter: "元",
                        },
                    }
                },
                {
                    type: InputType.rangeInput,
                    label: '销量',
                    decorator_id: "sales_num",
                },
                {
                    type: InputType.select,
                    label: '上架状态',
                    decorator_id: "active_status",
                    option: {
                        placeholder: "请选择上架状态",
                        childrens: [{
                            key: '',
                            label: '全部'
                        }, {
                            key: '上架',
                            label: '上架'
                        }, {
                            key: '下架',
                            label: '下架'
                        }].map((item: any) => <Option key={item.key} value={item.key}>{item.label}</Option>),
                    }
                },
            ],
            btn_props: this.btn_props,
            columns_data_source: this.columns_data_source,
            on_row_selection: this.on_row_selection,
            onRef: this.onRef,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: 'get_goods_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_goods'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            // edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            // delete_permission: this.props.delete_permission,
        };
        let products_list = Object.assign(products, table_param);
        return (
            <Row className="shangcheng-table products-list-table">
                <SignFrameLayout {...products_list} />
            </Row>
        );
    }
}

/**
 * 控件：商品列表控制器
 * @description 商品列表
 * @author
 */
@addon('ProductsView', '商品列表', '商品列表')
@reactControl(Form.create<any>()(ProductsView), true)
export class ProductsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}