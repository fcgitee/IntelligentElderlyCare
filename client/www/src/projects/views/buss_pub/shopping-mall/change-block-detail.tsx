import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info, beforeUpload } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";
import { Select, Row } from 'antd';
import { remote } from "src/projects/remote";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
let { Option } = Select;
/**
 * 状态：编辑板块页面
 */
export interface ChangeBlockDetailsViewState extends ReactViewState {
    parent_data?: any;
    service_type_list?: any;
    select_type: string;
}

/**
 * 组件：编辑板块页面视图
 */
export class ChangeBlockDetailsView extends ReactView<ChangeBlockDetailsViewControl, ChangeBlockDetailsViewState> {
    private formCreator: any = null;
    constructor(props: ChangeBlockDetailsViewControl) {
        super(props);
        this.state = {
            parent_data: {},
            service_type_list: [],
            select_type: '',
        };
    }
    componentDidMount() {
        // 获取父级数据
        if (this.props.match!.params.code) {
            request(this, AppServiceUtility.shopping_mall_service.get_blocks_list_all!({ id: this.props.match!.params.code })!)
                .then((datas: any) => {
                    if (datas && datas.result && datas.result[0]) {
                        this.setState({
                            parent_data: datas.result[0],
                        });
                        if (datas.result[0]['type'] === 'PRODUCT_TYPE') {
                            request(this, AppServiceUtility.shopping_mall_service.get_service_type_list!({})!)
                                .then((datas: any) => {
                                    if (datas && datas.result) {
                                        this.setState({
                                            service_type_list: datas.result
                                        });
                                    }
                                }).catch((error: Error) => {
                                    console.error(error);
                                });
                        }
                    }
                }).catch((error: Error) => {
                    console.error(error);
                });
        }
    }
    changeProduct = (e: any) => {
        const { select_type } = this.state;
        if (select_type === '商品') {
            this.formCreator.setChildFieldsValue({
                'image_url': (e['image_urls'] && e['image_urls'][0]) ? [e['image_urls'][0]] : [],
                'title': e['title'] || '',
            });
        } else if (select_type === '居家服务') {
            this.formCreator.setChildFieldsValue({
                'image_url': (e['picture_collection'] && e['picture_collection'][0]) ? [e['picture_collection'][0]] : [],
                'title': e['name'] || '',
            });
        }
    }

    changeSelectType = (e: any) => {
        this.setState(
            {
                select_type: e.target.value,
            },
            () => {
                // 需要用jquery清空残留的dom显示
                $('label[for=product_id]').parent().parent().find('.ant-select-selection-selected-value').text('');
                this.formCreator.setChildFieldsValue({
                    'product_id': '',
                    'image_url': [],
                    'title': '',
                });
            }
        );
    }

    changeServiceType = (e: any) => {
        let { service_type_list } = this.state;
        for (let i = 0; i <= service_type_list.length; i++) {
            if (e === service_type_list[i]['id']) {
                this.formCreator.setChildFieldsValue({
                    'image_url': service_type_list[i]['logo_image'] || [],
                    'title': service_type_list[i]['name'] || '',
                });
                break;
            }
        }
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    // 获取数据完成后的回调数据
    reach_func = (data: any) => {
        if (data && data.result && data.result[0] && data.result[0]['type']) {
            this.setState({
                select_type: data.result[0]['type']
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { parent_data, service_type_list, select_type } = this.state;
        let title: string = '';
        if (this.props.match!.params.key) {
            if (this.props.match!.params.key === 'parent') {
                title = '添加推荐';
            } else {
                title = '编辑推荐';
            }
        }
        let product_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "商品名称",
                    decorator_id: "title"
                },
            ],
            columns_data_source: [{
                title: '商品名称',
                dataIndex: 'title',
                key: 'title',
            }],
            service_name: AppServiceUtility.shopping_mall_service,
            service_func: 'get_goods_list_all',
            title: '商品查询',
            name_field: 'title',
            service_option: [{
                status: 'PASSED',
                active_status: '上架',
            }],
            select_option: {
                placeholder: "请选择商品",
            }
        };
        let service_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "居家服务名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: [{
                title: '居家服务名称',
                dataIndex: 'name',
                key: 'name',
            }],
            service_name: AppServiceUtility.shopping_mall_service,
            service_func: 'get_service_product_list',
            title: '居家服务查询',
            name_field: 'name',
            service_option: [{
                'state': '启用',
                'status': '通过',
            }],
            select_option: {
                placeholder: "请选择居家服务",
            }
        };
        let edit_props = {
            form_items_props: [{
                title: title,
                need_card: true,
                input_props: [
                    ...(parent_data && parent_data.type === 'PRODUCT' ? [{
                        type: InputType.radioGroup,
                        label: "商品",
                        decorator_id: 'type',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择商品" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            options: [{
                                label: '商品',
                                value: '商品',
                            }, {
                                label: '居家服务',
                                value: '居家服务',
                            }],
                            autoComplete: 'off',
                            onChange: this.changeSelectType
                        },
                    },
                    {
                        type: InputType.modal_search,
                        label: "选择商品",
                        decorator_id: 'product_id',
                        field_decorator_option: {
                            rules: [{ message: "请选择商品", required: true }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择商品",
                            modal_search_items_props: select_type === '居家服务' ? service_props : product_props,
                            onGetAll: this.changeProduct
                        },
                    },
                    {
                        type: InputType.antd_input,
                        label: "推荐标题",
                        decorator_id: 'title',
                        field_decorator_option: {
                            rules: [
                                { required: true, message: "请输入推荐标题" },
                                { max: 10, message: '推荐标题不能超过10字符', }
                            ],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入推荐标题",
                            autoComplete: 'off',
                        },
                    }, {
                        type: InputType.upload,
                        label: "宣传图",
                        decorator_id: 'image_url',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请上传宣传图" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请上传宣传图",
                            autoComplete: 'off',
                            action: remote.upload_url,
                            beforeUpload: beforeUpload,
                            tips: (
                                <Row>可上传图片，图片建议尺寸 1360像素680像素，仅支持 .jpg .jpeg .png格式，大小不超过 10M</Row>
                            ),
                        },
                    }] : parent_data && parent_data.type === 'PRODUCT_TYPE' ? [{
                        type: InputType.select,
                        label: "商品类型",
                        decorator_id: 'product_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择商品类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择商品类型",
                            childrens: service_type_list.map((item: any) => {
                                return (
                                    <Option key={item.id}>{item.name}</Option>
                                );
                            }),
                            autoComplete: 'off',
                            onChange: this.changeServiceType
                        },
                    }, {
                        type: InputType.antd_input,
                        label: "显示名称",
                        decorator_id: 'title',
                        field_decorator_option: {
                            rules: [
                                { required: true, message: "请输入显示名称" },
                                { max: 10, message: '显示名称不能超过10字符', }
                            ],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入显示名称",
                            autoComplete: 'off',
                        },
                    }, {
                        type: InputType.upload,
                        label: "分类图标",
                        decorator_id: 'image_url',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请上传分类图标" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请上传分类图标",
                            autoComplete: 'off',
                            action: remote.upload_url,
                            beforeUpload: beforeUpload,
                            tips: (
                                <Row>图片建议比例为1:1仅支持 .jpg .jpeg .png格式，大小不超过 100KB</Row>
                            ),
                        },
                    }] : []),
                    {
                        type: InputType.radioGroup,
                        label: "是否启用",
                        decorator_id: 'status',
                        field_decorator_option: {
                            initialValue: true,
                            rules: [{ required: true, message: "请选择是否启用" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            options: [{
                                label: '是',
                                value: true,
                            }, {
                                label: '否',
                                value: false,
                            }],
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "序号",
                        decorator_id: "sort",
                        field_decorator_option: {
                            initialValue: 0,
                            rules: [{ required: true, message: "请输入序号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "值越大，越靠前",
                            autoComplete: 'off',
                            min: 0,
                            max: 999,
                            step: 1,
                            tips: (
                                <Row>值越大，越靠前</Row>
                            ),
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.shopping_mall_service,
                operation_option: {
                    query: {
                        func_name: 'get_recommend_list_all',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: 'update_recommend'
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
            onRef: this.onRef,
            extra_submit_data: [
                {
                    key: 'recommend_module_id',
                    value: this.props.match!.params.code,
                }
            ],
            reach_func: this.reach_func,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑板块页面控件
 * @description 编辑板块页面控件
 * @author
 */
@addon('ChangeBlockDetailsViewControl', '编辑板块页面控件', '编辑板块页面控件')
@reactControl(ChangeBlockDetailsView, true)
export class ChangeBlockDetailsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}