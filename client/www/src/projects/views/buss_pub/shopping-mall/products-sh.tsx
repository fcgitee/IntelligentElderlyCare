
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { Form, Row, Select, Button, Modal, Col, Input, Radio, Checkbox, InputNumber, message } from 'antd';
// import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { request, checkPermission } from "src/business/util_tool";
import "./index.less";
import TextArea from "antd/lib/input/TextArea";
const Option = Select.Option;
/** 状态：商品审核 */
export interface ProductsShViewState extends ReactViewState {
    status_list?: any;
    service_type_list?: any;
    edit_permission?: boolean;
    model_visible: boolean;
    sh_data?: any;
    is_send: boolean;
    validate: any;
}

/** 组件：商品审核 */
export class ProductsShView extends React.Component<ProductsShViewControl, ProductsShViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '商品',
            dataIndex: 'title',
            key: 'title',
            render: (text: any, record: any) => {
                return (
                    <Row type="flex" justify="start" align="middle" style={{ flexFlow: 'unset', width: '100%' }}>
                        {record.image_urls && record.image_urls[0] ? <Row className="product-picture">
                            <img src={record.image_urls[0]} />
                        </Row> : null}
                        <Row className="maxLine3" style={{ WebkitBoxOrient: 'vertical' }}>{text}</Row>
                    </Row>
                );
            }
        },
        {
            title: '商品类型',
            dataIndex: 'category_name',
            key: 'category_name',
        }, {
            title: '价格（元）',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: '库存',
            dataIndex: 'stock_num',
            key: 'stock_num',
        },
        {
            title: '销量',
            dataIndex: 'sales_num',
            key: 'sales_num',
            render: (text: any, record: any) => {
                return (
                    <div>
                        <div className="tbfixed">
                            <Row>服务商名称：{record.organization_name}</Row>
                            <Row>
                                佣金：{record.commission_ratio || 0}%
                            {/* &nbsp;&nbsp;&nbsp;&nbsp;交易费率：{record.transaction_rate || 0}%&nbsp;&nbsp;&nbsp;&nbsp;慈善基金：{record.charitable_commission_ratio || 0}% */}
                            </Row>
                        </div>
                        {text || 0}
                    </div>
                );
            }
        },
        {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
        {
            title: '审核状态',
            dataIndex: 'status_name',
            key: 'status_name',
            render: (text: any, record: any) => {
                let color = '';
                if (text === '未审核') {
                    color = 'rgba(255, 0, 0, 1)';
                } else if (text === '审核通过') {
                    color = 'rgba(0, 164, 67, 1)';
                } else if (text === '审核未通过') {
                    color = 'rgba(231, 177, 0, 1)';
                }
                return (
                    <span style={{ color }}>
                        {text}
                    </span>
                );
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    this.state.edit_permission ? (
                        <Row className="shangcheng-table-action" type="flex" align="middle" justify="end">
                            <Button type="link" size="small" onClick={() => this.onIconClick('icon_edit', record)}>{record.status_name === '审核通过' ? '重新审核' : "审核"}</Button>
                        </Row>
                    ) : null
                );
            }
        },
    ];
    constructor(props: ProductsShViewControl) {
        super(props);
        this.state = {
            status_list: [],
            service_type_list: [],
            sh_data: [],
            edit_permission: false,
            model_visible: false,
            is_send: false,
            validate: false,
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState(
                {
                    sh_data: { ...contents },
                    validate: this.getDefaultValidate({}),
                },
                () => {
                    this.handleModelVisible(true);
                }
            );
            // this.props.history!.push(ROUTE_PATH.changeProductSh + '/' + contents.id);
        }
    }
    componentDidMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
        });
        // 获取审批字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_code: 'PRODUCT_STATUS' })!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        status_list: datas.result,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        // 获取商品类型
        request(this, AppServiceUtility.shopping_mall_service.get_service_type_list!({})!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        service_type_list: datas.result
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
        this.setState({
            validate: this.getDefaultValidate(),
        });
    }

    getDefaultValidate = (dvalue?: any) => {
        let validate = dvalue || this.state.validate || {};
        // ['commission_ratio', 'transaction_rate', 'charitable_commission_ratio', 'pay_type', 'reason']
        ['commission_ratio', 'pay_type', 'reason'].map((item: any) => {
            if (!validate[item]) {
                validate[item] = {
                    className: '',
                    textTips: '',
                };
            }
        });
        return validate;
    }

    handleOk = () => {
        const { is_send, sh_data } = this.state;
        if (is_send) {
            return;
        }
        let validate = this.getDefaultValidate({});
        let hasError = false;
        if (sh_data.commission_ratio === '') {
            hasError = true;
            validate['commission_ratio']['className'] = 'has-error';
            validate['commission_ratio']['textTips'] = '请输入佣金';
        } else {
            if (isNaN(sh_data.commission_ratio)) {
                hasError = true;
                validate['commission_ratio']['className'] = 'has-error';
                validate['commission_ratio']['textTips'] = '佣金必须为数字';
            } else if (sh_data.commission_ratio < 0.01 || sh_data.commission_ratio > 99.99) {
                hasError = true;
                validate['commission_ratio']['className'] = 'has-error';
                validate['commission_ratio']['textTips'] = '佣金取值范围为0.01-99.99';
            } else {
                validate['commission_ratio']['className'] = '';
                validate['commission_ratio']['textTips'] = '';
            }
        }
        // 过滤一下，防止01这种情况
        sh_data.commission_ratio = Number(sh_data.commission_ratio);
        // if (sh_data.transaction_rate === 0) {
        //     hasError = true;
        //     validate['transaction_rate']['className'] = 'has-error';
        //     validate['transaction_rate']['textTips'] = '请输入交易费率';
        // } else {
        //     if (isNaN(sh_data.transaction_rate)) {
        //         hasError = true;
        //         validate['transaction_rate']['className'] = 'has-error';
        //         validate['transaction_rate']['textTips'] = '交易费率必须为数字';
        //     } else if (sh_data.transaction_rate < 0 || sh_data.transaction_rate > 99) {
        //         hasError = true;
        //         validate['transaction_rate']['className'] = 'has-error';
        //         validate['transaction_rate']['textTips'] = '交易费率取值范围为0-99';
        //     } else {
        //         validate['transaction_rate']['className'] = '';
        //         validate['transaction_rate']['textTips'] = '';
        //     }
        // }
        // if (sh_data.charitable_commission_ratio === 0) {
        //     hasError = true;
        //     validate['charitable_commission_ratio']['className'] = 'has-error';
        //     validate['charitable_commission_ratio']['textTips'] = '请输入慈善基金比例';
        // } else {
        //     if (isNaN(sh_data.charitable_commission_ratio)) {
        //         hasError = true;
        //         validate['charitable_commission_ratio']['className'] = 'has-error';
        //         validate['charitable_commission_ratio']['textTips'] = '慈善基金比例必须为数字';
        //     } else if (sh_data.charitable_commission_ratio < 0 || sh_data.charitable_commission_ratio > 99) {
        //         hasError = true;
        //         validate['charitable_commission_ratio']['className'] = 'has-error';
        //         validate['charitable_commission_ratio']['textTips'] = '慈善基金比例取值范围为0-99';
        //     } else {
        //         validate['charitable_commission_ratio']['className'] = '';
        //         validate['charitable_commission_ratio']['textTips'] = '';
        //     }
        // }
        // 前面的都核准了，然后加起来不可以超过100
        // if (hasError === false && Number(sh_data.commission_ratio) + Number(sh_data.transaction_rate) + Number(sh_data.charitable_commission_ratio) > 100) {
        //     hasError = true;
        //     validate['commission_ratio']['className'] = 'has-error';
        //     validate['transaction_rate']['className'] = 'has-error';
        //     validate['charitable_commission_ratio']['className'] = 'has-error';
        //     validate['charitable_commission_ratio']['textTips'] = '（佣金+交易费率+慈善基金比例）不可超过100';
        // }
        if (!sh_data.pay_type || sh_data.pay_type.length === 0) {
            hasError = true;
            validate['pay_type']['className'] = 'has-error';
            validate['pay_type']['textTips'] = '请至少选择一项支付方式';
        }
        if (sh_data.status === 'APPROVAL_FAILED' && !sh_data.reason) {
            hasError = true;
            validate['reason']['className'] = 'has-error';
            validate['reason']['textTips'] = '不通过请输入原因';
        }
        this.setState({
            validate,
        });
        if (hasError) {
            return;
        }
        let param: any = {
            id: sh_data.id,
            status: sh_data.status,
            reason: sh_data.reason,
            pay_type: sh_data.pay_type,
            commission_ratio: sh_data.commission_ratio,
            transaction_rate: sh_data.transaction_rate || 0,
            charitable_commission_ratio: sh_data.charitable_commission_ratio || 0,
        };
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.shopping_mall_service.update_goods_sh!(param))
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('操作成功！', 1, () => {
                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                            });
                        } else {
                            message.error(`操作失败，${data}！`);
                        }
                    })
                    .catch(err => {
                        console.error(err);
                    })
                    .finally(() => {
                        this.setState({
                            is_send: false
                        });
                    });
            }
        );
        this.handleModelVisible(false);
    }

    handleModelVisible = (e: boolean) => {
        let param: any = {
            model_visible: e,
        };
        if (e === false) {
            param['sh_data'] = [];
        }
        this.setState(param);
    }

    setValue = (type: string, e: any) => {
        let sh_data = this.state.sh_data;
        switch (type) {
            case 'commission_ratio':
            // case 'transaction_rate':
            // case 'charitable_commission_ratio':
            case 'reason':
            case 'status':
                sh_data[type] = e.target.value;
                this.setState({
                    sh_data
                });
                break;
            case 'pay_type':
            case 'sort':
                sh_data[type] = e;
                this.setState({
                    sh_data
                });
                break;
            default: break;
        }
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let products = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '商品名称',
                    decorator_id: "title",
                    option: {
                        placeholder: "请输入要搜索的商品名称",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.select,
                    label: '商品类型',
                    decorator_id: "product_category_id",
                    option: {
                        placeholder: "请选择要搜索的商品类型",
                        autoComplete: 'off',
                        childrens: this.state.service_type_list.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>),
                    }
                },
                {
                    type: InputType.select,
                    label: '审核状态',
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择审核状态",
                        childrens: this.state.status_list.map((item: any) => <Option key={item.code} value={item.code}>{item.name}</Option>),
                    }
                },
                {
                    type: InputType.rangeInput,
                    label: '价格',
                    decorator_id: "price",
                    option: {
                        before_option: {
                            addonAfter: "元"
                        },
                        after_option: {
                            addonAfter: "元"
                        }
                    }
                },
                {
                    type: InputType.rangeInput,
                    label: '销量',
                    decorator_id: "sales_num",
                },
                // {
                //     type: InputType.select,
                //     label: '上架状态',
                //     decorator_id: "active_status",
                //     option: {
                //         placeholder: "请选择上架状态",
                //         childrens: [{
                //             key: '',
                //             label: '全部'
                //         }, {
                //             key: '上架',
                //             label: '上架'
                //         }, {
                //             key: '下架',
                //             label: '下架'
                //         }].map((item: any) => <Option key={item.key} value={item.key}>{item.label}</Option>),
                //     }
                // },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.shopping_mall_service,
            service_option: {
                select: {
                    service_func: 'get_goods_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            onRef: this.onRef,
            // edit_permission: this.props.edit_permission,
            // add_permission: this.props.add_permission,
            // delete_permission: this.props.delete_permission,
        };
        let products_list = Object.assign(products, table_param);
        const modal_left = 6;
        let { sh_data, status_list } = this.state;
        const validate = this.getDefaultValidate();
        return (
            <Row className="shangcheng-table products-list-table sh">
                <SignFrameLayout {...products_list} />
                <Modal
                    title="审核"
                    visible={this.state.model_visible}
                    onOk={this.handleOk}
                    onCancel={() => this.handleModelVisible(false)}
                    width={500}
                    cancelText="关闭"
                    className="products-list-modal"
                >
                    <>
                        <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>佣金</Col>
                            <Col span={24 - modal_left} className={validate.commission_ratio.className}>
                                <Input value={sh_data['commission_ratio'] || 0} onChange={(e) => this.setValue('commission_ratio', e)} addonAfter="%" style={{ width: 200, textAlign: 'center' }} />
                            </Col>
                        </Row>
                        {validate.commission_ratio.textTips ? <Row type="flex" justify="end" align="middle">
                            <Col span={24 - modal_left} className="error-tips">{validate.commission_ratio.textTips}</Col>
                        </Row> : null}
                        {/* <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>交易费率</Col>
                            <Col span={24 - modal_left} className={validate.transaction_rate.className}>
                                <Input value={sh_data['transaction_rate'] || 0} onChange={(e) => this.setValue('transaction_rate', e)} addonAfter="%" style={{ width: 200, textAlign: 'center' }} />
                            </Col>
                        </Row>
                        {validate.transaction_rate.textTips ? <Row type="flex" justify="end" align="middle">
                            <Col span={24 - modal_left} className="error-tips">{validate.transaction_rate.textTips}</Col>
                        </Row> : null}
                        <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>慈善基金比例</Col>
                            <Col span={24 - modal_left} className={validate.charitable_commission_ratio.className}>
                                <Input value={sh_data['charitable_commission_ratio'] || 0} onChange={(e) => this.setValue('charitable_commission_ratio', e)} addonAfter="%" style={{ width: 200, textAlign: 'center' }} />
                            </Col>
                        </Row>
                        {validate.charitable_commission_ratio.textTips ? <Row type="flex" justify="end" align="middle">
                            <Col span={24 - modal_left} className="error-tips">{validate.charitable_commission_ratio.textTips}</Col>
                        </Row> : null} */}
                        <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>支付方式</Col>
                            <Col span={24 - modal_left} className={validate.pay_type.className}>
                                <Checkbox.Group
                                    value={sh_data['pay_type'] || []}
                                    onChange={(e) => this.setValue('pay_type', e)}
                                    options={[
                                        // { label: '慈善基金', value: '慈善基金' },
                                        { label: '线上', value: '线上' },
                                        // { label: '余额', value: '余额' },
                                    ]}
                                    defaultValue={['慈善基金']}
                                />
                            </Col>
                        </Row>
                        {validate.pay_type.textTips ? <Row type="flex" justify="end" align="middle">
                            <Col span={24 - modal_left} className="error-tips">{validate.pay_type.textTips}</Col>
                        </Row> : null}
                        <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>审核</Col>
                            <Col span={24 - modal_left}>
                                <Radio.Group value={sh_data['status'] || ''} onChange={(e) => this.setValue('status', e)}>
                                    {status_list.map((item: any, index: number) => {
                                        return (
                                            <Radio key={item.code} value={item.code} >{item.name}</Radio>
                                        );
                                    })}
                                </Radio.Group>
                            </Col>
                        </Row>
                        {sh_data['status'] === 'APPROVAL_FAILED' ? <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left} />
                            <Col span={24 - modal_left} className={validate.reason.className}>
                                <TextArea value={sh_data['reason'] || ''} onChange={(e) => this.setValue('reason', e)} placeholder={'请输入审核不通过原因...'} />
                            </Col>
                        </Row> : null}
                        {validate.reason.textTips ? <Row type="flex" justify="end" align="middle">
                            <Col span={24 - modal_left} className="error-tips">{validate.reason.textTips}</Col>
                        </Row> : null}
                        <Row type="flex" align="middle" className="modal-section">
                            <Col span={modal_left}>序号</Col>
                            <Col span={24 - modal_left * 2}>
                                <InputNumber value={sh_data['sort'] || 0} onChange={(e) => this.setValue('sort', e)} min={1} max={999} style={{ width: '95%', textAlign: 'center' }} />
                            </Col>
                            <Col span={modal_left}>值越大，越靠前</Col>
                        </Row>
                    </>
                </Modal>
            </Row>
        );
    }
}

/**
 * 控件：商品审核控制器
 * @description 商品审核
 * @author
 */
@addon('ProductsShView', '商品审核', '商品审核')
@reactControl(Form.create<any>()(ProductsShView), true)
export class ProductsShViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}