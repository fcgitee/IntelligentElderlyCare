import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, Row, Col, Table, Button, Skeleton, Modal, Icon, message } from "antd";
import { request, checkPermission } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from "src/projects/router";
import { showActionByOrderStatus, showDeliverGoodsModal, updateOrder } from "./product-orders";
/**
 * 组件：商品订单详情状态
 */
export class ChangeProductOrderState {
    order_info?: any;
    logistics_list?: any;
    delivery_list?: any;
    edit_permission?: boolean;
    is_send?: boolean;
    delivery_goods_visible?: boolean;
    logistics_id?: string;
    logistics_number?: string;
    selected_id?: any;
}

/**
 * 组件：商品订单详情
 * 商品订单详情
 */
export class ChangeProductOrder extends React.Component<ChangeProductOrderControl, ChangeProductOrderState> {
    constructor(props: ChangeProductOrderControl) {
        super(props);
        this.state = {
            order_info: [],
            logistics_list: [],
            delivery_list: [],
            edit_permission: false,
            is_send: false,
            delivery_goods_visible: false,
            logistics_id: '',
            logistics_number: '',
            selected_id: '',
        };
    }
    componentWillMount() {
        this.setState({
            edit_permission: checkPermission(this.props.edit_permission!),
        });

        this.getData();

        // 获取物流字典
        request(this, AppServiceUtility.shopping_mall_service.get_data_dictionary_childs_list!({ parent_codes: ['LOGISTICS_TYPE', 'DELIVERY_TYPE'], in_select: true })!)
            .then((datas: any) => {
                let delivery_list: any = [];
                let logistics_list: any = [];
                for (let i = 0; i < datas.result.length; i++) {
                    if (datas.result[i]['parent_code'] === 'LOGISTICS_TYPE') {
                        logistics_list.push(datas.result[i]);
                    } else if (datas.result[i]['parent_code'] === 'DELIVERY_TYPE') {
                        delivery_list.push(datas.result[i]);
                    }
                }
                if (datas && datas.result) {
                    this.setState({
                        delivery_list,
                        logistics_list,
                    });
                }
            }).catch((error: Error) => {
                console.error(error);
            });
    }
    getData = (cb?: Function) => {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.shopping_mall_service.get_product_orders_list_all!({ id: this.props.match!.params.key })!)
                .then((datas: any) => {
                    if (datas && datas.result && datas.result[0]) {
                        this.setState(
                            {
                                order_info: datas.result[0],
                            },
                            () => {
                                cb && cb(datas.result[0]);
                            }
                        );
                    }
                });
        }
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.productOrders);
    }
    returnMoney = () => {
        Modal.confirm({
            title: '退款',
            content: '您确定要退款吗？退款后订单支付金额将原路退回！',
            okText: '确定',
            cancelText: '关闭',
            icon: (<Icon type="info-circle" />),
            onOk: () => {
                console.log('退款确认');
            },
            onCancel: () => {
                console.log('退款确认');
            },
        });
    }

    ctrl = (type: string, record?: any) => {
        const { selected_id, logistics_id, logistics_number } = this.state;
        switch (type) {
            case 'setValue':
                this.setState(
                    {
                        [record.key]: record.value,
                    }
                );
                break;
            case 'select':
                this.getData();
                break;
            case 'setCanceled':
                updateOrder('setCanceled', record.id, this, { order_status: 'CANCELED' }, () => {
                    this.getData();
                });
                break;
            case 'setDelivered':
                // 注意此时selected_id是Order_Line的主键id
                if (!logistics_id) {
                    message.error('请选择物流公司！');
                    return;
                }
                if (!logistics_number) {
                    message.error('请输入物流单号！');
                    return;
                }
                if (!/^[0-9a-zA-Z]+$/.test(logistics_number)) {
                    message.error('物流单号只能为英文+数字组合');
                    return;
                }
                updateOrder('setDelivered', selected_id, this, { logistics_id, logistics_number, order_status: 'DELIVERED' }, () => {
                    this.getData();
                });
                break;
            // 设为已完成
            case 'setCompleted':
                updateOrder('setCompleted', record.id, this, { order_status: 'COMPLETED' }, () => {
                    this.getData();
                });
                break;
            // 设为已付款
            case 'setPaid':
                updateOrder('setPaid', record.id, this, { order_status: 'PAID', pay_status: 'PAID' }, () => {
                    this.getData();
                });
                break;
            // 退款
            case 'setRefunded':
                updateOrder('setRefunded', record.id, this, { order_status: 'REFUNDED' }, () => {
                    this.getData();
                });
                break;
            default: break;
        }
    }
    render() {
        let { order_info, edit_permission, logistics_list, delivery_list } = this.state;
        let left_span = 6;
        let right_span = 24 - left_span;
        return (
            <MainContent>
                {order_info.hasOwnProperty('id') ? <Card className="product-order-detail">
                    <Row>
                        <span className="mr100">订单编号：{order_info.order_number}</span>
                        <span className="mr100">下单时间：{order_info.create_date}</span>
                        <span>发货时间：{order_info && order_info.line_info && order_info.line_info[0] && order_info.line_info[0].delivery_date ? order_info.line_info[0].delivery_date : '-'}</span>
                    </Row>
                    <table className="product-order-info-table">
                        <thead>
                            <tr>
                                <th>收货人信息</th>
                                <th>配送信息</th>
                                <th>付款信息</th>
                                <th>买家信息</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr key={0}>
                                <td>
                                    <Row>
                                        <Col span={left_span}>收货人：</Col>
                                        <Col span={right_span}>{order_info.create_user_name || '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>配送方式：</Col>
                                        <Col span={right_span}>
                                            {(() => {
                                                if (order_info.line_info && order_info.line_info[0] && order_info.line_info[0]['product_postage_type']) {
                                                    let product_postage_type = order_info.line_info[0]['product_postage_type'];
                                                    if (Array.isArray(product_postage_type)) {
                                                        let delivery_arr: any = [];
                                                        delivery_list.map((itm: any, idx: number) => {
                                                            delivery_arr[itm['code']] = itm['name'];
                                                        });
                                                        let delivery_name_arr: any = [];
                                                        product_postage_type.map((itm: any, index: number) => {
                                                            if (delivery_arr[itm]) {
                                                                delivery_name_arr.push(delivery_arr[itm]);
                                                            }
                                                        });
                                                        return delivery_name_arr.join('，');
                                                    }
                                                    return product_postage_type;
                                                } else {
                                                    return '-';
                                                }
                                            })()}
                                        </Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>实付金额：</Col>
                                        <Col span={right_span}>{order_info.final_amount ? `￥${order_info.final_amount}` : '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>买家：</Col>
                                        <Col span={right_span}>{order_info.buyer_name || '-'}</Col>
                                    </Row>
                                </td>
                            </tr>
                            <tr key={1}>
                                <td>
                                    <Row>
                                        <Col span={left_span}>联系电话：</Col>
                                        <Col span={right_span}>{order_info.create_user_mobile || '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>发货时间：</Col>
                                        <Col span={right_span}>{order_info && order_info.line_info && order_info.line_info[0] && order_info.line_info[0].delivery_date ? order_info.line_info[0].delivery_date : '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>快递费用：</Col>
                                        <Col span={right_span}>{order_info.postage_amount === 0 ? '包邮' : `￥${order_info.postage_amount}`}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>买家电话：</Col>
                                        <Col span={right_span}>{order_info.create_user_mobile || '-'}</Col>
                                    </Row>
                                </td>
                            </tr>
                            <tr key={2}>
                                <td>
                                    <Row>
                                        <Col span={left_span}>收货地址：</Col>
                                        <Col span={right_span}>{order_info.address || '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>物流公司：</Col>
                                        <Col span={right_span}>{order_info && order_info.line_info && order_info.line_info[0] && order_info.line_info[0].logistics_name ? order_info.line_info[0].logistics_name : '-'}</Col>
                                    </Row>
                                </td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>佣金：</Col>
                                        <Col span={right_span}>
                                            {`￥${order_info.commission_ratio_amount}`}
                                        </Col>
                                    </Row>
                                </td>
                                {/* <td>
                                    <Row>
                                        <Col span={left_span}>慈善账户：</Col>
                                        <Col span={right_span}>
                                            {order_info.charitable_commission_ratio_amount}
                                        </Col>
                                    </Row>
                                </td> */}
                                <td>
                                    <Row>
                                        <Col span={left_span}>备注：</Col>
                                        <Col span={right_span}>{order_info.remark || '-'}</Col>
                                    </Row>
                                </td>
                            </tr>
                            <tr key={3}>
                                <td>{null}</td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>物流单号：</Col>
                                        <Col span={right_span}>{order_info && order_info.line_info && order_info.line_info[0] && order_info.line_info[0].logistics_number ? order_info.line_info[0].logistics_number : '-'}</Col>
                                    </Row>
                                </td>
                                <td>{null}</td>
                            </tr>
                            {/* <tr key={4}>
                                <td>{null}</td>
                                <td>{null}</td>
                                <td>
                                    <Row>
                                        <Col span={left_span}>交易费：</Col>
                                        <Col span={right_span}>
                                            {order_info.transaction_rate_amount}
                                        </Col>
                                    </Row>
                                </td>
                            </tr> */}
                        </tbody>
                    </table>
                    <Table
                        columns={[
                            {
                                title: '商品',
                                dataIndex: 'product_name',
                                key: 'product_name',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row type="flex" justify="start" style={{ flexFlow: 'unset', justifyContent: 'left', alignItems: 'center' }} className="product-orders-table-row">
                                            {record.product_image_urls && record.product_image_urls[0] ? <Row className="product-picture">
                                                <img src={record.product_image_urls[0]} />
                                            </Row> : null}
                                            <Row className="maxLine3" style={{ WebkitBoxOrient: 'vertical' }}>{record.product_title}</Row>
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '价格（元）/数量',
                                dataIndex: 'unit_price',
                                key: 'unit_price',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="product-orders-table-row" type="flex">
                                            <Row>
                                                {record.unit_price}
                                            </Row>
                                            <Row>
                                                x{record.number}
                                            </Row>
                                        </Row>
                                    );
                                }
                            }, {
                                title: '买家/收货人',
                                dataIndex: 'buyer_name',
                                key: 'buyer_name',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="product-orders-table-row" type="flex">
                                            <Row>
                                                {order_info.buyer_name}
                                            </Row>
                                            <Row>
                                                {order_info.create_user_name}
                                            </Row>
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '配送方式',
                                dataIndex: 'logistics_name',
                                key: 'logistics_name',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="product-orders-table-row" type="flex">
                                            {(() => {
                                                if (record.product_postage_type) {
                                                    let product_postage_type = record.product_postage_type;
                                                    if (Array.isArray(product_postage_type)) {
                                                        let delivery_arr: any = [];
                                                        delivery_list.map((itm: any, idx: number) => {
                                                            delivery_arr[itm['code']] = itm['name'];
                                                        });
                                                        let delivery_name_arr: any = [];
                                                        product_postage_type.map((itm: any, index: number) => {
                                                            if (delivery_arr[itm]) {
                                                                delivery_name_arr.push(delivery_arr[itm]);
                                                            }
                                                        });
                                                        return delivery_name_arr.join('，');
                                                    }
                                                    return product_postage_type;
                                                }
                                            })()}
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '小计',
                                dataIndex: 'product_price',
                                key: 'product_price',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="product-orders-table-row" type="flex">
                                            {`￥${record.product_price}`}
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '订单状态',
                                dataIndex: 'order_status',
                                key: 'order_status',
                                render: (text: any, record: any) => {
                                    return (
                                        <Row className="product-orders-table-row" type="flex">
                                            {order_info.order_status_name}
                                        </Row>
                                    );
                                }
                            },
                            {
                                title: '操作',
                                dataIndex: 'action',
                                key: 'action',
                                render: (text: any, record: any) => {
                                    return (
                                        edit_permission ? showActionByOrderStatus(order_info, record, this) : null
                                    );
                                }
                            },
                        ]}
                        dataSource={order_info.line_info}
                        rowKey="id"
                        pagination={false}
                        className="change-shangcheng-table"
                    />
                    <Row className="price-info" type="flex" justify="end" align="bottom">
                        <Row className="price-info-row">
                            <Col span={10}>商品总价</Col>
                            <Col span={14}>￥{order_info.product_amount || 0}</Col>
                        </Row>
                        <Row className="price-info-row">
                            <Col span={10}>运费</Col>
                            <Col span={14}>￥{order_info.postage_amount || 0}</Col>
                        </Row>
                        <Row className="price-info-row all-price">
                            <Col span={10}>实收金额</Col>
                            <Col span={14}>￥{order_info.final_amount || 0}</Col>
                        </Row>
                    </Row>
                    <div className="product-order-back-btn-pr">
                        <Row className='product-order-back-btn' type="flex" justify="center" align="middle">
                            <Button onClick={this.backList}>
                                返回
                            </Button>
                        </Row>
                    </div>
                    {showDeliverGoodsModal(this, logistics_list)}
                </Card> : <Card><Skeleton paragraph={{ rows: 8 }} /></Card>}
            </MainContent>
        );
    }
}

/**
 * 控件：商品订单详情控制器
 * 商品订单详情
 */
@addon('ChangeProductOrder', '商品订单详情', '商品订单详情')
@reactControl(ChangeProductOrder, true)
export class ChangeProductOrderControl extends BaseReactElementControl {
    /** 编辑权限 */
    public edit_permission?: string;
}