import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
/**
 * 状态：收费减免编辑页面
 */
export interface ChangeDeductionViewState extends ReactViewState {
}

/**
 * 组件：收费减免编辑页面视图
 */
export class ChangeDeductionView extends ReactView<ChangeDeductionViewControl, ChangeDeductionViewState> {
    constructor(props: ChangeDeductionViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "人员名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            },],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_elder_list',
            title: '人员查询',
            select_option: {
                placeholder: "请选择人员",
            }
        };
        let edit_props = {
            form_items_props: [{
                title: "收费减免",
                need_card: true,
                input_props: [
                    {
                        type: InputType.modal_search,
                        label: "收款人",
                        decorator_id: 'get_user_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择收款人" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择收款人",
                            autoComplete: 'off',
                            modal_search_items_props: modal_search_items_props
                        },
                    },
                    {
                        type: InputType.antd_input,
                        label: "卡号",
                        decorator_id: "get_card_no",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入收款卡号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入收款卡号",
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.input_number,
                        label: "减免金额",
                        decorator_id: "price",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入减免金额" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入减免金额",
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.modal_search,
                        label: "付款人",
                        decorator_id: 'pay_user_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择付款人" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择付款人",
                            autoComplete: 'off',
                            modal_search_items_props: modal_search_items_props
                        },
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_deduction_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_deduction"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：收费减免编辑页面控件
 * @description 收费减免编辑页面控件
 * @author
 */
@addon('ChangeDeductionViewControl', '收费减免编辑页面控件', '收费减免编辑页面控件')
@reactControl(ChangeDeductionView, true)
export class ChangeDeductionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}