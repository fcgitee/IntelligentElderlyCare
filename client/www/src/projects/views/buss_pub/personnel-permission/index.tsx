import { addon, } from "pao-aop";
import { reactControl } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { BasePermissionViewState, BasePermissionView, BasePermissionViewControl } from "src/business/views/base";

/** 状态：人员清单视图 */
export interface PersonnelPermissionViewState extends BasePermissionViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 用户信息 */
    user_info?: any;
}
/** 组件：人员清单视图 */
export class PersonnelPermissionView extends BasePermissionView<PersonnelPermissionControl, PersonnelPermissionViewState> {
    private columns_data_source = [{
        title: '证件号码',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '证件类型',
        dataIndex: 'id_card_type',
        key: 'id_card_type',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生日期',
        dataIndex: 'date_birth',
        key: 'date_birth',
    }, {
        title: '备注',
        dataIndex: 'comment',
        key: 'comment',
    }, {
        title: '电话',
        dataIndex: 'telephone',
        key: 'telephone',
    }, {
        title: '联系人',
        dataIndex: 'contacts',
        key: 'contacts',
    }, {
        title: '联系人电话',
        dataIndex: 'contacts_telephone',
        key: 'contacts_telephone',
    }, {
        title: '家庭住址',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '人员类型',
        dataIndex: 'personnel_type',
        key: 'personnel_type',
    },];
    constructor(props: PersonnelPermissionControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10]
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changePersonnel);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(this.props.change_url ? this.props.change_url + '/' + contents.id : ROUTE_PATH.changePersonnel + '/' + contents.id);
        }
    }
    componentDidMount() {
        // 获取当前用户信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data) => {
                this.setState({
                    user_info: data,
                });
            })
            .catch((err) => {
                // console.log(err);
            });
    }
    render() {
        let get_user_list_func = '';
        if (this.state['user_type'] === 'is_area') {
            get_user_list_func = 'filter_current_area';
        } else if (this.state['user_type'] === 'is_org') {
            get_user_list_func = 'filter_current_org';
        } else if (this.state['user_type'] === 'is_all') {
            get_user_list_func = 'get_user_list';
        }
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "证件号码",
                    decorator_id: "id_card"
                },
            ],
            btn_props: [{
                label: '新增档案',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    // 根据用户信息判断调用接口
                    service_func: get_user_list_func,
                    service_condition: this.state.condition
                },
                delete: {
                    service_func: 'delete_personnel'
                }
            },
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <SignFrameLayout {...personnel_list} />
            )
        );
    }
}

/**
 * 控件：人员清单视图控制器
 * @description 人员清单视图
 * @author
 */
@addon('PersonnelView', '人员清单视图', '人员清单视图')
@reactControl(PersonnelPermissionView, true)
export class PersonnelPermissionControl extends BasePermissionViewControl {
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
}