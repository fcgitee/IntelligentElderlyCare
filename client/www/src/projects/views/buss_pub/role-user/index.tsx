
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { IPermissionService } from "src/business/models/role";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";

/** 状态：角色权限视图 */
export interface RolePermissionViewState extends ReactViewState {
    /** 列表数据源 */
    data_source?: any[];
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 数据总条数 */
    total_data?: number;
    /** 角色 */
    role_name?: string;
    /** 角色列表 */
    role_list?: any[];
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：角色权限视图 */
export class RolePermissionView extends ReactView<RolePermissionViewControl, RolePermissionViewState> {
    private columns_data_source = [{
        title: '工作人员名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '角色名称',
        dataIndex: 'role_name',
        key: 'role_name',
    }, {
        title: '所属组织',
        dataIndex: 'role_of_account',
        key: 'role_of_account',
    }, {
        title: '说明',
        dataIndex: 'comment',
        key: 'comment',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }];
    constructor(props: RolePermissionViewControl) {
        super(props);
        this.state = {
            loading: false,
            condition: {},
            org_list: [],
        };
    }
    /** 新建角色按钮 */
    addRolw = () => {
        // alert('新建角色');
        this.props.history!.push(ROUTE_PATH.changeRoleUser);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRoleUser + '/' + contents.id);
        }
    }
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    /** 角色名称输入框回调 */
    roleNameChange = (event: any) => {
        this.setState({
            role_name: event.target.value
        });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data:any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err:any) => {
                console.info(err);
            });
    }
    render() {
        let role_permission = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "工作人员名称",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入工作人员名称'
                    }
                },
                {
                    type: InputType.input,
                    label: "角色名称",
                    decorator_id: "role_name",
                    option: {
                        placeholder: '请输入角色名称'
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "organization_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "所属组织",
                //     decorator_id: "organization_name",
                //     option: {
                //         placeholder: '请输入所属组织'
                //     }
                // },
            ],
            btn_props: [{
                label: '新增用户关联',
                btn_method: this.addRolw,
                icon: 'plus'
            }],
            // table 表格配置
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            show_footer: true,
            page_on_click: this.pageOnCick,
            show_size_change: this.showSizeChange,
            rowKey: 'id',
            service_object: this.props.roleService,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_role_user'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        return (
            (
                <SignFrameLayout {...role_permission} />
            )
        );
    }
}

/**
 * 控件：角色权限视图控制器
 * @description 角色权限视图
 */
@addon('RolePermissionView', '角色权限视图', '角色权限视图')
@reactControl(RolePermissionView, true)
export class RolePermissionViewControl extends ReactViewControl {

    /** 角色服务 */
    public roleService?: IPermissionService;
    /** 视图权限 */
    public permission?: Permission;
    /** 权限判断方法 */
    public isPermission?: (permission: Permission) => React.ReactNode | undefined;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}