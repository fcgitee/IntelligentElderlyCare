
import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Ref, getObject, Permission, Role, IPermissionService, IRoleService } from "pao-aop";
import { message, Spin, Icon } from "antd";
import AddRoleForm from "src/business/security/add-role";
import './change-role-user.less';
import { IPersonOrgManageService } from "src/business/models/person-org-manage";
import { IPermissionService as IPermissionServices } from "src/business/models/role";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";

/** 状态：新增角色视图 */
export interface AddRoleUserViewState extends ReactViewState {
    /** 权限列表 */
    permission_list?: Permission[];
    /** 角色信息 */
    role?: Role;
    /** 滚动条 */
    loading?: boolean;
    /** 角色列表 */
    role_list?: any[];
    /** 组织机构列表 */
    organizational_list?: any[];
    /** 账户列表 */
    account_list?: any[];
}
/**
 * 获取的权限
 */
let permissionList: any[] = [];
/** 组件：新增角色视图 */
export class AddRoleUserView extends ReactView<AddRoleUserViewControl, AddRoleUserViewState> {
    constructor(props: AddRoleUserViewControl) {
        super(props);
        this.state = {
            loading: false
        };
    }
    /** 初始化服务 */
    permissionService?() {
        return getObject(this.props.permissionService_Fac!);
    }
    roleService?() {
        return getObject(this.props.roleService_Fac!);
    }
    per_org_service?() {
        return getObject(this.props.per_org_service!);
    }
    get_permission_services?() {
        return getObject(this.props.permission_services!);
    }
    componentDidMount() {
        /** 角色列表 */
        request_func(this, AppServiceUtility.role_service.get_role_list_all, [{}], (data: any) => {
            this.setState({
                role_list: data.result
            });
        });
        /** 工作人员列表 */
        request_func(this, AppServiceUtility.person_org_manage_service.get_worker_list_all, [{}], (data: any) => {
            this.setState({
                account_list: data.result
            });
        });
        /** 组织机构列表 */
        request_func(this, AppServiceUtility.person_org_manage_service.get_organization_list_all, [{}], (data: any) => {
            this.setState({
                organizational_list: data.result
            });
        });
        let id = this.props.match!.params.key;
        permissionList = this.props.permission_list!;
        if (id) {
            request_func(this, AppServiceUtility.role_service.get_role_user_list_all, [{ 'id': id }], (data: any) => {
                this.setState({
                    role: data.result![0]
                });
            });
        }
    }
    handleSubmit = (err: any, values: any) => {
        if (values.role_name === '') {
            message.info('角色名称不能为空');
            return;
        }
        if (permissionList) {
            values.permission = permissionList;
        }
        this.setState({
            loading: true
        });
        request_func(this, AppServiceUtility.role_service.update_role_user, [values], (data: any) => {
            if (data === 'Success') {
                message.info('保存成功');
                this.props.history!.goBack();
            } else {
                message.info(data);
            }
            this.setState({
                loading: false
            });
        });
    }
    /**
     * 权限选择
     * @param values 返回值
     */
    permission_select(values: any) {
        permissionList = values;
    }
    render() {
        const redeirect = this.props.isPermission ? this.props.isPermission!(this.props.permission!) : undefined;
        if (redeirect) {
            return redeirect;
        }
        const antIcon = <Icon type="loading" className='loading-icon' style={{ fontSize: 24 }} spin={true} />;
        return (
            <Spin spinning={this.state.loading ? this.state.loading : false} indicator={antIcon} >
                <AddRoleForm
                    handleSubmit={this.handleSubmit}
                    permission={this.state.permission_list}
                    role={this.state.role}
                    permission_select={this.permission_select}
                    role_list={this.state.role_list}
                    organization_list={this.state.organizational_list}
                    account_list={this.state.account_list}
                    return_btn={() => { this.props.history!.goBack(); }}
                />
            </Spin>
        );
    }
}

/**
 * 控件：新增角色关联视图控制器
 * @description 新增角色关联视图
 */
@addon('AddRoleUserView', '新增角色关联视图', '新增角色关联视图')
@reactControl(AddRoleUserView, true)
export class AddRoleUserViewControl extends ReactViewControl {
    /**
     * 权限服务接口
     */
    public permissionService_Fac?: Ref<IPermissionService>;
    /** 角色服务 */
    public roleService_Fac?: Ref<IRoleService>;
    /** 视图权限 */
    public permission?: Permission;
    /** 权限判断方法 */
    public isPermission?: (permission: Permission) => React.ReactNode | undefined;
    /** 权限列表 */
    public permission_list?: any[];
    /** 人员  */
    public per_org_service?: Ref<IPersonOrgManageService>;
    /** 角色设置接口 */
    public permission_services?: Ref<IPermissionServices>;
}