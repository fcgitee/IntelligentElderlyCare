import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
/**
 * 状态：用餐新增编辑页面
 */
export interface ChangeEdlerFoodViewState extends ReactViewState {
}

/**
 * 组件：用餐新增编辑页面视图
 */
export class ChangeEdlerFoodView extends ReactView<ChangeEdlerFoodViewControl, ChangeEdlerFoodViewState> {
    constructor(props: ChangeEdlerFoodViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            }],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_personnel_elder',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
                autoComplete: 'off',
            }
        };
        let edit_props = {
            form_items_props: [{
                title: "用餐信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.modal_search,
                        label: "长者",
                        decorator_id: 'elder_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择长者" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择长者",
                            modal_search_items_props: modal_search_items_props,
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.date,
                        label: "选择用餐时间",
                        decorator_id: "use_food_date",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择用餐时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择用餐时间",
                            format: "YYYY-MM-DD HH:mm:ss",
                            showTime: true,
                            autocomplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_elder_food_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_elder_food"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：用餐新增编辑页面控件
 * @description 用餐新增编辑页面控件
 * @author
 */
@addon('ChangeEdlerFoodViewControl', '用餐新增编辑页面控件', '用餐新增编辑页面控件')
@reactControl(ChangeEdlerFoodView, true)
export class ChangeEdlerFoodViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}