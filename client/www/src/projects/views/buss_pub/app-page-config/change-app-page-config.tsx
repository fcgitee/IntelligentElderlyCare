import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { Select } from "antd";
let Option = Select.Option;
/**
 * 状态：app页面设置编辑页面
 */
export interface ChangeAppPageConfigViewState extends ReactViewState {
}

/**
 * 组件：app页面设置编辑页面视图
 */
export class ChangeAppPageConfigView extends ReactView<ChangeAppPageConfigViewControl, ChangeAppPageConfigViewState> {
    constructor(props: ChangeAppPageConfigViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let font_list: JSX.Element[] = ['微软雅黑', '宋体'].map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let edit_props = {
            form_items_props: [{
                title: "app页面设置",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "页面名称",
                        decorator_id: 'name',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入页面名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            disabled: this.props.match!.params.key ? true : false,
                            placeholder: "请输入页面名称",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "字体大小",
                        decorator_id: 'font_size',
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入字体大小",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input,
                        label: "字体颜色",
                        decorator_id: 'font_color',
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入字体颜色",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.select,
                        label: "字体样式",
                        decorator_id: 'font_family',
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入字体样式",
                            childrens: font_list,
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: 'remark',
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            autoComplete: 'off',
                        },
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.app_page_config_service,
                operation_option: {
                    query: {
                        func_name: "get_app_page_config_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_app_page_config"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：app页面设置编辑页面控件
 * @description app页面设置编辑页面控件
 * @author
 */
@addon('ChangeAppPageConfigViewControl', 'app页面设置编辑页面控件', 'app页面设置编辑页面控件')
@reactControl(ChangeAppPageConfigView, true)
export class ChangeAppPageConfigViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}