import { Row } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';
/**
 * 组件：app页面设置列表状态
 */
export interface AppPageConfigListViewState extends ReactViewState {
    project_list?: any;
}

/**
 * 组件：app页面设置列表
 * 描述
 */
export class AppPageConfigListView extends ReactView<AppPageConfigListViewControl, AppPageConfigListViewState> {
    private columns_data_source = [{
        title: '页面名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '字体设置',
        dataIndex: 'font_family',
        key: 'font_family',
    }, {
        title: '字体大小',
        dataIndex: 'font_size',
        key: 'font_size',
    }, {
        title: '字体颜色',
        dataIndex: 'font_color',
        key: 'font_color',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            project_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeAppPageConfig);
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeAppPageConfig + '/' + contents.id);
        }
    }
    componentDidMount() {
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "页面名称",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入页面名称",
                        autoComplete: 'off',
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.app_page_config_service,
            service_option: {
                select: {
                    service_func: 'get_app_page_config_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_app_page_config'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：app页面设置列表
 * 描述
 */
@addon('AppPageConfigListView', 'app页面设置列表', '描述')
@reactControl(AppPageConfigListView, true)
export class AppPageConfigListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}