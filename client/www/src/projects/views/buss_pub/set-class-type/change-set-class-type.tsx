import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
/**
 * 状态：排班类型编辑页面
 */
export interface ChangeSetClassTypeViewState extends ReactViewState {
}

/**
 * 组件：排班类型编辑页面视图
 */
export class ChangeSetClassTypeView extends ReactView<ChangeSetClassTypeViewControl, ChangeSetClassTypeViewState> {
    constructor(props: ChangeSetClassTypeViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let edit_props = {
            form_items_props: [{
                title: "排班类型",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "排班名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入排班名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入排班名称",
                            autocomplete: 'off',
                        }
                    },
                    {
                        type: InputType.antd_input,
                        label: "时间段",
                        decorator_id: "time_range",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入时间段" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入时间段",
                            autocomplete: 'off',
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_set_class_type_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_set_class_type"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：排班类型编辑页面控件
 * @description 排班类型编辑页面控件
 * @author
 */
@addon('ChangeSetClassTypeViewControl', '排班类型编辑页面控件', '排班类型编辑页面控件')
@reactControl(ChangeSetClassTypeView, true)
export class ChangeSetClassTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}