import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { Select } from "antd";
const { Option } = Select;
/**
 * 状态：排班编辑页面
 */
export interface ChangeSetClassViewState extends ReactViewState {
    // 信息类型
    set_class_type: any;
}

/**
 * 组件：排班编辑页面视图
 */
export class ChangeSetClassView extends ReactView<ChangeSetClassViewControl, ChangeSetClassViewState> {
    constructor(props: ChangeSetClassViewControl) {
        super(props);
        this.state = {
            set_class_type: [],
        };
    }
    componentDidMount() {
        // 获取信息类型
        request(this, AppServiceUtility.person_org_manage_service.get_set_class_type_list!({})!)
            .then((datas: any) => {
                this.setState({
                    set_class_type: datas.result,
                });
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "人员名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            },],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_worker_list_all',
            title: '人员查询',
            select_option: {
                placeholder: "请选择人员",
            }
        };
        let type = this.state.set_class_type;
        let set_class_type: any[] = [];
        type!.map((item: any) => {
            set_class_type.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props = {
            form_items_props: [{
                title: "排班",
                need_card: true,
                input_props: [
                    {
                        type: InputType.modal_search,
                        label: "选择人员",
                        decorator_id: 'user_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择人员" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择人员",
                            autoComplete: 'off',
                            modal_search_items_props: modal_search_items_props
                        },
                    },
                    {
                        type: InputType.select,
                        label: "排班类型",
                        decorator_id: 'type_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择排班类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择排班类型",
                            childrens: set_class_type,
                            autoComplete: 'off',
                        },
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_set_class_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_set_class"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：排班编辑页面控件
 * @description 排班编辑页面控件
 * @author
 */
@addon('ChangeSetClassViewControl', '排班编辑页面控件', '排班编辑页面控件')
@reactControl(ChangeSetClassView, true)
export class ChangeSetClassViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}