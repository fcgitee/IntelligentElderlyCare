import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";

import { ROUTE_PATH } from "src/projects/router";

import { AppServiceUtility } from "src/projects/app/appService";

import { table_param } from "src/projects/app/util-tool";

import SignFrameLayout, { } from "src/business/components/buss-components/sign-frame-layout";

import React from "react";

import { addon } from "pao-aop";
import { request } from "src/business/util_tool";
import { message } from "antd";

/**
 * 组件：补贴自动充值控制页面列表状态
 */
export interface SubsidyAutoRechargeViewState extends ReactViewState {
    request_url?: string;
}

/**
 * 组件：补贴自动充值控制页面列表
 * 描述
 */
export class SubsidyAutoRechargeView extends ReactView<SubsidyAutoRechargeViewControl, SubsidyAutoRechargeViewState> {
    private columns_data_source = [];
    constructor(props: SubsidyAutoRechargeViewControl) {
        super(props);
        this.state = {
            request_url: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        request(this, AppServiceUtility.subsidy_auto_recharge.update_service!({ operation_type: 'start' }))
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('操作成功');
                } else {
                    message.info('操作失败');
                }
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeServiceItemPackage + '/' + contents.id);
        }
    }
    componentDidMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [],
            btn_props: [{
                label: '',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.subsidy_auto_recharge,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：补贴自动充值控制页面列表页面
 */
@addon('SubsidyAutoRechargeView', '补贴自动充值控制页面列表页面', '描述')
@reactControl(SubsidyAutoRechargeView, true)
export class SubsidyAutoRechargeViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public add_permission?: string;
} 