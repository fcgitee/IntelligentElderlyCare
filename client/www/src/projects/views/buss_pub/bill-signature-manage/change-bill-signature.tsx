import { message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：单据签核状态
 */
export interface ChangeBillSignatureState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：单据签核
 * 描述
 */
export class ChangeBillSignature extends ReactView<ChangeBillSignatureControl, ChangeBillSignatureState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: ''
        };
    }
    refuse = () => {
        let submitObj = document.getElementById('remark') as HTMLInputElement;
        // console.log('submitObj', submitObj!.value);
        request(this, AppServiceUtility.bill_manage_service.refuse_bill_by_personId!({ id: this.state.id, remark: submitObj!.value }))
            .then((data: any) => {
                if (data === 'Success') {
                    this.props.history!.push(ROUTE_PATH.billSignature);
                } else {
                    message.info('拒审失败');
                }
            });

    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "单据审批",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "审批意见",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入审批意见" }],
                                // initialValue: base_data ? base_data.nursing_type : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入审批意见"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "拒审",
                    cb: this.refuse
                },
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.billSignature);
                    }
                }
            ],
            submit_btn_propps: {
                text: "审批通过"
            },
            service_option: {
                service_object: AppServiceUtility.bill_manage_service,
                operation_option: {
                    save: {
                        func_name: "signature_bill_by_personId"
                    },
                    query: {
                        func_name: "get_signature_bill_by_personId_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.billSignature); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：单据签核控制器
 * 描述
 */
@addon('ChangeBillSignature', '单据签核', '描述')
@reactControl(ChangeBillSignature, true)
export class ChangeBillSignatureControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}