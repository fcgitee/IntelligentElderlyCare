import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：单据签核列表视图状态
 */
export interface BillSignatureManageViewState extends ReactViewState {
    request_url?:string;
}

/**
 * 组件：单据签核列表视图
 * 描述
 */
export class BillSignatureManageView extends ReactView<BillSignatureManageViewControl, BillSignatureManageViewState> {
    private columns_data_source = [{
        title: '单据编号',
        dataIndex: 'bill_code',
        key: 'bill_code',
    }, {
        title: '单据类型',
        dataIndex: 'bill_type',
        key: 'bill_type',
    }, {
        title: '发起人',
        dataIndex: 'initiator',
        key: 'initiator',
    }, {
        title: '关联方',
        dataIndex: 'related_parties',
        key: 'related_parties',
    }, {
        title: '更改内容',
        dataIndex: 'body_mes',
        key: 'body_mes',
    }];
    constructor(props: BillSignatureManageViewControl) {
        super(props);
        this.state = {
            request_url:'',
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeBillSignature + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let bill_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "单据编号",
                    decorator_id: "bill_code"
                },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.bill_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
        };
        let bill_list = Object.assign(bill_info_list, table_param);
        return (
            <SignFrameLayout {...bill_list} />
        );
    }
}

/**
 * 控件：单据签核列表视图控制器
 * 描述
 */
@addon('BillSignatureManageView', '单据签核列表视图', '描述')
@reactControl(BillSignatureManageView, true)
export class BillSignatureManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
}