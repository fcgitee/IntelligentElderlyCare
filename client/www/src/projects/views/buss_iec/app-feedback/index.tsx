
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form, Button, Select, Modal, Row, Col, message } from 'antd';
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import TextArea from "antd/lib/input/TextArea";
let { Option } = Select;
/** 状态：APP意见反馈统计 */
export interface AppFeedbackViewState extends ReactViewState {
    change_permission?: boolean;
    table_column?: any;
    remark_is_show?: boolean;
    remark?: string;
    selected_data?: any;
    edit_type?: any;
    is_send: boolean;
}
/** 组件：APP意见反馈统计 */

export class AppFeedbackView extends React.Component<AppFeedbackViewControl, AppFeedbackViewState> {
    // 表单构造器的映射
    private formCreater: any = null;
    constructor(props: AppFeedbackViewControl) {
        super(props);
        this.state = {
            change_permission: false,
            remark_is_show: false,
            is_send: false,
            remark: '',
            selected_data: {},
            edit_type: '',
            table_column: [{
                title: "姓名",
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '手机号',
                dataIndex: 'telephone',
                key: 'telephone',
            },
            {
                title: '反馈意见内容',
                dataIndex: 'content',
                key: 'content',
            },
            {
                title: '客服人员',
                dataIndex: 'process_name',
                key: 'process_name',
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
            },
            {
                title: '反馈时间',
                dataIndex: 'create_date',
                key: 'create_date',
            },
            {
                title: '处理状态',
                dataIndex: 'status',
                key: 'status',
                render(text: any, record: any) {
                    return (
                        <div>
                            {record.status === '已处理' ? '已处理' : '未处理'}
                        </div>
                    );
                }
            },
            ],
        };
    }
    zdyFunction(type: any, record: any) {
        if (type === 'beizhu') {
            this.setState({
                remark_is_show: true,
                selected_data: record,
            });
        } else {
            this.props.history!.push(ROUTE_PATH.changeAppFeedback + '/' + record.id);
        }
    }
    componentDidMount = () => {
        let that: any = this;
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: 'APP意见反馈', p: '编辑' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.state.table_column.push({
                        title: '操作',
                        dataIndex: 'action2',
                        key: 'action2',
                        render(text: any, record: any) {
                            return (
                                <div>
                                    {record.status === '已处理' ? <Button size="small" type="primary" onClick={() => that.zdyFunction('xiangqing', record)}>详情</Button> : <Button size="small" type="primary" onClick={() => that.zdyFunction('chuli', record)}>处理</Button>}
                                    {/* &nbsp;&nbsp;<Button size="small" type="primary" onClick={() => that.zdyFunction('beizhu', record)}>备注</Button> */}
                                </div>
                            );
                        }
                    });
                }
                this.setState({
                    change_permission: datas,
                });
            });
    }
    // 取消显示编辑弹窗
    cancelEdit = () => {
        this.setState({
            remark_is_show: false
        });
    }
    // 表单构造器映射
    onRef = (ref: any) => {
        this.formCreater = ref;
    }
    // 确认编辑
    confirmEdit = () => {
        let { remark, selected_data, is_send } = this.state;
        if (remark === '') {
            message.error('请输入备注！');
            return;
        }
        if (remark === selected_data.remark) {
            message.error('没有任何修改！');
            return;
        }
        if (is_send) {
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.opinion_feedback_service.update_opinion_feedback!({ id: selected_data.id, remark: remark }))
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.info('备注成功！');
                            this.formCreater && this.formCreater.reflash();
                        } else {
                            message.info('备注失败！');
                        }
                    })
                    .catch((error: any) => {
                        console.error(error);
                    })
                    .finally(() => {
                        this.setState({
                            remark_is_show: false,
                            is_send: false,
                        });
                    });
            }
        );
    }
    // 输入备注事件
    remarkChange = (e: any) => {
        this.setState({
            remark: e.target.value
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "反馈时间",
                    decorator_id: "date_range",
                    option: {
                        placeholder: ['开始时间', '结束时间'],
                    },
                },
                {
                    type: InputType.select,
                    label: "处理状态",
                    decorator_id: "status",
                    option: {
                        placeholder: '请选择处理状态',
                        childrens: ['未处理', '已处理'].map((item, idx) => <Option key={idx} value={item}>{item}</Option>),
                    },
                },
                {
                    type: InputType.input,
                    label: "备注",
                    decorator_id: "remark",
                },
            ],
            columns_data_source: this.state.table_column,
            service_object: AppServiceUtility.opinion_feedback_service,
            service_option: {
                select: {
                    service_func: 'get_app_feedback_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            onRef: this.onRef,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="编辑信息"
                    cancelText='取消'
                    okText='确认'
                    visible={this.state.remark_is_show}
                    onOk={this.confirmEdit}
                    onCancel={this.cancelEdit}
                    width='400px'
                >
                    <Row type="flex" align="middle">
                        <Col span={6}>备注：</Col>
                        <Col span={18}>
                            <TextArea rows={4} onChange={this.remarkChange} value={this.state.remark} />
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：APP意见反馈统计控制器
 * @description APP意见反馈统计
 * @author
 */
@addon('AppFeedbackView', 'APP意见反馈统计', 'APP意见反馈统计')
@reactControl(Form.create<any>()(AppFeedbackView), true)
export class AppFeedbackViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}