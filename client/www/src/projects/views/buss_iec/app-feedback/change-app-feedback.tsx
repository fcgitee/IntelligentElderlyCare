import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Row, Card, Input, Button, message } from "antd";
import { request } from "src/business/util_tool";
import TextArea from "antd/lib/input/TextArea";

/**
 * 组件：编辑APP意见反馈状态
 */
export interface ChangeAppFeedbackViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 用户
    data_info?: any;
    process_content?: string;
    remark?: string;
    is_send?: boolean;
}

/**
 * 组件：编辑APP意见反馈视图
 */
export class ChangeAppFeedbackView extends ReactView<ChangeAppFeedbackViewControl, ChangeAppFeedbackViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: {},
            id: '',
            process_content: '',
            remark: '',
            is_send: false,
        };
    }
    componentDidMount = () => {
        request(this, AppServiceUtility.opinion_feedback_service.get_app_feedback_list!({ id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            });
    }
    setPC(e: any) {
        this.setState({
            process_content: e.target.value,
        });
    }
    setREMARK(e: any) {
        this.setState({
            remark: e.target.value,
        });
    }
    backList() {
        this.props.history!.push(ROUTE_PATH.appFeedback);
    }
    save() {
        let { is_send, process_content, data_info, remark } = this.state;
        if (is_send) {
            message.info('请勿重复操作！');
            return;
        }
        if (!data_info.id) {
            message.info('找不到反馈数据！');
            return;
        }
        if (!process_content) {
            message.info('请输入处理记录回复！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let param: any = { id: data_info.id, process_content, user_id: data_info.user_id };
                if (remark) {
                    param['remark'] = remark;
                }
                request(this, AppServiceUtility.opinion_feedback_service.update_opinion_feedback!(param)!)
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.success('操作成功！', 1, () => {
                                this.backList();
                            });
                        } else {
                            if (typeof (datas) === 'string') {
                                message.error(datas);
                            } else {
                                message.error('操作失败！');
                            }
                        }
                    })
                    .catch((error: any) => {
                        console.error(error);
                    })
                    .finally(() => {
                        this.setState({
                            is_send: false,
                        });
                    });
            }
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        let { getFieldDecorator } = this.props.form;
        let { data_info } = this.state;
        return (
            <MainContent>
                <Card title="APP意见反馈">
                    <Row>
                        <Form {...formItemLayout}>
                            <MainContent>
                                <Form.Item label='姓名：'>
                                    {getFieldDecorator('name', {
                                        initialValue: data_info['name'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='手机号：'>
                                    {getFieldDecorator('telephone', {
                                        initialValue: data_info['telephone'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='反馈意见内容：'>
                                    {getFieldDecorator('content', {
                                        initialValue: data_info['content'] || '',
                                    })(
                                        <TextArea autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='反馈时间：'>
                                    {getFieldDecorator('create_date', {
                                        initialValue: data_info['create_date'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='处理记录回复：'>
                                    {getFieldDecorator('process_content', {
                                        initialValue: data_info['process_content'],
                                        rules: [{ required: true, message: "请输入处理记录回复", }],
                                    })(
                                        <TextArea onChange={(e: any) => this.setPC(e)} autoComplete="off" disabled={data_info.status === '已处理' ? true : false} />
                                    )}
                                </Form.Item>
                                <Form.Item label='备注：'>
                                    {getFieldDecorator('remark', {
                                        initialValue: data_info['remark'],
                                        rules: [{ required: false, message: "请输入备注", }],
                                    })(
                                        <TextArea onChange={(e: any) => this.setREMARK(e)} autoComplete="off" />
                                    )}
                                </Form.Item>
                            </MainContent>
                        </Form>
                    </Row>
                </Card>
                <Row type='flex' justify='center' className="ctrl-btns">
                    <Button onClick={() => history.back()} >返回</Button>
                    <Button onClick={() => this.save()} type="primary">保存</Button>
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：编辑APP意见反馈控件
 * @description 编辑APP意见反馈控件
 * @author
 */
@addon('ChangeAppFeedbackView', '编辑APP意见反馈控件', '编辑APP意见反馈控件')
@reactControl(Form.create<any>()(ChangeAppFeedbackView), true)
export class ChangeAppFeedbackViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}