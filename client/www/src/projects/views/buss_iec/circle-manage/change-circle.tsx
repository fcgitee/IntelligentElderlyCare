import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
/**
 * 组件：编辑圈子详情
 */
export interface ChangeCircleViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
}

/**
 * 组件：编辑圈子详情
 */
export default class ChangeCircleView extends ReactView<ChangeCircleViewControl, ChangeCircleViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        var edit_props = {
            form_items_props: [
                {
                    title: '圈子',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "圈子名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入圈子名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入圈子名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "创建人",
                            decorator_id: "creator_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入创建人" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入创建人",
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入描述",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.date,
                            label: "创建时间",
                            decorator_id: "create_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入创建时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入创建时间",
                                autocomplete: 'off',
                                showTime: true,
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.goBack();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.friends_circle_service,
                operation_option: {
                    save: {
                        func_name: "update_circle"
                    },
                    query: {
                        func_name: "get_circle_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑圈子详情
 * @description 编辑圈子详情
 * @author
 */
@addon('ChangeCircleView', '编辑圈子详情', '编辑圈子详情')
@reactControl(ChangeCircleView, true)
export class ChangeCircleViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}