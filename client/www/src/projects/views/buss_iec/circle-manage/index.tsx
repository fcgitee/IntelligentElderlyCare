
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form, Card, Row, Col, Input, Button, message } from 'antd';
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";

class MySecondForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }
    handleUpdateSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['name'] = values['name'];
                formValue['creator'] = values['creator'];
                formValue['description'] = values['description'];
                formValue['create_time'] = values['create_time'];
                request(this, AppServiceUtility.friends_circle_service.update_circle!(formValue))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                this.props.form.resetFields();
                                this.props.tableList && this.props.tableList.reflash && this.props.tableList.reflash();
                            });
                        }
                    })
                    .catch((error: any) => {
                        console.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let layout_personnel = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 8 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 16 },
                sm: { span: 20 },
            },
        };
        return (
            <Form {...layout_personnel} onSubmit={this.handleUpdateSubmit}>
                <Row gutter={24}>
                    <Col span={20}>
                        <Row>
                            <Col span={8}>
                                <Form.Item label='圈子名称'>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: "请输入圈子名称" }],
                                    })(
                                        <Input autoComplete="off" placeholder="请输入圈子名称" />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={16}>
                                <Form.Item label='描述'>
                                    {getFieldDecorator('description', {
                                        rules: [{ required: true, message: "请输入描述" }],
                                    })(
                                        <TextArea rows={4} placeholder="请输入描述" />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={4}>
                        <Row type='flex' justify='end' align="bottom" style={{ minHeight: '240px' }}>
                            <Button htmlType='submit' type='primary'>保存</Button>
                                    &nbsp;
                                    &nbsp;
                                    <Button onClick={() => { this.props.hide(); }} >取消</Button>
                        </Row>
                    </Col>
                </Row>
            </Form>
        );
    }
}
const Form2 = Form.create<any>()(MySecondForm);

/** 状态：圈子管理 */
export interface CircleViewState extends ReactViewState {
    add_permission?: boolean;
    is_show_add?: boolean;
}
/** 组件：圈子管理 */

export class CircleView extends React.Component<CircleViewControl, CircleViewState> {
    private tableList: any = null;
    private columns_data_source = [{
        title: "圈子名称",
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '描述',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '创建人',
        dataIndex: 'creator_name',
        key: 'creator_name',
    },
    ];
    constructor(props: CircleViewControl) {
        super(props);
        this.state = {
            add_permission: false,
            is_show_add: false,
        };
    }
    toDetail = (id: any) => {
        if (id) {
            this.props.history!.push(ROUTE_PATH.changeAppFriendCircle + '/' + id);
        }
    }
    componentDidMount = () => {
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: '圈子管理', p: '新增' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        add_permission: datas,
                    });
                }
            });
    }
    refreshTableList = () => {
        this.tableList && this.tableList.reflash && this.tableList.reflash();
    }
    onRef = (ref: any) => {
        this.tableList = ref;
    }
    add = () => {
        this.setState({
            is_show_add: true,
        });
    }
    hide = () => {
        this.setState({
            is_show_add: false,
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeCircle + '/' + contents.id);
        }
    }
    render() {
        // const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let circle = {
            type_show: false,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
            }],
            on_icon_click: this.onIconClick,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.friends_circle_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_circle'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let circle_list = Object.assign(circle, table_param);
        return (
            <MainContent>
                <Card title="圈子管理">
                    {this.state.add_permission && this.state.is_show_add ? <Form2 hide={this.hide} add={this.add} tableList={this.tableList} /> : null}
                    <SignFrameLayout {...circle_list} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：圈子管理控制器
 * @description 圈子管理
 * @author
 */
@addon('CircleView', '圈子管理', '圈子管理')
@reactControl(Form.create<any>()(CircleView), true)
export class CircleViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}