
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import './index.less';
/** 状态：账户明细列表视图 */
export interface AccountDetailsViewState extends ReactViewState {
    /** 接口名 */
    request_ulr?: string;
    /** 对话框 */
    model_show?: boolean;
}
/** 组件：账户明细列表视图 */
export class AccountDetailsView extends React.Component<AccountDetailsViewControl, AccountDetailsViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'user_info.name',
        key: 'user_info.user_name',
    }, {
        title: '身份证号',
        dataIndex: 'user_info.id_card',
        key: 'user_info.id_card',
    }, {
        title: '时间',
        dataIndex: 'order_create_date',
        key: 'order_create_date',
    }, {
        title: '摘要',
        dataIndex: 'record_info.abstract',
        key: 'personnel_classification',
    }, {
        title: '类型',
        dataIndex: 'record_info.is_clean',
        key: 'record_info.is_clean',
        render: (text: any, row: any, index: number) => {
            if (text) {
                return '非消费性扣除';
            } else {
                return '正常收支';
            }
        }
    }, {
        title: '金额（元）',
        dataIndex: 'record_info.amount',
        key: 'record_info.amount',
    }, {
        title: '余额',
        dataIndex: 'record_info.balance',
        key: 'record_info.balance',
    },
        // {
        //     title: '经办人',
        //     dataIndex: 'person_in_charge',
        //     key: 'person_in_charge',
        // }
    ];
    constructor(props: AccountDetailsViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            model_show: false,
        };
    }
    /** 批量充值 */
    backLast = () => {
        this.props.history!.goBack();
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // if ('icon_edit' === type) {
        //     this.props.history!.push(ROUTE_PATH.reserveBuyServiceEdit + '/' + contents.id);
        // }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        console.info(this.props.match!.params.key);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "date_range"
                },
            ],
            btn_props: [{
                label: '返回',
                btn_method: this.backLast,
                icon: ''
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.subsidy_account_recharg_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{ id: this.props.match!.params.key }, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: { id: this.props.match!.params.key }
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <SignFrameLayout {...allowance_read_list} />
        );
    }
}

/**
 * 控件：账户明细列表视图控制器
 * @description 账户明细列表视图
 * @author
 */
@addon(' AccountDetailsView', '账户明细列表视图', '账户明细列表视图')
@reactControl(AccountDetailsView, true)
export class AccountDetailsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}