import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Form, Input, Row, Button, Select } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { AppServiceUtility } from "src/projects/app/appService";
const { Option } = Select;
/**
 * 组件：账户清零表单状态
 */
export class AccountClearedFormState {
    personnel_classification_list?: any;
}

/**
 * 组件：账户清零表单
 * 账户清零表单
 */
export class AccountClearedForm extends React.Component<AccountClearedControl, AccountClearedFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
            personnel_classification_list: [],
        };
    }

    componentWillMount() {
        // 长者类型
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({})!
            .then(data => {
                this.setState({
                    personnel_classification_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });

    }
    formSub = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                this.props.formSubmit(err, values);
            }
        });

    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        const { getFieldDecorator } = this.props.form!;
        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.formSub}>
                    {
                        this.props.id ? ''
                            : (
                                <Form.Item label='长者类型'>
                                    {getFieldDecorator('personnel_classification_id', {
                                        initialValue: [],
                                        rules: [{
                                            required: true,
                                            message: '请选择长者类型'
                                        }],
                                    })(
                                        <Select placeholder="请选择长者类型" showSearch={true} mode="tags">
                                            {personnel_classification_list}
                                        </Select>
                                    )}
                                </Form.Item>
                            )
                    }
                    <Form.Item label='经办人'>
                        {getFieldDecorator('person_in_charge', {
                            // initialValue: baseData!['evaluator'] ? baseData!['evaluator'] : (CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '...'),
                            initialValue: this.props.login_user.name,
                            rules: [{
                                required: false,
                                message: '经办人'
                            }],
                        })(
                            <Input disabled={true} />
                        )}
                    </Form.Item>
                    <Form.Item label='备注'>
                        {getFieldDecorator('remark', {
                            initialValue: '',
                            rules: [{
                                required: true,
                                message: '请输入备注'
                            }],
                        })(
                            <TextArea />
                        )}
                    </Form.Item>
                    <Row type="flex" justify='center'>
                        <Button onClick={this.props.modelCancel} style={{ marginRight: '16px' }}>
                            取消
                        </Button>
                        <Button onClick={this.formSub}>
                            确定
                        </Button>
                    </Row>
                </Form>

            </div>
        );
    }
}

/**
 * 控件：账户清零表单控制器
 * 账户清零表单
 */
@addon('AccountClearedForm', '账户清零表单', '账户清零表单')
@reactControl(Form.create<any>()(AccountClearedForm))
export class AccountClearedControl extends BaseReactElementControl {
    /** 表单提交 */
    formSubmit?: any;
    /** 取消按钮 */
    modelCancel?: any;
    /** 用户id */
    user_id?: string;
    /** 登录用户 */
    login_user: any;

}
export default Form.create<any>()(AccountClearedForm);