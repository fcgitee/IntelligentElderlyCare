import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Form, Select, Input, Row, Button, InputNumber, } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { AppServiceUtility } from "src/projects/app/appService";
const { Option } = Select;
/**
 * 组件：批量充值表单表单状态
 */
export class BulkRechargeFormState {
    personnel_classification_list?: any;
}

/**
 * 组件：批量充值表单表单
 * 批量充值表单表单
 */
export class BulkRechargeForm extends React.Component<BulkRechargeFormControl, BulkRechargeFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
            personnel_classification_list: [],
        };
    }

    componentWillMount() {
        // 长者类型
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({ type: '政府补贴' })!
            .then(data => {
                this.setState({
                    personnel_classification_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });

    }
    formSub = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                this.props.formSubmit(err, values);
            }
        });

    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.formSub}>
                    <Form.Item label='长者类型'>
                        {getFieldDecorator('personnel_classification_id', {
                            initialValue: [],
                            rules: [{
                                required: true,
                                message: '请选择长者类型'
                            }],
                        })(
                            <Select placeholder="请选择长者类型" showSearch={true} mode="tags">
                                {personnel_classification_list}
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label='金额（元）'>
                        {getFieldDecorator('amount', {
                            // initialValue: baseData!['evaluator'] ? baseData!['evaluator'] : (CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '...'),
                            initialValue: '',
                            rules: [{
                                required: true,
                                message: '请输入金额'
                            }],
                        })(
                            <InputNumber style={{ width: '100%' }} />
                        )}
                    </Form.Item>
                    <Form.Item label='经办人'>
                        {getFieldDecorator('person_in_charge', {
                            // initialValue: baseData!['evaluator'] ? baseData!['evaluator'] : (CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '...'),
                            initialValue: this.props.login_user.name,
                            rules: [{
                                required: false,
                                message: '经办人'
                            }],
                        })(
                            <Input disabled={true} />
                        )}
                    </Form.Item>
                    <Form.Item label='备注'>
                        {getFieldDecorator('remark', {
                            initialValue: '',
                            rules: [{
                                required: false,
                                message: '请输入备注'
                            }],
                        })(
                            <TextArea />
                        )}
                    </Form.Item>
                    <Row type="flex" justify='center' >
                        <Button onClick={this.props.modelCancel} style={{ marginRight: '16px' }}>
                            取消
                        </Button>
                        <Button onClick={this.formSub}>
                            确定
                        </Button>
                    </Row>
                </Form>

            </div>
        );
    }
}

/**
 * 控件：批量充值表单表单控制器
 * 批量充值表单表单
 */
@addon('BulkRechargeForm', '批量充值表单表单', '批量充值表单表单')
@reactControl(Form.create<any>()(BulkRechargeForm))
export class BulkRechargeFormControl extends BaseReactElementControl {
    /** 表单提交 */
    formSubmit?: any;
    /** 取消按钮 */
    modelCancel?: any;
    /** 登录用户 */
    login_user: any;

}
export default Form.create<any>()(BulkRechargeForm);