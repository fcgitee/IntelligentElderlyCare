
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：行为能力评估项目视图 */
export interface AssessmentProjectViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：行为能力评估项目视图 */
export class AssessmentProjectView extends React.Component<AssessmentProjectViewControl, AssessmentProjectViewState> {
    private columns_data_source = [
        //     {
        //     title: '状态',
        //     dataIndex: 'state',
        //     key: 'state',
        // }, 
        {
            title: '项目名称',
            dataIndex: 'project_name',
            key: 'project_name',
        },
        {
            title: '项目描述',
            dataIndex: 'describe',
            key: 'describe',
        },
        {
            title: '记分方式',
            dataIndex: 'scoring_method',
            key: 'scoring_method',
            render(text: any, record: any) {
                if (text === '1') {
                    return '不计分';
                } else if (text === '2') {
                    return '累计';
                } else if (text === '3') {
                    return '最大值';
                } else if (text === '4') {
                    return '最小值';
                }
                return '';
            }
        },
        {
            title: '修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        },
        {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
        {
            title: '组织机构',
            dataIndex: 'org_name',
            key: 'org_name',
        },
    ];
    private hucolumns_data_source = [
        {
            title: '护理名称',
            dataIndex: 'project_name',
            key: 'project_name',
        }, {
            title: '创建日期',
            dataIndex: 'create_date',
            key: 'create_date',
        },];
    public text = "";
    constructor(props: AssessmentProjectViewControl) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        if (this.props.match!.url.indexOf("护理") !== -1) {
            this.props.history!.push(ROUTE_PATH.changeAssessmentProject + '/护理');
        } else {
            this.props.history!.push(ROUTE_PATH.changeAssessmentProject);
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeAssessmentProject + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        if (this.props.match!.url.indexOf("护理") !== -1) {
            this.text = '护理';
        }
        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: this.text ? '护理名称' : "项目名称",
                    decorator_id: "project_name"
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name"
                // }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            onsucess_func: (data: any) => {
                // console.log(this.text, 8888888);
                if (this.props.match!.url.indexOf("护理") !== -1) {
                    this.text = '护理';
                }
            },
            // search_cb: this.queryBtn,
            columns_data_source: this.text ? this.hucolumns_data_source : this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.assessment_project_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: this.props.del_url
                }
            },
            searchExtraParam: this.text ? { "selectlist": ["1", "4", "3"] } : "",
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        if (!this.text) {
            assessment_project.edit_form_items_props.push(
                {
                    type: InputType.input,
                    label: "评估类型",
                    decorator_id: "assessment_type"
                },
                {
                    type: InputType.rangePicker,
                    label: "创建日期",
                    decorator_id: "date_range"
                }
            );
        }
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：行为能力评估项目视图控制器
 * @description 行为能力评估项目视图
 * @author
 */
@addon('AssessmentProjectView', '行为能力评估项目视图', '行为能力评估项目视图')
@reactControl(AssessmentProjectView, true)
export class AssessmentProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public del_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}