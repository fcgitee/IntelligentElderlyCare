import { Select, message, Button, Modal } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { IAssessmentTemplateService } from "src/projects/models/assessment-template";
import { ROUTE_PATH } from 'src/projects/router';
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { InputType } from "src/business/components/buss-components/sign-frame-layout";

const { Option } = Select;
/** 模板状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：编辑行为能力评估模板状态
 */
export interface ChangeAssessmentTemplateViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
    /** 评估模板列表 */
    template_list?: any[];
    /** 用户选择的模板类型 */
    templatestase?: string;
    /** 用户选择的类型 */
    typevalue?: string;
    /** 是否是大模板 */
    templatestate?: boolean;
    /** 弹框显示状态 */
    modal_show?: any;
    /** 模板id */
    template_id?: any;
    /** 是否导入 */
    import?: any;
    is_send?: boolean;
}

/**
 * 组件：编辑行为能力评估模板视图
 */
export class ChangeAssessmentTemplateView extends ReactView<ChangeAssessmentTemplateViewControl, ChangeAssessmentTemplateViewState> {
    /** 编辑的数据 */
    private columns_data_source_search = [{
        title: '模板名称',
        dataIndex: 'template_name',
        key: 'template_name',
    }, {
        title: '描述',
        dataIndex: 'describe',
        key: 'describe',
    }, {
        title: '模板类型',
        dataIndex: 'template_type',
        key: 'template_type',
    }];
    public changedata?: object = {};
    public pagestatus?: Boolean = false;
    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            typevalue: '',
            templatestase: '',
            templatestate: false,
            modal_show: false,
            template_id: '',
            import: false,
            is_send: false,
        };
    }
    /** 权限服务 */
    assessmentTemplateServer?() {
        return getObject(this.props.assessmentTemplateServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.assessmentTemplate);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        console.log('报错？？？', value);
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                if (this.props.match!.params.key && this.props.match!.params.key.split('&')[1]) {
                    delete value.id;
                }
                if (!this.props.match!.params.key && this.props.match!.params.key !== '护理') {
                    value['state'] = SRAR_TUSING;
                } else if (this.props.match!.params.key && this.props.match!.params.key !== '护理') {
                    value['id'] = this.props.match!.params.key.split('&')[0];
                } else {
                    delete value.id;
                }
                if (this.state.templatestate === false) {
                    if (!value['dataSource'] || value['dataSource'].length === 0) {
                        message.error('请至少添加一个评估项目！');
                        this.setState({
                            is_send: false
                        });
                        return;
                    }
                } else {
                    if (!value['templateSource'] || value['templateSource'].length === 0) {
                        message.error('请至少添加一个评估模板！');
                        this.setState({
                            is_send: false
                        });
                        return;
                    }
                }
                /** 大模板 */
                if (value.type && value.type === '评估模板') {
                    let count: number = 0;
                    value.templateSource.forEach((item: any, index: number) => {
                        AppServiceUtility.assessment_template_service.get_assessment_template_list!({ id: item.assessment_template })!
                            .then((data: any) => {
                                value.templateSource[index]['template_list'] = data.result;
                            });
                    });
                    let settime = setInterval(
                        () => {
                            for (let i = 0; i < value.templateSource.length; i++) {
                                if (value.templateSource[i]['template_list'] && value.templateSource[i]['template_list'].length > 0 && i === value.templateSource.length - 1) {
                                    count = 1;
                                }
                            }
                            if (count) {
                                clearInterval(settime);
                                this.assessmentTemplateServer!()!.update_assessment_template!(value)!
                                    .then((data: any) => {
                                        if (data === 'Success') {
                                            message.info('保存成功！', 1, () => {
                                                history.back();
                                            });
                                        } else {
                                            this.setState({
                                                is_send: false
                                            });
                                        }
                                    }).catch((err: any) => {
                                        console.error(err);
                                        this.setState({
                                            is_send: false
                                        });
                                    });
                            }
                        }
                        ,
                        200
                    );
                } else {
                    this.assessmentTemplateServer!()!.update_assessment_template!(value)!
                        .then((data: any) => {
                            if (data === 'Success') {
                                message.info('保存成功！', 1, () => {
                                    history.back();
                                });
                            } else {
                                this.setState({
                                    is_send: false
                                });
                            }
                        }).catch((err: any) => {
                            console.error(err);
                            this.setState({
                                is_send: false
                            });
                        });
                }
            }
        );
    }
    /** 返回按钮回调 */
    back = (value: any) => {
        history.back();
    }
    componentDidMount() {
        AppServiceUtility.assessment_project_service.get_assessment_project_list!({})!
            .then((data: any) => {
                let project_list: any[];
                project_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.project_name}</Option>;
                });
                this.setState(
                    {
                        project_list
                    },
                    () => {
                        this.forceUpdate();
                    }
                );
            });
        AppServiceUtility.assessment_template_service.get_assessment_template_list!({})!
            .then((data: any) => {
                let template_list: any[];
                template_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.template_name}</Option>;
                });
                this.setState({
                    template_list
                });
            });
        if (this.props.match!.params.key) {
            // console.log('cahuncan????', this.props.match!.params.key.split('&')[0], this.props.match!.params.key.split('&')[1], this.props.match!.params.key.split('&'), this.props.match!.params.key);
            if (this.props.match!.params.key.split('&')[0] !== '护理') {
                this.setState(
                    {
                        selete_obj: {
                            id: this.props.match!.params.key.split('&')[0],
                            is_edit: true
                        }
                    },
                    () => {
                        this.forceUpdate();
                    }
                );
            }
        }
    }
    onchangeasslist = (e: string) => {
        this.setState({ templatestase: e }, () => {
            this.forceUpdate();
        });
    }
    readys = (value: any) => {
        if (this.props.match!.params.key && this.props.match!.params.key !== '护理' && !this.state.templatestate && !this.pagestatus) {
            let data = value[0];
            this.changedata = data;
            if (data.type === '评估模板') {
                this.setState({ typevalue: '评估模板', templatestate: true, templatestase: data.template_type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            } else if (!data.type && data.template_type === '详细能力评估') {
                this.setState({ templatestase: data.template_type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            } else if (data.type && data.template_type) {
                this.setState({ templatestase: data.template_type, typevalue: data.type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            }
        }
    }

    handleClick = () => {
        this.setState({
            modal_show: true
        });
    }

    checkTemplate = (value: any) => {
        this.setState({
            template_id: value
        });
    }

    handleOk = () => {
        this.setState({
            import: true
        });
        let param = this.state.template_id + '&' + 'import';
        this.props.history!.push(ROUTE_PATH.changeAssessmentTemplate + '/' + param);
    }
    render() {
        let modal_search_items_props = {
            // type: 'input',
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "模板名称",
                    decorator_id: "template_name"
                }
            ],
            change_method: this.checkTemplate,
            type: 'template',
            columns_data_source: this.columns_data_source_search,
            service_name: AppServiceUtility.assessment_template_service,
            service_func: 'get_assessment_template_list',
            title: '评估模板查询',
            select_option: {
                placeholder: "请选择模板",
            }
        };
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let columns_data_source;
        if (this.props.match!.params.key === '护理') {
            columns_data_source = [{
                title: `护理项目`,
                dataIndex: 'assessment_projects',
                key: 'assessment_projects',
                type: 'select' as ColTypeStr,
                option: {
                    placeholder: `请选择护理项目`,
                    childrens: this.state.project_list
                }
            }];
        } else {
            if (this.props.match!.params.key && this.state.templatestate) {
                if (this.state.typevalue !== '评估模板') {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估项目`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估项目`,
                            childrens: this.state.project_list
                        }
                    },
                        // {
                        //     title: '记分方式',
                        //     dataIndex: 'scoring_method',
                        //     key: 'scoring_method',
                        //     type: 'input' as ColTypeStr
                        // }
                    ];
                } else if (this.state.templatestase === '详细能力评估') {
                    columns_data_source = [{
                        title: '子模板名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估模板`,
                        dataIndex: 'assessment_template',
                        key: 'evaluation_template',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估模板`,
                            childrens: this.state.template_list
                        }
                    }];
                } else {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估项目`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估项目`,
                            childrens: this.state.project_list
                        }
                    },
                        // {
                        //     title: '记分方式',
                        //     dataIndex: 'scoring_method',
                        //     key: 'scoring_method',
                        //     type: 'input' as ColTypeStr
                        // }
                    ];
                }
            } else if (this.props.match!.params.key && !this.state.templatestate) {
                columns_data_source = [{
                    title: '分类名称',
                    dataIndex: 'category_name',
                    key: 'category_name',
                    type: 'input' as ColTypeStr
                }, {
                    title: `评估项目`,
                    dataIndex: 'assessment_projects',
                    key: 'assessment_projects',
                    type: 'select' as ColTypeStr,
                    option: {
                        placeholder: `请选择评估项目`,
                        childrens: this.state.project_list
                    }
                },
                    // {
                    //     title: '记分方式',
                    //     dataIndex: 'scoring_method',
                    //     key: 'scoring_method',
                    //     type: 'input' as ColTypeStr
                    // }
                ];
            } else {
                if (this.state.typevalue !== '评估模板') {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估项目`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估项目`,
                            childrens: this.state.project_list
                        }
                    },
                        // {
                        //     title: '记分方式',
                        //     dataIndex: 'scoring_method',
                        //     key: 'scoring_method',
                        //     type: 'input' as ColTypeStr
                        // }
                    ];
                } else if (this.state.templatestase === '详细能力评估' && this.state.typevalue === '评估模板') {
                    columns_data_source = [{
                        title: '子模板名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估模板`,
                        dataIndex: 'assessment_template',
                        key: 'evaluation_template',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估模板`,
                            childrens: this.state.template_list
                        }
                    }];
                } else {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估项目`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估项目`,
                            childrens: this.state.project_list
                        }
                    },
                        // {
                        //     title: '记分方式',
                        //     dataIndex: 'scoring_method',
                        //     key: 'scoring_method',
                        //     type: 'input' as ColTypeStr
                        // }
                    ];
                }
            }

        }
        const assessment_list: JSX.Element[] = [<Option key='初步能力评估'>初步能力评估</Option>, <Option key='详细能力评估'>详细能力评估</Option>, <Option key='补贴评估'>补贴评估</Option>, <Option key='护理评估'>护理评估</Option>, <Option key='慈善评估'>慈善评估</Option>];
        const list: JSX.Element[] = [<Option key='评估项目'>评估项目</Option>, <Option key='评估模板'>评估模板</Option>];
        let user_manege: object;
        if (this.props.match!.params.key === '护理') {
            user_manege = {
                form_items_props: [
                    {
                        type: InputTypeTable.input,
                        label: "模板名称",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入模板名称", }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "template_name",
                    },
                    {
                        type: InputTypeTable.select,
                        label: "模板类型",
                        field_decorator_option: {
                            initialValue: '护理评估'
                        } as GetFieldDecoratorOptions,
                        decorator_id: "template_type",
                        option: {
                            disabled: true,
                            childrens: assessment_list
                        },
                    },
                    {
                        type: InputTypeTable.text_area,
                        label: "描述",
                        field_decorator_option: {
                            rules: [{ required: false, message: "" }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "describe",
                    }
                ],
                submit_props: {
                    text: "保存",
                    cb: this.save_value,
                    btn_other_props: { disabled: this.state.is_send }
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: this.back,
                        btn_other_props: { disabled: this.state.is_send }
                    }
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                add_row_text: `添加护理项目`,
                table_field_name: 'dataSource',
                // table配置
                columns_data_source: columns_data_source,
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_template_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_template_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                        // save: {
                        //     func_name: "update_service_type"
                        // }
                    },
                },
                succ_func: () => { this.props.history!.push(ROUTE_PATH.assessmentTemplate); },
                id: this.props.match!.params.key
            };
        } else {
            user_manege = {
                submit_props: {
                    text: "保存",
                    cb: this.save_value,
                    btn_other_props: { disabled: this.state.is_send }
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: this.back,
                        btn_other_props: { disabled: this.state.is_send }
                    }
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                add_row_text: `添加${this.state.typevalue === '评估模板' ? '子模板' : '评估项目'}`,
                // table配置
                columns_data_source: columns_data_source,
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_template_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_template_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                        // save: {
                        //     func_name: "update_service_type"
                        // }
                    },
                },
                succ_func: () => { this.props.history!.push(ROUTE_PATH.assessmentTemplate); },
                id: this.props.match!.params.key
            };
            if (this.props.match!.params.key && this.state.templatestate) {
                if (this.state.typevalue !== '评估模板') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else {
                    user_manege['table_field_name'] = 'templateSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.select,
                            label: "类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择类型", }]
                            } as GetFieldDecoratorOptions,
                            decorator_id: "type",
                            option: {
                                childrens: list,
                                onChange: (e: string) => {
                                    this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                        this.forceUpdate();
                                    });
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            } else if (this.props.match!.params.key && !this.state.templatestate) {
                if (this.state.templatestase !== '详细能力评估') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.select,
                            label: "类型",
                            field_decorator_option: {
                                initialValue: this.changedata!['type'] ? this.changedata!['type'] : '评估项目',
                                rules: [{ required: true, message: "请选择类型", }]
                            } as GetFieldDecoratorOptions,
                            decorator_id: "type",
                            option: {
                                childrens: list,
                                onChange: (e: string) => {
                                    this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                        this.forceUpdate();
                                    });
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            } else {
                if (this.state.templatestase !== '详细能力评估') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else if (this.state.templatestase === '详细能力评估') {
                    if (this.state.typevalue === '评估模板') {
                        user_manege['table_field_name'] = 'templateSource';
                        user_manege['form_items_props'] = [
                            {
                                type: InputTypeTable.input,
                                label: "模板名称",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请输入模板名称", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_name",
                            },
                            {
                                type: InputTypeTable.select,
                                label: "模板类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择模板类型", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_type",
                                option: {
                                    childrens: assessment_list,
                                    onChange: (e: string) => {
                                        this.onchangeasslist(e);
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.select,
                                label: "类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择类型", }],
                                    initialValue: this.state.typevalue ? this.state.typevalue : '评估项目'
                                } as GetFieldDecoratorOptions,
                                decorator_id: "type",
                                option: {
                                    childrens: list,
                                    onChange: (e: string) => {
                                        this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                            this.forceUpdate();
                                        });
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.text_area,
                                label: "描述",
                                field_decorator_option: {
                                    rules: [{ required: false, message: "" }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "describe",
                            }
                        ];
                    } else {
                        user_manege['table_field_name'] = 'dataSource';
                        user_manege['form_items_props'] = [
                            {
                                type: InputTypeTable.input,
                                label: "模板名称",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请输入模板名称", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_name",
                            },
                            {
                                type: InputTypeTable.select,
                                label: "模板类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择模板类型", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_type",
                                option: {
                                    childrens: assessment_list,
                                    onChange: (e: string) => {
                                        this.onchangeasslist(e);
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.select,
                                label: "类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择类型", }],
                                    initialValue: this.state.typevalue ? this.state.typevalue : '评估项目'
                                } as GetFieldDecoratorOptions,
                                decorator_id: "type",
                                option: {
                                    childrens: list,
                                    onChange: (e: string) => {
                                        this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                            this.forceUpdate();
                                        });
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.text_area,
                                label: "描述",
                                field_decorator_option: {
                                    rules: [{ required: false, message: "" }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "describe",
                            }
                        ];
                    }
                } else {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "模板名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入模板名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "模板类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择模板类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            }
        }
        return (
            <div>
                <Button type='primary' style={{ position: 'absolute', top: '120px', right: '70px', zIndex: 999 }} onClick={this.handleClick}>导入参考模板</Button>
                <TableList {...user_manege} readys={this.readys} />
                <Modal title="选择模板" visible={this.state.modal_show} onOk={this.handleOk} onCancel={() => this.setState({ modal_show: false })} width={'800px'}>
                    <ModalSearch modal_search_items_props={modal_search_items_props} />
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：编辑行为能力评估模板编辑控件
 * @description 编辑行为能力评估模板编辑控件
 * @author
 */
@addon('ChangeAssessmentTemplateView', '编辑行为能力评估模板编辑控件', '编辑行为能力评估模板编辑控件')
@reactControl(ChangeAssessmentTemplateView, true)
export class ChangeAssessmentTemplateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估模板服务 */
    public assessmentTemplateServer_Fac?: Ref<IAssessmentTemplateService>;

}