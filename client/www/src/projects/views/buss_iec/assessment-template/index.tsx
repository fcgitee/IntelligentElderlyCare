
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：行为能力评估模板视图 */
export interface AssessmentTemplateViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：行为能力评估模板视图 */
export class AssessmentTemplateView extends React.Component<AssessmentTemplateViewControl, AssessmentTemplateViewState> {
    private columns_data_source = [
        //     {
        //     title: '状态',
        //     dataIndex: 'state',
        //     key: 'state',
        // }, 
        {
            title: '模板名称',
            dataIndex: 'template_name',
            key: 'template_name',
        }, {
            title: '描述',
            dataIndex: 'describe',
            key: 'describe',
            width: 500,
        }, {
            title: '模板类型',
            dataIndex: 'template_type',
            key: 'template_type',
        },
        {
            title: '组织机构',
            dataIndex: 'org_name',
            key: 'org_name',
        },];
    constructor(props: AssessmentTemplateViewControl) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        if (this.props.match!.params.key === '护理') {
            this.props.history!.push(ROUTE_PATH.changeAssessmentTemplate + '/' + this.props.match!.params.key);
        } else {
            this.props.history!.push(ROUTE_PATH.changeAssessmentTemplate);
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeAssessmentTemplate + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_template;
        if (this.props.match!.params.key === '护理') {
            assessment_template = {
                type_show: false,
                edit_form_items_props: [
                    {
                        type: InputType.input,
                        label: "模板名称",
                        decorator_id: "template_name"
                    }
                ],
                btn_props: [{
                    label: '新增护理模板',
                    btn_method: this.addBehavioralCompetenceAssessment,
                    icon: 'plus'
                }],
                // search_cb: this.queryBtn,
                columns_data_source: this.columns_data_source,
                on_icon_click: this.onIconClick,
                service_object: AppServiceUtility.assessment_template_service,
                service_option: {
                    select: {
                        service_func: this.state.request_url,
                        service_condition: [{ template_type: '护理评估' }, 1, 10]
                    },
                    delete: {
                        service_func: 'delete_assessment_template'
                    }
                },
                permission_class: this.props.permission_class,
                get_permission_name: this.props.get_permission_name,
                select_permission: this.props.select_permission,
                edit_permission: this.props.edit_permission,
                add_permission: this.props.add_permission,
                delete_permission: this.props.delete_permission,
            };
        } else {
            assessment_template = {
                type_show: false,
                edit_form_items_props: [
                    {
                        type: InputType.input,
                        label: "模板名称",
                        decorator_id: "template_name"
                    },
                    {
                        type: InputType.tree_select,
                        label: "组织机构",
                        col_span: 8,
                        decorator_id: "org_name",
                        option: {
                            showSearch: true,
                            treeNodeFilterProp: 'title',
                            allowClear: true,
                            dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                            treeDefaultExpandAll: true,
                            treeData: this.state.org_list,
                            placeholder: "请选择组织机构",
                        },
                    },
                    // {
                    //     type: InputType.input,
                    //     label: "组织机构",
                    //     decorator_id: "org_name"
                    // }
                ],
                btn_props: [{
                    label: '新增评估模板',
                    btn_method: this.addBehavioralCompetenceAssessment,
                    icon: 'plus'
                }],
                // search_cb: this.queryBtn,
                columns_data_source: this.columns_data_source,
                on_icon_click: this.onIconClick,
                service_object: AppServiceUtility.assessment_template_service,
                service_option: {
                    select: {
                        service_func: this.state.request_url,
                        service_condition: [{}, 1, 10]
                    },
                    delete: {
                        service_func: 'delete_assessment_template'
                    }
                },
                permission_class: this.props.permission_class,
                get_permission_name: this.props.get_permission_name,
                select_permission: this.props.select_permission,
                edit_permission: this.props.edit_permission,
                add_permission: this.props.add_permission,
                delete_permission: this.props.delete_permission,
            };
        }
        let behavioral_competence_assessment_list = Object.assign(assessment_template, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：行为能力评估项目视图控制器
 * @description 行为能力评估项目视图
 * @author
 */
@addon('AssessmentTemplateView', '行为能力评估项目视图', '行为能力评估项目视图')
@reactControl(AssessmentTemplateView, true)
export class AssessmentTemplateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}