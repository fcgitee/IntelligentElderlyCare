
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form, Button, Select, message, Modal, Row, Col } from 'antd';
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import TextArea from "antd/lib/input/TextArea";
let { Option } = Select;
// let { Option } = Select;
/** 状态：APP在线咨询 */
export interface AppOnlineTalkViewState extends ReactViewState {
    table_column?: any;
    change_permission?: boolean;
    is_send: boolean;
    is_show: boolean;
    remark: string;
    selected_id: string;
}
/** 组件：APP在线咨询 */

export class AppOnlineTalkView extends React.Component<AppOnlineTalkViewControl, AppOnlineTalkViewState> {
    private formCreator: any = null;
    constructor(props: AppOnlineTalkViewControl) {
        super(props);
        this.state = {
            table_column: [{
                title: "用户/手机号",
                dataIndex: 'telephone',
                key: 'telephone',
                render: (text: any, record: any) => {
                    if (!text) {
                        return '游客';
                    } else {
                        return text;
                    }
                }
            },
            {
                title: '姓名',
                dataIndex: 'ask_name',
                key: 'ask_name',
                render: (text: any, record: any) => {
                    if (!text) {
                        return '游客';
                    } else {
                        return text;
                    }
                }
            },
            {
                title: '信息状态',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: '客服人员',
                dataIndex: 'worker_name',
                key: 'worker_name',
            },
            {
                title: '最后回复时间',
                dataIndex: 'last_answer_date',
                key: 'last_answer_date',
            },
            {
                title: '咨询时间',
                dataIndex: 'create_date',
                key: 'create_date',
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
            }
            ],
            change_permission: false,
            is_send: false,
            is_show: false,
            remark: '',
            selected_id: '',
        };
    }
    toDetail = (id: any) => {
        if (id) {
            this.props.history!.push(ROUTE_PATH.changeAppOnlineTalk + '/' + id);
        }
    }
    openRemark = (record: any) => {
        this.setState({
            is_show: true,
            selected_id: record.id,
            remark: record.remark,
        });
    }
    closeRemark = () => {
        this.setState({
            is_show: false,
        });
    }
    addRemark = () => {
        const { is_send, remark, selected_id } = this.state;
        if (is_send === true) {
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                // 判断权限
                request(this, AppServiceUtility.message_service.update_app_online_message_remark!({ id: selected_id, remark: remark })!)
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.success('操作成功！', 1, () => {
                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                            });
                        }
                    }).catch((err: any) => {
                        console.error(err);
                    }).finally(() => {
                        this.setState({
                            is_send: false,
                            is_show: false,
                            remark: '',
                            selected_id: '',
                        });
                    });
            }
        );
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    componentDidMount = () => {
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: 'APP在线咨询', p: '编辑' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.state.table_column.push({
                        title: '操作',
                        dataIndex: 'action2',
                        key: 'action2',
                        render: (text: any, record: any) => {
                            return (
                                <div>
                                    <Button size="small" type="primary" onClick={() => { this.openRemark(record); }}>备注</Button>
                                    &nbsp;
                                    &nbsp;
                                    <Button size="small" type="primary" onClick={() => { this.toDetail(record.user_id); }}>回复</Button>
                                </div>
                            );
                        }
                    });
                    this.setState({
                        change_permission: datas,
                    });
                }
            });
    }
    // 输入备注事件
    remarkChange = (e: any) => {
        this.setState({
            remark: e.target.value
        });
    }
    render() {
        // const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {
            is_new: true,
        };
        let app_online_talk = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "信息状态",
                    decorator_id: "status",
                    option: {
                        placeholder: '请选择信息状态',
                        childrens: ['未读', '已读'].map((item, index: number) => {
                            return (
                                <Option key={item}>{item}</Option>
                            );
                        }),
                    },
                },
                {
                    type: InputType.rangePicker,
                    label: "咨询时间",
                    decorator_id: "date_range",
                    option: {
                        placeholder: ['开始时间', '结束时间'],
                    },
                },
            ],
            columns_data_source: this.state.table_column,
            service_object: AppServiceUtility.message_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
            onRef: this.onRef,
        };
        let app_online_talk_list = Object.assign(app_online_talk, table_param);
        const { is_show, remark } = this.state;
        return (
            <div>
                <SignFrameLayout {...app_online_talk_list} />
                <Modal
                    title="编辑备注"
                    cancelText='取消'
                    okText='确认'
                    visible={is_show}
                    onOk={this.addRemark}
                    onCancel={this.closeRemark}
                    width='400px'
                >
                    <Row type="flex" align="middle">
                        <Col span={6}>备注：</Col>
                        <Col span={18}>
                            <TextArea rows={4} onChange={this.remarkChange} value={remark} />
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：APP在线咨询控制器
 * @description APP在线咨询
 * @author
 */
@addon('AppOnlineTalkView', 'APP在线咨询', 'APP在线咨询')
@reactControl(Form.create<any>()(AppOnlineTalkView), true)
export class AppOnlineTalkViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}