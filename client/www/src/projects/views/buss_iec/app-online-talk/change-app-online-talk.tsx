import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Row, Card, Button } from "antd";
import { request } from "src/business/util_tool";
import TextArea from "antd/lib/input/TextArea";
import "./index.less";

/**
 * 组件：编辑APP在线咨询状态
 */
export interface ChangeAppOnlineTalkViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 数据
    data_list?: any;
    // 消息
    textareaValue?: any;
    // 当前页码
    count: number;
    // 总数
    total: number;
    // 首次进入
    isFirst?: boolean;
}

/**
 * 组件：编辑APP在线咨询视图
 */
export class ChangeAppOnlineTalkView extends ReactView<ChangeAppOnlineTalkViewControl, ChangeAppOnlineTalkViewState> {
    private stoTimer: any = null;
    private step: number = 5;
    constructor(props: any) {
        super(props);
        this.state = {
            data_list: [],
            id: '',
            textareaValue: '',
            count: this.step,
            total: 0,
            isFirst: true,
        };
    }
    componentDidMount = () => {
        this.getList(true);
    }
    getList(type: any = undefined) {
        let isFirst = this.state.isFirst;
        let param: any = { user_id: this.props.match!.params.key };
        if (isFirst) {
            param['check_unread'] = true;
            this.setState({
                isFirst: false,
            });
        }
        request(this, AppServiceUtility.message_service2.get_app_online_message_list!(param, 1, this.state.count)!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    let count = this.state.count;
                    let total = this.state.total;
                    this.setState(
                        {
                            data_list: datas.result.reverse(),
                            count: datas.isRewrite ? datas.total : count,
                            total: datas.total > total ? datas.total : total,
                        },
                        () => {
                            this.sto(8000);
                            if (type === true || (type === undefined && datas.total > total)) {
                                this.autoScroll(9999);
                            } else if (type === false) {
                                this.autoScroll(0);
                            }
                        }
                    );
                }
            });
    }
    backList() {
        this.props.history!.push(ROUTE_PATH.appOnlineTalk);
    }
    sto(times: number) {
        this.stoTimer = window.setTimeout(
            () => {
                this.getList();
            },
            times
        );
    }
    componentWillUnmount() {
        if (this.stoTimer) {
            window.clearTimeout(this.stoTimer);
        }
    }
    changeTextareaValue = (e: any) => {
        this.setState({
            textareaValue: e.target.value,
        });
    }
    autoScroll(height: number) {
        document.querySelector('#ob-list')!.scrollTop = height;
    }
    getMore = () => {
        let count = this.state.count;
        this.setState(
            {
                count: count + this.step,
            },
            () => {
                this.getList(false);
            }
        );
    }
    sendMsg = (e: any) => {
        let textareaValue = this.state.textareaValue;
        if (textareaValue) {
            request(this, AppServiceUtility.message_service.update_app_online_message!({ content: textareaValue, user_id: this.props.match!.params.key })!)
                .then((data: any) => {
                    if (data && data.code && data.code === 'Success') {
                        let count = this.state.count;
                        this.setState(
                            {
                                textareaValue: '',
                                count: count + 1,
                            },
                            () => {
                                this.getList(true);
                            }
                        );
                    }
                });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const data_list = this.state.data_list;
        return (
            <MainContent>
                <Card title='在线咨询'>
                    <Row className="ob-main">
                        <Row type="flex" justify="center">
                            <Button onClick={this.getMore} className="ob-more" type="primary">查看更多消息</Button>
                        </Row>
                        <Row className="ob-list" id="ob-list">
                            {data_list && data_list.length ? data_list.map((item: any, index: number) => {
                                return (
                                    <Row key={item.id} className={`ob-section`}>
                                        <Row type="flex" justify={item.type === 'request' ? 'start' : 'end'} className={`ob-info ${item.type === 'request' ? '' : 'even'}`}>
                                            <Row className="ob-user">{item.name}</Row>
                                            <Row className="ob-date">{item.create_date}</Row>
                                        </Row>
                                        <Row className="ob-content" type="flex" justify={item.type === 'request' ? 'start' : 'end'}>
                                            <span>{item.content}</span>
                                        </Row>
                                    </Row>
                                );
                            }) : null}
                        </Row>
                        <Row className="ob-write">
                            <TextArea onChange={this.changeTextareaValue} value={this.state.textareaValue} className="ob-textarea" rows={6} placeholder="请输入消息" />
                            <Button onClick={this.sendMsg} className="ob-send" type="primary">发送</Button>
                        </Row>
                    </Row>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：编辑APP在线咨询控件
 * @description 编辑APP在线咨询控件
 * @author
 */
@addon('ChangeAppOnlineTalkView', '编辑APP在线咨询控件', '编辑APP在线咨询控件')
@reactControl(Form.create<any>()(ChangeAppOnlineTalkView), true)
export class ChangeAppOnlineTalkViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}