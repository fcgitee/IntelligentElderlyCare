
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：行为能力评估项目视图 */
export interface BehavioralCompetenceAssessmentViewState extends ReactViewState {

}
/** 组件：行为能力评估项目视图 */
export class BehavioralCompetenceAssessmentView extends React.Component<BehavioralCompetenceAssessmentViewControl, BehavioralCompetenceAssessmentViewState> {
    private columns_data_source = [{
        title: '项目名称',
        dataIndex: 'project_name',
        key: 'project_name',
    }, {
        title: '一级类别',
        dataIndex: 'category_i',
        key: 'category_i',
    }, {
        title: '二级类别',
        dataIndex: 'category_ii',
        key: 'category_ii',
    },];
    constructor(props: BehavioralCompetenceAssessmentViewControl) {
        super(props);
        this.state = {
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.changeBehavioralCompetenceAssessment);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeBehavioralCompetenceAssessment + '/' + contents.id);
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let behavioral_competence_assessment = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "项目名称",
                    decorator_id: "name"
                },
            ],
            btn_props: [{
                label: '新增评估项目',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.behavioral_competence_assessment_service,
            service_option: {
                select: {
                    service_func: 'get_project_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_project'
                }
            },
        };
        let behavioral_competence_assessment_list = Object.assign(behavioral_competence_assessment, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：行为能力评估项目视图控制器
 * @description 行为能力评估项目视图
 * @author
 */
@addon('BehavioralCompetenceAssessmentView', '行为能力评估项目视图', '行为能力评估项目视图')
@reactControl(BehavioralCompetenceAssessmentView, true)
export class BehavioralCompetenceAssessmentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}