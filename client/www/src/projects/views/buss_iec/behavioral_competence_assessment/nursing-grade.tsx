import { Row, Collapse } from "antd";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { edit_props_info } from "src/projects/app/util-tool";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType as InputTypeNew } from "src/business/components/buss-components/form-creator";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
const { Panel } = Collapse;
/**
 * 组件：照护等级状态
 */
export interface NursingGradeViewState extends ReactViewState {

    /** 接口名 */
    request_url?: string;
    /** 数据 */
    data_base: any;
    add_permission?: boolean;
}

/**
 * 组件：照护等级
 */
export class NursingGradeView extends ReactView<NursingGradeViewControl, NursingGradeViewState> {
    // 列表显示项
    private columns_data_source = [{
        title: '照护等级',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '描述',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            data_base: {},
        };
    }
    componentWillMount() {
        this.setState({
            request_url: this.props.request_url
        });
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: '护理等级', p: '新增' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        add_permission: datas,
                    });
                }
            });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.nursingGrade);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        'icon_edit' === type && this.props.history!.push(ROUTE_PATH.nursingGrade + '/' + contents.id);
    }
    render() {
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "照护等级",
                    decorator_id: "name",
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.competence_assessment_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_nursing_grade'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);

        let edit_props = {
            form_items_props: [
                {
                    input_props: [
                        {
                            type: InputTypeNew.antd_input,
                            label: "照护等级",
                            col_span: 8,
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入照护等级" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入照护等级"
                            }
                        },
                        {
                            type: InputTypeNew.antd_input,
                            label: "描述",
                            col_span: 8,
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ message: "请输入描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入描述"
                            }
                        },
                        {
                            type: InputTypeNew.text_area,
                            label: "备注",
                            col_span: 8,
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.competence_assessment_service,
                operation_option: {
                    save: {
                        func_name: "update_nursing_grade"
                    },
                    query: {
                        func_name: 'get_nursing_grade_list',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            id: this.props.match!.params.key,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingGrade); },

        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <Row>
                {this.state.add_permission || this.props.match!.params.key ? <MainContent>
                    <div className='form-bottom'>
                        <Collapse defaultActiveKey={['1']}>
                            <Panel header="照护等级设定" key="1">
                                <FormCreator {...edit_props_list} />
                            </Panel>
                        </Collapse>
                    </div>
                </MainContent> : null}
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：照护等级设定
 * 描述
 */
@addon('NursingGradeView', '照护等级设定', '描述')
@reactControl(NursingGradeView, true)
export class NursingGradeViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}