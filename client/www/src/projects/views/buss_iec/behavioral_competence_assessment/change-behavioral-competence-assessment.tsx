import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { edit_props_info } from "../../../app/util-tool";

/**
 * 组件：编辑行为能力评估项目状态
 */
export interface ChangeBehavioralCompetenceAssessmentViewState extends ReactViewState {

    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：编辑行为能力评估项目视图
 */
export class ChangeBehavioralCompetenceAssessmentView extends ReactView<ChangeBehavioralCompetenceAssessmentViewControl, ChangeBehavioralCompetenceAssessmentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: ''
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.behavioralCompetenceAssessment);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "行为能力评估项目",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "项目名称",
                            decorator_id: "project_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目名称" }],
                                initialValue: base_data ? base_data.project_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入项目名称"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "一级类别",
                            decorator_id: "category_i",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入一级类别" }],
                                initialValue: base_data ? base_data.category_i : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入一级类别"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "二级类别",
                            decorator_id: "category_ii",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入二级类别" }],
                                initialValue: base_data ? base_data.category_ii : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入二级类别"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.behavioralCompetenceAssessment);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.behavioral_competence_assessment_service,
                operation_option: {
                    save: {
                        func_name: "update_project"
                    },
                    query: {
                        func_name: "get_project_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.behavioralCompetenceAssessment); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑行为能力评估项目编辑控件
 * @description 编辑行为能力评估项目编辑控件
 * @author
 */
@addon('ChangeBehavioralCompetenceAssessmentView', '编辑行为能力评估项目编辑控件', '编辑行为能力评估项目编辑控件')
@reactControl(ChangeBehavioralCompetenceAssessmentView, true)
export class ChangeBehavioralCompetenceAssessmentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}