
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { exprot_excel } from "src/business/util_tool";

/** 状态：能力评估视图 */
export interface CompetenceAssessmentViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
    /* 导入 */
    config?: any;
}
/** 组件：能力评估视图 */
export class CompetenceAssessmentView extends React.Component<CompetenceAssessmentViewControl, CompetenceAssessmentViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'org_name',
            key: 'org_name',
        },
        {
            title: '长者姓名',
            dataIndex: 'user_name',
            key: 'user_name',
        }, {
            title: '模板名称',
            dataIndex: 'template_name',
            key: 'template_name',
        }, {
            title: '护理等级',
            dataIndex: 'nursing_level',
            key: 'nursing_level'
        }, {
            title: '评估日期',
            dataIndex: 'nursing_time',
            key: 'nursing_time',
        }, {
            title: '评估得分',
            dataIndex: 'total_score',
            key: 'total_score',
        }];
    constructor(props: CompetenceAssessmentViewControl) {
        super(props);
        this.state = {
            org_list: [],
            config: {
                upload: false,
                func: AppServiceUtility.competence_assessment_service.import_pinggu_record,
                title: '导入长者评估名单'
            }
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.match!.url.indexOf("护理") !== -1 ? this.props.history!.push(ROUTE_PATH.competenceAssessment + '/护理') : this.props.history!.push(ROUTE_PATH.competenceAssessment);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.competenceAssessment + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /* 导入 */
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    /* 模板 */
    export_template = () => {
        const data = [{
            name: '长者评估导入模板', value: [{
                '身份证': '4416XXXXXXXXXXXXXX',
                '护理等级': '自理/介护1/介护2/介助1/介助2',
                '信息提供者姓名': 'XXX',
                '联系人姓名': 'XXX',
                '联系人电话': '1XXXXXXXXXX'
            }]
        }];
        exprot_excel(data, data[0].name, 'xlsx');
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_template = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name"
                }, {
                    type: InputType.rangePicker,
                    label: "评估日期",
                    decorator_id: "date_range"
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name"
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            },
            {
                label: '导入',
                btn_method: this.upload,
                icon: 'upload'
            }, {
                label: '下载模板',
                btn_method: this.export_template,
                icon: 'download'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.competence_assessment_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_competence_assessment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_template, table_param);
        return (
            <>
                <SignFrameLayout {...behavioral_competence_assessment_list} />
                <UploadFile {...this.state.config} />
            </>
        );
    }
}

/**
 * 控件：能力评估视图控制器
 * @description 能力评估视图
 * @author
 */
@addon('CompetenceAssessmentView', '能力评估视图', '能力评估视图')
@reactControl(CompetenceAssessmentView, true)
export class CompetenceAssessmentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}