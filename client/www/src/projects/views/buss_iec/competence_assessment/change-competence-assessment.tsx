import { Button, Radio, Row, Checkbox, Input, Select, Card, message, DatePicker, LocaleProvider } from "antd";
import Form, { WrappedFormUtils } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { remote } from "src/projects/remote";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { beforeUpload } from "src/projects/app/util-tool";
import './index.less';
const { TextArea } = Input;
const { Option } = Select;
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from "moment";
import { request_func } from "src/business/util_tool";

/**
 * 组件：行为能力评估状态
 */
export interface ChangeCompetenceAssessmentViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
    /** 长者列表 */
    elder_list?: any[];
    /** 模板列表 */
    template_list?: any[];
    /** 长者 */
    elder?: any;
    /** 长者id */
    elderid?: string;
    /** 模板 */
    template?: string;
    /*申请表里的选项和值 */
    project?: any;
    /*单选按钮的值 */
    radioValue?: any;
    /*多行文本的值 */
    textAreaValue?: any;
    checkValue?: any;
    /** 项目列表 */
    projectobjdata?: any[];
    /** 总分 */
    datacount?: any[];
    /** 单选框值 */
    radiocount?: number;
    /** 计算 */
    data_data?: any[];
    /** 模板id */
    templateid?: string | number;
    /** 修改的模板数组 */
    changetemplate_list?: any[];
    /** 最终计算数据 */
    countdata?: number[];
    /** 护理等级列表 */
    NursingLevel?: any;
    /** 护理等级 */
    nursing_level?: any;
    /** 评估时间 */
    nursing_time?: any;
    /** 护理时间 */
    record_time?: any;
    /** 当前模板id */
    template_id: string;
    // 单模板总分
    total?: any;
    total_obj?: any;
    showTotal?: any;
}
export interface CompetenceAssessmentListFormValues {
    id?: string;
}
/**
 * 组件：能力评估视图
 */
export class ChangeCompetenceAssessmentView extends ReactView<ChangeCompetenceAssessmentViewControl, ChangeCompetenceAssessmentViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'personnel_info.sex',
        key: 'personnel_info.sex',
    }, {
        title: '年龄',
        dataIndex: 'personnel_info.age',
        key: 'personnel_info.age',
    }, {
        title: '出生年月',
        dataIndex: 'personnel_info.date_birth',
        key: 'personnel_info.date_birth',
    }];
    /** 默认是普通模板 */
    public status = false;
    public index = 0;
    public idx = 0;
    public indx = 0;
    constructor(props: any) {
        super(props);
        this.state = {
            elder_list: [],
            template_list: [],
            project_list: [],
            projectobjdata: [],
            elder: '',
            template: '',
            project: [],
            elderid: '',
            datacount: [],
            radiocount: 0,
            data_data: [],
            templateid: '',
            changetemplate_list: [],
            countdata: [],
            NursingLevel: [],
            nursing_level: '',
            nursing_time: moment(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            record_time: "",
            template_id: "",
            total: 0,
            total_obj: [],
            showTotal: false
        };
    }

    /** 提交方法 */
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: CompetenceAssessmentListFormValues) => {
            if (err) {
                return;
            }
            let obj: any = {}, project_list = [], data: any = [];
            let list = [];
            if (!this.status) {
                if (this.state.elderid) {
                    if (this.props.match!.params.key !== '护理') {
                        /** 普遍模板 */
                        for (const key of Object.keys(values)) {
                            if (values.hasOwnProperty(key)) {
                                if (key.indexOf('___') > 0) {
                                    if (key.split('___').length > 2) {
                                        list = key.split('___');
                                        let radio_list = [];
                                        radio_list = list[1].split(';');
                                        let new_radio_list: any = [];
                                        radio_list.forEach((item: any) => {
                                            new_radio_list.push(JSON.parse(item));
                                        });
                                        let project = {
                                            ass_info: {
                                                project_name: list[0],
                                                dataSource: new_radio_list,
                                                selete_type: list[2],
                                                value: values[key]
                                            }
                                        };
                                        project_list.push(project);
                                    } else {
                                        let project = {
                                            ass_info: {
                                                project_name: key.split('___')[0],
                                                selete_type: key.split('___')[1],
                                                value: values[key]
                                            }
                                        };
                                        project_list.push(project);
                                    }
                                    if (this.props.match!.params.key) {
                                        obj['id'] = this.props.match!.params.key;
                                    }
                                    obj['elder'] = this.state.elderid;
                                    obj['total_score'] = this.state.total;
                                } else {
                                    obj[key] = values[key];
                                }
                            }
                            obj['project_list'] = project_list;
                        }
                        if (this.state.nursing_level) {
                            obj['nursing_level'] = this.state.nursing_level;
                        }
                        if (this.state.nursing_time) {
                            obj['nursing_time'] = this.state.nursing_time;
                            AppServiceUtility.competence_assessment_service.update_competence_assessment!(obj)!
                                .then((data: any) => {
                                    if (data) {
                                        message.info('保存成功');
                                        this.props.history!.push(ROUTE_PATH.competenceAssessmentList);
                                    }
                                });
                        } else {
                            message.error('请选择评估时间！');
                            return;
                        }
                    } else {
                        for (const key of Object.keys(values)) {
                            if (values.hasOwnProperty(key)) {
                                if (key.indexOf('___') > 0) {
                                    if (key.split('___').length > 2) {
                                        list = key.split('___');
                                        let radio_list = [];
                                        radio_list = list[1].split(';');
                                        let new_radio_list: any = [];
                                        radio_list.forEach((item: any) => {
                                            new_radio_list.push(JSON.parse(item));
                                        });
                                        let project = {
                                            ass_info: {
                                                project_name: list[0],
                                                dataSource: new_radio_list,
                                                selete_type: list[2],
                                                value: values[key]
                                            }
                                        };
                                        project_list.push(project);
                                    } else {
                                        let project = {
                                            ass_info: {
                                                project_name: key.split('___')[0],
                                                selete_type: key.split('___')[1],
                                                value: values[key]
                                            }
                                        };
                                        project_list.push(project);
                                    }
                                    if (this.props.match!.params.key && this.props.match!.params.key !== '护理') {
                                        obj['id'] = this.props.match!.params.key;
                                    }
                                    obj['elder'] = this.state.elderid;
                                } else {
                                    obj[key] = values[key];
                                }
                            }
                            obj['project_list'] = project_list;
                        }
                        if (this.state.template_id) {
                            obj["id"] = this.state.template_id;
                        }
                        if (this.state.record_time) {
                            obj['record_time'] = this.state.record_time;
                            AppServiceUtility.nursing_record.update_nursing_recode!({ obj })!
                                .then((data: any) => {
                                    if (data) {
                                        message.info('保存成功');
                                        this.props.history!.push(ROUTE_PATH.nursingRecord);
                                    }
                                });
                        } else {
                            message.error('请选择护理时间！');
                            return;
                        }
                    }
                } else {
                    message.error('请选择长者！');
                    return;
                }
            } else if (this.status) {
                if (this.state.elderid) {
                    /** 大模板 */
                    let obj: any = {};
                    obj['template_list'] = [];
                    let counts = 0;
                    let total_score: number = 0;
                    this.state.project_list!.forEach((item: any, index: number) => {
                        data = [];
                        if (!item.template_list) {
                            let value: any[] = [];
                            for (let key of Object.keys(values)) {
                                if (key === 'nursing_level') {
                                    obj['nursing_level'] = values[key];
                                }
                                if (key !== 'elder' && key !== 'template' && key !== 'nursing_level') {
                                    value.push(values[key]);
                                } else if (key === 'elder' || key === 'template') {
                                    continue;
                                } else {
                                    break;
                                }
                            }
                            item && item.forEach((itm: any, idx: number) => {
                                counts = counts + 1;
                                data.push({
                                    ass_info: {
                                        dataSource: itm.ass_info.dataSource,
                                        project_name: itm.ass_info.project_name,
                                        selete_type: itm.ass_info.selete_type,
                                        value: value[counts],
                                        template_name: item.category_name,
                                        serial_number: item.serial_number,
                                    }
                                });
                            });
                        } else {
                            let value: any[] = [];
                            for (let key of Object.keys(values)) {
                                if (key === 'nursing_level') {
                                    obj['nursing_level'] = values[key];
                                }
                                if (key !== 'elder' && key !== 'template' && key !== 'nursing_level') {
                                    value.push(values[key]);
                                } else if (key === 'elder' || key === 'template') {
                                    continue;
                                } else {
                                    break;
                                }
                            }
                            item.template_list.forEach((itm: any, idx: number) => {
                                counts = counts + 1;
                                data.push({
                                    ass_info: {
                                        dataSource: itm.ass_info.dataSource,
                                        project_name: itm.ass_info.project_name,
                                        selete_type: itm.ass_info.selete_type,
                                        value: value[counts],
                                        template_name: item.category_name,
                                        serial_number: item.serial_number,
                                    }
                                });
                            });
                        }
                        total_score = total_score + this.state.countdata![index];
                        obj.template_list.push(data);
                    });
                    obj['elder'] = this.state.elderid;
                    if (this.props.match!.params.key) {
                        obj['id'] = this.props.match!.params.key;
                    }
                    if (this.state.templateid) {
                        obj['template'] = this.state.templateid;
                    }
                    if (this.state.nursing_time) {
                        obj['nursing_time'] = this.state.nursing_time;
                        obj['total_score'] = total_score;
                        AppServiceUtility.competence_assessment_service.update_competence_assessment!(obj)!
                            .then((data: any) => {
                                if (data) {
                                    message.info('保存成功');
                                    this.props.history!.push(ROUTE_PATH.competenceAssessmentList);
                                }
                            });
                    } else {
                        message.error('请选择评估时间！');
                        return;
                    }
                } else {
                    message.error('请选择长者！');
                    return;
                }
            } else {
                // 待修复
            }
        });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.competenceAssessmentList);
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_personnel_elder!({})!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        elder_list: data.result
                    });
                }
            });
        if (this.props.match!.url.indexOf("护理") !== -1) {
            AppServiceUtility.assessment_template_service.get_assessment_template_list!({ template_type: '详细能力评估' })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            template_list: data.result
                        });
                    }
                });
        } else if (this.props.match!.params.key) {
            AppServiceUtility.assessment_template_service.get_assessment_template_list!({ template_type: '详细能力评估' })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            template_list: data.result
                        });
                    }
                });
        } else if (!this.props.match!.params.key) {
            AppServiceUtility.assessment_template_service.get_assessment_template_list_all_look!({ template_type: '详细能力评估' })!
                .then((data: any) => {
                    if (data.result) {
                        this.setState({
                            template_list: data.result
                        });
                    }
                });
        }
        AppServiceUtility.assessment_project_service.get_assessment_project_list_all_look!({})!
            .then((data: any) => {
                if (data.result) {
                    this.setState({
                        projectobjdata: data.result
                    });
                }
            });
        if (this.props.match!.params.key && this.props.match!.url.indexOf("护理") === -1) {
            AppServiceUtility.competence_assessment_service.get_competence_assessment_list!({ id: this.props.match!.params.key }, 1, 1)!
                .then((data) => {
                    if (data!.result![0]!['nursing_time']) {
                        this.setState({
                            nursing_time: moment(data!.result![0]!['nursing_time'], 'YYYY-MM-DD HH:mm:ss')
                        });
                    }
                    this.setState({ elder: data!.result![0].user_name, template: data!.result![0].template!['id'], project_list: data!.result![0].project_list ? data!.result![0].project_list : data!.result![0].template_list, elderid: data!.result![0]!.elder, templateid: data!.result![0].template!['id'], changetemplate_list: data!.result![0]!['template']!['templateSource'] ? data!.result![0]!['template']!['templateSource'] : data!.result![0]!['template']!['dataSource'], nursing_level: data!.result![0].nursing_level }, () => {
                        let data_data: any[] = [];
                        let data_date: any[] = [];
                        let data_dete: any[] = [];
                        let datas = this.state.project_list;
                        let countdata: number[] = [];
                        let zongdata: number = 0;
                        let zong: number[] = [];
                        // let temporary: any = [];
                        datas!.forEach((item: any, idx: number) => {
                            data_date = [];
                            countdata = [];
                            zongdata = 0;
                            if (Object.prototype.toString.call(item) === '[object Array]') {
                                item.forEach((itm: any, i: number) => {
                                    if (itm.ass_info.selete_type === '1' || itm.ass_info.selete_type === '2' || itm.ass_info.selete_type === '4') {
                                        countdata.push(itm.ass_info.value);
                                        itm.ass_info.dataSource.forEach((i: any, x: number) => {
                                            data_dete.push(0);
                                        });
                                    } else {
                                        data_dete.push(0);
                                    }
                                    data_dete = [];
                                    data_date.push(data_dete);
                                });
                                data_data.push(data_date);
                            } else {
                                if (item.ass_info.selete_type === '1' || item.ass_info.selete_type === '2' || item.ass_info.selete_type === '4') {
                                    countdata.push(item.ass_info.value);
                                    data_dete = [];
                                    if (item.ass_info.dataSource) {
                                        item.ass_info.dataSource.forEach((i: any, x: number) => {
                                            data_dete.push(0);
                                        });
                                    } else {
                                        data_dete.push(0);
                                    }
                                } else {
                                    data_dete.push(0);
                                }
                                data_date.push(data_dete);
                            }
                            countdata.forEach((i: any, x: number) => {
                                zongdata = zongdata + parseInt(i, 10);
                            });
                            zong.push(zongdata);
                        });
                        this.setState({
                            data_data: data_data,
                            countdata: zong
                        });
                    });
                    if (data!.result![0].template!['type'] === '评估模板') {
                        this.status = true;
                    }
                    this.hasNursing(data!.result![0].template!['template_type']);
                });
        } else {
            var str = this.props!.location!.pathname;
            var index = str.indexOf("护理/");
            if (index !== -1) {
                var code = str.substr(index + 3, str.length);
                AppServiceUtility.nursing_record.get_nursing_recode_list!({ id: code }, 1, 1)!
                    .then((data) => {
                        if (data!.result![0]!['obj']["record_time"]) {
                            this.setState({
                                record_time: moment(data!.result![0]!['obj']["record_time"], 'YYYY-MM-DD HH:mm:ss')
                            });
                        }
                        this.setState({
                            template: data!.result![0]!["obj"]!["template"],
                            project_list: data!.result![0]!["obj"]!["project_list"] ? data!.result![0]!["obj"]!["project_list"] : data!.result![0]!["obj"]!["template_list"],
                            elderid: data!.result![0]!["obj"]!["elder"],
                            elder: data!.result![0]!["obj"]!["elder"],
                            templateid: data!.result![0]!["obj"]!["template"],
                            template_id: data!.result![0]!["id"]
                        });
                    });
            }
        }
    }
    hasNursing = (template_type: any) => {
        let childen: Array<string> = [];
        if (template_type === "详细能力评估") {
            request_func(this, AppServiceUtility.competence_assessment_service.get_nursing_grade_list, [{}], (data: any) => {
                childen = data.result.map((item: any) => {
                    return (<Option key={item.name} value={item.name}>{item.name}</Option>);
                });
                this.setState({ NursingLevel: childen });
            });
            // const nursing_level = ['自理', '介护1', '介护2', '介助1', '介助2', '个性化护理'];
            // const childen = nursing_level.map!((val: any) => {
            //     return (<Option key={val} value={val}>{val}</Option>);
            // });
        } else {
            this.setState({ NursingLevel: [], nursing_level: '' });
        }
    }
    /** 选择模板选择框值改变 */
    template_change = (value: any) => {
        this.setState({
            total: 0
        });
        AppServiceUtility.assessment_template_service.get_assessment_template!({ id: value, is_edit: true }, 1, 1)!
            .then((data) => {
                this.setState({ template: data!.result![0]['id'], project_list: data!.result![0]['dataSource'] ? data!.result![0]['dataSource'] : data!.result![0]['templateSource'], changetemplate_list: data!.result![0]!['dataSource'] ? data!.result![0]!['dataSource'] : data!.result![0]!['templateSource'] }, () => {
                    if (this.state.template_list!.length > 0) {
                        let data_obj = {};
                        let data_date: any[] = [];
                        let data_dete: any[] = [];
                        this.state.template_list!.forEach((item: any, index: number) => {
                            if (value === item.id) {
                                this.hasNursing(item.template_type);
                                data_obj = item;
                                this.setState({
                                    templateid: value
                                });
                            }
                        });
                        if (data_obj!['dataSource'] && data_obj!['dataSource'].length > 0) {
                            this.status = false;
                            let values: any = [];
                            data_obj!['dataSource'].forEach((item: any, index: number) => {
                                this.state.projectobjdata!.forEach((ida: any, idx: number) => {
                                    if (item.assessment_projects === ida.id) {
                                        values.push(
                                            {
                                                ass_info: ida,
                                                assessment_projects: item.assessment_projects
                                            }
                                        );
                                    }
                                });
                            });
                            this.setState({ project_list: values });
                        } else if (data_obj!['templateSource'] && data_obj!['templateSource'].length > 0) {
                            this.status = true;
                            if (!data_obj!['templateSource'][0]['assessment_projects']) {
                                this.setState({ project_list: data_obj!['templateSource'] }, () => {
                                    let datass = [];
                                    for (let x = 0; x < this.state.project_list!.length; x++) {
                                        data_date = [];
                                        for (let y = 0; y < this.state.project_list![x].template_list.length; y++) {
                                            data_dete = [];
                                            if (this.state.project_list![x].template_list[y].ass_info.selete_type === '1' || this.state.project_list![x].template_list[y].ass_info.selete_type === '2' || this.state.project_list![x].template_list[y].ass_info.selete_type === '4') {
                                                for (let z = 0; z < this.state.project_list![x].template_list[y].ass_info.dataSource.length; z++) {
                                                    data_dete.push(0);
                                                }
                                            } else {
                                                data_dete.push(0);
                                            }
                                            data_date.push(data_dete);
                                        }
                                        datass = data_date;
                                        break;
                                    }
                                    this.setState({ data_data: datass }, () => {
                                        let data_data: any[] = [];
                                        let datas = this.state.project_list;
                                        let countdata: number[] = [];
                                        datas!.forEach((item: any, idx: number) => {
                                            countdata.push(0);
                                            data_date = [];
                                            if (Object.prototype.toString.call(item) === '[object Array]') {
                                                item.forEach((itm: any, i: number) => {
                                                    data_dete = [];
                                                    if (itm.ass_info.selete_type === '1' || itm.ass_info.selete_type === '2' || itm.ass_info.selete_type === '4') {
                                                        itm.ass_info.dataSource.forEach((i: any, x: number) => {
                                                            data_dete.push(0);
                                                        });
                                                    } else {
                                                        data_dete.push(0);
                                                    }
                                                    data_date.push(data_dete);
                                                });
                                                data_data.push(data_date);
                                            } else {
                                                item.template_list.forEach((itm: any, i: number) => {
                                                    data_dete = [];
                                                    if (itm.ass_info.selete_type === '1' || itm.ass_info.selete_type === '2' || itm.ass_info.selete_type === '4') {
                                                        itm.ass_info.dataSource.forEach((i: any, x: number) => {
                                                            data_dete.push(0);
                                                        });
                                                    } else {
                                                        data_dete.push(0);
                                                    }
                                                    data_date.push(data_dete);
                                                });
                                                data_data.push(data_date);
                                            }
                                        });
                                        this.setState({
                                            data_data: data_data,
                                            countdata
                                        });
                                    });
                                });
                            }
                        }
                    }
                });
                if (data!.result![0]['type'] === '评估模板') {
                    this.status = true;
                }
            });
    }
    handelchange(value: any) {
        this.setState({
            elderid: value
        });
    }

    /** 处理新增dom的extra */
    addextradom = (x: number) => {
        let status: boolean = false;
        if (this.state.project_list![x]['template_list']) {
            for (let itm: number = 0; itm < this.state.project_list![x]['template_list'].length; itm++) {
                if (this.state.project_list![x]['template_list'][itm]['ass_info']['selete_type'] === '1' || this.state.project_list![x]['template_list'][itm]['ass_info']['selete_type'] === '2' || this.state.project_list![x]['template_list'][itm]['ass_info']['selete_type'] === '4') {
                    status = true;
                }
            }
        } else {
            for (let itm: number = 0; itm < this.state.project_list![x].length; itm++) {
                if (this.state.project_list![x][itm]['ass_info']['selete_type'] === '1' || this.state.project_list![x][itm]['ass_info']['selete_type'] === '2' || this.state.project_list![x][itm]['ass_info']['selete_type'] === '4') {
                    status = true;
                }
            }
        }
        if (status) {
            return <a>总分：{this.state.countdata![x]}</a>;
        } else {
            return null;
        }
    }

    datechange = (date: any, dateString: string) => {
        this.setState({
            nursing_time: dateString
        });
    }
    daterecordchange = (date: any, dateString: string) => {
        this.setState({
            record_time: dateString
        });
    }

    handlecount = (e: any) => {
        let data = setTimeout(
            () => {
                clearTimeout(data);
                let item = this.state.data_data;
                let countdata: any = [];
                countdata = this.state.countdata;
                item![this.index][this.indx][this.idx] = parseInt(e.target ? e.target.value.split('/')[0] : e.split('/')[0], 10);
                item![this.index][this.indx].forEach((itm: any, idx: number) => {
                    if (this.idx === idx) {
                        itm = parseInt(e.target ? e.target.value.split('/')[0] : e.split('/')[0], 10);
                    } else {
                        item![this.index][this.indx][idx] = 0;
                    }
                });
                let datas = this.state.data_data![this.index];
                datas = this.flat(datas);
                datas = datas.reduce(this.getSum);
                countdata[this.index] = datas;
                this.setState({
                    countdata
                });
            }
            ,
            100
        );
    }
    /** 多选框计算 */
    handlecheckbox = (e: any) => {
        let data = setTimeout(
            () => {
                clearTimeout(data);
                /** 深拷贝data_data */
                let data_data: any = [];
                let data_date: any[] = [];
                let data_dete: any[] = [];
                let zong_data: any = [];
                this.state.data_data!.forEach((item: any, idx: number) => {
                    data_date = [];
                    item.forEach((itm: any, i: number) => {
                        data_dete = [];
                        itm.forEach((i: any, x: number) => {
                            data_dete.push(i);
                        });
                        data_date.push(data_dete);
                    });
                    data_data.push(data_date);
                    zong_data.push(0);
                });
                data_data![this.index][this.indx] = e ? e.split('/')[0] : [0];
                this.setState({
                    data_data
                });
                let datas = this.flat(data_data[this.index]);
                zong_data[this.index] = eval(datas.join("+"));
                this.setState({
                    countdata: zong_data
                });
            }
            ,
            100
        );
    }

    /** 点击 */
    handleradioclicl = (index: number, indx: number, idx: number) => {
        this.index = index;
        this.indx = idx;
        this.idx = indx;
    }
    /** 求和 */
    getSum = (total: any, num: any) => {
        return total + num;
    }
    /** 返回按钮 */
    retLisit = () => {
        this.props.history!.push(ROUTE_PATH.competenceAssessmentList);
    }
    flat = (arr: any) => {
        return arr.reduce(
            (pre: any, value: any) => {
                return Array.isArray(value) ? [...pre, ...this.flat(value)] : [...pre, value];
            }
            ,
            []
        );
    }
    //  单模板计算分数
    totalCount = (e: any, index: any, type: any) => {
        this.setState({
            showTotal: true
        });
        let list = this.state.total_obj;
        if (type === 'radio') {
            list[index] = e.target.value.split('/')[0];
        } else {
            list[index] = e[0].split('/')[0];
        }
        this.setState({
            total_obj: list
        });
        let total = 0;
        list.map((item: any) => {
            total = total + Number(item);
        });
        this.setState({
            total
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const { getFieldDecorator } = this.props.form!;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 2 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 22 },
            },
        };
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_personnel_elder',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let template = this.state.template_list!.map((key, value) => {
            return <Select.Option key={key.id}>{key.template_name}</Select.Option>;
        });
        let project: any = [];
        let name_data: string[] = [];
        this.state.project_list!.forEach((item: any, index: number) => {
            if (this.state.changetemplate_list!.length > 0) {
                name_data[index] = this.state.changetemplate_list![index]!['category_name'];
            } else if (this.props.match!.params.key && this.props.match!.url.indexOf("护理") === -1) {
                for (let i = 0; i < this.state.project_list!.length; i++) {
                    name_data[i] = this.state.project_list![i][0].ass_info.template_name;
                }
            } else if (this.props.match!.url.indexOf("护理") !== -1) {
                for (let i = 0; i < this.state.project_list!.length; i++) {
                    name_data[i] = this.state.project_list![i].ass_info.project_name;
                }
            }
            if (Object.prototype.toString.call(item) === '[object Array]') {
                let dom: any = [];
                item.map((itm: any, idx: number) => {
                    if (itm.hasOwnProperty("ass_info")) {
                        if (itm.ass_info.hasOwnProperty("selete_type")) {
                            if (itm.ass_info.selete_type === "1") {
                                let radio_list = '';
                                if (itm.ass_info.hasOwnProperty("dataSource")) {
                                    itm.ass_info.dataSource.forEach((dates: any) => {
                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                    });
                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                    dom.push(
                                        <Form.Item label={itm.ass_info.project_name} key={idx} className="content-body">
                                            {getFieldDecorator(itm.ass_info.project_name + "___" + radio_list + "___1", {
                                                initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择' + itm.ass_info.project_name,
                                                }],
                                            })(
                                                <Radio.Group onChange={this.handlecount}>
                                                    {itm.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                        return <Radio onClick={() => { this.handleradioclicl(index, idxs, idx); }} value={i.score + '/' + i.serial_number} key={idxs}>{i.option_content}</Radio>;
                                                    })}
                                                </Radio.Group>
                                            )}
                                        </Form.Item>
                                    );
                                } else {
                                    return null;
                                }
                            } else if (itm.ass_info.selete_type === "2") {
                                let radio_list = '';
                                if (itm.ass_info.hasOwnProperty("dataSource")) {
                                    itm.ass_info.dataSource.forEach((dates: any) => {
                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                    });
                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                    dom.push(
                                        <Form.Item label={itm.ass_info.project_name} key={idx} className="content-body">
                                            {getFieldDecorator(itm.ass_info.project_name + '___' + radio_list + '___2', {
                                                initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择' + itm.ass_info.project_name,
                                                }],
                                            })(
                                                <Checkbox.Group onChange={this.handlecheckbox}>
                                                    {itm.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                        return <Checkbox key={idxs} value={i.score + '/' + i.serial_number} onClick={() => { this.handleradioclicl(index, idxs, idx); }}>{i.option_content}</Checkbox>;
                                                    })}
                                                </Checkbox.Group>
                                            )}
                                        </Form.Item>
                                    );
                                } else {
                                    return null;
                                }
                            } else if (itm.ass_info.selete_type === "3") {
                                dom.push(
                                    <Form.Item label={itm.ass_info.project_name} key={idx} className="content-body">
                                        {getFieldDecorator(itm.ass_info.project_name + '___3', {
                                            initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                            rules: [{
                                                required: true,
                                                message: '请输入' + itm.ass_info.project_name,
                                            }],
                                        })(
                                            <Input value={this.state.project} placeholder={`请输入${itm.ass_info.project_name}`} />
                                        )}
                                    </Form.Item>
                                );
                            } else if (itm.ass_info.selete_type === "4") {
                                let radio_list = '';
                                if (itm.ass_info.hasOwnProperty("dataSource")) {
                                    itm.ass_info.dataSource.forEach((dates: any) => {
                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                    });
                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                    dom.push(
                                        <Form.Item label={itm.ass_info.project_name} key={idx} className="content-body">
                                            {getFieldDecorator(itm.ass_info.project_name + '___' + radio_list + '___4', {
                                                initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择' + itm.ass_info.project_name,
                                                }],
                                            })(
                                                <Select defaultValue={itm.ass_info.dataSource[0].name} style={{ width: 120 }}>
                                                    {itm.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                        return <Option key={idxs} value={i.score + '/' + i.serial_number}>{i.option_content}</Option>;
                                                    })}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    );
                                } else {
                                    return null;
                                }
                            } else if (itm.ass_info.selete_type === "5") {
                                dom.push(
                                    <Form.Item label={itm.ass_info.project_name} key={idx} className="content-body">
                                        {getFieldDecorator(itm.ass_info.project_name + '___5', {
                                            initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                            rules: [{
                                                required: true,
                                                message: '请输入' + itm.ass_info.project_name,
                                            }],
                                        })(
                                            <TextArea
                                                value={this.state.textAreaValue}
                                                placeholder={`请输入${itm.ass_info.project_name}`}
                                                autosize={{ minRows: 3, maxRows: 5 }}
                                            />
                                        )}
                                    </Form.Item>
                                );
                            } else if (itm.ass_info.selete_type === "6") {
                                dom.push(
                                    <Form.Item label={itm.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={idx} className="content-body">
                                        {getFieldDecorator(itm.ass_info.project_name + '___6', {
                                            initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                            rules: [{
                                                required: true,
                                            }],
                                        })(
                                            <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                        )}
                                    </Form.Item>
                                );
                            } else if (itm.ass_info.selete_type === "7") {
                                dom.push(
                                    <Form.Item label={itm.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={idx} className="content-body">
                                        {getFieldDecorator(itm.ass_info.project_name + '___7', {
                                            initialValue: itm.ass_info.value ? itm.ass_info.value : '',
                                            rules: [{
                                                required: true,
                                            }],
                                        })(
                                            <FileUploadBtn contents={"button"} action={remote.upload_url} />
                                        )}
                                    </Form.Item>
                                );
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    }
                    return project;
                });
                if (dom && dom.length > 0 && name_data.length > 0) {
                    let data: any = this.addextradom(index);
                    const item_content = (
                        <div style={{ background: '#ECECEC', padding: '30px' }} key={index} className="content-body">
                            <Card title={name_data![index]} bordered={false} extra={data}>
                                {dom}
                            </Card>
                        </div>
                    );
                    project.push(item_content);
                }
            } else if (item.hasOwnProperty("ass_info")) {
                // console.log('hahha',item);
                if (item.ass_info.hasOwnProperty("selete_type")) {
                    if (item.ass_info.selete_type === "1") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} className="content-body">
                                    {getFieldDecorator(item.ass_info.project_name + "___" + radio_list + "___1", {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Radio.Group onChange={(e) => { this.totalCount(e, index, 'radio'); }}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Radio value={i.score + '/' + i.serial_number} key={i.serial_number}>{i.option_content}</Radio>;
                                            })}
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "2") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} className="content-body">
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___2', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Checkbox.Group onChange={(e) => { this.totalCount(e, index, 'checkbox'); }}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Checkbox key={i.serial_number} value={i.score + '/' + i.serial_number}>{i.option_content}</Checkbox>;
                                            })}
                                        </Checkbox.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "3") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name} className="content-body">
                                {getFieldDecorator(item.ass_info.project_name + '___3', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <Input value={this.state.project} placeholder={`请输入${item.ass_info.project_name}`} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "4") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} className="content-body">
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___4', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Select defaultValue={item.ass_info.dataSource[0].name} style={{ width: 120 }}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Option key={i.serial_number} value={i.score + '/' + i.serial_number}>{i.option_content}</Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "5") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name} className="content-body">
                                {getFieldDecorator(item.ass_info.project_name + '___5', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <TextArea
                                        value={this.state.textAreaValue}
                                        placeholder={`请输入${item.ass_info.project_name}`}
                                        autosize={{ minRows: 3, maxRows: 5 }}
                                    />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "6") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} className="content-body">
                                {getFieldDecorator(item.ass_info.project_name + '___6', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                    }],
                                })(
                                    <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                    } else if (item.ass_info.selete_type === "7") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} className="content-body">
                                {getFieldDecorator(item.ass_info.project_name + '___7', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                    }],
                                })(
                                    <FileUploadBtn contents={"button"} action={remote.upload_url} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                    }
                }
            } else if (item.template_list && item.template_list.length > 0) {
                let data: any = this.addextradom(index);
                const item_content = (
                    <div style={{ background: '#ECECEC', padding: '30px' }} key={index} className="content-body">
                        <Card title={item.category_name} bordered={false} extra={data}>
                            {
                                item.template_list.map((date: any, idx: number) => {
                                    if (date.hasOwnProperty("ass_info")) {
                                        if (date.ass_info.hasOwnProperty("selete_type")) {
                                            if (date.ass_info.selete_type === "1") {
                                                let radio_list = '';
                                                if (date.ass_info.hasOwnProperty("dataSource")) {
                                                    date.ass_info.dataSource.forEach((dates: any) => {
                                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                                    });
                                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                                    return (<Form.Item label={date.ass_info.project_name} key={idx}>
                                                        {getFieldDecorator(date.ass_info.project_name + "___" + radio_list + "___1", {
                                                            initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                            rules: [{
                                                                required: true,
                                                                message: '请选择' + date.ass_info.project_name,
                                                            }],
                                                        })(
                                                            <Radio.Group onChange={this.handlecount}>
                                                                {date.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                                    return <Radio value={i.score + '/' + i.serial_number} key={idxs} onClick={() => { this.handleradioclicl(index, idxs, idx); }}>{i.option_content}</Radio>;
                                                                })}
                                                            </Radio.Group>
                                                        )}
                                                    </Form.Item>);
                                                }
                                                return null;
                                            }
                                            if (date.ass_info.selete_type === "2") {
                                                let radio_list = '';
                                                if (date.ass_info.hasOwnProperty("dataSource")) {
                                                    date.ass_info.dataSource.forEach((dates: any) => {
                                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                                    });
                                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                                    return (<Form.Item label={date.ass_info.project_name} key={idx}>
                                                        {getFieldDecorator(date.ass_info.project_name + '___' + radio_list + '___2', {
                                                            initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                            rules: [{
                                                                required: true,
                                                                message: '请选择' + date.ass_info.project_name,
                                                            }],
                                                        })(
                                                            <Checkbox.Group onChange={this.handlecheckbox}>
                                                                {date.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                                    return <Checkbox key={idxs} value={i.score + '/' + i.serial_number} onClick={() => { this.handleradioclicl(index, idxs, idx); }}>{i.option_content}</Checkbox>;
                                                                })}
                                                            </Checkbox.Group>
                                                        )}
                                                    </Form.Item>);
                                                }
                                                return null;
                                            }
                                            if (date.ass_info.selete_type === "3") {
                                                return (<Form.Item label={date.ass_info.project_name} key={idx}>
                                                    {getFieldDecorator(date.ass_info.project_name + '___3', {
                                                        initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                        rules: [{
                                                            required: true,
                                                            message: '请输入' + date.ass_info.project_name,
                                                        }],
                                                    })(
                                                        <Input value={this.state.project} placeholder={`请输入${date.ass_info.project_name}`} />
                                                    )}
                                                </Form.Item>);
                                            }
                                            if (date.ass_info.selete_type === "4") {
                                                let radio_list = '';
                                                if (date.ass_info.hasOwnProperty("dataSource")) {
                                                    date.ass_info.dataSource.forEach((dates: any) => {
                                                        radio_list = radio_list + JSON.stringify(dates) + ";";
                                                    });
                                                    radio_list = radio_list.slice(0, radio_list.length - 1);
                                                    return (<Form.Item label={date.ass_info.project_name} key={idx}>
                                                        {getFieldDecorator(date.ass_info.project_name + '___' + radio_list + '___4', {
                                                            initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                            rules: [{
                                                                required: true,
                                                                message: '请选择' + date.ass_info.project_name,
                                                            }],
                                                        })(
                                                            <Select defaultValue={date.ass_info.dataSource[0].name} style={{ width: 120 }} onChange={this.handlecount}>
                                                                {date.ass_info.dataSource!.map((i: any, idxs: any) => {
                                                                    return <option key={idxs} value={i.score + '/' + i.serial_number} onClick={() => { this.handleradioclicl(index, idxs, idx); }}>{i.option_content}</option>;
                                                                })}
                                                            </Select>
                                                        )}
                                                    </Form.Item>);
                                                }
                                                return null;
                                            }
                                            if (date.ass_info.selete_type === "5") {
                                                return (<Form.Item label={date.ass_info.project_name} key={idx}>
                                                    {getFieldDecorator(date.ass_info.project_name + '___5', {
                                                        initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                        rules: [{
                                                            required: true,
                                                            message: '请输入' + date.ass_info.project_name,
                                                        }],
                                                    })(
                                                        <TextArea
                                                            value={this.state.textAreaValue}
                                                            placeholder={`请输入${date.ass_info.project_name}`}
                                                            autosize={{ minRows: 3, maxRows: 5 }}
                                                        />
                                                    )}
                                                </Form.Item>);
                                            }
                                            if (date.ass_info.selete_type === "6") {
                                                return (<Form.Item label={date.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={idx}>
                                                    {getFieldDecorator(date.ass_info.project_name + '___6', {
                                                        initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                        rules: [{
                                                            required: true,
                                                        }],
                                                    })(
                                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                                    )}
                                                </Form.Item>);
                                            } else if (date.ass_info.selete_type === "7") {
                                                return (<Form.Item label={date.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={idx}>
                                                    {getFieldDecorator(date.ass_info.project_name + '___7', {
                                                        initialValue: date.ass_info.value ? date.ass_info.value : '',
                                                        rules: [{
                                                            required: true,
                                                        }],
                                                    })(
                                                        <FileUploadBtn contents={"button"} action={remote.upload_url} />
                                                    )}
                                                </Form.Item>);
                                            }
                                            return null;
                                        }
                                        return null;
                                    } else {
                                        return true;
                                    }
                                })
                            }
                        </Card>
                    </div>
                );
                project.push(item_content);
            } else {
                return true;
            }
        });
        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <MainContent>
                    <MainCard title='能力评估'>
                        <Form.Item label='选择长者'>
                            {getFieldDecorator('elder', {
                                initialValue: this.state.elder ? this.state.elder : '',
                                rules: [{
                                    required: true,
                                    message: '请选择长者'
                                }],
                            })(
                                <ModalSearch onChange={(e: any) => { this.handelchange(e); }} modal_search_items_props={modal_search_items_props} />
                            )}
                        </Form.Item>
                        <Form.Item label='选择模板'>
                            {getFieldDecorator('template', {
                                initialValue: this.state.template ? this.state.template : '',
                                rules: [{
                                    required: true,
                                    message: '请选择模板'
                                }],
                            })(
                                <Select placeholder={'请选择模板'} showSearch={true} onChange={this.template_change}>
                                    {template}
                                </Select>
                            )}
                        </Form.Item>
                        {project}
                        {this.state.showTotal ?
                            (<Form.Item label='总分'>
                                <span style={{ fontSize: '30px', fontWeight: 'bold' }}>{this.state.total}</span>
                            </Form.Item>)
                            : ''}
                        {this.state.NursingLevel.length > 0 ? (<Form.Item label='护理等级' style={{ marginTop: '30px' }}>
                            {getFieldDecorator('nursing_level', {
                                initialValue: this.state.nursing_level ? this.state.nursing_level : '',
                                rules: [{
                                    required: true,
                                    message: '请选择护理等级'
                                }],
                            })(
                                <Select>{this.state.NursingLevel}</Select>
                            )}
                        </Form.Item>) : ''}
                        {this.props.match!.params.key !== '护理' ? (<LocaleProvider locale={zh_CN}>
                            <Form.Item label='评估时间' style={{ marginTop: '30px' }}>
                                {getFieldDecorator('nursing_time', {
                                    initialValue: this.state.nursing_time ? this.state.nursing_time : '',
                                    rules: [{
                                        required: true,
                                        message: '请选择评估时间'
                                    }],
                                })(
                                    <DatePicker showTime={true} placeholder="请选择评估时间" onChange={this.datechange} />
                                )}
                            </Form.Item>
                        </LocaleProvider>) : <LocaleProvider locale={zh_CN}>
                                <Form.Item label='护理时间' style={{ marginTop: '30px' }}>
                                    {getFieldDecorator('record_time', {
                                        initialValue: this.state.record_time ? this.state.record_time : moment(new Date(), 'YYYY-MM-DD HH:mm:ss'),
                                        rules: [{
                                            required: true,
                                            message: '请选择护理时间'
                                        }],
                                    })(
                                        <DatePicker showTime={true} placeholder="请选择护理时间" onChange={this.daterecordchange} />
                                    )}
                                </Form.Item>
                            </LocaleProvider>}
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button htmlType='submit' type='primary'>保存</Button>
                            <Button type='ghost' name='返回' htmlType='button' onClick={this.retLisit}>返回</Button>
                        </Row>
                    </MainCard>
                </MainContent>
            </Form>
        );
    }
}

/**
 * 控件：能力评估控件
 * @description 能力评估控件
 * @author
 */
@addon('ChangeAssessmentTemplateView', '能力评估控件', '能力评估控件')
@reactControl(Form.create<any>()(ChangeCompetenceAssessmentView), true)
export class ChangeCompetenceAssessmentViewControl extends ReactViewControl {
    form?: WrappedFormUtils<any>;
    /** 视图权限 */
    public permission?: Permission;
}