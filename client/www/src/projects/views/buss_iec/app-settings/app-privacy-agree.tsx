
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Form, Row } from 'antd';
/** 状态：APP隐私政策同意 */
export interface AppPrivacyAgreeViewState extends ReactViewState {
    table_column?: any;
}
/** 组件：APP隐私政策同意 */

export class AppPrivacyAgreeView extends React.Component<AppPrivacyAgreeViewControl, AppPrivacyAgreeViewState> {
    constructor(props: AppPrivacyAgreeViewControl) {
        super(props);
        this.state = {
            table_column: [{
                title: "IP地址",
                dataIndex: 'ip',
                key: 'ip',
            },
            {
                title: '创建时间',
                dataIndex: 'create_date',
                key: 'create_date',
            },
            ],
        };
    }
    render() {
        // const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let privacy_agree = {
            type_show: false,
            edit_form_items_props: [
            ],
            // search_cb: this.queryBtn,
            columns_data_source: this.state.table_column,
            service_object: AppServiceUtility.app_page_config_service,
            service_option: {
                select: {
                    service_func: 'get_app_privacy_agree_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_app_privacy_agree'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            delete_permission: this.props.delete_permission,
        };
        let privacy_agree_list = Object.assign(privacy_agree, table_param);
        return (
            <Row>
                <SignFrameLayout {...privacy_agree_list} />
            </Row>
        );
    }
}

/**
 * 控件：APP隐私政策同意控制器
 * @description APP隐私政策同意
 * @author
 */
@addon('AppPrivacyAgreeView', 'APP隐私政策同意', 'APP隐私政策同意')
@reactControl(Form.create<any>()(AppPrivacyAgreeView), true)
export class AppPrivacyAgreeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}