
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Form, Row, Tabs, Card, Button, message } from 'antd';
import { request } from "src/business/util_tool";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";
// import { ROUTE_PATH } from "src/projects/router";

class MyServiceAgreementForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            config_type: 'service_agreement',
            data_info: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: this.state.config_type }))
            .then((datas: any) => {
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((error: any) => {
                console.error(error);
            });
    }
    handleFeedbackSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['data'] = {
                    content: values['content'],
                };
                let { config_type, data_info } = this.state;
                formValue['type'] = config_type;
                if (data_info.hasOwnProperty('id')) {
                    formValue['id'] = data_info.id;
                }
                request(this, AppServiceUtility.app_page_config_service.update_app_config!(formValue))
                    .then((datas: any) => {
                        if (datas && datas.code === 'Success') {
                            data_info['id'] = datas['id'];
                            this.setState({
                                data_info,
                            });
                            message.info('保存成功');
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { formItemLayout } = this.props;
        let data_info = this.state.data_info;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleFeedbackSubmit}>
                    <MainContent>
                        <Form.Item label='服务协议：'>
                            {getFieldDecorator('content', {
                                initialValue: data_info && data_info.data && data_info.data.content ? data_info.data.content : '',
                                rules: [{
                                    required: true,
                                    message: '请输入服务协议'
                                }],
                            })(
                                <TextArea rows={20} />
                            )}
                        </Form.Item>
                    </MainContent>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                    </Row>
                </Form>
            </Row>
        );
    }
}
const FormServiceAgreement = Form.create<any>()(MyServiceAgreementForm);

class MyPrivacyPolicyForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            config_type: 'privacy_policy',
            data_info: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: this.state.config_type }))
            .then((datas: any) => {
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((error: any) => {
                console.error(error);
            });
    }
    handleFeedbackSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['data'] = {
                    content: values['content'],
                };
                let { config_type, data_info } = this.state;
                formValue['type'] = config_type;
                if (data_info.hasOwnProperty('id')) {
                    formValue['id'] = data_info.id;
                }
                request(this, AppServiceUtility.app_page_config_service.update_app_config!(formValue))
                    .then((datas: any) => {
                        if (datas && datas.code === 'Success') {
                            data_info['id'] = datas['id'];
                            this.setState({
                                data_info,
                            });
                            message.info('保存成功');
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { formItemLayout } = this.props;
        let data_info = this.state.data_info;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleFeedbackSubmit}>
                    <MainContent>
                        <Form.Item label='隐私政策：'>
                            {getFieldDecorator('content', {
                                initialValue: data_info && data_info.data && data_info.data.content ? data_info.data.content : '',
                                rules: [{
                                    required: true,
                                    message: '请输入隐私政策'
                                }],
                            })(
                                <TextArea rows={20} />
                            )}
                        </Form.Item>
                    </MainContent>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                    </Row>
                </Form>
            </Row>
        );
    }
}
const FormPrivacyPolicy = Form.create<any>()(MyPrivacyPolicyForm);

class MyAboutUsForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            config_type: 'about_us',
            data_info: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.app_page_config_service.get_app_config_list!({ type: this.state.config_type }))
            .then((datas: any) => {
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((error: any) => {
                console.error(error);
            });
    }
    handleFeedbackSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['data'] = {
                    content: values['content'],
                };
                let { config_type, data_info } = this.state;
                formValue['type'] = config_type;
                if (data_info.hasOwnProperty('id')) {
                    formValue['id'] = data_info.id;
                }
                request(this, AppServiceUtility.app_page_config_service.update_app_config!(formValue))
                    .then((datas: any) => {
                        if (datas && datas.code === 'Success') {
                            data_info['id'] = datas['id'];
                            this.setState({
                                data_info,
                            });
                            message.info('保存成功');
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { formItemLayout } = this.props;
        let data_info = this.state.data_info;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleFeedbackSubmit}>
                    <MainContent>
                        <Form.Item label='关于我们：'>
                            {getFieldDecorator('content', {
                                initialValue: data_info && data_info.data && data_info.data.content ? data_info.data.content : '',
                                rules: [{
                                    required: true,
                                    message: '请输入关于我们'
                                }],
                            })(
                                <TextArea rows={20} />
                            )}
                        </Form.Item>
                    </MainContent>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                    </Row>
                </Form>
            </Row>
        );
    }
}
const FormAboutUs = Form.create<any>()(MyAboutUsForm);

/** 状态：APP设置 */
export interface AppSettingsViewState extends ReactViewState {
    tabs?: any;
}
/** 组件：APP设置 */

export class AppSettingsView extends React.Component<AppSettingsViewControl, AppSettingsViewState> {
    constructor(props: AppSettingsViewControl) {
        super(props);
        this.state = {
            tabs: {
                'function_introduction': {
                    label: '功能介绍'
                },
                'about_us': {
                    label: '关于我们',
                },
                'service_agreement': {
                    label: '服务协议',
                },
                'privacy_policy': {
                    label: '隐私政策',
                },
                'scoring_record': {
                    label: '打分记录',
                },
            },
        };
    }
    componentDidMount = () => {
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: 'APP设置', p: '编辑' })!)
            .then((datas: any) => {
                if (datas === true) {
                }
            });
    }
    changeTabs = (e: any) => {
        // console.log(e);
    }
    render() {
        const { TabPane } = Tabs;
        let { tabs } = this.state;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        return (
            <MainContent>
                <Row>
                    <Card title="APP设置">
                        <Tabs defaultActiveKey="1" onChange={this.changeTabs}>
                            {Object.keys(tabs).map((item: any, index: number) => {
                                let formCreator: any = null;
                                if (item === 'service_agreement') {
                                    formCreator = <FormServiceAgreement formItemLayout={formItemLayout} />;
                                } else if (item === 'privacy_policy') {
                                    formCreator = <FormPrivacyPolicy formItemLayout={formItemLayout} />;
                                } else if (item === 'about_us') {
                                    formCreator = <FormAboutUs formItemLayout={formItemLayout} />;
                                }
                                return (
                                    <TabPane tab={tabs[item]['label']} key={item}>
                                        {formCreator}
                                    </TabPane>
                                );
                            })}
                        </Tabs>
                    </Card>
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：APP设置控制器
 * @description APP设置
 * @author
 */
@addon('AppSettingsView', 'APP设置', 'APP设置')
@reactControl(Form.create<any>()(AppSettingsView), true)
export class AppSettingsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}