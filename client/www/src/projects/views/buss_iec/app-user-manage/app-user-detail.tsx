import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Row, Card, Input, Table, Button } from "antd";
import { request } from "src/business/util_tool";

/**
 * 组件：编辑行为能力评估项目状态
 */
export interface ChangeAppUserManageViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 用户
    user_info?: any;
}

/**
 * 组件：编辑行为能力评估项目视图
 */
export class ChangeAppUserManageView extends ReactView<ChangeAppUserManageViewControl, ChangeAppUserManageViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            user_info: {},
            id: ''
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    componentDidMount = () => {
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.get_app_user_list!({ id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    this.setState({
                        user_info: datas.result[0],
                    });
                }
            });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.appUserManage);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        let { getFieldDecorator } = this.props.form;
        let { user_info } = this.state;
        return (
            <MainContent>
                <Card title="基本信息">
                    <Row>
                        <Form {...formItemLayout}>
                            <MainContent>
                                <Form.Item label='帐号：'>
                                    {getFieldDecorator('account', {
                                        initialValue: user_info['account'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='姓名：'>
                                    {getFieldDecorator('name', {
                                        initialValue: user_info['name'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='性别：'>
                                    {getFieldDecorator('sex', {
                                        initialValue: user_info.sex || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='常住地址：'>
                                    {getFieldDecorator('address', {
                                        initialValue: user_info.address || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='联系电话：'>
                                    {getFieldDecorator('telephone', {
                                        initialValue: user_info.telephone || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                            </MainContent>
                        </Form>
                    </Row>
                </Card>
                <Card title="账户信息">
                    <Row>
                        <Form {...formItemLayout}>
                            <MainContent>
                                <Form.Item label='总账户余额：'>
                                    {getFieldDecorator('balance_all', {
                                        initialValue: user_info['balance_all'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='机构储值账户：'>
                                    {getFieldDecorator('balance_jg', {
                                        initialValue: user_info['balance_jg'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='APP储值账户：'>
                                    {getFieldDecorator('balance_app', {
                                        initialValue: user_info['balance_app'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='真实账户：'>
                                    {getFieldDecorator('balance_zs', {
                                        initialValue: user_info['balance_zs'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='补贴账户：'>
                                    {getFieldDecorator('balance_bt', {
                                        initialValue: user_info['balance_bt'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='慈善账户：'>
                                    {getFieldDecorator('balance_cs', {
                                        initialValue: user_info['balance_cs'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='已绑定的银行卡号：'>
                                    {getFieldDecorator('card_number', {
                                        initialValue: user_info.card_number || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='持卡人：'>
                                    {getFieldDecorator('card_number', {
                                        initialValue: user_info.card_name || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                            </MainContent>
                        </Form>
                    </Row>
                </Card>
                <Card title="入账记录">
                    <Table
                        columns={[
                            {
                                title: '账户类型',
                                dataIndex: 'account_type',
                                key: 'account_type',
                            },
                            {
                                title: '入账金额',
                                dataIndex: 'amount',
                                key: 'amount',
                            },
                            {
                                title: '资金来源',
                                dataIndex: 'abstract',
                                key: 'abstract',
                            },
                            {
                                title: '入账时间',
                                dataIndex: 'create_date',
                                key: 'create_date',
                            },
                            {
                                title: '备注信息',
                                dataIndex: 'remark',
                                key: 'remark',
                            },
                        ]}
                        dataSource={user_info.ruzhang_list}
                        bordered={true}
                        size="middle"
                        rowKey="id"
                        pagination={false}
                    />
                </Card>
                <Card title="消费记录">
                    <Table
                        columns={[
                            {
                                title: '消费项目',
                                dataIndex: 'abstract',
                                key: 'abstract',
                            },
                            {
                                title: '消费金额',
                                dataIndex: 'amount',
                                key: 'amount',
                            },
                            {
                                title: '消费时间',
                                dataIndex: 'create_date',
                                key: 'create_date',
                            },
                            {
                                title: '支付方式',
                                dataIndex: 'account_type',
                                key: 'account_type',
                            },
                            {
                                title: '备注信息',
                                dataIndex: 'remark',
                                key: 'remark',
                            },
                        ]}
                        dataSource={user_info.xiaofei_list}
                        bordered={true}
                        size="middle"
                        rowKey="id"
                        pagination={false}
                    />
                </Card>
                <Row type='flex' justify='center' className="ctrl-btns">
                    <Button onClick={() => history.back()} >返回</Button>
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：编辑APP用户控件
 * @description 编辑APP用户控件
 * @author
 */
@addon('ChangeAppUserManageView', '编辑APP用户控件', '编辑APP用户控件')
@reactControl(Form.create<any>()(ChangeAppUserManageView), true)
export class ChangeAppUserManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}