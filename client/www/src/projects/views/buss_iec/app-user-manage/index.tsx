
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param, getAge2 } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Form, Button, Modal, Row, message } from 'antd';
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
/** 状态：APP用户管理统计 */
export interface AppUserManageViewState extends ReactViewState {
    change_permission?: boolean;
    table_column?: any;
    ban_modal?: boolean;
    edit_type?: any;
    selected_info?: any;
}
/** 组件：APP用户管理统计 */

export class AppUserManageView extends React.Component<AppUserManageViewControl, AppUserManageViewState> {
    private form: any = null;
    constructor(props: AppUserManageViewControl) {
        super(props);
        this.state = {
            change_permission: false,
            ban_modal: false,
            selected_info: [],
            edit_type: '',
            table_column: [{
                title: "帐号",
                dataIndex: 'account',
                key: 'account',
            },
            {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '性别',
                dataIndex: 'sex',
                key: 'sex',
            },
            {
                title: '年龄',
                dataIndex: 'id_card',
                key: 'id_card',
                render(text: any, record: any) {
                    return getAge2(text);
                }
            },
            {
                title: '联系电话',
                dataIndex: 'telephone',
                key: 'telephone',
            },
            {
                title: '常住地址',
                dataIndex: 'address',
                key: 'address',
            },
            {
                title: '注册时间',
                dataIndex: 'create_date',
                key: 'create_date',
            },
            {
                title: '帐号状态',
                dataIndex: 'is_ban',
                key: 'is_ban',
                render: (text: any, record: any) => {
                    if (text === true) {
                        return '冻结';
                    } else {
                        return '正常';
                    }
                }
            },
            ],
        };
    }
    onRef = (ref: any) => {
        this.form = ref;
    }
    zdyFunction(type: any, record: any) {
        this.setState(
            {
                selected_info: record,
            },
            () => {
                if (type === 'detail') {
                    this.props.history!.push(ROUTE_PATH.changeAppUserManage + '/' + record.id);
                } else if (type === 'freeze') {
                    this.toggleModal(true, type);
                } else if (type === 'reboot') {
                    this.toggleModal(true, type);
                } else if (type === 'edit') {
                    this.props.history!.push(ROUTE_PATH.changePersonnel + '/' + record.id);
                }
            }
        );
    }
    toggleModal(type: boolean, type2: any) {
        this.setState({
            ban_modal: type,
            edit_type: type2,
        });
    }
    handleOk() {
        let param: any = {
            id: this.state.selected_info.id,
            is_ban: this.state.edit_type === 'reboot' ? false : true,
        };
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.change_app_user!(param)!)
            .then((datas: any) => {
                this.toggleModal(false, '');
                if (datas === '操作成功') {
                    message.info('操作成功！', 1, () => {
                        if (this.form && this.form!.reflash) {
                            this.form!.reflash();
                        }
                    });
                } else {
                    message.info(datas);
                }
            });
    }
    componentDidMount = () => {
        let that: any = this;
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: 'APP用户管理', p: '编辑' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.state.table_column.push({
                        title: '操作',
                        dataIndex: 'action',
                        key: 'action',
                        render(text: any, record: any) {
                            return (
                                <div>
                                    <Button size="small" type="primary" onClick={() => that.zdyFunction('detail', record)}>详情</Button>
                                    &nbsp;
                                    &nbsp;
                                    <Button size="small" type="primary" onClick={() => that.zdyFunction('edit', record)}>编辑</Button>
                                    &nbsp;
                                    &nbsp;
                                    {record.is_ban && record.is_ban === true ? <Button size="small" onClick={() => that.zdyFunction('reboot', record)}>重启</Button> : <Button size="small" onClick={() => that.zdyFunction('freeze', record)}>冻结</Button>}
                                </div>
                            );
                        }
                    });
                }
                this.setState({
                    change_permission: datas,
                });
            });
    }
    render() {
        // const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "帐号",
                    decorator_id: "account",
                    option: {
                        placeholder: "请输入帐号",
                    },
                },
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入姓名",
                    },
                },
                {
                    type: InputType.input,
                    label: "联系方式",
                    decorator_id: "telephone",
                    option: {
                        placeholder: "请输入联系方式",
                    },
                },
            ],
            // search_cb: this.queryBtn,
            columns_data_source: this.state.table_column,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_app_user_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="提示"
                    visible={this.state.ban_modal}
                    onOk={() => this.handleOk()}
                    onCancel={() => this.toggleModal(false, '')}
                >
                    <Row type="flex" justify="center" align="middle">
                        {this.state.edit_type === 'freeze' ? `确定要冻结帐号"${this.state.selected_info.account}"的用户吗？` : `确定要重启帐号"${this.state.selected_info.account}"的用户吗？`}
                    </Row>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：APP用户管理统计控制器
 * @description APP用户管理统计
 * @author
 */
@addon('AppUserManageView', 'APP用户管理统计', 'APP用户管理统计')
@reactControl(Form.create<any>()(AppUserManageView), true)
export class AppUserManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}