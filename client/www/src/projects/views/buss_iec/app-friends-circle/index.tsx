
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form, Button } from 'antd';
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
// let { Option } = Select;
/** 状态：APP老友圈 */
export interface AppFriendsCircleViewState extends ReactViewState {
}
/** 组件：APP老友圈 */

export class AppFriendsCircleView extends React.Component<AppFriendsCircleViewControl, AppFriendsCircleViewState> {
    private columns_data_source = [{
        title: "发帖人",
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '内容',
        dataIndex: 'content',
        key: 'content',
    },
    {
        title: '发帖时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '评论数',
        dataIndex: 'comment_num',
        key: 'comment_num',
    },
    {
        title: '点赞总数',
        dataIndex: 'clike_num',
        key: 'clike_num',
    },
    {
        title: '操作',
        dataIndex: 'action2',
        key: 'action2',
        render: (text: any, record: any) => {
            return (
                <Button size="small" type="primary" onClick={() => { this.toDetail(record.id); }}>详情</Button>
            );
        }
    },
    ];
    constructor(props: AppFriendsCircleViewControl) {
        super(props);
        this.state = {
        };
    }
    toDetail = (id: any) => {
        if (id) {
            this.props.history!.push(ROUTE_PATH.changeAppFriendCircle + '/' + id);
        }
    }
    componentDidMount = () => {
    }
    render() {
        // const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "发帖人",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入发帖人',
                    },
                },
                {
                    type: InputType.rangePicker,
                    label: "发帖时间",
                    decorator_id: "date_range",
                    option: {
                        placeholder: ['开始时间', '结束时间'],
                    },
                },
            ],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.friends_circle_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
            </div>
        );
    }
}

/**
 * 控件：APP老友圈控制器
 * @description APP老友圈
 * @author
 */
@addon('AppFriendsCircleView', 'APP老友圈', 'APP老友圈')
@reactControl(Form.create<any>()(AppFriendsCircleView), true)
export class AppFriendsCircleViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}