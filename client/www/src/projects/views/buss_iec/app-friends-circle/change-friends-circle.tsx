import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Row, Card, Input, Button, Col } from "antd";
import { request } from "src/business/util_tool";
import TextArea from "antd/lib/input/TextArea";

/**
 * 组件：编辑APP老友圈状态
 */
export interface ChangeAppFriendsCircleViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 数据
    data_info?: any;
}

/**
 * 组件：编辑APP老友圈视图
 */
export class ChangeAppFriendsCircleView extends ReactView<ChangeAppFriendsCircleViewControl, ChangeAppFriendsCircleViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: {},
            id: '',
        };
    }
    componentDidMount = () => {
        this.getInfo();
    }
    getInfo() {
        request(this, AppServiceUtility.friends_circle_service.get_friends_circle_list!({ id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    let data_info = datas.result[0];
                    // 点赞用户列表
                    let like_user_list: any = [];
                    if (data_info.like_list && data_info.like_list.length > 0) {
                        for (let i = 0; i < data_info.like_list.length; i++) {
                            if (data_info.like_list[i].name) {
                                like_user_list.push(data_info.like_list[i].name);
                            }
                        }
                    }
                    data_info['like_user_list'] = like_user_list.join(',');
                    this.setState({
                        data_info,
                    });
                }
            });
    }
    backList() {
        this.props.history!.push(ROUTE_PATH.appFriendCircle);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        let { getFieldDecorator } = this.props.form;
        let { data_info } = this.state;
        return (
            <MainContent>
                <Card title="发帖信息">
                    <Row>
                        <Form {...formItemLayout}>
                            <MainContent>
                                <Form.Item label='发帖人：'>
                                    {getFieldDecorator('name', {
                                        initialValue: data_info['name'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='内容：'>
                                    {getFieldDecorator('content', {
                                        initialValue: data_info['content'] || '',
                                    })(
                                        <TextArea autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='发帖时间：'>
                                    {getFieldDecorator('create_date', {
                                        initialValue: data_info['create_date'],
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                            </MainContent>
                        </Form>
                    </Row>
                </Card>
                <Card title="帖子互动">
                    <Row>
                        <Form {...formItemLayout}>
                            <MainContent>
                                <Form.Item label='点赞总数：'>
                                    {getFieldDecorator('clike_num', {
                                        initialValue: data_info.hasOwnProperty('clike_num') ? data_info['clike_num'] : '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='点赞用户列表'>
                                    {getFieldDecorator('like_user_list', {
                                        initialValue: data_info['like_user_list'] || '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='评论总数：'>
                                    {getFieldDecorator('comment_num', {
                                        initialValue: data_info.hasOwnProperty('comment_num') ? data_info['clike_num'] : '',
                                    })(
                                        <Input autoComplete="off" disabled={true} />
                                    )}
                                </Form.Item>
                                {data_info.comment_list && data_info.comment_list.length > 0 ? data_info.comment_list.map((item: any, index: number) => {
                                    return (
                                        <Row key={index}>
                                            <Col span={12}>
                                                <Form.Item label={`评论用户${index + 1}：`}>
                                                    {getFieldDecorator(`comment_${index}：`, {
                                                        initialValue: item['name'] || ''
                                                    })(
                                                        <Input autoComplete="off" disabled={true} />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                            <Col span={12}>
                                                <Form.Item label={`评论内容：`}>
                                                    {getFieldDecorator(`content_${index}：`, {
                                                        initialValue: item['content'] || ''
                                                    })(
                                                        <Input autoComplete="off" disabled={true} />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    );
                                }) : null}
                            </MainContent>
                        </Form>
                    </Row>
                </Card>
                <Row type='flex' justify='center' className="ctrl-btns">
                    <Button onClick={() => this.backList()} >返回</Button>
                    {/* <Button onClick={() => this.getInfo()} type="primary">刷新</Button> */}
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：编辑APP老友圈控件
 * @description 编辑APP老友圈控件
 * @author
 */
@addon('ChangeAppFriendsCircleView', '编辑APP老友圈控件', '编辑APP老友圈控件')
@reactControl(Form.create<any>()(ChangeAppFriendsCircleView), true)
export class ChangeAppFriendsCircleViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}