
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Form } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
/** 状态：南海呼叫中心帐号 */
export interface CallCenterZhViewState extends ReactViewState {
}
/** 组件：南海呼叫中心帐号 */
export class CallCenterZhView extends React.Component<CallCenterZhViewControl, CallCenterZhViewState> {
    private columns_data_source = [
        {
            title: '工作人员名称',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '坐席工号',
            dataIndex: 'agentId',
            key: 'agentId',
        }, {
            title: '坐席分机号',
            dataIndex: 'phoneNum',
            key: 'phoneNum',
        },
        {
            title: '外显号码',
            dataIndex: 'disPlayNumber',
            key: 'disPlayNumber',
        }, {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
        }, {
            title: '最后修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }, {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }
    ];
    constructor(props: CallCenterZhViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount = () => {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeCallCenterZh + '/' + contents.id);
        }
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeCallCenterZh);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        let call_center_zh = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '工作人员名称',
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入工作人员名称',
                    }
                },
                {
                    type: InputType.input,
                    label: '坐席工号',
                    decorator_id: "agentId",
                    option: {
                        placeholder: '请输入坐席工号',
                    }
                },
                {
                    type: InputType.input,
                    label: '坐席分机号',
                    decorator_id: "phoneNum",
                    option: {
                        placeholder: '请输入坐席分机号',
                    }
                },
                {
                    type: InputType.input,
                    label: '备注',
                    decorator_id: "remark",
                    option: {
                        placeholder: '请输入备注',
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.call_center_nh_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'delete_call_center_zh'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
        };
        let call_center_zh_list = Object.assign(call_center_zh, table_param);
        return (
            <div>
                <SignFrameLayout {...call_center_zh_list} />
            </div>
        );
    }
}

/**
 * 控件：南海呼叫中心帐号控制器
 * @description 南海呼叫中心帐号
 * @author
 */
@addon('CallCenterZhView', '南海呼叫中心帐号', '南海呼叫中心帐号')
@reactControl(Form.create<any>()(CallCenterZhView), true)
export class CallCenterZhViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}