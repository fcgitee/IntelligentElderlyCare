import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { Card, Row, Form, Col, Input, Select, Button, message, Tabs, Modal, Icon, Mentions, notification, Radio, List, Typography } from 'antd';
import "./index.less";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { WhiteSpace } from "antd-mobile";
import TextArea from "antd/lib/input/TextArea";
import { getAge2 } from "src/projects/app/util-tool";
import { BaiduMap } from "../call-center/map";
import moment from 'moment';
const Option = Select.Option;
const { TabPane } = Tabs;
const { Text } = Typography;
/**
 * 组件：南海呼叫中心
 */
export interface CallCenterNhViewState extends ReactViewState {
    form_values?: any;
    headers?: any;
    is_register?: any;
    is_signin?: any;
    is_busy?: any;
    is_calling?: any;
    record_show?: boolean;
    edit_show?: boolean;
    transform_show?: boolean;
    record_url?: any;
    locationId?: any;
    selected_data?: any;
    elder_info?: any;
    remark?: any;
    full_screen?: boolean;
    organization_info?: any;
    zh_list?: any;
    zh_no_list?: any;
    selected_transout_zh?: any;
    calling_phone?: any;
    is_holding?: boolean;
    is_talking?: boolean;
    is_callouting?: boolean;
    out_number?: any;
    transform_type?: any;
    incall_show?: boolean;
    is_agh_showing?: boolean;
    agh_result?: string;
    warning_waiting: boolean;
    error_times: number;
    is_trying_resign: boolean;
    uuid: string;
    is_log_show?: boolean;
    log_datas: any;
    dsn?:any;
}

/**
 * 组件：南海呼叫中心
 * 描述
 */
export class CallCenterNhView extends ReactView<CallCenterNhViewControl, CallCenterNhViewState> {
    // 表单构造器的映射
    private formCreater: any = null;
    // 紧急呼叫弹窗的储存标识
    private emergency_key: any = null;
    // 爱关怀的弹窗表示
    private aiguanhuai_key: any = null;
    // 轮询时间
    private pool_time: number = 100;
    // 录音列表的配置
    private callRecordListColumns: any = [
        {
            title: '主叫电话',
            dataIndex: 'caller_no',
            key: 'caller_no',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '被叫电话',
            dataIndex: 'called_no',
            key: 'called_no',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '坐席工号',
            dataIndex: 'agent_id',
            key: 'agent_id',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '长者姓名',
            dataIndex: 'elder_name',
            key: 'elder_name',
            width: 100,
            render: (text: any, record: any) => {
                if (record.relations) {
                    if (text) {
                        return `${text}【${record.relations}】`;
                    } else {
                        return `【${record.relations}】`;
                    }
                } else {
                    return (text || '-');
                }
            }
        },
        {
            title: '坐席人员',
            dataIndex: 'cs_name',
            key: 'cs_name',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '呼叫类型',
            dataIndex: 'call_type',
            key: 'call_type',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '紧急呼叫',
            dataIndex: 'emergency',
            key: 'emergency',
            width: 100,
            render: (text: any, record: any) => {
                if (record.emergency === 'yes') {
                    return '是';
                } else {
                    return '否';
                }
            },
        },
        {
            title: '是否接通',
            dataIndex: 'pickup',
            key: 'pickup',
            width: 100,
            render: (text: any, record: any) => {
                if (record.pickup === true) {
                    return '接通';
                } else {
                    return <span style={{ color: 'red' }}>未接通</span>;
                }
            },
        },
        {
            title: '进入系统时间',
            dataIndex: 'create_date',
            key: 'create_date',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '通话开始时间',
            dataIndex: 'begin_time',
            key: 'begin_time',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '通话结束时间',
            dataIndex: 'end_time',
            key: 'end_time',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '接听时间（秒）',
            dataIndex: 'call_time',
            key: 'call_time',
            width: 100,
            render: (text: any, record: any) => {
                if (record.hasOwnProperty('begin_time') && record.hasOwnProperty('end_time')) {
                    return (
                        this.getSecondsFromRange(record.begin_time, record.end_time)
                    );
                }
                return '-';
            },
        },
        {
            title: '录音播放',
            dataIndex: 'sound_play',
            key: 'sound_play',
            render: (text: any, record: any) => {
                if (record.hasOwnProperty('record_file_name')) {
                    return (
                        <Icon
                            type="play-square"
                            theme="twoTone"
                            style={{ fontSize: '30px' }}
                            onClick={
                                () => {
                                    this.getAudioPlay(record.record_file_name, record.locationId);
                                }
                            }
                        />
                    );
                }
                return '-';
            },
            width: 100,
        },
        {
            title: '挂断方',
            dataIndex: 'cut_type',
            key: 'cut_type',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '未应答时长（秒）',
            dataIndex: 'e_300_datas',
            key: 'e_300_datas',
            width: 100,
            render: (text: any, record: any) => {
                if (!record.hasOwnProperty('e_304_dates')) {
                    let seconds: number = this.getSecondsFromRange(record.e_300_dates, record.e_301_dates);
                    return (
                        record.call_type === '呼出' ? (seconds > 19 ? (seconds - 19) : seconds) : seconds
                    );
                }
                return '-';
            },
        },
        {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
            width: 100,
            render: (text: any, record: any) => {
                return (text || '-');
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return <Button type="primary" onClick={() => { this.edit(record); }}>编辑</Button>;
            },
            width: 150,
        },
    ];
    // 通过录音原始地址获取真实下载地址
    getAudioPlay = (record_file_name?: any, locationId?: any) => {
        this.setState(
            {
                record_url: record_file_name,
                locationId: locationId,
            },
            () => {
                this.getRecordUrl();
            }
        );
    }
    constructor(props: any) {
        super(props);
        this.state = {
            // 移动账号配置
            form_values: {
                // 坐席工号
                // agentId: '',
                // 坐席密码
                // password: '',
                // 坐席技能组
                // skillIds: '',
                // 坐席号码
                // phoneNum: '',
                // 呼叫中心标识
                // ccId: '',
                // 坐席状态
                // agentState: '3',
                // 软电话诶性
                // phoneStyle: false,
                // 外显号码
                // disPlayNumber: '',
                // 呼出的号码
                // number: '',
            },
            // 是否注册，null：未尝试注册，false：注册失败，true：注册成功
            is_register: null,
            // 是否签入，false：签入失败，true：签入成功
            is_signin: false,
            // 是否繁忙，false：当前坐席闲，true：当前坐席忙
            is_busy: false,
            // 是否拨号状态，false：否，true：是。不代表通话中
            is_talking: false,
            // 是否通话中，false：否，true：是
            is_calling: false,
            // 是否保持通话，false：否，true：是
            is_holding: false,
            // 是否外呼
            is_callouting: false,
            // 录音弹窗是否显示
            record_show: false,
            // 编辑弹窗是否显示
            edit_show: false,
            // 转接弹窗是否显示
            transform_show: false,
            // 地图是否全屏
            full_screen: false,
            // 录音地址，流程中首先是原始地址，最后会赋值加密后的下载地址
            record_url: '',
            locationId: '',
            // 备注
            remark: '',
            // 编辑时存起的数据
            selected_data: {},
            // 呼叫中心帐号列表
            zh_list: [],
            // 正在呼叫的手机号码
            calling_phone: '',
            // 转接的外呼号码
            out_number: '',
            // 选中的转接坐席
            selected_transout_zh: '',
            // 转接模式，默认2，2为坐席，5为号码
            transform_type: '2',
            // 应答弹窗是否显示
            incall_show: false,
            // 是否正在显示爱关怀
            is_agh_showing: false,
            // 爱关怀处理结果
            agh_result: '',
            // 爱关怀等待处理结果，在为true之前不允许弹出，防止重复
            warning_waiting: false,
            // 错误计时
            error_times: 0,
            // 是否重新尝试签入
            is_trying_resign: false,
            // 唯一索引
            uuid: this.getUniqueKey(),
            // 日志界面是否显示
            is_log_show: false,
            // 日志数据
            log_datas: [],
            // 暂存dsn
            dsn:''
        };
    }
    // 初始化
    componentDidMount = () => {

        // 获取移动呼叫中心帐号列表，用于呼叫转接
        request(this, AppServiceUtility.call_center_nh_service.get_call_center_zh_list!({}))
            .then((data: any) => {
                if (data && data.result) {
                    let zh_no_list: any = [];
                    for (let i = 0; i < data.result.length; i++) {
                        zh_no_list.push(data.result[i]['phoneNum']);
                    }
                    this.setState({
                        zh_list: data.result,
                        zh_no_list,
                    });
                }
            })
            .catch((error: any) => {
                this.insertLog(`【获取移动呼叫中心帐号列表】【${error}】`, 'f');
            });

        // 获取当前账号绑定的移动呼叫中心帐号
        request(this, AppServiceUtility.call_center_nh_service.api!('get_zh', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    this.setState(
                        {
                            form_values: {
                                agentId: data.result.agentId,
                                password: data.result.password,
                                skillIds: data.result.skillIds,
                                phoneNum: data.result.phoneNum,
                                ccId: data.result.ccId,
                                agentState: data.result.agentState,
                                phoneStyle: false,
                                disPlayNumber: data.result.disPlayNumber,
                                number: '',
                            },
                        }
                    );
                }
            })
            .catch((error: any) => {
                this.insertLog(`【获取当前绑定帐号】【${error}】`, 'f');
            });

        // 获取机构定位
        request(this, AppServiceUtility.call_center_nh_service.api!('get_location', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    this.setState({
                        organization_info: data.result,
                    });
                }
            })
            .catch((error: any) => {
                this.insertLog(`【获取机构定位】【${error}】`, 'f');
            });

        // 地图组件全屏事件监听
        const mapEle = document.getElementById("olderMap")!;
        mapEle.addEventListener("fullscreenchange", (e) => {
            if (document.fullscreenElement === null) {
                this.setState({
                    full_screen: false
                });
            }
        });

        let sessionStorage: any = window.sessionStorage;

        if (sessionStorage.getItem("agent_state")) {
            this.insertLog(`【重新返回系统】`);
            // 给window对象挂载新的uuid，用来终结旧循环
            window['uuid'] = this.state.uuid;
            window.setTimeout(
                () => {
                    this.getPool();
                },
                this.pool_time
            );
            this.setState({
                is_signin: true,
                is_register: true,
                is_busy: sessionStorage.getItem('agent_state') === '3' ? true : false,
            });
        }
        window.onbeforeunload = null;
        window.onbeforeunload = () => {
            // 这句话没用的，这个函数只是用来弹个窗
            return '确定要退出系统吗？';
        };

        window.onunload = () => {
            // 即时注销
            this.unregister();
            // 即时签出
            this.signOut();
        };
    }
    // 根据时间获取分钟为单位的计数
    getMinutesFromRange = (begin_time: any, end_time: any) => {
        return Number(end_time && begin_time ? (((new Date(end_time).getTime() - new Date(begin_time).getTime()) % 3600000) / 60000).toFixed(2) : 0);
    }
    // 根据时间获取秒为单位的计数
    getSecondsFromRange = (begin_time: any, end_time: any) => {
        return Number(end_time && begin_time ? ((new Date(end_time).getTime() - new Date(begin_time).getTime()) / 1000) : 0);
    }
    // 获取唯一索引
    getUniqueKey() {
        return new Date().getTime() + '' + Math.ceil(Math.random() * 10) + '';
    }
    // 输入备注事件
    remarkChange = (e: any) => {
        this.setState({
            remark: e.target.value
        });
    }
    // 打开编辑弹窗
    edit = (data: any) => {
        // 打开弹框
        this.setState({
            selected_data: data,
            remark: data.remark,
            edit_show: true
        });
    }
    componentWillUnmount = () => {
        window.onbeforeunload = null;
        this.emergency_key && notification.close(this.emergency_key);
        this.aiguanhuai_key && notification.close(this.aiguanhuai_key);
        this.setPhoneNotAuto();
        this.setState = () => false;
    }
    // 注册软电话
    register = (callback: Function) => {
        let form_values = this.state.form_values;
        if (!form_values.phoneNum || !form_values.password) {
            message.info('注册软电话请输入坐席工号与坐席密码！');
            return;
        }
        $.ajax({
            url: 'http://127.0.0.1:23412/sipphone/register',
            method: "POST",
            data: JSON.stringify({
                "userName": form_values.phoneNum,
                "userPasswd": form_values.password,
                "domin": "221.181.128.232:55066"
            }),
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': '*/*'
            },
            success: (res: any) => {
                if (res.message !== 'success') {
                    this.insertLog(`【软电话注册失败】【${JSON.stringify(res)}】`, 'f');
                    this.setState({
                        is_register: false,
                    });
                } else {
                    this.insertLog(`【软电话注册成功】【${JSON.stringify(res)}】`);
                    this.setState(
                        {
                            is_register: true,
                        },
                        () => {
                            this.setPhoneNotAuto();
                            callback && callback();
                        }
                    );
                }
            },
            error: (err: any) => {
                message.info('软电话注册失败！');
                this.insertLog(`【软电话注册失败】【${JSON.stringify(err)}】`, 'f');
                this.setState({
                    is_register: false,
                });
            }
        });
    }
    // 注销软电话
    unregister = () => {
        $.ajax({
            url: 'http://127.0.0.1:23412/sipphone/unregister',
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': '*/*'
            },
            success: (res: any) => {
                if (res.message !== 'success') {
                    this.unregisterError(res);
                } else {
                    this.setState({
                        is_register: null,
                        error_times: 0,
                    });
                    this.insertLog(`【软电话注销成功】【${JSON.stringify(res)}】`);
                }
            },
            error: (err: any) => {
                this.unregisterError(err);
            }
        });
    }
    // 注销软电话失败的方法
    unregisterError = (res: any) => {
        let error_times = this.state.error_times;
        if (error_times > 5) {
            message.error('软电话注销失败，请重启系统！');
            return;
        }
        this.insertLog(`【软电话软电话注销失败】【${JSON.stringify(res)}】【启动重新尝试】【重新尝试次数${++error_times}】`, 'f');
        // 重新尝试
        this.setState(
            {
                error_times,
            },
            () => {
                window.setTimeout(
                    () => {
                        this.unregister();
                    },
                    200
                );
            }
        );
    }
    // 设置软电话手动应答，呼入必须手动应答否则自动接通
    setPhoneNotAuto = () => {
        // 非注册模式无需操作
        if (!this.state.is_register) {
            return;
        }
        $.ajax({
            url: 'http://127.0.0.1:23412/sipphone/setautoAnswer',
            method: "POST",
            data: JSON.stringify({
                "answerType": "0",
            }),
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': '*/*'
            },
            success: (res: any) => {
                if (res.message !== 'success') {
                    this.setPhoneNotAutoError(res);
                } else {
                    this.setState({
                        error_times: 0,
                    });
                    this.insertLog(`【设置软电话手动应答模式成功】【${JSON.stringify(res)}】`);
                }
            },
            error: (err: any) => {
                this.setPhoneNotAutoError(err);
            }
        });
    }
    // 设置软电话手动应答失败的方法
    setPhoneNotAutoError = (res: any) => {
        let error_times = this.state.error_times;
        if (error_times > 5) {
            this.setState({
                error_times: 0,
            });
            message.error('设置软电话手动应答失败，请重启系统！');
            return;
        }
        this.insertLog(`【设置软电话手动应答模式失败】【${JSON.stringify(res)}】【启动重新尝试】【重新尝试次数${++error_times}】`, 'f');
        // 重新尝试
        this.setState(
            {
                error_times,
            },
            () => {
                window.setTimeout(
                    () => {
                        this.setPhoneNotAuto();
                    },
                    200
                );
            }
        );
    }
    // 设置软电话自动应答，外呼必须自动应答否则没反应
    setPhoneAuto = (Func?: Function) => {
        // 非注册模式无需操作
        if (!this.state.is_register) {
            return;
        }
        $.ajax({
            url: 'http://127.0.0.1:23412/sipphone/setautoAnswer',
            method: "POST",
            data: JSON.stringify({
                "answerType": "1",
            }),
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': '*/*'
            },
            success: (res: any) => {
                if (res.message !== 'success') {
                    this.setPhoneAutoError(res, Func);
                } else {
                    // 清空错误次数
                    this.setState({
                        error_times: 0,
                    });
                    // 插入日志
                    this.insertLog(`【设置软电话自动应答模式成功】【${JSON.stringify(res)}】`);
                    // 执行回调函数
                    Func && Func();
                }
            },
            error: (err: any) => {
                this.setPhoneAutoError(err);
            }
        });
    }
    // 设置软电话自动应答失败的方法
    setPhoneAutoError = (res: any, Func?: Function) => {
        let error_times = this.state.error_times;
        // 5次以内重新尝试
        if (error_times > 5) {
            this.setState({
                error_times: 0,
            });
            message.error('设置软电话自动应答失败，请重启系统！');
            return;
        }
        this.insertLog(`【设置软电话自动应答模式失败】【${JSON.stringify(res)}】【启动重新尝试】【重新尝试次数${++error_times}】`, 'f');
        // 重新尝试
        this.setState(
            {
                error_times,
            },
            () => {
                window.setTimeout(
                    () => {
                        this.setPhoneAuto(Func);
                    },
                    200
                );
            }
        );
    }
    // 表单设置方法
    setFormValues = (e: any, key: any, type: any = 'input') => {
        let form_values = this.state.form_values;
        if (type === 'input') {
            form_values[key] = e.target.value;
        } else {
            form_values[key] = e;
        }
        this.setState({
            form_values,
        });
    }
    // 坐席签入操作，need_loading为false时静默签入
    signIn = (need_loading: boolean) => {
        this.register(() => {
            let { form_values } = this.state;
            if (!form_values.agentId || !form_values.ccId || !form_values.phoneNum) {
                message.error("请输入签入数据！");
                return;
            }
            let call_center_service = need_loading === true ? AppServiceUtility.call_center_nh_service : AppServiceUtility.call_center_nh_no_loading_service;
            request(this, call_center_service.api!('login', {
                agentId: form_values.agentId,
                ccId: form_values.ccId,
                password: form_values.password,
                phoneNum: form_values.phoneNum,
                skillIds: "['" + form_values.skillIds + "']",
                isForceLogon: "true",
                domin: '221.181.128.232:55066',
                agentState: form_values.agentState
            }))
                .then((data: any) => {
                    if (data.code === 200) {
                        this.insertLog(`【坐席签入成功】`);
                        // 需要提示
                        if (need_loading === true) {
                            message.info('坐席签入成功！');
                        }
                        window.setTimeout(
                            () => {
                                this.getPool();
                            },
                            this.pool_time
                        );
                        this.setState(
                            {
                                is_signin: true,
                                is_busy: form_values.agentState === '3' ? true : false,
                            },
                            () => {
                                // 储存，为了不断签到
                                sessionStorage.setItem("agent_state", form_values.agentState);
                            }
                        );
                    } else {
                        if (typeof (data.msg) === 'string') {
                            message.error(`签入失败：【${data.msg}】`);
                        } else {
                            message.error('签入失败！');
                        }
                    }
                })
                .catch((error: any) => {
                    this.insertLog(`【坐席签入】【${error}】`, 'f');
                });
        });
    }
    // 签出操作
    signOut = (force: boolean = false) => {
        request(this, AppServiceUtility.call_center_nh_service.api!('logout', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('坐席签出成功！', 1);
                    this.insertLog(`【坐席签出成功】`);
                    this.onSignOut();
                } else {
                    if (typeof (data.msg) === 'string') {
                        if (data.msg === '坐席已签出') {
                            message.info('签出成功！');
                            this.onSignOut();
                        } else {
                            message.error(`签出失败：【${data.msg}】`, 2, () => {
                                this.onSignOut();
                                window['uuid'] = undefined;
                                window.location.reload();
                            });
                        }
                    } else {
                        message.error('签出失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【坐席签出】${error}`, 'f');
            }).
            finally(() => {
                // 强制签出
                if (force) {
                    this.onSignOut();
                }
            });
    }
    // 签出时的操作
    onSignOut = () => {
        window.setTimeout(
            () => {
                let toEnd: any = setTimeout(function () { }, 1);
                for (let i = 0; i <= toEnd + 10; i++) {
                    window.clearTimeout(i);
                }
            },
            1200,
        );
        // 用于清空定时器
        sessionStorage.removeItem("agent_state");
        this.setState({
            is_signin: false,
            is_calling: false,
            is_holding: false,
            is_talking: false,
            is_callouting: false,
            incall_show: false,
            record_show: false,
            transform_show: false,
            warning_waiting: false,
            elder_info: {},
        });
        this.emergency_key && notification.close(this.emergency_key);
        this.aiguanhuai_key && notification.close(this.aiguanhuai_key);
        this.aiguanhuai_key = null;
        this.setPhoneNotAuto();
        // 注销软电话
        this.unregister();
    }
    // 挂机时的操作
    onHangUp = () => {
        this.setState({
            is_calling: false,
            is_holding: false,
            is_talking: false,
            is_callouting: false,
            incall_show: false,
            warning_waiting: false,
            elder_info: {},
        });
        this.emergency_key && notification.close(this.emergency_key);
        this.aiguanhuai_key && notification.close(this.aiguanhuai_key);
        this.aiguanhuai_key = null;
        this.setPhoneNotAuto();
    }
    // 外呼操作
    doCall = () => {
        let form_values = this.state.form_values;
        if (!form_values.number) {
            message.error("请输入外呼号码！");
            return;
        }
        if (!form_values.disPlayNumber) {
            message.error("请输入外显号码！");
            return;
        }
        let db_telephone = form_values.number + '';
        if (form_values.number.length === 12 && form_values.number.split('')[0] === '0') {
            db_telephone = form_values.number.substr(1);
        }
        this.setState(
            {
                is_callouting: true,
            },
            () => {
                this.getUser(db_telephone, '');
            }
        );
        this.setPhoneAuto(() => {
            request(this, AppServiceUtility.call_center_nh_service.api!('callout', {
                calledDigits: form_values.number,
                callerDigits: form_values.disPlayNumber,
                agentId: form_values.agentId,
            }))
                .then((data: any) => {
                    if (data.code === 200) {
                        message.info('呼叫成功！');
                        this.setState({
                            is_callouting: true,
                        });
                    } else {
                        if (typeof (data.msg) === 'string') {
                            if (data.msg === '坐席已签出') {
                                message.error(`呼叫失败：【${data.msg}】`);
                                this.onSignOut();
                            } else {
                                message.error(`呼叫失败：【${data.msg}】`);
                            }
                        } else {
                            message.error('呼叫失败！');
                        }
                        this.setState({
                            calling_phone: form_values.disPlayNumber,
                            is_callouting: false,
                            elder_info: {},
                        });
                    }
                })
                .catch((error: any) => {
                    this.insertLog(`【外呼】【${error}】`, 'f');
                    this.setState({
                        elder_info: {},
                        is_callouting: false,
                    });
                });
        });
    }
    // 爱关怀更新状态
    updateAGH = (condition: any, need_alert: boolean = true) => {
        request(this, AppServiceUtility.call_center_nh_service.update_agh!(condition))
            .then((data: any) => {
                if (data.code === 200) {
                    if (need_alert) {
                        // 两秒后重新轮询
                        message.info('提交成功！', 2, () => {
                            this.setState({
                                warning_waiting: false,
                            });
                        });
                    } else {
                        setTimeout(
                            () => {
                                this.setState({
                                    warning_waiting: false,
                                });
                            },
                            this.pool_time,
                        );
                    }
                } else {
                    // 两秒后重新轮询
                    message.info(data.msg, 2, () => {
                        this.setState({
                            warning_waiting: false,
                        });
                    });
                }
            })
            .catch((error: any) => {
                this.insertLog(`【爱关怀处理】【${error}】`, 'f');
            });
    }
    setAGHStatus = (condition: any) => {
        this.setState(
            {
                elder_info: {},
                warning_waiting: true,
            },
            () => {
                notification.close(this.aiguanhuai_key);
                this.aiguanhuai_key = null;
                this.updateAGH(condition);
            }
        );
    }
    // 状态轮询，包含呼叫中心轮询和爱关怀警报轮询
    getPool = () => {
        let { is_signin, is_calling, warning_waiting, agh_result, form_values, zh_no_list } = this.state;
        if (this.props.match!.path !== window.location.pathname) {
            warning_waiting = true;
        }
        request(this, AppServiceUtility.call_center_nh_no_loading_service.api!('getpool', { pause_agh: warning_waiting }))
            .then((data: any) => {
                if (window['uuid'] && window['uuid'] !== this.state.uuid) {
                    // 结束轮询
                    this.insertLog('【清除上一次进入页面未销毁的轮询】');
                    return;
                }
                // 至少正常返回数据，把错误机制去掉
                this.setState({
                    error_times: 0,
                    is_trying_resign: false,
                });
                if (data.code === 200) {
                    // 爱关怀的轮询
                    if (data.warn_info.hasOwnProperty('id') && !is_calling) {
                        this.setState(
                            {
                                elder_info: data.warn_info.user_info,
                            },
                            () => {
                                if (!this.aiguanhuai_key && !warning_waiting) {
                                    // 设置爱关怀的轮询再弹窗
                                    this.setState(
                                        {
                                            warning_waiting: true,
                                        },
                                        () => {
                                            const key = `open${Date.now()}`;
                                            this.aiguanhuai_key = key;
                                            const btn: any = (
                                                <Row>
                                                    <Button
                                                        type="primary"
                                                        size="small"
                                                        onClick={
                                                            () => {
                                                                this.setAGHStatus({
                                                                    id: data.warn_info.id,
                                                                    check_status: '已处理',
                                                                    result: '暂不处理'
                                                                });
                                                            }
                                                        }
                                                    >
                                                        暂不处理
                                                    </Button>
                                                    &nbsp;
                                                    &nbsp;
                                                    <Button
                                                        type="primary"
                                                        size="small"
                                                        onClick={
                                                            () => {
                                                                if (!agh_result) {
                                                                    message.info('请输入处理结果！');
                                                                    return;
                                                                }
                                                                this.setAGHStatus({
                                                                    id: data.warn_info.id,
                                                                    check_status: '已处理',
                                                                    result: agh_result
                                                                });
                                                            }
                                                        }
                                                    >
                                                        提交处理结果
                                                    </Button>
                                                    &nbsp;
                                                    &nbsp;
                                                    <Button
                                                        type="primary"
                                                        size="small"
                                                        onClick={
                                                            () => {
                                                                if (data.warn_info && data.warn_info.user_info && data.warn_info.user_info.personnel_info && data.warn_info.user_info.personnel_info.telephone) {
                                                                    form_values['number'] = data.warn_info.user_info.personnel_info.telephone;
                                                                }
                                                                this.setState(
                                                                    {
                                                                        form_values,
                                                                    },
                                                                    () => {
                                                                        this.doCall();
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    >
                                                        拨号
                                                    </Button>
                                                </Row>
                                            );
                                            const close = () => {
                                                // 直接关闭的就是暂不处理
                                                this.setAGHStatus({
                                                    id: data.warn_info.id,
                                                    check_status: '已处理',
                                                    result: '暂不处理'
                                                });
                                            };
                                            notification['warning']({
                                                message: '爱关怀异常警报',
                                                description: (
                                                    <Row>
                                                        <Row>
                                                            警报位置：{data.warn_info.location}
                                                        </Row>
                                                        <Row>
                                                            异常内容：{data.warn_info.warn_type}
                                                        </Row>
                                                        <Row style={{ marginTop: '20px', fontSize: '16px' }}>
                                                            <TextArea onChange={(e: any) => this.changeAGHContent(e)} autoComplete='off' placeholder="请输入处理结果" />
                                                        </Row>
                                                    </Row>
                                                ),
                                                btn,
                                                key,
                                                duration: null,
                                                onClose: close,
                                            });
                                        }
                                    );
                                }
                            }
                        );
                    }

                    // 呼叫中心的轮询，后端无Cookie，前端签出
                    if (data.pool_info && data.pool_info.events) {
                        if (data.pool_info.events[0] && data.pool_info.events[0]['eventId'] === 352) {
                            // 这里是被强制签出了
                        } else {
                            // 非签出就重新设置两秒后轮询
                            window.setTimeout(
                                () => {
                                    this.getPool();
                                },
                                (data.pool_info.result && data.pool_info.result === '0' ? this.pool_time : 100)
                            );
                        }
                        this.insertLog(`【轮询】【${JSON.stringify(data.pool_info)}】`);
                        data.pool_info.events.map((item: any) => {
                            switch (item.eventId) {
                                case 300:
                                    // 此时有可能来自于坐席转接的呼叫，这时候会显示坐席的号码而不显示原始号码，需要从后端获取
                                    if (zh_no_list.indexOf(item.callerAddress) > -1) {
                                        this.getPhoneFromDsn(item.callId.dsn);
                                    } else {
                                        // 呼出的300
                                        if (item.callDirection === '2') {
                                            this.getUser(this.state.calling_phone, item.callId.dsn);
                                        } else {
                                            this.setState({
                                                incall_show: true,
                                            });
                                            this.getUser(item.callerAddress, item.callId.dsn);
                                        }
                                        // 下面是旧版本的357/0状态的判断事件
                                        // if (is_callouting === true) {
                                        //     // 呼出
                                        //     this.getUser(item.calledAddress, item.callId.dsn);
                                        // } else {
                                        //     // 呼入
                                        //     this.setState({
                                        //         incall_show: true,
                                        //     });
                                        //     this.getUser(item.callerAddress, item.callId.dsn);
                                        // }
                                    }
                                    // window.setTimeout(
                                    //     () => {
                                    //         this.formCreater && this.formCreater.reflash && this.formCreater.reflash();
                                    //     },
                                    //     3000
                                    // );
                                    this.setState({
                                        is_calling: true,
                                    });
                                    break;
                                case 301:
                                    // 通话挂机
                                    this.onHangUp();
                                    break;
                                case 305:
                                    // 接通通话
                                    this.setState({
                                        is_talking: true,
                                    });
                                    break;
                                case 351:
                                    if (item.agentState === 4) {
                                        // 示闲状态
                                        this.setState({
                                            is_busy: false,
                                        });
                                    } else if (item.agentState === 3) {
                                        // 示忙状态
                                        this.setState({
                                            is_busy: true,
                                        });
                                    }
                                    break;
                                case 352:
                                    // 手动退出状态不需要提示
                                    if (is_signin === true) {
                                        message.error('坐席被强制签出！');
                                    }
                                    this.setState({
                                        is_signin: false,
                                    });
                                    this.onSignOut();
                                    break;
                                case 357:
                                    if (item.phoneState === 2) {
                                        // 坐席挂机
                                        this.onHangUp();
                                    } else if (item.phoneState === 3) {
                                        // 用户挂机
                                        this.onHangUp();
                                    } else if (item.phoneState === 1) {
                                        // 摘机状态
                                    } else if (item.phoneState === 0) {
                                        // 响铃状态
                                    }
                                    break;
                                default: break;
                            }
                        });
                    }
                } else if (data.code === 502) {
                    // 手动退出状态不需要提示
                    if (is_signin === true) {
                        message.error('签入状态已中断！');
                    }
                    if (this.state.is_trying_resign === true) {
                        this.signIn(false);
                    } else {
                        this.onSignOut();
                    }
                }
            })
            .catch((error: any) => {
                // 20200831优化由于服务不存在导致的无法轮询问题
                let error_times = this.state.error_times;
                if (error_times > 5) {
                    this.signOut();
                    message.error('网络状态不稳定，请重启系统！');
                    return;
                }
                if (/服务不存在/i.test(error)) {
                    this.insertLog(`【服务不存在】【重试次数：${++error_times}】`, 'f');
                    this.setState(
                        {
                            error_times,
                            is_trying_resign: true,
                        },
                        () => {
                            this.getPool();
                        }
                    );
                } else {
                    this.signOut(true);
                    message.error(`【轮询】【${error}】，请联系管理员！`);
                    return;
                }
                this.insertLog(`【轮询】【${error}】`, 'f');
            });
    }
    // 通过dsn任务号获取手机号
    getPhoneFromDsn = (dsn: any) => {
        request(this, AppServiceUtility.call_center_nh_service.api!('get_phone', {
            dsn: dsn,
        }))
            .then((data: any) => {
                if (data.code === 200) {
                    this.getUser(data.msg, dsn);
                }
            })
            .catch((error: any) => {
                this.insertLog(`【根据DSN获取手机号】【${error}】`, 'f');
            });
    }
    // 通过手机号获取用户信息，包括长者信息和设备信息
    getUser = (telephone: any, dsn: any) => {
        this.setState({
            calling_phone: telephone,
        });
        request(this, AppServiceUtility.call_center_nh_no_loading_service.api!('get_user', {
            telephone: telephone,
            dsn: dsn,
        }))
            .then((data: any) => {
                if (data.code === 200) {
                    this.setState({
                        elder_info: data.result,
                        dsn: dsn
                    });
                    this.insertLog(`【根据手机号码获取长者信息】【${telephone}】【${JSON.stringify(data.result)}】`);
                    // 非外呼情况下才是紧急呼叫
                    if (data.result.is_emergency === true && !this.state.is_callouting) {
                        const key = `open${Date.now()}`;
                        this.emergency_key = key;
                        const btn: any = (
                            <Button
                                type="primary"
                                size="small"
                                onClick={
                                    () => {
                                        this.answerCall();
                                        notification.close(this.emergency_key);
                                    }
                                }
                            >
                                接听
                            </Button>
                        );
                        const close = () => {
                            this.hangUp();
                        };
                        notification['warning']({
                            message: data.result.name,
                            description: (
                                <Row>
                                    <Row>
                                        <img src={require("./img/sos.jpg")} style={{ maxWidth: '200px' }} />
                                    </Row>
                                    <Row style={{ marginTop: '20px', fontSize: '16px' }}>
                                        来电号码：{telephone}
                                    </Row>
                                </Row>
                            ),
                            btn,
                            key,
                            duration: null,
                            onClose: close,
                        });
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【获取手机号信息】【${telephone}】【${error}】`, 'f');
            });
    }
    // 应答操作
    answerCall = () => {
        // 调用软电话
        $.ajax({
            url: 'http://127.0.0.1:23412/sipphone/answerCall',
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': '*/*'
            },
            success: (res: any) => {
                if (res.message !== 'success') {
                    message.info('应答失败！');
                } else {
                    this.setState({
                        is_talking: true,
                    });
                }
                window.setTimeout(
                    () => {
                        this.formCreater && this.formCreater.reflash && this.formCreater.reflash();
                    },
                    3000
                );
            },
            error: (err: any) => {
                message.info('应答失败！');
            }
        });
        // 将点击应答的时间以及这通电话的dsn记录起来
        request(this, AppServiceUtility.call_center_nh_service.update_yingda!({'dsn': this.state.dsn, 'time': new Date()}))
            .then((data: any) => {
                console.log('点击应答时间插入成功',data);
            })
            .catch((error: any) => {
                console.log('插入失败');
            });
        this.setState({
            incall_show: false
        });
    }
    // 挂机操作
    hangUp = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('hangup', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('挂机成功！');
                    // 通用挂机方法
                    this.onHangUp();
                } else {
                    if (typeof (data.msg) === 'string') {
                        if (data.msg === '挂机失败，通话已结束') {
                            this.onHangUp();
                        }
                        message.error(`挂机失败：【${data.msg}】`);
                    } else {
                        message.error('挂机失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【挂机】【${error}】`, 'f');
            });
    }
    // 保持通话操作
    holdOn = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('holdon', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('保持成功！');
                    this.setState({
                        is_holding: true,
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`保持失败：【${data.msg}】`);
                    } else {
                        message.error('保持失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【保持】【${error}】`, 'f');
            });
    }
    // 取消保持通话操作
    unHoldOn = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('unholdon', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('取消保持成功！');
                    this.setState({
                        is_holding: false,
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`取消保持失败：【${data.msg}】`);
                    } else {
                        message.error('取消保持失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【取消保持】【${error}】`, 'f');
            });
    }
    // 示忙操作
    setBusy = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('setbusy', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('示忙成功！');
                    this.setState({
                        is_busy: true,
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`示忙失败：【${data.msg}】`);
                    } else {
                        message.error('示忙失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【示忙】【${error}】`, 'f');
            });
    }
    // 示闲操作
    setFree = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('setfree', {}))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('示闲成功！');
                    this.setState({
                        is_busy: false,
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`示闲失败：【${data.msg}】`);
                    } else {
                        message.error('示闲失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【示闲】【${error}】`, 'f');
            });
    }
    // 获取通话真实地址
    getRecordUrl = () => {
        request(this, AppServiceUtility.call_center_nh_service.api!('get_record_url', { pathname: this.state.record_url, locationId: this.state.locationId }))
            .then((data: any) => {
                if (data.code === 200) {
                    this.setState({
                        record_show: true,
                        record_url: data.msg
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`获取录音失败：【${data.msg}】`);
                    } else {
                        message.error('获取录音失败！');
                    }
                }
            })
            .catch((error: any) => {
                this.insertLog(`【获取录音】【${error}】`, 'f');
            });
    }
    // 确认编辑
    confirmEdit = () => {
        let { remark, selected_data } = this.state;
        if (remark === '') {
            message.error('请输入备注！');
            return;
        }
        if (remark === selected_data.remark) {
            message.error('没有任何修改！');
            return;
        }
        request(this, AppServiceUtility.call_center_nh_service.update_record!({ id: selected_data.id, remark: remark }))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('修改成功！');
                    this.setState({
                        edit_show: false,
                    });
                    this.formCreater && this.formCreater.reflash();
                } else {
                    message.info('修改失败！');
                }
            })
            .catch((error: any) => {
                this.insertLog(`【更新录音】【${error}】`, 'f');
            });
    }
    // 爱关怀处理结果
    changeAGHContent = (e: any) => {
        this.setState({
            agh_result: e.target.value,
        });
    }
    // 下载录音
    download = () => {
        window.open(this.state.record_url);
        this.setState({
            record_show: false,
        });
    }
    // 取消显示录音弹窗
    cancelAudio = () => {
        document && document.getElementById('play-audio') && document.getElementById('play-audio')!['pause']();
        this.setState({
            record_show: false
        });
    }
    // 取消显示编辑弹窗
    cancelEdit = () => {
        this.setState({
            edit_show: false
        });
    }
    // 取消显示来电提醒
    cancelIncall = () => {
        this.setState({
            incall_show: false
        });
    }
    // 选择坐席
    changeZuoxi = (e: any) => {
        this.setState({
            selected_transout_zh: e,
        });
    }
    // 输入外呼号码
    changeOutNumber = (e: any) => {
        this.setState({
            out_number: e,
        });
    }
    // 输入外呼模式
    changeTransformType = (e: any) => {
        this.setState({
            transform_type: e.target.value,
        });
    }
    // 显示转接弹窗
    showTransform = () => {
        this.setState({
            transform_show: true
        });
    }
    // 确认转接操作
    confirmTransform = () => {
        let { selected_transout_zh, calling_phone, transform_type, out_number } = this.state;
        if (transform_type === '2' && !selected_transout_zh) {
            message.error('请选择要转接的坐席帐号！');
            return;
        } else if (transform_type === '5' && !out_number) {
            message.error('请选择要转接的外呼号码！');
            return;
        }
        let condition: any = {
            calledDeviceType: transform_type,
            calledDigits: transform_type === '2' ? selected_transout_zh : out_number,
            transferMode: '2',
            callerDigits: calling_phone,
            origedDigits: ''
        };
        // 呼叫转移
        request(this, AppServiceUtility.call_center_nh_service.api!('transout', condition))
            .then((data: any) => {
                if (data.code === 200) {
                    message.info('转接成功！');
                    this.setState({
                        transform_show: false,
                    });
                } else {
                    if (typeof (data.msg) === 'string') {
                        message.error(`转接失败：【${data.msg}】`);
                    } else {
                        message.error('转接失败！');
                    }
                    this.setState({
                        transform_show: false,
                    });
                }
            })
            .catch((error: any) => {
                this.insertLog(`【转接】【${error}】`, 'f');
            });
    }
    // 取消转接操作
    cancelTransform = () => {
        this.setState({
            transform_show: false
        });
    }
    // 表单构造器映射
    onRef = (ref: any) => {
        this.formCreater = ref;
    }
    // 地图全屏
    showFull = () => {
        let full = document.getElementById("olderMap");
        if (full) {
            this.launchIntoFullscreen(full!);
            this.setState({
                full_screen: true
            });
        }
    }
    // 附属地图全屏方法
    launchIntoFullscreen = (element: HTMLElement) => {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        }
    }
    // 附属地图全屏方法
    exitFullscreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
    // 构造地图组件
    getMapEle = () => {
        let { elder_info, organization_info } = this.state;
        if (elder_info && elder_info.hasOwnProperty('device_name')) {
            return (
                <BaiduMap
                    lat={elder_info.device_lat}
                    lng={elder_info.device_lng}
                    older_data={{
                        name: elder_info.device_name,
                        address: elder_info.device_address,
                        emergency: '',
                    }}
                />
            );
        } else if (organization_info && organization_info.hasOwnProperty('address')) {
            return (
                <BaiduMap
                    lat={organization_info["lat"]}
                    lng={organization_info["lng"]}
                    older_data={{
                        name: organization_info.name,
                        address: organization_info.address,
                        emergency: ''
                    }}
                />
            );
        }
        return null;
    }
    // 切换显示日志
    toggleLog = () => {
        this.setState({
            is_log_show: !this.state.is_log_show,
        });
    }
    // 插入日志
    insertLog = (logs: string, type: string = 's') => {
        let log_datas = this.state.log_datas;
        let text_option: any = type === 's' ? {} : { type: 'danger' };
        // 超过五十就清空
        if (log_datas.length > 100) {
            log_datas.pop();
        }
        log_datas.unshift({
            logs: (<Text {...text_option}>{logs}</Text>),
            date: new Date()
        });
        this.setState(
            {
                log_datas,
            },
        );
    }
    clearLogs = () => {
        this.setState(
            {
                log_datas: [],
            },
        );
    }
    // 渲染
    render() {
        const formLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        let call_record_option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "坐席工号",
                decorator_id: "called_agent",
                option: {
                    placeholder: "请输入坐席工号",
                },
            }, {
                type: InputType.input,
                label: "主叫号码",
                decorator_id: "caller_no",
                option: {
                    placeholder: "请输入主叫号码",
                },
            }, {
                type: InputType.input,
                label: "被呼号码",
                decorator_id: "called_no",
                option: {
                    placeholder: "请输入被呼号码",
                },
            },
            {
                type: InputType.select,
                label: "呼叫类型",
                decorator_id: "call_type",
                option: {
                    placeholder: "请选择呼叫类型",
                    childrens: ['呼入', '呼出'].map((item: any) => {
                        return (
                            <Option key={item}>{item}</Option>
                        );
                    }),
                }
            },
            {
                type: InputType.select,
                label: "是否接通",
                decorator_id: "pickup",
                option: {
                    placeholder: "请选择是否接通",
                    childrens: [{ value: true, label: '是' }, { value: false, label: '否' }].map((item: any) => {
                        return (
                            <Option key={item.value}>{item.label}</Option>
                        );
                    }),
                }
            }, {
                type: InputType.input,
                label: "备注",
                decorator_id: "remark",
                option: {
                    placeholder: "请输入备注",
                },
            },
            {
                type: InputType.select,
                label: "是否备注",
                decorator_id: "is_remark",
                option: {
                    placeholder: "请选择是否备注",
                    childrens: [{ value: true, label: '是' }, { value: false, label: '否' }].map((item: any) => {
                        return (
                            <Option key={item.value}>{item.label}</Option>
                        );
                    }),
                }
            }, {
                type: InputType.rangePicker,
                label: "进入系统时间",
                decorator_id: "date_range",
                option: {
                    placeholder: ['开始时间', '结束时间'],
                },
            },
            ],
            btn_props: [],
            columns_data_source: this.callRecordListColumns,
            service_object: AppServiceUtility.call_center_nh_service,
            service_option: {
                select: {
                    service_func: 'get_record_list',
                    service_condition: [{}, 1, 10]
                },
            },
            onRef: this.onRef,
        };
        const { getFieldDecorator } = this.props.form!;
        const { is_register, is_signin, is_calling, is_busy, form_values, record_show, record_url, edit_show, elder_info, full_screen, transform_show, calling_phone, zh_list, is_holding, is_talking, is_callouting, out_number, transform_type, incall_show, is_log_show, log_datas } = this.state;
        const modal_left: number = 4;
        return (
            <MainContent>
                <Row className='call-center' style={{ position: 'relative' }}>
                    <Tabs type="card">
                        <TabPane tab={"移动呼叫中心"} key='移动呼叫中心'>
                            <Form {...formLayout}>
                                <Row>
                                    <Col span={8}>
                                        <Form.Item label='坐席工号'>
                                            {getFieldDecorator(`agentId`, {
                                                initialValue: form_values['agentId'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入坐席工号'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'agentId')} autoComplete='off' placeholder="请输入坐席工号" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='坐席密码'>
                                            {getFieldDecorator(`password`, {
                                                initialValue: form_values['password'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入坐席密码'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'password')} autoComplete='off' placeholder="请输入坐席密码" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='技能ID'>
                                            {getFieldDecorator(`skillIds`, {
                                                initialValue: form_values['skillIds'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入技能ID'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'skillIds')} autoComplete='off' placeholder="请输入技能ID" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='坐席分机号'>
                                            {getFieldDecorator(`phoneNum`, {
                                                initialValue: form_values['phoneNum'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入坐席分机号'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'phoneNum')} autoComplete='off' placeholder="请输入坐席分机号" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='呼叫中心标识'>
                                            {getFieldDecorator(`ccId`, {
                                                initialValue: form_values['ccId'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入呼叫中心标识'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'ccId')} autoComplete='off' placeholder="请输入呼叫中心标识" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='签入后状态'>
                                            {getFieldDecorator(`agentState`, {
                                                initialValue: form_values['agentState'],
                                                rules: [{
                                                    required: false,
                                                    message: '请选择签入后状态'
                                                }],
                                            })(
                                                <Select
                                                    showSearch={true}
                                                    onChange={(e: any) => this.setFormValues(e, 'agentState', 'select')}
                                                >
                                                    {[{ value: '3', label: '示忙' }, { value: '4', label: '示闲' }].map((item: any, index: number) => {
                                                        return (
                                                            <Option key={index} value={item.value}>{item.label}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='软电话模式'>
                                            {getFieldDecorator(`phoneStyle`, {
                                                initialValue: form_values['phoneStyle'] || true,
                                                rules: [{
                                                    required: false,
                                                    message: '请选择软电话模式'
                                                }],
                                            })(
                                                <Select
                                                    showSearch={true}
                                                    onChange={(e: any) => this.setFormValues(e, 'phoneStyle', 'select')}
                                                >
                                                    {/* [{ value: true, label: '内置软电话' }, { value: false, label: '外置软电话' }] */}
                                                    {[{ value: true, label: '内置软电话' }].map((item: any, index: number) => {
                                                        return (
                                                            <Option key={index} value={item.value}>{item.label}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={8}>
                                        <Form.Item label='外显号码'>
                                            {getFieldDecorator(`disPlayNumber`, {
                                                initialValue: form_values['disPlayNumber'],
                                                rules: [{
                                                    required: false,
                                                    message: '请输入外显号码'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'disPlayNumber')} autoComplete='off' placeholder="请输入外显号码" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={8}>
                                        <Form.Item label='外呼号码'>
                                            {getFieldDecorator(`number`, {
                                                initialValue: form_values['number'] || '',
                                                rules: [{
                                                    required: false,
                                                    message: '请输入外呼号码'
                                                }],
                                            })(
                                                <Input onChange={(e: any) => this.setFormValues(e, 'number')} autoComplete='off' placeholder="请输入外呼号码" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" className="call-center-nh-btns">
                                    <Button disabled={is_signin} type="primary" onClick={() => this.signIn(true)}>签入</Button>
                                    <Button disabled={!is_signin || is_calling} type="danger" onClick={() => this.signOut()}>签出</Button>
                                    <Button disabled={!is_signin || is_calling} type="primary" onClick={this.doCall}>外呼</Button>
                                    <Button disabled={is_callouting || !is_calling} type="primary" onClick={this.answerCall}>应答</Button>
                                    <Button disabled={!is_callouting && !is_talking} type="primary" onClick={this.hangUp}>挂机</Button>
                                    <Button disabled={!is_talking} type="primary" onClick={is_holding ? this.unHoldOn : this.holdOn}>{is_holding ? '取消保持' : '保持'}</Button>
                                    <Button type="primary" disabled={!is_talking} onClick={this.showTransform}>转接</Button>
                                    <Button disabled={!is_signin || is_busy || is_calling} type="primary" onClick={this.setBusy}>示忙</Button>
                                    <Button disabled={!is_signin || !is_busy || is_calling} type="primary" onClick={this.setFree}>示闲</Button>
                                </Row>
                                {is_register === false ? <Row className="call-center-register-error" type="flex" justify="center" align="middle">软电话注册失败！你可以选择打开软电话后重新尝试签入</Row> : null}
                            </Form>
                        </TabPane>
                    </Tabs>
                    {is_log_show ? <Tabs type="card" style={{ marginTop: 20 }} tabBarExtraContent={<Button onClick={() => this.clearLogs()}>清空日志</Button>}>
                        <TabPane tab={"日志记录"} key="日志记录">
                            <List
                                size="small"
                                className="call-center-list"
                                bordered={true}
                                dataSource={log_datas}
                                renderItem={(item: any) => (
                                    <List.Item>
                                        <Row type="flex" justify="space-between" align="middle" style={{ width: '100%' }}>
                                            <Col span={21} style={{ whiteSpace: 'pre-wrap', wordBreak: 'break-all' }}>{item.logs}</Col>
                                            <Col span={3} style={{ textAlign: 'right' }}>{moment(item.date).format('YYYY-MM-DD HH:mm:ss')}</Col>
                                        </Row>
                                    </List.Item>
                                )}
                            />
                        </TabPane>
                    </Tabs> : null}
                    <Tabs type="card" style={{ marginTop: 20 }}>
                        <TabPane tab={"长者信息"} key="长者信息">
                            <Row className="call-center-nh-elderinfo">
                                <Col span={6}>
                                    <Row type="flex" justify="center" align="middle" className="call-center-elder-img">
                                        <img
                                            onDoubleClick={this.toggleLog}
                                            src={
                                                (() => {
                                                    if (elder_info && elder_info.personnel_info && elder_info.personnel_info.picture && elder_info.personnel_info.picture[0]) {
                                                        return elder_info.personnel_info.picture[0];
                                                    }
                                                    return require("../call-center/img/older.jpg");
                                                })()
                                            }
                                        />
                                    </Row>
                                </Col>
                                {elder_info && elder_info.hasOwnProperty('id') ?
                                    <Col span={6}>
                                        <Card title="长者信息">
                                            <p>姓名：<span>{elder_info.name || ''}{elder_info.relations ? `【${elder_info.relations}】` : ''}</span></p>
                                            <p>性别：<span>{elder_info.personnel_info && elder_info.personnel_info.sex ? elder_info.personnel_info.sex : ''}</span></p>
                                            <p>年龄：<span>{elder_info.id_card ? getAge2(elder_info.id_card) : ''}</span></p>
                                            <p>家庭地址：<span>{elder_info.personnel_info && elder_info.personnel_info.address ? elder_info.personnel_info.address : ''}</span></p>
                                            <p>电话号码：<span>{elder_info.personnel_info && elder_info.personnel_info.telephone ? elder_info.personnel_info.telephone : ''}</span></p>
                                            <p>证件类型：<span>{elder_info.personnel_info && elder_info.id_card_type ? elder_info.id_card_type : ''}</span></p>
                                            <p>证件号码：<span>{elder_info.personnel_info && elder_info.personnel_info.id_card ? elder_info.personnel_info.id_card : ''}</span></p>
                                        </Card>
                                    </Col>
                                    :
                                    <Col span={6}>
                                        <Card title="长者信息">
                                            <p>未知号码，查找不到相关长者信息，请登记</p>
                                        </Card>
                                    </Col>
                                }
                                <Col span={12}>
                                    <div id={'olderMap'} className={full_screen ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
                                        {
                                            !(full_screen)
                                            &&
                                            <Button
                                                style={{
                                                    position: 'absolute',
                                                    zIndex: 1,
                                                    right: 2,
                                                    top: 1
                                                }}
                                                shape="circle"
                                                icon="fullscreen"
                                                onClick={() => {
                                                    this.showFull();
                                                }}
                                            />
                                        }
                                        {(() => {
                                            return this.getMapEle();
                                        })()}
                                    </div>
                                </Col>
                            </Row>
                        </TabPane>
                    </Tabs>
                    <Tabs type="card" style={{ marginTop: 20 }}>
                        <TabPane tab={"通话记录"} key="通话记录">
                            <SignFrameLayout {...call_record_option} />
                        </TabPane>
                    </Tabs>
                </Row>
                <Modal
                    title="来电提醒"
                    visible={incall_show}
                    onOk={this.answerCall}
                    okText='应答'
                    cancelText='隐藏'
                    onCancel={this.cancelIncall}
                >
                    来电号码：{calling_phone}
                </Modal>
                <Modal
                    title="录音播放"
                    visible={record_show}
                    onOk={this.download}
                    okText='下载'
                    cancelText='取消'
                    style={{ textAlign: 'center' }}
                    onCancel={this.cancelAudio}
                    zIndex={1999}
                >
                    <audio controls={true} src={record_url} id="play-audio">
                        你的浏览器不支持录音播放
                    </audio>
                </Modal>
                <Modal
                    title="编辑信息"
                    cancelText='取消'
                    okText='确认'
                    visible={edit_show}
                    onOk={this.confirmEdit}
                    onCancel={this.cancelEdit}
                    width='400px'
                >
                    {/* <Row type="flex" align="middle">
                        <Col span={modal_left}>主叫号码：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.caller_no} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>被叫号码：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.called_no} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>呼叫类型：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.call_type} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>开始时间：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.begin_time} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>结束时间：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.end_time} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>时长（秒）：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={this.getSecondsFromRange(selected_data.begin_time, selected_data.end_time)} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>接听人员：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={selected_data.cs_name} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>录音播放：</Col>
                        <Col span={24 - modal_left}>
                            <Icon type="play-square" theme="twoTone" style={{ fontSize: '30px' }} onClick={() => { this.getAudioPlay(selected_data.record_file_name, selected_data.locationId); }} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" /> */}
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>备注：</Col>
                        <Col span={24 - modal_left}>
                            <TextArea rows={4} onChange={this.remarkChange} value={this.state.remark} />
                        </Col>
                    </Row>
                </Modal>
                <Modal
                    title="呼叫转移"
                    cancelText='取消'
                    okText='确认'
                    visible={transform_show}
                    onOk={this.confirmTransform}
                    onCancel={this.cancelTransform}
                    width='400px'
                >
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>当前通话号码：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={calling_phone} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>当前坐席号码：</Col>
                        <Col span={24 - modal_left}>
                            <Mentions value={form_values.phoneNum} readOnly={true} />
                        </Col>
                    </Row>
                    <WhiteSpace size="sm" />
                    <Row type="flex" align="middle">
                        <Col span={modal_left}>选择转接模式：</Col>
                        <Col span={24 - modal_left}>
                            <Radio.Group onChange={this.changeTransformType}>
                                <Radio value={'2'} defaultChecked={true}>转接到坐席</Radio>
                                <Radio value={'5'}>转接到号码</Radio>
                            </Radio.Group>
                        </Col>
                    </Row>
                    {(() => {
                        if (transform_type === '2') {
                            return (
                                <div>
                                    <WhiteSpace size="sm" />
                                    <Row type="flex" align="middle">
                                        <Col span={modal_left}>转接坐席号码：</Col>
                                        <Col span={24 - modal_left}>
                                            <Select style={{ width: '235px' }} onChange={this.changeZuoxi}>
                                                {zh_list && zh_list.length ? zh_list.map((item: any, index: number) => {
                                                    if (item.phoneNum !== form_values.phoneNum) {
                                                        return (
                                                            <Option key={index} value={item.agentId}>{item.name}</Option>
                                                        );
                                                    }
                                                    return null;
                                                }) : null}
                                            </Select>
                                        </Col>
                                    </Row>
                                </div>
                            );
                        } else if (transform_type === '5') {
                            return (
                                <div>
                                    <WhiteSpace size="sm" />
                                    <Row type="flex" align="middle">
                                        <Col span={modal_left}>转接外呼号码：</Col>
                                        <Col span={24 - modal_left}>
                                            <Mentions value={out_number} onChange={this.changeOutNumber} />
                                        </Col>
                                    </Row>
                                </div>
                            );
                        }
                        return null;
                    })()}
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：南海呼叫中心
 * 描述
 */
@addon('CallCenterNhView', '南海呼叫中心', '南海呼叫中心')
@reactControl(Form.create<any>()(CallCenterNhView), true)
export class CallCenterNhViewControl extends ReactViewControl {
}