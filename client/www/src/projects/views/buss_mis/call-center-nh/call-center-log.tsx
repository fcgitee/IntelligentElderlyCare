import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Button, Form, Modal, Select } from 'antd';
import ChangeCallCenterLog from "./change-call-center-log";
const Option = Select.Option;
/** 状态：南海呼叫中心日志 */
export interface CallCenterLogViewState extends ReactViewState {
    selected_log_info: any;
    is_modal_show: boolean;
}
/** 组件：南海呼叫中心日志 */
export class CallCenterLogView extends React.Component<CallCenterLogViewControl, CallCenterLogViewState> {
    private columns_data_source = [
        {
            title: '坐席工号',
            dataIndex: 'agent_id',
            key: 'agent_id',
        },
        {
            title: '工作人员名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '方法',
            dataIndex: 'method',
            key: 'method',
        },
        {
            title: '系统占用时间',
            dataIndex: 'xt_api_times',
            key: 'xt_api_times',
        },
        {
            title: '移动接口时间',
            dataIndex: 'yd_api_times',
            key: 'yd_api_times',
        },
        {
            title: '描述信息',
            dataIndex: 'message',
            key: 'message',
        }, {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }, {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    <Button onClick={() => { this.ctrl('check', record); }}>查看</Button>
                );
            }
        }
    ];
    constructor(props: CallCenterLogViewControl) {
        super(props);
        this.state = {
            selected_log_info: {},
            is_modal_show: false,
        };
    }
    ctrl = (type: string, record: any = {}) => {
        switch (type) {
            case 'check':
                this.setState(
                    {
                        selected_log_info: record,
                        is_modal_show: true,
                    },
                );
                break;
            case 'hide':
                this.setState(
                    {
                        selected_log_info: {},
                        is_modal_show: false,
                    },
                );
                break;
            default: break;
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let call_center_log: any = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '坐席ID',
                    decorator_id: "agent_id",
                    option: {
                        placeholder: '请输入坐席ID',
                    }
                },
                {
                    type: InputType.input,
                    label: '方法名',
                    decorator_id: "method",
                    option: {
                        placeholder: '请输入方法名',
                    }
                },
                {
                    type: InputType.select,
                    label: '是否有事件',
                    decorator_id: "has_events",
                    option: {
                        placeholder: '请选择是否有事件',
                        childrens: [{
                            label: '有',
                            key: 'true'
                        }, {
                            label: '没有',
                            key: 'false'
                        }].map((item: any) => {
                            return (
                                <Option key={item.key}>{item.label}</Option>
                            );
                        }),
                    }
                },
                {
                    type: InputType.input,
                    label: '事件代码',
                    decorator_id: "event_id",
                    option: {
                        placeholder: '请输入事件代码',
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.call_center_nh_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
        };
        let call_center_log_list = Object.assign(call_center_log, table_param);
        const { is_modal_show, selected_log_info } = this.state;
        return (
            <div>
                <SignFrameLayout {...call_center_log_list} />
                <Modal
                    width="90%"
                    title="查看日志"
                    visible={is_modal_show}
                    onCancel={() => this.ctrl('hide')}
                    footer={null}
                >
                    <ChangeCallCenterLog log_info={selected_log_info} />
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：南海呼叫中心日志控制器
 * @description 南海呼叫中心日志
 * @author
 */
@addon('CallCenterLogView', '南海呼叫中心日志', '南海呼叫中心日志')
@reactControl(Form.create<any>()(CallCenterLogView), true)
export class CallCenterLogViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}