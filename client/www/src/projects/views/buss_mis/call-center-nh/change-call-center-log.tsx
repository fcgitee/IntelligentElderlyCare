import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Skeleton, Descriptions } from "antd";
import moment from "moment";
import "./index.less";
/**
 * 组件：南海呼叫中心日志
 */
export interface ChangeCallCenterLogState extends ReactViewState {
}

/**
 * 组件：南海呼叫中心日志
 */
export default class ChangeCallCenterLog extends ReactView<ChangeCallCenterLogControl, ChangeCallCenterLogState> {
    jsonFormat(format: any) {
        let msg: string = '', pos: number = 0, prevChar: string = '', outOfQuotes: boolean = true;
        // 循环每一个字符
        for (let i = 0; i < format.length; i++) {
            // 获取到该字符
            let char = format.substring(i, i + 1);
            // 如果转移
            if (char === '"' && prevChar !== '\\') {
                outOfQuotes = !outOfQuotes;
            } else if ((char === '}' || char === ']') && outOfQuotes) {
                // 如果是关闭
                msg += "<br/>";
                pos--;
                for (let j = 0; j < pos; j++) {
                    msg += '    ';
                }
            }
            msg += char;
            if ((char === ',' || char === '{' || char === '[') && outOfQuotes) {
                msg += "<br/>";
                if (char === '{' || char === '[') {
                    pos++;
                }
                for (let k = 0; k < pos; k++) {
                    msg += '    ';
                }
            }
            prevChar = char;
        }
        return msg;
    }
    getYMDHIS(timestamp: any) {
        return timestamp ? moment(parseInt(timestamp, 0) * 1000).format('YYYY-MM-DD HH:mm:ss') : null;
    }
    render() {
        let { log_info } = this.props;

        // 初始化，抑制报错
        if (!log_info) {
            log_info = [];
        }
        return (
            <div className="call-center-log-main">
                {log_info && log_info.id ? <Descriptions layout="vertical" bordered={true}>
                    <Descriptions.Item label="坐席ID">
                        {log_info.agent_id}
                    </Descriptions.Item>
                    <Descriptions.Item label="工作人员名称">
                        {log_info.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="方法">
                        {log_info.method}
                    </Descriptions.Item>
                    <Descriptions.Item label="系统调用发起时间">
                        {this.getYMDHIS(log_info.xt_api_begin)}
                    </Descriptions.Item>
                    <Descriptions.Item label="系统调用完成时间">
                        {this.getYMDHIS(log_info.xt_api_finish)}
                    </Descriptions.Item>
                    <Descriptions.Item label="系统调用使用时间">
                        {log_info.xt_api_times}
                    </Descriptions.Item>
                    <Descriptions.Item label="移动接口发起时间">
                        {this.getYMDHIS(log_info.yd_api_begin)}
                    </Descriptions.Item>
                    <Descriptions.Item label="移动接口完成时间">
                        {this.getYMDHIS(log_info.yd_api_finish)}
                    </Descriptions.Item>
                    <Descriptions.Item label="移动接口使用时间">
                        {log_info.yd_api_times}
                    </Descriptions.Item>
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="爱关怀查询发起时间">
                            {this.getYMDHIS(log_info.agh_api_begin)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="爱关怀查询完成时间">
                            {this.getYMDHIS(log_info.agh_api_finish)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="爱关怀查询使用时间">
                            {log_info.agh_api_times}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="地图查询发起时间">
                            {this.getYMDHIS(log_info.map_api_begin)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="地图查询完成时间">
                            {this.getYMDHIS(log_info.map_api_finish)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '轮询' ?
                        <Descriptions.Item label="地图查询使用时间">
                            {log_info.map_api_times}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '获取用户' ?
                        <Descriptions.Item label="获取设备发起时间">
                            {this.getYMDHIS(log_info.device_api_begin)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '获取用户' ?
                        <Descriptions.Item label="获取设备完成时间">
                            {this.getYMDHIS(log_info.device_api_finish)}
                        </Descriptions.Item>
                        : null}
                    {log_info.method === '获取用户' ? <>
                        <Descriptions.Item label="获取设备使用时间">
                            {log_info.device_api_times}
                        </Descriptions.Item>
                    </> : null}
                    <Descriptions.Item label="HEADERS">
                        <pre>
                            {JSON.stringify(log_info.headers, null, 2)}
                        </pre>
                    </Descriptions.Item>
                    <Descriptions.Item label="SESSION">
                        <pre>
                            {JSON.stringify(log_info.session, null, 2)}
                        </pre>
                    </Descriptions.Item>
                    <Descriptions.Item label="响应">
                        <pre>
                            {JSON.stringify(log_info.response, null, 2)}
                        </pre>
                    </Descriptions.Item>
                    <Descriptions.Item label="入参">
                        <pre>
                            {JSON.stringify(log_info.params, null, 2)}
                        </pre>
                    </Descriptions.Item>
                    <Descriptions.Item label="出参">
                        <pre>
                            {JSON.stringify(log_info.condition, null, 2)}
                        </pre>
                    </Descriptions.Item>
                    <Descriptions.Item label="描述信息">
                        {log_info.message}
                    </Descriptions.Item>
                    <Descriptions.Item label="创建时间">
                        {log_info.create_date}
                    </Descriptions.Item>
                </Descriptions> : <Skeleton />}
            </div>
        );
    }
}

/**
 * 控件：南海呼叫中心日志
 * @description 南海呼叫中心日志
 * @author
 */
@addon('ChangeCallCenterLog', '南海呼叫中心日志', '南海呼叫中心日志')
@reactControl(ChangeCallCenterLog, true)
export class ChangeCallCenterLogControl extends ReactViewControl {
    /** 基础数据 */
    log_info?: any;
}