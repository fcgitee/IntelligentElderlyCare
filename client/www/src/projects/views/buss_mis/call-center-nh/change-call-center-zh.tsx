import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { edit_props_info } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import { message, Select } from "antd";
import { ROUTE_PATH } from "src/projects/router";
const Option = Select.Option;
/**
 * 组件：南海呼叫中心帐号
 */
export interface ChangeCallCenterZhViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
}

/**
 * 组件：南海呼叫中心帐号
 */
export default class ChangeCallCenterZhView extends ReactView<ChangeCallCenterZhViewControl, ChangeCallCenterZhViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    handleSubmit = (err: any, value: any) => {
        value['id'] = this.props.match!.params.key;
        // 获取移动呼叫中心帐号
        request(this, AppServiceUtility.call_center_nh_service.update_call_center_zh!(value))
            .then((data: any) => {
                if (data && data.code === 200) {
                    message.info('保存成功！', 1, () => {
                        this.props.history!.push(ROUTE_PATH.callCenterZh);
                    });
                } else if (data.code === 302) {
                    message.error('该人员已录入帐号，请前往编辑！', 1, () => {
                        this.props.history!.push(ROUTE_PATH.changeCallCenterZh + '/' + data.msg);
                    });
                }
            })
            .catch((error: any) => {
                console.error(error);
            });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "工作人员姓名",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "工作人员身份证",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [
                {
                    title: '姓名',
                    dataIndex: 'name',
                    key: 'name',
                }, {
                    title: '身份证号',
                    dataIndex: 'id_card',
                    key: 'id_card',
                },],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_worker_list_all',
            title: '工作人员查询',
            select_option: {
                placeholder: "请选择工作人员",
            }
        };

        var edit_props = {
            form_items_props: [
                {
                    title: '移动呼叫中心帐号',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "工作人员帐号",
                            decorator_id: "user_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择工作人员帐号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入工作人员帐号",
                                autocomplete: 'off',
                                modal_search_items_props: modal_search_items_props,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "坐席工号",
                            decorator_id: "agentId",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入坐席工号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入坐席工号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "坐席密码",
                            decorator_id: "password",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入坐席密码" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入坐席密码",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "技能ID",
                            decorator_id: "skillIds",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入技能ID" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入技能ID",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "坐席分机号",
                            decorator_id: "phoneNum",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入坐席分机号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入坐席分机号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "呼叫中心标识",
                            decorator_id: "ccId",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入呼叫中心标识" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入呼叫中心标识",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.select,
                            label: "签入后状态",
                            decorator_id: "agentState",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择签入后状态" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择签入后状态",
                                autocomplete: 'off',
                                childrens: [{ value: '4', label: '示闲' }, { value: '3', label: '示忙' }].map((item: any) => {
                                    return (
                                        <Option key={item.value}>{item.label}</Option>
                                    );
                                }),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "外显号码",
                            decorator_id: "disPlayNumber",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入外显号码" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入外显号码",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.callCenterZh);
                        // this.props.history!.goBack();
                    }
                },
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.call_center_nh_service,
                operation_option: {
                    save: {
                        func_name: "update_call_center_zh"
                    },
                    query: {
                        func_name: "get_call_center_zh_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：南海呼叫中心帐号
 * @description 南海呼叫中心帐号
 * @author
 */
@addon('ChangeCallCenterZhView', '南海呼叫中心帐号', '南海呼叫中心帐号')
@reactControl(ChangeCallCenterZhView, true)
export class ChangeCallCenterZhViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}