import { Row, Form } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：服务计划列表状态
 */
export interface ServicePlanListViewState extends ReactViewState {
}

/**
 * 组件：服务计划列表
 * 描述
 */
export class ServicePlanListView extends ReactView<ServicePlanListViewControl, ServicePlanListViewState> {
    private columns_data_source = [
        {
            title: '计划名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '上传人',
            dataIndex: 'uploader_name',
            key: 'uploader_name',
        }, {
            title: '上传时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeServicePlan + '/' + contents.id);
        } else if ('icon_menu' === type) {
            window.open(contents.file);
        }
    }
    componentDidMount() {
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServicePlan);
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "计划名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.rangePicker,
                    label: "上传时间",
                    decorator_id: "date_range"
                },
            ],
            btn_props: [{
                label: '新增服务计划',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: 'get_service_plan_list_all',
                    service_condition: []
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let table_param = {
            other_label_type: [
                { type: 'icon', label_key: 'icon_menu', label_parameter: { icon: 'antd@menu' } },
                { type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } },
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：服务计划列表
 * 描述
 */
@addon('ServicePlanListView', '服务计划列表', '描述')
@reactControl(Form.create<any>()(ServicePlanListView), true)
export class ServicePlanListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}