import { message, Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { nursingTemplateService } from "src/projects/models/nursing-template";

const { Option } = Select;
/**
 * 组件：新增护理模板状态
 */
export interface addNursingTemplateState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
}

/**
 * 组件：新增护理模板视图
 */
export class addNursingTemplate extends ReactView<addNursingTemplateControl, addNursingTemplateState> {

    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {}
        };
    }
    /** 权限服务 */
    assessmentTemplateServer?() {
        return getObject(this.props.assessmentTemplateServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.nursingTemplate);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        let data = value;
        data.dataSource.forEach((item: any) => {
            delete item.serial_number;
        });
        this.assessmentTemplateServer!()!.update_nursing_template!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.nursingTemplate);
                }
            });
    }
    /** 返回按钮回调 */
    back = (value: any) => {
        // console.log('返回', value);
        history.back();
    }
    componentDidMount() {
        AppServiceUtility.nursing_content.get_nursing_project_list!({}, 1, 10)!
            .then((data: any) => {
                let project_list: any[];
                project_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.name}</Option>;
                });
                this.setState({ project_list });
            });
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key,
                    is_edit: true
                }
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let columns_data_source = [{
            title: '模板项目',
            dataIndex: 'project_id',
            key: 'project_id',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择模板项目",
                childrens: this.state.project_list
            }
        }];
        let user_manege = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "模板名称",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入模板名称", }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "name",
                },
                {
                    type: InputTypeTable.text_area,
                    label: "描述",
                    field_decorator_option: {
                        rules: [{ required: false, message: "" }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "describe",
                }
            ],
            submit_props: {
                text: "保存",
                cb: this.save_value
            },
            other_btn_propps: [
                {
                    text: "返回",
                    cb: this.back
                }
            ],
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            add_row_text: "添加模板项目",
            table_field_name: 'dataSource',
            // table配置
            columns_data_source: columns_data_source,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.assessment_template_service,
                operation_option: {
                    query: {
                        func_name: "get_assessment_template_list_all",
                        arguments: [this.state.selete_obj, 1, 1]
                    },
                    // save: {
                    //     func_name: "update_service_type"
                    // }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingTemplate); },
            id: this.props.match!.params.key
        };

        return (
            <TableList {...user_manege} />
        );
    }
}

/**
 * 控件：新增护理模板控件
 * @description 新增护理模板控件
 * @author
 */
@addon('addNursingTemplate', '新增护理模板控件', '新增护理模板控件')
@reactControl(addNursingTemplate, true)
export class addNursingTemplateControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估模板服务 */
    public assessmentTemplateServer_Fac?: Ref<nursingTemplateService>;

}