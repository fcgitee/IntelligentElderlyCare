import { message, Row, Radio } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { IAssessmentProjectService } from "src/projects/models/assessment-project";
import { ROUTE_PATH } from 'src/projects/router';
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import './index.less';
import { request } from "src/business/util_tool";

/** 项目状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：新增/编辑护理内容状态
 */
export interface addNursingState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    // 要显示的组件
    showType?: number;
    // 选择框类
    selete_type?: any;
}

/**
 * 组件：新增/编辑护理内容
 */
export class addNursingView extends ReactView<addNursingControl, addNursingState> {

    constructor(props: any) {
        super(props);
        this.state = {
            showType: 0,// 0不显示，1显示form，2显示table_list
            selete_obj: {}
        };
    }
    /** 权限服务 */
    assessmentProjectServer?() {
        return getObject(this.props.assessmentProjectServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.assessmentProject);
    }
    /** 保存按钮回调方法 */
    // 不同组件是不同的传入方法
    save_value = (value1: any, value2: any) => {
        let value;
        if (this.state.showType === 1) {
            // 1是table-list可编辑列表组件
            value = value1;
        } else {
            // 2是form表单组件
            value = value2;
            // 固定计分方式为不计分
            value['scoring_method'] = 1;
        }
        if (this.props.match!.params.key) {
            value['state'] = SRAR_TUSING;
            value['id'] = this.props.match!.params.key;
        }
        value['selete_type'] = this.state.selete_type;
        this.assessmentProjectServer!()!.update_assessment_project!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.assessmentProject);
                }
            });
    }
    componentDidMount() {
        // console.log(this.props.match!.params.key);
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
            request(this, AppServiceUtility.assessment_project_service.get_assessment_project_list!({ id: this.props.match!.params.key }, 1, 1)!)
                .then((datas: any) => {
                    let showType = 0,
                        selete_type = datas.result[0].selete_type;
                    if (selete_type === '1' || selete_type === '3' || selete_type === '4') {
                        showType = 1;
                    }
                    this.setState({
                        selete_type,
                        showType,
                    });
                });
        }
    }
    radisChange(e: any) {
        let showType = this.state.showType;
        if (e.target.value === '1' || e.target.value === '4' || e.target.value === '3') {
            showType = 1;
        }
        this.setState({
            selete_type: e.target.value,
            showType: showType,
        });
    }
    backList() {
        this.props.history!.go(-1);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let table_list_props;
        table_list_props = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "护理名称",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入护理名称", }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "project_name",
                },
                {
                    type: InputTypeTable.input,
                    label: "护理描述",
                    field_decorator_option: {
                        rules: [{ required: false, message: "" }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "describe",
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.backList();
                    }
                }
            ],
            submit_props: {
                text: "保存",
                cb: this.save_value
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            table_field_name: 'dataSource',
            // table配置
            columns_data_source: [{
                title: '选项内容',
                dataIndex: 'option_content',
                key: 'option_content',
                type: 'input' as ColTypeStr
            }],
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.assessment_project_service,
                operation_option: {
                    query: {
                        func_name: "get_assessment_project_list",
                        arguments: [this.state.selete_obj, 1, 1]
                    },
                },
            },
            add_row_text: `新增护理`,
            succ_func: () => { this.backList(); },
            id: this.props.match!.params.key
        };
        // let style = {
        //     marginBottom: '-60px',
        //     padding: '20px 0 40px 10px',
        // };
        // selete_type
        let layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 19 },
            },
        };
        let edit_props;
        edit_props = {
            form_items_props: [
                {
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "护理名称",
                                        decorator_id: "project_name",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入护理名称" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入护理名称",
                                        },
                                        layout: layout
                                    },
                                    {
                                        type: InputType.antd_input,
                                        label: "护理描述",
                                        col_span: 8,
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "" }],
                                        } as GetFieldDecoratorOptions,
                                        decorator_id: "describe",
                                        layout: layout
                                    }
                                ]
                            },
                        }
                    ]
                }
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.save_value
            },
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => this.backList!()
                },
            ],
            row_btn_props: {
                style: {
                    justifyContent: " start"
                }
            },
            service_option: {
                service_object: AppServiceUtility.assessment_project_service,
                operation_option: {
                    query: {
                        func_name: "get_assessment_project_list_all",
                        arguments: [this.state.selete_obj, 1, 1]
                    },
                },
            },
            id: this.props.match!.params.key,
        };

        let edit_props_list = Object.assign(edit_props, edit_props_info);
        let selete_type = this.state.selete_type;
        let showType: any = this.state.showType;
        let radiodom = [<Radio value='1' key="1">单选框</Radio>, <Radio value='4' key="4">下拉框</Radio>, <Radio value='3' key="3">输入框</Radio>];
        return (
            <div>
                <MainContent>
                    <MainCard>
                        <Row type="flex" justify="start" style={{ 'fontSize': '14px' }}>
                            选择框类：
                        <Radio.Group value={selete_type} onChange={(e) => this.radisChange(e)}>
                                {
                                    radiodom.map((item: any, index: number) => {
                                        return (item);
                                    })
                                }
                            </Radio.Group>
                        </Row>
                    </MainCard>
                    {showType === 1 ? (
                        <TableList {...table_list_props} />
                    ) : showType === 2 ? <FormCreator {...edit_props_list} /> : null}
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：新增/编辑护理内容控件
 * @description 新增/编辑护理内容控件
 * @author
 */
@addon('addNursingView', '新增/编辑护理内容', '新增/编辑护理内容控件')
@reactControl(addNursingView, true)
export class addNursingControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估项目服务 */
    public assessmentProjectServer_Fac?: Ref<IAssessmentProjectService>;

}