import { ReactViewState, reactControl, ReactViewControl, ReactView } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { Select } from 'antd';
let { Option } = Select;
/**
 * 组件：调研数据录入状态
 */
export interface ResearchInsertDisabledViewState extends ReactViewState {
    /** 采集状态 */
    collection_state:string [];
    /** 调查方式 */
    research_type:string [];
    /** 调查结果 */
    research_result:string [];
    /** 就业形式 */
    work_type:string [];
}
/**
 * 组件：调研数据录入视图
 */
export class ResearchInsertDisabledView extends ReactView<ResearchInsertDisabledViewControl, ResearchInsertDisabledViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            collection_state:["待采集","采集中","采集结束"],
            research_type:["入户调查","电话调查"],
            research_result:["查无此人","已搬迁","空挂户","外出","注销"],
            work_type:["合同中","临时工"],
        };
    }
    render() {
        const collection_state: JSX.Element[] = this.state.collection_state!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        const research_type: JSX.Element[] = this.state.research_type!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        const research_result: JSX.Element[] = this.state.research_result!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        const work_type: JSX.Element[] = this.state.work_type!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        let edit_props = {
            form_items_props: [
                {
                    title: "调研数据录入",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "姓名",
                            decorator_id: "ask_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入姓名" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入姓名"
                            }
                        }, 
                        {
                            type: InputType.antd_input,
                            label: "残疾证号",
                            decorator_id: "card_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入残疾证号" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入残疾证号"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "年龄",
                            decorator_id: "age",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入年龄" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入年龄"
                            }
                        }, 
                        {
                            type: InputType.select,
                            label: "采集状态",
                            decorator_id: "collection_state",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择采集状态" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: collection_state,
                                placeholder: "请选择采集状态",
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "电话",
                            decorator_id: "phone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入电话" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入电话"
                            }
                        },
                        {
                                            
                            type: InputType.antd_input,
                            label: "地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写地址" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写地址"
                            },
                        },
                        {
                            type: InputType.select,
                            label: "调查方式",
                            decorator_id: "research_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择调查方式" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: research_type,
                                placeholder: "请选择调查方式"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "调查结果",
                            decorator_id: "research_result",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择调查结果" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: research_result,
                                placeholder: "请选择调查结果"
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "属于国家建档立卡",
                            decorator_id: "has_card",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'属于国家建档立卡',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '属于国家建档立卡',
                                }, {
                                    label: '否',
                                    value: '不属于国家建档立卡',
                                }]
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否低保",
                            decorator_id: "subsistence_allowances",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'有低保',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '有低保',
                                }, {
                                    label: '否',
                                    value: '没有低保',
                                }]
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否在校大学生",
                            decorator_id: "college_student",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'在校大学生',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '在校大学生',
                                }, {
                                    label: '否',
                                    value: '不是在校大学生',
                                }]
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否危房改造",
                            decorator_id: "dangerous_house",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'危房改造',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '危房改造',
                                }, {
                                    label: '否',
                                    value: '不是危房改造',
                                }]
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否就业",
                            decorator_id: "has_work",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'就业',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '就业',
                                }, {
                                    label: '否',
                                    value: '没有就业',
                                }]
                            }
                        },
                        {
                            type: InputType.select,
                            label: "就业形式",
                            decorator_id: "work_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写就业形式" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: work_type,
                                placeholder: "请填写就业形式"
                            }
                        },
                        {                 
                            type: InputType.text_area,
                            label: "未就业原因",
                            decorator_id: "unwork_reason",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写未就业原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写未就业原因"
                            },
                        },
                        {                 
                            type: InputType.text_area,
                            label: "职业技能培训",
                            decorator_id: "vocational_skills_training",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写职业技能培训" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写职业技能培训"
                            },
                        },
                        {                 
                            type: InputType.text_area,
                            label: "农村实用技能培训",
                            decorator_id: "practical_skills_training",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写农村实用技能培训" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写农村实用技能培训"
                            },
                        },
                        {                 
                            type: InputType.text_area,
                            label: "其他帮扶",
                            decorator_id: "other_help",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写其他帮扶" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写其他帮扶"
                            },
                        },
                        {               
                            type: InputType.antd_input,
                            label: "困难残疾人生活补",
                            decorator_id: "difficulty_help",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写困难残疾人生活补" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写困难残疾人生活补"
                            },
                        },
                        {
                                            
                            type: InputType.antd_input,
                            label: "重度残疾人护理补",
                            decorator_id: "seriously_help",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写重度残疾人护理补" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写重度残疾人护理补"
                            },
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否享受托养服务",
                            decorator_id: "support_help",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'享受托养服务',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '享受托养服务',
                                }, {
                                    label: '否',
                                    value: '没有享受托养服务',
                                }]
                            }
                        },
                        {
                            type: InputType.date,
                            label: "动态更新年度是",
                            decorator_id: "update_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择日期" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择动态更新日期",
                            },
                        },
                        {
                            type: InputType.radioGroup,
                            label: "五年内是否有过家",
                            decorator_id: "has_newLife",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue:'有',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '有',
                                }, {
                                    label: '否',
                                    value: '没有',
                                }]
                            }
                        },
                        {                
                            type: InputType.antd_input,
                            label: "行政规划区名称",
                            decorator_id: "administrative_region",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写行政规划区名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写行政规划区名称"
                            },
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.researchManage);
                    }
                }
            ],
            submit_btn_propps: {
                    text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.research_service,
                operation_option: {
                    save: {
                        func_name: "update_research_data"
                    },
                    query: {
                        func_name: "",
                        arguments:[{}, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.allowanceSuccess); },
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：调研数据录入编辑控件
 * @description 调研数据录入编辑控件
 * @author
 */
@addon('ResearchInsertDisabledView', '调研数据录入编辑控件', '调研数据录入编辑控件')
@reactControl(ResearchInsertDisabledView, true)
export class ResearchInsertDisabledViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}