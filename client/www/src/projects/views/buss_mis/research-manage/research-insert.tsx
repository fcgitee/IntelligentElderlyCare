import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Icon, Row, Col } from "antd";
import { HomeCard } from "src/projects/components/home-card";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：调研资料录入视图控件状态
 */
export interface ResearchInsertViewState extends ReactViewState {
}
/**
 * 组件：调研资料录入视图控件
 */
export class ResearchInsertView extends ReactView<ResearchInsertViewControl, ResearchInsertViewState> {
    hrefUrl = (url: string) => {
        this.props.history!.push(url);
    }
    render() {
        return (
            <Row style={{paddingTop:420}}>
                <Row type="flex" justify="center" gutter={30}>
                    <Col span={5}    onClick={this.hrefUrl.bind(this, ROUTE_PATH.researchInsertDisabled)}>
                        <HomeCard>
                            <Icon type="edit" theme="filled" />
                            <strong>老年人</strong>
                        </HomeCard>
                    </Col>
                    <Col span={5}  onClick={this.hrefUrl.bind(this, ROUTE_PATH.researchInsertDisabled)}>
                        <HomeCard>
                            <Icon type="edit" theme="filled" />
                            <strong>残疾人</strong>
                        </HomeCard>
                    </Col>
                    <Col span={5}  onClick={this.hrefUrl.bind(this, ROUTE_PATH.researchInsertDisabled)}>
                        <HomeCard>
                            <Icon type="edit" theme="filled" />
                            <strong>低保户</strong>
                        </HomeCard>
                    </Col>
                </Row>
            </Row>
        );
    }
}

/**
 * 组件：调研资料录入视图控件
 * 控制调研资料录入视图控件
 */
@addon('ResearchInsertView', '调研资料录入视图控件', '控制调研资料录入视图控件')
@reactControl(ResearchInsertView, true)
export class ResearchInsertViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
