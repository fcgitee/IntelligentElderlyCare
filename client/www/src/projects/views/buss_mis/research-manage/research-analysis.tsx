import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
// 引入 ECharts 主模块
import echarts from 'echarts/lib/echarts';
// 引入柱状图
import  'echarts/lib/chart/bar';
// 引入柱状图
import  'echarts/lib/chart/pie';
// 引入提示框和标题组件
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
/**
 * 组件：调研数据分析页面
 */
export interface ResearchAnalysisViewState extends ReactViewState {
}

/**
 * 组件：调研数据分析页面
 * 描述
 */
export class  ResearchAnalysisView extends ReactView<ResearchAnalysisViewControl,  ResearchAnalysisViewState> {
    componentDidMount() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById("main") as HTMLDivElement);
        // 绘制图表
        myChart.setOption({
            title: { text: '近三年各年龄段人数变化' },
            legend: {},
            tooltip: {},
            dataset: {
                source: [
                    ['product', '2015', '2016', '2017'],
                    ['70-79岁', 400, 600, 800],
                    ['80-89岁', 500, 500, 400],
                    ['90-99岁', 800, 600, 500],
                    ['100岁以上', 900, 800, 600]
                ]
            },
            xAxis: {type: 'category'},
            yAxis: {},
            // Declare several bar series, each will be mapped
            // to a column of dataset.source by default.
            series: [
                {type: 'bar'},
                {type: 'bar'},
                {type: 'bar'}
            ]
        });
        // 基于准备好的dom，初始化echarts实例
        var myChart2 = echarts.init(document.getElementById("main2") as HTMLDivElement);
        // 绘制图表
        myChart2.setOption({
            title : {
                text: '享受的帮扶',
                subtext: '',
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: ['困难残疾人生活补','低保','享受托养服务','重度残疾人护理补','其他帮扶']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:335, name:'困难残疾人生活补'},
                        {value:310, name:'低保'},
                        {value:234, name:'享受托养服务'},
                        {value:135, name:'重度残疾人护理补'},
                        {value:1548, name:'其他帮扶'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        });
    }
    render() {
        return (
            <div>
            <div id="main" style={{ width: 720, height: 720 ,marginTop:50,paddingLeft:50}} />
            <div id="main2" style={{ width: 720, height: 720,marginTop:50,paddingRight:100,position:"absolute",right:0,top:0}} />
            </div>
        );
    }
}

/**
 * 控件：调研数据分析页面
 * 描述
 */
@addon(' ResearchAnalysisView', '调研数据分析页面', '提示')
@reactControl( ResearchAnalysisView, true)
export class  ResearchAnalysisViewControl extends ReactViewControl {
}