import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param, taskState } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from "antd";
const { Option } = Select;
/** 状态：调研现场管理 */
export interface ResearchManageViewState extends ReactViewState {
    // 任务紧急程度
    task_urgent?: any[];
    task_type?: any;
    /** 接口名 */
    request_url?: string;
}
/** 组件：调研现场管理 */
export class ResearchManageView extends React.Component<ResearchManageViewControl, ResearchManageViewState> {
    private columns_data_source = [
        {
            title: '调研任务名称',
            dataIndex: 'task_name',
            key: 'task_name',
        },
        {
            title: '任务执行人员',
            dataIndex: 'receiver_info_name',
            key: 'receiver_info_name',
        },
        {
            title: '任务执行状态',
            dataIndex: 'task_state',
            key: 'task_state',
        },
        {
            title: '紧急程度',
            dataIndex: 'task_urgent',
            key: 'task_urgent',
        },];
    constructor(props: ResearchManageViewControl) {
        super(props);
        this.state = {
            task_urgent: [],
            task_type: '4c4291de-c7e1-11e9-96ea-a0a4c57e9ebe',
            request_url: '',
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changeTask);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeTask + '/' + contents.id);
        }
    }
    componentDidMount() {
        // 获取任务紧急程度
        AppServiceUtility.task_service.input_task_urgent!()!
            .then((datas: any) => {
                this.setState({
                    task_urgent: datas,
                });
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let task_state_array: any = [];
        for (let i in taskState) {
            if (taskState[i]) {
                task_state_array.push(<Option key={i} value={taskState[i]}>{taskState[i]}</Option>);
            }
        }
        let urgent = this.state.task_urgent;
        let task_urgent: any[] = [];
        urgent!.map((item: any) => {
            task_urgent.push(<Option key={item} value={item}>{item}</Option>);
        });
        let personnel = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "任务名称",
                    decorator_id: "task_name"
                },
                {
                    type: InputType.select,
                    label: "任务状态",
                    decorator_id: "task_state",
                    option: {
                        placeholder: "请选择状态",
                        showSearch: true,
                        childrens: task_state_array,
                    }
                },
                {
                    type: InputType.select,
                    label: "任务紧急程度",
                    decorator_id: "task_urgent",
                    option: {
                        placeholder: "请选择任务紧急程度",
                        showSearch: true,
                        childrens: task_urgent,
                    }
                },
            ],
            btn_props: [{
                label: '新建任务',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.task_service,
            service_option: {
                select: {
                    service_func: 'get_all_task_list_all',
                    service_condition: [{ task_type: this.state.task_type }, 1, 10],
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <SignFrameLayout {...personnel_list} />
            )
        );
    }
}

/**
 * 控件：调研现场管理控制器
 * @description 调研现场管理
 * @author
 */
@addon('ResearchManageView', '调研现场管理', '调研现场管理')
@reactControl(ResearchManageView, true)
export class ResearchManageViewControl extends ReactViewControl {
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}