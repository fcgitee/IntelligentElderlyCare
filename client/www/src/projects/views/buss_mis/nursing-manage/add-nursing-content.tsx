import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { edit_props_info } from "src/projects/app/util-tool";
import { Select } from 'antd';
let { Option } = Select;
/**
 * 组件：编辑监控设备状态
 */
export interface AddNursingContentViewState extends ReactViewState {
    /** 组织机构 */
    level_list?: any;
    user_name?: any;
}

/**
 * 组件：编辑监控设备视图
 */
export class AddNursingContentView extends ReactView<AddNursingContentViewControl, AddNursingContentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            level_list: [],
            user_name: '',
        };
    }
    componentDidMount() {
        AppServiceUtility.competence_assessment_service.get_nursing_grade_list!({})!
            .then((data: any) => {
                // console.log(data);
                // 服务产品
                let level_list: any = [];
                data!.result!.map((item: any) => {
                    level_list.push(<Option key={item.id} value={item.id}>{item.name}</Option>);
                });
                this.setState({
                    level_list
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
        // 获取当前用户信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data) => {
                console.info(data);
                this.setState({
                    user_name: data[0].name,
                });
            })
            .catch((err) => {
                // console.log(err);
            });
    }
    render() {
        console.info(this.props.match!.params.key);
        let edit_props = {
            form_items_props: [{
                title: "护理内容",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "护理内容",
                        decorator_id: "nursing_content",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入护理内容" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入护理内容"
                        }
                    }, {
                        type: InputType.select,
                        label: "护理等级",
                        decorator_id: "nursing_level",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择护理等级" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            mode: "multiple",
                            placeholder: "请选择护理等级",
                            childrens: this.state.level_list,
                            showSearch: true,
                            optionFilterProp: "children",
                            filterOption: (input: any, option: any) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0,
                        },
                    }, {
                        type: InputType.text_area,
                        label: "服务描述",
                        decorator_id: "nursing_describe",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入服务描述" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入服务描述"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "创建人",
                        decorator_id: "founder",
                        field_decorator_option: {
                            rules: [{ required: false, message: "请输入创建人" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: this.state.user_name,
                            disabled: true
                        }
                    }, {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "nursing_remark",
                        field_decorator_option: {
                            rules: [{ message: "请输入备注" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注"
                        }
                    }
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    this.props.history!.push(ROUTE_PATH.nursingContent);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.competence_assessment_service,
                operation_option: {
                    query: {
                        func_name: "get_nursing_content_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_nursing_content"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingContent); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：新增/编辑护理内容控件
 * @description 新增/编辑护理内容控件
 * @author
 */
@addon('AddNursingContentView', '新增/编辑护理内容控件', '新增/编辑护理内容控件')
@reactControl(AddNursingContentView, true)
export class AddNursingContentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}