import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainContent } from "src/business/components/style-components/main-content";
import { Checkbox, Input, Row, Col, Card, Button, message } from 'antd';
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { ROUTE_PATH } from 'src/projects/router';
import './add-nursing-record.less';
/**
 * 组件：编辑监控设备状态
 */
export interface AddNursingRecordViewState extends ReactViewState {
    user_name?: any;
    nursing_content_list?: any;
    elder_id?: any;
    user_id?: any;
    elder_name?: any;
    user_status?: any;
}

/**
 * 组件：编辑监控设备视图
 */
export class AddNursingRecordView extends ReactView<AddNursingRecordViewControl, AddNursingRecordViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'personnel_info.sex',
        key: 'personnel_info.sex',
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            user_name: '',
            nursing_content_list: [],
            elder_id: '',
            elder_name: '',
            user_status: false
        };
    }
    componentWillMount() {
        if (this.props.match!.params.key) {
            // 编辑
            AppServiceUtility.competence_assessment_service!.get_nursing_record_list!({ id: this.props.match!.params.key }, 1, 1)!
                .then((datax: any) => {
                    // // console.log('护理内容', datax);
                    this.setState({
                        user_name: datax.result[0].operator_name,
                        user_id: datax.result[0].operator,
                        nursing_content_list: datax.result[0].content_list,
                        elder_id: datax.result[0].elder,
                        elder_name: datax.result[0].elder_name,
                        user_status: true
                    });
                });
        } else {
            // 获取当前用户信息
            AppServiceUtility.person_org_manage_service.get_current_user_info!()!
                .then((data) => {
                    console.info(data);
                    this.setState({
                        user_name: data[0].name,
                        user_id: data[0].id,
                    });
                })
                .catch((err) => {
                    // console.log(err);
                });
        }
    }
    older_change = (value: any) => {
        // console.info(value);
        this.setState({
            elder_id: value
        });
        // // console.log(value);
        // 拿到id去找到对应的护理等级
        AppServiceUtility.hotel_zone_service!.get_nursing_level_Byid!({ id: value })!
            .then((data: any) => {
                // // console.log(data);
                if (data.result.length > 0) {
                    let nursing_level = data.result[0].nursing_level;
                    // 拿到长者的护理等级去找到对应的id
                    AppServiceUtility.competence_assessment_service!.get_nursing_grade_list!({ name: nursing_level })!
                        .then((datas: any) => {
                            if (datas && datas.result && datas.result[0] && datas.result[0].id) {
                                let nursing_level_id = datas.result[0].id;
                                // 拿到长者的护理等级去找到对应的护理内容列表
                                AppServiceUtility.competence_assessment_service!.get_nursing_content_list!({ nursing_level: [nursing_level_id] })!
                                    .then((datax: any) => {
                                        // // console.log('护理内容', datax);
                                        let nursing_content_list: any = [];
                                        datax.result.map((item: any) => {
                                            nursing_content_list.push({
                                                id: item.id,
                                                nursing_content: item.nursing_content,
                                                state: false
                                            });
                                        });
                                        // // console.log(nursing_content_list);
                                        this.setState({
                                            nursing_content_list
                                        });
                                    });
                            } else {
                                message.error('没有找到该护理等级！');
                            }
                        });
                } else {
                    message.error('你选择的长者没有评估记录,请先进行能力评估');
                    return;
                }
            });
    }

    radioChange = (id: any, item: any) => {
        // // console.log(id);
        // let content_list: any = [];
        // let index_list = this.state.check_list;
        // content_list = this.state.content_list;
        // if (index_list.indexOf(id) !== -1) {
        //     index_list.splice(index_list.indexOf(id), 1);
        //     content_list.map((item: any, index: any) => {
        //         if (item.id === id) {
        //             content_list.splice(index, 1);
        //         }
        //     });
        // } else {
        //     index_list.push(id);
        //     content_list.push({
        //         id: item.id,
        //         nursing_content: item.nursing_content
        //     });
        // }
        // // console.log(content_list);
        // // console.log(index_list);
        // this.setState({
        //     check_list: index_list,
        //     content_list
        // });
        let nursing_content_list = this.state.nursing_content_list;
        nursing_content_list.map((item: any, index: any) => {
            if (id === item.id) {
                nursing_content_list[index].state = !nursing_content_list[index].state;
                nursing_content_list[index].remark = '';
            }
        });
        // // console.log('选择', nursing_content_list);
        this.setState({
            nursing_content_list
        });
    }
    inputChange = (e: any, id: any) => {
        let nursing_content_list = this.state.nursing_content_list;
        nursing_content_list.map((item: any, index: any) => {
            if (item.id === id) {
                nursing_content_list[index].remark = e.target.value;
            }
        });
        // // console.log(nursing_content_list);
        this.setState({
            nursing_content_list
        });
    }

    //  提交
    submit = () => {
        if (this.state.nursing_content_list.length === 0) {
            return;
        }
        let param = {
            id: this.props.match!.params.key ? this.props.match!.params.key : undefined,
            elder: this.state.elder_id,
            content_list: this.state.nursing_content_list,
            operator: this.state.user_id
        };
        AppServiceUtility.competence_assessment_service!.update_nursing_record!(param)!
            .then((datax: any) => {
                // // console.log('护理内容', datax);
                if (datax === 'Success') {
                    message.success('提交成功');
                    this.cancel();
                }
            });
    }
    cancel = () => {
        this.props.history!.push(ROUTE_PATH.elderNursingRecord);
    }
    render() {
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'check_in_alone_all',
            id_field: 'user_id',
            name_field: 'name',
            service_option: [{}, 1, 1],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let option = {
            placeholder: this.props.match!.params.key ? this.state.elder_name : "请选择长者姓名",
            modal_search_items_props: modal_search_items_props,
            onChange: this.older_change
        };
        return (
            <MainContent>
                <Card title="长者护理记录" >
                    <Row>
                        <Col span={12} offset={6}>
                            <Row type="flex">
                                <Col span={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    长者姓名：
                                </Col>
                                <Col span={20}>
                                    {this.state.user_status ?
                                        <Input value={this.state.elder_name} disabled={true} />
                                        :
                                        <ModalSearch {...option} />
                                    }
                                </Col>
                            </Row>
                            <br />
                            {this.state.nursing_content_list.map((item: any, index: any) => {
                                return (<Row type="flex" justify="start" key={item.id} style={{ marginBottom: '25px' }}>
                                    <Col span={4}>{item.nursing_content}</Col>
                                    <Col span={1}><Checkbox checked={item.state} onChange={() => { this.radioChange(item.id, item); }} /></Col>
                                    <Col span={19}>{item.state === true ? <Input value={item.remark} placeholder="备注" onChange={(e: any) => { this.inputChange(e, item.id); }} /> : ''}</Col>
                                </Row>);
                            })}
                            <Row type="flex">
                                <Col span={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    护理人：
                                </Col>
                                <Col span={20}>
                                    <Input placeholder={this.state.user_name} disabled={true} />
                                </Col>
                            </Row>
                            <br />
                            <br />
                            <Row type={"flex"} justify="center">
                                <Col span={4}>
                                    <Button onClick={this.submit}>提交</Button>
                                </Col>
                                <Col span={4}>
                                    <Button onClick={this.cancel}>取消</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Card>
            </MainContent >
        );
    }
}

/**
 * 控件：新增/编辑长者护理记录控件
 * @description 新增/编辑长者护理记录控件
 * @author
 */
@addon('AddNursingRecordView', '新增/编辑长者护理记录控件', '新增/编辑长者护理记录控件')
@reactControl(AddNursingRecordView, true)
export class AddNursingRecordViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}