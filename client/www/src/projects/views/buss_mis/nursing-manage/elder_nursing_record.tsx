
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：长者护理记录组件数据 */
export interface ElderBursingRecordState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：长者护理记录组件视图 */
export class ElderBursingRecord extends React.Component<ElderBursingRecordControl, ElderBursingRecordState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    }, {
        title: '护理内容',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <div>
                {
                    record.content_list.map((item: any) => {
                        return <div key={item}>{item.nursing_content}</div>;
                    })
                }
            </div>

        ),
    }, {
        title: '备注',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <div>
                {
                    record.content_list.map((item: any) => {
                        return <div key={item}>{item.remark}</div>;
                    })
                }
            </div>

        ),
    }, {
        title: '护理员',
        dataIndex: 'operator_name',
        key: 'operator_name',
    }, {
        title: '护理时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }];
    constructor(props: ElderBursingRecordControl) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.addElderNursingRecord);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addElderNursingRecord + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.competence_assessment_service!.get_nursing_record_list!({})!
            .then((datax: any) => {
                // console.log('护理内容', datax);
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name",
                    option: {
                        placeholder: '请输入长者姓名',
                    },
                },
                {
                    type: InputType.input,
                    label: "护理员",
                    decorator_id: "operator_name",
                    option: {
                        placeholder: '请输入护理员',
                    },
                },
                {
                    type: InputType.date,
                    label: "护理时间",
                    decorator_id: "create_date",
                    option: {
                        placeholder: '请选择护理时间',
                    },
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: '请输入组织机构',
                //     },
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.competence_assessment_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_nursing_record'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：长者护理记录组件控制器
 * @description 长者护理记录组件
 * @author
 */
@addon('ElderBursingRecord', '长者护理记录组件', '长者护理记录组件')
@reactControl(ElderBursingRecord, true)
export class ElderBursingRecordControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}