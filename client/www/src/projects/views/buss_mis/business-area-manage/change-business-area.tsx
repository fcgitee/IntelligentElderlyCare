import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;

/**
 * 组件：编辑业务区域管理状态
 */
export interface ChangeBusinessAreaViewState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 业务区域配置项 */
    item_list?: any[];
    /** 组织机构列表 */
    user_list?: any[];
}

/**
 * 组件：编辑业务区域管理
 * 描述
 */
export class ChangeBusinessAreaView extends ReactView<ChangeBusinessAreaViewControl, ChangeBusinessAreaViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            item_list: [],
            user_list: []
        };
    }
    componentDidMount() {
        AppServiceUtility.business_area_service.get_business_area_item!({})!
            .then((data: any) => {
                let list = data.result;
                let item_list: any[] = [];
                list!.map((item: any, idx: number) => {
                    item_list.push(<Option key={item['id']}>{item['name']}</Option>);
                });
                this.setState({
                    item_list
                });
                // console.log('item_list', item_list);
            });
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list_all!({}))
            .then((datas: any) => {
                this.setState({
                    user_list: datas.result,
                });
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let user = this.state.user_list;
        let user_list: any[] = [];
        user!.map((item, idx) => {
            user_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let columns_data_source = [{
            title: '配置项',
            dataIndex: 'item_id',
            key: 'item_id',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择配置项",
                childrens: this.state.item_list
            }
        }, {
            title: '配置值',
            dataIndex: 'value',
            key: 'value',
            type: 'input' as ColTypeStr
        }];
        let edit_props = {
            form_items_props: [
                {
                    type: InputTypeTable.select,
                    label: "组织机构",
                    decorator_id: "organization_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入组织机构" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入组织机构",
                        childrens: user_list
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "LOGO",
                    decorator_id: "logo",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入LOGO" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入LOGO"
                    }
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.businessArea);
                    }
                }
            ],
            submit_props: {
                text: "保存",
                // cb: this.handleSubmit
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            columns_data_source: columns_data_source,
            add_row_text: "新增行",
            table_field_name: 'aera_dispose',
            service_option: {
                service_object: AppServiceUtility.business_area_service,
                operation_option: {
                    query: {
                        func_name: "get_business_area_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "add_business_area"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.businessArea); },
            id: this.props.match!.params.key
        };
        return (
            <TableList {...edit_props} />
        );
    }
}

/**
 * 控件：编辑业务区域管理
 * 描述
 */
@addon('ChangeBusinessAreaView', '编辑业务区域管理', '描述')
@reactControl(ChangeBusinessAreaView, true)
export class ChangeBusinessAreaViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}