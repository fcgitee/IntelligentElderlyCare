import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：业务区域管理列表状态
 */
export interface BusinessAreaManageViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：业务区域管理列表
 * 描述
 */
export class BusinessAreaManageView extends ReactView<BusinessAreaManageViewControl, BusinessAreaManageViewState> {
    private columns_data_source = [{
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: 'LOGO',
        dataIndex: 'logo',
        key: 'logo',
    }];
    constructor(props: BusinessAreaManageViewControl) {
        super(props);
        this.state = {
            request_url:'',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeBusinessArea);
    }
    // /** 删除按钮 */
    // on_click_del = (key: any) => {
    //     request(this, this.businessAreaService!()!.del_business_area!([key.id]))
    //         .then((data: any) => {
    //             if (data === 'Success') {
    //                 message.info('删除成功');
    //             } else {
    //                 message.info(data);
    //             }
    //         })
    //         .catch((error: any) => {
    //             message.info('删除失败');
    //         });
    // }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeBusinessArea + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let behavioral_competence_assessment = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "组织机构",
                    decorator_id: "org_name"
                },
            ],
            btn_props: [{
                label: '新增业务区域',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.business_area_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_business_area'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let behavioral_competence_assessment_list = Object.assign(behavioral_competence_assessment, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：业务区域管理列表
 * 描述
 */
@addon('BusinessAreaManageView', '业务区域管理列表', '描述')
@reactControl(BusinessAreaManageView, true)
export class BusinessAreaManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}