
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：护理内容组件数据 */
export interface nursingRecordState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}
/** 组件：护理内容组件视图 */
export class nursingRecord extends React.Component<nursingRecordControl, nursingRecordState> {
    private columns_data_source = [{
        title: '护理模板',
        dataIndex: 'template_name',
        key: 'template_name',
    }, {
        title: '长者名字',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '护理时间',
        dataIndex: 'nursing_time',
        key: 'nursing_time',
    }, {
        title: '护理员名字',
        dataIndex: 'worker.name',
        key: 'worker.name',
    }];
    constructor(props: nursingRecordControl) {
        super(props);
        this.state = {
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.addNursingRecord);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addNursingRecord + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名字",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "护理员",
                    decorator_id: "worker_name"
                },
                {
                    type: InputType.date,
                    label: "护理时间",
                    decorator_id: "date"
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.competence_assessment_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{ is_nursing: true }, 1, 10]
                },
                delete: {
                    service_func: 'delete_competence_assessment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：护理内容组件控制器
 * @description 护理内容组件
 * @author
 */
@addon('nursingRecord', '护理记录组件', '护理记录组件')
@reactControl(nursingRecord, true)
export class nursingRecordControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}