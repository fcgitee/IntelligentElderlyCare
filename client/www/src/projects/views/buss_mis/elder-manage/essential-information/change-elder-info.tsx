import { Form, Row, Col, Card, Button } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { isPermission } from "src/projects/app/permission";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainContent } from "src/business/components/style-components/main-content";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";

export function getGoodLookTimeShow(begin_date: string, end_date: string) {
    if (!begin_date && !end_date) {
        // 否则就显示年月日
        return null;
    }
    if (!begin_date || !end_date) {
        // 否则就显示年月日
        return `${begin_date} - ${end_date}`;
    }
    let by = begin_date.slice(0, 4);
    let bm = begin_date.slice(5, 7);
    let bd = begin_date.slice(8, 10);
    let ey = end_date.slice(0, 4);
    let em = end_date.slice(5, 7);
    let ed = end_date.slice(8, 10);
    // 如果都是一天内，就只显示一个
    let nyear = new Date().getFullYear();
    // 2020-01-02 09:30:00 2020-01-02 10:30:00
    let string: string = '';

    // 开始时间的年份为基准
    if (Number(by) !== nyear) {
        // 非今年的都要加
        string += `${by}`;
    }
    // 月份直接加
    if (Number(by) !== nyear) {
        string += string === '' ? `${bm}` : `.${bm}`;
    } else {
        // string += string === '' ? `${Number(bm) + ''}月` : `${Number(bm) + ''}月`;
        string += `${Number(bm) + ''}月`;
    }
    // 日期直接加
    if (Number(by) !== nyear) {
        string += string === '' ? `${bd}` : `.${bd}`;
    } else {
        // string += string === '' ? `${bd}` : `${Number(bd) + ''}日`;
        string += `${Number(bd) + ''}日`;
    }
    // 时间直接加
    string += ` ${begin_date.slice(11, -3)} - `;

    let eflag = false;
    if (ey !== by && Number(ey) !== nyear) {
        // 结束时间非今年的并且跟开始年份不一样的都要加
        string += `${ey}.`;
        eflag = true;
    }
    if (eflag || em !== bm || ed !== bd) {
        // 加了年份日期不一样就要加
        if (Number(ey) !== nyear) {
            string += `${Number(bm) + ''}.${Number(ed) + ''}`;
        } else {
            string += `${Number(em) + ''}月${Number(ed) + ''}日`;
        }
    }
    // 时间直接加
    string += ` ${end_date.slice(11, -3)}`;
    return string;
}
/**
 * 组件：个案服务情况状态
 */
export interface ChangeElderInfoViewState extends ReactViewState {
    visible?: any;
    visible2?: any;
    id_card?: any;
    has_elder?: any;
    elder_id?: any;
    elder_name?: any;
    user_info?: any;
    description?: any;
    service_total_num?: any;
    edit_show?: any;
    startValue?: any;
    endValue?: any;
    endOpen?: any;
    data_list?: any;
    situation_summary?: any;
    edit_item?: any;
    show?: any;
    organization_id?: any;
    is_send?: boolean;
}

/**
 * 组件：个案服务情况视图
 */
export class ChangeElderInfoView extends ReactView<ChangeElderInfoViewControl, ChangeElderInfoViewState> {
    private columns_data_source = [{
        title: '活动名称',
        dataIndex: 'activity_name',
        key: 'activity_name',
    }, {
        title: '活动室',
        dataIndex: 'activity_room_name',
        key: 'activity_room_name',
    }, {
        title: '报名/限定人数',
        dataIndex: 'bmxdrs',
        key: 'bmxdrs',
        render: (text: any, record: any) => {
            return `${record.participate_count}/${record.max_quantity}`;
        },
    }, {
        title: '活动时间',
        dataIndex: 'hdsj',
        key: 'hdsj',
        render: (text: any, record: any) => {
            return getGoodLookTimeShow(record.begin_date, record.end_date);
        },
    },
        // {
        //     title: '服务对象',
        //     dataIndex: 'service_object',
        //     key: 'service_object',
        // }, 
        // {
        //     title: '发布时间',
        //     dataIndex: 'create_date',
        //     key: 'create_date',
        // },
        // {
        //     title: '活动状态',
        //     dataIndex: 'act_status',
        //     key: 'act_status',
        // },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            visible: false,
            visible2: false,
            id_card: '',
            has_elder: undefined,
            elder_id: undefined,
            user_info: {},
            description: '',
            service_total_num: '',
            edit_show: false,
            startValue: null,
            endValue: null,
            endOpen: false,
            data_list: {},
            situation_summary: [],
            edit_item: {},
            show: false,
            organization_id: '',
            is_send: false,
        };
    }

    componentDidMount() {
        // 获取当前登陆人的信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                this.setState({
                    organization_id: data[0].organization_id
                });
            });
        if (this.props.match!.params.key) {
            this.setState({
                show: true
            });
            AppServiceUtility.person_org_manage_service.user_org_link_list!({ id: this.props.match!.params.key }, 1, 1)!
                .then((data: any) => {
                    if (data && data.result && data.result[0] && data.result[0].user_info) {
                        this.setState({
                            user_info: data.result[0].user_info
                        });
                    }
                });
        }
    }

    add = () => {
        this.setState({
            edit_show: true
        });
    }

    // editZJ = (item: any) => {
    //     // console.log(item);
    //     this.setState({
    //         edit_show: true,
    //         edit_item: item
    //     });
    // }

    cancel = () => {
        this.props.form.resetFields();
        history.back();
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 10 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 14 },
                sm: { span: 16 },
            },
        };
        let elder_activity = {
            type_show: false,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: 'get_activity_participate_list',
                    service_condition: [{ 'user_id': this.state.user_info && this.state.user_info.id }, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
        };
        let elder_activity_list = Object.assign(elder_activity, table_param);
        return (
            (
                <MainContent>
                    <Card title="长者信息" className='grfwqk'>
                        <Form {...formItemLayout} className="ant-advanced-search-form">
                            <Row type="flex" justify="center" gutter={24}>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='姓名'>
                                        {this.state.user_info.name ? this.state.user_info.name : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='身份证'>
                                        {this.state.user_info.id_card ? this.state.user_info.id_card : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='性别'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.sex ? this.state.user_info.personnel_info.sex : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='周岁'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.age ? this.state.user_info.personnel_info.age : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='户籍所在地'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.native_place ? this.state.user_info.personnel_info.native_place : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='常住地址'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.address ? this.state.user_info.personnel_info.address : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='联系电话'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.telephone ? this.state.user_info.personnel_info.telephone : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='子女数'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.child_num ? this.state.user_info.personnel_info.child_num : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={12} style={{ display: 'block' }}>
                                    <Form.Item label='以往从事职业'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.old_job ? this.state.user_info.personnel_info.old_job : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={12} style={{ display: 'block' }}>
                                    <Form.Item label='兴趣爱好'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.interesting ? this.state.user_info.personnel_info.interesting : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='居住情况'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.living_conditions ? this.state.user_info.personnel_info.living_conditions : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='居住环境'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.living_environment ? this.state.user_info.personnel_info.living_environment : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='紧急联系人'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.emergency_contact_people ? this.state.user_info.personnel_info.emergency_contact_people : ''}
                                    </Form.Item>
                                </Col>
                                <Col span={6} style={{ display: 'block' }}>
                                    <Form.Item label='紧急联系电话'>
                                        {this.state.user_info.personnel_info && this.state.user_info.personnel_info.emergency_contact_phone ? this.state.user_info.personnel_info.emergency_contact_phone : ''}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row type='flex' justify='center' className="ctrl-btns">
                                <Button disabled={this.state.is_send} onClick={this.cancel}>返回</Button>
                            </Row>
                        </Form>
                    </Card>
                    {this.state.show ? <Card title="长者参与活动列表"><SignFrameLayout {...elder_activity_list} /></Card> : null}
                </MainContent>
            )
        );
    }
}

/**
 * 控件：老人档案-长者详情控件
 * @description 老人档案-长者详情控件
 * @author
 */
@addon('ChangeElderInfoView', '老人档案-长者详情控件', '老人档案-长者详情控件')
@reactControl(Form.create()(ChangeElderInfoView), true)
export class ChangeElderInfoViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public select_param?: any;

}