import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, message, Modal, Select } from "antd";
import { exprot_excel } from "src/business/util_tool";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
const { confirm } = Modal;
let { Option } = Select;
/**
 * 组件：编辑组织机构状态
 */
export interface ElderInfoListViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    visible?: any;
    photo?: any;
    show?: any;
    userShow?: boolean;
    activeUserId?: string;
    /** 导出的条件 */
    select_type?: any;
    sex_list?: any;
    state_list?: any;
}

/**
 * 组件：编辑组织机构视图
 */
export class ElderInfoListView extends ReactView<ElderInfoListViewControl, ElderInfoListViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'user_info.name',
        key: 'user_info.name',
    },
    {
        title: '身份证号',
        dataIndex: 'user_info.id_card',
        key: 'user_info.id_card',
    },
    {
        title: '性别',
        dataIndex: 'user_info.personnel_info.sex',
        key: 'user_info.personnel_info.sex',
    },
    {
        title: '周岁',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => {
            return (Number(new Date().getFullYear()) - Number(record.user_info.id_card.slice(6, 10)));
        }
    },
    {
        title: '常住地址',
        dataIndex: 'user_info.personnel_info.address',
        key: 'user_info.personnel_info.address',
    },
    {
        title: '子女数',
        dataIndex: 'user_info.personnel_info.child_num',
        key: 'user_info.personnel_info.child_num',
    },
    {
        title: '联系电话',
        dataIndex: 'user_info.personnel_info.telephone',
        key: 'user_info.personnel_info.telephone',
    },
    {
        title: '认定状态',
        dataIndex: 'days',
        key: 'days',
        render: (text: any, record: any) => {
            let days: any = record.days;
            if (days === null) {
                return '暂未认定';
            } else if (days > 90) {
                return '90天以上未认定';
            } else if (60 < days && days <= 90) {
                return '61-90天之内认定过';
            } else if (days <= 60) {
                return '60天内认定过';
            } else {
                return '无法认定';
            }
        }
    },
    {
        title: '参与活动数',
        dataIndex: 'user_info.participate_num',
        key: 'user_info.participate_num',
    },
    {
        title: '编辑时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },
    {
        title: '操作',
        dataIndex: 'action2',
        key: 'action2',
        render: (text: any, record: any) => (
            <div>
                <div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>查看</Button>
                        <Button type="danger" onClick={() => { this.del(record.id); }}>删除</Button>
                    </div>
                </div>
            </div>
        ),
    }];
    constructor(props: ElderInfoListViewControl) {
        super(props);
        this.state = {
            visible: false,
            photo: '',
            show: false,
            activeUserId: "",
            userShow: false,
            select_type: 1,
            sex_list: ['男', '女'],
            state_list: ['90天以上未认定', '61-90之间认定过', '60天内认定过', '无法认定']
        };
    }
    /** 新增按钮 */
    add = () => {
        this.setState({
            userShow: true
        });
    }
    /** 查看 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.changeElderInfoXfy + '/' + record.id);
    }

    // 删除
    del = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk() {
                AppServiceUtility.person_org_manage_service.delete_user_org_link!({ id: ids })!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功');
                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        message.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
    }

    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.setState({ show: true });
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            AppServiceUtility.person_org_manage_service.user_org_link_list!({})!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "姓名": item.name,
                                "身份证号": item.id_card,
                                "性别": item.personnel_info.sex,
                                "周岁": item.personnel_info.age,
                                "常住地址": item.personnel_info.address,
                                "子女数": item.personnel_info.child_num,
                                "联系电话": item.personnel_info.telephone,
                                "认定状态": item.days > 90 ? '90天以上未认定' : (item.days < 90 && item.days > 60 ? '61-90之间认定过' : (item.days <= 60 ? '60天内认定过' : '无法认定')),
                                "参与活动数": item.activity_num,
                                "编辑时间": item.modify_date
                            });
                        });
                        exprot_excel([{ name: '长者列表', value: new_data }], '长者列表（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }
    addUser = () => {
        if (this.state.activeUserId) {
            AppServiceUtility.person_org_manage_service.user_org_link!(this.state.activeUserId)!
                .then((data: any) => {
                    if (data === "Success") {
                        Modal.success({
                            content: '新增用户成功！',
                        });
                        this.setState({
                            userShow: false
                        });
                    }
                })
                .catch(e => {
                    console.error(e);
                });
        } else {

        }
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    render() {
        let sex_list: JSX.Element[] = this.state.sex_list.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let state_list: JSX.Element[] = this.state.state_list.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let service_item = {
            type_show: false,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            },
            {
                label: '导出',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'download'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "身份证号",
                    decorator_id: "id_card"
                },
                {
                    type: InputType.select,
                    label: "性别",
                    decorator_id: "sex",
                    option: {
                        childrens: sex_list,
                    }
                },
                {
                    type: InputType.input,
                    label: "子女数",
                    decorator_id: "child_num",
                },
                {
                    type: InputType.select,
                    label: "认定状态",
                    decorator_id: "days",
                    option: {
                        childrens: state_list,
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'user_org_link_list',
                    service_condition: [{}, 1, 1]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            }, {
                title: '性别',
                dataIndex: 'personnel_info.sex',
                key: 'personnel_info.sex',
            },
            ],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_all_elder_list',
            service_option: [{}, 1, 1],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let option = {
            placeholder: "请选择长者姓名",
            modal_search_items_props: modal_search_items_props,
            onChange: ((e: any) => {
                this.setState({
                    activeUserId: e
                });
            })
        };
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                />
                <Modal
                    title="请选择用户"
                    visible={this.state.userShow}
                    onOk={() => this.addUser()}
                    onCancel={() => this.onCancel({ userShow: !this.state.userShow })}
                    okText="确定"
                    cancelText="取消"
                >
                    <ModalSearch {...option} />
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：长者列表控件
 * @description 长者列表控件
 * @author
 */
@addon('ElderInfoListView', '长者列表控件', '长者列表控件')
@reactControl(ElderInfoListView, true)
export class ElderInfoListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}