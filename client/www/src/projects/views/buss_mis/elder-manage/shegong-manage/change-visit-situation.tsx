import { Button, Form, Row, Col, Input, Select, message, Icon, TimePicker, DatePicker, Card } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { isPermission } from "src/projects/app/permission";
import { AppServiceUtility } from "src/projects/app/appService";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import moment from 'moment';
const { Option } = Select;
const { TextArea } = Input;
import './index.less';
import { MainContent } from "src/business/components/style-components/main-content";
import { getGoodLookTimeShow } from "../../activity-manage/activityPublish";
// import { ROUTE_PATH } from "src/projects/router";
class MySecondForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            num: 1,
            show: this.props.show,
            volunteer_list: [],
            work_list: []
        };
    }
    getData = () => {
        AppServiceUtility.person_org_manage_service.get_visit_situation_list!({ id: (this.props.id || 'abc') }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        num: data.result[0].situation_summary.length === 0 ? 1 : data.result[0].situation_summary.length + 1
                    });
                }
            })
            .catch(e => {
                console.error(e);
            });
    }
    componentDidMount() {
        if (JSON.stringify(this.props.edit_item) === '{}') {
            this.props.form.resetFields();
        }
        this.getData();
        // 获取当前机构下的志愿者队伍
        AppServiceUtility.person_org_manage_service.get_groups_volunteer_list!({})!
            .then((data: any) => {
                if (data.result) {
                    this.setState({
                        volunteer_list: data.result
                    });
                }
            })
            .catch(e => {
                console.error(e);
            });
        // 获取当前机构下的服务人员
        AppServiceUtility.person_org_manage_service.get_worker_list_all!({ type: '幸福院' })!.then((data: any) => {
            if (data.result) {
                this.setState({
                    work_list: data.result
                });
            }
        });
    }

    // 表单提交
    submit = (e: any) => {
        if (!this.props.id) {
            message.info('请先添加探访长者基本信息！');
            return;
        }
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (err) {
                return;
            }
            values['modify_date'] = new Date();
            let param = {
                id: this.props.id,
                situation_summary: values,
                table_name: 'PT_Visit_Situation'
            };
            AppServiceUtility.person_org_manage_service.update_situation_summary!(param)!
                .then((data: any) => {
                    if (data === 'Success') {
                        message.success('新增成功');
                        this.props.ok && this.props.ok();
                        // this.props.form.resetFields();
                        // this.props.hidden(false);
                    }
                })
                .catch(e => {
                    console.error(e);
                });
        });
    }

    cancle = () => {
        this.props.form.resetFields();
        this.props.hidden(false);
    }

    render() {
        let { edit_item } = this.props;
        let { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                md: { span: 6 },
            },
            wrapperCol: {
                md: { span: 18 },
            },
        };
        // const formItemLayout2 = {
        //     labelCol: {
        //         md: { span: 3 },
        //     },
        //     wrapperCol: {
        //         md: { span: 21 },
        //     },
        // };
        return (
            <Form className="ant-advanced-search-form" onSubmit={this.submit}>
                <Row gutter={24}>
                    <Col span={6}>
                        <Form.Item label='标题' {...formItemLayout}>
                            {getFieldDecorator('title', {
                                initialValue: edit_item.title ? edit_item.title : '第' + this.state.num + '次探访服务反馈',
                                rules: [
                                    {
                                        required: true,
                                        message: '请输入标题！',
                                    },
                                ],
                            })(<Input />)}
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label='服务时间' {...formItemLayout}>
                            {getFieldDecorator('day', {
                                initialValue: edit_item.day ? moment(edit_item.day, 'YYYY/MM/DD') : '',
                                rules: [
                                    {
                                        required: true,
                                        message: '请输入服务时间！',
                                    },
                                ],
                            })(<DatePicker placeholder="请选择服务日期" />)}
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item>
                            {getFieldDecorator('start', {
                                initialValue: edit_item.day ? moment(edit_item.start) : '',
                                rules: [
                                    {
                                        required: true,
                                        message: '请选择开始时间！',
                                    },
                                ],
                            })(<TimePicker placeholder="请选择开始时间！" />)}
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item>
                            {getFieldDecorator('end', {
                                initialValue: edit_item.day ? moment(edit_item.end) : '',
                                rules: [
                                    {
                                        required: true,
                                        message: '请选择结束时间！',
                                    },
                                ],
                            })(<TimePicker placeholder="请选择结束时间！" />)}
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item label='情况总结'>
                            {getFieldDecorator('description', {
                                initialValue: edit_item.description ? edit_item.description : '',
                                rules: [
                                    {
                                        required: true,
                                        message: '请输入情况总结！',
                                    },
                                ],
                            })(<TextArea rows={4} />)}
                        </Form.Item>
                    </Col>
                    <Col span={18}>
                        <Form.Item label='服务人员'>
                            {getFieldDecorator('servicer', {
                                initialValue: edit_item.servicer ? edit_item.servicer : [],
                                rules: [
                                    {
                                        required: false,
                                        message: '请选择服务人员！',
                                    },
                                ],
                            })(<Select
                                mode="multiple"
                                style={{ width: '100%' }}
                                placeholder="请选择服务人员"
                            >
                                {this.state.work_list && this.state.work_list.map((item: any, index: number) => {
                                    return (
                                        <Option key={item.id}>{item.name}</Option>
                                    );
                                })}
                            </Select>)}
                        </Form.Item>
                    </Col>
                    <Col span={18}>
                        <Form.Item label='志愿者'>
                            {getFieldDecorator('volunteer', {
                                initialValue: edit_item.volunteer ? edit_item.volunteer : [],
                                rules: [
                                    {
                                        required: false,
                                        message: '请选择志愿者！',
                                    },
                                ],
                            })(<Select
                                mode="multiple"
                                style={{ width: '100%' }}
                                placeholder="请选择志愿者"
                            >
                                {this.state.volunteer_list && this.state.volunteer_list.map((item: any, index: number) => {
                                    return (
                                        <Option key={item.id}>{item.name}</Option>
                                    );
                                })}
                            </Select>)}
                        </Form.Item>
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Button type="primary" htmlType="submit">
                        保存
                        </Button>
                    <Button style={{ marginLeft: 8 }} onClick={this.cancle}>
                        取消
                        </Button>
                </Row>
            </Form>
        );
    }
}
const Form2 = Form.create<any>()(MySecondForm);

/**
 * 组件：探访服务反馈状态
 */
export interface ChangeVisitSituationViewState extends ReactViewState {
    visible?: any;
    visible2?: any;
    id_card?: any;
    has_elder?: any;
    elder_id?: any;
    elder_name?: any;
    user_info?: any;
    description?: any;
    service_total_num?: any;
    edit_show?: any;
    startValue?: any;
    endValue?: any;
    endOpen?: any;
    data_list?: any;
    situation_summary?: any;
    edit_item?: any;
    new_id?: any;
}

/**
 * 组件：探访服务反馈视图
 */
export class ChangeVisitSituationView extends ReactView<ChangeVisitSituationViewControl, ChangeVisitSituationViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'personnel_info.sex',
        key: 'personnel_info.sex',
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            visible: false,
            visible2: false,
            id_card: '',
            has_elder: undefined,
            elder_id: undefined,
            user_info: {
                personnel_info: {}
            },
            description: '',
            service_total_num: '',
            edit_show: false,
            startValue: null,
            endValue: null,
            endOpen: false,
            data_list: {},
            situation_summary: [],
            edit_item: {},
            new_id: '',
        };
    }

    select_list = () => {
        AppServiceUtility.person_org_manage_service.get_visit_situation_list!({ id: (this.props.match!.params.key || this.state.new_id || 'abc') }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        situation_summary: data.result[0].situation_summary,
                        data_list: data.result[0],
                        user_info: data.result[0].user,
                        service_total_num: data.result[0].service_total_num,
                        description: data.result[0].description,
                    });
                }
            })
            .catch(e => {
                console.error(e);
            });
    }
    ok = () => {
        this.select_list();
        this.setState({
            edit_show: false,
        });
    }

    componentDidMount() {
        if (!this.props.match!.params.key) {
            this.props.form.resetFields();
        }
        if (this.props.match!.params.key) {
            this.select_list();
        }
    }

    // 表单提交
    submit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (err) {
                return;
            }
            values['elder_id'] = this.state.elder_id;
            if (this.props.match!.params.key) {
                values['id'] = this.props.match!.params.key;
                values['situation_summary'] = this.state.data_list.situation_summary;
            } else {
                values['situation_summary'] = [];
            }
            AppServiceUtility.person_org_manage_service.update_visit_situation!(values)!
                .then((data: any) => {
                    if (data.status === 'Success') {
                        message.info('保存成功！');
                        // this.props.form.resetFields();
                        this.setState(
                            {
                                new_id: data.new_id
                            }
                        );
                        // this.props.history!.push(ROUTE_PATH.serviceSituationList);
                    } else {
                        message.info('保存失败！');
                    }
                })
                .catch(e => {
                    console.error(e);
                });
        });
    }

    older_change = (value: any) => {
        this.setState({
            elder_id: value
        });
        AppServiceUtility.person_org_manage_service.get_elder_list!({ id: value }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        user_info: data.result[0]
                    });
                }
            })
            .catch(e => {
                console.error(e);
            });
    }

    add = () => {
        if (!this.props.match!.params.key && !this.state.new_id) {
            message.info('请先添加探访长者基本信息！');
            return;
        }
        this.setState({
            edit_show: true
        });
    }

    editZJ = (item: any) => {
        this.setState({
            edit_show: true,
            edit_item: item
        });
    }

    cancel = () => {
        this.props.form.resetFields();
        history.back();
    }

    hidden = (value: any) => {
        this.select_list();
        this.setState({
            edit_show: value,
            edit_item: {}
        });
    }

    render() {
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_all_elder_list',
            service_option: [{}, 1, 1],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let option = {
            placeholder: this.props.match!.params.key ? this.state.elder_name : "请选择长者姓名",
            modal_search_items_props: modal_search_items_props,
            onChange: this.older_change
        };
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                md: { span: 6 },
            },
            wrapperCol: {
                md: { span: 18 },
            },
        };
        const formItemLayout2 = {
            labelCol: {
                md: { span: 3 },
            },
            wrapperCol: {
                md: { span: 21 },
            },
        };
        let { user_info, data_list } = this.state;
        if (!user_info) {
            user_info = {
                personnel_info: {}
            };
        }
        if (user_info && !user_info['personnel_info']) {
            user_info['personnel_info'] = {};
        }
        return (
            (
                <MainContent>
                    <Card title={'探访长者基本信息'} className='grfwqk'>
                        <Form className="ant-advanced-search-form" onSubmit={this.submit}>
                            <Row>
                                <Col span={6}>
                                    <Form.Item label='姓名' {...formItemLayout}>
                                        {getFieldDecorator('name', {
                                            initialValue: data_list.name || user_info.name || '',
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '请输入姓名！',
                                                },
                                            ],
                                        })(<ModalSearch {...option} />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='身份证' {...formItemLayout}>
                                        {getFieldDecorator('id_card', {
                                            initialValue: data_list.id_card || user_info.id_card || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入身份证！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入姓名" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='性别' {...formItemLayout}>
                                        {getFieldDecorator('sex', {
                                            initialValue: data_list.sex || user_info.personnel_info.sex || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请选择性别！',
                                                },
                                            ],
                                        })(<Select>
                                            <Option value="男">男</Option>
                                            <Option value="女">女</Option>
                                        </Select>)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='周岁' {...formItemLayout}>
                                        {getFieldDecorator('age', {
                                            initialValue: data_list.age || user_info.personnel_info.age || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入周岁！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入周岁" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={6}>
                                    <Form.Item label='户籍所在地' {...formItemLayout}>
                                        {getFieldDecorator('native_place', {
                                            initialValue: data_list.native_place || user_info.personnel_info.native_place || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入户籍所在地！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入户籍所在地" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='常住地址' {...formItemLayout}>
                                        {getFieldDecorator('address', {
                                            initialValue: data_list.address || user_info.personnel_info.address || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入常住地址！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入常住地址" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='联系电话' {...formItemLayout}>
                                        {getFieldDecorator('telephone', {
                                            initialValue: data_list.telephone || user_info.personnel_info.telephone || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入联系电话！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入联系电话" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='子女数' {...formItemLayout}>
                                        {getFieldDecorator('child_num', {
                                            initialValue: data_list.child_num || user_info.personnel_info.child_num || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入子女数！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入子女数" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <Form.Item label='以往从事职业' {...formItemLayout2}>
                                        {getFieldDecorator('old_job', {
                                            initialValue: data_list.old_job || user_info.personnel_info.old_job || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入以往从事职业！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入以往从事职业" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item label='兴趣爱好' {...formItemLayout2}>
                                        {getFieldDecorator('interesting', {
                                            initialValue: data_list.interesting || user_info.personnel_info.interesting || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入兴趣爱好！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入兴趣爱好" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={6}>
                                    <Form.Item label='居住情况' {...formItemLayout}>
                                        {getFieldDecorator('living_conditions', {
                                            initialValue: data_list.living_conditions || user_info.personnel_info.living_conditions || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入居住情况！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入居住情况" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='居住环境' {...formItemLayout}>
                                        {getFieldDecorator('living_environment', {
                                            initialValue: data_list.living_environment || user_info.personnel_info.living_environment || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入居住环境！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入居住环境" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='紧急联系人' {...formItemLayout}>
                                        {getFieldDecorator('emergency_contact_people', {
                                            initialValue: data_list.emergency_contact_people || user_info.personnel_info.emergency_contact_people || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入紧急联系人！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入紧急联系人" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item label='紧急联系电话' {...formItemLayout}>
                                        {getFieldDecorator('emergency_contact_phone', {
                                            initialValue: data_list.emergency_contact_phone || user_info.personnel_info.emergency_contact_phone || '',
                                            rules: [
                                                {
                                                    required: false,
                                                    message: '请输入紧急联系电话！',
                                                },
                                            ],
                                        })(<Input placeholder="请输入紧急联系电话" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row type="flex" justify="center">
                                <Button type="primary" htmlType="submit">
                                    保存
                                    </Button>
                                <Button style={{ marginLeft: 8 }} onClick={this.cancel}>
                                    取消
                                    </Button>
                                {/* <Col span={8} style={{ textAlign: 'right' }}>
                                <a style={{ marginLeft: 8, fontSize: 12 }}>
                                    取消编辑<Icon type={this.state.editShow ? 'up' : 'down'} />
                                </a>
                            </Col> */}
                            </Row>
                            <br /><br /><br />
                            <Form.Item>
                                <Button type="dashed" style={{ width: '100%' }} onClick={this.add}>
                                    <Icon type="plus" /> 添加探访服务反馈
                                    </Button>
                            </Form.Item>
                        </Form>
                        {this.state.edit_show ?
                            <Form2 id={this.props.match!.params.key || this.state.new_id} show={this.state.edit_show} edit_item={this.state.edit_item} hidden={this.hidden} ok={this.ok} />
                            : ''
                        }
                        {this.state.situation_summary.map((item: any) => {
                            return <Card
                                style={{ marginTop: '30px' }}
                                key={item}
                                title={<div>
                                    <span style={{ fontSize: '20px', color: '#f6c511', marginRight: '15px' }}>{item.title}</span>
                                    <span style={{ fontSize: '13px', marginRight: '10px' }}>{getGoodLookTimeShow(item.start_date, item.end_date, true)}</span>
                                    <span style={{ fontSize: '14px' }}>{item.servicer && item.servicer.map((items: any, index: any) => {
                                        return index === item.servicer.length - 1 ? items : items + '、';
                                    })} {item.servicer ? item.servicer.length : 0}{"名社工和"}{item.volunteer_list && item.volunteer_list.join ? item.volunteer_list.join('、') : ''} {item.volunteer_list ? item.volunteer_list.length : 0}{'名志愿者参与了本次探访服务'}
                                    </span>
                                </div>}
                            >
                                <Row type="flex" justify="center">
                                    <Col span={2}>情况总结：</Col>
                                    <Col span={22} className="zjbjsj-qktj">{item.description}</Col>
                                </Row>
                                <br /><br />
                                <Row type="flex" justify="center">
                                    <Col className="zjbjsj">
                                        最近编辑时间：{item.modify_date}
                                    </Col>
                                    <Col className="zjbjsj-line">
                                        |
                                    </Col>
                                    <Col>
                                        <Button type="primary" onClick={() => { this.editZJ(item); }}>
                                            编辑
                                    </Button>
                                    </Col>
                                </Row>
                            </Card>;
                        })}
                    </Card>
                </MainContent>
            )
        );
    }
}

/**
 * 控件：探访服务反馈编辑控件
 * @description 探访服务反馈编辑控件
 * @author
 */
@addon('ChangeVisitSituationView', '探访服务反馈编辑控件', '探访服务反馈编辑控件')
@reactControl(Form.create()(ChangeVisitSituationView), true)
export class ChangeVisitSituationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public select_param?: any;

}