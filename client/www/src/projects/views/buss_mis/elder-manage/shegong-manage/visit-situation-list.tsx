import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, message, Modal, Select } from "antd";
const { confirm } = Modal;
let { Option } = Select;
/**
 * 组件：探访服务情况列表状态
 */
export interface VisitSituationListViewState extends ReactViewState {
    sex_list?: any;
    is_edit_btn?: any;
    is_delete_btn?: any;
}

/**
 * 组件：探访服务情况列表视图
 */
export class VisitSituationListView extends ReactView<VisitSituationListViewControl, VisitSituationListViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        //     {
        //     title: '序号',
        //     dataIndex: 'name',
        //     key: 'name',
        // },
        {
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '身份证号',
            dataIndex: 'id_card',
            key: 'id_card',
        },
        {
            title: '性别',
            dataIndex: 'sex',
            key: 'sex',
        },
        {
            title: '周岁',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: '常住地址',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: '联系电话',
            dataIndex: 'telephone',
            key: 'telephone',
        },
        {
            title: '已探访总次数',
            dataIndex: 'situation_summary',
            key: 'situation_summary',
            render: (text: any, record: any) => {
                return record.situation_summary.length;
            }
        },
        {
            title: '编辑时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        },
        {
            title: '操作',
            dataIndex: 'action2',
            key: 'action2',
            render: (text: any, record: any) => (
                <div>
                    <div>
                        <div>
                            {
                                this.state.is_edit_btn ?
                                    <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>编辑</Button>
                                    : ''
                            }
                            {
                                this.state.is_delete_btn ?
                                    <Button type="danger" onClick={() => { this.del(record.id); }}>删除</Button>
                                    : ''
                            }
                        </div>
                    </div>
                </div>
            ),
        }];
    constructor(props: VisitSituationListViewControl) {
        super(props);
        this.state = {
            sex_list: ['男', '女'],
            is_edit_btn: false,
            is_delete_btn: false
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeVisitSituation);
    }
    /** 编辑 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.changeVisitSituation + '/' + record.id);
    }

    // 删除
    del = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                AppServiceUtility.person_org_manage_service.delete_visit_situation_byid!({ id: ids })!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功');
                            this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                        }
                    })
                    .catch(error => {
                        console.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
    }

    componentWillMount() {
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        } else if (value.function + value.permission === this.props.delete_permission) {
                            this.setState({
                                is_delete_btn: true
                            });
                        }
                    });
                }
            });
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }

    render() {
        let sex_list: JSX.Element[] = this.state.sex_list.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let service_item = {
            type_show: false,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入姓名',
                    }
                },
                {
                    type: InputType.input,
                    label: "身份证号",
                    decorator_id: "id_card",
                    option: {
                        placeholder: '请输入身份证号',
                    }
                },
                {
                    type: InputType.select,
                    label: "性别",
                    decorator_id: "sex",
                    option: {
                        childrens: sex_list,
                        placeholder: '请选择性别',
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "date_range",
                    option: {
                        placeholder: ['请输入开始时间', '请输入结束时间'],
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_visit_situation_list',
                    service_condition: [{}, 1, 1]
                },
                delete: {
                    service_func: 'delete_visit_situation_byid'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            onRef: this.onRef,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
            </div>
        );
    }
}

/**
 * 控件：探访服务情况列表控件
 * @description 探访服务情况列表控件
 * @author
 */
@addon('VisitSituationListView', '探访服务情况列表控件', '探访服务情况列表控件')
@reactControl(VisitSituationListView, true)
export class VisitSituationListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}