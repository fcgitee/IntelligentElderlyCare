import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, message, Modal, Select } from "antd";
const { confirm } = Modal;
let { Option } = Select;
/**
 * 组件：编辑组织机构状态
 */
export interface ServiceSituationListViewState extends ReactViewState {
    sex_list?: any;
    is_edit_btn?: any;
    is_delete_btn?: any;
}

/**
 * 组件：编辑组织机构视图
 */
export class ServiceSituationListView extends ReactView<ServiceSituationListViewControl, ServiceSituationListViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        //     {
        //     title: '序号',
        //     dataIndex: 'name',
        //     key: 'name',
        // },
        {
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '身份证号',
            dataIndex: 'id_card',
            key: 'id_card',
        },
        {
            title: '性别',
            dataIndex: 'sex',
            key: 'sex',
        },
        {
            title: '周岁',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: '常住地址',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: '联系电话',
            dataIndex: 'telephone',
            key: 'telephone',
        },
        {
            title: '预计服务总次数',
            dataIndex: 'service_total_num',
            key: 'service_total_num',
        },
        {
            title: '已服务总次数',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => {
                return record.situation_summary.length;
            }
        },
        {
            title: '编辑时间',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                <div>
                    <div style={{ textAlign: 'center' }}>
                        <div>{record.modify_date}</div>
                    </div>
                </div>

            ),
        },
        {
            title: '操作',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                <div>
                    <div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.is_edit_btn ?
                                    <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>编辑</Button>
                                    : ''
                            }
                            {
                                this.state.is_delete_btn ?
                                    <Button type="danger" onClick={() => { this.del(record.id); }}>删除</Button>
                                    : ''
                            }
                        </div>
                    </div>
                </div>

            ),
        }];
    constructor(props: ServiceSituationListViewControl) {
        super(props);
        this.state = {
            sex_list: ['男', '女'],
            is_edit_btn: false,
            is_delete_btn: false
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceSituation);
    }
    /** 编辑 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.changeServiceSituation + '/' + record.id);
    }

    // 删除
    del = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                AppServiceUtility.person_org_manage_service.delete_service_situation_byid!({ id: ids })!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功');
                            this.formCreator && this.formCreator.reflash();
                        }
                    })
                    .catch(error => {
                        console.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
    }

    componentWillMount() {
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        } else if (value.function + value.permission === this.props.delete_permission) {
                            this.setState({
                                is_delete_btn: true
                            });
                        }
                    });
                }
            });
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }

    render() {
        let sex_list: JSX.Element[] = this.state.sex_list.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let service_item = {
            type_show: false,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "身份证号",
                    decorator_id: "id_card"
                },
                {
                    type: InputType.select,
                    label: "性别",
                    decorator_id: "sex",
                    option: {
                        childrens: sex_list,
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_service_situation_list',
                    service_condition: [{}, 1, 1]
                },
                delete: {
                    service_func: 'delete_service_situation_byid'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            onRef: this.onRef,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
            </div>
        );
    }
}

/**
 * 控件：个案服务情况列表控件
 * @description 个案服务情况列表控件
 * @author
 */
@addon('ServiceSituationListView', '个案服务情况列表控件', '个案服务情况列表控件')
@reactControl(ServiceSituationListView, true)
export class ServiceSituationListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}