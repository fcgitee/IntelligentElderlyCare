import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { getAge2 } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Card, Row, Col, Input, Button, Table } from "antd";
import { request } from "src/business/util_tool";
/**
 * 组件：适老化设备管理
 */
export interface ChangeDeviceManageViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
    data_info?: any;
    data_list?: any;
    table_total_size?: number;
    default_page_size: number;
}

/**
 * 组件：适老化设备管理
 */
export default class ChangeDeviceManageView extends ReactView<ChangeDeviceManageViewControl, ChangeDeviceManageViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            data_info: {},
            data_list: [],
            table_total_size: 0,
            default_page_size: 10,
        };
    }
    componentDidMount = () => {
        request(this, AppServiceUtility.call_center_nh_service.get_device_manage_list!({ user_id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });

        this.getDataList(1, this.state.default_page_size);
    }
    getDataList = (page: number, pageSize: number) => {
        request(this, AppServiceUtility.call_center_nh_service.get_edler_device_list!({ user_id: this.props.match!.params.key }, page, pageSize)!)
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        data_list: datas.result,
                        table_total_size: datas.total
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (contents: any) => {
        this.props.history!.push(ROUTE_PATH.changeDeviceFile + '/' + contents.device_id);
    }
    backList = () => {
        this.props.history!.push(ROUTE_PATH.deviceManage);
    }
    getHostId = (record: any) => {
        let host_ids: any = [];
        let exists: any = {};
        if (record.room_device_info && record.room_device_info.map) {
            record.room_device_info.map((item: any, index: number) => {
                if (!exists[item.host_id]) {
                    host_ids.push(item.host_id);
                    exists[item.host_id] = true;
                }
            });
        }
        return host_ids.length > 0 ? host_ids.join() : "";
    }

    onPageChange = (page: number, pageSize: number) => {
        this.getDataList(page, pageSize);
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 10 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 14 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form;
        let { data_info, data_list } = this.state;

        const columns_data_source = [
            {
                title: '设备名称',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '设备类型',
                dataIndex: 'device_type',
                key: 'device_type',
            }, {
                title: '设备ID',
                dataIndex: 'device_id',
                key: 'device_id',
            }, {
                title: '位置',
                dataIndex: 'location',
                key: 'location',
            }, {
                title: '主机ID',
                dataIndex: 'host_id',
                key: 'host_id',
            }, {
                title: '安装日期',
                dataIndex: 'create_date',
                key: 'create_date',
            }, {
                title: '操作',
                dataIndex: 'action',
                key: 'check_status',
                render: (text: any, record: any) => {
                    return (
                        <Button type={'link'} onClick={() => this.onIconClick(record)}>查看</Button>
                    );
                }
            }
        ];
        return (
            <MainContent>
                <Card title="居家智能化设备档案">
                    <Form {...formItemLayout}>
                        <Row type="flex" justify="center" gutter={24}>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='长者姓名'>
                                    {getFieldDecorator('elder_name', {
                                        initialValue: data_info.elder_name ? data_info.elder_name : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='性别'>
                                    {getFieldDecorator('elder_sex', {
                                        initialValue: data_info.elder_sex ? data_info.elder_sex : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='年龄'>
                                    {getFieldDecorator('elder_age', {
                                        initialValue: data_info.elder_id_card ? getAge2(data_info.elder_id_card) : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='身份证号码'>
                                    {getFieldDecorator('elder_id_card', {
                                        initialValue: data_info.elder_id_card ? data_info.elder_id_card : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='身份证地址'>
                                    {getFieldDecorator('elder_id_card_address', {
                                        initialValue: data_info.elder_id_card_address ? data_info.elder_id_card_address : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='出生日期'>
                                    {getFieldDecorator('elder_date_birth', {
                                        initialValue: data_info.elder_date_birth ? data_info.elder_date_birth : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='联系方式'>
                                    {getFieldDecorator('elder_telephone', {
                                        initialValue: data_info.elder_telephone ? data_info.elder_telephone : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='家庭住址'>
                                    {getFieldDecorator('elder_address', {
                                        initialValue: data_info.elder_address ? data_info.elder_address : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='主机ID'>
                                    {getFieldDecorator('room_device_info', {
                                        initialValue: data_info.room_device_info ? this.getHostId(data_info) : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button onClick={() => this.backList()}>返回</Button>
                        </Row>
                    </Form>
                </Card>
                {data_info.user_id ? <Card title="设备列表">
                    <Table
                        columns={columns_data_source}
                        dataSource={data_list}
                        bordered={true}
                        rowKey="id"
                        pagination={{ pageSize: this.state.default_page_size, total: this.state.table_total_size, onChange: this.onPageChange }}
                    />
                </Card> : null}
            </MainContent>
        );
    }
}

/**
 * 控件：适老化设备管理
 * @description 适老化设备管理
 * @author
 */
@addon('ChangeDeviceManageView', '适老化设备管理', '适老化设备管理')
@reactControl(Form.create()(ChangeDeviceManageView), true)
export class ChangeDeviceManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}