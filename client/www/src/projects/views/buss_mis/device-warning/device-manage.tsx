
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
/** 状态：适老化设备管理 */
export interface DeviceManageViewState extends ReactViewState {
}
/** 组件：适老化设备管理 */
export class DeviceManageView extends React.Component<DeviceManageViewControl, DeviceManageViewState> {
    private columns_data_source = [
        {
            title: '长者姓名',
            dataIndex: 'elder_name',
            key: 'elder_name',
        }, {
            title: '身份证号码',
            dataIndex: 'elder_id_card',
            key: 'elder_id_card',
        }, {
            title: '主机ID',
            dataIndex: 'host_id',
            key: 'host_id',
            render: (text: any, record: any) => {
                let host_ids: any = [];
                let exists: any = {};
                if (record.room_device_info && record.room_device_info.map) {
                    record.room_device_info.map((item: any, index: number) => {
                        if (!exists[item.host_id]) {
                            host_ids.push(item.host_id);
                            exists[item.host_id] = true;
                        }
                    });
                }
                return host_ids.length > 0 ? host_ids.join() : "";
            }
        }, {
            title: '设备数量',
            dataIndex: 'device_count',
            key: 'device_count'
        }, {
            title: '联系电话',
            dataIndex: 'elder_telephone',
            key: 'elder_telephone',
        }, {
            title: '家庭地址',
            dataIndex: 'elder_address',
            key: 'elder_address',
        }
    ];
    constructor(props: DeviceManageViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount = () => {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeDeviceManage + '/' + contents.user_id);
        }
    }
    // add = () => {
    //     this.props.history!.push(ROUTE_PATH.changeDeviceManage);
    // }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        let deal_report = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '长者姓名',
                    decorator_id: "elder_name",
                    option: {
                        placeholder: '请输入长者姓名',
                    }
                },
                {
                    type: InputType.input,
                    label: '身份证号码',
                    decorator_id: "elder_id_card",
                    option: {
                        placeholder: '请输入身份证号码',
                    }
                },
                {
                    type: InputType.input,
                    label: '主机ID',
                    decorator_id: "host_id",
                    option: {
                        placeholder: '请输入主机ID',
                    }
                },
            ],
            // btn_props: [{
            //     label: '新增',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.call_center_nh_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [param, 1, 10]
                },
                // delete: {
                //     service_func: 'delete_call_center_zh'
                // }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            // delete_permission: this.props.delete_permission,
            searchExtraParam: param,
        };
        let deal_report_list = Object.assign(deal_report, table_param);
        return (
            <div>
                <SignFrameLayout {...deal_report_list} />
            </div>
        );
    }
}

/**
 * 控件：适老化设备管理控制器
 * @description 适老化设备管理
 * @author
 */
@addon('DeviceManageView', '适老化设备管理', '适老化设备管理')
@reactControl(Form.create<any>()(DeviceManageView), true)
export class DeviceManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}