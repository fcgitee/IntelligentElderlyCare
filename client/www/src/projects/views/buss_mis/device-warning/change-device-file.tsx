import Form from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { Card, Row, Col, Input, Button, Table } from "antd";
import { request } from "src/business/util_tool";
/**
 * 组件：适老化设备档案
 */
export interface ChangeDeviceFileViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
    data_info?: any;
}

/**
 * 组件：适老化设备档案
 */
export default class ChangeDeviceFileView extends ReactView<ChangeDeviceFileViewControl, ChangeDeviceFileViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            data_info: {},
        };
    }
    componentDidMount = () => {
        request(this, AppServiceUtility.call_center_nh_service.get_device_warn_list!({ device_id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result[0]) {
                    this.setState({
                        data_info: datas.result[0],
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (contents: any) => {
        this.props.history!.push(ROUTE_PATH.changeDeviceWarning + '/' + contents.id);
    }
    backList = () => {
        history.back();
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 10 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 14 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form;
        let { data_info } = this.state;

        const columns_data_source = [
            {
                title: '警报内容',
                dataIndex: 'warn_type',
                key: 'warn_type',
            }, {
                title: '警报时间',
                dataIndex: 'time',
                key: 'time',
            }, {
                title: '处理结果',
                dataIndex: 'check_status',
                key: 'check_status',
                render: (text: any, record: any) => {
                    if (text === '已处理') {
                        return `已处理，处理结果：【${record.result}】`;
                    }
                    return text;
                }
            }, {
                title: '操作',
                dataIndex: 'action',
                key: 'check_status',
                render: (text: any, record: any) => {
                    return (
                        <Button type={'link'} onClick={() => this.onIconClick(record)}>查看</Button>
                    );
                }
            }
        ];
        return (
            <MainContent>
                <Card title="居家适老化设备详情">
                    <Form {...formItemLayout}>
                        <Row type="flex" justify="center" gutter={24}>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='设备ID'>
                                    {getFieldDecorator('device_id', {
                                        initialValue: data_info.device_id ? data_info.device_id : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='设备名称'>
                                    {getFieldDecorator('name', {
                                        initialValue: data_info.name ? data_info.name : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='设备类型'>
                                    {getFieldDecorator('device_type', {
                                        initialValue: data_info.device_type ? data_info.device_type : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='关联主机ID'>
                                    {getFieldDecorator('host_id', {
                                        initialValue: data_info.host_id ? data_info.host_id : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='设备位置'>
                                    {getFieldDecorator('location', {
                                        initialValue: data_info.location ? data_info.location : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                            <Col span={8} style={{ display: 'block' }}>
                                <Form.Item label='安装日期'>
                                    {getFieldDecorator('create_date', {
                                        initialValue: data_info.create_date ? data_info.elder_date_bicreate_daterth : '',
                                        rules: [],
                                    })(<Input disabled={true} />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button onClick={() => this.backList()}>返回</Button>
                        </Row>
                    </Form>
                </Card>
                {data_info.user_id ? <Card title="警报列表">
                    <Table
                        columns={columns_data_source}
                        dataSource={data_info.warn_info}
                        bordered={true}
                        rowKey="id"
                    />
                </Card> : null}
            </MainContent>
        );
    }
}

/**
 * 控件：适老化设备档案
 * @description 适老化设备档案
 * @author
 */
@addon('ChangeDeviceFileView', '适老化设备档案', '适老化设备档案')
@reactControl(Form.create()(ChangeDeviceFileView), true)
export class ChangeDeviceFileViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}