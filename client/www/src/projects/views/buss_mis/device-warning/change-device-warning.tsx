import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
/**
 * 组件：适老化设备警报
 */
export interface ChangeDeviceWarningViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
}

/**
 * 组件：适老化设备警报
 */
export default class ChangeDeviceWarningView extends ReactView<ChangeDeviceWarningViewControl, ChangeDeviceWarningViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        var edit_props = {
            form_items_props: [
                {
                    title: '适老化设备警报',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "长者姓名",
                            decorator_id: "elder_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入长者姓名" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "身份证号码",
                            decorator_id: "elder_id_card",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入身份证号码" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "设备类型",
                            decorator_id: "device_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入设备类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "设备ID",
                            decorator_id: "device_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入设备ID" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "主机ID",
                            decorator_id: "host_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入主机ID" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "位置",
                            decorator_id: "location",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入位置" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "警报内容",
                            decorator_id: "warn_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入警报内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "警报时间",
                            decorator_id: "time",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入警报时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "处理状态",
                            decorator_id: "check_status",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入处理状态" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "处理结果",
                            decorator_id: "result",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入处理结果" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                autocomplete: 'off',
                                disabled: true,
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        history.back();
                        // this.props.history!.push(ROUTE_PATH.deviceWarning);
                    }
                },
            ],
            // submit_btn_propps: {
            //     text: "保存",
            // },
            service_option: {
                service_object: AppServiceUtility.call_center_nh_service,
                operation_option: {
                    // save: {
                    //     func_name: "update_call_center_zh"
                    // },
                    query: {
                        func_name: "get_device_warning_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：适老化设备警报
 * @description 适老化设备警报
 * @author
 */
@addon('ChangeDeviceWarningView', '适老化设备警报', '适老化设备警报')
@reactControl(ChangeDeviceWarningView, true)
export class ChangeDeviceWarningViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}