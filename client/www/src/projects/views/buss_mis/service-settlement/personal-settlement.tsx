import { Modal, Row, message } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param, edit_props_info, serviceSettlementState } from "src/projects/app/util-tool";
import FormCreator, { InputType as FormInputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";

/**
 * 组件：服务人员结算列表状态
 */
export interface ServicePersonalSettlementViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 结算id */
    id?: string;
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：服务人员结算列表
 * 描述
 */
export class ServicePersonalSettlementView extends ReactView<ServicePersonalSettlementViewControl, ServicePersonalSettlementViewState> {
    private columns_data_source = [{
        title: '服务订单编号',
        dataIndex: 'service_order_id',
        key: 'service_order_id',
    }, {
        title: '服务项目',
        dataIndex: 'service_project_id',
        key: 'service_project_id',
    }, {
        title: '服务开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '服务完成时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '服务人员',
        dataIndex: 'service_worker_id',
        key: 'service_worker_id',
    }, {
        title: '计价金额',
        dataIndex: 'valuation_amount',
        key: 'valuation_amount',
    }, {
        title: '服务人员结算金额',
        dataIndex: 'personal_amount',
        key: 'personal_amount',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            request_url: '',
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 信用查询 */
    settlement = () => {
        this.setState({
            modal_visible: true
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.setState({ modal_visible: true, id: contents.id });
        }
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 审核保存按钮 */
    handleSubmit = (err: any, value: any) => {
        value['id'] = this.state.id;
        value['personal_state'] = serviceSettlementState.settled;
        AppServiceUtility.service_operation_service.update_service_settlement!(value)!
            .then((data) => {
                if (data) {
                    this.setState({
                        modal_visible: false,
                    });
                    message.info('结算成功');
                }

            });
        console.info(value);
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let service_provider = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务订单编号",
                    decorator_id: "service_order_id"
                },
                {
                    type: InputType.input,
                    label: "服务人员",
                    decorator_id: "service_worker_id"
                }
            ],
            btn_props: [{
                label: '新增服务人员结算',
                btn_method: this.settlement,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: 'get_service_record_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
        };
        let service_provider_list = Object.assign(service_provider, table_param);
        let edit_props = {
            form_items_props: [
                {
                    title: "服务人员结算信息",
                    need_card: true,
                    input_props: [
                        {
                            type: FormInputType.input_number,
                            label: "结算金额",
                            decorator_id: "personal_amount",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入结算金额" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入结算金额"
                            }
                        },
                        {
                            type: FormInputType.text_area,
                            label: "备注",
                            decorator_id: "personal_remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.service_record_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "personal_settlement"
                    }
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.setState({ modal_visible: false, }); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <Row>
                <Modal
                    title='结算'
                    visible={this.state.modal_visible}
                    onCancel={this.handleCancel}
                    width={900}
                    okButtonProps={{ disabled: true, ghost: true }}
                    cancelButtonProps={{ disabled: true, ghost: true }}
                >
                    <FormCreator {...edit_props_list} />
                </Modal>
                <SignFrameLayout {...service_provider_list} />
            </Row>
        );
    }
}

/**
 * 控件：服务人员结算列表
 * 描述
 */
@addon('ServicePersonalSettlementView', '服务人员结算列表', '描述')
@reactControl(ServicePersonalSettlementView, true)
export class ServicePersonalSettlementViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}