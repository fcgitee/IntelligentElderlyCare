import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：交易管理列表视图状态
 */
export interface TransactionManageViewState extends ReactViewState {
}

/**
 * 组件：交易管理列表视图
 * 描述
 */
export class TransactionManageView extends ReactView<TransactionManageViewControl, TransactionManageViewState> {
    private columns_data_source = [{
        title: '交易编号',
        dataIndex: 'transaction_code',
        key: 'transaction_code',
    }, {
        title: '交易甲方',
        dataIndex: 'party_a',
        key: 'party_a',
    }, {
        title: '交易乙方',
        dataIndex: 'party_b',
        key: 'party_b',
    }, {
        title: '交易内容',
        dataIndex: 'transaction_content',
        key: 'transaction_content',
    }, {
        title: '交易状态',
        dataIndex: 'transaction_state',
        key: 'transaction_state',
    }, {
        title: '甲方评价',
        dataIndex: 'transaction_evaluate_a',
        key: 'transaction_evaluate_a',
    }, {
        title: '乙方评价',
        dataIndex: 'transaction_evaluate_b',
        key: 'transaction_evaluate_b',
    }];
    constructor(props: TransactionManageViewControl) {
        super(props);
        this.state = {
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeTransaction + '/' + contents.id);
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let transaction_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "交易编号",
                    decorator_id: "transaction_code"
                }, {
                    type: InputType.input,
                    label: "交易甲方",
                    decorator_id: "party_a"
                }, {
                    type: InputType.input,
                    label: "交易乙方",
                    decorator_id: "party_b"
                },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.transaction_service,
            service_option: {
                select: {
                    service_func: 'get_transaction_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_transaction_by_id'
                }
            },
        };
        let transaction_list = Object.assign(transaction_info_list, table_param);
        return (
            <SignFrameLayout {...transaction_list} />
        );
    }
}

/**
 * 控件：交易管理列表视图控制器
 * 描述
 */
@addon('TransactionManageView', '交易管理列表视图', '描述')
@reactControl(TransactionManageView, true)
export class TransactionManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}