/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Monday July 22nd 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Monday, 22nd July 2019 11:07:38 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、交易信用记录页面
 */

import { Modal, Row } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";

/**
 * 组件：信用记录
 */
export interface TransactionCommentViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 交易清单集合 */
    service_list?: any[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 交易评价分数 */
    total?: number;
    /** 标题 */
    title?: string;
    /** 表格列信息 */
    columns_service_item?: any[];
}

/**
 * 组件：信用记录信息
 * 描述
 */
export class TransactionCommentView extends ReactView<TransactionCommentViewControl, TransactionCommentViewState> {
    private columns_data_source = [{
        title: '交易甲方',
        dataIndex: 'party_a',
        key: 'party_a',
    }, {
        title: '交易乙方',
        dataIndex: 'party_b',
        key: 'party_b',
    }, {
        title: '交易编号',
        dataIndex: 'transaction_id',
        key: 'transaction_id',
    }, {
        title: '交易内容',
        dataIndex: 'transaction_content',
        key: 'transaction_content',
    }, {
        title: '信用评分',
        dataIndex: 'comment_score',
        key: 'comment_score',
    }];
    private comment_columns = [{
        title: '交易编号',
        dataIndex: 'transaction_id',
        key: 'transaction_id',
    }, {
        title: '评论人',
        dataIndex: 'comment_person',
        key: 'comment_person',
    }, {
        title: '评价分数',
        dataIndex: 'comment_score',
        key: 'comment_score',
    }, {
        title: '评价内容',
        dataIndex: 'comment_content',
        key: 'comment_content',
    }

    ];
    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 信用查询 */
    queryTransactionComment = () => {
        let ids = this.state.select_ids;
        if (ids && ids!.length > 0) {
            let columns_transaction_item = this.comment_columns;
            request(this, AppServiceUtility.transaction_service.query_comment_details_list!({ 'ids': ids }))
                .then((data: any) => {
                    this.setState({
                        service_list: data.result,
                        total: data.total,
                        columns_service_item: columns_transaction_item,
                        modal_visible: true
                    });
                });
        }
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }
    render() {
        // const redeirect = this.props.isPermission ? this.props.isPermission!(this.props.permission!) : undefined;
        // if (redeirect) {
        //     return redeirect;
        // }
        let transaction_comment = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "经济主体",
                    decorator_id: "user_name"
                }
            ],
            btn_props: [{
                label: '信用查询',
                btn_method: this.queryTransactionComment,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.transaction_service,
            service_option: {
                select: {
                    service_func: 'query_transaction_comment_list',
                    service_condition: [{}, 1, 10]
                }
            },
        };
        let transaction_comment_list = Object.assign(transaction_comment, table_param);
        return (
            (
                <Row>
                    <Modal
                        title={this.state.title}
                        visible={this.state.modal_visible}
                        onCancel={this.handleCancel}
                        width={900}
                    >
                        <NTOperationTable
                            data_source={this.state.service_list}
                            columns_data_source={this.state.columns_service_item}
                            table_size='middle'
                            showHeader={true}
                            bordered={true}
                            total={this.state.total}
                            default_page_size={10}
                            total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                            show_footer={true}
                        />
                    </Modal>
                    <SignFrameLayout {...transaction_comment_list} />
                </Row>
            )
        );
    }
}

/**
 * 控件：服务商登记信息
 * 描述
 */
@addon('TransactionCommentView', '信用记录', '描述')
@reactControl(TransactionCommentView, true)
export class TransactionCommentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}