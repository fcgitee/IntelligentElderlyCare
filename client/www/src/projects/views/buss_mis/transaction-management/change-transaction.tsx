import { message } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：交易状态
 */
export interface ChangeTransactionViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：交易状态视图
 */
export class ChangeTransactionView extends ReactView<ChangeTransactionViewControl, ChangeTransactionViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: ''
        };
    }
    /** 恢复交易 */
    refuse = () => {
        request(this, AppServiceUtility.transaction_service.return_transaction_by_id!(this.props.match!.params.key))
            .then((data: any) => {
                // console.log(data);
                if (data === 'Success') {
                    this.props.history!.push(ROUTE_PATH.transaction);
                } else {
                    message.info('恢复失败');
                }
            });

    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.transaction);
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let edit_props = {
            form_items_props: [
                {
                    title: "交易管理",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "交易编号",
                            decorator_id: "transaction_code",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "交易甲方",
                            decorator_id: "party_a",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "交易乙方",
                            decorator_id: "party_b",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "交易内容",
                            decorator_id: "transaction_content",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "交易状态",
                            decorator_id: "transaction_state",
                            option: {
                                disabled: true
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.transaction);
                    }
                }, {
                    text: "恢复交易",
                    cb: this.refuse
                }
            ],
            submit_btn_propps: {
                text: "取消交易",
            },
            service_option: {
                service_object: AppServiceUtility.transaction_service,
                operation_option: {
                    save: {
                        func_name: "cancel_transaction_by_id",
                        arguments: [this.props.match!.params.key]
                    },
                    query: {
                        func_name: "get_transaction_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.transaction); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：交易状态编辑控件
 * @description 交易状态编辑控件
 * @author
 */
@addon('ChangeTransactionView', '交易状态编辑控件', '交易状态编辑控件')
@reactControl(ChangeTransactionView, true)
export class ChangeTransactionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}