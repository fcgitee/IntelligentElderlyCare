
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：回访问卷题目列表视图 */
export interface SubjectListViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}
/** 组件：回访问卷题目列表视图 */
export class SubjectListView extends React.Component<SubjectListViewControl, SubjectListViewState> {
    private columns_data_source = [
        //     {
        //     title: '状态',
        //     dataIndex: 'state',
        //     key: 'state',
        // }, 
        {
            title: '题目名称',
            dataIndex: 'project_name',
            key: 'project_name',
        },
        {
            title: '描述',
            dataIndex: 'describe',
            key: 'describe',
        },
        {
            title: '创建日期',
            dataIndex: 'create_date',
            key: 'create_date',
        },];
    public text = "";
    constructor(props: SubjectListViewControl) {
        super(props);
        this.state = {
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.addSubject);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addSubject + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "题目名称",
                    decorator_id: "project_name"
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.assessment_project_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: this.props.del_url
                }
            },
            searchExtraParam: "",
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        if (!this.text) {
            assessment_project.edit_form_items_props.push(
                {
                    type: InputType.input,
                    label: "题目类型",
                    decorator_id: "assessment_type"
                },
                {
                    type: InputType.rangePicker,
                    label: "创建日期",
                    decorator_id: "date_range"
                }
            );
        }
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：回访问卷题目列表视图控制器
 * @description 回访问卷题目列表视图
 * @author
 */
@addon('SubjectListView', '回访问卷题目列表视图', '回访问卷题目列表视图')
@reactControl(SubjectListView, true)
export class SubjectListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public del_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}