import { Row, Radio, message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { IAssessmentProjectService } from "src/projects/models/assessment-project";
// import { ROUTE_PATH } from 'src/projects/router';
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
// import './index.less';
import { request } from "src/business/util_tool";

/** 项目状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：编辑行为能力评估项目状态
 */
export interface AddProjectViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    // 要显示的组件
    showType?: number;
    // 选择框类
    selete_type?: any;
    /** 组件文字 */
    text?: string;
}

/**
 * 组件：编辑行为能力评估项目视图
 */
export class AddProjectView extends ReactView<AddProjectViewControl, AddProjectViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            showType: 0,// 0不显示，1显示form，2显示table_list
            selete_obj: {},
            text: ''
        };
    }
    /** 权限服务 */
    assessmentProjectServer?() {
        return getObject(this.props.assessmentProjectServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.goBack();
    }
    /** 保存按钮回调方法 */
    // 不同组件是不同的传入方法
    save_value = (value1: any, value2: any) => {
        const isNursing = this.props.match!.params.key && this.props.match!.params.key !== '护理';
        let value;
        if (this.state.showType === 1) {
            // 1是table-list可编辑列表组件
            value = value1;
        } else {
            // 2是form表单组件
            value = value2;
            // 固定计分方式为不计分
            value['scoring_method'] = 1;
        }
        if (isNursing) {
            value['state'] = SRAR_TUSING;
            value['id'] = this.props.match!.params.key;
        } else {
            delete value['id'];
        }
        value['selete_type'] = this.state.selete_type;
        this.assessmentProjectServer!()!.update_assessment_project!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    console.info(isNursing, '是否为护理');
                    // isNursing ? this.props.history!.push(ROUTE_PATH.assessmentProject) : this.props.history!.push(ROUTE_PATH.assessmentProject + '/护理');
                    this.props.history!.goBack();

                }
            });
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            if (this.props.match!.params.key === '护理') {
                this.setState({
                    text: this.props.match!.params.key
                });
            } else {
                this.setState({
                    selete_obj: {
                        id: this.props.match!.params.key
                    }
                });
                request(this, AppServiceUtility.assessment_project_service.get_assessment_project_list!({ id: this.props.match!.params.key }, 1, 1)!)
                    .then((datas: any) => {
                        let showType = 0,
                            selete_type = datas.result[0].selete_type;
                        if (!this.state.text) {
                            if (selete_type === '1' || selete_type === '2' || selete_type === '4') {
                                showType = 1;
                            } else if (selete_type === '3' || selete_type === '5' || selete_type === '6' || selete_type === '7') {
                                showType = 2;
                            }
                        } else {
                            if (selete_type === '1' || selete_type === '3' || selete_type === '4') {
                                showType = 1;
                            }
                        }
                        this.setState({
                            selete_type,
                            showType,
                        });
                    });
            }
        }
    }
    radisChange(e: any) {
        let showType = this.state.showType;
        if (!this.state.text) {  // 评估
            if (e.target.value === '1' || e.target.value === '2' || e.target.value === '4') {
                showType = 1;
            } else if (e.target.value === '3' || e.target.value === '5' || e.target.value === '6' || e.target.value === '7') {
                showType = 2;
            }
        } else { // 护理
            if (e.target.value === '1' || e.target.value === '4') {
                showType = 1;
            }
            if (e.target.value === '3') {
                showType = 2;
            }
        }
        this.setState({
            selete_type: e.target.value,
            showType: showType,
        });
    }
    backList() {
        this.props.history!.goBack();
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let table_list_props;
        if (!this.state.text) {// 评估
            table_list_props = {
                form_items_props: [
                    {
                        type: InputTypeTable.input,
                        label: "题目名称",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入题目名称", }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "project_name",
                    },
                    // {
                    //     type: InputTypeTable.input,
                    //     label: `${this.state.text ? '护理' : '评估'}类型`,
                    //     field_decorator_option: {
                    //         rules: [{ required: false, message: "" }],
                    //     } as GetFieldDecoratorOptions,
                    //     decorator_id: "assessment_type",
                    // },
                    {
                        type: InputTypeTable.radio,
                        label: "记分方式",
                        field_decorator_option: {
                            rules: [{ required: false, message: "" }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "scoring_method",
                        option: [
                            {
                                label: '不计分',
                                value: '1'
                            }, {
                                label: '累计',
                                value: '2'
                            }, {
                                label: '最大值',
                                value: '3'
                            }, {
                                label: '最小值',
                                value: '4'
                            },
                        ]
                    },
                    {
                        type: InputTypeTable.text_area,
                        label: "题目描述",
                        field_decorator_option: {
                            rules: [{ required: false, message: "" }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "describe",
                    }
                ],
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: () => {
                            this.backList();
                        }
                    }
                ],
                submit_props: {
                    text: "保存",
                    cb: this.save_value
                },
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                // table配置
                table_field_name: 'dataSource',
                // table配置
                columns_data_source: [{
                    title: '选项内容',
                    dataIndex: 'option_content',
                    key: 'option_content',
                    type: 'input' as ColTypeStr
                }, {
                    title: '分值',
                    dataIndex: 'score',
                    key: 'score',
                    type: 'input' as ColTypeStr
                }],
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_project_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_project_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                    },
                },
                add_row_text: `新增题目内容`,
                succ_func: () => { this.backList(); },
                id: this.props.match!.params.key
            };
        } else {
            table_list_props = {
                form_items_props: [
                    {
                        type: InputTypeTable.input,
                        label: "题目名称",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入题目名称", }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "project_name",
                    },
                    {
                        type: InputTypeTable.input,
                        label: "题目描述",
                        field_decorator_option: {
                            rules: [{ required: false, message: "" }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "describe",
                    }
                ],
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: () => {
                            this.backList();
                        }
                    }
                ],
                submit_props: {
                    text: "保存",
                    cb: this.save_value
                },
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                // table配置
                table_field_name: 'dataSource',
                // table配置
                columns_data_source: [{
                    title: '选项内容',
                    dataIndex: 'option_content',
                    key: 'option_content',
                    type: 'input' as ColTypeStr
                }],
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_project_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_project_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                    },
                },
                add_row_text: `新增选项内容`,
                succ_func: () => { this.backList(); },
                id: this.props.match!.params.key
            };
        }

        // let style = {
        //     marginBottom: '-60px',
        //     padding: '20px 0 40px 10px',
        // };
        // selete_type
        let layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 19 },
            },
        };
        let edit_props;
        if (!this.state.text) {// 评估
            edit_props = {
                form_items_props: [
                    {
                        need_card: true,
                        input_props: [
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "题目名称",
                                            decorator_id: "project_name",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请输入题目名称" }],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入题目名称",
                                            },
                                            layout: layout
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: `题目类型`,
                                            decorator_id: "assessment_type",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: `请输入题目类型` }],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: `请输入题目类型`,
                                            },
                                            layout: layout
                                        },
                                        {
                                            type: InputType.text_area,
                                            label: "题目描述",
                                            col_span: 8,
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "" }],
                                            } as GetFieldDecoratorOptions,
                                            decorator_id: "describe",
                                            layout: layout
                                        }
                                    ]
                                },
                            }
                        ]
                    }
                ],
                submit_btn_propps: {
                    text: "保存",
                    cb: this.save_value
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: () => this.backList!()
                    },
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " start"
                    }
                },
                service_option: {
                    service_object: AppServiceUtility.assessment_project_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_project_list_all",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                    },
                },
                id: this.props.match!.params.key,
            };
        } else {// 护理
            edit_props = {
                form_items_props: [
                    {
                        need_card: true,
                        input_props: [
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "题目名称",
                                            decorator_id: "project_name",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请输入题目名称" }],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入题目名称",
                                            },
                                            layout: layout
                                        },
                                        {
                                            type: InputType.antd_input,
                                            label: "题目描述",
                                            col_span: 8,
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "" }],
                                            } as GetFieldDecoratorOptions,
                                            decorator_id: "describe",
                                            layout: layout
                                        }
                                    ]
                                },
                            }
                        ]
                    }
                ],
                submit_btn_propps: {
                    text: "保存",
                    cb: this.save_value
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: () => this.backList!()
                    },
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " start"
                    }
                },
                service_option: {
                    service_object: AppServiceUtility.assessment_project_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_project_list_all",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                    },
                },
                id: this.props.match!.params.key,
            };
        }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        let selete_type = this.state.selete_type;
        let showType: any = this.state.showType;
        let radiodom = [<Radio value='1' key="1">单选框</Radio>, <Radio value='4' key="4">下拉框</Radio>, <Radio value='3' key="3">输入框</Radio>, <Radio value='2' key="2">多选框</Radio>, <Radio value='5' key="5">多行文本</Radio>, <Radio value='6' key="6">图片上传</Radio>, <Radio value='7' key="7">文件上传</Radio>];
        return (
            <div>
                <MainContent>
                    <MainCard>
                        <Row type="flex" justify="start" style={{ 'fontSize': '14px' }}>
                            选择框类：
                        <Radio.Group value={selete_type} onChange={(e) => this.radisChange(e)}>
                                {
                                    radiodom.map((item: any, index: number) => {
                                        if (this.props.match!.params.key === '护理') {
                                            if (index < 3) {
                                                return (item);
                                            }
                                        } else {
                                            return (item);
                                        }
                                    })
                                }
                            </Radio.Group>
                        </Row>
                    </MainCard>
                    {showType === 1 ? (
                        <TableList {...table_list_props} />
                    ) : showType === 2 ? <FormCreator {...edit_props_list} /> : null}
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：新增回访问卷问题控件
 * @description 新增回访问卷问题控件
 * @author
 */
@addon('AddProjectView', '新增回访问卷问题控件', '新增回访问卷问题控件')
@reactControl(AddProjectView, true)
export class AddProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估项目服务 */
    public assessmentProjectServer_Fac?: Ref<IAssessmentProjectService>;

}