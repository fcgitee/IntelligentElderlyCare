
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：回访问卷列表视图 */
export interface QuestionnaireListViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}
/** 组件：回访问卷列表视图 */
export class QuestionnaireListView extends React.Component<QuestionnaireListViewControl, QuestionnaireListViewState> {
    private columns_data_source = [
        //     {
        //     title: '状态',
        //     dataIndex: 'state',
        //     key: 'state',
        // }, 
        {
            title: '问卷名称',
            dataIndex: 'template_name',
            key: 'template_name',
        }, {
            title: '描述',
            dataIndex: 'describe',
            key: 'describe',
        }, {
            title: '问卷类型',
            dataIndex: 'template_type',
            key: 'template_type',
        },];
    constructor(props: QuestionnaireListViewControl) {
        super(props);
        this.state = {
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        if (this.props.match!.params.key === '护理') {
            this.props.history!.push(ROUTE_PATH.addQuestionnaire + '/' + this.props.match!.params.key);
        } else {
            this.props.history!.push(ROUTE_PATH.addQuestionnaire);
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addQuestionnaire + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_template;
        if (this.props.match!.params.key === '护理') {
            assessment_template = {
                type_show: false,
                edit_form_items_props: [
                    {
                        type: InputType.input,
                        label: "问卷名称",
                        decorator_id: "template_name"
                    }
                ],
                btn_props: [{
                    label: '新增问卷',
                    btn_method: this.addBehavioralCompetenceAssessment,
                    icon: 'plus'
                }],
                // search_cb: this.queryBtn,
                columns_data_source: this.columns_data_source,
                on_icon_click: this.onIconClick,
                service_object: AppServiceUtility.assessment_template_service,
                service_option: {
                    select: {
                        service_func: this.state.request_url,
                        service_condition: [{ template_type: '回访问卷' }, 1, 10]
                    },
                    delete: {
                        service_func: 'delete_assessment_template'
                    }
                },
                permission_class: this.props.permission_class,
                get_permission_name: this.props.get_permission_name,
                select_permission: this.props.select_permission,
                edit_permission: this.props.edit_permission,
                add_permission: this.props.add_permission,
                delete_permission: this.props.delete_permission,
            };
        } else {
            assessment_template = {
                type_show: false,
                edit_form_items_props: [
                    {
                        type: InputType.input,
                        label: "问卷名称",
                        decorator_id: "template_name"
                    }
                ],
                btn_props: [{
                    label: '新增问卷',
                    btn_method: this.addBehavioralCompetenceAssessment,
                    icon: 'plus'
                }],
                // search_cb: this.queryBtn,
                columns_data_source: this.columns_data_source,
                on_icon_click: this.onIconClick,
                service_object: AppServiceUtility.assessment_template_service,
                service_option: {
                    select: {
                        service_func: this.state.request_url,
                        service_condition: [{template_type: '回访问卷'}, 1, 10]
                    },
                    delete: {
                        service_func: 'delete_assessment_template'
                    }
                },
                permission_class: this.props.permission_class,
                get_permission_name: this.props.get_permission_name,
                select_permission: this.props.select_permission,
                edit_permission: this.props.edit_permission,
                add_permission: this.props.add_permission,
                delete_permission: this.props.delete_permission,
            };
        }
        let behavioral_competence_assessment_list = Object.assign(assessment_template, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：回访问卷列表目视图控制器
 * @description 回访问卷列表目视图
 * @author
 */
@addon('QuestionnaireListView', '回访问卷列表目视图', '回访问卷列表目视图')
@reactControl(QuestionnaireListView, true)
export class QuestionnaireListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}