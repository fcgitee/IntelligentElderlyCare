import { Select, message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { IAssessmentTemplateService } from "src/projects/models/assessment-template";
import { ROUTE_PATH } from 'src/projects/router';

const { Option } = Select;
/** 模板状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：回访问卷新增状态
 */
export interface AddQuestionnaireViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
    /** 评估模板列表 */
    template_list?: any[];
    /** 用户选择的模板类型 */
    templatestase?: string;
    /** 用户选择的类型 */
    typevalue?: string;
    /** 是否是大模板 */
    templatestate?: boolean;
}

/**
 * 组件：回访问卷新增视图
 */
export class AddQuestionnaireView extends ReactView<AddQuestionnaireViewControl, AddQuestionnaireViewState> {
    /** 编辑的数据 */
    public changedata?: object = {};
    public pagestatus?: Boolean = false;
    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            typevalue: '',
            templatestase: '',
            templatestate: false
        };
    }
    /** 权限服务 */
    assessmentTemplateServer?() {
        return getObject(this.props.assessmentTemplateServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.assessmentTemplate);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        if (!this.props.match!.params.key && this.props.match!.params.key !== '护理') {
            value['state'] = SRAR_TUSING;
        } else if (this.props.match!.params.key && this.props.match!.params.key !== '护理') {
            value['id'] = this.props.match!.params.key;
        } else {
            delete value.id;
        }
        /** 大模板 */
        if (value.type && value.type === '评估模板') {
            let count: number = 0;
            value.templateSource.forEach((item: any, index: number) => {
                AppServiceUtility.assessment_template_service.get_assessment_template_list!({ id: item.assessment_template })!
                    .then((data: any) => {
                        value.templateSource[index]['template_list'] = data.result;
                    });
            });
            let settime = setInterval(
                () => {
                    for (let i = 0; i < value.templateSource.length; i++) {
                        if (value.templateSource[i]['template_list'] && value.templateSource[i]['template_list'].length > 0 && i === value.templateSource.length - 1) {
                            count = 1;
                        }
                    }
                    if (count) {
                        clearInterval(settime);
                        this.assessmentTemplateServer!()!.update_assessment_template!(value)!
                            .then((data: any) => {
                                if (data === 'Success') {
                                    message.info('保存成功');
                                    history.back();
                                }
                            });
                    }
                }
                ,
                200
            );
        } else {
            // console.log(value, 99999);
            this.assessmentTemplateServer!()!.update_assessment_template!(value)!
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('保存成功');
                        history.back();
                    }
                });
        }
    }
    /** 返回按钮回调 */
    back = (value: any) => {
        // console.log('返回', value);
        history.back();
    }
    componentDidMount() {
        AppServiceUtility.assessment_project_service.get_assessment_project_list!({})!
            .then((data: any) => {
                let project_list: any[];
                project_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.project_name}</Option>;
                });
                this.setState(
                    {
                        project_list
                    },
                    () => {
                        this.forceUpdate();
                    }
                );
            });
        AppServiceUtility.assessment_template_service.get_assessment_template_list!({})!
            .then((data: any) => {
                let template_list: any[];
                template_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.template_name}</Option>;
                });
                this.setState({
                    template_list
                });
            });
        if (this.props.match!.params.key) {
            if (this.props.match!.params.key !== '护理') {
                this.setState(
                    {
                        selete_obj: {
                            id: this.props.match!.params.key,
                            is_edit: true
                        }
                    },
                    () => {
                        this.forceUpdate();
                    }
                );
            }
        }
    }
    onchangeasslist = (e: string) => {
        this.setState({ templatestase: e }, () => {
            this.forceUpdate();
        });
    }
    readys = (value: any) => {
        if (this.props.match!.params.key && this.props.match!.params.key !== '护理' && !this.state.templatestate && !this.pagestatus) {
            let data = value[0];
            this.changedata = data;
            if (data.type === '评估模板') {
                this.setState({ typevalue: '评估模板', templatestate: true, templatestase: data.template_type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            } else if (!data.type && data.template_type === '详细能力评估') {
                this.setState({ templatestase: data.template_type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            } else if (data.type && data.template_type) {
                this.setState({ templatestase: data.template_type, typevalue: data.type }, () => {
                    this.forceUpdate();
                    this.pagestatus = true;
                });
            }
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let columns_data_source;
        if (this.props.match!.params.key === '护理') {
            columns_data_source = [{
                title: `问题名称`,
                dataIndex: 'assessment_projects',
                key: 'assessment_projects',
                type: 'select' as ColTypeStr,
                option: {
                    placeholder: `请选择问题名称`,
                    childrens: this.state.project_list
                }
            }];
        } else {
            if (this.props.match!.params.key && this.state.templatestate) {
                if (this.state.typevalue !== '评估模板') {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `问题名称`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择问题名称`,
                            childrens: this.state.project_list
                        }
                    }, {
                        title: '记分方式',
                        dataIndex: 'scoring_method',
                        key: 'scoring_method',
                        type: 'input' as ColTypeStr
                    }];
                } else if (this.state.templatestase === '详细能力评估') {
                    columns_data_source = [{
                        title: '子模板名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估模板`,
                        dataIndex: 'assessment_template',
                        key: 'evaluation_template',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估模板`,
                            childrens: this.state.template_list
                        }
                    }];
                } else {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `问题名称`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择问题名称`,
                            childrens: this.state.project_list
                        }
                    }, {
                        title: '记分方式',
                        dataIndex: 'scoring_method',
                        key: 'scoring_method',
                        type: 'input' as ColTypeStr
                    }];
                }
            } else if (this.props.match!.params.key && !this.state.templatestate) {
                columns_data_source = [{
                    title: '分类名称',
                    dataIndex: 'category_name',
                    key: 'category_name',
                    type: 'input' as ColTypeStr
                }, {
                    title: `问题名称`,
                    dataIndex: 'assessment_projects',
                    key: 'assessment_projects',
                    type: 'select' as ColTypeStr,
                    option: {
                        placeholder: `请选择问题名称`,
                        childrens: this.state.project_list
                    }
                }, {
                    title: '记分方式',
                    dataIndex: 'scoring_method',
                    key: 'scoring_method',
                    type: 'input' as ColTypeStr
                }];
            } else {
                if (this.state.typevalue !== '评估模板') {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `问题名称`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择问题名称`,
                            childrens: this.state.project_list
                        }
                    }, {
                        title: '记分方式',
                        dataIndex: 'scoring_method',
                        key: 'scoring_method',
                        type: 'input' as ColTypeStr
                    }];
                } else if (this.state.templatestase === '详细能力评估' && this.state.typevalue === '评估模板') {
                    columns_data_source = [{
                        title: '子模板名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `评估模板`,
                        dataIndex: 'assessment_template',
                        key: 'evaluation_template',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择评估模板`,
                            childrens: this.state.template_list
                        }
                    }];
                } else {
                    columns_data_source = [{
                        title: '分类名称',
                        dataIndex: 'category_name',
                        key: 'category_name',
                        type: 'input' as ColTypeStr
                    }, {
                        title: `问题名称`,
                        dataIndex: 'assessment_projects',
                        key: 'assessment_projects',
                        type: 'select' as ColTypeStr,
                        option: {
                            placeholder: `请选择问题名称`,
                            childrens: this.state.project_list
                        }
                    }, {
                        title: '记分方式',
                        dataIndex: 'scoring_method',
                        key: 'scoring_method',
                        type: 'input' as ColTypeStr
                    }];
                }
            }

        }
        const assessment_list: JSX.Element[] = [<Option key='初步能力评估'>初步能力评估</Option>, <Option key='详细能力评估'>详细能力评估</Option>, <Option key='补贴评估'>补贴评估</Option>, <Option key='护理评估'>护理评估</Option>, <Option key='慈善评估'>慈善评估</Option>, <Option key='回访问卷'>回访问卷</Option>];
        const list: JSX.Element[] = [<Option key='评估项目'>评估项目</Option>, <Option key='评估模板'>评估模板</Option>];
        let user_manege: object;
        // console.log(this.props.match!.params.key);
        if (this.props.match!.params.key === '护理') {
            user_manege = {
                form_items_props: [
                    {
                        type: InputTypeTable.input,
                        label: "问卷名称",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入模板名称", }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "template_name",
                    },
                    {
                        type: InputTypeTable.select,
                        label: "问卷类型",
                        field_decorator_option: {
                            initialValue: '护理评估'
                        } as GetFieldDecoratorOptions,
                        decorator_id: "template_type",
                        option: {
                            disabled: true,
                            childrens: assessment_list
                        },
                    },
                    {
                        type: InputTypeTable.text_area,
                        label: "描述",
                        field_decorator_option: {
                            rules: [{ required: false, message: "" }],
                        } as GetFieldDecoratorOptions,
                        decorator_id: "describe",
                    }
                ],
                submit_props: {
                    text: "保存",
                    cb: this.save_value
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: this.back
                    }
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                add_row_text: `添加护理项目`,
                table_field_name: 'dataSource',
                // table配置
                columns_data_source: columns_data_source,
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_template_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_template_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                        // save: {
                        //     func_name: "update_service_type"
                        // }
                    },
                },
                succ_func: () => { this.props.history!.push(ROUTE_PATH.assessmentTemplate); },
                id: this.props.match!.params.key
            };
        } else {
            user_manege = {
                submit_props: {
                    text: "保存",
                    cb: this.save_value
                },
                other_btn_propps: [
                    {
                        text: "返回",
                        cb: this.back
                    }
                ],
                row_btn_props: {
                    style: {
                        justifyContent: " center"
                    }
                },
                add_row_text: `添加${this.state.typevalue === '评估模板' ? '子模板' : '回访问卷'}`,
                // table配置
                columns_data_source: columns_data_source,
                other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
                showHeader: true,
                bordered: false,
                show_footer: true,
                rowKey: 'id',
                service_option: {
                    service_object: AppServiceUtility.assessment_template_service,
                    operation_option: {
                        query: {
                            func_name: "get_assessment_template_list",
                            arguments: [this.state.selete_obj, 1, 1]
                        },
                        // save: {
                        //     func_name: "update_service_type"
                        // }
                    },
                },
                succ_func: () => { this.props.history!.push(ROUTE_PATH.assessmentTemplate); },
                id: this.props.match!.params.key
            };
            if (this.props.match!.params.key && this.state.templatestate) {
                if (this.state.typevalue !== '评估模板') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else {
                    user_manege['table_field_name'] = 'templateSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.select,
                            label: "类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择类型", }]
                            } as GetFieldDecoratorOptions,
                            decorator_id: "type",
                            option: {
                                childrens: list,
                                onChange: (e: string) => {
                                    this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                        this.forceUpdate();
                                    });
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            } else if (this.props.match!.params.key && !this.state.templatestate) {
                if (this.state.templatestase !== '详细能力评估') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.select,
                            label: "类型",
                            field_decorator_option: {
                                initialValue: this.changedata!['type'] ? this.changedata!['type'] : '评估项目',
                                rules: [{ required: true, message: "请选择类型", }]
                            } as GetFieldDecoratorOptions,
                            decorator_id: "type",
                            option: {
                                childrens: list,
                                onChange: (e: string) => {
                                    this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                        this.forceUpdate();
                                    });
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            } else {
                if (this.state.templatestase !== '详细能力评估') {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                } else if (this.state.templatestase === '详细能力评估') {
                    if (this.state.typevalue === '评估模板') {
                        user_manege['table_field_name'] = 'templateSource';
                        user_manege['form_items_props'] = [
                            {
                                type: InputTypeTable.input,
                                label: "问卷名称",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请输入问卷名称", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_name",
                            },
                            {
                                type: InputTypeTable.select,
                                label: "问卷类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择问卷类型", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_type",
                                option: {
                                    childrens: assessment_list,
                                    onChange: (e: string) => {
                                        this.onchangeasslist(e);
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.select,
                                label: "类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择类型", }],
                                    initialValue: this.state.typevalue ? this.state.typevalue : '评估项目'
                                } as GetFieldDecoratorOptions,
                                decorator_id: "type",
                                option: {
                                    childrens: list,
                                    onChange: (e: string) => {
                                        this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                            this.forceUpdate();
                                        });
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.text_area,
                                label: "描述",
                                field_decorator_option: {
                                    rules: [{ required: false, message: "" }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "describe",
                            }
                        ];
                    } else {
                        user_manege['table_field_name'] = 'dataSource';
                        user_manege['form_items_props'] = [
                            {
                                type: InputTypeTable.input,
                                label: "问卷名称",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请输入问卷名称", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_name",
                            },
                            {
                                type: InputTypeTable.select,
                                label: "问卷类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择问卷类型", }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "template_type",
                                option: {
                                    childrens: assessment_list,
                                    onChange: (e: string) => {
                                        this.onchangeasslist(e);
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.select,
                                label: "类型",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择类型", }],
                                    initialValue: this.state.typevalue ? this.state.typevalue : '评估项目'
                                } as GetFieldDecoratorOptions,
                                decorator_id: "type",
                                option: {
                                    childrens: list,
                                    onChange: (e: string) => {
                                        this.setState({ typevalue: e, templatestate: e === '评估模板' ? true : false }, () => {
                                            this.forceUpdate();
                                        });
                                    }
                                },
                            },
                            {
                                type: InputTypeTable.text_area,
                                label: "描述",
                                field_decorator_option: {
                                    rules: [{ required: false, message: "" }],
                                } as GetFieldDecoratorOptions,
                                decorator_id: "describe",
                            }
                        ];
                    }
                } else {
                    user_manege['table_field_name'] = 'dataSource';
                    user_manege['form_items_props'] = [
                        {
                            type: InputTypeTable.input,
                            label: "问卷名称",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入问卷名称", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_name",
                        },
                        {
                            type: InputTypeTable.select,
                            label: "问卷类型",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择问卷类型", }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "template_type",
                            option: {
                                childrens: assessment_list,
                                onChange: (e: string) => {
                                    this.onchangeasslist(e);
                                }
                            },
                        },
                        {
                            type: InputTypeTable.text_area,
                            label: "描述",
                            field_decorator_option: {
                                rules: [{ required: false, message: "" }],
                            } as GetFieldDecoratorOptions,
                            decorator_id: "describe",
                        }
                    ];
                }
            }
        }
        return (
            <TableList {...user_manege} readys={this.readys} />
        );
    }
}

/**
 * 控件：回访问卷新增编辑控件
 * @description 回访问卷新增编辑控件
 * @author
 */
@addon('AddQuestionnaireView', '回访问卷新增编辑控件', '回访问卷新增编辑控件')
@reactControl(AddQuestionnaireView, true)
export class AddQuestionnaireViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估模板服务 */
    public assessmentTemplateServer_Fac?: Ref<IAssessmentTemplateService>;

}