
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Form, Select } from 'antd';
let { Option } = Select;
/** 状态：民办非营机构床位资助统计 */
export interface OrgBedFundViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
}
/** 组件：民办非营机构床位资助统计 */
export class OrgBedFundView extends React.Component<OrgBedFundViewControl, OrgBedFundViewState> {
    private columns_data_source = [{
        title: "月份",
        dataIndex: 'month',
        key: 'month',
    },
    {
        title: '自理长者数',
        dataIndex: 'zlzz_num',
        key: 'zlzz_num',
    },
    {
        title: '介助长者数',
        dataIndex: 'jzzz_num',
        key: 'jzzz_num',
    },
    {
        title: '介护长者数',
        dataIndex: 'jhzz_num',
        key: 'jhzz_num',
    },
    {
        title: '资助总金额',
        dataIndex: 'fund_all',
        key: 'fund_all',
    },
    ];
    constructor(props: OrgBedFundViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
        };
    }
    componentDidMount = () => {
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let year: any = [];
        let current_year: number = new Date().getFullYear();
        for (let i = current_year - 10; i <= current_year; i++) {
            year.push(i + '');
        }
        let org_bed = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: '年度',
                    decorator_id: "year",
                    option: {
                        placeholder: "请选择年度",
                        childrens: year.map((item: any) => <Option key={item}>{item}</Option>),
                    },
                },
            ],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_org_bed_fund_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let org_bed_list = Object.assign(org_bed, table_param);
        return (
            <div>
                <SignFrameLayout {...org_bed_list} />
            </div>
        );
    }
}

/**
 * 控件：民办非营机构床位资助统计控制器
 * @description 民办非营机构床位资助统计
 * @author
 */
@addon('OrgBedFundView', '民办非营机构床位资助统计', '民办非营机构床位资助统计')
@reactControl(Form.create<any>()(OrgBedFundView), true)
export class OrgBedFundViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}