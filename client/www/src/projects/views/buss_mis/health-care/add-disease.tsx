import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { Select } from 'antd';
const { Option } = Select;
/**
 * 组件：疾病档案新增查看状态
 */
export interface AddDiseaseViewState extends ReactViewState {
    status?: any;
    base_data?: any;
}

/**
 * 组件：疾病档案新增查看
 * 描述
 */
export class AddDiseaseView extends ReactView<AddDiseaseViewControl, AddDiseaseViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: AddDiseaseViewControl) {
        super(props);
        this.state = {
            status: ["男", "女"],
            base_data: {},
        };
    }

    render() {
        let { base_data } = this.state;
        let elder_id = '';
        let key = this.props.match!.params.key;
        if (key) {
            elder_id = key.split('__')[1];
        }
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_elder_list',
            service_option: elder_id ? [{ id: elder_id }, 1, 1] : [{}, 1, 10],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "疾病档案新增",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者姓名",
                            decorator_id: 'elder_id',
                            field_decorator_option: {
                                rules: [{ message: "请输入名称" }],
                                initialValue: base_data.elder_name,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者姓名",
                                modal_search_items_props: modal_search_items_props
                            },
                        },
                        {
                            type: InputType.select,
                            label: "性别",
                            decorator_id: "sex",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择性别" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择性别",
                                showSearch: true,
                                childrens: this.state.status!.map((item: any, idx: any) => <Option key={idx} value={item}>{item}</Option>),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "年龄",
                            decorator_id: "age",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入年龄" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入年龄"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "诊断结果",
                            decorator_id: "result",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入诊断结果" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入诊断结果"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "症状说明",
                            decorator_id: "explain",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入症状说明" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入症状说明"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "治疗",
                            decorator_id: "treatment",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入治疗" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入治疗"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "保健",
                            decorator_id: "healthcare",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入保健" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入保健"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "诊断时间",
                            decorator_id: "time",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入诊断时间" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入诊断时间"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.diseaseFile);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.health_care_service,
                operation_option: {
                    save: {
                        func_name: "update_disease_file"
                    },
                    query: {
                        func_name: "get_disease_file_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.diseaseFile); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：疾病档案新增查看
 * 描述
 */
@addon('AddDiseaseView', '疾病档案新增查看', '描述')
@reactControl(AddDiseaseView, true)
export class AddDiseaseViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}