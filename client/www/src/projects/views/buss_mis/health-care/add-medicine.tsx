import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from 'src/projects/remote';
import { Select } from 'antd';
const { Option } = Select;
/**
 * 组件：药品档案新增查看状态
 */
export interface AddMedicineViewState extends ReactViewState {
    status?: any;
}

/**
 * 组件：药品档案新增查看
 * 描述
 */
export class AddMedicineView extends ReactView<AddMedicineViewControl, AddMedicineViewState> {
    constructor(props: AddMedicineViewControl) {
        super(props);
        this.state = {
            status: ["分类1", "分类2", "分类3", "分类4"],
        };
    }
    componentDidMount() {
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "药品档案新增",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "编码",
                            decorator_id: "num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入编码" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入编码"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "药品名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入药品名称" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入药品名称"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "药品类型",
                            decorator_id: "type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择药品类型" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择药品类型",
                                showSearch: true,
                                childrens: this.state.status!.map((item: any, idx: any) => <Option key={idx} value={item}>{item}</Option>),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "有效期",
                            decorator_id: "time",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入有效期" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入有效期"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "价格",
                            decorator_id: "price",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入价格" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入价格"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "单位",
                            decorator_id: "allowance_project_name6",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入单位" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入单位"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "剂型",
                            decorator_id: "dosage",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入剂型" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入剂型"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "规格",
                            decorator_id: "specifications",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入规格" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入规格"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "厂家",
                            decorator_id: "manufactor",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入厂家" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入厂家"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "拆零数量",
                            decorator_id: "cl_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入拆零数量" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入拆零数量"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "拆零规格",
                            decorator_id: "cl_specifications",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入拆零规格" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入拆零规格"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "拆零价格",
                            decorator_id: "cl_price",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入拆零价格" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入拆零价格"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "用途",
                            decorator_id: "purpose",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入用途" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入用途"
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.upload,
                                        label: "图片（大小小于2M，格式支持jpg/jpeg/png）",
                                        decorator_id: "photo",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请选择药品相关图片" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            action: remote.upload_url,
                                            // contents: 'button'
                                        },
                                    }
                                ]
                            },
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.medicineFile);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.health_care_service,
                operation_option: {
                    save: {
                        func_name: "update_medicine_file"
                    },
                    query: {
                        func_name: "get_medicine_file_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.medicineFile); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：药品档案新增查看
 * 描述
 */
@addon('AddMedicineView', '药品档案新增查看', '描述')
@reactControl(AddMedicineView, true)
export class AddMedicineViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}