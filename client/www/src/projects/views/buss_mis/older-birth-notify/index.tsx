import { Select, Button } from "antd";
// import moment from 'moment';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;
// const startValue = moment().hour(0).utc();
// const endValue = moment().hour(23).utc();
/**
 * 组件：长者生日提醒状态
 */
export interface OlderBirthNotifyState extends ReactViewState {/** 选择行Ids */
    select_ids?: string[];
    request_ulr?: string;
    delete_modal_visible?: boolean;
    order_id?: string;
}

export function getAgeFromDateBirth(strBirthday: any) {
    if (!strBirthday || strBirthday.split("-").length === 0) {
        return '';
    }
    let returnAge,
        strBirthdayArr = strBirthday.split("-"),
        birthYear = strBirthdayArr[0],
        birthMonth = strBirthdayArr[1],
        birthDay = strBirthdayArr[2],
        d = new Date(),
        nowYear = d.getFullYear(),
        nowMonth = d.getMonth() + 1,
        nowDay = d.getDate();
    if (Number(nowYear) === Number(birthYear)) {
        // 同年 则为1周岁
        return 1;
    } else {
        // 年之差
        let ageDiff = nowYear - birthYear;
        if (ageDiff > 0) {
            if (nowMonth === birthMonth) {
                // 日之差
                let dayDiff = nowDay - birthDay;
                if (dayDiff < 0) {
                    returnAge = ageDiff - 1;
                } else {
                    returnAge = ageDiff;
                }
            } else {
                // 月之差
                let monthDiff = nowMonth - birthMonth;
                if (monthDiff < 0) {
                    returnAge = ageDiff - 1;
                } else {
                    returnAge = ageDiff;
                }
            }
        } else {
            // 表示出生日期输入错误 晚于今天
            returnAge = '';
        }
    }
    // 返回周岁年龄
    return returnAge;
}

/**
 * 组件：长者生日提醒信息
 * 描述
 */
export class OlderBirthNotify extends ReactView<OlderBirthNotifyControl, OlderBirthNotifyState> {
    private columns_data_source = [{
        title: '日期',
        dataIndex: 'date_birth',
        key: 'date_birth',
    }, {
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '年龄',
        dataIndex: 'id_card',
        key: 'id_card',
        render: (text: any, record: any) => {
            return getAgeFromDateBirth(record.date_birth);
        }
    }, {
        title: '家庭地址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '镇街',
        dataIndex: 'area_name',
        key: 'area_name',
    },
    {
        title: '长者类型',
        dataIndex: 'old_type_name',
        key: 'old_type_name',
    },
    {
        title: '电话号码',
        dataIndex: 'phone',
        key: 'phone',
    },
    {
        title: '操作',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <Button type='primary' onClick={() => { this.goDetail(record.id); }}>编辑</Button>
        )
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            delete_modal_visible: false,
            order_id: '',
        };
    }

    goDetail = (id: any) => {
        // this.props.history!.push(ROUTE_PATH.changeElderInfo + '/' + id);
        window.open(ROUTE_PATH.changeElderInfo + '/' + id, '_blank');
    }

    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
    }
    componentDidMount() {
    }
    render() {
        let table_param = {
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        const date_scope = ['今天', '本周', '本月'];
        const date_scope_list: JSX.Element[] = date_scope!.map((item) => {
            return <Option key={item} value={item}>{item}</Option>;
        });

        let service_provider = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "生日范围",
                    decorator_id: "date_scope",
                    option: {
                        childrens: date_scope_list,
                        placeholder: "请选择生日范围",
                    },
                }
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
            },
            select_permission: this.props.select_permission,
        };
        let service_order_list = Object.assign(service_provider, table_param);
        return (
            <div>
                <SignFrameLayout {...service_order_list} />
            </div>
        );
    }
}

/**
 * 控件：长者生日提醒
 * 描述
 */
@addon('OlderBirthNotify', '控件：长者生日提醒', '描述')
@reactControl(OlderBirthNotify, true)
export class OlderBirthNotifyControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 额外参数 */
    public select_param?: any;
}