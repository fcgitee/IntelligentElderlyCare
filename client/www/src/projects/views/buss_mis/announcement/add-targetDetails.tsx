import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { Select } from 'antd';
const { Option } = Select;
/**
 * 组件：新增评比详细指标查看状态
 */
export interface AddTargetDetailsViewState extends ReactViewState {
    list?: any;
}

/**
 * 组件：新增评比详细指标查看
 * 描述
 */
export class AddTargetDetailsView extends ReactView<AddTargetDetailsViewControl, AddTargetDetailsViewState> {
    constructor(props: AddTargetDetailsViewControl) {
        super(props);
        this.state = {
            list: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_xfy_target_setting_list!({})!
            .then(data => {
                this.setState({
                    list: data.result
                });
            });
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "评比详细指标",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "一级指标",
                            decorator_id: "first_level_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择一级指标" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择一级指标",
                                showSearch: true,
                                autoComplete: 'off',
                                childrens: this.state.list!.map((item: any, idx: any) => <Option key={idx} value={item.id}>{item.name}</Option>),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "二级指标",
                            decorator_id: "second_level_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入二级指标" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                autoComplete: 'off',
                                placeholder: "请输入二级指标"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "三级指标",
                            decorator_id: "third_level_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入三级指标" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                autoComplete: 'off',
                                placeholder: "请输入三级指标"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "评比细则",
                            decorator_id: "evaluation_rules",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入评比细则" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                autoComplete: 'off',
                                placeholder: "请输入评比细则"
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "基础分",
                            decorator_id: "base_score",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入基础分" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                autoComplete: 'off',
                                placeholder: "请输入基础分"
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "加分项",
                            decorator_id: "bonus_points",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入加分项" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                autoComplete: 'off',
                                placeholder: "请输入加分项"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.targetDetails);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_xfy_target_setting_details"
                    },
                    query: {
                        func_name: "get_xfy_target_setting_details_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.targetDetails); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：新增评比详细指标查看
 * 描述
 */
@addon('AddTargetDetailsView', '新增评比详细指标查看', '描述')
@reactControl(AddTargetDetailsView, true)
export class AddTargetDetailsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}