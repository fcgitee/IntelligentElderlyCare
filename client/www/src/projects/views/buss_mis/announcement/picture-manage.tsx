
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Input, Button, Row, Col, Icon, Form } from 'antd';
import { isPermission } from "src/projects/app/permission";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
/** 状态：图片管理视图 */
export interface PictureManageViewState extends ReactViewState {
    show?: any;
    org_id?: any;
    show_value?: any;
    option?: any;
    administrative_division_list?: any;
    service_Type_list?: any;
    service_product_list?: any;
    type?: any;
    service_product_id?: any;
    show_product_name?: any;
    order_date?: any;
    order_code?: any;
    amout?: any;
    comment?: any;
    show_name?: any;
}
/** 组件：图片管理视图 */
export class PictureManageView extends React.Component<PictureManageViewControl, PictureManageViewState> {
    private columns_data_source = [{
        title: '图片名称',
        dataIndex: '',
        key: '',
        render: (text: any, record: any, index: number) => {
            return '图片名称';
        }
    },
    {
        title: '图片',
        dataIndex: '',
        key: '',
        render: (text: any, record: any, index: number) => (
            <img style={{width: '50px', height: '50px'}} src="http://a2.att.hudong.com/01/94/20300000926291132247944405335.jpg" alt=""/>
        )
    },
    {
        title: '备注',
        dataIndex: '',
        key: '',
        render: (text: any, record: any, index: number) => {
            return '备注';
        }
    },
    {
        title: '操作时间',
        dataIndex: '',
        key: '',
        render: (text: any, record: any, index: number) => {
            return '操作时间';
        }
    }];
    constructor(props: PictureManageViewControl) {
        super(props);
        this.state = {
            show: false,
            org_id: '',
            show_value: '',
            administrative_division_list: [],
            option: {},
            service_Type_list: [],
            service_product_list: [],
            type: '',
            service_product_id: '',
            show_product_name: '',
            order_date: '',
            order_code: '',
            amout: '',
            comment: '',
            show_name: '',
        };
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({
                show: true
            });
        }
    }

    iconClick = () => {
        let show = this.state.show;
        this.setState({
            show: !show
        });
    }

    // 取消
    cancle = () => {
        this.setState({
            show: false
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.comfirm_order_service,
            service_option: {
                select: {
                    service_func: 'get_rt_order_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <div style={{ position: 'relative', backgroundColor: '#ffffff', height: this.state.show === true ? '220px' : '60px', margin: '36px 36px 0px 36px', paddingTop: '20px' }}>
                    {this.state.show === true ?
                        <div>
                            <Form>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('order_code', {
                                                initialValue: '',
                                            })(
                                                <div>图片名称：<Input style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('service_provider_id', {
                                                initialValue: '',
                                            })(

                                                <div>备注：<Input style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('service_provider_id', {
                                                initialValue: '',
                                            })(

                                                <div style={{display: 'flex'}}>图片（大小小于2M，格式支持jpg/jpeg/png）：<FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <div style={{ paddingLeft: '45%' }}>
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }}>保存</Button>
                                    <Button onClick={this.cancle}>取消</Button>
                                </div>
                            </Form>
                        </div>
                        :
                        ''
                    }
                    {this.state.show === false ?
                        <div onClick={this.iconClick} style={{ backgroundColor: '#ffffff', width: '50px', height: '50px', position: 'absolute', top: '10px', right: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon type='plus' />
                        </div> : ''
                    }
                </div>
                <SignFrameLayout {...allowance_read_list} />
            </div >
        );
    }
}

/**
 * 控件：图片管理视图控制器
 * @description 图片管理视图
 * @author
 */
@addon(' PictureManageView', '图片管理视图', '图片管理视图')
@reactControl(Form.create<any>()(PictureManageView), true)
export class PictureManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}