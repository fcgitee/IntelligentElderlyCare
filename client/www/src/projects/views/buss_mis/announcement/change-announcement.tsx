import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：系统公告页面状态
 */
export interface ChangeAnnouncementViewState extends ReactViewState {
    /** 数据id */
}

/**
 * 组件：系统公告页面
 * 描述
 */
export class ChangeAnnouncementView extends ReactView<ChangeAnnouncementViewControl, ChangeAnnouncementViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "公告",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.text_area,
                            label: "公告内容",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入公告内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入名称"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "公告类型",
                            decorator_id: "type",
                            field_decorator_option: {
                                rules: [{ required: true }],
                            } as GetFieldDecoratorOptions,
                            option: {
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "接收方",
                            decorator_id: "receiver",
                            field_decorator_option: {
                                rules: [{ required: true }],
                            } as GetFieldDecoratorOptions,
                            option: {
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.announcement);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.annoutment_service,
                operation_option: {
                    query: {
                        func_name: "get_annoutment_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_annoutment"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.announcement); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：系统公告页面
 * 描述
 */
@addon('ChangeArticleTypeView', '系统公告页面', '描述')
@reactControl(ChangeAnnouncementView, true)
export class ChangeAnnouncementViewControl extends ReactViewControl {
}
