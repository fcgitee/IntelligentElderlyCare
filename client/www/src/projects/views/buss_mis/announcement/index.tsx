import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：系统公告列表状态
 */
export interface AnnouncementViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：系统公告列表
 * 描述
 */
export class AnnouncementView extends ReactView<AnnouncementViewControl, AnnouncementViewState> {
    private columns_data_source = [{
        title: '公告内容',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '发布方',
        dataIndex: 'user.name',
        key: 'user.name',
    },
    {
        title: '公告类型',
        dataIndex: 'type',
        key: 'type',
    },
    {
        title: '接收方',
        dataIndex: 'receiver',
        key: 'receiver',
    },
    {
        title: '发布时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }];
    constructor(props: AnnouncementViewControl) {
        super(props);
        this.state = {
            request_url: '',
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeAnnouncement);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeAnnouncement + '/' + contents.id);
        }
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "公告内容",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入公告内容"
                    }
                }, {
                    type: InputType.date,
                    label: "发布时间",
                    decorator_id: "modify_date",
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.annoutment_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_annoutment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：系统公告列表页面
 * 描述
 */
@addon('AnnouncementView', '系统公告列表页面', '描述')
@reactControl(AnnouncementView, true)
export class AnnouncementViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}