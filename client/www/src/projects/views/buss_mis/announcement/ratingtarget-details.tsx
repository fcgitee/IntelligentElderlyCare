
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { UploadFile } from "src/business/components/buss-components/upload-file/index";

/** 状态：评比详细指标视图 */
export interface RatingtargetDetailsViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    config?: any;
}
/** 组件：评比详细指标视图 */
export class RatingtargetDetailsView extends React.Component<RatingtargetDetailsViewControl, RatingtargetDetailsViewState> {
    private columns_data_source = [{
        title: '一级指标',
        dataIndex: 'first_level_name',
        key: 'first_level_name',
    },
    {
        title: '二级指标',
        dataIndex: 'second_level_name',
        key: 'second_level_name',
    },
    {
        title: '序号',
        dataIndex: 'code',
        key: '',
    }, {
        title: '三级指标',
        dataIndex: 'third_level_name',
        key: 'third_level_name',
        width: 400,
    },
    {
        title: '评比细则',
        dataIndex: 'evaluation_rules',
        key: 'evaluation_rules',
        width: 400,
    },
    {
        title: '分值',
        dataIndex: '',
        key: '',
        children: [
            {
                title: '基础分',
                dataIndex: 'base_score',
                key: 'base_score',
            },
            {
                title: '加分项',
                dataIndex: 'bonus_points',
                key: 'bonus_points',
            },
        ]
    }];
    constructor(props: RatingtargetDetailsViewControl) {
        super(props);
        this.state = {
            request_url: '',
            config: {
                upload: false,
                func: AppServiceUtility.person_org_manage_service.upload_index_excel,
                title: '导入',
                isFormat: true,
            }
        };
    }

    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.addTargetDetails);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.addTargetDetails + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let target_detail = {
            type_show: false,
            btn_props: [{
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            }, {
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_xfy_target_setting_details_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_xfy_target_setting_details'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let target_detail_list = Object.assign(target_detail, table_param);
        return (
            <div>
                <SignFrameLayout {...target_detail_list} />
                <UploadFile {...this.state.config} />
            </div>
        );
    }
}

/**
 * 控件：评比详细指标视图控制器
 * @description 评比详细指标视图
 * @author
 */
@addon(' RatingtargetDetailsView', '评比详细指标视图', '评比详细指标视图')
@reactControl(RatingtargetDetailsView, true)
export class RatingtargetDetailsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}