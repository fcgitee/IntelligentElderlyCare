
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Input, Button, Row, Col, Form, message, Card, Icon } from 'antd';
import { isPermission } from "src/projects/app/permission";
import { request } from "src/business/util_tool";
import { MainContent } from "src/business/components/style-components/main-content";

class EditForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_send: false,
        };
    }
    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this);
        }
    }
    resetForm = () => {
        this.props.form.setFieldsValue({
            name: '',
            description: '',
        });
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['name'] = values['name'];
                formValue['description'] = values['description'];
                // 编辑
                if (this.props.data_info && this.props.data_info.id) {
                    formValue['id'] = this.props.data_info['id'];
                }
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.person_org_manage_service.update_xfy_target_setting!(formValue))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    message.info('保存成功', 1, () => {
                                        if (this.props.onOk) {
                                            // this.resetForm();
                                            this.props.onOk();
                                        }
                                    });
                                } else {
                                    this.setState({
                                        is_send: false
                                    });
                                }
                            })
                            .catch((error: any) => {
                                // console.log(error);
                                this.setState({
                                    is_send: false
                                });
                            });
                    }
                );
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { data_info, formItemLayout } = this.props;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Row justify="space-around" style={{ textAlign: 'center' }}>
                        <Col span={12}>
                            <Form.Item label='一级指标：'>
                                {getFieldDecorator('name', {
                                    initialValue: data_info['name'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入一级指标'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入一级指标" />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label='指标说明：'>
                                {getFieldDecorator('description', {
                                    initialValue: data_info['description'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入指标说明'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入指标说明" />
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                        {/* &nbsp;
                        &nbsp;
                        <Button type='primary' onClick={this.resetForm}>重置</Button> */}
                    </Row>
                </Form>
            </Row>
        );
    }
}
const ChangeRatingTargetSetting = Form.create<any>()(EditForm);

/** 状态：社区幸福院评比指标设置视图 */
export interface RatingtargetSettingViewState extends ReactViewState {
    show?: any;
    data_info?: any;
    add_permission?: boolean;
}
/** 组件：社区幸福院评比指标设置视图 */
export class RatingtargetSettingView extends React.Component<RatingtargetSettingViewControl, RatingtargetSettingViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '一级指标',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '指标说明',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },];
    constructor(props: RatingtargetSettingViewControl) {
        super(props);
        this.state = {
            show: false,
            add_permission: false,
            data_info: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: '社区幸福院评比指标设置', p: '新增' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        add_permission: datas,
                    });
                }
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({
                show: true,
                data_info: contents,
            });
        }
    }
    toggleAdd = () => {
        this.setState(
            {
                data_info: [],
                show: !this.state.show
            }
        );
    }
    // 取消
    cancle = () => {
        this.setState({
            show: false
        });
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    onOk = () => {
        this.formCreator && this.formCreator.reflash();
        this.setState({
            show: false,
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let target_setting = {
            type_show: false,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_xfy_target_setting_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_xfy_target_setting'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            delete_permission: this.props.delete_permission,
            add_permission: this.props.add_permission,
            onRef: this.onRef,
        };
        let target_setting_list = Object.assign(target_setting, table_param);
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        let { show, add_permission, data_info } = this.state;
        // 要求展开
        let is_icon_show: boolean = true;
        if (!add_permission) {
            // 没有新增权限
            if (data_info && !data_info.id) {
                is_icon_show = false;
            }
        }
        return (
            <Row>
                {show ? <MainContent>
                    <Card title={data_info && data_info.id ? '编辑指标' : '新增指标'}>
                        <ChangeRatingTargetSetting formItemLayout={formItemLayout} data_info={data_info} onOk={this.onOk} />
                    </Card>
                </MainContent> : null}
                <MainContent>
                    <Card title='社区幸福院评比指标设置' extra={is_icon_show ? <Icon type={show ? 'minus-circle' : 'plus-circle'} onClick={this.toggleAdd} /> : null}>
                        <SignFrameLayout {...target_setting_list} />
                    </Card>
                </MainContent>
            </Row>
        );
    }
}

/**
 * 控件：社区幸福院评比指标设置视图控制器
 * @description 社区幸福院评比指标设置视图
 * @author
 */
@addon(' RatingtargetSettingView', '社区幸福院评比指标设置视图', '社区幸福院评比指标设置视图')
@reactControl(Form.create<any>()(RatingtargetSettingView), true)
export class RatingtargetSettingViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}