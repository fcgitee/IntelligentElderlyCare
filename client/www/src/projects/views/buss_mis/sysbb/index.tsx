import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { Table } from "antd";
/**
 * 组件：双鸭山报表总页面状态
 */
export interface SysbbState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    // 详情
    data_info?: any;
}

/**
 * 组件：双鸭山报表总页面
 * 双鸭山报表总页面
 */
export class Sysbb extends ReactView<SysbbViewControl, SysbbState> {
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
            data_info: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
    }
    get_number(num: number, flag = false) {
        if (flag === true) {
            return this.randomFrom(4, 80);
        }
        return this.randomFrom(4, 80) + '%';
    }
    randomFrom(lowerValue: any, upperValue: any) {
        return Math.floor(Math.random() * (upperValue - lowerValue + 1) + lowerValue);
    }
    format() {
        var myDate = new Date();
        let Y: any = myDate.getFullYear() + '-';
        let M: any = (myDate.getMonth() + 1 < 10 ? '0' + (myDate.getMonth() + 1) : myDate.getMonth() + 1) + '-';
        let D: any = myDate.getDate() + ' ';
        let h: any = myDate.getHours() + ':';
        let m: any = myDate.getMinutes() + ':';
        let s: any = myDate.getSeconds();
        return Y + M + D + h + m + s;
    }
    render() {
        let select_param = this.props.select_param.type;
        let columns: any = [];
        let data: any = [];
        let nameList: any = '梦琪、忆柳、之桃、慕青、问兰、尔岚、元香、初夏、沛菡、傲珊、曼文、乐菱、痴珊、恨玉、惜文、香寒、新柔、语蓉、海安、夜蓉、涵柏、水桃、醉蓝、春儿、语琴、从彤、傲晴、语兰、又菱、碧彤、元霜、怜梦、紫寒、妙彤、曼易、南莲、紫翠、雨寒、易烟、如萱、若南、寻真、晓亦、向珊、慕灵、以蕊、寻雁、映易、雪柳、孤岚、笑霜、海云、凝天、沛珊、寒云、冰旋、宛儿、绿真、盼儿、晓霜、碧凡、夏菡、曼香、若烟、半梦、雅绿、冰蓝、灵槐、平安、书翠、翠风、香巧、代云、梦曼、幼翠、友巧、听寒、梦柏、醉易、访旋、亦玉、凌萱、访卉、怀亦、笑蓝、春翠、靖柏、夜蕾、冰夏、梦松、书雪、乐枫、念薇、靖雁、寻春、恨山、从寒、忆香、'.split('、');
        let xList: any = ['陈', '李', '张', '黄', '何', '史', '廖', '林', '白', '蔡'];
        if (select_param === '长者类型') {
            columns = [
                {
                    title: '名字',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'left'
                },
                {
                    title: '性别',
                    dataIndex: 'sex',
                    key: 'sex',
                    align: 'left'
                },
                {
                    title: '年龄',
                    dataIndex: 'age',
                    key: 'age',
                    align: 'left'
                },
                {
                    title: '证件类型',
                    dataIndex: 'zjlx',
                    key: 'zjlx',
                    align: 'left'
                },
                {
                    title: '民族',
                    dataIndex: 'minzu',
                    key: 'minzu',
                    align: 'left'
                },
                {
                    title: '长者类型',
                    dataIndex: 'type',
                    key: 'type',
                    align: 'left'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    name: xList[this.randomFrom(0, xList.length - 1)] + '' + nameList[this.randomFrom(0, nameList.length - 1)],
                    sex: ['男', '女'][this.randomFrom(0, 1)],
                    age: this.randomFrom(0, 10) + 50,
                    zjlx: '身份证',
                    minzu: '汉族',
                    type: ['A1', 'A2', 'A3', 'B1', 'B2', 'B3'][this.randomFrom(0, 5)],
                });
            }
        } else if (select_param === '居家长者') {
            columns = [
                {
                    title: '名字',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'left'
                },
                {
                    title: '性别',
                    dataIndex: 'sex',
                    key: 'sex',
                    align: 'left'
                },
                {
                    title: '年龄',
                    dataIndex: 'age',
                    key: 'age',
                    align: 'left'
                },
                {
                    title: '证件类型',
                    dataIndex: 'zjlx',
                    key: 'zjlx',
                    align: 'left'
                },
                {
                    title: '民族',
                    dataIndex: 'minzu',
                    key: 'minzu',
                    align: 'left'
                },
                {
                    title: '长者类型',
                    dataIndex: 'type',
                    key: 'type',
                    align: 'left'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    name: xList[this.randomFrom(0, xList.length - 1)] + '' + nameList[this.randomFrom(0, nameList.length - 1)],
                    sex: ['男', '女'][this.randomFrom(0, 1)],
                    age: this.randomFrom(0, 10) + 50,
                    zjlx: '身份证',
                    minzu: '汉族',
                    type: ['A1', 'A2', 'A3', 'B1', 'B2', 'B3'][this.randomFrom(0, 5)],
                });
            }
        } else if (select_param === '幸福院') {
            columns = [
                {
                    title: '类型',
                    dataIndex: 'type',
                    key: 'type',
                    // fixed: 'left',
                    align: 'left'
                },
                {
                    title: '项目',
                    dataIndex: 'name',
                    key: 'name',
                    // fixed: 'left',
                    align: 'left'
                },
                {
                    title: '数值',
                    dataIndex: 'shuzhi',
                    key: 'shuzhi',
                    align: 'center'
                },
                {
                    title: '上月同比变化率',
                    dataIndex: 'sytbbhl',
                    key: 'sytbbhl',
                    align: 'center'
                },
                {
                    title: '上半年同比变化率',
                    dataIndex: 'sbntbbhl',
                    key: 'sbntbbhl',
                    align: 'center'
                },
                {
                    title: '上年同比变化率',
                    dataIndex: 'sntbbhl',
                    key: 'sntbbhl',
                    align: 'center'
                },
                {
                    title: '统计时间',
                    dataIndex: 'tjsj',
                    key: 'tjsj',
                    align: 'center',
                }
            ];
            let Zh: any = [{
                'type': '基础',
                'childs': [
                    '幸福院挂牌总数',
                    '幸福院挂牌通过验收总数',
                    '自运营幸福院总数',
                    '社工机构总数',
                    'APP流量总数',
                    '幸福院覆盖率',
                    '志愿者工作站总数',
                    '幸福小站总数',
                    '社区长者饭堂总数',
                ]
            }, {
                'type': '评比',
                'childs': [
                    '幸福院挂牌参与评比总数',
                    '幸福院评比达到优良总数',
                ]
            }, {
                'type': '运营',
                'childs': [
                    '幸福院设备使用总人次',
                    '幸福院工作人员总数',
                    '持证工作人员总数',
                    '幸福院工作人员中专以上学历总数',
                    '投诉总次数',
                    '幸福院活动组织总数',
                    '幸福院参加活动总人数',
                    '幸福院APP活动发布总数',
                    '志愿者总数',
                    '志愿者总时长',
                    '幸福小站商品类别总数',
                    '社区长者饭堂用餐总数',
                ]
            }, {
                'type': '收支',
                'childs': [
                    '幸福院建设资助总额',
                    '幸福院评比奖励总额',
                    '平台捐赠总额',
                    '平台受赠总额',
                    '交卖商城成交总数',
                    '幸福小站交易总额',
                    '长者饭堂补贴总额',
                ]
            }];

            Zh.map((item: any, index: number) => {
                var type = false;
                item.childs.map((itm: any, idx: number) => {
                    data.push({
                        type: type === false ? item.type : '',
                        name: itm,
                        shuzhi: this.get_number(10000, true),
                        sytbbhl: this.get_number(10000),
                        sbntbbhl: this.get_number(10000),
                        sntbbhl: this.get_number(10000),
                        tjsj: this.format(),
                    });
                    if (type === false) {
                        type = true;
                    }
                });
            });
        } else if (select_param === '机构') {
            columns = [
                {
                    title: '类型',
                    dataIndex: 'type',
                    key: 'type',
                    // fixed: 'left',
                    align: 'left'
                },
                {
                    title: '项目',
                    dataIndex: 'name',
                    key: 'name',
                    // fixed: 'left',
                    align: 'left'
                },
                {
                    title: '数值',
                    dataIndex: 'shuzhi',
                    key: 'shuzhi',
                    align: 'center'
                },
                {
                    title: '上月同比变化率',
                    dataIndex: 'sytbbhl',
                    key: 'sytbbhl',
                    align: 'center'
                },
                {
                    title: '上半年同比变化率',
                    dataIndex: 'sbntbbhl',
                    key: 'sbntbbhl',
                    align: 'center'
                },
                {
                    title: '上年同比变化率',
                    dataIndex: 'sntbbhl',
                    key: 'sntbbhl',
                    align: 'center'
                },
                {
                    title: '统计时间',
                    dataIndex: 'tjsj',
                    key: 'tjsj',
                    align: 'center',
                }
            ];

            let Zh: any = [{
                'type': '基础',
                'childs': [
                    '机构挂牌总数',
                    '机构挂牌通过验收总数',
                    '自运营机构总数',
                    '社工机构总数',
                    'APP流量总数',
                    '机构覆盖率',
                    '志愿者工作站总数',
                    '机构小站总数',
                    '社区长者饭堂总数',
                ]
            }, {
                'type': '评比',
                'childs': [
                    '机构挂牌参与评比总数',
                    '机构评比达到优良总数',
                ]
            }, {
                'type': '运营',
                'childs': [
                    '机构设备使用总人次',
                    '机构工作人员总数',
                    '持证工作人员总数',
                    '机构工作人员中专以上学历总数',
                    '投诉总次数',
                    '机构活动组织总数',
                    '机构参加活动总人数',
                    '机构APP活动发布总数',
                    '志愿者总数',
                    '志愿者总时长',
                    '机构小站商品类别总数',
                    '社区长者饭堂用餐总数',
                ]
            }, {
                'type': '收支',
                'childs': [
                    '机构建设资助总额',
                    '机构评比奖励总额',
                    '平台捐赠总额',
                    '平台受赠总额',
                    '交卖商城成交总数',
                    '机构小站交易总额',
                    '长者饭堂补贴总额',
                ]
            }];

            Zh.map((item: any, index: number) => {
                var type = false;
                item.childs.map((itm: any, idx: number) => {
                    data.push({
                        type: type === false ? item.type : '',
                        name: itm,
                        shuzhi: this.get_number(10000, true),
                        sytbbhl: this.get_number(10000),
                        sbntbbhl: this.get_number(10000),
                        sntbbhl: this.get_number(10000),
                        tjsj: this.format(),
                    });
                    if (type === false) {
                        type = true;
                    }
                });
            });
        }
        // console.log(columns, data);
        return (
            <MainContent>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    rowKey="name"
                    pagination={false}
                /></MainContent>
        );
    }
}

/**
 * 控件：双鸭山报表总页面
 * 双鸭山报表总页面
 */
@addon('Sysbb', '双鸭山报表总页面', '双鸭山报表总页面')
@reactControl(Sysbb, true)
export class SysbbViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public select_param?: any;
}