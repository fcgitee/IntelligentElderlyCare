import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { remote } from "src/projects/remote";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;

/** 状态：社区运营情况统计视图 */
export interface OperateSituationStatisticViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: any[];
    /** 接口名 */
    request_url?: string;
    select_type?: any;
    /** 对话框显示状态 */
    visible?: boolean;
    /** 导入参数 */
    config: any;
    /** 组织机构列表 */
    organization_list?: any;
    administrative_division_list?: any;
}
/** 组件：社区运营情况统计视图 */
export class OperateSituationStatisticView extends React.Component<OperateSituationStatisticViewControl, OperateSituationStatisticViewState> {
    private columns_data_source = [{
        title: '年度',
        dataIndex: 'year',
        key: 'year',
    },
    // {
    //     title: '镇街',
    //     dataIndex: 'street',
    //     key: 'street',
    // },
    {
        title: '社区',
        dataIndex: 'division_name',
        key: 'division_name',
    }, {
        title: '幸福院名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '设立时间',
        dataIndex: 'buildtime',
        key: 'buildtime',
    }, {
        title: '星级',
        dataIndex: 'organization_info.star_level',
        key: 'organization_info.star_level',
        render: (text: string, record: any) => {
            if (record.organization_info.star_level) {
                switch (record.organization_info.star_level) {
                    case "1":
                        return '一星';
                    case "2":
                        return '二星';
                    case "3":
                        return '三星';
                    case "4":
                        return '四星';
                    case "5":
                        return '五星';
                    default:
                        return undefined;
                }
            }
            return undefined;
        }
    },
    {
        title: '服务机构',
        dataIndex: 'server_org',
        key: 'server_org',
    },
    {
        title: '当年获得建筑资助（万元）',
        dataIndex: 'organization_info.year_building_total_fee',
        key: 'organization_info.year_building_total_fee',
    },
    {
        title: '建筑资助累计',
        dataIndex: 'organization_info.build',
        key: 'organization_info.build',
    },
    {
        title: '获得评比奖励',
        dataIndex: 'organization_info/rate_reward',
        key: 'organization_info/rate_reward',
    },
    {
        title: '配备图书数量',
        dataIndex: 'organization_info.book_num',
        key: 'organization_info.book_num',
    },
    {
        title: '每周开放时间',
        dataIndex: 'organization_info.week_open_days',
        key: 'organization_info.week_open_days',
    },
    {
        title: '当年开展活动次数',
        dataIndex: 'activity_count',
        key: 'activity_count',
    },
    {
        title: '社区长者数',
        dataIndex: 'organization_info.active_times.older_num',
        key: 'organization_info.active_times.older_num',
    },
    {
        title: '服务人数占豁辖区长者百分比',
        dataIndex: 'ser_',
        key: 'ser_',
    },
    {
        title: '满意度',
        dataIndex: 'organization_info.active_times.statisfy',
        key: 'organization_info.active_times.statisfy',
    },
    {
        title: '被投诉次数',
        dataIndex: 'organization_info.active_times.complaint_times',
        key: 'organization_info.active_times.complaint_times',
    },
    ];
    constructor(props: OperateSituationStatisticViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: [{}, 1, 10],
            request_url: '',
            select_type: {},
            administrative_division_list: [],
            visible: false,
            config: {
                isFormat: true,
                upload: false,
                func: AppServiceUtility.person_org_manage_service.import_excel_manage,
                title: '导入人员'
            }
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(this.props.change_url ? this.props.change_url : ROUTE_PATH.changePersonnel);
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        let select_type = this.props.select_type;
        this.setState({
            request_url,
            select_type
        });
    }
    componentDidMount() {
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        let rate: any[] = ['一星', '二星', '三星', '四星', '五星'];
        let rate_list: JSX.Element[] = rate.map((item, idx) => <Option key={item} value={`${idx + 1}`}>{item}</Option>);
        if (redeirect) {
            return redeirect;
        }
        let personnel = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.tree_select,
                    label: "社区",
                    decorator_id: "admin_area_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择社区" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        allowClear: true,
                        treeDefaultExpandAll: true,
                        treeData: this.state.administrative_division_list,
                        placeholder: "请选择社区",
                    },
                }, {
                    type: InputType.select,
                    label: "机构星级",
                    decorator_id: "star_level",
                    option: {
                        placeholder: "请选择机构星级",
                        childrens: rate_list,
                    }
                }
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.state.select_type ? this.state.select_type : {}, 1, 10]
                },
                delete: {
                    service_func: this.props.delete_url
                }
            },
            searchExtraParam: this.state.select_type,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            select_type: this.props.select_type
        };
        let personnel_list = Object.assign(personnel, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...personnel_list} />
                </div>
            )
        );
    }
}

/**
 * 控件：社区运营情况统计视图控制器
 * @description 社区运营情况统计视图
 * @author
 */
@addon('PersonnelView', '社区运营情况统计视图', '社区运营情况统计视图')
@reactControl(OperateSituationStatisticView, true)
export class OperateSituationStatisticViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 人员类型 */
    public personnel_type?: string;
    /** 编辑页面Url */
    public change_url?: string;
    /** 请求接口名 */
    public request_url?: string;
    /** 删除接口名 */
    public delete_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 查询条件 */
    public select_type?: any;
}