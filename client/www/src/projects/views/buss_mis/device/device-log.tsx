import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Row, Button } from "antd";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：设备日志状态
 */
export interface DeviceLogViewState extends ReactViewState {

}

/**
 * 组件：设备日志
 * 描述
 */
export class DeviceLogView extends ReactView<DeviceLogViewControl, DeviceLogViewState> {
    private columns_data_source = [{
        title: '设备名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '生成时间',
        dataIndex: 'log_time',
        key: 'log_time',
    }, {
        title: '日志类型',
        dataIndex: 'log_type',
        key: 'log_type',
    }, {
        title: '等级',
        dataIndex: 'log_level',
        key: 'log_level',
    }, {
        title: '经度',
        dataIndex: 'log_content.latitude',
        key: 'log_content.latitude',
    }, {
        title: '纬度',
        dataIndex: 'log_content.longitude',
        key: 'log_content.longitude',
    }];

    constructor(props: DeviceLogViewControl) {
        super(props);
        this.state = {

        };
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.Device);
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: 'get_device_log_all',
                    service_condition: [{ id: this.props.match!.params.key }, 1, 10]
                }
            },
        };
        let service_item_list = Object.assign(service_item, table_param);
        delete service_item_list.other_label_type;
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
                <Row type='flex' justify='center'>
                    <Button onClick={this.returnBtn}>返回</Button>
                </Row>
            </div>
        );
    }
}

/**
 * 设备日志页面
 */
@addon('DeviceView', '设备日志页面', '描述')
@reactControl(DeviceLogView, true)
export class DeviceLogViewControl extends ReactViewControl {
} 