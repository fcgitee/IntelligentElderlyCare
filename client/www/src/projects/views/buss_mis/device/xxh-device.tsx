import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { message, Button } from "antd";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ROUTE_PATH } from "src/projects/router";
import { getAge } from "src/projects/app/util-tool";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { exprot_excel } from "src/business/util_tool";
/**
 * 组件：设备列表状态
 */
export interface XxhDeviceViewState extends ReactViewState {
    /** 对话框 */
    visible?: boolean;
    /** 人员id */
    user_id?: string;
    /** 设备id */
    device_id?: string;
    /** 设备编号 */
    imei?: string;
    /** 接口名 */
    request_url?: string;
    config?: any;
}

/**
 * 组件：设备列表
 * 描述
 */
export class XxhDeviceView extends ReactView<XxhDeviceViewControl, XxhDeviceViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
        render: (text: any, record: any) => {
            return getAge(record.id_card);
        }
    }, {
        title: '联系电话',
        dataIndex: 'telephone',
        key: 'telephone',
    }, {
        title: '家庭住址',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '设备名称',
        dataIndex: 'device_name',
        key: 'device_name',
    }, {
        title: '设备ID',
        dataIndex: 'device_id',
        key: 'device_id',
    }, {
        title: '设备SIM',
        dataIndex: 'device_sim',
        key: 'device_sim',
    }, {
        title: '智能通话设置',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <Button type='primary' onClick={() => { this.props.history!.push(ROUTE_PATH.callSettings + '/' + record.id); }}>智能通话设置</Button>
        )
    },];
    constructor(props: XxhDeviceViewControl) {
        super(props);
        this.state = {
            visible: false,
            user_id: '',
            device_id: '',
            imei: '',
            request_url: '',
            config: {
                upload: false,
            }
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            // this.setState({
            //     // visible: true,
            //     device_id: contents.id,
            //     imei: contents.imei,
            // });
            // this.props.history!.push(ROUTE_PATH.ChangeDevice + '/' + contents.id + '__' + contents.elder_name);
            this.props.history!.push(ROUTE_PATH.ChangeDevice + '/' + contents.id);
        } else if ('icon_snippets' === type) {
            console.info(contents.id);
            this.props.history!.push(ROUTE_PATH.DeviceLog + '/' + contents.id);
        }
    }
    handleOk = () => {
        AppServiceUtility.device_service.update_device!({ 'id': this.state.device_id, 'user_id': this.state.user_id })!
            .then((data) => {
                if (data) {
                    this.setState({
                        visible: false,
                    });
                    message.info('绑定成功');

                } else {
                    message.info('绑定失败');
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    handleCancel = () => {
        this.setState({
            visible: false,
            device_id: '',
            user_id: '',

        });
    }
    addDevice = () => {
        this.props.history!.push(ROUTE_PATH.ChangeDevice);
    }
    upload = () => {
        const config = {
            upload: true,
            title: '导入长者设备',
            func: AppServiceUtility.device_service.import_device
        };
        this.setState({ config });
    }
    template_down = () => {
        const data = [{
            '长者姓名': 'xxx', '身份证': '4405xxxxx', '设备ID': '', '设备SIM卡': '', '设备名称': '咪狗'
        }];
        exprot_excel([{ name: '设备管理导入模板', value: data }], '设备管理导入模板', 'xls'); // 下载excel
    }
    changeDdvice = (value: any) => {
        this.setState({
            user_id: value
        });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "设备名称",
                decorator_id: "device_name",
                option: {
                    placeholder: "请输入设备名称",
                }
            }, {
                type: InputType.input,
                label: "设备ID",
                decorator_id: "device_id",
                option: {
                    placeholder: "请输入设备ID",
                }
            }, {
                type: InputType.input,
                label: "长者姓名",
                decorator_id: "elder_name",
                option: {
                    placeholder: "请输入长者姓名",
                }
            }, {
                type: InputType.input,
                label: "长者身份证号",
                decorator_id: "id_card",
                option: {
                    placeholder: "请输入长者身份证号",
                }
            }, {
                type: InputType.input,
                label: "SIM卡号",
                decorator_id: "device_sim",
                option: {
                    placeholder: "请输入SIM卡号",
                }
            }],
            btn_props: [{
                label: '新增',
                btn_method: this.addDevice,
                icon: 'plus'
            }, {
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            }, {
                label: '下载模板',
                btn_method: this.template_down,
                icon: 'plus'

            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                delete: {
                    service_func: 'delete_device'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let table_params = {
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let service_item_list = Object.assign(service_item, table_params);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
                <UploadFile {...this.state.config} />
            </div>
        );
    }
}

/**
 * 设备列表页面
 */
@addon('XxhDeviceView', '设备列表页面', '描述')
@reactControl(XxhDeviceView, true)
export class XxhDeviceViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 