import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { message, Select } from "antd";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { edit_props_info } from "src/projects/app/util-tool";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;
message.config({
    top: 350,
});

/**
 * 组件：新增/编辑设备状态
 */
export interface ChangeDeviceViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    is_send: boolean;
    service_date_type: string;
}

/**
 * 组件：新增/编辑设备视图
 */
export class ChangeDeviceView extends ReactView<ChangeDeviceViewControl, ChangeDeviceViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            is_send: false,
            service_date_type: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                if (values) {
                    let key = this.props.match!.params.key;
                    if (key) {
                        // values['id'] = key.split('__')[0];
                        values['id'] = key;
                    }
                    AppServiceUtility.device_service.update_device!(values)!
                        .then((data) => {
                            if (data === 'Success') {
                                message.info('保存成功', 1, () => {
                                    this.props.history!.goBack();
                                });
                            } else {
                                message.info(data === 'fail' ? '保存失败' : data);
                                this.setState({
                                    is_send: false
                                });
                            }
                        })
                        .catch(err => {
                            console.info(err);
                            this.setState({
                                is_send: false
                            });
                        });
                }
            }
        );
    }
    componentWillMount() {
        let key = this.props.match!.params.key;
        if (key) {
            // AppServiceUtility.device_service.get_device_all!({ id: key.split('__')[0] }, 1, 1)!
            //     .then((data) => {
            //         if (data.result) {
            //             this.setState({
            //                 base_data: data.result[0]
            //             });
            //         }
            //     });
            // AppServiceUtility.device_service.get_device_all!({ id: this.props.match!.params.key }, 1, 1)!
            //     .then((data) => {
            //         if (data.result) {
            //             this.setState({
            //                 base_data: data.result[0]
            //             });
            //         }
            //     });
        }
    }
    reach_func = (data: any) => {
        if (data && data.result && data['result'][0]) {
            this.setState({
                base_data: data.result[0],
                service_date_type: data.result[0]['service_date_type']
            });
        }
    }

    onChange = (e: any, type: string) => {
        if (type === 'service_date_type') {
            this.setState({
                service_date_type: e,
            });
        }
    }
    render() {
        let { base_data } = this.state;
        // let elder_id = '';
        // let key = this.props.match!.params.key;
        // if (key) {
        // elder_id = key.split('__')[1];
        // elder_id = key;
        // }
        let device = ['咪狗', '孝心环', '睡睡康', '云芯', '柏颐'];
        let device_dom: JSX.Element[] = device.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let date = ['年月日', '无限'];
        let date_item: JSX.Element[] = date.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_elder_list',
            // service_option: elder_id ? [{ id: elder_id }, 1, 1] : [{}, 1, 10],
            service_option: [{}, 1, 10],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let service_option = {
            service_object: AppServiceUtility.device_service,
            operation_option: {
                save: {
                    func_name: "update_device"
                },
                query: {
                    func_name: "get_device_all",
                    arguments: [{ id: this.props.match!.params.key }, 1, 1]
                }
            },
        };
        const { service_date_type } = this.state;
        let isNYR: boolean = (service_date_type && service_date_type === '年月日') || (base_data.service_date_type && base_data.service_date_type === service_date_type && base_data.service_date_type === '年月日') ? true : false;
        let edit_props = {
            form_items_props: [
                {
                    title: "编辑设备",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者姓名",
                            decorator_id: 'elder_id',
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入名称" }],
                                initialValue: base_data.elder_id,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者姓名",
                                modal_search_items_props: modal_search_items_props
                            },
                        }, {
                            type: InputType.select,
                            label: "设备名称",
                            decorator_id: "device_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择设备名称" }],
                                initialValue: base_data.device_name,
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: device_dom,
                                placeholder: "请选择设备名称",
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "设备ID",
                            decorator_id: "device_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入设备ID" }],
                                initialValue: base_data.device_id,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入设备ID",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "设备SIM卡(手机号)",
                            decorator_id: "device_sim",
                            field_decorator_option: {
                                rules: [{ message: "设备SIM卡" }],
                                initialValue: base_data.device_sim,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "设备SIM卡",
                            }
                        },
                        {
                            type: InputType.select,
                            label: "服务限期",
                            decorator_id: "service_date_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务限期" }],
                                initialValue: base_data.service_date,
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: date_item,
                                placeholder: "请选择服务限期",
                                onChange: (e: any) => {
                                    this.onChange(e, 'service_date_type');
                                }
                            }
                        },
                        {
                            ...(isNYR ? {
                                type: InputType.date,
                                label: "选择年月日",
                                decorator_id: 'service_date_end',
                                field_decorator_option: {
                                    rules: [{ required: true, message: "请选择年月日" }],
                                    initialValue: base_data['service_date_end'],
                                } as GetFieldDecoratorOptions,
                                option: {
                                    placeholder: "请选择年月日",
                                },
                            } : {})
                        },
                        {
                            ...(isNYR ? {
                                type: InputType.radioGroup,
                                label: "自动循环",
                                decorator_id: 'duration',
                                field_decorator_option: {
                                    rules: [{ message: "请选择自动循环时长" }],
                                    initialValue: base_data['duration'],
                                } as GetFieldDecoratorOptions,
                                option: {
                                    placeholder: "请选择自动循环时长",
                                    options: [{
                                        label: '一个月',
                                        value: 'one_month',
                                    }, {
                                        label: '一年',
                                        value: 'one_year',
                                    }]
                                },
                            } : {})
                        },
                        {
                            ...(isNYR ? {
                                type: InputType.checkbox_group,
                                label: "到期提醒",
                                decorator_id: "due_reminder",
                                field_decorator_option: {
                                    rules: [{ required: false, message: "请选择到期提醒" }],
                                    initialValue: base_data.due_reminder,
                                } as GetFieldDecoratorOptions,
                                option: {
                                    placeholder: "请选择到期提醒",
                                    list: [{
                                        name: '提前三日',
                                        id: 'three_days',
                                    }, {
                                        name: '提前一周',
                                        id: 'one_week',
                                    }, {
                                        name: '提前一个月',
                                        id: 'one_month',
                                    }],
                                    autocomplete: 'off',
                                }
                            } : {})
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        // this.props.history!.push(ROUTE_PATH.Device);
                        history.back();
                    },
                    btn_other_props: { disabled: this.state.is_send }
                }
            ],
            submit_btn_propps: {
                text: "确定",
                cb: this.handleSubmit,
                btn_other_props: { disabled: this.state.is_send }
            },
            service_option: service_option,
            reach_func: this.reach_func
            // id: this.props.id,
        };
        // let succ_func = () => { this.props.history!.push(ROUTE_PATH.organization); }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}

/**
 * 控件：新增/编辑设备编辑控件
 * @description 新增/编辑设备编辑控件
 * @author
 */
@addon('ChangeDeviceView', '新增/编辑设备编辑控件', '新增/编辑设备编辑控件')
@reactControl(ChangeDeviceView, true)
export class ChangeDeviceViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}