import { ReactViewState, reactControl, ReactViewControl, ReactView } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { Modal } from "antd";
import { remote } from "src/projects/remote";

/**
 * 组件：调研数据录入状态
 */
export interface AddXFYOrganizatonInfoViewState extends ReactViewState {
    administrative_division_list?: any;
    isBaiduMapShow?: any;
    base_data?: any;
}
/**
 * 组件：调研数据录入视图
 */
export class AddXFYOrganizatonInfoView extends ReactView<AddXFYOrganizatonInfoViewControl, AddXFYOrganizatonInfoViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            administrative_division_list: [],
            isBaiduMapShow: false,
            base_data: {}
        };
    }

    componentDidMount() {
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            });

        AppServiceUtility.person_org_manage_service.get_xfy_org_info!({ id: this.props.match!.params.key }, 1, 1)!
            .then((data: any) => {
                this.setState({
                    base_data: data!.result[0]
                });
            });
    }

    toggleBaiduMap(type: boolean) {
        window.open('http://api.map.baidu.com/lbsapi/getpoint/');
        // this.setState({
        //     isBaiduMapShow: type,
        // });
    }

    render() {
        const { base_data } = this.state;
        let edit_props = {
            form_items_props: [
                {
                    title: "信息录入",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "机构名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入机构名称" }],
                                initialValue: base_data.name ? base_data.name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构名称"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "法定代表人",
                            decorator_id: "legal_person",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入法定代表人" }],
                                initialValue: base_data.legal_person ? base_data.legal_person : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入法定代表人"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "办公电话",
                            decorator_id: "telephone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入办公电话" }],
                                initialValue: base_data.telephone ? base_data.telephone : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入办公电话"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "邮政编码",
                            decorator_id: "postal_code",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入邮政编码" }],
                                initialValue: base_data.postal_code ? base_data.postal_code : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入邮政编码"
                            }
                        },
                        {
                            type: InputType.tree_select,
                            label: "所属镇街",
                            decorator_id: "admin_area_id_zj",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择行政区划" }],
                                initialValue: base_data.admin_area_id_zj ? base_data.admin_area_id_zj : ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择所属镇街",
                            },
                        },
                        {
                            type: InputType.tree_select,
                            label: "所属村居",
                            decorator_id: "admin_area_id_cj",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择行政区划" }],
                                initialValue: base_data.admin_area_id_cj ? base_data.admin_area_id_cj : ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择所属村居",
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "详细地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入机构地址" }],
                                initialValue: base_data.address ? base_data.address : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入详细地址"
                            }
                        },
                        {
                            type: InputType.btn_input,
                            label: "经度",
                            decorator_id: "lon",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入经度" }],
                                initialValue: base_data.lon ? base_data.lon : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入经度",
                                enterButton: "点击拾取坐标",
                                onSearch: () => this.toggleBaiduMap(true)
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "纬度",
                            decorator_id: "lat",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入纬度" }],
                                initialValue: base_data.lat ? base_data.lat : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入纬度"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "经营范围",
                            decorator_id: "business_scope",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入经营范围" }],
                                initialValue: base_data.business_scope ? base_data.business_scope : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入经营范围"
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "统一社会信用代码证（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "unified_social_credit",
                            field_decorator_option: {
                                initialValue: base_data.unified_social_credit ? base_data.unified_social_credit : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "经营范围相关的资质（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "other",
                            field_decorator_option: {
                                initialValue: base_data.other ? base_data.other : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.researchManage);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_xfy_org_info"
                    },
                    query: {
                        func_name: "",
                        arguments: [{}, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.allowanceSuccess); },
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                    <Modal
                        title="坐标拾取系统"
                        width='90%'
                        bodyStyle={{ height: '700px' }}
                        visible={this.state.isBaiduMapShow}
                        onOk={() => this.toggleBaiduMap(false)}
                        onCancel={() => this.toggleBaiduMap(false)}
                        okText="复制后请手动输入"
                    >
                        <iframe name="baiduMapName" id="baiduMapId" src="http://api.map.baidu.com/lbsapi/getpoint/" style={{ width: '100%', height: '100%' }} />
                    </Modal>
                </MainContent>
            )
        );
    }
}
/**
 * 控件：幸福院-运营机构信息录入控件
 * @description 幸福院-运营机构信息录入控件
 * @author
 */
@addon('AddXFYOrganizatonInfoView', '幸福院-运营机构信息录入控件', '幸福院-运营机构信息录入控件')
@reactControl(AddXFYOrganizatonInfoView, true)
export class AddXFYOrganizatonInfoViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}