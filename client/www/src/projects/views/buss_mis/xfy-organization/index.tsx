import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, Modal, message } from "antd";
const { confirm } = Modal;
/**
 * 组件：社区幸福院-运营机构状态
 */
export interface XFYOrganizationListViewState extends ReactViewState {
    is_edit_btn?: any;
    is_delete_btn?: any;
}

/**
 * 组件：社区幸福院-运营机构视图
 */
export class XFYOrganizationListView extends ReactView<XFYOrganizationListViewControl, XFYOrganizationListViewState> {
    private columns_data_source = [
        {
            title: '机构名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '法定代表人',
            dataIndex: 'legal_person',
            key: 'legal_person',
        },
        {
            title: '办公电话',
            dataIndex: 'telephone',
            key: 'telephone',
        },
        {
            title: '邮政编码',
            dataIndex: 'postal_code',
            key: 'postal_code',
        },
        {
            title: '详细地址',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: '经度',
            dataIndex: 'lon',
            key: 'lon',
        },
        {
            title: '纬度',
            dataIndex: 'lat',
            key: 'lat',
        },
        {
            title: '经营范围',
            dataIndex: 'business_scope',
            key: 'business_scope',
        },
        {
            title: '操作',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                <div>
                    <div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.is_edit_btn ?
                                    <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>编辑</Button>
                                    : ''
                            }
                            {
                                this.state.is_delete_btn ?
                                    <Button type="danger" onClick={() => { this.del(record.id); }}>删除</Button>
                                    : ''
                            }
                        </div>
                    </div>
                </div>

            ),
        }];
    constructor(props: XFYOrganizationListViewControl) {
        super(props);
        this.state = {
            is_edit_btn: false,
            is_delete_btn: false
        };
    }

    /** 编辑 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.addXfyOrgInfo + '/' + record.id);
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.addXfyOrgInfo);
    }

    // 删除
    del = (ids: any) => {
        confirm({
            title: '你确认要删除这条数据吗?',
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk() {
                AppServiceUtility.person_org_manage_service.delete_xfy_org_info!({ id: ids })!
                    .then((data: any) => {
                        if (data === 'Success') {
                            message.success('删除成功');
                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        message.error(error.message);
                    });
            },
            onCancel() {
                // console.log('Cancel');
            },
        });
    }

    componentWillMount() {
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        } else if (value.function + value.permission === this.props.delete_permission) {
                            this.setState({
                                is_delete_btn: true
                            });
                        }
                    });
                }
            });
    }

    render() {
        let service_item = {
            type_show: false,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            service_option: {
                select: {
                    service_func: 'get_xfy_org_info',
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'delete_service_situation_byid'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
            </div>
        );
    }
}

/**
 * 控件：个案服务情况列表控件
 * @description 个案服务情况列表控件
 * @author
 */
@addon('XFYOrganizationListView', '个案服务情况列表控件', '个案服务情况列表控件')
@reactControl(XFYOrganizationListView, true)
export class XFYOrganizationListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}