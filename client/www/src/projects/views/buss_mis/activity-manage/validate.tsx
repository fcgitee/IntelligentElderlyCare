// 验证手机号码
export function checkPhone(_: any, value: string, cb: any, notice: string) {
    if (value === '') {
        cb(`请输入${notice}`);
    } else if (!(/^1[3456789]\d{9}$/.test(value))) {
        cb(`请输入正确的${notice}`);
    }
    cb();
}

// 限制输入数字，最小值为1
export function checkMin1(_: any, value: string, cb: any, notice: string) {
    if (value === '') {
        cb(`请输入${notice}`);
    } else if (!/^\+?[1-9][0-9]*$/.test(value)) {
        cb(`请输入正确的${notice}`);
    }
    cb();
}

// 限制输入数字，最小值为0
export function checkMin0(_: any, value: string, cb: any, notice: string) {
    if (value === '') {
        cb(`请输入${notice}`);
    } else if (!/(^[0-9]\d*$)/.test(value)) {
        cb(`请输入正确的${notice}`);
    }
    cb();
}