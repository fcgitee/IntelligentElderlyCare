import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Descriptions, Row, } from "antd";
/**
 * 组件：活动信息状态
 */
export class ActivityInfoState {
}

/**
 * 组件：活动信息
 * 活动信息
 */
export class ActivityInfo extends React.Component<ActivityInfoControl, ActivityInfoState> {

    componentWillMount() {
        this.setState({
            activity_info: this.props.activity_info || [],
        });
    }
    render() {
        let { activity_info } = this.props;

        // 初始化，抑制报错
        if (!activity_info) {
            activity_info = [];
        }

        return (
            <Descriptions title="活动信息" bordered={true}>
                <Descriptions.Item label="活动名称">{activity_info['activity_name'] || ''}</Descriptions.Item>
                <Descriptions.Item label="活动类型">{activity_info['activity_type_name'] ? activity_info['activity_type_name'] : ''}</Descriptions.Item>
                <Descriptions.Item label="团体">{activity_info['activity_group_name'] ? activity_info['activity_group_name'] : ''}</Descriptions.Item>
                <Descriptions.Item label="活动开始时间">{activity_info['begin_date'] || ''}</Descriptions.Item>
                <Descriptions.Item label="活动结束时间">{activity_info['end_date'] || ''}</Descriptions.Item>
                <Descriptions.Item label="活动地点">{activity_info['address'] || ''}</Descriptions.Item>
                <Descriptions.Item label="活动场室">{activity_info['activity_room_name'] ? activity_info['activity_room_name'] : ''}</Descriptions.Item>
                {/* <Descriptions.Item label="商家">{activity_info['organization_name'] ? activity_info['organization_name'] : ''}</Descriptions.Item> */}
                <Descriptions.Item label="最大参与人数">{activity_info['max_quantity'] || 0}</Descriptions.Item>
                <Descriptions.Item label="联系人">{activity_info['contacts'] || ''}</Descriptions.Item>
                <Descriptions.Item label="联系人电话">{activity_info['phone'] || ''}</Descriptions.Item>
                <Descriptions.Item label="活动计划">{activity_info['plan'] && activity_info['plan'][0] ? <a href={activity_info['plan'][0]} target="_blank">点击查看</a> : null}</Descriptions.Item>
                <Descriptions.Item label="活动图片" className="viewPictureWhenClick">
                    {activity_info['photo'] && activity_info['photo'].length > 0 ? activity_info['photo'].map((item: any, index: number) => {
                        return (
                            <Row key={index}>
                                <img alt={activity_info['title']} src={item} />
                            </Row>
                        );
                    }) : null}
                </Descriptions.Item>
                <Descriptions.Item label="活动介绍" span={3}><div dangerouslySetInnerHTML={{ __html: activity_info['introduce'] || '' }} className="viewPictureWhenClick" /></Descriptions.Item>
                <Descriptions.Item label="提交时间">{activity_info['create_date'] ? activity_info['create_date'] : ''}</Descriptions.Item>
            </Descriptions>
        );
    }
}

/**
 * 控件：活动信息控制器
 * 活动信息
 */
@addon('ActivityInfo', '活动信息', '活动信息')
@reactControl(ActivityInfo, true)
export class ActivityInfoControl extends BaseReactElementControl {
    /** 基础数据 */
    activity_info?: any;
}