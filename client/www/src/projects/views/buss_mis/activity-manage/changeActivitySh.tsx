import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon, Permission } from "pao-aop";
import React from "react";
import { Card, Row, Button, message, Spin, Steps, Select, Col, Descriptions, } from "antd";
import { request } from "src/business/util_tool";
import { activity_step } from "./changeActivityPublish";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";
import { ActivityInfo } from "./activityInfo";
import "./index.less";
/**
 * 组件：活动审核状态
 */
export class ChangeActivityShState {
    activity_info?: any;
    reason?: any;
    is_send?: boolean;
    action_value?: string;
}

/**
 * 组件：活动审核
 * 活动审核
 */
export class ChangeActivitySh extends React.Component<ChangeActivityShViewControl, ChangeActivityShState> {
    constructor(props: ChangeActivityShViewControl) {
        super(props);
        this.state = {
            action_value: '',
            reason: '',
            activity_info: [],
            is_send: false,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.activity_service.get_activity_pure_list_all!({ id: this.props.match!.params.key }))
                .then((data: any) => {
                    if (data && data.result && data.result.length > 0) {
                        this.setState({
                            activity_info: data.result[0],
                        });
                    }
                });
        }
    }
    back() {
        history.back();
    }
    change(e: any) {
        this.setState({
            action_value: e,
        });
    }
    save() {
        let { activity_info, reason, is_send, action_value } = this.state;
        if (!activity_info.hasOwnProperty('id')) {
            message.info('没有找到可操作的数据！');
            return;
        }
        if (!action_value) {
            message.info('请选择审核结果！');
            return;
        }
        if (action_value !== '1' && reason === '') {
            message.info('不通过请填写原因！');
            return;
        }
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.activity_service.update_activity!({ id: activity_info.id, action: "sh", action_value: action_value, reason: reason }))
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('操作成功！', 1, () => {
                                history.back();
                            });
                        } else {
                            this.setState({
                                is_send: false
                            });
                            message.info('操作失败！');
                        }
                    });
            }
        );
    }
    setRejectReason(e: any) {
        this.setState({
            reason: e.target.value,
        });
    }
    render() {
        let { activity_info } = this.state;

        // 初始化，抑制报错
        if (!activity_info) {
            activity_info = [];
        }

        const { Step } = Steps;
        const { Option } = Select;

        let step = 0;
        if (activity_info.status === '不通过') {
            step = 2;
        } else if (activity_info.status === '通过') {
            step = 2;
        } else if (activity_info.status === '待审批') {
            step = 1;
        }
        return (
            <MainContent>
                <Steps current={step} style={{ background: "white", padding: "25px" }}>
                    {activity_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} />
                        );
                    })}
                </Steps>
                {activity_info.hasOwnProperty('id') ? <Row>
                    <Card className="shenhe-card">
                        <ActivityInfo activity_info={activity_info} />
                    </Card>
                    {activity_info.status === '待审批' ? <MainCard>
                        <Row>
                            <Col span={3}>
                                <Select style={{ width: '90%' }} placeholder="请选择审核结果" onChange={(e: any) => this.change(e)}>
                                    <Option value="1">通过</Option>
                                    <Option value="2">不通过</Option>
                                    {/* <Option value="3">打回</Option> */}
                                </Select>
                            </Col>
                            <Col span={21}>
                                <TextArea placeholder="原因" rows={4} onChange={(e) => { this.setRejectReason(e); }} />
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button disabled={this.state.is_send} type='primary' onClick={() => this.save()}>保存</Button>
                            <Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                        </Row>
                    </MainCard> : <Row>
                            <Card>
                                <Descriptions title="审核信息" bordered={true}>
                                    <Descriptions.Item label="审核结果">{activity_info['status'] || ''}</Descriptions.Item>
                                    {activity_info.status === '不通过' ? <Descriptions.Item label="不通过原因">{activity_info['reason'] || ''}</Descriptions.Item> : null}
                                    <Descriptions.Item label="审核时间">{activity_info['audit_date'] || ''}</Descriptions.Item>
                                </Descriptions>
                            </Card>
                            <MainCard>
                                <Row type="flex" justify="center" className="ctrl-btns">
                                    <Button type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                                </Row>
                            </MainCard>
                        </Row>}
                </Row> : <Row type="flex" justify="center">
                        <Spin spinning={true} tip={"加载中"} size={"large"} /></Row>}
            </MainContent>
        );
    }
}

/**
 * 控件：活动审核控制器
 * 活动审核
 */
@addon('ChangeActivitySh', '活动审核', '活动审核')
@reactControl(ChangeActivitySh, true)
export class ChangeActivityShViewControl extends BaseReactElementControl {
    /** 视图权限 */
    public permission?: Permission;
}