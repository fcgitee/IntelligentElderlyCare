import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { Table } from "antd";
// import { Table, Row, Card, Button } from "antd";
// import { ROUTE_PATH } from 'src/projects/router';
// import { Steps, Descriptions, Result, Card } from 'antd';
// import { request } from "src/business/util_tool";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { DatePicker } from 'antd';
// import moment from 'moment';
// const { MonthPicker } = DatePicker;
// const monthFormat = 'YYYY-MM';

/**
 * 组件：活动信息汇总页面状态
 */
export interface ActivityInformationState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    // 详情
    data_info?: any;
    // 活动列表
    activity_list?: any;
    // 活动列表数量
    activity_list_count?: number;
    // 报名总人数
    participate_sum?: number;
    // 签到数量
    signin_sum?: number;
    // 评论数量
    comment_sum?: number;
    // 当前月份
    now_month?: string;
}

/**
 * 组件：活动信息汇总页面
 * 活动信息汇总页面
 */
export class ActivityInformation extends ReactView<ActivityInformationViewControl, ActivityInformationState> {
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
            data_info: [],
            activity_list: [],
            activity_list_count: 0,
            participate_sum: 0,
            signin_sum: 0,
            comment_sum: 0,
            now_month: '',
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        // this.getMonthData();
    }
    // 变更月份获取数据的封装
    // getMonthData() {
    //     let now_month = this.state.now_month || this.getNowMonth();
    //     let param: any = {
    //         month: now_month,
    //     };
    //     // 汇总信息
    //     request(this, AppServiceUtility.activity_service.get_activity_information_list_all!(param, 1, 10))
    //         .then((datas: any) => {
    //             this.setState({

    //                 activity_list_count: datas['activity_list'] && datas['activity_list']['total'] ? datas['activity_list']['total'] : 0,
    //                 // 
    //                 activity_list: datas['activity_list'] && datas['activity_list']['result'] ? datas['activity_list']['result'] : [],
    //                 // 
    //                 signin_sum: datas['signin_list'] && datas['signin_list']['total'] ? datas['signin_list']['total'] : 0,
    //                 // 
    //                 participate_sum: datas['participate_list'] && datas['participate_list']['total'] ? datas['participate_list']['total'] : 0,
    //                 // 
    //                 comment_sum: datas['comment_list'] && datas['comment_list']['total'] ? datas['comment_list']['total'] : 0,
    //             });
    //         });
    // }
    // // 得到年份/月份
    // getNowMonth() {
    //     return new Date().getFullYear() + '-' + (new Date().getMonth() + 1);
    // }
    // // 月份变更
    // changeMonth(e: any, values: any) {
    //     this.setState({
    //         now_month: values,
    //     });
    // }
    // render() {
    //     const columns: any = [
    //         {
    //             title: '当月活动数量',
    //             dataIndex: 'activity_list_count',
    //             key: 'activity_list_count',
    //             width: 120,
    //             align: 'center'

    //         },
    //         {
    //             title: '当月报名人数',
    //             dataIndex: 'participate_sum',
    //             key: 'participate_sum',
    //             width: 120,
    //             align: 'center'
    //         },
    //         {
    //             title: '当月签到数量',
    //             dataIndex: 'signin_sum',
    //             key: 'signin_sum',
    //             width: 120,
    //             align: 'center'
    //         },
    //         {
    //             title: '当月评论人数',
    //             dataIndex: 'comment_sum',
    //             key: 'comment_sum',
    //             width: 120,
    //             align: 'center'
    //         },
    //     ];
    //     const { activity_list_count, activity_list, participate_sum, signin_sum, comment_sum } = this.state;

    //     const data = [];
    //     data.push({
    //         key: 1,
    //         activity_list_count: activity_list_count,
    //         participate_sum: participate_sum,
    //         signin_sum: signin_sum,
    //         comment_sum: comment_sum,
    //     });
    //     let activity_cfg: any = [
    //         {
    //             title: '活动名称',
    //             dataIndex: 'activity_name',
    //             key: 'activity_name',
    //             width: 120,
    //             align: 'center'
    //         },
    //         {
    //             title: '活动场地',
    //             dataIndex: 'address',
    //             key: 'address',
    //             width: 120,
    //             align: 'center'
    //         },
    //         {
    //             title: '开始时间',
    //             dataIndex: 'begin_date',
    //             key: 'begin_date',
    //             width: 120,
    //             align: 'center'
    //         },
    //         {
    //             title: '开始时间',
    //             dataIndex: 'end_date',
    //             key: 'end_date',
    //             width: 120,
    //             align: 'center'
    //         },
    //     ];
    //     return (
    //         <MainContent>
    //             <Card>
    //                 选择月份：
    //                 <MonthPicker defaultValue={moment(this.getNowMonth(), monthFormat)} format={monthFormat} onChange={(e, values) => this.changeMonth(e, values)} />
    //                 <Button type="primary" onClick={() => this.getMonthData()}>查询</Button>
    //             </Card>
    //             <Card>
    //                 <Row><strong>当月活动信息汇总</strong></Row>
    //                 <br />
    //                 <Table
    //                     columns={columns}
    //                     dataSource={data}
    //                     bordered={true}
    //                     size="middle"
    //                     pagination={false}
    //                 />
    //             </Card>
    //             <Card>
    //                 <Row><strong>当月活动信息</strong></Row>
    //                 <br />
    //                 <Table pagination={false} columns={activity_cfg} dataSource={activity_list} />
    //             </Card>
    //         </MainContent>
    //     );
    // }
    get_number(num: number, flag = false) {
        if (flag === true) {
            return this.randomFrom(4, 80);
        }
        return this.randomFrom(4, 80) + '%';
    }
    randomFrom(lowerValue: any, upperValue: any) {
        return Math.floor(Math.random() * (upperValue - lowerValue + 1) + lowerValue);
    }
    format() {
        var myDate = new Date();
        let Y: any = myDate.getFullYear() + '-';
        let M: any = (myDate.getMonth() + 1 < 10 ? '0' + (myDate.getMonth() + 1) : myDate.getMonth() + 1) + '-';
        let D: any = myDate.getDate() + ' ';
        let h: any = myDate.getHours() + ':';
        let m: any = myDate.getMinutes() + ':';
        let s: any = myDate.getSeconds();
        return Y + M + D + h + m + s;
    }
    render() {
        const columns: any = [
            {
                title: '类型',
                width: 150,
                dataIndex: 'type',
                key: 'type',
                // fixed: 'left',
                align: 'left'
            },
            {
                title: '项目',
                width: 150,
                dataIndex: 'name',
                key: 'name',
                // fixed: 'left',
                align: 'left'
            },
            {
                title: '数值',
                width: 150,
                dataIndex: 'shuzhi',
                key: 'shuzhi',
                align: 'center'
            },
            {
                title: '上月同比变化率',
                width: 150,
                dataIndex: 'sytbbhl',
                key: 'sytbbhl',
                align: 'center'
            },
            {
                title: '上半年同比变化率',
                width: 150,
                dataIndex: 'sbntbbhl',
                key: 'sbntbbhl',
                align: 'center'
            },
            {
                title: '上年同比变化率',
                width: 150,
                dataIndex: 'sntbbhl',
                key: 'sntbbhl',
                align: 'center'
            },
            {
                title: '统计时间',
                width: 150,
                dataIndex: 'tjsj',
                key: 'tjsj',
                align: 'center',
            }
        ];

        let Zh: any = [{
            'type': '基础',
            'childs': [
                '幸福院挂牌总数',
                '幸福院挂牌通过验收总数',
                '自运营幸福院总数',
                '社工机构总数',
                'APP流量总数',
                '幸福院覆盖率',
                '志愿者工作站总数',
                '幸福小站总数',
                '社区长者饭堂总数',
            ]
        }, {
            'type': '评比',
            'childs': [
                '幸福院挂牌参与评比总数',
                '幸福院评比达到优良总数',
            ]
        }, {
            'type': '运营',
            'childs': [
                '幸福院设备使用总人次',
                '幸福院工作人员总数',
                '持证工作人员总数',
                '幸福院工作人员中专以上学历总数',
                '投诉总次数',
                '幸福院活动组织总数',
                '幸福院参加活动总人数',
                '幸福院APP活动发布总数',
                '志愿者总数',
                '志愿者总时长',
                '幸福小站商品类别总数',
                '社区长者饭堂用餐总数',
            ]
        }, {
            'type': '收支',
            'childs': [
                '幸福院建设资助总额',
                '幸福院评比奖励总额',
                '平台捐赠总额',
                '平台受赠总额',
                '交卖商城成交总数',
                '幸福小站交易总额',
                '长者饭堂补贴总额',
            ]
        }];

        let data: any = [];

        Zh.map((item: any, index: number) => {
            var type = false;
            item.childs.map((itm: any, idx: number) => {
                data.push({
                    type: type === false ? item.type : '',
                    name: itm,
                    shuzhi: this.get_number(10000, true),
                    sytbbhl: this.get_number(10000),
                    sbntbbhl: this.get_number(10000),
                    sntbbhl: this.get_number(10000),
                    tjsj: this.format(),
                });
                if (type === false) {
                    type = true;
                }
            });
        });
        // console.log(data);
        return (
            <MainContent>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    rowKey="name"
                    pagination={false}
                    scroll={{ y: '650px' }}
                /></MainContent>
        );
    }
}

/**
 * 控件：活动信息汇总页面
 * 活动信息汇总页面
 */
@addon('ActivityInformation', '活动信息汇总页面', '活动信息汇总页面')
@reactControl(ActivityInformation, true)
export class ActivityInformationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}