import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { Select } from "antd";
let { Option } = Select;

/**
 * 组件：活动室预约列表状态
 */
export interface ActivityRoomReservationViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    // 活动室列表
    room_list?: any;
}

/**
 * 组件：活动室预约列表
 * 描述
 */
export class ActivityRoomReservationView extends ReactView<ActivityRoomReservationViewControl, ActivityRoomReservationViewState> {
    private table_params = {
        other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    private columns_data_source = [{
        title: '活动室名称',
        dataIndex: 'activity_room_name',
        key: 'activity_room_name',
    }, {
        title: '预约联系人',
        dataIndex: 'contacts',
        key: 'contacts',
    }, {
        title: '联系方式',
        dataIndex: 'phone',
        key: 'phone',
    }, {
        title: '预约人数',
        dataIndex: 'reservate_quantity',
        key: 'reservate_quantity',
    }, {
        title: '预约日期',
        dataIndex: 'reservate_datestr',
        key: 'reservate_datestr',
    }];
    constructor(props: ActivityRoomReservationViewControl) {
        super(props);
        this.state = {
            room_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeActivityRoomReservation);
    }
    componentDidMount() {
        // 活动室详情
        request(this, AppServiceUtility.activity_service.get_activity_room_list!({}, 1, 99))
            .then((datas: any) => {
                this.setState({
                    room_list: datas.result ? datas.result : [],
                });
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeActivityRoomReservation + '/' + contents.id);
        } else if ('icon_align_center' === type) {
            this.props.history!.push(ROUTE_PATH.changeActivityRoomReservation + '/' + contents.id);
        }
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.select,
                label: "活动室",
                decorator_id: "reservate_room_id",
                option: {
                    placeholder: "请选择活动室",
                    childrens: this.state.room_list.map((item: any) => {
                        return (
                            <Option key={item.id} value={item.id}>{item.activity_room_name}</Option>
                        );
                    }),
                }
            }],
            btn_props: [{
                label: '预约',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                delete: {
                    service_func: 'del_activity_room_reservation'
                }
            },
            searchExtraParam: this.props.select_param,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let select_param = this.props.select_param;
        let select_param_type = select_param['type'] ? select_param['type'] : '';
        if (select_param_type === 'record') {
            this.table_params.other_label_type = [{ type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }];
        }
        let service_item_list = Object.assign(service_item, this.table_params);
        // delete service_item_list.other_label_type;
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 活动室预约列表页面
 */
@addon('ActivityRoomReservationView', '活动室预约列表页面', '描述')
@reactControl(ActivityRoomReservationView, true)
export class ActivityRoomReservationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    // 带餐
    public select_param?: any;
} 