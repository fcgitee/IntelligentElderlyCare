import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 状态：活动类型新增页面
 */
export interface ChangeActivityTypeViewState extends ReactViewState {
}

/**
 * 组件：活动类型新增页面视图
 */
export class ChangeActivityTypeView extends ReactView<ChangeActivityTypeViewControl, ChangeActivityTypeViewState> {
    constructor(props: ChangeActivityTypeViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let edit_props = {
            form_items_props: [{
                title: "活动类型信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "活动类型名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入活动类型名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动类型名称"
                        }
                    }, {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remarks",
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            rows: 3
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    this.props.history!.push(ROUTE_PATH.activityTypeManage);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.activity_service,
                operation_option: {
                    query: {
                        func_name: "get_activity_type_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_activity_type"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.activityTypeManage); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：活动类型新增页面控件
 * @description 活动类型新增页面控件
 * @author
 */
@addon('ChangeActivityTypeViewControl', '活动类型新增页面控件', '活动类型新增页面控件')
@reactControl(ChangeActivityTypeView, true)
export class ChangeActivityTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}