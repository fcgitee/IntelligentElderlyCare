import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { ROUTE_PATH } from "src/projects/router";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
/**
 * 状态：活动报名新增页面
 */
export interface ChangeActivityParticipateViewState extends ReactViewState {
}

/**
 * 组件：活动报名新增页面视图
 */
export class ChangeActivityParticipateView extends ReactView<ChangeActivityParticipateViewControl, ChangeActivityParticipateViewState> {
    constructor(props: ChangeActivityParticipateViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let elder_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            }, {
                title: '性别',
                dataIndex: 'sex',
                key: 'sex',
            }, {
                title: '出生年月',
                dataIndex: 'birth_date',
                key: 'birth_date',
            }],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_all_elder_list',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let activity_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "活动名称",
                    decorator_id: "activity_name"
                },
            ],
            columns_data_source: [{
                title: '活动名称',
                dataIndex: 'activity_name',
                key: 'activity_name',
            }, {
                title: '报名人数/最大报名人数',
                dataIndex: 'cc1',
                key: 'cc1',
                render: (text: any, record: any) => {
                    return `${record.participate_count}/${record.max_quantity}`;
                }
            }, {
                title: '开始时间',
                dataIndex: 'begin_date',
                key: 'begin_date',
            }, {
                title: '结束时间',
                dataIndex: 'end_date',
                key: 'end_date',
            }],
            service_name: AppServiceUtility.activity_service,
            service_func: 'get_activity_pure_list_all',
            title: '活动查询',
            name_field: 'activity_name',
            service_option: [{
                status: '通过'
            }],
            select_option: {
                placeholder: "请选择活动",
            }
        };
        let edit_props = {
            form_items_props: [{
                title: "活动报名",
                need_card: true,
                input_props: [
                    {
                        type: InputType.modal_search,
                        label: "长者",
                        decorator_id: 'elder_id',
                        field_decorator_option: {
                            rules: [{ message: "请选择长者", required: true }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择长者",
                            modal_search_items_props: elder_props
                        },
                    },
                    {
                        type: InputType.modal_search,
                        label: "报名活动",
                        decorator_id: 'activity_id',
                        field_decorator_option: {
                            rules: [{ message: "请选择活动", required: true }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择活动",
                            modal_search_items_props: activity_props
                        },
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remarks",
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            rows: 3
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    this.props.history!.push(ROUTE_PATH.activityParticipate);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.activity_service,
                operation_option: {
                    query: {
                        func_name: "get_activity_participate_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "participate_activity_pc"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.activityParticipate); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：活动报名新增页面控件
 * @description 活动报名新增页面控件
 * @author
 */
@addon('ChangeActivityParticipateViewControl', '活动报名新增页面控件', '活动报名新增页面控件')
@reactControl(ChangeActivityParticipateView, true)
export class ChangeActivityParticipateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}