import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：活动签到列表状态
 */
export interface ActivitySignInViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：活动签到列表
 * 描述
 */
export class ActivitySignInView extends ReactView<ActivitySignInViewControl, ActivitySignInViewState> {
    private columns_data_source = [{
        title: '活动名称',
        dataIndex: 'activity_name',
        key: 'activity_name',
    }, {
        title: '签到人',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '签到人性别',
        dataIndex: 'user_sex',
        key: 'user_sex',
    }, {
        title: '签到人联系方式',
        dataIndex: 'user_telephone',
        key: 'user_telephone',
    }, {
        title: '签到人地址',
        dataIndex: 'user_address',
        key: 'user_address',
    }, {
        title: '签到时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }];
    constructor(props: ActivitySignInViewControl) {
        super(props);
        this.state = {
            org_list: []
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeActivityManage + '/' + contents.id);
        }
    }
    componentDidMount() {
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "活动名称",
                decorator_id: "activity_name",
                option: {
                    placeholder: "请输入活动名称",
                }
            },
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构",
                //     }
                // }
            ],
            // btn_props: [{
            //     label: '',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                delete: {
                    service_func: 'delete_activity_signin'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        delete service_item_list.other_label_type;
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 活动签到列表页面
 */
@addon('ActivitySignInView', '活动签到列表页面', '描述')
@reactControl(ActivitySignInView, true)
export class ActivitySignInViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 