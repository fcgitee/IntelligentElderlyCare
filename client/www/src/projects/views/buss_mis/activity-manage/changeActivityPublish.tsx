import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info, beforeUpload, beforeUpload2, getAge } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView, CookieUtil } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import Form, { GetFieldDecoratorOptions } from "antd/lib/form/Form";
// import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { Select, Steps, message, Table, Col, Row, Card, Button, Input, Icon, Modal, Checkbox, Radio, Progress } from 'antd';
let { Option } = Select;
import moment from 'moment';
import { remote } from "src/projects/remote";
import { checkMin1 } from './validate';
import "./index.less";
import TextArea from "antd/lib/input/TextArea";
import { User } from "src/business/models/user";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
const { Step } = Steps;

const returnEnIndex = (index: number) => {
    let char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (index < 27) {
        return char[index];
    } else {
        return 'Z' + index;
    }
};

export const activity_step = [{
    'step_name': '发布',
}, {
    'step_name': '审核',
}, {
    'step_name': '审核完成',
}];
class MySecondForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
    }
    handleFeedbackSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['feedback_content'] = values['feedback_content'];
                formValue['pureEdit'] = true;
                formValue['id'] = this.props.activity_info['id'];
                request(this, AppServiceUtility.activity_service.update_activity!(formValue))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                window.location.reload();
                            });
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { activity_info, formItemLayout, cookie_name } = this.props;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleFeedbackSubmit}>
                    <MainContent>
                        <Row className="mb20">互动【收藏数:】<span className="activity_bold">{activity_info.collection_count || 0}</span></Row>
                        <Form.Item label='总结：'>
                            {getFieldDecorator('feedback_content', {
                                initialValue: activity_info['feedback_content'] || '',
                                rules: [{
                                    required: false,
                                    message: '请输入总结正文，限1000字'
                                }],
                            })(
                                <TextArea rows={4} />
                            )}
                        </Form.Item>
                        <Form.Item label='编辑者：'>
                            {getFieldDecorator('editor', {
                                initialValue: cookie_name,
                                rules: [{
                                    required: true,
                                    message: '请输入编辑者'
                                }],
                            })(
                                <Input autoComplete="off" placeholder="请输入编辑者" />
                            )}
                        </Form.Item>
                    </MainContent>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                    </Row>
                </Form>
            </Row>
        );
    }
}
const Form2 = Form.create<any>()(MySecondForm);

class MyThirdForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
    }
    copy(content: any) {
        this.props.form.setFieldsValue({
            'content': content,
        });
    }
    handleNewsCreate = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let param: any = {};
                param['content'] = values['template'];
                param['title'] = values['title'];
                param['subhead'] = values['subhead'];
                param['editor'] = values['editor'];
                param['author'] = values['editor'];
                param['like'] = 0;
                param['share'] = 0;
                param['app_img_list'] = [];
                request(this, AppServiceUtility.article_service.update_news!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功，请等待审核！', 1, () => {
                                window.location.reload();
                            });
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { activity_info, activity_signin_list, formItemLayout, cookie_name } = this.props;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleNewsCreate}>
                    <MainContent>
                        <Form.Item label='标题：'>
                            {getFieldDecorator('title', {
                                initialValue: activity_info.activity_name || '',
                                rules: [{
                                    required: true,
                                    message: '请输入标题'
                                }],
                            })(
                                <Input autoComplete="off" placeholder="请输入标题" />
                            )}
                        </Form.Item>
                        <Form.Item label='副标题：'>
                            {getFieldDecorator('subhead', {
                                initialValue: '',
                                rules: [{
                                    required: false,
                                    message: ''
                                }],
                            })(
                                <Input autoComplete="off" placeholder="请输入副标题" />
                            )}
                        </Form.Item>
                        <Form.Item label='参考模板：'>
                            {getFieldDecorator('template', {
                                initialValue: `${activity_info.create_date.slice(0, 10)}，${activity_info.organization_name}开展了“${activity_info.activity_name}”的活动，有${activity_signin_list.length}位长者参与该次活动。`,
                                rules: [{
                                    required: false,
                                    message: ''
                                }],
                            })(
                                <TextArea rows={4} disabled={true} />
                            )}
                        </Form.Item>
                        <Row type='flex' justify='end' className="mb20">
                            <Button type="primary" onClick={() => this.copy(`${activity_info.create_date.slice(0, 10)}，${activity_info.organization_name}开展了“${activity_info.activity_name}”的活动，有${activity_signin_list.length}位长者参与该次活动。`)}>复制到正文</Button>
                        </Row>
                        <Form.Item label='正文：'>
                            {getFieldDecorator('content', {
                                initialValue: this.state.news_content || '',
                                rules: [{
                                    required: true,
                                    message: '请输入总结正文，限1000字'
                                }],
                            })(
                                <TextArea rows={4} />
                            )}
                        </Form.Item>
                        <Form.Item label='编辑者：'>
                            {getFieldDecorator('editor', {
                                initialValue: cookie_name,
                                rules: [{
                                    required: true,
                                    message: '请输入编辑者'
                                }],
                            })(
                                <Input autoComplete="off" placeholder="请输入编辑者" />
                            )}
                        </Form.Item>
                    </MainContent>
                    <Row type='flex' justify='center'>
                        <Button htmlType='submit' type='primary'>保存</Button>
                    </Row>
                </Form>
            </Row>
        );
    }
}
const Form3 = Form.create<any>()(MyThirdForm);

class MyFourthForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_send: false,
        };
    }
    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this);
        }
    }
    handleDCWJSubmit = (e: any) => {
        e && e.preventDefault();
        const { form, activity_info, close } = this.props;
        let { is_send } = this.state;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                if (is_send) {
                    return;
                }
                let param = {};
                // 业务ID
                param['business_id'] = activity_info['id'];
                // 业务类型
                param['type'] = 'activity';
                param['result'] = values;
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.activity_service.update_questionnaire!(param))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    this.setState({
                                        is_send: false,
                                    });
                                    message.info('录入成功', 1, () => {
                                        this.props.form.resetFields();
                                        if (close) {
                                            close();
                                        }
                                    });
                                }
                            })
                            .catch((error: any) => {
                                message.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { activity_info, previewing, question_list } = this.props;
        return (
            <Row>
                <Form onSubmit={this.handleDCWJSubmit} style={previewing ? { pointerEvents: 'none' } : {}}>
                    <Row className="mb20" type="flex" justify="center">{activity_info.activity_name}调查问卷</Row>
                    {question_list.map((item: any, index: number) => {
                        // let isBreak = index > 0 && index % 5 === 0 ? true : false;
                        if (item.type === '单选') {
                            return (
                                <Row key={item.key} style={{ pageBreakAfter: 'auto' }}>
                                    <Form.Item label={`${index + 1}. ${item.value}`} key={item.key} className={item.hasOther ? 'mb0' : ""}>
                                        {getFieldDecorator(item.key, {
                                            initialValue: '',
                                            rules: [{
                                                required: !previewing,
                                                message: `${item.value}不能为空`
                                            }],
                                        })(
                                            <Radio.Group>
                                                {item.options!.map((itm: any, idx: any) => {
                                                    return <Radio key={itm.key} value={itm.value}>{returnEnIndex(idx)} {itm.value}</Radio>;
                                                })}
                                            </Radio.Group>
                                        )}
                                    </Form.Item>
                                    {item.hasOther ? <Form.Item label="其他" key={`${item.key}_其他`}>
                                        {getFieldDecorator(item.key + '_other', {
                                            initialValue: '',
                                        })(
                                            <TextArea rows={1} />
                                        )}
                                    </Form.Item> : null}
                                </Row>
                            );
                        } else if (item.type === '多选') {
                            return (
                                <Row key={item.key} style={{ pageBreakAfter: 'auto' }}>
                                    <Form.Item label={`${index + 1}. ${item.value}`} key={item.key} className={item.hasOther ? 'mb0' : ""}>
                                        {getFieldDecorator(item.key, {
                                            initialValue: [],
                                            rules: [{
                                                required: !previewing,
                                                message: `${item.value}不能为空`
                                            }],
                                        })(
                                            <Checkbox.Group>
                                                {item.options!.map((itm: any, idx: any) => {
                                                    return <Checkbox key={itm.key} value={itm.value}>{returnEnIndex(idx)} {itm.value}</Checkbox>;
                                                })}
                                            </Checkbox.Group>
                                        )}
                                    </Form.Item>
                                    {item.hasOther ? <Form.Item label="其他" key={`${item.key}_其他`}>
                                        {getFieldDecorator(item.key + '_other', {
                                            initialValue: '',
                                        })(
                                            <TextArea rows={1} />
                                        )}
                                    </Form.Item> : null}
                                </Row>
                            );
                        } else if (item.type === '填空') {
                            return (
                                <Row key={item.key} style={{ pageBreakAfter: 'auto' }}>
                                    <Form.Item label={`${index + 1}. ${item.value}（填空，${item.options}）`} key={item.key}>
                                        {getFieldDecorator(item.key, {
                                            initialValue: '',
                                            rules: [{
                                                required: !previewing,
                                                message: `${item.value}不能为空`
                                            }],
                                        })(
                                            <TextArea rows={4} />
                                        )}
                                    </Form.Item>
                                </Row>
                            );
                        }
                        return null;
                    })}
                </Form>
            </Row>
        );
    }
}
const Form4 = Form.create<any>()(MyFourthForm);

/**
 * 状态：活动新增页面
 */
export interface ChangeActivityPublishViewState extends ReactViewState {
    /** 服务商列表 */
    provider_list?: [];
    // 团体列表
    groups_list?: [];
    // 满意度问题
    question_list?: any;
    // 调查问卷列表
    questionnaire_list?: any;
    default_questionnaire_list?: any;
    questionnaire_modal?: boolean;
    default_question_list?: any;
    /** 活动类型列表 */
    activity_type_list?: [];
    /** 活动室 */
    activity_room_list?: [];
    // 未签到列表
    notSignInUserList?: any;
    // 已签到列表
    signInUserList?: any;
    // 签到人数
    activity_signin_list?: any;
    // 审批内容
    sp_msg?: string;
    // 是否发送
    is_send?: boolean;
    // 发布步骤
    activity_step: any;
    endOpen?: any;
    begin_date?: any;
    end_date?: any;
    // 
    hdme_disable?: any;
    activity_room?: any;
    // 活动详情
    activity_info?: any;
    // 资讯正文
    news_content?: any;
    // 预览
    preview_modal?: boolean;
    isPrinting?: boolean;
    previewing?: boolean;
    is_has_draft?: boolean;
    draft_info?: any;
}

/**
 * 组件：活动新增页面视图
 */
export class ChangeActivityPublishView extends ReactView<ChangeActivityPublishViewControl, ChangeActivityPublishViewState> {
    private rand: any = [];
    private formCreator: any = null;
    private formCreator2: any = null;
    private typeOptions = ['单选', '多选', '填空'];
    private textareaOptions = ['限20字', '限50字', '限100字'];
    constructor(props: ChangeActivityPublishViewControl) {
        super(props);
        this.state = {
            provider_list: [],
            question_list: [],
            questionnaire_list: [],
            questionnaire_modal: false,
            default_question_list: [],
            groups_list: [],
            activity_info: [],
            activity_type_list: [],
            activity_room_list: [],
            signInUserList: [],
            notSignInUserList: [],
            activity_signin_list: [],
            sp_msg: '',
            is_send: false,
            endOpen: false,
            begin_date: false,
            end_date: false,
            hdme_disable: false,
            preview_modal: false,
            isPrinting: false,
            previewing: false,
            news_content: '',
            activity_step: activity_step,
            draft_info: {},
            is_has_draft: false,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.activity_service.get_activity_pure_list!({ id: this.props.match!.params.key }, 1, 1))
                .then((datas: any) => {
                    let question_list = datas.result[0].hasOwnProperty('question') ? datas.result[0]['question'] : [];
                    this.setState(
                        {
                            activity_info: datas.result[0],
                            question_list: question_list,
                            default_question_list: JSON.parse(JSON.stringify(question_list)),
                        },
                        () => {
                            // 有id和通过，获取签到列表和报名列表
                            if (status === '通过') {
                                request(this, AppServiceUtility.activity_service.get_activity_participate_list!({ activity_id: this.props.match!.params.key }))
                                    .then((datas: any) => {
                                        let activity_participate_list = datas.result;
                                        request(this, AppServiceUtility.activity_service.get_activity_sign_in_list!({ activity_id: this.props.match!.params.key }))
                                            .then((datas: any) => {
                                                let notSignInUser: any = [];
                                                let signInUser: any = [];
                                                let signInTempUser: any = {};
                                                if (datas && datas.result && datas.result.length > 0) {
                                                    // 把签到的人列为方便查找的
                                                    for (let i = 0; i < datas.result.length; i++) {
                                                        signInTempUser[datas.result[i]['user_id']] = true;
                                                    }
                                                }
                                                // 找出报名了但是未签到的人
                                                for (let j = 0; j < activity_participate_list.length; j++) {
                                                    if (!signInTempUser[activity_participate_list[j]['user_id']]) {
                                                        notSignInUser.push(activity_participate_list[j]);
                                                    } else {
                                                        signInUser.push(activity_participate_list[j]);
                                                    }
                                                }
                                                this.setState({
                                                    notSignInUserList: notSignInUser,
                                                    signInUserList: signInUser,
                                                    activity_signin_list: datas.result,
                                                });
                                            });
                                    });
                            }
                        }
                    );
                });

        } else {
            // 草稿列表
            request(this, AppServiceUtility.person_org_manage_service.get_draft_list!({ type: 'activity' }))
                .then((data: any) => {
                    if (data && data.result && data.result.length) {
                        this.setState({
                            draft_info: data.result[0],
                            is_has_draft: true
                        });
                    }
                });
        }
        // 活动类型
        request(this, AppServiceUtility.activity_service.get_activity_type_list_all!({}))
            .then((datas: any) => {
                this.setState({
                    activity_type_list: datas.result,
                });
            });
        // 团体列表
        request(this, AppServiceUtility.person_org_manage_service.get_groups_list!({}))
            .then((datas: any) => {
                this.setState({
                    groups_list: datas.result,
                });
            });
        // 获取活动室
        request(this, AppServiceUtility.activity_service.get_activity_room_list!({ get_now_org_id: true })!)
            .then((datas: any) => {
                this.setState({
                    activity_room_list: datas.result,
                });
            });
    }
    getDataValue(value: string) {
        // 此时的value为moment可以直接进行装换
        value = moment(value).format('YYYY-MM-DD HH:mm:ss'); // 转换 
    }
    // changeOrganization(e: any) {
    //     // 获取活动室
    //     request(this, AppServiceUtility.activity_service.get_activity_room_list!({ organization_id: e }, 1, 99)!)
    //         .then((datas: any) => {
    //             this.setState({
    //                 activity_room_list: datas.result,
    //             });
    //         });
    // }
    handleStartOpenChange = (open: any) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    }
    handleEndOpenChange = (open: any) => {
        this.setState({ endOpen: open });
    }
    onBeginChange(value: any) {
        this.setState({
            'begin_date': value,
        });
    }
    onEndChange(value: any) {
        this.setState({
            'end_date': value,
        });
    }
    disabledEndDate(endValue: any) {
        const { begin_date } = this.state;
        if (!endValue || !begin_date) {
            return false;
        }
        // 如果是同一日，就特别点判断
        if (endValue.format('YYYY-MM-DD') === begin_date.format('YYYY-MM-DD')) {
            return false;
        }
        return endValue.valueOf() <= begin_date.valueOf();
    }
    hdsChange(e: any) {
        let activity_room_list = this.state.activity_room_list;
        for (let i = 0; i < activity_room_list!.length; i++) {
            if (activity_room_list![i]['id'] === e) {
                this.setState({
                    activity_room: activity_room_list![i],
                });
                if (this.state.hdme_disable === true) {
                    this.formCreator.setChildFieldsValue({
                        'max_quantity': activity_room_list![i]['max_quantity'],
                    });
                }
                break;
            }
        }
    }
    hdmeChange(e: any) {
        if (e.target.value === '限定名额') {
            this.setState({
                hdme_disable: false,
            });
            this.formCreator.setChildFieldsValue({
                'max_quantity': '',
            });
        } else if (e.target.value === '按活动室容纳人数') {
            if (!this.state.activity_room) {
                message.info('请选择活动室！');
                return;
            }
            this.setState({
                hdme_disable: true,
            });
            this.formCreator.setChildFieldsValue({
                'max_quantity': this.state.activity_room['max_quantity'],
            });
        }
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    onRef2 = (ref: any) => {
        this.formCreator2 = ref;
    }

    readDraft() {
        let draft_info = this.state.draft_info;
        draft_info.draft = draft_info.draft || {};
        this.formCreator.setChildFieldsValue({
            'activity_name': draft_info.draft.activity_name,
            'activity_type_id': draft_info.draft.activity_type_id,
            'photo': draft_info.draft.photo,
            'plan': draft_info.draft.plan,
            'group_id': draft_info.draft.group_id,
            'begin_date': moment(draft_info.draft.begin_date),
            'end_date': moment(draft_info.draft.end_date),
            'address': draft_info.draft.address,
            'activity_room_id': draft_info.draft.activity_room_id,
            'count_by': draft_info.draft.count_by,
            'max_quantity': draft_info.draft.max_quantity,
            'contacts': draft_info.draft.contacts,
            'phone': draft_info.draft.phone,
            'introduce': draft_info.draft.introduce,
        });
        message.info('读取草稿成功！');
    }
    returnPublishForm() {
        const activity_room_list: JSX.Element[] = this.state.activity_room_list!.map((item, idx) => <Option key={item['id']}>{item['activity_room_name']}</Option>);
        const activity_type_list: JSX.Element[] = this.state.activity_type_list!.map((item, idx) => <Option key={item['id']}>{item['name']}</Option>);
        const groups_list: JSX.Element[] = this.state.groups_list!.map((item, idx) => <Option key={item['id']}>{item['name']}</Option>);
        // let provider = this.state.provider_list;
        // let provider_list: any[] = [];
        // provider!.map((item, idx) => {
        //     provider_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });
        let layout = {
            labelCol: {
                xs: { span: 9 },
                md: { span: 9 },
                sm: { span: 9 },
            },
            wrapperCol: {
                xs: { span: 15 },
                md: { span: 15 },
                sm: { span: 15 },
            },
        };
        let edit_props = {
            form_items_props: [{
                title: "活动发布信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "活动名称",
                        decorator_id: "activity_name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入活动名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动名称",
                            autoComplete: 'off',
                        } as any
                    }, {
                        type: InputType.select,
                        label: "活动类型",
                        decorator_id: "activity_type_id",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择活动类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择活动类型",
                            childrens: activity_type_list,
                            autoComplete: 'off',
                        } as any
                    }, {
                        type: InputType.upload,
                        label: "活动图片（大小小于2M，格式支持jpg/png/jpeg）",
                        decorator_id: "photo",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请上传活动图片" }],
                        } as GetFieldDecoratorOptions,
                        layout: layout,
                        option: {
                            action: remote.upload_url,
                            beforeUpload: beforeUpload,
                        } as any
                    }, {
                        type: InputType.upload,
                        label: "活动计划（大小小于2M，格式支持doc/docx）",
                        layout: layout,
                        decorator_id: "plan",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请上传活动计划" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            action: remote.upload_url,
                            fileType: 'doc',
                            beforeUpload: (e: any) => beforeUpload2(e, ['doc', 'docx']),
                        } as any
                    },
                    // {
                    //     type: InputType.select,
                    //     label: "商家",
                    //     decorator_id: "organization_id",
                    //     field_decorator_option: {
                    //         rules: [{ required: true, message: "请选择商家" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请选择商家",
                    //         childrens: provider_list,
                    //         autoComplete: 'off',
                    //         onChange: (e: any) => this.changeOrganization(e),
                    //     }
                    // },
                    {
                        type: InputType.select,
                        label: "团体",
                        decorator_id: "group_id",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请选择团体活动" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择团体活动",
                            childrens: groups_list,
                            autoComplete: 'off',
                        } as any
                    },
                    {
                        type: InputType.date,
                        label: "活动开始时间",
                        decorator_id: "begin_date",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入活动开始时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动开始时间",
                            format: "YYYY-MM-DD HH:mm",
                            showTime: true,
                            autoComplete: 'off',
                            onChange: (e: any) => this.onBeginChange(e),
                            onOpenChange: (e: any) => this.handleStartOpenChange(e),
                        } as any
                    }, {
                        type: InputType.date,
                        label: "活动结束时间",
                        decorator_id: "end_date",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入活动结束时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动结束时间",
                            format: "YYYY-MM-DD HH:mm",
                            onOk: this.getDataValue,
                            showTime: true,
                            autoComplete: 'off',
                            open: this.state.endOpen,
                            onChange: (e: any) => this.onEndChange(e),
                            onOpenChange: (e: any) => this.handleEndOpenChange(e),
                            disabledDate: (e: any) => this.disabledEndDate(e)
                        } as any
                    }, {
                        type: InputType.antd_input,
                        label: "活动地点",
                        decorator_id: "address",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入活动地点" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动地点",
                            autoComplete: 'off',
                        } as any
                    }, {
                        type: InputType.select,
                        label: "活动场室",
                        decorator_id: "activity_room_id",
                        field_decorator_option: {
                            rules: [{
                                message: "请选择活动场室"
                            }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: activity_room_list.length === 0 ? "当前无可选活动室" : "请选择活动场室",
                            childrens: activity_room_list,
                            autoComplete: 'off',
                            disabled: activity_room_list.length === 0 ? true : false,
                            onChange: (e: any) => this.hdsChange(e)
                        } as any
                    }, {
                        type: InputType.radioGroup,
                        label: "活动名额",
                        decorator_id: "count_by",
                        field_decorator_option: {
                            rules: [{
                                message: "请选择活动名额",
                                required: true,
                            }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            autoComplete: 'off',
                            options: [{
                                label: '限定名额',
                                value: '限定名额',
                            }, {
                                label: '按活动室容纳人数',
                                value: '按活动室容纳人数',
                            }],
                            onChange: (e: any) => this.hdmeChange(e)
                        } as any
                    },
                    {
                        type: InputType.antd_input,
                        label: "最大参与人数",
                        decorator_id: "max_quantity",
                        field_decorator_option: {
                            rules: [{
                                required: true,
                                validator: (_: any, value: any, cb: any) => checkMin1(_, value, cb, '最大参与人数')
                            }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入最大参与人数",
                            autoComplete: 'off',
                            disabled: this.state.hdme_disable,
                        } as any
                    }, {
                        type: InputType.antd_input,
                        label: "联系人",
                        decorator_id: "contacts",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入联系人" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入联系人",
                            autoComplete: 'off',
                        } as any
                    }, {
                        type: InputType.antd_input,
                        label: "联系人电话",
                        decorator_id: "phone",
                        field_decorator_option: {
                            rules: [{
                                required: true,
                                // validator: (_: any, value: any, cb: any) => checkPhone(_, value, cb, '联系人电话')
                            }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入联系人电话",
                            autoComplete: 'off',
                        } as any
                    }, {
                        type: InputType.nt_rich_text,
                        label: "活动介绍",
                        decorator_id: "introduce",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入活动介绍" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入活动介绍",
                            autoComplete: 'off',
                            remoteUrl: remote.upload_url
                        } as any
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // this.props.history!.push(ROUTE_PATH.activityManage);
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "提交审核",
            },
            caogao_btn_propps: {
                text: "保存草稿",
                type: { 'type': '草稿' }
            },
            service_option: {
                service_object: AppServiceUtility.activity_service,
                operation_option: {
                    query: {
                        func_name: "get_activity_pure_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_activity"
                    }
                },
            },
            succ_func: () => {
                // this.props.history!.push(ROUTE_PATH.activityManage); 
                history.back();
            },
            err_func: (result: any) => {
                if (result && Array.isArray(result) && result.join) {
                    message.error('该活动室以下时间段已被其他活动预约：' + result.join("\r\n"));
                }
            },
            id: this.props.match!.params.key,
            onRef: this.onRef,
        };
        // if(){
        //     edit_props.form_items_props[0].input_props.push({
        //             type: InputType.antd_input,
        //             label: "审核原因",
        //             decorator_id: "reason",
        //             field_decorator_option: {
        //             } as GetFieldDecoratorOptions,
        //             option: {
        //                 disabled: true
        //             }
        //         });
        // }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <FormCreator {...edit_props_list} />
        );
    }
    // 获取唯一索引
    getUniqueDeviceKey() {
        let uniqueKey = this.getDeviceKey();
        while (this.rand.indexOf(uniqueKey) > -1) {
            uniqueKey = this.getDeviceKey();
        }
        this.rand.push(uniqueKey);
        return uniqueKey;
    }
    getDeviceKey() {
        return new Date().getTime() + '' + Math.ceil(Math.random() * 10) + '';
    }
    // 变更值
    changeValues(e: any, field: any, key: any) {
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === key) {
                if (field === 'type') {
                    if (e === '填空') {
                        question_list[i]['options'] = this.textareaOptions[0];
                    } else {
                        if (question_list[i][field] === '填空') {
                            question_list[i]['options'] = [];
                        }
                    }
                }
                question_list[i][field] = e;
                break;
            }
        }
        this.setState({
            question_list,
        });
    }
    // 新增选项的值
    addOptionValue(e: any, pkey: any, ckey: any) {
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === pkey) {
                for (let j = 0; j < question_list[i].options.length; j++) {
                    if (question_list[i].options[j]['key'] === ckey) {
                        question_list[i].options[j]['value'] = e;
                        break;
                    }
                }
                break;
            }
        }
        this.setState({
            question_list,
        });
    }
    // 删除选项的值
    delOptionValue(pkey: any, ckey: any) {
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === pkey) {
                let newOption: any = [];
                for (let j = 0; j < question_list[i].options.length; j++) {
                    if (question_list[i].options[j]['key'] !== ckey) {
                        newOption.push(question_list[i].options[j]);
                    }
                }
                question_list[i]['options'] = newOption;
                break;
            }
        }
        this.setState({
            question_list,
        });
    }
    // 删除问题
    delQuestion(key: any) {
        let question_list = this.state.question_list;
        let new_question_list: any = [];
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] !== key) {
                new_question_list.push(question_list[i]);
            }
        }
        this.setState({
            question_list: new_question_list,
        });
    }
    // 还原
    reset() {
        this.setState({
            question_list: this.state.default_question_list,
        });
    }
    // 预览
    preview() {
        if (this.state.question_list.length === 0) {
            message.info('请至少添加一个选项！');
            return;
        }
        this.setState({
            preview_modal: true,
            isPrinting: false,
            previewing: true,
        });
    }
    // 查看统计
    checkChart() {
        let { activity_info, question_list } = this.state;
        request(this, AppServiceUtility.activity_service.get_questionnaire_list!({ business_id: activity_info.id }))
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length > 0) {
                    this.setState({
                        default_questionnaire_list: datas.result,
                        questionnaire_list: this.getQuestionnaireList(question_list, datas.result),
                        questionnaire_modal: true,
                    });
                } else {
                    message.info('暂无录入数据！');
                }
            });
    }
    getQuestionnaireList(question_list: any, questionnaire_list: any) {
        // 构建可以展示的骨架
        let new_questionnaire_list: any = [];
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['type'] === '单选' || question_list[i]['type'] === '多选') {
                new_questionnaire_list[question_list[i]['key']] = {
                    all: 0,
                    options: [],
                };
                for (let j = 0; j < question_list[i]['options'].length; j++) {
                    new_questionnaire_list[question_list[i]['key']]['options'].push({
                        'value': question_list[i]['options'][j]['value'],
                        'key': question_list[i]['options'][j]['key'],
                        'count': 0,
                    });
                }
            }
        }
        for (let k = 0; k < questionnaire_list.length; k++) {
            let objectKeys: any = Object.keys(questionnaire_list[k]['result']);
            for (let l = 0; l < objectKeys.length; l++) {
                if (new_questionnaire_list.hasOwnProperty(objectKeys[l])) {
                    new_questionnaire_list[objectKeys[l]]['all']++;
                    for (let m = 0; m < new_questionnaire_list[objectKeys[l]]['options'].length; m++) {
                        // 多选会是一个数组
                        if (typeof (questionnaire_list[k]['result'][objectKeys[l]]) === 'object') {
                            for (let n = 0; n < questionnaire_list[k]['result'][objectKeys[l]].length; n++) {
                                if (new_questionnaire_list[objectKeys[l]]['options'][m]['value'] === questionnaire_list[k]['result'][objectKeys[l]][n]) {
                                    new_questionnaire_list[objectKeys[l]]['options'][m]['count']++;
                                    break;
                                }
                            }
                        } else if (new_questionnaire_list[objectKeys[l]]['options'][m]['value'] === questionnaire_list[k]['result'][objectKeys[l]]) {
                            new_questionnaire_list[objectKeys[l]]['options'][m]['count']++;
                            break;
                        }
                    }
                }
            }
        }
        // console.log(question_list, questionnaire_list, new_questionnaire_list);
        return new_questionnaire_list;
    }
    checkChartCancel() {
        this.setState({
            questionnaire_modal: false,
        });
    }
    // 手工录入
    humanSet() {
        this.setState({
            preview_modal: true,
            isPrinting: false,
            previewing: false,
        });
    }
    handleCancel() {
        this.setState({
            preview_modal: false,
        });
    }
    // 新增问题
    addQuestion() {
        let question_list = this.state.question_list;
        if (!question_list) {
            question_list = [];
        }
        // 有数组的情况下检查最后一个是否全填了
        if (question_list.length > 0) {
            let last = question_list[question_list.length - 1];
            if (last.value.length === 0) {
                message.info('请输入选项题目内容！');
                return;
            }
            if (last.type !== '填空') {
                if (last.options.length === 0) {
                    message.info('请添加选项的值！');
                    return;
                }
                let flag = true;
                for (let i = 0; i < last.options.length; i++) {
                    if (last.options[i].value === '') {
                        flag = false;
                    }
                }
                if (flag === false) {
                    message.info('请输入选项的值！');
                    return;
                }
            }
        }
        question_list.push({
            // 唯一key
            key: this.getUniqueDeviceKey(),
            // 选项类别
            type: this.typeOptions[0],
            // 标题
            value: '',
            // 单选和多选是子项目，填空是规则
            options: [],
            // 是否有其他选项
            hasOther: false,
        });
        this.setState({
            question_list,
        });
    }
    addOption(key: any) {
        let flag = true;
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === key) {
                for (let j = 0; j < question_list[i]['options'].length; j++) {
                    if (question_list[i]['options'][j].value === '') {
                        flag = false;
                        message.info('请输入选项的值！');
                        break;
                    }
                }
                if (flag) {
                    question_list[i]['options'].push({
                        key: this.getUniqueDeviceKey(),
                        value: '',
                    });
                }
                break;
            }
        }
        if (flag) {
            this.setState({
                question_list,
            });
        }
    }
    addOptionOther(key: any) {
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === key) {
                question_list[i]['hasOther'] = true;
                break;
            }
        }
        this.setState({
            question_list,
        });
    }
    delOptionOther(key: any) {
        let question_list = this.state.question_list;
        for (let i = 0; i < question_list.length; i++) {
            if (question_list[i]['key'] === key) {
                question_list[i]['hasOther'] = false;
                break;
            }
        }
        this.setState({
            question_list,
        });
    }
    saveDCWJ() {
        let { is_send, activity_info, question_list } = this.state;
        if (is_send === true) {
            return;
        }
        let flagAll = true;
        // 判断是否全填了
        if (question_list.length > 0) {
            for (let i = 0; i < question_list.length; i++) {
                if (question_list[i].value.length === 0) {
                    flagAll = false;
                    message.info('请输入选项题目内容！');
                    return;
                }
                if (question_list[i].type !== '填空') {
                    if (question_list[i].options.length === 0) {
                        flagAll = false;
                        message.info('请添加选项的值！');
                        return;
                    }
                    let flag = true;
                    for (let j = 0; j < question_list[i].options.length; j++) {
                        if (question_list[i].options[j].value === '') {
                            flag = false;
                        }
                    }
                    if (flag === false) {
                        flagAll = false;
                        message.info('请输入选项的值！');
                        return;
                    }
                }
            }
        }
        if (!flagAll) {
            return;
        }
        this.setState(
            {
                is_send: true
            },
            () => {
                let param = {};
                param['pureEdit'] = true;
                param['id'] = activity_info['id'];
                param['question'] = question_list;
                request(this, AppServiceUtility.activity_service.update_activity!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                // window.location.reload();
                            });
                            this.setState({
                                is_send: false,
                                default_question_list: question_list,
                                questionnaire_list: this.getQuestionnaireList(question_list, this.state.default_questionnaire_list),
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        console.error(error);
                    });
            }
        );
    }
    set() {
        this.formCreator2.handleDCWJSubmit(false);
    }
    print() {
        this.setState(
            {
                isPrinting: true,
            },
            () => {
                setTimeout(
                    () => {
                        document.body.style.visibility = "hidden";
                        if (document.querySelector('#root')) {
                            document.querySelector('#root')!['style'].height = "0%";
                        }
                        if (document.querySelector('.ant-modal-wrap')) {
                            document.querySelector('.ant-modal-wrap')!.className = 'ant-modal-wrap2';
                        }
                        // document.body.innerHTML = document.querySelector('.hideAll')!.innerHTML;
                        window.print();
                        document.body.style.visibility = "";
                        this.setState({
                            preview_modal: false,
                        });
                        if (document.querySelector('#root')) {
                            document.querySelector('#root')!['style'].height = "100%";
                        }
                        if (document.querySelector('.ant-modal-wrap2')) {
                            document.querySelector('.ant-modal-wrap2')!.className = 'ant-modal-wrap';
                        }
                    },
                    300
                );
            }
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { activity_step, activity_info, signInUserList, notSignInUserList, question_list, preview_modal, activity_signin_list, previewing, questionnaire_list, questionnaire_modal, default_question_list, is_has_draft, draft_info } = this.state;
        let stepStatus: any = {};
        // 步骤条状态
        let step = 0;
        if (activity_info.status === '不通过') {
            stepStatus.status = 'error';
            activity_step[activity_step.length - 1]['step_desc'] = activity_info.reason || '审核不通过';
            step = 2;
        } else if (activity_info.status === '通过') {
            stepStatus.status = 'finish';
            activity_step[activity_step.length - 1]['step_desc'] = activity_info.reason || '审核通过';
            step = 2;
        } else if (activity_info.status === '待审批') {
            step = 1;
        }
        const column: any = [
            {
                title: '姓名',
                dataIndex: 'user_name',
                key: 'user_name',
            },
            {
                title: '性别',
                dataIndex: 'user_sex',
                key: 'user_sex',
            },
            {
                title: '周岁',
                dataIndex: 'age',
                key: 'age',
                render: (text: any, record: any) => {
                    return record.id_card ? getAge(record.id_card) : '';
                },
            },
            {
                title: '联系电话',
                dataIndex: 'user_telephone',
                key: 'user_telephone',
            },
            {
                title: '报名时间',
                dataIndex: 'create_date',
            },
        ];
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        // const { getFieldDecorator } = this.props.form!;
        let cookie_name = CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : "";
        return (
            <MainContent>
                {Object.keys(questionnaire_list).length > 0 ? <Modal
                    title="调查问卷统计"
                    width="900px"
                    visible={questionnaire_modal}
                    okText='确定'
                    onOk={() => this.checkChartCancel()}
                    onCancel={() => this.checkChartCancel()}
                >
                    <Row>
                        {default_question_list.map((item: any, index: number) => {
                            {
                                return (item.type === '单选' || item.type === '多选') ? <Row key={index}>
                                    <Row style={{ padding: '15px 5px' }}>{index + 1}、{item.value}[{item.type}题]</Row>
                                    <Table
                                        key={index}
                                        columns={[
                                            {
                                                title: '选项',
                                                dataIndex: 'value',
                                                key: 'value',
                                                width: 300,
                                                render: (text: any, record: any, index: number) => {
                                                    if (record.rate === false) {
                                                        return (
                                                            <strong>{text}</strong>
                                                        );
                                                    }
                                                    return (
                                                        returnEnIndex(index) + ' ' + text
                                                    );
                                                },
                                            },
                                            {
                                                title: '小计',
                                                dataIndex: 'count',
                                                key: 'count',
                                                width: 300,
                                                // defaultSortOrder: 'descend',
                                                // sorter: (a, b) => {
                                                //     return a.count - b.count;
                                                // },
                                                render: (text: any, record: any, index: number) => {
                                                    if (record.rate === false) {
                                                        return (
                                                            <strong>{text}</strong>
                                                        );
                                                    }
                                                    return text;
                                                },
                                            },
                                            {
                                                title: '比例',
                                                dataIndex: 'rate',
                                                key: 'rate',
                                                width: 300,
                                                render: (text: any, record: any) => {
                                                    if (text === false) {
                                                        return null;
                                                    }
                                                    let percent = record.count / questionnaire_list[item.key]['all'] * 100;
                                                    return (
                                                        <Row>
                                                            <Progress
                                                                strokeColor='#1890ff'
                                                                percent={percent}
                                                                status='normal'
                                                                format={(percent: any) => {
                                                                    return percent === 0 ? '0%' : `${percent!.toFixed(2)}%`;
                                                                }}
                                                            />
                                                        </Row>
                                                    );
                                                },
                                            },
                                        ]}
                                        dataSource={questionnaire_list[item.key]['options'].concat({
                                            'value': '本题有效填写人次',
                                            'count': questionnaire_list[item.key]['all'],
                                            'key': this.getUniqueDeviceKey(),
                                            'rate': false,
                                        })}
                                        size="middle"
                                        rowKey="key"
                                        pagination={false}
                                    />
                                </Row> : null;
                            }
                        })}
                    </Row>
                </Modal> : null}
                <Modal
                    title="调查问卷"
                    width="900px"
                    visible={preview_modal}
                    okText={previewing ? '打印' : '录入'}
                    onOk={previewing ? () => this.print() : () => this.set()}
                    onCancel={() => this.handleCancel()}
                    className={this.state.isPrinting ? 'hideAll' : ''}
                >
                    <Form4 onRef={this.onRef2} question_list={question_list} previewing={previewing} activity_info={activity_info} close={() => this.handleCancel()} />
                </Modal>
                <Steps current={step} style={{ background: "white", padding: "25px" }} {...stepStatus}>
                    {activity_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} description={item.step_desc} />
                        );
                    })}
                </Steps>
                {this.returnPublishForm()}
                {is_has_draft ? <Row type="flex" style={{ paddingBottom: '10px', background: '#fff' }} justify="center">上次草稿保存时间：{draft_info.create_date}，可选择&nbsp;&nbsp;&nbsp;<span style={{ color: '#1890ff', cursor: 'pointer' }} onClick={() => this.readDraft()}>读取草稿</span></Row> : null}
                {activity_info.status === '通过' ?
                    <Row>
                        <Row className="activity_card_title">报名人数/限定人数：<span className="activity_bold">{activity_info['participate_count']}</span>/{activity_info['max_quantity']}</Row>
                        <Card>
                            <Row>
                                <Col span={12}>
                                    <Row>未签到人数：<span className="activity_bold">{notSignInUserList.length}</span></Row>
                                    <Table
                                        columns={column}
                                        dataSource={notSignInUserList}
                                        bordered={true}
                                        size="middle"
                                        rowKey="id"
                                        pagination={false}
                                    />
                                </Col>
                                <Col span={12}>
                                    <Row>已签到人数：<span className="activity_bold">{signInUserList.length}</span></Row>
                                    <Table
                                        columns={column}
                                        dataSource={signInUserList}
                                        bordered={true}
                                        size="middle"
                                        rowKey="id"
                                        pagination={false}
                                    />
                                </Col>
                            </Row>
                        </Card>
                        <Card title="满意度调查" extra={<Row><Button className="mr20" onClick={() => this.reset()}>还原默认</Button><Button className="mr20" onClick={() => this.checkChart()}>查看统计</Button><Button className="mr20" onClick={() => this.humanSet()}>手工录入</Button><Button className="mr20" onClick={() => this.preview()}>打印预览</Button></Row>}>
                            <Table
                                columns={[
                                    {
                                        title: '题目内容',
                                        dataIndex: 'value',
                                        key: 'value',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Row>
                                                    <Input placeholder="请输入题目内容" autoComplete="off" onChange={(e: any) => this.changeValues(e.target.value, 'value', record.key)} defaultValue={text} />
                                                </Row>
                                            );
                                        },
                                    },
                                    {
                                        title: '单选/多选/填空',
                                        dataIndex: 'type',
                                        key: 'type',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Row>
                                                    <Select onChange={(e: any) => this.changeValues(e, 'type', record.key)} defaultValue={text}>
                                                        {this.typeOptions.map((item: any) => {
                                                            return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                                        })}
                                                    </Select>
                                                    {/* <Icon type="caret-down" /> */}
                                                </Row>
                                            );
                                        },
                                    },
                                    {
                                        title: '选项内容',
                                        dataIndex: 'options',
                                        key: 'options',
                                        render: (text: any, record: any) => {
                                            if (record.type === '填空') {
                                                return (
                                                    <Row>
                                                        <Select onChange={(e: any) => this.changeValues(e, 'options', record.key)} defaultValue={text}>
                                                            {this.textareaOptions.map((item: any) => {
                                                                return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                                            })}
                                                        </Select>
                                                    </Row>
                                                );
                                            } else {
                                                let options = record.options.map((itm: any, index: number) => {
                                                    return (
                                                        <Row key={itm.key} className="mb20">
                                                            <Col span={18}>
                                                                <Input placeholder="请输入选项内容" autoComplete="off" defaultValue={itm.value || ''} onChange={(e: any) => this.addOptionValue(e.target.value, record.key, itm.key)} />
                                                            </Col>
                                                            <Col span={6}>
                                                                <Row type="flex" align="middle" justify="end" className="option-minus">
                                                                    <Icon type="minus-circle" onClick={() => this.delOptionValue(record.key, itm.key)} />
                                                                </Row>
                                                            </Col>
                                                        </Row>
                                                    );
                                                });
                                                let other: any = '';
                                                if (record.type !== '填空') {
                                                    if (record.hasOther) {
                                                        other = <Button type="primary" onClick={() => this.delOptionOther(record.key)}>已添加其他填空</Button>;
                                                    } else {
                                                        other = <Button type="default" onClick={() => this.addOptionOther(record.key)}>添加其他填空</Button>;
                                                    }
                                                }
                                                return (
                                                    <Row>
                                                        {options}
                                                        <Row type="flex" align="middle" justify="space-between">
                                                            {other}
                                                            <Icon onClick={() => this.addOption(record.key)} type="plus-circle" />
                                                        </Row>
                                                    </Row>
                                                );
                                            }
                                        },
                                    },
                                    {
                                        title: '编辑题目',
                                        dataIndex: 'edit',
                                        key: 'edit',
                                        render: (text: any, record: any) => {
                                            return (
                                                <Icon type="minus-circle" onClick={() => this.delQuestion(record.key)} style={{ fontSize: '30px' }} />
                                            );
                                        },
                                    },
                                ]}
                                dataSource={this.state.question_list}
                                bordered={true}
                                size="middle"
                                rowKey="key"
                                pagination={false}
                            />
                            <Row type='flex' justify='center' className="ctrl-btns">
                                <Button onClick={() => this.addQuestion()}>新增选项</Button>
                                <Button onClick={() => this.saveDCWJ()} type='primary'>保存</Button>
                            </Row>
                        </Card>
                        <Row>
                            <Col span={12}>
                                <Card title="反馈总结" className="color_orange">
                                    <Form2 formItemLayout={formItemLayout} activity_info={activity_info} cookie_name={cookie_name} />
                                </Card>
                            </Col>
                            <Col span={12}>
                                <Card title="资讯发布" className="color_orange">
                                    <Form3 formItemLayout={formItemLayout} activity_info={activity_info} cookie_name={cookie_name} activity_signin_list={activity_signin_list} />
                                </Card>
                            </Col>
                        </Row>
                    </Row> : null
                }
            </MainContent>
        );
    }
}

/**
 * 控件：活动新增页面控件
 * @description 活动新增页面控件
 * @author
 */
@addon('ChangeActivityPublishViewControl', '活动新增页面控件', '活动新增页面控件')
@reactControl(Form.create<any>()(ChangeActivityPublishView), true)
export class ChangeActivityPublishViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 带参 */
    public select_param?: any;
}