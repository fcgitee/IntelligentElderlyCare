import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import moment from "moment";

/**
 * 组件：活动室管理列表状态
 */
export interface ActivityRoomViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：活动室管理列表
 * 描述
 */
export class ActivityRoomView extends ReactView<ActivityRoomViewControl, ActivityRoomViewState> {
    private columns_data_source = [{
        title: '活动室名称',
        dataIndex: 'activity_room_name',
        key: 'activity_room_name',
    }, {
        title: '最大参与人数',
        dataIndex: 'max_quantity',
        key: 'max_quantity',
    }, {
        title: '开放时间',
        dataIndex: 'open_date',
        key: 'open_date',
        render: (text: any, record: any) => {
            return (
                moment(record.begin_date).format('HH:mm') + '-' + moment(record.end_date).format('HH:mm')
            );
        }
    }, {
        title: '组织机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    }, {
        title: '地址',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '备注',
        dataIndex: 'remarks',
        key: 'remarks',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }];
    constructor(props: ActivityRoomViewControl) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeActivityRoom);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeActivityRoom + '/' + contents.id);
        }
    }
    componentDidMount() {
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "活动室名称",
                decorator_id: "activity_room_name",
                option: {
                    placeholder: "请输入活动室名称",
                    // childrens: user_list,
                }
            },
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
                // {
                //     type: InputType.input,
                //     label: "所属机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入所属机构",
                //         // childrens: user_list,
                //     }
                // }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'del_activity_room'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 活动室管理列表页面
 */
@addon('ActivityRoomView', '活动室管理列表页面', '描述')
@reactControl(ActivityRoomView, true)
export class ActivityRoomViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 