import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { exprot_excel } from "src/business/util_tool";
import { message, Select } from "antd";
import { UploadFile } from "src/business/components/buss-components/upload-file";
const Option = Select.Option;

/**
 * 组件：活动参与列表状态
 */
export interface ActivityParticipateViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    btn_props?: any;
    config?: any;
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：活动参与列表
 * 描述
 */
export class ActivityParticipateView extends ReactView<ActivityParticipateViewControl, ActivityParticipateViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '活动名称',
        dataIndex: 'activity_name',
        key: 'activity_name',
    }, {
        title: '报名人',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '报名人性别',
        dataIndex: 'user_sex',
        key: 'user_sex',
    }, {
        title: '报名人联系方式',
        dataIndex: 'user_telephone',
        key: 'user_telephone',
    }, {
        title: '报名人地址',
        dataIndex: 'user_address',
        key: 'user_address',
    },
    // {
    //     title: '账号',
    //     dataIndex: 'account',
    //     key: 'account',
    // }, 
    {
        title: '报名时间',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: '报名方式',
        dataIndex: 'is_pc',
        key: 'is_pc',
        render: (text: any, record: any) => {
            if (text === true) {
                return '后台补录';
            } else {
                return 'APP报名';
            }
        }
    },
    {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    },];
    constructor(props: ActivityParticipateViewControl) {
        super(props);
        this.state = {
            btn_props: [{
                label: '导出报名列表',
                btn_method: this.download,
                icon: 'download'
            }],
            config: {
                upload: false,
                func: AppServiceUtility.activity_service.upload_sign_in_list,
                title: '模板导入活动报名（请使用系统提供的模板）',
                extra: {
                    upload_date: true,
                }
            },
            org_list: []
        };
    }
    toAdd = () => {
        this.props.history!.push(ROUTE_PATH.chagneActivityParticipate);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeActivityManage + '/' + contents.id);
        }
    }
    // 导入
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    // 导入
    template = () => {
        exprot_excel(
            [{
                name: '活动报名导入模板',
                value: [{
                    "活动名称": '',
                    "长者身份证": '',
                }]
            }],
            '活动报名导入模板',
            'xls'
        );
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    let btn_props: Array<any> = this.state.btn_props;
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.add_permission) {
                            btn_props.push({
                                label: '新增后台补录',
                                btn_method: this.toAdd,
                                icon: 'plus',
                            });
                            btn_props.push({
                                label: '下载导入模板',
                                btn_method: this.template,
                                icon: 'file'
                            });
                            btn_props.push({
                                label: '使用模板导入',
                                btn_method: this.upload,
                                icon: 'upload'
                            });
                        }
                    });
                    this.setState({ btn_props });
                }
            });
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    // 下载
    download = () => {
        let _this = this.formCreator;
        AppServiceUtility.activity_service[this.state.request_url!]!(_this.state.condition)!
            .then((data: any) => {
                if (data.result.length) {
                    let new_data: any = [];
                    let file_name = '活动报名表';
                    data.result.map((item: any) => {
                        new_data.push({
                            "活动名称": item.activity_name,
                            "报名人": item.user_name,
                            "报名人性别": item.user_sex,
                            "报名人联系方式": item.user_telephone,
                            "报名人地址": item.user_address,
                            "报名时间": item.date,
                            "组织机构": item.org_name,
                        });
                    });
                    exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
                } else {
                    message.info('暂无数据可导出', 2);
                }
            });
    }

    render() {
        let activity_participate: any = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "活动名称",
                decorator_id: "activity_name",
                option: {
                    placeholder: "请输入活动名称",
                }
            }, {
                type: InputType.select,
                label: "报名方式",
                decorator_id: "is_pc",
                option: {
                    placeholder: "请选择报名方式",
                    childrens: [{ label: 'APP报名', value: false }, { label: '后台补录', value: true }]!.map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                }
            },
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构",
                //     }
                // },
            ],
            onRef: this.onRef,
            btn_props: this.state.btn_props || [{
                label: '导出报名列表',
                btn_method: this.download,
                icon: 'download'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'delete_activity_participate'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let activity_participate_list = Object.assign(activity_participate, table_param);
        if (activity_participate_list.other_label_type) {
            delete activity_participate_list.other_label_type;
        }
        return (
            <div>
                <SignFrameLayout {...activity_participate_list} />
                <UploadFile {...this.state.config} />
            </div>
        );
    }
}

/**
 * 活动参与列表页面
 */
@addon('ActivityParticipateView', '活动参与列表页面', '描述')
@reactControl(ActivityParticipateView, true)
export class ActivityParticipateViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 