import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Row, message, Select } from "antd";
import { request } from "src/business/util_tool";
import { table_param } from "src/projects/app/util-tool";
const Option = Select.Option;

export function getGoodLookTimeShow(begin_date: string, end_date: string, showYear: boolean = false) {
    if (!begin_date && !end_date) {
        // 否则就显示年月日
        return null;
    }
    if (!begin_date || !end_date) {
        // 否则就显示年月日
        return `${begin_date} - ${end_date}`;
    }
    let by = begin_date.slice(0, 4);
    let bm = begin_date.slice(5, 7);
    let bd = begin_date.slice(8, 10);
    let ey = end_date.slice(0, 4);
    let em = end_date.slice(5, 7);
    let ed = end_date.slice(8, 10);
    // 如果都是一天内，就只显示一个
    let nyear = new Date().getFullYear();
    // 2020-01-02 09:30:00 2020-01-02 10:30:00
    let string: string = '';

    // 开始时间的年份为基准
    if (Number(by) !== nyear || showYear === true) {
        // 非今年的都要加或者必须加
        string += `${by}`;
    }
    // 月份直接加
    if (Number(by) !== nyear) {
        string += string === '' ? `${bm}` : `.${bm}`;
    } else {
        // string += string === '' ? `${Number(bm) + ''}月` : `${Number(bm) + ''}月`;
        string += `${Number(bm) + ''}月`;
    }
    // 日期直接加
    if (Number(by) !== nyear) {
        string += string === '' ? `${bd}` : `.${bd}`;
    } else {
        // string += string === '' ? `${bd}` : `${Number(bd) + ''}日`;
        string += `${Number(bd) + ''}日`;
    }
    // 时间直接加
    string += ` ${begin_date.slice(11, -3)} - `;

    let eflag = false;
    if (ey !== by && Number(ey) !== nyear) {
        // 结束时间非今年的并且跟开始年份不一样的都要加
        string += `${ey}.`;
        eflag = true;
    }
    if (eflag || em !== bm || ed !== bd) {
        // 加了年份日期不一样就要加
        if (Number(ey) !== nyear) {
            string += `${Number(bm) + ''}.${Number(ed) + ''}`;
        } else {
            string += `${Number(em) + ''}月${Number(ed) + ''}日`;
        }
    }
    // 时间直接加
    string += ` ${end_date.slice(11, -3)}`;
    return string;
}
/**
 * 组件：活动管理列表状态
 */
export interface ActivityPublishViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    // 活动类型
    activity_type: any;
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：活动管理列表
 * 描述
 */
export class ActivityPublishView extends ReactView<ActivityPublishViewControl, ActivityPublishViewState> {
    private columns_data_source = [{
        title: '活动名称',
        dataIndex: 'activity_name',
        key: 'activity_name',
    }, {
        title: '活动类型',
        dataIndex: 'activity_type_name',
        key: 'activity_type_name',
    }, {
        title: '活动室',
        dataIndex: 'activity_room_name',
        key: 'activity_room_name',
    }, {
        title: '报名/限定人数',
        dataIndex: 'bmxdrs',
        key: 'bmxdrs',
        render: (text: any, record: any) => {
            return `${record.participate_count}/${record.max_quantity}`;
        },
    }, {
        title: '活动时间',
        dataIndex: 'hdsj',
        key: 'hdsj',
        render: (text: any, record: any) => {
            return getGoodLookTimeShow(record.begin_date, record.end_date);
        },
    },
    // {
    //     title: '服务对象',
    //     dataIndex: 'service_object',
    //     key: 'service_object',
    // }, 
    {
        title: '发布方',
        dataIndex: 'organization_name',
        key: 'organization_name',
    },
    {
        title: '活动状态',
        dataIndex: 'act_status',
        key: 'act_status',
    }, {
        title: '审批状态',
        dataIndex: 'status',
        key: 'status',
        render: (text: any, record: any) => {
            if (text === '不通过') {
                return `不通过，原因：【${record.reason || '无'}】`;
            }
            return text;
        },
    }, {
        title: '审批时间',
        dataIndex: 'audit_date',
        key: 'audit_date',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }, {
        title: '发布时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },];
    constructor(props: ActivityPublishViewControl) {
        super(props);
        this.state = {
            activity_type: [],
            org_list: []
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeActivityManage);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            if (this.props.hasOwnProperty('list_type') && this.props.list_type === 'sh') {
                this.props.history!.push(ROUTE_PATH.changeActivitySh + '/' + contents.id);
            } else {
                if (contents.hasOwnProperty('status') && contents['status'] === '待审批') {
                    message.info('该活动待审批中，请耐心等候！');
                    return;
                }
                this.props.history!.push(ROUTE_PATH.changeActivityManage + '/' + contents.id);
            }
        }
    }
    componentDidMount() {
        // 获取活动类型
        request(this, AppServiceUtility.activity_service.get_activity_type_list!({}, 1, 99)!)
            .then((datas: any) => {
                if (datas && datas['result']) {
                    this.setState({
                        activity_type: datas['result'],
                    });
                }
            }).catch((error: Error) => {
                message.error(error.message);
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    render() {
        const param = this.props.org_type ? { org_type: this.props.org_type } : {};
        let activity_publish = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "活动名称",
                decorator_id: "activity_name",
                option: {
                    placeholder: "请输入活动名称",
                }
            }, {
                type: InputType.select,
                label: "活动类型",
                decorator_id: "activity_type_id",
                option: {
                    placeholder: "请选择活动类型",
                    childrens: this.state.activity_type!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>),
                }
            }, {
                type: InputType.select,
                label: "审批状态",
                decorator_id: "status",
                option: {
                    placeholder: "请选择审批状态",
                    childrens: [{
                        value: '待审批',
                        label: '待审批'
                    }, {
                        value: '通过',
                        label: '通过'
                    }, {
                        value: '不通过',
                        label: '不通过'
                    }].map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                }
            },
            {
                type: InputType.tree_select,
                label: "发布方",
                col_span: 8,
                decorator_id: "organization_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择发布方",
                },
            },
                // {
                //     type: InputType.input,
                //     label: "发布方",
                //     decorator_id: "organization_name",
                //     option: {
                //         placeholder: "请输入发布方名称",
                //         // childrens: user_list,
                //     }
                // },
            ],
            btn_props: [{
                label: '活动发布',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.activity_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'del_activity'
                }
            },
            tabButton: [
                { label: '按发布时间', condition: { 'sort': '发布时间' } },
                { label: '按活动时间', condition: { 'sort': '活动时间' } }
            ],
            searchExtraParam: param,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let activity_publish_list = Object.assign(activity_publish, table_param);
        return (
            <Row>
                <SignFrameLayout {...activity_publish_list} />
            </Row>
        );
    }
}

/**
 * 活动管理列表页面
 */
@addon('ActivityPublishView', '活动管理列表页面', '描述')
@reactControl(ActivityPublishView, true)
export class ActivityPublishViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 带参 */
    public list_type?: any;
    // 机构类型
    public org_type?: any;
} 