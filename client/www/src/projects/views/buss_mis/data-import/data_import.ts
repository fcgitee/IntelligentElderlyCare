import { NullablePromise, DataList, addon } from "pao-aop";

/**
 * 权限状态
 */
export enum DataImportState {
    /** 默认 */
    default = "default",
    /** 授予 */
    grant = "grant",
    /** 禁止 */
    forbid = "forbid"
}
/**
 * 权限
 */
export interface DataImport {
    /** 权限/功能名 */
    permission?: string;
    /** 权限名称 */
    per_name?: string;
    /** 模块 */
    module?: string;
    /** 模块名称 */
    module_name?: string;
    /** 状态 */
    permission_state: PermissionState;
}

/**
 * 数据导入
 */

@addon('Test', '数据导入', '数据导入')
export class Test {
    /**
     * 获取角色列表
     * @param condition 查询条件对象
     * @param page 页码
     * @param count 条数
     */
    happy_rate?(condition?: {}): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    happy_build_process?(condition?: {}): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    export_elder?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    exprot_org?(condition: []): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    old_allowence?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    import_old_list_del?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    add_gov_data?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    add_platform_data?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
    deal_status_old?(): NullablePromise<DataList<DataImport>> {
        return undefined;
    }
}
