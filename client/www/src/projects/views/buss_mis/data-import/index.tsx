import { addon, Permission, } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { isPermission } from "src/projects/app/permission";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { request_func, exprot_excel } from "src/business/util_tool";
// const Option = Select.Option;
/**
 * 组件：数据页面状态
 */
export interface DataImportState extends ReactViewState {
    config?: any;
}

/**
 * 组件：数据页面
 */
export class DataImport extends ReactView<DataImportControl, DataImportState> {

    private columns_data_source = [{
        title: '机构',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '订单数量',
        dataIndex: 'count',
        key: 'count',
    }, {
        title: '总金额',
        dataIndex: 'money',
        key: 'money',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            config: {
                upload: false,
            }
        };
    }
    upload = (title: any, func: any) => {
        const config = {
            upload: true,
            func: func,
            title: '导入幸福院评比'
        };
        this.setState({ config });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRole + '/' + contents.id);
        }
    }
    export = (url: any, name: string, condtion: Array<any> = []) => {
        request_func(this, url, condtion, (data: any) => {
            // console.info('返回数据', data);
            exprot_excel([{ name: name, value: data }], name, 'xls'); // 下载excel
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "角色姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入角色名称'
                    }
                },
            ],
            btn_props: [
                // {
                //     label: '导入高龄津贴已认证长者',
                //     btn_method: () => { this.upload('导入高龄津贴已认证长者', AppServiceUtility.data_import.old_allowence); },
                //     icon: 'plus'
                // },
                // {
                //     label: '导入高龄津贴已死亡名单',
                //     btn_method: () => { this.upload('导入高龄津贴已死亡名单', AppServiceUtility.data_import.import_old_list_del); },
                //     icon: 'plus'
                // },
                {
                    label: '统计不在名单内的高龄长者',
                    btn_method: () => { this.upload('统计不在名单内的高龄长者', AppServiceUtility.data_import.add_gov_data); },
                    icon: 'plus'
                },
                {
                    label: '补充平台平台长者',
                    btn_method: () => { this.upload('补充平台长者', AppServiceUtility.data_import.add_platform_data); },
                    icon: 'plus'
                },
                {
                    label: '更新长者生存字段',
                    btn_method: () => { this.upload('更新长者生存字段', AppServiceUtility.data_import.deal_status_old); },
                    icon: 'plus'
                },

                //     {
                //     label: '幸福院评比',
                //     btn_method: () => { this.upload('导入幸福院评比', AppServiceUtility.data_import.happy_rate); },
                //     icon: 'plus'
                // },
                // {
                //     label: '幸福院建设情况',
                //     btn_method: () => { this.upload('导入幸福院建设情况', AppServiceUtility.data_import.happy_build_process); },
                //     icon: 'plus'
                // }
            ],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <div>
                <SignFrameLayout {...info_list} />
                <UploadFile {...this.state.config} />
            </div>
        );
    }
}

/**
 * 控件：数据页面
 * 描述
 */
@addon('DataImport', '数据页面', '描述')
@reactControl(DataImport, true)
export class DataImportControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    public add_permission?: string;
}