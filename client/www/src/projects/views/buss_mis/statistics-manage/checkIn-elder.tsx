
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Tabs } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";

const { TabPane } = Tabs;

/** 状态：入住长者情况统计 */
export interface CheckInElderViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    org_list?:any;
}
/** 组件：入住长者情况统计 */
export class CheckInElderView extends React.Component<CheckInElderViewControl, CheckInElderViewState> {
    private columns_data_source = [{
        title: '机构名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '床位总数',
        dataIndex: 'bed_count',
        key: 'bed_count',
        width: 100
    },
    {
        title: '入住总人数',
        dataIndex: 'check_in_count',
        key: 'check_in_count',
        width: 50
    }, {
        title: '性别',
        dataIndex: '',
        key: '',
        width: 100,
        children: [{
            title: '女性人数',
            dataIndex: 'women_num',
            key: 'women_num',
            width: 50
        }, {
            title: '男性人数',
            dataIndex: 'man_num',
            key: 'man_num',
            width: 50
        },]
    },
    {
        title: '年龄段分布',
        dataIndex: '',
        key: '',
        width: 150,
        children: [{
            title: '80岁以下人数',
            dataIndex: '80_num',
            key: '80_num',
            width: 50
        }, {
            title: '80至99人数',
            dataIndex: '80_99_num',
            key: '80_99_num',
            width: 50
        }, {
            title: '100岁及以上',
            dataIndex: '100_num',
            key: '100_num',
            width: 50
        }]
    },
    {
        title: '自费长者数',
        dataIndex: 'zf_num',
        key: 'zf_num',
        width: 50
    },
    {
        title: '补贴类型',
        dataIndex: 'btlx',
        key: 'btlx',
        children: [{
            title: '五保人数',
            dataIndex: 'wb_num',
            key: 'wb_num',
        }, {
            title: '三无人数',
            dataIndex: 'sw_num',
            key: 'sw_num',
        }, {
            title: '孤寡人数',
            dataIndex: 'gg_num',
            key: 'gg_num',
        }, {
            title: '享受低保救济人数',
            dataIndex: 'dbjj_num',
            key: 'dbjj_num',
        }]
    },
    {
        title: '护理级别',
        dataIndex: 'hljb',
        key: 'hljb',
        children: [{
            title: '生活自理人数',
            dataIndex: 'shzl_num',
            key: 'shzl_num',
        }, {
            title: '介助1人数',
            dataIndex: 'jz1_num',
            key: 'jz1_num',
        }, {
            title: '介助2人数',
            dataIndex: 'jz2_num',
            key: 'jz2_num',
        }, {
            title: '介护1人数',
            dataIndex: 'jh1_num',
            key: 'jh1_num',
        }, {
            title: '介护2人数',
            dataIndex: 'jh2_num',
            key: 'jh2_num',
        }]
    },
    ];
    constructor(props: CheckInElderViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        // AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '福利院', type: 'bb' })!
        //     .then((data: any) => {
        //         // console.log('养老机构', data);
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addAllergyFile + '/' + contents.id);
        }
    }
    // componentWillMount() {
    //     AppServiceUtility.service_record_service!.get_app_order_list!({}, 1, 10)!
    //         .then((data: any) => {
    //             // console.log(data);
    //         });
    // }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.person_org_manage_service.get_cyryqktj_list!({ ...condition, personnel_category: '福利院', type: 'rzzz' })!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "机构名称": item.name,
                                "床位总数": item.bed_count,
                                "入住总人数": item.check_in_count,
                                "女性人数": item.women_num,
                                "男性人数": item.man_num,
                                "80岁以下人数": item['80_num'],
                                "80至99人数": item['80_99_num'],
                                "100岁及以上人数": item['100_num'],
                                "自费长者数": item.zf_num,
                                "五保人数": item.wb_num,
                                "三无人数": item.sw_num,
                                "孤寡人数": item.gg_num,
                                "享受低保救济人数": item.dbjj_num,
                                "生活自理人数": item.shzl_num,
                                "介助1人数": item.jz1_num,
                                "介助2人数": item.jz2_num,
                                "介护1人数": item.jh1_num,
                                "介护2人数": item.jh2_num,
                            });
                        });
                        exprot_excel([{ name: '入住长者情况统计', value: new_data }], '入住长者情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let checkin_elder = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "name",
                // },
                {
                    type: InputType.tree_select,
                    label: "机构名称",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择机构名称",
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_rzzzqktj_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let checkin_platform = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_apttj_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            }
        };
        let checkin_area_statistics = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_aqtj_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            }
        };
        let checkin_town_street = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_azjtj_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            }
        };
        let checkin_elder_op = Object.assign(checkin_elder, table_param);
        let checkin_platform_op = Object.assign(checkin_platform, table_param);
        let checkin_area_statistics_op = Object.assign(checkin_area_statistics, table_param);
        let checkin_town_street_op = Object.assign(checkin_town_street, table_param);
        return (
            <div>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="按机构统计" key="1">
                        <SignFrameLayout {...checkin_elder_op} />
                    </TabPane>
                    <TabPane tab="按平台统计" key="2">
                        <SignFrameLayout {...checkin_platform_op} />
                    </TabPane>
                    <TabPane tab="按区统计" key="3">
                        <SignFrameLayout {...checkin_area_statistics_op} />
                    </TabPane>
                    <TabPane tab="按镇街统计" key="4">
                        <SignFrameLayout {...checkin_town_street_op} />
                    </TabPane>
                </Tabs>
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：入住长者情况统计控制器
 * @description 入住长者情况统计
 * @author
 */
@addon(' CheckInElderView', '入住长者情况统计', '入住长者情况统计')
@reactControl(Form.create<any>()(CheckInElderView), true)
export class CheckInElderViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}