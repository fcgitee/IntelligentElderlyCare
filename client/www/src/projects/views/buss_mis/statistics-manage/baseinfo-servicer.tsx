
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
let { Option } = Select;
/** 状态：服务商基本情况统计 */
export interface BaseinfoServicerViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    org_list?: any;
}
/** 组件：服务商基本情况统计 */

export function filterHTMLTag(msg: string) {
    if (msg && msg.length > 0) {
        return msg.replace(/<\/?[^>]*>/g, '').replace(/[|]*\n/, '').replace(/&nbsp;/ig, '').replace(/[\r\n]/g, '');
    }
    return msg;
}

export class BaseinfoServicerView extends React.Component<BaseinfoServicerViewControl, BaseinfoServicerViewState> {
    private columns_data_source = [{
        title: "机构名称",
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '机构简介',
        dataIndex: 'description',
        key: 'description',
        width: 600,
        render(text: any, record: any) {
            return filterHTMLTag(text);
        }
    },
    {
        title: '机构性质',
        dataIndex: 'organization_nature',
        key: 'organization_nature',
    },
    {
        title: '法定代表人',
        dataIndex: 'legal_person',
        key: 'legal_person',
    },
    {
        title: '统一社会信用代码',
        dataIndex: 'unified_social',
        key: 'unified_social',
    },
    {
        title: '机构信用等级',
        dataIndex: 'taxes_level',
        key: 'taxes_level',
    },
    {
        title: '行政区划',
        dataIndex: 'division_name',
        key: 'division_name',
    },
    {
        title: '机构地址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '成立时间',
        dataIndex: 'reg_date',
        key: 'reg_date',
    },
        // {
        //     title: '服务类型',
        //     dataIndex: 'service_type',
        //     key: 'service_type',
        // },
    ];
    constructor(props: BaseinfoServicerViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let param: any = {};
            AppServiceUtility.person_org_manage_service.get_servicer_list!(param)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '服务商基本情况统计', value: new_data }], '服务商基本情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeOrganizationFWS + '/' + contents.id);
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "name",
                //     option: {
                //         placeholder: "请输入机构名称",
                //     },
                // },
                {
                    type: InputType.tree_select,
                    label: "机构名称",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择机构",
                    },
                },
                {
                    type: InputType.select,
                    label: '机构性质',
                    decorator_id: "organization_nature",
                    option: {
                        placeholder: "请选择机构性质",
                        childrens: ["公办", "公办民营", "民办企业", "民办非营利"].map((item: any) => <Option key={item}>{item}</Option>),
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            on_icon_click: this.onIconClick,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_servicer_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        allowance_read_list['rowKey'] = 'name';
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：服务商基本情况统计控制器
 * @description 服务商基本情况统计
 * @author
 */
@addon('BaseinfoServicerView', '服务商基本情况统计', '服务商基本情况统计')
@reactControl(Form.create<any>()(BaseinfoServicerView), true)
export class BaseinfoServicerViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}