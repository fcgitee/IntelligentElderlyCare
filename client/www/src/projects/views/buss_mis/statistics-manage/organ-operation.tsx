
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
let { Option } = Select;
/** 状态：机构运营情况统计 */
export interface OperationOrganViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    org_list?:any;
}
/** 组件：机构运营情况统计 */
export class OperationOrganView extends React.Component<OperationOrganViewControl, OperationOrganViewState> {
    private columns_data_source = [{
        title: "机构名称",
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: "年度",
        dataIndex: 'year',
        key: 'year',
    }, {
        title: "年度收入",
        dataIndex: 'year_income',
        key: 'year_income',
    }, {
        title: "年度支出",
        dataIndex: 'year_payout',
        key: 'year_payout',
    }, {
        title: "区级财政建设投入资金",
        dataIndex: 'qu_income',
        key: 'qu_income',
    },
    {
        title: '镇级财政建设投入资金',
        dataIndex: 'zhen_income',
        key: 'zhen_income',
    },
    {
        title: '慈善基金投入资金',
        dataIndex: 'charitable_income',
        key: 'charitable_income',
    }, {
        title: '机构自筹资金',
        dataIndex: 'self_income',
        key: 'self_income',
    },
    {
        title: '社会资助',
        dataIndex: 'social_assistance',
        key: 'social_assistance',
    },
    {
        title: '物品折合',
        dataIndex: 'goods',
        key: 'goods',
    },
    ];
    constructor(props: OperationOrganViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let param: any = {};
            AppServiceUtility.person_org_manage_service.get_operation_situation_list!(param)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '机构运营情况统计', value: new_data }], '机构运营情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let year: any = [];
        let current_year: number = new Date().getFullYear();
        for (let i = current_year - 10; i <= current_year; i++) {
            year.push(i + '');
        }
        let org_operation = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name",
                // },
                {
                    type: InputType.tree_select,
                    label: "机构名称",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择机构名称",
                    },
                },
                {
                    type: InputType.select,
                    label: "年份",
                    decorator_id: "year",
                    option: {
                        placeholder: "请选择年份",
                        childrens: year.map((item: any) => <Option key={item}>{item}</Option>),
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_operation_situation_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let org_operation_list = Object.assign(org_operation, table_param);
        return (
            <div>
                <SignFrameLayout {...org_operation_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：机构运营情况统计控制器
 * @description 机构运营情况统计
 * @author
 */
@addon('OperationOrganView', '机构运营情况统计', '机构运营情况统计')
@reactControl(Form.create<any>()(OperationOrganView), true)
export class OperationOrganViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}