
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
let { Option } = Select;
/** 状态：机构养老基本情况统计 */
export interface BaseinfoOrganViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    administrative_division_list?: any;
    org_list?:any;
}

export class BaseinfoOrganView extends React.Component<BaseinfoOrganViewControl, BaseinfoOrganViewState> {
    private columns_data_source = [{
        title: "机构名称",
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '机构类型',
        dataIndex: 'organization_category',
        key: 'organization_category',
    },
    {
        title: '机构性质',
        dataIndex: 'organization_nature',
        key: 'organization_nature',
    },
    {
        title: '机构星级',
        dataIndex: 'star_level',
        key: 'star_level',
        render: (text: string, record: any) => {
            if (text) {
                switch (text) {
                    case "1":
                        return '一星';
                    case "2":
                        return '二星';
                    case "3":
                        return '三星';
                    case "4":
                        return '四星';
                    case "5":
                        return '五星';
                    default:
                        return undefined;
                }
            }
            return undefined;
        }
    },
    {
        title: '法定代表人',
        dataIndex: 'legal_person',
        key: 'legal_person',
    },
    {
        title: '地址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '办公电话',
        dataIndex: 'telephone',
        key: 'telephone',
    },
    {
        title: '床位总数',
        dataIndex: 'bed_count',
        key: 'bed_count',
    },
    {
        title: '占地面积/m²',
        dataIndex: 'position_area',
        key: 'position_area',
    },
    {
        title: '建筑面积/m²',
        dataIndex: 'build_area',
        key: 'build_area',
    },
    {
        title: '配置医疗机构名称',
        dataIndex: 'configured_medical',
        key: 'configured_medical',
    },
    {
        title: '合作医疗名称',
        dataIndex: 'cooperative_medical',
        key: 'cooperative_medical',
    },
    {
        title: '养老机构设立许可证',
        dataIndex: 'older_org_permission_no',
        key: 'older_org_permission_no',
    },
    {
        title: '统一社会信用代码',
        dataIndex: 'unified_social',
        key: 'unified_social',
    },
    {
        title: '土地证号',
        dataIndex: 'land_deed_no',
        key: 'land_deed_no',
    },
    {
        title: '房产证号',
        dataIndex: 'property_no',
        key: 'property_no',
    },
    {
        title: '卫生许可证',
        dataIndex: 'health_license_no',
        key: 'health_license_no',
    },
    {
        title: '消防许可证',
        dataIndex: 'fire_safety_permit_no',
        key: 'fire_safety_permit_no',
    },
    ];
    constructor(props: BaseinfoOrganViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result!
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let param: any = {};
            AppServiceUtility.person_org_manage_service.get_organ_list!(param)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '机构养老基本情况统计', value: new_data }], '机构养老基本情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeOrganizationJG + '/' + contents.id);
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let baseinfo_organ = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.tree_select,
                    label: "镇街",
                    decorator_id: "admin_area_id",
                    option: {
                        placeholder: "请选择镇街",
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        allowClear: true,
                        treeDefaultExpandAll: true,
                        treeData: this.state.administrative_division_list,
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "name",
                //     option: {
                //         placeholder: "请输入机构名称",
                //     },
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: '机构性质',
                    decorator_id: "organization_nature",
                    option: {
                        placeholder: "请选择机构性质",
                        childrens: ["公办", "公办民营", "民办企业", "民办非营利"].map((item: any) => <Option key={item}>{item}</Option>),
                    },
                },
                {
                    type: InputType.select,
                    label: '机构星级',
                    decorator_id: "star_level",
                    option: {
                        placeholder: "请选择机构星级",
                        childrens: [{
                            'label': '一星',
                            'value': '1',
                        }, {
                            'label': '二星',
                            'value': '2',
                        }, {
                            'label': '三星',
                            'value': '3',
                        }, {
                            'label': '四星',
                            'value': '4',
                        }, {
                            'label': '五星',
                            'value': '5',
                        }].map((item: any) => <Option key={item.value}>{item.label}</Option>),
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            on_icon_click: this.onIconClick,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_organ_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let baseinfo_organ_list = Object.assign(baseinfo_organ, table_param);
        baseinfo_organ_list['rowKey'] = 'name';
        return (
            <div>
                <SignFrameLayout {...baseinfo_organ_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：机构养老基本情况统计控制器
 * @description 机构养老基本情况统计
 * @author
 */
@addon('BaseinfoOrganView', '机构养老基本情况统计', '机构养老基本情况统计')
@reactControl(Form.create<any>()(BaseinfoOrganView), true)
export class BaseinfoOrganViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}