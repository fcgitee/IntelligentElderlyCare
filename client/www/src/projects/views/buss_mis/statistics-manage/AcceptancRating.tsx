
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
/** 状态：验收评级情况统计 */
export interface AcceptancRatingViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
}
/** 组件：验收评级情况统计 */
export class AcceptancRatingView extends React.Component<AcceptancRatingViewControl, AcceptancRatingViewState> {
    private columns_data_source = [{
        title: '镇街',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '当年度申请数',
        dataIndex: 'apply_num_year',
        key: 'apply_num_year',
    },
    {
        title: '累计申请数',
        dataIndex: 'apply_num_all',
        key: 'apply_num_all',
    },
    {
        title: '当年建设经费补助',
        dataIndex: 'build_fund_year',
        key: 'build_fund_year',
    },
    {
        title: '累计建设补助',
        dataIndex: 'build_fund_all',
        key: 'build_fund_all',
    },
    {
        title: '当年度验收数',
        dataIndex: 'acceptance_num_year',
        key: 'acceptance_num_year',
    },
    {
        title: '累计验收数',
        dataIndex: 'acceptance_num_all',
        key: 'acceptance_num_all',
    },
    {
        title: '三星级数量',
        dataIndex: 'star_3_num',
        key: 'star_3_num',
    },
    {
        title: '四星级数量',
        dataIndex: 'star_4_num',
        key: 'star_4_num',
    },
    {
        title: '五星级数量',
        dataIndex: 'star_5_num',
        key: 'star_5_num',
    },
    {
        title: '试运行幸福院数量',
        dataIndex: 'try_run_num',
        key: 'try_run_num',
    },
    ];
    constructor(props: AcceptancRatingViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
        };
    }
    componentDidMount = () => {
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            AppServiceUtility.person_org_manage_service.get_area_operation_list!({})!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '验收及评级情况统计', value: new_data }], '验收及评级情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: '镇街/社区/管理处',
                    decorator_id: "name",
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_area_operation_list',
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        allowance_read_list['rowKey'] = 'name';
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：验收评级情况统计控制器
 * @description 验收评级情况统计
 * @author
 */
@addon('AcceptancRatingView', '验收评级情况统计', '验收评级情况统计')
@reactControl(Form.create<any>()(AcceptancRatingView), true)
export class AcceptancRatingViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}