
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";

/** 状态：投入管理视图 */
export interface InputManageViewState extends ReactViewState {
}
/** 组件：投入管理视图 */
export class InputManageView extends React.Component<InputManageViewControl, InputManageViewState> {

    render() {
        return (
            <div style={{textAlign: 'center',marginTop: '20%'}}>
               <h2>待完善</h2> 
            </div>
        );
    }
}

/**
 * 控件：投入管理视图控制器
 * @description 投入管理视图
 * @author
 */
@addon(' InputManageView', '投入管理视图', '投入管理视图')
@reactControl(InputManageView, true)
export class InputManageViewControl extends ReactViewControl {
}