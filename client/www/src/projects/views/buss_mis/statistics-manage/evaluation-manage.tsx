
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";

/** 状态：评比管理视图 */
export interface EvaluationManageViewState extends ReactViewState {
}
/** 组件：评比管理视图 */
export class EvaluationManageView extends React.Component<EvaluationManageViewControl, EvaluationManageViewState> {

    render() {
        return (
            <div style={{textAlign: 'center',marginTop: '20%'}}>
               <h2>待完善</h2> 
            </div>
        );
    }
}

/**
 * 控件：评比管理视图控制器
 * @description 评比管理视图
 * @author
 */
@addon(' EvaluationManageView', '评比管理视图', '评比管理视图')
@reactControl(EvaluationManageView, true)
export class EvaluationManageViewControl extends ReactViewControl {
}