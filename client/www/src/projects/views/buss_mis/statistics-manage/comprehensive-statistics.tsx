import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Button, Table, Tabs, Modal, Form, Radio, Card } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
import { MainContent } from "src/business/components/style-components/main-content";
const { TabPane } = Tabs;
/**
 * 组件：综合统计页面
 */
export interface ComprehensiveStatisticsViewState extends ReactViewState {
    list?: any;
    list2?: any;
    show?: any;
    select_type?: any;
    list3?: any;
    list4?: any;
}

/**
 * 组件：综合统计页面
 * 描述
 */
export class ComprehensiveStatisticsView extends ReactView<ComprehensiveStatisticsViewControl, ComprehensiveStatisticsViewState> {
    columns: any = [
        {
            title: '镇街',
            dataIndex: 'name',
            key: 'name',
            width: 150,
        },
        {
            title: '本地户籍长者总数（人）',
            dataIndex: 'bd_elder_num',
            key: 'bd_elder_num',
            width: 150,
        },
        {
            title: '机构养老',
            dataIndex: '机构养老',
            key: '机构养老',
            width: 450,
            children: [
                {
                    title: '养老机构数（家）',
                    dataIndex: 'yljg_num',
                    key: 'yljg_num',
                    width: 150,
                },
                {
                    title: '床位总数（张）',
                    dataIndex: 'bed_num',
                    key: 'bed_num',
                    width: 150,
                },
                {
                    title: '入住长者数（人）',
                    dataIndex: 'checkIn_num',
                    key: 'checkIn_num',
                    width: 150,
                },
            ]
        },
        {
            title: '社区幸福院',
            dataIndex: '社区幸福院',
            key: '社区幸福院',
            width: 600,
            children: [
                {
                    title: '幸福院总数（家）',
                    dataIndex: 'xfy_num',
                    key: 'xfy_num',
                    width: 150,
                },
                {
                    title: '服务长者总数（人）',
                    dataIndex: 'xfy_service_elder_num',
                    key: 'xfy_service_elder_num',
                    width: 150,
                },
                {
                    title: '累计开展活动次数（次）',
                    dataIndex: 'act_num',
                    key: 'act_num',
                    width: 150,
                },
                {
                    title: '累计参与活动长者数（人次）',
                    dataIndex: 'signIn_elder_num',
                    key: 'signIn_elder_num',
                    width: 150,
                },
            ]
        },
        {
            title: '居家服务供应商',
            dataIndex: '居家服务供应商',
            key: '居家服务供应商',
            width: 450,
            children: [
                {
                    title: '服务供应商数（家）',
                    dataIndex: 'fws_num',
                    key: 'fws_num',
                    width: 150,
                },
                {
                    title: '居家服务长者总数（人）',
                    dataIndex: 'elder_num',
                    key: 'elder_num',
                    width: 150,
                },
                {
                    title: '居家服务总订单数（单）',
                    dataIndex: 'all_order_num',
                    key: 'all_order_num',
                    width: 150,
                },
            ]
        },
    ];
    columns2: any = [
        {
            title: '镇街',
            dataIndex: 'name',
            key: 'name',
            width: 150,
        },
        {
            title: '养老机构数/家',
            dataIndex: 'yljg_num',
            key: 'yljg_num',
            width: 150,
        },
        {
            title: '医养结合数/家',
            dataIndex: '',
            key: '',
            width: 150,
        },
        {
            title: '床位总数/张',
            dataIndex: 'all_bed_num',
            key: 'all_bed_num',
            width: 150,
        },
        {
            title: '从业人员总数/人',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '建设投入/万元',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '区级财政建设投入资金',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '镇级财政建设投入资金',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '慈善投入资金',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '入住长者数',
            dataIndex: '入住长者数',
            key: '入住长者数',
            width: 300,
            children: [
                {
                    title: '特困供养',
                    dataIndex: '特困供养',
                    key: '特困供养',
                    width: 150,
                },
                {
                    title: '自费长者',
                    dataIndex: '自费长者',
                    key: '自费长者',
                    width: 150,
                },
            ]
        },
        {
            title: '公办机构',
            dataIndex: '公办机构',
            key: '公办机构',
            width: 450,
            children: [
                {
                    title: '床位数/张',
                    dataIndex: 'gb_bed_num',
                    key: 'gb_bed_num',
                    width: 150,
                },
                {
                    title: '入住长者数/人',
                    dataIndex: 'gb_checkIn_num',
                    key: 'gb_checkIn_num',
                    width: 150,
                },
                {
                    title: '从业人员数/人',
                    dataIndex: 'gb_work_num',
                    key: 'gb_work_num',
                    width: 150,
                },
            ]
        },
        {
            title: '公办民营',
            dataIndex: '公办民营',
            key: '公办民营',
            width: 450,
            children: [
                {
                    title: '床位数/张',
                    dataIndex: 'gbmy_bed_num',
                    key: 'gbmy_bed_num',
                    width: 150,
                },
                {
                    title: '入住长者数/人',
                    dataIndex: 'gbmy_checkIn_num',
                    key: 'gbmy_checkIn_num',
                    width: 150,
                },
                {
                    title: '从业人员数/人',
                    dataIndex: 'gbmy_work_num',
                    key: 'gbmy_work_num',
                    width: 150,
                },
            ]
        },
        {
            title: '民办非盈利',
            dataIndex: '民办非盈利',
            key: '民办非盈利',
            width: 450,
            children: [
                {
                    title: '床位数/张',
                    dataIndex: 'mbfyl_bed_num',
                    key: 'mbfyl_bed_num',
                    width: 150,
                },
                {
                    title: '入住长者数/人',
                    dataIndex: 'mbfyl_checkIn_num',
                    key: 'mbfyl_checkIn_num',
                    width: 150,
                },
                {
                    title: '从业人员数/人',
                    dataIndex: 'mbfyl_work_num',
                    key: 'mbfyl_work_num',
                    width: 150,
                },
            ]
        },
        {
            title: '民办企业',
            dataIndex: '民办企业',
            key: '民办企业',
            width: 450,
            children: [
                {
                    title: '床位数/张',
                    dataIndex: 'mb_bed_num',
                    key: 'mb_bed_num',
                    width: 150,
                },
                {
                    title: '入住长者数/人',
                    dataIndex: 'mb_checkIn_num',
                    key: 'mb_checkIn_num',
                    width: 150,
                },
                {
                    title: '从业人员数/人',
                    dataIndex: 'mb_work_num',
                    key: 'mb_work_num',
                    width: 150,
                },
            ]
        },
    ];
    columns3: any = [
        {
            title: '镇街',
            dataIndex: 'name',
            key: 'name',
            width: 150,
        },
        {
            title: '长者总数/人',
            dataIndex: 'yljg_num',
            key: 'yljg_num',
            width: 150,
        },
        {
            title: '社区总数/个',
            dataIndex: '',
            key: '',
            width: 150,
        },
        {
            title: '已建社区幸福院数/家',
            dataIndex: 'all_bed_num',
            key: 'all_bed_num',
            width: 150,
        },
        {
            title: '未建社区幸福院数/家',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '评比优良率/%',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '建设运营投入/万元',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 600,
            children: [
                {
                    title: '总投入',
                    dataIndex: '总投入',
                    key: '总投入',
                    width: 150,
                },
                {
                    title: '区级投入',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '镇级投入',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
                {
                    title: '村级投入',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '社区幸福院建设统计/家',
            dataIndex: '入住长者数',
            key: '入住长者数',
            width: 750,
            children: [
                {
                    title: '五星级',
                    dataIndex: '特困供养',
                    key: '特困供养',
                    width: 150,
                },
                {
                    title: '四星级',
                    dataIndex: '自费长者',
                    key: '自费长者',
                    width: 150,
                },
                {
                    title: '三星级',
                    dataIndex: '自费长者',
                    key: '自费长者',
                    width: 150,
                },
                {
                    title: '已验收',
                    dataIndex: '自费长者',
                    key: '自费长者',
                    width: 150,
                },
                {
                    title: '未验收',
                    dataIndex: '自费长者',
                    key: '自费长者',
                    width: 150,
                },
            ]
        },
        {
            title: '长者高龄津贴认证服务',
            dataIndex: '公办机构',
            key: '公办机构',
            width: 750,
            children: [
                {
                    title: '记录长者总数',
                    dataIndex: 'gb_bed_num',
                    key: 'gb_bed_num',
                    width: 150,
                },
                {
                    title: '60天内认定过',
                    dataIndex: 'gb_checkIn_num',
                    key: 'gb_checkIn_num',
                    width: 150,
                },
                {
                    title: '61-90天之间认定过',
                    dataIndex: 'gb_work_num',
                    key: 'gb_work_num',
                    width: 150,
                },
                {
                    title: '90天内未认定',
                    dataIndex: 'gb_work_num',
                    key: 'gb_work_num',
                    width: 150,
                },
                {
                    title: '无法认定',
                    dataIndex: 'gb_work_num',
                    key: 'gb_work_num',
                    width: 150,
                },
            ]
        },
        {
            title: '社区人员/人',
            dataIndex: '公办民营',
            key: '公办民营',
            width: 450,
            children: [
                {
                    title: '社区干部',
                    dataIndex: 'gbmy_bed_num',
                    key: 'gbmy_bed_num',
                    width: 150,
                },
                {
                    title: '社区服务人员',
                    dataIndex: 'gbmy_checkIn_num',
                    key: 'gbmy_checkIn_num',
                    width: 150,
                },
                {
                    title: '志愿者',
                    dataIndex: 'gbmy_work_num',
                    key: 'gbmy_work_num',
                    width: 150,
                },
            ]
        },
        {
            title: '团体个数/个',
            dataIndex: '民办非盈利',
            key: '民办非盈利',
            width: 150,
        },
        {
            title: '活动累计次数',
            dataIndex: '民办企业',
            key: '民办企业',
            width: 150,
        },
    ];
    columns4: any = [
        {
            title: '镇街',
            dataIndex: 'name',
            key: 'name',
            width: 150,
        },
        {
            title: '居家服务供应商/家',
            dataIndex: 'yljg_num',
            key: 'yljg_num',
            width: 150,
        },
        {
            title: '服务长者总数',
            dataIndex: '',
            key: '',
            width: 150,
        },
        {
            title: '累计服务订单/单',
            dataIndex: 'all_bed_num',
            key: 'all_bed_num',
            width: 150,
        },
        {
            title: '服务费用总额/元',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '服务产品数/个',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '从业人员数/人',
            dataIndex: 'all_work_num',
            key: 'all_work_num',
            width: 150,
        },
        {
            title: '补贴A类',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '购买人数',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '购买次数',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '服务总金额',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '补贴B类',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '购买人数',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '购买次数',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '服务总金额',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '补贴C类',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '购买人数',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '购买次数',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '服务总金额',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '自费',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '购买人数',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '购买次数',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '服务总金额',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
        {
            title: '慈善捐助',
            dataIndex: '建设投入',
            key: '建设投入',
            width: 450,
            children: [
                {
                    title: '人数',
                    dataIndex: '区级财政建设投入资金',
                    key: '区级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '购买次数',
                    dataIndex: '镇级财政建设投入资金',
                    key: '镇级财政建设投入资金',
                    width: 150,
                },
                {
                    title: '服务总金额',
                    dataIndex: '慈善投入资金',
                    key: '慈善投入资金',
                    width: 150,
                },
            ]
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            list: [],
            list2: [],
            list3: [],
            list4: [],
            show: false,
            select_type: 1
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_all_tj_info!({})!
            .then(data => {
                // console.log('所有组织机构数据', data);
                this.setState({
                    list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }

    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let new_data: any = [];
            this.state.list.map((item: any) => {
                new_data.push({
                    "镇街": item.name,
                    "本地户籍长者总数（人）": item.bd_elder_num,
                    "养老机构数（家）": item.yljg_num,
                    "床位总数（张）": item.bed_num,
                    "入住长者数（人）": item.checkIn_num,
                    "幸福院总数（家）": item.xfy_num,
                    "服务长者总数（人）": item.xfy_service_elder_num,
                    "累计开展活动次数（次）": item.act_num,
                    "累计参与活动长者数（人次）": item.signIn_elder_num,
                    "服务供应商数（家）": item.fws_num,
                    "居家服务长者总数（人）": item.elder_num,
                    "居家服务总订单数（单）": item.all_order_num,
                });
            });
            exprot_excel([{ name: '总体统计（汇总）', value: new_data }], '总体统计（汇总）', 'xls');
            this.setState({ 'show': false });

        }
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    onTabChange = (key: any) => {
        // console.log(key);
        if (key === '2') {
            if (this.state.list2.length === 0) {
                AppServiceUtility.person_org_manage_service.yljg_tj!({})!
                    .then(data => {
                        // console.log('养老机构数据', data);
                        this.setState({
                            list2: data!.result
                        });
                    })
                    .catch(err => {
                        console.info(err);
                    });
            }
        }
        if (key === '4') {
            if (this.state.list2.length === 0) {
                AppServiceUtility.person_org_manage_service.jjfws_tj!({})!
                    .then(data => {
                        // console.log('居家服务数据', data);
                        // this.setState({
                        //     list4: data!.result
                        // });
                    })
                    .catch(err => {
                        console.info(err);
                    });
            }
        }
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <MainContent>
                <Card title={'综合统计'}>
                    <Tabs defaultActiveKey="1" onChange={this.onTabChange}>
                        <TabPane tab="总体统计" key="1">
                            <div style={{ height: '30px' }}>
                                <Button type="primary" style={{ float: 'left', marginRight: '5px' }} onClick={this.export}>导出</Button>
                            </div>
                            <Table columns={this.columns} dataSource={this.state.list} pagination={{ pageSize: 10 }} bordered={true} />
                            <Modal
                                title="请选择导出类型"
                                visible={this.state.show}
                                onOk={() => this.download()}
                                onCancel={() => this.onCancel({ show: !this.state.show })}
                                okText="下载"
                                cancelText="取消"
                            >
                                <MainCard>
                                    <Form>
                                        <Form.Item>
                                            {getFieldDecorator('isAll', {
                                            })(
                                                <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                                    <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                                </Radio.Group>
                                            )}
                                        </Form.Item>
                                    </Form>
                                </MainCard >
                            </Modal>
                        </TabPane>
                        <TabPane tab="机构养老" key="2">
                            <div style={{ height: '30px' }}>
                                <Button type="primary" style={{ float: 'left', marginRight: '5px' }} onClick={this.export}>导出</Button>
                            </div>
                            <Table columns={this.columns2} dataSource={this.state.list2} pagination={{ pageSize: 10 }} bordered={true} />
                        </TabPane>
                        <TabPane tab="社区服务" key="3">
                            <div style={{ height: '30px' }}>
                                <Button type="primary" style={{ float: 'left', marginRight: '5px' }} onClick={this.export}>导出</Button>
                            </div>
                            <Table columns={this.columns3} dataSource={this.state.list3} pagination={{ pageSize: 10 }} bordered={true} />
                        </TabPane>
                        <TabPane tab="居家服务" key="4">
                            <div style={{ height: '30px' }}>
                                <Button type="primary" style={{ float: 'left', marginRight: '5px' }} onClick={this.export}>导出</Button>
                            </div>
                            <Table columns={this.columns4} dataSource={this.state.list4} pagination={{ pageSize: 10 }} bordered={true} />
                        </TabPane>
                    </Tabs>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：综合统计页面
 * 描述
 */
@addon(' ComprehensiveStatisticsView', '综合统计页面', '提示')
@reactControl(Form.create<any>()(ComprehensiveStatisticsView), true)
export class ComprehensiveStatisticsViewControl extends ReactViewControl {
}