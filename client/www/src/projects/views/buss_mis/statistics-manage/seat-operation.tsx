
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
const { Option } = Select;
/** 状态：坐席运营统计 */
export interface SeatOperationViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    now_circle?: any;
}
/** 组件：坐席运营统计 */
export class SeatOperationView extends React.Component<SeatOperationViewControl, SeatOperationViewState> {
    private columns_data_source = [{
        title: '年份',
        dataIndex: 'year',
        key: 'year',
    },
    {
        title: '统计周期',
        dataIndex: 'count_circle',
        key: 'count_circle',
    },
    {
        title: '呼叫总量（min）',
        dataIndex: 'call_summary',
        key: 'call_summary',
    },
    {
        title: '呼叫次数（次）',
        dataIndex: 'call_count',
        key: 'call_count',
    },
    {
        title: '服务需求（次）',
        dataIndex: 'service_demand_summary',
        key: 'service_demand_summary',
    },
    {
        title: '平均通话时长（min）',
        dataIndex: 'call_time_avg',
        key: 'call_time_avg',
    },
    {
        title: '接通率',
        dataIndex: 'on_call_rate',
        key: 'on_call_rate',
    },
    {
        title: '平均等待时长（s）',
        dataIndex: 'wait_time_avg',
        key: 'wait_time_avg',
    },
    {
        title: '平均处理时间（h）',
        dataIndex: 'handle_time_avg',
        key: 'handle_time_avg',
    },
    {
        title: '满意度（%）',
        dataIndex: 'good_rate',
        key: 'good_rate',
    },
    {
        title: '投诉率（%）',
        dataIndex: 'bad_rate',
        key: 'bad_rate',
    },
    ];
    constructor(props: SeatOperationViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            now_circle: '',
        };
    }
    componentDidMount = () => {
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            AppServiceUtility.emi_service_with_loading.get_seat_operation_list!({ circle: '年度' })!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '坐席运营统计', value: new_data }], '坐席运营统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    changeCircle = (e: any) => {
        this.setState({
            now_circle: e,
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {
            circle: this.state.now_circle || '年度'
        };
        let sear_operation = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: '统计周期',
                    decorator_id: "circle",
                    default_value: '年度',
                    option: {
                        placeholder: "统计周期",
                        showSearch: true,
                        childrens: ['月度', '季度', '年度'].map((item: any, idx: any) => <Option key={item}>{item}</Option>),
                        onChange: this.changeCircle,
                    }
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.call_center_nh_service,
            service_option: {
                select: {
                    service_func: 'get_seat_operation_list',
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
        };
        let sear_operation_list = Object.assign(sear_operation, table_param);
        sear_operation_list['rowKey'] = 'name';
        return (
            <div>
                <SignFrameLayout {...sear_operation_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：坐席运营统计控制器
 * @description 坐席运营统计
 * @author
 */
@addon('SeatOperationView', '坐席运营统计', '坐席运营统计')
@reactControl(Form.create<any>()(SeatOperationView), true)
export class SeatOperationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}