
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";

/** 状态：从业人员情况统计 */
export interface EmployeesViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    org_list?:any;
}
/** 组件：从业人员情况统计 */
export class EmployeesView extends React.Component<EmployeesViewControl, EmployeesViewState> {
    private columns_data_source = [{
        title: '机构名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '从业人员总数',
        dataIndex: 'employees_count',
        key: 'employees_count',
        width: 50
    }, {
        title: '性别',
        dataIndex: '',
        key: '',
        width: 100,
        children: [{
            title: '女性人数',
            dataIndex: 'women_num',
            key: 'women_num',
            width: 50
        }, {
            title: '男性人数',
            dataIndex: 'man_num',
            key: 'man_num',
            width: 50
        },]
    },
    {
        title: '岗位分类',
        dataIndex: '',
        key: '',
        width: 150,
        children: [{
            title: '机构管理人员',
            dataIndex: 'jggl_num',
            key: 'jggl_num',
            width: 50
        }, {
            title: '专业技能人员',
            dataIndex: 'zyjn_num',
            key: 'zyjn_num',
            width: 50
        }, {
            title: '院长人数',
            dataIndex: 'yz_num',
            key: 'yz_num',
            width: 50
        }, {
            title: '医生',
            dataIndex: 'ys_num',
            key: 'ys_num',
            width: 50
        }, {
            title: '护士人数',
            dataIndex: 'hs_num',
            key: 'hs_num',
            width: 50
        }, {
            title: '护理员',
            dataIndex: 'hly_num',
            key: 'hly_num',
            width: 50
        }, {
            title: '后勤人员',
            dataIndex: 'hqry_num',
            key: 'hqry_num',
            width: 50
        }, {
            title: '社工人数',
            dataIndex: 'sg_num',
            key: 'sg_num',
            width: 50
        }]
    },
    {
        title: '学历',
        dataIndex: 'xueli',
        key: 'xueli',
        children: [{
            title: '大专以下人数',
            dataIndex: 'dzyx_num',
            key: 'dzyx_num',
        }, {
            title: '大专',
            dataIndex: 'dz_num',
            key: 'dz_num',
        }, {
            title: '本科及以上',
            dataIndex: 'bkys_num',
            key: 'bkys_num',
        }]
    },
    {
        title: '年龄段分布',
        dataIndex: 'age',
        key: 'age',
        children: [{
            title: '35岁以下',
            dataIndex: '35_num',
            key: '35_num',
        }, {
            title: '36-45岁',
            dataIndex: '36_45_num',
            key: '36_45_num',
        }, {
            title: '46-55岁',
            dataIndex: '46_55_num',
            key: '46_55_num',
        }, {
            title: '56岁以上',
            dataIndex: '56_num',
            key: '56_num',
        }]
    },
    {
        title: '薪酬（人均月薪）',
        dataIndex: 'xinchou',
        key: 'xinchou',
        children: [{
            title: '院长',
            dataIndex: 'yz_money',
            key: 'yz_money',
        }, {
            title: '医生',
            dataIndex: 'ys_money',
            key: 'ys_money',
        }, {
            title: '护士',
            dataIndex: 'hs_money',
            key: 'hs_money',
        }, {
            title: '护理员',
            dataIndex: 'hly_money',
            key: 'hly_money',
        }, {
            title: '后勤人员',
            dataIndex: 'hqry_money',
            key: 'hqry_money',
        }, {
            title: '社工',
            dataIndex: 'sg_money',
            key: 'sg_money',
        }]
    },
    ];
    constructor(props: EmployeesViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        // AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '福利院', type: 'bb' })!
        //     .then((data: any) => {
        //         // console.log('养老机构', data);
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    add = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addAllergyFile + '/' + contents.id);
        }
    }
    // componentWillMount() {
    //     AppServiceUtility.service_record_service!.get_app_order_list!({}, 1, 10)!
    //         .then((data: any) => {
    //             // console.log(data);
    //         });
    // }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.person_org_manage_service.get_cyryqktj_list!({ ...condition, personnel_category: '福利院', type: 'cyry' })!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "机构名称": item.name,
                                "从业人员总数": item.employees_count,
                                "女性人数": item.women_num,
                                "男性人数": item.man_num,
                                "机构管理人员": item.jggl_num,
                                "专业技能人员": item.zyjn_num,
                                "院长人数": item.yz_num,
                                "医生人数": item.ys_num,
                                "护士人数": item.hs_num,
                                "护理员": item.hly_num,
                                "后勤人员": item.hqry_num,
                                "社工人数": item.sg_num,
                                "大专以下人数": item.dzyx_num,
                                "大专": item.dz_num,
                                "本科及以上": item.bkys_num,
                                "35岁以下": item['35_num'],
                                "36-45岁": item['36_45_num'],
                                "46-55岁": item['46_55_num'],
                                "56岁以上": item['56_num'],
                                "院长薪酬": item['yz_money'],
                                "医生薪酬": item['ys_money'],
                                "护士薪酬": item['hs_money'],
                                "护理员薪酬": item['hly_money'],
                                "后勤人员薪酬": item['hqry_money'],
                                "社工薪酬": item['sg_money'],
                            });
                        });
                        exprot_excel([{ name: '从业人员情况统计', value: new_data }], '从业人员情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let employees = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "name",
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.add,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_cyryqktj_list',
                    service_condition: [{ personnel_category: '福利院', type: 'cyry' }, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let employees_list = Object.assign(employees, table_param);
        return (
            <div>
                <SignFrameLayout {...employees_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：从业人员情况统计控制器
 * @description 从业人员情况统计
 * @author
 */
@addon(' EmployeesView', '从业人员情况统计', '从业人员情况统计')
@reactControl(Form.create<any>()(EmployeesView), true)
export class EmployeesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}