
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
const { Option } = Select;

/** 状态：养老机构基本信息 */
export interface OrganizationImformationViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    administrative_division_list?: any;
    star_level?: any;
}
/** 组件：养老机构基本信息 */
export class OrganizationImformationView extends React.Component<OrganizationImformationViewControl, OrganizationImformationViewState> {
    private columns_data_source = [{
        title: '机构名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '机构类型',
        dataIndex: 'organization_info.personnel_category',
        key: 'organization_info.personnel_category',
    },
    {
        title: '机构性质',
        dataIndex: 'organization_info.organization_nature',
        key: 'organization_info.organization_nature',
    }, {
        title: '机构星级',
        dataIndex: 'organization_info.star_level',
        key: 'organization_info.star_level',
    },
    {
        title: '法定代表人',
        dataIndex: 'organization_info.legal_person',
        key: 'organization_info.legal_person',
    },
    {
        title: '地址',
        dataIndex: 'organization_info.address',
        key: 'organization_info.address',
    },
    {
        title: '办公电话',
        dataIndex: 'organization_info.telephone',
        key: 'organization_info.telephone',
    },
    {
        title: '床位总数',
        dataIndex: 'bed_count',
        key: 'bed_count',
    },
    {
        title: '占地面积/m²',
        dataIndex: '',
        key: '',
    },
    {
        title: '建筑面积/m²',
        dataIndex: '',
        key: '',
    },
    {
        title: '配备医疗机构名称',
        dataIndex: '',
        key: '',
    },
    {
        title: '合作医疗名称',
        dataIndex: '',
        key: '',
    },
    {
        title: '养老机构设立许可证编号',
        dataIndex: '',
        key: '',
    },
    {
        title: '统一社会信用代码',
        dataIndex: '',
        key: '',
    },
    {
        title: '土地证号',
        dataIndex: '',
        key: '',
    },
    {
        title: '房产证号',
        dataIndex: '',
        key: '',
    },
    {
        title: '卫生许可证号',
        dataIndex: '',
        key: '',
    },
    {
        title: '消防许可证号',
        dataIndex: '',
        key: '',
    },
    ];
    constructor(props: OrganizationImformationViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            administrative_division_list: [],
            status: ["公办", "公办民营", "民办企业", "民办非营利"],
            star_level: [{ name: "五星级", value: '5' }, { name: "四星级", value: '4' }, { name: "三星级", value: '3' }, { name: "二星级", value: '2' }, { name: "一星级", value: '1' }, { name: "零星级", value: '0' }],
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '福利院' })!
            .then((data: any) => {
                // console.log('养老机构', data);
            });

        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addAllergyFile + '/' + contents.id);
        }
    }
    // componentWillMount() {
    //     AppServiceUtility.service_record_service!.get_app_order_list!({}, 1, 10)!
    //         .then((data: any) => {
    //             // console.log(data);
    //         });
    // }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.person_org_manage_service.get_all_organization_list!({ ...condition, personnel_category: '福利院' })!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "机构名称": item.name,
                                "机构类型": item.organization_info.personnel_category,
                                "机构性质": item.organization_info.organization_nature,
                                "机构星级": item.organization_info.star_level,
                                "法定代表人": item.organization_info.legal_person,
                                "地址": item.organization_info.address,
                                "办公电话": item.organization_info.telephone,
                                "床位总数": item.bed_count,
                                "占地面积/m²": '',
                                "建筑面积/m²": '',
                                "配置医疗机构名称": '',
                                "合作医疗名称": '',
                                "养老机构设立许可证编号": '',
                                "社会统一信用代码": '',
                                "土地证号": '',
                                "房产证号": '',
                                "卫生许可证号": '',
                                "消防许可证号": '',
                            });
                        });
                        exprot_excel([{ name: '养老机构基本信息', value: new_data }], '养老机构基本信息（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.cascader,
                    col_span: 6,
                    label: "镇街",
                    decorator_id: "admin_area_id",
                    option: {
                        showSearch: this.filter,
                        options: this.state.administrative_division_list,
                        placeholder: "请选择镇街",
                        fieldNames: { lable: 'name', value: 'id', children: 'children' }
                    },
                },
                {
                    type: InputType.input,
                    label: "机构名称",
                    decorator_id: "name",
                },
                {
                    type: InputType.select,
                    label: "机构性质",
                    decorator_id: "organization_nature",
                    option: {
                        placeholder: "请选择机构性质",
                        showSearch: true,
                        childrens: this.state.status!.map((item: any, idx: any) => <Option key={idx} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.select,
                    label: "星级情况",
                    decorator_id: "star_level",
                    option: {
                        placeholder: "请选择星级情况",
                        showSearch: true,
                        childrens: this.state.star_level!.map((item: any, idx: any) => <Option key={idx} value={item.value}>{item.name}</Option>),
                    }
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_all_organization_list',
                    service_condition: [{ personnel_category: '福利院' }, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：养老机构基本信息控制器
 * @description 养老机构基本信息
 * @author
 */
@addon(' OrganizationImformationView', '养老机构基本信息', '养老机构基本信息')
@reactControl(Form.create<any>()(OrganizationImformationView), true)
export class OrganizationImformationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}