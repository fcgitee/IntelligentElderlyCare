
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio, Select } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
let { Option } = Select;
/** 状态：幸福院运营情况统计 */
export interface OperationHappinessViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    org_list?: any;
}
/** 组件：幸福院运营情况统计 */
export class OperationHappinessView extends React.Component<OperationHappinessViewControl, OperationHappinessViewState> {
    private columns_data_source = [{
        title: "幸福院名称",
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '社区',
        dataIndex: 'division_name',
        key: 'division_name',
    },
    {
        title: '设立时间',
        dataIndex: 'reg_date',
        key: 'reg_date',
        render(text: any, record: any) {
            if (text && text.length > 10 && text.substr) {
                return text.substr(0, 10);
            }
            return text;
        }
    }, {
        title: '星级',
        dataIndex: 'star_level',
        key: 'star_level',
    },
    {
        title: '服务机构',
        dataIndex: 'service_organization',
        key: 'service_organization',
    },
    {
        title: '当年获得建设资助（万元）',
        dataIndex: 'current_year_fund',
        key: 'current_year_fund',
    },
    {
        title: '建设资助累计（万元）',
        dataIndex: 'all_fund',
        key: 'all_fund',
    },
    {
        title: '获得评比奖励（万元）',
        dataIndex: 'rate_reward',
        key: 'rate_reward',
    },
    {
        title: '评比奖励累计（万元）',
        dataIndex: 'all_rate_reward',
        key: 'all_rate_reward',
    },
    {
        title: '配备图书数量',
        dataIndex: 'book_num',
        key: 'book_num',
    },
    {
        title: '每周开放时间',
        dataIndex: 'open_days',
        key: 'open_days',
        render(text: any, record: any) {
            if (typeof (text) === 'object' && text.length && text.join) {
                return text.join('，');
            }
            return text;
        }
    },
    {
        title: '社工人数',
        dataIndex: 'social_worker_num',
        key: 'social_worker_num',
    },
    {
        title: '当年开展活动次数',
        dataIndex: 'current_activity_num',
        key: 'current_activity_num',
    },
    {
        title: '社区长者数',
        dataIndex: 'elder_num',
        key: 'elder_num',
    },
    {
        title: '服务人数占辖区长者百分比',
        dataIndex: 'elder_rate',
        key: 'elder_rate',
    },
    {
        title: '满意度',
        dataIndex: 'satisfaction',
        key: 'satisfaction',
    },
    {
        title: '被投诉次数',
        dataIndex: 'complaints_num',
        key: 'complaints_num',
    },
    ];
    constructor(props: OperationHappinessViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentDidMount = () => {
        // AppServiceUtility.person_org_manage_service.get_all_organization_list!({ personnel_category: '福利院', type: 'bb' })!
        //     .then((data: any) => {
        //         // console.log('养老机构', data);
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["幸福院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    daochu = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
    }
    // componentWillMount() {
    //     AppServiceUtility.service_record_service!.get_app_order_list!({}, 1, 10)!
    //         .then((data: any) => {
    //             // console.log(data);
    //         });
    // }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let param: any = {};
            AppServiceUtility.person_org_manage_service.get_happiness_operation_list!(param)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '幸福院运营情况统计', value: new_data }], '幸福院运营情况统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "幸福院名称",
                //     decorator_id: "name",
                // },
                {
                    type: InputType.tree_select,
                    label: "幸福院名称",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择幸福院",
                    },
                },
                {
                    type: InputType.input,
                    label: '镇街/社区/管理处',
                    decorator_id: "division_name",
                },
                {
                    type: InputType.select,
                    label: '星级',
                    decorator_id: "star_level",
                    option: {
                        placeholder: "请选择星级",
                        childrens: [
                            {
                                label: '一星',
                                value: '1',
                            },
                            {
                                label: '二星',
                                value: '2',
                            },
                            {
                                label: '三星',
                                value: '3',
                            },
                            {
                                label: '四星',
                                value: '4',
                            },
                            {
                                label: '五星',
                                value: '5',
                            }
                        ].map((item: any) => <Option key={item.value}>{item.label}</Option>),
                    },
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_happiness_operation_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        allowance_read_list['rowKey'] = 'name';
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：幸福院运营情况统计控制器
 * @description 幸福院运营情况统计
 * @author
 */
@addon('OperationHappinessView', '幸福院运营情况统计', '幸福院运营情况统计')
@reactControl(Form.create<any>()(OperationHappinessView), true)
export class OperationHappinessViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}