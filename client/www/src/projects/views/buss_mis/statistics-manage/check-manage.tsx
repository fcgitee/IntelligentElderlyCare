
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Input, Button, Row, Col, DatePicker, Select, message } from 'antd';
import moment from 'moment';
import { NTCascader } from "src/business/components/buss-components/cascader";
const { Option } = Select;

/** 状态：验收管理列表视图 */
export interface CheckManageViewState extends ReactViewState {
    star_level: any[];
    /** 接口名 */
    request_ulr?: string;
    administrative_division_list?: any;
    status?: any;
    show?: any;
    check_date?: any;
    admin_area_id?: any;
    xfy_name?: any;
    level?: any;
    check_time?: any;
    zhenjie_ids?: any;
    /* 组织机构 */
    org_list?: any;
}
/** 组件：验收管理列表视图 */
export class CheckManageView extends React.Component<CheckManageViewControl, CheckManageViewState> {
    private columns_data_source = [{
        title: '序号',
        dataIndex: 'num',
        key: 'num',
        render: (text: any, record: any, index: number) => {
            return index + 1;
        }
    },
    {
        title: '镇街',
        dataIndex: 'zhenjie',
        key: 'zhenjie',
    },
    {
        title: '社区幸福院名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '星级情况',
        dataIndex: 'level',
        key: 'level',
    },
    {
        title: '地址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '验收日期',
        dataIndex: 'check_date',
        key: 'check_date',
        render: (text: any, record: any) => {
            return (
                moment(record.check_date).format('YYYY-MM-DD')
            );
        }
    },
    {
        title: '验收状态',
        dataIndex: 'check_status',
        key: 'check_status',
        render: (text: any, record: any, index: number) => {
            if (record.check_date) {
                let day = (new Date().getTime() - new Date(record.check_date).getTime()) / (1000 * 60 * 60 * 24);
                if (day > 1) {
                    return '已验收';
                } else {
                    return '未验收';
                }
            } else {
                return '未验收';
            }
        }
    },
    {
        title: '编辑时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    },
    ];
    constructor(props: CheckManageViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            administrative_division_list: [],
            star_level: [{
                value: '1',
                label: '一星级'
            }, {
                value: '2',
                label: '二星级'
            }, {
                value: '3',
                label: '三星级'
            }, {
                value: '4',
                label: '四星级'
            }, {
                value: '5',
                label: '五星级'
            },],
            status: ["已验收", "未验收"],
            show: false,
            check_date: '',
            admin_area_id: '',
            xfy_name: '',
            level: '',
            check_time: '',
            zhenjie_ids: [],
            org_list: []
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.checkManage + '/' + contents.id);
        }
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        if (this.props.match!.params.key) {
            this.setState({
                show: true
            });
            // 组织机构
            AppServiceUtility.person_org_manage_service.get_ys_xfy_list!({ id: this.props.match!.params.key })!
                .then((data: any) => {
                    // // console.log('幸福院', data);
                    this.setState({
                        admin_area_id: data.result[0].admin_area_id,
                        xfy_name: data.result[0].name,
                        level: data.result[0].star_level,
                        zhenjie_ids: data.result[0].zhenjie_ids || [],
                        check_date: moment(data.result[0].check_date ? data.result[0].check_date : new Date(), 'YYYY/MM/DD'),
                    });
                });
        }
        // 组织机构
        // AppServiceUtility.person_org_manage_service.get_ys_xfy_list!({})!
        //     .then(data => {
        //         // console.log('幸福院', data);
        //     });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }

    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }

    dateChange = (date: any, dateString: any) => {
        // // console.log(dateString);
        this.setState({
            check_date: dateString
        });
    }

    submit = () => {
        AppServiceUtility.person_org_manage_service.update_ys_xfy!({ id: this.props.match!.params.key, star_level: this.state.level, name: this.state.xfy_name, admin_area_id: this.state.admin_area_id, zhenjie_ids: this.state.zhenjie_ids, check_date: this.state.check_date })!
            .then(data => {
                if (data === 'Success') {
                    message.success('保存成功！');
                    this.props.history!.push(ROUTE_PATH.checkManage);
                } else {
                    message.error('保存失败！');
                    return;
                }
            });
    }

    cancel = () => {
        this.setState({
            show: false
        });
    }
    changeZhenjie = (e: any) => {
        // console.log(e);
        this.setState({
            zhenjie_ids: e,
            admin_area_id: e[e.length - 1],
        });
    }
    changeName = (e: any) => {
        this.setState({
            xfy_name: e.target.value,
        });
    }
    changeStar = (e: any) => {
        this.setState({
            level: e,
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let check_manage = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.cascader,
                    col_span: 6,
                    label: "镇街",
                    decorator_id: "admin_area_id",
                    option: {
                        showSearch: this.filter,
                        options: this.state.administrative_division_list,
                        placeholder: "请选择镇街",
                        fieldNames: { lable: 'name', value: 'id', children: 'children' }
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "幸福院名称",
                //     decorator_id: "name",
                //     option: {
                //         placeholder: "请输入幸福院名称",
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: "星级情况",
                    decorator_id: "star_level",
                    option: {
                        placeholder: "请选择星级情况",
                        showSearch: true,
                        childrens: this.state.star_level.map((item: any, idx) => <Option key={item.value}>{item.label}</Option>),
                    }
                },
                {
                    type: InputType.select,
                    label: "验收状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择星级情况",
                        showSearch: true,
                        childrens: this.state.status!.map((item: any, idx: any) => <Option key={idx} value={item}>{item}</Option>),
                    }
                },
            ],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_ys_xfy_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_disease_file'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let check_manage_list = Object.assign(check_manage, table_param);
        let star_level = this.state.star_level && this.state.star_level.map((item: any, idx: any) => <Option key={item.value}>{item.label}</Option>);
        let optionss: any = {
            showSearch: this.filter,
            options: this.state.administrative_division_list,
            placeholder: "请选择镇街",
            disabled: true,
            fieldNames: { lable: 'name', value: 'id', children: 'children' },
            onChange: this.changeZhenjie,
            value: Array.isArray(this.state.zhenjie_ids) ? this.state.zhenjie_ids : [this.state.admin_area_id],
        };
        return (
            <div>
                <div style={{ position: 'relative', display: this.state.show === true ? 'block' : 'none', backgroundColor: '#ffffff', height: '130px', margin: '36px 36px 0px 36px', paddingTop: '20px' }}>
                    {this.state.show === true ?
                        <div>
                            <Row justify="space-around" style={{ textAlign: 'center', marginBottom: '30px' }}>
                                <Col span={6}>镇街：<NTCascader option={...optionss} /></Col>
                                <Col span={6}>社区幸福院名称：<Input onChange={this.changeName} value={this.state.xfy_name} style={{ width: '150px' }} /></Col>
                                <Col span={6}>星级情况：<Select value={this.state.level} onChange={this.changeStar} style={{ width: '150px' }} >{star_level}</Select></Col>
                                <Col span={6}>验收日期：<DatePicker value={this.state.check_date} onChange={this.dateChange} placeholder='请选择开始日期' style={{ width: '200px' }} /></Col>
                            </Row>
                            <div style={{ paddingLeft: '85%' }}>
                                <Button type="primary" onClick={this.submit} style={{ marginRight: '20px' }}>保存</Button>
                                <Button onClick={this.cancel}>取消</Button>
                            </div>
                        </div>
                        :
                        ''
                    }
                </div>
                <SignFrameLayout {...check_manage_list} />
            </div >
        );
    }
}

/**
 * 控件：验收管理列表视图控制器
 * @description 验收管理列表视图
 * @author
 */
@addon(' CheckManageView', '验收管理列表视图', '验收管理列表视图')
@reactControl(CheckManageView, true)
export class CheckManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}