import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainCard } from "src/business/components/style-components/main-card";
import { Select, Button } from 'antd';
let { Option } = Select;

/**
 * 组件：知识库录入页面
 */
export interface KnowledgeEditViewState extends ReactViewState {
    type?: any;
}

/**
 * 组件：知识库录入页面
 * 描述
 */
export class KnowledgeEditView extends ReactView<KnowledgeEditViewControl, KnowledgeEditViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            type: ["居家服务", "通讯录"],
        };
    }
     // 返回
     goBack = () => {
        this.props.history!.push(ROUTE_PATH.knowledgeBase);
    }
    // 呼叫中心
    goCenter = () => {
        this.props.history!.push(ROUTE_PATH.callCenter);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        const list: JSX.Element[] = this.state.type!.map((item: any, idx: any) => <Option key={item} value={item}>{item}</Option>);
        if (redeirect) {
            return redeirect;
        }

        let edit_props = {
            form_items_props: [
                {
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "标题",
                            decorator_id: "title",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入标题" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入标题",
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "关键词",
                            decorator_id: "keyword",
                            field_decorator_option: {
                                rules: [{ message: "请输入关键词" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入关键词",
                            }
                        }, {
                            type: InputType.select,
                            label: "请选择类型",
                            decorator_id: "type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: list,
                                placeholder: "请选择类型",
                                showSearch: true
                            }
                        }, {
                            type: InputType.nt_rich_text,
                            label: "内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入内容",
                                value: ' '

                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.knowledgeBase);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.knowledge_service,
                operation_option: {
                    query: {
                        func_name: "get_knowledge_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_knowledge"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.knowledgeBase); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <div>
                <MainContent>
                    <MainCard title='文章管理' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goCenter}>呼叫中心</Button>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goBack}>返回</Button>
                        </div>
                        <FormCreator {...edit_props_list} />
                    </MainCard>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：知识库录入页面
 * 描述
 */
@addon(' KnowledgeEditView', '知识库录入页面', '提示')
@reactControl(KnowledgeEditView, true)
export class KnowledgeEditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}