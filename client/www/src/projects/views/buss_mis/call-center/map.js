import React from 'react';
import { InfoWindow, Map, Marker } from 'react-bmap';

export function BaiduMap({ lng = 113.14958, lat = 23.035191, zoom = 17, older_data /** = { name: '', phone: '', emergency: 'no' } */ }) {
    const fontSize = 16;
    return (
        <Map
            amapkey={"jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji"}
            center={{ lng: lng, lat: lat }}
            zoom={zoom}
            enableDragging={true}
            // enableContinuousZoom={true}
            enableScrollWheelZoom={true}
        >
            {
                older_data ?
                    older_data.address?
                        <InfoWindow
                            position={{ lng: lng, lat: lat }}
                        >
                            {/* TODO: 获取实时地址 */}
                            <div style={{ fontSize: fontSize }}>{older_data.name}</div>
                            <div style={{ fontSize: fontSize }}>{older_data.address}</div>
                            {
                                older_data.emergency === "yes" ?
                                    <span style={{ fontSize: 32, color: "red" }}>SOS</span>
                                    :
                                    undefined
                            }
                        </InfoWindow>
                        :
                        undefined
                    :
                    undefined
            }
            <Marker position={{ lng: lng, lat: lat }} />
        </Map>);
}