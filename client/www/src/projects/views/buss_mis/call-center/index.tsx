import { Button, Card, Col, DatePicker, Icon, Input, Mentions, message, Modal, Row, Select, Table, Tabs } from 'antd';
import { WhiteSpace } from "antd-mobile";
import { addon, createObject, log } from "pao-aop";
import { CookieUtil, reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from 'src/business/components/style-components/main-card';
import { MainContent } from "src/business/components/style-components/main-content";
import { COOKIE_OLDER_CALL_CENTER } from 'src/business/mainForm/backstageManageMainForm';
import { encodeUrlParam, idCardCheck, nameCheck, telPhoneCheck } from 'src/business/util_tool';
import { AppServiceUtility } from "src/projects/app/appService";
import { IntelligentElderlyCareAppStorage } from 'src/projects/app/appStorage';
import { changeNameforStatus, decodeUrlParam } from "src/projects/app/util-tool";
import { HomeCard } from "src/projects/components/home-card";
import { ServiceOrder } from 'src/projects/models/service-operation';
import { ROUTE_PATH } from "src/projects/router";
import { ServiceOrderViewControl } from '../service-operation-manage/service-order-manage';
import { ServiceRecordViewControl } from '../service-operation-manage/service-record-manage';
import './index.less';
import { BaiduMap } from './map';

const { TextArea } = Input;
const { Option } = Select;
const { TabPane } = Tabs;
/**
 * 组件：呼叫中心页面
 */
export interface CallCenterViewState extends ReactViewState {
    showState?: any;
    Infodata?: any;
    record_data?: any;
    caller?: any;
    type?: any;
    create_time?: any;
    cs_name?: any;
    edit_data?: any;
    note?: any;
    fullScrean?: boolean;
    remarkText?: string;
    url?: any;
    luyinState?: any;
    older_list: any[];
    ser_order_list: any[];
    ser_record_list: any[];
    tab_select_older?: ServiceOrder;
    on_duty_info: any;
}

/**
 * 组件：呼叫中心页面
 * 描述
 */
export class CallCenterView extends ReactView<CallCenterViewControl, CallCenterViewState> {
    callRecordColumns: any = [
        {
            title: '主叫电话',
            dataIndex: 'caller',
            key: 'caller',
            width: 100,
        },
        {
            title: '被叫电话',
            dataIndex: 'called',
            key: 'called',
            width: 100,
        },
        {
            title: '呼叫类型',
            dataIndex: 'call_type',
            key: 'call_type',
            width: 100,
        },
        {
            title: '紧急呼叫',
            dataIndex: 'emergency',
            key: 'emergency',
            width: 100,
            render: (text: any, record: any) => {
                if (record.emergency === 'yes') {
                    return '是';
                } else {
                    return '否';
                }
            },
        },
        {
            title: '是否接通',
            dataIndex: 'pickup',
            key: 'pickup',
            width: 100,
            render: (text: any, record: any) => {
                if (record.pickup === true) {
                    return '接通';
                } else {
                    return <span style={{ color: 'red' }}>未接通</span>;
                }
            },
        },
        {
            title: '呼叫对象',
            dataIndex: 'call_name',
            key: 'call_name',
            width: 100,
        },
        {
            title: '接听时间（分钟）',
            dataIndex: 'call_time',
            key: 'call_time',
            width: 100,
        },
        {
            title: '进入系统时间',
            dataIndex: 'create_time',
            key: 'create_time',
            width: 100,
        },
        {
            title: '接听人员',
            dataIndex: 'cs_name',
            key: 'cs_name',
            width: 100,
        },
        {
            title: '录音播放',
            dataIndex: 'sound_play',
            key: 'sound_play',
            render: (text: any, record: any) => (
                <Icon
                    type="play-square"
                    theme="twoTone"
                    style={{ fontSize: '30px' }}
                    onClick={
                        () => {
                            if (record.call_time === '0') {
                                message.warn('该通话记录无录音');
                            } else {
                                this.luyin(record.call_id);
                            }
                        }
                    }
                />
            ),
            width: 100,
        },
        {
            title: '备注',
            dataIndex: 'note',
            key: 'note',
            width: 100,
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                // TODO: 编辑框确定后点击确定，没刷新列表备注
                return <Button type="primary" onClick={() => { this.edit(record.key); }}>编辑</Button>;
            },
            width: 150,
        },
    ];
    serverOrderColumns = [
        {
            title: '订单编号',
            dataIndex: 'order_code',
            key: 'order_code',
        }, {
            title: '采购人',
            dataIndex: 'purchaser',
            key: 'purchaser',
        }, {
            title: '服务商',
            dataIndex: 'service_provider',
            key: 'service_provider',
        }, {
            title: '订单内容',
            dataIndex: 'origin_product.product_name',
            key: 'origin_product.product_name',
        }, {
            title: '订单时间',
            dataIndex: 'order_date',
            key: 'order_date',
        }, {
            title: '服务时间',
            dataIndex: 'service_date',
            key: 'service_date',
        }, {
            title: '付款时间',
            dataIndex: 'pay_date',
            key: 'pay_date',
        }, {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render: (text: string, record: any, index: any) => {
                return (
                    changeNameforStatus(text)
                );
            }
        }, {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
        }
    ];

    serverRecordColumns = [
        {
            title: '服务订单编号',
            dataIndex: 'service_order_code',
            key: 'service_order_code',
        }, {
            title: '服务项目',
            dataIndex: 'task_name',
            key: 'task_name',
        }, {
            title: '服务开始时间',
            dataIndex: 'start_date',
            key: 'start_date',
        }, {
            title: '服务完成时间',
            dataIndex: 'end_date',
            key: 'end_date',
        }, {
            title: '服务人员',
            dataIndex: 'service_worker_id',
            key: 'service_worker_id',
        }, {
            title: '服务状态',
            dataIndex: 'task_state',
            key: 'task_state',
        }, {
            title: '计价金额',
            dataIndex: 'valuation_amount',
            key: 'valuation_amount',
        }, {
            title: '是否已回访',
            dataIndex: 'visit_status',
            key: 'visit_status',
        },
    ];
    constructor(props: CallCenterViewControl) {
        super(props);
        this.state = {
            showState: false,
            Infodata: {},
            record_data: [],
            caller: '',
            type: '',
            create_time: '',
            cs_name: '',
            edit_data: {},
            note: '',
            fullScrean: false,
            remarkText: "",
            url: '',
            luyinState: false,
            older_list: [],
            ser_order_list: [],
            ser_record_list: [],
            tab_select_older: undefined,
            on_duty_info: undefined,
        };
    }

    getRecordList = (condition: any, needLoading: boolean) => {
        const server = needLoading ? AppServiceUtility.emi_service_with_loading! : AppServiceUtility.emi_service_no_loading!;
        server.get_call_record_list!(condition)!
            .then((data: any) => {
                // this.setState({
                //     record_data: data.result
                // });
                if (data.result.length > 0) {
                    let record_data: any = [];
                    data.result.map((item: any) => {
                        record_data.push(
                            {
                                key: item.id,
                                caller: item.caller,
                                called: item.called,
                                call_type: item.call_type ? item.call_type === '5' ? '呼入' : '呼出' : '',
                                call_name: item.elder ? item.elder.name : '',
                                call_time: item.duration ? (item.duration / 60).toFixed(2) : '0',
                                create_time: item.create_time,
                                cs_name: item.cs_name,
                                emergency: item.emergency,
                                note: item.note,
                                call_id: item.call_id,
                                action: item.id,
                                pickup: item.pickup
                            },
                        );
                        this.setState({
                            record_data
                        });
                    });
                } else {
                    this.setState({
                        record_data: []
                    });
                }
            });
    }

    getServOrderList = (condition: any) => {
        AppServiceUtility.service_operation_service!.get_service_order_list!(condition)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        ser_order_list: data.result
                    });
                } else {
                    this.setState({
                        ser_order_list: []
                    });
                }
            });
    }

    getServRecordList = (condition: any) => {
        AppServiceUtility.service_record_service!.get_service_record_list_all!(condition)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        ser_record_list: data.result
                    });
                } else {
                    this.setState({
                        ser_record_list: []
                    });
                }
            });
    }

    componentDidMount = () => {
        const that = this;
        this.props.addEventHandler!('call', () => {
            that.getRecordList({ user_id: IntelligentElderlyCareAppStorage.getCurrentUser().id }, true);
        });

        if (this.props.match!.params.key !== undefined) {
            let Infodata = decodeUrlParam(this.props.match!.params.key);
            this.setState({
                Infodata: Infodata ? JSON.parse(Infodata) : {}
            });
        } else {
            const Infodata = CookieUtil.read(COOKIE_OLDER_CALL_CENTER);
            if (Infodata) {
                this.setState({
                    Infodata
                });
            } else {
                // TODO: 获取坐席人员位置信息
                AppServiceUtility.emi_service_no_loading!.get_cs_org_location!()!
                    .then(e => {
                        // console.log(e);

                        this.setState({
                            on_duty_info: e
                        });
                    })
                    .catch(e => {
                        console.error(e);
                    });
            }
        }
        this.getRecordList({ user_id: IntelligentElderlyCareAppStorage.getCurrentUser().id }, false);

        const mapEle = document.getElementById("olderMap")!;
        mapEle.addEventListener("fullscreenchange", function (e) {
            if (document.fullscreenElement === null) {
                that.setState({
                    fullScrean: false
                });
            }
        });
    }

    edit = (id: any) => {
        // console.log(id);
        // 根据传入的id调用接口,请求这条呼叫信息的相关信息
        AppServiceUtility.emi_service_no_loading!.get_call_record_list!({ id })!
            .then((data: any) => {
                // console.log('编辑的数据》》》》》》》》', data);
                this.setState({
                    edit_data: data.result[0],
                    note: data.result[0].note
                });
            });
        // 打开弹框
        this.setState({
            showState: true
        });
    }
    handleOk = () => {
        // 调用接口更新这条信息
        // console.log({ 'id': this.state.edit_data.id, 'note': this.state.note });
        AppServiceUtility.emi_service_no_loading!.update_one_call_record!({ 'id': this.state.edit_data.id, 'note': this.state.note })!
            .then((data: any) => {
                if (data) {
                    // 关闭弹框
                    this.getRecordList({ user_id: IntelligentElderlyCareAppStorage.getCurrentUser().id }, true);
                    this.setState({
                        showState: false
                    });
                }
            });
    }
    handleCancel = () => {
        this.setState({
            showState: false
        });
    }

    // 主叫号码
    nameChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            caller: e.target.value
        });
    }

    // 类型
    typeChange = (value: any) => {
        // console.log(value);
        this.setState({
            type: value
        });
    }

    // 时间
    timeChange = (date: any, dateString: any) => {
        // console.log(dateString);
        if (dateString) {
            let arr = dateString.split(' ');
            // console.log(arr);
            let time1 = arr[0].split('-');
            let time2 = arr[1].split(':');
            // console.log(time1, time2);
            let create_time = time1[0] + time1[1] + time1[2] + time2[0] + time2[1] + time2[2];
            // console.log(create_time);
            this.setState({
                create_time
            });
        }
    }
    // 人员
    personChange = (e: any) => {
        this.setState({
            cs_name: e.target.value
        });
    }
    // 查询
    lookup = () => {
        const { caller, type, create_time, cs_name } = this.state;
        let condition = {};
        if (caller !== '') {
            condition = { ...condition, 'caller': caller };
        }
        if (type !== '') {
            let type_new = '';
            if (type === '呼入') {
                type_new = 'in';
                condition = { ...condition, 'type': type_new };
            } else if (type === '呼出') {
                type_new = 'out';
                condition = { ...condition, 'type': type_new };
            } else if (type === 'SOS') {
                condition = { ...condition, emergency: 'yes' };
            }
        }
        if (create_time !== '') {
            condition = { ...condition, 'create_time': create_time };
        }
        if (cs_name !== '') {
            condition = { ...condition, 'cs_name': cs_name };
        }
        AppServiceUtility.emi_service_no_loading!.get_call_record_list!(condition)!
            .then((data: any) => {
                // console.log('根据条件查出来的数据》》》》》》》》》》》', data);
                if (data.result.length > 0) {
                    let record_data: any = [];
                    data.result.map((item: any) => {
                        record_data.push(
                            {
                                key: item.id,
                                caller: item.caller,
                                called: item.called,
                                call_type: item.call_type ? item.call_type === '5' ? '呼入' : '呼出' : '',
                                call_name: item.elder ? item.elder.name : "",
                                call_time: item.duration ? (item.duration / 60).toFixed(2) : '0',
                                create_time: item.create_time,
                                emergency: item.emergency,
                                cs_name: item.cs_name,
                                action: item.id,
                                pickup: item.pickup
                            },
                        );
                        this.setState({
                            record_data
                        });
                    });
                }

            });
    }
    // 重置
    clear = () => {
        location.reload();
    }

    // 备注
    noteChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            note: e.target.value
        });
    }
    hrefUrl = (url: string) => {
        this.props.history!.push(url);
    }
    // luyin
    luyin = (id: any) => {
        this.setState({
            luyinState: true
        });
        AppServiceUtility.emi_service_no_loading!.get_call_record_url!(id)!
            .then((data: any) => {
                // console.log(data);
                if (data) {
                    this.setState({
                        url: data.resp.callRecordUrl.url
                    });
                }
            });
        // window.location.href = 'https://ks3-cn-beijing.ksyun.com/yimi-record-1-year/1000_00023674_913106736801_test_13590547083_2_20191211_150747_150903_76_8713786.mp3?Expires=1576747582&response-content-type=application%2Foctet-stream&response-content-disposition=attachment%3B%20filename%3D1000_00023674_913106736801_test_13590547083_2_20191211_150747_150903_76_8713786.mp3&KSSAccessKeyId=AKLT0ChC2dqZRK6viMM8BwGZLQ&Signature=ijtedYRVsl1ARcSnbhy%2Bgg%2BvfJ8%3D';

    }

    showFull = () => {
        let full = document.getElementById("olderMap");
        if (full) {
            this.launchIntoFullscreen(full!);
            this.setState({
                fullScrean: true
            });
        }
    }

    delFull = () => {
        if (this.state.fullScrean === true) {
            this.exitFullscreen();
            this.setState({
                fullScrean: false
            });
        }
    }

    launchIntoFullscreen = (element: HTMLElement) => {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        }
    }

    exitFullscreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }

    onChangeRemark = (e: any) => {
        this.setState({ remarkText: e.target.value });
    }

    saveRemark = () => {
        // TODO:保存备注提示
        const { Infodata } = this.state;
        AppServiceUtility.emi_service_no_loading!.add_call_note!(Infodata['call_type'], Infodata['call_id'], this.state.remarkText!)!
            .then((data: any) => {
                // console.log(data);
                if (data === "Success") {
                    message.info('增加备注成功');
                }
            });
    }

    // 关闭弹框
    cancelTk = () => {
        this.setState({
            luyinState: false
        });
    }

    handleSearch = (e: any) => {
        let keyCode = e.keyCode;
        let value = e.target.value;
        let conditionCheck = false;
        if (keyCode === 13) {
            let condition = {};
            if (!value || (value.trim() === '')) {
                message.warn('请输入查询的长者姓名/电话/身份证');
                return;
            }

            if (idCardCheck(value)) {
                // message.info("身份证号");
                conditionCheck = true;
                condition['id_card'] = value;
            }
            if (telPhoneCheck(value)) {
                // message.info("号码");
                conditionCheck = true;
                condition['tel'] = value;
            }
            if (nameCheck(value)) {
                // message.info('姓名');
                conditionCheck = true;
                condition['name'] = value;
            }

            if (conditionCheck) {
                if (JSON.stringify(condition) !== "{}") {
                    AppServiceUtility.person_org_manage_service.get_personnel_elder!(condition)!
                        .then(e => {
                            if (e.result!.length === 0) {
                                message.info('未查到相关长者');
                            }
                            this.setState({
                                older_list: e.result!
                            });
                        });
                }
            } else {
                message.error('请输入正确的查询信息');
            }
        }
    }

    handleCallRecordSearch = (e: any) => {
        let keyCode = e.keyCode;
        let value = e.target.value;
        if (keyCode === 13) {
            let condition = {};
            if (!value || (value.trim() === '')) {
                return;
            }

            if (idCardCheck(value)) {
                // message.info("身份证号");
                condition['elder_card_number'] = value;
            }
            if (telPhoneCheck(value)) {
                // message.info("号码");
                condition['caller'] = value;
            }
            if (nameCheck(value)) {
                // message.info('姓名');
                condition['elder_name'] = value;
            }

            if (JSON.stringify(condition) !== "{}") {
                this.getRecordList(condition, true);
            }
        }
    }

    // 下载录音
    download = () => {
        window.location.href = this.state.url;
        this.setState({
            luyinState: false
        });
    }

    onOlderTabChange = (phoneNum_id: string) => {
        const [phoneNum, id] = phoneNum_id.split("_");
        console.warn(phoneNum, id);

        if (phoneNum !== '长者信息') {
            this.getRecordList({ caller: phoneNum }, false);
        } else {
            this.getRecordList({ user_id: IntelligentElderlyCareAppStorage.getCurrentUser().id }, false);
        }

        // if (id !== '') {
        //     this.getServOrderList({ purchaser_id: id });
        //     this.getServRecordList({ purchaser_id: id });
        // } else {

        // }
    }

    onEscKeyDown = (e: any) => {
        e.preventDefault();
        // console.log('onEscKeyDown');

        let keyCode = e.keyCode;
        if (keyCode === 27) {
            this.delFull();
        }
    }

    render() {
        const { Infodata, older_list, on_duty_info } = this.state;
        const { edit_data } = this.state;

        function getMapEle(elder_data: any) {
            if (elder_data.elder) {
                log("呼叫中心", "for debug");
                if (elder_data.elder['location'] !== undefined && elder_data.elder['location'] !== null) {
                    return (
                        <BaiduMap
                            lat={elder_data.elder['location']["lat"]}
                            lng={elder_data.elder['location']["lng"]}
                            older_data={{
                                address: elder_data!.elder['location'].address ? elder_data.elder['location'].address : '',
                                emergency: elder_data!['emergency']
                            }}
                        />
                    );
                }
            }
            if (on_duty_info) {
                return (
                    <BaiduMap
                        lng={on_duty_info.lng}
                        lat={on_duty_info.lat}
                        older_data={
                            {

                                address: on_duty_info!.name ? on_duty_info.name : '',
                                emergency: false
                            }
                        }
                    />
                );
            }
            return (
                <BaiduMap
                    older_data={
                        elder_data!.elder ?
                            {
                                address: elder_data!.elder.address ? elder_data.elder.address : '',
                                emergency: elder_data!['emergency']
                            }
                            : undefined
                    }
                />
            );
        }

        const older_id_param = Infodata['elder'] ? Infodata['elder']['elder_id'] ? '/' + encodeUrlParam(Infodata['elder']['elder_id']) : '' : '';

        const menudata = [
            {
                url: ROUTE_PATH.emergencyCall,
                icon: <Icon type="alert" theme="filled" />,
                text: '紧急呼叫',
                permission: '紧急呼叫'
            },
            {
                url: ROUTE_PATH.knowledgeBase,
                icon: <Icon type="folder-open" theme="filled" />,
                text: '知识库',
                permission: '知识库'
            },
            {
                url: ROUTE_PATH.buyService + older_id_param,
                icon: <Icon type="shop" theme="filled" />,
                text: '下单',
                permission: '下单'
            },
            {
                url: ROUTE_PATH.elderInfo,
                icon: <Icon type="menu-unfold" />,
                text: '长者信息',
                permission: '长者信息'
            },
        ];

        const remarkElement = (Infodata: any) => {
            // // console.log(Infodata);
            // // console.log(JSON.stringify(Infodata));
            // // console.log(JSON.stringify(Infodata) !== '{}');

            if (JSON.stringify(Infodata) !== '{}') {
                return (
                    <>
                        <Input.TextArea onChange={this.onChangeRemark} />
                        <Button onClick={this.saveRemark}>保存备注</Button>
                    </>
                );
            }
            return undefined;
        };

        // const elder_info_ele = (Infodata: any, needRemark: boolean) => {
        //     return (
        //         <Row>
        //             <Col span={6}>
        //                 <div style={{ textAlign: 'center' }} >
        //                     <img
        //                         src={
        //                             (() => {
        //                                 if (Infodata.elder) {
        //                                     if (Infodata.elder.pic !== "") {
        //                                         return Infodata.elder.pic;
        //                                     }
        //                                 }
        //                                 return require("./img/older.jpg");
        //                             })()
        //                         }
        //                         alt=""
        //                         style={{ width: '200px', height: '200px' }}
        //                     />
        //                 </div>
        //                 {
        //                     needRemark ?
        //                         remarkElement(Infodata)
        //                         :
        //                         undefined
        //                 }
        //                 {/* <Input.TextArea onChange={this.onChangeRemark} />
        //                         <Button onClick={this.saveRemark}>保存备注</Button> */}
        //             </Col>
        //             {Infodata.elder ?
        //                 <Col span={6}>
        //                     <Card title="长者信息">
        //                         <p>姓名：<span>{Infodata.elder.name ? Infodata.elder.name : '空'}</span></p>
        //                         <p>性别：<span>{Infodata.elder.sex ? Infodata.elder.sex : '空'}</span></p>
        //                         <p>年龄：<span>{Infodata.elder.card_number ? new Date().getFullYear() - Infodata.elder.card_number.slice(6, 10) : '暂无相关信息'}</span></p>
        //                         <p>家庭地址：<span>{Infodata.elder.address ? Infodata.elder.address : '空'}</span></p>
        //                         <p>电话号码：<span>{Infodata.elder.tel ? Infodata.elder.tel : '空'}</span></p>
        //                         <br />
        //                         <p>证件类型：<span>{Infodata.elder.idCardType ? Infodata.elder.idCardType : '空'}</span></p>
        //                         <p>证件号码：<span>{Infodata.elder.cardNumber ? Infodata.elder.cardNumber : '空'}</span></p>
        //                     </Card>
        //                 </Col>
        //                 :
        //                 <Col span={6}>
        //                     <Card title="长者信息">
        //                         <p>未知号码，查找不到相关长者信息，请登记</p>
        //                     </Card>
        //                 </Col>
        //             }
        //             <Col span={10} >
        //                 <div id={'olderMap'} className={this.state.fullScrean ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
        //                     {
        //                         !(this.state.fullScrean)
        //                         &&
        //                         <Button
        //                             style={{
        //                                 position: 'absolute',
        //                                 zIndex: 1,
        //                                 right: 2,
        //                                 top: 1
        //                             }}
        //                             shape="circle"
        //                             icon="fullscreen"
        //                             onClick={() => {
        //                                 this.showFull();
        //                             }}
        //                         />
        //                     }
        //                     {getMapEle(Infodata)}
        //                 </div>
        //             </Col>
        //         </Row>
        //     );
        // };

        const elder_info_ele = (Infodata: any, needRemark: boolean) => {
            if (JSON.stringify(Infodata) === "{}" || Infodata.elder) {
                return (
                    <TabPane tab={"长者信息"} key="长者信息_" closable={false}>
                        <Row>
                            <Col span={6}>
                                <div style={{ textAlign: 'center' }} >
                                    <img
                                        src={
                                            (() => {
                                                if (Infodata.elder) {
                                                    if (Infodata.elder.pic !== "") {
                                                        return Infodata.elder.pic;
                                                    }
                                                }
                                                return require("./img/older.jpg");
                                            })()
                                        }
                                        alt=""
                                        style={{ width: '200px', height: '200px' }}
                                    />
                                </div>
                                {
                                    needRemark ?
                                        remarkElement(Infodata)
                                        :
                                        undefined
                                }
                                {/* <Input.TextArea onChange={this.onChangeRemark} />
                                    <Button onClick={this.saveRemark}>保存备注</Button> */}
                            </Col>
                            {Infodata.elder ?
                                <Col span={6}>
                                    <Card title="长者信息">
                                        <p>姓名：<span>{Infodata.elder.name ? Infodata.elder.name : '空'}</span></p>
                                        <p>性别：<span>{Infodata.elder.sex ? Infodata.elder.sex : '空'}</span></p>
                                        <p>年龄：<span>{Infodata.elder.card_number ? new Date().getFullYear() - Infodata.elder.card_number.slice(6, 10) : '暂无相关信息'}</span></p>
                                        <p>家庭地址：<span>{Infodata.elder.address ? Infodata.elder.address : '空'}</span></p>
                                        <p>电话号码：<span>{Infodata.elder.tel ? Infodata.elder.tel : '空'}</span></p>
                                        <br />
                                        <p>证件类型：<span>{Infodata.elder.idCardType ? Infodata.elder.idCardType : '空'}</span></p>
                                        <p>证件号码：<span>{Infodata.elder.cardNumber ? Infodata.elder.cardNumber : '空'}</span></p>
                                    </Card>
                                </Col>
                                :
                                <Col span={6}>
                                    <Card title="长者信息">
                                        <p>未知号码，查找不到相关长者信息，请登记</p>
                                    </Card>
                                </Col>
                            }
                            <Col span={10} >
                                <div id={'olderMap'} className={this.state.fullScrean ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
                                    {
                                        !(this.state.fullScrean)
                                        &&
                                        <Button
                                            style={{
                                                position: 'absolute',
                                                zIndex: 1,
                                                right: 2,
                                                top: 1
                                            }}
                                            shape="circle"
                                            icon="fullscreen"
                                            onClick={() => {
                                                this.showFull();
                                            }}
                                        />
                                    }
                                    {getMapEle(Infodata)}
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                );
            } else {
                if (Infodata.guardian) {
                    return Infodata.guardian.elder.map((e: any, i: number) => {
                        return (
                            <TabPane tab={e.name} key={`${e.name}`} closable={false}>
                                <Row>
                                    <Col span={6}>
                                        <div style={{ textAlign: 'center' }} >
                                            <img
                                                src={
                                                    (() => {
                                                        if (e.pic !== "") {
                                                            return e.pic;
                                                        }
                                                        return require("./img/older.jpg");
                                                    })()
                                                }
                                                alt=""
                                                style={{ width: '200px', height: '200px' }}
                                            />
                                        </div>
                                        {
                                            needRemark ?
                                                remarkElement(Infodata)
                                                :
                                                undefined
                                        }
                                        {/* <Input.TextArea onChange={this.onChangeRemark} />
                                        <Button onClick={this.saveRemark}>保存备注</Button> */}
                                    </Col>
                                    <Col span={6}>
                                        <Card title="长者信息">
                                            <p>姓名：<span>{e.name ? e.name : '空'}</span></p>
                                            <p>性别：<span>{e.sex ? e.sex : '空'}</span></p>
                                            <p>年龄：<span>{e.card_number ? new Date().getFullYear() - e.card_number.slice(6, 10) : '暂无相关信息'}</span></p>
                                            <p>家庭地址：<span>{e.address ? e.address : '空'}</span></p>
                                            <p>电话号码：<span>{e.tel ? e.tel : '空'}</span></p>
                                            <br />
                                            <p>证件类型：<span>{e.idCardType ? e.idCardType : '空'}</span></p>
                                            <p>证件号码：<span>{e.cardNumber ? e.cardNumber : '空'}</span></p>
                                        </Card>
                                        <Col>
                                            <p>紧急联系人信息：</p>
                                            <span>{Infodata.guardian.guardian_name}：{Infodata.guardian.guardian_telephone}</span>
                                        </Col>
                                    </Col>
                                    <Col span={10} >
                                        <div id={'olderMap'} className={this.state.fullScrean ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
                                            {
                                                !(this.state.fullScrean)
                                                &&
                                                <Button
                                                    style={{
                                                        position: 'absolute',
                                                        zIndex: 1,
                                                        right: 2,
                                                        top: 1
                                                    }}
                                                    shape="circle"
                                                    icon="fullscreen"
                                                    onClick={() => {
                                                        this.showFull();
                                                    }}
                                                />
                                            }
                                            {getMapEle({ elder: e })}
                                        </div>
                                    </Col>
                                </Row>
                            </TabPane>
                        );
                    });
                }
            }
        };

        return (
            <div className='call-center' style={{ position: 'relative' }}>
                <MainContent>
                    <Row type={"flex"} justify={"end"}>
                        <Col span={6}>
                            <Input placeholder="长者姓名/电话/身份证" onKeyDown={this.handleSearch} />
                        </Col>
                    </Row>
                    <Row gutter={20}>
                        {
                            menudata!.map((item: any, index: number) => {
                                return (
                                    <Col span={4} key={index} onClick={this.hrefUrl.bind(this, item.url)} style={{ width: '250px' }}>
                                        <HomeCard>
                                            {item.icon}
                                            <strong>{item.text}</strong>
                                        </HomeCard>
                                    </Col>
                                );
                            })
                        }
                    </Row>
                    <Tabs type="editable-card" hideAdd={true} onChange={this.onOlderTabChange}>
                        {elder_info_ele(Infodata, true)}
                        {
                            older_list.map((e, i) => {
                                const Infodata = {
                                    elder: {
                                        name: e.name,
                                        sex: e.personnel_info.sex,
                                        card_number: e.personnel_info.id_card,
                                        address: e.personnel_info["address"] ? e.personnel_info["address"] : undefined,
                                        id_card_type: e.id_card_type,
                                        cardNumber: e.personnel_info.id_card,
                                        location: undefined,
                                        tel: e.personnel_info.telephone
                                    }
                                };
                                return (
                                    <TabPane tab={e.name} key={`${e.personnel_info.telephone}_${e.id}`} closable={false}>
                                        {elder_info_ele(Infodata, false)}
                                    </TabPane>
                                );
                            })
                        }
                    </Tabs>
                    <Tabs type="card" style={{ marginTop: 20 }}>
                        <TabPane tab={"通话记录"} key="通话记录">
                            <MainCard>
                                {/* TODO:搜索有问题 */}
                                主叫人：<Input style={{ width: '15%' }} placeholder="长者姓名/电话/身份证" onKeyDown={this.handleCallRecordSearch} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                {/* 主叫号码：<Input placeholder='主叫号码' style={{ width: '15%' }} onChange={this.nameChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
                                进入系统时间： <DatePicker showTime={true} placeholder="选择时间" style={{ width: '15%' }} onChange={this.timeChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                呼叫类型：<Select style={{ width: '15%' }} onChange={this.typeChange}>
                                    <Option value="呼出">呼出</Option>
                                    <Option value="呼入">呼入</Option>
                                    <Option value="SOS">SOS</Option>
                                </Select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                接听人员：<Input placeholder='接听人员姓名' style={{ width: '15%' }} onChange={this.personChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <WhiteSpace size="lg" />
                                <div style={{ overflow: 'hidden' }}>
                                    <Button style={{ float: 'right' }} onClick={this.clear}>重置</Button>
                                    <Button type="primary" style={{ float: 'right', marginRight: '10px' }} onClick={this.lookup}>查询</Button>
                                </div>
                                <WhiteSpace size="lg" />
                                <Table columns={this.callRecordColumns} dataSource={this.state.record_data} pagination={{ pageSize: 3 }} />
                            </MainCard>
                        </TabPane>
                        <TabPane style={{ padding: 20 }} tab={"服务订单"} key="服务订单">
                            {/* <Table columns={this.serverOrderColumns} dataSource={this.state.ser_order_list} pagination={{ pageSize: 3 }} /> */}
                            {
                                createObject(
                                    ServiceOrderViewControl,
                                    {
                                        request_url: 'get_service_order_fwyy_fwdd_list',
                                        permission_class: AppServiceUtility.login_service,
                                        get_permission_name: 'get_function_list',
                                        select_permission: '坐席人员用户管理查看',
                                    })!.createElement!()
                            }
                        </TabPane>
                        <TabPane style={{ padding: 20 }} tab={"服务记录"} key="服务记录">
                            {/* <Table columns={this.serverRecordColumns} dataSource={this.state.ser_record_list} pagination={{ pageSize: 3 }} /> */}
                            {
                                createObject(
                                    ServiceRecordViewControl,
                                    {
                                        permission_class: AppServiceUtility.login_service,
                                        get_permission_name: 'get_function_list',
                                        request_url: 'get_service_record_list_all',
                                        select_permission: '服务记录查看',
                                    })!.createElement!()
                            }
                        </TabPane>
                    </Tabs>
                    {/* <MainCard title='通话记录'>
                        主叫号码：<Input placeholder='主叫号码' style={{ width: '15%' }} onChange={this.nameChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        进入系统时间： <DatePicker showTime={true} placeholder="选择时间" style={{ width: '15%' }} onChange={this.timeChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        呼叫类型：<Select style={{ width: '15%' }} onChange={this.typeChange}>
                            <Option value="呼出">呼出</Option>
                            <Option value="呼入">呼入</Option>
                        </Select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        接听人员：<Input placeholder='接听人员姓名' style={{ width: '15%' }} onChange={this.personChange} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <WhiteSpace size="lg" />
                        <div style={{ overflow: 'hidden' }}>
                            <Button style={{ float: 'right' }} onClick={this.clear}>重置</Button>
                            <Button type="primary" style={{ float: 'right', marginRight: '10px' }} onClick={this.lookup}>查询</Button>
                        </div>
                        <WhiteSpace size="lg" />
                        <Table columns={this.columns} dataSource={this.state.record_data} pagination={{ pageSize: 3 }} />
                    </MainCard> */}
                    <Modal
                        title="编辑信息"
                        cancelText='取消'
                        okText='确认'
                        visible={this.state.showState}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        width='400px'
                    >
                        主叫号码：<Mentions style={{ width: '260px' }} value={edit_data.caller} readOnly={true} />
                        {/* <Input style={{ width: '260px' }} value={edit_data.caller} /><br /> */}
                        <WhiteSpace size="sm" />
                        被叫号码：<Mentions style={{ width: '260px' }} value={edit_data.called} readOnly={true} />
                        {/* <Input style={{ width: '260px' }} value={edit_data.called} /><br /> */}
                        <WhiteSpace size="sm" />
                        呼叫类型：<Mentions style={{ width: '260px' }} value={edit_data.call_type === '5' ? '呼入' : '呼出'} readOnly={true} />
                        {/* <Select style={{ width: '260px' }} value={edit_data.call_type === '5' ? '呼入' : '呼出'}>
                            <Option value="呼叫">呼叫</Option>
                            <Option value="紧急呼叫">紧急呼叫</Option>
                        </Select><br /> */}
                        <WhiteSpace size="sm" />
                        开始时间：<Mentions style={{ width: '260px' }} value={edit_data.start_time} readOnly={true} />
                        {/* <DatePicker showTime={true} placeholder="选择时分秒" style={{ width: '260px' }} value={edit_data.start_time} /><br /> */}
                        <WhiteSpace size="sm" />
                        结束时间：<Mentions style={{ width: '260px' }} value={edit_data.stop_time} readOnly={true} />
                        {/* <DatePicker showTime={true} placeholder="选择时分秒" style={{ width: '260px' }} value={edit_data.stop_time} /><br /> */}
                        <WhiteSpace size="sm" />
                        时长（分钟）： <Mentions style={{ width: '230px' }} value={String(edit_data.stop_time && edit_data.start_time ? (((new Date(edit_data.stop_time).getTime() - new Date(edit_data.start_time).getTime()) % 3600000) / 60000).toFixed(2) : 0)} readOnly={true} />
                        {/* <Input style={{ width: '200px' }} value={((new Date(edit_data.stop_time).getTime() - new Date(edit_data.start_time).getTime()) % 3600000) / 60000} /><br /> */}
                        <WhiteSpace size="sm" />
                        接听人员：<Mentions style={{ width: '260px' }} value={edit_data.cs_name} readOnly={true} />
                        {/* <Input style={{ width: '260px' }} value={edit_data.cs_name} /><br /> */}
                        <WhiteSpace size="sm" />
                        录音播放：<Icon type="play-square" theme="twoTone" style={{ fontSize: '30px' }} onClick={() => { this.luyin(edit_data.call_id); }} /><br />
                        <WhiteSpace size="sm" />
                        备注：<TextArea rows={4} style={{ width: '260px' }} onChange={this.noteChange} value={this.state.note} /><br />
                        <WhiteSpace size="sm" />
                    </Modal>
                </MainContent>
                <Modal
                    title="录音播放"
                    visible={this.state.luyinState}
                    onOk={this.download}
                    okText='下载'
                    cancelText='取消'
                    style={{ textAlign: 'center' }}
                    onCancel={this.cancelTk}
                    zIndex={1999}
                >
                    <audio controls={true} src={this.state.url}>
                        你的浏览器不支持录音播放
                    </audio>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：呼叫中心页面
 * 描述
 */
@addon(' CallCenterView', '呼叫中心页面', '提示')
@reactControl(CallCenterView, true)
export class CallCenterViewControl extends ReactViewControl {
    handleCall?= () => {
        this.fire!('call', undefined, true);
    }
}