import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { Button } from 'antd';
import { ROUTE_PATH } from 'src/projects/router';
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：知识库详情页面
 */
export interface KnowledgeDetailsViewState extends ReactViewState {
    data?: any;
}

/**
 * 组件：知识库详情页面
 * 描述
 */
export class KnowledgeDetailsView extends ReactView<KnowledgeDetailsViewControl, KnowledgeDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: []
        };
    }
    componentDidMount = () => {
        let id = this.props.match!.params.key;
        // console.log(id);
        // 根据id查找
        AppServiceUtility.knowledge_service!.get_knowledge_list!({ id })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    document.getElementsByClassName('content')[0].innerHTML = data.result[0].content!;
                }
            });
    }
    // 返回
    goBack = () => {
        this.props.history!.push(ROUTE_PATH.knowledgeBase);
    }
    // 呼叫中心
    goCenter = () => {
        this.props.history!.push(ROUTE_PATH.callCenter);
    }
    render() {
        return (
            <div>
                <MainContent>
                    <MainCard title='知识库' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goCenter}>呼叫中心</Button>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goBack}>返回</Button>
                        </div>
                        <div className='content'/>
                    </MainCard>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：知识库详情页面
 * 描述
 */
@addon('KnowledgeDetailsView', '知识库详情页面', '提示')
@reactControl(KnowledgeDetailsView, true)
export class KnowledgeDetailsViewControl extends ReactViewControl {
}