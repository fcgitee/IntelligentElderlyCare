import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { ROUTE_PATH } from 'src/projects/router';
import { Table, Button, Popconfirm, Input, Select, message } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
const { Option } = Select;
const { Search } = Input;
/**
 * 组件：知识库列表页面页面
 */
export interface KnowledgeBaseListViewState extends ReactViewState {
    type?: any;
    data?: any;
}

/**
 * 组件：知识库列表页面页面
 * 描述
 */
export class KnowledgeBaseListView extends ReactView<KnowledgeBaseListViewControl, KnowledgeBaseListViewState> {
    columns: any = [
        {
            title: '标题',
            dataIndex: 'title',
            key: 'title',
            width: 300,
            render: (text: any, record: any) => <a href={'/knowledge-details' + '/' + record.key}>{text}</a>,
        },
        {
            title: '类型',
            dataIndex: 'type',
            key: 'type',
            width: 100,
        },
        {
            title: '创建人',
            dataIndex: 'create_people',
            key: 'create_people',
            width: 100,
        },
        {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
            width: 100,
        },
        {
            title: '修改人',
            dataIndex: 'modify_people',
            key: 'modify_people',
            width: 100,
        },
        {
            title: '修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
            width: 100,
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => (
                <div>
                    <Button type="primary" size="small" onClick={() => { this.edit(record.key); }}>编辑</Button>&nbsp;
                    <Popconfirm
                        title="你确定要删除吗?"
                        okText="确定"
                        cancelText="取消"
                        onConfirm={() => { this.delete(record.key); }}
                        onCancel={this.cancel}
                    >
                        <Button type="danger" size="small">删除</Button>
                    </Popconfirm>
                </div>
            ),
            width: 150,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            type: ["全部", "居家服务"],
            data: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.knowledge_service!.get_knowledge_list!({})!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let data_new: any = [];
                    data.result.map((item: any) => {
                        data_new.push(
                            {
                                key: item.id,
                                title: item.title,
                                type: item.type,
                                create_people: item.create_person_name,
                                create_date: item.create_date,
                                modify_people: item.update_person_name ? item.update_person_name : '',
                                modify_date: item.modify_date
                            }
                        );
                    });
                    this.setState({
                        data: data_new
                    });
                }
            });
    }
    // 编辑
    edit = (id: any) => {
        this.props.history!.push(ROUTE_PATH.knowledgeEdit + '/' + id);
    }
    // 确认删除
    delete = (id: any) => {
        // console.log(id);
        AppServiceUtility.knowledge_service!.del_knowledge!(id)!
            .then((data: any) => {
                // console.log(data);
                if (data === 'Success') {
                    message.success('删除成功！！！', 3);
                    location.reload();
                }
            });
    }
    // 取消
    cancel = () => {
        // console.log('取消');
    }
    // 呼叫中心
    goCenter = () => {
        this.props.history!.push(ROUTE_PATH.callCenter);
    }
    // 新增
    goEdit = () => {
        this.props.history!.push(ROUTE_PATH.knowledgeEdit);
    }
    // 输入框搜索
    inputSearch = (value: any) => {
        // console.log(value);
        // 请求后台接口，拿到数据
        AppServiceUtility.knowledge_service!.get_knowledge_list!({ 'word': value })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let data_new: any = [];
                    data.result.map((item: any) => {
                        data_new.push(
                            {
                                key: item.id,
                                title: item.title,
                                type: item.type,
                                create_people: item.create_person_name,
                                create_date: item.create_date,
                                modify_people: item.update_person_name ? item.update_person_name : '',
                                modify_date: item.modify_date
                            }
                        );
                    });
                    this.setState({
                        data: data_new
                    });
                }
            });
    }
    // 类型查询
    selectSearch = (value: any) => {
        // console.log(value);
        // 请求后台接口，拿到数据
        let condition = {};
        if (value === '全部') {
            condition = {};
        } else {
            condition = { 'type': value };
        }
        AppServiceUtility.knowledge_service!.get_knowledge_list!(condition)!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let data_new: any = [];
                    data.result.map((item: any) => {
                        data_new.push(
                            {
                                key: item.id,
                                title: item.title,
                                type: item.type,
                                create_people: item.create_person_name,
                                create_date: item.create_date,
                                modify_people: item.update_person_name ? item.update_person_name : '',
                                modify_date: item.modify_date
                            }
                        );
                    });
                    this.setState({
                        data: data_new
                    });
                }
            });
    }
    render() {
        return (
            <div>
                <MainContent>
                    <MainCard title='知识库' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '10px', right: '15px' }}>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goCenter}>呼叫中心</Button>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goEdit}>新增</Button>
                            <Select defaultValue="全部" style={{ float: 'right', width: 120, marginRight: '3px' }} onChange={this.selectSearch}>
                                {this.state.type.map((item: any) => {
                                    return <Option value={item} key={item}>{item}</Option>;
                                })}
                            </Select>
                            <Search
                                placeholder="标题/关键词"
                                onSearch={(value: any) => { this.inputSearch(value); }}
                                style={{ float: 'right', width: '150px', marginRight: '3px' }}
                            />
                        </div>
                        <Table columns={this.columns} dataSource={this.state.data} pagination={{ pageSize: 10 }} />
                    </MainCard>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：知识库列表页面页面
 * 描述
 */
@addon(' KnowledgeBaseListView', '知识库列表页面页面', '提示')
@reactControl(KnowledgeBaseListView, true)
export class KnowledgeBaseListViewControl extends ReactViewControl {
}