import { Button, DatePicker, Input, Select, Table } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from 'src/projects/router';

const { Search } = Input;
const { Option } = Select;

/**
 * 组件：紧急呼叫页面
 */
export interface EmergencyCallViewState extends ReactViewState {
    type?: any;
    record_data?: any;
}

/**
 * 组件：紧急呼叫页面
 * 描述
 */
export class EmergencyCallView extends ReactView<EmergencyCallViewControl, EmergencyCallViewState> {
    columns: any = [
        {
            title: '社区',
            dataIndex: 'shequ',
            key: 'shequ',
            width: 300,
        },
        {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
            width: 100,
        },
        {
            title: '呼救号码',
            dataIndex: 'phoneNum',
            key: 'phoneNum',
            width: 150,
        },
        {
            title: '呼救时间',
            dataIndex: 'time',
            key: 'time',
            width: 100,
        },
        {
            title: '接通坐席号',
            dataIndex: 'pickphoneNum',
            key: 'pickphoneNum',
            width: 100,
        },
        {
            title: '呼叫来源',
            dataIndex: 'laiyuan',
            key: 'laiyuan',
            width: 100,
        },
        {
            title: '处理时间',
            dataIndex: 'clTime',
            key: 'clTime',
            width: 100,
        },
        {
            title: '处理状态',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: '处理人',
            dataIndex: 'clPeople',
            key: 'clPeople',
            width: 100,
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => (
                <Button type="primary" size="small" onClick={() => { this.goDetails(record.key); }}>详情</Button>
            ),
            width: 150,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            type: ["全部", "居家服务"],
            record_data: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.emi_service_no_loading!.get_emergency_call_record_list!({})!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    let record_data: any = [];
                    data.result.map((item: any) => {
                        record_data.push(
                            {
                                key: item.id,
                                shequ: item.cs_name,
                                name: item.elder.name,
                                phoneNum: item.caller,
                                time: item.start_time,
                                pickphoneNum: item.called,
                                laiyuan: item.call_type,
                                clTime: item.stop_time,
                                clState: item.type,
                                clPeople: item.cs_name
                            },
                        );
                        this.setState({
                            record_data
                        });
                    });
                }

            });
    }
    // 详情
    goDetails = (id: any) => {
        this.props.history!.push(ROUTE_PATH.emergencyCallDetails + '/' + id);
    }
    // 返回
    goBack = () => {
        this.props.history!.push(ROUTE_PATH.knowledgeBase);
    }
    // 呼叫中心
    goCenter = () => {
        this.props.history!.push(ROUTE_PATH.callCenter);
    }
    // 输入框搜索
    inputSearch = (value: any) => {
        // console.log(value);
        // 请求后台接口，拿到数据
    }
    // 类型查询
    selectSearch = (value: any) => {
        // console.log(value);
        // 请求后台接口，拿到数据
    }
    // 时间
    timeChange = (date: any, dateString: any) => {
        // console.log(dateString);
        // if (dateString) {
        //     let arr = dateString.split(' ');
        //     // console.log(arr);
        //     let time1 = arr[0].split('-');
        //     let time2 = arr[1].split(':');
        //     // console.log(time1, time2);
        //     let create_time = time1[0] + time1[1] + time1[2] + time2[0] + time2[1] + time2[2];
        // console.log(create_time);
        // 请求后台接口，拿到数据
        // }
    }
    render() {
        return (
            <div>
                <MainContent>
                    <MainCard title='紧急呼叫' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goCenter}>呼叫中心</Button>
                            <DatePicker showTime={true} placeholder="选择时间" onChange={this.timeChange} style={{ float: 'right', width: 120, marginRight: '3px' }} />
                            <Select defaultValue="全部" style={{ float: 'right', width: 120, marginRight: '3px' }} onChange={this.selectSearch}>
                                {this.state.type.map((item: any) => {
                                    return <Option value={item} key={item}>{item}</Option>;
                                })}
                            </Select>
                            <Search
                                placeholder="长者姓名"
                                onSearch={(value: any) => { this.inputSearch(value); }}
                                style={{ float: 'right', width: '150px', marginRight: '3px' }}
                            />
                        </div>
                        <Table columns={this.columns} dataSource={this.state.record_data} pagination={{ pageSize: 10 }} />
                    </MainCard>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：紧急呼叫页面
 * 描述
 */
@addon(' EmergencyCallView', '紧急呼叫页面', '提示')
@reactControl(EmergencyCallView, true)
export class EmergencyCallViewControl extends ReactViewControl {
}