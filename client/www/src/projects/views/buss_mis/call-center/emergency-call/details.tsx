import { Button, Card, Descriptions, List, Table, Tabs } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from 'src/projects/router';
const { TabPane } = Tabs;
/**
 * 组件：紧急呼叫详情页面
 */
export interface EmergencyCallDetailsViewState extends ReactViewState {
    data?: any;
}

/**
 * 组件：紧急呼叫详情页面
 * 描述
 */
export class EmergencyCallDetailsView extends ReactView<EmergencyCallDetailsViewControl, EmergencyCallDetailsViewState> {
    columns1: any = [
        {
            title: '与长者关系',
            dataIndex: 'caller',
            key: 'caller',
            width: 200,
        },
        {
            title: '姓名',
            dataIndex: 'called',
            key: 'called',
            width: 200,
        },
        {
            title: '是否为紧急联系人',
            dataIndex: 'call_type',
            key: 'call_type',
            width: 200,
        },
        {
            title: '电话',
            dataIndex: 'call_name',
            key: 'call_name',
            width: 200,
        },
        {
            title: '固定电话',
            dataIndex: 'call_time',
            key: 'call_time',
            width: 200,
        },
    ];
    columns2: any = [
        {
            title: '处理人',
            dataIndex: 'caller',
            key: 'caller',
            width: 200,
        },
        {
            title: '处理时间',
            dataIndex: 'called',
            key: 'called',
            width: 200,
        },
        {
            title: '内容',
            dataIndex: 'call_type',
            key: 'call_type',
            width: 200,
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => (
                <Button type="danger">删除</Button>
            ),
            width: 150,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            data: []
        };
    }
    componentDidMount = () => {
        let id = this.props.match!.params.key;
        AppServiceUtility.emi_service_no_loading!.get_emergency_call_record_list!({ id })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        data: data.result[0]
                    });
                }
            });
    }
    // 返回
    goBack = () => {
        this.props.history!.push(ROUTE_PATH.emergencyCall);
    }
    // 呼叫中心
    goCenter = () => {
        this.props.history!.push(ROUTE_PATH.callCenter);
    }
    render() {
        const data = [
            {
                title: '姓名',
                value: this.state.data.elder ? this.state.data.elder.name : '暂无相关信息'
            },
            {
                title: '是否紧急呼叫',
                value: this.state.data.emergency ? this.state.data.emergency === 'yes' ? '是' : '否' : '暂无相关信息'
            },
            {
                title: '手机号码',
                value: this.state.data.caller ? this.state.data.caller : '暂无相关数据'
            },
            {
                title: '性别',
                value: this.state.data.elder ? this.state.data.elder.sex : '暂无相关信息'
            },
            {
                title: '年龄',
                value: this.state.data.elder ? new Date().getFullYear() - this.state.data.elder.card_number.slice(6, 10) : '暂无相关信息'
            },
            {
                title: '固定电话',
                value: this.state.data.caller ? this.state.data.caller : '暂无相关数据'
            },
        ];
        return (
            <div>
                <MainContent>
                    <MainCard title='紧急呼叫' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goCenter}>呼叫中心</Button>
                            <Button type="primary" style={{ float: 'right', marginRight: '3px' }} onClick={this.goBack}>返回</Button>
                        </div>
                    </MainCard>
                    <MainCard>
                        <Descriptions layout="vertical" bordered={true} style={{ textAlign: 'center' }}>
                            <Descriptions.Item label="紧急呼叫号码">{this.state.data.caller}</Descriptions.Item>
                            <Descriptions.Item label="呼救时间">{this.state.data.start_time}</Descriptions.Item>
                            <Descriptions.Item label="地址">{this.state.data.elder ? this.state.data.elder.address : '暂无相关信息'}</Descriptions.Item>
                        </Descriptions>
                    </MainCard>
                    <MainCard >
                        <div style={{ background: '#ECECEC', padding: '30px' }}>
                            <Tabs type="card">
                                <TabPane tab={this.state.data.elder ? this.state.data.elder.name : '暂无相关信息'} key="1">
                                    <List
                                        grid={{ gutter: 30, column: 3 }}
                                        dataSource={data}
                                        renderItem={item => (
                                            <List.Item>
                                                <Card title={item.title}>{item.value}</Card>
                                            </List.Item>
                                        )}
                                    />
                                </TabPane>
                            </Tabs>
                        </div>
                    </MainCard>
                    <MainCard title='联系人信息'>
                        <Table columns={this.columns1} pagination={{ pageSize: 3 }} />
                    </MainCard>
                    <MainCard title='处理日志'>
                        <Table columns={this.columns2} pagination={{ pageSize: 3 }} />
                    </MainCard>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：紧急呼叫详情页面
 * 描述
 */
@addon(' EmergencyCallDetailsView', '紧急呼叫详情页面', '提示')
@reactControl(EmergencyCallDetailsView, true)
export class EmergencyCallDetailsViewControl extends ReactViewControl {
}