
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Button, Table, DatePicker, Modal, Form, Radio, LocaleProvider, Select, Row, Col, message } from "antd";
import { AppServiceUtility } from 'src/projects/app/appService';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
const { MonthPicker } = DatePicker;
import moment from 'moment';
import { request } from "src/business/util_tool";
import { exprot_excel } from "src/business/util_tool";
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import { remote } from "src/projects/remote";
const Option = Select.Option;
let columns: any = [
    // {
    //     title: "序号",
    //     dataIndex: 'index_no',
    //     key: 'index_no',
    //     width: 100,
    // },
    {
        title: "社区",
        dataIndex: 'social_worker',
        key: 'social_worker',
        width: 100,
    },
    {
        title: '姓名',
        dataIndex: 'user_name',
        key: 'user_name',
        width: 100,
    },
    {
        title: '联系地址',
        dataIndex: 'user_address',
        key: 'user_address',
        width: 150,
    },
    {
        title: '类别',
        dataIndex: 'classification',
        key: 'classification',
        width: 150,
    },
    {
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
        width: 100,
    },
    {
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
        width: 100,
    },
    {
        title: '服务开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
        width: 100,
    },
    {
        title: '服务结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
        width: 100,
    },
    {
        title: '服务项目',
        dataIndex: 'item_type_name',
        key: 'item_type_name',
        width: 100,
    },
    {
        title: '服务标准',
        dataIndex: 'item_name',
        key: 'item_name',
        width: 100,
    },
    {
        title: '价格(元)',
        dataIndex: 'price',
        key: 'price',
        width: 100,
    },
    {
        title: '账户类型',
        dataIndex: 'buy_type',
        key: 'buy_type',
        width: 100,
    }
];
/** 打印table列 */
let print_columns: any = [
    {
        title: "序号",
        dataIndex: 'index_no',
        key: 'index_no',
        align: 'center',
        width: 50,
        render: (text: any, record: any, index: number) => {
            if (record.service_provider_name !== '合计') {
                return index + 1;
            } else {
                return '';
            }
        }
    },
    {
        title: '姓名',
        dataIndex: 'user_name',
        key: 'user_name',
        align: 'center',
        width: 80,
    },
    {
        title: '联系地址',
        dataIndex: 'user_address',
        key: 'user_address',
        align: 'center',
        width: 100,
    },
    {
        title: '类别',
        dataIndex: 'classification',
        key: 'classification',
        align: 'center',
        width: 50,
    },
    {
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
        align: 'center',
        width: 130,
    },
    {
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
        align: 'center',
        width: 50,
    },
    {
        title: '服务开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
        align: 'center',
        width: 100,
    },
    {
        title: '服务结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
        align: 'center',
        width: 100,
    },
    {
        title: '服务项目',
        dataIndex: 'item_type_name',
        key: 'item_type_name',
        align: 'center',
        width: 150,
    },
    {
        title: '服务标准',
        dataIndex: 'item_name',
        key: 'item_name',
        align: 'center',
        width: 150,
    },
    {
        title: '金额(元)',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        width: 80,
    },
    {
        title: '支付方式',
        dataIndex: 'buy_type',
        key: 'buy_type',
        align: 'center',
        width: 100,
    }
];
/** 状态：服务记录结算组件数据 */
export interface serviceRecordSettlementViewState extends ReactViewState {
    /** 明细数据 */
    dataSource: any[];
    /** 日期条件 */
    query_month?: any;
    /** 控制弹框显示 */
    show?: any;
    /** 导出的条件 */
    select_type?: any;
    /** 服务商列表 */
    org_list: any[];
    /** 选择的服务商Id */
    org_id: string;
    /** 选择的服务商中文 */
    org_name: string;
    /** 当前页 */
    current_page?: number;
    /** 每页数 */
    pageSize?: number;
    /** 总页数 */
    total_pages?: number;
    /** 数据总数 */
    total_data?: number;
    /** 社工局数组 */
    social_worker_list?: any[];
    /** 所选社工局 */
    social_worker_id?: string;
    /** 所选社工局中文 */
    social_worker_name?: string;
    /** 所选账户类型 */
    buy_type?: string;
    /** 打印状态 */
    preview_modal?: boolean;
    /** 打印内容数据 */
    data_source_print?: any[];

}
/** 组件：服务记录结算组件视图 */
export class serviceRecordSettlementView extends React.Component<serviceRecordSettlementViewControl, serviceRecordSettlementViewState> {
    constructor(props: serviceRecordSettlementViewControl) {
        super(props);
        this.state = {
            dataSource: [],
            select_type: 1,
            show: false,
            query_month: moment().format('YYYY/MM'),
            org_list: [],
            org_id: '',
            org_name: '',
            social_worker_list: [],
            social_worker_id: '',
            current_page: 1,
            pageSize: 10,
            social_worker_name: '',
            buy_type: '补贴账户',
            preview_modal: false,
            data_source_print: [],
        };
    }
    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }
    /** 打印 */
    print = () => {
        let { query_month, org_id, org_name, social_worker_id, social_worker_name, buy_type } = this.state;
        if (query_month) {
            let month = query_month.split('/').join('-');
            let orgId = undefined;
            let socialWorkerId = undefined;
            let buyType = undefined;
            let dy_condition = { 'month': query_month };
            if (org_id) {
                dy_condition['org_id'] = org_id;
                orgId = org_id;
            }
            if (social_worker_id) {
                dy_condition['social_worker_id'] = social_worker_id;
                socialWorkerId = social_worker_id;
            }
            if (buy_type) {
                dy_condition['buy_type'] = buy_type;
                buyType = buy_type;
            }
            // 跳转新页面打开打印预览
            window.open(remote.path + '/service-record-detail/' + month + '/' + orgId + '/' + org_name + '/' + socialWorkerId + '/' + social_worker_name + '/' + buyType);
            // request(this, AppServiceUtility.report_management.get_service_provider_record_settlement!(dy_condition))!
            //     .then((res: any) => {
            //         console.log('res.result>>', res.total);
            //         this.setState(
            //             {
            //                 preview_modal: true,
            //                 data_source_print: res.result
            //             },
            //             () => {
            //                 setTimeout(
            //                     () => {
            //                         document.body.style.visibility = "hidden";
            //                         if (document.querySelector('#root')) {
            //                             document.querySelector('#root')!['style'].height = "0%";
            //                         }
            //                         if (document.querySelector('.ant-modal-wrap')) {
            //                             document.querySelector('.ant-modal-wrap')!.className = 'ant-modal-wrap2';
            //                         }
            //                         // document.body.innerHTML = document.querySelector('.hideAll')!.innerHTML;
            //                         window.print();
            //                         document.body.style.visibility = "";
            //                         this.setState({
            //                             preview_modal: false,
            //                         });
            //                         if (document.querySelector('#root')) {
            //                             document.querySelector('#root')!['style'].height = "100%";
            //                         }
            //                         if (document.querySelector('.ant-modal-wrap2')) {
            //                             document.querySelector('.ant-modal-wrap2')!.className = 'ant-modal-wrap';
            //                         }
            //                     },
            //                     300
            //                 );
            //             }
            //         );
            //     });
        } else {
            message.info('需要选择日期！');
        }
    }
    /** 查询方法 */
    search = () => {
        let condition = {};
        if (this.state.buy_type) {
            condition['buy_type'] = this.state.buy_type;
        }
        if (this.state.query_month) {
            condition['month'] = this.state.query_month;
        }
        if (this.state.social_worker_id) {
            condition['social_worker_id'] = this.state.social_worker_id;
        }
        if (this.state.org_id) {
            condition['org_id'] = this.state.org_id;
        }
        this.select_func(condition);
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // let export_data: any = [];
        // 全部
        if (select_type === 1) {
            let condition = {};
            if (this.state.query_month) {
                condition['month'] = this.state.query_month;
            }
            if (this.state.org_id) {
                condition['org_id'] = this.state.org_id;
            }
            if (this.state.social_worker_id) {
                condition['social_worker_id'] = this.state.social_worker_id;
            }
            if (this.state.buy_type) {
                condition['buy_type'] = this.state.buy_type;
            }
            request(this, AppServiceUtility.report_management.get_service_provider_record_settlement!(condition))!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "社区": item.social_worker,
                                "姓名": item.user_name ? item.user_name : '',
                                "联系地址": item.user_address,
                                "类别": item.classification,
                                "服务商": item.service_provider_name,
                                "订单编号": item.order_code,
                                "服务开始时间": item.start_date,
                                "服务结束时间": item.end_date,
                                "服务项目": item.item_type_name,
                                "服务标准": item.item_name,
                                "价格(元)": item.price,
                                "购买类型": item.buy_type,
                            });
                        });
                        exprot_excel([{ name: '服务记录结算', value: new_data }], '服务记录结算', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    componentWillMount() {
        (async () => {
            // 查询社工局列表
            await request(this, AppServiceUtility.person_org_manage_service.get_organization_all_list!({ 'personnel_category': '街道' }))!
                .then((data: any) => {
                    this.setState({
                        social_worker_list: data!.result!,
                        social_worker_id: data!.result!.length > 0 ? data!.result![0]['id'] : '',
                        social_worker_name: data!.result!.length > 0 ? data!.result![0]['name'] : ''
                    });
                })
                .catch(err => {
                    console.info(err);
                });
            // 查询服务商列表
            await request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ 'personnel_category': '服务商', 'contract_status': '签约' }))!
                .then((data: any) => {
                    this.setState({
                        org_list: data!.result!,
                        org_id: data!.result!.length > 0 ? data!.result![0]['id'] : '',
                        org_name: data!.result!.length > 0 ? data!.result![0]['name'] : ''
                    });
                })
                .catch(err => {
                    console.info(err);
                });
            // await this.select_func({ 'month': moment().format('YYYY/MM'), 'org_id': this.state.org_id, 'social_worker_id': this.state.social_worker_id, buy_type: this.state.buy_type });
            await this.search();
        })();
    }

    /**
     * 查询方法
     */
    private select_func = (condition: {}, current_page?: number, pageSize?: number) => {
        request(this, AppServiceUtility.report_management.get_service_provider_record_settlement!(condition, current_page ? current_page : this.state.current_page, pageSize ? pageSize : this.state.pageSize))!
            .then((res: any) => {
                this.setState({
                    dataSource: res.result,
                    total_data: res.total
                });
            });
    }
    /** 机构树形选择框改变事件 */
    onChangeTree = (value: any, option: any) => {
        this.setState({
            org_id: value,
            org_name: option && option.props ? option.props.children : ''
        });
        // let condition = { 'org_id': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // this.select_func(condition);
    }
    /** 账户类型选择框改变事件 */
    onChangeBuyType = (value: any) => {
        this.setState({
            buy_type: value
        });
        // let condition = { 'buy_type': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // this.select_func(condition);
    }
    /** 社工局下拉框改变事件 */
    onChangeSelect = (value: any, option: any) => {
        this.setState({
            social_worker_id: value,
            social_worker_name: option && option.props ? option.props.children : ''
        });
        // let condition = { 'social_worker_id': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // this.select_func(condition);
    }
    /** 日期范围回调 */
    time_change = (date: any, dateString: string) => {
        this.setState({
            query_month: dateString
        });
        // let condition = { 'month': dateString };
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // this.select_func(condition);
    }
    /** 分页改变回调事件 */
    pageOnClick = (page: any, pageSize: any) => {
        this.setState(
            {
                current_page: page
            },
            () => {
                this.search();
            }
        );
        // let condition = {};
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // this.select_func(condition, page);

    }
    /** 改变分页数量回调事件 */
    onShowSizeChange = (current: number, pageSize: number) => {
        this.setState(
            {
                pageSize: pageSize,
                current_page: current,
                total_pages: Math.ceil((this.state.total_data ? this.state.total_data : 0) / pageSize)
            },
            () => {
                this.search();
            }
        );
        // let condition = {};
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // this.select_func(condition, current, pageSize);
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        const { preview_modal, query_month, social_worker_name, buy_type, org_name, data_source_print } = this.state;
        // console.log('data_source_print>>>', data_source_print);
        return (
            <MainContent>
                <LocaleProvider locale={zh_CN}>
                    <MainCard title='服务记录结算' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            {/* <Button type="primary" onClick={this.export} style={{ float: 'right', marginRight: '3px' }}>导出</Button> */}
                            <Button type="primary" onClick={this.print} style={{ float: 'right', marginRight: '3px' }}>打印</Button>
                            <Button type="primary" onClick={this.search} style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                            <div style={{ float: 'right', marginRight: '3px' }}>
                                <MonthPicker onChange={this.time_change} defaultValue={moment(moment(), 'YYYY/MM')} format={'YYYY/MM'} />
                            </div>
                            <div style={{ float: 'right', marginRight: '10px' }}>
                                <Select
                                    showSearch={true}
                                    // allowClear={true}
                                    onChange={this.onChangeBuyType}
                                    placeholder={"请选择账户类型"}
                                    style={{
                                        width: '12em'
                                    }}
                                    value={this.state.buy_type}
                                >
                                    {['自费', '补贴账户', '慈善账户'].map((item: any) => <Option key={item} value={item}>{item}</Option>)}
                                </Select>
                            </div>
                            <div style={{ float: 'right', marginRight: '10px' }}>
                                <Select
                                    showSearch={true}
                                    allowClear={true}
                                    onChange={this.onChangeTree}
                                    placeholder={"请选择服务商"}
                                    style={{
                                        width: '16em'
                                    }}
                                    value={this.state.org_id}
                                >
                                    {this.state.org_list!.length > 0 ? this.state.org_list!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>) : null}
                                </Select>
                            </div>
                            <div style={{ float: 'right', marginRight: '10px' }}>
                                <Select
                                    showSearch={true}
                                    allowClear={true}
                                    onChange={this.onChangeSelect}
                                    placeholder={"请选择社工局"}
                                    style={{
                                        width: '12em'
                                    }}
                                    value={this.state.social_worker_id}
                                >
                                    {this.state.social_worker_list!.length > 0 ? this.state.social_worker_list!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>) : null}
                                </Select>
                            </div>
                        </div>
                        <Table
                            columns={columns}
                            dataSource={this.state.dataSource}
                            pagination={{
                                // pageSize: 10,
                                showQuickJumper: true,
                                showSizeChanger: true,
                                onShowSizeChange: this.onShowSizeChange,
                                onChange: this.pageOnClick,
                                defaultPageSize: this.state.pageSize,
                                current: this.state.current_page,
                                total: this.state.total_data
                            }}
                            bordered={true}
                            size="middle"
                        />
                    </MainCard>
                    <Modal
                        title="打印预览"
                        width="100%"
                        visible={preview_modal}
                        okText={'打印'}
                        // onOk={previewing ? () => this.print() : () => this.set()}
                        // onCancel={() => this.handleCancel()}
                        className={'hideAll'}
                    >
                        <Row>
                            <Col>
                                <div style={{ fontSize: '18px', fontWeight: 'bold', textAlign: 'center', margin: '0 auto' }}>南海区智慧养老综合服务管理平台</div>
                            </Col>
                            <Col>
                                <div style={{ fontSize: '18px', fontWeight: 'bold', textAlign: 'center', margin: '0 auto' }}>服务综合报表</div>
                            </Col>
                            <Col style={{ marginTop: '8px' }}>
                                <div style={{ fontSize: '12px', textAlign: 'center', margin: '0 auto' }}>日期：{query_month}</div>
                            </Col>
                            <Col style={{ margin: '10px 0px 10px 0px' }}>
                                <div style={{ fontSize: '12px', fontWeight: 'bold', }}>所属镇街:{social_worker_name}</div>
                            </Col>
                            <Col style={{ margin: '10px 0px 10px 0px' }}>
                                <div style={{ fontSize: '12px', fontWeight: 'bold', }}>服务商名称:{org_name}</div>
                            </Col>
                            <Col style={{ margin: '10px 0px 10px 0px' }}>
                                <div style={{ fontSize: '8px', fontWeight: 'bold', }}>资金来源:{buy_type}</div>
                            </Col>
                            <Col style={{ margin: '10px 0px 10px 0px' }}>
                                <div style={{ fontSize: '8px', fontWeight: 'bold', }}>完成情况:已确认</div>
                            </Col>
                        </Row>
                        <Table style={{ pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }} className='table-detail' pagination={false} columns={print_columns} dataSource={data_source_print} bordered={true} size="middle" />
                        <Row style={{ pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                            <Col style={{ pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                                <div style={{ fontSize: '10px', fontWeight: 'bold', }}>注：1、以上数据经服务商确认无误后于五个工作日盖章回传，我司据以上报政府主管部门作支付依据。</div>
                            </Col>
                            <Col style={{ pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                                <div style={{ fontSize: '10px', fontWeight: 'bold', }}>2、此报表一式3份，运营商、服务商、政府相关部门各一份。</div>
                            </Col>
                            <Col style={{ marginTop: '10px', pageBreakAfter: 'avoid' }}>
                                <p style={{ fontSize: '12px', display: 'inline' }}>经办人:</p><p style={{ fontSize: '12px', marginLeft: '100px', display: 'inline' }}>审核:</p>
                            </Col>
                            <Col style={{ pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                                <p style={{ fontSize: '12px', fontWeight: 'bold', display: 'inline' }}>服务商确认：经核对，以上数据准确无误。</p><p style={{ fontSize: '12px', marginLeft: '145px', display: 'inline' }}>平台运营方： 广东壹佰健大健康科技有限公司</p>
                            </Col>
                            <Col style={{ marginTop: '10px', pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                                <p style={{ fontSize: '12px', display: 'inline' }}>服务商盖章：</p><p style={{ fontSize: '12px', marginLeft: '300px', display: 'inline' }}>运营商盖章：</p>
                            </Col>
                            <Col style={{ marginTop: '10px', pageBreakAfter: 'avoid', pageBreakBefore: 'avoid' }}>
                                <p style={{ fontSize: '12px', display: 'inline' }}>日期：</p><p style={{ fontSize: '12px', marginLeft: '335px', display: 'inline' }}>日期：</p>
                            </Col>
                        </Row>
                    </Modal>
                    <Modal
                        title="请选择导出类型"
                        visible={this.state.show}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ show: !this.state.show })}
                        okText="下载"
                        cancelText="取消"
                    >
                        <MainCard>
                            <Form>
                                <Form.Item>
                                    {getFieldDecorator('isAll', {
                                        initialValue: ''
                                    })(
                                        <Radio.Group onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                            <Radio key={1} value={1}>导出数据</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            </Form>
                        </MainCard >
                    </Modal>
                </LocaleProvider>
            </MainContent>
        );
    }
}

/**
 * 控件：服务记录结算组件控制器
 * @description 服务记录结算组件
 * @author
 */
@addon('serviceRecordSettlementView', '服务记录结算组件', '服务记录结算组件')
@reactControl(Form.create<any>()(serviceRecordSettlementView), true)
export class serviceRecordSettlementViewControl extends ReactViewControl {
}