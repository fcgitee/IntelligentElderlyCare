import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Tree, Button, Row, Col, Input } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
/**
 * 组件：行政区划列表状态
 */
export interface AdministrationDivisionViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 树形结构 */
    tree?: any;
    // 行政区划名字
    name?: any;
}

/**
 * 组件：行政区划列表
 * 描述
 */
export class AdministrationDivisionView extends ReactView<AdministrationDivisionViewControl, AdministrationDivisionViewState> {
    constructor(props: AdministrationDivisionViewControl) {
        super(props);
        this.state = {
            request_url: '',
            tree: [],
            name: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeAdministrationDivision);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: any, contents: any) => {
        console.info(type);
        this.props.history!.push(ROUTE_PATH.changeAdministrationDivision + '/' + type[0]);
    }
    componentWillMount() {
        this.select({});
    }
    nameChange(e: any) {
        let name: any = e.target.value;
        this.setState(
            {
                name,
            }
        );
    }
    select(param: any = false) {
        let request_url = this.props.request_url;
        param = param || { name: this.state.name };
        AppServiceUtility.business_area_service[request_url!](param)!
            .then((data: any) => {
                if (data.result) {
                    this.setState({
                        tree: data.result,
                    });
                }
            });
    }
    render() {
        return (
            <MainContent>
                <MainCard title={'行政区划'}>
                    <Row>
                        <Col span={4}>
                            <Button type={'primary'} onClick={this.add}>新增行政区划</Button>
                        </Col>
                        <Col span={16}>
                            <Input placeholder="请输入行政区划名字" onChange={(e: any) => this.nameChange(e)} />
                        </Col>
                        <Col span={4}>
                            <Button type={'primary'} onClick={() => this.select()}>查询</Button>
                        </Col>
                    </Row>

                    <MainContent>
                        <Tree
                            defaultExpandAll={true}
                            showIcon={true}
                            treeData={this.state.tree}
                            onSelect={this.onIconClick}
                        />
                    </MainContent>
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 行政区划列表页面
 */
@addon('AdministrationDivisionView', '行政区划列表页面', '描述')
@reactControl(AdministrationDivisionView, true)
export class AdministrationDivisionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 