import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { Modal, message, Select } from "antd";
let { Option } = Select;

/**
 * 状态：行政区划新增页面
 */
export interface ChangeAdministrationDivisionViewState extends ReactViewState {
    /** 行政区划列表 */
    admin_division_list?: [];
    /** 对方话框 */
    visible?: boolean;
}

/**
 * 组件：行政区划新增页面视图
 */
export class ChangeAdministrationDivisionView extends ReactView<ChangeAdministrationDivisionViewControl, ChangeAdministrationDivisionViewState> {
    constructor(props: ChangeAdministrationDivisionViewControl) {
        super(props);
        this.state = {
            admin_division_list: [],
            visible: false
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.business_area_service.get_admin_division_list!({ 'is_no_myself': this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    admin_division_list: datas.result,
                });
            });
    }
    handleOk = () => {
        AppServiceUtility.business_area_service.del_admin_division!([this.props.match!.params.key!])!
            .then(data => {
                if (data) {
                    message.info('删除成功');
                    this.props.history!.push(ROUTE_PATH.administrationDivision);
                    this.setState({
                        visible: false,
                    });
                }
            });
    }
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const type = ['市', '区', '镇', '办事处', '管理处', '社区', '村'];
        const type_list: JSX.Element[] = type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        let edit_props: any = {
            form_items_props: [{
                title: "行政区划信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "行政区划名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入行政区划名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入行政区划名称"
                        }
                    },
                    {
                        type: InputType.select,
                        label: "行政区划类型",
                        decorator_id: "type",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选行政区划类型" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择行政区划类型",
                            childrens: type_list
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "英文名称",
                        decorator_id: "english_name",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入英文名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入英文名称"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "编号",
                        decorator_id: "code",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入英文名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入编号"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "简称",
                        decorator_id: "short_name",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入简称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入简称"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "首府",
                        decorator_id: "capital",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入首府" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入首府"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "经度",
                        decorator_id: "location.lon",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入经度" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入经度"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "纬度",
                        decorator_id: "location.lat",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入纬度" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入纬度"
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "行政区划级别",
                        decorator_id: "division_level",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入行政区划级别" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入行政区划级别"
                        }
                    }, {
                        type: InputType.tree_select,
                        label: "上级行政区划",
                        decorator_id: "parent_id",
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            showSearch: true,
                            treeNodeFilterProp: 'title',
                            dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                            allowClear: true,
                            treeDefaultExpandAll: true,
                            treeData: this.state.admin_division_list,
                            placeholder: "请选择行政区划",
                        },
                    }, {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remarks",
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注",
                            rows: 3
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    history.back();
                    // this.props.history!.push(ROUTE_PATH.administrationDivision);
                }
            },],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.business_area_service,
                operation_option: {
                    query: {
                        func_name: "get_admin_division_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_admin_division"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.administrationDivision); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        if (this.props.match!.params.key) {
            let del_btn = {
                text: "删除",
                cb: () => {
                    this.setState({
                        visible: true,
                    });
                }
            };
            edit_props_list.other_btn_propps.push(del_btn);
        }
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
                <Modal
                    title="温馨提示"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    确定要删除该行政区划吗？
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：行政区划新增页面控件
 * @description 行政区划新增页面控件
 * @author
 */
@addon('ChangeAdministrationDivisionViewControl', '行政区划新增页面控件', '行政区划新增页面控件')
@reactControl(ChangeAdministrationDivisionView, true)
export class ChangeAdministrationDivisionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}