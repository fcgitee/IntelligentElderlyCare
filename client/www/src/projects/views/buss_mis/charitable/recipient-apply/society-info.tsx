import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, Descriptions, } from "antd";
import "../charitable-donate/index.less";
/**
 * 组件：慈善会信息状态
 */
export class SocietyInfoState {
}

/**
 * 组件：慈善会信息
 * 慈善会信息
 */
export class SocietyInfo extends React.Component<SocietyInfoControl, SocietyInfoState> {

    componentWillMount() {
        this.setState({
            data_info: this.props.data_info || [],
        });
    }
    render() {
        let { data_info } = this.props;

        // 慈善会
        // 初始化，抑制报错
        if (!data_info) {
            data_info = [];
        }
        // 名字
        data_info!['name'] = data_info!['name'] || "";
        // 描述
        data_info!['description'] = data_info!['description'] || "";
        // 图片
        data_info!['picture'] = data_info!['picture'] || "";
        // 证书吧
        data_info!['certificate'] = data_info!['certificate'] || "";

        const picCls = {
            maxWidth: '200px',
        };
        return (
            <Card className="cshxx">
                <Descriptions title="慈善会信息">
                    <Descriptions.Item label="慈善会名称" span={3}>{data_info['name']}</Descriptions.Item>
                    <Descriptions.Item label="慈善会描述" span={3}>{data_info['description']}</Descriptions.Item>
                    <Descriptions.Item label="慈善会图片" span={3}>
                        {
                            data_info['picture'] && data_info['picture'].length > 0 ? data_info['picture'].map((item: any, index: number) => {
                                return (
                                    <div key={index}>
                                        <img src={item} alt={item} style={picCls} />
                                    </div>
                                );
                            }) : ''
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label="慈善会募捐证书" span={3}>
                        {
                            data_info['certificate'] && data_info['certificate'].length > 0 ? data_info['certificate'].map((item: any, index: number) => {
                                return (
                                    <div key={index}>
                                        <img src={item} alt={item} style={picCls} />
                                    </div>
                                );
                            }) : ''
                        }
                    </Descriptions.Item>
                </Descriptions>
            </Card>
        );
    }
}

/**
 * 控件：慈善会信息控制器
 * 慈善会信息
 */
@addon('SocietyInfo', '慈善会信息', '慈善会信息')
@reactControl(SocietyInfo, true)
export class SocietyInfoControl extends BaseReactElementControl {
    /** 基础数据 */
    data_info?: any;
}