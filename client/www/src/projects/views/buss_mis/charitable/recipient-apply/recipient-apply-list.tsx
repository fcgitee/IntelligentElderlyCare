import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import './index.less';

/**
 * 组件：个人募捐申请状态
 */
export interface RecipientApplyListViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：个人募捐申请
 * 个人募捐申请列表
 */
export class RecipientApplyListView extends ReactView<RecipientApplyListViewControl, RecipientApplyListViewState> {
    private columns_data_source = [{
        title: '申请名义',
        dataIndex: 'apply_name',
        key: 'apply_name',
    }, {
        title: '申请金额',
        dataIndex: 'apply_money',
        key: 'apply_money',
    }, {
        title: '申请类型',
        dataIndex: 'apply_type',
        key: 'apply_type',
    }, {
        title: '申请状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '申请时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    private table_params = {
        other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            request_url: '',
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.recipientApply);
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.recipientApply + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "申请名义",
                decorator_id: "apply_name"
            },
            {
                type: InputType.radioGroup,
                label: "申请类型",
                decorator_id: "apply_type",
                option: {
                    placeholder: "请选择申请类型",
                    options: [{
                        label: '个人',
                        value: '个人',
                    }, {
                        label: '机构',
                        value: '机构',
                    }]
                },
            },
            {
                type: InputType.radioGroup,
                label: "审核状态",
                decorator_id: "status",
                option: {
                    placeholder: "请选择项目状态",
                    options: [{
                        label: '待审批',
                        value: '待审批',
                    }, {
                        label: '通过',
                        value: '通过',
                    }, {
                        label: '不通过',
                        value: '不通过',
                    }]
                },
            }],
            btn_props: [],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let charitable_list = Object.assign(option, this.table_params);
        return (
            <Row>
                <SignFrameLayout {...charitable_list} />
            </Row>
        );
    }
}

/**
 * 控件：个人募捐申请
 * 个人募捐申请列表
 */
@addon('RecipientApplyListView', '个人募捐申请', '个人募捐申请列表')
@reactControl(RecipientApplyListView, true)
export class RecipientApplyListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}