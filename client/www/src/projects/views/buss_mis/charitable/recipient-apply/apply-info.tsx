import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Descriptions, } from "antd";
/**
 * 组件：申请信息状态
 */
export class ApplyInfoState {
}

/**
 * 组件：申请信息
 * 申请信息
 */
export class ApplyInfo extends React.Component<ApplyInfoControl, ApplyInfoState> {

    componentWillMount() {
    }
    render() {
        let { apply_info } = this.props;

        return (
            <Descriptions title="募捐申请信息明细" bordered={true}>
                {apply_info.hasOwnProperty('project_info') && apply_info['project_info'].length > 0 ? <Descriptions.Item label="申请基金名称">{apply_info.project_info[0]['donate_name']}</Descriptions.Item> : null}
                {(() => {
                    if (apply_info.apply_type === '个人') {
                        return (
                            <Descriptions.Item label="申请人">{apply_info.apply_name || ''}</Descriptions.Item>
                        );
                    } else {
                        return (
                            <Descriptions.Item label="申请机构">{apply_info.apply_name || ''}</Descriptions.Item>
                        );
                    }
                })()}
                {(() => {
                    if (apply_info.apply_type === '个人') {
                        return (
                            <Descriptions.Item label="身份证号码">{apply_info.id_card_no || ''}</Descriptions.Item>
                        );
                    }
                    return null;
                })()}
                <Descriptions.Item label="联系方式">{apply_info.contacts || ''}</Descriptions.Item>
                <Descriptions.Item label="申请金额">{apply_info.apply_money || ''}</Descriptions.Item>
                <Descriptions.Item label="银行卡帐号">{apply_info.account_number || ''}</Descriptions.Item>
                <Descriptions.Item label="详细地址">{apply_info.address || ''}</Descriptions.Item>
                <Descriptions.Item label="申请时间">{apply_info.create_date || ''}</Descriptions.Item>
                <Descriptions.Item label="申请原因">{apply_info.description || ''}</Descriptions.Item>
                {apply_info.beianhao ? <Descriptions.Item label="备案号">{apply_info.beianhao || ''}</Descriptions.Item> : null}
                {(() => {
                    if (apply_info.project_list && apply_info.project_list.length > 0) {
                        return apply_info.project_list.map((item: any, index: number) => {
                            if (item.ass_info.selete_type === "6") {
                                return (
                                    <Descriptions.Item key={index} label={item.ass_info.project_name}>
                                        {(() => {
                                            if (item.ass_info.value && item.ass_info.value[0]) {
                                                return (
                                                    <img
                                                        alt={item.ass_info.project_name}
                                                        src={item.ass_info.value[0]}
                                                    />
                                                );
                                            } else {
                                                return null;
                                            }
                                        })()}
                                    </Descriptions.Item>
                                );
                            } else {
                                return (
                                    <Descriptions.Item key={index} label={item.ass_info.project_name}>{item.ass_info.value || ''}</Descriptions.Item>
                                );
                            }
                        });
                    }
                    return null;
                })()}
            </Descriptions>
        );
    }
}

/**
 * 控件：申请信息控制器
 * 申请信息
 */
@addon('ApplyInfo', '申请信息', '申请信息')
@reactControl(ApplyInfo, true)
export class ApplyInfoControl extends BaseReactElementControl {
    /** 基础数据 */
    apply_info?: any;
}