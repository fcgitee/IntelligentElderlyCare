// import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
// import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Steps, Card, message } from 'antd';
import { request } from "../../../../../business/util_tool";
// import { remote } from 'src/projects/remote';
import "../charitable-donate/index.less";
import ApplyTemplateForm from "./apply-template";
import { SocietyInfo } from "./society-info";
const { Step } = Steps;

/**
 * 组件：机构募捐申请页面状态
 */
export interface OrganizationApplyState extends ReactViewState {
    /**
     * 申请状态
     */
    step?: number;
    /**
     * 表单权限控制
     */
    disabled?: boolean;
    /** 接口名 */
    request_url?: string;
    // 详情
    apply_info?: any;
    // 慈善会信息
    society_info?: any;
    // 募捐步骤
    apply_step: any;
}

/**
 * 组件：机构募捐申请页面
 * 机构募捐申请页面
 */
export class OrganizationApply extends ReactView<OrganizationApplyViewControl, OrganizationApplyState> {
    constructor(props: any) {
        super(props);
        this.state = {
            step: -2,
            disabled: false,
            request_url: '',
            society_info: [],
            apply_step: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {

        // 获取募捐步骤
        request(this, AppServiceUtility.charitable_service.get_recipient_step!({ 'step_type': '机构' }, 1, 1))
            .then((data: any) => {
                if (data) {
                    this.setState({
                        apply_step: data,
                    });
                } else {
                    message.error(`错误信息：${data}`);
                }
            })
            .catch(error => {
                message.error(error.message);
            });

        // 获取慈善会信息
        request(this, AppServiceUtility.charitable_service.get_charitable_society_list!({}, 1, 1))
            .then((data: any) => {
                if (data.result && data.result[0]) {
                    this.setState({
                        society_info: data.result[0] || [],
                    });
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    handleSubmit(err: any, values: any, child: any) {
        // 机构申请标识
        values['apply_type'] = '机构';
        // 保存
        request(this, AppServiceUtility.charitable_service.update_charitable_recipient_apply!(values))
            .then((data: any) => {
                if (data === 'Success') {
                    message.success(`提交成功！`, 3, () => {
                        child.props.pprops.history.push(ROUTE_PATH.recipientApplyList);
                    });
                } else {
                    message.error(`错误信息：${data}`);
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    render() {
        const { apply_step } = this.state;
        return (
            <MainContent>
                <Steps current={0} style={{ background: "white", padding: "25px" }}>
                    {apply_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} />
                        );
                    })}
                </Steps>
                <Card>
                    <ApplyTemplateForm formSubmit={this.handleSubmit} pprops={this.props} apply_type="机构" />
                    <br />
                    <br />
                    <SocietyInfo data_info={this.state.society_info} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：机构募捐申请页面
 * 机构募捐申请页面
 */
@addon('OrganizationApply', '机构募捐申请页面', '机构募捐申请页面')
@reactControl(OrganizationApply, true)
export class OrganizationApplyViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}