// import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { reactControl, BaseReactElementControl, CookieUtil } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Form, Select, Input, Radio, Button, Row, Checkbox, InputNumber, message, } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { AppServiceUtility } from "src/projects/app/appService";
import { User } from "src/business/models/user";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
const { Option } = Select;
import { request } from "src/business/util_tool";
/**
 * 组件：慈善申请表单表单状态
 */
export class ApplyTemplateFormState {
    /** 模板集合 */
    template_list?: any[];
    /** 项目集合 */
    project_list?: any[];
    /** 项目评分 */
    score?: number;
    // 项目评分储存
    score_array: any[];
    /*申请表里的选项和值 */
    project?: any;
    // 模板是否改变了
    isTplchange?: boolean;
    // 申请类型，个人，机构
    apply_type?: string;
}

/**
 * 组件：慈善申请表单表单
 * 慈善申请表单表单
 */
export class ApplyTemplateForm extends React.Component<ApplyTemplateFormControl, ApplyTemplateFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
            template_list: [],
            project_list: [],
            score_array: [],
            isTplchange: false,
            apply_type: '',
        };
    }
    /** 选择模板选择框值改变 */
    template_change = (value: any) => {
        request(this, AppServiceUtility.assessment_template_service.get_assessment_template_list!({ id: value }, 1, 99)!)
            .then((data: any) => {
                if (data.result) {
                    this.setState({
                        project_list: data.result,
                        score: 0,
                        isTplchange: true,
                    });
                }
            }).catch((error: Error) => {
                message.error(error.message);
            });
    }
    componentWillMount() {
        let apply_type = this.props.apply_type;
        this.setState({
            apply_type,
        });
        /** 获取申请模板 */
        let condition = { template_type: '慈善评估', template_name: apply_type };
        request(this, AppServiceUtility.assessment_template_service.get_assessment_template_list!(condition)!)
            .then((data: any) => {
                this.setState({
                    template_list: data.result
                });
            }).catch((error: Error) => {
                message.error(error.message);
            });
        if (this.props.baseData) {
            request(this, AppServiceUtility.competence_assessment_service.get_competence_assessment_list!({ id: this.props.baseData['id'] }, 1, 1)!)
                .then((data: any) => {
                    this.setState({
                        project_list: data!.result![0].project_list
                    });
                }).catch((error: Error) => {
                    message.error(error.message);
                });
        }

    }
    formSub = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                let obj: any = {}, project_list = [];
                let list = [];
                for (let key in values) {
                    if (key.indexOf('___') > 0) {
                        if (key.split('___').length > 2) {
                            list = key.split('___');
                            let radio_list = [];
                            radio_list = list[1].split(';');
                            let new_radio_list: any = [];
                            radio_list.forEach((item: any) => {
                                new_radio_list.push(JSON.parse(item));
                            });
                            let project = {
                                ass_info: {
                                    project_name: list[0],
                                    dataSource: new_radio_list,
                                    selete_type: list[2],
                                    value: values[key]
                                }
                            };
                            project_list.push(project);
                        } else {
                            let project = {
                                ass_info: {
                                    project_name: key.split('___')[0],
                                    selete_type: key.split('___')[1],
                                    value: values[key]
                                }
                            };
                            project_list.push(project);
                        }

                    } else {
                        obj[key] = values[key];
                    }
                }
                /** 申请记录 */
                let assessment_records: object = {
                    // 选择模板
                    template: values['template'],
                    // 真实姓名
                    apply_name: values['apply_name'],
                    // 联系方式
                    contacts: values['contacts'],
                    // 联系地址
                    address: values['address'],
                    // 身份证号码
                    id_card_no: values['id_card_no'],
                    // 申请金额
                    apply_money: values['apply_money'],
                    // 银行卡帐号
                    account_number: values['account_number'],
                    // 申请原因
                    description: values['description'],
                    // 申请类别
                    apply_type: this.state.apply_type,
                    // 服务项目
                    project_list: project_list,
                };
                this.props.formSubmit(err, assessment_records, this);
            }
        });

    }
    // 计算得分
    // 要通过value，遍历dataSource得出真正的分数
    score_calc = (id: any, e: any, type: number, dataSource: any) => {
        let score_array = this.state.score_array;
        let serial_number;
        if (type === 1) {
            // 单选
            serial_number = [e.target.value || 0];
        } else if (type === 2) {
            // 多选
            serial_number = e || [];
        } else if (type === 4) {
            // 下拉
            serial_number = [e || 0];
        }
        let relation: any[] = [];
        dataSource.map((item: any) => {
            relation['ny' + item['serial_number']] = item['score'];
        });
        // 这是得出真正的分数
        let now_score = serial_number.map((item: any) => {
            return relation['ny' + item];
        });
        // 储存在中转变量的都是真正的分数
        score_array[id] = now_score;
        let score = 0;
        for (let i in score_array) {
            if (score_array[i]) {
                for (let j in score_array[i]) {
                    if (score_array[i][j]) {
                        score += parseInt(score_array[i][j], 0);
                    }
                }
            }
        }
        // score = score! + parseInt(e.target.value, undefined);
        this.setState({
            score,
            score_array,
        });
    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 2 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 22 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        let { baseData } = this.props;
        let { isTplchange, apply_type } = this.state;
        let template = this.state.template_list!.map((key, value) => {
            return <Select.Option key={key.id}>{key.template_name}</Select.Option>;
        });

        // 慈善申请表单
        // 初始化，抑制报错
        if (!baseData) {
            baseData = {};
        }
        // 模板
        baseData!['template'] = baseData!['template'] || "";
        // 真实姓名
        baseData!['apply_name'] = baseData!['apply_name'] || "";
        // 联系方式
        baseData!['contacts'] = baseData!['contacts'] || "";
        // 身份证号码
        baseData!['id_card_no'] = baseData!['id_card_no'] || "";
        // 银行卡帐号
        baseData!['account_number'] = baseData!['account_number'] || "";
        // 申请原因
        baseData!['description'] = baseData!['description'] || "";
        // 申请金额
        baseData!['apply_money'] = baseData!['apply_money'] || "";
        // 联系地址
        baseData!['address'] = baseData!['address'] || "";
        // 模板选项
        baseData!['project_list'] = isTplchange ? this.state.project_list : (baseData!['project_list'] || this.state.project_list);
        // 申请得分
        baseData!['total_score'] = isTplchange ? this.state.score : (baseData!['total_score'] || this.state.score);
        // 申请人
        baseData!['evaluator'] = baseData!['evaluator'] || CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '';
        // 申请意见
        baseData!['assessment_opinions'] = baseData!['assessment_opinions'] || "";
        let project: any = baseData!['project_list']!.map((item: any, index: number) => {
            if (item.hasOwnProperty("ass_info")) {
                if (item.ass_info.hasOwnProperty("selete_type")) {
                    if (item.ass_info.selete_type === "1") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            // console.log(radio_list, "a-----------------------------------");
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} key={index}>
                                    {getFieldDecorator(item.ass_info.project_name + "___" + radio_list + "___1", {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Radio.Group onChange={(e: any) => this.score_calc(item.ass_info.id, e, 1, item.ass_info.dataSource)}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                // console.log(idx, item.ass_info.value);
                                                return <Radio value={i.serial_number} key={idx}>{i.option_content}</Radio>;
                                            })}
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        return item_content;
                    } else if (item.ass_info.selete_type === "2") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} key={index}>
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___2', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Checkbox.Group onChange={(e: any) => this.score_calc(item.ass_info.id, e, 2, item.ass_info.dataSource)}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Checkbox key={idx} value={i.serial_number}>{i.option_content}</Checkbox>;
                                            })}
                                        </Checkbox.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        return item_content;
                    } else if (item.ass_info.selete_type === "3") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name} key={index}>
                                {getFieldDecorator(item.ass_info.project_name + '___3', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <Input placeholder={`请输入${item.ass_info.project_name}`} value={this.state.project} />
                                )}
                            </Form.Item>
                        );
                        return item_content;
                    } else if (item.ass_info.selete_type === "4") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} key={index}>
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___4', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Select defaultValue={item.ass_info.dataSource[0].name} style={{ width: 120 }} onChange={(e: any) => this.score_calc(item.ass_info.id, e, 4, item.ass_info.dataSource)}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Option key={idx} value={i.serial_number}>{i.option_content}</Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            );
                        }
                        return item_content;
                    } else if (item.ass_info.selete_type === "5") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name} key={index}>
                                {getFieldDecorator(item.ass_info.project_name + '___5', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <TextArea
                                        placeholder={`请输入${item.ass_info.project_name}`}
                                        autosize={{ minRows: 3, maxRows: 5 }}
                                    />
                                )}
                            </Form.Item>
                        );
                        return item_content;
                    } else if (item.ass_info.selete_type === "6") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={index}>
                                {getFieldDecorator(item.ass_info.project_name + '___6', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: false,
                                    }],
                                })(
                                    <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                )}
                            </Form.Item>
                        );
                        return item_content;
                    } else if (item.ass_info.selete_type === "7") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} key={index}>
                                {getFieldDecorator(item.ass_info.project_name + '___7', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: false,
                                    }],
                                })(
                                    <FileUploadBtn contents={"button"} action={remote.upload_url} />
                                )}
                            </Form.Item>
                        );
                        return item_content;
                    }
                }
            } else {
                return true;
            }
        });
        const cn = apply_type === '个人' ? '申请人真实姓名' : '机构名字';
        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.formSub}>
                    <Form.Item label={cn}>
                        {getFieldDecorator('apply_name', {
                            initialValue: baseData!['apply_name'],
                            rules: [{
                                required: true,
                                message: `请输入${cn}`
                            }],
                        })(
                            <Input placeholder={`请输入${cn}`} />
                        )}
                    </Form.Item>
                    <Form.Item label='联系方式'>
                        {getFieldDecorator('contacts', {
                            initialValue: baseData!['contacts'],
                            rules: [{
                                required: true,
                                message: '请输入联系方式'
                            }],
                        })(
                            <Input placeholder="请输入联系方式" />
                        )}
                    </Form.Item>
                    {apply_type === '个人' ? <Form.Item label='身份证号码'>
                        {getFieldDecorator('id_card_no', {
                            initialValue: baseData!['id_card_no'],
                            rules: [{
                                required: true,
                                message: '请输入身份证号码'
                            }],
                        })(
                            <Input placeholder="请输入身份证号码" />
                        )}
                    </Form.Item> : null}
                    <Form.Item label='申请金额'>
                        {getFieldDecorator('apply_money', {
                            initialValue: baseData!['apply_money'],
                            rules: [{
                                required: true,
                                message: '请输入申请金额'
                            }],
                        })(
                            <InputNumber defaultValue={1} step={0.1} placeholder="请输入申请金额" />
                        )}
                    </Form.Item>
                    <Form.Item label='银行卡帐号'>
                        {getFieldDecorator('account_number', {
                            initialValue: baseData!['account_number'],
                            rules: [{
                                required: true,
                                message: '请输入银行卡帐号'
                            }],
                        })(
                            <Input placeholder="请输入银行卡帐号" />
                        )}
                    </Form.Item>
                    <Form.Item label='联系地址'>
                        {getFieldDecorator('address', {
                            initialValue: baseData!['address'],
                            rules: [{
                                required: true,
                                message: '请输入联系地址'
                            }],
                        })(
                            <TextArea
                                placeholder="请输入联系地址"
                                autosize={{ minRows: 3 }}
                            />
                        )}
                    </Form.Item>
                    <Form.Item label='申请原因'>
                        {getFieldDecorator('description', {
                            initialValue: baseData!['description'],
                            rules: [{
                                required: true,
                                message: '请输入申请原因'
                            }],
                        })(
                            <TextArea
                                placeholder="请输入申请原因"
                                autosize={{ minRows: 3 }}
                            />
                        )}
                    </Form.Item>
                    <Form.Item label='选择申请模板'>
                        {getFieldDecorator('template', {
                            initialValue: baseData!['template'],
                            rules: [{
                                required: true,
                                message: '请选择申请模板'
                            }],
                        })(
                            <Select placeholder="请选择申请模板" showSearch={true} onChange={this.template_change}>
                                {template}
                            </Select>
                        )}
                    </Form.Item>
                    {project}
                </Form>
                <Row type="flex" justify='center'>
                    <Button onClick={this.formSub}>
                        提交
                    </Button>
                </Row>
            </div>
        );
    }
}

/**
 * 控件：慈善申请表单表单控制器
 * 慈善申请表单表单
 */
@addon('ApplyTemplateForm', '慈善申请表单表单', '慈善申请表单表单')
@reactControl(Form.create<any>()(ApplyTemplateForm), true)
export class ApplyTemplateFormControl extends BaseReactElementControl {
    /** 表单提交 */
    formSubmit?: any;
    /** 基础数据 */
    baseData?: {};
    // 
    pprops?: any;
    // 
    apply_type?: string;

}
// const ApplyTemplateFormCP = Form.create<any>()(ApplyTemplateForm);
// export default ApplyTemplateFormCP;
export default Form.create<any>()(ApplyTemplateForm);