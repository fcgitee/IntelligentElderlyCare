import { Row, Modal, Descriptions, message, Button, Input } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import './index.less';
const { TextArea } = Input;

/**
 * 组件：机构募捐申请状态
 */
export interface OrganizationApplyListViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    /** 接口名 */
    request_url?: string;
    // 弹窗显示
    modal_visible: boolean;
    // 拒绝原因弹窗
    reject_modal_visible: boolean;
    // 拒绝原因
    reject_reason: string;
    // 募捐详情
    apply_info: any;
    // 是否已发送
    is_send: boolean;
}

/**
 * 组件：机构募捐申请
 * 机构募捐申请列表
 */
export class OrganizationApplyListView extends ReactView<OrganizationApplyListViewControl, OrganizationApplyListViewState> {
    private columns_data_source = [{
        title: '申请人姓名',
        dataIndex: 'realname',
        key: 'realname',
    }, {
        title: '手机号码',
        dataIndex: 'telephone',
        key: 'telephone',
    }, {
        title: '受助描述',
        dataIndex: 'self_introduction',
        key: 'self_introduction',
    }, {
        title: '申请状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '申请时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    private table_params = {
        other_label_type: [{ type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }, { type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            request_url: '',
            modal_visible: false,
            apply_info: [],
            reject_modal_visible: false,
            reject_reason: '',
            is_send: false,
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.recipientApply);
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // this.props.history!.push(ROUTE_PATH.recipientApply + '/' + contents.id);
            this.setState({
                apply_info: contents,
                modal_visible: true,
            });
        } else if ('icon_align_center' === type) {
            this.props.history!.push(ROUTE_PATH.recipientApply + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    toCharitableAppropriation(id: string) {
        this.props.history!.push(ROUTE_PATH.changeCharitableAppropriation + '/' + id);
    }
    changeStatus(value: any) {
        const is_send = this.state.is_send;
        const that = this;
        // 正在发送请求
        if (is_send === true) {
            message.error('请勿操作过快！');
            return false;
        }
        this.setState(
            {
                is_send: is_send,
            },
            () => {
                request(that, AppServiceUtility.charitable_service.update_charitable_recipient_apply!(value))
                    .then(data => {
                        that.setState({
                            modal_visible: false,
                            is_send: false,
                        });
                        message.success('操作成功！', 3, () => that.toCharitableAppropriation(value.id));
                    }).catch((error: Error) => {
                        message.error(error.message);
                    });
            }
        );

        return false;
    }
    // 拨款
    giveMoney() {
        this.toCharitableAppropriation(this.state.apply_info.id);
    }
    adopt() {
        const { apply_info } = this.state;
        if (!apply_info['id']) {
            message.error('数据出错，请刷新重试！');
            return false;
        }
        let param: any = {
            'id': apply_info['id'],
            'status': '已通过',
        };
        this.changeStatus(param);
        return false;
    }
    reject(type: number) {
        if (type === 2) {
            const { reject_reason, apply_info } = this.state;
            if (!apply_info['id']) {
                message.error('数据出错，请刷新重试！');
                return false;
            }
            if (!reject_reason) {
                message.error('请填写拒绝原因！');
                return false;
            }
            let param: any = {
                'id': apply_info['id'],
                'status': '已拒绝',
                'reject_reason': reject_reason,
            };
            this.changeStatus(param);
        } else if (type === 1) {
            this.setState({
                reject_modal_visible: true,
            });
        }
        return false;
    }
    setRejectReason(e: any) {
        this.setState({
            reject_reason: e.target.value,
        });
    }
    hideRejectModal() {
        this.setState({
            reject_modal_visible: false,
        });
    }
    hideModal() {
        this.setState({
            modal_visible: false,
        });
    }
    render() {
        let apply_info = this.state.apply_info;
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "申请人名称",
                decorator_id: "realname"
            },
            {
                type: InputType.radioGroup,
                label: "审核状态",
                decorator_id: "status",
                option: {
                    placeholder: "请选择项目状态",
                    options: [{
                        label: '未审核',
                        value: '未审核',
                    }, {
                        label: '已通过',
                        value: '已通过',
                    }, {
                        label: '已拨款',
                        value: '已拨款',
                    }]
                },
            }],
            btn_props: [],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let charitable_list = Object.assign(option, this.table_params);
        return (
            <Row>
                <Modal
                    width={700}
                    visible={this.state.reject_modal_visible}
                    okText="拒绝"
                    cancelText="返回"
                    onOk={() => this.reject(2)}
                    onCancel={() => this.hideRejectModal()}
                >
                    <Row>
                        <strong>请填写拒绝原因</strong>
                    </Row>
                    <br />
                    <Row>
                        <TextArea rows={4} onChange={(e) => { this.setRejectReason(e); }} />
                    </Row>
                </Modal>
                <Modal
                    // title="项目详情"
                    className="recipient-apply-modal"
                    width={1300}
                    visible={this.state.modal_visible}
                    okText="通过"
                    cancelText="取消"
                    onOk={() => this.hideModal()}
                    onCancel={() => this.hideModal()}
                >
                    <Descriptions title="募捐申请信息明细" bordered={true}>
                        <Descriptions.Item label="真实姓名">{apply_info.realname || ''}</Descriptions.Item>
                        <Descriptions.Item label="身份证号码">{apply_info.id_card_no || ''}</Descriptions.Item>
                        <Descriptions.Item label="手机号码">{apply_info.telephone || ''}</Descriptions.Item>
                        <Descriptions.Item label="银行卡帐号">{apply_info.account_number || ''}</Descriptions.Item>
                        <Descriptions.Item label="详细地址">{apply_info.address || ''}</Descriptions.Item>
                        <Descriptions.Item label="申请时间">{apply_info.create_date || ''}</Descriptions.Item>
                        <Descriptions.Item label="身份证正面照">
                            {(() => {
                                if (apply_info.id_card_1 && apply_info.id_card_1[0]) {
                                    return (
                                        <img
                                            alt="身份证正面照"
                                            src={apply_info.id_card_1[0]}
                                        />
                                    );
                                } else {
                                    return null;
                                }
                            })()}
                        </Descriptions.Item>
                        <Descriptions.Item label="身份证背面照">
                            {(() => {
                                if (apply_info.id_card_2 && apply_info.id_card_2[0]) {
                                    return (
                                        <img
                                            alt="身份证背面照"
                                            src={apply_info.id_card_2[0]}
                                        />
                                    );
                                } else {
                                    return null;
                                }
                            })()}
                        </Descriptions.Item>
                        <Descriptions.Item label="诊断证明">
                            {(() => {
                                if (apply_info.diagnostic_proof && apply_info.diagnostic_proof[0]) {
                                    return (
                                        <img
                                            alt="诊断证明"
                                            src={apply_info.diagnostic_proof[0]}
                                        />
                                    );
                                } else {
                                    return null;
                                }
                            })()}
                        </Descriptions.Item>
                        <Descriptions.Item label="受助描述" span={3}>{apply_info.self_introduction || ''}</Descriptions.Item>
                    </Descriptions>
                    {(() => {
                        if (apply_info['status'] === '未审核') {
                            return (
                                <Row type="flex" justify="center" className="ctrl-btns">
                                    <Button onClick={() => this.reject(1)}>拒绝</Button>
                                    <Button type="primary" onClick={() => this.adopt()}>通过</Button>
                                </Row>
                            );
                        } else if (apply_info['status'] === '已通过') {
                            return (
                                <Row type="flex" justify="center" className="ctrl-btns">
                                    <Button type="primary" onClick={() => this.giveMoney()}>拨款</Button>
                                </Row>
                            );
                        }
                        return null;
                    })()}
                </Modal>
                <SignFrameLayout {...charitable_list} />
            </Row>
        );
    }
}

/**
 * 控件：机构募捐申请
 * 机构募捐申请列表
 */
@addon('OrganizationApplyListView', '机构募捐申请', '机构募捐申请列表')
@reactControl(OrganizationApplyListView, true)
export class OrganizationApplyListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}