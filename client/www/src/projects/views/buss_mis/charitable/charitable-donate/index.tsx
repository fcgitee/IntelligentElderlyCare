import { Row, Modal, Descriptions, Card } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
// import { MainContent } from "src/business/components/style-components/main-content";
// let { Option } = Select;
import { CharitableDonateInfo } from "./charitable-donate-info";
/**
 * 组件：慈善捐赠状态
 */
export interface CharitableDonateViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    // 捐款项目集合
    donate_project_list: string[];
    /** 接口名 */
    request_url?: string;
    // 弹窗是否显示
    modal_visible: boolean;
    // 捐款详情
    donate_info: any[];
}

/**
 * 组件：慈善捐赠
 * 慈善捐赠列表
 */
export class CharitableDonateView extends ReactView<CharitableDonateViewControl, CharitableDonateViewState> {
    private columns_data_source = [{
        title: '捐款人/企业',
        dataIndex: 'donate_name',
        key: 'donate_name',
    },
    //  {
    //     title: '捐款项目',
    //     dataIndex: 'donate_project_name',
    //     key: 'donate_project_name',
    // },
    {
        title: '捐款金额',
        dataIndex: 'donate_money',
        key: 'donate_money',
    }, {
        title: '已用金额',
        dataIndex: 'payed_money',
        key: 'payed_money',
    }, {
        title: '冻结金额',
        dataIndex: 'freeze_money',
        key: 'freeze_money',
    }, {
        title: '支付状态',
        dataIndex: 'pay_status',
        key: 'pay_status',
    }, {
        title: '捐款时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];

    private table_params = {
        // other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }, { type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        other_label_type: [{ type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            donate_project_list: [],
            request_url: '',
            modal_visible: false,
            donate_info: [],
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeCharitableDonate);
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
        } else if ('icon_align_center' === type) {
            // this.viewDonateInfo(contents.id);
            this.props.history!.push(ROUTE_PATH.viewCharitableDonate + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url,
        });
        // request(this, AppServiceUtility.charitable_service.get_charitable_project_list_all!({}, 1, 999))
        //     .then((datas: any) => {
        //         // console.log(datas.result);
        //         this.setState({
        //             request_url,
        //             donate_project_list: datas.result,
        //         });
        //     });
    }
    // 查看详情
    viewDonateInfo(id: string) {
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({ id: id }, 1, 1))
            .then((datas: any) => {
                this.setState(
                    {
                        donate_info: datas.result[0] || [],
                    },
                    () => {
                        this.showModal();
                    }
                );
            });
    }
    // 显示模态框
    showModal() {
        this.setState({
            modal_visible: true,
        });
    }
    // 隐藏模态框
    hideModal() {
        this.setState({
            modal_visible: false,
        });
    }
    render() {
        // let donate_project_list: any[] = [];
        // this.state.donate_project_list!.map((item, idx) => {
        //     donate_project_list.push(<Option key={item['id']}>{item['project_name']}</Option>);
        // });
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "捐款人/企业",
                decorator_id: "donate_name"
            },
            // {
            //     type: InputType.select,
            //     label: "捐款项目",
            //     decorator_id: "donate_project_id",
            //     option: {
            //         placeholder: "请选择捐款项目",
            //         childrens: donate_project_list,
            //     }
            // },
            {
                type: InputType.radioGroup,
                label: "支付状态",
                decorator_id: "pay_status",
                option: {
                    placeholder: "请选择支付状态",
                    options: [{
                        label: '未支付',
                        value: '未支付',
                    }, {
                        label: '已支付',
                        value: '已支付',
                    }]
                },
            }],
            btn_props: [{
                label: '新增捐款',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let charitable_list = Object.assign(option, this.table_params);
        const { donate_info } = this.state;
        const apply_info = donate_info['use_info'] && donate_info['use_info']['result'] && donate_info['use_info']['result'][0] ? donate_info['use_info']['result'][0] : [];
        return (
            <Row>
                <Modal
                    // title="项目详情"
                    className="charitable-appropriation-modal"
                    width={1100}
                    visible={this.state.modal_visible}
                    okText="OK"
                    cancelText="返回"
                    onOk={() => this.hideModal()}
                    onCancel={() => this.hideModal()}
                    okButtonProps={{ className: 'okBtn' }}
                >
                    <CharitableDonateInfo donate_info={this.state.donate_info} />
                    <br />
                    {(() => {
                        if (donate_info['status'] === '已使用') {
                            return (
                                <Card>
                                    <Row>
                                        <Descriptions title="款项去向" bordered={true}>
                                            <Descriptions.Item label="真实姓名">{apply_info.realname || ''}</Descriptions.Item>
                                            <Descriptions.Item label="身份证号码">{apply_info.id_card_no || ''}</Descriptions.Item>
                                            <Descriptions.Item label="手机号码">{apply_info.telephone || ''}</Descriptions.Item>
                                            <Descriptions.Item label="银行卡帐号">{apply_info.account_number || ''}</Descriptions.Item>
                                            <Descriptions.Item label="详细地址">{apply_info.address || ''}</Descriptions.Item>
                                            <Descriptions.Item label="申请时间">{apply_info.create_date || ''}</Descriptions.Item>
                                            <Descriptions.Item label="身份证正面照">
                                                {(() => {
                                                    if (apply_info.id_card_1 && apply_info.id_card_1[0]) {
                                                        return (
                                                            <img
                                                                alt="身份证正面照"
                                                                src={apply_info.id_card_1[0]}
                                                            />
                                                        );
                                                    } else {
                                                        return null;
                                                    }
                                                })()}
                                            </Descriptions.Item>
                                            <Descriptions.Item label="身份证背面照">
                                                {(() => {
                                                    if (apply_info.id_card_2 && apply_info.id_card_2[0]) {
                                                        return (
                                                            <img
                                                                alt="身份证背面照"
                                                                src={apply_info.id_card_2[0]}
                                                            />
                                                        );
                                                    } else {
                                                        return null;
                                                    }
                                                })()}
                                            </Descriptions.Item>
                                            <Descriptions.Item label="诊断证明">
                                                {(() => {
                                                    if (apply_info.diagnostic_proof && apply_info.diagnostic_proof[0]) {
                                                        return (
                                                            <img
                                                                alt="诊断证明"
                                                                src={apply_info.diagnostic_proof[0]}
                                                            />
                                                        );
                                                    } else {
                                                        return null;
                                                    }
                                                })()}
                                            </Descriptions.Item>
                                            <Descriptions.Item label="受助描述" span={3}>{apply_info.self_introduction || ''}</Descriptions.Item>
                                        </Descriptions>
                                    </Row>
                                </Card>
                            );
                        }
                        return null;
                    })()}
                    <br />
                    {(() => {
                        if (donate_info['status'] === '已使用') {
                            return (
                                <Card>
                                    <Descriptions title="反馈信息" bordered={true}>
                                        <Descriptions.Item label="反馈信息" span={3}>{apply_info.feedback || '暂无'}</Descriptions.Item>
                                    </Descriptions>
                                </Card>
                            );
                        }
                        return null;
                    })()}
                </Modal>
                <SignFrameLayout {...charitable_list} />
            </Row>
        );
    }
}

/**
 * 控件：慈善捐赠
 * 慈善捐赠列表
 */
@addon('CharitableDonateView', '慈善捐赠', '慈善捐赠列表')
@reactControl(CharitableDonateView, true)
export class CharitableDonateViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}