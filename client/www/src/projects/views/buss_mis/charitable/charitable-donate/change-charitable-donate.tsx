import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { notification, Button, message, Descriptions, Card } from "antd";
import { request } from "src/business/util_tool";
import "./index.less";
/**
 * 组件：慈善捐款详情
 */
export interface ChangeCharitableDonateViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 慈善会信息
    society_info?: any;
}

/**
 * 组件：慈善捐款详情
 */
export default class ChangeCharitableDonateView extends ReactView<ChangeCharitableDonateViewControl, ChangeCharitableDonateViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            society_info: [],
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
        // 个人捐赠标识
        values['donate_type'] = '个人';
        request(this, AppServiceUtility.charitable_service.update_charitable_donate!(values))
            .then((data: any) => {
                if (data === 'Success') {
                    const notificationKey = `nk${Date.now()}`;
                    notification.open({
                        message: '捐款成功',
                        description: '您有一笔捐款成功到账，感谢您的支持！',
                        key: notificationKey,
                        btn: <Button type="primary" size="small" onClick={() => this.closedAndToList(notificationKey)}>确定</Button>,
                        onClose: () => {
                            this.closedAndToList(notificationKey);
                        },
                    });
                    setTimeout(
                        () => {
                            this.returnBtn();
                        }
                        ,
                        3000
                    );
                } else {
                    message.error(`错误信息：${data}`);
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    closedAndToList(notificationKey: any) {
        notification.close(notificationKey);
        this.returnBtn();
    }
    /** 返回回调 */
    returnBtn() {
        this.props.history!.push(ROUTE_PATH.charitableDonate);
    }
    componentDidMount() {
        // 获取慈善会信息
        request(this, AppServiceUtility.charitable_service.get_charitable_society_list!({}, 1, 1))
            .then((data: any) => {
                if (data.result && data.result[0]) {
                    this.setState({
                        society_info: data.result[0] || [],
                    });
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    societyHtml() {
        const society_info = this.state.society_info;
        if (society_info['id']) {
            const picCls = {
                maxWidth: '200px',
            };
            return (
                <Card className="cshxx">
                    <Descriptions title="慈善会信息">
                        <Descriptions.Item label="慈善会名称" span={3}>{society_info['name']}</Descriptions.Item>
                        <Descriptions.Item label="慈善会描述" span={3}>{society_info['description']}</Descriptions.Item>
                        <Descriptions.Item label="慈善会图片" span={3}>
                            {
                                society_info['picture'] && society_info['picture'].length > 0 ? society_info['picture'].map((item: any, index: number) => {
                                    return (
                                        <div key={index}>
                                            <img src={item} alt={item} style={picCls} />
                                        </div>
                                    );
                                }) : ''
                            }
                        </Descriptions.Item>
                        <Descriptions.Item label="慈善会募捐证书" span={3}>
                            {
                                society_info['certificate'] && society_info['certificate'].length > 0 ? society_info['certificate'].map((item: any, index: number) => {
                                    return (
                                        <div key={index}>
                                            <img src={item} alt={item} style={picCls} />
                                        </div>
                                    );
                                }) : ''
                            }
                        </Descriptions.Item>
                    </Descriptions>
                </Card>
            );
        }
        return null;
    }
    render() {
        // let modal_search_items_props = {
        //     edit_form_items_props: [
        //         {
        //             type: ListInputType.input,
        //             label: "项目名称",
        //             decorator_id: "donate_project"
        //         },
        //     ],
        //     columns_data_source: [{
        //         title: '项目名称',
        //         dataIndex: 'project_name',
        //         key: 'project_name',
        //     }, {
        //         title: '项目状态',
        //         dataIndex: 'project_status',
        //         key: 'project_status',
        //     }],
        //     service_name: AppServiceUtility.charitable_service,
        //     service_func: 'get_charitable_project_list_all',
        //     title: '捐赠项目查询',
        //     name_field: 'project_name',
        //     select_option: {
        //         placeholder: "请选择捐赠项目",
        //     }
        // };
        let edit_props = {
            form_items_props: [
                {
                    title: '慈善捐款',
                    need_card: true,
                    input_props: [
                        // {
                        //     type: InputType.modal_search,
                        //     label: "捐款项目",
                        //     decorator_id: "donate_project_id",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请选择捐赠项目" }],
                        //         initialValue: this.props.match!.params.code
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请选择捐赠项目",
                        //         modal_search_items_props: modal_search_items_props,
                        //     }
                        // },
                        {
                            type: InputType.antd_input,
                            label: "捐款人/企业",
                            decorator_id: "donate_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入捐款人/企业" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入捐款人/企业",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "捐款金额",
                            decorator_id: "donate_money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入捐款金额" }],
                                initialValue: 1,
                            } as GetFieldDecoratorOptions,
                            option: {
                                min: 0,
                                step: 0.1,
                                placeholder: "请输入捐款金额",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "捐款意向",
                            decorator_id: "donate_intention",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入捐款意向" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入捐款意向",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "联系方式",
                            decorator_id: "contacts",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入联系方式" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入联系方式",
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否匿名",
                            decorator_id: "is_anonymous",
                            field_decorator_option: {
                                rules: [{ required: true }],
                                initialValue: '否',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "捐赠留言",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入捐赠留言" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入捐赠留言",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.charitableDonate);
                    }
                },
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.charitable_service,
                operation_option: {
                    save: {
                        func_name: "update_charitable_donate"
                    },
                    query: {
                        func_name: "get_charitable_donate_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.charitableDonate); },
            id: this.props.match!.params.key,
            extra_html: this.societyHtml(),
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：慈善捐款详情
 * @description 慈善捐款详情
 * @author
 */
@addon('ChangeCharitableDonateView', '慈善捐款详情', '慈善捐款详情')
@reactControl(ChangeCharitableDonateView, true)
export class ChangeCharitableDonateViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}