import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import { Card, Descriptions, Row, Button, Table } from "antd";
import { CharitableDonateInfo } from "./charitable-donate-info";
/**
 * 组件：查看慈善捐款详情
 */
export interface ViewCharitableDonateViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 数据
    donate_info?: any;
}

/**
 * 组件：查看慈善捐款详情
 */
export default class ViewCharitableDonateView extends ReactView<ViewCharitableDonateViewControl, ViewCharitableDonateViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            donate_info: [],
        };
    }
    /** 返回回调 */
    backList = () => {
        this.props.history!.push(ROUTE_PATH.charitableDonate);
    }
    componentWillMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({ id: this.props.match!.params.key }, 1, 1))
            .then((datas: any) => {
                this.setState({
                    donate_info: datas.result[0],
                });
            });
    }
    render() {
        let columns_data_source = [{
            title: '申请名义',
            dataIndex: 'apply_name',
            key: 'apply_name',
        }, {
            title: '联系方式',
            dataIndex: 'contacts',
            key: 'contacts',
        }, {
            title: '申请类型',
            dataIndex: 'apply_type',
            key: 'apply_type',
        }, {
            title: '拨款金额',
            dataIndex: 'give_money',
            key: 'give_money',
        }, {
            title: '拨款时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }];
        const donate_info = this.state.donate_info;
        return (
            <MainContent>
                <CharitableDonateInfo donate_info={donate_info} />
                <br />
                <Card>
                    <Descriptions title="款项去向" layout="vertical" bordered={false} />
                    <Table pagination={false} columns={columns_data_source} dataSource={donate_info.recipient_info} />
                    <Row type={"flex"} justify={'center'} className="mt20">
                        <Button htmlType='button' onClick={() => this.backList()}>返回</Button>
                    </Row>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：查看慈善捐款详情
 * @description 查看慈善捐款详情
 * @author
 */
@addon('ViewCharitableDonateView', '查看慈善捐款详情', '查看慈善捐款详情')
@reactControl(ViewCharitableDonateView, true)
export class ViewCharitableDonateViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}