import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, Descriptions, } from "antd";
import "../charitable-donate/index.less";
/**
 * 组件：慈善捐赠信息状态
 */
export class CharitableDonateInfoState {
}

/**
 * 组件：慈善捐赠信息
 * 慈善捐赠信息
 */
export class CharitableDonateInfo extends React.Component<CharitableDonateInfoControl, CharitableDonateInfoState> {

    componentWillMount() {
        this.setState({
            donate_info: this.props.donate_info || [],
        });
    }
    render() {
        let { donate_info } = this.props;

        // 初始化，抑制报错
        if (!donate_info) {
            donate_info = [];
        }

        if (donate_info['donate_type'] === '基金') {
            return (
                <Card><Descriptions title="基金详情" bordered={true}>
                    <Descriptions.Item label="基金名字">{donate_info['donate_name'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="基金金额">￥{donate_info['donate_money'] || 0}</Descriptions.Item>
                    {donate_info['status'] === '通过' ? <Descriptions.Item label="已用金额">￥{donate_info['payed_money'] || 0}</Descriptions.Item> : null}
                    {donate_info['status'] === '通过' ? <Descriptions.Item label="冻结金额">￥{donate_info['freeze_money'] || 0}</Descriptions.Item> : null}
                    <Descriptions.Item label="基金意向">{donate_info['donate_intention'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="救助对象">{donate_info['donate_object'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="联系方式">{donate_info['contacts'] || ''}</Descriptions.Item>
                    {donate_info['status'] === '通过' ? <Descriptions.Item label="支付状态">￥{donate_info['pay_status'] || 0}</Descriptions.Item> : null}
                    <Descriptions.Item label="开始时间">{donate_info['begin_date'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="结束时间">{donate_info['end_date'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="申请时间">{donate_info['create_date'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="基金描述">{donate_info['remark'] || ''}</Descriptions.Item>
                </Descriptions>
                </Card>
            );
        } else {
            return (
                <Card>
                    <Descriptions title="捐款详情" bordered={true}>
                        <Descriptions.Item label="捐赠人/企业">{donate_info['donate_name'] || ''}</Descriptions.Item>
                        <Descriptions.Item label="捐款金额">￥{donate_info['donate_money'] || 0}</Descriptions.Item>
                        <Descriptions.Item label="已用金额">￥{donate_info['payed_money'] || 0}</Descriptions.Item>
                        <Descriptions.Item label="冻结金额">￥{donate_info['freeze_money'] || 0}</Descriptions.Item>
                        <Descriptions.Item label="捐款意向">{donate_info['donate_intention'] || ''}</Descriptions.Item>
                        <Descriptions.Item label="联系信息">{donate_info['contacts'] || ''}</Descriptions.Item>
                        <Descriptions.Item label="支付状态">{donate_info['pay_status'] || ''}</Descriptions.Item>
                        <Descriptions.Item label="捐款时间">{donate_info['create_date'] || ''}</Descriptions.Item>
                        <Descriptions.Item label="捐款留言">{donate_info['remark'] || ''}</Descriptions.Item>
                    </Descriptions>
                </Card>
            );
        }
    }
}

/**
 * 控件：慈善捐赠信息控制器
 * 慈善捐赠信息
 */
@addon('CharitableDonateInfo', '慈善捐赠信息', '慈善捐赠信息')
@reactControl(CharitableDonateInfo, true)
export class CharitableDonateInfoControl extends BaseReactElementControl {
    /** 基础数据 */
    donate_info?: any;
}