import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Steps } from 'antd';
import { request } from "../../../../../business/util_tool";
const { Step } = Steps;
let { Option } = Select;

/**
 * 组件：志愿者申请页面状态
 */
export interface VolunteerApplyState extends ReactViewState {
    /**
     * 申请状态
     */
    status?: number;
    /**
     * 表单权限控制
     */
    disabled?: boolean;
    /** 接口名 */
    request_url?: string;
    // 从业数据
    job_list?: string[];
    // 学历数据
    education_list?: string[];
    // 服务类别
    service_category_list?: any[];
}

/**
 * 组件：志愿者申请页面
 * 志愿者申请页面
 */
export class VolunteerApply extends ReactView<VolunteerApplyViewControl, VolunteerApplyState> {
    constructor(props: any) {
        super(props);
        this.state = {
            status: 0,
            disabled: false,
            request_url: '',
            service_category_list: [],
            job_list: ['国家公务员（含参照、依照公务员管理）', '专业技术人员', '职员', '企业管理人员', '工人', '农民', '学生', '教师', '现役军人', '自由职业者', '个体经营者', '无业人员', '退（离）休人员', '其他'],
            education_list: ['博士研究生', '硕士研究生', '大学本科', '大学专科', '中等专科', '职业高中', '技工学校', '高中', '初中', '小学', '其他'],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        // let step = new Map([['', 0], ['正在申请', 1], ['同意', 2], ['不同意', 2]]);
        request(this, AppServiceUtility.charitable_service.get_volunteer_service_category_list_all!({}, 1, 99))
            .then((datas: any) => {
                // let num = step.get(datas.result[0].status);
                this.setState({
                    service_category_list: datas.result,
                    // disabled: num !== 0 || datas.result[0].status === '不同意',
                });
            });
    }
    render() {
        // console.log(this.state.service_category_list, '<<<<<<<<<<<<<<<<<');
        let edit_props = {
            form_items_props: [
                {
                    title: "志愿者申请",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "真实姓名",
                            decorator_id: "realname",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入真实姓名" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入真实姓名",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "手机号码",
                            decorator_id: "telephone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入手机号码" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入手机号码",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "电子邮箱",
                            decorator_id: "email",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入电子邮箱" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入电子邮箱",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "详细地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入详细地址" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入详细地址",
                            }
                        },
                        {
                            type: InputType.select,
                            label: "最高学历",
                            decorator_id: "education",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择最高学历" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择最高学历",
                                childrens: [
                                    this.state.job_list!.map((item, index) => {
                                        return <Option key={index}>{item}</Option>;
                                    })
                                ]
                            }
                        },
                        {
                            type: InputType.select,
                            label: "从业状况",
                            decorator_id: "occupation",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择从业状况" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择从业状况",
                                childrens: [
                                    this.state.education_list!.map((item, index) => {
                                        return <Option key={index}>{item}</Option>;
                                    })
                                ]
                            }
                        },
                        {
                            type: InputType.checkbox_group,
                            label: "服务类别",
                            decorator_id: "service_category",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务类别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务类别",
                                list: this.state.service_category_list!.map((item, index) => {
                                    return {
                                        id: item.id,
                                        name: item.category_name
                                    };
                                })
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "自我介绍",
                            decorator_id: "self_introduction",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入自我介绍",
                                rows: 3,
                            }
                        }
                    ]
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: 1 }, 1, 1]
                    },
                    save: {
                        func_name: "update_volunteer_apply"
                    }
                },
                service_reject: (error: Error) => {
                    // console.log(error);
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceProviderApply); },
            id: this.props.match!.params.key,
        };
        if (this.state.status !== 0) {
            delete edit_props.submit_btn_propps;
        }
        // console.log(edit_props);
        let edit_props_list = Object.assign(edit_props, edit_props_info);

        return (
            <MainContent>
                <Steps current={this.state.status} style={{ background: "white", padding: "25px" }}>
                    <Step title="填写申请信息" />
                    <Step title="等待审核" />
                    <Step title="审核完成" />
                </Steps>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：志愿者申请页面
 * 志愿者申请页面
 */
@addon('VolunteerApply', '志愿者申请页面', '志愿者申请页面')
@reactControl(VolunteerApply, true)
export class VolunteerApplyViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}