import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import { Card, Descriptions, Row, Button, Table } from "antd";
/**
 * 组件：查看慈善项目详情
 */
export interface ViewCharitableProjectViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    // 数据集
    project_info?: any;
    // 数据集
    donate_list?: any;
}

/**
 * 组件：查看慈善项目详情
 */
export default class ViewCharitableProjectView extends ReactView<ViewCharitableProjectViewControl, ViewCharitableProjectViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            project_info: [],
            donate_list: [],
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    backList = () => {
        this.props.history!.push(ROUTE_PATH.charitableProject);
    }
    componentWillMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_project_list_all!({ id: this.props.match!.params.key }, 1, 1))
            .then((datas: any) => {
                this.setState({
                    project_info: datas.result[0],
                });
            });
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({ donate_project_id: this.props.match!.params.key, pay_status: '已支付' }, 1, 99))
            .then((datas: any) => {
                this.setState({
                    donate_list: datas.result,
                });
            });
    }
    render() {
        let project_info = this.state.project_info!;
        let donate_list = this.state.donate_list!;
        let columns_data_source = [{
            title: '捐款人/企业',
            dataIndex: 'donate_name',
            key: 'donate_name',
        }, {
            title: '捐款金额',
            dataIndex: 'donate_money',
            key: 'donate_money',
        }, {
            title: '联系方式',
            dataIndex: 'contacts',
            key: 'contacts',
        }, {
            title: '捐款时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }];
        return (
            <MainContent>
                <Card>
                    <Descriptions title="项目信息" bordered={true}>
                        <Descriptions.Item label="项目名称">{project_info.project_name || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目扶助意向">{project_info.project_intention || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目已接募捐款">￥{project_info.donate_money_sum || 0}</Descriptions.Item>
                        <Descriptions.Item label="项目发起机构">{project_info.organization_name || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目时间" span={2}>{project_info.begin_date || ''} - {project_info.end_date || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目描述" span={3}>{project_info.remark || ''}</Descriptions.Item>
                    </Descriptions>
                </Card>
                <br />
                <Card>
                    <Descriptions title="捐款信息" layout="vertical" bordered={false} />
                    <Table columns={columns_data_source} dataSource={donate_list} />
                    <Row type={"flex"} justify={'center'}>
                        <Button htmlType='button' onClick={() => this.backList()}>返回</Button>
                    </Row>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：查看慈善项目详情
 * @description 查看慈善项目详情
 * @author
 */
@addon('ViewCharitableProjectView', '查看慈善项目详情', '查看慈善项目详情')
@reactControl(ViewCharitableProjectView, true)
export class ViewCharitableProjectViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}