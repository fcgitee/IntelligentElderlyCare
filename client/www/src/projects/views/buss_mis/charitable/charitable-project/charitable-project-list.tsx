import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission } from "pao-aop";
import { Row, Col, Pagination, Descriptions, Button, Modal } from "antd";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import Search from "antd/lib/input/Search";

/**
 * 组件：慈善捐款项目列表页状态
 */
export interface CharitableProjectListState extends ReactViewState {
    /** 页面列表数据 */
    data_list?: any;
    /** 当前页数 */
    current: number;
    /** 一页展示多少条数据 */
    pagesize: number;
    /** 接口名 */
    request_url?: string;
    total?: number;
    // 弹窗是否显示
    modal_visible: boolean;
    // 当前查看的
    project_info: any;
    // 当前关键字
    intention: string;
    // 搜索框的内容
    input_value: string;
}

/**
 * 组件：慈善捐款项目列表页
 */
export class CharitableProjectList extends ReactView<CharitableProjectListViewControl, CharitableProjectListState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data_list: [],
            current: 1,
            pagesize: 10,
            total: 0,
            request_url: '',
            modal_visible: false,
            project_info: [],
            intention: '',
            input_value: '',
        };
    }
    onChange = (page: number, pagesize: number) => {
        this.selectdata(page, pagesize, this.state.intention);
    }
    onShowSizeChange = (current: number, pagesize: number) => {
        this.selectdata(current, pagesize, this.state.intention);
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
        this.selectdata(this.state.current, this.state.pagesize, '');
    }
    selectdata = (current: number, pagesize: number, intention: string) => {
        current = current || this.state.current;
        pagesize = pagesize || this.state.pagesize;
        intention = intention || this.state.intention;
        request(this, AppServiceUtility.charitable_service.get_charitable_project_list_all!({ id: this.props.match!.params.key, intention: intention }, 1, 1))
            .then((datas: any) => {
                // console.log(datas.result);
                this.setState({
                    data_list: datas.result,
                });
            });
    }
    searchList(value: any) {
        this.setState(
            {
                intention: value,
                input_value: value,
            },
            () => {
                this.selectdata(this.state.current, this.state.pagesize, value);
            }
        );
    }
    toDefaultProjectDnoate(e: any) {
        this.toProjectDonate(e, this.state.project_info.id);
    }
    toProjectDonate = (e: any, id: string) => {
        this.props.history!.push(ROUTE_PATH.changeCharitableDonate + '/project_id/' + id);
    }
    viewProjectDetail(e: any, id: string) {
        let data_list = this.state.data_list;
        for (let i in data_list) {
            if (data_list[i]) {
                if (data_list[i]['id'] === id) {
                    this.setState(
                        {
                            project_info: data_list[i]
                        },
                        () => {
                            this.showModal();
                        }
                    );
                    break;
                }
            }
        }
    }
    showModal() {
        this.setState({
            modal_visible: true,
        });
    }
    hideModal() {
        this.setState({
            modal_visible: false,
        });
    }
    render() {
        let project_info = this.state.project_info;
        let data_list = this.state.data_list;
        return (
            <Row className="charitable-project-list">
                <Modal
                    // title="项目详情"
                    width={1100}
                    visible={this.state.modal_visible}
                    okText="我要捐款"
                    cancelText="返回列表"
                    onOk={(e) => this.toDefaultProjectDnoate(e)}
                    onCancel={() => this.hideModal()}
                >
                    <Descriptions title="捐款详情" bordered={true}>
                        <Descriptions.Item label="项目名称">{project_info.project_name || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目扶助意向">{project_info.project_intention || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目已接募捐款">￥{project_info.donate_money_sum || 0}</Descriptions.Item>
                        <Descriptions.Item label="项目发起机构">{project_info.organization_name || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目时间" span={2}>{project_info.begin_date || ''} - {project_info.end_date || ''}</Descriptions.Item>
                        <Descriptions.Item label="项目描述" span={3}>{project_info.remark || ''}</Descriptions.Item>
                    </Descriptions>
                </Modal>
                <Descriptions title="慈善捐款项目" layout="vertical" bordered={false} />
                <br />
                <Row gutter={20}>
                    <Col>
                        <Search
                            placeholder="请输入捐款意向"
                            enterButton="一键匹配"
                            size="large"
                            onSearch={(value) => this.searchList(value)}
                        />
                    </Col>
                </Row>
                <br />
                <Row gutter={16} className="list-content">
                    {(() => {
                        if (data_list.length > 0) {
                            return data_list!.map((item: any, index: number) => {
                                return (
                                    <Col key={index} className="projectlist">
                                        <Row className="project-section">
                                            <Row className="picture">
                                                <img src={item.project_picture && item.project_picture[0] ? item.project_picture[0] : ''} />
                                            </Row>
                                            <Row className="detail">
                                                <Row className="price">
                                                    已接募捐款:￥<strong>{item.donate_money_sum}</strong>
                                                </Row>
                                                <Row className="projectinfotitle">{item.project_name}</Row>
                                                {
                                                    item.organization_name && item.organization_name.length > 0 ? item.organization_name!.map((names: any, count: number) => {
                                                        return (
                                                            <Row key={count} className="projectinfo">
                                                                {names}
                                                            </Row>
                                                        );
                                                    }) : ''
                                                }
                                                <Row className="button-ctrl" type="flex" justify="space-between">
                                                    <Button size="large" onClick={(e) => this.viewProjectDetail(e, item.id)}>查看详情</Button>
                                                    <Button size="large" onClick={(e) => { this.toProjectDonate(e, item.id); }}>我要捐款</Button>
                                                </Row>
                                            </Row>
                                        </Row>
                                    </Col>
                                );
                            });
                        } else {
                            return <Row className="empty-data">
                                <Row type="flex" justify="center">暂无数据</Row>
                                {/* <Row type="flex" justify="center">
                                    <Button onClick={() => this.searchList('')}>重新匹配</Button>
                                </Row> */}
                            </Row>;
                        }
                    })()}
                </Row>
                <Pagination
                    showSizeChanger={true}
                    onChange={this.onChange}
                    onShowSizeChange={this.onShowSizeChange}
                    total={this.state.total}
                    pageSize={this.state.pagesize}
                />
            </Row>
        );
    }
}

/**
 * 组件：慈善捐款项目列表页
 * 慈善捐款项目列表页
 */
@addon('CharitableProjectList', '慈善捐款项目列表页', '描述')
@reactControl(CharitableProjectList, true)
export class CharitableProjectListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}
