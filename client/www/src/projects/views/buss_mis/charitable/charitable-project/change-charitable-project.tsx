import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";
import moment from 'moment';
/**
 * 组件：慈善项目详情
 */
export interface ChangeCharitableProjectViewState extends ReactViewState {
    /** 数据id */
    id?: string;
}

/**
 * 组件：慈善项目详情
 */
export default class ChangeCharitableProjectView extends ReactView<ChangeCharitableProjectViewControl, ChangeCharitableProjectViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.charitableProject);
    }

    render() {
        const dateFormat: string = 'YYYY/MM/DD';
        const nowTime: any = new Date();
        const nowDate: string = nowTime.getFullYear() + "/" + (nowTime.getMonth() + 1) + "/"
            + nowTime.getDate();

        const oneMonthLater = nowTime.getMonth() + 2 === 13 ? 1 : nowTime.getMonth() + 2;
        const oneMonthLaterDate: string = nowTime.getFullYear() + "/" + oneMonthLater + "/"
            + nowTime.getDate();
        var edit_props = {
            form_items_props: [
                {
                    title: '添加慈善项目',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "慈善项目名称",
                            decorator_id: "project_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入慈善项目名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入慈善项目名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "慈善项目图片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "project_picture",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入慈善项目图片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入慈善项目图片",
                                action: remote.upload_url,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "项目意向",
                            decorator_id: "project_intention",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目意向" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入项目意向",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.rangePicker,
                            label: "项目时间",
                            decorator_id: "project_time",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目时间" }],
                                initialValue: [moment(nowDate, dateFormat), moment(oneMonthLaterDate, dateFormat)],
                            } as GetFieldDecoratorOptions,
                            option: {
                                format: dateFormat,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入慈善备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.charitableProject);
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.charitable_service,
                operation_option: {
                    save: {
                        func_name: "update_charitable_project"
                    },
                    query: {
                        func_name: "get_charitable_project_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.charitableProject); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：慈善项目详情
 * @description 慈善项目详情
 * @author
 */
@addon('ChangeCharitableProjectView', '慈善项目详情', '慈善项目详情')
@reactControl(ChangeCharitableProjectView, true)
export class ChangeCharitableProjectViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}