import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { table_param } from "src/projects/app/util-tool";
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：慈善项目状态
 */
export interface CharitableProjectViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：慈善项目
 * 慈善项目列表
 */
export class CharitableProjectView extends ReactView<CharitableProjectViewControl, CharitableProjectViewState> {
    private columns_data_source = [{
        title: '项目名称',
        dataIndex: 'project_name',
        key: 'project_name',
    }, {
        title: '项目创建人',
        dataIndex: 'project_creator_name',
        key: 'project_creator_name',
    }, {
        title: '项目意向',
        dataIndex: 'project_intention',
        key: 'project_intention',
    }, {
        title: '项目状态',
        dataIndex: 'project_status',
        key: 'project_status',
    }, {
        title: '项目开始时间',
        dataIndex: 'begin_date',
        key: 'begin_date',
    }, {
        title: '项目截止时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }];
    private table_params = {
        other_label_type: [{ type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            request_url: '',
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeCharitableProject);
    }
    componentDidMount() {
    }

    // 查看任务详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeCharitableProject + '/' + contents.id);
        } else if ('icon_align_center' === type) {
            this.props.history!.push(ROUTE_PATH.viewCharitableProject + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "项目名称",
                decorator_id: "project_name"
            },
            {
                type: InputType.radioGroup,
                label: "项目状态",
                decorator_id: "project_status",
                option: {
                    placeholder: "请选择项目状态",
                    options: [{
                        label: '未开始',
                        value: '未开始',
                    }, {
                        label: '进行中',
                        value: '进行中',
                    }, {
                        label: '已结束',
                        value: '已结束',
                    }]
                },
            }],
            btn_props: [{
                label: '创建项目',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let charitable_list = Object.assign(option, this.table_params);
        return (
            <Row>
                <SignFrameLayout {...charitable_list} />
            </Row>
        );
    }
}

/**
 * 控件：慈善项目
 * 慈善项目列表
 */
@addon('CharitableProjectView', '慈善项目', '慈善项目列表')
@reactControl(CharitableProjectView, true)
export class CharitableProjectViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}