import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：志愿者服务类别状态
 */
export interface VolunteerServiceCategoryViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：志愿者服务类别
 * 志愿者服务类别列表
 */
export class VolunteerServiceCategoryView extends ReactView<VolunteerServiceCategoryViewControl, VolunteerServiceCategoryViewState> {
    private columns_data_source = [{
        title: '服务类别名称',
        dataIndex: 'category_name',
        key: 'category_name',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            request_url: '',
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeVolunteerServiceCategory);
    }
    componentDidMount() {
    }

    // 查看任务详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeVolunteerServiceCategory + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "服务类别名称",
                decorator_id: "category_name"
            }],
            btn_props: [{
                label: '新增类别',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let volunteer_service_category_list = Object.assign(option, table_param);
        return (
            <Row>
                <SignFrameLayout {...volunteer_service_category_list} />
            </Row>
        );
    }
}

/**
 * 控件：志愿者服务类别
 * 志愿者服务类别列表
 */
@addon('VolunteerServiceCategoryView', '志愿者服务类别', '志愿者服务类别列表')
@reactControl(VolunteerServiceCategoryView, true)
export class VolunteerServiceCategoryViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}