import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
/**
 * 组件：志愿者服务类别详情
 */
export interface ChangeVolunteerServiceCategoryViewState extends ReactViewState {
    /** 数据id */
    id?: string;
}

/**
 * 组件：志愿者服务类别详情
 */
export default class ChangeVolunteerServiceCategoryView extends ReactView<ChangeVolunteerServiceCategoryViewControl, ChangeVolunteerServiceCategoryViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.volunteerServiceCategory);
    }

    render() {
        var edit_props = {
            form_items_props: [
                {
                    title: '添加志愿者服务类别',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "类别名称",
                            decorator_id: "category_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入类别名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入类别名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "类别描述",
                            decorator_id: "category_description",
                            field_decorator_option: {
                                // rules: [{ required: false, message: "请输入类别描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入类别描述",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入慈善备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.volunteerServiceCategory);
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.charitable_service,
                operation_option: {
                    save: {
                        func_name: "update_volunteer_service_category"
                    },
                    query: {
                        func_name: "get_volunteer_service_category_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.volunteerServiceCategory); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：志愿者服务类别详情
 * @description 志愿者服务类别详情
 * @author
 */
@addon('ChangeVolunteerServiceCategoryView', '志愿者服务类别详情', '志愿者服务类别详情')
@reactControl(ChangeVolunteerServiceCategoryView, true)
export class ChangeVolunteerServiceCategoryViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}