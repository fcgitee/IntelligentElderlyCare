import { Row, message, Modal, Button, Table } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import './index.less';
/**
 * 组件：慈善拨款状态
 */
export interface CharitableAppropriationViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    // 当前选中的
    select_id?: string;
    // 捐款
    appropriation_info: string[];
    /** 接口名 */
    request_url?: string;
    // 弹窗是否显示
    modal_visible: boolean;
    // 单条数据
    data_info: any[];
}

/**
 * 组件：慈善拨款
 * 慈善拨款列表
 */
export class CharitableAppropriationView extends ReactView<CharitableAppropriationViewControl, CharitableAppropriationViewState> {
    private columns_data_source = [{
        title: '申请人',
        dataIndex: 'apply_name',
        key: 'apply_name',
    }, {
        title: '拨款金额',
        dataIndex: 'apply_money',
        key: 'apply_money',
    }, {
        title: '拨款发起人',
        dataIndex: 'creator_name',
        key: 'creator_name',
    }, {
        title: '审核状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '申请时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '申请过期时间',
        dataIndex: 'overdue_date',
        key: 'overdue_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            select_id: '',
            ids: [],
            appropriation_info: [],
            request_url: '',
            modal_visible: false,
            data_info: [],
        };
    }

    // 创建项目
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeCharitableAppropriation);
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.viewAppropriationInfo(contents);
        }
    }
    // 查看详情，主要是读取ids的数据
    viewAppropriationInfo(value: any) {
        if (typeof (value.donate_ids) !== 'object') {
            message.error('数据出错！');
            return false;
        }
        request(this, AppServiceUtility.charitable_service.get_charitable_donate_list_all!({ ids: value.donate_ids }, 1, 99))
            .then((datas: any) => {
                this.setState(
                    {
                        ids: value.donate_ids,
                        select_id: value.id,
                        data_info: value,
                        appropriation_info: datas.result || [],
                    },
                    () => {
                        this.showModal();
                    }
                );
            });
        return false;
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url,
        });
    }
    // 显示模态框
    showModal() {
        this.setState({
            modal_visible: true,
        });
    }
    // 隐藏模态框
    hideModal() {
        this.setState({
            modal_visible: false,
        });
    }
    // 更改状态
    changeStatus(status: any) {
        const { select_id, ids } = this.state;
        if (!select_id || !ids) {
            message.error('数据错误，请刷新重试！');
            return false;
        }
        let param = {
            id: select_id,
            recipient_id: this.state.data_info['recipient_id'],
            ids: ids,
            status: status,
        };
        const that = this;
        request(this, AppServiceUtility.charitable_service.update_charitable_appropriation!(param))
            .then(data => {
                this.setState(
                    {
                        modal_visible: false,
                    },
                    () => {
                        message.success('操作成功！', 3, () => {
                            that.props.history!.push(ROUTE_PATH.charitableAppropriation);
                        });
                    }
                );
            }).catch((error: Error) => {
                message.error(error.message);
            });
        return false;
    }
    // 通过
    adopt() {
        this.changeStatus('已通过');
    }
    // 拒绝
    reject() {
        this.changeStatus('已拒绝');
    }
    render() {
        let option = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.radioGroup,
                    label: "审核状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择项目状态",
                        options: [{
                            label: '未审核',
                            value: '未审核',
                        }, {
                            label: '已通过',
                            value: '已通过',
                        }, {
                            label: '已拒绝',
                            value: '已拒绝',
                        }]
                    },
                }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let appropriation_list = Object.assign(option, table_param);
        let appropriation_info = this.state.appropriation_info;
        let data_info = this.state.data_info;
        let columns_data_source = [{
            title: '捐款人/企业',
            dataIndex: 'donate_name',
            key: 'donate_name',
        }, {
            title: '捐款金额',
            dataIndex: 'donate_money',
            key: 'donate_money',
        }, {
            title: '联系方式',
            dataIndex: 'contacts',
            key: 'contacts',
        }, {
            title: '捐款时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }];
        return (
            <Row>
                <Modal
                    // title="项目详情"
                    className="charitable-appropriation-modal"
                    width={1100}
                    visible={this.state.modal_visible}
                    okText="OK"
                    cancelText="返回"
                    onOk={() => this.hideModal()}
                    onCancel={() => this.hideModal()}
                    okButtonProps={{ className: 'okBtn' }}
                >
                    <Row><strong>使用款项明细</strong></Row>
                    <Table pagination={false} columns={columns_data_source} dataSource={appropriation_info} />
                    {(() => {
                        if (data_info['status'] && data_info['status'] === '未审核') {
                            return (
                                <Row type="flex" justify="center" className="ctrl-btns">
                                    <Button onClick={() => this.reject()}>拒绝</Button>&nbsp;&nbsp;<Button type="primary" onClick={() => this.adopt()}>通过</Button>
                                </Row>
                            );
                        }
                        return null;
                    })()}
                </Modal>
                <SignFrameLayout {...appropriation_list} />
            </Row>
        );
    }
}

/**
 * 控件：慈善拨款
 * 慈善拨款列表
 */
@addon('CharitableAppropriationView', '慈善拨款', '慈善拨款列表')
@reactControl(CharitableAppropriationView, true)
export class CharitableAppropriationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}