import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { Table, Row, Card, Button } from "antd";
// import { ROUTE_PATH } from 'src/projects/router';
// import { Steps, Descriptions, Result, Card } from 'antd';
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { DatePicker } from 'antd';
import moment from 'moment';
const { MonthPicker } = DatePicker;
const monthFormat = 'YYYY-MM';

/**
 * 组件：慈善信息汇总页面状态
 */
export interface CharitableInformationState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    // 详情
    data_info?: any;
    // 捐款列表
    donate_list?: any;
    // 捐款列表数量
    donate_list_count?: number;
    // 受助列表
    recipient_list?: any;
    // 受助列表数量
    recipient_list_count?: number;
    // 审核数量
    appropriation_list_count?: number;
    // 当前月份
    now_month?: string;
    // 收入总金额
    income_sum?: number;
    // 支出总金额
    output_sum?: number;
    // 总余额
    all_money_sum?: number;
    // 过审数量
    pass_sum?: number;
}

/**
 * 组件：慈善信息汇总页面
 * 慈善信息汇总页面
 */
export class CharitableInformation extends ReactView<CharitableInformationViewControl, CharitableInformationState> {
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
            data_info: [],
            donate_list: [],
            recipient_list: [],
            donate_list_count: 0,
            recipient_list_count: 0,
            appropriation_list_count: 0,
            now_month: '',
            pass_sum: 0,
            income_sum: 0,
            output_sum: 0,
            all_money_sum: 0,
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        this.getMonthData();
    }
    // 变更月份获取数据的封装
    getMonthData() {
        let now_month = this.state.now_month || this.getNowMonth();
        let param: any = {
            month: now_month,
        };
        // 汇总信息
        request(this, AppServiceUtility.charitable_service.get_charitable_information_list_all!(param, 1, 10))
            .then((datas: any) => {

                let donate_list = datas['donate_list'] && datas['donate_list']['result'] ? datas['donate_list']['result'] : [];

                let income_sum = 0;

                donate_list.map((item: any, index: number) => {
                    income_sum += item['donate_money'];
                });

                let recipient_list = datas['recipient_list'] && datas['recipient_list']['result'] ? datas['recipient_list']['result'] : [];

                let output_sum = 0;

                recipient_list.map((item: any, index: number) => {
                    output_sum += item['apply_money'];
                });

                this.setState({
                    // 
                    pass_sum: datas['appropriation_list'] && datas['appropriation_list']['total'] ? datas['appropriation_list']['total'] : 0,
                    // 
                    donate_list_count: datas['donate_list'] && datas['donate_list']['total'] ? datas['donate_list']['total'] : 0,
                    // 
                    donate_list: donate_list,
                    // 
                    income_sum,

                    // 
                    recipient_list_count: datas['recipient_list'] && datas['recipient_list']['total'] ? datas['recipient_list']['total'] : 0,
                    // 
                    recipient_list,
                    //
                    output_sum,

                    // 
                    all_money_sum: income_sum - output_sum,
                });
            });
    }
    // 得到年份/月份
    getNowMonth() {
        return new Date().getFullYear() + '-' + (new Date().getMonth() + 1);
    }
    // 月份变更
    changeMonth(e: any, values: any) {
        this.setState({
            now_month: values,
        });
    }
    render() {
        const { donate_list_count, recipient_list_count, donate_list, recipient_list, pass_sum, income_sum, output_sum, all_money_sum } = this.state;
        const columns: any = [
            {
                title: '当月捐款数量',
                dataIndex: 'donate_list_count',
                key: 'donate_list_count',
                width: 120,
                align: 'center'
            },
            {
                title: '当月募捐数量',
                dataIndex: 'recipient_list_count',
                key: 'recipient_list_count',
                width: 120,
                align: 'center'
            },
            {
                title: '审批数量',
                dataIndex: 'pass_sum',
                key: 'pass_sum',
                width: 120,
                align: 'center'
            },
            {
                title: '收入总金额',
                dataIndex: 'income_sum',
                key: 'income_sum',
                width: 120,
                align: 'center'
            },
            {
                title: '支出总金额',
                dataIndex: 'output_sum',
                key: 'output_sum',
                width: 120,
                align: 'center'
            },
            {
                title: '当月总余额',
                dataIndex: 'all_money_sum',
                key: 'all_money_sum',
                width: 120,
                align: 'center'
            },
        ];

        const data = [];
        data.push({
            key: 1,
            donate_list_count,
            recipient_list_count,
            pass_sum,
            income_sum,
            output_sum,
            all_money_sum,
        });
        let donate_cfg: any = [
            {
                title: '捐款人',
                dataIndex: 'donate_name',
                key: 'donate_name',
                width: 120,
                align: 'center'
            },
            {
                title: '捐款金额',
                dataIndex: 'donate_money',
                key: 'donate_money',
                width: 120,
                align: 'center'
            },
            {
                title: '捐款时间',
                dataIndex: 'create_date',
                key: 'create_date',
                width: 120,
                align: 'center'
            },
        ];
        let apply_cfg: any = [
            {
                title: '申请名字',
                dataIndex: 'apply_name',
                key: 'apply_name',
                width: 120,
                align: 'center'
            },
            {
                title: '申请金额',
                dataIndex: 'apply_money',
                key: 'apply_money',
                width: 120,
                align: 'center'
            },
            {
                title: '申请时间',
                dataIndex: 'create_date',
                key: 'create_date',
                width: 120,
                align: 'center'
            },
        ];
        return (
            <MainContent>
                <Card>
                    选择月份：
                    <MonthPicker defaultValue={moment(this.getNowMonth(), monthFormat)} format={monthFormat} onChange={(e, values) => this.changeMonth(e, values)} />
                    <Button type="primary" onClick={() => this.getMonthData()}>查询</Button>
                </Card>
                <Card>
                    <Row><strong>当月信息汇总</strong></Row>
                    <br />
                    <Table
                        columns={columns}
                        dataSource={data}
                        bordered={true}
                        size="middle"
                        pagination={false}
                        rowKey="key"
                    />
                </Card>
                <Card>
                    <Row><strong>当月捐赠信息</strong></Row>
                    <br />
                    <Table rowKey="id" pagination={false} columns={donate_cfg} dataSource={donate_list} />
                </Card>
                <Card>
                    <Row><strong>当月募捐信息</strong></Row>
                    <br />
                    <Table rowKey="id" pagination={false} columns={apply_cfg} dataSource={recipient_list} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：慈善信息汇总页面
 * 慈善信息汇总页面
 */
@addon('CharitableInformation', '慈善信息汇总页面', '慈善信息汇总页面')
@reactControl(CharitableInformation, true)
export class CharitableInformationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}