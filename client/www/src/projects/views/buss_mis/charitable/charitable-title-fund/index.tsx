import { Row, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
// import { MainContent } from "src/business/components/style-components/main-content";
let { Option } = Select;
/**
 * 组件：冠名基金状态
 */
export interface CharitableTitleFundViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    // 基金项目集合
    donate_project_list: string[];
    /** 接口名 */
    request_url?: string;
    // 弹窗是否显示
    modal_visible: boolean;
    // 基金详情
    donate_info: any[];
    // 当前选择的id
    now_selected_id: any;
}

/**
 * 组件：冠名基金
 * 冠名基金列表
 */
export class CharitableTitleFundView extends ReactView<CharitableTitleFundViewControl, CharitableTitleFundViewState> {
    private columns_data_source = [{
        title: '基金名字',
        dataIndex: 'donate_name',
        key: 'donate_name',
    }, {
        title: '基金金额',
        dataIndex: 'donate_money',
        key: 'donate_money',
    }, {
        title: '审批状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '申请时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];

    private table_params = {
        // other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }, { type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            donate_project_list: [],
            request_url: '',
            modal_visible: false,
            donate_info: [],
            now_selected_id: '',
        };
    }
    componentDidMount() {
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeCharitableTitleFund + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url,
        });
    }
    render() {
        let donate_project_list: any[] = [];
        this.state.donate_project_list!.map((item, idx) => {
            donate_project_list.push(<Option key={item['id']}>{item['project_name']}</Option>);
        });
        let option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "基金名字",
                decorator_id: "donate_name"
            }, {
                type: InputType.radioGroup,
                label: "审批状态",
                decorator_id: "status",
                option: {
                    placeholder: "请选择审批状态",
                    options: [{
                        label: '待审批',
                        value: '待审批',
                    }, {
                        label: '通过',
                        value: '通过',
                    }]
                },
            }],
            btn_props: [],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.charitable_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let charitable_list = Object.assign(option, this.table_params);
        return (
            <Row>
                <SignFrameLayout {...charitable_list} />
            </Row>
        );
    }
}

/**
 * 控件：冠名基金
 * 冠名基金列表
 */
@addon('CharitableTitleFundView', '冠名基金', '冠名基金列表')
@reactControl(CharitableTitleFundView, true)
export class CharitableTitleFundViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}