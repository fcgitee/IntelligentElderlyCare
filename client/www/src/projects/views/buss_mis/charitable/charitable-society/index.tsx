import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { request } from "../../../../../business/util_tool";
import { remote } from 'src/projects/remote';
import { message } from "antd";

/**
 * 组件：慈善会资料管理状态
 */
export interface CharitableSocietyState extends ReactViewState {
    /**
     * 表单权限控制
     */
    disabled?: boolean;
    /** 接口名 */
    request_url?: string;
    // 资料详情
    data_info?: any;
}

/**
 * 组件：慈善会资料管理
 * 慈善会资料管理
 */
export class CharitableSociety extends ReactView<CharitableSocietyViewControl, CharitableSocietyState> {
    constructor(props: any) {
        super(props);
        this.state = {
            disabled: false,
            request_url: '',
            data_info: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        request(this, AppServiceUtility.charitable_service.get_charitable_society_list_all!({}, 1, 1))
            .then((datas: any) => {
                this.setState({
                    data_info: (datas.result && datas.result[0]) ? datas.result[0] : [],
                });
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    submitCb(err: any, values: any) {
        let param = {
            name: values.name || '',
            description: values.description || '',
            picture: values.picture || [],
        };
        request(this, AppServiceUtility.charitable_service.update_charitable_society!(param))
            .then((datas: any) => {
                if (datas === 'Success') {
                    message.success('保存成功！');
                } else {
                    message.info(datas);
                }
            })
            .catch(error => {
                message.error(error.message);
            });
    }
    render() {
        const data_info = this.state.data_info;
        let edit_props = {
            form_items_props: [
                {
                    title: "慈善会资料",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "慈善会名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入慈善会名称" }],
                                initialValue: data_info['name'] || '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入慈善会名称",
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "慈善会介绍",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入慈善会介绍" }],
                                initialValue: data_info['description'] || '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入慈善会介绍",
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "慈善会图片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "picture",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传慈善会图片" }],
                                initialValue: data_info['picture'] || [],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请上传慈善会图片",
                                action: remote.upload_url
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "慈善会募捐证书（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "certificate",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传慈善会募捐证书" }],
                                initialValue: data_info['certificate'] || [],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请上传慈善会募捐证书",
                                action: remote.upload_url
                            }
                        },
                    ]
                }
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.submitCb
            },
            service_option: {
                service_object: AppServiceUtility.charitable_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_charitable_society"
                    }
                },
                service_reject: (error: Error) => {
                    // console.log(error);
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.recipientApplyList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：慈善会资料管理
 * 慈善会资料管理
 */
@addon('CharitableSociety', '慈善会资料管理', '慈善会资料管理')
@reactControl(CharitableSociety, true)
export class CharitableSocietyViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}