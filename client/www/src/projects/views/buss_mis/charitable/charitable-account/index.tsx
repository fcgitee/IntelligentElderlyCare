
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH } from 'src/projects/router';
import './index.less';
import { Modal, message } from "antd";
import PersonalRechargeControl from "./personal-recharge-form";
import AccountClearedControl from "./account-cleared-form";
/** 状态：慈善账户充值列表视图 */
export interface CharitableAccountViewState extends ReactViewState {
    /** 接口名 */
    request_ulr?: string;
    /** 个人充值 */
    personal_model_show?: boolean;
    /** 用户id */
    user_id?: string;
    /** 清零对话框 */
    clearde_model_show?: boolean;
    /** 养老院 */
    org_list?: any;
    /** 登录用户 */
    login_user?: any;
}
/** 组件：慈善账户充值列表视图 */
export class CharitableAccountView extends React.Component<CharitableAccountViewControl, CharitableAccountViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'card_id',
        key: 'card_id',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '消费金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '账户余额',
        dataIndex: 'balance',
        key: 'balance',
    }];
    constructor(props: CharitableAccountViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            personal_model_show: false,
            clearde_model_show: false,
            user_id: '',
            org_list: [],
            login_user: {},
        };
    }
    /** 对话框取消按钮 */
    batchhandleCancel = () => {
        this.setState({
            personal_model_show: false,
            clearde_model_show: false,
            user_id: '',
        });
    }
    /** 单个慈善账户充值 */
    personalHandleSubmit = (err: any, value: any) => {
        if (!err) {
            value['id'] = this.state.user_id;
            AppServiceUtility.subsidy_account_recharg_service.recharg_charitable_account!(value)!
                .then((data: any) => {
                    if (data) {
                        message.info('充值成功');
                        this.batchhandleCancel();
                    }
                })
                .catch(error => {
                    message.error(error.message);
                });
        }

    }
    /** 账户清零 */
    clearedHandleSubmit = (err: any, value: any) => {
        if (!err) {
            this.state.user_id ? value['id'] = this.state.user_id : '';
            value['amount'] = 0;
            AppServiceUtility.subsidy_account_recharg_service.clearn_charitable_account!(value)!
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('清零成功');
                        this.batchhandleCancel();
                    }
                })
                .catch(error => {
                    message.error(error.message);
                });
        }

    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_menu' === type) {
            this.props.history!.push(ROUTE_PATH.charitableAccountDetails + '/' + contents.user_id);
        } else if ('icon_export' === type) {
            this.setState({
                personal_model_show: true,
                user_id: contents.user_id,
            });
        } else if ('icon_reload' === type) {
            this.setState({
                clearde_model_show: true,
                user_id: contents.user_id,
            });
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        /** 获取当前登录用户 */
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then(data => {
                this.setState({
                    login_user: data[0]
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "证件号码",
                    decorator_id: 'card_id'
                },
                {
                    type: InputType.inputNumber,
                    label: "余额从",
                    decorator_id: "balance_low",
                },
                {
                    type: InputType.inputNumber,
                    label: "至",
                    decorator_id: "balance_upper",
                },
            ],
            btn_props: [],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.subsidy_account_recharg_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let table_param = {
            other_label_type: [
                { type: 'icon', label_key: 'icon_menu', label_parameter: { icon: 'antd@menu' } },
                { type: 'icon', label_key: 'icon_export', label_parameter: { icon: 'antd@export' } },
                { type: 'icon', label_key: 'icon_reload', label_parameter: { icon: 'antd@reload' } },
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);

        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="充值"
                    visible={this.state.personal_model_show}
                    onCancel={this.batchhandleCancel}
                    footer={null}
                >
                    <PersonalRechargeControl
                        formSubmit={this.personalHandleSubmit}
                        modelCancel={this.batchhandleCancel}
                        login_user={this.state.login_user}
                    />
                </Modal>
                <Modal
                    title={'清零'}
                    visible={this.state.clearde_model_show}
                    onCancel={this.batchhandleCancel}
                    footer={null}
                >
                    <AccountClearedControl
                        formSubmit={this.clearedHandleSubmit}
                        modelCancel={this.batchhandleCancel}
                        id={this.state.user_id}
                        login_user={this.state.login_user}
                    />
                </Modal>
            </div>

        );
    }
}

/**
 * 控件：慈善账户充值列表视图控制器
 * @description 慈善账户充值列表视图
 * @author
 */
@addon(' CharitableAccountView', '慈善账户充值列表视图', '慈善账户充值列表视图')
@reactControl(CharitableAccountView, true)
export class CharitableAccountViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}