import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Form, Input, Row, Button, InputNumber, } from "antd";
import TextArea from "antd/lib/input/TextArea";
/**
 * 组件：单个充值表单表单状态
 */
export class PersonalRechargeFormState {
}

/**
 * 组件：单个充值表单表单
 * 单个充值表单表单
 */
export class PersonalRechargeForm extends React.Component<PersonalRechargeControl, PersonalRechargeFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }

    componentWillMount() {

    }
    formSub = (e: any) => {
        e.preventDefault();
        let { form } = this.props;
        form.validateFields((err: any, values: any) => {
            if (!err) {
                this.props.formSubmit(err, values);
            }
        });

    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.formSub}>
                    <Form.Item label='金额（元）'>
                        {getFieldDecorator('amount', {
                            // initialValue: baseData!['evaluator'] ? baseData!['evaluator'] : (CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '...'),
                            initialValue: '',
                            rules: [{
                                required: true,
                                message: '请输入金额'
                            }],
                        })(
                            <InputNumber style={{ width: '100%' }} />
                        )}
                    </Form.Item>
                    <Form.Item label='经办人'>
                        {getFieldDecorator('person_in_charge', {
                            initialValue: this.props.login_user.name,
                            rules: [{
                                required: false,
                                message: '经办人'
                            }],
                        })(
                            <Input disabled={true} />
                        )}
                    </Form.Item>
                    <Form.Item label='备注'>
                        {getFieldDecorator('remark', {
                            initialValue: '',
                            rules: [{
                                required: true,
                                message: '请输入备注'
                            }],
                        })(
                            <TextArea />
                        )}
                    </Form.Item>
                    <Row type="flex" justify='center'>
                        <Button onClick={this.props.modelCancel} style={{ marginRight: '16px' }}>
                            取消
                        </Button>
                        <Button onClick={this.formSub}>
                            确定
                        </Button>
                    </Row>
                </Form>

            </div>
        );
    }
}

/**
 * 控件：单个充值表单表单控制器
 * 单个充值表单表单
 */
@addon('PersonalRechargeForm', '单个充值表单表单', '单个充值表单表单')
@reactControl(Form.create<any>()(PersonalRechargeForm))
export class PersonalRechargeControl extends BaseReactElementControl {
    /** 表单提交 */
    formSubmit?: any;
    /** 取消按钮 */
    modelCancel?: any;
    /** 登录用户 */
    login_user: any;

}
export default Form.create<any>()(PersonalRechargeForm);