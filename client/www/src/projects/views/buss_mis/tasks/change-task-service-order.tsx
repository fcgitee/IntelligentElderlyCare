import { AppServiceUtility } from "src/projects/app/appService";
import { beforeUpload } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import Form from "antd/lib/form/Form";
// import { ROUTE_PATH } from "src/projects/router";
import { remote } from "src/projects/remote";
import { Row, Button, Col, message, DatePicker } from "antd";
import { MainCard } from "src/business/components/style-components/main-card";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { request } from "src/business/util_tool";
/**
 * 状态：服务订单分派编辑
 */
export interface ChangeTaskServiceOrderViewState extends ReactViewState {
    // 详情
    data_info: any;
    // 步骤标识，
    // 0的时候，只能看不能改
    // 1的时候，表示待接收，必上传开始内容，选上传结束内容，并根据内容决定状态
    // 2的时候，表示待完成，必上传结束内容，提交为完成
    stepStatus?: any;
}

/**
 * 组件：服务订单分派编辑
 */
export class ChangeTaskServiceOrderView extends ReactView<ChangeTaskServiceOrderViewControl, ChangeTaskServiceOrderViewState> {
    constructor(props: ChangeTaskServiceOrderViewControl) {
        super(props);
        this.state = {
            data_info: [],
            stepStatus: 0,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.service_record_service.get_service_record_list_all!({ id: this.props.match!.params.key }, 1, 1))
                .then((datas: any) => {
                    let data_info = datas.result && datas.result[0] ? datas.result[0] : [];
                    let stepStatus = 0;
                    if (data_info.hasOwnProperty('task_state')) {
                        if (data_info['task_state'] && data_info['task_state'] === '待接收') {
                            // 待接收的时候，开始必填，完成选填
                            stepStatus = 1;
                        } else if (data_info['task_state'] && data_info['task_state'] === '进行中') {
                            // 进行中的时候，必须填完成
                            stepStatus = 2;
                        }
                    }
                    this.setState({
                        stepStatus,
                        data_info,
                    });
                });
        }
    }
    backList() {
        history.back();
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        let data_info = this.state.data_info;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let param: any = {
                    order_id: data_info['order_id'],
                    id: data_info['task_id'] && data_info['task_id'] ? data_info['task_id'] : '',
                    'begin_photo': values['begin_photo'].length > 0 ? values['begin_photo'] : '',
                    'end_photo': values['end_photo'].length > 0 ? values['end_photo'] : '',
                    'start_date': values['start_date'] ? values['start_date'] : '',
                    'end_date': values['end_date'] ? values['start_date'] : '',
                    is_app: false
                };
                request(this, AppServiceUtility.task_service.change_task_service_order!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功');
                            history.back();
                        } else {
                            message.info(datas);
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        let { data_info, stepStatus } = this.state;
        return (
            <MainContent>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <MainCard title='任务详情'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                {/* <Form.Item label='任务名字'>
                                    <div>{data_info.task_name}</div>
                                </Form.Item> */}
                                {/* <Form.Item label='任务内容'>
                                    <div>{data_info.task_content}</div>
                                </Form.Item> */}
                                {/* <Form.Item label='任务类型'>
                                    <div>{data_info.task_type_name}</div>
                                </Form.Item> */}
                                <Form.Item label='任务地址'>
                                    <div>{data_info.task_address}</div>
                                </Form.Item>
                                {/* <Form.Item label='任务紧急程度'>
                                    <div>{data_info.task_urgent}</div>
                                </Form.Item> */}
                                <Form.Item label='任务状态'>
                                    <div>{data_info.task_state}</div>
                                </Form.Item>
                                <Form.Item label='备注'>
                                    <div>{data_info.task_remark}</div>
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    <MainCard title='长者详情'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='长者姓名'>
                                    <div>{data_info.purchaser_name ? data_info.purchaser_name : ''}</div>
                                </Form.Item>
                                <Form.Item label='联系方式'>
                                    <div>{data_info.purchaser_telephone ? data_info.purchaser_telephone : ''}</div>
                                </Form.Item>
                                <Form.Item label='长者类型'>
                                    <div>{data_info.classification_name ? data_info.classification_name : ''}</div>
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    <MainCard title='服务开始'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='服务开始时间'>
                                    {getFieldDecorator('start_date', {
                                        initialValue: data_info['start_date'] ? data_info['start_date'] : '',
                                        rules: [{
                                            required: stepStatus === 1 ? true : false,
                                            message: '请选择服务开始时间'
                                        }],
                                    })(
                                        <DatePicker disabled={stepStatus === 2 || stepStatus === 0 ? true : false} showTime={true} placeholder="选择服务开始时间" />
                                    )}
                                </Form.Item>
                                <Form.Item label='上传图片（大小小于2M，格式支持jpg/jpeg/png）'>
                                    {getFieldDecorator('begin_photo', {
                                        initialValue: data_info['task_begin_photo'] ? data_info['task_begin_photo'] : '',
                                        rules: [{
                                            required: stepStatus === 1 ? true : false,
                                            message: '请上传服务开始图片'
                                        }],
                                    })(
                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} disabled={stepStatus === 2 || stepStatus === 0 ? true : false} />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    <MainCard title='服务结束'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='服务结束时间'>
                                    {getFieldDecorator('end_date', {
                                        initialValue: data_info['end_date'] ? data_info['end_date'] : '',
                                        rules: [{
                                            required: stepStatus === 2 ? true : false,
                                            message: '请选择服务结束时间'
                                        }],
                                    })(
                                        <DatePicker disabled={stepStatus === 0 ? true : false} showTime={true} placeholder="选择服务结束时间" />
                                    )}
                                </Form.Item>
                                <Form.Item label='上传图片（大小小于2M，格式支持jpg/jpeg/png）'>
                                    {getFieldDecorator('end_photo', {
                                        initialValue: data_info['task_end_photo'] ? data_info['task_end_photo'] : '',
                                        rules: [{
                                            required: stepStatus === 2 ? true : false,
                                            message: '请上传服务结束图片'
                                        }],
                                    })(
                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} disabled={stepStatus === 0 ? true : false} />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    <MainCard>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button htmlType='submit' type='primary' disabled={stepStatus === 0 ? true : false}>保存</Button>
                            <Button type='ghost' name='返回' htmlType='button' onClick={this.backList}>返回</Button>
                        </Row>
                    </MainCard>
                </Form>
            </MainContent >
        );
    }
}

/**
 * 控件：服务订单分派编辑
 * @description 服务订单分派编辑
 * @author
 */
@addon('ChangeTaskServiceOrderViewControl', '服务订单分派编辑', '服务订单分派编辑')
@reactControl(Form.create<any>()(ChangeTaskServiceOrderView), true)
export class ChangeTaskServiceOrderViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}