import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { request } from 'src/business/util_tool';

const { Option } = Select;
/**
 * 组件：任务详情
 */
export interface ChangeTaskViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
    task_data?: [];
    /** 可分派人士 */
    task_receive_person?: any;
    /** 任务类型 */
    task_type?: any;
    /** 任务紧急程度 */
    task_urgent?: any;
}

class ConfigObj {
    isCreate: boolean = false;
    inputType: string = '';
    selectType: string = '';
    dateType: string = '';
    type_id: string = '';
    urgent_id: string = '';
    receive_id: string = '';
    pageTitle: string = '';
    taskCreatorObj: any = [];
    taskStateObj: any = [];
    submitBtn?: any;
    taskTypeConf: any = {
        option: {},
        rules: {}
    };
    taskPersonConf: any = {
        option: {},
        rules: {}
    };
    taskUrgentConf: any = {
        option: {},
        rules: {}
    };
}

/**
 * 组件：任务详情
 */
export default class ChangeTaskView extends ReactView<ChangeTaskViewControl, ChangeTaskViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            task_data: [],
            task_receive_person: [],
            task_type: [],
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.taskList);
    }

    componentDidMount() {
        request(this, AppServiceUtility.task_service.input_task_type!()!)
            .then((datas: any) => {
                this.setState({
                    task_type: datas,
                });
            });
        request(this, AppServiceUtility.task_service.input_person!()!)
            .then((datas: any) => {
                this.setState({
                    task_receive_person: datas,
                });
            });
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let pesron = this.state.task_receive_person;
        let task_receive_person: any[] = [];
        pesron!.map((item: any) => {
            task_receive_person.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let type = this.state.task_type;
        let task_type: any[] = [];
        type!.map((item: any) => {
            task_type.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let cobj = new ConfigObj();

        if (this.props.match!.params.key) {
            //  查看的
            cobj.inputType = cobj.selectType = cobj.dateType = 'input_uneditable';
            cobj.type_id = 'task_type_name';
            cobj.urgent_id = 'task_urgent';
            cobj.receive_id = 'receiver_info_name';
            cobj.pageTitle = '查看任务';
            cobj.taskCreatorObj = [{
                type: InputType[cobj.inputType],
                label: "任务创建人",
                decorator_id: "task_creator_name",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入任务内容" }],
                } as GetFieldDecoratorOptions,
                option: {
                    placeholder: "请输入任务内容"
                }
            }];
            cobj.taskStateObj = [{
                type: InputType[cobj.inputType],
                label: "任务状态",
                decorator_id: "task_state",
                field_decorator_option: {
                } as GetFieldDecoratorOptions,
            }, {
                type: InputType[cobj.inputType],
                label: "审核不通过/拒绝原因",
                decorator_id: "task_fail_reason",
                field_decorator_option: {
                } as GetFieldDecoratorOptions,
            }];
        } else {
            // 创建的
            cobj.isCreate = true;
            cobj.submitBtn = { text: "保存" };
            cobj.inputType = 'antd_input';
            cobj.selectType = 'select';
            cobj.dateType = 'date';
            cobj.type_id = 'task_type';
            cobj.urgent_id = 'task_urgent';
            cobj.receive_id = 'task_receive_person';
            cobj.pageTitle = '创建任务';
            cobj.taskTypeConf = {
                option: {
                    placeholder: "请选择任务类型",
                    childrens: task_type,
                },
                rules: { required: true, message: "请选择任务类型" }
            };
            cobj.taskUrgentConf = {
                option: {
                    placeholder: "请选择任务紧急程度",
                    options: [{
                        label: '紧急',
                        value: '紧急',
                    }, {
                        label: '一般',
                        value: '一般',
                    }]
                },
                rules: { required: true, message: "请选择任务紧急程度" }
            };
            cobj.taskPersonConf = {
                option: {
                    placeholder: "请选择分派人员",
                    childrens: task_receive_person,
                },
                rules: { required: true, message: "请选择分派人员" }
            };
        }
        // var inputType:string = 'input_uneditable';
        var edit_props = {
            form_items_props: [
                {
                    title: cobj.pageTitle,
                    need_card: true,
                    input_props: [
                        {
                            type: InputType[cobj.inputType],
                            label: "任务名称",
                            decorator_id: "task_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType[cobj.inputType],
                            label: "任务内容",
                            decorator_id: "task_content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务内容",
                                autocomplete: 'off',
                            }
                        },
                        ...cobj.taskCreatorObj,
                        {
                            type: InputType[cobj.selectType],
                            label: "任务类型",
                            decorator_id: cobj.type_id,
                            field_decorator_option: {
                                rules: [cobj.taskTypeConf.rules],
                            } as GetFieldDecoratorOptions,
                            option: cobj.taskTypeConf.option,
                        },
                        {
                            type: InputType[cobj.selectType],
                            label: "任务紧急程度",
                            decorator_id: cobj.urgent_id,
                            field_decorator_option: {
                                rules: [cobj.taskUrgentConf.rules],
                            } as GetFieldDecoratorOptions,
                            option: cobj.taskUrgentConf.option,
                        },
                        {
                            type: InputType[cobj.selectType],
                            label: "任务分派人员",
                            decorator_id: cobj.receive_id,
                            field_decorator_option: {
                                rules: [cobj.taskPersonConf.rules],
                            } as GetFieldDecoratorOptions,
                            option: cobj.taskPersonConf.option,
                        },
                        {
                            type: InputType[cobj.inputType],
                            label: "任务地址",
                            decorator_id: "task_address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务地址" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务地址",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType[cobj.inputType],
                            label: "任务地理信息",
                            decorator_id: "task_location",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务地理信息" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务地理信息",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType[cobj.inputType],
                            label: "任务备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务备注",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType[cobj.dateType],
                            label: "任务提醒时间",
                            decorator_id: "task_notice_time",
                            field_decorator_option: {
                                // rules: [{ message: "任务提醒时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务提醒时间",
                                autocomplete: 'off',
                            },
                        },
                        ...cobj.taskStateObj
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push(this.props.back_url ? this.props.back_url : ROUTE_PATH.taskList);
                    }
                },
            ],
            submit_btn_propps: cobj.submitBtn,
            service_option: {
                service_object: AppServiceUtility.task_service,
                operation_option: {
                    save: {
                        func_name: "add_new_receive_task"
                    },
                    query: {
                        func_name: "get_all_task_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：任务详情
 * @description 任务详情
 * @author
 */
@addon('ChangeTaskView', '任务详情', '任务详情')
@reactControl(ChangeTaskView, true)
export class ChangeTaskViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}