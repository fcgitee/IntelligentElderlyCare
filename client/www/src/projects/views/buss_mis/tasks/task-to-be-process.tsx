import { Modal, Row, message, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param, edit_props_info, taskState } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import FormCreator, { InputType as FormInputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { ROUTE_PATH } from "src/projects/router";
const { Option } = Select;
/**
 * 组件：任务管理系统未审核任务列表状态
 */
export interface TaskToBeProcessViewState extends ReactViewState {
    // 弹出框是否显示
    modal_visible?: boolean;
    // 选择行的Id集合
    ids?: string[];
    // 选择单行Id
    id?: string;
    // 任务总数总条数
    total?: number;
    // 任务列表
    task_list?: any[];
    // 任务类型
    task_type?: any[];
    // 是否多选
    isMany: boolean;
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：任务管理系统未审核任务列表
 * 任务管理系统未审核任务列表
 */
export class TaskToBeProcessView extends ReactView<TaskToBeProcessViewControl, TaskToBeProcessViewState> {
    private columns_data_source = [{
        title: '任务名称',
        dataIndex: 'task_name',
        key: 'task_name',
    }, {
        title: '任务创建人',
        dataIndex: 'task_creator_name',
        key: 'task_creator_name',
    }, {
        title: '任务类型',
        dataIndex: 'task_type_name',
        key: 'task_type_name',
    }, {
        title: '紧急程度',
        dataIndex: 'task_urgent',
        key: 'task_urgent',
    }, {
        title: '任务状态',
        dataIndex: 'task_state',
        key: 'task_state',
    }, {
        title: '任务创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            id: '',
            task_type: [],
            modal_visible: false,
            isMany: false,
            request_url: '',
        };
    }

    // 弹窗取消
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        // 获取任务类型
        request(this, AppServiceUtility.task_service.input_task_type!()!)
            .then((datas: any) => {
                this.setState({
                    task_type: datas,
                });
            });
    }

    // 弹出审核窗口
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({
                modal_visible: true,
                isMany: false,
                id: contents.id,
            });
        }
    }

    // 审核通过
    processOk = () => {
        var arr: any = {};
        arr.ids = this.state.isMany ? this.state.ids : [this.state.id];
        AppServiceUtility.task_service.task_examine_passed!(arr)!
            .then((data: any) => {
                if (data === 'Success') {
                    this.setState({
                        modal_visible: false,
                    });
                    message.info('操作成功');
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.taskToBeProcess);
                        },
                        300
                    );
                } else {
                    message.info('操作失败');
                }
            });
    }

    // 审核不通过
    handleSubmit = (e: any, value: any) => {
        value['ids'] = this.state.isMany ? this.state.ids : [this.state.id];
        AppServiceUtility.task_service.task_examine_failed!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    this.setState({
                        modal_visible: false,
                    });
                    message.info('操作成功');
                    setTimeout(
                        () => {
                            this.props.history!.push(ROUTE_PATH.taskToBeProcess);
                        },
                        300
                    );
                } else {
                    message.info('操作失败');
                }
            });
    }

    examineMany = () => {
        const ids = this.state.ids;
        // 批量操作不允许小于0
        if (ids!.length <= 0) {
            message.warn('请选择一行或多行！');
            return;
        }
        this.setState({
            isMany: true,
            modal_visible: true,
        });
    }

    // 行选择事件
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            ids,
        });
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        let task_state: any = [];
        for (let i in taskState) {
            if (taskState[i]) {
                task_state.push(<Option key={i} value={taskState[i]}>{taskState[i]}</Option>);
            }
        }
        let type = this.state.task_type;
        let task_type: any[] = [];
        type!.map((item: any) => {
            task_type.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let task_option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "任务名称",
                decorator_id: "task_name"
            },
            {
                type: InputType.select,
                label: "任务状态",
                decorator_id: "task_state",
                option: {
                    placeholder: "请选择任务状态",
                    showSearch: true,
                    childrens: task_state,
                }
            },
            {
                type: InputType.radioGroup,
                label: "任务紧急程度",
                decorator_id: "task_urgent",
                option: {
                    placeholder: "请选择任务紧急程度",
                    options: [{
                        label: '紧急',
                        value: '紧急',
                    }, {
                        label: '一般',
                        value: '一般',
                    }]
                },
            },
            {
                type: InputType.select,
                label: "任务类型",
                decorator_id: "task_type",
                option: {
                    placeholder: "请选择任务类型",
                    showSearch: true,
                    childrens: task_type,
                }
            }],
            btn_props: [{
                label: '批量审核',
                btn_method: this.examineMany,
                icon: 'edit'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.task_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                // delete: {
                //     service_func: 'del_task'
                // }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
        };
        let task_list = Object.assign(task_option, table_param);
        let edit_props = {
            form_items_props: [
                {
                    title: "任务审核",
                    need_card: true,
                    input_props: [
                        {
                            type: FormInputType.text_area,
                            label: "原因",
                            decorator_id: "task_fail_reason",
                            field_decorator_option: {
                                rules: [{ required: true, message: "审核不通过请输入原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "审核不通过请输入原因"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "通过",
                    cb: this.processOk,
                },
            ],
            submit_btn_propps: {
                text: "不通过",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.task_service,
                operation_option: {
                    query: {
                        func_name: "get_to_be_processed_task_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            // succ_func: () => { this.setState({ modal_visible: false, }); },
            // id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <Row>
                <Modal
                    visible={this.state.modal_visible}
                    onCancel={this.handleCancel}
                    width={900}
                    okButtonProps={{ disabled: true, ghost: true }}
                    cancelButtonProps={{ disabled: true, ghost: true }}
                >
                    <FormCreator {...edit_props_list} />
                </Modal>
                <SignFrameLayout {...task_list} />
            </Row>
        );
    }
}

/**
 * 控件：任务管理系统未审核任务列表
 * 任务管理系统未审核任务列表
 */
@addon('TaskToBeProcessView', '任务管理系统未审核任务列表', '任务管理系统未审核任务列表')
@reactControl(TaskToBeProcessView, true)
export class TaskToBeProcessViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}