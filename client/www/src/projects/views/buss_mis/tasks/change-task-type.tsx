import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";
/**
 * 组件：任务类型详情
 */
export interface ChangeTaskTypeViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
    task_data?: [];
}

/**
 * 组件：任务类型详情
 */
export default class ChangeTaskTypeView extends ReactView<ChangeTaskTypeViewControl, ChangeTaskTypeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            task_data: [],
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }
    /** 返回回调 */
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.taskTypeList);
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        var edit_props = {
            form_items_props: [
                {
                    title: '添加任务类型',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "任务类型名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务类型名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务类型名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "任务类型图标（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "type_icon",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入任务类型图标" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入任务类型图标",
                                action: remote.upload_url,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.goBack();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.task_service,
                operation_option: {
                    save: {
                        func_name: "update_task_type"
                    },
                    query: {
                        func_name: "get_task_type_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：任务类型详情
 * @description 任务类型详情
 * @author
 */
@addon('ChangeTaskTypeView', '任务类型详情', '任务类型详情')
@reactControl(ChangeTaskTypeView, true)
export class ChangeTaskTypeViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
}