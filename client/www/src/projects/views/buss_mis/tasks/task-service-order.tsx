import { Row, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
// import { request } from "src/business/util_tool";

const { Option } = Select;
/**
 * 组件：服务订单分派任务列表状态
 */
export interface TaskServiceOrderViewState extends ReactViewState {
    // 弹出框是否显示
    modal_visible?: boolean;
    // id
    id?: string;
    // 选择行的Id集合
    ids?: string[];
    // 任务总数总条数
    total?: number;
    // 任务列表
    task_list?: any[];
    // 任务类型
    task_type?: any[];
    // 可分派人员
    task_receive_person?: any[];
    // 是否多选
    isMany: boolean;
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：服务订单分派任务列表
 * 服务订单分派任务列表
 */
export class TaskServiceOrderView extends ReactView<TaskServiceOrderViewControl, TaskServiceOrderViewState> {
    private columns_data_source = [{
        title: '任务名称',
        dataIndex: 'task_name',
        key: 'task_name',
    }, {
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
    }, {
        title: '服务对象',
        dataIndex: 'purchaser_name',
        key: 'purchaser_name',
    }, {
        title: '联系电话',
        dataIndex: 'purchaser_telephone',
        key: 'purchaser_telephone',
    }, {
        title: '服务地址',
        dataIndex: 'task_address',
        key: 'task_address',
    }, {
        title: '任务状态',
        dataIndex: 'task_state',
        key: 'task_state',
    }, {
        title: '任务创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            ids: [],
            id: '',
            task_type: [],
            task_receive_person: [],
            modal_visible: false,
            isMany: false,
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
    }

    // 行选择事件
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            ids,
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeTaskServiceOrder + '/' + contents.id);
        }
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let TaskState = {
            // to_be_receive: '待接收',
            ongoing: '服务中',
            completed: '已完成',
        };
        let task_state: any = [];
        for (let i in TaskState) {
            if (TaskState[i]) {
                task_state.push(<Option key={i} value={TaskState[i]}>{TaskState[i]}</Option>);
            }
        }
        let task_option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "任务名称",
                decorator_id: "task_name"
            },
            {
                type: InputType.select,
                label: "任务状态",
                decorator_id: "task_state",
                option: {
                    placeholder: "请选择任务状态",
                    showSearch: true,
                    childrens: task_state,
                }
            }],
            btn_props: [],
            // on_row_selection: this.on_row_selection,
            on_icon_click: this.onIconClick,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            searchExtraParam: { user_filter: true },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
        };
        let task_list = Object.assign(task_option, table_param);
        return (
            <Row>
                <SignFrameLayout {...task_list} />
            </Row>
        );
    }
}

/**
 * 控件：服务订单分派任务列表
 * 服务订单分派任务列表
 */
@addon('TaskServiceOrderView', '服务订单分派任务列表', '服务订单分派任务列表')
@reactControl(TaskServiceOrderView, true)
export class TaskServiceOrderViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
}