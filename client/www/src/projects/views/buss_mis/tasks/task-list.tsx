import { Modal, Row, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param, taskState } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
const { Option } = Select;
/**
 * 组件：任务管理系统状态
 */
export interface TaskListViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    // 任务总数总条数
    total?: number;
    // 任务列表
    task_list?: any[];
    // 任务类型
    task_type?: any[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：任务管理系统
 * 描述
 */
export class TaskListView extends ReactView<TaskListViewControl, TaskListViewState> {
    private columns_data_source = [{
        title: '任务名称',
        dataIndex: 'task_name',
        key: 'task_name',
    }, {
        title: '任务创建人',
        dataIndex: 'task_creator_name',
        key: 'task_creator_name',
    }, {
        title: '任务类型',
        dataIndex: 'task_type_name',
        key: 'task_type_name',
    }, {
        title: '紧急程度',
        dataIndex: 'task_urgent',
        key: 'task_urgent',
    }, {
        title: '任务状态',
        dataIndex: 'task_state',
        key: 'task_state',
    }, {
        title: '任务创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            task_type: [],
            ids: [],
            request_url: '',
        };
    }

    // 创建任务
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeTask);
    }
    componentDidMount() {
        // 获取任务类型
        request(this, AppServiceUtility.task_service.input_task_type!()!)
            .then((datas: any) => {
                this.setState({
                    task_type: datas,
                });
            });
    }

    // 查看任务详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeTask + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let task_state: any = [];
        for (let i in taskState) {
            if (taskState[i]) {
                task_state.push(<Option key={i} value={taskState[i]}>{taskState[i]}</Option>);
            }
        }
        let type = this.state.task_type;
        let task_type: any[] = [];
        type!.map((item: any) => {
            task_type.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let task_option = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "任务名称",
                decorator_id: "task_name"
            },
            {
                type: InputType.select,
                label: "任务状态",
                decorator_id: "task_state",
                option: {
                    placeholder: "请选择任务状态",
                    showSearch: true,
                    childrens: task_state,
                }
            },
            {
                type: InputType.radioGroup,
                label: "任务紧急程度",
                decorator_id: "task_urgent",
                option: {
                    placeholder: "请选择任务紧急程度",
                    options: [{
                        label: '紧急',
                        value: '紧急',
                    }, {
                        label: '一般',
                        value: '一般',
                    }]
                },
            },
            {
                type: InputType.select,
                label: "任务类型",
                decorator_id: "task_type",
                option: {
                    placeholder: "请选择任务类型",
                    showSearch: true,
                    childrens: task_type,
                }
            }],
            btn_props: [{
                label: '创建任务',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.task_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let task_list = Object.assign(task_option, table_param);
        return (
            <Row>
                <Modal
                    width={900}
                >
                    <NTOperationTable
                        data_source={this.state.task_list}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal>
                <SignFrameLayout {...task_list} />
            </Row>
        );
    }
}

/**
 * 控件：任务管理系统
 * 描述
 */
@addon('TaskListView', '任务管理系统', '描述')
@reactControl(TaskListView, true)
export class TaskListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}