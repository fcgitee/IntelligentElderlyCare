import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：任务类型状态
 */
export interface TaskTypeListViewState extends ReactViewState {
    // 任务总数总条数
    total?: number;
    // 任务列表
    task_list?: any[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：任务类型
 * 描述
 */
export class TaskTypeListView extends ReactView<TaskTypeListViewControl, TaskTypeListViewState> {
    private columns_data_source = [{
        title: '任务类型名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '任务图标',
        dataIndex: 'type_icon',
        key: 'type_icon',
        render: (text: string, record: any, index: any) => {
            return (
                <img style={{ height: '40px', width: '40px', marginRight: '10px', }} src={text} />
            );
        }
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }, {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
        };
    }

    // 创建任务类型
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeTaskType);
    }

    // 查看任务详情
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeTaskType + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let task_option = {
            edit_form_items_props: [{
                type: InputType.input,
                label: "任务类型名称",
                decorator_id: "name"
            }],
            btn_props: [{
                label: '添加任务类型',
                btn_method: this.add,
                icon: 'plus'
            },],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.task_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let task_list = Object.assign(task_option, table_param);
        return (
            <Row>
                <SignFrameLayout {...task_list} />
            </Row>
        );
    }
}

/**
 * 控件：任务类型
 * 描述
 */
@addon('TaskTypeListView', '任务类型', '描述')
@reactControl(TaskTypeListView, true)
export class TaskTypeListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}