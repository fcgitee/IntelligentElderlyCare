import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Card, Button, Row } from "antd";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
/**
 * 组件：导入Excel状态
 */
export interface ImportExcelViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    config?: any;
    action?: any;
}

/**
 * 组件：导入Excel
 * 描述
 */
export class ImportExcelView extends ReactView<ImportExcelViewControl, ImportExcelViewState> {
    constructor(props: ImportExcelViewControl) {
        super(props);
        this.state = {
            request_url: '',
            action: 'ImportExcel',
            config: {
                isFormat: true,
                upload: false,
                func: AppServiceUtility.person_org_manage_service.import_excel_manage,
                title: '导入Excel'
            }
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 导入接口 */
    upload = (type: string) => {
        let config = this.state.config;
        config.upload = true;
        config.type = type;
        this.setState({
            config,
            action: type,
        });
    }
    send = (type: string) => {
        request(this, AppServiceUtility.person_org_manage_service.import_excel_manage!(type))
            .then((data: any) => {
                if (data) {
                    window.alert(data);
                }
            });
    }
    render() {
        return (
            <MainContent>
                <Card title={this.state.action}>
                    <Row>
                        <Button onClick={() => this.upload('活动')}>导入活动</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('资讯')}>导入资讯</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('服务订单')}>导入订单</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('服务工单')}>导入工单</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('工作人员')}>导入工作人员</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('长者')}>导入长者</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.upload('评估记录')}>导入评估记录</Button>
                    </Row>
                    <br /><br /><br />
                    <Row>
                        <Button onClick={() => this.send('修复活动报名')}>修复活动报名</Button>
                    </Row>
                    <UploadFile {...this.state.config} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 导入Excel页面
 */
@addon('ImportExcelView', '导入Excel页面', '描述')
@reactControl(ImportExcelView, true)
export class ImportExcelViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 