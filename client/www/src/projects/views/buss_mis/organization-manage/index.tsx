
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Button, message, Select } from 'antd';
let { Option } = Select;

/** 状态：能力评估视图 */
export interface OrganizationManageNewViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    administrative_division_list?: any;
    /* 编辑权限按钮 */
    is_edit_btn?: any;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：能力评估视图 */
export class OrganizationManageNewView extends React.Component<OrganizationManageNewViewControl, OrganizationManageNewViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '组织机构名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '养老服务类型',
            dataIndex: 'org_type',
            key: 'org_type',
        },
        {
            title: '机构性质',
            dataIndex: 'org_nature',
            key: 'org_nature',
        },
        {
            title: '行政区划',
            dataIndex: 'area_name',
            key: 'area_name',
        },
        {
            title: 'APP上架/App下架',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                this.state.is_edit_btn ?
                    (record['organization_info'] && record['organization_info']['is_show_app'] && record['organization_info']['is_show_app'] === '下架' ?
                        <Button type="primary" onClick={() => { this.shangjia(record.id); }}>上架</Button>
                        :
                        <Button type="danger" onClick={() => { this.xiajia(record.id); }}>下架</Button>)
                    : ''
            )
        },
        {
            title: '机构启用/机构停用',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                this.state.is_edit_btn ?
                    (record['account_status'] && record['account_status'] === '停用' ?
                        <div style={{ marginRight: '5px' }}>已停用<Button type="primary" onClick={() => { this.start(record.id); }}>启用</Button></div>
                        :
                        <div style={{ marginRight: '5px' }}>已启用<Button type="danger" onClick={() => { this.stop(record.id); }}>停用</Button></div>)
                    : ''
            )
        }];
    constructor(props: OrganizationManageNewViewControl) {
        super(props);
        this.state = {
            administrative_division_list: [],
            is_edit_btn: false,
            org_list: [],
        };
    }
    /* 上架 */
    shangjia = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_show_app!({ id, "show": "上架" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('上架成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }

    /* 下架 */
    xiajia = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_show_app!({ id, "show": "下架" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('下架成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 启用 */
    start = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_start_stop!({ id, "show": "启用" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('启用成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 停用 */
    stop = (id: any) => {
        AppServiceUtility.person_org_manage_service.update_org_start_stop!({ id, "show": "停用" })!
            .then(data => {
                if (data === 'Success') {
                    message.success('停用成功');
                    this.formCreator && this.formCreator.reflash();
                }
            })
            .catch(err => {
                console.info(err);
            });
    }

    /** 新增按钮 */
    add = () => {
        window.open(ROUTE_PATH.changeOrganization, '_blank');
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            window.open(ROUTE_PATH.changeOrganization + '/' + contents.id, '_blank');
        }
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        }
                    });
                }
            });
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let assessment_template = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "name"
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: "机构性质",
                    decorator_id: "org_nature",
                    option: {
                        childrens: ['公办', '公办民营', '民办非盈利', '民办企业'].map((item, idx) => <Option key={item} value={item}>{item}</Option>),
                        placeholder: "请选择机构性质",
                        showSearch: true
                    },
                },
                {
                    type: InputType.tree_select,
                    label: "行政区划",
                    decorator_id: "admin_area_id",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        allowClear: true,
                        treeDefaultExpandAll: true,
                        treeData: this.state.administrative_division_list,
                        placeholder: "请选择行政区划",
                        autocomplete: 'off',
                    },
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.props.org_type ? { "org_type": this.props.org_type } : {}, 1, 10]
                },
                delete: {
                    service_func: 'del_organization'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_template, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：能力评估视图控制器
 * @description 能力评估视图
 * @author
 */
@addon('OrganizationManageNewView', '能力评估视图', '能力评估视图')
@reactControl(OrganizationManageNewView, true)
export class OrganizationManageNewViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: any;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 机构类型 */
    public org_type?: string;
}