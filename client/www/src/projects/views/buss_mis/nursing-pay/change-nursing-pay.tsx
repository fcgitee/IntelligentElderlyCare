import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { Select } from "antd";
let Option = Select.Option;
/**
 * 状态：护理收费编辑页面
 */
export interface ChangeNursingPayViewState extends ReactViewState {
    project_list?: any;
    order_list?: any;
}

/**
 * 组件：护理收费编辑页面视图
 */
export class ChangeNursingPayView extends ReactView<ChangeNursingPayViewControl, ChangeNursingPayViewState> {
    constructor(props: ChangeNursingPayViewControl) {
        super(props);
        this.state = {
            project_list: [],
            order_list: [],
        };
    }
    componentDidMount() {
        // 护理项目列表
        AppServiceUtility.assessment_project_service.get_assessment_project_list_all!({})!
            .then(data => {
                this.setState({
                    project_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 服务订单
        AppServiceUtility.service_operation_service.get_service_order_list!({})!
            .then(data => {
                this.setState({
                    order_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let project_list: JSX.Element[] = this.state.project_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.project_name}</Option>
            );
        });
        let order_list: JSX.Element[] = this.state.order_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.order_code}</Option>
            );
        });
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "人员名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '身份证号',
                dataIndex: 'id_card',
                key: 'id_card',
            }],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_worker_list_all',
            title: '人员查询',
            select_option: {
                placeholder: "请选择人员",
            }
        };
        let edit_props = {
            form_items_props: [{
                title: "护理收费",
                need_card: true,
                input_props: [
                    {
                        type: InputType.select,
                        label: "选择订单编号",
                        decorator_id: 'order_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择订单编号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择订单编号",
                            childrens: order_list,
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.select,
                        label: "选择护理项目",
                        decorator_id: 'project_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择护理项目" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择护理项目",
                            childrens: project_list,
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.modal_search,
                        label: "选择护理人员",
                        decorator_id: 'user_id',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择护理人员" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择护理人员",
                            autoComplete: 'off',
                            modal_search_items_props: modal_search_items_props
                        },
                    },
                    {
                        type: InputType.date,
                        label: "护理开始时间",
                        decorator_id: 'begin_date',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择护理开始时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: '请选择护理开始时间',
                            format: "YYYY-MM-DD HH:mm:ss",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.date,
                        label: "护理结束时间",
                        decorator_id: 'end_date',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请选择护理结束时间" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: '请选择护理结束时间',
                            format: "YYYY-MM-DD HH:mm:ss",
                            autoComplete: 'off',
                        },
                    },
                    {
                        type: InputType.antd_input_number,
                        label: "金额",
                        decorator_id: 'price',
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入金额" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入金额",
                            autoComplete: 'off',
                        },
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    history.back();
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_nursing_pay_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_nursing_pay"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：护理收费编辑页面控件
 * @description 护理收费编辑页面控件
 * @author
 */
@addon('ChangeNursingPayViewControl', '护理收费编辑页面控件', '护理收费编辑页面控件')
@reactControl(ChangeNursingPayView, true)
export class ChangeNursingPayViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}