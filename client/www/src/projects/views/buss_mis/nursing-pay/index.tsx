import { Row, Select } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';
let Option = Select.Option;
/**
 * 组件：护理收费列表状态
 */
export interface NursingPayListViewState extends ReactViewState {
    project_list?: any;
}

/**
 * 组件：护理收费列表
 * 描述
 */
export class NursingPayListView extends ReactView<NursingPayListViewControl, NursingPayListViewState> {
    private columns_data_source = [{
        title: '服务订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    }, {
        title: '护理项目',
        dataIndex: 'project_name',
        key: 'project_name',
    }, {
        title: '护理开始时间',
        dataIndex: 'begin_date',
        key: 'begin_date',
    }, {
        title: '护理完成时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '护理人员',
        dataIndex: 'nursing_name',
        key: 'nursing_name',
    }, {
        title: '护理金额',
        dataIndex: 'price',
        key: 'price',
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            project_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeNursingPay);
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeNursingPay + '/' + contents.id);
        }
    }
    componentDidMount() {
        // 护理项目列表
        AppServiceUtility.assessment_project_service.get_assessment_project_list_all!({})!
            .then(data => {
                this.setState({
                    project_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let project_list: JSX.Element[] = this.state.project_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.project_name}</Option>
            );
        });
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务订单编号",
                    decorator_id: "record_code",
                    option: {
                        placeholder: "请输入服务订单编号",
                        autoComplete: 'off',
                    }
                },
                {
                    type: InputType.select,
                    label: "护理项目",
                    decorator_id: "project_id",
                    option: {
                        placeholder: "请选择护理项目",
                        childrens: project_list,
                        autoComplete: 'off',
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_nursing_pay_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_nursing_pay'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <Row>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：护理收费列表
 * 描述
 */
@addon('NursingPayListView', '护理收费列表', '描述')
@reactControl(NursingPayListView, true)
export class NursingPayListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}