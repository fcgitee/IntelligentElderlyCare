
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Select, Button } from 'antd';
import { getAge } from "src/projects/app/util-tool";
const { Option } = Select;
/** 状态：智能监护管理视图 */
export interface CareManageViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_url?: string;
}
/** 组件：智能监护管理视图 */
export class CareManageView extends React.Component<CareManageViewControl, CareManageViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card',
        key: 'id_card',
    },
    {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
        render: (text: any, record: any) => {
            return getAge(record.id_card);
        }
    },
    {
        title: '联系号码',
        dataIndex: 'telephone',
        key: 'telephone',
    },
    {
        title: '家庭住址',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: '入住养老机构',
        dataIndex: 'org',
        key: 'org',
    },
    {
        title: '床位',
        dataIndex: 'bed',
        key: 'bed',
    },
    {
        title: '使用设备名称',
        dataIndex: 'device_name',
        key: 'device_name',
    },
    {
        title: '设备ID',
        dataIndex: 'device_id',
        key: 'device_id',
    },
    {
        title: '设备SIM卡',
        dataIndex: 'device_sim',
        key: 'device_sim',
    },
    {
        title: '设备有效期',
        dataIndex: 'long',
        key: 'long',
    },
    {
        title: '设备状态',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text: any, record: any) => (
            <span style={{ display: 'flex' }}>
                <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.ctrl('location', record); }}>定位/轨迹</Button>
                <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.ctrl('fence', record); }}>电子围栏</Button>
                <Button onClick={() => { this.ctrl('edit', record); }}>编辑</Button>
            </span>
        ),
    },
    ];
    constructor(props: CareManageViewControl) {
        super(props);
        this.state = {
            status: ["有效", "过期"],
            request_url: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        // this.props.history!.push(ROUTE_PATH.addArchives);
    }
    ctrl = (type: string, record: any) => {
        if (type === 'fence') {
            this.props.history!.push(ROUTE_PATH.electronicFence + '/' + record.id);
        } else if (type === 'location') {
            this.props.history!.push(ROUTE_PATH.deviceLocation + '/' + record.device_id);
        } else if (type === 'edit') {
            this.props.history!.push(ROUTE_PATH.ChangeDevice + '/' + record.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let care_manage = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                },
                {
                    type: InputType.input,
                    label: "设备名称",
                    decorator_id: "device_name"
                },
                {
                    type: InputType.input,
                    label: "设备SIM卡",
                    decorator_id: "device_sim"
                },
                {
                    type: InputType.select,
                    label: "设备状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择设备状态",
                        showSearch: true,
                        childrens: this.state.status!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>),
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: 'get_device_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_device'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let care_manage_list = Object.assign(care_manage, table_param);
        return (
            <SignFrameLayout {...care_manage_list} />
        );
    }
}

/**
 * 控件：智能监护管理视图控制器
 * @description 智能监护管理视图
 * @author
 */
@addon('CareManageView', '智能监护管理视图', '智能监护管理视图')
@reactControl(CareManageView, true)
export class CareManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}