import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Button, message, Row, Spin } from 'antd';
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import LocationMap from "./location-map";
import "./index.less";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
/**
 * 组件：设备定位/轨迹页面
 */
export interface DeviceLocationViewState extends ReactViewState {
    data_info: any;
    is_loading: boolean;
    map_type: string;
}

/**
 * 组件：设备定位/轨迹页面
 * 描述
 */
export class DeviceLocationView extends ReactView<DeviceLocationViewControl, DeviceLocationViewState> {
    constructor(props: DeviceLocationViewControl) {
        super(props);
        this.state = {
            is_loading: false,
            data_info: {
                lng: 113.14958,
                lat: 23.035191,
                address: '智慧养老平台',
                points: [],
            },
            map_type: 'location',
        };
    }

    componentDidMount = () => {
        this.ctrl('getLocation');
    }

    ctrl = (type: string) => {
        if (type === 'setLocation') {
            this.setState(
                {
                    map_type: 'location',
                    is_loading: true
                },
                () => {
                    this.ctrl('getLocation');
                }
            );
        } else if (type === 'setFootprint') {
            this.setState(
                {
                    map_type: 'footprint',
                    is_loading: true
                },
                () => {
                    this.ctrl('getFootprint');
                }
            );
        } else if (type === 'getLocation') {
            request(this, AppServiceUtility.device_service.get_device_location!({ id: this.props.match!.params.key, from: 'device' }))
                .then((datas: any) => {
                    if (datas && datas.code === 0) {
                        this.setState({
                            data_info: datas.msg
                        });
                    } else {
                        message.error(datas.msg);
                    }
                })
                .catch((err) => {
                    console.error(err);
                }).finally(() => {
                    this.setState({
                        is_loading: false,
                    });
                });
        } else if (type === 'getFootprint') {
            request(this, AppServiceUtility.device_service.get_device_footprint!({ id: this.props.match!.params.key, from: 'device' }))
                .then((datas: any) => {
                    if (datas && datas.code === 0) {
                        if (datas.msg && datas.msg.length) {
                            this.setState({
                                data_info: {
                                    lat: datas.msg[0]['lat'],
                                    lng: datas.msg[0]['lng'],
                                    points: datas.msg,
                                },
                            });
                        }
                    } else {
                        message.error(datas.msg);
                    }
                })
                .catch((err) => {
                    console.error(err);
                }).finally(() => {
                    this.setState({
                        is_loading: false,
                    });
                });
        } else if (type === 'back') {
            history.back();
        }
    }

    render() {
        const { is_loading, data_info, map_type } = this.state;
        return (
            <Spin spinning={is_loading}>
                <MainContent>
                    <MainCard>
                        <Row type="flex" justify="space-between" align="middle" style={{ marginBottom: 20, height: 40 }}>
                            <span style={{ fontSize: 20 }}>定位/轨迹</span>
                            <span>
                                <Button style={{ marginRight: 8 }} type="primary" onClick={() => { this.ctrl('back'); }}>返回</Button>
                            </span>
                        </Row>
                        <Row>
                            {data_info.lng && data_info.lat ? <LocationMap lng={data_info.lng} lat={data_info.lat} address={data_info.address} map_type={map_type} points={data_info.points} /> : null}
                            <div className="toggle-btns">
                                <span onClick={() => this.ctrl('setLocation')} className={`location${map_type === 'location' ? ' active' : ''}`}>实时定位</span>
                                <span onClick={() => this.ctrl('setFootprint')} className={`footprint${map_type === 'footprint' ? ' active' : ''}`}>历史轨迹</span>
                            </div>
                        </Row>
                    </MainCard>
                </MainContent>
            </Spin >
        );
    }
}

/**
 * 控件：设备定位/轨迹页面
 * 描述
 */
@addon('DeviceLocationView', '设备定位/轨迹页面', '设备定位/轨迹页面')
@reactControl(DeviceLocationView, true)
export class DeviceLocationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}