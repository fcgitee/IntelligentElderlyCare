import React from 'react';
import Map from 'react-bmapgl/Map';
import Marker from 'react-bmapgl/Overlay/Marker';
import InfoWindow from 'react-bmapgl/Overlay/InfoWindow';
import Polyline from 'react-bmapgl/Overlay/Polyline';

class LocationMap extends React.Component {
    render() {
        const { lng, lat, name, address, map_type, points } = this.props;
        return (
            map_type ?
                <Map
                    // zoom={17}
                    amapkey={"jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji"}
                    center={{ lng: lng, lat: lat }}
                    enableDragging={true}
                    enableScrollWheelZoom={true}
                    style={{ height: "calc(100vh - 303px)" }}>
                    {
                        map_type === 'location' ? <Marker position={{ lng: lng, lat: lat }} /> : null
                    }
                    {
                        map_type === 'location' ? <InfoWindow
                            position={{ lng: lng, lat: lat }}
                        >
                            <div style={{ fontSize: 14 }}>{address}</div>
                        </InfoWindow> : null
                    }
                    {
                        map_type === 'footprint' && points && points.length && points.map ?
                            <Polyline
                                key={Math.random()}
                                path={
                                    points.map((item, index) => {
                                        return (
                                            new BMapGL.Point(item.lng, item.lat)
                                        )
                                    })
                                }
                                strokeColor="#f00"
                                strokeWeight={3}
                            /> : null
                    }
                    {
                        map_type === 'footprint' && points && points.length && points.map ?
                            points.map((item, index) => {
                                return (
                                    <Marker key={index} position={{ lng: item.lng, lat: item.lat }} />
                                );
                            }) : null
                    }
                </Map>
                : null
        )
    }
}

export default LocationMap;