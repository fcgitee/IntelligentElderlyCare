
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Card, Tabs } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
const { TabPane } = Tabs;
/** 状态：智能设备警报视图 */
export interface DeviceMessageViewState extends ReactViewState {
    active_key: string;
}
/** 组件：智能设备警报视图 */
export class DeviceMessageView extends React.Component<DeviceMessageViewControl, DeviceMessageViewState> {
    private columns_data_source = [
        {
            title: '设备名称',
            dataIndex: 'device_name',
            key: 'device_name',
        },
        {
            title: '设备ID',
            dataIndex: 'imei',
            key: 'imei',
        },
        {
            title: '警报信息',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                if (text === 'ELECTRICITY') {
                    return `【电量】${record.remaining_power}%`;
                } else if (text === 'TEMPERATURE') {
                    return `【体温】${record.temperature}℃`;
                } else if (text === 'LOCATION') {
                    return `【坐标】经度：${record.lon}，纬度：${record.lat}，地址：${record.address}`;
                } else if (text === 'SOS') {
                    return `【SOS】经度：${record.lon}，纬度：${record.lat}，地址：${record.address}`;
                } else if (text === 'MESSAGE') {
                    if (record.type === 4) {
                        return `【电子围栏报警】`;
                    }
                }
                return null;
            }
        },
        {
            title: '报警时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
    ];
    constructor(props: DeviceMessageViewControl) {
        super(props);
        this.state = {
            active_key: '今日',
        };
    }
    onTabsChange = (e: any) => {
        this.setState({
            active_key: e,
        });
    }
    componentWillMount() {
    }
    getNowYMD = () => {
        let nowDate: Date = new Date(),
            nowYear: number | string = nowDate.getFullYear(),
            nowMonth: number | string = nowDate.getMonth() + 1,
            nowDay: number | string = nowDate.getDate();

        if (nowMonth.toString().length === 1) {
            nowMonth = '0' + nowMonth;
        }
        // 2020-11-13T06:33:56.796Z
        return `${String(nowYear)}-${String(nowMonth)}-${String(nowDay)}T00:00:00.000Z`;
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        if (this.props.match!.params.key) {
            param['imei'] = this.props.match!.params.key;
        }
        if (this.state.active_key && this.state.active_key === '今日') {
            param['date_range'] = this.getNowYMD();
        }
        let device_message = {
            type_show: false,
            // edit_form_items_props: [
            //     {
            //         type: InputType.select,
            //         label: "类型",
            //         decorator_id: "type",
            //         option: {
            //             placeholder: "请选择警报类型",
            //             showSearch: true,
            //             childrens: [
            //                 {
            //                     key: 'ELECTRICITY',
            //                     value: '电量'
            //                 }
            //             ].map((item, idx) => <Option key={idx} value={item.key}>{item.value}</Option>),
            //         }
            //     },
            // ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: 'get_device_message_list_all',
                    service_condition: [param, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            searchExtraParam: param,
        };
        let device_message_list = Object.assign(device_message, table_param);
        return (
            <MainContent>
                <Card>
                    <Tabs onChange={this.onTabsChange} type="card">
                        <TabPane tab="今日" key="今日" />
                        <TabPane tab="全部" key="全部" />
                    </Tabs>
                    <SignFrameLayout key={Math.random()} {...device_message_list} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：智能设备警报视图控制器
 * @description 智能设备警报视图
 * @author
 */
@addon('DeviceMessageView', '智能设备警报视图', '智能设备警报视图')
@reactControl(DeviceMessageView, true)
export class DeviceMessageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}