import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Button, message, Row, Spin } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import ElectronicFenceMap from "./map";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：电子围栏页面
 */
export interface ElectronicFenceViewState extends ReactViewState {
    center_lat: number;
    center_lng: number;
    loading: boolean;
    overlay: any;
    data_info: any;
}

/**
 * 组件：电子围栏页面
 * 描述
 */
export class ElectronicFenceView extends ReactView<ElectronicFenceViewControl, ElectronicFenceViewState> {
    private mapObject: any = null;
    constructor(props: ElectronicFenceViewControl) {
        super(props);
        this.state = {
            center_lat: 23.035191,
            center_lng: 113.14958,
            loading: false,
            overlay: {},
            data_info: {},
        };
    }

    componentDidMount = () => {
        request(this, AppServiceUtility.device_service.get_device_all!({ id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length > 0) {

                    let data_info = datas['result'][0];

                    let setStateParam: any = {};
                    // 数据源
                    setStateParam['data_info'] = data_info;
                    // 赋值原有的数据
                    if (data_info.overlay) {
                        setStateParam['overlay'] = data_info.overlay;
                    }

                    // 设置中心点，好看点
                    if (data_info.overlay) {
                        if (data_info.overlay.drawing_mode === 'rectangle') {
                            setStateParam['center_lng'] = (data_info.overlay.points[0].lng + data_info.overlay.points[1].lng) / 2;
                            setStateParam['center_lat'] = (data_info.overlay.points[0].lat + data_info.overlay.points[2].lat) / 2;
                        } else if (data_info.overlay.drawing_mode === 'circle') {
                            setStateParam['center_lng'] = data_info.overlay.points[0].lng;
                            setStateParam['center_lat'] = data_info.overlay.points[0].lat;
                        }
                    }

                    this.setState(setStateParam);
                }
            });
    }

    ctrl = (type: string) => {
        switch (type) {
            case 'back':
                history.back();
                break;
            case 'clear':
                this.setState(
                    {
                        overlay: {},
                    },
                    () => {
                        this.mapObject && this.mapObject.map && this.mapObject.map.clearOverlays();
                        $('.BMapGLLib_Drawing').fadeIn();
                    }
                );
                break;
            case 'save':
                const { overlay } = this.state;
                if (overlay.type === 0) {
                    message.error('请选择电子围栏范围！');
                    return;
                }
                this.setState(
                    {
                        loading: true,
                    },
                    () => {
                        this.setState({
                            loading: false,
                        });
                        let param: any = {};
                        param['id'] = this.props.match!.params.key;
                        param['overlay'] = overlay;
                        // 保存围栏坐标
                        request(this, AppServiceUtility.device_service.update_device_weilan!(param)!)
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    message.success('保存成功！', 1, () => {
                                        history.back();
                                    });
                                } else {
                                    message.error(datas);
                                }
                            }).catch((error: Error) => {
                                message.error(error.message);
                            });
                    }
                );
                break;
            default:
                break;
        }
    }

    initMap = () => {
        let geolocation = new BMap.Geolocation();
        // 开启SDK辅助定位
        geolocation.enableSDKLocation();
        geolocation.getCurrentPosition((response: any) => {
            this.setState({
                loading: false,
            });
            if (response && response.point && response.point.lat && response.point.lng) {
                this.setState({
                    center_lat: response.point.lat,
                    center_lng: response.point.lng,
                });
            }
        });
    }

    cb = (e: any) => {
        if (e) {
            this.mapObject = e;
        }
    }

    onComplete = (e: any, info: any) => {
        if (info.drawingMode) {
            let overlay: any = {
                points: [],
                drawing_mode: info.drawingMode,
            };
            if (info.drawingMode === 'rectangle') {
                // 矩形
                for (let i = 0; i < info.overlay.points.length; i++) {
                    if (info.overlay.points[i].latLng) {
                        overlay.points.push({
                            lat: info.overlay.points[i].latLng['lat'],
                            lng: info.overlay.points[i].latLng['lng'],
                        });
                    }
                }
            } else if (info.drawingMode === 'circle') {
                // 圆形
                overlay.points.push({
                    lat: info.overlay.point.latLng['lat'],
                    lng: info.overlay.point.latLng['lng'],
                });
                // 半径
                overlay.radius = info.overlay.radius;
            }
            this.setState(
                {
                    overlay
                },
                () => {
                    $('.BMapGLLib_Drawing').fadeOut();
                }
            );
        }
    }

    render() {
        const { loading, data_info, center_lat, center_lng } = this.state;
        return (
            <Spin spinning={loading}>
                <MainContent>
                    <MainCard>
                        <Row type="flex" justify="space-between" align="middle" style={{ marginBottom: 20, height: 40 }}>
                            <span style={{ fontSize: 20 }}>电子围栏</span>
                            <span>
                                <Button style={{ marginRight: 8 }} type="primary" onClick={() => { this.ctrl('back'); }}>返回</Button>
                                <Button style={{ marginRight: 8 }} type="primary" onClick={() => { this.ctrl('save'); }}>保存</Button>
                                <Button style={{ marginRight: 8 }} type="primary" onClick={() => { this.ctrl('clear'); }}>清空</Button>
                            </span>
                        </Row>
                        <ElectronicFenceMap center_lng={center_lng} center_lat={center_lat} cb={this.cb} onComplete={this.onComplete} defaultOverlay={data_info.overlay} />
                    </MainCard>
                </MainContent>
            </Spin>
        );
    }
}

/**
 * 控件：电子围栏页面
 * 描述
 */
@addon('ElectronicFenceView', '电子围栏页面', '提示')
@reactControl(ElectronicFenceView, true)
export class ElectronicFenceViewControl extends ReactViewControl {
}