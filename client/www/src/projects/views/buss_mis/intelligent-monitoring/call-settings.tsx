import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：智能监护通话设置状态
 */
export interface CallSettingsViewState extends ReactViewState {
    status?: any;
    elder_name?: any;
    device_id?: any;
    device_name?: any;
}

/**
 * 组件：智能监护通话设置
 * 描述
 */
export class CallSettingsView extends ReactView<CallSettingsViewControl, CallSettingsViewState> {
    constructor(props: CallSettingsViewControl) {
        super(props);
        this.state = {
            status: ["分类1", "分类2", "分类3", "分类4"],
            device_id: '',
            elder_name: '',
            device_name: ''
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            AppServiceUtility.device_service.get_device_all!({ id: this.props.match!.params.key }, 1, 1)!
                .then((data: any) => {
                    // console.log('数据》？？', data);
                    this.setState({
                        device_id: data.result[0].device_id,
                        elder_name: data.result[0].elder_name,
                        device_name: data.result[0].device_name,
                    });
                });
        }
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "智能通话管理",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "设备名称",
                            decorator_id: "device_name",
                            field_decorator_option: {
                                initialValue: this.state.device_name ? this.state.device_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                                placeholder: this.state.device_name
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "设备id",
                            decorator_id: "device_id",
                            field_decorator_option: {
                                initialValue: this.state.device_id ? this.state.device_id : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                                placeholder: this.state.device_id
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "绑定长者",
                            decorator_id: "",
                            field_decorator_option: {
                                initialValue: this.state.elder_name ? this.state.elder_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                                placeholder: this.state.elder_name
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "紧急呼叫电话（红键）",
                            decorator_id: "emergency_call",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入紧急呼叫电话" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入紧急呼叫电话"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "紧急求助电话（黄键）",
                            decorator_id: "emergency_qz_phone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入紧急求助电话" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入紧急求助电话"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "呼叫亲情电话（绿键）",
                            decorator_id: "emergency_family_phone",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入呼叫亲情电话" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入呼叫亲情电话"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入备注" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.XxhDevice);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.device_service,
                operation_option: {
                    save: {
                        func_name: "update_device"
                    },
                    query: {
                        func_name: "get_device_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.XxhDevice); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：智能监护通话设置
 * 描述
 */
@addon('CallSettingsView', '智能监护通话设置', '描述')
@reactControl(CallSettingsView, true)
export class CallSettingsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}