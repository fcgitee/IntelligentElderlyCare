
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Button } from 'antd';
/** 状态：智能监护运营视图 */
export interface GuardianshipOperationViewState extends ReactViewState {
}
/** 组件：智能监护运营视图 */
export class GuardianshipOperationView extends React.Component<GuardianshipOperationViewControl, GuardianshipOperationViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card',
        key: 'id_card',
    },
    {
        title: '使用设备名称',
        dataIndex: 'device_name',
        key: 'device_name',
    },
    {
        title: '设备ID',
        dataIndex: 'device_id',
        key: 'device_id',
    },
    {
        title: '设备SIM卡',
        dataIndex: 'device_sim',
        key: 'device_sim',
    },
    {
        title: '操作',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <span style={{ display: 'flex' }}>
                <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.ctrl('location', record); }}>定位/轨迹</Button>
                <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.ctrl('msg', record); }}>围栏报警</Button>
                <Button onClick={() => { this.ctrl('edit', record); }}>编辑</Button>
            </span>
        ),
    },
    ];
    constructor(props: GuardianshipOperationViewControl) {
        super(props);
        this.state = {
        };
    }
    /** 自定义图标按钮回调事件 */
    ctrl = (type: string, record: any) => {
        if (type === 'edit') {
            this.props.history!.push(ROUTE_PATH.ChangeDevice + '/' + record.id);
        } else if (type === 'msg') {
            this.props.history!.push(ROUTE_PATH.deviceMessage + '/' + record.device_id);
        } else if (type === 'location') {
            this.props.history!.push(ROUTE_PATH.deviceLocation + '/' + record.device_id);
        }
    }
    componentWillMount() {
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                },
                {
                    type: InputType.input,
                    label: "设备名称",
                    decorator_id: "device_name"
                },
                {
                    type: InputType.input,
                    label: "设备SIM卡",
                    decorator_id: "device_sim"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: 'get_device_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_device'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            // edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <SignFrameLayout {...allowance_read_list} />
        );
    }
}

/**
 * 控件：智能监护运营视图控制器
 * @description 智能监护运营视图
 * @author
 */
@addon('GuardianshipOperationView', '智能监护运营视图', '智能监护运营视图')
@reactControl(GuardianshipOperationView, true)
export class GuardianshipOperationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}