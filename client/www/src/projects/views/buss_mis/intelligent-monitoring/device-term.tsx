
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { MainContent } from "src/business/components/style-components/main-content";
import { Card, Select } from "antd";
const { Option } = Select;
/** 状态：智能设备期限视图 */
export interface DeviceTermViewState extends ReactViewState {
    active_key: string;
}
/** 组件：智能设备期限视图 */
export class DeviceTermView extends React.Component<DeviceTermViewControl, DeviceTermViewState> {
    private columns_data_source = [
        {
            title: '设备名称',
            dataIndex: 'device_name',
            key: 'device_name',
        },
        {
            title: '设备ID',
            dataIndex: 'device_id',
            key: 'device_id',
        },
        {
            title: '距离到期',
            dataIndex: 'type',
            key: 'type',
            render: (text: any, record: any) => {
                return this.getEndDis(record.service_date_end);
            }
        },
        {
            title: '到期时间',
            dataIndex: 'service_date_end',
            key: 'service_date_end',
        },
    ];
    getEndDis = (end_date: any) => {
        if (!end_date) {
            return null;
        }
        let end_date_obj: any = new Date(end_date),
            now_date_obj: any = new Date(),
            end_date_timestamp = Date.parse(end_date_obj) / 1000,
            now_date_timestamp = Date.parse(now_date_obj) / 1000,
            xiangcha_timestrap = end_date_timestamp - now_date_timestamp;

        let days: number = 0;
        let hours: number = 0;
        let minutes: number = 0;
        let seconds: number = 0;
        if (xiangcha_timestrap > 86400) {
            days = Math.floor(xiangcha_timestrap / 86400);
            xiangcha_timestrap = xiangcha_timestrap - days * 86400;
        }
        if (xiangcha_timestrap > 3600) {
            hours = Math.floor(xiangcha_timestrap / 3600);
            xiangcha_timestrap = xiangcha_timestrap - hours * 3600;
        }
        if (xiangcha_timestrap > 60) {
            minutes = Math.floor(xiangcha_timestrap / 60);
            xiangcha_timestrap = xiangcha_timestrap - minutes * 60;
        }
        if (xiangcha_timestrap > 0) {
            seconds = xiangcha_timestrap;
        }
        return `${days}天${hours}时${minutes}分${seconds}秒`;
    }
    constructor(props: DeviceTermViewControl) {
        super(props);
        this.state = {
            active_key: '今日',
        };
    }
    componentWillMount() {
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let device_message = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "到期期限",
                    decorator_id: "term_type",
                    option: {
                        placeholder: "请选择到期期限",
                        showSearch: true,
                        childrens: [
                            {
                                key: 'three_days',
                                value: '三天'
                            },
                            {
                                key: 'one_week',
                                value: '一周'
                            },
                            {
                                key: 'one_month',
                                value: '一个月'
                            }
                        ].map((item, idx) => <Option key={idx} value={item.key}>{item.value}</Option>),
                    }
                },
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.device_service,
            service_option: {
                select: {
                    service_func: 'get_device_term_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let device_message_list = Object.assign(device_message, table_param);
        return (
            <MainContent>
                <Card>
                    <SignFrameLayout key={Math.random()} {...device_message_list} />
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：智能设备期限视图控制器
 * @description 智能设备期限视图
 * @author
 */
@addon('DeviceTermView', '智能设备期限视图', '智能设备期限视图')
@reactControl(DeviceTermView, true)
export class DeviceTermViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}