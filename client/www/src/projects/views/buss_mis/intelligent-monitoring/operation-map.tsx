import { Button, Card, Col, Input, message, Row, Tabs } from 'antd';
import { addon, log } from "pao-aop";
import { CookieUtil, reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { COOKIE_OLDER_CALL_CENTER } from 'src/business/mainForm/backstageManageMainForm';
import { AppServiceUtility } from "src/projects/app/appService";
import { decodeUrlParam } from "src/projects/app/util-tool";
import { ServiceOrder } from 'src/projects/models/service-operation';
import ElectronicFenceMap from "./map";
const { TabPane } = Tabs;
/**
 * 组件：监护运营页面
 */
export interface OperationMapViewState extends ReactViewState {
    showState?: any;
    Infodata?: any;
    record_data?: any;
    caller?: any;
    type?: any;
    create_time?: any;
    cs_name?: any;
    edit_data?: any;
    note?: any;
    fullScrean?: boolean;
    remarkText?: string;
    url?: any;
    luyinState?: any;
    older_list: any[];
    ser_order_list: any[];
    ser_record_list: any[];
    tab_select_older?: ServiceOrder;
    on_duty_info: any;
}

/**
 * 组件：监护运营页面
 * 描述
 */
export class OperationMapView extends ReactView<OperationMapViewControl, OperationMapViewState> {
    constructor(props: OperationMapViewControl) {
        super(props);
        this.state = {
            showState: false,
            Infodata: {},
            record_data: [],
            caller: '',
            type: '',
            create_time: '',
            cs_name: '',
            edit_data: {},
            note: '',
            fullScrean: false,
            remarkText: "",
            url: '',
            luyinState: false,
            older_list: [],
            ser_order_list: [],
            ser_record_list: [],
            tab_select_older: undefined,
            on_duty_info: undefined,
        };
    }

    componentDidMount = () => {
        const that = this;

        if (this.props.match!.params.key !== undefined) {
            let Infodata = decodeUrlParam(this.props.match!.params.key);
            this.setState({
                Infodata: Infodata ? JSON.parse(Infodata) : {}
            });
        } else {
            const Infodata = CookieUtil.read(COOKIE_OLDER_CALL_CENTER);
            if (Infodata) {
                this.setState({
                    Infodata
                });
            } else {
                // TODO: 获取坐席人员位置信息
                AppServiceUtility.emi_service_no_loading!.get_cs_org_location!()!
                    .then(e => {
                        // console.log(e);

                        this.setState({
                            on_duty_info: e
                        });
                    })
                    .catch(e => {
                        console.error(e);
                    });
            }
        }

        const mapEle = document.getElementById("olderMap")!;
        mapEle.addEventListener("fullscreenchange", function (e) {
            if (document.fullscreenElement === null) {
                that.setState({
                    fullScrean: false
                });
            }
        });
    }
    showFull = () => {
        let full = document.getElementById("olderMap");
        if (full) {
            this.launchIntoFullscreen(full!);
            this.setState({
                fullScrean: true
            });
        }
    }

    delFull = () => {
        if (this.state.fullScrean === true) {
            this.exitFullscreen();
            this.setState({
                fullScrean: false
            });
        }
    }

    launchIntoFullscreen = (element: HTMLElement) => {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        }
    }

    exitFullscreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }

    onChangeRemark = (e: any) => {
        this.setState({ remarkText: e.target.value });
    }

    saveRemark = () => {
        // TODO:保存备注提示
        const { Infodata } = this.state;
        AppServiceUtility.emi_service_no_loading!.add_call_note!(Infodata['call_type'], Infodata['call_id'], this.state.remarkText!)!
            .then((data: any) => {
                // console.log(data);
                if (data === "Success") {
                    message.info('增加备注成功');
                }
            });
    }

    // 关闭弹框
    cancelTk = () => {
        this.setState({
            luyinState: false
        });
    }

    onOlderTabChange = (phoneNum_id: string) => {
        const [phoneNum, id] = phoneNum_id.split("_");
        console.warn(phoneNum, id);

        // if (id !== '') {
        //     this.getServOrderList({ purchaser_id: id });
        //     this.getServRecordList({ purchaser_id: id });
        // } else {

        // }
    }

    // onEscKeyDown = (e: any) => {
    //     e.preventDefault();
    //     // console.log('onEscKeyDown');

    //     let keyCode = e.keyCode;
    //     if (keyCode === 27) {
    //         this.delFull();
    //     }
    // }

    render() {
        const { Infodata, older_list, on_duty_info } = this.state;

        function getMapEle(elder_data: any) {
            if (elder_data.elder) {
                log("监护运营", "for debug");
                if (elder_data.elder['location'] !== undefined && elder_data.elder['location'] !== null) {
                    return (
                        <ElectronicFenceMap lng={113.14958} lat={23.035191} />
                    );
                }
            }
            if (on_duty_info) {
                return (
                    <ElectronicFenceMap lng={113.14958} lat={23.035191} />
                );
            }
            return (
                <ElectronicFenceMap lng={113.14958} lat={23.035191} />
            );
        }
        const remarkElement = (Infodata: any) => {
            // // console.log(Infodata);
            // // console.log(JSON.stringify(Infodata));
            // // console.log(JSON.stringify(Infodata) !== '{}');

            if (JSON.stringify(Infodata) !== '{}') {
                return (
                    <>
                        <Input.TextArea onChange={this.onChangeRemark} />
                        <Button onClick={this.saveRemark}>保存备注</Button>
                    </>
                );
            }
            return undefined;
        };

        const elder_info_ele = (Infodata: any, needRemark: boolean) => {
            if (JSON.stringify(Infodata) === "{}" || Infodata.elder) {
                return (
                    <TabPane tab={"长者信息"} key="长者信息_" closable={false}>
                        <Row>
                            <Col span={6}>
                                <div style={{ textAlign: 'center' }} >
                                    <img
                                        src={
                                            (() => {
                                                if (Infodata.elder) {
                                                    if (Infodata.elder.pic !== "") {
                                                        return Infodata.elder.pic;
                                                    }
                                                }
                                                return require("./img/older.jpg");
                                            })()
                                        }
                                        alt=""
                                        style={{ width: '200px', height: '200px' }}
                                    />
                                </div>
                                {
                                    needRemark ?
                                        remarkElement(Infodata)
                                        :
                                        undefined
                                }
                                {/* <Input.TextArea onChange={this.onChangeRemark} />
                                    <Button onClick={this.saveRemark}>保存备注</Button> */}
                            </Col>
                            {Infodata.elder ?
                                <Col span={6}>
                                    <Card title="长者信息">
                                        <p>姓名：<span>{Infodata.elder.name ? Infodata.elder.name : '空'}</span></p>
                                        <p>性别：<span>{Infodata.elder.sex ? Infodata.elder.sex : '空'}</span></p>
                                        <p>年龄：<span>{Infodata.elder.card_number ? new Date().getFullYear() - Infodata.elder.card_number.slice(6, 10) : '暂无相关信息'}</span></p>
                                        <p>家庭地址：<span>{Infodata.elder.address ? Infodata.elder.address : '空'}</span></p>
                                        <p>电话号码：<span>{Infodata.elder.tel ? Infodata.elder.tel : '空'}</span></p>
                                        <br />
                                        <p>证件类型：<span>{Infodata.elder.idCardType ? Infodata.elder.idCardType : '空'}</span></p>
                                        <p>证件号码：<span>{Infodata.elder.cardNumber ? Infodata.elder.cardNumber : '空'}</span></p>
                                    </Card>
                                </Col>
                                :
                                <Col span={6}>
                                    <Card title="长者信息">
                                        <p>查找不到相关长者信息，请登记</p>
                                    </Card>
                                </Col>
                            }
                            <Col span={10} >
                                <div id={'olderMap'} className={this.state.fullScrean ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
                                    {
                                        !(this.state.fullScrean)
                                        &&
                                        <Button
                                            style={{
                                                position: 'absolute',
                                                zIndex: 1,
                                                right: 2,
                                                top: 1
                                            }}
                                            shape="circle"
                                            icon="fullscreen"
                                            onClick={() => {
                                                this.showFull();
                                            }}
                                        />
                                    }
                                    {getMapEle(Infodata)}
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                );
            } else {
                if (Infodata.guardian) {
                    return Infodata.guardian.elder.map((e: any, i: number) => {
                        return (
                            <TabPane tab={e.name} key={`${e.name}`} closable={false}>
                                <Row>
                                    <Col span={6}>
                                        <div style={{ textAlign: 'center' }} >
                                            <img
                                                src={
                                                    (() => {
                                                        if (e.pic !== "") {
                                                            return e.pic;
                                                        }
                                                        return require("./img/older.jpg");
                                                    })()
                                                }
                                                alt=""
                                                style={{ width: '200px', height: '200px' }}
                                            />
                                        </div>
                                        {
                                            needRemark ?
                                                remarkElement(Infodata)
                                                :
                                                undefined
                                        }
                                        {/* <Input.TextArea onChange={this.onChangeRemark} />
                                        <Button onClick={this.saveRemark}>保存备注</Button> */}
                                    </Col>
                                    <Col span={6}>
                                        <Card title="长者信息">
                                            <p>姓名：<span>{e.name ? e.name : '空'}</span></p>
                                            <p>性别：<span>{e.sex ? e.sex : '空'}</span></p>
                                            <p>年龄：<span>{e.card_number ? new Date().getFullYear() - e.card_number.slice(6, 10) : '暂无相关信息'}</span></p>
                                            <p>家庭地址：<span>{e.address ? e.address : '空'}</span></p>
                                            <p>电话号码：<span>{e.tel ? e.tel : '空'}</span></p>
                                            <br />
                                            <p>证件类型：<span>{e.idCardType ? e.idCardType : '空'}</span></p>
                                            <p>证件号码：<span>{e.cardNumber ? e.cardNumber : '空'}</span></p>
                                        </Card>
                                        <Col>
                                            <p>紧急联系人信息：</p>
                                            <span>{Infodata.guardian.guardian_name}：{Infodata.guardian.guardian_telephone}</span>
                                        </Col>
                                    </Col>
                                    <Col span={10} >
                                        <div id={'olderMap'} className={this.state.fullScrean ? "olderMapFull" : 'olderMap'} style={{ textAlign: 'center', height: "100%", width: "100%" }} >
                                            {
                                                !(this.state.fullScrean)
                                                &&
                                                <Button
                                                    style={{
                                                        position: 'absolute',
                                                        zIndex: 1,
                                                        right: 2,
                                                        top: 1
                                                    }}
                                                    shape="circle"
                                                    icon="fullscreen"
                                                    onClick={() => {
                                                        this.showFull();
                                                    }}
                                                />
                                            }
                                            {getMapEle({ elder: e })}
                                        </div>
                                    </Col>
                                </Row>
                            </TabPane>
                        );
                    });
                }
            }
        };

        return (
            <div className='call-center' style={{ position: 'relative' }}>
                <MainContent>
                    <Button style={{ float: 'right' }} type="primary" onClick={() => { history.back(); }}>返回</Button>
                    <Tabs type="editable-card" hideAdd={true} onChange={this.onOlderTabChange}>
                        {elder_info_ele(Infodata, true)}
                        {
                            older_list.map((e, i) => {
                                const Infodata = {
                                    elder: {
                                        name: e.name,
                                        sex: e.personnel_info.sex,
                                        card_number: e.personnel_info.id_card,
                                        address: e.personnel_info["address"] ? e.personnel_info["address"] : undefined,
                                        id_card_type: e.id_card_type,
                                        cardNumber: e.personnel_info.id_card,
                                        location: undefined,
                                        tel: e.personnel_info.telephone
                                    }
                                };
                                return (
                                    <TabPane tab={e.name} key={`${e.personnel_info.telephone}_${e.id}`} closable={false}>
                                        {elder_info_ele(Infodata, false)}
                                    </TabPane>
                                );
                            })
                        }
                    </Tabs>
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：监护运营页面
 * 描述
 */
@addon(' OperationMapView', '监护运营页面', '提示')
@reactControl(OperationMapView, true)
export class OperationMapViewControl extends ReactViewControl {
}