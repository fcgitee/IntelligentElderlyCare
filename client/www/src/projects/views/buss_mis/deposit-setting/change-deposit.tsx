import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { Select } from 'antd';
// const { Option } = Select;
/**
 * 组件：押金结算设置新增状态
 */
export interface ChangeDepositViewState extends ReactViewState {
    status?:any;
    base_data?:any;
}

/**
 * 组件：押金结算设置新增
 * 描述
 */
export class ChangeDepositView extends ReactView<ChangeDepositViewControl, ChangeDepositViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: ChangeDepositViewControl) {
        super(props);
        this.state = {
            status: ["男", "女"],
            base_data: {},
        };
    }

    render() {
        let { base_data } = this.state;
        let elder_id = '';
        let key = this.props.match!.params.key;
        if (key) {
            elder_id = key.split('__')[1];
        }
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_elder_list',
            service_option: elder_id ? [{ id: elder_id }, 1, 1] : [{}, 1, 10],
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "押金结算设置",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者姓名",
                            decorator_id: 'elder_id',
                            field_decorator_option: {
                                rules: [{ message: "请输入名称" }],
                                initialValue: base_data.elder_name,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者姓名",
                                modal_search_items_props: modal_search_items_props
                            },
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "金额",
                            decorator_id: "amout",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入金额" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入备注" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.depositSetting);
                    }
                }
            ],
            submit_btn_propps: {
                    text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.health_care_service,
                operation_option: {
                    save: {
                        func_name: "update_deposit_setting"
                    },
                    query: {
                        func_name: "get_deposit_setting_list",
                        arguments:[{id: this.props.match!.params.key}, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.depositSetting); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
}
}
/**
 * 控件：押金结算设置新增
 * 描述
 */
@addon('ChangeDepositView', '押金结算设置新增', '描述')
@reactControl(ChangeDepositView, true)
export class ChangeDepositViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}