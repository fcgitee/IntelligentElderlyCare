import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from 'src/projects/remote';
import { Select } from 'antd';
import { request } from "src/business/util_tool";
const { Option } = Select;
/**
 * 组件：物料分类新增查看状态
 */
export interface ChangeThingArchivesViewState extends ReactViewState {
    units_list?: any;
    thing_sort_list?: any;
}

/**
 * 组件：物料分类新增查看
 * 描述
 */
export class ChangeThingArchivesView extends ReactView<ChangeThingArchivesViewControl, ChangeThingArchivesViewState> {
    constructor(props: ChangeThingArchivesViewControl) {
        super(props);
        this.state = {
            units_list: [],
            thing_sort_list: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.materiel_service.get_thing_sort_list!({}))
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        thing_sort_list: datas.result,
                    });
                }
            });
        request(this, AppServiceUtility.materiel_service.get_units_list!({}))
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        units_list: datas.result,
                    });
                }
            });
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "物料新增",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "物料编码",
                            decorator_id: "code",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入物料编码" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入物料编码",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入名称",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.select,
                            label: "分类",
                            decorator_id: "sort_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择分类" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择分类",
                                autocomplete: "off",
                                showSearch: true,
                                childrens: this.state.thing_sort_list!.map((item: any, idx: any) => <Option key={item.id}>{item.name}</Option>),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "单价/元",
                            decorator_id: "price",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入单价" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入单价",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "促销单价/元",
                            decorator_id: "promotion_price",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入促销单价" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入促销单价",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.select,
                            label: "单位",
                            decorator_id: "unit_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择单位" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择单位",
                                autocomplete: "off",
                                showSearch: true,
                                childrens: this.state.units_list!.map((item: any, idx: any) => <Option key={item.id}>{item.name}</Option>),
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "规格",
                            decorator_id: "specifications",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入规格" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入规格",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "型号",
                            decorator_id: "model",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入型号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入型号",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "厂家",
                            decorator_id: "manufactor",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入厂家" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入厂家",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "采购价",
                            decorator_id: "purchase_price",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入采购价" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入采购价",
                                autocomplete: "off",
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "图片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "picture",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传图片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传图片"
                            },
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.thingArchives);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.materiel_service,
                operation_option: {
                    save: {
                        func_name: "update_thing_archives"
                    },
                    query: {
                        func_name: "get_thing_archives_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.thingArchives); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：物料分类新增查看
 * 描述
 */
@addon('ChangeThingArchivesView', '物料分类新增查看', '描述')
@reactControl(ChangeThingArchivesView, true)
export class ChangeThingArchivesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}