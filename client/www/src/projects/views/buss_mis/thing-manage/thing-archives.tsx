
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { request } from "src/business/util_tool";
import { Select } from "antd";
const { Option } = Select;
/** 状态：申请审核查看视图 */
export interface ThingArchivesViewState extends ReactViewState {
    /** 申请状态 */
    thing_sort_list?: any[];
    /** 接口名 */
    request_ulr?: string;
}
/** 组件：申请审核查看视图 */
export class ThingArchivesView extends React.Component<ThingArchivesViewControl, ThingArchivesViewState> {
    private columns_data_source = [{
        title: '物料编码',
        dataIndex: 'code',
        key: 'code',
    },
    {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '分类',
        dataIndex: 'sort_name',
        key: 'sort_name',
    }, {
        title: '单价/元',
        dataIndex: 'price',
        key: 'price',
    },
    {
        title: '单位',
        dataIndex: 'unit_name',
        key: 'unit_name',
    },
    {
        title: '规格',
        dataIndex: 'specifications',
        key: 'specifications',
    },
    {
        title: '厂家',
        dataIndex: 'manufactor',
        key: 'manufactor',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    },
    ];
    constructor(props: ThingArchivesViewControl) {
        super(props);
        this.state = {
            thing_sort_list: [],
            request_ulr: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeThingArchives);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeThingArchives + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        request(this, AppServiceUtility.materiel_service.get_thing_sort_list!({}))
            .then((datas: any) => {
                if (datas && datas.result) {
                    this.setState({
                        thing_sort_list: datas.result,
                    });
                }
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let thing_archives = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "物料名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.select,
                    label: "物料分类",
                    decorator_id: "sort_id",
                    option: {
                        childrens: this.state.thing_sort_list!.map((item: any, idx: any) => <Option key={item.id}>{item.name}</Option>),
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.materiel_service,
            service_option: {
                select: {
                    service_func: 'get_thing_archives_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_thing_archives'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let thing_archives_list = Object.assign(thing_archives, table_param);
        return (
            <SignFrameLayout {...thing_archives_list} />
        );
    }
}

/**
 * 控件：申请审核查看视图控制器
 * @description 申请审核查看视图
 * @author
 */
@addon('ThingArchivesView', '申请审核查看视图', '申请审核查看视图')
@reactControl(ThingArchivesView, true)
export class ThingArchivesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}