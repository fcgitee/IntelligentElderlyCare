
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { AppServiceUtility } from 'src/projects/app/appService';
import { Input, Button, Row, Col, Icon, message } from 'antd';
import { isPermission } from "src/projects/app/permission";
import { request } from "src/business/util_tool";
/** 状态：计量单位视图 */
export interface UnitsViewState extends ReactViewState {
    show?: any;
    code?: any;
    name?: any;
    remark?: any;
    is_send?: boolean;
}
/** 组件：计量单位视图 */
export class UnitsView extends React.Component<UnitsViewControl, UnitsViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '序号',
        dataIndex: 'code',
        key: 'code',
    },
    {
        title: '单位名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    },
    ];
    constructor(props: UnitsViewControl) {
        super(props);
        this.state = {
            show: true,
            code: '',
            name: '',
            remark: '',
            is_send: false,
        };
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeUnits);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeUnits + '/' + contents.id);
        }
    }

    iconClick = () => {
        let show = this.state.show;
        this.setState({
            show: !show
        });
    }

    save = () => {
        let { code, name, remark, is_send } = this.state;
        if (!code) {
            message.info('请输入序号！');
            return;
        }
        if (!name) {
            message.info('请输入名称！');
            return;
        }
        if (is_send) {
            message.info('请勿操作过快！');
            return;
        }
        let param: any = {
            code,
            name,
        };
        if (remark) {
            param['remark'] = remark;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.materiel_service.update_units!(param)!)
                    .then((datas: any) => {
                        this.setState({
                            is_send: false,
                        });
                        if (datas === 'Success') {
                            message.info('保存成功！');
                            this.setState({
                                code: '',
                                name: '',
                                remark: '',
                            });
                            this.formCreator.reflash();
                        } else {
                            message.info(datas);
                        }
                    });
            }
        );
    }
    changeCode = (e: any) => {
        this.setState({
            code: e.target.value,
        });
    }
    changeName = (e: any) => {
        this.setState({
            name: e.target.value,
        });
    }

    changeRemark = (e: any) => {
        this.setState({
            remark: e.target.value,
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let units = {
            type_show: false,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.materiel_service,
            service_option: {
                select: {
                    service_func: 'get_units_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_units'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let units_list = Object.assign(units, table_param);
        return (
            <div>
                <div style={{ position: 'relative', backgroundColor: '#ffffff', height: this.state.show === true ? '130px' : '60px', margin: '36px 36px 0px 36px', paddingTop: '20px' }}>
                    {this.state.show === true ?
                        <div>
                            <Row justify="space-around" style={{ textAlign: 'center', marginBottom: '30px' }}>
                                <Col span={6}>序号：<Input onChange={this.changeCode} style={{ width: '150px' }} value={this.state.code} /></Col>
                                <Col span={8}>单位名称：<Input onChange={this.changeName} style={{ width: '150px' }} value={this.state.name} /></Col>
                                <Col span={10}>备注：<Input onChange={this.changeRemark} style={{ width: '150px' }} value={this.state.remark} /></Col>
                            </Row>
                            <div style={{ paddingLeft: '85%' }}>
                                <Button onClick={this.save} type="primary" style={{ marginRight: '20px' }}>保存</Button>
                                <Button>取消</Button>
                            </div>
                        </div>
                        :
                        ''
                    }
                    <div onClick={this.iconClick} style={{ backgroundColor: '#ffffff', width: '50px', height: '50px', position: 'absolute', top: '10px', right: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='down' />
                    </div>
                </div>
                <SignFrameLayout {...units_list} />
            </div>
        );
    }
}

/**
 * 控件：计量单位视图控制器
 * @description 计量单位视图
 * @author
 */
@addon(' UnitsView', '计量单位视图', '计量单位视图')
@reactControl(UnitsView, true)
export class UnitsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}