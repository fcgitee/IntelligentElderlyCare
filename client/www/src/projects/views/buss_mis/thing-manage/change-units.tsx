import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
// import { remote } from "src/projects/remote";
/**
 * 组件：计量单位详情
 */
export interface ChangeUnitsViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
}

/**
 * 组件：计量单位详情
 */
export default class ChangeUnitsView extends ReactView<ChangeUnitsViewControl, ChangeUnitsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        var edit_props = {
            form_items_props: [
                {
                    title: '计量单位',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.input_number,
                            label: "序号",
                            decorator_id: "code",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入序号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入序号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "单位名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入单位名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入单位名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.goBack();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.materiel_service,
                operation_option: {
                    save: {
                        func_name: "update_units"
                    },
                    query: {
                        func_name: "get_units_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：计量单位详情
 * @description 计量单位详情
 * @author
 */
@addon('ChangeUnitsView', '计量单位详情', '计量单位详情')
@reactControl(ChangeUnitsView, true)
export class ChangeUnitsViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}