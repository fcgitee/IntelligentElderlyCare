import { Button, Table, TreeSelect } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from 'src/projects/app/appService';
/**
 * 组件：基本情况统计报表
 */
export interface BaseSituationState extends ReactViewState {
    administrative_division_list: any[];
    area_id: string;
    table_total_size: number;
    base_situation: any[];
    page: number;
    page_size: number;
}

/**
 * 组件：基本情况统计报表
 * 描述
 */
export class BaseSituation extends ReactView<BaseSituationControl, BaseSituationState> {
    columns: any = [
        {
            title: '镇街',
            dataIndex: 'admin_info.name',
            key: 'admin_info.name',
        },
        {
            title: '幸福院名称',
            dataIndex: 'org_info.name',
            key: 'org_info.name',
        },
        {
            title: '村（局）幸福院负责人',
            dataIndex: 'org_info.organization_info.legal_person',
            key: 'org_info.organization_info.legal_person',
        },
        {
            title: '设立时间',
            dataIndex: 'reg_date',
            key: 'reg_date',
        },
        {
            title: '星级',
            dataIndex: 'star_level',
            key: 'star_level',
            render: (text: string, record: any) => {
                if (text) {
                    switch (text) {
                        case "1":
                            return '一星';
                        case "2":
                            return '二星';
                        case "3":
                            return '三星';
                        case "4":
                            return '四星';
                        case "5":
                            return '五星';
                        default:
                            return undefined;
                    }
                }
                return undefined;
            }
        },
        {
            title: '服务机构',
            dataIndex: 'ser_org',
            key: 'ser_org',
        },
        {
            title: '占地面积（/㎡）',
            dataIndex: 'cover_area',
            key: 'cover_area',
        },
        {
            title: '建筑面积',
            dataIndex: 'build_area',
            key: 'build_area',
        },
        {
            title: '覆盖社区',
            dataIndex: 'cover_zoom',
            key: 'cover_zoom',
        },
        {
            title: '覆盖长者数',
            dataIndex: 'elder_num',
            key: 'elder_num',
        },
        {
            title: '建设方式',
            dataIndex: 'build_type',
            key: 'build_type',
        },
        {
            title: '办公电话',
            dataIndex: 'org_info.organization_info.telephone',
            key: 'org_info.organization_info.telephone',
        },
        {
            title: '地址',
            dataIndex: 'org_info.organization_info.address',
            key: 'org_info.organization_info.address',
        },
        {
            title: '功能室',
            dataIndex: 'fn_room',
            key: 'fn_room',
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    <a onClick={() => this.onIconClick(record.org_info.id)}>编辑</a>
                );
            }
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            administrative_division_list: [],
            area_id: '',
            table_total_size: 0,
            base_situation: [],
            page: 0,
            page_size: 0
        };
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (id: any) => {
        this.props.history!.push(ROUTE_PATH.changeOrganizationXYF + '/' + id);
    }
    componentWillMount = () => {
        // AppServiceUtility.service_base_situation!.get_base_situation_list!({}, 1, 10)!
        //     .then((data) => {
        //         // console.log(data);
        //         this.setState({
        //             base_situation_list: data.result!
        //         });
        //     });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState(
                    {
                        administrative_division_list: data!.result!,
                        area_id: data && data.result && data.result[0] && data.result[0]['id'] ? data.result[0]['id'] : '',
                    },
                    () => {
                        this.getBaseSituationData(this.state.page, this.state.page_size);
                    }
                );
            })
            .catch(err => {
                console.info(err);
            });

    }

    onChangeTree = (value: any) => {
        this.setState({
            area_id: value
        });
    }

    onSearch = () => {
        this.getBaseSituationData(this.state.page, this.state.page_size);
    }

    getBaseSituationData = (page: number, pageSize: number) => {
        AppServiceUtility.service_base_situation!.get_base_situation_list!({ id: this.state.area_id }, page, pageSize)!
            .then(data => {
                this.setState({
                    base_situation: data.result!,
                    table_total_size: data.total!
                });
            }).catch(e => {
                console.error(e);
            });

    }

    onPageChange = (page: number, pageSize: number) => {
        this.getBaseSituationData(page, pageSize);
    }

    onon = (value: any, label: any, extra: any) => {
        // console.log(value, label, extra);

    }
    render() {
        return (
            <MainContent>
                <MainCard title='基本情况统计' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                        <Button type="primary" onClick={this.onSearch} style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                        <div style={{ float: 'right', marginRight: '140px' }}>
                            <TreeSelect
                                showSearch={true}
                                treeNodeFilterProp={'title'}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                allowClear={true}
                                onChange={this.onChangeTree}
                                treeDefaultExpandAll={true}
                                treeData={this.state.administrative_division_list}
                                placeholder={"请选择行政区划"}
                                style={{
                                    width: '12em'
                                }}
                            />
                        </div>
                    </div>
                    <Table
                        columns={this.columns}
                        dataSource={this.state.base_situation}
                        pagination={{ pageSize: 10, total: this.state.table_total_size, onChange: this.onPageChange }}

                        bordered={true}
                        size="middle"
                    // scroll={{ x: 'calc(600px+50%)' }}
                    />
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：基本情况统计报表
 * 描述
 */
@addon(' BaseSituation', '基本情况统计报表', '提示')
@reactControl(BaseSituation, true)
export class BaseSituationControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 判断类型 */
    public select_type?: any;
}