import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { BaiduMap } from './map';
import { Card, Badge } from 'antd';
import './order-all.less';
import { AppServiceUtility } from 'src/projects/app/appService';
/**
 * 组件：呼叫中心页面
 */
export interface OrderAllJViewState extends ReactViewState {
    citylist?: any;
    lat?: any;
    lng?: any;
    unserved_num?: any;
    going_num?: any;
    assign_num?: any;
    completed_num?: any;
}

/**
 * 组件：呼叫中心页面
 * 描述
 */
export class OrderAllJView extends ReactView<OrderAllJViewControl, OrderAllJViewState> {
    constructor(props: OrderAllJViewControl) {
        super(props);
        this.state = {
            citylist: {},
            lat: '',
            lng: '',
            unserved_num: '',
            going_num: '',
            assign_num: '',
            completed_num: ''
        };
    }

    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log("登录？？？？？？", data);
                AppServiceUtility.person_org_manage_service.getOrderByBussiness!({ id: data[0].organization_id }, 1, 1)!
                    .then((data: any) => {
                        // console.log("登录？？？？？？", data);
                        this.setState({
                            unserved_num: data.result[0]['unserved_num'],
                            going_num: data.result[0]['going_num'],
                            completed_num: data.result[0]['completed_num'],
                            assign_num: data.result[0]['assign_num'],
                            lat: data.result[0]['organization_info']['lat'],
                            lng: data.result[0]['organization_info']['lon'],
                            citylist: { "text": data.result[0]['name'], "location": data.result[0]['organization_info']['lat'] + ',' + data.result[0]['organization_info']['lon'], "count": 4 }
                        });
                    });
            });
    }

    render() {
        const { unserved_num, going_num, completed_num } = this.state;
        return (
            <div className='order-all'>
                <BaiduMap lng={this.state.lng} lat={this.state.lat} citylist={this.state.citylist} />
                <Card className='card-right'>
                    <div style={{ marginBottom: '10px', color: 'blue' }}><Badge color='blue' />服务点（1）</div>
                    <div style={{ marginBottom: '8px' }}><Badge color='red' />{'未服务（' + unserved_num + '）'}</div>
                    {/* <div style={{ marginBottom: '8px' }}><Badge color='cyan' />{'已分派（' + assign_num + '）'}</div> */}
                    <div style={{ marginBottom: '8px' }}><Badge color='orange' />{'服务中（' + going_num + '）'}</div>
                    <div style={{ marginBottom: '8px' }}><Badge color='green' />{'已完成（' + completed_num + '）'}</div>
                </Card>
            </div>
        );
    }
}

/**
 * 控件：呼叫中心页面
 * 描述
 */
@addon(' OrderAllJView', '呼叫中心页面', '提示')
@reactControl(OrderAllJView, true)
export class OrderAllJViewControl extends ReactViewControl {
}