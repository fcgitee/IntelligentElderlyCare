import React from 'react';
import { Map, MarkerList, Marker } from 'react-bmap';

export function BaiduMap({lng = 113.14958, lat = 23.035191,citylist}) {
    const fontSize = 16;
    return (
        <div>
            <Map center={{
                lng: lng,
                lat: lat
            }}
                zoom='17'
                amapkey={"jEmXEmo4L2GkmC1Ops1gvFGm75ALGDji"}
                enableDragging={true}
                // enableContinuousZoom={true}
                enableScrollWheelZoom={true}
                style={{ height: '770px' }}>
                {/* <MarkerList
                    data={citylist}
                    onClick={(item) => {
                        console.log(item);
                    }}
                    splitList={{
                        4: '#d53938',
                        3: '#fe6261',
                        2: '#ffb02d',
                        1: '#80db69'
                    }}
                    coordType="bd09mc"
                    isShowNumber={true}
                    animation={true}
                    multiple={true}
                    autoViewport={true}
                /> */}
                <Marker position={{ lng: lng, lat: lat }} />
            </Map>
        </div>
    );
}