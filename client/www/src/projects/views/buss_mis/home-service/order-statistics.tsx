import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { Button, Table, TreeSelect, DatePicker, Modal, Form, Radio } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
import moment from 'moment';
import { exprot_excel } from "src/business/util_tool";

const { MonthPicker } = DatePicker;
/**
 * 组件：订单统计报表页面
 */
export interface OrderStatisticsViewState extends ReactViewState {
    /** 合计数据 */
    dataTotalSource: any[];
    /** 日期条件 */
    query_month?: any;
    /** 控制弹框显示 */
    show?: any;
    /** 导出的条件 */
    select_type?: any;
    /** 服务商列表 */
    org_list: any[];
    /** 选择的服务商Id */
    org_id: string;
    condition?: any;
}

/**
 * 组件：订单统计报表页面
 * 描述
 */
export class OrderStatisticsView extends ReactView<OrderStatisticsViewControl, OrderStatisticsViewState> {
    columns: any = [
        {
            title: '结算月份',
            dataIndex: 'year_month',
            key: 'year_month',
            width: 100,
        },
        {
            title: '机构名称',
            dataIndex: 'org_name',
            key: 'org_name',
            width: 200,
        },
        {
            title: '服务长者数',
            dataIndex: 'buy_people',
            key: 'buy_people',
            width: 100,
        },
        {
            title: '补贴订单数',
            dataIndex: 'bt_order_num',
            key: 'bt_order_num',
            width: 100,
        },
        {
            title: '补贴金额',
            dataIndex: 'bt_amout',
            key: 'bt_amout',
            width: 100,
        },
        {
            title: '自费订单数',
            dataIndex: 'zf_order_num',
            key: 'zf_order_num',
            width: 100,
        },
        {
            title: '自费金额',
            dataIndex: 'zf_amout',
            key: 'zf_amout',
            width: 100,
        },
        {
            title: '慈善订单数',
            dataIndex: 'cs_order_num',
            key: 'cs_order_num',
            width: 100,
        },
        {
            title: '慈善基金金额',
            dataIndex: 'cs_amout',
            key: 'cs_amout',
            width: 100,
        },
        {
            title: '合计订单',
            dataIndex: 'total_order_num',
            key: 'total_order_num',
            width: 100,
        },
        {
            title: '合计金额',
            dataIndex: 'total_amout',
            key: 'total_amout',
            width: 100,
        }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            dataTotalSource: [],
            select_type: 1,
            show: false,
            query_month: moment(moment(), 'YYYY/MM'),
            org_list: [],
            org_id: '',
            condition: {}
        };
    }
    /**
     * 查询方法
     */
    private select_func = (condition: {}) => {
        AppServiceUtility.service_record_service!.get_order_statistics!(condition)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    dataTotalSource: data.result
                });
            });
    }
    componentDidMount = () => {
        this.select_func({ 'month': this.state.query_month });
        // 查询服务商列表
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ 'organization_type': '服务商' })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result!
                });
            })
            .catch(err => {
                console.info(err);
            });
    }

    /** 树形选择框改变事件 */
    onChangeTree = (value: any) => {
        this.setState({
            org_id: value
        });
        let condition = {};
        if (this.state.query_month !== '') {
            condition = { 'month': this.state.query_month, 'org_id': value };
        } else {
            condition = { 'org_id': value };
        }
        this.setState({
            condition
        });
        this.select_func(condition);
    }

    /** 日期范围回调 */
    time_change = (date: any, dateString: string) => {
        this.setState({
            query_month: dateString
        });
        let condition = {};
        condition = { 'month': dateString };
        if (this.state.org_id !== '') {
            condition = { ...condition, "org_id": this.state.org_id };
        }
        this.setState({
            condition
        });
        this.select_func(condition);
    }

    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.service_record_service.get_order_statistics!(condition)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "结算月份": item.year_month,
                                "机构名称": item.org_name[0],
                                "服务长者数": item.buy_people,
                                "补贴订单数": item.bt_order_num,
                                "补贴金额": item.bt_amout,
                                "自费订单数": item.zf_order_num,
                                "自费金额": item.zf_amout,
                                "慈善订单数": item.cs_order_num,
                                "慈善基金金额": item.cs_amout,
                                "合计订单数": item.total_order_num,
                                "合计金额": item.total_amout,
                            });
                        });
                        exprot_excel([{ name: '订单统计（汇总）', value: new_data }], '订单统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <MainContent>
                <MainCard title='订单统计报表' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button onClick={this.export} type="primary" style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                        <div style={{ float: 'right', marginRight: '3px' }}>
                            <MonthPicker placeholder='选择日期' onChange={this.time_change} defaultValue={moment(moment(), 'YYYY/MM')} format={'YYYY/MM'} />
                        </div>
                        <div style={{ float: 'right', marginRight: '240px' }}>
                            <TreeSelect
                                showSearch={true}
                                treeNodeFilterProp={'title'}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                allowClear={true}
                                onChange={this.onChangeTree}
                                treeDefaultExpandAll={true}
                                treeData={this.state.org_list}
                                placeholder={"请选择服务商"}
                                style={{
                                    width: '12em'
                                }}
                            />
                        </div>
                    </div>
                    <Table columns={this.columns} dataSource={this.state.dataTotalSource} pagination={{ pageSize: 10 }} bordered={true} />
                </MainCard>
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：订单统计报表页面
 * 描述
 */
@addon(' OrderStatisticsView', '订单统计报表页面', '提示')
@reactControl(Form.create<any>()(OrderStatisticsView), true)
export class OrderStatisticsViewControl extends ReactViewControl {
}