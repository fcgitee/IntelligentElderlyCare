
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Form } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
/** 状态：年度计划与总结 */
export interface YearplanAndSummaryViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    condition?: any;
    /* 组织机构 */
    org_list?: any;
}
/** 组件：年度计划与总结 */
export class YearplanAndSummaryView extends React.Component<YearplanAndSummaryViewControl, YearplanAndSummaryViewState> {
    private columns_data_source = [{
        title: '机构名字',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }
    ];
    constructor(props: YearplanAndSummaryViewControl) {
        super(props);
        this.state = {
            request_url: '',
            condition: {},
            org_list: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeYearplanAndSummary + '/' + contents.id);
        }
    }

    add = () => {
        this.props.history!.push(ROUTE_PATH.changeYearplanAndSummary);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let param: any = {};
        let yearplan_summary = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: '组织机构',
                //     decorator_id: "org_name",
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.input,
                    label: '备注',
                    decorator_id: "remark",
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'delete_yearplan_and_summary'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
        };
        let yearplan_summary_list = Object.assign(yearplan_summary, table_param);
        return (
            <div>
                <SignFrameLayout {...yearplan_summary_list} />
            </div>
        );
    }
}

/**
 * 控件：年度计划与总结控制器
 * @description 年度计划与总结
 * @author
 */
@addon('YearplanAndSummaryView', '年度计划与总结', '年度计划与总结')
@reactControl(Form.create<any>()(YearplanAndSummaryView), true)
export class YearplanAndSummaryViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}