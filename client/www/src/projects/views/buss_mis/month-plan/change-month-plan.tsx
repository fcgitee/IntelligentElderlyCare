import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info, beforeUpload2 } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";
/**
 * 组件：编辑月度计划
 */
export interface ChangeMonthPlanViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
}

/**
 * 组件：编辑月度计划
 */
export default class ChangeMonthPlanView extends ReactView<ChangeMonthPlanViewControl, ChangeMonthPlanViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
        };
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        var edit_props = {
            form_items_props: [
                {
                    title: '月度计划',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "计划名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入计划名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入计划名称",
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "月度计划（大小小于2M，格式支持docx/docx",
                            decorator_id: "plan",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传月度计划" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请上传月度计划",
                                fileType: 'doc',
                                action: remote.upload_url,
                                beforeUpload: (e: any) => beforeUpload2(e, ['doc', 'docx'])
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入任务备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                autocomplete: 'off',
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.goBack();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_month_plan"
                    },
                    query: {
                        func_name: "get_month_plan_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑月度计划
 * @description 编辑月度计划
 * @author
 */
@addon('ChangeMonthPlanView', '编辑月度计划', '编辑月度计划')
@reactControl(ChangeMonthPlanView, true)
export class ChangeMonthPlanViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}