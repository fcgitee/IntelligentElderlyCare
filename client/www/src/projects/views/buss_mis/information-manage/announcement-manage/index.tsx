
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";

/**
 * 组件：公告编辑视图状态
 */
export interface AnnouncementIssueViewState extends ReactViewState {

    /** 公告发布人列表 */
    author_list?: [];

}

/**
 * 组件：公告编辑视图
 * 描述
 */
export class AnnouncementIssueView extends ReactView<AnnouncementIssueViewControl, AnnouncementIssueViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let edit_props = {
            form_items_props: [
                {
                    title: "公告编辑",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "公告标题",
                            decorator_id: "title",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入公告标题" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入公告标题",
                            }
                        },
                        // {
                        //     type: InputType.select,
                        //     label: "公告发布者",
                        //     decorator_id: "promulgator_id",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请输入公告发布者" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入公告发布者",     
                        //         childrens: author_list
                        //     }
                        // }, 
                        {
                            type: InputType.text_area,
                            label: "公告描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入公告描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入公告描述",
                            }
                        },
                        {
                            type: InputType.nt_rich_text,
                            label: "公告内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入公告内容" }]
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入公告内容"
                            }
                        },
                        // {
                        //     type: InputType.date,
                        //     label: "公告发布时间",
                        //     decorator_id: "issue_date",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请输入公告发布时间" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入公告发布时间",

                        //     }
                        // },
                        {
                            type: InputType.radioGroup,
                            label: "显示状态",
                            decorator_id: "showing_status",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择显示状态" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '首页',
                                    value: '首页',
                                }, {
                                    label: '置顶',
                                    value: '置顶',
                                }]
                            }
                        },
                        // {
                        //     type: InputType.antd_input,
                        //     label: "备注",
                        //     decorator_id: "remark",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "备注"
                        //     }
                        // }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.announcementList);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.article_service,
                operation_option: {
                    query: {
                        func_name: "get_announcement_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_announcement"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.announcementList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：公告编辑视图
 * 描述
 */
@addon('AnnouncementIssueView', '公告编辑视图', '描述')
@reactControl(AnnouncementIssueView, true)
export class AnnouncementIssueViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}