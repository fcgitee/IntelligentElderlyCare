import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
// import { request } from "src/business/util_tool";
// import { Select } from 'antd';

// let { Option } = Select;
/**
 * 组件：公告管理列表状态
 */
export interface AnnouncementListViewState extends ReactViewState {
    // promulgator_list?:[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：公告管理列表
 * 描述
 */
export class AnnouncementListView extends ReactView<AnnouncementListViewControl, AnnouncementListViewState> {
    private columns_data_source = [{
        title: '公告标题',
        dataIndex: 'title',
        key: 'title',
    }, {
        title: '公告描述',
        dataIndex: 'description',
        key: 'description',
    }, {
        title: '公告发布者',
        dataIndex: 'promulgator',
        key: 'promulgator',
    }, {
        title: '公告发布时间',
        dataIndex: 'issue_date',
        key: 'issue_date',
    }, {
        title: '公告显示状态',
        dataIndex: 'showing_status',
        key: 'showing_status',
    }, {
        title: '公告审核状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '审核意见',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            // promulgator_list: [],
            request_url: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.announcementIssue);
    }
    /** 删除按钮 */
    // on_click_del = (key: any) => {
    //     request(this, this.AnouncementListService!()!.del_business_area!([key.id]))
    //         .then((data: any) => {
    //             if (data === 'Success') {
    //                 message.info('删除成功');
    //             } else {
    //                 message.info(data);
    //             }
    //         })
    //         .catch((error: any) => {
    //             message.info('删除失败');
    //         });
    // }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.announcementIssue + '/' + contents.id);
        }
    }

    componentDidMount() {

    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        // let promulgator_list: any[] = [];
        // this.state.promulgator_list!.map((item, idx) => {
        //     promulgator_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });

        let announcement = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "公告标题",
                    decorator_id: "title"
                }, {
                    type: InputType.input,
                    label: "公告发布者",
                    decorator_id: "promulgator",
                    option: {
                        placeholder: "请输入公告发布者",
                        // childrens: promulgator_list
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "公告发布时间",
                    decorator_id: "date_range"
                }, {
                    type: InputType.radioGroup,
                    label: "公告显示状态",
                    decorator_id: "showing_status",
                    option: {
                        options: [
                            {
                                label: '首页',
                                value: '首页'
                            }, {
                                label: '置顶',
                                value: '置顶'
                            }
                        ]
                    }
                },
            ],
            btn_props: [{
                label: '新增公告',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.article_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_announcement'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let announcement_list = Object.assign(announcement, table_param);
        return (
            <SignFrameLayout {...announcement_list} />
        );
    }
}

/**
 * 控件：公告管理列表
 * 描述
 */
@addon('AnnouncementListView', '公告管理列表', '描述')
@reactControl(AnnouncementListView, true)
export class AnnouncementListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}