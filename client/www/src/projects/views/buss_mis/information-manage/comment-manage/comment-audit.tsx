import { message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';

let { Option } = Select;
/**
 * 组件：评论审核状态
 */
export interface ChangeCommentAuditState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /* 评论类型列表*/
    type_list?:[];
}

/**
 * 组件：评论审核
 * 描述
 */
export class ChangeCommentAudit extends ReactView<ChangeCommentAuditControl, ChangeCommentAuditState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: '',
            type_list:[],
        };
    }
    refuse = () => {
        let submitObj = document.getElementById('remark') as HTMLInputElement;
        // console.log('submitObj', submitObj);
        request(this, AppServiceUtility.comment_manage_service.comment_audit_fail!({ id: this.state.id, remark: submitObj ? submitObj!.value : '无' }))
            .then((data: any) => {
                if (data === 'Success') {
                    this.props.history!.push(ROUTE_PATH.commentAudit);
                } else {
                    message.info('操作失败');
                }
            });

    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
        request(this, AppServiceUtility.comment_type_service.get_comment_type_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    type_list: datas.result,
                });
            });

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let type_list: any[] = [];
        this.state.type_list!.map((item, idx) => {
            type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "评论审核",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "评论对象",
                            decorator_id: "comment_object_id",
                            field_decorator_option: {
                                rules: [{  message: "请选择评论对象" }],
                                initialValue: base_data ? base_data.comment_object : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择评论对象",
                                childrens: [],
                                disabled: true,
                            }
                        }, {
                            type: InputType.select,
                            label: "父级评论",
                            decorator_id: "father_comment_id",
                            field_decorator_option: {
                                rules: [{ message: "请选择父级评论" }],
                                initialValue: base_data ? base_data.father_comment : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择父级评论",
                                childrens: [],
                                disabled: true,
                            }
                        },{
                            type: InputType.select,
                            label: "评论类型",
                            decorator_id: "type_id",
                            field_decorator_option: {
                                rules: [{ message: "选择评论类型" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择评论类型",
                                childrens: type_list,
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "评论等级",
                            decorator_id: "level",
                            field_decorator_option: {
                                rules: [{  message: "请输入评论等级" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入评论等级",
                                disabled: true,
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "评论内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{  message: "请输入评论内容" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入评论内容",
                                // childrens: [],
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "评论时间",
                            decorator_id: "comment_date",
                            field_decorator_option: {
                                rules: [{  message: "请输入评论时间" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入评论时间",
                                disabled: true,
                            }
                        }, 
                        {
                            type: InputType.antd_input,
                            label: "审核意见",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入审核意见" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入审核意见"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "审核失败",
                    cb: this.refuse
                },
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.commentAudit);
                    }
                }
            ],
            submit_btn_propps: {
                text: "审核通过"
            },
            service_option: {
                service_object: AppServiceUtility.comment_manage_service,
                operation_option: {
                    save: {
                        func_name: "comment_audit_success"
                    },
                    query: {
                        func_name: "get_comment_audit_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.commentAudit); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：评论审核控制器
 * 描述
 */
@addon('ChangeCommentAudit', '评论审核', '描述')
@reactControl(ChangeCommentAudit, true)
export class ChangeCommentAuditControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}