import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, IPermissionService, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "../../../../app/util-tool";
// import { request } from "src/business/util_tool";
// import { Select } from 'antd';

// let { Option } = Select;
/**
 * 组件：评论状态
 */
export interface CommentViewState extends ReactViewState {

    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /* 服务项目列表*/
    item_list?: [];
    /* 评论类型列表*/
    type_list?: [];
    /* 公告列表*/
    gg_list?: [];
    /* 新闻列表*/
    news_list?: [];
    /* 活动列表*/
    activity_list?: [];
}

/**
 * 组件：评论视图
 */
export class CommentView extends ReactView<CommentViewControl, CommentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: '',
            item_list: [],
            type_list: [],
            gg_list: [],
            news_list: [],
            activity_list: [],
        };
    }
    /** 权限服务 */
    permissionService?() {
        return getObject(this.props.permissionService_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    componentDidMount() {

        // request(this, AppServiceUtility.service_operation_service.get_service_item_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             item_list: datas.result,
        //         });
        //     });
        // request(this, AppServiceUtility.article_service.get_announcement_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             gg_list: datas.result,
        //         });
        //     });
        // request(this, AppServiceUtility.article_service.get_news_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             news_list: datas.result,
        //         });
        //     });
        // request(this, AppServiceUtility.activity_service.get_activity_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             news_list: datas.result,
        //         });
        //     });
        // request(this, AppServiceUtility.comment_type_service.get_comment_type_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             type_list: datas.result,
        //         });
        //     });
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.commentList);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "评论",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "资讯标题",
                            decorator_id: "article_title",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入资讯标题" }],
                                initialValue: base_data ? base_data.article_title : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "发布方",
                            decorator_id: "article_author",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入发布方" }],
                                initialValue: base_data ? base_data.article_author : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "评论内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入评论内容" }],
                                initialValue: base_data ? base_data.content : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入评论内容",
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "评论人",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入评论人" }],
                                initialValue: base_data ? base_data.comment_user_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "评论时间",
                            decorator_id: "comment_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入评论时间" }],
                                initialValue: base_data ? base_data.comment_date : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入评论时间",
                                disabled: true,
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.commentList);
                    }
                }
            ],
            // submit_btn_propps: {
            //     text: "保存",
            //     // cb: this.handleSubmit
            // },
            service_option: {
                service_object: AppServiceUtility.comment_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_comment"
                    },
                    query: {
                        func_name: "get_comment_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.commentList); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：评论编辑控件
 * @description 评论编辑控件
 * @author
 */
@addon('CommentView', '评论编辑控件', '评论编辑控件')
@reactControl(CommentView, true)
export class CommentViewControl extends ReactViewControl {
    /** 权限服务接口 */
    public permissionService_Fac?: Ref<IPermissionService>;
    /** 视图权限 */
    public permission?: Permission;

}