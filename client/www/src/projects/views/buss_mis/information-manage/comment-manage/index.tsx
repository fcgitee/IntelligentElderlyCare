import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
// import { request } from "src/business/util_tool";
import { Select, Row, Modal, Input, Button, message } from 'antd';
import { request } from "src/business/util_tool";

let { Option } = Select;
/**
 * 组件：评论管理列表状态
 */
export interface CommentListViewState extends ReactViewState {
    // 评论类型列表
    type_list?: [];
    /** 接口名 */
    request_url?: string;
    // 模态框
    modal_show?: boolean;
    // 选中的id
    selected_id?: any;
    // 原因
    reason?: string;
    // 发送状态
    is_send?: boolean;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：评论管理列表
 * 描述
 */
export class CommentListView extends ReactView<CommentListViewControl, CommentListViewState> {
    private columns_data_source = [{
        title: '资讯标题',
        dataIndex: 'article_title',
        key: 'article_title',
    }, {
        title: '发布方',
        dataIndex: 'article_author',
        key: 'article_author',
        render: (text: any, record: any) => {
            return text || record.article_org_name || '';
        }
    }, {
        title: '评论内容',
        dataIndex: 'content',
        key: 'content',
    }, {
        title: '评论时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '评论用户',
        dataIndex: 'comment_user_name',
        key: 'comment_user_name',
    }, {
        title: '评论时间',
        dataIndex: 'comment_date',
        key: 'comment_date',
    }, {
        title: '审核状态',
        dataIndex: 'audit_status',
        key: 'audit_status',
    }, {
        title: '审核内容',
        dataIndex: 'reason',
        key: 'reason',
    }, {
        title: '审核时间',
        dataIndex: 'audit_date',
        key: 'audit_date',
    },
    {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            type_list: [],
            request_url: '',
            modal_show: false,
            is_send: false,
            org_list: [],
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.comment);
    }

    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            if (this.props.list_type && this.props.list_type === 'sh') {
                this.setState({
                    selected_id: contents,
                    modal_show: true,
                    reason: contents.reason
                });
            } else {
                this.props.history!.push(ROUTE_PATH.comment + '/' + contents.id);
            }
        }
    }
    componentDidMount() {
        // request(this, AppServiceUtility.comment_type_service.get_comment_type_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         this.setState({
        //             type_list: datas.result,
        //         });
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    changeValue(e: any) {
        this.setState({
            reason: e.target.value,
        });
    }
    hideModal() {
        this.setState({
            modal_show: false,
        });
    }
    auditThis(type: string) {
        const { is_send, reason, selected_id } = this.state;
        if (type === '不通过' && !reason) {
            message.info('不通过请输入原因');
            return;
        }
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        if (!selected_id.id) {
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.comment_manage_service.update_comment!({ audit_status: type, id: selected_id.id, reason: reason }))
                    .then((datas: any) => {
                        if (datas && datas === 'Success') {
                            message.info('操作成功！', 1, () => {
                                window.location.reload();
                            });
                        } else {
                            message.info('不通过请输入原因');
                            return;
                        }
                    }).catch((error: Error) => {
                        this.setState({
                            is_send: false,
                        });
                        console.error(error.message);
                    });
            }
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let type_list: any[] = [];
        // this.state.type_list!.map((item, idx) => {
        //     type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });

        let comment_data = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "资讯标题",
                    decorator_id: "article_title",
                    option: {
                        placeholder: "请输入资讯标题",
                    }
                }, {
                    type: InputType.input,
                    label: "发布方",
                    decorator_id: "article_author",
                    option: {
                        placeholder: "请输入发布方",
                    }
                },
                // {
                //     type: InputType.select,
                //     label: "评论等级",
                //     decorator_id: "level",
                //     option: {
                //         placeholder: "请输入评论等级",
                //         childrens: [
                //             <Option key={'5'}>{'5'}</Option>,
                //             <Option key={'4'}>{'4'}</Option>,
                //             <Option key={'3'}>{'3'}</Option>,
                //             <Option key={'2'}>{'2'}</Option>,
                //             <Option key={'1'}>{'1'}</Option>,
                //         ]
                //     }
                // }, 
                // {
                //     type: InputType.input,
                //     label: "评论用户",
                //     decorator_id: "comment_user_id",
                //     option: {
                //         placeholder: "请输入评论用户"
                //     }
                // }, 
                {
                    type: InputType.rangePicker,
                    label: "评论时间",
                    decorator_id: "date_range"
                },
                {
                    type: InputType.select,
                    label: "审核状态",
                    decorator_id: "audit_status",
                    option: {
                        placeholder: "请选择审核状态",
                        childrens: [{
                            value: '待审批',
                            label: '待审核'
                        }, {
                            value: '通过',
                            label: '审核通过'
                        }, {
                            value: '不通过',
                            label: '审核不通过'
                        }].map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构",
                //     }
                // }
            ],
            // btn_props: [{
            //     label: '添加评论',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.comment_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_comment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let comment_list = Object.assign(comment_data, table_param);
        return (
            <Row>
                <Modal
                    title="请选择审核结果"
                    visible={this.state.modal_show}
                    onOk={() => this.hideModal()}
                    onCancel={() => this.hideModal()}
                    okText="确定"
                    cancelText="取消"
                    okButtonProps={{ disabled: true }}
                    cancelButtonProps={{ disabled: true }}
                >
                    <Row>
                        <Input placeholder="不通过请输入原因" value={this.state.reason} onChange={(e: any) => this.changeValue(e)} />
                    </Row>
                    <br />
                    <br />
                    <Row type="flex" justify="center">
                        <Button type="danger" onClick={() => this.auditThis('不通过')}>不通过</Button>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <Button type="primary" onClick={() => this.auditThis('通过')}>通过</Button>
                    </Row>
                </Modal>
                <SignFrameLayout {...comment_list} />
            </Row>
        );
    }
}

/**
 * 控件：评论管理列表
 * 描述
 */
@addon('CommentListView', '评论管理列表', '描述')
@reactControl(CommentListView, true)
export class CommentListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public list_type?: string;
    public org_type?: string;
}