import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { request } from "src/business/util_tool";
import { Select } from 'antd';

let { Option } = Select;
/**
 * 组件：评论审核列表状态
 */
export interface CommentAuditViewState extends ReactViewState {
    // 评论类型列表
    type_list?: [];
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：评论审核列表
 * 描述
 */
export class CommentAuditView extends ReactView<CommentAuditViewControl, CommentAuditViewState> {
    private columns_data_source = [{
        title: '评论对象',
        dataIndex: 'comment_object',
        key: 'comment_object',
    }, {
        title: '父级评论',
        dataIndex: 'father_comment',
        key: 'father_comment',
    }, {
        title: '评论类型',
        dataIndex: 'type',
        key: 'type',
    }, {
        title: '评论等级',
        dataIndex: 'level',
        key: 'level',
    }, {
        title: '评论内容',
        dataIndex: 'content',
        key: 'content',
    }, {
        title: '评论时间',
        dataIndex: 'comment_date',
        key: 'comment_date',
    }, {
        title: '评论审核状态',
        dataIndex: 'audit_status',
        key: 'audit_status',
    }, {
        title: '审核意见',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            type_list: [],
            request_url: '',
        };
    }
    /** 新增按钮 */
    // add = () => {
    //     this.props.history!.push(ROUTE_PATH.comment);
    // }

    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeCommentAudit + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        request(this, AppServiceUtility.comment_type_service.get_comment_type_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    type_list: datas.result,
                });
            });

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let type_list: any[] = [];
        this.state.type_list!.map((item, idx) => {
            type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let comment_data = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "评论对象名称",
                    decorator_id: "comment_object_id",
                    option: {
                        placeholder: "请输入评论对象名称",
                    }
                }, {
                    type: InputType.select,
                    label: "评论类型",
                    decorator_id: "type",
                    option: {
                        placeholder: "请选择评论类型",
                        childrens: type_list
                    }

                }, {
                    type: InputType.select,
                    label: "评论等级",
                    decorator_id: "level",
                    option: {
                        placeholder: "请输入评论等级",
                        childrens: [
                            <Option key={'5'}>{'5'}</Option>,
                            <Option key={'4'}>{'4'}</Option>,
                            <Option key={'3'}>{'3'}</Option>,
                            <Option key={'2'}>{'2'}</Option>,
                            <Option key={'1'}>{'1'}</Option>,
                        ]
                    }

                }, {
                    type: InputType.input,
                    label: "评论用户",
                    decorator_id: "comment_user",
                    option: {
                        placeholder: "请输入评论用户"
                    }

                }, {
                    type: InputType.rangePicker,
                    label: "评论时间",
                    decorator_id: "date_range"
                },
            ],
            // btn_props: [{
            //     label: '添加评论',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.comment_manage_service,
            service_option: {
                select: {
                    service_func: 'get_comment_audit_list_all',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_comment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let comment_list = Object.assign(comment_data, table_param);
        return (
            <SignFrameLayout {...comment_list} />
        );
    }
}

/**
 * 控件：评论审核列表
 * 描述
 */
@addon('CommentAuditView', '评论审核列表', '描述')
@reactControl(CommentAuditView, true)
export class CommentAuditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}