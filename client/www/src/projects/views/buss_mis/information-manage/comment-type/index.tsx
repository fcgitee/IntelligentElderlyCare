import { Row } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { isPermission } from "src/projects/app/permission";

/**
 * 组件：评论类型状态
 */
export interface CommentTypeViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 评论类型集合 */
    // service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 评论类型总条数 */
    total?: number;
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：评论类型
 * 描述
 */
export class CommentTypeView extends ReactView<CommentTypeViewControl, CommentTypeViewState> {

    private columns_data_source = [{
        title: '类型名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            request_url: '',
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeCommentType);
    }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }

    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeCommentType + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "编号",
                    decorator_id: "code"
                },
            ],
            btn_props: [{
                label: '新增评论类型',
                btn_method: this.add,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.comment_type_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_comment_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：评论类型
 * 描述
 */
@addon('CommentTypeView', '评论类型', '描述')
@reactControl(CommentTypeView, true)
export class CommentTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}