import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：文章类型页面状态
 */
export interface ChangeArticleTypeViewState extends ReactViewState {
    /** 数据id */
    id?: string;
}

/**
 * 组件：文章类型页面
 * 描述
 */
export class ChangeArticleTypeView extends ReactView<ChangeArticleTypeViewControl, ChangeArticleTypeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: ''
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }

    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "资讯类型",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "资讯类型名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入资讯类型名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入资讯类型名称"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "编号",
                            decorator_id: "code",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入编号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入编号"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入描述"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.articleType);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.article_type_service,
                operation_option: {
                    query: {
                        func_name: "get_article_type_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_article_type"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.articleType); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：文章类型页面
 * 描述
 */
@addon('ChangeArticleTypeView', '文章类型页面', '描述')
@reactControl(ChangeArticleTypeView, true)
export class ChangeArticleTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}