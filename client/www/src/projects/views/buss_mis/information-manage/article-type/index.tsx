import { Row, Form, Button, Input, message, Col, Card, Icon } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { isPermission } from "src/projects/app/permission";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import TextArea from "antd/lib/input/TextArea";
import './index.less';

class MySecondForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            isShow: true,
        };
    }
    componentDidMount() {
    }
    handleUpdateSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['name'] = values['name'];
                formValue['code'] = values['code'];
                formValue['description'] = values['description'];
                formValue['remark'] = values['remark'];
                request(this, AppServiceUtility.article_type_service.update_article_type!(formValue))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                this.props.formCreator && this.props.formCreator.reflash && this.props.formCreator.reflash();
                            });
                        }
                    })
                    .catch((error: any) => {
                        message.error(error);
                    });
            }
        });
    }
    toggleAdd() {
        // console.log(this.state.isShow);
        this.setState({
            isShow: !this.state.isShow
        });
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let layout_personnel = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 8 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 16 },
                sm: { span: 20 },
            },
        };
        let isShow = this.state.isShow;
        return (
            <MainContent>
                <Card title="资讯类型设定" extra={<Icon type="double-right" rotate={isShow ? -90 : 90} onClick={() => this.toggleAdd()} />} className={!isShow ? 'articleTypeAddForm justHide' : 'articleTypeAddForm'}>
                    <Form {...layout_personnel} onSubmit={this.handleUpdateSubmit}>
                        <Row gutter={24}>
                            <Col span={20}>
                                <Row>
                                    <Col span={8}>
                                        <Form.Item label='资讯类型名称'>
                                            {getFieldDecorator('name', {
                                                rules: [{ required: true, message: "请输入资讯类型名称" }],
                                            })(
                                                <Input autoComplete="off" placeholder="请输入资讯类型名称" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='编号'>
                                            {getFieldDecorator('code', {
                                                rules: [{ required: true, message: "请输入编号" }],
                                            })(
                                                <Input autoComplete="off" placeholder="请输入编号" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item label='备注'>
                                            {getFieldDecorator('remark', {
                                            })(
                                                <TextArea rows={4} placeholder="请输入备注" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={24}>
                                        <Form.Item label='描述'>
                                            {getFieldDecorator('description', {
                                                rules: [{ required: true, message: "请输入描述" }],
                                            })(
                                                <TextArea rows={4} placeholder="请输入描述" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={4}>
                                <Row type='flex' justify='end' align="bottom" style={{ minHeight: '240px' }}>
                                    <Button htmlType='submit' type='primary'>保存</Button>
                                </Row>
                            </Col>
                        </Row>
                    </Form>
                </Card>
            </MainContent>
        );
    }
}
const Form2 = Form.create<any>()(MySecondForm);

/**
 * 组件：文章类型状态
 */
export interface ArticleTypeViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 文章类型集合 */
    // service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 文章类型总条数 */
    total?: number;
    /** 接口名 */
    request_url?: string;
    // 是否有新增权限
    isAddPermission?: boolean;
}

/**
 * 组件：文章类型
 * 描述
 */
export class ArticleTypeView extends ReactView<ArticleTypeViewControl, ArticleTypeViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '资讯类型名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '描述',
        dataIndex: 'description',
        key: 'description',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            request_url: '',
            isAddPermission: false,
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeArticleType);
    }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }

    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeArticleType + '/' + contents.id);
        }
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        request(this, AppServiceUtility.article_type_service.check_article_type_permission!())
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        isAddPermission: true,
                    });
                }
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "资讯类型名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "编号",
                    decorator_id: "code"
                },
            ],
            // btn_props: [{
            //     label: '新增资讯类型',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.article_type_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_article_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                {this.state.isAddPermission ? <Form2 formCreator={this.formCreator} /> : null}
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：文章类型
 * 描述
 */
@addon('ArticleTypeView', '文章类型', '描述')
@reactControl(ArticleTypeView, true)
export class ArticleTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}