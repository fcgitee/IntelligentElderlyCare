import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
// import { Card, Descriptions, Row, } from "antd";
import { Card, Descriptions, Row, } from "antd";
/**
 * 组件：资讯详情状态
 */
export class NewsInfoState {
}

/**
 * 组件：资讯详情
 * 资讯详情
 */
export class NewsInfo extends React.Component<NewsInfoControl, NewsInfoState> {

    componentWillMount() {
        this.setState({
            news_info: this.props.news_info || [],
        });
    }
    render() {
        let { news_info } = this.props;

        // 初始化，抑制报错
        if (!news_info) {
            news_info = [];
        }

        return (
            <Card>
                <Descriptions title="新闻详情" bordered={true}>
                    <Descriptions.Item label="新闻标题">{news_info['title'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="新闻副标题">{news_info['subhead'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="显示状态">{news_info['showing_status'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="新闻描述">{news_info['description'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="作者">{news_info['author'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="新闻来源">{news_info['source'] || ''}</Descriptions.Item>
                    <Descriptions.Item label="新闻app图片" span={3}>
                        {news_info['app_img_list'] && news_info['app_img_list'].length > 0 ? news_info['app_img_list'].map((item: any, index: number) => {
                            return (
                                <Row key={index}>
                                    <img style={{ maxWidth: '200px' }} alt={news_info['title']} src={item} />
                                </Row>
                            );
                        }) : null}
                    </Descriptions.Item>
                    <Descriptions.Item label="新闻内容" span={3}><div dangerouslySetInnerHTML={{ __html: news_info['content'] || '' }} /></Descriptions.Item>
                </Descriptions>
            </Card>
        );
    }
}

/**
 * 控件：资讯详情控制器
 * 资讯详情
 */
@addon('NewsInfo', '资讯详情', '资讯详情')
@reactControl(NewsInfo, true)
export class NewsInfoControl extends BaseReactElementControl {
    /** 基础数据 */
    news_info?: any;
}