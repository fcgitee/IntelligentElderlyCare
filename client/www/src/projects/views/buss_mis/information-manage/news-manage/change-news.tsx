import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState, CookieUtil } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { edit_props_info, beforeUpload } from "src/projects/app/util-tool";
// import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { remote } from 'src/projects/remote';
import { request } from "src/business/util_tool";
import { Steps, Select, Row, message } from "antd";
import { User } from 'src/business/models/user';
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
const { Step } = Steps;
const { Option } = Select;
/**
 * 组件：新闻编辑视图状态
 */
export interface ChangeNewsViewState extends ReactViewState {
    // 审批内容
    sp_msg?: string;
    // 是否发送
    is_send?: boolean;
    // 发布步骤
    news_step: any;
    // 类型列表
    type_list?: any;
    // 资讯内容
    data_info?: any;
    // 草稿内容
    draft_info?: any;
    is_has_draft?: any;
    title?: any;
    subhead?: any;
    type_id?: any;
    tags?: any;
    showing_status?: any;
    description?: any;
    content?: any;
    app_img_list?: any;
    source?: any;
    author?: any;
}

export const news_step = [{
    'step_name': '发布',
}, {
    'step_name': '审核',
}, {
    'step_name': '完成',
}];

/**
 * 组件：新闻编辑视图
 * 描述
 */
export class ChangeNewsView extends ReactView<ChangeNewsViewControl, ChangeNewsViewState> {
    private formCreator: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            is_send: false,
            type_list: [],
            data_info: [],
            news_step: news_step,
            draft_info: {},
            is_has_draft: false,
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.article_type_service.get_article_type_list_all!({}))
            .then((data: any) => {
                if (data && data.result && data.result.length) {
                    this.setState({
                        type_list: data.result
                    });
                }
            });
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.article_service.get_news_list!({ id: this.props.match!.params.key, all: true }, 1, 1))
                .then((datas: any) => {
                    if (datas.result && datas.result[0]) {
                        this.setState({
                            data_info: datas.result[0],
                        });
                    }
                });
        } else {
            request(this, AppServiceUtility.person_org_manage_service.get_draft_list!({ type: 'article' }))
                .then((data: any) => {
                    if (data && data.result && data.result.length) {
                        this.setState({
                            draft_info: data.result[0],
                            is_has_draft: true
                        });
                    }
                });
        }
    }

    readDraft() {
        let draft_info = this.state.draft_info;
        draft_info.draft = draft_info.draft || {};
        this.formCreator.setChildFieldsValue({
            'title': draft_info.draft.title,
            'subhead': draft_info.draft.subhead,
            'type_id': draft_info.draft.type_id,
            'tags': draft_info.draft.tags,
            'showing_status': draft_info.draft.showing_status,
            'description': draft_info.draft.description,
            'content': draft_info.draft.content,
            'app_img_list': draft_info.draft.app_img_list,
            'source': draft_info.draft.source,
            'author': draft_info.draft.author,
        });
        message.info('读取草稿成功！');
    }

    onRef = (ref: any) => {
        this.formCreator = ref;
    }

    returnPublishForm() {
        const type_list: JSX.Element[] = this.state.type_list!.map((item: any, idx: any) => <Option key={item.id} value={item.id}>{item.name}</Option>);
        // let data_info = this.state.data_info;
        let edit_props = {
            form_items_props: [
                {
                    title: "新闻编辑",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "资讯标题",
                            decorator_id: "title",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入资讯标题" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入资讯标题",
                                autoComplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "资讯副标题",
                            decorator_id: "subhead",
                            field_decorator_option: {
                                rules: [{ message: "请输入资讯副标题" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入资讯副标题",
                                autoComplete: 'off',
                            }
                        },
                        {
                            type: InputType.select,
                            label: "资讯类型",
                            decorator_id: "type_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择资讯类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                childrens: type_list,
                                placeholder: "请选择类型",
                                showSearch: true,
                                autoComplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "资讯标签",
                            decorator_id: "tags",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入资讯标签，以逗号分割" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入资讯标签，以逗号分割",
                                autoComplete: 'off',
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "显示状态",
                            decorator_id: "showing_status",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '置顶',
                                    value: '置顶',
                                }]
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "新闻描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ message: "请输入新闻描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入新闻描述",
                                autoComplete: 'off',
                            }
                        }, {
                            type: InputType.nt_rich_text,
                            label: "新闻内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入新闻内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入新闻内容",
                                value: ' ',
                                autoComplete: 'off',
                                remoteUrl: remote.upload_url
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "发布方",
                            decorator_id: "author",
                            field_decorator_option: {
                                rules: [{ message: "请输入发布方" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "发布方",
                                autoComplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "标题图片上传（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "app_img_list",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传标题图片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                beforeUpload: beforeUpload
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "新闻来源",
                            decorator_id: "source",
                            field_decorator_option: {
                                rules: [{ message: "请输入新闻来源" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "新闻来源",
                                autoComplete: 'off',
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        history.back();
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交审核"
            },
            caogao_btn_propps: {
                text: "保存草稿",
                type: { 'type': '草稿' }
            },
            service_option: {
                service_object: AppServiceUtility.article_service,
                operation_option: {
                    query: {
                        func_name: "get_news_pure_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_news"
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
            onRef: this.onRef,
        };
        if (this.props.match!.params.key) {
            edit_props.form_items_props[0].input_props.push({
                type: InputType.antd_input,
                label: "编辑者",
                decorator_id: "editor",
                field_decorator_option: {
                    initialValue: CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : "",
                } as GetFieldDecoratorOptions,
                option: {
                    placeholder: "编辑者",
                    autoComplete: 'off',
                }
            });
        }
        // if (data_info.hasOwnProperty('status') && data_info.status === '不通过') {
        //     edit_props.form_items_props[0]['extra'] = <Row type="flex" justify="end" style={{ border: '1px solid red', padding: '10px' }}>审核不通过原因：{data_info.reason}</Row>;
        // }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <FormCreator {...edit_props_list} />
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let { data_info, news_step, is_has_draft, draft_info } = this.state;

        let stepStatus: any = {};
        // 步骤条状态
        let step = 0;
        if (data_info.status === '不通过') {
            stepStatus.status = 'error';
            news_step[news_step.length - 1]['step_desc'] = data_info.reason || '审核不通过';
            step = 2;
        } else if (data_info.status === '通过') {
            stepStatus.status = 'finish';
            news_step[news_step.length - 1]['step_desc'] = data_info.reason || '审核通过';
            step = 2;
        } else if (data_info.status === '待审批') {
            step = 1;
        }
        return (
            <MainContent>
                <Steps current={step} {...stepStatus} style={{ background: "white", padding: "25px" }}>
                    {news_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} description={item.step_desc} />
                        );
                    })}
                </Steps>
                {this.returnPublishForm()}
                {is_has_draft ? <Row type="flex" style={{ paddingBottom: '10px', background: '#fff' }} justify="center">上次草稿保存时间：{draft_info.create_date}，可选择&nbsp;&nbsp;&nbsp;<span style={{ color: '#1890ff', cursor: 'pointer' }} onClick={() => this.readDraft()}>读取草稿</span></Row> : null}
            </MainContent>
        );
    }
}

/**
 * 控件：新闻编辑视图
 * 描述
 */
@addon('ChangeNewsView', '新闻编辑视图', '描述')
@reactControl(ChangeNewsView, true)
export class ChangeNewsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 带参 */
    public select_param?: any;
}