import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
// import { request } from "src/business/util_tool";
import { Select, message } from 'antd';

let { Option } = Select;
/**
 * 组件：新闻管理列表状态
 */
export interface NewsListViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：新闻管理列表
 * 描述
 */
export class NewsListView extends ReactView<NewsListViewControl, NewsListViewState> {
    private columns_data_source = [{
        title: '资讯标题',
        dataIndex: 'title',
        key: 'title',
    }, {
        title: '资讯类型',
        dataIndex: 'type_name',
        key: 'type_name',
    }, {
        title: '发布方',
        dataIndex: 'author',
        key: 'author',
        render: (text: any, record: any) => {
            if (record.author) {
                return record.author;
            } else {
                return record.org_name;
            }
        }
    }, {
        title: '审批状态',
        dataIndex: 'status',
        key: 'status',
        render: (text: any, record: any) => {
            if (text === '不通过') {
                return `不通过，原因：【${record.reason || '无'}】`;
            }
            return text;
        },
    }, {
        title: '审批时间',
        dataIndex: 'audit_date',
        key: 'audit_date',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }, {
        title: '发布时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
            org_list: [],
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeNews);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            if (this.props.hasOwnProperty('list_type') && this.props.list_type === 'sh') {
                this.props.history!.push(ROUTE_PATH.changeNewsSh + '/' + contents.id);
            } else {
                if (contents.hasOwnProperty('status') && contents['status'] === '待审批') {
                    message.info('该资讯待审批中，请耐心等候！');
                    return;
                }
                this.props.history!.push(ROUTE_PATH.changeNews + '/' + contents.id);
            }
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const param = this.props.org_type ? { org_type: this.props.org_type } : {};
        let news_data: any = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "资讯标题",
                    decorator_id: "title",
                    option: {
                        placeholder: "请输入资讯标题",
                    }
                }, {
                    type: InputType.input,
                    label: "发布方",
                    decorator_id: "author",
                    option: {
                        placeholder: "请输入发布方",
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "发布时间",
                    decorator_id: "date_range"
                },
                {
                    type: InputType.select,
                    label: "审批状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择审批状态",
                        childrens: [{
                            value: '待审批',
                            label: '待审批'
                        }, {
                            value: '通过',
                            label: '通过'
                        }, {
                            value: '不通过',
                            label: '不通过'
                        }].map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构",
                //     }
                // },
            ],
            btn_props: [{
                label: '新增资讯',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.article_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'delete_news'
                }
            },
            searchExtraParam: param,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let news_list = Object.assign(news_data, table_param);
        return (
            <SignFrameLayout {...news_list} />
        );
    }
}

/**
 * 控件：新闻管理列表
 * 描述
 */
@addon('NewsListView', '新闻管理列表', '描述')
@reactControl(NewsListView, true)
export class NewsListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    // 列表类型
    public list_type?: string;
    // 机构类型
    public org_type?: string;
}