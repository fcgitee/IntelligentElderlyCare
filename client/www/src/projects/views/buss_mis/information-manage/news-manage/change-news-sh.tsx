import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon, Permission } from "pao-aop";
import React from "react";
import { Card, Descriptions, Row, Button, message, Spin, Steps, Select, Col, } from "antd";
import { request, autoShowPicture } from "src/business/util_tool";
import { news_step } from "./change-news";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";
/**
 * 组件：资讯审核状态
 */
export class ChangeNewsShState {
    news_info?: any;
    reason?: any;
    is_send?: boolean;
    action_value?: string;
}

/**
 * 组件：资讯审核
 * 资讯审核
 */
export class ChangeNewsSh extends React.Component<ChangeNewsShViewControl, ChangeNewsShState> {
    constructor(props: ChangeNewsShViewControl) {
        super(props);
        this.state = {
            action_value: '',
            reason: '',
            news_info: [],
            is_send: false,
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.article_service.get_news_pure_list!({ id: this.props.match!.params.key }))
                .then((data: any) => {
                    if (data && data.result && data.result.length > 0) {
                        this.setState({
                            news_info: data.result[0],
                        });
                    }
                });
        }
        // 放大图片
        autoShowPicture();
    }
    back() {
        history.back();
    }
    change(e: any) {
        this.setState({
            action_value: e,
        });
    }
    save() {
        let { news_info, reason, is_send, action_value } = this.state;
        if (!news_info.hasOwnProperty('id')) {
            message.info('没有找到可操作的数据！');
            return;
        }
        if (!action_value) {
            message.info('请选择审核结果！');
            return;
        }
        if (action_value !== '1' && reason === '') {
            message.info('不通过请填写原因！');
            return;
        }
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.article_service.update_news!({ id: news_info.id, action: "sh", action_value: action_value, reason: reason }))
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('操作成功！', 1, () => {
                                history.back();
                            });
                        } else {
                            this.setState({
                                is_send: false
                            });
                            message.info('操作失败！');
                        }
                    });
            }
        );
    }
    setRejectReason(e: any) {
        this.setState({
            reason: e.target.value,
        });
    }
    render() {
        let { news_info } = this.state;

        // 初始化，抑制报错
        if (!news_info) {
            news_info = [];
        }

        const { Step } = Steps;
        const { Option } = Select;

        let step = 0;
        if (news_info.status === '不通过') {
            step = 2;
        } else if (news_info.status === '通过') {
            step = 2;
        } else if (news_info.status === '待审批') {
            step = 1;
        }
        return (
            <MainContent>
                <Steps current={step} style={{ background: "white", padding: "25px" }}>
                    {news_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} />
                        );
                    })}
                </Steps>
                {news_info.hasOwnProperty('id') ? <Row>
                    <Card className="shenhe-card">
                        <Descriptions title="资讯审核" bordered={true}>
                            <Descriptions.Item label="资讯标题">{news_info['title'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="资讯副标题">{news_info['subhead'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="资讯类型">{news_info['type_name'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="资讯标签">{news_info['tags'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="显示状态">{news_info['showing_status'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="新闻描述">{news_info['description'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="发布方">{news_info['author'] || news_info['org_name'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="新闻来源">{news_info['source'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="标题图片" className="viewPictureWhenClick">
                                {news_info['app_img_list'] && news_info['app_img_list'].length > 0 ? news_info['app_img_list'].map((item: any, index: number) => {
                                    return (
                                        <Row key={index}>
                                            <img alt={news_info['title']} src={item} />
                                        </Row>
                                    );
                                }) : null}
                            </Descriptions.Item>
                            <Descriptions.Item label="新闻内容" span={3}><div dangerouslySetInnerHTML={{ __html: news_info['content'] || '' }} className="viewPictureWhenClick" /></Descriptions.Item>
                            <Descriptions.Item label="提交时间">{news_info['create_date'] ? news_info['create_date'] : ''}</Descriptions.Item>
                        </Descriptions>
                    </Card>
                    {news_info.status === '待审批' ? <MainCard>
                        <Row>
                            <Col span={3}>
                                <Select style={{ width: '90%' }} placeholder="请选择审核结果" onChange={(e: any) => this.change(e)}>
                                    <Option value="1">通过</Option>
                                    <Option value="2">不通过</Option>
                                    {/* <Option value="3">打回</Option> */}
                                </Select>
                            </Col>
                            <Col span={21}>
                                <TextArea placeholder="原因" rows={4} onChange={(e) => { this.setRejectReason(e); }} />
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button disabled={this.state.is_send} type='primary' onClick={() => this.save()}>保存</Button>
                            <Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                        </Row>
                    </MainCard> : <Row>
                            <Card>
                                <Descriptions title="审核信息" bordered={true}>
                                    <Descriptions.Item label="审核结果">{news_info['status'] || ''}</Descriptions.Item>
                                    {news_info.status === '不通过' ? <Descriptions.Item label="不通过原因">{news_info['reason'] || ''}</Descriptions.Item> : null}
                                    <Descriptions.Item label="审核时间">{news_info['audit_date'] || ''}</Descriptions.Item>
                                </Descriptions>
                            </Card>
                            <MainCard>
                                <Row type="flex" justify="center" className="ctrl-btns">
                                    <Button type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                                </Row>
                            </MainCard>
                        </Row>}
                </Row> : <Row type="flex" justify="center">
                        <Spin spinning={true} tip={"加载中"} size={"large"} /></Row>}
            </MainContent>
        );
    }
}

/**
 * 控件：资讯审核控制器
 * 资讯审核
 */
@addon('ChangeNewsSh', '资讯审核', '资讯审核')
@reactControl(ChangeNewsSh, true)
export class ChangeNewsShViewControl extends BaseReactElementControl {
    /** 视图权限 */
    public permission?: Permission;
}