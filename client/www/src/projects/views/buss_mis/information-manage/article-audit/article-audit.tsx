import { message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';

let { Option } = Select;
/**
 * 组件：文章审核状态
 */
export interface ChangeArticleAuditState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /* 文章类型列表*/
    type_list?:[];
}

/**
 * 组件：文章审核
 * 描述
 */
export class ChangeArticleAudit extends ReactView<ChangeArticleAuditControl, ChangeArticleAuditState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: '',
            type_list:[],
        };
    }
    refuse = () => {
        let submitObj = document.getElementById('remark') as HTMLInputElement;
        // console.log('submitObj', submitObj!.value);
        request(this, AppServiceUtility.article_service.article_audit_fail!({ id: this.state.id, remark: submitObj!.value }))
            .then((data: any) => {
                if (data === 'Success') {
                    this.props.history!.push(ROUTE_PATH.articleAudit);
                } else {
                    message.info('操作失败');
                }
            });

    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
        request(this, AppServiceUtility.article_type_service.get_article_type_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    type_list: datas.result,
                });
            });

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let type_list: any[] = [];
        this.state.type_list!.map((item, idx) => {
            type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        // let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "文章审核",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "文章标题",
                            decorator_id: "title",
                            field_decorator_option: {
                                rules: [{  message: "请输入文章标题" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章标题",
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "文章类型",
                            decorator_id: "title",
                            field_decorator_option: {
                                rules: [{  message: "请输入文章类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章类型",
                                disabled: true
                            }
                        },  {
                            type: InputType.antd_input,
                            label: "发布者",
                            decorator_id: "promulgator",
                            field_decorator_option: {
                                rules: [{  message: "请选择发布者" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择发布者",
                                disabled: true
                            }
                        }, {
                            type: InputType.radioGroup,
                            label: "显示状态",
                            decorator_id: "showing_status",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择显示状态" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '首页',
                                    value: '首页',
                                }, {
                                    label: '置顶',
                                    value: '置顶',
                                }],
                                disabled: true
                            }
                        }, {
                            type: InputType.text_area,
                            label: "文章描述",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{  message: "请输入文章描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章描述",
                                disabled: true
                            }
                        }, {
                            type: InputType.nt_rich_text,
                            label: "文章内容",
                            decorator_id: "content",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入文章内容" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章内容",
                                disabled: true,
                            }
                        }, {
                            type: InputType.upload,
                            label: "文章web图片列表（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "web_img_list",
                            field_decorator_option: {
                                rules: [{  message: "请输入文章web图片列表" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章web图片列表",
                                disabled: true
                            }
                        }, {
                            type: InputType.upload,
                            label: "文章app图片列表（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "app_img_list",
                            field_decorator_option: {
                                rules: [{  message: "请输入文章app图片列表" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章app图片列表",
                                disabled: true
                            }
                        },                        
                        {
                            type: InputType.antd_input,
                            label: "文章发布时间",
                            decorator_id: "issue_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入文章发布时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入文章发布时间",
                                disabled: true
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "审核意见",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "审核意见"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "审核失败",
                    cb: this.refuse
                },
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.articleAudit);
                    }
                }
            ],
            submit_btn_propps: {
                text: "审核通过"
            },
            service_option: {
                service_object: AppServiceUtility.article_service,
                operation_option: {
                    save: {
                        func_name: "article_audit_success"
                    },
                    query: {
                        func_name: "get_article_audit_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.articleAudit); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：文章审核控制器
 * 描述
 */
@addon('ChangeArticleAudit', '文章审核', '描述')
@reactControl(ChangeArticleAudit, true)
export class ChangeArticleAuditControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}