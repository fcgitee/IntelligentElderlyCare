import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { request } from "src/business/util_tool";
import { Select } from 'antd';

let { Option } = Select;
/**
 * 组件：文章审核列表状态
 */
export interface ArticleAuditViewState extends ReactViewState {
    // 文章类型列表
    type_list?: [];
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：文章审核列表
 * 描述
 */
export class ArticleAuditView extends ReactView<ArticleAuditViewControl, ArticleAuditViewState> {
    private columns_data_source = [{
        title: '文章标题',
        dataIndex: 'title',
        key: 'ntitle',
    }, {
        title: '文章类型',
        dataIndex: 'type',
        key: 'type',
    }, {
        title: '文章发布者',
        dataIndex: 'promulgator',
        key: 'promulgator',
    }, {
        title: '文章描述',
        dataIndex: 'description',
        key: 'description',
    }, {
        title: '文章发布时间',
        dataIndex: 'issue_date',
        key: 'issue_date',
    }, {
        title: '文章状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            type_list: [],
            request_url: '',
        };
    }
    /** 新增按钮 */
    // add = () => {
    //     this.props.history!.push(ROUTE_PATH.article);
    // }

    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeArticleAudit + '/' + contents.id);
        }
    }
    componentDidMount() {
        request(this, AppServiceUtility.article_type_service.get_article_type_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    type_list: datas.result,
                });
            });

    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let type_list: any[] = [];
        this.state.type_list!.map((item, idx) => {
            type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let article_data = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "文章类型",
                    decorator_id: "type",
                    option: {
                        placeholder: "请选择文章类型",
                        childrens: type_list
                    }

                }, {
                    type: InputType.input,
                    label: "文章发布者",
                    decorator_id: "promulgator",
                    option: {
                        placeholder: "请输入文章发布者"
                    }

                }, {
                    type: InputType.rangePicker,
                    label: "文章发布时间",
                    decorator_id: "date_range"
                },
            ],
            // btn_props: [{
            //     label: '添加文章',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.article_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_article'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let article_list = Object.assign(article_data, table_param);
        return (
            <SignFrameLayout {...article_list} />
        );
    }
}

/**
 * 控件：文章审核列表
 * 描述
 */
@addon('ArticleAuditView', '文章审核列表', '描述')
@reactControl(ArticleAuditView, true)
export class ArticleAuditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}