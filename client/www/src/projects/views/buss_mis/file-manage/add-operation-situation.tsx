import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
// import { Select } from 'antd';
// const { Option } = Select;
/**
 * 组件：机构运营情况新增状态
 */
export interface AddOperationSituationState extends ReactViewState {
    base_data?: any;
    org_list?: any;
}

/**
 * 组件：机构运营情况新增
 * 描述
 */
export class AddOperationSituation extends ReactView<AddOperationSituationControl, AddOperationSituationState> {
    constructor(props: AddOperationSituationControl) {
        super(props);
        this.state = {
            base_data: {},
            org_list: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type: '福利院' })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "新增运营情况记录",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.tree_select,
                            label: "组织机构",
                            decorator_id: "organization_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "组织机构" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                allowClear: true,
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                treeDefaultExpandAll: true,
                                treeData: this.state.org_list,
                                placeholder: "请选择组织机构",
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "年度",
                            decorator_id: "year",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入年度" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入年度"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "年度收入",
                            decorator_id: "year_income",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入年度收入" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入年度收入"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "年度支出",
                            decorator_id: "year_payout",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入年度支出" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入年度支出"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "区级财政建设投入资金",
                            decorator_id: "qu_income",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入区级财政建设投入资金" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入区级财政建设投入资金"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "镇级财政建设投入资金",
                            decorator_id: "zhen_income",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入镇级财政建设投入资金" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入镇级财政建设投入资金"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "慈善资金投入资金",
                            decorator_id: "charitable_income",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入慈善资金投入资金" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入慈善资金投入资金"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "机构自筹资金",
                            decorator_id: "self_income",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入机构自筹资金" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构自筹资金"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "社会资助",
                            decorator_id: "social_assistance",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入社会资助" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入社会资助"
                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "物品折合",
                            decorator_id: "goods",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入物品折合" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入物品折合"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.organizationManage);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_operation_situation"
                    },
                    query: {
                        func_name: "get_operation_situation_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.organizationManage); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
    }
}
/**
 * 控件：机构运营情况新增
 * 描述
 */
@addon('AddOperationSituation', '机构运营情况新增', '描述')
@reactControl(AddOperationSituation, true)
export class AddOperationSituationControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}