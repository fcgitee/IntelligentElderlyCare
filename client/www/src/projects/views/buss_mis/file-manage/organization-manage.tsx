
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Card, Modal, Form, Radio } from 'antd';
import { exprot_excel } from "src/business/util_tool";
import { MainCard } from "src/business/components/style-components/main-card";
import './index.less';
import { MainContent } from "src/business/components/style-components/main-content";
/** 状态：机构运营情况记录列表视图 */
export interface OrganizationManageViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    select_type?: any;
}
/** 组件：机构运营情况记录列表视图 */
export class OrganizationManageView extends React.Component<OrganizationManageViewControl, OrganizationManageViewState> {
    private columns_data_source = [{
        title: '机构名称',
        dataIndex: 'org_name',
        key: 'org_name',
    },
    {
        title: '年度',
        dataIndex: 'year',
        key: 'year',
    },
    {
        title: '年份收入',
        dataIndex: 'year_income',
        key: 'year_income',
    },
    {
        title: '年份支出',
        dataIndex: 'year_payout',
        key: 'year_payout',
    },
    {
        title: '区级财政建设投入资金',
        dataIndex: 'qu_income',
        key: 'qu_income',
    },
    {
        title: '镇级财政建设投入资金',
        dataIndex: 'zhen_income',
        key: 'zhen_income',
    },
    {
        title: '慈善资金投入资金',
        dataIndex: 'charitable_income',
        key: 'charitable_income',
    },
    {
        title: '机构自筹资金',
        dataIndex: 'self_income',
        key: 'self_income',
    },
    {
        title: '社会资助',
        dataIndex: 'social_assistance',
        key: 'social_assistance',
    },
    {
        title: '物品折合',
        dataIndex: 'goods',
        key: 'goods',
    },
    ];
    constructor(props: OrganizationManageViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            show: false,
            select_type: 1,
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.addOperationSituation);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addOperationSituation + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }
    // 导出
    export = () => {
        this.setState({ show: true });
    }
    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            AppServiceUtility.person_org_manage_service.get_operation_situation_list!({})!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "组织机构": item.org_name[0],
                                "年度": item.year,
                                "年度收入": item.year_income,
                                "年度支出": item.year_payout,
                                "区级财政建设投入资金": item.qu_income,
                                "镇级财政建设投入资金": item.zhen_income,
                                "慈善资金投入资金": item.charitable_income,
                                "机构自筹资金": item.self_income,
                                "社会资助": item.social_assistance,
                                "物品折合": item.goods,
                            });
                        });
                        exprot_excel([{ name: '机构运营情况', value: new_data }], '机构运营情况（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        const tabList = [
            // {
            //     key: '基本信息',
            //     tab: '基本信息',
            // },
            // {
            //     key: '服务信息',
            //     tab: '服务信息',
            // },
            {
                key: '运营情况记录',
                tab: '运营情况记录',
            },
        ];
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let organ_operation = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "年份",
                    decorator_id: "year"
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            },
            {
                label: '导出',
                btn_method: this.export,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_operation_situation_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_operation_situation'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let organ_operation_list = Object.assign(organ_operation, table_param);
        return (
            <div>
                <MainContent>
                    <MainCard>
                        <Card
                            tabList={tabList}
                            activeTabKey={'运营情况记录'}
                            className='org-file'
                        >
                            <SignFrameLayout {...organ_operation_list} />
                        </Card>
                    </MainCard>
                </MainContent>
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：机构运营情况记录列表视图控制器
 * @description 机构运营情况记录列表视图
 * @author
 */
@addon('OrganizationManageView', '机构运营情况记录列表视图', '机构运营情况记录列表视图')
@reactControl(Form.create<any>()(OrganizationManageView), true)
export class OrganizationManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}