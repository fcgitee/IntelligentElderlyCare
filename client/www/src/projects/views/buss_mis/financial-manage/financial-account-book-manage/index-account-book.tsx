/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Thursday July 25th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Thursday, 25th July 2019 10:47:51 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、账簿管理列表
 */
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：账簿管理列表视图状态
 */
export interface FinancialAccountBookViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：账簿管理列表视图
 * 描述
 */
export class FinancialAccountBookView extends ReactView<FinancialAccountBookViewControl, FinancialAccountBookViewState> {
    private columns_data_source = [{
        title: '经济主体名称',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '账簿名称',
        dataIndex: 'account_book_name',
        key: 'account_book_name',
    }, {
        title: '账簿类型',
        dataIndex: 'type_book',
        key: 'type_book',
    }, {
        title: '设立日期',
        dataIndex: 'set_date',
        key: 'set_date',
    }, {
        title: '上次结账日期',
        dataIndex: 'settle_account_date',
        key: 'settle_account_date',
    }];

    constructor(props: FinancialAccountBookViewControl) {
        super(props);
        this.state = {
            request_url: '',
        };
    }

    /** 新增账簿按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changefinancialAccountBook);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changefinancialAccountBook + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let account_book_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "经济主体名称",
                    decorator_id: "user_name",
                },
            ], btn_props: [{
                label: '新增账簿',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let account_book_list = Object.assign(account_book_info_list, table_param);
        return (
            <SignFrameLayout {...account_book_list} />
        );
    }
}

/**
 * 控件：账簿管理列表视图控制器
 * 描述
 */
@addon('FinancialAccountBookView', '账簿管理列表视图', '描述')
@reactControl(FinancialAccountBookView, true)
export class FinancialAccountBookViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}