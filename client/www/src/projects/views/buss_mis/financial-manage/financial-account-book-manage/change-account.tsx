/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Thursday July 25th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Thursday, 25th July 2019 10:48:36 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、新增/修改账户内容
 */
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { Select } from "antd";
let { Option } = Select;

/**
 * 组件：新增/修改账户内容
 */
export interface ChangeAccountViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** inputProps */
    inputProps: any[];
}

/**
 * 组件：新增/修改账户内容视图
 */
export class ChangeAccountView extends ReactView<ChangeAccountViewControl, ChangeAccountViewState> {
    constructor(props: any) {
        super(props);
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "账户名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: [{
                title: '账户名称',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: '证件类型',
                dataIndex: 'id_card_type',
                key: 'id_card_type',
            }, {
                title: '证件号',
                dataIndex: 'id_card',
                key: 'id_card',
            }
            ],
            service_name: AppServiceUtility.ipersonnel_service,
            service_func: 'get_personnel_list',
            title: '经济主体查询',
            name_field: 'name',
            select_option: {
                placeholder: "请选择经济主体",
            }
        };
        const account_type = ['真实账户', '机构储蓄'];
        const account_type_list: JSX.Element[] = account_type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
            inputProps: [
                {
                    type: InputType.antd_input,
                    label: "账户名称",
                    decorator_id: "name"
                }, {
                    type: this.props.match!.params.key ? InputType.antd_input : InputType.modal_search,
                    label: "所属经济主体",
                    decorator_id: this.props.match!.params.key ? "main_user_name" : "user_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择经济主体" }],
                        initialValue: this.props.match!.params.code
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请选择经济主体",
                        modal_search_items_props: modal_search_items_props,
                        disabled: this.props.match!.params.key ? true : false,
                    }
                }, {
                    type: this.props.match!.params.key ? InputType.antd_input : InputType.modal_search,
                    label: "代管经济主体",
                    decorator_id: this.props.match!.params.key ? "agent_user_name" : "agent_user_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择经济主体" }],
                        initialValue: this.props.match!.params.code
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请选择经济主体",
                        modal_search_items_props: modal_search_items_props,
                        disabled: this.props.match!.params.key ? true : false,
                    }
                }, {
                    type: InputType.antd_input,
                    label: "账户状态",
                    decorator_id: "account_status",
                    option: {
                        disabled: this.props.match!.params.key ? true : false,
                    }
                }, {
                    type: InputType.antd_input,
                    label: "付款顺序号",
                    decorator_id: "pay_squence",
                    option: {
                        disabled: this.props.match!.params.key ? true : false,
                    }
                }, {
                    type: InputType.select,
                    label: "账号类型",
                    decorator_id: "account_type",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入账号类型" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入账号类型",
                        childrens: account_type_list,
                        disabled: this.props.match!.params.key ? true : false,
                        onChange: this.accountTypeChange,
                    }
                }, {
                    type: InputType.antd_input_number,
                    label: "余额",
                    decorator_id: "balance",
                    option: {
                        disabled: true,
                        value: '0',
                    }
                },
            ]
        };
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.financialAccountManage);
    }

    componentDidMount() {

    }

    accountTypeChange = (value: string) => {
        let inputProps = this.state.inputProps;
        if (value === '真实账户') {
            inputProps.push({
                type: InputType.antd_input,
                label: "拓展账户名",
                decorator_id: "extend_account_name",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入拓展账户名" }],
                } as GetFieldDecoratorOptions,
                option: {
                    disabled: this.props.match!.params.key ? true : false,
                }
            });
            inputProps.push({
                type: InputType.antd_input,
                label: "拓展卡号",
                decorator_id: "extend_card_number",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入拓展卡号" }],
                } as GetFieldDecoratorOptions,
                option: {
                    disabled: this.props.match!.params.key ? true : false,
                }
            });
            this.setState({
                inputProps: inputProps
            });
        } else {
            for (let i = 0; i < inputProps.length; i++) {
                if (inputProps[i].decorator_id === 'extend_account_name') {
                    inputProps.splice(i, 2);
                }
            }
            this.setState({
                inputProps: inputProps,
            });
        }
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let edit_props = {
            form_items_props: [
                {
                    title: "账户",
                    need_card: true,
                    input_props: this.state.inputProps,
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.financialAccountManage);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    save: {
                        func_name: "update_account_data",
                        arguments: [this.props.match!.params.key]
                    },
                    query: {
                        func_name: "get_account_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.financialAccountManage); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：账户信息编辑控件
 * @description 账户信息编辑控件
 * @author
 */
@addon('ChangeTransactionView', '账户信息编辑控件', '账户信息编辑控件')
@reactControl(ChangeAccountView, true)
export class ChangeAccountViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}