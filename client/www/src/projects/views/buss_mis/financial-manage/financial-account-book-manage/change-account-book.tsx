/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Thursday July 25th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Thursday, 25th July 2019 2:46:52 pm
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、新增/修改账簿内容
 */
import { Select } from 'antd';
import { OptionProps } from "antd/lib/select";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

const { Option } = Select;
/**
 * 组件：新增/修改账簿内容
 */
export interface ChangeAccountBookViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** input下拉框数据源 */
    dataSource?: any[];
}

/**
 * 组件：新增/修改账簿内容视图
 */
export class ChangeAccountBookView extends ReactView<ChangeAccountBookViewControl, ChangeAccountBookViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
            dataSource: []
        };
    }
    /** 账户信息管理 */
    accountManage = () => {
        const book_id = this.props.match!.params.key;
        this.props.history!.push(`${ROUTE_PATH.financialAccountManage}/${book_id}`);
    }
    /** 科目信息管理 */
    subjectManage = () => {
    }
    /** 结账管理 */
    settleAccountManage = () => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.financialAccountBook);
    }

    componentDidMount() {
        request(this, AppServiceUtility.financial_service.input_user_name_list!({}))
            .then((dataSource: any) => {
                this.setState({ dataSource });
            });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let edit_props = {
            form_items_props: [
                {
                    title: "账簿信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "经济主体名称",
                            decorator_id: "user_id",
                            option: {
                                placeholder: "请选择经济主体",
                                showSearch: true,
                                childrens: this.state.dataSource!.map((item: any) => <Option key={item.key} value={item.key}>{item.text}</Option>),
                                filterOption: (inputValue: string, option: React.ReactElement<OptionProps>) =>
                                    (option.props.children! as string).indexOf(inputValue) !== -1 ||
                                    (option.key as string).indexOf(inputValue) !== -1
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "账簿名称",
                            decorator_id: "account_book_name"
                        }, {
                            type: InputType.antd_input,
                            label: "账簿类型",
                            decorator_id: "type_book"
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark"
                        }, {
                            type: InputType.antd_input,
                            label: "设立日期",
                            decorator_id: "set_date",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "上次结账日期",
                            decorator_id: "settle_account_date",
                            option: {
                                disabled: true
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.financialAccountBook);
                    }
                }, {
                    text: "账户信息管理",
                    cb: this.accountManage
                }, {
                    text: "科目信息管理",
                    cb: this.subjectManage
                }, {
                    text: "结账管理",
                    cb: this.settleAccountManage
                }
            ],
            submit_btn_propps: {
                text: "新增/修改账簿信息",
            },
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    save: {
                        func_name: "update_account_book_data",
                        arguments: [this.props.match!.params.key]
                    },
                    query: {
                        func_name: "get_account_book_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.financialAccountBook); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：账簿信息编辑控件
 * @description 账簿信息编辑控件
 * @author
 */
@addon('ChangeTransactionView', '账簿信息编辑控件', '账簿信息编辑控件')
@reactControl(ChangeAccountBookView, true)
export class ChangeAccountBookViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}