import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { List, Button, message } from "antd";
import { Card } from "antd";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';

/**
 * 更改账户顺序状态
 */
export interface ChangeSquenceViewState extends ReactViewState {
    account_list: [];
}

/**
 * 组件：更改账户顺序
 * 描述
 */
export class ChangeSquenceView extends ReactView<ChangeSquenceControl, ChangeSquenceViewState> {
    componentDidMount() {
        request(this, AppServiceUtility.financial_service.get_user_account_sequence_all!({}))
            .then((data: any) => {
                this.setState({ account_list: data.result[0].agent_pay_sequence });
            });
    }
    constructor(props: any) {
        super(props);
        this.setState({
            account_list: [],
        });
    }
    moveUp = (value: any) => {
        let list = this.state.account_list;
        let index: number = 0;
        for (let x = 0; x < list.length; x++) {
            if (list[x] === value) {
                index = x;
                break;
            }
        }
        if (index !== 0) {
            let temp = list[index - 1];
            list[index - 1] = list[index];
            list[index] = temp;
        }
        this.setState({
            account_list: list,
        });
    }

    moveDown = (value: any) => {
        let list = this.state.account_list;
        let index: number = list.length - 1;
        for (let x = 0; x < list.length; x++) {
            if (list[x] === value) {
                index = x;
                break;
            }
        }
        if (index !== list.length - 1) {
            let temp = list[index + 1];
            list[index + 1] = list[index];
            list[index] = temp;
        }
        this.setState({
            account_list: list,
        });
    }

    save = () => {
        request(this, AppServiceUtility.financial_service.update_account_data_list!(this.state.account_list))
            .then((data: any) => {
                // console.log(data);
                if (data === 'Success') {
                    message.success('保存成功');
                    // this.props.history!.push(ROUTE_PATH.financialAccountManage);
                }
            });
    }

    render() {
        return (
            <Card>
                <div>
                    <h3 style={{ marginBottom: 16 }}>代付账户顺序</h3>
                    <List
                        header={<div>账户</div>}
                        bordered={true}
                        dataSource={this.state.account_list}
                        renderItem={item => (
                            <List.Item>
                                {item}
                                <Button type="primary" icon="caret-up" onClick={() => { this.moveUp(item); }} />
                                <Button type="primary" icon="caret-down" onClick={() => { this.moveDown(item); }} />
                            </List.Item>
                        )}
                    />
                </div>
                {this.state.account_list && this.state.account_list.length > 0 ? <Button type="primary" onClick={this.save}>保存</Button> : ''}
            </Card>
        );
    }
}

/**
 * 控件：代付账户顺序编辑控件
 * @description 代付账户顺序编辑控件
 * @author
 */
@addon('ChangeSquenceView', '代付账户顺序编辑控件', '代付账户顺序编辑控件')
@reactControl(ChangeSquenceView, true)
export class ChangeSquenceControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}