/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Thursday July 25th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Thursday, 25th July 2019 2:39:42 pm
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、账户管理列表
 */
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：账簿管理列表视图状态
 */
export interface FinancialAccountViewState extends ReactViewState {
    book_id?: string;
}

/**
 * 组件：账簿管理列表视图
 * 描述
 */
export class FinancialAccountView extends ReactView<FinancialAccountViewControl, FinancialAccountViewState> {
    private columns_data_source = [{
        title: '账户名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '所属经济主体',
        dataIndex: 'main_user_name',
        key: 'main_user_name',
    }, {
        title: '代管经济主体',
        dataIndex: 'agent_user_name',
        key: 'agent_user_name',
    }, {
        title: '账号状态',
        dataIndex: 'account_status',
        key: 'account_status',
    }, {
        title: '账号类型',
        dataIndex: 'account_type',
        key: 'account_type',
    }, {
        title: '余额',
        dataIndex: 'balance',
        key: 'balance',
    }];

    constructor(props: FinancialAccountViewControl) {
        super(props);
        this.state = {
        };
    }

    /** 新增账簿按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changefinancialAccount);
    }

    changSeq = () => {
        this.props.history!.push(ROUTE_PATH.changeSquence);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changefinancialAccount + '/' + contents.id);
        }
    }

    componentDidMount() {
        const book_id = this.props.match!.params.key;
        // console.log(book_id);
        this.setState({ book_id });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let account_book_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "账户名",
                    decorator_id: "name",
                }, {
                    type: InputType.input,
                    label: "所属经济主体",
                    decorator_id: "user_id"
                }, {
                    type: InputType.input,
                    label: "经济主体类型",
                    decorator_id: "user_type"
                },
            ],
            btn_props: [{
                label: '开户',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '设置代付顺序',
                btn_method: this.changSeq,
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: 'get_account_list_all',
                    service_condition: [{ 'book_id': this.props.match!.params.key }]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let account_book_list = Object.assign(account_book_info_list, table_param);
        return (
            <SignFrameLayout {...account_book_list} />
        );
    }
}

/**
 * 控件：账户管理列表视图控制器
 * 描述
 */
@addon('FinancialAccountBookView', '账户管理列表视图', '描述')
@reactControl(FinancialAccountView, true)
export class FinancialAccountViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}