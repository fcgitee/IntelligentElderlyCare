/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Wednesday July 24th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Wednesday, 24th July 2019 11:41:21 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、收纳-付款操作
 */
import { Select } from 'antd';
import { OptionProps } from "antd/lib/select";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

const { Option } = Select;
/**
 * 组件：付款操作
 */
export default interface ChangePaymentViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** input下拉框数据源 */
    dataSource?: any[];
}

/**
 * 组件：付款视图
 */
export class ChangePaymentView extends ReactView<ChangePaymentViewControl, ChangePaymentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
            dataSource: []
        };
    }
    /**
     * 打开钱箱操作
     */
    openCashBox = () => {
    }
    /**
     * 第三方付款操作
     */
    thirdReceive = () => {
    }

    componentDidMount() {
        request(this, AppServiceUtility.financial_service.input_account_list!({}))
            .then((dataSource: any) => {
                this.setState({ dataSource });
            });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let edit_props = {
            form_items_props: [
                {
                    title: "付款",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "付款账户",
                            decorator_id: "account_id",
                            option: {
                                placeholder: "请选择账户",
                                showSearch: true,
                                childrens: this.state.dataSource!.map((item: any) => <Option key={item.key} value={item.key}>{item.text}</Option>),
                                filterOption: (inputValue: string, option: React.ReactElement<OptionProps>) =>
                                    (option.props.children! as string).indexOf(inputValue) !== -1 ||
                                    (option.key as string).indexOf(inputValue) !== -1
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "摘要",
                            decorator_id: "abstract",
                        }, {
                            type: InputType.antd_input,
                            label: "金额",
                            decorator_id: "amount",
                        }, {
                            type: InputType.antd_input,
                            label: "原始凭证",
                            decorator_id: "original_voucher",
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.financialAccountFlow);
                    }
                }, {
                    text: "第三方付款",
                    cb: this.thirdReceive
                }
            ],
            submit_btn_propps: {
                text: "付款",
            },
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    save: {
                        func_name: "save_account_payment_data",
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.changepayment); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：新增付款编辑控件
 * @description 新增付款编辑控件
 * @author
 */
@addon('ChangePaymentView', '新增付款编辑控件', '新增付款编辑控件')
@reactControl(ChangePaymentView, true)
export class ChangePaymentViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}