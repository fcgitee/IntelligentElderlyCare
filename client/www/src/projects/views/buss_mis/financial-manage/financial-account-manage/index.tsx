/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Wednesday July 24th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Wednesday, 24th July 2019 11:32:51 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、出纳-账户流水查询界面
 */
import { Select } from 'antd';
import { OptionProps } from "antd/lib/select";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

const { Option } = Select;

/**
 * 组件：账户流水列表视图状态
 */
export interface FinancialAccountFlowViewState extends ReactViewState {
    current?: number;
    param?: string;
    /** input下拉框数据源 */
    dataSource?: any[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：账号流水
 * 描述
 */
export class FinancialAccountFlowView extends ReactView<FinancialAccountFlowViewControl, FinancialAccountFlowViewState> {

    private columns_data_source = [{
        title: '时间',
        dataIndex: 'date',
        key: 'date',
    }, {
        title: '账号',
        dataIndex: 'account',
        key: 'account',
    }, {
        title: '摘要',
        dataIndex: 'abstract',
        key: 'abstract',
    }, {
        title: '借/贷',
        dataIndex: 'borrow_loan',
        key: 'borrow_loan',
    }, {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '原始凭证',
        dataIndex: 'original_voucher',
        key: 'original_voucher',
    }, {
        title: '红冲账户',
        dataIndex: 'return_id',
        key: 'return_id',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            dataSource: [],
            request_url: '',
        };
    }

    /** 收款按钮 */
    receive = () => {
        this.props.history!.push(ROUTE_PATH.changereceive);
    }
    /** 付款按钮 */
    payment = () => {
        this.props.history!.push(ROUTE_PATH.changepayment);
    }
    /** 转账按钮 */
    transfer = () => {
        this.props.history!.push(ROUTE_PATH.changetransfer);
    }

    componentDidMount() {
        request(this, AppServiceUtility.financial_service.input_account_list!({}))
            .then((dataSource: any) => {
                this.setState({ dataSource });
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    /** 自定义图标按钮回调事件-红冲 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeFinancialReturn + '/' + contents.id);
        }
    }

    render() {
        // const redeirect = this.props.isPermission ? this.props.isPermission!(this.props.permission!) : undefined;
        // if (redeirect) {
        //     return redeirect;
        // }
        let financial_account = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "时间范围",
                    decorator_id: "date_range"
                }, {
                    type: InputType.input,
                    label: "摘要",
                    decorator_id: "abstract"
                }, {
                    type: InputType.select,
                    label: "账号",
                    decorator_id: "account_id",
                    option: {
                        placeholder: "请选择账户",
                        showSearch: true,
                        childrens: this.state.dataSource!.map((item: any) => <Option key={item.key} value={item.key}>{item.text}</Option>),
                        filterOption: (inputValue: string, option: React.ReactElement<OptionProps>) =>
                            (option.props.children! as string).indexOf(inputValue) !== -1 ||
                            (option.key as string).indexOf(inputValue) !== -1
                    }
                }
            ],
            btn_props: [{
                label: '收款',
                btn_method: this.receive,
                icon: 'plus'
            }, {
                label: '付款',
                btn_method: this.payment,
                icon: 'plus'
            }, {
                label: '转账',
                btn_method: this.transfer,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        // const steps = [
        //     {
        //         title: "选择账簿",
        //         content: <Button onClick={() => this.setState({ current: (this.state.current! || 0) + 1 })}>查询</Button>
        //     },
        //     {
        //         title: "账户流水",
        //         content: <SignFrameLayout {...financial_account} />
        //     }
        // ];
        let financial = Object.assign(financial_account, table_param);
        return (
            <MainContent>
                {/* <Steps current={this.state.current || 0}>
                    {steps.map(item => <Steps.Step key={item.title} title={item.title} />)}
                </Steps>
                <Row>{steps[this.state.current || 0].content}</Row> */}
                < SignFrameLayout {...financial} />
            </MainContent >
        );
    }
}

/**
 * 控件：财务账号流水信息
 * 描述
 */
@addon('FinancialAccountView', '账号流水', '描述')
@reactControl(FinancialAccountFlowView, true)
export class FinancialAccountFlowViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}