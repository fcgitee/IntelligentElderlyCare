/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Wednesday July 24th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Wednesday, 24th July 2019 2:35:29 pm
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、红冲操作表单
 */
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { edit_props_info } from "src/projects/app/util-tool";

/**
 * 组件：账户记录红冲
 */
export interface ChangeAccountReturnState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
    /** input下拉框数据源 */
    dataSource?: any[];
}

/**
 * 组件：账户记录红冲
 * 描述
 */
export class ChangeAccountReturn extends ReactView<ChangeAccountReturnViewControl, ChangeAccountReturnState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: '',
            dataSource: []
        };
    }

    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "账户红冲",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "时间",
                            decorator_id: "date",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "账号",
                            decorator_id: "account",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "摘要",
                            decorator_id: "abstract",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "借/贷",
                            decorator_id: "borrow_loan",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "金额",
                            decorator_id: "amount",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "原始凭证",
                            decorator_id: "original_voucher",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "账户id",
                            decorator_id: "account_id",
                            option: {
                                disabled: true
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "红冲原因",
                            decorator_id: "red_reason",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入红冲原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入红冲原因"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.goBack();
                    }
                }
            ],
            submit_btn_propps: {
                text: "红冲"
            },
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    save: {
                        func_name: "return_account_data"
                    },
                    query: {
                        func_name: "get_account_flow_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：账户记录红冲
 * 描述
 */
@addon('ChangeAccountReturn', '账户记录红冲', '描述')
@reactControl(ChangeAccountReturn, true)
export class ChangeAccountReturnViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}