/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 12:03:49 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、导出/打印银行对账单
 */
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：导出/打印银行对账单列表视图状态
 */
export interface FinancialBankReconciliationViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：导出/打印银行对账单列表视图
 * 描述
 */
export class FinancialBankReconciliationView extends ReactView<FinancialBankReconciliationViewControl, FinancialBankReconciliationViewState> {
    private columns_data_source = [{
        title: '账单号',
        dataIndex: 'bill_id',
        key: 'bill_id',
    }, {
        title: '扣款账户姓名',
        dataIndex: 'pay_info.name',
        key: 'pay_info.name',
    }, {
        title: '扣款银行卡号',
        dataIndex: 'pay_info.card_id',
        key: 'pay_info.card_id',
    }, {
        title: '收款账户姓名',
        dataIndex: 'receive_info.name',
        key: 'receive_info.name',
    }, {
        title: '收款银行卡号',
        dataIndex: 'receive_info.card_number',
        key: 'receive_info.card_number',
    }, {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '扣款单产生时间',
        dataIndex: 'date',
        key: 'date',
    }];
    constructor(props: FinancialBankReconciliationViewControl) {
        super(props);
        this.state = {
            request_url: '',
        };
    }
    /** 打印按钮 */
    print = () => { };
    /** 导出按钮 */
    export = () => { };
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let reconciliation_info_list = {
            type_show: false,
            btn_props: [{
                label: '打印银行扣款单',
                btn_method: this.print,
                icon: 'plus'
            }, {
                label: '导出银行扣款单',
                btn_method: this.export,
                icon: 'plus'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "扣款账户姓名",
                    decorator_id: "pay_account_name"
                }, {
                    type: InputType.input,
                    label: "扣款银行卡号",
                    decorator_id: "pay_account_card_number"
                }, {
                    type: InputType.input,
                    label: "收款账户姓名",
                    decorator_id: "receive_info_name"
                }, {
                    type: InputType.input,
                    label: "收款银行卡号",
                    decorator_id: "receive_info_card_number"
                }, {
                    type: InputType.rangePicker,
                    label: "产生时间",
                    decorator_id: "date"
                }
            ],
            columns_data_source: this.columns_data_source,
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_object: AppServiceUtility.financial_account_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,
        };
        return (
            <SignFrameLayout {...reconciliation_info_list} />
        );
    }
}

/**
 * 控件：导出/打印银行对账单视图控制器
 * 描述
 */
@addon('FinancialBankReconciliationView', '导出/打印银行对账单视图', '描述')
@reactControl(FinancialBankReconciliationView, true)
export class FinancialBankReconciliationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}