/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 4:52:05 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、查看银行扣款记录
 */
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：银行扣款记录查询列表视图状态
 */
export interface FinancialReconciliationRecordViewState extends ReactViewState {
}

/**
 * 组件：银行扣款记录查询列表视图
 * 描述
 */
export class FinancialReconciliationRecordView extends ReactView<FinancialReconciliationRecordViewControl, FinancialReconciliationRecordViewState> {
    private columns_data_source = [{
        title: '账单号',
        dataIndex: 'bill_id',
        key: 'bill_id',
    }, {
        title: '扣款账户姓名',
        dataIndex: 'pay_info.name',
        key: 'pay_info.name',
    }, {
        title: '扣款银行卡号',
        dataIndex: 'pay_info.card_number',
        key: 'pay_info.card_number',
    }, {
        title: '收款账户姓名',
        dataIndex: 'receive_info.name',
        key: 'receive_info.name',
    }, {
        title: '收款银行卡号',
        dataIndex: 'receive_info.card_number',
        key: 'receive_info.card_number',
    }, {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '扣款单产生时间',
        dataIndex: 'date',
        key: 'date',
    }, {
        title: '扣款时间',
        dataIndex: 'pay_date',
        key: 'pay_date',
    }, {
        title: '扣款结果',
        dataIndex: 'pay_state',
        key: 'pay_state',
    }];
    render() {
        let reconciliation_record_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "收款人姓名",
                    decorator_id: "receive_account_name"
                }, {
                    type: InputType.input,
                    label: "付款人姓名",
                    decorator_id: "pay_account_name"
                }, {
                    type: InputType.input,
                    label: "账单号",
                    decorator_id: "reconciliation_number"
                }
            ],
            columns_data_source: this.columns_data_source,
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_object: AppServiceUtility.financial_account_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
        };
        return (
            <SignFrameLayout {...reconciliation_record_info_list} />
        );
    }
}

/**
 * 控件：银行扣款记录查询列表视图控制器
 * 描述
 */
@addon('FinancialReconciliationRecordView', '银行扣款记录查询列表视图', '描述')
@reactControl(FinancialReconciliationRecordView, true)
export class FinancialReconciliationRecordViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
}