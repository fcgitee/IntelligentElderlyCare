/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 4:52:05 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、查看银行扣款记录
 */
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { Modal, Form, Radio, message, Select } from 'antd';
import { request_func } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
const { Option } = Select;
/**
 * 组件：银行扣款记录查询列表视图状态
 */
export interface UnFinancialReconciliationRecordViewState extends ReactViewState {
    select_ids?: string[];
    select_id?: string;
    show?: boolean;
    select_state?: string;
    condition?: any;
}

/**
 * 组件：银行扣款记录查询列表视图
 * 描述
 */
export class UnFinancialReconciliationRecordView extends ReactView<UnFinancialReconciliationRecordViewControl, UnFinancialReconciliationRecordViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '身份证号',
        dataIndex: 'elder_id_card',
        key: 'elder_id_card',
    },
    {
        title: '收款人',
        dataIndex: 'receiver_name',
        key: 'receiver_name',
    },
    {
        title: '支付方式',
        dataIndex: 'pay_type',
        key: 'pay_type',
    },
    {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: '核算时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '扣款时间',
        dataIndex: 'pay_date',
        key: 'pay_date',
    },
    {
        title: '扣款状态',
        dataIndex: 'pay_status',
        key: 'pay_status',
    }];
    constructor(props: UnFinancialReconciliationRecordViewControl) {
        super(props);
        this.state = {
            select_ids: [],
            show: false,
            select_state: '成功',
            condition: [],
        };
    }
    on_row_selection = (selectedRows: any) => {
        console.info('下载');
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
        console.info(ids);
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({ select_id: contents.id }, () => {
                this.setState({ show: true, });
            });
            // console.log('自定义按钮edit返回值：', contents);
        }
    }
    onCancel = () => {
        this.setState({ show: false });
    }
    onchange = (data: any) => {
        this.setState(data);
    }
    dealwith = () => {
        const select_ids = this.state.select_ids;
        if (select_ids!.length === 0) {
            message.info('请选择要处理的账单');
            return;
        }
        this.setState({ show: true });
    }
    onOk = () => {
        let condition: any = [];
        const select_id = this.state.select_id;
        const select_ids = this.state.select_ids;
        console.info('选择的id', this.state.select_ids);
        if (!select_id && select_ids!.length === 0) {
            message.info('请选择要处理的账单');
            return;
        }
        if (select_id) {
            condition.push({ id: select_id, pay_status: this.state.select_state });
        } else {
            select_ids!.map((val: any) => {
                condition.push({ id: val, pay_status: this.state.select_state });
            });
        }
        request_func(this, AppServiceUtility.financial_account_service.update_unbank_reconciliation, [condition], (data: any) => {
            const mes = data === 'Success' ? '成功' : '失败';
            message.info(mes);
            this.setState({ show: false });
            data === 'Success' && this.props.history!.push(ROUTE_PATH.unfinancialBankReconciliation);
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        const status = ['成功', '失败', '待支付'];
        let reconciliation_record_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                },
                {
                    type: InputType.select,
                    label: "支付状态",
                    decorator_id: "pay_status",
                    option: {
                        placeholder: "请选择状态",
                        showSearch: true,
                        childrens: status!.map((item) => <Option key={item} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date_range"
                }
            ],
            btn_props: [{
                label: '账单批量处理',
                btn_method: this.dealwith,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_row_selection: this.on_row_selection,
            on_icon_click: this.onIconClick,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_object: AppServiceUtility.financial_account_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        // let role_permission_list = Object.assign(role_permission, this.table_params);
        return (
            <div>
                <SignFrameLayout {...reconciliation_record_info_list} />
                {this.state.show ? <Modal
                    title="请选择状态"
                    visible={this.state.show}
                    onOk={this.onOk}
                    onCancel={() => this.onCancel()}
                    okText="修改"
                    cancelText="取消"
                >
                    <Form>
                        <Form.Item>
                            {getFieldDecorator('isSuccess', {
                                initialValue: '成功'
                            })(
                                <Radio.Group onChange={(e: any) => { this.onchange({ select_state: e.target.value }); }}>
                                    <Radio key={1} value='成功' defaultChecked={true} autoFocus={true}>成功</Radio>
                                    <Radio key={0} value='失败'>失败</Radio>
                                </Radio.Group>
                            )}

                        </Form.Item>
                    </Form>
                </Modal> : null}
            </div>
        );
    }
}

/**
 * 控件：银行扣款记录查询列表视图控制器
 * 描述
 */
@addon('UnFinancialReconciliationRecordView', '非银行扣款单列表视图', '描述')
@reactControl(Form.create<any>()(UnFinancialReconciliationRecordView), true)
export class UnFinancialReconciliationRecordViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 编辑 */
    public edit_permission?: string;

}