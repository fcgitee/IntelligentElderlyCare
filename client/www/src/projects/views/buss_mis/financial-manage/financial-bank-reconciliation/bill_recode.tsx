/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 12:03:49 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、账单列表
 */
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { Select, Modal, Form, Checkbox, Button, Icon, Upload, message } from 'antd';
import { request_func, exprot_excel } from "src/business/util_tool";
import { MainCard } from "src/business/components/style-components/main-card";
import XLSX from 'xlsx';
import { ROUTE_PATH } from "src/projects/router";
const { Option } = Select;
/**
 * 组件：账单列表列表视图状态
 */
export interface BillRecodeViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    isAll?: boolean;
    financial_list?: any;
    show?: boolean;
    preview?: boolean;
    preview_parm?: any;
    /** 导出url */
    export_url?: string;
    /** 是否显示上传按钮 */
    upload?: boolean;
    fileList?: any;
    uploading?: boolean;
    file_data?: any;
    data?: any;
    /** 选择导出的参数 */
    export_option?: string[];
}
export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：账单列表列表视图
 * 描述
 */
export class BillRecodeView extends ReactView<BillRecodeViewControl, BillRecodeViewState> {
    private columns_data_source = [{
        title: '账单号',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '身份证号',
        dataIndex: 'elder_id_card',
        key: 'elder_id_card',
    },
    {
        title: '扣款账号',
        dataIndex: 'pay_card_number',
        key: 'pay_card_number',
    },
    {
        title: '扣款人姓名',
        dataIndex: 'pay_card_name',
        key: 'pay_card_name',
    },
    {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: '扣款人证件类型',
        dataIndex: 'payer_id_card_type',
        key: 'payer_id_card_type',
    },
    {
        title: '扣款人证件号码',
        dataIndex: 'payer_id_card',
        key: 'payer_id_card',
    },
    {
        title: '核算时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },
    {
        title: '协议书编号',
        dataIndex: 'deal_book_code',
        key: 'deal_book_code',
    },
    {
        title: '扣款时间',
        dataIndex: 'pay_date',
        key: 'pay_date',
    },
    {
        title: '扣款状态',
        dataIndex: 'pay_status',
        key: 'pay_status',
    }];
    constructor(props: BillRecodeViewControl) {
        super(props);
        this.state = {
            request_url: '',
            export_url: '',
            isAll: false,
            financial_list: [],
            show: false,
            preview: false,
            preview_parm: [],
            upload: false,
            fileList: [],
            uploading: false,
            file_data: '',
            data: {},
            export_option: []
        };
    }
    /** 打印按钮 */
    print = () => {
        this.setState({ upload: true });
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    requesr_finacial(financial_id: any, func?: Function) {
        request_func(this, AppServiceUtility.financial_account_service![this.state.request_url!], [financial_id], (data: any) => {
            func && func(data);
        });
    }
    // 预览
    onOk(isOk: boolean) {
        const { form } = this.props;
        form!.validateFields((err: Error, values: ServiceItemPackageFormValues) => {
            const parm = values['isAll'] === 0 ? [values['financial_id']] : [{}];
            if (!values['isAll'] && !values['financial_id']) {
                message.info('暂无数据可预览');
                return;
            }
            this.setState({ preview: true, preview_parm: parm });
        });
    }
    // 下载
    download() {
        let export_option = this.state.export_option;
        if (!export_option || export_option.length === 0) {
            message.info('请先选择需要导出的数据');
            return;
        }
        request_func(this, AppServiceUtility.financial_account_service.get_bank_unreconciliation_list_manage, [{ 'pay_status_list': export_option }], (data: any) => {
            // console.log(data.result);
            let new_data: any = [];
            data['result'].map((item: any) => {
                new_data.push({
                    "工号": item.id,
                    '帐号': item.pay_card_number,
                    '用户名': item.pay_card_name,
                    "金额": item.amount,
                    "证件类型": null,
                    "证件号码": null,
                    "协议书编号": null,
                });
            });
            exprot_excel([{ name: '银行扣款单', value: new_data }], '银行扣款单', 'xls');
            this.setState({ show: false, export_option: [] });
        });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    // 导出类型选择
    onChangeRadio(value: any) {
        // const isAll = value === 0;
        // this.setState({ isAll });
        // if (isAll) { // 若选择导出账单号，则要获取账单列表，供下拉选择
        //     this.requesr_finacial({ pay_status: '待付款' }, (data: any) => {
        //         if (data.result.length > 0) {
        //             let financial_list: any = [];
        //             data.result.map!((item: any) => { // 账单号去重
        //                 if (item.hasOwnProperty('financial_id') && item.financial_id !== '' && !financial_list.includes(item.financial_id)) {
        //                     financial_list.push(item.financial_id);
        //                 }
        //             });
        //             this.setState({ financial_list });
        //         }
        //     });
        // }
        // console.log('多选框的值>>>>', value);
        this.setState({
            export_option: value
        });
    }
    // 导入
    handleUpload = () => {
        this.setState({
            uploading: true,
            upload: true,
        });
        request_func(this, AppServiceUtility.financial_account_service.upload_excel_band_record, [this.state.file_data], (data: any) => {
            message.info(data);
            if (data === '导入成功') {
                // location.reload();
                this.props.history!.push(ROUTE_PATH.financialBankReconciliationManage);
            }
        });
        this.setState({ fileList: [], uploading: false, upload: false });
    }
    // 查询成功
    onsucess_func(data: any) {
        if (data.result.length) {
            this.setState({ data });
        }
        console.info('查询成功了');
        console.info(data);
    }
    componentWillMount() {
        this.setState({
            request_url: this.props.request_url,
            export_url: this.props.export_url,
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        const status = ['成功', '失败', '待支付'];
        let reconciliation_info_list = {
            type_show: false,
            btn_props: [{
                label: '导入',
                btn_method: this.print,
                icon: 'plus'
            }, {
                label: '导出',
                btn_method: this.export,
                icon: 'download'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                },
                {
                    type: InputType.select,
                    label: "支付状态",
                    decorator_id: "pay_status",
                    option: {
                        placeholder: "请选择状态",
                        showSearch: true,
                        childrens: status!.map((item) => <Option key={item} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date_range"
                }
            ],
            columns_data_source: this.columns_data_source,
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_object: AppServiceUtility.financial_account_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            add_permission: this.props.add_permission,

        };
        // // 导出账单预览配置
        // let json_list = {
        //     type_show: false,
        //     columns_data_source: [{
        //         title: '工号',
        //         dataIndex: '工号',
        //         key: '工号',
        //     }, {
        //         title: '帐号',
        //         dataIndex: '帐号',
        //         key: '帐号',
        //     }, {
        //         title: '用户名',
        //         dataIndex: '用户名',
        //         key: '用户名',
        //     }, {
        //         title: '金额',
        //         dataIndex: '金额',
        //         key: '金额',
        //     }, {
        //         title: '证件类型',
        //         dataIndex: '证件类型',
        //         key: '证件类型',
        //     }, {
        //         title: '证件号码',
        //         dataIndex: '证件号码',
        //         key: '证件号码',
        //     }, {
        //         title: '协议书编号',
        //         dataIndex: '协议书编号',
        //         key: '协议书编号'
        //     }
        //     ],
        //     onsucess_func: (data: any) => { this.onsucess_func(data); },
        //     service_object: AppServiceUtility.financial_account_service,
        //     service_option: {
        //         select: {
        //             service_func: this.state.export_url,
        //             service_condition: this.state.preview_parm
        //         }
        //     },
        // };
        // 导入组件配置
        // const { uploading, fileList } = this.state;
        const props = {
            onRemove: (file: any) => {
                this.setState(state => {
                    const index = state.fileList.indexOf(file);
                    const newFileList = state.fileList.slice();
                    newFileList.splice(index, 1);
                    console.info('newFileList', newFileList);
                    return {
                        fileList: newFileList,
                        uploading: false,
                    };
                });
            },
            beforeUpload: (file: any, fileList: any) => {
                const fileType = file.name.split('.').slice(-1)[0];
                const typeList = ['xls', 'xlsx'];
                console.info('文件格式：：', file.name.split('.'), fileType);
                if (!typeList.includes(fileType)) {
                    message.info('文件格式错误，请导入xls或xlsx格式的文件');
                    return false;
                }
                const _this = this;
                var files = file;
                var fileReader = new FileReader();
                fileReader.onload = function (ev: any) {
                    let file_data: any = [];
                    try {
                        var data = ev!.target.result,
                            workbook = XLSX.read(data, {
                                type: 'binary'
                            }); // 以二进制流方式读取得到整份excel表格对象
                        file_data = []; // 存储获取到的数据
                    } catch (e) {
                        // console.log('文件类型不正确');
                        return;
                    }

                    // 表格的表格范围，可用于判断表头是否数量是否正确
                    // var fromTo: any = '';
                    // 遍历每张表读取
                    for (var sheet in workbook.Sheets) {
                        if (workbook.Sheets.hasOwnProperty(sheet)) {
                            // fromTo = workbook.Sheets[sheet]['!ref'];
                            // console.log('文件内容', fromTo);
                            file_data = file_data.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
                            break; // 如果只取第一张表，就取消注释这行
                        }
                    }

                    _this.setState({ file_data });
                };

                // 以二进制方式打开文件
                fileReader.readAsBinaryString(files);
                this.setState(state => ({
                    fileList: [...state.fileList, file],
                }));
                return false;
            },
            fileList: this.state.fileList,
        };
        return (
            <div>
                <SignFrameLayout {...reconciliation_info_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                    initialValue: [1]
                                })(
                                    <Checkbox.Group onChange={(e: any) => { this.onChangeRadio(e); }}>
                                        <Checkbox key={0} value={'待支付'}>待支付</Checkbox>
                                        <Checkbox key={1} value={'成功'}>扣款成功账单</Checkbox>
                                        <Checkbox key={2} value={'失败'}>扣款失败账单</Checkbox>
                                    </Checkbox.Group>
                                )}
                            </Form.Item>
                            {
                                this.state.isAll ? (
                                    <Form.Item>
                                        {getFieldDecorator('financial_id', {
                                            initialValue: this.state.financial_list ? this.state.financial_list[0] : ''
                                        })(
                                            <Select>
                                                {
                                                    this.state.financial_list!.map((provinceItem: any, index: any) => {
                                                        return <Option key={provinceItem}>{provinceItem}</Option>;
                                                    })
                                                }
                                            </Select>
                                        )}
                                    </Form.Item>
                                ) : null
                            }
                        </Form>
                    </MainCard >
                </Modal >
                {/* <Modal
                    title="预览"
                    visible={this.state.preview}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ preview: !this.state.preview })}
                    okText="下载"
                    width={1000}
                    cancelText="取消"
                >
                    {this.state.preview ? <SignFrameLayout {...json_list} /> : null}
                </Modal> */}
                <Modal
                    title="导入银行回款单"
                    visible={this.state.upload}
                    footer={null}
                    onCancel={() => { this.onCancel({ upload: !this.state.upload }); }}
                >
                    {this.state.upload ? (<div>
                        <Upload {...props}>
                            <Button disabled={this.state.fileList.length === 1}>
                                <Icon type="upload" /> {'选择文件'}
                            </Button>
                        </Upload>
                        <Button
                            type="primary"
                            onClick={this.handleUpload}
                            disabled={this.state.fileList.length === 0}
                            loading={this.state.uploading}
                            style={{ marginTop: 16 }}
                        >
                            {this.state.uploading ? '上传中...' : '上传'}
                        </Button>
                    </div>) : null}
                </Modal></div >
        );
    }
}

/**
 * 控件：账单列表视图控制器
 * 描述
 */
@addon('BillRecodeView', '账单列表视图', '描述')
@reactControl(Form.create<any>()(BillRecodeView), true)
export class BillRecodeViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 导出权限 */
    public export_url?: string;

}