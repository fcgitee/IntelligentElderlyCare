/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 3:56:09 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、查看收费记录
 */
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { Select } from 'antd';
const { Option } = Select;

/**
 * 组件：收费记录查询列表视图状态
 */
export interface FinancialChargeRecordViewState extends ReactViewState {
}

/**
 * 组件：收费记录查询列表视图
 * 描述
 */
export class FinancialChargeRecordView extends ReactView<FinancialChargeRecordViewControl, FinancialChargeRecordViewState> {
    private columns_data_source = [
        {
            title: '账单号',
            dataIndex: 'order_id',
            key: 'order_id',
        },
        {
            title: '长者姓名',
            dataIndex: 'elder_name',
            key: 'elder_name',
        },
        {
            title: '身份证',
            dataIndex: 'elder_id_card',
            key: 'elder_id_card',
        },
        {
            title: '扣款账号',
            dataIndex: 'pay_card_number',
            key: 'pay_card_number',
        },
        {
            title: '扣款人姓名',
            dataIndex: 'pay_card_name',
            key: 'pay_card_name',
        },
        {
            title: '收款人',
            dataIndex: 'receiver_name',
            key: 'receiver_name',
        },
        {
            title: '支付方式',
            dataIndex: 'pay_type',
            key: 'pay_type',
        },
        {
            title: '金额/元',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: '核算时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },
        {
            title: '协议书编号',
            dataIndex: 'deal_book_code',
            key: 'deal_book_code',
        },
        {
            title: '扣款时间',
            dataIndex: 'pay_date',
            key: 'pay_date',
        },
        {
            title: '扣款状态',
            dataIndex: 'pay_status',
            key: 'pay_status',
        }];
    on_icon_click = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            // this.props.history!.push(ROUTE_PATH.changeAssessmentProject + '/' + contents.id);
        }
    }
    render() {
        const status = ['成功', '失败', '待付款'];
        const paystate = ['全部', '银行划扣', '其他']; //  'APP储值账户划扣', '机构储值账户划扣', '补贴账户划扣', '慈善账户划扣', '微信支付', '支付宝支付', 
        let charge_record_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                },
                // {
                //     type: InputType.input,
                //     label: "收款账户姓名",
                //     decorator_id: "receiver_name"
                // },
                {
                    type: InputType.select,
                    label: "支付状态",
                    decorator_id: "pay_status",
                    option: {
                        placeholder: "请选择支付状态",
                        showSearch: true,
                        childrens: status!.map((item) => <Option key={item} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.select,
                    label: '支付方式',
                    decorator_id: "pay_type",
                    option: {
                        placeholder: "请选择支付方式",
                        showSearch: true,
                        childrens: paystate!.map((item) => <Option key={item} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date_range"
                }
            ],
            row_btn_props: {
                style: {
                    justifyContent: "center"
                }
            },
            columns_data_source: this.columns_data_source,
            on_icon_click: this.on_icon_click,
            service_object: AppServiceUtility.financial_account_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
        };
        return (
            <SignFrameLayout {...charge_record_info_list} />
        );
    }
}

/**
 * 控件：收费记录查询列表视图控制器
 * 描述
 */
@addon('FinancialChargeRecordView', '收费记录查询列表视图', '描述')
@reactControl(FinancialChargeRecordView, true)
export class FinancialChargeRecordViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
}
