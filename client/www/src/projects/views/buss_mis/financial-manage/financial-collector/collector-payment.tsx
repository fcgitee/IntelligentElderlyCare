/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Wednesday August 14th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Wednesday, 14th August 2019 3:30:37 pm
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、收费员退款页面
 */
import { Button, Card, Col, Form, Icon, Row, Select, Steps, Table } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { AppServiceUtility } from "src/projects/app/appService";
const { Step } = Steps;

/**
 * 组件：扫描身份信息
 */
export interface ChangeCollectorPaymentViewState extends ReactViewState {
    /** 步骤 */
    current?: number;
    /** 表单值集合 */
    form_value_list?: {};
    /** 支付方式 */
    pay_type?: string;
    /** 付款详情列表 */
    items?: any[];
}
/**
 * 组件：扫描身份信息
 * 描述
 */
export class ChangeCollectorPaymentView extends ReactView<ChangeCollectorPaymentViewControl, ChangeCollectorPaymentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            current: 0,
            form_value_list: {},
            pay_type: '',
        };
    }
    infoSubmit = (err: any, values: any) => {
        AppServiceUtility.financial_account_service.get_need_pay_account_list!(values)!
            .then((data: any) => {
                console.info(data);
                let current = this.state.current! + 1;
                this.setState({
                    current,
                    items: data
                });
            })
            .catch((err) => {
                console.info(err);
            });
    }
    /** 上一步 */
    prev = () => {
        let current = this.state.current! - 1;
        this.setState({ current });
    }
    /** 退款 */
    pay = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (!err) {
                let data = {};
                let items_id: any[] = [];
                this.state.items!.map((key, value) => {
                    if (key.id) {
                        items_id.push(key.id);
                    }
                });
                data['items'] = items_id;
                data['pay_type'] = values['type'];
                AppServiceUtility.financial_account_service.complete_pay_account!(data)!
                    .then((data: any) => {
                        console.info(data);
                        let current = this.state.current! + 1;
                        this.setState({
                            current,
                            // items: data
                        });
                    })
                    .catch((err) => {
                        console.info(err);
                    });

            }
        });
    }
    /** 完成按钮 */
    compelet = () => {

        console.info(this.state.items);
        console.info(this.state.pay_type);
    }
    render() {
        // 通用信息配置
        const layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        let identity_info = {
            submit_btn_propps: {
                text: "下一步",
                cb: this.infoSubmit
            },
            form_items_props: [
                {
                    title: "长者信息",
                    need_card: true,
                    side_props: {
                        col_span: 8,
                        // childrens: <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" />
                    },
                    card_btn: [
                        {
                            text: "读取身份证",
                            cb: () => {
                                // 调用身份证读卡器接口
                                alert("读取信息成功！");
                            }
                        }
                        // , {
                        //     text: "扫描身份证",
                        //     cb: () => {
                        //         // ？功能待定
                        //     }
                        // }
                    ],
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "长者姓名",
                                        decorator_id: "name",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入长者姓名" }],
                                            initialValue: '',
                                        } as GetFieldDecoratorOptions,
                                        layout: layout,
                                        option: {
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "身份证号码",
                                        decorator_id: "id_card",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入身份证号码" }],
                                            initialValue: ''
                                        } as GetFieldDecoratorOptions,
                                        layout: layout,
                                        option: {
                                        }
                                    }
                                ]
                            }
                        },
                    ]
                },
            ],
            service_option: {
                service_object: '',
                operation_option: {
                    query: {
                        func_name: "get_service_item_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_item"
                    }
                },
            },
        };
        // 表格配置
        const columns = [
            {
                title: '项目',
                dataIndex: 'item',
                key: 'item',
            },
            {
                title: '金额',
                dataIndex: 'amount',
                key: 'amount',
            },
        ];
        // 预约成功
        let success = (
            <Row type='flex' justify='center'>
                <Col span={8}>
                    <Row type='flex' justify='center'>
                        <Icon type="check-circle" style={{ fontSize: 200 }} theme="twoTone" twoToneColor="#52c41a" />
                    </Row>
                    {/* <Row type='flex' justify='center'>
                        <Button>打印小票</Button>
                    </Row> */}
                </Col>
            </Row>
        );
        let steps = [
            {
                title: '扫描身份信息',
                content: (<FormCreator {...identity_info} />)
            },
            {
                title: '账单明细信息',
                content: (
                    <Card>
                        <Table pagination={false} dataSource={this.state.items} key='id' columns={columns} />
                        <Form onSubmit={this.pay}>
                            <Form.Item label='选择退款方式'>
                                {getFieldDecorator('type', {
                                    initialValue: '',
                                    rules: [{
                                        required: true,
                                        message: '选择退款方式'
                                    }],
                                })(
                                    <Select>
                                        <Select.Option value='现金'>现金</Select.Option>
                                        <Select.Option value='转账'>转账</Select.Option>
                                    </Select>
                                )}
                            </Form.Item>
                        </Form>
                    </Card>
                )
            },
            {
                title: '退款成功',
                content: success
            },
        ];
        let { current } = this.state;
        return (
            <MainContent>
                <MainCard title='收费员退款'>
                    <Steps current={current}>
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>
                    <MainContent />
                    {steps[current!].content}
                    <Row type="flex" justify='center'>
                        {current !== 0 && current! < steps.length - 1 && (
                            current === 1 ?
                                <Button onClick={this.pay}>
                                    下一步
                                </Button> :
                                ''
                        )}
                        {current === steps.length - 1 && (
                            <Button type="primary" onClick={this.compelet}>
                                完成
                            </Button>
                        )}
                        {current !== 0 && current! < steps.length - 1 ?
                            <Button onClick={this.prev}>
                                上一步
                        </Button>
                            :
                            ''
                        }
                    </Row>
                </MainCard>
            </MainContent>
        );
    }
}

@addon('ChangeCollectorPaymentView', '收费员付款', '描述')
@reactControl(Form.create<any>()(ChangeCollectorPaymentView), true)
export class ChangeCollectorPaymentViewControl extends ReactViewControl {
}
