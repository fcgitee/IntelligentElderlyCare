import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
/**
 * 组件：服务商结算状态
 */
export interface ServiceProviderSettlementViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：服务商结算
 * 描述
 */
export class ServiceProviderSettlementView extends ReactView<ServiceProviderSettlementsViewControl, ServiceProviderSettlementViewState> {
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        const columns: any = [
            {
                title: '服务项目（销售收入）',
                align: 'center',
                children: [
                    {
                        title: '服务项目名称',
                        dataIndex: 'name',
                        key: 'name',
                        align: 'center'

                    },
                    {
                        title: '产品上架单价',
                        dataIndex: 'price',
                        key: 'price',
                        align: 'center'

                    }, {
                        title: '折扣或优惠',
                        dataIndex: 'discount',
                        key: 'discount',
                        align: 'center'

                    }, {
                        title: '付款渠道',
                        align: 'center',
                        children: [
                            {
                                title: '补贴账户支付',
                                dataIndex: 'subsidy_account',
                                key: 'subsidy_account',
                                align: 'center'

                            }, {
                                title: '第三方账户支付',
                                dataIndex: 'the_third_party',
                                key: 'the_third_party',
                                align: 'center'

                            }, {
                                title: '现金支付',
                                dataIndex: 'cash',
                                key: 'cash',
                                align: 'center'
                            }, {
                                title: '订单价格',
                                dataIndex: 'order_price',
                                key: 'order_price',
                                align: 'center'
                            }, {
                                title: '订单编号',
                                dataIndex: 'order_number',
                                key: 'order_number',
                                align: 'center'
                            },
                        ]

                    }, {
                        title: '实际成交数量',
                        dataIndex: 'deal_number',
                        key: 'deal_number',
                        align: 'center'
                    }, {
                        title: '合计金额',
                        dataIndex: 'total_sum',
                        key: 'total_sum',
                        align: 'center'
                    },
                ]
            },
            {
                title: '资金流向',
                align: 'center',
                children: [
                    {
                        title: '慈善捐款',
                        dataIndex: 'charitable_donations',
                        key: 'charitable_donations',
                        align: 'center'

                    },
                    {
                        title: '服务商',
                        dataIndex: 'service_provider',
                        key: 'service_provider',
                        align: 'center'

                    },
                    {
                        title: '服务人员',
                        dataIndex: 'service_personal',
                        key: 'service_personal',
                        align: 'center'

                    }, {
                        title: '结算时间',
                        dataIndex: 'date',
                        key: 'date',
                        align: 'center'

                    },
                ]

            },
        ];

        const data = [
            {
                key: "1",
                name: '助餐服务',
                'price': '12',
                'discount': "免配送费",

                'subsidy_account': "",

                'the_third_party': '12',

                'cash': "",

                'order_price': "12",

                'order_number': "",

                'deal_number': "3",

                'total_sum': "36",

                'management_cost': "0.36",

                'promotion_cost': "",

                'financial_cost': "",

                'aggregate_cost': "",

                'platform_profit': "",

                'charitable_donations': "",

                'service_provider': "",

                'service_personal': "",

                'other': "",
                'date': '2019-10-08 19:34:45',
            }
        ];

        return (
            <MainContent>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：服务商结算
 * 描述
 */
@addon('ServiceProviderSettlementView', '服务商结算', '描述')
@reactControl(ServiceProviderSettlementView, true)
export class ServiceProviderSettlementsViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}