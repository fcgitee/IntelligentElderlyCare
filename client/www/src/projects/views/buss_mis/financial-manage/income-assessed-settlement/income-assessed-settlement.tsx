import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { Select } from 'antd';
const { Option } = Select;

/**
 * 组件：收入分摊报表
 */
export interface incomeAssessedSettlementViewState extends ReactViewState {

}

/**
 * 组件：收入分摊报表
 */
export class incomeAssessedSettlementView extends ReactView<incomeAssessedSettlementViewControl, incomeAssessedSettlementViewState> {
    private columns_data_source = [{
        title: '组织机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    }, {
        title: '是否分摊',
        dataIndex: 'is_distributed',
        key: 'is_distributed',
    }, {
        title: '分摊比例',
        dataIndex: 'divide_percent',
        key: 'divide_percent',
    }, {
        title: '订单金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '摘要',
        dataIndex: 'abstract',
        key: 'abstract',
    }, {
        title: '日期',
        dataIndex: 'date',
        key: 'date',
    }];
    constructor(props: incomeAssessedSettlementViewControl) {
        super(props);
        this.state = {
        };
    }

    render() {
        let income_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "是否分摊",
                    decorator_id: "is_distributed",
                    option: {
                        placeholder: "请输入组织机构类型",
                        childrens: [<Option key='true' value='true'>是</Option>, <Option key='false' value='false'>否</Option>],
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "日期",
                    decorator_id: "date_range"
                }, {
                    type: InputType.input,
                    label: "组织机构",
                    decorator_id: "organization_name"
                }
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: 'get_divide_record_list',
                    service_condition: [this.props.select_param || {}]
                },
            },
            searchExtraParam: this.props.select_param || {},
        };
        let account_book_list = Object.assign(income_list, table_param);
        return (
            <SignFrameLayout {...account_book_list} />
        );
    }
}

@addon('incomeAssessedSettlementView', '收入分摊列表视图', '描述')
@reactControl(incomeAssessedSettlementView, true)
export class incomeAssessedSettlementViewControl extends ReactViewControl {
    public select_param?: any;
}