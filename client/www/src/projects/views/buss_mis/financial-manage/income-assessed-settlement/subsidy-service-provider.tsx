import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";

/**
 * 组件：服务商补贴收入报表
 */
export interface subsidyServiceProviderViewState extends ReactViewState {

}

/**
 * 组件：服务商补贴收入报表
 */
export class subsidyServiceProviderView extends ReactView<subsidyServiceProviderViewControl, subsidyServiceProviderViewState> {
    private columns_data_source = [{
        title: '组织机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    }, {
        title: '数量',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '日期',
        dataIndex: 'date',
        key: 'date',
    }, {
        title: '摘要',
        dataIndex: 'abstract',
        key: 'abstract',
    }];
    constructor(props: subsidyServiceProviderViewControl) {
        super(props);
        this.state = {
        };
    }

    render() {
        let income_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "组织机构",
                    decorator_id: "organization_name",
                }, {
                    type: InputType.rangePicker,
                    label: "服务时间",
                    decorator_id: "date_range"
                }, 
            ],
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: 'get_subsidy_service_provider_list',
                    service_condition: [{}]
                },
            },
        };
        let account_book_list = Object.assign(income_list, table_param);
        return (
            <SignFrameLayout {...account_book_list} />
        );
    }
}

@addon('subsidyServiceProviderView', '服务商补贴收入列表视图', '描述')
@reactControl(subsidyServiceProviderView, true)
export class subsidyServiceProviderViewControl extends ReactViewControl {

}