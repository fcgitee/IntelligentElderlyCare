/*
 * 版权：Copyright (c) 2019 China
 *
 * 创建日期：Tuesday July 30th 2019
 * 创建者：ymq(ymq) - <<email>>
 *
 * 修改日期: Tuesday, 30th July 2019 11:12:24 am
 * 修改者: ymq(ymq) - <<email>>
 *
 * 说明
 *  1、科目管理列表查询
 */
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：科目管理列表查询视图状态
 */
export interface FinancialSubjectViewState extends ReactViewState {
    subject_id?: string;
}
/**
 * 组件：科目管理列表查询视图
 * 描述
 */
export class FinancialSubjectView extends ReactView<FinancialSubjectViewControl, FinancialSubjectViewState> {
    private columns_data_source = [{
        title: '科目编码',
        dataIndex: 'subject_code',
        key: 'subject_code',
    }, {
        title: '科目名称',
        dataIndex: 'subject_name',
        key: 'subject_name',
    }, {
        title: '科目类别',
        dataIndex: 'subject_type',
        key: 'subject_type',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }, {
        title: '上级科目',
        dataIndex: 'super_subject',
        key: 'super_subject',
    }, {
        title: '借/贷',
        dataIndex: 'borrow_loan',
        key: 'borrow_loan',
    }, {
        title: '科目余额',
        dataIndex: 'balance',
        key: 'balance',
    }];

    constructor(props: FinancialSubjectViewControl) {
        super(props);
        this.state = {
        };
    }

    /** 新增科目按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeFinancialSubject);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeFinancialSubject + '/' + contents.id);
        }
    }

    componentDidMount() {
        const subject_id = this.props.match!.params.key;
        // console.log(subject_id);
        this.setState({ subject_id: subject_id });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        let subject_info_list = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "科目类型",
                    decorator_id: "subject_type",
                }, {
                    type: InputType.input,
                    label: "科目名称",
                    decorator_id: "subject_name"
                },
            ], btn_props: [{
                label: '新增科目',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: 'get_subject_list',
                    service_condition: [{ 'subject_id': this.props.match!.params.key }]
                },
                delete: {
                    service_func: 'del_subject_data'
                }
            },
        };
        let subject_list = Object.assign(subject_info_list, table_param);
        return (
            <SignFrameLayout {...subject_list} />
        );
    }
}

/**
 * 控件：账户管理列表视图控制器
 * 描述
 */
@addon('FinancialSubjectView', '账户管理列表视图', '描述')
@reactControl(FinancialSubjectView, true)
export class FinancialSubjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}