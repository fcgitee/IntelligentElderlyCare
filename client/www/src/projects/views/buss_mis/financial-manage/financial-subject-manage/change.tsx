/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Tuesday July 30th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Tuesday, 30th July 2019 11:12:33 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、新增/编辑会计科目
 */
import { Select } from 'antd';
import { OptionProps } from "antd/lib/select";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;

/**
 * 组件：编辑记账凭证状态
 */
export interface ChangeAccountVoucherViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** input下拉框数据源 */
    dataSource?: any[];
}
/**
 * 组件：凭证记录视图
 */
export class ChangeAccountVoucherView extends ReactView<ChangeFinancialSubjectViewControl, ChangeAccountVoucherViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            dataSource: []
        };
    }

    componentDidMount() {
        // 获取会计科目
        request(this, AppServiceUtility.financial_service.input_subject_list!({}))
            .then((dataSource: any) => {
                this.setState({ dataSource });
            });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let columns_data_source = [{
            title: '下级科目名称',
            dataIndex: 'subject_name',
            key: 'subject_name',
            type: 'input' as ColTypeStr
        }, {
            title: '下级科目编码',
            dataIndex: 'subject_code',
            key: 'subject_code',
            type: 'input' as ColTypeStr,
            // }, {
            //     title: '下级科目id',
            //     dataIndex: 'id',
            //     key: 'id',
            //     type: 'input' as ColTypeStr,
        }];
        let edit_props = {
            form_items_props: [{
                type: InputTypeTable.input,
                label: "科目类别",
                decorator_id: "subject_type",
            }, {
                type: InputTypeTable.input,
                label: "科目编码",
                decorator_id: "subject_code",
            }, {
                type: InputTypeTable.input,
                label: "科目名称",
                decorator_id: "subject_name",
            }, {
                type: InputTypeTable.select,
                label: "上级科目",
                decorator_id: "super_subject_id",
                option: {
                    placeholder: "请选择上级科目",
                    showSearch: true,
                    childrens: this.state.dataSource!.map((item: any) => <Option key={item.key} value={item.key}>{item.code} {item.text}</Option>),
                    filterOption: (inputValue: string, option: React.ReactElement<OptionProps>) =>
                        (option.props.children! as string).indexOf(inputValue) !== -1 ||
                        (option.key as string).indexOf(inputValue) !== -1
                }
            }, {
                type: InputTypeTable.input,
                label: "借/贷",
                decorator_id: "borrow_subject",
            }, {
                type: InputTypeTable.input,
                label: "科目余额",
                decorator_id: "balance",
            }, {
                type: InputTypeTable.input,
                label: "备注",
                decorator_id: "remark",
            },
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.financialSubjectManage);
                    }
                }
            ],
            submit_props: {
                text: "保存",
                // cb: () => {
                //     this.props.history!.push(ROUTE_PATH.financialSubjectManage);
                // }
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            columns_data_source: columns_data_source,
            add_row_text: "新增下级会计科目",
            table_field_name: 'add_sub_data',
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    query: {
                        func_name: "get_subject_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_subject_data"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.financialSubjectManage); },
            id: this.props.match!.params.key,
        };
        return (
            <TableList {...edit_props} />
        );
    }
}
/**
 * 控件：凭证记录编辑控件
 * @description 凭证记录编辑控件
 * @author
 */
@addon('ChangeFinancialSubjectViewControl', '凭证记录控件', '凭证记录控件')
@reactControl(ChangeAccountVoucherView, true)
export class ChangeFinancialSubjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}