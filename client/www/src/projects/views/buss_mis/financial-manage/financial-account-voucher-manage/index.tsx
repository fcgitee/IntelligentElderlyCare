/*
 * 版权：Copyright (c) 2019 China
 * 
 * 创建日期：Saturday July 27th 2019
 * 创建者：ymq(ymq) - <<email>>
 * 
 * 修改日期: Saturday, 27th July 2019 10:34:10 am
 * 修改者: ymq(ymq) - <<email>>
 * 
 * 说明
 *  1、分类明细账查询
 */
import { Modal, Row } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：分类明细账查询视图状态
 */
export interface FinancialVoucherViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 交易清单集合 */
    service_list?: any[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 交易评价分数 */
    total?: number;
    /** 标题 */
    title?: string;
    /** 表格列信息 */
    columns_service_item?: any[];
}

/**
 * 组件：分类明细账
 * 描述
 */
export class FinancialVoucherView extends ReactView<FinancialVoucherViewControl, FinancialVoucherViewState> {
    private columns_data_source = [{
        title: '时间',
        dataIndex: 'date',
        key: 'date',
    }, {
        title: '摘要',
        dataIndex: 'abstract',
        key: 'abstract',
    }, {
        title: '凭证类型',
        dataIndex: 'voucher_type',
        key: 'voucher_type',
    }, {
        title: '凭证编号',
        dataIndex: 'voucher_id',
        key: 'voucher_id',
    }];
    private comment_columns = [{
        title: '时间',
        dataIndex: 'date',
        key: 'date',
    }, {
        title: '凭证编号',
        dataIndex: 'voucher_id',
        key: 'voucher_id',
    }, {
        title: '分录摘要',
        dataIndex: 'entry_abstract',
        key: 'entry_abstract',
    }, {
        title: '分录科目',
        dataIndex: 'entry_subject',
        key: 'entry_subject',
    }, {
        title: '借/贷',
        dataIndex: 'borrow_loan',
        key: 'borrow_loan',
    }, {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    }, {
        title: '红冲凭证',
        dataIndex: 'return_id',
        key: 'return_id',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 新增记账凭证 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeFinancialVoucher);
    }
    /** 查看凭证 */
    checkVoucher = () => {
        let ids = this.state.select_ids;
        if (ids && ids!.length > 0) {
            let columns_voucher_item = this.comment_columns;
            request(this, AppServiceUtility.financial_service.query_account_voucher_list!({ 'ids': ids }))
                .then((data: any) => {
                    this.setState({
                        service_list: data.result,
                        total: data.total,
                        columns_service_item: columns_voucher_item,
                        modal_visible: true
                    });
                });
        }
    }

    /** 自定义图标按钮回调事件-红冲 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeFinancialVoucher + '/' + contents.id);
        }
    }

    /** 模版选择框取消回调方法 */
    handleCancel = () => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    render() {
        // const redeirect = this.props.isPermission ? this.props.isPermission!(this.props.permission!) : undefined;
        // if (redeirect) {
        //     return redeirect;
        // }
        let financial_voucher = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "时间范围",
                    decorator_id: "date_range"
                }, {
                    type: InputType.input,
                    label: "摘要",
                    decorator_id: "abstract"
                }
            ],
            btn_props: [{
                label: '新增记账凭证',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '查看凭证',
                btn_method: this.checkVoucher,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            on_icon_click: this.onIconClick,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.financial_service,
            service_option: {
                select: {
                    service_func: 'get_voucher_list',
                    service_condition: []
                }
            },
        };
        let voucher_list = Object.assign(financial_voucher, table_param);
        return (
            (
                <Row>
                    <Modal
                        title={this.state.title}
                        visible={this.state.modal_visible}
                        onCancel={this.handleCancel}
                        width={900}
                    >
                        <NTOperationTable
                            data_source={this.state.service_list}
                            columns_data_source={this.state.columns_service_item}
                            table_size='middle'
                            showHeader={true}
                            bordered={true}
                            total={this.state.total}
                            default_page_size={10}
                            total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                            show_footer={true}
                        />
                    </Modal>
                    <SignFrameLayout {...voucher_list} />
                </Row>
            )
        );
    }
}

/**
 * 控件：分类明细账列表
 * 描述
 */
@addon('FinanciallVoucherView', '分类明细账', '描述')
@reactControl(FinancialVoucherView, true)
export class FinancialVoucherViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}