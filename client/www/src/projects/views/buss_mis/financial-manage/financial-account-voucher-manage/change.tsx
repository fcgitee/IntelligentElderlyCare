/*
* 版权：Copyright (c) 2019 China
*
* 创建日期：Saturday July 27th 2019
* 创建者：ymq(ymq) - <<email>>
*
* 修改日期: Saturday, 27th July 2019 10:34:20 am
* 修改者: ymq(ymq) - <<email>>
*
* 说明
* 1、新增/红冲记账凭证
*/
import { message, Select } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { request, request_func } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;

/**
 * 组件：编辑记账凭证状态
 */
export interface ChangeAccountVoucherViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** input下拉框数据源 */
    dataSource?: any[];
}
/**
 * 组件：凭证记录视图
 */
export class ChangeAccountVoucherView extends ReactView<ChangeAccountVoucherViewControl, ChangeAccountVoucherViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            dataSource: []
        };
    }
    /** 红冲记账凭证 */
    return_voucher = (value?: {}) => {
        // console.log(value);
        request(this, AppServiceUtility.financial_service.return_account_voucher!(value))
            .then((data: any) => {
                if (data === 'Success') {
                    this.props.history!.push(ROUTE_PATH.financialAccountVoucherManage);
                } else {
                    message.info('红冲失败');
                }
            });
    }

    componentDidMount() {
        // 获取会计科目
        request_func(this, AppServiceUtility.financial_service.input_subject_list, [{}], (data: any) => {
            // console.log("data??????", data);
            let dataSource: any[] = [];
            const data_list: any[] = data;
            // console.log(data_list);
            data_list!.map((item) => {
                dataSource.push(<Option key={item['key']}>{item['text']}</Option>);
            });
            // date字段格式化过为moment可用
            console.warn("-----------------------------------", dataSource);

            this.setState({
                dataSource
            });
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        const select_option = [{
            name: '借'
        }, {
            name: "贷"
        }];

        const options = select_option.map((e, i) =>
            <Option key={e.name}>{e.name}</Option>
        );

        if (redeirect) {
            return redeirect;
        }
        let columns_data_source = [{
            title: '分录摘要',
            dataIndex: 'entry_abstract',
            key: 'entry_abstract',
            type: 'input' as ColTypeStr
        }, {
            title: '分录科目',
            dataIndex: 'entry_subject',
            key: 'entry_subject',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择会计科目",
                childrens: this.state.dataSource
            }
        }, {
            title: '借/贷',
            dataIndex: 'borrow_loan',
            key: 'borrow_loan',
            type: 'select' as ColTypeStr,
            option: {
                childrens: options
            }
        }, {
            title: '金额',
            dataIndex: 'amount',
            key: 'amount',
            type: 'input' as ColTypeStr
        }];
        let edit_props = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "时间",
                    decorator_id: "date",
                    option: {
                        disabled: this.props.match!.params.key ? true : false,
                        // defaultValue: moment('2015/01/01')
                    }
                },
                {
                    type: InputTypeTable.input,
                    label: "摘要",
                    decorator_id: "abstract",
                    option: {
                        disabled: this.props.match!.params.key ? true : false
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "凭证类型",
                    decorator_id: "voucher_type",
                    option: {
                        disabled: this.props.match!.params.key ? true : false
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "凭证编号",
                    decorator_id: "voucher_id",
                    option: {
                        disabled: this.props.match!.params.key ? true : false
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "原始凭证",
                    decorator_id: "init_voucher",
                    option: {
                        disabled: this.props.match!.params.key ? true : false
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "红冲原因",
                    decorator_id: "red_reason",
                    option: {
                        disabled: this.props.match!.params.key ? false : true
                    }
                },
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.financialAccountVoucherManage);
                    }
                },
                {
                    text: "红冲",
                    btn_other_props: {
                        disabled: this.props.match!.params.key ? false : true
                    },
                    cb: this.return_voucher
                }
            ],
            submit_props: {
                text: "保存"
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            columns_data_source: columns_data_source,
            add_row_text: "新增",
            table_field_name: 'add_sub_data',
            service_option: {
                service_object: AppServiceUtility.financial_service,
                operation_option: {
                    query: {
                        func_name: "get_voucher_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "add_account_voucher"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.financialAccountVoucherManage); },
            id: this.props.match!.params.key,
        };
        return (
            <TableList {...edit_props} />
        );
    }
}
/**
 * 控件：凭证记录编辑控件
 * @description 凭证记录编辑控件
 * @author
 */
@addon('ChangeAccountVoucherViewControl', '凭证记录控件', '凭证记录控件')
@reactControl(ChangeAccountVoucherView, true)
export class ChangeAccountVoucherViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}