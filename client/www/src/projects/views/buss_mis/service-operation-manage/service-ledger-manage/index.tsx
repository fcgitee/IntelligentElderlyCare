import { Select } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

let { Option } = Select;

/**
 * 组件：台账列表页面状态
 */
export interface ServiceLedgerViewState extends ReactViewState {
    /** 用户列表 */
    user_list?: [];
    /** 选择的行Id */
    select_ids?: string[];
}

/**
 * 组件：台账列表页面
 * 描述
 */
export class ServiceLedgerView extends ReactView<ServiceLedgerViewControl, ServiceLedgerViewState> {
    private columns_data_source = [{
        title: '人员名称',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '日期',
        dataIndex: 'service_end_date',
        key: 'service_end_date',
    }, {
        title: '总金额',
        dataIndex: 'total_amount',
        key: 'total_amount'
    }];
    constructor(props: ServiceLedgerViewControl) {
        super(props);
        this.state = {
            user_list: []
        };
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeServiceLedger + '/' + contents.id);
        }
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }
    componentDidMount() {
        request(this, AppServiceUtility.user_service.get_user_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    user_list: datas.result,
                });
            });
    }
    /** 发送人员台账到财务系统 */
    send_ledger = () => {

    }
    render() {
        let user = this.state.user_list;
        let user_list: any[] = [];
        user!.map((user, idx) => {
            user_list.push(<Option key={user['user_id']}>{user['name']}</Option>);
        });
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.select,
                label: "人员名称",
                decorator_id: "user_id",
                option: {
                    placeholder: "请选择人员",
                    childrens: user_list,
                }
            }],
            btn_props: [{
                label: '发送扣费记录',
                btn_method: this.send_ledger,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_row_selection: this.on_row_selection,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_ledger_service,
            service_option: {
                select: {
                    service_func: 'get_service_ledger_list',
                    service_condition: []
                },
                delete: {
                    service_func: 'delete_services_ledger'
                }
            },
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：台账列表页面
 */
@addon('ServiceLedgerView', '台账列表页面', '描述')
@reactControl(ServiceLedgerView, true)
export class ServiceLedgerViewControl extends ReactViewControl {
} 