import { message, Select, Modal } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { request, exprot_excel } from "src/business/util_tool";

let { Option } = Select;
/**
 * 组件：服务记录信息状态
 */
export interface ServiceRecordViewState extends ReactViewState {
    /** 服务订单列表 */
    // service_order_list?: [];
    /** 服务项目列表 */
    service_project_list?: [];
    /** 服务人员列表 */
    service_worker_list?: [];
    /** 选择行Ids */
    select_ids?: string[];
    /** 接口名 */
    request_ulr?: string;
    /** 服务商列表 */
    org_list: any[];
    /** 社工局数组 */
    social_worker_list?: any[];
    /** 对话框 */
    model_show?: boolean;
    /** 操作状态 */
    is_send?: boolean;
    // 多选项目行
    item_list?: any[];
    // 按钮
    permission?: any;
}

/**
 * 组件：服务记录信息
 * 描述
 */
export class ServiceRecordView extends ReactView<ServiceRecordViewControl, ServiceRecordViewState> {
    private formCreator: any = null;
    private columns_data_source = [{
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
    }, {
        title: '服务订单编号',
        dataIndex: 'service_order_code',
        key: 'service_order_code',
    }, {
        title: '服务工单编号',
        dataIndex: 'record_code',
        key: 'record_code',
    }, {
        title: '服务产品',
        dataIndex: 'service_project_name',
        key: 'service_project_name',
    }, {
        title: '长者姓名',
        dataIndex: 'purchaser_name',
        key: 'purchaser_name',
    }, {
        title: '长者身份证',
        dataIndex: 'purchaser_id_card',
        key: 'purchaser_id_card',
    }, {
        title: '服务创建时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '服务开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '服务完成时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '服务人员',
        dataIndex: 'servicer_name',
        key: 'servicer_name',
    }, {
        title: '服务状态',
        dataIndex: 'status',
        key: 'status',
    }, {
        title: '计价金额',
        dataIndex: 'valuation_amount',
        key: 'valuation_amount',
    }, {
        title: '是否已回访',
        dataIndex: 'visit_status',
        key: 'visit_status',
    },
    {
        title: '上传方式',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => {
            if (record['is_app'] === true) {
                return 'APP上传';
            } else if (record['is_app'] === false) {
                return 'PC上传';
            } else {
                return '';
            }
        },
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            // service_order_list: [],
            service_project_list: [],
            service_worker_list: [],
            request_ulr: '',
            org_list: [],
            social_worker_list: [],
            model_show: false,
            is_send: false,
            item_list: [],
            permission: [
                {
                    label: '导出',
                    btn_method: this.download,
                    icon: 'download',
                }]
        };
    }
    /** 行选中事件 */
    onRowSelection = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({
            item_list: selectedRowKeys
        });
    }
    /** 重置服务记录出发操作 */
    resetRecord = () => {
        let { item_list } = this.state;
        if (!item_list || item_list!.length === 0) {
            message.error('先选中所需重置的服务记录！');
        } else {
            this.setState({
                model_show: true,
            });
        }
    }
    /** 重置服务记录确定操作 */
    handleOk = () => {
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let { item_list } = this.state;
                AppServiceUtility.comfirm_order_service.reset_record!(item_list)!
                    .then(data => {
                        if (data === 'Success') {
                            message.info('重置服务记录成功');
                            this.formCreator && this.formCreator.reflash();
                        } else {
                            alert(data);
                            this.formCreator && this.formCreator.reflash();
                        }
                        this.setState({
                            model_show: false,
                            is_send: false
                        });
                    })
                    .catch(err => {
                        console.info(err);
                        this.setState({
                            is_send: false
                        });
                    });
            }
        );
    }
    handleCancel = () => {
        this.setState({
            model_show: false
        });
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    /** 发送费用记录按钮 */
    sendRecord = () => {
        let ids = this.state.select_ids;
        if (ids && ids!.length > 0) {
            request(this, AppServiceUtility.service_record_service.send_record!(ids))
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('发送成功');
                    } else {
                        message.info('发送失败');
                    }
                }).catch((error: Error) => {
                    message.error('发送失败！' + error.message);
                });
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            // this.props.history!.push(ROUTE_PATH.changeServiceRecord + '/' + contents.id);
            window.open(ROUTE_PATH.changeServiceRecord + '/' + contents.id, '_blank');
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
        // 获取当前用户信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data) => {
                if (data.length > 0 && data[0]['id'] === '23b3630a-d92c-11e9-8b9a-983b8f0bcd67') {
                    let new_permission = this.state.permission;
                    new_permission.push({
                        label: '重置记录(勾选)',
                        btn_method: this.resetRecord,
                        icon: 'undo',
                    });
                    this.setState({
                        permission: new_permission
                    });
                }
            })
            .catch((err) => {
                // console.log(err);
            });
        // 查询社工局列表
        request(this, AppServiceUtility.person_org_manage_service.get_organization_all_list!({ 'personnel_category': '街道' }))!
            .then((data: any) => {
                this.setState({
                    social_worker_list: data!.result!
                });
            })
            .catch(err => {
                console.info(err);
            });
        // // 查询服务商列表
        // request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ 'personnel_category': '服务商', 'contract_status': '签约' }))!
        //     .then((data: any) => {
        //         this.setState({
        //             org_list: data!.result!,
        //         });
        //     })
        //     .catch(err => {
        //         console.info(err);
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type_list: ["服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }

    // 下载
    download = () => {
        let _this = this.formCreator;
        console.log(_this.state.condition, 9999);
        AppServiceUtility.service_record_service.get_service_record_list_all!(_this.state.condition)!
            .then((data: any) => {
                // console.log('服务订单', data);
                if (data.result.length) {
                    let new_data: any = [];
                    let file_name = '服务记录汇总表';
                    data.result.map((item: any) => {
                        new_data.push({
                            "服务商": item.service_provider_name ? item.service_provider_name : '',
                            "服务订单编号": item.service_order_code ? item.service_order_code : '',
                            "服务工单编号": item.record_code ? item.record_code : '',
                            "服务产品": item.service_project_name ? item.service_project_name : '',
                            "长者姓名": item.purchaser_name ? item.purchaser_name : '',
                            "长者身份证": item.purchaser_id_card ? item.purchaser_id_card : '',
                            "服务创建时间": item.create_date ? item.create_date : '',
                            "服务开始时间": item.start_date ? item.start_date : '',
                            "服务完成时间": item.end_date ? item.end_date : '',
                            "服务人员": item.servicer_name ? item.servicer_name : '',
                            "服务状态": item.status ? item.status : '',
                            "计价金额": item.valuation_amount ? item.valuation_amount : '',
                            "是否已回访": item.visit_status ? item.visit_status : '',
                        });

                    });
                    exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
                } else {
                    message.info('暂无数据可导', 2);
                }
            });
    }
    render() {
        // let worker = this.state.service_worker_list;
        // let service_worker_list: any[] = [];
        // worker!.map((item, idx) => {
        //     service_worker_list.push(<Option key={item['user_id']}>{item['name']}</Option>);
        // });
        // let service_order_list: any[] = [];
        // this.state.service_order_list!.map((item, idx) => {
        //     service_order_list.push(<Option key={item['id']}>{item['id']}</Option>);
        // });
        // let service_project_list: any[] = [];
        // this.state.service_project_list!.map((item, idx) => {
        //     service_project_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });
        let status_list: any[] = [];
        ['已完成', '服务中', '未服务'].map((item, idx) => {
            status_list.push(<Option key={item}>{item}</Option>);
        });
        // let service_provider_id_list: any[] = [];
        // if (this.state.org_list!.length > 0) {
        //     this.state.org_list!.map((item: any) => {
        //         service_provider_id_list.push(<Option key={item.id} value={item.id}>{item.name}</Option>);
        //     });
        // }
        let social_worker_id_list: any[] = [];
        if (this.state.social_worker_list!.length > 0) {
            this.state.social_worker_list!.map((item: any) => {
                social_worker_id_list.push(<Option key={item.id} value={item.id}>{item.name}</Option>);
            });
        }
        let huifang_list: any[] = [];
        ['已回访', '未回访'].map((item, idx) => {
            huifang_list.push(<Option key={item}>{item}</Option>);
        });
        let service_record = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务订单编号",
                    decorator_id: "service_order_code",
                    option: {
                        placeholder: "请输入服务订单编号",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.input,
                    label: "服务记录编号",
                    decorator_id: "service_record_code",
                    option: {
                        placeholder: "请输入服务记录编号",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "创建时间范围",
                    decorator_id: "order_create_date",
                    option: {
                        // placeholder: "请输入开始时间范围",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "开始时间范围",
                    decorator_id: "start_date",
                    option: {
                        // placeholder: "请输入开始时间范围",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "完成时间范围",
                    decorator_id: "end_date",
                    option: {
                        // placeholder: "请输入完成时间范围",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.input,
                    label: "服务人员",
                    decorator_id: "servicer_name",
                    option: {
                        placeholder: "请输入服务人员",
                        // childrens: service_order_list,
                    }
                }, {
                    type: InputType.input,
                    label: "服务项目",
                    decorator_id: "service_project_name",
                    option: {
                        placeholder: "请输入服务项目",
                        // childrens: service_project_list,
                    }
                }, {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "purchaser_name",
                    option: {
                        placeholder: "请输入长者姓名",
                        // childrens: service_project_list,
                    }
                }, {
                    type: InputType.input,
                    label: "长者身份证",
                    decorator_id: "purchaser_id_card",
                    option: {
                        placeholder: "请输入长者身份证",
                        // childrens: service_project_list,
                    }
                }, {
                    type: InputType.select,
                    label: "服务状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请输入服务状态",
                        childrens: status_list,
                        allowClear: true,
                    }
                },
                // {
                //     type: InputType.select,
                //     label: "服务商",
                //     decorator_id: "service_provider_id",
                //     option: {
                //         placeholder: "请选择服务商",
                //         childrens: service_provider_id_list,
                //         allowClear: true,
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "service_provider_id",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: "社工局",
                    decorator_id: "social_worker_id",
                    option: {
                        placeholder: "请选择社工局",
                        childrens: social_worker_id_list,
                        allowClear: true,
                    }
                },
                {
                    type: InputType.select,
                    label: "是否回访",
                    decorator_id: "visit_status",
                    option: {
                        placeholder: "请选择是否回访",
                        childrens: huifang_list,
                        allowClear: true,
                    }
                },
            ],
            btn_props: this.state.permission,
            scroll: { x: '100%' },
            table_size: "middle",
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            on_row_selection: this.onRowSelection,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_service_record'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            onRef: this.onRef,
        };
        let service_record_list = Object.assign(service_record, table_param);
        if (this.props.provider) {
            service_record_list.columns_data_source.pop();
        }
        return (
            <div>
                <Modal
                    title="温馨提示"
                    visible={this.state.model_show}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <div style={{ textAlign: 'center' }}>确定要把勾选的服务记录重置吗？</div>
                </Modal>
                <SignFrameLayout {...service_record_list} />
            </div>
        );
    }
}

/**
 * 控件：服务项目登记信息
 * 描述
 */
@addon('ServiceRecordView', '服务项目登记信息', '描述')
@reactControl(ServiceRecordView, true)
export class ServiceRecordViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    public permission?: Permission;
    public provider?: boolean;
}