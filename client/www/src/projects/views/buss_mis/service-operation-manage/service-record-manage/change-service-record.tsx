import { Select, Row, Col, Form, Button, message } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
// import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
// import { TableList, InputTypeTable } from "src/business/components/buss-components/table-list";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool"; // isPermission,
import { request, beforUploadCompress } from "src/business/util_tool";
import { MainCard } from "src/business/components/style-components/main-card";
import TextArea from "antd/lib/input/TextArea";
import { remote } from 'src/projects/remote';
import { ROUTE_PATH } from "src/projects/router";

// import { request_func } from "src/business/util_tool";
// const { TabPane } = Tabs;
/**
 * 组件：服务记录详情编辑状态
 */
export interface ChangeServiceRecordViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 服务项目 */
    service_project_list?: any[];
    /** 服务人员 */
    service_worker_list?: any[];
    /** 选项清单 */
    option_list?: any;
    /** 服务选项 */
    loading?: boolean;
    /** 服务评价 */
    service_evaluate?: any;
    evaluate?: any;

    // application_scope?: any[];
    // 数据详情
    data_info?: any;
    // 回访状态
    visit_status?: any;
    is_send?: boolean;
    endOpen?: boolean;
    startValue?: any;
}

/**
 * 组件：服务记录详情视图
 */
export class ChangeServiceRecordView extends ReactView<ChangeServiceRecordViewControl, ChangeServiceRecordViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            service_project_list: [],
            service_worker_list: [],
            option_list: {},
            service_evaluate: {},
            evaluate: [],
            data_info: [],
            visit_status: '已回访',
            is_send: false,
            endOpen: false,
        };
    }
    /** 确定回调 */
    handleSubmit = (e: any) => {
        e.preventDefault();
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        const { form } = this.props;
        let { data_info, visit_status } = this.state;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                if (visit_status === '已回访') {
                    message.error('已经有回访内容');
                    return;
                }
                let param: any = {
                    id: [data_info.id],
                    service_satisfaction: values['service_satisfaction'],
                    visit_satisfaction: values['visit_satisfaction'],
                    other_satisfaction: values['other_satisfaction'],
                };
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.service_operation_service.update_service_return_visit_record!(param))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    message.info('提交成功！', 1, () => {
                                        window.location.reload();
                                    });
                                } else {
                                    message.error('提交失败！');
                                    this.setState({
                                        is_send: false,
                                    });
                                }
                            })
                            .catch((error: any) => {
                                message.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }

    returnBtn = () => {
        this.props.history!.goBack();
    }

    componentDidMount() {
        // const param = [{ id: this.props.match!.params.key }, 1, 1];
        // request_func(this, AppServiceUtility.service_record_service['get_service_record_list'], param, (data: any) => {
        //     console.info(data);
        //     let evaluate: any = [];
        //     console.info(data.result[12]._evaluate_list);
        //     data.result[12]._evaluate_list!.map!((item: any, index: Number) => {
        //         evaluate.push(<Option key={item.id}>{item.evaluate_list}</Option>);
        //     });
        //     this.setState({ evaluate, service_evaluate: data.result[12] });
        // });
        AppServiceUtility.service_item_serviceinfo.get_product_package_list!({})!
            .then((data: any) => {
                if (data) {
                    let service_project = data.result.map((key: any, value: any) => {
                        return <Select.Option key={key.id}>{key.name}</Select.Option>;
                    });
                    this.setState({
                        service_project_list: service_project
                    });
                }
            });
        AppServiceUtility.person_org_manage_service.get_worker_list!({})!
            .then((data: any) => {
                if (data) {
                    let worker = data.result.map((key: any, value: any) => {
                        return <Select.Option key={key.id}>{key.name}</Select.Option>;
                    });
                    this.setState({
                        service_worker_list: worker
                    });
                }
            });

        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.service_record_service.get_service_record_list_all!({ id: this.props.match!.params.key }, 1, 1))
                .then((datas: any) => {
                    let data_info = datas.result && datas.result[0] ? datas.result[0] : [];
                    this.setState({
                        data_info,
                        visit_status: data_info.hasOwnProperty('visit_status') ? data_info['visit_status'] : '未回访'
                    });
                });
        }
    }
    handleStartOpenChange = (open: any) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    }
    onStartChange = (value: any) => {
        this.setState({ startValue: value });
    }
    disabledEndDate = (endValue: any) => {
        const { startValue } = this.state;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    }
    handleEndOpenChange = (open: any) => {
        this.setState({ endOpen: open });
    }
    dateFormat(d: any) {
        var date = (d.getFullYear()) + "-" +
            ((d.getMonth() + 1) + '').padStart(2, '0') + "-" +
            (d.getDate() + '').padStart(2, '0') + " " +
            (d.getHours() + '').padStart(2, '0') + ":" +
            (d.getMinutes() + '').padStart(2, '0') + ":" +
            (d.getSeconds() + '').padStart(2, '0');
        return date;
    }
    submitCB(err: any, values: any) {
        // console.log(values);
        let { is_send, data_info } = this.state;
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        // 待接收/服务中
        if (data_info !== '已完成') {
            if (!values['start_date']) {
                message.info('请选择服务开始时间');
                return;
            }
            if (!values['begin_photo'] || values['begin_photo'].length === 0) {
                message.info('至少上传服务开始图片！');
                return;
            }
        }
        // 服务中/服务中
        if (data_info['status'] === '服务中') {
            if (!values['end_date']) {
                message.info('请选择服务完成时间');
                return;
            }
            if (!values['end_photo'] || values['end_photo'].length === 0) {
                message.info('请上传服务结束图片！');
                return;
            }
        }
        if (!data_info['task_id']) {
            message.info('未知错误，请刷新重试！');
            return;
        }
        let param: any = {
            // 这里发过去要任务的ID
            id: data_info['task_id'] && data_info['task_id'] ? data_info['task_id'] : '',
            order_id: data_info['order_id'],
            begin_photo: values['begin_photo'].length > 0 ? values['begin_photo'] : '',
            end_photo: values['end_photo'].length > 0 ? values['end_photo'] : '',
            start_date: values['start_date'] ? this.dateFormat(values['start_date']['_d']) : '',
            end_date: values['end_date'] ? this.dateFormat(values['end_date']['_d']) : '',
            remarks: values['remarks'] || '',
            start_position: values['start_position'] || '',
            end_position: values['end_position'] || '',
            is_app: false
        };
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.task_service.change_task_service_order!(param))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                history.back();
                            });
                        } else {
                            message.info(datas);
                            this.setState({
                                is_send: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    render() {
        let { data_info, visit_status } = this.state;
        let layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        let edit_props = {
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            form_items_props: [
                {
                    title: "服务记录信息",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "服务项目",
                                        decorator_id: "service_product_id",
                                        layout: layout,
                                        option: {
                                            childrens: this.state.service_project_list,
                                            disabled: true
                                        }
                                    }, {
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "服务人员",
                                        decorator_id: "servicer_id",
                                        layout: layout,
                                        option: {
                                            childrens: this.state.service_worker_list,
                                            disabled: true
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "计价金额",
                                        decorator_id: "valuation_amount",
                                        layout: layout,
                                        option: {
                                            disabled: true
                                        }
                                    }
                                ]
                            }
                        },
                    ]
                },
                {
                    title: "服务内容",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.date,
                                        col_span: 12,
                                        label: "服务开始时间",
                                        decorator_id: "start_date",
                                        layout: layout,
                                        option: {
                                            showTime: true,
                                            disabled: data_info.status === '已完成' ? true : false,
                                            onChange: this.onStartChange,
                                            onOpenChange: this.handleStartOpenChange
                                        }
                                    }, {
                                        type: InputType.date,
                                        col_span: 12,
                                        label: "服务结束时间",
                                        decorator_id: "end_date",
                                        layout: layout,
                                        option: {
                                            showTime: true,
                                            disabled: data_info.status === '已完成' ? true : false,
                                            open: this.state.endOpen,
                                            disabledDate: this.disabledEndDate,
                                            onOpenChange: this.handleEndOpenChange,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "服务前位置",
                                        decorator_id: "start_position",
                                        layout: layout,
                                        option: {
                                            disabled: data_info.status === '已完成' ? true : false,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "服务后位置",
                                        decorator_id: "end_position",
                                        layout: layout,
                                        option: {
                                            disabled: data_info.status === '已完成' ? true : false,
                                        }
                                    },
                                    {
                                        type: InputType.upload,
                                        col_span: 12,
                                        label: "服务开始图片（大小小于2M，格式支持jpg/jpeg/png）",
                                        decorator_id: "begin_photo",
                                        layout: layout,
                                        option: {
                                            disabled: data_info.status === '已完成' ? true : false,
                                            beforeUpload: beforUploadCompress,
                                            action: remote.upload_url
                                        }
                                    }, {
                                        type: InputType.upload,
                                        col_span: 12,
                                        label: "服务结束图片（大小小于2M，格式支持jpg/jpeg/png）",
                                        decorator_id: "end_photo",
                                        layout: layout,
                                        option: {
                                            disabled: data_info.status === '已完成' ? true : false,
                                            beforeUpload: beforUploadCompress,
                                            action: remote.upload_url
                                        }
                                    }, {
                                        type: InputType.text_area,
                                        col_span: 12,
                                        label: "执行情况",
                                        decorator_id: "remarks",
                                        layout: layout,
                                        option: {
                                            disabled: data_info.status === '已完成' ? true : false,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "上传方式",
                                        decorator_id: "is_app",
                                        layout: layout,
                                        option: {
                                            disabled: true,
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    title: "服务订单信息",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "服务订单编号",
                                        decorator_id: "service_order_code",
                                        layout: layout,
                                        option: {
                                            disabled: true
                                        }
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "服务工单编号",
                                        decorator_id: "record_code",
                                        layout: layout,
                                        option: {
                                            disabled: true
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "服务商",
                                        decorator_id: "service_provider_name",
                                        layout: layout,
                                        option: {
                                            disabled: true
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "采购人",
                                        decorator_id: "purchaser_name",
                                        layout: layout,
                                        option: {
                                            disabled: true
                                        }
                                    }
                                ]
                            }
                        },
                    ]
                },
            ],
            service_option: {
                service_object: AppServiceUtility.service_record_service,
                operation_option: {
                    query: {
                        func_name: "get_service_record_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "updateupdate_service_record"
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.props.match!.params.key,
        };
        const columns_data_option = [{
            title: '选项名称',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '选项值',
            dataIndex: 'value',
            key: 'value',
        }, {
            title: '说明',
            dataIndex: 'explain',
            key: 'explain',
        }, {
            title: '备注',
            dataIndex: 'remark',
            key: 'remark',
        },];
        const option_list = {
            loading: this.state.loading,
            type_show: false,
            columns_data_source: columns_data_option,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: 'get_record_option_list',
                    service_condition: [{ id: this.props.match!.params.key }, 1, 10]
                },

            },
        };
        // 服务评价
        const columns_data_evaluate = [{
            title: '评价人',
            dataIndex: 'comment_user',
            key: 'evaluator_id',
        }, {
            title: '评分内容',
            dataIndex: 'content',
            key: 'content',
        }, {
            title: '评分等级',
            dataIndex: 'level',
            key: 'level',
        }, {
            title: '评价时间',
            dataIndex: 'comment_date',
            key: 'comment_date',
        },];
        const service_evaluate = {
            loading: this.state.loading,
            type_show: false,
            columns_data_source: columns_data_evaluate,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: 'get_record_evaluate_list',
                    service_condition: [{ record_id: this.props.match!.params.key }, 1, 10]
                },
            },
        };
        Object.assign(service_evaluate, table_param);
        Object.assign(option_list, table_param);
        const { getFieldDecorator } = this.props.form!;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        let disabled = data_info['task_state'] !== '已完成' || visit_status === '已回访' ? true : false;
        if (data_info.status !== '已完成') {
            edit_props['submit_btn_propps'] = {
                text: "保存",
                cb: (err: any, values: any) => {
                    this.submitCB(err, values);
                }
            };
        }
        return (
            // <Tabs defaultActiveKey="1" >
            <Row>
                <MainContent>
                    <FormCreator {...edit_props} />
                </MainContent>
                <MainContent>
                    <SignFrameLayout {...option_list} />
                </MainContent>
                <MainContent>
                    <SignFrameLayout {...service_evaluate} />
                </MainContent>
                <MainContent>
                    <SignFrameLayout {...service_evaluate} />
                </MainContent>
                <MainContent>
                    <MainCard title='长者信息'>
                        <Row type="flex" justify="center">
                            <Col span={8}>
                                长者姓名：{data_info['purchaser_name']}
                            </Col>
                            <Col span={8}>
                                联系电话：{data_info['purchaser_telephone']}
                            </Col>
                            <Col span={8}>
                                服务地址：{data_info['task_address']}
                            </Col>
                        </Row>
                        <br />
                        <Row type="flex" justify="center">
                            <Col span={8}>
                                监护人姓名：{data_info['guardian_name']}
                            </Col>
                            <Col span={8}>
                                监护人电话：{data_info['guardian_telephone']}
                            </Col>
                            <Col span={8}>
                                监护人身份证：{data_info['guardian_id_card']}
                            </Col>
                        </Row>
                        <br />
                        {
                            data_info['family_name'] && data_info['family_name'].length > 0 ? data_info['family_name'] && data_info['family_name'].map((item: any) => {
                                return <Row key={item} type="flex" justify="center">
                                    <Col span={8}>
                                        联系人姓名：{item['title']}
                                    </Col>
                                    <Col span={8}>
                                        联系人电话：{item['contents']}
                                    </Col>
                                    <Col span={8}>
                                        关系：{item['relation']}
                                    </Col>
                                </Row>;
                            }) : ''
                        }
                    </MainCard>
                </MainContent>
                <MainContent>
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                        <MainCard title='回访'>
                            <Row type="flex" justify="center">
                                <Col span={12}>
                                    <Form.Item label='服务满意度'>
                                        {getFieldDecorator('service_satisfaction', {
                                            initialValue: data_info.service_satisfaction || '',
                                            rules: [{
                                                required: true,
                                                message: '请选择服务满意度'
                                            }],
                                        })(
                                            <Select disabled={disabled}>
                                                {['很满意', '满意', '一般', '不满意'].map((item, index) => {
                                                    return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                                })}
                                            </Select>
                                        )}
                                    </Form.Item>
                                    {/* <Form.Item label='回访满意度'>
                                        {getFieldDecorator('visit_satisfaction', {
                                            initialValue: data_info.visit_satisfaction || '',
                                            rules: [{
                                                required: true,
                                                message: '请选择回访满意度'
                                            }],
                                        })(
                                            <Select disabled={disabled}>
                                                {['很满意', '满意', '一般', '不满意'].map((item, index) => {
                                                    return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                                })}
                                            </Select>
                                        )}
                                    </Form.Item> */}
                                    <Form.Item label='其他评价'>
                                        {getFieldDecorator('other_satisfaction', {
                                            initialValue: data_info.other_satisfaction || '',
                                        })(
                                            <TextArea autoComplete='off' disabled={disabled} />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard>
                            <Row type="flex" justify="center" className="ctrl-btns">
                                <Button htmlType='submit' type='primary' disabled={disabled}>保存</Button>
                                <Button onClick={() => this.props.history!.push(ROUTE_PATH.serviceRecord)}>返回</Button>
                            </Row>
                        </MainCard>
                    </Form>
                </MainContent>
            </Row>
            // </Tabs>
        );
    }
}

/**
 * 控件：服务记录详情控件
 * @description 服务记录详情控件
 * @author
 */
@addon('ChangeServiceRecordtView', '服务记录详情控件', '服务记录详情控件')
@reactControl(Form.create<any>()(ChangeServiceRecordView), true)
export class ChangeServiceRecordViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}