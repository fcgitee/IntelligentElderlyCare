import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：服务选项状态
 */
export interface ServiceOptionViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：服务选项
 * 描述
 */
export class ServiceOptionViewView extends ReactView<ServiceOptionViewControl, ServiceOptionViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '选项值类型',
        dataIndex: 'option_value_type',
        key: 'option_value_type',
    }, {
        title: '默认值',
        dataIndex: 'default_value',
        key: 'default_value',
    }, {
        title: '是否允许修改',
        dataIndex: 'is_modification_mermissible',
        key: 'is_modification_mermissible',
    }, {
        title: '备注',
        dataIndex: 'remarks',
        key: 'remarks',
    },
    {
        title: '组织机构',
        dataIndex: 'org_name',
        key: 'org_name',
    },];
    constructor(props: any) {
        super(props);
        this.state = {
            request_url: '',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceOption);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeServiceOption + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "选项名称",
                    decorator_id: "name"
                }, {
                    type: InputType.radioGroup,
                    label: "选项值类型",
                    decorator_id: "option_value_type",
                    option: {
                        options: [
                            {
                                label: '1-单选框',
                                value: '1'
                            }, {
                                label: '2-多选框',
                                value: '2'
                            }, {
                                label: '3-输入框',
                                value: '3'
                            },
                        ],
                    }

                }, {
                    type: InputType.radioGroup,
                    label: "是否允许修改",
                    decorator_id: "is_modification_mermissible",
                    option: {
                        options: [
                            {
                                label: '是',
                                value: '是'
                            }, {
                                label: '否',
                                value: '否'
                            }
                        ]
                    }
                }, {
                    type: InputType.radioGroup,
                    label: "服务前后",
                    decorator_id: "option_type",
                    option: {
                        options: [
                            {
                                label: '服务前',
                                value: 'service_before'
                            }, {
                                label: '服务后',
                                value: 'service_after'
                            }
                        ]
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_option_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'delete_servicre_option'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：服务选项
 * 描述
 */
@addon('ServiceOptionViewView', '服务选项', 'ServiceScopeView')
@reactControl(ServiceOptionViewView, true)
export class ServiceOptionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}