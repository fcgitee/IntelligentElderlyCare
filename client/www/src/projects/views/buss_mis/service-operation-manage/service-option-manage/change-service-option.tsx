import { message, Row, Radio } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainCard } from "src/business/components/style-components/main-card";
import { edit_props_info } from "src/projects/app/util-tool";
import { request } from "src/business/util_tool";

/**
 * 组件：编辑行为能力评估项目状态
 */
export interface ChangeServiceOptionViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    // 选择框类
    option_value_type?: string;
    // 销毁组件附带的数据
    old_data?: any;
}

/**
 * 组件：编辑行为能力评估项目视图
 */
export class ChangeServiceOptionViewView extends ReactView<ChangeServiceOptionViewControl, ChangeServiceOptionViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            old_data: {},
        };
    }
    /** 保存按钮回调方法 */
    // 不同组件是不同的传入方法
    save_value = (value1: any, value2: any) => {
        let value: any;
        let option_value_type = this.state.option_value_type!;
        if (option_value_type === '1' || option_value_type === '2') {
            // 1,2是table-list可编辑列表组件
            value = value1;
        } else {
            // 3是form表单组件
            value = value2;
            // 添加option_value = 0,否则计价公式会报错
            value.option_value = '0';
        }
        if (this.props.match!.params.key) {
            value['id'] = this.props.match!.params.key;
        }

        // 检测默认值是否在选项值中
        // 在输入框的地方会报错，20191101改动
        // 在单选和多选的状态下
        if (option_value_type === '1' || option_value_type === '2') {
            // 首先判断是否有选项值，至少要有
            let option_content = value['option_content'] || [];
            if (option_content.length < 1) {
                message.error('请至少添加一项！');
                return;
            }
            // 如果有默认值，这样判断可以无视0
            if (value.default_value !== '') {
                // 标识值
                let flag = false;
                for (let item of option_content) {
                    if (item.option_value === value.default_value) {
                        // 改变标识
                        flag = true;
                    }
                }
                // 在这里做判断
                if (flag === false) {
                    message.error('错误，默认值不存在于选项中！');
                    return;
                }
            }
        }

        value['option_value_type'] = option_value_type;

        request(this, AppServiceUtility.service_option_service.update_servicre_option!(value)!)
            .then((datas: any) => {
                if (datas) {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.serviceOption);
                }
            });
    }
    toggleComponent = (value: any) => {
        // console.log('unmonut', value);
        this.setState({
            old_data: value,
        });
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
            request(this, AppServiceUtility.service_option_service.get_servicre_option_list!({ id: this.props.match!.params.key }, 1, 1)!)
                .then((datas: any) => {
                    this.setState({
                        option_value_type: datas.result[0].option_value_type,
                    });
                });
        } else {
            this.setState({
                option_value_type: '1',
            });
        }
    }
    radisChange(e: any) {
        this.setState({
            option_value_type: e.target.value,
        });
    }
    backList() {
        this.props.history!.push(ROUTE_PATH.serviceOption);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let table_list_props = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "选项名称",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入选项名称", }],
                        initialValue: this.state.old_data.name || '',
                    } as GetFieldDecoratorOptions,
                    decorator_id: "name",
                }, {
                    type: InputTypeTable.input,
                    label: "默认值",
                    field_decorator_option: {
                        rules: [{ required: false, message: "" }],
                        initialValue: this.state.old_data.default_value || '',
                    } as GetFieldDecoratorOptions,
                    decorator_id: "default_value",
                }, {
                    type: InputTypeTable.radio,
                    label: "是否允许修改",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择是否允许修改" }],
                        initialValue: this.state.old_data.is_modification_mermissible || '',
                    } as GetFieldDecoratorOptions,
                    decorator_id: "is_modification_mermissible",
                    option: [
                        {
                            label: '是',
                            value: '是'
                        }, {
                            label: '否',
                            value: '否'
                        }
                    ]
                },
                {
                    type: InputTypeTable.text_area,
                    label: "备注",
                    field_decorator_option: {
                        rules: [{ required: false, message: "" }],
                        initialValue: this.state.old_data.remarks || '',
                    } as GetFieldDecoratorOptions,
                    decorator_id: "remarks",
                },
                {
                    type: InputTypeTable.radio,
                    label: "选项类型",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择选项类型" }],
                        initialValue: this.state.old_data.option_type || '',
                    } as GetFieldDecoratorOptions,
                    decorator_id: "option_type",
                    option: [
                        {
                            label: '服务前',
                            value: 'service_before'
                        }, {
                            label: '服务后',
                            value: 'service_after'
                        }
                    ]
                },
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.backList();
                    }
                }
            ],
            submit_props: {
                text: "保存",
                cb: this.save_value
            },
            destroy_func: {
                cb: this.toggleComponent
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            table_field_name: 'option_content',
            // table配置
            columns_data_source: [{
                title: '选项内容',
                dataIndex: 'option_content',
                key: 'option_content',
                type: 'input' as ColTypeStr
            }, {
                title: '选项值',
                dataIndex: 'option_value',
                key: 'option_value',
                type: 'input' as ColTypeStr
            }],
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.service_option_service,
                operation_option: {
                    query: {
                        func_name: "get_servicre_option_list_all",
                        arguments: [this.state.selete_obj, 1, 1]
                    },
                },
            },
            add_row_text: "新增服务选项",
            succ_func: () => { this.backList(); },
            id: this.props.match!.params.key
        };
        let layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 19 },
            },
        };
        // option_value_type
        let edit_props = {
            form_items_props: [
                {
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "选项名称",
                                        decorator_id: "name",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入选项名称" }],
                                            initialValue: this.state.old_data.name || '',
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入项目名称",
                                        },
                                        layout: layout
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "默认值",
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "" }],
                                            initialValue: this.state.old_data.default_value || '',
                                        } as GetFieldDecoratorOptions,
                                        decorator_id: "default_value",
                                        option: {
                                            placeholder: "默认值",
                                        },
                                        layout: layout
                                    },
                                    {
                                        type: InputType.radioGroup,
                                        col_span: 8,
                                        label: "是否允许修改",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请选择是否允许修改" }],
                                            initialValue: this.state.old_data.is_modification_mermissible || '',
                                        } as GetFieldDecoratorOptions,
                                        decorator_id: "is_modification_mermissible",
                                        option: {
                                            options: [
                                                {
                                                    label: '是',
                                                    value: '是'
                                                }, {
                                                    label: '否',
                                                    value: '否'
                                                }
                                            ]
                                        },
                                        layout: layout
                                    }
                                ]
                            },
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.text_area,
                                        col_span: 8,
                                        label: "备注",
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "" }],
                                            initialValue: this.state.old_data.remarks || '',
                                        } as GetFieldDecoratorOptions,
                                        decorator_id: "remarks",
                                        layout: layout
                                    },
                                    {
                                        type: InputType.radioGroup,
                                        col_span: 8,
                                        label: "选项类型",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请选择选项类型" }],
                                            initialValue: this.state.old_data.option_type || '',
                                        } as GetFieldDecoratorOptions,
                                        decorator_id: "option_type",
                                        option: {
                                            options: [
                                                {
                                                    label: '服务前',
                                                    value: 'service_before'
                                                }, {
                                                    label: '服务后',
                                                    value: 'service_after'
                                                }
                                            ]
                                        },
                                        layout: layout
                                    },
                                ]
                            },
                        }
                    ]
                }
            ],
            submit_btn_propps: {
                text: "保存",
                cb: this.save_value
            },
            destroy_func: {
                cb: this.toggleComponent
            },
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => this.backList!()
                },
            ],
            row_btn_props: {
                style: {
                    justifyContent: " start"
                }
            },
            service_option: {
                service_object: AppServiceUtility.service_option_service,
                operation_option: {
                    query: {
                        func_name: "get_servicre_option_list_all",
                        arguments: [this.state.selete_obj, 1, 1]
                    },
                },
            },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        let option_value_type = this.state.option_value_type!;
        // console.log('olddata', this.state.old_data);
        // console.log(table_list_props);
        // console.log(edit_props_list);
        return (
            <div>
                <MainContent>
                    <MainCard>
                        <Row type="flex" justify="start" style={{ 'fontSize': '14px' }}>
                            选项值类型：
                        <Radio.Group value={option_value_type} onChange={(e) => this.radisChange(e)}>
                                <Radio value='1'>单选框</Radio>
                                <Radio value='2'>多选框</Radio>
                                <Radio value='3'>输入框</Radio>
                            </Radio.Group>
                        </Row>
                    </MainCard>
                    {option_value_type === '1' || option_value_type === '2' ? (
                        <TableList {...table_list_props} />
                    ) : option_value_type === '3' ? <FormCreator {...edit_props_list} /> : null}
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：编辑行为能力评估项目编辑控件
 * @description 编辑行为能力评估项目编辑控件
 * @author
 */
@addon('ChangeAssessmentProjectView', '编辑行为能力评估项目编辑控件', '编辑行为能力评估项目编辑控件')
@reactControl(ChangeServiceOptionViewView, true)
export class ChangeServiceOptionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}