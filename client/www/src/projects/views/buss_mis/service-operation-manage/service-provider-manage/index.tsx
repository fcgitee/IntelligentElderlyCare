import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { table_param } from "src/projects/app/util-tool";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：服务商登记信息状态
 */
export interface ServiceProviderViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 服务项目集合 */
    service_list?: any[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 服务项目总条数 */
    total?: number;
    /** 标题 */
    title?: string;
    /** 表格列信息 */
    columns_service_item?: any[];
    /** 审核弹出框 */
    audit_visible?: boolean;
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：服务商登记信息
 * 描述
 */
export class ServiceProviderView extends ReactView<ServiceProviderViewControl, ServiceProviderViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'org[0].name',
        key: 'org[0].name',
    }, {
        title: '地址',
        dataIndex: 'org[0].organization_info.address',
        key: 'org[0].organization_info.address',
    }, {
        title: '电话',
        dataIndex: 'org[0].organization_info.telephone',
        key: 'org[0].organization_info.telephone',
    }, {
        title: '服务商能力级别',
        dataIndex: 'sp_ability_level',
        key: 'sp_ability_level',
    }, {
        title: '服务商信用级别',
        dataIndex: 'sp_credit_level',
        key: 'sp_credit_level',
    }, {
        title: '登记时间',
        dataIndex: 'register_date',
        key: 'register_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            audit_visible: false,
            request_url: '',
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.ProviderApplyReviewed + '/' + contents.id);
        }
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let service_provider = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务商名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "服务商机构性质",
                    decorator_id: "date_range"
                },
                {
                    type: InputType.input,
                    label: "服务商地址",
                    decorator_id: "address"
                },
                {
                    type: InputType.rangePicker,
                    label: "登记时间",
                    decorator_id: "date_range"
                },

            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_provider_list = Object.assign(service_provider, table_param);
        return (
            <SignFrameLayout {...service_provider_list} />
        );
    }
}

/**
 * 控件：服务商登记信息
 * 描述
 */
@addon('ServiceProviderView', '服务商登记信息', '描述')
@reactControl(ServiceProviderView, true)
export class ServiceProviderViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}