import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { remote } from 'src/projects/remote';
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

let { Option } = Select;

/**
 * 组件：服务商登记编辑页面状态
 */
export interface ChangeServiceProviderViewState extends ReactViewState {
    /** 服务商列表 */
    provider_list?: [];
}

/**
 * 组件：服务商登记编辑页面
 * 描述
 */
export class ChangeServiceProviderView extends ReactView<ChangeServiceProviderViewControl, ChangeServiceProviderViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, ];
    constructor(props: any) {
        super(props);
        this.state = {
            provider_list: []
        };
    }
    // /** 权限服务 */
    // permissionService?() {
    //     return getObject(this.props.permissionService_Fac!);
    // }
    componentDidMount() {
        request(this, AppServiceUtility.personnel_service['get_organizational_list']!({}, 1, 999))
            .then((datas: any) => {
                // console.log('datas.result', datas.result);
                this.setState({
                    provider_list: datas.result,
                });
            });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let provider = this.state.provider_list;
        let provider_list: any[] = [];
        provider!.map((item, idx) => {
            provider_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "服务商名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_organizational_list_all',
            title: '服务商查询',
            select_option: {
                placeholder: "请选择服务商",
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "服务商登记信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "服务商",
                            decorator_id: "service_provider_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务商" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                modal_search_items_props:modal_search_items_props
                            }
                        },
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务商类别",
                        //     decorator_id: "sp_type_id",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请输入服务商类别" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务商类别"
                        //     }
                        // }, 
                        {
                            type: InputType.select,
                            label: "服务商能力级别",
                            decorator_id: "sp_ability_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务商能力级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务商能力级别",
                                childrens:[
                                    <Option key={1}>{'一级'}</Option>,
                                    <Option key={2}>{'二级'}</Option>,
                                    <Option key={3}>{'三级'}</Option>,
                                    <Option key={4}>{'四级'}</Option>,
                                    <Option key={5}>{'五级'}</Option>,
                                    <Option key={'other'}>{'其他'}</Option>,
                                ]
                            }
                        },
                        {
                            type: InputType.select,
                            label: "服务商信用级别",
                            decorator_id: "sp_credit_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务商信用级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务商信用级别",
                                childrens:[
                                    <Option key={1}>{'一级'}</Option>,
                                    <Option key={2}>{'二级'}</Option>,
                                    <Option key={3}>{'三级'}</Option>,
                                    <Option key={4}>{'四级'}</Option>,
                                    <Option key={5}>{'五级'}</Option>,
                                    <Option key={'other'}>{'其他'}</Option>,
                                ]
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "服务商资质信息（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "sp_qualifications",
                            field_decorator_option: {
                                rules: [{  message: "请输入服务商资质信息" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action:remote.upload_url,
                                // placeholder: "请输入服务商资质信息"
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "分成占有百分比",
                            decorator_id: "share_percentage",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入分成占有百分比" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入分成占有百分比"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "服务机构合同条款",
                            decorator_id: "sp_contract_clauses",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务机构合同条款" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务机构合同条款",
                                rows: 3
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                rows: 3

                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.serviceProvider);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: "get_service_provider_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_provider"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceProvider); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：服务商登记编辑页面
 * 描述
 */
@addon('ChangeServiceProviderView', '服务商登记编辑页面', '描述')
@reactControl(ChangeServiceProviderView, true)
export class ChangeServiceProviderViewControl extends ReactViewControl {
}