import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Descriptions, Button, Row, Radio } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from 'src/projects/app/appService';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import TextArea from "antd/lib/input/TextArea";

/**
 * 组件：服务商审核页面
 */
export interface ServiceProviderReviewedViewState extends ReactViewState {
    data?: any;
    reason?: any;
    result?: any;
}

/**
 * 组件：服务商审核页面
 * 描述
 */
export class ServiceProviderReviewedView extends ReactView<ServiceProviderReviewedViewControl, ServiceProviderReviewedViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            result: '',
            reason: '',
        };
    }
    componentDidMount = () => {
        let id = this.props.match!.params.key;
        AppServiceUtility.service_operation_service.get_Subsidy_service_provider_list_all!({ id })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        data: data.result[0]
                    });
                }
            });
    }
    // 提交
    handleSubmit = () => {
        let id = this.props.match!.params.key;
        const { result, reason } = this.state;
        var params = {
            id,
            subsidy_res: result,
            opinion: reason
        };
        // console.log(params);
        AppServiceUtility.service_operation_service.update_service_provider_record!(params)!
            .then((data: any) => {
                if (data) {
                    this.props.history!.push(ROUTE_PATH.serviceProvider);
                }
            });
    }

    // 审核结果
    resultChange = (e: any) => {
        this.setState({
            result: e.target.value
        });
    }

    // 原因
    reasonChange = (e: any) => {
        this.setState({
            reason: e.target.value
        });
    }
    render() {
        const { data } = this.state;
        return (
            <div>
                <MainContent>
                    <MainCard title='服务商申请审核'>
                        <Descriptions
                            title="申请信息"
                            bordered={true}
                            column={3}
                        >
                            <Descriptions.Item label="名称">{data.name}</Descriptions.Item>
                            <Descriptions.Item label="地址">{data.address}</Descriptions.Item>
                            <Descriptions.Item label="简介">{data.introduction}</Descriptions.Item>
                            <Descriptions.Item label="相关图片"><img src={data.business_photo ? data.business_photo : ''} /></Descriptions.Item>
                            <Descriptions.Item label="经度">{data.lon}</Descriptions.Item>
                            <Descriptions.Item label="纬度">{data.lat}</Descriptions.Item>
                            <Descriptions.Item label="法人">{data.legal_person}</Descriptions.Item>
                            <Descriptions.Item label="营业执照"><img src={data.business_license_url ? data.business_license_url : ''} /></Descriptions.Item>
                            <Descriptions.Item label="联系电话">{data.telephone}</Descriptions.Item>
                            <Descriptions.Item label="组织机构性质">{data.organization_nature}</Descriptions.Item>
                            <Descriptions.Item label="行政区划">{data.admin_area_id}</Descriptions.Item>
                            <Descriptions.Item label="上级组织机构">{data.super_org_id}</Descriptions.Item>
                            <Descriptions.Item label="服务商能力级别">{data.sp_ability_level}</Descriptions.Item>
                            <Descriptions.Item label="服务商信用级别">{data.sp_credit_level}</Descriptions.Item>
                            <Descriptions.Item label="服务商资质信息"><img src={data.sp_qualifications ? data.sp_qualifications : ''} /></Descriptions.Item>
                            <Descriptions.Item label="服务机构条款">{data.sp_contract_clauses}</Descriptions.Item>
                            <Descriptions.Item label="备注">{data.remark}</Descriptions.Item>
                        </Descriptions>
                        <br />
                        <br />
                        <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>审核结果<span>(请选择审核结果)</span></Row>
                        <br />
                        <Radio.Group onChange={this.resultChange}>
                            <Radio value={'通过'}>通过</Radio>
                            <Radio value={'不通过'}>不通过</Radio>
                        </Radio.Group>
                        <br />
                        <br />
                        <br />
                        <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>填写原因<span>(请填写原因)</span></Row>
                        <br />
                        <TextArea rows={4} onChange={this.reasonChange} />
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button type="primary" onClick={this.handleSubmit}>提交</Button>
                        </Row>
                    </MainCard>
                </MainContent>
            </div >
        );
    }
}

/**
 * 控件：服务商审核页面
 * 描述
 */
@addon(' ServiceProviderReviewedView', '服务商审核页面', '提示')
@reactControl(ServiceProviderReviewedView, true)
export class ServiceProviderReviewedViewControl extends ReactViewControl {
}