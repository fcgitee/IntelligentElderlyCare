import { Select } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";

let { Option } = Select;
/**
 * 组件：服务评价列表状态
 */
export interface ServiceEvaluateViewState extends ReactViewState {
    /** 服务商列表 */
    item_list?: [];
    /** 服务类型列表 */
    service_type_list?: [];
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：服务评价列表
 * 描述
 */
export class ServiceEvaluateView extends ReactView<ServiceEvaluateViewControl, ServiceEvaluateViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '服务项目',
        dataIndex: 'product_name',
        key: 'product_name',
    }, {
        title: '服务供应商',
        dataIndex: 'provider_name',
        key: 'provider_name',
    }, {
        title: '下单时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '派单时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '服务人员',
        dataIndex: 'servicer_name',
        key: 'servicer_name',
    }, {
        title: '服务开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '服务结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '服务评价状态',
        dataIndex: 'status',
        key: 'status',
        render: (text: string, record: any) => {
            if ((record.hasOwnProperty('service_attitude') && record['service_attitude'] !== '') || (record.hasOwnProperty('service_quality') && record['service_quality'] !== '') || (record.hasOwnProperty('opinion_remarks') && record['opinion_remarks'] !== '')) {
                return '已评价';
            } else {
                return '未评价';
            }
        }
    }, {
        title: '服务态度',
        dataIndex: 'service_attitude',
        key: 'service_attitude',
    }, {
        title: '服务质量',
        dataIndex: 'service_quality',
        key: 'service_quality',
    }, {
        title: '评价内容',
        dataIndex: 'opinion_remarks',
        key: 'opinion_remarks',
    }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            service_type_list: [],
            org_list: []
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name",
                    option: {
                        placeholder: "请输入长者姓名",
                    }
                },
                // {
                //     type: InputType.input,
                //     label: "服务供应商",
                //     decorator_id: "provider_name",
                //     option: {
                //         placeholder: "请输入服务供应商",
                //     }
                // }, 
                {
                    type: InputType.tree_select,
                    label: "服务供应商",
                    col_span: 8,
                    decorator_id: "provider_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择服务供应商",
                    },
                },
                {
                    type: InputType.select,
                    label: "评价状态",
                    decorator_id: "is_evaluate",
                    option: {
                        placeholder: "请选择评价状态",
                        childrens: ['未评价', '已评价'].map((item, idx) => {
                            return (
                                <Option key={item}>{item}</Option>
                            );
                        }),
                    }
                }, {
                    type: InputType.rangePicker,
                    label: "下单时间",
                    decorator_id: "date_range"
                },
            ],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: 'get_service_evaluate_list',
                    service_condition: []
                }
            },
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：服务评价列表
 * 描述
 */
@addon('ServiceEvaluateView', '服务评价列表', '描述')
@reactControl(ServiceEvaluateView, true)
export class ServiceEvaluateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}