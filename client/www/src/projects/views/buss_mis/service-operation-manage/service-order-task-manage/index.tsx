import { Modal, Row, Select, message } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info, getAge } from "src/projects/app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { request } from "src/business/util_tool";

const { Option } = Select;
/**
 * 组件：服务订单分派任务列表状态
 */
export interface ServiceOrderTaskViewState extends ReactViewState {
    // 选择行的Id集合
    ids?: string[];
    // 任务项目集合
    task_list?: any[];
    // 任务总数总条数
    total?: number;
    // 弹出框是否显示
    modal_visible?: boolean;
    // id集合
    id?: string;
    // 可服务人员
    task_receive_person?: any;
    // 服务项目类别
    service_item_category?: [];
    // 单个项目
    item?: any;
    /** 接口名 */
    request_url?: string;
    // 弹出框是否显示
    batch_modal_visible?: boolean;
    // 多选项目行
    item_list?: any[];
    // is_send
    is_send?: boolean;
    // 撤销派工弹窗
    modal_cancel_assign_visible?: boolean;
}

/**
 * 组件：服务订单分派任务列表
 * 服务订单分派任务列表
 */
export class ServiceOrderTaskView extends ReactView<ServiceOrderTaskViewControl, ServiceOrderTaskViewState> {
    private formCreator: any = null;
    private child_formCreator: any = null;
    private columns_data_source = [{
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    }, {
        title: '长者姓名',
        dataIndex: 'purchaser',
        key: 'purchaser',
    }, {
        title: '镇街',
        dataIndex: 'admin_zj_name',
        key: 'admin_zj_name',
        render: (text: any, record: any, index: number) => {
            if (record.admin_name === '佛山市') {
                return '佛山市';
            } else {
                return text;
            }
        }
    }, {
        title: '服务供应商',
        dataIndex: 'service_provider',
        key: 'service_provider',
    },
    {
        title: '服务项目',
        dataIndex: 'origin_product.product_name',
        key: 'origin_product.product_name',
    },
    {
        title: '服务地址',
        dataIndex: 'purchaser_address',
        key: 'purchaser_address',
    },
    {
        title: '下单时间',
        dataIndex: 'order_date',
        key: 'order_date',
    },
    // {
    //     title: '分派状态',
    //     dataIndex: 'assign_state',
    //     key: 'assign_state',
    //     render: (text: string, record: any) => {
    //         if (text !== '已分派') {
    //             return '待分派';
    //         } else {
    //             return text;
    //         }
    //     }
    // },
    {
        title: '订单状态',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: '服务开始时间',
        dataIndex: 'service_date',
        key: 'service_date',
    },
    {
        title: '服务人员',
        dataIndex: 'service_person',
        key: 'service_person',
    },
    {
        title: '备注',
        dataIndex: 'comment',
        key: 'comment',
    },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            item: {},
            task_receive_person: [],
            modal_visible: false,
            batch_modal_visible: false,
            service_item_category: [],
            id: '',
            request_url: '',
            is_send: false,
            modal_cancel_assign_visible: false,
        };
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 撤销派工取消回调方法 */
    handleAssignCancel = (e: any) => {
        this.setState({
            modal_cancel_assign_visible: false,
        });
    }
    cancelAssign = (e: any) => {
        let { item, is_send } = this.state;
        if (!item.id) {
            message.error('数据出错！');
            return;
        }
        if (is_send === true) {
            message.error('请勿操作过快！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.service_order_task_service.cancel_assign_service_order_task_manage!(item.id))
                    .then((data: any) => {
                        if (data === 'Success') {
                            this.setState({
                                modal_cancel_assign_visible: false,
                                is_send: false,
                            });
                            message.success('撤销派工成功', 3, () => {
                                const _this = this.child_formCreator;
                                _this.select_func([_this.state.condition, _this.state.page, _this.state.pageSize]);
                            });
                        } else {
                            this.setState({
                                is_send: false,
                            });
                            message.error('撤销派工失败！' + data);
                        }
                    }).catch((error: Error) => {
                        this.setState({
                            is_send: false,
                        });
                        message.error('撤销派工失败！' + error.message);
                    });
            }
        );
    }
    /** 批量分派弹框取消回调方法 */
    batchHandleCancel = (e: any) => {
        this.setState({
            batch_modal_visible: false,
        });
    }
    get_child_component = (e: any) => {
        this.child_formCreator = e;
    }
    /** 指派任务 */
    handleSubmit = (e: any, value: any) => {
        let { item, is_send } = this.state;
        if (!item.id || !item.origin_product.product_id || !item.organization_id) {
            message.error('数据出错！');
            return;
        }
        if (is_send === true) {
            message.error('请勿操作过快！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let param: any = {
                    // 组装要传入服务记录接口的数据
                    record_data: {
                        order_id: item.id,
                        product_id: item.origin_product.product_id,
                        other: {
                            servicer_id: value.task_receive_person,
                            // servicer_id: item.organization_id,
                        }
                    },
                    // 构建要创建任务的接口数据
                    task_data: {
                        // 任务名称
                        'task_name': item.origin_product.product_name,
                        // 任务内容
                        'task_content': item.origin_product.product_name,
                        // 任务派单者
                        'task_receive_person': value.task_receive_person,
                        // 任务地址
                        'task_address': item.address,
                        // 任务坐标
                        'task_location': item.address,
                        // 任务备注
                        'remark': item.comment,
                        // 主键
                        'task_object_id': item.id,
                    }
                };
                request(this, AppServiceUtility.service_order_task_service.update_service_order_task_manage!([param]))
                    .then((data: any) => {
                        if (data === 'Success') {
                            this.setState({
                                modal_visible: false,
                                is_send: false,
                            });
                            message.success('操作成功', 3, () => {
                                const _this = this.child_formCreator;
                                _this.select_func([_this.state.condition, _this.state.page, _this.state.pageSize]);
                            });
                        } else {
                            this.setState({
                                is_send: false,
                            });
                            message.error('派工失败！' + data);
                        }
                    }).catch((error: Error) => {
                        this.setState({
                            is_send: false,
                        });
                        message.error('派工失败！' + error.message);
                    });
            }
        );
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
        // 获取服务项目类型
        request(this, AppServiceUtility.service_order_task_service.get_services_item_category_list!()!)
            .then((datas: any) => {
                this.setState({
                    service_item_category: datas,
                });
            });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if (contents.hasOwnProperty('status') && contents['status'] === '服务中') {
            message.info('该订单已分派！');
            return;
        }
        if ('icon_edit' === type) {
            this.setState({ modal_visible: true, item: contents });
        }
        if ('icon_cancel_assign' === type) {
            this.setState({ modal_cancel_assign_visible: true, item: contents });
        }
    }
    /** 批量派工弹框事件 */
    batchTask = () => {
        let { item_list } = this.state;
        if (!item_list || item_list!.length === 0) {
            message.error('先选中所需分派订单！');
        } else {
            let flag = true;
            for (let index = 0; index < item_list.length; index++) {
                const record = item_list[index];
                if (record['status'] !== '未服务') {
                    message.error('所选择的第' + index + 1 + '行数据已服务，不能再进行分派！');
                    flag = false;
                }
            }
            if (flag) {
                this.setState({ batch_modal_visible: true });
            }
        }
    }
    /** 批量派工执行事件 */
    batchSubmit = (e: any, value: any) => {
        let { item_list, is_send } = this.state;
        if (is_send === true) {
            message.error('请勿操作过快！');
            return;
        }
        if (item_list && item_list!.length > 0) {
            this.setState(
                {
                    is_send: true,
                },
                () => {
                    let params = [];
                    for (let index = 0; index < item_list!.length; index++) {
                        const item = item_list![index];
                        let param = {
                            // 组装要传入服务记录接口的数据
                            record_data: {
                                order_id: item.id,
                                product_id: item.origin_product.product_id,
                                other: {
                                    servicer_id: value.task_receive_person,
                                    // servicer_id: item.organization_id,
                                }
                            },
                            // 构建要创建任务的接口数据
                            task_data: {
                                // 任务名称
                                'task_name': item.origin_product.product_name,
                                // 任务内容
                                'task_content': item.origin_product.product_name,
                                // 任务派单者
                                'task_receive_person': value.task_receive_person,
                                // 任务地址
                                'task_address': item.address,
                                // 任务坐标
                                'task_location': item.address,
                                // 任务备注
                                'remark': item.comment,
                                // 主键
                                'task_object_id': item.id,
                            }
                        };
                        params.push(param);
                    }

                    request(this, AppServiceUtility.service_order_task_service.update_service_order_task_manage!(params))
                        .then((data: any) => {
                            if (data === 'Success') {
                                this.setState({
                                    batch_modal_visible: false,
                                    is_send: false,
                                });
                                message.info('操作成功！', 3, () => {
                                    const _this = this.child_formCreator;
                                    _this.select_func([_this.state.condition, _this.state.page, _this.state.pageSize]);
                                });
                            } else {
                                this.setState({
                                    is_send: false,
                                });
                                message.info(data);
                            }
                        }).catch((error: Error) => {
                            this.setState({
                                is_send: false,
                            });
                            message.error('创建失败！' + error.message);
                        });
                }
            );
        } else {
            message.error('先选中所需分派订单！');
        }
    }
    /** 行选中事件 */
    onRowSelection = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({
            item_list: selectedRowKeys
        });
    }
    changeValue = (e: any, data_info: any) => {
        if (data_info) {
            this.formCreator.setChildFieldsValue({
                'telephone': data_info['personnel_info'] && data_info['personnel_info']['telephone'] ? data_info['personnel_info']['telephone'] : '',
                'sex': data_info['personnel_info'] && data_info['personnel_info']['sex'] ? data_info['personnel_info']['sex'] : '',
                'age': getAge(data_info['personnel_info'] && data_info['personnel_info']['id_card'] ? data_info['personnel_info']['id_card'] : ''),
            });
        }
    }
    onRef = (ref: any) => {
        this.formCreator = ref;
    }
    render() {
        let item = this.state.item;
        let service_status = [
            {
                value: '未服务',
                name: '未服务',
            },
            {
                value: '服务中',
                name: '服务中',
            },
            {
                value: '已完成',
                name: '已完成',
            },
        ];
        let service_status_dow: JSX.Element[] = service_status.map((value: any) => {
            return (
                <Option key={value.value}>{value.name}</Option>
            );
        });
        let assign_status = [
            {
                value: '已分派',
                name: '已分派',
            },
            {
                value: '未分派',
                name: '未分派',
            }
        ];
        let assign_status_dow: JSX.Element[] = assign_status.map((value: any) => {
            return (
                <Option key={value.value}>{value.name}</Option>
            );
        });
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "服务人员姓名",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "服务人员身份证",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: [
                // {
                //     title: '编号',
                //     dataIndex: 'code',
                //     key: 'code',
                // }, 
                {
                    title: '姓名',
                    dataIndex: 'name',
                    key: 'name',
                }, {
                    title: '身份证号',
                    dataIndex: 'id_card',
                    key: 'id_card',
                },],
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_worker_list_all',
            title: '服务人员查询',
            select_option: {
                placeholder: "请选择服务人员",
            }
        };
        let category = this.state.service_item_category;
        let service_category: any[] = [];
        category!.map((item: any) => {
            service_category.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let task_option = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者姓名",
                    decorator_id: "purchaser_name"
                },
                {
                    type: ListInputType.input,
                    label: "镇街",
                    decorator_id: "admin_zj_name"
                },
                {
                    type: ListInputType.rangePicker,
                    label: "订单时间",
                    decorator_id: "order_date"
                },
                {
                    type: InputType.select,
                    label: "订单状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择订单状态",
                        showSearch: true,
                        childrens: service_status_dow,
                    }
                },
                {
                    type: ListInputType.input,
                    label: "服务供应商",
                    decorator_id: "organization_name"
                },
                {
                    type: ListInputType.select,
                    label: "分派状态",
                    decorator_id: "is_assign",
                    option: {
                        placeholder: "请选择分派状态",
                        childrens: assign_status_dow,
                        allowClear: true
                    }
                },
                {
                    type: ListInputType.input,
                    label: "订单编号",
                    decorator_id: "transation_code",
                    option: {
                        placeholder: "请输入订单编号"
                    }
                },
                {
                    type: ListInputType.input,
                    label: "服务人员",
                    decorator_id: "service_person",
                    option: {
                        placeholder: "请输入服务人员姓名"
                    }
                }
            ],
            btn_props: [{
                label: '批量派工',
                btn_method: this.batchTask,
                icon: ''
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            on_row_selection: this.onRowSelection,
            onRef: this.get_child_component,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: this.state.request_url ? this.state.request_url : '',
                    service_condition: [{ need_fp: true }, 1, 10]
                },
            },
            searchExtraParam: { need_fp: true },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            // add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let new_table_param: any = {
            other_label_type: [
                {
                    type: 'button',
                    label_text: '',
                    label_key: 'icon_edit',
                    label_parameter: { size: 'small' },
                    label_text_func(record: any) {
                        // if (record.hasOwnProperty('assign_state') && record['assign_state'] === '待分派') {
                        //     return record.hasOwnProperty('status') && record['status'] === '未服务' ? '派工' : undefined;
                        // }
                        // return undefined;
                        return record.hasOwnProperty('status') && record['status'] === '未服务' ? '派工' : undefined;
                    }
                },
                {
                    type: 'button',
                    label_text: '',
                    label_key: 'icon_cancel_assign',
                    label_parameter: { size: 'small' },
                    label_text_func(record: any) {
                        return record.hasOwnProperty('assign_state') && record['assign_state'] === '已分派' && record.hasOwnProperty('status') && record['status'] === '未服务' ? '撤销派工' : undefined;
                    }
                }
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        const layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        let task_list = Object.assign(task_option, new_table_param);
        let edit_props = {
            form_items_props: [
                {
                    title: "订单信息",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "长者姓名",
                                        decorator_id: "purchaser",
                                        layout: layout,
                                        field_decorator_option: {
                                            initialValue: item && item.purchaser,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true,
                                        }
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "服务项目",
                                        decorator_id: "product_name",
                                        layout: layout,
                                        field_decorator_option: {
                                            initialValue: item && item.origin_product && item.origin_product.product_name,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true,
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "服务供应商",
                                        decorator_id: "service_provider",
                                        layout: layout,
                                        field_decorator_option: {
                                            initialValue: item && item.service_provider,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true,
                                        }
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "服务地址",
                                        decorator_id: "purchaser_address",
                                        layout: layout,
                                        field_decorator_option: {
                                            initialValue: item ? (item.address ? item.address : (item.purchaser_address ? item.purchaser_address : '')) : '',
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true,
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "下单时间",
                                        decorator_id: "order_date",
                                        layout: layout,
                                        field_decorator_option: {
                                            initialValue: item && item.order_date,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true,
                                        }
                                    },
                                ]
                            }
                        },
                    ]
                },
                {
                    title: "派工信息",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.modal_search,
                                        col_span: 12,
                                        label: "服务人员",
                                        decorator_id: "task_receive_person",
                                        layout: layout,
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请选择服务人员" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请选择服务人员",
                                            modal_search_items_props: modal_search_items_props,
                                            onChange: this.changeValue
                                        }
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "手机号码",
                                        decorator_id: "telephone",
                                        layout: layout,
                                        field_decorator_option: {
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "性别",
                                        decorator_id: "sex",
                                        layout: layout,
                                        field_decorator_option: {
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true
                                        }
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "年龄",
                                        decorator_id: "age",
                                        layout: layout,
                                        field_decorator_option: {
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            disabled: true
                                        }
                                    },
                                ]
                            }
                        },
                    ]
                },
            ],
            submit_btn_propps: {
                text: "分派",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.service_order_task_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.setState({ modal_visible: false, }); },
            id: this.props.match!.params.key,
            onRef: this.onRef,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        let batch_props = {
            form_items_props: [
                {
                    title: "派工信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "服务人员",
                            decorator_id: "task_receive_person",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务人员" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务人员",
                                modal_search_items_props: modal_search_items_props
                            }
                        },
                    ]
                },
            ],
            submit_btn_propps: {
                text: "分派",
                cb: this.batchSubmit
            },
            service_option: {
                service_object: AppServiceUtility.service_order_task_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.setState({ modal_visible: false, }); },
            id: this.props.match!.params.key,
        };
        let batch_props_list = Object.assign(batch_props, edit_props_info);
        return (
            <Row>
                <Modal
                    visible={this.state.modal_visible}
                    onCancel={this.handleCancel}
                    width={900}
                    okButtonProps={{ disabled: true, ghost: true }}
                    cancelButtonProps={{ disabled: true, ghost: true }}
                >
                    <FormCreator {...edit_props_list} />
                </Modal>
                <Modal
                    visible={this.state.batch_modal_visible}
                    onCancel={this.batchHandleCancel}
                    width={900}
                    okButtonProps={{ disabled: true, ghost: true }}
                    cancelButtonProps={{ disabled: true, ghost: true }}
                >
                    <FormCreator {...batch_props_list} />
                </Modal>
                <Modal
                    visible={this.state.modal_cancel_assign_visible}
                    onCancel={this.handleAssignCancel}
                    onOk={this.cancelAssign}
                    width={500}
                >
                    <p>订单号：{this.state.item.order_code}是否撤销该订单的派工记录</p>
                </Modal>
                <SignFrameLayout {...task_list} />
            </Row >
        );
    }
}

/**
 * 控件：服务订单分派任务列表
 * 服务订单分派任务列表
 */
@addon('ServiceOrderTaskView', '服务订单分派任务列表', '服务订单分派任务列表')
@reactControl(ServiceOrderTaskView, true)
export class ServiceOrderTaskViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}