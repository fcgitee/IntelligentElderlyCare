import { Select, Result } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Ref, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { remote } from 'src/projects/remote';
import { Steps } from 'antd';
import { IUserService } from "src/business/models/user";
import { request } from "../../../../../business/util_tool";

const { Step } = Steps;

let { Option } = Select;

/**
 * 组件：服务人员申请编辑页面状态
 */
export interface ServicePersonalApplyViewState extends ReactViewState {
    /** 服务人员列表 */
    provider_list?: [];
    /**
     * 申请状态
     */
    status?: number;

    /**
     * 表单权限控制
     */
    disabled?: boolean;
    /** 服务人员能力级别 */
    servicer_ability_level?: any[];
    /** 服务人员信用级别 */
    servicer_credit_level?: any[];
    user_info?: any;
    /** 接口名 */
    request_url?: string;
    org_list?: any;
}

/**
 * 组件：服务人员申请编辑页面
 * 描述
 */
export class ServicePersonalApplyView extends ReactView<ServicePersonalApplyViewControl, ServicePersonalApplyViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            provider_list: [],
            status: 0,
            disabled: false,
            servicer_ability_level: ["一级", "二级", "三级"],
            servicer_credit_level: ["一级", "二级", "三级"],
            user_info: {},
            request_url: '',
            org_list: [],
        };
    }
    userService?() {
        return getObject(this.props.userService_Fac!);
    }
    componentDidMount() {
        // let step = new Map([['',0],['正在申请',1],['同意',2],['不同意',2]]);
        // request(this, AppServiceUtility.service_operation_service['get_current_service_provider']!({}, 1, 999))
        //     .then((datas: any) => {
        //         // console.log('datas.result', datas.result);
        //         // console.log(step.get(datas.result[0].status));
        //         // let num=step.get(datas.result[0].status);
        //         this.setState({
        //             provider_list: [],
        //             // status: num,
        //             // disabled: num !== 0 || datas.result[0].status === '不同意',
        //         });
        //     });
        request(this, AppServiceUtility.person_org_manage_service.get_current_user_info!())
            .then((data: any) => {
                // console.log(data[0]);
                this.setState({
                    user_info: data[0],
                });
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const servicer_ability_level: JSX.Element[] = this.state.servicer_ability_level!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        const servicer_credit_level: JSX.Element[] = this.state.servicer_credit_level!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>);
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let edit_props: any = {
            form_items_props: [
                {
                    title: "服务人员申请信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "姓名",
                            decorator_id: "servicer_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入姓名" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                // disabled: true,
                                placeholder: "请输入姓名",
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "性别",
                            decorator_id: "servicer_sex",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择性别" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '男',
                                    value: '男',
                                }, {
                                    label: '女',
                                    value: '女',
                                }]
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "身份证",
                            decorator_id: "servicer_id_card",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入身份证" }],
                                initialValue: this.state.user_info.id_card,
                            } as GetFieldDecoratorOptions,
                            option: {
                                // disabled: true,
                                placeholder: "请输入身份证",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "手机号码",
                            decorator_id: "servicer_phone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入手机号码" }],
                                initialValue: this.state.user_info.mobile,
                            } as GetFieldDecoratorOptions,
                            option: {
                                // disabled: true,
                                placeholder: "请输入手机号码",
                            }
                        },

                        {
                            type: InputType.antd_input,
                            label: "邮箱",
                            decorator_id: "servicer_email",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入邮箱" }],
                                initialValue: this.state.user_info.email,
                            } as GetFieldDecoratorOptions,
                            option: {
                                // disabled: true,
                                placeholder: "请输入邮箱",
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "家庭住址",
                            decorator_id: "servicer_address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入家庭住址" }],
                                initialValue: this.state.user_info.address,
                            } as GetFieldDecoratorOptions,
                            option: {
                                // disabled: true,
                                placeholder: "请输入家庭住址",
                            }
                        },
                        {
                            type: InputType.tree_select,
                            label: "所属组织机构",
                            decorator_id: "servicer_organization_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择所属组织机构" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.org_list,
                                placeholder: "请选择上级组织机构",
                            },
                        },
                        {
                            type: InputType.select,
                            label: "服务人员能力级别",
                            decorator_id: "servicer_ability_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务人员能力级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务人员能力级别",
                                childrens: servicer_ability_level,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.select,
                            label: "服务人员信用级别",
                            decorator_id: "servicer_credit_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务人员信用级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务人员信用级别",
                                childrens: servicer_credit_level,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "资质(资格证)照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "servicer_qualifications_photos",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请上传资质(资格证)照片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "合同照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "servicer_contract_photos",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请上传合同照片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "服务机构合同条款",
                            decorator_id: "servicer_contract_clauses",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务机构合同条款" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务机构合同条款",
                                rows: 3,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "servicer_remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                rows: 3,
                                disabled: this.state.disabled,
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.servicePersonalManage);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: 1 }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_personal_apply"
                    }
                },
                service_reject: (error: Error) => {
                    // console.log(error);
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.servicePersonalManage); },
            id: this.props.match!.params.key,
        };
        if (this.state.status !== 0) {
            delete edit_props.submit_btn_propps;
        }
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        let steps = [
            {
                title: '填写申请资料',
                content: (<FormCreator {...edit_props_list} />)
            },
            {
                title: '平台审核',
                content: (<FormCreator {...edit_props_list} />)
            },
            {
                title: '区民政审核',
                content: (<FormCreator {...edit_props_list} />)
            },
            {
                title: '审核完成',
                content: (<Result status="success" title="结果为：同意" subTitle="恭喜你" />)
            },
        ];
        return (
            <MainContent>
                <MainCard title='服务人员申请'>
                    <Steps current={this.state.status}>
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>
                    <MainContent />
                    {steps[this.state.status!].content}
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：服务人员登记编辑页面
 * 描述
 */
@addon('ServicePersonalApplyView', '服务人员登记编辑页面', '描述')
@reactControl(ServicePersonalApplyView, true)
export class ServicePersonalApplyViewControl extends ReactViewControl {
    /** 用户服务 */
    public userService_Fac?: Ref<IUserService>;
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}