import { message, Select, Modal, Button, Form } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request, exprot_excel } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { changeNameforStatus } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";

let { Option } = Select;
/**
 * 组件：服务订单信息状态
 */
export interface ServiceOrderViewState extends ReactViewState {/** 选择行Ids */
    select_ids?: string[];
    request_ulr?: string;
    delete_modal_visible?: boolean;
    order_id?: string;
    commentShow?: any;
    comment?: any;
    org_list?: any;
}

/**
 * 组件：服务订单记信息
 * 描述
 */
export class ServiceOrderView extends ReactView<ServiceOrderViewControl, ServiceOrderViewState> {
    private columns_data_source = [{
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    }, {
        title: '长者姓名',
        dataIndex: 'purchaser',
        key: 'purchaser',
    }, {
        title: '服务机构',
        dataIndex: 'service_provider',
        key: 'service_provider',
    }, {
        title: '服务项目',
        dataIndex: 'origin_product.product_name',
        key: 'origin_product.product_name',
    }, {
        title: '订单金额',
        dataIndex: 'amout',
        key: 'amout',
    }, {
        title: '下单时间',
        dataIndex: 'order_date',
        key: 'order_date',
    }, {
        title: '服务开始时间',
        dataIndex: 'service_date',
        key: 'service_date',
    }, {
        title: '服务结束时间',
        dataIndex: 'service_end_date',
        key: 'service_end_date',
    }, {
        title: '订单状态',
        dataIndex: 'status',
        key: 'status',
        render: (text: string, record: any, index: any) => {
            return (
                changeNameforStatus(text)
            );
        }
    },
    {
        title: '备注',
        dataIndex: '',
        key: '',
        render: (text: string, record: any, index: any) => (
            <Button type="primary" onClick={() => { this.buttonClick(record); }}>查看备注</Button>
        )
    },
        // {
        //     title: '订单来源',
        //     dataIndex: 'buy_type',
        //     key: 'buy_type',
        // },
    ];
    private formCreator: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            delete_modal_visible: false,
            order_id: '',
            org_list: [],
            commentShow: false,
            comment: ''
        };
    }

    // 查看备注
    buttonClick = (record: any) => {
        this.setState({
            commentShow: true,
            comment: record.comment
        });
    }
    commentClose = () => {
        this.setState({
            commentShow: false
        });
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceOrder);
    }
    /** 创建服务记录 */
    creatRecord = () => {
        let ids = this.state.select_ids;
        if (ids && ids!.length > 0) {
            request(this, AppServiceUtility.service_operation_service.add_service_record!(ids))
                .then((data: any) => {
                    if (data === 'Success') {
                        message.info('创建成功');
                    } else {
                        message.info('创建失败');
                    }
                }).catch((error: Error) => {
                    message.error('创建失败！' + error.message);
                });
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // if ('icon_delete' === type) {
        //     this.setState({
        //         order_id: contents.id,
        //         delete_modal_visible: true,
        //     });
        // }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        // 获取机构列表
        // request(this, AppServiceUtility.person_org_manage_service.get_organization_list_all!({})!)
        //     .then((datas: any) => {
        //         if (datas && datas.result && datas.result.length) {
        //             this.setState({
        //                 org_list: datas.result,
        //             });
        //         }
        //     });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    handleOk = () => {
        this.setState({
            delete_modal_visible: false,
        });
        AppServiceUtility.service_operation_service.delete_service_order!([this.state.order_id!])!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('删除成功');
                    this.formCreator && this.formCreator.reflash();
                } else {
                    message.info(data);
                }
            });
    }
    handleCancel = () => {
        this.setState({
            delete_modal_visible: false,
            order_id: '',
        });
    }
    on_click_del = (contents: any) => {
        this.setState({
            order_id: contents.id,
            delete_modal_visible: true,
        });
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }

    // 下载
    download = () => {
        let _this = this.formCreator;
        AppServiceUtility.service_operation_service.get_service_order_fwyy_fwdd_list!(_this.state.condition)!
            .then((data: any) => {
                // console.log('服务订单', data);
                if (data.result.length) {
                    let new_data: any = [];
                    let file_name = '服务订单汇总表';
                    data.result.map((item: any) => {
                        new_data.push({
                            "订单编号": item.order_code ? item.order_code : '',
                            "长者姓名": item.purchaser ? item.purchaser : '',
                            "服务机构": item.service_provider ? item.service_provider : '',
                            "服务项目": item.origin_product && item.origin_product.product_name ? item.origin_product.product_name : '',
                            "订单金额": item.amout ? item.amout : '',
                            "下单时间": item.order_date ? item.order_date : '',
                            "服务时间": item.service_date ? item.service_date : '',
                            "服务结束": item.service_end_date ? item.service_end_date : '',
                            "订单状态": item.status ? changeNameforStatus(item.status) : '',
                            "备注": item.comment ? item.comment : ''
                        });

                    });
                    exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
                } else {
                    message.info('暂无数据可导', 2);
                }
            });
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let provider_type = [
            {
                name: "居家订单",
                value: "服务商"
            }, {
                name: "机构订单",
                value: "福利院"
            }, {
                name: "日托订单",
                value: "幸福院"
            },
        ];
        let service_status = [
            {
                value: '未服务',
                name: '未服务',
            },
            {
                value: '服务中',
                name: '服务中',
            },
            {
                value: '已完成',
                name: '已完成',
            },
        ];

        let provider_type_dow: JSX.Element[] = provider_type.map((value: any) => {
            return (
                <Option key={value.value}>{value.name}</Option>
            );
        });
        let service_status_dow: JSX.Element[] = service_status.map((value: any) => {
            return (
                <Option key={value.value}>{value.name}</Option>
            );
        });
        // let org_list_dow: JSX.Element[] = this.state.org_list.map((value: any) => {
        //     return (
        //         <Option key={value.id}>{value.name}</Option>
        //     );
        // });
        // let table_param = {
        //     other_label_type: [{ type: 'icon', label_key: 'icon_delete', label_parameter: { icon: 'antd@delete' } }],
        //     showHeader: true,
        //     bordered: false,
        //     show_footer: true,
        //     rowKey: 'id',
        // };
        let service_provider = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.download,
                icon: 'download'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "purchaser_name"
                },
                {
                    type: InputType.input,
                    label: "长者身份证",
                    decorator_id: "purchaser_id_card"
                },
                {
                    type: InputType.input,
                    label: "订单编号",
                    decorator_id: "transation_code"
                },
                {
                    type: InputType.rangePicker,
                    label: "下单时间",
                    decorator_id: "order_date"
                },
                // {
                //     type: InputType.rangePicker,
                //     label: "付款时间",
                //     decorator_id: "pay_date"
                // }, {
                //     type: InputType.rangePicker,
                //     label: "服务时间",
                //     decorator_id: "service_date"
                // }, 
                {
                    type: InputType.select,
                    label: "订单状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择订单状态",
                        showSearch: true,
                        childrens: service_status_dow,
                    }
                },
                {
                    type: InputType.select,
                    label: "订单类型",
                    decorator_id: "provider_type",
                    option: {
                        placeholder: "请选择订单类型",
                        childrens: provider_type_dow,
                    }
                },
                // {
                //     type: InputType.input,
                //     label: "服务机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入服务机构名称",
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "服务机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择服务机构",
                    },
                },
            ],
            // btn_props: [{
            //     label: '新增',
            //     btn_method: this.add,
            //     icon: 'plus'
            // }, {
            //     label: '生成服务记录',
            //     btn_method: this.creatRecord,
            //     icon: 'plus'
            // }],
            onRef: this.onRef,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            on_click_del: this.on_click_del,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [this.props.select_param || {}, 1, 10]
                },

            },
            searchExtraParam: this.props.select_param || {},
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            add_permission: this.props.add_permission,
            select_permission: this.props.select_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_order_list = Object.assign(service_provider, table_param);
        return (
            <div>
                <SignFrameLayout {...service_order_list} />
                <Modal
                    title="温馨提醒"
                    visible={this.state.delete_modal_visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p style={{ textAlign: 'center' }}>确定要删除吗？</p>
                </Modal>
                <Modal
                    title="备注"
                    visible={this.state.commentShow}
                    onOk={this.commentClose}
                    onCancel={this.commentClose}
                >
                    <p style={{ textAlign: 'center' }}>{this.state.comment}</p>
                </Modal>
            </div>

        );
    }
}

/**
 * 控件：服务商登记信息
 * 描述
 */
@addon('ServiceOrderView', '服务商登记信息', '描述')
@reactControl(Form.create<any>()(ServiceOrderView), true)
export class ServiceOrderViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 撤销权限 */
    public delete_permission?: string;
    /** 额外参数 */
    public select_param?: any;
    /** 新增权限 */
    public add_permission?: string;
}