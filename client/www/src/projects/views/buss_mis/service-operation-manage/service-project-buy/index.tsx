import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission } from "pao-aop";
import { Row, Col, Pagination } from "antd";
import './index.less';
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：服务套餐列表页状态
 */
export interface ServerProjectState extends ReactViewState {
    /** 页面列表数据 */
    listdata?: any;
    /** 当前选中的数据 */
    active?: string;
    /** 头部条件数据 */
    headerdata?: any;
    /** 当前页数 */
    current: number;
    /** 一页展示多少条数据 */
    PageSize: number;
    /** 接口名 */
    request_url?: string;
    total?: number;
}

/**
 * 组件：服务套餐列表页
 */
export class ServerProject extends ReactView<ServiceProjectControl, ServerProjectState> {
    constructor(props: any) {
        super(props);
        this.state = {
            listdata: [],
            active: '',
            headerdata: [
                {
                    title: '条件',
                    data: ['综合', '距离', '预约']
                }
            ],
            current: 1,
            PageSize: 10,
            total: 0,
            request_url: '',
        };
    }
    onChange = (page: number, pageSize: number) => {
        this.selectdata(page, pageSize);
    }
    onShowSizeChange = (current: number, pageSize: number) => {
        this.selectdata(current, pageSize);
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
        this.selectdata(this.state.current, this.state.PageSize);
    }
    datalist(item: any) {
        this.setState({
            active: item
        });
        this.selectdata(this.state.current, this.state.PageSize);
    }
    selectdata = (current: number, pagesize: number) => {
        // if (this.props.edit_permission) {
        current = current || this.state.current;
        pagesize = pagesize || this.state.PageSize;
        AppServiceUtility.service_item_service.get_service_product_package_list!({}, current, pagesize)!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    listdata: data.result,
                    total: data.total,
                });
            })
            .catch((err) => {
                console.info(err);
            });
        // }
    }
    dealdata = (data: any) => {
        this.props.history!.push(ROUTE_PATH.ServiceProjectDetail + '/' + data.id);
    }
    render() {
        // console.log(this.state);
        return (
            <div className="service-project-buy">
                <h4>服务套餐</h4>
                <Row gutter={46} style={{ margin: '0' }}>
                    {
                        this.state.headerdata!.map((item: any, index: number) => {
                            return (
                                <div key={index} className="header-list">
                                    <Col className="header-subtitle">{item.title}</Col>
                                    {
                                        item.data.map((data: any, inde: number) => {
                                            return (
                                                <Col key={inde} className={this.state.active === data ? 'header-condition active' : 'header-condition'} onClick={() => this.datalist(data)}>
                                                    {data}
                                                </Col>
                                            );
                                        })
                                    }
                                </div>
                            );
                        })
                    }
                </Row>
                <Row gutter={16} className="list-content">
                    {
                        this.state.listdata!.map((item: any, index: number) => {
                            return (
                                <Col key={index} className="projectlist" onClick={() => { this.dealdata(item); }}>
                                    <div className="project-section">
                                        <div className="picture">
                                            <img src={item.picture_collection.length > 0 && item.picture_collection ? item.picture_collection[0] : ''} />
                                        </div>
                                        <div className="detail">
                                            <div className="price">
                                                ￥<strong>{item.total_price}</strong>
                                            </div>
                                            <p className="projectinfotitle">{item.name}</p>
                                            {
                                                item.organization_name && item.organization_name.length > 0 ? item.organization_name!.map((names: any, count: number) => {
                                                    return (
                                                        <p key={count} className="projectinfo">
                                                            {names}
                                                        </p>
                                                    );
                                                }) : ''
                                            }
                                        </div>
                                    </div>
                                </Col>
                            );
                        })
                    }
                </Row>
                <Pagination
                    showSizeChanger={true}
                    onChange={this.onChange}
                    onShowSizeChange={this.onShowSizeChange}
                    total={this.state.total}
                    pageSize={this.state.PageSize}
                />
            </div>
        );
    }
}

/**
 * 组件：服务套餐列表页
 * 服务套餐列表页
 */
@addon('ServerProject', '服务套餐列表页', '描述')
@reactControl(ServerProject, true)
export class ServiceProjectControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}
