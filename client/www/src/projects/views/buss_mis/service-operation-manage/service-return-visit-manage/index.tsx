import { Select, Row, Modal, Form, Input, message } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState, CookieUtil } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import TextArea from 'antd/lib/input/TextArea';
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
import { request } from 'src/business/util_tool';
import moment from 'moment';
import { User } from 'src/business/models/user';

let { Option } = Select;
/**
 * 组件：服务回访列表状态
 */
export interface ServiceReturnVisitListViewState extends ReactViewState {
    /** 所选服务记录列表 */
    item_list?: [];
    /** 服务类型列表 */
    service_type_list?: [];
    modal_visible?: boolean;
    visit_id?: string;
    is_send?: boolean;
    /* 组织机构 */
    org_list?: any;
    batch_modal_visible?: boolean;
}

/**
 * 组件：服务回访列表
 * 描述
 */
export class ServiceReturnVisitListView extends ReactView<ServiceReturnVisitListViewControl, ServiceReturnVisitListViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '工单编号',
            dataIndex: 'record_code',
            key: 'record_code',
        }, {
            title: '长者姓名',
            dataIndex: 'elder_name',
            key: 'elder_name',
        }, {
            title: '性别',
            dataIndex: 'elder_sex',
            key: 'elder_sex',
        }, {
            title: '长者号码',
            dataIndex: 'elder_telephone',
            key: 'elder_telephone',
        }, {
            title: '服务供应商',
            dataIndex: 'provider_name',
            key: 'provider_name',
        }, {
            title: '服务项目',
            dataIndex: 'product_name',
            key: 'product_name',
        }, {
            title: '服务人员',
            dataIndex: 'servicer_name',
            key: 'servicer_name',
        }, {
            title: '服务完成时间',
            dataIndex: 'end_date',
            key: 'end_date',
        }, {
            title: '服务满意度',
            dataIndex: 'service_satisfaction',
            key: 'service_satisfaction',
        },
        // {
        //     title: '回访满意度',
        //     dataIndex: 'visit_satisfaction',
        //     key: 'visit_satisfaction',
        // },
        {
            title: '其他评价',
            dataIndex: 'other_satisfaction',
            key: 'other_satisfaction',
        }, {
            title: '回访状态',
            dataIndex: 'visit_status',
            key: 'visit_status',
        }, {
            title: '回访时间',
            dataIndex: 'visit_date',
            key: 'visit_date',
        }, {
            title: '回访人',
            dataIndex: 'visit_name',
            key: 'visit_name',
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            service_type_list: [],
            modal_visible: false,
            batch_modal_visible: false,
            visit_id: '',
            is_send: false,
            org_list: []
        };
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }

    /** 模版选择框取消回调方法 */
    handleOk(e: any) {
        this.handleSubmit(e);
    }
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 模版选择框取消回调方法 */
    handleBatchOk(e: any) {
        this.handleBatchSubmit(e);
    }
    handleBatchCancel = (e: any) => {
        this.setState({
            batch_modal_visible: false,
        });
    }
    visitThis(id: string) {
        this.setState({
            visit_id: id,
            modal_visible: true,
        });
    }
    onIconClick = (type: string, contents: any) => {
        this.setState({
            visit_id: contents.id,
            modal_visible: true,
        });
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
    }
    handleSubmit = (e: any) => {
        let { is_send, visit_id } = this.state;
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        if (visit_id === '') {
            message.info('请选择要回访的工单！');
            return;
        }
        e && e.preventDefault && e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {

                let param: any = {
                    id: [visit_id],
                    service_satisfaction: values['service_satisfaction'],
                    visit_satisfaction: values['visit_satisfaction'],
                    other_satisfaction: values['other_satisfaction'],
                };
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.service_operation_service.update_service_return_visit!(param))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    this.setState(
                                        {
                                            modal_visible: false,
                                        },
                                        () => {
                                            message.info('提交成功！', 1, () => {
                                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                            });
                                        }
                                    );
                                } else {
                                    this.setState({
                                        is_send: false
                                    });
                                    message.error(datas);
                                }
                            })
                            .catch((error: any) => {
                                message.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }
    handleBatchSubmit = (e: any) => {
        let { is_send, item_list } = this.state;
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        if (item_list === []) {
            message.info('请选择要回访的工单！');
            return;
        }
        let ids: string[] = [];
        item_list!.forEach((item: any) => {
            ids.push(item.id);
        });
        e && e.preventDefault && e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {

                let param: any = {
                    id: ids,
                    service_satisfaction: values['service_satisfaction'],
                    visit_satisfaction: values['visit_satisfaction'],
                    other_satisfaction: values['other_satisfaction'],
                };
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.service_operation_service.update_service_return_visit!(param))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    this.props.form.resetFields();
                                    this.setState(
                                        {
                                            batch_modal_visible: false,
                                            is_send: false,
                                        },
                                        () => {
                                            message.info('提交成功！', 1, () => {
                                                this.formCreator && this.formCreator.reflash && this.formCreator.reflash();
                                            });
                                        }
                                    );
                                } else {
                                    this.setState({
                                        is_send: false
                                    });
                                    message.error(datas);
                                }
                            })
                            .catch((error: any) => {
                                message.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }
    /** 重置服务记录出发操作 */
    undo = () => {
        let { item_list } = this.state;
        if (!item_list || item_list!.length === 0) {
            message.error('先选中所需回访的服务记录！');
        } else {
            this.setState({
                batch_modal_visible: true,
            });
        }
    }
    /** 行选中事件 */
    onRowSelection = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({
            item_list: selectedRowKeys
        });
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "工单编号",
                    decorator_id: "record_code",
                    option: {
                        placeholder: "请输入工单编号",
                    }
                },
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name",
                    option: {
                        placeholder: "请输入长者姓名",
                    }
                },
                {
                    type: InputType.input,
                    label: "长者身份证",
                    decorator_id: "id_card",
                    option: {
                        placeholder: "长者身份证",
                    }
                },
                // {
                //     type: InputType.input,
                //     label: "服务供应商",
                //     decorator_id: "provider_name",
                //     option: {
                //         placeholder: "请输入服务供应商",
                //     }
                // }, 
                {
                    type: InputType.tree_select,
                    label: "服务供应商",
                    col_span: 8,
                    decorator_id: "provider_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择服务供应商",
                    },
                },
                {
                    type: InputType.select,
                    label: "回访状态",
                    decorator_id: "visit_status",
                    option: {
                        placeholder: "请选择回访状态",
                        childrens: ['未回访', '已回访'].map((item, idx) => {
                            return (
                                <Option key={item}>{item}</Option>
                            );
                        }),
                    }
                }, {
                    type: InputType.input,
                    label: "回访人",
                    decorator_id: "visit_name",
                    option: {
                        placeholder: "回访人",
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "回访时间",
                    decorator_id: "date_range"
                },
                {
                    type: InputType.rangePicker,
                    label: "服务结束时间",
                    decorator_id: "end_date"
                },
                {
                    type: InputType.select,
                    label: "服务满意度",
                    decorator_id: "service_satisfaction",
                    option: {
                        placeholder: "服务满意度",
                        childrens: ['很满意', '满意', '一般', '不满意'].map((item, idx) => {
                            return (
                                <Option key={item}>{item}</Option>
                            );
                        }),
                    }
                },
            ],
            btn_props: [
                {
                    label: '批量回访',
                    btn_method: this.undo,
                    icon: 'undo',
                }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            on_row_selection: this.onRowSelection,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: 'get_service_return_visit_list',
                    service_condition: [{}, 1, 10]
                }
            },
            onRef: this.onRef,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let table_param = {
            other_label_type: [
                {
                    type: 'button',
                    label_text: '回访',
                    label_key: 'icon_menu',
                    label_parameter: { size: 'small' },
                    label_func(record: any) {
                        return record.hasOwnProperty('visit_status') && record['visit_status'] === '已回访' ? { 'disabled': true } : {};
                    }
                },
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let service_item_list = Object.assign(service_item, table_param);
        const { getFieldDecorator } = this.props.form!;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        return (
            <Row>
                <Modal
                    title="服务回访"
                    visible={this.state.modal_visible}
                    onOk={(e: any) => this.handleOk(e)}
                    onCancel={this.handleCancel}
                >
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                        <Row>
                            <Form.Item label='服务满意度'>
                                {getFieldDecorator('service_satisfaction', {
                                    initialValue: '',
                                    rules: [{
                                        required: true,
                                        message: '请选择服务满意度'
                                    }],
                                })(
                                    <Select>
                                        {['很满意', '满意', '一般', '不满意'].map((item, index) => {
                                            return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                        </Row>
                        {/* <Row>
                            <Form.Item label='回访满意度'>
                                {getFieldDecorator('visit_satisfaction', {
                                    initialValue: '',
                                    rules: [{
                                        required: true,
                                        message: '请选择回访满意度'
                                    }],
                                })(
                                    <Select>
                                        {['很满意', '满意', '一般', '不满意'].map((item, index) => {
                                            return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                        </Row> */}
                        <Row>
                            <Form.Item label='其他评价'>
                                {getFieldDecorator('other_satisfaction', {
                                    initialValue: '',
                                })(
                                    <TextArea autoComplete='off' />
                                )}
                            </Form.Item>
                        </Row>
                        <Row>
                            <Form.Item label='回访时间'>
                                {getFieldDecorator('visit_date', {
                                    initialValue: moment(new Date()).format("YYYY-MM-DD HH:mm"),
                                })(
                                    <Input autoComplete='off' disabled={true} />
                                )}
                            </Form.Item>
                        </Row>
                        <Row>
                            <Form.Item label='回访人'>
                                {getFieldDecorator('remarks', {
                                    initialValue: CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '',
                                })(
                                    <Input autoComplete='off' disabled={true} />
                                )}
                            </Form.Item>
                        </Row>
                    </Form>
                </Modal>
                <Modal
                    title="服务批量回访"
                    visible={this.state.batch_modal_visible}
                    onOk={(e: any) => this.handleBatchOk(e)}
                    onCancel={this.handleBatchCancel}
                >
                    <Form {...formItemLayout} onSubmit={this.handleBatchSubmit}>
                        <Row>
                            <Form.Item label='服务满意度'>
                                {getFieldDecorator('service_satisfaction', {
                                    initialValue: '',
                                    rules: [{
                                        required: true,
                                        message: '请选择服务满意度'
                                    }],
                                })(
                                    <Select>
                                        {['很满意', '满意', '一般', '不满意'].map((item, index) => {
                                            return <Select.Option key={item} value={item}>{item}</Select.Option>;
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                        </Row>
                        <Row>
                            <Form.Item label='其他评价'>
                                {getFieldDecorator('other_satisfaction', {
                                    initialValue: '',
                                })(
                                    <TextArea autoComplete='off' />
                                )}
                            </Form.Item>
                        </Row>
                        <Row>
                            <Form.Item label='回访时间'>
                                {getFieldDecorator('visit_date', {
                                    initialValue: moment(new Date()).format("YYYY-MM-DD HH:mm"),
                                })(
                                    <Input autoComplete='off' disabled={true} />
                                )}
                            </Form.Item>
                        </Row>
                        <Row>
                            <Form.Item label='回访人'>
                                {getFieldDecorator('remarks', {
                                    initialValue: CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '',
                                })(
                                    <Input autoComplete='off' disabled={true} />
                                )}
                            </Form.Item>
                        </Row>
                    </Form>
                </Modal>
                <SignFrameLayout {...service_item_list} />
            </Row>
        );
    }
}

/**
 * 控件：服务回访列表
 * 描述
 */
@addon('ServiceReturnVisitListView', '服务回访列表', '描述')
@reactControl(Form.create<any>()(ServiceReturnVisitListView), true)
export class ServiceReturnVisitListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}