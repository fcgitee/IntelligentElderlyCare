import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Select } from "antd";

/**
 * 组件：服务类型信息编辑页面状态
 */
export interface ChangeServiceTypeViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 上级分类列表 */
    service_Type_list?: any[];
}

/**
 * 组件：服务类型信息编辑页面
 * 描述
 */
export class ChangeServiceTypeView extends ReactView<ChangeServiceTypeViewControl, ChangeServiceTypeViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: ''
        };
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        if (id) {
            this.setState({
                id
            });
        }
        AppServiceUtility.service_operation_service.get_service_type_list!({})!
            .then((data: any) => {
                if (data) {
                    let worker = data.result.map((key: any, value: any) => {
                        return <Select.Option key={key.id}>{key.name}</Select.Option>;
                    });
                    this.setState({
                        service_Type_list: worker
                    });
                }
            });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        let edit_props = {
            form_items_props: [
                {
                    title: "服务类型",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务类型"
                            }
                        }, {
                            type: InputType.text_area,
                            label: "服务描述",
                            decorator_id: "service_des",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务描述" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务描述"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "上级服务类型",
                            decorator_id: "parent_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择上级服务类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择上级服务类型",
                                childrens: this.state.service_Type_list,
                            }
                        },
                        {
                            type: InputType.checkbox_group,
                            label: "app上是否显示",
                            decorator_id: "is_show_app",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入app上是否显示" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入app上是否显示",
                                list: [{
                                    id: true,
                                    name: '是'
                                }, {
                                    id: false,
                                    name: '否'
                                }]
                            }
                        },
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务预订页面",
                        //     decorator_id: "service_book_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务预订页面"
                        //     }
                        // }, {
                        //     type: InputType.antd_input,
                        //     label: "服务确认页面",
                        //     decorator_id: "service_confirm_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务确认页面"
                        //     }
                        // }, {
                        //     type: InputType.antd_input,
                        //     label: "服务登记页面",
                        //     decorator_id: "service_register_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务登记页面"
                        //     }
                        // },
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务计价页面",
                        //     decorator_id: "service_valuation_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务计价页面"
                        //     }
                        // }, 
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务人员页面",
                        //     decorator_id: "service_person_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务人员页面"
                        //     }
                        // }, 
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务接受人页面",
                        //     decorator_id: "service_receiver_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务接受人页面"
                        //     }
                        // }, 
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务评价页面",
                        //     decorator_id: "service_evaluate_view",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务评价页面"
                        //     }
                        // }, {
                        //     type: InputType.antd_input,
                        //     label: "服务计价公式",
                        //     decorator_id: "service_valuation_formula",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务计价公式"
                        //     }
                        // }, {
                        //     type: InputType.antd_input,
                        //     label: "服务绩效计算公式",
                        //     decorator_id: "service_achievements_formula",
                        //     field_decorator_option: {
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务绩效计算公式"
                        //     }
                        // }, 
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.serviceType);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: "get_service_type_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_type"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceType); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：服务类型信息编辑页面
 * 描述
 */
@addon('ChangeServiceTypeView', '服务类型信息编辑页面', '描述')
@reactControl(ChangeServiceTypeView, true)
export class ChangeServiceTypeViewControl extends ReactViewControl {
}