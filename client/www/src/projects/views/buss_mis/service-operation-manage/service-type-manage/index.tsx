import { Modal, Row } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ServiceItem } from "src/projects/models/service-operation";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：服务类型信息状态
 */
export interface ServiceTypeViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 服务项目集合 */
    service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 服务项目总条数 */
    total?: number;
}

/**
 * 组件：服务类型信息
 * 描述
 */
export class ServiceTypeView extends ReactView<ServiceTypeViewControl, ServiceTypeViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '服务类别',
        dataIndex: 'service_type',
        key: 'service_type',
    }, {
        title: '服务描述',
        dataIndex: 'service_des',
        key: 'service_des',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];
    private columns_service_item = [{
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
    },
    {
        title: '服务类型',
        dataIndex: 'service_type_name',
        key: 'service_type_name',
    }, {
        title: '服务项目参数',
        dataIndex: 'service_item_param',
        key: 'service_item_param',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceType);
    }
    /** 查询服务项目 */
    queryServiceItem = () => {
        let ids = this.state.select_ids;
        if (ids && ids!.length > 0) {
            request(this, AppServiceUtility.service_operation_service.get_service_item_list!({ 'service_type_id': ids }))
                .then((data: any) => {
                    this.setState({
                        service_item_list: data.result,
                        total: data.total,
                        modal_visible: true
                    });
                });
        }
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeServiceType + '/' + contents.id);
        }
    }

    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "服务类别",
                    decorator_id: "service_type"
                }
            ],
            btn_props: [{
                label: '新增服务类型',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '查询服务项目',
                btn_method: this.queryServiceItem,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_service_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <Modal
                    title="服务项目列表"
                    visible={this.state.modal_visible}
                    // onOk={() => this.handleOk(formProps)}
                    onCancel={this.handleCancel}
                >
                    <NTOperationTable
                        data_source={this.state.service_item_list}
                        columns_data_source={this.columns_service_item}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：服务类型信息
 * 描述
 */
@addon('ServiceTypeView', '服务类型信息', '描述')
@reactControl(ServiceTypeView, true)
export class ServiceTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}