import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：业务区域管理列表状态
 */
export interface ServiceWokerViewState extends ReactViewState {
}

/**
 * 组件：业务区域管理列表
 * 描述
 */
export class ServiceWokerView extends ReactView<ServiceWokerViewControl, ServiceWokerViewState> {
    private columns_data_source = [{
        title: '服务员',
        dataIndex: 'user_name',
        key: 'user_name',
    }, {
        title: '创建时间',
        dataIndex: 'create_date',
        key: 'create_date'
    }];
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.ChangeServiceWoker);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.ChangeServiceWoker + '/' + contents.id);
        }
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let behavioral_competence_assessment = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "user_name"
                },
                // {
                //     type: InputType.input,
                //     label: "创建时间",
                //     decorator_id: "create_date"
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_click_del: this.on_click_del,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: 'get_service_woker_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'service_woker_delete'
                }
            },
        };
        let behavioral_competence_assessment_list = Object.assign(behavioral_competence_assessment, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：业务区域管理列表
 * 描述
 */
@addon('ServiceWokerView', '护理类型管理列表', '描述')
@reactControl(ServiceWokerView, true)
export class ServiceWokerViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}