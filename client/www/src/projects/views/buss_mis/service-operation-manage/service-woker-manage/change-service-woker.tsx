import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { personnelCategory, personnel_type } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;

// import { Select } from 'antd';
// import { request } from "src/business/util_tool";
// let { Option } = Select;

/**
 * 组件：编辑业务区域管理状态
 */
export interface ChangeServiceWokerViewState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
    user_list: any;
    item_list: any;
}

/**
 * 组件：编辑业务区域管理
 * 描述
 */
export class ChangeServiceWokerView extends ReactView<ChangeServiceWokerViewControl, ChangeServiceWokerViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            user_list: [],
            item_list: []
        };
    }
    componentDidMount() {
        // 获取护理人员
        const param = { personnel_category: personnelCategory.staff, personnel_type: personnel_type.person };
        request_func(this, AppServiceUtility.personnel_service['get_personnel_list'], [param], (data: any) => {
            let user_list: any[] = [];
            const data_list: any[] = data.result;
            data_list!.map((item, idx) => {
                user_list.push(<Option key={item['id']}>{item['name']}</Option>);
            });
            this.setState({
                user_list
            });
        });

        // 获取服务项目
        request_func(this, AppServiceUtility.services_project_service['get_services_project_list'], [{}], (data: any) => {
            let item_list: any[] = [];
            const data_list: any[] = data.result;
            data_list!.map((item, idx) => {
                item_list.push(<Option key={item['id']}>{item['name']}</Option>);
            });
            this.setState({
                item_list
            });
        });

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        console.info(this.props.match!.params.key, 'id');
        let columns_data_source = [{
            title: '服务项目',
            dataIndex: 'item_id',
            key: 'item_id',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择评估项目",
                childrens: this.state.item_list
            }
        }, {
            title: '备注',
            dataIndex: 'value',
            key: 'value',
            type: 'input' as ColTypeStr
        }];
        let edit_props = {
            form_items_props: [
                {
                    type: InputTypeTable.select,
                    label: "服务人员",
                    decorator_id: "user_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择服务人员" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请选择服务人员",
                        childrens: this.state.user_list
                    }
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.serviceWoker);
                    }
                }
            ],
            submit_props: {
                text: "保存",
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            columns_data_source: columns_data_source,
            add_row_text: "新增",
            table_field_name: 'aera_dispose',
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: "get_service_woker_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "service_woker_update"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceWoker); },
            id: this.props.match!.params.key
        };
        return (
            <TableList {...edit_props} />
        );
    }
}

/**
 * 控件：编辑业务区域管理
 * 描述
 */
@addon('ChangeServiceWokerViewView', '编辑服务提供者登记信息', '描述')
@reactControl(ChangeServiceWokerView, true)
export class ChangeServiceWokerViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}