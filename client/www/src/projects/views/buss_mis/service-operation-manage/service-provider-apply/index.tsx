import { Select, Steps } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from 'src/projects/remote';
import { ROUTE_PATH } from 'src/projects/router';
import { request } from "../../../../../business/util_tool";

const { Step } = Steps;

let { Option } = Select;

/**
 * 组件：服务商登记编辑页面状态
 */
export interface ChangeServiceProviderViewState extends ReactViewState {
    /** 服务商列表 */
    provider_list?: [];
    /**
     * 申请状态
     */
    status?: number;

    /**
     * 表单权限控制
     */
    disabled?: boolean;
    /** 接口名 */
    request_url?: string;
    administrative_division_list?: any;
    org_list?: any;
}

/**
 * 组件：服务商登记编辑页面
 * 描述
 */
export class ChangeServiceProviderView extends ReactView<ApplyServiceProviderViewControl, ChangeServiceProviderViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            provider_list: [],
            status: 0,
            disabled: false,
            request_url: '',
            administrative_division_list: [],
            org_list: []
        };
    }
    // /** 权限服务 */
    // permissionService?() {
    //     return getObject(this.props.permissionService_Fac!);
    // }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
        let step = new Map([['', 0], ['正在申请', 1], ['同意', 2], ['不同意', 2]]);
        request(this, AppServiceUtility.service_operation_service['get_current_service_provider']!({}, 1, 999))
            .then((datas: any) => {
                if (datas.result.length > 0) {
                    // console.log('datas.result', datas.result);
                    let num = step.get(datas.result[0].status);
                    this.setState({
                        provider_list: [],
                        status: num,
                        disabled: num !== 0 || datas.result[0].status === '不同意',
                    });
                }
            });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        const org_nature = ['公办', '公办民营', '民办企业', '民办非营利'];
        const org_nature_list: JSX.Element[] = org_nature!.map((item) => <Option key={item} value={item}>{item}</Option>);
        let provider = this.state.provider_list;
        let provider_list: any[] = [];
        provider!.map((item, idx) => {
            provider_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props: any = {
            form_items_props: [
                {
                    title: "服务商申请信息",
                    need_card: true,
                    input_props: [
                        // {
                        //     type: InputType.antd_input,
                        //     label: "服务商类别",
                        //     decorator_id: "sp_type_id",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请输入服务商类别" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入服务商类别"
                        //     }
                        // }, 
                        {
                            type: InputType.antd_input,
                            label: "机构名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入名称" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入名称"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入地址" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入地址"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "简介",
                            decorator_id: "introduction",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入简介" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入简介",
                                rows: 3,
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "相关图片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "picture_list",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传营业执照" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "经度",
                            decorator_id: "lon",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入经度" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入经度"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "纬度",
                            decorator_id: "lat",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入纬度" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入纬度"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "法人",
                            decorator_id: "legal_person",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入法人" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入法人"
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "营业执照（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "business_license_url",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传营业执照" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "联系电话",
                            decorator_id: "telephone",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入联系电话" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入联系电话"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "组织机构类型",
                            decorator_id: "personnel_category",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入组织机构类型" }],
                                initialValue: '服务商',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入组织机构类型",
                                disabled: true

                            }
                        },
                        {
                            type: InputType.select,
                            label: "组织机构性质",
                            decorator_id: "organization_nature",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入组织机构性质" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入组织机构性质",
                                childrens: org_nature_list

                            }
                        }, {
                            type: InputType.tree_select,
                            label: "行政区划",
                            decorator_id: "admin_area_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择行政区划" }],
                                initialValue: ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择行政区划",
                            },
                        },
                        {
                            type: InputType.tree_select,
                            label: "上级组织机构",
                            decorator_id: "super_org_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "上级组织机构" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.org_list,
                                placeholder: "请选择上级组织机构",
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "服务商能力级别",
                            decorator_id: "sp_ability_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务商能力级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务商能力级别",
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "服务商信用级别",
                            decorator_id: "sp_credit_level",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务商信用级别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务商信用级别",
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "服务商资质信息（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "sp_qualifications",
                            field_decorator_option: {
                                rules: [{ required: false }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                disabled: this.state.disabled,
                                // placeholder: "请输入服务商资质信息"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "服务机构合同条款",
                            decorator_id: "sp_contract_clauses",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务机构合同条款" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务机构合同条款",
                                rows: 3,
                                disabled: this.state.disabled,
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                rows: 3,
                                disabled: this.state.disabled,
                            }
                        }
                    ]
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: this.state.request_url ? this.state.request_url : '',
                        arguments: [{ id: 1 }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_provider"
                    }
                },
                service_reject: (error: Error) => {
                    // console.log(error);
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceProviderApply); },
            id: this.props.match!.params.key,
        };
        if (this.state.status !== 0) {
            delete edit_props.submit_btn_propps;
        }
        let edit_props_list = Object.assign(edit_props, edit_props_info);

        return (
            <MainContent>
                <Steps current={this.state.status} style={{ background: "white", padding: "25px" }}>
                    <Step title="填写申请信息" description="填写信息." />
                    <Step title="平台审核" description="请等待." />
                    <Step title="民政审核" description="请等待." />
                    <Step title="审核完成" description="查看结果." />
                </Steps>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：服务商登记编辑页面
 * 描述
 */
@addon('ChangeServiceProviderView', '服务商登记编辑页面', '描述')
@reactControl(ChangeServiceProviderView, true)
export class ApplyServiceProviderViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}