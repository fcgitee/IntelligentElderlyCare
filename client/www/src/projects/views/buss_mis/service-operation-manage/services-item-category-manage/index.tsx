
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
import { Form, Table, Row, Button, Col, Input, Card, Icon, Modal, message } from "antd";
import { MainContent } from "src/business/components/style-components/main-content";
import { request_func } from "src/business/util_tool";
/** 状态：服务项目类别视图 */
export interface ServicesItemCategoryViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    base_data?: any;
    pagination?: any;
    loading?: any;
    condition?: any;
    visible?: boolean;
    selected_data?: any;
    search_value?: any;
    is_send?: boolean;
}
export enum InputType { "upload", "input", "select", "preText", "customProperties", "date", "rangePicker", "radioGroup", "modal_search" }
/** 组件：服务项目类别视图 */
export class ServicesItemCategoryView extends React.Component<ServicesItemCategoryViewControl, ServicesItemCategoryViewState> {
    constructor(props: ServicesItemCategoryViewControl) {
        super(props);
        this.state = {
            request_url: '',
            base_data: [],
            pagination: {},
            loading: false,
            visible: false,
            selected_data: [],
            search_value: '',
            is_send: false,
        };
    }
    /** 新增按钮 */
    addServiceProject = () => {
        this.props.history!.push(ROUTE_PATH.changeServicesItemCategory);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeServicesItemCategory + '/' + contents.id);
        }
    }
    handleSearch = (e: any) => {
        e.preventDefault();
        let { props } = this;
        props.form!.validateFields((err: any, values: any) => {
            // this.setState({ condition: values });
            console.info('hello world', values);
            this.request_order(values, 1);
        });
    }
    componentDidMount() {
        this.request_order({}, 1);
    }
    setSearchValue(e: any) {
        // console.log(e);
        this.setState({
            search_value: e,
        });
    }
    reset() {
        let { form } = this.props;
        form.resetFields();
    }
    request_order = (condition: any, page: any) => {
        condition['all'] = true;
        let request_url = this.props.request_url;
        request_func(this, AppServiceUtility.service_operation_service[request_url!], [condition], (data: any) => {
            const pagination = { ...this.state.pagination };
            if (data && data.result && data.result.length > 0) {
                let count = 0;
                data.result.map((item: any) => {
                    if (!item.hasOwnProperty('parent_id')) {
                        count++;
                    }
                });
                pagination.total = count;
            } else {
                pagination.total = 0;
            }
            this.setState({ base_data: data.result, pagination });
        });
    }
    handleTableChange = (pagination: any, filters: any, sorter: any) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        // this.request_order(this.state.condition, pagination.current);
    }
    handleCancel() {
        this.setState({
            visible: false,
        });
    }
    filterHTMLTag(msg: string) {
        if (msg && msg.length > 0) {
            return msg.replace(/<\/?[^>]*>/g, '').replace(/[|]*\n/, '').replace(/&nbsp;/ig, '').replace(/[\r\n]/g, '');
        }
        return msg;
    }
    onClickDel = (content: any) => {
        this.setState({
            visible: true,
            selected_data: content
        });
    }
    /** 确定回调事件 */
    handleOk = (e: any) => {
        let { selected_data } = this.state;
        this.setState(
            {
                visible: false,
            },
            () => {
                request_func(this, AppServiceUtility.service_operation_service['del_service_type'], [[selected_data.id]], (data: any) => {
                    if (data === 'Success') {
                        message.info('删除成功！', 1, () => {
                            window.location.reload();
                        });
                    }
                });
            }
        );
    }
    onChangeThis(id: string, value: boolean) {
        let { is_send, base_data } = this.state;
        if (is_send === true) {
            message.info('请勿操作太快！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request_func(this, AppServiceUtility.service_operation_service.update_service_type, [{ id, is_show_app: value }], (data: any) => {
                    if (data === 'Success') {
                        message.info('操作成功！');
                        let newBaseData: any = [];
                        base_data.map((item: any, index: number) => {
                            if (item.id === id) {
                                item.is_show_app = value;
                            }
                            newBaseData.push(item);
                        });
                        this.setState({
                            is_send: false,
                            base_data: newBaseData,
                        });
                    } else {
                        message.info(data);
                        this.setState({
                            is_send: false,
                        });
                    }
                });
            }
        );
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name",
                    option: {
                        placeholder: ''
                    }
                },
            ],
            btn_props: [
                {
                    label: '新增',
                    btn_method: this.addServiceProject,
                    icon: 'plus'
                },
            ],
            // on_row_selection: this.on_row_selection,
            service_object: AppServiceUtility.cost_accounting_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: [{}]
                }
            }
        };

        let tableCols = [{
            title: '服务类型',
            width: 100,
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '编号',
            width: 60,
            dataIndex: 'number',
            key: 'number',
        },
        {
            title: '服务类型描述',
            width: 400,
            dataIndex: 'service_des',
            key: 'service_des',
        }, {
            title: '备注',
            dataIndex: 'remark',
            width: 300,
            key: 'remark',
        }, {
            title: '状态',
            dataIndex: 'is_show_app',
            width: 100,
            key: 'is_show_app',
            render: (text: string, record: any) => (
                <div>
                    {record.hasOwnProperty('is_show_app') && record.is_show_app === true ? '启用' : '停用'}
                </div>
            ),
        }, {
            title: '操作',
            dataIndex: 'action',
            width: 120,
            key: 'action',
            render: (text: string, record: any) => (
                <div>
                    <span style={{ marginRight: '10px' }}>
                        <Button size="small" onClick={() => this.onChangeThis(record.id, !record.is_show_app)}>
                            {record.is_show_app === true ? '停用' : '启用'}
                        </Button>
                    </span>
                    <span style={{ marginRight: '10px' }}>
                        <Icon className='icon-type' type="edit" onClick={() => { this.onIconClick('icon_edit', record); }} />
                    </span>
                    <span>
                        <Icon className='icon-type' type="delete" onClick={() => { this.onClickDel(record); }} />
                    </span>
                </div>
            ),
        }];

        const expandedRowRender = (item: any) => {
            const base_data: any = this.state.base_data;
            const columns = tableCols;

            const data_recode: any = [];
            for (var i = 0; i < base_data.length; i++) {
                if (base_data[i].hasOwnProperty('parent_id') && base_data[i]['parent_id'] === item.id) {
                    data_recode.push({
                        key: i,
                        id: base_data[i].id,
                        number: base_data[i].number,
                        is_show_app: base_data[i].is_show_app,
                        name: base_data[i].name,
                        service_des: base_data[i].service_des,
                        remark: base_data[i].remark,
                    });
                }
            }
            return <Table columns={columns} dataSource={data_recode} pagination={false} />;
        };
        const columns = tableCols;

        const data = [];
        const base_data = this.state.base_data;
        const search_value = this.state.search_value;
        // 父级title数据
        for (let i = 0; i < base_data.length; ++i) {
            // 获取一级，没有parent_id的就是一级或者有搜索的时候全显示，其实这里有问题，不可描述
            if (!base_data[i].hasOwnProperty('parent_id') || search_value !== '') {
                data!.push({
                    key: i,
                    id: base_data[i].id,
                    name: base_data[i].name,
                    number: base_data[i].number,
                    is_show_app: base_data[i].is_show_app,
                    service_des: base_data[i].service_des,
                    remark: base_data[i].remark,
                });
            }
        }
        const { getFieldDecorator } = this.props.form!;
        return (
            <MainContent>
                <Card>
                    <div className='form-bottom'>
                        <Form
                            className="nt-sign-manage-layout"
                            onSubmit={this.handleSearch}
                        >
                            <Row gutter={24}>
                                {
                                    json_info.edit_form_items_props ? json_info.edit_form_items_props.map((e, i) =>
                                        (
                                            <Col span={8} key={i}>
                                                <Form.Item
                                                    label={e.label}
                                                >
                                                    {getFieldDecorator(e.decorator_id, {
                                                        /** 日期特殊处理 暂不需要初始化当天日期 */
                                                        initialValue: '', // e.type === InputType.date ? moment(e.default_value) : e.default_value,
                                                    })(
                                                        <Input onChange={(e) => this.setSearchValue(e)} />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        )
                                    )
                                        :
                                        null
                                }
                                <Col span={24}>
                                    <Row type="flex" style={{ flexDirection: "row-reverse" }}>
                                        <Button type='ghost' htmlType='button' onClick={() => this.reset()} >重置</Button>
                                        <Button htmlType="submit" type='primary' className='res-btn'>查询</Button>
                                    </Row>
                                </Col>
                            </Row >
                            <Row type="flex" >
                                {
                                    json_info.btn_props ?
                                        json_info.btn_props !== undefined && json_info.btn_props.map((item, idx) => {
                                            return <Button type='primary' onClick={item.btn_method} key={idx} icon={item.icon!} style={{ marginLeft: '10px' }}>{item.label}</Button>;
                                        }) : <span />

                                }
                            </Row>
                        </Form>
                    </div>
                    <Table
                        className="components-table-demo-nested"
                        columns={columns}
                        rowKey='id'
                        defaultExpandAllRows={true}
                        expandedRowRender={expandedRowRender}
                        dataSource={data}
                        pagination={this.state.pagination}
                        onChange={this.handleTableChange}
                    />
                </Card>
                <Modal
                    title="注意！"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>是否删除该行信息</p>
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：服务项目类别视图控制器
 * @description 服务项目类别视图
 * @author
 */
@addon('ServicesItemCategoryView', '服务项目类别视图', '服务项目类别视图')
@reactControl(Form.create<any>()(ServicesItemCategoryView), true)
export class ServicesItemCategoryViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}