import Form from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { beforeUpload } from "src/projects/app/util-tool";
import { Select, Button, Row, Col, message, Input, Radio } from "antd";
import { remote } from "src/projects/remote";
import { MainCard } from "src/business/components/style-components/main-card";
import TextArea from "antd/lib/input/TextArea";
import { request } from "src/business/util_tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { checkMin1 } from "../../activity-manage/validate";

/**
 * 组件：服务项目类别编辑页面状态
 */
export interface ChangeServicesItemCategoryViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 上级分类列表 */
    service_type_list?: any[];
    // 最新的number
    newest_p_number?: any;
    newest_c_number?: any;
    // 选择的上级
    selected_top?: any;
    // 数据列表
    data_list?: any;
    // 数据详情
    data_info?: any;
    // 是否初始化，初始化状态显示原来的数据，触发过选择上级就会显示更新后的
    isInit?: boolean;
    // 这个用来储存二级节点的父级编号
    parent_number?: any;
    is_send?: boolean;
}
/**
 * 组件：服务项目类别编辑页面
 * 描述
 */
export class ChangeServicesItemCategoryView extends ReactView<ChangeServicesItemCategoryViewControl, ChangeServicesItemCategoryViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            newest_p_number: '',
            newest_c_number: '',
            selected_top: [],
            data_list: [],
            data_info: [],
            isInit: true,
            parent_number: '',
            is_send: false,
        };
    }
    // 获取最大的编号, what a big gongcheng
    getMaxNumber(type: string, pid: string = '') {
        let data_list = this.state.data_list;
        let maxNumber = 0;
        let maxItem: any = '';
        data_list.map((item: any) => {
            // 符合条件
            if (item.hasOwnProperty('number') && /^[0-9]*$/.test(item.number) && Number(item.number) > maxNumber) {
                // 是第一级
                if (type === '1' && (!item.hasOwnProperty('parent_id') || !item.parent_id)) {
                    maxNumber = Number(item.number);
                } else if (type === '2' && pid && (item.hasOwnProperty('parent_id') && item.parent_id && item.parent_id === pid)) {
                    // 获取父级的编号
                    let pnumber = 1;
                    for (let i = 0; i < data_list.length; i++) {
                        if (data_list[i].id === pid) {
                            pnumber = data_list[i].number;
                        }
                        break;
                    }
                    // 替换掉父级的编号
                    let reg = new RegExp("^" + pnumber + "");
                    maxItem = item;
                    maxNumber = Number((item.number + '').replace(reg, ''));
                }
            }
        });
        // 二级节点切换走再切换回来，会自己加1，这是避免
        if (this.props.match!.params.key && maxItem['id'] === this.props.match!.params.key) {
            if (maxNumber < 10) {
                return '0' + maxNumber;
            }
            return maxNumber;
        }
        if (maxNumber < 10) {
            return '0' + ++maxNumber;
        }
        return ++maxNumber;
    }
    componentDidMount() {
        let id = this.props.match!.params.key;
        // 服务类型详情
        request(this, AppServiceUtility.service_operation_service.get_service_type_list!({ all: true }))
            .then((data: any) => {
                if (data) {
                    let service_type_list = data.result.map((key: any, value: any) => {
                        // 顺便找当前的
                        if (id && key.id === id) {
                            let setState = {
                                data_info: key
                            };
                            // 是二级节点，就再循环一次获取父级编号
                            if (key.hasOwnProperty('parent_id') && key['parent_id']) {
                                data.result.map((k: any, v: any) => {
                                    if (k.id === key.parent_id) {
                                        setState['parent_number'] = k.number;
                                    }
                                });
                            }
                            this.setState(setState);
                        }
                        // 紧致套娃和过滤二级节点
                        if (key.id !== id && (!key.hasOwnProperty('parent_id') || !key['parent_id'])) {
                            return <Select.Option key={key.id}>{key.name}</Select.Option>;
                        }
                        return null;
                    });
                    this.setState(
                        {
                            data_list: data.result,
                            service_type_list: service_type_list
                        },
                        () => {
                            this.setState({
                                newest_p_number: this.getMaxNumber('1'),
                            });
                        }
                    );
                }
            });
    }
    changeTop(e: any) {
        let data_list = this.state.data_list!;
        let flag = false;
        for (let i = 0; i < data_list.length; i++) {
            if (data_list[i].id === e) {
                flag = true;
                this.setState({
                    isInit: false,
                    selected_top: data_list[i],
                    newest_p_number: this.getMaxNumber('1'),
                    newest_c_number: this.getMaxNumber('2', data_list[i]['id']),
                });
                break;
            }
        }
        if (flag === false) {
            this.setState({
                isInit: false,
                selected_top: [],
                newest_p_number: this.getMaxNumber('1'),
                newest_c_number: '',
            });
        }
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                if (this.props.match!.params.key) {
                    values['id'] = this.props.match!.params.key;
                }
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(this, AppServiceUtility.service_operation_service.update_service_type!(values))
                            .then((datas: any) => {
                                if (datas === 'Success') {
                                    message.info('保存成功', 1, () => {
                                        this.props.history!.goBack();
                                    });
                                } else {
                                    message.error(datas);
                                    this.setState({
                                        is_send: false,
                                    });
                                }
                            })
                            .catch((error: any) => {
                                message.error('保存失败！');
                                console.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }
    back() {
        history.back();
    }
    render() {
        let { selected_top, newest_p_number, service_type_list, newest_c_number, data_info, isInit, parent_number } = this.state;
        const { getFieldDecorator } = this.props.form!;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        return (
            <MainContent>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <MainCard title='服务类型'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='名称'>
                                    {getFieldDecorator('name', {
                                        initialValue: data_info['name'] ? data_info['name'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请输入名称'
                                        }],
                                    })(
                                        <Input autoComplete='off' placeholder="请输入名称" />
                                    )}
                                </Form.Item>
                                <Form.Item label='服务描述'>
                                    {getFieldDecorator('service_des', {
                                        initialValue: data_info['service_des'] ? data_info['service_des'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请输入服务描述'
                                        }],
                                    })(
                                        <TextArea placeholder="请输入服务描述" />
                                    )}
                                </Form.Item>
                                <Form.Item label='上级服务类型'>
                                    {getFieldDecorator('parent_id', {
                                        initialValue: data_info['parent_id'] ? data_info['parent_id'] : '',
                                    })(
                                        <Select
                                            onChange={(e: any) => this.changeTop(e)}
                                            placeholder="请选择上级服务类型"
                                        >
                                            {service_type_list}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='上级服务编号'>
                                    {getFieldDecorator('parent_number', {
                                        initialValue: (parent_number && isInit) ? parent_number : selected_top.hasOwnProperty('number') ? selected_top['number'] : '',
                                    })(
                                        <Input autoComplete='off' disabled={true} />
                                    )}
                                </Form.Item>
                                {/* selected_top.hasOwnProperty('number') ? selected_top['number'] : '',
                                
                                initialValue: selected_top.hasOwnProperty('number') ? selected_top['number'] + '' + newest_c_number : newest_p_number, */}
                                <Form.Item label='服务编号'>
                                    {getFieldDecorator('number', {
                                        initialValue: (data_info['number'] && isInit) ? data_info['number'] : selected_top.hasOwnProperty('number') ? selected_top['number'] + '' + newest_c_number : newest_p_number,
                                    })(
                                        <Input autoComplete='off' disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item label='服务类型图标（大小小于2M，格式支持jpg/jpeg/png）'>
                                    {getFieldDecorator('logo_image', {
                                        initialValue: data_info['logo_image'] ? data_info['logo_image'] : '',
                                    })(
                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                    )}
                                </Form.Item>
                                <Form.Item label='app上是否显示'>
                                    {getFieldDecorator('is_show_app', {
                                        initialValue: data_info['is_show_app'] ? data_info['is_show_app'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择app上是否显示'
                                        }],
                                    })(
                                        <Radio.Group>
                                            {[{
                                                label: '是',
                                                value: true,
                                            }, {
                                                label: '否',
                                                value: false,
                                            }].map((i: any, idx: any) => {
                                                return <Radio value={i.value} key={idx}>{i.label}</Radio>;
                                            })}
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                                <Form.Item label='显示顺序'>
                                    {getFieldDecorator('show_num', {
                                        initialValue: data_info['show_num'] ? data_info['show_num'] : '',
                                        rules: [{
                                            required: true,
                                            validator: (_: any, value: any, cb: any) => checkMin1(_, value, cb, '显示顺序')
                                        }],
                                    })(
                                        <Input autoComplete='off' placeholder="请输入显示顺序" />
                                    )}
                                </Form.Item>
                                <Form.Item label='备注'>
                                    {getFieldDecorator('remark', {
                                        initialValue: data_info['remark'] ? data_info['remark'] : '',
                                    })(
                                        <TextArea placeholder="请输入备注" autoComplete="off" />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    <MainCard>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button disabled={this.state.is_send} htmlType='submit' type='primary'>保存</Button>
                            <Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                        </Row>
                    </MainCard>
                </Form>
            </MainContent >
        );
    }
}

/**
 * 控件：服务项目类别编辑页面
 * 描述
 */
@addon('ChangeServicesItemCategoryView', '服务项目类别编辑页面', '描述')
@reactControl(Form.create<any>()(ChangeServicesItemCategoryView), true)
export class ChangeServicesItemCategoryViewControl extends ReactViewControl {
}