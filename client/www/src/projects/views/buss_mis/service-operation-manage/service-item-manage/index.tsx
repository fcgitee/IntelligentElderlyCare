import { Select } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

let { Option } = Select;
/**
 * 组件：服务项目登记信息状态
 */
export interface ServiceItemViewState extends ReactViewState {
    /** 服务商列表 */
    item_list?: [];
    /** 服务类型列表 */
    service_type_list?: [];
}

/**
 * 组件：服务项目登记信息
 * 描述
 */
export class ServiceItemView extends ReactView<ServiceItemViewControl, ServiceItemViewState> {
    private columns_data_source = [{
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
    },
    {
        title: '服务类型',
        dataIndex: 'service_type_name',
        key: 'service_type_name',
    }, {
        title: '服务项目参数',
        dataIndex: 'service_item_param',
        key: 'service_item_param',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            service_type_list: []
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceItem);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeServiceItem + '/' + contents.id);
        }
    }
    componentDidMount() {
        request(this, AppServiceUtility.user_service.get_user_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    item_list: datas.result,
                });
            });
        request(this, AppServiceUtility.service_operation_service.get_service_type_list!({}))
            .then((datas: any) => {
                this.setState({
                    service_type_list: datas.result,
                });
            });
    }
    render() {
        let item = this.state.item_list;
        let item_list: any[] = [];
        item!.map((item, idx) => {
            item_list.push(<Option key={idx}>{item['name']}</Option>);
        });
        let service_type_list: any[] = [];
        this.state.service_type_list!.map((item, idx) => {
            service_type_list.push(<Option key={idx}>{item['name']}</Option>);
        });
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "服务商",
                    decorator_id: "service_provider_id",
                    option: {
                        placeholder: "请选择服务商",
                        childrens: item_list,
                    }
                }, {
                    type: InputType.select,
                    label: "服务类型",
                    decorator_id: "service_type_id",
                    option: {
                        placeholder: "请输入服务类型",
                        childrens: service_type_list,
                    }
                }
            ],
            btn_props: [{
                label: '新增服务项目',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: 'get_service_item_list',
                    service_condition: []
                },
                delete: {
                    service_func: 'del_service_item'
                }
            },
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：服务项目登记信息
 * 描述
 */
@addon('ServiceItemView', '服务项目登记信息', '描述')
@reactControl(ServiceItemView, true)
export class ServiceItemViewControl extends ReactViewControl {
}