import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Card, Descriptions, Row, Button, message, Spin, Steps, Select, Col, } from "antd";
import { request } from "src/business/util_tool";
import { product_step } from "./change-service-item-package";
import { AppServiceUtility } from "src/projects/app/appService";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import TextArea from "antd/lib/input/TextArea";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：服务产品审核状态
 */
export class ServiceProductShState {
    product_info?: any;
    reason?: any;
    is_send?: boolean;
    action_value?: string;
}

/**
 * 组件：服务产品审核
 * 服务产品审核
 */
export class ServiceProductSh extends React.Component<ServiceProductShControl, ServiceProductShState> {
    constructor(props: ServiceProductShControl) {
        super(props);
        this.state = {
            action_value: '',
            reason: '',
            product_info: [],
            is_send: false,
        };
    }
    componentWillMount() {
        if (this.props.match!.params.key) {
            // 服务产品
            request(this, AppServiceUtility.service_item_package.get_service_item_package_pure!({ id: this.props.match!.params.key }))
                .then((data: any) => {
                    if (data && data.result && data.result.length > 0) {
                        this.setState({
                            product_info: data.result[0],
                        });
                    }
                });
        }
    }
    back() {
        history.back();
    }
    change(e: any) {
        this.setState({
            action_value: e,
        });
    }
    save() {
        let { product_info, reason, is_send, action_value } = this.state;
        if (!product_info.hasOwnProperty('id')) {
            message.info('没有找到可操作的数据！');
            return;
        }
        if (!action_value) {
            message.info('请选择审核结果！');
            return;
        }
        if (action_value !== '1' && reason === '') {
            message.info('请填写原因！');
            return;
        }
        if (is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                request(this, AppServiceUtility.service_item_package.update_service_item_package!({ id: product_info.id, action: "sh", action_value: action_value, reason: reason }))
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('操作成功！', 1, () => {
                                this.props.history!.push(ROUTE_PATH.serviceItemPackageSh);
                            });
                        } else {
                            this.setState({
                                is_send: false
                            });
                            message.info('操作失败！');
                        }
                    });
            }
        );
    }
    setRejectReason(e: any) {
        this.setState({
            reason: e.target.value,
        });
    }
    render() {
        let { product_info } = this.state;

        // 初始化，抑制报错
        if (!product_info) {
            product_info = [];
        }

        const { Step } = Steps;
        const { Option } = Select;

        let step = 0;
        if (product_info.status === '不通过') {
            step = 2;
        } else if (product_info.status === '通过') {
            step = 2;
        } else if (product_info.status === '待审批') {
            step = 1;
        }
        return (
            <MainContent>
                <Steps current={step} style={{ background: "white", padding: "25px" }}>
                    {product_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} />
                        );
                    })}
                </Steps>
                {product_info.hasOwnProperty('id') ? <Row>
                    <Card className="shenhe-card">
                        <Descriptions title="服务产品审核" bordered={true}>
                            <Descriptions.Item label="名称">{product_info['name'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="所属机构">{product_info['organization_name'] && product_info['organization_name'] ? product_info['organization_name'] : ''}</Descriptions.Item>
                            <Descriptions.Item label="简介">{product_info['introduce'] ? product_info['introduce'] : ''}</Descriptions.Item>
                            <Descriptions.Item label="服务产品介绍">{product_info['service_product_introduce'] || ''}</Descriptions.Item>
                            <Descriptions.Item label="总价">{product_info['service_package_price'] || ''}元</Descriptions.Item>
                            <Descriptions.Item label="备注">{product_info['remarks'] ? product_info['remarks'] : ''}</Descriptions.Item>
                            {/* <Descriptions.Item label="是否启用">{product_info['state'] === 1 || product_info['state'] === '1' ? '是' : '否'}</Descriptions.Item> */}
                            <Descriptions.Item label="是否需要分派">{product_info['is_assignable'] === 1 || product_info['is_assignable'] === '1' ? '是' : '否'}</Descriptions.Item>
                            {/* <Descriptions.Item label="是否外部产品">{product_info['is_external'] === 1 || product_info['is_external'] === '1' ? '是' : '否'}</Descriptions.Item> */}
                            <Descriptions.Item label="是否推荐产品">{product_info['is_recommend'] ? product_info['is_recommend'] : ''}</Descriptions.Item>
                            {/* <Descriptions.Item label="服务次数">{product_info['remaining_times'] || ''}</Descriptions.Item> */}
                            {/* <Descriptions.Item label="附加合同条款">{product_info['additonal_contract_terms'] || ''}</Descriptions.Item> */}
                            <Descriptions.Item label="服务类别">{product_info['is_service_item'] === 'true' ? '单个项目' : '服务套餐'}</Descriptions.Item>
                            {product_info.is_service_item === 'true' ? <Descriptions.Item label="服务项目">
                                {product_info.childs && product_info.childs.map ? product_info.childs.map((item: any, index: number) => {
                                    return (
                                        <Row key={index}>{`${item.name}`}</Row>
                                    );
                                }) : null}
                            </Descriptions.Item> : <Descriptions.Item label="服务细项">
                                    {product_info.childs && product_info.childs.map ? product_info.childs.map((item: any, index: number) => {
                                        return (
                                            <Row key={index}>{`${item.name}【服务次数：${product_info.remaining_times || 1}次】 【单价：${item.service_package_price}元】`}</Row>
                                        );
                                    }) : null}
                                </Descriptions.Item>}
                            <Descriptions.Item label="服务产品图片" span={3}>
                                {product_info['picture_collection'] && product_info['picture_collection'].length > 0 ? product_info['picture_collection'].map((item: any, index: number) => {
                                    return (
                                        <Row key={index}>
                                            <img style={{ maxWidth: '200px' }} alt={product_info['name']} src={item} />
                                        </Row>
                                    );
                                }) : null}
                            </Descriptions.Item>
                            <Descriptions.Item label="提交时间">{product_info['create_date'] ? product_info['create_date'] : ''}</Descriptions.Item>
                        </Descriptions>
                    </Card>
                    <MainCard>
                        <Row>
                            <Col span={3}>
                                <Select style={{ width: '90%' }} placeholder="请选择审核结果" onChange={(e: any) => this.change(e)}>
                                    <Option value="1">通过</Option>
                                    <Option value="2">不通过</Option>
                                    {/* <Option value="3">打回</Option> */}
                                </Select>
                            </Col>
                            <Col span={21}>
                                <TextArea placeholder="原因" rows={4} onChange={(e) => { this.setRejectReason(e); }} />
                            </Col>
                        </Row>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button disabled={this.state.is_send} type='primary' onClick={() => this.save()}>保存</Button>
                            <Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={() => this.back()}>返回</Button>
                        </Row>
                    </MainCard>
                </Row> : <Row type="flex" justify="center">
                        <Spin spinning={true} tip={"加载中"} size={"large"} /></Row>}
            </MainContent>
        );
    }
}

/**
 * 控件：服务产品审核控制器
 * 服务产品审核
 */
@addon('ServiceProductSh', '服务产品审核', '服务产品审核')
@reactControl(ServiceProductSh, true)
export class ServiceProductShControl extends BaseReactElementControl {
}