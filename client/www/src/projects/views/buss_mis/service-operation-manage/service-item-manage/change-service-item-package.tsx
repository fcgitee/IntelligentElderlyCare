import { Button, Card, InputNumber, Col, Form, Input, Row, Select, Radio, Checkbox, message, Steps } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import TextArea from "antd/lib/input/TextArea";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { Multiple } from "src/business/components/buss-components/multiple";
// import { ModalSearch } from "src/business/components/buss-components/modal-search";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { ModalSearch } from "src/business/components/buss-components/modal-search";
/**
 * 组件：名称状态
 */
export interface ChangeServiceItemPackageViewState extends ReactViewState {
    /** 项目列表 */
    item_list?: any[];
    /** 项目列表集合 */
    all_item_list?: any;
    /** 服务产品列表 */
    service_product_list?: any[];
    /** 服务产品列表 */
    all_service_product_list?: any[];
    /** 基础数据 */
    base_data?: any;
    /** 适用范围 */
    application_scope?: any[];
    /** 组织机构 */
    organizational?: any[];
    /** 服务项目可视性 */
    is_service_item?: boolean;
    // 切换的下拉选择
    select_id?: any;
    catering_state?: any;
    product_step?: any;
    // is_external_show?: boolean;
    is_has_draft?: boolean;
    can_draft?: boolean;
    draft_info?: any;
    all_money?: number;
    service_times?: any;
    service_price?: any;
    is_send?: boolean;
}
export interface ServiceItemPackageFormValues {
    id?: string;
}

export const product_step = [{
    'step_name': '发布',
}, {
    'step_name': '审核',
}, {
    'step_name': '完成',
}];
/**
 * 组件：名称
 * 描述
 */
export class ChangeServiceItemPackageView extends ReactView<ChangeServiceItemPackageViewControl, ChangeServiceItemPackageViewState> {
    /** 总项目列表 */
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            all_item_list: null,
            base_data: {},
            application_scope: [],
            organizational: [],
            is_service_item: true,
            service_product_list: [],
            all_service_product_list: [],
            select_id: '',
            catering_state: false,
            product_step: product_step,
            // is_external_show: false,
            is_has_draft: false,
            can_draft: false,
            draft_info: [],
            service_times: [],
            service_price: [],
            all_money: 0,
            is_send: false,
        };
    }
    /** 提交方法 */
    handleSubmit = (e: any, type: boolean = false) => {
        e && e.preventDefault && e.preventDefault();
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        const { form } = this.props;
        let is_service_item = this.state.is_service_item;
        form!.validateFieldsAndScroll((err: Error, values: ServiceItemPackageFormValues) => {
            if (err) {
                let errKeys = Object.keys(err);
                // 在服务套餐下，这里会导致一直报错
                if (errKeys.length !== 1 || errKeys[0] !== 'service_item___0') {
                    return;
                }
            }
            let formValue = {};
            let service_item: any = [];
            let service_product: any = [];
            let service_option = this.state.all_item_list && this.state.all_item_list.service_option ? this.state.all_item_list.service_option : [];
            service_item.push({ service_options: [] });
            for (let key in values) {
                if (key.indexOf('___') !== -1) {
                    let keys = key.split('___');
                    if (keys[0] === 'service_type') {
                        service_item[parseInt(keys[1], undefined)][keys[0]] = values[key];
                    } else if (keys[0] === 'service_item') {
                        service_item[parseInt(keys[1], undefined)][keys[0]] = values[key];
                        formValue['service_item_id'] = values[key];
                    } else if (keys[0] === 'service_product') {
                        // 因为可以重复选，所以这里判断一下是否有，有的话就叠加解决，没有就新增
                        let isHave = false;
                        if (service_product.length > 0) {
                            for (let i in service_product) {
                                if (service_product[i]) {
                                    // 已经有，叠加数据
                                    if (service_product[i]['product_id'] === values[key]) {
                                        isHave = true;
                                        // 叠加服务次数
                                        service_product[i]['remaining_times'] = String(Number(service_product[i]['remaining_times']) + Number(values['service_times___' + keys[1]]));
                                        // 如果有设置服务套餐价格
                                        if (values['service_package_price___' + keys[1]]) {
                                            // 这里用最后一个覆盖
                                            service_product[i]['service_package_price'] = Number(values['service_package_price___' + keys[1]]);
                                        }
                                    }
                                    // 在最后一次来判断是否需要新增
                                    if (Number(i) === service_product.length - 1 && isHave === false) {
                                        // 新增一个服务产品
                                        let newProduct: any = {
                                            product_id: values[key],
                                            remaining_times: values['service_times___' + keys[1]],
                                        };
                                        // 判空，空的话不加
                                        if (values['service_package_price___' + keys[1]] !== '') {
                                            newProduct['service_package_price'] = Number(values['service_package_price___' + keys[1]]);
                                        }
                                        // 新增
                                        service_product.push(newProduct);
                                    }
                                }
                            }
                        } else {
                            // 新增一个服务产品
                            let newProduct: any = {
                                product_id: values[key],
                                remaining_times: values['service_times___' + keys[1]],
                            };
                            // 判空，空的话不加
                            if (values['service_package_price___' + keys[1]] !== '') {
                                newProduct['service_package_price'] = Number(values['service_package_price___' + keys[1]]);
                            }
                            service_product.push(newProduct);
                        }
                    } else if (keys[0] === 'valuation_formula') {
                        formValue['valuation_formula'] = values[key];
                    } else if (keys[0] === 'proportion') {
                        formValue['proportion'] = values[key];
                    } else {
                        is_service_item === true && service_option.forEach((item: any) => {
                            if (item['id'] === keys[0]) {
                                item['option_value'] = values[key];
                            }
                        });
                    }
                } else {
                    formValue[key] = values[key];
                }
            }
            if (is_service_item === true) {
                // 不允许不添加选项
                if (service_option.length === 0) {
                    message.info('请至少新增一个服务项目！');
                    return;
                }
                formValue['service_option'] = service_option;
            } else if (is_service_item === false) {
                if (service_product.length === 0) {
                    message.info('请至少新增一个服务细项！');
                    return;
                }
                formValue['service_product'] = service_product;
            }
            // formValue['service_item'] = service_item;
            // formValue['service_option'] = service_option;
            // formValue['service_product'] = service_product;
            formValue['id'] = this.props.match!.params.key;
            // 如果是服务产品，就固定这两个参数为1
            if (!is_service_item) {
                // 是否可分派
                formValue['is_assignable'] = '1';
                // 服务次数
                formValue['remaining_times'] = 1;
            }
            if (type === true) {
                // 保存草稿
                let param = {
                    'type': 'service_product',
                    'draft': formValue,
                };
                // 保存草稿
                AppServiceUtility.person_org_manage_service.create_draft!(param)!
                    .then((data) => {
                        if (data === 'Success') {
                            message.info('保存成功！', 1, () => {
                                history.back();
                            });
                        } else {
                            message.info(data);
                        }
                    });
            } else {
                this.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        AppServiceUtility.service_item_package.update_service_item_package!(formValue)!
                            .then((data) => {
                                if (data === 'Success') {
                                    message.info('提交成功！', 1, () => {
                                        history.back();
                                    });
                                } else {
                                    message.info(data);
                                    this.setState({
                                        is_send: false,
                                    });
                                }
                            })
                            .catch((error: any) => {
                                message.error(error);
                                this.setState({
                                    is_send: false,
                                });
                            });
                    }
                );
            }
        });
    }
    componentWillMount() {
        // 服务项目
        AppServiceUtility.services_project_service.get_services_project_list_by_scope!({})!
            .then((data: any) => {
                if (data) {
                    this.setState({
                        item_list: data,
                        all_item_list: data,
                    });
                }
            });
    }
    componentDidMount() {
        // 服务产品
        AppServiceUtility.service_item_package.get_service_item_package!({})!
            .then((data) => {
                this.setState({
                    service_product_list: data!.result,
                });
            });
        // 适用范围
        AppServiceUtility.service_scope_service.get_service_scope_list!({})!
            .then((data) => {
                let data_sourd: any[] = [];
                data!.result!.map((value) => {
                    data_sourd.push({ label: value.name, value: value.id });
                });
                this.setState({
                    application_scope: data_sourd
                });
            });
        // 组织机构
        AppServiceUtility.person_org_manage_service.get_organization_list_all!({})!
            .then((data) => {
                this.setState({
                    organizational: data.result,
                });
            });
        if (this.props.match!.params.key) {
            AppServiceUtility.service_item_package.get_service_item_package!({ id: this.props.match!.params.key, all: true })!
                .then((data) => {
                    this.setToForm(data!.result![0]);
                });
        } else {
            this.setState({
                can_draft: true,
            });
            // 检测草稿
            AppServiceUtility.person_org_manage_service.get_draft_list!({ type: 'service_product' })!
                .then((data) => {
                    if (data && data.result && data.result.length > 0) {
                        this.setState({
                            is_has_draft: true,
                            draft_info: data.result[0],
                        });
                    }
                });
        }
    }
    // 把已有的放回到表单上，需要特殊处理
    setToForm(data_info: any, msg: string = '') {
        // 基础处理
        let param: any = {
            // 源数据
            base_data: data_info,
            // 服务产品/套餐标识
            is_service_item: data_info['is_service_item'] === 'true'
        };
        if (data_info.is_service_item === 'true') {
            // 服务产品，要把服务套餐的列表清空，否则表单会报套餐字段的空错
            param['all_service_product_list'] = [];
        } else {
            // 服务套餐，要把服务产品的列表清空，否则表单会报产品字段的空错
            param['all_item_list'] = null;
            // 给服务套餐一个空数组，防止赋值错误
            param['all_service_product_list'] = [];
            // 赋值key字段，防止重复
            for (let i = 0; i < data_info['service_product'].length; i++) {
                param['all_service_product_list'].push(this.randomString(8));
            }
        }
        // 赋值
        this.setState(
            param,
            () => {
                if (data_info.is_service_item === 'true' && data_info['service_item_id']) {
                    // 服务产品
                    this.itemOnChange(data_info['service_item_id'], { key: "0" }, msg);
                } else {
                    // 服务套餐
                    this.taocanSetValue(param['all_service_product_list'], data_info['service_product'], msg);
                }
            }
        );
    }
    readDraft() {
        let { draft_info, item_list } = this.state;

        if (item_list!.length === 0) {
            message.info('加载中，请稍候再试！');
            return;
        }
        if (!draft_info.draft) {
            message.info('没有找到草稿！');
            return;
        }
        this.setToForm(draft_info.draft, '读取草稿成功！');
    }

    taocanSetValue(keyList: any, valueList: any, msg: string = '') {
        let param: any = {};
        let newServicePrice: any = [];
        let newServiceTimes: any = [];

        // 理论上keyList和valueList数量是一样的，不一样的话就。。。
        for (let i = 0; i < keyList.length; i++) {
            newServicePrice[keyList[i]] = valueList[i]['service_package_price'];
            newServiceTimes[keyList[i]] = valueList[i]['remaining_times'];
            param['service_package_price___' + keyList[i]] = valueList[i]['service_package_price'];
            param['service_product___' + keyList[i]] = valueList[i]['product_id'];
            param['service_times___' + keyList[i]] = valueList[i]['remaining_times'];
        }
        this.setState(
            {
                service_price: newServicePrice,
                service_times: newServiceTimes,
            },
            () => {
                this.jisuan();
            }
        );
        this.props.form.setFieldsValue(param);
        if (msg !== '') {
            message.info(msg);
        }
    }

    itemOnChange = (value: any, option: any, msg: string = '') => {
        AppServiceUtility.services_project_service.get_services_project_list_by_scope!({ id: value })!
            .then((data: any) => {
                if (data && data.length > 0) {
                    if (typeof (data[0]['item_type']) === 'object' && data[0]['item_type'].length === 0) {
                        data[0]['item_type'] = ['未定义服务类型'];
                    }
                    this.setState(
                        {
                            select_id: value,
                            all_item_list: data[0]
                        },
                        () => {
                            if (msg !== '') {
                                message.info(msg);
                            }
                        }
                    );
                }
            });
    }
    /** 返回按钮 */
    retBtn = () => {
        this.props.history!.goBack();
    }
    /** 添加项目按钮 */
    addItem = () => {
        let list = this.state.all_item_list!;
        if (!list) {
            list = {};
        }
        this.setState({
            all_item_list: list
        });
    }
    /** 删除项目按钮 */
    delItem = () => {
        this.setState({
            all_item_list: null
        });
    }
    dropThis(value: any) {
        let { all_service_product_list, service_price, service_times } = this.state;

        let newAllServiceProductList: any = [];
        let newServicePrice: any = [];
        let newServiceTimes: any = [];

        for (let i in all_service_product_list) {
            if (all_service_product_list.hasOwnProperty(i) && value !== all_service_product_list[i]) {
                newAllServiceProductList.push(all_service_product_list[i]);
            }
        }

        for (let i in service_price) {
            if (service_price.hasOwnProperty(i) && value !== i) {
                newServicePrice[i] = service_price[i];
            }
        }
        for (let i in service_times) {
            if (service_times.hasOwnProperty(i) && value !== i) {
                newServiceTimes[i] = service_times[i];
            }
        }
        this.setState(
            {
                all_service_product_list: newAllServiceProductList,
                service_price: newServicePrice,
                service_times: newServiceTimes,
            },
            () => {
                this.jisuan();
            }
        );
    }
    randomString(len: number) {
        len = len || 32;
        let $chars: any = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        let maxPos: any = $chars.length;
        let pwd: any = '';
        for (let i: number = 0; i < len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    }
    addService = () => {
        let list: any[] | undefined = this.state.all_service_product_list;
        if (!list) {
            list = [];
        }
        list.push(this.randomString(8));
        this.setState({
            all_service_product_list: list
        });
    }
    /** 服务项目与服务产品卡片之间的切换 */
    toggleCard = (value: any) => {
        if (value.target.value === 'true') {
            this.setState({
                is_service_item: true,
                all_money: 0,
                all_service_product_list: [],
            });
        } else {
            this.setState({
                is_service_item: false,
                all_item_list: null,
            });
        }
    }
    typeChange = (value: any) => {
        if (value[0] === "餐饮服务") {
            this.setState({
                catering_state: true
            });
        } else {
            this.setState({
                catering_state: false
            });
        }
    }
    changeAllMoney = (e: any, key: string, type: string) => {
        let { service_times, service_price } = this.state;
        if (isNaN(e)) {
            return;
        }
        // 改变服务次数
        if (type === 'service_times') {
            service_times[key] = e;
            this.setState(
                {
                    service_times,
                },
                () => {
                    this.jisuan();
                }
            );
        } else if (type === 'service_price') {
            service_price[key] = e;
            this.setState(
                {
                    service_price,
                },
                () => {
                    this.jisuan();
                }
            );
        }
    }
    // 计算总金额
    jisuan = () => {
        let { service_times, service_price } = this.state;
        let objectKeys = Object.keys(service_price);
        let all_money = 0;
        for (let i = 0; i < objectKeys.length; i++) {
            // 次数默认1
            let times = service_times.hasOwnProperty(objectKeys[i]) ? (isNaN(service_times[objectKeys[i]]) ? 1 : Number(service_times[objectKeys[i]])) : 1;
            // 价格默认0
            let price = service_price.hasOwnProperty(objectKeys[i]) ? (isNaN(service_price[objectKeys[i]]) ? 0 : Number(service_price[objectKeys[i]])) : 0;
            // console.log(times, price, 'jisuan');
            all_money += times * price;
        }
        // console.log(objectKeys, service_times, service_price, all_money);
        this.setState({
            all_money,
        });
    }
    changeOrg(e: any) {
        // let organizational = this.state.organizational!;
        // for (let i = 0; i < organizational.length; i++) {
        //     if (organizational[i].id === e) {
        //         if (organizational[i].hasOwnProperty('organization_info') && organizational[i]['organization_info'].hasOwnProperty('personnel_category') && (organizational[i]['organization_info']['personnel_category'] === '福利院' || organizational[i]['organization_info']['personnel_category'] === '幸福院')) {
        //             this.setState({
        //                 is_external_show: true,
        //             });
        //         } else {
        //             this.setState({
        //                 is_external_show: false,
        //             });
        //         }
        //     }
        // }
    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form!;

        let { product_step, base_data, organizational, is_service_item, catering_state, all_item_list, is_has_draft, draft_info, can_draft, select_id, all_service_product_list } = this.state;
        // 如果服务项目不为空则生成
        let item_list;
        if (all_item_list) {
            const value = all_item_list;
            let keyId: string = base_data.service_item_id;
            if (keyId && keyId === select_id) {
                // 在编辑页面，如果选择的ID跟源ID一样，就赋值
                value.service_option = base_data.service_option;
                value.valuation_formula = base_data.valuation_formula;
            }
            item_list = (
                <Card>
                    <Row type="flex" justify="center">
                        <Col span={12}>
                            <Form.Item label='服务项目'>
                                {getFieldDecorator('service_item___' + 0, {
                                    initialValue: value.id,
                                    rules: [{
                                        required: true,
                                        message: '请选择服务项目'
                                    }]
                                })(
                                    <Select showSearch={true} onChange={this.itemOnChange} >
                                        {this.state.item_list!.map((key, index) => {
                                            return <Select.Option key={index} value={key['id']}>{key['name']}</Select.Option>;
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                            {
                                value.valuation_formula ? (
                                    <Form.Item label='计价方式'>
                                        {getFieldDecorator('valuation_formula___' + 0, {
                                            initialValue: value.valuation_formula ? value.valuation_formula : "",
                                            rules: [{
                                                required: false,
                                                message: '请选择计价方式'
                                            }],
                                            option: {
                                                disabled: true,
                                                placeholder: '请选择计价方式'
                                            }
                                        })(
                                            <Select>
                                                {typeof (value.valuation_formula) === 'string' ? <Select.Option key={0} value={value.valuation_formula}>{value.valuation_formula}</Select.Option> : value.valuation_formula.map((value: any, index: any) => {
                                                    return <Select.Option key={index} value={value}>{value.name}</Select.Option>;
                                                })}
                                            </Select>
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.proportion ? (
                                    <Form.Item label='分成比例'>
                                        {getFieldDecorator('proportion___' + 0, {
                                            initialValue: value.proportion ? value.proportion : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true,
                                            }
                                        })(
                                            <Multiple
                                                defaultFormat="{title}={contents}"
                                                btn_display={true}
                                                edit_form_items_props={[
                                                    {
                                                        type: 'input',
                                                        decorator_id: "title",
                                                        placeholder: "请输入经济主体",
                                                        required: true,
                                                        span: 7,
                                                        disabled: true
                                                    },
                                                    {
                                                        type: 'input',
                                                        decorator_id: "contents",
                                                        required: true,
                                                        placeholder: "请输入分成比例",
                                                        span: 15
                                                    }
                                                ]}
                                            />
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.type_name ? (
                                    <Form.Item label='服务类型'>
                                        {getFieldDecorator('service_type___' + 0, {
                                            initialValue: value.item_type ? value.item_type : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true
                                            }
                                        })(
                                            <Select>
                                                <Select.Option key={value.item_type || '0'} >{(typeof (value.type_name) === 'object' && value.type_name.length === 0) || value.type_name === '' ? '未定义服务类型' : value.type_name}</Select.Option>
                                            </Select>
                                            // 未定义服务类型是因为服务类型数据是不对的，指向不到类型
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {
                                value.is_allowance ? (
                                    <Form.Item label='是否补贴'>
                                        {getFieldDecorator('is_allowance___' + 0, {
                                            initialValue: value.is_allowance ? value.is_allowance : "",
                                            rules: [{
                                                required: false,
                                            }],
                                            option: {
                                                disabled: true
                                            }
                                        })(
                                            <Radio.Group>
                                                <Radio value={"1"}>是</Radio>
                                                <Radio value={"0"}>否</Radio>
                                            </Radio.Group>
                                        )}
                                    </Form.Item>
                                ) : ''
                            }
                            {(() => {
                                if (!value.service_option) {
                                    return null;
                                }
                                return value.service_option.map((service_option: any, indexs: any) => {
                                    if (service_option.option_type === 'service_after') {
                                        return null;
                                    }
                                    if (service_option.option_value_type === '1') {
                                        return (
                                            <Form.Item label={service_option.option_name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Radio.Group>
                                                        {
                                                            service_option.option_content!.map((item: any, content: any) => {
                                                                return <Radio key={content} value={item.option_value}>{item.option_content}</Radio>;
                                                            })
                                                        }
                                                    </Radio.Group>
                                                )}
                                            </Form.Item>
                                        );
                                    } else if (service_option.option_value_type === '2') {
                                        return (
                                            <Form.Item label={service_option.option_name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Checkbox.Group>
                                                        {
                                                            service_option.option_content!.map((item: any, idx: any) => {
                                                                return <Checkbox key={idx} value={item.option_value}>{item.option_content}</Checkbox>;
                                                            })
                                                        }
                                                    </Checkbox.Group>
                                                )}
                                            </Form.Item>
                                        );
                                    } else if (service_option.option_value_type === '3') {
                                        return (
                                            <Form.Item label={service_option.option_name} key={indexs}>
                                                {getFieldDecorator(service_option.id + '___' + 0, {
                                                    initialValue: service_option.option_value || (service_option.default_value || ''),
                                                    rules: [{
                                                        required: false,
                                                    }],
                                                })(
                                                    <Input autoComplete="off" />
                                                )}
                                            </Form.Item>
                                        );
                                    } else {
                                        return null;
                                    }
                                });
                            })()
                            }
                        </Col>
                    </Row>
                </Card>
            );
        }

        // 如果服务产品不为空则生成
        let product_list;
        if (all_service_product_list && all_service_product_list.length > 0) {
            product_list = all_service_product_list.map((value, index) => {
                return (
                    <Card key={value}>
                        <Row type="flex" justify="center">
                            <Col span={2} />
                            <Col span={12}>
                                <Form.Item label='服务产品'>
                                    {getFieldDecorator('service_product___' + value, {
                                        initialValue: value.product_id,
                                        rules: [{
                                            required: true,
                                            message: '请选择服务产品'
                                        }]
                                    })(
                                        <Select showSearch={true} key={index}>
                                            {this.state.service_product_list!.map((key) => {
                                                return <Select.Option key={index} value={key['id']}>{key['name']}</Select.Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='服务次数'>
                                    {getFieldDecorator('service_times___' + value, {
                                        initialValue: value.remaining_times || 1,
                                        rules: [{
                                            required: true,
                                            message: '请输入服务次数'
                                        }]
                                    })(
                                        <InputNumber style={{ minWidth: '300px' }} min={0} autoComplete="off" onChange={(e: any) => this.changeAllMoney(e, value, 'service_times')} />
                                    )}
                                </Form.Item>
                                <Form.Item label='服务套餐价格'>
                                    {getFieldDecorator('service_package_price___' + value, {
                                        initialValue: value.service_package_price || '',
                                        rules: []
                                    })(
                                        <InputNumber style={{ minWidth: '300px' }} min={0} autoComplete="off" onChange={(e: any) => this.changeAllMoney(e, value, 'service_price')} />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={2}>
                                <Button onClick={() => this.dropThis(value)}>删除</Button>
                            </Col>
                        </Row>
                    </Card>
                );
            });
        }
        // const { Option } = Select;
        const { Step } = Steps;
        let current = -1;
        if (base_data.hasOwnProperty('step_no')) {
            current = base_data['step_no'];
        }
        let stepStatus = {};
        if (base_data.hasOwnProperty('status')) {
            if (base_data['status'] !== '通过') {
                product_step[1].step_name = base_data['status'];
                product_step[1].description = `原因：${base_data['reason']}`;
                stepStatus['status'] = 'error';
            }
        }
        return (
            <MainContent>
                {current !== -1 ? <Steps current={current} {...stepStatus} style={{ background: "white", padding: "25px" }}>
                    {product_step.map((item: any, index: number) => {
                        return (
                            <Step key={index} title={item.step_name} description={item.description} />
                        );
                    })}
                </Steps> : null}
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <MainCard title='服务产品'>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='服务产品名称'>
                                    {getFieldDecorator('name', {
                                        initialValue: base_data['name'] ? base_data['name'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请输入服务产品名称'
                                        }],
                                    })(
                                        <Input autoComplete="off" />
                                    )}
                                </Form.Item>
                                <Form.Item label='所属机构'>
                                    {getFieldDecorator('organization_id', {
                                        initialValue: base_data['organization_id'] ? base_data['organization_id'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择所属机构'
                                        }],
                                    })(
                                        <Select showSearch={true} onChange={(e) => this.changeOrg(e)}>
                                            {
                                                organizational ? organizational.map((option: any, index: number) => {
                                                    return (<Select.Option key={option.id} value={option.id}>{option.name}</Select.Option>);
                                                }) : null
                                            }
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='简介'>
                                    {getFieldDecorator('introduce', {
                                        initialValue: base_data['introduce'] ? base_data['introduce'] : '',
                                    })(
                                        <TextArea autoComplete="off" />
                                    )}
                                </Form.Item>
                                <Form.Item label='服务产品介绍'>
                                    {getFieldDecorator('service_product_introduce', {
                                        initialValue: base_data['service_product_introduce'] ? base_data['service_product_introduce'] : '',
                                        rules: [{
                                            required: false,
                                            message: '请输入服务产品介绍'
                                        }],
                                    })(
                                        <TextArea autoComplete="off" />
                                    )}
                                </Form.Item>
                                <Form.Item label='备注'>
                                    {getFieldDecorator('remarks', {
                                        initialValue: base_data['remarks'] ? base_data['remarks'] : '',
                                    })(
                                        <TextArea autoComplete="off" />
                                    )}
                                </Form.Item>
                                {/* <Form.Item label='是否启用'>
                                    {getFieldDecorator('state', {
                                        initialValue: this.state.base_data!['state'] ? this.state.base_data!['state'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择'
                                        }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={1}>启用</Radio>
                                            <Radio value={0}>停用</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item> */}
                                {is_service_item ? <Form.Item label='是否需要分派'>
                                    {getFieldDecorator('is_assignable', {
                                        initialValue: base_data['is_assignable'] ? base_data['is_assignable'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择'
                                        }],
                                    })(
                                        <Radio.Group>
                                            <Radio value="1">是</Radio>
                                            <Radio value="0">否 </Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item> : null}
                                {/* {is_external_show === true ? <Form.Item label='是否外部产品'>
                                    {getFieldDecorator('is_external', {
                                        initialValue: base_data['is_external'] ? base_data['is_external'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择'
                                        }],
                                    })(
                                        <Radio.Group>
                                            <Radio value="1">是</Radio>
                                            <Radio value="0">否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item> : null} */}
                                <Form.Item label='是否推荐产品'>
                                    {getFieldDecorator('is_recommend', {
                                        initialValue: base_data['is_recommend'] ? base_data['is_recommend'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择'
                                        }],
                                    })(
                                        <Radio.Group>
                                            <Radio value={'是'}>是</Radio>
                                            <Radio value={'否'}>否</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                                {/* <Form.Item label='产品类型'>
                                    {getFieldDecorator('service_product_type', {
                                        initialValue: this.state.base_data!['service_product_type'] ? this.state.base_data!['service_product_type'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择产品类型'
                                        }],
                                    })(
                                        <Cascader options={this.state.service_Type_list} onChange={(value: any) => this.typeChange(value)} placeholder="请选择产品类型" />
                                        // <Select showSearch={true} onChange={(value: any) => this.typeChange(value)}>
                                        //     <Select.Option key={'助餐服务'}>助餐服务</Select.Option>
                                        //     <Select.Option key={'助洁服务'}>助洁服务</Select.Option>
                                        //     <Select.Option key={'居家服务'}>居家服务</Select.Option>
                                        //     <Select.Option key={'幸福院'}>幸福院</Select.Option>
                                        // </Select>
                                    )}
                                </Form.Item> */}
                                {/* {is_service_item ? <Form.Item label='服务次数'>
                                    {getFieldDecorator('remaining_times', {
                                        initialValue: base_data['remaining_times'] ? base_data['remaining_times'] : '',
                                        rules: [{
                                            required: false,
                                            message: '请输入服务次数'
                                        }],
                                    })(
                                        <Input autoComplete="off" />
                                    )}
                                </Form.Item> : null} */}
                                {catering_state === true ?
                                    <Col>
                                        <Form.Item label='所属星期'>
                                            {getFieldDecorator('catering_week', {
                                                initialValue: base_data['catering_week'] ? base_data['catering_week'] : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择所属星期'
                                                }],
                                            })(
                                                <Select showSearch={true}>
                                                    <Select.Option key={'星期一'}>星期一</Select.Option>
                                                    <Select.Option key={'星期二'}>星期二</Select.Option>
                                                    <Select.Option key={'星期三'}>星期三</Select.Option>
                                                    <Select.Option key={'星期四'}>星期四</Select.Option>
                                                    <Select.Option key={'星期五'}>星期五</Select.Option>
                                                    <Select.Option key={'星期六'}>星期六</Select.Option>
                                                    <Select.Option key={'星期日'}>星期日</Select.Option>
                                                </Select>
                                            )}
                                        </Form.Item>
                                        <Form.Item label='午餐/晚餐'>
                                            {getFieldDecorator('catering_time', {
                                                initialValue: base_data['catering_time'] ? base_data['catering_time'] : '',
                                                rules: [{
                                                    required: true,
                                                    message: '请选择午餐/晚餐'
                                                }],
                                            })(
                                                <Radio.Group>
                                                    <Radio value="午餐">午餐</Radio>
                                                    <Radio value="晚餐">晚餐</Radio>
                                                </Radio.Group>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    :
                                    ''}
                                <Form.Item label='服务产品图片（大小小于2M，格式支持jpg/jpeg/png）'>
                                    {getFieldDecorator('picture_collection', {
                                        initialValue: base_data['picture_collection'] ? base_data['picture_collection'] : '',
                                    })(
                                        <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                    )}
                                </Form.Item>
                                {/* <Form.Item label='附加合同条款'>
                                    {getFieldDecorator('additonal_contract_terms', {
                                        initialValue: this.state.base_data!['additonal_contract_terms'] ? this.state.base_data!['additonal_contract_terms'] : '',
                                        rules: [{
                                            required: true,
                                            message: '请输入附加合同条款'
                                        }],
                                    })(
                                        <TextArea autoComplete="off" />
                                    )}
                                </Form.Item> */}
                                <Form.Item label='服务类别'>
                                    {getFieldDecorator('is_service_item', {
                                        initialValue: base_data['is_service_item'] ? base_data['is_service_item'] : is_service_item ? 'true' : 'false',
                                        rules: [{
                                            required: true,
                                            message: ''
                                        }],
                                    })(
                                        <Radio.Group onChange={this.toggleCard}>
                                            <Radio value={'false'}>服务套餐</Radio>
                                            <Radio value={'true'}>单个项目</Radio>
                                        </Radio.Group>
                                        // <Select showSearch={true} onChange={this.toggleCard}>
                                        //     <Option value="true">是</Option>
                                        //     <Option value="false">否</Option>
                                        // </Select>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </MainCard>
                    {is_service_item ?
                        <Card title='服务项目'>
                            {item_list}
                            <Button type='primary' onClick={this.addItem} disabled={all_item_list}>新增服务项目</Button>
                            <Button type='primary' onClick={this.delItem} disabled={all_item_list === null}>删除</Button>
                        </Card> :
                        <Card title='服务细项' extra={<Row type="flex" justify="end">总金额：{this.state.all_money}</Row>}>
                            {product_list}
                            <Button type='primary' onClick={this.addService}>新增细项</Button>
                        </Card>}
                    <Card style={{ marginTop: '20px' }}>
                        <Row type="flex" justify="center" style={{ background: '#fff' }}>
                            {can_draft ? <span><Button disabled={this.state.is_send} onClick={() => this.handleSubmit(false, true)}>保存草稿</Button>&nbsp;&nbsp;&nbsp;</span> : null}
                            <Button disabled={this.state.is_send} htmlType='submit' >提交审核</Button>&nbsp;&nbsp;&nbsp;<Button disabled={this.state.is_send} type='ghost' name='返回' htmlType='button' onClick={this.retBtn}>返回</Button>
                        </Row>
                        {is_has_draft ? <Row type="flex" style={{ background: '#fff', marginTop: '10px' }} justify="center">上次草稿保存时间：{draft_info.create_date}，可选择&nbsp;&nbsp;&nbsp;<span style={{ color: '#1890ff', cursor: 'pointer' }} onClick={() => this.readDraft()}>读取草稿</span></Row> : null}
                    </Card>
                </Form>
            </MainContent>
        );
    }
}

/**
 * 控件：名称控制器
 * 描述
 */
@addon('ChangeServiceItemPackageView', '名称', '描述')
@reactControl(Form.create<any>()(ChangeServiceItemPackageView), true)
export class ChangeServiceItemPackageViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}