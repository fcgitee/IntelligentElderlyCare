import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from 'src/projects/app/appService';

let { Option } = Select;

/**
 * 组件：服务商登记编辑页面状态
 */
export interface ChangeServiceItemViewState extends ReactViewState {
    /** 服务商列表 */
    item_list?: [];
    /** 服务类型列表 */
    service_type_list?: [];
}

/**
 * 组件：服务商登记编辑页面
 * 描述
 */
export class ChangeServiceItemView extends ReactView<ChangeServiceItemViewControl, ChangeServiceItemViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            service_type_list: []
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list_all!({}))
            .then((datas: any) => {
                this.setState({
                    item_list: datas.result,
                });
            });
        request(this, AppServiceUtility.service_operation_service.get_service_type_list!({}))
            .then((datas: any) => {
                this.setState({
                    service_type_list: datas.result,
                });
            });
    }
    render() {
        let item = this.state.item_list;
        let item_list: any[] = [];
        item!.map((item, idx) => {
            item_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let service_type_list: any[] = [];
        this.state.service_type_list!.map((item, idx) => {
            service_type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props = {
            form_items_props: [
                {
                    title: "服务项目登记",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "服务商",
                            decorator_id: "service_provider_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务商" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择服务商",
                                childrens: item_list,
                            }
                        }, {
                            type: InputType.select,
                            label: "服务类型",
                            decorator_id: "service_type_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务类型",
                                childrens: service_type_list,
                            }
                        }, {
                            type: InputType.antd_input_number,
                            label: "服务项目费用",
                            decorator_id: "item_fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务项目费用" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务项目费用"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "服务项目内容",
                            decorator_id: "item_content",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务项目内容"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "服务项目参数",
                            decorator_id: "service_item_param",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入服务项目参数" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务项目参数"
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.serviceItem);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: "get_service_item_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_item"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.serviceItem); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：服务商登记编辑页面
 * 描述
 */
@addon('ChangeServiceItemView', '服务商登记编辑页面', '描述')
@reactControl(ChangeServiceItemView, true)
export class ChangeServiceItemViewControl extends ReactViewControl {
}