import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { message, Select, Modal } from "antd";
import { request } from "src/business/util_tool";
const { Option } = Select;
const { confirm } = Modal;
/**
 * 组件：服务项目包列表状态
 */
export interface ServiceItemPackageViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    is_send?: boolean;
    /** 项目类别 */
    service_type_list?: any[];
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：服务项目包列表
 * 描述
 */
export class ServiceItemPackageView extends ReactView<ServiceItemPackageViewControl, ServiceItemPackageViewState> {
    private columns_data_source = [{
        title: '服务产品',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '所属机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    },
    {
        title: '服务类别',
        dataIndex: 'is_service_item',
        key: 'is_service_item',
        render: (text: any, record: any) => {
            if (text === 'true') {
                return `单个项目`;
            } else if (text === 'false') {
                return '服务套餐';
            }
            return text;
        },
    },
    {
        title: '审批状态',
        dataIndex: 'status',
        key: 'status',
        render: (text: any, record: any) => {
            if (text === '不通过') {
                return `不通过，原因：【${record.reason || '无'}】`;
            }
            return text;
        },
    },
    {
        title: '使用状态',
        dataIndex: 'state',
        key: 'state',
        render: (text: string, record: any) => {
            if (record.hasOwnProperty('status') && record['status'] === '通过') {
                return text === '启用' ? '已启用' : '已停用';
            }
            return '';
        }
    },
    {
        title: '是否推荐产品',
        dataIndex: 'is_recommend',
        key: 'is_recommend',
    }, {
        title: '审批时间',
        dataIndex: 'audit_date',
        key: 'audit_date',
    }, {
        title: '上次修改时间',
        dataIndex: 'modify_date',
        key: 'modify_date',
    }, {
        title: '发布时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },];
    constructor(props: ServiceItemPackageViewControl) {
        super(props);
        this.state = {
            request_url: '',
            service_type_list: [],
            is_send: false,
            org_list: [],
        };
    }
    onChangeThis(record: any) {
        let state = record.state === '启用' ? '停用' : '启用';
        let that = this;
        confirm({
            title: `是否确认${state}该产品：【${record.name}】`,
            onOk() {
                if (that.state.is_send === true) {
                    message.info('请勿操作太快！');
                    return;
                }
                that.setState(
                    {
                        is_send: true,
                    },
                    () => {
                        request(that, AppServiceUtility.service_item_package.update_service_item_package!({ id: record.id, state: state, pureEdit: true }))
                            .then((data: any) => {
                                if (data === 'Success') {
                                    message.info('操作成功！', 1, () => {
                                        window.location.reload();
                                    });
                                } else {
                                    message.info(data);
                                    that.setState({
                                        is_send: false,
                                    });
                                }
                            });
                    }
                );
            },
            onCancel() { },
        });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        /** 获取服务类别 */
        request(this, AppServiceUtility.service_operation_service.get_service_type_list_tree!({}))
            .then((datas: any) => {
                if (datas) {
                    this.setState({
                        service_type_list: [{
                            children: [],
                            id: "套餐",
                            label: "套餐",
                            value: "套餐",
                        }].concat(datas.result)
                    });
                }
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceItemPackage);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            if (this.props.hasOwnProperty('list_type') && this.props.list_type === 'sh') {
                this.props.history!.push(ROUTE_PATH.changeServiceItemPackageSh + '/' + contents.id);
            } else {
                if (contents.hasOwnProperty('status') && contents['status'] === '待审批') {
                    message.info('该产品待审批中，请耐心等候！');
                    return;
                }
                this.props.history!.push(ROUTE_PATH.changeServiceItemPackage + '/' + contents.id);
            }
        } else if ('icon_state' === type) {
            this.onChangeThis(contents);
        }
    }
    render() {
        let is_sh = this.props.hasOwnProperty('list_type') && this.props.list_type === 'sh' ? true : false;
        let service_item = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务产品",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入服务产品"
                    }
                },
                {
                    type: InputType.select,
                    label: "服务类别",
                    decorator_id: "is_service_item",
                    option: {
                        allowClear: true,
                        placeholder: "请选择服务类别",
                        childrens: [{
                            value: 'true',
                            label: '单个项目'
                        }, {
                            value: 'false',
                            label: '服务套餐'
                        }].map((item: any) => <Option key={item.value}>{item.label}</Option>),
                    }
                },
                // (is_sh ? {} : {
                //     type: InputType.cascader,
                //     label: "服务类型",
                //     decorator_id: "item_type_id",
                //     option: {
                //         options: this.state.service_type_list,
                //         placeholder: "请选择服务类型",
                //         fieldNames: { label: 'label', value: 'id', children: 'children' },
                //     }
                // })
                // ,
                {
                    type: InputType.select,
                    label: "审批状态",
                    decorator_id: "status",
                    option: {
                        allowClear: true,
                        placeholder: "请选择审批状态",
                        childrens: [{
                            value: '待审批',
                            label: '待审批'
                        }, {
                            value: '通过',
                            label: '通过'
                        }, {
                            value: '不通过',
                            label: '不通过'
                        }].map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                    }
                },
                (is_sh ? {
                    type: InputType.select,
                    label: "使用状态",
                    decorator_id: "state",
                    option: {
                        allowClear: true,
                        placeholder: "请选择使用状态",
                        childrens: ['启用', '停用'].map((item: any) => <Option key={item} value={item}>{item}</Option>),
                    }
                } : {})
                ,
                // {
                //     type: InputType.input,
                //     label: "所属机构",
                //     decorator_id: "organization_name",
                //     option: {
                //         placeholder: "请输入所属机构",
                //     }
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.select,
                    label: "是否推荐产品",
                    decorator_id: "is_recommend",
                    option: {
                        allowClear: true,
                        placeholder: "请选择",
                        childrens: [{
                            value: '是',
                            label: '是'
                        }, {
                            value: '否',
                            label: '否'
                        }].map((item: any) => <Option key={item.value} value={item.value}>{item.label}</Option>),
                    }
                }
            ],
            btn_props: [{
                label: '新增服务产品',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_item_package,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_service_item_package'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let new_table_param: any = {
            other_label_type: [
                {
                    type: 'button',
                    label_text: '',
                    label_key: 'icon_state',
                    label_parameter: { size: 'small' },
                    label_text_func(record: any) {
                        if (record.hasOwnProperty('status') && record['status'] === '通过') {
                            return record.hasOwnProperty('state') && record['state'] === '停用' ? '启用' : '停用';
                        }
                        return undefined;
                    }
                },
                { ...table_param.other_label_type[0] }
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let service_item_list = Object.assign(service_item, new_table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 控件：服务项目包列表页面
 * 描述
 */
@addon('ServiceItemPackageView', '服务项目包列表页面', '描述')
@reactControl(ServiceItemPackageView, true)
export class ServiceItemPackageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    // 列表类型
    public list_type?: string;
}