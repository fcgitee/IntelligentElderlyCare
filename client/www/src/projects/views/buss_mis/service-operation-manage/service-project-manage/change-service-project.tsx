import { message, Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
export const options = [
    {
        value: '生活照料',
        label: '生活照料',
        children: [
            {
                value: '个人清洁',
                label: '个人清洁',
                children: []
            },
            {
                value: '饮食照料',
                label: '饮食照料',
            },
            {
                value: '排泄照料',
                label: '排泄照料',
            },
        ],
    },
    {
        value: '家政服务',
        label: '家政服务',
        children: [
            {
                value: '饮食料理',
                label: '饮食料理',
            },
            {
                value: '居室清洁',
                label: '居室清洁',
            },
            {
                value: '更换洗涤',
                label: '更换洗涤',
            },
        ],
    },
    {
        value: '餐饮服务',
        label: '餐饮服务',
        children: [
            {
                value: '集中用餐',
                label: '集中用餐',
            },
            {
                value: '送餐服务',
                label: '送餐服务',
            },
        ],
    },
    {
        value: '陪同服务',
        label: '陪同服务',
        children: [
            {
                value: '陪同购物',
                label: '陪同购物',
            },
            {
                value: '陪同就医',
                label: '陪同就医',
            },
            {
                value: '陪同旅游',
                label: '陪同旅游',
            },
        ],
    },
    {
        value: '心理慰藉',
        label: '心理慰藉',
        children: [
            {
                value: '普通谈心',
                label: '普通谈心',
            },
            {
                value: '专业慰藉',
                label: '专业慰藉',
            },
        ],
    },
    {
        value: '法律援助',
        label: '法律援助',
        children: [
            {
                value: '法律咨询',
                label: '法律咨询',
            },
            {
                value: '权益维护',
                label: '权益维护',
            },
        ],
    },
    {
        value: '文化教育',
        label: '文化教育',
        children: [
            {
                value: '中医养生',
                label: '中医养生',
            },
            {
                value: '护理康复',
                label: '护理康复',
            },
            {
                value: '心理健康',
                label: '心理健康',
            },
            {
                value: '艺术教学',
                label: '艺术教学',
            },
            {
                value: '外语教学',
                label: '外语教学',
            },
        ],
    },
    {
        value: '健身娱乐',
        label: '健身娱乐',
        children: [
            {
                value: '球类',
                label: '球类',
            },
            {
                value: '舞蹈',
                label: '舞蹈',
            },
            {
                value: '乐器',
                label: '乐器',
            },
            {
                value: '茶艺',
                label: '茶艺',
            },
            {
                value: '唱歌',
                label: '唱歌',
            },
            {
                value: '手工',
                label: '手工',
            },
            {
                value: '摄影',
                label: '摄影',
            },
            {
                value: '游戏',
                label: '游戏',
            },
            {
                value: '花卉',
                label: '花卉',
            },
            {
                value: '钓鱼',
                label: '钓鱼',
            },
        ],
    },
    {
        value: '安宁服务',
        label: '安宁服务',
        children: [
            {
                value: '临终关怀',
                label: '临终关怀',
            },
            {
                value: '后事指导',
                label: '后事指导',
            },
        ],
    },
    {
        value: '安全援助',
        label: '安全援助',
        children: [
            {
                value: '紧急呼援产品',
                label: '紧急呼援产品',
            }
        ],
    },
    {
        value: '康复护理',
        label: '康复护理',
        children: [
            {
                value: '肢体活动帮助',
                label: '肢体活动帮助',
            },
            {
                value: '仪器使用协助',
                label: '仪器使用协助',
            }
        ],
    },
    {
        value: '医疗保健',
        label: '医疗保健',
        children: [
            {
                value: '测量血压、体温、脉搏',
                label: '测量血压、体温、脉搏',
            },
            {
                value: '家庭病床',
                label: '家庭病床',
            },
            {
                value: '换药',
                label: '换药',
            },
            {
                value: '理疗',
                label: '理疗',
            },
            {
                value: '刮痧',
                label: '刮痧',
            },
            {
                value: '针灸',
                label: '针灸',
            },
        ],
    },
    {
        value: '日托服务',
        label: '日托服务',
        children: [
            {
                value: '日间托养服务',
                label: '日间托养服务',
            }
        ],
    },
    {
        value: '日间托养服务',
        label: '日间托养服务',
        children: [
            {
                value: '志愿者上门服务',
                label: '志愿者上门服务',
            }
        ],
    },
    {
        value: '特色服务',
        label: '特色服务',
        children: [
            {
                value: '居家套餐',
                label: '居家套餐',
            }
        ],
    },
    {
        value: '其他服务',
        label: '其他服务',
        children: [
            {
                value: '机构服务',
                label: '机构服务',
            }
        ],
    },
];
/**
 * 组件：编辑服务项目状态
 */
export interface ChangeServiceProjectViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目类别 */
    item_type_list?: any[];
    /** 所属组织机构 */
    organization_list?: any[];
    /** 服务选项 */
    option_name_list?: any[];
    /** 适用范围 */
    application_scope?: any[];
    /** 服务周期 */
    periodicity_list?: any[];
    /** 选项属性列表 */
    option_object_list?: any[];
    /** 服务项目分类树形结构列表 */
    service_type_list?: any[];
    is_send: boolean;
}

/**
 * 组件：编辑服务项目视图
 */
export class ChangeServiceProjectView extends ReactView<ChangeServiceProjectViewControl, ChangeServiceProjectViewState> {
    private default_data = [{
        title: '平台',
        contents: 0,
    }, {
        title: '服务商',
        contents: 1,
    }, {
        title: '慈善',
        contents: 0,
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            item_type_list: [],
            organization_list: [],
            option_name_list: [],
            periodicity_list: [],
            service_type_list: options,
            is_send: false,
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.serviceProject);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        // 20190904修复问题，更新没有传入主键ID，导致无法更新
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                const id = this.props.match!.params.key;
                if (id) {
                    value['id'] = id;
                }
                if (value['service_option'].length < 1) {
                    message.error('请至少选择一项选项！');
                    return;
                }
                // 将option的属性信息加到表中
                let service_option = [];
                for (let i = 0; i < value['service_option'].length; i++) {
                    let flag = false;
                    for (let j = 0; j < this.state.option_object_list!.length; j++) {
                        if (value['service_option'][i].option_name === this.state.option_object_list![j]['id']) {
                            flag = true;
                            service_option.push({
                                name: this.state.option_object_list![j].name,
                                id: this.state.option_object_list![j].id,
                                option_content: this.state.option_object_list![j].option_content,
                                option_value_type: this.state.option_object_list![j].option_value_type,
                                default_value: this.state.option_object_list![j].default_value,
                                is_modification_mermissible: this.state.option_object_list![j].is_modification_mermissible,
                                option_type: this.state.option_object_list![j].option_type,
                                remarks: this.state.option_object_list![j].remarks,
                                option_value: this.state.option_object_list![j].option_value || '0',
                            });
                        }
                    }
                    // 没有找到，说明是原数据，不需要数据转换
                    if (!flag) {
                        // 但是需要还原转换，把option_name还原成name
                        let temp = {
                            name: '',
                            id: '',
                            option_content: value['service_option'][i].option_content,
                            option_value_type: value['service_option'][i].option_value_type,
                            default_value: value['service_option'][i].default_value,
                            is_modification_mermissible: value['service_option'][i].is_modification_mermissible,
                            option_type: value['service_option'][i].option_type,
                            remarks: value['service_option'][i].remarks,
                            option_value: value['service_option'][i].option_value
                        };
                        temp.name = value['service_option'][i].name || value['service_option'][i].option_name;
                        temp.id = value['service_option'][i].id || value['service_option'][i].option_id;
                        service_option.push(temp);
                    }
                }
                value['service_option'] = service_option;
                AppServiceUtility.services_project_service.update_services_project!(value)!
                    .then((data) => {
                        if (data) {
                            message.info('保存成功！', 1, () => {
                                this.props.history!.push(ROUTE_PATH.serviceProject);
                            });
                        } else {
                            this.setState({
                                is_send: false
                            });
                        }
                    }).catch((err: any) => {
                        console.error(err);
                        this.setState({
                            is_send: false
                        });
                    });
            }
        );
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
        }
        // /** 获取服务类别 */
        // AppServiceUtility.services_item_category_service.get_services_item_category_list_all!({})!
        //     .then((data: any) => {
        //         if (data) {
        //             let item_category = data.result.map((key: any, value: any) => {
        //                 return <Select.Option key={key.id}>{key.name}</Select.Option>;
        //             });
        //             this.setState({
        //                 item_type_list: item_category
        //             });
        //         }
        //     });
        /** 获取组织列表 */
        AppServiceUtility.person_org_manage_service.get_organization_list_all!({})!
            .then((data: any) => {
                if (data) {
                    let organization = data.result.map((key: any, value: any) => {
                        return <Select.Option key={key.id}>{key.name}</Select.Option>;
                    });
                    this.setState({
                        organization_list: organization
                    });
                }
            });
        /** 获取服务周期列表 */
        // AppServiceUtility.services_project_service.get_services_periodicity_list!({})!
        //     .then((data: any) => {
        //         if (data) {
        //             let periodicity_list = data.result.map((key: any, value: any) => {
        //                 return <Select.Option key={key.id}>{key.name}</Select.Option>;
        //             });
        //             this.setState({
        //                 periodicity_list
        //             });
        //         }
        //     });
        AppServiceUtility.service_option_service.get_servicre_option_list_all!({})!
            .then((data: any) => {
                if (data) {
                    let option_name_list = data.result.map((key: any, value: any) => {
                        return <Select.Option key={key.id}>{key.name}</Select.Option>;
                    });
                    this.setState(
                        {
                            option_object_list: data.result,
                            option_name_list,
                        },
                        () => {
                            this.forceUpdate();
                        }
                    );
                }
            });
        AppServiceUtility.service_scope_service.get_service_scope_list_all!({})!
            .then((data: any) => {
                if (data) {
                    let application_scope = data.result.map((key: any, value: any) => {
                        return { label: key.name, value: key.id };
                    });
                    this.setState({
                        application_scope
                    });
                }
            });

        AppServiceUtility.service_operation_service.get_service_type_list_tree!({})!
            .then((data: any) => {
                if (data) {
                    this.setState({
                        service_type_list: data.result
                    });
                }
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let table_option = {
            table_field_name: 'service_option',
            // table配置
            columns_data_source: [{
                title: '选项名称',
                dataIndex: 'option_name',
                key: 'option_name',
                type: 'select' as ColTypeStr,
                option: {
                    placeholder: "请选择",
                    childrens: this.state.option_name_list
                }
            },],
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: true,
            show_footer: true,
            rowKey: 'id',
            noFilter: true,
            forbidOldEdit: true,
            service_option: {
                service_object: AppServiceUtility.services_project_service,
                operation_option: {
                    query: {
                        func_name: "get_services_project_list2",
                        arguments: [this.state.selete_obj, 1, 10]
                    },
                },
            }
        };

        let form_items_props = [
            {
                type: InputTypeTable.input,
                label: "服务细项名称",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入服务细项名称", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "name",
            },
            // {
            //     type: InputTypeTable.select,
            //     label: "服务周期",
            //     field_decorator_option: {
            //         rules: [{ required: false, message: "" }],
            //     } as GetFieldDecoratorOptions,
            //     decorator_id: "periodicity",
            //     option: {
            //         placeholder: "请选择服务周期",
            //         childrens: this.state.periodicity_list
            //     }
            // },
            {
                type: InputTypeTable.select,
                label: "所属组织机构",
                field_decorator_option: {
                    rules: [{ required: false, message: "" }],
                } as GetFieldDecoratorOptions,
                decorator_id: "organization_id",
                option: {
                    placeholder: "请选择所属组织机构",
                    childrens: this.state.organization_list
                }
            }, {
                type: InputTypeTable.cascader,
                label: "服务类型",
                field_decorator_option: {
                    rules: [{ required: true, message: "请选择服务类型", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "item_type",
                option: {
                    placeholder: "请选择服务类型",
                    options: this.state.service_type_list,
                    fieldNames: { label: 'label', value: 'id', children: 'children' },
                    // onChange: (value: any) => this.typeChange(value)
                }
            },
            // {
            //     type: InputTypeTable.input,
            //     label: "服务介绍页面",
            //     field_decorator_option: {
            //         rules: [{ required: false, message: "" }],
            //     } as GetFieldDecoratorOptions,
            //     decorator_id: "service_project_website",
            // }, 
            {
                type: InputTypeTable.check_box,
                label: "适用范围",
                field_decorator_option: {
                    rules: [{ required: true, message: "请选择适用范围", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "application_scope",
                option: this.state.application_scope
            }, {
                type: InputTypeTable.radio,
                label: "是否标准服务",
                field_decorator_option: {
                    rules: [{ required: true, message: "请选择是否标准服务", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "is_standard_service",
                option: [
                    {
                        label: '是',
                        value: '是'
                    }, {
                        label: '否',
                        value: '否'
                    },
                ]
            },
            {
                type: InputTypeTable.radio,
                label: "是否补贴",
                field_decorator_option: {
                    rules: [{ required: true, message: "请选择是否补贴", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "is_allowance",
                option: [
                    {
                        label: '是',
                        value: '1'
                    }, {
                        label: '否',
                        value: '0'
                    },
                ]
            },
            {
                type: InputTypeTable.multiple,
                label: "分成比例",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入分成比例" }],
                    initialValue: this.default_data
                } as GetFieldDecoratorOptions,
                decorator_id: "proportion",
                option: {
                    title: "分成比例",
                    defaultKey: "contents",
                    defaultFormat: "{title}={contents}",
                    btn_display: true,
                    edit_form_items_props: [
                        {
                            type: 'input',
                            decorator_id: "title",
                            placeholder: "请输入经济主体",
                            required: true,
                            span: 7,
                            disabled: true
                        },
                        {
                            type: 'number',
                            decorator_id: "contents",
                            required: true,
                            placeholder: "请输入分成比例",
                            span: 15,
                        },
                    ],
                }
            },
            {
                type: InputTypeTable.multiple,
                label: "计价公式",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入计价公式" }],
                } as GetFieldDecoratorOptions,
                decorator_id: "valuation_formula",
                option: {
                    title: "计价公式",
                    defaultKey: "formula",
                    defaultFormat: "{name}={formula}",
                    edit_form_items_props: [
                        {
                            type: 'input',
                            decorator_id: "name",
                            placeholder: "请输入名称",
                            required: true,
                            span: 5,
                        },
                        {
                            type: 'input',
                            decorator_id: "remark",
                            placeholder: "请输入备注",
                            span: 5,
                        },
                        {
                            type: 'textarea',
                            decorator_id: "formula",
                            required: true,
                            placeholder: "请输入计价公式",
                            span: 12,
                        },
                    ],
                }
            },
            {
                type: InputTypeTable.input,
                label: "服务简介",
                field_decorator_option: {
                    rules: [{ required: false, message: "" }],
                } as GetFieldDecoratorOptions,
                decorator_id: "service_project_description",
            },
            {
                type: InputTypeTable.text_area,
                label: "合同条款",
                field_decorator_option: {
                    rules: [{ required: false, message: "" }],
                } as GetFieldDecoratorOptions,
                decorator_id: "contract_clauses",
            },
        ];

        let submit_props = {
            text: "保存",
            cb: this.save_value,
            btn_other_props: { disabled: this.state.is_send }
        };

        let add_row_text = "添加选项";

        let other_btn_propps =
            [{
                text: "返回",
                cb: () => {
                    this.props.history!.push(ROUTE_PATH.serviceProject);
                },
                btn_other_props: { disabled: this.state.is_send }
            }];

        return (
            <TableList {...table_option} form_items_props={form_items_props} submit_props={submit_props} add_row_text={add_row_text} other_btn_propps={other_btn_propps} />
        );
    }
}

/**
 * 控件：编辑服务项目编辑控件
 * @description 编辑服务项目编辑控件
 * @author
 */
@addon('ChangeServiceProjectView', '编辑服务项目编辑控件', '编辑服务项目编辑控件')
@reactControl(ChangeServiceProjectView, true)
export class ChangeServiceProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}