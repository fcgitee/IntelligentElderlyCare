
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';
import { request } from "src/business/util_tool";
let { Option } = Select;
// import { options } from "./change-service-project";
/** 状态：服务项目视图 */
export interface ServiceProjectViewState extends ReactViewState {
    /** 项目类别 */
    service_type_list?: any[];
    /** 接口名 */
    request_url?: string;
    application_scope?: any[];
    org_list?: any;

}
/** 组件：服务项目视图 */
export class ServiceProjectView extends React.Component<ServiceProjectViewControl, ServiceProjectViewState> {
    private columns_data_source = [{
        title: '服务细项名称',
        dataIndex: 'name',
        key: 'name',
    },
    // {
    //     title: '是否标准服务',
    //     dataIndex: 'is_standard_service',
    //     key: 'is_standard_service',
    // },
    {
        title: '适用范围',
        dataIndex: 'scope_name',
        key: 'scope_name',
    },
    {
        title: '服务类型',
        dataIndex: 'type_name',
        key: 'type_name',
    },
    // {
    //     title: '计价公式',
    //     dataIndex: 'valuation_formula',
    //     key: 'valuation_formula',
    // }, 
    // {
    //     title: '服务介绍页面',
    //     dataIndex: 'service_project_website',
    //     key: 'service_project_website',
    // }, {
    //     title: '合同条款',
    //     dataIndex: 'contract_clauses',
    //     key: 'contract_clauses',
    // }, 
    {
        title: '所属组织机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    },
    ];
    constructor(props: ServiceProjectViewControl) {
        super(props);
        this.state = {
            service_type_list: [],
            application_scope: [],
            request_url: '',
            org_list: []
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 新增按钮 */
    addServiceProject = () => {
        this.props.history!.push(ROUTE_PATH.changeServiceProject);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeServiceProject + '/' + contents.id);
        }
    }

    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        /** 获取服务类别 */
        request(this, AppServiceUtility.service_operation_service.get_service_type_list_tree!({}))
            .then((datas: any) => {
                if (datas) {
                    this.setState({
                        service_type_list: datas.result
                    });
                }
            });
        request(this, AppServiceUtility.service_scope_service.get_service_scope_list_all!({}))
            .then((datas: any) => {
                this.setState({
                    application_scope: datas.result,
                });
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let item_type_list: any[] = [];
        // this.state.item_type_list!.map((item, idx) => {
        //     item_type_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });
        let application_scope_list: any[] = [];
        this.state.application_scope!.map((item, idx) => {
            application_scope_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let service_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务细项名称",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入服务细项名称",
                    }
                },
                {
                    type: InputType.cascader,
                    label: "服务类型",
                    decorator_id: "item_type_id",
                    option: {
                        options: this.state.service_type_list,
                        placeholder: "请选择服务类型",
                        fieldNames: { label: 'label', value: 'id', children: 'children' },
                    }
                },
                {
                    type: InputType.select,
                    label: "适用范围",
                    decorator_id: "application_scope",
                    option: {
                        childrens: application_scope_list,
                        placeholder: "请选择适用范围",
                    }
                },
                // {
                //     type: InputType.radioGroup,
                //     label: "是否符合标准服务",
                //     decorator_id: "is_standard_service",
                //     option: {
                //         // placeholder: "请选择显示状态",
                //         options: [

                //             {
                //                 label: '是',
                //                 value: '是'
                //             }, {
                //                 label: '否',
                //                 value: '否'
                //             }
                //         ],
                //     }
                // },
                // {
                //     type: InputType.input,
                //     label: "所属组织机构",
                //     decorator_id: "organization_name"
                // },
                {
                    type: InputType.tree_select,
                    label: "所属组织机构",
                    col_span: 8,
                    decorator_id: "organization_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addServiceProject,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.services_project_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_services_project'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_project_list = Object.assign(service_project, table_param);
        return (
            <SignFrameLayout {...service_project_list} />
        );
    }
}

/**
 * 控件：服务项目视图控制器
 * @description 服务项目视图
 * @author
 */
@addon('ServiceProjectView', '服务项目视图', '服务项目视图')
@reactControl(ServiceProjectView, true)
export class ServiceProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}