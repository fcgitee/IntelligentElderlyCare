import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Descriptions, Button, Row, Radio } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from 'src/projects/app/appService';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import TextArea from "antd/lib/input/TextArea";

/**
 * 组件：服务人员审核页面
 */
export interface ServicePersonReviewedViewState extends ReactViewState {
    data?: any;
    reason?:any;
    result?:any;
}

/**
 * 组件：服务人员审核页面
 * 描述
 */
export class ServicePersonReviewedView extends ReactView<ServicePersonReviewedViewControl, ServicePersonReviewedViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            result:'',
            reason:'',
        };
    }
    componentDidMount = () => {
        let id = this.props.match!.params.key;
        AppServiceUtility.service_operation_service.get_service_personal_list!({ id })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        data: data.result[0]
                    });
                }
            });
    }
    // 提交
    handleSubmit = () => {
        let id = this.props.match!.params.key;
        const { result, reason } = this.state;
        var params = {
            id,
            subsidy_res: result,
            opinion: reason
        };
        // console.log(params);
        AppServiceUtility.service_operation_service.subsidy_service_personal_apply!(params)!
            .then((data: any) => {
                if (data) {
                    this.props.history!.push(ROUTE_PATH.serviceProvider);
                }
            });
    }

    // 审核结果
    resultChange = (e:any) => {
        this.setState({
            result: e.target.value 
        });
    }
    
    // 原因
    reasonChange = (e:any) => {
        this.setState({
            reason: e.target.value
        });
    }
    render() {
        const { data } = this.state;
        return (
            <div>
                <MainContent>
                    <MainCard title='服务人员申请审核'>
                        <Descriptions
                            title="申请信息"
                            bordered={true}
                            column={3}
                        >
                            <Descriptions.Item label="姓名">{data.servicer_name}</Descriptions.Item>
                            <Descriptions.Item label="性别">{data.servicer_sex}</Descriptions.Item>
                            <Descriptions.Item label="身份证">{data.servicer_id_card}</Descriptions.Item>
                            <Descriptions.Item label="手机号码">{data.servicer_phone}</Descriptions.Item>
                            <Descriptions.Item label="邮箱">{data.servicer_email}</Descriptions.Item>
                            <Descriptions.Item label="所属组织机构">{data.servicer_organization_id}</Descriptions.Item>
                            <Descriptions.Item label="服务人员能力级别">{data.servicer_ability_level}</Descriptions.Item>
                            <Descriptions.Item label="服务人员信用级别">{data.servicer_credit_level}</Descriptions.Item>
                            <Descriptions.Item label="资质(资格证)照片"><img src={data.servicer_qualifications_photos ? data.servicer_qualifications_photos : ''} /></Descriptions.Item>
                            <Descriptions.Item label="合同照片"><img src={data.servicer_contract_photos ? data.servicer_contract_photos : ''} /></Descriptions.Item>
                            <Descriptions.Item label="服务机构合同条款">{data.servicer_contract_clauses}</Descriptions.Item>
                            <Descriptions.Item label="备注">{data.servicer_remark}</Descriptions.Item>
                        </Descriptions>
                        <br />
                        <br />
                        <Row style={{ fontWeight: 'bold', fontSize:'17px' }}>审核结果<span>(请选择审核结果)</span></Row>
                        <br />
                        <Radio.Group onChange={this.resultChange}>
                            <Radio value={'通过'}>通过</Radio>
                            <Radio value={'不通过'}>不通过</Radio>
                        </Radio.Group>
                        <br />
                        <br />
                        <br />
                        <Row style={{ fontWeight: 'bold', fontSize:'17px' }}>填写原因<span>(请填写原因)</span></Row>
                        <br />
                        <TextArea rows={4} onChange={this.reasonChange}/>
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button type="primary" onClick={this.handleSubmit}>提交</Button>
                        </Row>
                    </MainCard>
                </MainContent>
            </div >
        );
    }
}

/**
 * 控件：服务人员审核页面
 * 描述
 */
@addon(' ServicePersonReviewedView', '服务人员审核页面', '提示')
@reactControl(ServicePersonReviewedView, true)
export class ServicePersonReviewedViewControl extends ReactViewControl {
}