import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from 'src/projects/remote';
// import { Select } from 'antd';

// let { Option } = Select;
/**
 * 组件：服务人员登记列表状态
 */
export interface ServicePersonalViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;

}

/**
 * 组件：服务人员登记列表
 * 描述
 */
export class ServicePersonalView extends ReactView<ServicePersonalViewControl, ServicePersonalViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'servicer_name',
        key: 'servicer_name',
    }, {
        title: '注册时间',
        dataIndex: 'register_date',
        key: 'register_date',
    }, {
        title: '性别',
        dataIndex: 'servicer_sex',
        key: 'servicer_sex',
    }, {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    }, {
        title: '住址',
        dataIndex: 'servicer_address',
        key: 'servicer_address',
    }, {
        title: '合同照片',
        dataIndex: 'servicer_contract_photos',
        key: 'servicer_contract_photos',
        render: (text: string, record: any, index: any) => {
            return (
                <img style={{ height: '40px', width: '40px', marginRight: '10px', }} src={remote.upload_url + text} />
            );
        }
    }, {
        title: '资质（资格证）照片',
        dataIndex: 'servicer_qualifications_photos',
        key: 'servicer_qualifications_photos',
        render: (text: string, record: any, index: any) => {
            return (
                <img style={{ height: '40px', width: '40px', marginRight: '10px', }} src={remote.upload_url + text} />
            );
        }
    }, {
        title: '备注',
        dataIndex: 'servicer_remark',
        key: 'servicer_remark',
    }];
    constructor(props: ServicePersonalViewControl) {
        super(props);
        this.state = {
            request_url:'',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeServicePersonal);
    }
    /** 审批 */
    check = () => {
        alert('审批');
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.ServicePersonApplyReviewed + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "姓名",
                decorator_id: "servicer_name",
                option: {
                    placeholder: "请选择人员名称",
                    // childrens: user_list,
                },
            },{
                type: InputType.input,
                label: "身份证",
                decorator_id: "servicer_id_card",
                option: {
                    placeholder: "请输入身份证",
                    // childrens: user_list,
                },
            },{
                type: InputType.rangePicker,
                label: "登记时间",
                decorator_id: "date_range",
                option: {
                    placeholder: "请输入登记时间",
                    // childrens: user_list,
                },
            },{
                type: InputType.radioGroup,
                label: "性别",
                decorator_id: "servicer_sex",
                option: {
                    placeholder: "请选择性别",
                    options: [
                        {
                            label:'男',
                            value:'男'
                        },{
                            label:'女',
                            value:'女'
                        }
                    ],
                },
            },],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '审核',
                btn_method: this.check,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_operation_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                // delete: {
                //     service_func: 'del_service_personal'
                // }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 服务人员登记列表页面
 */
@addon('ServicePersonalView', '服务人员登记列表页面', '描述')
@reactControl(ServicePersonalView, true)
export class ServicePersonalViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 