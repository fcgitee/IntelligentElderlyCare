import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info, beforeUpload } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { remote } from 'src/projects/remote';
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

let { Option } = Select;

/**
 * 组件：服务人员登记编辑页面状态
 */
export interface ChangeServicePersonalViewState extends ReactViewState {
    /** 服务人员列表 */
    personal_list?: [];
}

/**
 * 组件：服务人员登记编辑页面
 * 描述
 */
export class ChangeServicePersonalView extends ReactView<ChangeServicePersonalViewControl, ChangeServicePersonalViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            personal_list: []
        };
    }
    // /** 权限服务 */
    // permissionService?() {
    //     return getObject(this.props.permissionService_Fac!);
    // }
    componentDidMount() {
        request(this, AppServiceUtility.personnel_service['get_personnel_list']!({}, 1, 999))
            .then((datas: any) => {
                // console.log('datas.result', datas.result);
                this.setState({
                    personal_list: datas.result,
                });
            });
    }
    render() {
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "人员名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_personnel_list',
            title: '人员查询',
            select_option: {
                placeholder: "请选择人员",
            }
        };
        let personal = this.state.personal_list;
        let personal_list: any[] = [];
        personal!.map((item, idx) => {
            personal_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props = {
            form_items_props: [
                {
                    title: "服务人员登记信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "服务人员",
                            decorator_id: "service_personal_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择服务人员" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                // placeholder: "请选择服务人员",
                                modal_search_items_props:modal_search_items_props

                            }
                        },
                        {
                            type: InputType.upload,
                            label: "资质（资格证）照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "qualifications_photos",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传资质（资格证）照片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                beforeUpload: beforeUpload
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "合同照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "contract_photos",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传合同照片款" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                beforeUpload: beforeUpload
                            }
                        }, {
                            type: InputType.input_number,
                            label: "分成占有百分比",
                            decorator_id: "share_percentage",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入分成占有百分比" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入分成占有百分比"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.servicePersonalManage);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.service_operation_service,
                operation_option: {
                    query: {
                        func_name: "get_service_personal_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_service_personal"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.servicePersonalManage); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：服务人员登记编辑页面
 * 描述
 */
@addon('ChangeServicePersonalView', '服务人员登记编辑页面', '描述')
@reactControl(ChangeServicePersonalView, true)
export class ChangeServicePersonalViewControl extends ReactViewControl {
}