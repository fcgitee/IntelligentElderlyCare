import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：监控设备列表状态
 */
export interface ChargeTypeViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：监控设备列表
 * 描述
 */
export class ChargeTypeView extends ReactView<ChargeTypeViewControl, ChargeTypeViewState> {
    private columns_data_source = [{
        title: '类型名称',
        dataIndex: 'type_name',
        key: 'type_name',
    }, {
        title: '编号',
        dataIndex: 'no',
        key: 'no',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: ChargeTypeViewControl) {
        super(props);
        this.state = {
            request_url: '',
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.ChangeChargeType + '/' + contents.id);
        }
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.ChangeChargeType);
    }
    render() {
        let service_item = {
            type_show: false,
            btn_props: [{
                label: '新增收费类型',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                delete: {
                    service_func: 'delete_charge_list'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 监控设备列表页面
 */
@addon('ChargeTypeView', '监控设备列表页面', '描述')
@reactControl(ChargeTypeView, true)
export class ChargeTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 