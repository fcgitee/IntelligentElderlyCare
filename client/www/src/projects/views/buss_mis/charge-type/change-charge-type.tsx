import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：收费类型状态
 */
export interface ChangeChargeTypeViewState extends ReactViewState {
}

/**
 * 组件：收费类型视图
 */
export class ChangeChargeTypeView extends ReactView<ChangeChargeTypeViewControl, ChangeChargeTypeViewState> {
    constructor(props: any) {
        super(props);
    }
    render() {
        console.info(this.props.match!.params.key);
        let edit_props = {
            form_items_props: [{
                title: "收费类型",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "类型名称",
                        decorator_id: "type_name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入类型名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入类型名称"
                        }
                    },
                    {
                        type: InputType.antd_input,
                        label: "编号",
                        decorator_id: "no",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入编号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入编号"
                        }
                    },
                    {
                        type: InputType.text_area,
                        label: "备注",
                        decorator_id: "remark",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入备注" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入备注"
                        }
                    }
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    this.props.history!.push(ROUTE_PATH.ChargeType);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    query: {
                        func_name: "get_charge_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_charge_list"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.ChargeType); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：收费类型编辑控件
 * @description 收费类型编辑控件
 * @author
 */
@addon('ChangeChargeTypeView', '收费类型编辑控件', '收费类型编辑控件')
@reactControl(ChangeChargeTypeView, true)
export class ChangeChargeTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}