
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";

/** 状态：长者能力评估视图 */
export interface olderAbilityAssessmentViewState extends ReactViewState {
}
/** 组件：长者能力评估视图 */
export class olderAbilityAssessmentView extends React.Component<olderAbilityAssessmentViewControl, olderAbilityAssessmentViewState> {

    render() {
        return (
            <div style={{textAlign: 'center',marginTop: '20%'}}>
               <h2>功能待开发</h2> 
            </div>
        );
    }
}

/**
 * 控件：长者能力评估视图控制器
 * @description 长者能力评估视图
 * @author
 */
@addon(' olderAbilityAssessmentView', '长者能力评估视图', '长者能力评估视图')
@reactControl(olderAbilityAssessmentView, true)
export class olderAbilityAssessmentViewControl extends ReactViewControl {
}