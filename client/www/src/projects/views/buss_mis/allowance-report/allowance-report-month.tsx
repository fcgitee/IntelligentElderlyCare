import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { Button, Table, DatePicker } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";

// const { Option } = Select;
const { RangePicker } = DatePicker;
/**
 * 组件：成功页面
 */
export interface AllowanceReportMonthViewState extends ReactViewState {
    mode?: any;
    record_data?: any;
}

/**
 * 组件：成功页面
 * 描述
 */
export class AllowanceReportMonthView extends ReactView<AllowanceReportMonthViewControl, AllowanceReportMonthViewState> {
    columns: any = [
        {
            title: '镇街',
            dataIndex: 'zhenjie',
            key: 'zhenjie',
            width: 150,
        },
        {
            title: '社区',
            dataIndex: 'shequ',
            key: 'shequ',
            width: 150,
        },
        {
            title: '月份',
            dataIndex: 'date',
            key: 'date',
            width: 100,
        },
        {
            title: '服务对象人数（人）',
            dataIndex: 'num',
            key: 'num',
            width: 100,
        },
        {
            title: '月服务总费用（元）',
            dataIndex: 'count',
            key: 'count',
            width: 100,
        },
        {
            title: 'A类购买人数',
            dataIndex: 'AbuyPeople',
            key: 'AbuyPeople',
            width: 100,
        },
        {
            title: 'A类服务次数',
            dataIndex: 'AbuyNum',
            key: 'AbuyNum',
            width: 100,
        },
        {
            title: 'A类服务总金额',
            dataIndex: 'Abuycount',
            key: 'Abuycount',
            width: 100,
        },
        {
            title: 'B类购买人数',
            dataIndex: 'BbuyPeople',
            key: 'BbuyPeople',
            width: 100,
        },
        {
            title: 'B类服务次数',
            dataIndex: 'BbuyNum',
            key: 'BbuyNum',
            width: 100,
        },
        {
            title: 'B类服务总金额',
            dataIndex: 'Bbuycount',
            key: 'Bbuycount',
            width: 100,
        },
        {
            title: 'C类购买人数',
            dataIndex: 'CbuyPeople',
            key: 'CbuyPeople',
            width: 100,
        },
        {
            title: 'C类服务次数',
            dataIndex: 'CbuyNum',
            key: 'CbuyNum',
            width: 100,
        },
        {
            title: 'C类服务总金额',
            dataIndex: 'Cbuycount',
            key: 'Cbuycount',
            width: 100,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            mode: ['month', 'month'],
            record_data: []
        };
    }
    componentDidMount = () => {
        AppServiceUtility.service_record_service!.get_servicer_report_month!({})!
            .then((data: any) => {
                // console.log(data);
                if (data.length > 0) {
                    let record_data: any = [];
                    data.map((item: any) => {
                        if (item['res'].length > 0) {
                            item['res'].map((items: any) => {
                                record_data.push(
                                    {
                                        key: (new Date()).getTime(),
                                        shequ: items.shequ,
                                        zhenjie: items.zhenjie,
                                        num: items.num,
                                        count: items.count,
                                        date: items.date,
                                    }
                                );
                                this.setState({
                                    record_data
                                });
                            });
                        }
                    });
                }
            });
    }
    render() {
        return (
            <MainContent>
                <MainCard title='居家养老服务对象月报表' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                        <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                        <div style={{ float: 'right', marginRight: '3px' }}>
                            <RangePicker style={{ width: '200px' }} placeholder={['开始时间', '结束时间']} />
                        </div>
                        {/* <div style={{ float: 'right', marginRight: '140px' }}>
                            <Select defaultValue="lucy" style={{ width: 120 }}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                            </Select>
                            <Select defaultValue="lucy" style={{ width: 120 }}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                            </Select>
                        </div> */}
                    </div>
                    <Table columns={this.columns} dataSource={this.state.record_data} pagination={{ pageSize: 10 }} bordered={true} />
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：成功页面
 * 描述
 */
@addon(' AllowanceReportMonthView', '成功页面', '提示')
@reactControl(AllowanceReportMonthView, true)
export class AllowanceReportMonthViewControl extends ReactViewControl {
}