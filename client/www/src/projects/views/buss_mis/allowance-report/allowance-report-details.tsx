import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { Button, Table, DatePicker } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
// const { Option } = Select;
const { RangePicker } = DatePicker;
/**
 * 组件：成功页面
 */
export interface AllowanceReportDetailsViewState extends ReactViewState {
    mode?: any;
}

/**
 * 组件：成功页面
 * 描述
 */
export class AllowanceReportDetailsView extends ReactView<AllowanceReportDetailsViewControl, AllowanceReportDetailsViewState> {
    columns: any = [
        {
            title: '月份',
            dataIndex: 'phoneNum',
            key: 'phoneNum',
            width: 100,
        },
        {
            title: '镇街',
            dataIndex: 'shequ',
            key: 'shequ',
            width: 150,
        },
        {
            title: '社区',
            dataIndex: 'name',
            key: 'name',
            width: 150,
        },
        {
            title: 'A1(/人)',
            dataIndex: 'time',
            key: 'time',
            width: 100,
        },
        {
            title: 'A2(/人)',
            dataIndex: 'pickphoneNum',
            key: 'pickphoneNum',
            width: 100,
        },
        {
            title: 'A3(/人)',
            dataIndex: 'laiyuan',
            key: 'laiyuan',
            width: 100,
        },
        {
            title: 'A4(/人)',
            dataIndex: 'clTime',
            key: 'clTime',
            width: 100,
        },
        {
            title: 'A5(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'A6(/人)',
            dataIndex: 'laiyuan',
            key: 'laiyuan',
            width: 100,
        },
        {
            title: 'A7(/人)',
            dataIndex: 'clTime',
            key: 'clTime',
            width: 100,
        },
        {
            title: 'A类合计',
            dataIndex: 'laiyuan',
            key: 'laiyuan',
            width: 100,
        },
        {
            title: 'B8(/人)',
            dataIndex: 'clTime',
            key: 'clTime',
            width: 100,
        },
        {
            title: 'B9(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'B10(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'B11(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'B类合计',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'C12(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'C13(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'C14(/人)',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
        {
            title: 'C类总计',
            dataIndex: 'clState',
            key: 'clState',
            width: 100,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            mode: ['month', 'month'],
        };
    }
    componentDidMount = () => {
        AppServiceUtility.service_record_service!.get_servicer_report_month!({})!
            .then((data: any) => {
                // console.log(data);
            });
    }
    render() {
        const dataSource = [
            {
                key: '1',
                name: '胡彦斌',
                age: 32,
                address: '西湖区湖底公园1号',
            },
            {
                key: '2',
                name: '胡彦祖',
                age: 42,
                address: '西湖区湖底公园1号',
            },
        ];
        return (
            <MainContent>
                <MainCard title='居家养老服务对象明细表' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                        <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                        <div style={{ float: 'right', marginRight: '3px' }}>
                            <RangePicker style={{ width: '200px' }} placeholder={['开始时间', '结束时间']} />
                        </div>
                        {/* <div style={{ float: 'right', marginRight: '140px' }}>
                            <Select defaultValue="lucy" style={{ width: 120 }}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                            </Select>
                            <Select defaultValue="lucy" style={{ width: 120 }}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                            </Select>
                        </div> */}
                    </div>
                    <Table columns={this.columns} dataSource={dataSource} pagination={{ pageSize: 10 }} bordered={true} />
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：成功页面
 * 描述
 */
@addon(' AllowanceReportDetailsView', '成功页面', '提示')
@reactControl(AllowanceReportDetailsView, true)
export class AllowanceReportDetailsViewControl extends ReactViewControl {
}