import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { Button, Table, TreeSelect } from 'antd';
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：成功页面
 */
export interface WorkPeopleStatisticsViewState extends ReactViewState {
    mode?: any;
    record_data?: any;
    administrative_division_list?: any;
    org_list?: any;
    zhenjie?: any;
    shequ?: any;
}

/**
 * 组件：成功页面
 * 描述
 */
export class WorkPeopleStatisticsView extends ReactView<WorkPeopleStatisticsViewControl, WorkPeopleStatisticsViewState> {
    columns: any = [
        {
            title: '镇街',
            dataIndex: 'zhenjie',
            key: 'zhenjie',
            width: 150,
        },
        {
            title: '社区',
            dataIndex: 'shequ',
            key: 'shequ',
            width: 150,
        },
        {
            title: '幸福院名称',
            dataIndex: 'xfy_name',
            key: 'xfy_name',
            width: 100,
        },
        {
            title: '工作人员总数',
            dataIndex: 'work_people_all',
            key: 'work_people_all',
            width: 100,
        },
        {
            title: '女性人数',
            dataIndex: 'women_num',
            key: 'women_num',
            width: 100,
        },
        {
            title: '男性人数',
            dataIndex: 'man_num',
            key: 'man_num',
            width: 100,
        },
        {
            title: '管理人员',
            dataIndex: 'guanli_num',
            key: 'guanli_num',
            width: 100,
        },
        {
            title: '专业技能人员',
            dataIndex: 'zhuanye_num',
            key: 'zhuanye_num',
            width: 100,
        },
        {
            title: '社工人员',
            dataIndex: 'shegong_num',
            key: 'shegong_num',
            width: 100,
        },
        {
            title: '后勤人员',
            dataIndex: 'houqin_num',
            key: 'houqin_num',
            width: 100,
        },
        {
            title: '义工服务队数',
            dataIndex: 'yigongdui_num',
            key: 'yigongdui_num',
            width: 100,
        },
        {
            title: '义工人数',
            dataIndex: 'yigong_num',
            key: 'yigong_num',
            width: 100,
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            mode: ['month', 'month'],
            record_data: [],
            administrative_division_list: [],
            org_list: [],
            zhenjie: '',
            shequ: ''
        };
    }
    componentDidMount = () => {
        this.getList();
        // 行政区划
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 组织机构
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type: '社区' })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    getList() {
        let { zhenjie, shequ } = this.state;
        let param: any = {};
        if (zhenjie !== '') {
            param['admin_area_id'] = zhenjie;
        }
        if (shequ !== '') {
            param['organization_id'] = shequ;
        }
        // 查询列表数据
        AppServiceUtility.service_operation_service!.get_work_people_statistics!(param)!
            .then((data: any) => {
                if (data.length > 0) {
                    let record_data: any = [];
                    data.map((item: any) => {
                        record_data.push(
                            {
                                key: (new Date()).getTime(),
                                shequ: item.shequ,
                                zhenjie: item.zhenjie,
                                xfy_name: item.xfy_name,
                                work_people_all: item.work_people_all,
                                women_num: item.women_num,
                                man_num: item.man_num,
                                guanli_num: item.guanli_num,
                                zhuanye_num: item.zhuanye_num,
                                shegong_num: item.shegong_num,
                                houqin_num: item.houqin_num,
                                yigongdui_num: item.yigongdui_num,
                                yigong_num: item.yigong_num
                            }
                        );
                        this.setState({
                            record_data
                        });
                    });
                } else {
                    this.setState({
                        record_data: [],
                    });
                }
            });
    }
    zjChange = (value: any) => {
        this.setState({
            zhenjie: value
        });
    }

    sqChange = (value: any) => {
        this.setState({
            shequ: value
        });
    }

    search = () => {
        this.getList();
    }

    render() {
        return (
            <MainContent>
                <MainCard title='工作人员统计' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button type="primary" style={{ float: 'right', marginRight: '5px' }}>导出</Button>
                        <Button type="primary" style={{ float: 'right', marginRight: '5px' }} onClick={this.search}>查询</Button>
                        <div style={{ float: 'right', marginRight: '280px' }}>
                            <TreeSelect
                                style={{ width: 140, fontSize: '10px' }}
                                placeholder="请选择镇（街道）"
                                showSearch={true}
                                treeNodeFilterProp='title'
                                allowClear={true}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                treeDefaultExpandAll={true}
                                treeData={this.state.administrative_division_list}
                                onChange={this.zjChange}
                            />
                            &nbsp;&nbsp;
                            <TreeSelect
                                style={{ width: 140, fontSize: '10px' }}
                                placeholder="请选择社区"
                                showSearch={true}
                                treeNodeFilterProp='title'
                                allowClear={true}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                treeDefaultExpandAll={true}
                                treeData={this.state.org_list}
                                onChange={this.sqChange}
                            />
                        </div>
                    </div>
                    <Table columns={this.columns} dataSource={this.state.record_data} pagination={{ pageSize: 10 }} bordered={true} />
                </MainCard>
            </MainContent>
        );
    }
}

/**
 * 控件：成功页面
 * 描述
 */
@addon(' WorkPeopleStatisticsView', '成功页面', '提示')
@reactControl(WorkPeopleStatisticsView, true)
export class WorkPeopleStatisticsViewControl extends ReactViewControl {
}