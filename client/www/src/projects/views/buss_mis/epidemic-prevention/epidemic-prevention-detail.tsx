import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Descriptions, Card, Table, Button, message, Modal } from 'antd';
import { getAge, getBirthday } from "src/projects/app/util-tool";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { Toast } from "antd-mobile";
/**
 * 组件：出入登记详情状态
 */
export interface EpidemicPreventionDetailViewState extends ReactViewState {
    is_edit_btn?: any;
    list?: any;
    day_list?: any;
    visible?: any;
    isShow?: boolean; // 是否显示图片预览
    cur_img?: string; // 当前预览图片
    headlth_user_id?: string; // 当前记录的用户id
}

/**
 * 组件：出入登记详情视图
 */
export class EpidemicPreventionDetailView extends ReactView<EpidemicPreventionDetailViewControl, EpidemicPreventionDetailViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            is_edit_btn: false,
            list: {},
            day_list: [],
            visible: false,
            isShow: false,
            cur_img: '',
            headlth_user_id: '',
        };
    }

    componentWillMount = () => {
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        }
                    });
                }
            });
        AppServiceUtility.friends_circle_service.get_epidemic_prevention_all!({ id: this.props.match!.params.key }, 1, 1)!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        list: data.result[0],
                        headlth_user_id: data.result[0].user_id
                    });
                    this.get_day_epidemic(data.result[0].user_id);
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    get_day_epidemic = (user_id: any) => {
        AppServiceUtility.friends_circle_service.get_day_epidemic_prevention_all!({ user_id: user_id }, 1, 10)!
            .then((datas: any) => {
                if (datas.result.length > 0) {
                    this.setState({
                        day_list: datas.result
                    });
                }
            })
            .catch((err) => {
                console.info(err);
            });
    }
    panding = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (type: any) => {
        let status = '';
        if (type === '高风险') {
            status = '高风险';
        } else if (type === '中风险') {
            status = '中风险';
        } else {
            status = '低风险';
        }
        AppServiceUtility.friends_circle_service.decide_epidemic_prevention!({ id: this.props.match!.params.key, status })!
            .then((data: any) => {
                if (data === 'Success') {
                    message.success('判定成功');
                    this.get_day_epidemic(this.state.headlth_user_id);
                } else {
                    Toast.fail(data, 3);
                }
                this.setState({
                    visible: false
                });
            })
            .catch((err) => {
                Toast.fail(err.message, 3);
                this.setState({
                    visible: false
                });
                console.info(err);
            });
    }

    handleCancel = () => {
        this.setState({
            visible: false
        });
    }
    set_img_status = (isShow: boolean = true) => {
        this.setState({ isShow: isShow });
    }
    clickImg = (url: string) => {
        this.setState({ cur_img: url });
        this.set_img_status();
    }
    imgPreview = () => {
        return (
            <Modal visible={true} style={{ width: "700px", height: '500px' }} footer={null} onCancel={() => this.set_img_status(false)} centered={true}>
                <div style={{ maxHeight: '50vh', overflow: 'auto' }}>
                    <img src={this.state.cur_img} style={{ width: '100%' }} alt="" />
                </div>
            </Modal>
        );
    }
    render() {
        const { list } = this.state;
        const columns = [
            {
                title: '体温',
                dataIndex: 'temperature',
                key: 'temperature',
            },
            {
                title: '出现症状',
                dataIndex: 'symptom',
                key: 'symptom',
            }, {
                title: '外出期间行程',
                dataIndex: 'trip',
                key: 'trip',
            }, {
                title: '是否逗留或途径疫情重灾区',
                dataIndex: 'stay_disaster_area',
                key: 'stay_disaster_area',
            }, {
                title: '是否接触过疫情重灾区人员',
                dataIndex: 'contact_disaster_area_people',
                key: 'contact_disaster_area_people',
            }, {
                title: '上下班交通方式',
                dataIndex: 'transportation',
                key: 'transportation',
            }, {
                title: '近期是否出佛山市（14天内）',
                dataIndex: 'is_out',
                key: 'is_out',
            }, {
                title: '当天是否提供服务',
                dataIndex: 'is_work',
                key: 'is_work',
            }, {
                title: '健康码截图',
                dataIndex: 'photo',
                key: 'photo',
                render: (text: any, record: any) => (
                    < img style={{ width: '70px', height: '70px' }} src={record.photo ? record.photo[0] : ''} alt="健康码" onClick={() => { this.clickImg(record.photo ? record.photo[0] : ''); }} />
                )
            }, {
                title: '健康风险判定',
                dataIndex: 'status',
                key: 'status',
            }, {
                title: '上报日期',
                dataIndex: 'create_time',
                key: 'create_time',
                render: (text: any, record: any) => {
                    if (record.create_time) {
                        return record.create_time;
                    } else {
                        return record.modify_date;
                    }
                }
            },];
        const columns2 = [
            {
                title: '家庭成员姓名',
                dataIndex: 'family_name',
                key: 'family_name',
            },
            {
                title: '关系',
                dataIndex: 'relationship',
                key: 'relationship',
            },
            {
                title: '手机号码',
                dataIndex: 'phone',
                key: 'phone',
            },
            {
                title: '出现症状',
                dataIndex: 'symptoms_present',
                key: 'symptoms_present',
            },
        ];
        const data: any = [];
        list && list.family_info ? list.family_info.map((item: any, index: any) => {
            data.push(
                {
                    key: index,
                    family_name: item.family_name,
                    relationship: item.relationship,
                    phone: item.phone,
                    symptoms_present: item.symptoms_present,
                },
            );
        }) : '';
        return (
            (
                <div>
                    {this.state.isShow && this.imgPreview()}
                    <MainContent>
                        <Card>
                            <Descriptions title="服务人员健康档案" layout="vertical" bordered={true}>
                                <Descriptions.Item label="姓名">{list && list.user_name ? list.user_name : ''}</Descriptions.Item>
                                {/* <Descriptions.Item label="性别">{getSex(list.id_card)}</Descriptions.Item> */}
                                <Descriptions.Item label="年龄">{list && list.id_card ? getAge(list.id_card) : ''}</Descriptions.Item>
                                <Descriptions.Item label="身份证号码">{list && list.id_card ? list.id_card : ''}</Descriptions.Item>
                                <Descriptions.Item label="出生日期">{list && list.id_card ? getBirthday(list.id_card) : ''}</Descriptions.Item>
                                <Descriptions.Item label="电话号码">{list && list.phone ? list.phone : ''}</Descriptions.Item>
                                <Descriptions.Item label="行政区划">{list && list.area_name ? list.area_name : ''}</Descriptions.Item>
                                <Descriptions.Item label="组织机构">{list && list.org_name ? list.org_name : ''}</Descriptions.Item>
                                <Descriptions.Item label="1月以来是否有工作地/常住地以外的旅行史">{list && list.day_info && list.day_info.trip_history ? list.day_info.trip_history : ''}</Descriptions.Item>
                                <Descriptions.Item label="复工后居住地">{list && list.day_info && list.day_info.backWork_address ? (list.day_info.backWork_address.province + list.day_info.backWork_address.city + list.day_info.backWork_address.area) : ''}</Descriptions.Item>
                                <Descriptions.Item label="复工后居住地（详细）">{list && list.day_info && list.day_info.backWork_address ? list.day_info.backWork_address.detail : ''}</Descriptions.Item>
                                <Descriptions.Item label="近期是否出佛山市">{list && list.day_info && list.day_info.is_out ? list.day_info.is_out : ''}</Descriptions.Item>
                                <Descriptions.Item label="出发日期">{list && list.day_info && list.day_info.begin_date ? list.day_info.begin_date : ''}</Descriptions.Item>
                                <Descriptions.Item label="出发交通工具">{list && list.day_info && list.day_info.backWork_transportation ? list.day_info.backWork_transportation : ''}</Descriptions.Item>
                                <Descriptions.Item label="出发交通工具备注">{list && list.day_info && list.day_info.vehicle_num}</Descriptions.Item>
                                <Descriptions.Item label="返工前出发地">{list && list.day_info && list.day_info.place_of_departure ? (list.day_info.place_of_departure.province + list.day_info.place_of_departure.city + list.day_info.place_of_departure.area) : ''}</Descriptions.Item>
                                <Descriptions.Item label="停留城市">{list && list.day_info && list.day_info.stay_city ? (list.day_info.stay_city.province + list.day_info.stay_city.city + list.day_info.stay_city.area) : ''}</Descriptions.Item>
                                <Descriptions.Item label="到达日期">{list && list.day_info && list.day_info.arrive_time ? list.day_info.arrive_time : ''}</Descriptions.Item>
                                <Descriptions.Item label="到达交通工具">{list && list.day_info && list.day_info.reach_transportation ? list.day_info.backWork_transportation : ''}</Descriptions.Item>
                                <Descriptions.Item label="到达交通工具备注">{list && list.day_info && list.day_info.reach_vehicle_num}</Descriptions.Item>
                                <Descriptions.Item label="是否有同行人">{list && list.day_info && list.day_info.has_partner ? list.day_info.has_partner : ''}</Descriptions.Item>
                                <Descriptions.Item label="同行人数量">{list && list.day_info && list.day_info.partner_num ? list.day_info.partner_num : ''}</Descriptions.Item>
                                <Descriptions.Item label="行程描述">{list && list.day_info && list.day_info.trip_description ? list.day_info.trip_description : ''}</Descriptions.Item>
                                <Descriptions.Item label="创建档案时间">{list && list.create_date ? list.create_date : ''}</Descriptions.Item>
                                <Descriptions.Item label="更新时间">{list.hasOwnProperty('create_time') ? list['create_time'] : (list && list.day_info && list.day_info.modify_date ? list.day_info.modify_date : '')}</Descriptions.Item>
                            </Descriptions>
                            <br />
                            <br />
                            <Table columns={columns2} dataSource={data} />
                            <br />
                            <br />
                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>

                                {
                                    this.state.is_edit_btn ?
                                        <Button type="danger" onClick={() => { this.panding(); }}>判定</Button>
                                        : ''
                                }
                                <Button onClick={() => { history.back(); }}>返回</Button>
                            </div>
                        </Card>
                        <br />
                        <br />
                        <Card title="每日健康信息记录">
                            <Table columns={columns} dataSource={this.state.day_list} />
                        </Card>
                        <Modal
                            title="请选择判定结果"
                            visible={this.state.visible}
                            onCancel={this.handleCancel}
                            footer={null}
                        >
                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <Button type='danger' onClick={() => { this.handleOk('高风险'); }}>高风险</Button>
                                <Button style={{ color: 'white', backgroundColor: 'orange' }} onClick={() => { this.handleOk('中风险'); }}>中风险</Button>
                                <Button style={{ color: 'white', backgroundColor: 'green' }} onClick={() => { this.handleOk('低风险'); }}>低风险</Button>
                            </div>
                        </Modal>
                    </MainContent>
                </div>
            )
        );
    }
}

/**
 * 控件：出入登记详情编辑控件
 * @description 出入登记详情编辑控件
 * @author
 */
@addon('EpidemicPreventionDetailView', '出入登记详情编辑控件', '出入登记详情编辑控件')
@reactControl(EpidemicPreventionDetailView, true)
export class EpidemicPreventionDetailViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 编辑权限 */
    public edit_permission?: string;

}