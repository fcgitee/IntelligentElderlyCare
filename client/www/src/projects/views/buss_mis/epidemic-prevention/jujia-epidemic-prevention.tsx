import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button, Modal, message, Select, Form } from "antd";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ROUTE_PATH } from "src/projects/router";
// import { table_param } from "src/projects/app/util-tool";
import { exprot_excel } from "src/business/util_tool";
import { UploadFile } from "src/business/components/buss-components/upload-file/index";
// import { getAge } from "src/projects/app/util-tool";
const Option = Select.Option;
/**
 * 组件：设备列表状态
 */
export interface JujiaEpidemicPreventionViewState extends ReactViewState {
    is_edit_btn?: any;
    is_delete_btn?: any;
    visible?: any;
    select_id?: any;
    columns_data_source?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    is_select_btn?: boolean;
    visible2?: any;
    btn_props?: any;
    isShow?: boolean;
    cur_img?: string;
    recode?: any; // 每日健康数据id
    config?: any;
    /* 组织机构 */
    org_list?:any;
}

/**
 * 组件：设备列表
 * 描述
 */
export class JujiaEpidemicPreventionView extends ReactView<JujiaEpidemicPreventionViewControl, JujiaEpidemicPreventionViewState> {
    private formCreator: any = null;
    constructor(props: JujiaEpidemicPreventionViewControl) {
        super(props);
        this.state = {
            is_edit_btn: false,
            is_delete_btn: false,
            visible: false,
            select_id: '',
            columns_data_source: [],
            select_type: 1,
            condition: {},
            is_select_btn: false,
            visible2: false,
            btn_props: [],
            isShow: false,
            cur_img: '',
            recode: [],
            config: {
                upload: false,
                func: AppServiceUtility.friends_circle_service.upload_day_info_excl,
                title: '导入每日防疫数据',
                isFormat: true,
            },
            org_list: []
        };
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
        let columns_data_source: any = [];
        if (this.props.type === '幸福院') {
            columns_data_source = [
                {
                    title: '镇街',
                    dataIndex: 'area_name',
                    key: 'area_name',
                    width: 150,
                    align: 'center'
                },
                {
                    title: '所属幸福院',
                    dataIndex: 'org_name',
                    key: 'org_name',
                    width: 150,
                    align: 'center'
                },
                {
                    title: '运营方',
                    dataIndex: 'org_name',
                    key: 'org_name',
                    align: 'center',
                    width: 150
                },
                {
                    title: '姓名',
                    dataIndex: 'user_name',
                    key: 'user_name',
                    align: 'center',
                    width: 150
                },
                {
                    title: '证件号码',
                    dataIndex: 'id_card',
                    key: 'id_card',
                    align: 'center',
                    width: 150
                },
                {
                    title: '手机号码',
                    dataIndex: 'phone',
                    key: 'phone',
                    align: 'center',
                    width: 150
                },
                {
                    title: '体温',
                    dataIndex: 'day_info.temperature',
                    key: 'day_info.temperature',
                    align: 'center',
                    width: 150
                },
                {
                    title: '出现症状',
                    dataIndex: 'day_info.symptom',
                    key: 'day_info.symptom',
                    align: 'center',
                    width: 150
                },
                {
                    title: '是否来自高风险地区',
                    dataIndex: 'day_info.stay_disaster_area',
                    key: 'day_info.stay_disaster_area',
                    align: 'center',
                    width: 150
                },
                {
                    title: '复工后居住地',
                    dataIndex: 'day_info.backWork_address',
                    key: 'day_info.backWork_address',
                    align: 'center',
                    width: 150
                },
                {
                    title: '上下班交通方式',
                    dataIndex: 'day_info.transportation',
                    key: 'day_info.transportation',
                    align: 'center',
                    width: 150
                },
                {
                    title: '近期是否出佛山市（14天内）',
                    dataIndex: 'day_info.is_out',
                    key: 'day_info.is_out',
                    align: 'center',
                    width: 150
                },
                {
                    title: '健康码截图',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => (
                        < img style={{ width: '70px', height: '70px' }} src={record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''} alt="健康码" onClick={() => { this.clickImg(record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''); }} />
                    )
                },
                {
                    title: '健康风险判定',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record['day_info'] && record['day_info']['status']) {
                            if (record['day_info']['status'] === '高风险') {
                                return <Button type='danger'>高风险</Button>;
                            } else if (record['day_info']['status'] === '中风险') {
                                return <Button style={{ color: 'white', backgroundColor: 'orange' }}>中风险</Button>;
                            } else if (record['day_info']['status'] === '低风险') {
                                return <Button style={{ color: 'white', backgroundColor: 'green' }}>低风险</Button>;
                            } else {
                                return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                            }
                        } else {
                            return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                        }
                    }
                },
                {
                    title: '状态',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record.hasOwnProperty('day_info') && record.day_info.hasOwnProperty('temperature')) {
                            if (parseFloat(record.day_info['temperature']) >= 37 || parseFloat(record.day_info['temperature']) <= 36) {
                                return <Button type='danger'>体温异常</Button>;
                            } else {
                                return '体温正常';
                            }
                        } else {
                            return '暂无体温数据';
                        }
                    }
                },
                {
                    title: '上报日期',
                    dataIndex: 'create_time',
                    key: 'create_time',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record.create_time) {
                            return record.create_time;
                        } else {
                            return record.modify_date;
                        }
                    }
                },
                {
                    title: '操作',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    ellipsis: true,
                    render: (text: any, record: any) => (
                        <div>
                            <div>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    {
                                        this.state.is_select_btn ?
                                            <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.goDetail(record.id); }}>查看</Button>
                                            : ''
                                    }
                                    {
                                        this.state.is_edit_btn ?
                                            <Button type="danger" onClick={() => { this.panding(record.id); }}>判定</Button>
                                            : ''
                                    }
                                </div>
                            </div>
                        </div>
                    ),
                }];
        } else if (this.props.type === '服务商') {
            columns_data_source = [
                {
                    title: '所属机构',
                    dataIndex: 'org_name',
                    key: 'org_name',
                    align: 'center',
                    width: 150,
                    // ellipsis: true,
                },
                {
                    title: '申报人信息',
                    dataIndex: '',
                    key: '',
                    width: 600,
                    align: 'center',
                    ellipsis: true,
                    children: [
                        {
                            title: '姓名',
                            dataIndex: 'user_name',
                            key: 'user_name',
                            align: 'center',
                            width: 150,
                            // ellipsis: true,
                        }, {
                            title: '证件号码',
                            dataIndex: 'id_card',
                            key: 'id_card',
                            align: 'center',
                            width: 150,
                            // ellipsis: true,
                        }, {
                            title: '手机号码',
                            dataIndex: 'phone',
                            key: 'phone',
                            align: 'center',
                            width: 150,
                            // ellipsis: true,
                        }, {
                            title: '近期住址',
                            dataIndex: 'day_info.backWork_address',
                            key: 'day_info.backWork_address',
                            align: 'center',
                            width: 150,
                            // ellipsis: true,
                        }
                    ]
                },
                {
                    title: '个人情况',
                    dataIndex: '',
                    key: '',
                    width: 750,
                    align: 'center',
                    ellipsis: true,
                    children: [
                        {
                            title: '体温',
                            dataIndex: 'day_info.temperature',
                            key: 'day_info.temperature',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        }, {
                            title: '出现症状',
                            dataIndex: 'day_info.symptom',
                            key: 'day_info.symptom',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        }, {
                            title: '是否逗留或途径疫情重灾区（湖北等地）',
                            dataIndex: 'day_info.stay_disaster_area',
                            key: 'day_info.stay_disaster_area',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        }, {
                            title: '是否接触过疫情重灾区人员（从湖北等地返回）',
                            dataIndex: 'day_info.contact_disaster_area_people',
                            key: 'day_info.contact_disaster_area_people',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        },
                        {
                            title: '一月以来是否有工作地/常住地以外的旅行史',
                            dataIndex: 'day_info.trip_history',
                            key: 'day_info.trip_history',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        }
                    ]
                },
                // {
                //     title: '出行情况',
                //     dataIndex: '',
                //     key: '',
                //     width: 460,
                //     ellipsis: true,
                //     children: [
                //         {
                //             title: '出发日期',
                //             dataIndex: 'day_info.begin_date',
                //             key: 'day_info.begin_date',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '出发交通工具',
                //             dataIndex: 'day_info.backWork_transportation',
                //             key: 'day_info.backWork_transportation',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '停留城市',
                //             dataIndex: 'day_info.stay_city',
                //             key: 'day_info.stay_city',
                //             width: 46,
                //             ellipsis: true,
                //         },
                //         {
                //             title: '是否已回到本市',
                //             dataIndex: 'day_info.is_back',
                //             key: 'day_info.is_back',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '到达日期',
                //             dataIndex: 'day_info.arrive_time',
                //             key: 'day_info.arrive_time',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '到达交通工具',
                //             dataIndex: 'day_info.reach_transportation',
                //             key: 'day_info.reach_transportation',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '预计到达日期',
                //             dataIndex: 'day_info.unarrive_time',
                //             key: 'day_info.unarrive_time',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '是否有同行人',
                //             dataIndex: 'day_info.has_partner',
                //             key: 'day_info.has_partner',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '同行人数',
                //             dataIndex: 'day_info.partner_num',
                //             key: 'day_info.partner_num',
                //             width: 46,
                //             ellipsis: true,
                //         }, {
                //             title: '行程描述',
                //             dataIndex: 'day_info.trip_description',
                //             key: 'day_info.trip_description',
                //             width: 46,
                //             ellipsis: true,
                //         }
                //     ]
                // },
                {
                    title: '健康情况',
                    dataIndex: '',
                    key: '',
                    width: 750,
                    align: 'center',
                    ellipsis: true,
                    children: [
                        {
                            title: '当天是否提供服务',
                            dataIndex: 'day_info.is_work',
                            key: 'day_info.is_work',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                        }, {
                            title: '健康码截图',
                            dataIndex: '',
                            key: '',
                            align: 'center',
                            width: 150,
                            ellipsis: true,
                            render: (text: any, record: any) => (
                                < img style={{ width: '70px', height: '70px' }} src={record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''} alt="健康码" onClick={() => { this.clickImg(record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''); }} />
                            )
                        },
                        {
                            title: '健康风险判定',
                            dataIndex: '',
                            key: '',
                            align: 'center',
                            width: 150,
                            render: (text: any, record: any) => {
                                if (record['day_info'] && record['day_info']['status']) {
                                    if (record['day_info']['status'] === '高风险') {
                                        return <Button type='danger'>高风险</Button>;
                                    } else if (record['day_info']['status'] === '中风险') {
                                        return <Button style={{ color: 'white', backgroundColor: 'orange' }}>中风险</Button>;
                                    } else if (record['day_info']['status'] === '低风险') {
                                        return <Button style={{ color: 'white', backgroundColor: 'green' }}>低风险</Button>;
                                    } else {
                                        return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                                    }
                                } else {
                                    return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                                }
                            }
                        },
                        {
                            title: '状态',
                            dataIndex: '',
                            key: '',
                            align: 'center',
                            width: 150,
                            render: (text: any, record: any) => {
                                if (record.hasOwnProperty('day_info') && record.day_info.hasOwnProperty('temperature')) {
                                    if (parseFloat(record.day_info['temperature']) >= 37 || parseFloat(record.day_info['temperature']) <= 36) {
                                        return <Button type='danger'>体温异常</Button>;
                                    } else {
                                        return '体温正常';
                                    }
                                } else {
                                    return '暂无体温数据';
                                }
                            }
                        }, {
                            title: '上报日期',
                            dataIndex: 'create_time',
                            key: 'create_time',
                            width: 150,
                            align: 'center',
                            ellipsis: true,
                            render: (text: any, record: any) => {
                                if (record.create_time) {
                                    return record.create_time;
                                } else {
                                    return record.modify_date;
                                }
                            }
                        }
                    ]
                },
                {
                    title: '操作',
                    dataIndex: '',
                    key: '',
                    width: 46,
                    align: 'center',
                    ellipsis: true,
                    render: (text: any, record: any) => (
                        <div>
                            <div>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    {
                                        this.state.is_select_btn ?
                                            <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.goDetail(record.id); }}>查看</Button>
                                            : ''
                                    }
                                    {
                                        this.state.is_edit_btn ?
                                            <Button type="danger" onClick={() => { this.panding(record.id); }}>判定</Button>
                                            : ''
                                    }
                                </div>
                            </div>
                        </div>
                    ),
                }];
        } else if (this.props.type === '福利院') {
            columns_data_source = [
                {
                    title: '镇街',
                    dataIndex: 'area_name',
                    key: 'area_name',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '所属机构',
                    dataIndex: 'org_name',
                    key: 'org_name',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '姓名',
                    dataIndex: 'user_name',
                    key: 'user_name',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '手机号码',
                    dataIndex: 'phone',
                    key: 'phone',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '体温',
                    dataIndex: 'day_info.temperature',
                    key: 'day_info.temperature',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '外出期间行程',
                    dataIndex: 'day_info.trip',
                    key: 'day_info.trip',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '是否来自高风险地区',
                    dataIndex: 'day_info.stay_disaster_area',
                    key: 'day_info.stay_disaster_area',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '复工后居住地',
                    dataIndex: 'day_info.backWork_address',
                    key: 'day_info.backWork_address',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '上下班交通方式',
                    dataIndex: 'day_info.transportation',
                    key: 'day_info.transportation',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '近期是否出佛山市（14天内）',
                    dataIndex: 'day_info.is_out',
                    key: 'day_info.is_out',
                    align: 'center',
                    width: 150,
                },
                {
                    title: '健康码截图',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => (
                        <img style={{ width: '70px', height: '70px' }} src={record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''} alt="健康码" onClick={() => { this.clickImg(record.day_info && record.day_info.photo ? record.day_info.photo[0] : ''); }} />
                    )
                },
                {
                    title: '健康风险判定',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record['day_info'] && record['day_info']['status']) {
                            if (record['day_info']['status'] === '高风险') {
                                return <Button type='danger'>高风险</Button>;
                            } else if (record['day_info']['status'] === '中风险') {
                                return <Button style={{ color: 'white', backgroundColor: 'orange' }}>中风险</Button>;
                            } else if (record['day_info']['status'] === '低风险') {
                                return <Button style={{ color: 'white', backgroundColor: 'green' }}>低风险</Button>;
                            } else {
                                return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                            }
                        } else {
                            return <Button style={{ color: 'white', backgroundColor: 'gray' }}>待判定</Button>;
                        }
                    }
                },
                {
                    title: '状态',
                    dataIndex: '',
                    key: '',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record.hasOwnProperty('day_info') && record.day_info.hasOwnProperty('temperature')) {
                            if (parseFloat(record.day_info['temperature']) > 37 || parseFloat(record.day_info['temperature']) < 36) {
                                return <Button type='danger'>体温异常</Button>;
                            } else {
                                return '体温正常';
                            }
                        } else {
                            return '暂无体温数据';
                        }
                    }
                }, {
                    title: '上报日期',
                    dataIndex: 'create_time',
                    key: 'create_time',
                    align: 'center',
                    width: 150,
                    render: (text: any, record: any) => {
                        if (record.create_time) {
                            return record.create_time;
                        } else {
                            return record.modify_date;
                        }
                    }
                },
                {
                    title: '操作',
                    dataIndex: '',
                    key: '',
                    width: 150,
                    align: 'center',
                    ellipsis: true,
                    render: (text: any, record: any) => (
                        <div>
                            <div>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    {
                                        this.state.is_select_btn ?
                                            <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.goDetail(record.id); }}>查看</Button>
                                            : ''
                                    }
                                    {
                                        this.state.is_edit_btn ?
                                            <Button type="danger" onClick={() => { this.panding(record.id); }}>判定</Button>
                                            : ''
                                    }
                                </div>
                            </div>
                        </div>
                    ),
                }];
        }
        this.setState({
            columns_data_source
        });
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.select_permission) {
                            this.setState({
                                is_select_btn: true,
                            });
                        } else if (value.function + value.permission === this.props.edit_permission) {
                            let btn_props: Array<any> = this.state.btn_props;
                            btn_props.push({
                                label: '一键判定',
                                btn_method: this.YiJianPanDing,
                                icon: 'plus'
                            });
                            this.setState({
                                is_edit_btn: true,
                                btn_props
                            });
                        } else if (value.function + value.permission === this.props.add_permission) {
                            let btn_props: Array<any> = this.state.btn_props;
                            btn_props.push(
                                {
                                    label: '导出',
                                    btn_method: this.download,
                                    icon: 'download'
                                },
                                {
                                    label: '导入',
                                    btn_method: this.upload,
                                    icon: 'plus'
                                },
                                {
                                    label: '下载模板',
                                    btn_method: this.downloadMoban,
                                    icon: 'download'
                                });
                            this.setState({ btn_props });
                        }
                    });
                }
            });
    }
    // 导入
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }

    goDetail = (id: any) => {
        this.props.history!.push(ROUTE_PATH.EpidemicPreventionDetail + '/' + id);
    }
    panding = (id: any) => {
        this.setState({
            visible: true,
            select_id: id
        });

    }
    handleOk = (type: any) => {
        let status = '';
        if (type === '高风险') {
            status = '高风险';
        } else if (type === '中风险') {
            status = '中风险';
        } else {
            status = '低风险';
        }
        AppServiceUtility.friends_circle_service.decide_epidemic_prevention!({ id: this.state.select_id, status })!
            .then((data: any) => {
                if (data === 'Success') {
                    message.success('判定成功', 2, () => {
                        // 刷新数据
                        let _this = this.formCreator;
                        _this.select_func([_this.state.condition, _this.state.page, _this.state.pageSize]);
                    });
                } else {
                    message.info('判定失败', 2);
                }
                this.setState({
                    visible: false
                });
            })
            .catch((err) => {
                message.info(err.message);
                this.setState({
                    visible: false
                });
            });
    }
    handleCancel = () => {
        this.setState({
            visible: false
        });
    }

    handleCancel2 = () => {
        this.setState({
            visible2: false
        });
    }
    // 下载
    download = () => {
        let _this = this.formCreator;
        console.log(_this.state.condition, 'shenwu');
        AppServiceUtility.friends_circle_service.get_epidemic_prevention_all!(_this.state.condition)!
            .then((data: any) => {
                console.log(data, 'shenwu');
                if (data.result.length) {
                    let new_data: any = [];
                    let file_name = '居家服务人员汇总表';
                    data.result.map((item: any) => {
                        if (this.props.type === '服务商') {
                            new_data.push({
                                "所属机构": item.org_name,
                                "姓名": item.user_name,
                                "证件号码": item.id_card,
                                "手机号码": item.phone,
                                "近期住址": item.day_info && item.day_info.backWork_address ? item.day_info.backWork_address : '',
                                "体温": item.day_info && item.day_info.temperature ? item.day_info.temperature : '',
                                "出现症状": item.day_info && item.day_info.symptom ? item.day_info.symptom : '',
                                "是否逗留或途径疫情重灾区（湖北等地）": item.day_info && item.day_info.stay_disaster_area ? item.day_info.stay_disaster_area : '',
                                "是否接触过疫情重灾区人员（从湖北等地返回）": item.day_info && item.day_info.contact_disaster_area_people ? item.day_info.contact_disaster_area_people : '',
                                "一月以来是否有工作地/常住地以外的旅行史": item.day_info && item.day_info.trip_history ? item.day_info.trip_history : '',
                                "出发日期": item.day_info && item.day_info.begin_date ? item.day_info.begin_date : '',
                                "出发交通工具": item.day_info && item.day_info.backWork_transportation ? item.day_info.backWork_transportation : '',
                                "停留城市": item.day_info && item.day_info.stay_city ? item.day_info.stay_city : '',
                                "是否已回到本市": item.day_info && item.day_info.is_back ? item.day_info.is_back : '',
                                "到达日期": item.day_info && item.day_info.arrive_time ? item.day_info.arrive_time : '',
                                "到达交通工具": item.day_info && item.day_info.reach_transportation ? item.day_info.reach_transportation : '',
                                "预计到达日期": item.day_info && item.day_info.unarrive_time ? item.day_info.unarrive_time : '',
                                "是否有同行人": item.day_info && item.day_info.has_partner ? item.day_info.has_partner : '',
                                "同行人数": item.day_info && item.day_info.partner_num ? item.day_info.partner_num : '',
                                "行程描述": item.day_info && item.day_info.trip_description ? item.day_info.trip_description : '',
                                "当天是否提供服务": item.day_info && item.day_info.is_work ? item.day_info.is_work : '',
                                "健康码截图": '',
                                "健康风险判定": item.status,
                                "上报日期": item.create_time ? item.create_time : item.modify_date,
                            });
                        } else if (this.props.type === '幸福院') {
                            file_name = '社区服务人员汇总表';
                            new_data.push({
                                "镇街": item.area_name,
                                "所属幸福院": item.org_name,
                                "运营方": item.org_name,
                                "姓名": item.user_name,
                                "证件号码": item.id_card,
                                "手机号码": item.phone,
                                "体温": item.day_info && item.day_info.temperature ? item.day_info.temperature : '',
                                "出现症状": item.day_info && item.day_info.symptom ? item.day_info.symptom : '',
                                "是否来自高风险地区": item.day_info && item.day_info.stay_disaster_area ? item.day_info.stay_disaster_area : '',
                                "复工后居住地": item.day_info && item.day_info.backWork_address ? item.day_info.backWork_address : '',
                                "上下班交通方式": item.day_info && item.day_info.transportation ? item.day_info.transportation : '',
                                "近期是否出佛山市（14天内）": item.day_info && item.day_info.is_out ? item.day_info.is_out : '',
                                "健康码截图": '',
                                "健康风险判定": item.status,
                                "上报日期": item.create_time ? item.create_time : item.modify_date,
                            });
                        } else if (this.props.type === '福利院') {
                            file_name = '养老机构工作人员出入登记';
                            new_data.push({
                                "镇街": item.area_name,
                                "所属机构": item.org_name,
                                "姓名": item.user_name,
                                "手机号码": item.phone,
                                "体温": item.day_info && item.day_info.temperature ? item.day_info.temperature : '',
                                "外出期间行程": item.day_info && item.day_info.trip ? item.day_info.trip : '',
                                "是否来自高风险地区": item.day_info && item.day_info.stay_disaster_area ? item.day_info.stay_disaster_area : '',
                                "复工后居住地": item.day_info && item.day_info.backWork_address ? item.day_info.backWork_address : '',
                                "上下班交通方式": item.day_info && item.day_info.transportation ? item.day_info.transportation : '',
                                "近期是否出佛山市（14天内）": item.day_info && item.day_info.is_out ? item.day_info.is_out : '',
                                "健康码截图": '',
                                "健康风险判定": item.status,
                                "上报日期": item.create_time ? item.create_time : item.modify_date,
                            });
                        }

                    });
                    exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
                } else {
                    message.info('暂无数据可导', 2);
                }
            });
    }

    // 下载模板
    downloadMoban = () => {
        let new_data: any = [];
        let file_name = '防疫模板';
        new_data = [{
            "证件号码": '',
            "手机号码": '',
            "近期住址": '',
            "体温": '',
            "出现症状": '',
            "是否逗留或途径疫情重灾区（湖北等地）": '',
            "是否接触过疫情重灾区人员（从湖北等地返回）": '',
            "一月以来是否有工作地/常住地以外的旅行史": '',
            "出发日期": '',
            "出发交通工具": '',
            "停留城市": '',
            "是否已回到本市": '',
            "到达日期": '',
            "到达交通工具": '',
            "预计到达日期": '',
            "是否有同行人": '',
            "同行人数": '',
            "行程描述": '',
            "当天是否提供服务": '',
        }];
        exprot_excel([{ name: file_name, value: new_data }], file_name, 'xls');
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }
    YiJianPanDing = () => {
        this.setState({
            visible2: true
        });
    }

    onePanDing = (type: any) => {
        const param = this.state.recode ? this.state.recode : null;
        AppServiceUtility.friends_circle_service.decide_by_once!({ status: type, 'type': this.props.type }, param)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.success('判定成功', 2, () => {
                        // 刷新数据
                        let _this = this.formCreator;
                        _this.select_func([_this.state.condition, _this.state.page, _this.state.pageSize]);
                    });
                } else {
                    message.info(data, 3);
                }
                this.setState({
                    visible: false,
                    visible2: false
                });
            }).catch((err) => {
                message.info(err.message);
                this.setState({
                    visible: false,
                    visible2: false
                });
            });
    }
    set_img_status = (isShow: boolean = true) => {
        this.setState({ isShow: isShow });
    }
    clickImg = (url: string) => {
        this.setState({ cur_img: url });
        this.set_img_status();
    }
    imgPreview = () => {
        return (
            <Modal visible={true} style={{ width: "700px", height: '500px' }} footer={null} onCancel={() => this.set_img_status(false)} centered={true}>
                <div style={{ maxHeight: '50vh', overflow: 'auto' }}>
                    <img src={this.state.cur_img} style={{ width: '100%' }} alt="" />
                </div>
            </Modal>
        );
    }
    /** 行选中事件 */
    onRowSelection = (data: any, selectedRows: any) => {
        console.info('拿到的数据', data);
        let recode: Array<string> = [];
        if (data.length) {
            recode = data.map((val: any) => {
                return val.day_info.id;
            });
        }
        this.setState({ recode });
        console.info('数据id>>>>>>>', recode);
    }
    render() {
        let leave_data: any[] = [
            {
                id: '高风险',
                name: '高风险'
            },
            {
                id: '中风险',
                name: '中风险'
            },
            {
                id: '低风险',
                name: '低风险'
            },
            {
                id: '未判定',
                name: '未判定'
            }
        ];
        let leave_datalist: any[] = [];
        leave_data!.map((item, idx) => {
            leave_datalist.push(<Option key={item.id}>{item.name}</Option>);
        });
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "姓名",
                decorator_id: "user_name",
                option: {
                    placeholder: "请输入姓名",
                }
            }, {
                type: InputType.input,
                label: "身份证号码",
                decorator_id: "id_card",
                option: {
                    placeholder: "请输入身份证号码",
                }
            }, {
                type: InputType.date,
                label: "上报时间",
                decorator_id: "create_time",
            },
            // {
            //     type: InputType.input,
            //     label: "所属机构",
            //     decorator_id: "org_name",
            //     option: {
            //         placeholder: "请输入所属机构",
            //     }
            // }, 
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
            {
                type: InputType.select,
                label: '风险判定',
                decorator_id: "status",
                option: {
                    childrens: leave_datalist,
                }
            },],
            btn_props: this.state.btn_props || [],
            columns_data_source: this.state.columns_data_source,
            scroll: { x: '100%' },
            table_size: "middle",
            bordered: true,
            service_object: AppServiceUtility.friends_circle_service,
            service_option: {
                select: {
                    service_func: 'get_epidemic_prevention_all',
                    service_condition: [{ type: this.props.type }, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            show_footer: true,
            rowKey: 'id',
            onRef: this.onRef,
            on_row_selection: this.onRowSelection,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let service_item_list = Object.assign(service_item);
        return (
            <div>
                {this.state.isShow && this.imgPreview()}
                <SignFrameLayout {...service_item_list} />
                <Modal
                    title="请选择判定结果"
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                        <Button type='danger' onClick={() => { this.handleOk('高风险'); }}>高风险</Button>
                        <Button style={{ color: 'white', backgroundColor: 'orange' }} onClick={() => { this.handleOk('中风险'); }}>中风险</Button>
                        <Button style={{ color: 'white', backgroundColor: 'green' }} onClick={() => { this.handleOk('低风险'); }}>低风险</Button>
                        {/* <Button onClick={this.handleCancel}>取消</Button> */}
                    </div>
                </Modal>
                <Modal
                    title="请选择判定结果"
                    visible={this.state.visible2}
                    onCancel={this.handleCancel2}
                    footer={null}
                >
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                        <Button type='danger' onClick={() => { this.onePanDing('高风险'); }}>高风险</Button>
                        <Button style={{ color: 'white', backgroundColor: 'orange' }} onClick={() => { this.onePanDing('中风险'); }}>中风险</Button>
                        <Button style={{ color: 'white', backgroundColor: 'green' }} onClick={() => { this.onePanDing('低风险'); }}>低风险</Button>
                        {/* <Button onClick={this.handleCancel}>取消</Button> */}
                    </div>
                </Modal>
                <UploadFile {...this.state.config} />
            </div>
        );
    }
}

/**
 * 设备列表页面
 */
@addon('JujiaEpidemicPreventionView', '设备列表页面', '描述')
@reactControl(Form.create<any>()(JujiaEpidemicPreventionView), true)
export class JujiaEpidemicPreventionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 类型 */
    public type?: string;
} 