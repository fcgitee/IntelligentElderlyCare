import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { ROUTE_PATH } from "src/projects/router";
import { table_param } from "src/projects/app/util-tool";
import { Form, Button } from 'antd';
import { exprot_excel } from "src/business/util_tool";
/**
 * 组件：机构每日登记信息状态
 */
export interface OrgDayInfoPreventionViewState extends ReactViewState {
    is_edit_btn?: any;
    is_delete_btn?: any;
    visible?: any;
    select_id?: any;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
    /* 组织机构 */
    org_list?: any;
}

/**
 * 组件：机构每日登记信息
 * 描述
 */
export class OrgDayInfoPreventionView extends ReactView<OrgDayInfoPreventionViewControl, OrgDayInfoPreventionViewState> {
    private formCreator: any = null;
    private columns_data_source = [
        {
            title: '镇街',
            dataIndex: 'area_name',
            key: 'area_name',
        },
        {
            title: '机构名称',
            dataIndex: 'org_name',
            key: 'org_name',
        },
        {
            title: '因特殊原因下班后需离开机构的工作人数',
            dataIndex: 'leave_work_num',
            key: 'leave_work_num',
        },
        {
            title: '是否有工作人员出现发热情况,如有请列明详细',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => {
                if (record.has_work_fever === '是') {
                    return record.has_work_fever + ',' + record.fever_people_detail;
                } else {
                    return record.has_work_fever;
                }
            }
        },
        {
            title: '新收老人数',
            dataIndex: 'new_old_num',
            key: 'new_old_num',
        },
        {
            title: '新收老人是否有做核酸测试',
            dataIndex: 'has_hesuan',
            key: 'has_hesuan',
        },
        {
            title: '新收老人是否有做隔离',
            dataIndex: 'has_geli',
            key: 'has_geli',
        },
        {
            title: '因发热送医院治疗的老人数并反馈医院诊断情况',
            dataIndex: 'hospital_elder_num',
            key: 'hospital_elder_num',
        },
        {
            title: '离开机构老人人数及情况（如因住院、退住等原因离开机构）',
            dataIndex: 'leave_elder_num',
            key: 'leave_elder_num',
        },
        {
            title: '因外出、住院等原因回机构老人是否有做核酸测试',
            dataIndex: 'back_has_hesuan',
            key: 'back_has_hesuan',
        },
        {
            title: '因外出、住院等原因回机构老人是否有做隔离',
            dataIndex: 'back_has_geli',
            key: 'back_has_geli',
        },
        {
            title: '在机构的所有工作人员和老人是否全部做过核酸检测',
            dataIndex: 'all_has_hesuan',
            key: 'all_has_hesuan',
        },
        {
            title: '未做过核酸检测的人数',
            dataIndex: 'unhesuan_num',
            key: 'unhesuan_num',
        },
        {
            title: '未做过核酸检测的人的具体原因',
            dataIndex: 'unhesuan_reason',
            key: 'unhesuan_reason',
        },
        {
            title: '是否有机构的老人离世？如有离世老人，请填写详细情况',
            dataIndex: 'has_elder_dead',
            key: 'has_elder_dead',
            render: (text: any, record: any) => {
                if (record.has_elder_dead === '是') {
                    return record.has_elder_dead + ',' + record.dead_detail;
                } else {
                    return record.has_elder_dead;
                }
            }
        },
        {
            title: '当天新入住老人、工作人员、探访人员在14天内是否有到过香港、澳门、新疆等高风险地区',
            dataIndex: 'new_has_to_high',
            key: 'new_has_to_high',
        },
        {
            title: '14天内有到过香港、澳门、新疆等高风险地区的人员是否有做核酸检测和三个一（做一次健康告知（问询）、扫一次健康码、指导在健康码上进行14天连续自主健康申报）',
            dataIndex: 'to_high_has_hesuan',
            key: 'to_high_has_hesuan',
        },
        {
            title: '填报人信息',
            dataIndex: 'user_name',
            key: 'user_name',
        },
        {
            title: '上报日期',
            dataIndex: 'modify_date',
            key: 'modify_date',
        },
        {
            title: '操作',
            dataIndex: '',
            key: '',
            width: 46,
            ellipsis: true,
            render: (text: any, record: any) => (
                <div>
                    <div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.is_edit_btn ?
                                    <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.goDetail(record.id); }}>编辑/查看</Button>
                                    : ''
                            }
                        </div>
                    </div>
                </div>
            ),
        }];
    constructor(props: OrgDayInfoPreventionViewControl) {
        super(props);
        this.state = {
            is_edit_btn: false,
            select_type: 1,
            condition: {},
            show: false,
            org_list: []
        };
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then((data: any) => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch((err: any) => {
                console.info(err);
            });
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.select_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        }
                    });
                }
            });
    }
    add = () => {
        this.props.history!.push(ROUTE_PATH.editOrgDayInfoPrevention);
    }
    goDetail = (id: any) => {
        this.props.history!.push(ROUTE_PATH.editOrgDayInfoPrevention + '/' + id);
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    // 下载
    download = () => {
        const _this = this.formCreator;
        AppServiceUtility.friends_circle_service.get_org_day_info_all!(_this.state.condition)!
            .then((data: any) => {
                if (data.result.length) {
                    let new_data: any = [];
                    data.result.map((item: any) => {
                        new_data.push({
                            "镇街": item.area_name[0],
                            "机构名称": item.org_name[0],
                            "因特殊原因下班后需离开机构的工作人数": item.leave_work_num,
                            "是否有工作人员出现发热情况,如有请列明详细": item.has_work_fever + ',' + item.fever_people_detail,
                            "新收老人数": item.new_old_num,
                            "新收老人是否有做核酸测试": item.has_hesuan,
                            "新收老人是否有做隔离": item.has_geli,
                            "因发热送医院治疗的老人数并反馈医院诊断情况": item.hospital_elder_num,
                            "离开机构老人人数及情况（如因住院、退住等原因离开机构）": item.leave_elder_num,
                            "因外出、住院等原因回机构老人是否有做核酸测试": item.back_has_hesuan,
                            "因外出、住院等原因回机构老人是否有做隔离": item.back_has_geli,
                            "在机构的所有工作人员和老人是否全部做过核酸检测": item.all_has_hesuan,
                            "未做过核酸检测的人数": item.unhesuan_num,
                            "未做过核酸检测的人的具体原因": item.unhesuan_reason,
                            "是否有机构的老人离世,如有离世来人请填写详细情况": item.has_elder_dead + ',' + item.dead_detail,
                            "填报人信息": item.user_name[0],
                            "上报日期": item.modify_date,
                        });
                    });
                    exprot_excel([{ name: '全省民政部门重点场所防疫工作情况汇总表', value: new_data }], '全省民政部门重点场所防疫工作情况汇总表', 'xls');
                }
            });

    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "镇街",
                decorator_id: "area_name",
                option: {
                    placeholder: "请输入镇街名称",
                }
            },
            // {
            //     type: InputType.input,
            //     label: "机构名称",
            //     decorator_id: "org_name",
            //     option: {
            //         placeholder: "请输入机构名称",
            //     }
            // },
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
            {
                type: InputType.date,
                label: "上报时间",
                decorator_id: "modify_date",
            }],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '导出',
                btn_method: this.download,
                icon: 'download'
            }],
            onRef: this.onRef,
            columns_data_source: this.columns_data_source,
            bordered: true,
            service_object: AppServiceUtility.friends_circle_service,
            service_option: {
                select: {
                    service_func: 'get_org_day_info_all',
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
            </div>
        );
    }
}

/**
 * 机构每日登记信息页面
 */
@addon('OrgDayInfoPreventionView', '机构每日登记信息页面', '描述')
@reactControl(Form.create<any>()(OrgDayInfoPreventionView), true)
export class OrgDayInfoPreventionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 