import { ReactViewState, reactControl, ReactViewControl, ReactView } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { Card, Table } from "antd";
/**
 * 组件：机构每日登记状态
 */
export interface EditOrgDayInfoViewState extends ReactViewState {
    day_list?: any;
}
/**
 * 组件：机构每日登记视图
 */
export class EditOrgDayInfoView extends ReactView<EditOrgDayInfoViewControl, EditOrgDayInfoViewState> {
    private columns_data_source = [
        {
            title: '镇街',
            dataIndex: 'area_name',
            key: 'area_name',
        },
        {
            title: '机构名称',
            dataIndex: 'org_name',
            key: 'org_name',
        },
        {
            title: '因特殊原因下班后需离开机构的工作人数',
            dataIndex: 'leave_work_num',
            key: 'leave_work_num',
        },
        {
            title: '是否有工作人员出现发热情况,如有请列明详细',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => {
                if (record.has_work_fever === '是') {
                    return record.has_work_fever + ',' + record.fever_people_detail;
                } else {
                    return record.has_work_fever;
                }
            }
        },
        {
            title: '新收老人数',
            dataIndex: 'new_old_num',
            key: 'new_old_num',
        },
        {
            title: '新收老人是否有做核酸测试',
            dataIndex: 'has_hesuan',
            key: 'has_hesuan',
        },
        {
            title: '新收老人是否有做隔离',
            dataIndex: 'has_geli',
            key: 'has_geli',
        },
        {
            title: '因发热送医院治疗的老人数并反馈医院诊断情况',
            dataIndex: 'hospital_elder_num',
            key: 'hospital_elder_num',
        },
        {
            title: '离开机构老人人数及情况（如因住院、退住等原因离开机构）',
            dataIndex: 'leave_elder_num',
            key: 'leave_elder_num',
        },
        {
            title: '因外出、住院等原因回机构老人是否有做核酸测试',
            dataIndex: 'back_has_hesuan',
            key: 'back_has_hesuan',
        },
        {
            title: '因外出、住院等原因回机构老人是否有做隔离',
            dataIndex: 'back_has_geli',
            key: 'back_has_geli',
        },
        {
            title: '在机构的所有工作人员和老人是否全部做过核酸检测',
            dataIndex: 'all_has_hesuan',
            key: 'all_has_hesuan',
        },
        {
            title: '未做过核酸检测的人数',
            dataIndex: 'unhesuan_num',
            key: 'unhesuan_num',
        },
        {
            title: '未做过核酸检测的人的具体原因',
            dataIndex: 'unhesuan_reason',
            key: 'unhesuan_reason',
        },
        {
            title: '是否有机构的老人离世？如有离世老人，请填写详细情况',
            dataIndex: 'has_elder_dead',
            key: 'has_elder_dead',
            render: (text: any, record: any) => {
                if (record.has_elder_dead === '是') {
                    return record.has_elder_dead + ',' + record.dead_detail;
                } else {
                    return record.has_elder_dead;
                }
            }
        },
        {
            title: '当天新入住老人、工作人员、探访人员在14天内是否有到过香港、澳门、新疆等高风险地区',
            dataIndex: 'new_has_to_high',
            key: 'new_has_to_high',
        },
        {
            title: '14天内有到过香港、澳门、新疆等高风险地区的人员是否有做核酸检测和三个一（做一次健康告知（问询）、扫一次健康码、指导在健康码上进行14天连续自主健康申报）',
            dataIndex: 'to_high_has_hesuan',
            key: 'to_high_has_hesuan',
        },
        {
            title: '填报人信息',
            dataIndex: 'user_name',
            key: 'user_name',
        },
        {
            title: '上报日期',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }];
    constructor(props: any) {
        super(props);
        this.state = {
            day_list: []
        };
    }
    componentDidMount() {
        AppServiceUtility.friends_circle_service.get_org_day_info_all!({ id: this.props.match!.params.key }, 1, 1)!
            .then((data: any) => {
                AppServiceUtility.friends_circle_service.get_org_day_info_all!({ organization_id: data.result[0]['organization_id'] }, 1, 10)!
                    .then((datas: any) => {
                        if (datas.result.length > 0) {
                            this.setState({
                                day_list: datas.result
                            });
                        }
                    })
                    .catch((err) => {
                        console.info(err);
                    });
            })
            .catch((err) => {
                console.info(err);
            });
    }
    render() {
        let layout = {
            labelCol: {
                xs: { span: 10 },
                sm: { span: 10 },
            },
            wrapperCol: {
                xs: { span: 14 },
                sm: { span: 14 },
            },
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "全省民政部门重点场所防疫工作情况汇总表",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "上报时间",
                            decorator_id: "modify_date",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                            },
                            layout: layout
                        },
                        {
                            type: InputType.antd_input,
                            label: "当天因特殊原因下班后需离开机构的工作人数",
                            decorator_id: "leave_work_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入当天因特殊原因下班后需离开机构的工作人数" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入当天因特殊原因下班后需离开机构的工作人数"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否有工作人员出现发热情况",
                            decorator_id: "has_work_fever",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.text_area,
                            label: "如有请列明详细",
                            decorator_id: "fever_people_detail",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请填写如有请列明详细" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写如有请列明详细"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.antd_input,
                            label: "当天新收老人数",
                            decorator_id: "new_old_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入当天新收老人数" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入当天新收老人数"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "新收老人是否有做核酸测试",
                            decorator_id: "has_hesuan",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "新收老人是否有做隔离",
                            decorator_id: "has_geli",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.text_area,
                            label: "因发热送医院治疗的老人数（请反馈医院诊断情况）",
                            decorator_id: "hospital_elder_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写因发热送医院治疗的老人数并反馈医院诊断情况" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写因发热送医院治疗的老人数并反馈医院诊断情况"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.text_area,
                            label: "离开机构老人人数及情况（如因住院、退住等原因离开机构）",
                            decorator_id: "leave_elder_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写离开机构老人人数及情况（如因住院、退住等原因离开机构）" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写离开机构老人人数及情况（如因住院、退住等原因离开机构）"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "因外出、住院等原因回机构老人是否有做核酸测试",
                            decorator_id: "back_has_hesuan",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "因外出、住院等原因回机构老人是否有做隔离",
                            decorator_id: "back_has_geli",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "在机构的所有工作人员和老人是否全部做过核酸检测",
                            decorator_id: "all_has_hesuan",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.antd_input,
                            label: "未做过核酸检测的人数",
                            decorator_id: "unhesuan_num",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入未做过核酸检测的人数" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入未做过核酸检测的人数"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.text_area,
                            label: "未做过核酸检测的人的具体原因",
                            decorator_id: "unhesuan_reason",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请填写未做过核酸检测的人的具体原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写未做过核酸检测的人的具体原因"
                            },
                            layout: layout
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否有机构的老人离世",
                            decorator_id: "has_elder_dead",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择是否" }],
                                initialValue: '是',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '是',
                                    value: '是',
                                }, {
                                    label: '否',
                                    value: '否',
                                }]
                            },
                            layout: layout
                        },
                        {
                            type: InputType.text_area,
                            label: "如有请列明详细",
                            decorator_id: "dead_detail",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请填写" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请填写"
                            },
                            layout: layout
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.OrgDayInfoPrevention);
                    }
                }
            ],
            submit_btn_propps: {
                text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.friends_circle_service,
                operation_option: {
                    save: {
                        func_name: "update_org_day_info"
                    },
                    query: {
                        func_name: "get_org_day_info_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                },
            },
            id: this.props.match!.params.key,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.OrgDayInfoPrevention); },
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                    <br />
                    <Card title="历史记录">
                        <Table columns={this.columns_data_source} dataSource={this.state.day_list} />
                    </Card>
                </MainContent>
            )
        );
    }
}
/**
 * 控件：机构每日登记编辑控件
 * @description 机构每日登记编辑控件
 * @author
 */
@addon('EditOrgDayInfoView', '机构每日登记编辑控件', '机构每日登记编辑控件')
@reactControl(EditOrgDayInfoView, true)
export class EditOrgDayInfoViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}