import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";
import { isPermission } from "src/projects/app/permission";
/**
 * 组件：编辑消防设施
 */
export interface ChangeFireEquipmentViewState extends ReactViewState {
}

/**
 * 组件：编辑消防设施
 */
export default class ChangeFireEquipmentView extends ReactView<ChangeFireEquipmentViewControl, ChangeFireEquipmentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }

        var edit_props = {
            form_items_props: [
                {
                    title: '消防设施',
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "消防设施名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入消防设施名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消防设施名称",
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "消防设施描述",
                            decorator_id: "introduction",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入消防设施名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消防设施名称",
                                row: 6,
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "消防设施图片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "photo",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请上传消防设施图片" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请上传消防设施图片",
                                action: remote.upload_url,
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        history.back();
                    }
                },
            ],
            submit_btn_propps: { text: "保存" },
            service_option: {
                service_object: AppServiceUtility.person_org_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_fire_equipment"
                    },
                    query: {
                        func_name: "get_fire_equipment_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { history.back(); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑消防设施
 * @description 编辑消防设施
 * @author
 */
@addon('ChangeFireEquipmentView', '编辑消防设施', '编辑消防设施')
@reactControl(ChangeFireEquipmentView, true)
export class ChangeFireEquipmentViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}