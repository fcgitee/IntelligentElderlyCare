import { Row, Form, message } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';
import { table_param } from 'src/projects/app/util-tool';
import { request } from 'src/business/util_tool';

/**
 * 组件：社区幸福院年度自评状态
 */
export interface YearSelfAssessmentListViewState extends ReactViewState {
    toyear_have?: boolean;
}

/**
 * 组件：社区幸福院年度自评
 * 描述
 */
export class YearSelfAssessmentListView extends ReactView<YearSelfAssessmentListViewControl, YearSelfAssessmentListViewState> {
    private columns_data_source = [
        {
            title: '年度',
            dataIndex: 'year',
            key: 'year',
        }, {
            title: '上次修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            toyear_have: false,
        };
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeYearSelfAssessment + '/' + contents.id);
        }
    }
    componentDidMount() {
        let year: number = new Date().getFullYear();
        request(this, AppServiceUtility.person_org_manage_service.get_happiness_year_self_assessment_list!({ year: year })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    this.setState({
                        toyear_have: true,
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
    }
    /** 新增按钮 */
    add = () => {
        if (this.state.toyear_have === true) {
            message.info('当前年份已有自评，如要修改请选择编辑！');
            return;
        }
        this.props.history!.push(ROUTE_PATH.changeYearSelfAssessment);
    }
    render() {
        let year_self_assessment = {
            type_show: false,
            edit_form_items_props: [
            ],
            btn_props: [{
                label: '新增年度自评',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_happiness_year_self_assessment_list_all',
                    service_condition: []
                },
                delete: {
                    service_func: 'delete_happiness_year_self_assessment'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let year_self_assessment_list = Object.assign(year_self_assessment, table_param);
        return (
            <Row>
                <SignFrameLayout {...year_self_assessment_list} />
            </Row>
        );
    }
}

/**
 * 控件：社区幸福院年度自评
 * 描述
 */
@addon('YearSelfAssessmentListView', '社区幸福院年度自评', '描述')
@reactControl(Form.create<any>()(YearSelfAssessmentListView), true)
export class YearSelfAssessmentListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}