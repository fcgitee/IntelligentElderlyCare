import { Form, Button, Card, Modal, Radio } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// import { table_param } from 'src/projects/app/util-tool';
import { ROUTE_PATH } from 'src/projects/router';
import { table_param } from 'src/projects/app/util-tool';
import { MainContent } from 'src/business/components/style-components/main-content';
import { MainCard } from 'src/business/components/style-components/main-card';
import { exprot_excel } from 'src/business/util_tool';
// import { request } from 'src/business/util_tool';

/**
 * 组件：社区幸福院年度自评汇总状态
 */
export interface HappinessYearSelfAssessmentListViewState extends ReactViewState {
    show?: boolean;
    select_type?: any;
}

/**
 * 组件：社区幸福院年度自评汇总
 * 描述
 */
export class HappinessYearSelfAssessmentListView extends ReactView<HappinessYearSelfAssessmentListViewControl, HappinessYearSelfAssessmentListViewState> {
    private columns_data_source = [
        {
            title: '幸福院名称',
            dataIndex: 'organization_name',
            key: 'organization_name',
        }, {
            title: '地址',
            dataIndex: 'organization_address',
            key: 'organization_address',
        }, {
            title: '自评得分',
            dataIndex: 'self_score',
            key: 'self_score',
        }, {
            title: '提交时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }, {
            title: '提交人员',
            dataIndex: 'creator_name',
            key: 'creator_name',
        }, {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            render: (text: any, record: any) => {
                return (
                    <Button onClick={() => this.add(record)} type="primary">查看</Button>
                );
            }
        }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            show: false,
            select_type: 1,
        };
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeHappinessYearSelfAssessment + '/' + contents.id);
        }
    }
    componentDidMount() {
    }
    /** 新增按钮 */
    add = (contents: any) => {
        this.props.history!.push(ROUTE_PATH.changeHappinessYearSelfAssessment + '/' + contents.id);
    }
    /** 导出按钮 */
    daochu = () => {
        this.setState({ show: true });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }
    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            AppServiceUtility.person_org_manage_service.get_happiness_year_self_assessment_list!({})!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            let param: any = {};
                            for (let i = 0; i < this.columns_data_source.length; i++) {
                                if (this.columns_data_source[i]['title'] === '操作') {
                                    continue;
                                }
                                param[this.columns_data_source[i]['title']] = item[this.columns_data_source[i]['key']];
                            }
                            new_data.push(param);
                        });
                        exprot_excel([{ name: '社区幸福院自评汇总', value: new_data }], '社区幸福院自评汇总', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }
    render() {
        let year_self_assessment = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "幸福院名称",
                    decorator_id: "organization_name",
                    option: {
                        placeholder: "请输入幸福院名称",
                    }
                }
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.daochu,
                icon: 'download'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_happiness_year_self_assessment_list_all',
                    service_condition: []
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let year_self_assessment_list = Object.assign(year_self_assessment, table_param);
        const { getFieldDecorator } = this.props.form!;
        return (
            <MainContent>
                <Card title={'社区幸福院自评汇总'}>
                    <SignFrameLayout {...year_self_assessment_list} />
                </Card>
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard>
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：社区幸福院年度自评汇总
 * 描述
 */
@addon('HappinessYearSelfAssessmentListView', '社区幸福院年度自评汇总', '描述')
@reactControl(Form.create<any>()(HappinessYearSelfAssessmentListView), true)
export class HappinessYearSelfAssessmentListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}