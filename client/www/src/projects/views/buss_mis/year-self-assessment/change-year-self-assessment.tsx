// import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { Card, Table, InputNumber, Form, Button, Row, message } from "antd";
import { request } from "src/business/util_tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：社区幸福院年度自评
 */
export interface ChangeYearSelfAssessmentViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    /** 任务数组 */
    columns?: any;
    data?: any;
    self_score?: number;
    score_data?: any;
    is_send?: any;
    data_info?: any;
}

/**
 * 组件：社区幸福院年度自评
 */
export default class ChangeYearSelfAssessmentView extends ReactView<ChangeYearSelfAssessmentViewControl, ChangeYearSelfAssessmentViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            is_send: false,
            columns: [],
            data: [],
            data_info: [],
            score_data: {},
            self_score: 0,
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.person_org_manage_service.get_happiness_year_self_assessment_list!({ id: this.props.match!.params.key })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    this.setState({
                        data_info: datas.result[0],
                        self_score: datas.result[0].self_score,
                        score_data: datas.result[0].score_data,
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
        request(this, AppServiceUtility.person_org_manage_service.get_xfy_target_setting_details_list!({})!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    let score_data: any = {};
                    let new_data: any = [];
                    let temp_data: any = {};
                    for (let i = 0; i < datas.result.length; i++) {
                        if (temp_data[datas.result[i].first_level_id]) {
                            temp_data[datas.result[i].first_level_id].push(datas.result[i]);
                        } else {
                            temp_data[datas.result[i].first_level_id] = [
                                datas.result[i]
                            ];
                        }
                    }
                    let objectKeys = Object.keys(temp_data);
                    let base_all: number = 0;
                    let bonus_all: number = 0;
                    for (let j = 0; j < objectKeys.length; j++) {
                        for (let k = 0; k < temp_data[objectKeys[j]].length; k++) {
                            // 基础分
                            if (!isNaN(temp_data[objectKeys[j]][k]['base_score'])) {
                                base_all += Number(temp_data[objectKeys[j]][k]['base_score']);
                            }
                            // 加分
                            if (!isNaN(temp_data[objectKeys[j]][k]['bonus_points'])) {
                                bonus_all += Number(temp_data[objectKeys[j]][k]['bonus_points']);
                            }
                            if (k === 0) {
                                temp_data[objectKeys[j]][k]['is_row_span'] = true;
                                temp_data[objectKeys[j]][k]['row_span'] = temp_data[objectKeys[j]].length;
                            } else {
                                temp_data[objectKeys[j]][k]['is_row_span'] = true;
                                temp_data[objectKeys[j]][k]['row_span'] = 0;
                            }
                            score_data[temp_data[objectKeys[j]][k]['id']] = undefined;
                            new_data.push(temp_data[objectKeys[j]][k]);
                        }
                    }
                    // push一个统计的
                    new_data.push({
                        first_level_name: '合计',
                        base_score: base_all,
                        bonus_points: bonus_all,
                    });
                    // 编辑界面不需要更新分数字段
                    if (this.props.match!.params.key) {
                        score_data = this.state.score_data;
                    }
                    this.setState({
                        data: new_data,
                        score_data: score_data,
                        columns: [
                            {
                                title: '一级指标',
                                key: 'first_level_name',
                                dataIndex: 'first_level_name',
                                render: (text: any, record: any, index: number) => {
                                    const obj = {
                                        children: text,
                                        props: {
                                            rowSpan: 1,
                                            colSpan: 1,
                                        },
                                    };
                                    if (record.is_row_span === true) {
                                        obj.props.rowSpan = record.row_span;
                                    }
                                    if (index === new_data.length - 1) {
                                        obj.props.colSpan = 4;
                                    }
                                    return obj;
                                },
                            },
                            {
                                title: '二级指标',
                                key: 'second_level_name',
                                dataIndex: 'second_level_name',
                                width: 200,
                                render: (text: any, record: any, index: number) => {
                                    const obj = {
                                        children: text,
                                        props: {
                                            rowSpan: 1,
                                            colSpan: 1,
                                        },
                                    };
                                    if (index === new_data.length - 1) {
                                        obj.props.colSpan = 0;
                                    }
                                    return obj;
                                }
                            },
                            {
                                title: '三级指标',
                                key: 'third_level_name',
                                dataIndex: 'third_level_name',
                                width: 300,
                                render: (text: any, record: any, index: number) => {
                                    const obj = {
                                        children: text,
                                        props: {
                                            rowSpan: 1,
                                            colSpan: 1,
                                        },
                                    };
                                    if (index === new_data.length - 1) {
                                        obj.props.colSpan = 0;
                                    }
                                    return obj;
                                }
                            },
                            {
                                title: '评比细则',
                                key: 'evaluation_rules',
                                dataIndex: 'evaluation_rules',
                                width: 300,
                                render: (text: any, record: any, index: number) => {
                                    const obj = {
                                        children: text,
                                        props: {
                                            rowSpan: 1,
                                            colSpan: 1,
                                        },
                                    };
                                    if (index === new_data.length - 1) {
                                        obj.props.colSpan = 0;
                                    }
                                    return obj;
                                }
                            },
                            {
                                title: '分值',
                                dataIndex: 'score',
                                key: 'score',
                                children: [
                                    {
                                        title: '基础分',
                                        dataIndex: 'base_score',
                                        key: 'base_score',
                                    },
                                    {
                                        title: '加分项',
                                        dataIndex: 'bonus_points',
                                        key: 'bonus_points',
                                    },
                                ]
                            },
                            {
                                title: '自评分',
                                dataIndex: 'self',
                                key: 'self',
                                render: (text: any, record: any, index: number) => {
                                    if (index === new_data.length - 1) {
                                        return this.state.self_score;
                                    } else {
                                        let max: number = (isNaN(record.base_score) ? 0 : Number(record.base_score)) + (isNaN(record.bonus_points) ? 0 : Number(record.bonus_points));
                                        return (
                                            <InputNumber key={record.id} onChange={(e: any) => this.changeScore(e, record.id)} placeholder="请输入自评分" autoComplete="off" value={this.state.score_data && this.state.score_data[record.id] ? this.state.score_data[record.id] : ''} style={{ minWidth: '200px' }} min={0} max={max} />
                                        );
                                    }
                                }
                            },
                        ]
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
    }
    limitNumber = (value: any) => {
        if (value === 0 || value === '0') {
            return value;
        }
        if (typeof value === 'string') {
            return !isNaN(Number(value)) ? value.replace(/^(0+)|[^\d]/g, '') : '';
        } else if (typeof value === 'number') {
            return !isNaN(value) ? String(value).replace(/^(0+)|[^\d]/g, '') : '';
        } else {
            return '';
        }
    }
    changeScore = (e: any, record_id: string) => {
        let { score_data } = this.state;
        let self_score = 0;
        score_data[record_id] = e;
        let objectKeys = Object.keys(score_data);
        for (let i = 0; i < objectKeys.length; i++) {
            self_score += isNaN(score_data[objectKeys[i]]) ? 0 : Number(score_data[objectKeys[i]]);
        }
        this.setState({
            score_data,
            self_score,
        });
    }
    resetForm = () => {
        this.setState({
            score_data: {},
        });
    }
    save = () => {
        if (this.state.is_send === true) {
            message.info('请勿操作过快！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let param: any = {
                    score_data: this.state.score_data,
                    self_score: this.state.self_score
                };
                request(this, AppServiceUtility.person_org_manage_service.update_happiness_year_self_assessment!(param)!)
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                this.props.history!.push(ROUTE_PATH.yearSelfAssessment);
                            });
                        } else {
                            this.setState({
                                is_send: false,
                            });
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let year: any = '';
        if (!this.props.match!.params.key) {
            year = new Date().getFullYear() + '年';
        } else if (this.state.data_info && this.state.data_info.year) {
            year = this.state.data_info.year + '年';
        }
        return (
            <MainContent>
                <Card title={`${year}幸福院年度自评`}>
                    <Table rowKey="id" columns={this.state.columns} dataSource={this.state.data} bordered={true} pagination={false} />
                    <Row type='flex' justify='center' className="ctrl-btns">
                        <Button type='primary' onClick={this.save}>保存</Button>
                        <Button type='primary' onClick={this.resetForm}>重置</Button>
                    </Row>
                </Card>
            </MainContent>
        );
    }
}

/**
 * 控件：社区幸福院年度自评
 * @description 社区幸福院年度自评
 * @author
 */
@addon('ChangeYearSelfAssessmentView', '社区幸福院年度自评', '社区幸福院年度自评')
@reactControl(Form.create<any>()(ChangeYearSelfAssessmentView), true)
export class ChangeYearSelfAssessmentViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}