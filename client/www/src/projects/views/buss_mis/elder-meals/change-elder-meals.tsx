import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
// import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
// import { isPermission } from "src/projects/app/permission";
// import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { Table, Input, InputNumber, Popconfirm, Form, Button, Card, Row, Icon, message, Col, Radio } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
const EditableContext = React.createContext('');
const keys: any = ['主食', '主菜', '副主菜', '配菜', '汤', '水果'];
const type: any = ['午餐', '晚餐'];
const emptyData: any = [];
keys.map((item: any, index: number) => {
    emptyData.push({
        key: `${index}`,
        name: `${item}`,
        monday: '',
        tuesday: '',
        wednesday: '',
        thursday: '',
        friday: '',
        saturday: '',
        sunday: '',
        action: '',
    });
});
/**
 * 组件：长者膳食详情
 */
export interface ChangeElderMealsViewState extends ReactViewState {
    /** 数据id */
    id?: string;
    data?: any;
    editingKey?: any;
    picture?: any;
    nowWeekStr?: any;
    nowWeekKey?: any;
    nowDate?: any;
    is_add_permission?: boolean;
    type?: string;
    current_data_info?: any;
    radio?: any;
    is_send?: any;
    /** 任务数组 */
}

export function formatDateFunc(date: any) {
    let nowTime = date.getTime();
    let day = date.getDay();
    let oneDayTime = 24 * 60 * 60 * 1000;
    // 显示周一
    let MondayTime = nowTime - (day - 1) * oneDayTime;
    // 显示周日
    let SundayTime = nowTime + (7 - day) * oneDayTime;

    let monday_month: any = new Date(MondayTime).getMonth() + 1;
    let monday_day: any = new Date(MondayTime).getDate();
    // 周一
    if (monday_month < 10) {
        monday_month = "0" + monday_month;
    }
    if (monday_day < 10) {
        monday_day = "0" + monday_day;
    }
    // 周日
    let sunday_month: any = new Date(SundayTime).getMonth() + 1;
    let sunday_day: any = new Date(SundayTime).getDate();
    if (sunday_month < 10) {
        sunday_month = "0" + sunday_month;
    }
    if (sunday_day < 10) {
        sunday_day = "0" + sunday_day;
    }
    return {
        nowWeekStr: `${monday_month}月${monday_day}日-${sunday_month}月${sunday_day}日`,
        nowWeekKey: `${monday_month}-${monday_day}-${sunday_month}-${sunday_day}`,
    };
}
/**
 * 组件：长者膳食详情
 */
export default class ChangeElderMealsView extends ReactView<ChangeElderMealsViewControl, ChangeElderMealsViewState> {
    private columns: any = [
        {
            title: '菜谱',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '周一',
            dataIndex: 'monday',
            key: 'monday',
            editable: true,
        },
        {
            title: '周二',
            dataIndex: 'tuesday',
            key: 'tuesday',
            editable: true,
        },
        {
            title: '周三',
            dataIndex: 'wednesday',
            key: 'wednesday',
            editable: true,
        },
        {
            title: '周四',
            dataIndex: 'thursday',
            key: 'thursday',
            editable: true,
        },
        {
            title: '周五',
            dataIndex: 'friday',
            key: 'friday',
            editable: true,
        },
        {
            title: '周六',
            dataIndex: 'saturday',
            key: 'saturday',
            editable: true,
        },
        {
            title: '周日',
            dataIndex: 'sunday',
            key: 'sunday',
            editable: true,
        },
        {
            title: '操作',
            dataIndex: 'action',
            render: (text: any, record: any) => {
                const { editingKey } = this.state;
                const editable = this.isEditing(record);
                return editable ? (
                    <span>
                        <EditableContext.Consumer>
                            {(form: any) => (
                                <a
                                    onClick={() => this.save(form, record.key)}
                                    style={{ marginRight: 8 }}
                                >
                                    保存
                                </a>
                            )}
                        </EditableContext.Consumer>
                        <Popconfirm title="确定取消吗" cancelText="取消" onConfirm={() => this.cancel()}>
                            <a>取消</a>
                        </Popconfirm>
                    </span>
                ) : (
                        <Button disabled={editingKey !== ''} onClick={() => this.edit(record.key)}>
                            编辑
                        </Button>
                    );
            },
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            id: '',
            data: emptyData,
            editingKey: '',
            nowWeekStr: '',
            is_add_permission: true,
            radio: type[0],
            current_data_info: [],
            is_send: false,
        };
    }
    componentDidMount = () => {
        // 判断权限
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: '长者膳食', p: '新增' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        is_add_permission: true,
                    });
                }
            });
        let formatDate: any = formatDateFunc(new Date());
        this.setState(
            {
                nowWeekStr: formatDate['nowWeekStr'],
                nowWeekKey: formatDate['nowWeekKey'],
                nowDate: new Date(),
            },
            () => {
                this.getData();
            }
        );
    }
    isEditing = (record: any) => record.key === this.state.editingKey;

    getData() {
        request(this, AppServiceUtility.person_org_manage_service.get_elder_meals_list_all!({ key: this.state.nowWeekKey, type: this.state.radio })!)
            .then((datas: any) => {
                if (datas && datas.result && datas.result.length) {
                    this.props.form.setFieldsValue({
                        picture: datas.result[0]['picture'],
                    });
                    this.setState({
                        data: datas.result[0]['data'],
                        current_data_info: datas.result[0],
                    });
                } else {
                    this.props.form.setFieldsValue({
                        picture: [],
                    });
                    this.setState({
                        data: emptyData,
                        current_data_info: [],
                    });
                }
            });
    }

    cancel = () => {
        this.setState({ editingKey: '' });
    }
    setPrev = () => {
        let nowDate: any = new Date(this.state.nowDate.getTime() - 86400 * 7 * 1000);
        let formatDate: any = formatDateFunc(nowDate);
        this.setState(
            {
                nowWeekStr: formatDate['nowWeekStr'],
                nowWeekKey: formatDate['nowWeekKey'],
                nowDate: nowDate
            },
            () => {
                this.getData();
            }
        );
    }
    setNext = () => {
        if ((new Date().getTime() - this.state.nowDate.getTime()) < 86400 * 7) {
            message.info('当前已经是最新一周');
            return;
        }
        let nowDate: any = new Date(this.state.nowDate.getTime() + 86400 * 7 * 1000);
        let formatDate: any = formatDateFunc(nowDate);
        this.setState(
            {
                nowWeekStr: formatDate['nowWeekStr'],
                nowWeekKey: formatDate['nowWeekKey'],
                nowDate: nowDate
            },
            () => {
                this.getData();
            }
        );
    }

    save(form: any, key: any) {
        form.validateFields((error: any, row: any) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                this.setState({ data: newData, editingKey: '' });
            } else {
                newData.push(row);
                this.setState({ data: newData, editingKey: '' });
            }
        });
    }

    edit(key: any) {
        this.setState({ editingKey: key });
    }
    saveMeals() {
        if (this.state.is_send === true) {
            message.info('请勿操作过快！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                let param: any = {};
                if (this.state.current_data_info && this.state.current_data_info.hasOwnProperty('id')) {
                    param['id'] = this.state.current_data_info.id;
                }
                param['picture'] = this.state.picture;
                param['key'] = this.state.nowWeekKey;
                param['type'] = this.state.radio;
                let new_data: any = [];
                this.state.data.map((item: any, index: number) => {
                    new_data.push({
                        key: item.key,
                        name: item.name,
                        monday: item.monday,
                        tuesday: item.tuesday,
                        wednesday: item.wednesday,
                        thursday: item.thursday,
                        friday: item.friday,
                        saturday: item.saturday,
                        sunday: item.sunday,
                    });
                });
                param['data'] = new_data;
                request(this, AppServiceUtility.person_org_manage_service.update_elder_meals!(param)!)
                    .then((datas: any) => {
                        this.setState({
                            is_send: false,
                        });
                        if (datas === 'Success') {
                            message.info('保存成功！', 1, () => {
                                this.getData();
                            });
                        } else {
                            message.info(datas);
                        }
                    })
                    .catch((error: any) => {
                        this.setState({
                            is_send: false,
                        });
                        // console.log(error);
                    });
            }
        );
    }
    changePicture = (e: any) => {
        this.setState({
            picture: e,
        });
    }
    changeRadio = (e: any) => {
        this.setState(
            {
                radio: e.target.value,
            },
            () => {
                this.getData();
            }
        );
    }
    reset() {
        this.props.form.setFieldsValue({
            picture: [],
        });
        this.setState({ data: emptyData });
    }
    setNewest = () => {
        if ((new Date().getTime() - this.state.nowDate.getTime()) < 86400 * 7) {
            message.info('当前已经是最新一周');
            return;
        }
        let formatDate: any = formatDateFunc(new Date());
        this.setState(
            {
                nowWeekStr: formatDate['nowWeekStr'],
                nowWeekKey: formatDate['nowWeekKey'],
                nowDate: new Date(),
            },
            () => {
                this.getData();
            }
        );
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        const components = {
            body: {
                cell: EditableFormTable,
            },
        };

        const columns = this.columns.map((col: any) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: (record: any) => ({
                    record,
                    inputType: col.dataIndex === 'age' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });
        const { getFieldDecorator } = this.props.form;
        const { is_add_permission } = this.state;
        return (
            <MainContent>
                <Card title="长者膳食">
                    <Row type="flex" justify="start" align="middle">
                        <Col span={4}>
                            <Icon type="caret-left" onClick={this.setPrev} />
                            &nbsp;
                            &nbsp;
                            {this.state.nowWeekStr}
                            &nbsp;
                            &nbsp;
                            <Icon type="caret-right" onClick={this.setNext} />
                        </Col>
                        <Col span={4}>
                            <Button onClick={this.setNewest}>最新</Button>
                        </Col>
                        <Col span={16}>
                            <Radio.Group onChange={this.changeRadio} value={this.state.radio}>
                                {type.map((item: any, index: number) => {
                                    return (
                                        <Radio key={item} value={item}>{item}</Radio>
                                    );
                                })}
                            </Radio.Group>
                        </Col>
                    </Row>
                    <br />
                    <EditableContext.Provider value={this.props.form}>
                        <Table
                            components={components}
                            bordered={true}
                            dataSource={this.state.data}
                            columns={columns}
                            pagination={false}
                        />
                    </EditableContext.Provider>
                    <Row>
                        <Form.Item label='膳食图片（大小小于2M，格式支持jpg/jpeg/png）'>
                            {getFieldDecorator('picture', {
                                initialValue: [],
                                onChange: this.changePicture
                            })(
                                <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                            )}
                        </Form.Item>
                    </Row>
                    {is_add_permission ? <Row type='flex' justify='center' className="ctrl-btns">
                        <Button onClick={() => this.reset()} >重置</Button>
                        <Button onClick={() => this.saveMeals()} type="primary">保存</Button>
                    </Row> : null}
                </Card>
            </MainContent>
        );
    }
}

class EditableCell extends React.Component<any, any> {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />;
        }
        return <Input />;
    }

    renderCell = (form: any) => {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            children,
            ...restProps
        } = this.props;
        const { getFieldDecorator } = form;
        return (
            <td {...restProps}>
                {editing ? (
                    <Form.Item style={{ margin: 0 }}>
                        {getFieldDecorator(dataIndex, {
                            initialValue: record[dataIndex],
                        })(this.getInput())}
                    </Form.Item>
                ) : (
                        children
                    )}
            </td>
        );
    }

    render() {
        return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
    }
}

const EditableFormTable = Form.create()(EditableCell);

/**
 * 控件：长者膳食详情
 * @description 长者膳食详情
 * @author
 */
@addon('ChangeElderMealsView', '长者膳食详情', '长者膳食详情')
@reactControl(Form.create()(ChangeElderMealsView), true)
export class ChangeElderMealsViewControl extends ReactViewControl {
    /** 上一页url */
    public back_url?: string;
    /** 视图权限 */
    public permission?: Permission;
}