import { Button, Table, Modal, LocaleProvider, Form, Radio, Select } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
import { request } from "src/business/util_tool";
import { exprot_excel } from "src/business/util_tool";
import zh_CN from 'antd/lib/locale-provider/zh_CN';
const { Option } = Select;
/**
 * 组件：公众号关注统计
 */
export interface officialAccountStatisticsState extends ReactViewState {
    administrative_division_list: any[];
    area_id: string;
    table_total_size: number;
    base_situation: any[];
    page: number;
    page_size: number;
    /** 控制弹框显示 */
    show?: any;
    /** 排序字段 */
    sort?: string;
    /** 服务商列表 */
    org_list: any[];
    /** 选择的服务商Id */
    organization_id: string;
    /** 选择的镇街名称 */
    zj_name?: string;
    /* 总数 */
    total?: any;
}

/**
 * 组件：公众号关注统计
 * 描述
 */
export class officialAccountStatistics extends ReactView<officialAccountStatisticsControl, officialAccountStatisticsState> {
    columns: any = [
        {
            title: '幸福院',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '镇街',
            dataIndex: 'admin_zj_name',
            key: 'admin_zj_name',
            render: (text: any, record: any, index: number) => {
                if (record.admin_name === '佛山市') {
                    return '佛山市';
                } else {
                    return text;
                }
            }
        },
        {
            title: '社区',
            dataIndex: 'admin_name',
            key: 'admin_name',
            render: (text: any, record: any, index: number) => {
                if (text === '佛山市') {
                    return '';
                } else {
                    return text;
                }
            }
        },
        {
            title: '关注数',
            dataIndex: 'follow_quantity',
            key: 'follow_quantity',
        },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            administrative_division_list: [],
            area_id: '',
            table_total_size: 0,
            base_situation: [],
            page: 1,
            page_size: 10,
            show: false,
            sort: '-1',
            org_list: [],
            organization_id: '',
            total: 0
        };
    }
    componentWillMount = () => {
        // 查询幸福院列表
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ 'personnel_category': '幸福院' }))!
            .then((data: any) => {
                let res = data!.result!;
                res.push({ 'id': '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67', 'name': '智慧养老平台' });
                // console.log('res>>', res);
                this.setState({
                    org_list: res,
                    // organization_id: data!.result!.length > 0 ? data!.result![0]['id'] : ''
                });
            })
            .catch(err => {
                console.info(err);
            });
        this.getofficialAccountStatisticsData({ sort: this.state.sort }, this.state.page, this.state.page_size);
    }

    // onChangeTree = (value: any) => {
    //     this.setState({
    //         area_id: value
    //     });
    // }

    // onSearch = () => {
    //     this.getofficialAccountStatisticsData(this.state.page, this.state.page_size);
    // }

    getofficialAccountStatisticsData = (condition: {}, page: number, pageSize: number) => {
        AppServiceUtility.official_follow_service!.get_Statistics_list!(condition, page, pageSize)!
            .then(data => {
                let total: any = 0;
                data!.result!.map((item: any) => {
                    total += item['follow_quantity'];
                });
                this.setState({
                    base_situation: data!.result!,
                    table_total_size: data!.total!,
                    total
                });
            }).catch(e => {
                console.error(e);
            });

    }
    // 导出类型选择
    onChangeRadio(value: any) {

    }
    onPageChange = (page: number, pageSize: number) => {
        this.setState({
            page: page,
            page_size: pageSize
        });
        let condition = {};
        if (this.state.sort) {
            condition['sort'] = this.state.sort;
        }
        if (this.state.organization_id) {
            condition['organization_id'] = this.state.organization_id;
        }
        if (this.state.zj_name) {
            condition['zj_name'] = this.state.zj_name;
        }
        this.getofficialAccountStatisticsData(condition, page, pageSize);
    }

    // 下载
    download() {
        // let export_data: any = [];
        // 全部
        let condition = {};
        if (this.state.sort) {
            condition['sort'] = this.state.sort;
        }
        if (this.state.organization_id) {
            condition['organization_id'] = this.state.organization_id;
        }
        request(this, AppServiceUtility.official_follow_service.get_Statistics_list!(condition))!
            .then((data: any) => {
                if (data['result']) {
                    let new_data: any = [];
                    data['result'].map((item: any) => {
                        new_data.push({
                            "幸福院": item.name,
                            '镇街': item.admin_name === '佛山市' ? '佛山市' : item.admin_zj_name,
                            '社区': item.admin_name === '佛山市' ? '' : item.admin_name,
                            "关注数": item.follow_quantity,
                        });
                    });
                    exprot_excel([{ name: '幸福院公众号关注统计', value: new_data }], '幸福院公众号关注统计', 'xls');
                }
            });
        this.setState({ 'show': false });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    /** 下拉框改变事件 */
    select_change = (value: any) => {
        this.setState({
            sort: value
        });
        let condition = { 'sort': value, };
        if (this.state.organization_id) {
            condition['organization_id'] = this.state.organization_id;
        }
        if (this.state.zj_name) {
            condition['zj_name'] = this.state.zj_name;
        }
        this.getofficialAccountStatisticsData(condition, this.state.page, this.state.page_size);
    }
    /** 树形选择框改变事件 */
    onChangeTree = (value: any) => {
        this.setState({
            organization_id: value,
            page: 1,
            page_size: 10,
        });
        let condition = { 'organization_id': value };
        if (this.state.sort) {
            condition['sort'] = this.state.sort;
        }
        if (this.state.zj_name) {
            condition['zj_name'] = this.state.zj_name;
        }
        this.getofficialAccountStatisticsData(condition, 1, 10);
    }
    footer = () => '当前 ' + this.state.page + ' / ' + Math.ceil(this.state.table_total_size / 10) + '，共 ' + this.state.table_total_size + ' 条';

    /** 镇街选择改变事件 */
    onChangeZj = (value: any) => {
        // console.log("这里");
        this.setState({
            zj_name: value,
            page: 1,
            page_size: 10,
        });
        let condition = { 'zj_name': value };
        if (this.state.sort) {
            condition['sort'] = this.state.sort;
        }
        if (this.state.organization_id) {
            condition['organization_id'] = this.state.organization_id;
        }
        this.getofficialAccountStatisticsData(condition, 1, 10);
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        let admin_zj_list = ['佛山市', '桂城', '大沥', '丹灶', '狮山', '九江', '里水', '西樵'];
        return (
            <MainContent>
                <LocaleProvider locale={zh_CN}>
                    <MainCard title={`公众号关注数统计   总数：${this.state.total}`} style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" onClick={this.export} style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                            {/* <Button type="primary" onClick={this.onSearch} style={{ float: 'right', marginRight: '3px' }}>查询</Button> */}
                            <div style={{ float: 'right', marginRight: '50px' }}>
                                <Select
                                    showSearch={true}
                                    allowClear={true}
                                    optionFilterProp="children"
                                    onChange={this.onChangeZj}
                                    placeholder={"请选择镇街"}
                                    style={{
                                        width: '20em'
                                    }}
                                >
                                    {admin_zj_list!.length > 0 ? admin_zj_list!.map((item: any) => <Option key={item} value={item}>{item}</Option>) : null}
                                </Select>
                                <Select
                                    showSearch={true}
                                    allowClear={true}
                                    optionFilterProp="children"
                                    onChange={this.onChangeTree}
                                    placeholder={"请选择机构"}
                                    style={{
                                        width: '20em'
                                    }}
                                >
                                    {this.state.org_list!.length > 0 ? this.state.org_list!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>) : null}
                                </Select>
                                <Select
                                    style={{ width: 120 }}
                                    showSearch={true}
                                    onChange={this.select_change}
                                    defaultValue={-1}
                                >
                                    <Option key={1} value={1}>{'升序'}</Option>
                                    <Option key={2} value={-1}>{'降序'}</Option>
                                </Select>
                            </div>
                        </div>
                        <Table
                            columns={this.columns}
                            dataSource={this.state.base_situation}
                            // pagination={{ pageSize: 10, total: this.state.table_total_size, onChange: this.onPageChange }}
                            bordered={true}
                            size="middle"
                            footer={this.footer}
                            className="table-footer"
                            pagination={{
                                showQuickJumper: true,
                                // showSizeChanger: true,
                                // onShowSizeChange: this.onShowSizeChange,
                                onChange: this.onPageChange,
                                defaultPageSize: this.state.page_size,
                                current: this.state.page!,
                                total: this.state.table_total_size
                            }}
                        />
                    </MainCard>
                    <Modal
                        title="请选择导出类型"
                        visible={this.state.show}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ show: !this.state.show })}
                        okText="下载"
                        cancelText="取消"
                    >
                        <MainCard>
                            <Form>
                                <Form.Item>
                                    {getFieldDecorator('isAll', {
                                        initialValue: ''
                                    })(
                                        <Radio.Group onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                            <Radio key={1} value={1}>导出数据</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            </Form>
                        </MainCard >
                    </Modal>
                </LocaleProvider>
            </MainContent>
        );
    }
}

/**
 * 控件：公众号关注统计
 * 描述
 */
@addon('officialAccountStatistics', '公众号关注统计', '')
@reactControl(Form.create<any>()(officialAccountStatistics), true)
export class officialAccountStatisticsControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 判断类型 */
    public select_type?: any;
}