
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";

/** 状态：费用统计视图 */
export interface CostStatisticsViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
}
/** 组件：费用统计视图 */
export class CostStatisticsView extends React.Component<CostStatisticsViewControl, CostStatisticsViewState> {
    private columns_data_source = [
        {
            title: '统计时间段（周）',
            dataIndex: 'year_month',
            key: 'year_month',
        }, {
            title: '社区幸福院名称',
            dataIndex: 'org_name',
            key: 'org_name',
        }, {
            title: '日托订单',
            dataIndex: 'account',
            key: 'account',
            children: [{
                title: '订单数量',
                dataIndex: 'rt_total_order_num',
                key: 'rt_total_order_num',
            }, {
                title: '订单总金额/元',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            },]
        },
        {
            title: '长者膳食',
            dataIndex: 'account',
            key: 'account',
            children: [{
                title: '午餐膳食长者数',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            }, {
                title: '盈利金额/元',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            }, {
                title: '晚餐膳食长者数',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            }, {
                title: '盈利金额/元',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            },]
        }, {
            title: '幸福小站销售情况',
            dataIndex: 'account',
            key: 'account',
            children: [{
                title: '销售产品总数',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            }, {
                title: '销售总金额/元',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            },]
        }, {
            title: '活动费用情况',
            dataIndex: 'account',
            key: 'account',
            children: [{
                title: '收费活动数',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            }, {
                title: '活动总金额/元',
                dataIndex: 'rt_total_amout',
                key: 'rt_total_amout',
            },]
        },
    ];
    constructor(props: CostStatisticsViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addAllergyFile + '/' + contents.id);
        }
    }
    componentWillMount() {
        AppServiceUtility.service_record_service!.get_cost_statistics!({}, 1, 10)!
            .then((data: any) => {
                // console.log(data);
            });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.service_record_service.get_cost_statistics!(condition)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "统计时间段（周）": item.year_month,
                                "社区幸福院名称": item.org_name[0],
                                "日托订单-订单数量": item.rt_total_order_num,
                                "日托订单-订单总金额/元": item.rt_total_amout,
                                "长者膳食-午餐膳食长者数": item.rt_total_amout,
                                "长者膳食-午餐盈利金额/元": item.rt_total_amout,
                                "长者膳食-晚餐膳食长者数": item.rt_total_amout,
                                "长者膳食-晚餐盈利金额/元": item.rt_total_amout,
                                "幸福小站销售情况-销售产品总数": item.rt_total_amout,
                                "幸福小站销售情况-销售总金额/元": item.rt_total_amout,
                                "活动费用情况-收费活动数": item.rt_total_amout,
                                "活动费用情况-活动总金额/元": item.rt_total_amout,
                            });
                        });
                        exprot_excel([{ name: '费用统计', value: new_data }], '费用统计（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "time"
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: 'get_cost_statistics',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_allergy_file'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：费用统计视图控制器
 * @description 费用统计视图
 * @author
 */
@addon(' CostStatisticsView', '费用统计视图', '费用统计视图')
@reactControl(Form.create<any>()(CostStatisticsView), true)
export class CostStatisticsViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}