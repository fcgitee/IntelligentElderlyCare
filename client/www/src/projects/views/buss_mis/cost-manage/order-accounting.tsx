
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Modal, Form, Radio } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";

/** 状态：App工单核算视图 */
export interface OrderAccountingViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 控制弹框显示 */
    show?: any;
    condition?: any;
    /** 导出的条件 */
    select_type?: any;
}
/** 组件：App工单核算视图 */
export class OrderAccountingView extends React.Component<OrderAccountingViewControl, OrderAccountingViewState> {
    private columns_data_source = [{
        title: '账号',
        dataIndex: 'account',
        key: 'account',
    },
    {
        title: '长者姓名',
        dataIndex: 'elder_name',
        key: 'elder_name',
    },
    {
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    }, {
        title: '工单编号',
        dataIndex: 'record_code',
        key: 'record_code',
    },
    {
        title: '服务产品',
        dataIndex: 'product_name',
        key: 'product_name',
    },
    // {
    //     title: '服务类型',
    //     dataIndex: 'treatment',
    //     key: 'treatment',
    // },
    {
        title: '工单金额',
        dataIndex: 'valuation_amount',
        key: 'valuation_amount',
    },
    {
        title: '服务供应商',
        dataIndex: 'org_name',
        key: 'org_name',
    },
    {
        title: '预约时间',
        dataIndex: 'start_date',
        key: 'start_date',
    },
    {
        title: '工单状态',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: '支付方式',
        dataIndex: 'buy_type',
        key: 'buy_type',
    },
    ];
    constructor(props: OrderAccountingViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
            select_type: 1,
            condition: {},
            show: false,
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.setState({ show: true });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        // console.log(contents);
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.addAllergyFile + '/' + contents.id);
        }
    }
    // componentWillMount() {
    //     AppServiceUtility.service_record_service!.get_app_order_list!({}, 1, 10)!
    //         .then((data: any) => {
    //             // console.log(data);
    //         });
    // }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }

    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // 全部
        if (select_type === 1) {
            let condition = this.state.condition;
            AppServiceUtility.service_record_service.get_app_order_list!(condition)!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "账号": item.account[0],
                                "长者姓名": item.elder_name[0],
                                "订单编号": item.order_code[0],
                                "工单编号": item.record_code,
                                "服务产品": item.product_name[0],
                                "工单金额": item.valuation_amount,
                                "服务供应商": item.org_name[0],
                                "预约时间": item.start_date,
                                "工单状态": item.status,
                                "支付方式": item.buy_type[0],
                            });
                        });
                        exprot_excel([{ name: 'app工单核算', value: new_data }], 'app工单核算（汇总）', 'xls');
                    }
                });
        }
        this.setState({ 'show': false });
    }

    render() {
        const { getFieldDecorator } = this.props.form!;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "手机号/账号",
                    decorator_id: "account_phone",
                },
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "time"
                },
                {
                    type: InputType.input,
                    label: "支付方式",
                    decorator_id: "buy_type"
                },
            ],
            btn_props: [{
                label: '导出',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'download'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.service_record_service,
            service_option: {
                select: {
                    service_func: 'get_app_order_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_allergy_file'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <SignFrameLayout {...allowance_read_list} />
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                })(
                                    <Radio.Group defaultValue={1} onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio defaultChecked={true} key={1} value={1}>导出全部记录</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：App工单核算视图控制器
 * @description App工单核算视图
 * @author
 */
@addon(' OrderAccountingView', 'App工单核算视图', 'App工单核算视图')
@reactControl(Form.create<any>()(OrderAccountingView), true)
export class OrderAccountingViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}