import { Button, Card, Col, Form, Input, message, Row, TreeSelect } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：机构信息档案新增状态
 */
export interface ChangeOrgInfoRecoreViewState extends ReactViewState {
    /** 项目列表 */
    item_list?: any[];
    /** 项目列表集合 */
    all_item_list?: any[];
    /** 基础数据 */
    base_data?: {};
    org_info: any;
    all_service_product_list: any[];
    /** 服务商编号 */
    service_provider_list?: any[];
    administrative_division_list?: any;
}
export interface ChangeServiceOrderFormValues {
    id?: string;
}
/**
 * 组件：机构信息档案新增
 * 描述
 */
export class ChangeOrgInfoRecoreView extends ReactView<ChangeOrgInfoRecordViewControl, ChangeOrgInfoRecoreViewState> {
    /** 总项目列表 */
    constructor(props: any) {
        super(props);
        this.state = {
            item_list: [],
            org_info: {},
            all_service_product_list: [],
            all_item_list: [],
            base_data: {},
            service_provider_list: [],
            administrative_division_list: [],
        };
    }
    dropThis(value: any) {
        let all_service_product_list = this.state.all_service_product_list;

        let newList = [];

        for (let i in all_service_product_list) {
            if (all_service_product_list.hasOwnProperty(i) && value !== all_service_product_list[i]) {
                newList.push(all_service_product_list[i]);
            }
        }
        this.setState({
            all_service_product_list: newList,
        });
    }
    /** 提交方法 */
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: ChangeServiceOrderFormValues) => {
            let formValue = {};
            let str_zoom_list: any = [];
            // this.state.all_item_list!.map(() => {
            //     service_item.push({ service_options: [] });
            // });
            for (let key in values) {
                if (key.indexOf('___') !== -1) {
                    let keys = key.split('___');
                    if (keys[0] === 'admin_area_id' || keys[0] === 'street') {
                        str_zoom_list[keys[1]] = { ...str_zoom_list[keys[1]], [`${keys[0]}`]: values[key] };
                    }
                } else {
                    formValue[key] = values[key];
                }
            }
            formValue['org_info_record'] = str_zoom_list;
            if (this.props.match!.params.key) {
                formValue['id'] = this.props.match!.params.key;
            }
            formValue['state'] = false;
            AppServiceUtility.person_org_manage_service.update_org_info_record!(formValue)!
                .then((data) => {
                    if (data) {
                        message.info('保存成功');
                        this.props.history!.push(ROUTE_PATH.OrgInfoRecord);
                    }
                });
        });
    }

    addService = () => {
        let list: any[] | undefined = this.state.all_service_product_list;
        if (!list) {
            list = [];
        }
        list.push('');
        this.setState({
            all_service_product_list: list
        });
    }

    componentWillMount() {
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }

    componentDidMount() {
        // AppServiceUtility.services_project_service.get_services_project_list!({})!
        //     .then((data: any) => {
        //         if (data) {
        //             this.setState({
        //                 item_list: data['result']
        //             });
        //         }
        //     });
        // AppServiceUtility.service_operation_service.get_service_provider_list_all!({})!
        //     .then((data: any) => {
        //         if (data) {
        //             this.setState({
        //                 service_provider_list: data['result']
        //             });
        //         }
        //     });
        if (this.props.match!.params.key) {
            AppServiceUtility.person_org_manage_service.get_org_info_record_list!({ id: this.props.match!.params.key })!
                .then((data) => {
                    this.setState({
                        base_data: data!.result![0],
                        all_service_product_list: data!.result![0]['org_info_record'],
                    });
                });
        }

        AppServiceUtility.person_org_manage_service.get_current_org_info!()!
            .then((data) => {
                this.setState({
                    org_info: data[0],
                });
            });
    }
    /** 返回按钮 */
    retBtn = () => {
        this.props.history!.push(ROUTE_PATH.OrgInfoRecord);
    }
    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form!;
        // 如果服务项目不为空则生成
        let item_list;
        if (this.state.all_service_product_list!.length > 0) {
            item_list = this.state.all_service_product_list!.map((value, index) => {
                return (
                    <Card key={index}>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='服务区域'>
                                    {getFieldDecorator('admin_area_id___' + index, {
                                        initialValue: value.admin_area_id ? value.admin_area_id : '',
                                        // rules: [{
                                        //     required: false,
                                        //     // message: '请选择服务项目'
                                        // }],
                                    })(
                                        <TreeSelect
                                            showSearch={true}
                                            treeNodeFilterProp='title'
                                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                            allowClear={true}
                                            treeDefaultExpandAll={true}
                                            treeData={this.state.administrative_division_list}
                                            placeholder={"请选择行政区划"}
                                        />
                                        // <Cascader
                                        //     // defaultValue={['zhejiang', 'hangzhou', 'xihu']}
                                        //     options={city}
                                        //     placeholder={'请选择区域'}
                                        // // onChange={onChange}
                                        // />
                                    )}
                                </Form.Item>
                                <Form.Item label='镇街'>
                                    {getFieldDecorator('street___' + index, {
                                        initialValue: value.street ? value.street : '',
                                        rules: [{
                                            required: true,
                                            message: '请输入镇街'
                                        }],
                                    })(
                                        <Input />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={2}>
                                <Button onClick={() => this.dropThis(value)}>删除</Button>
                            </Col>
                        </Row>
                    </Card>
                );
            });
        }
        return (
            <MainContent>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <MainCard title='机构信息档案'>
                        <Col >
                            <Form.Item label='机构名称'>
                                {getFieldDecorator('org_name', {
                                    initialValue: this.state.org_info ? this.state.org_info.name : '',
                                    rules: [{
                                        required: false,
                                        message: '请输入机构名称'
                                    }],
                                })(
                                    <Input disabled={this.props.match!.params.key ? true : false} />
                                )}
                            </Form.Item>
                            <Form.Item label='服务点名称'>
                                {getFieldDecorator('ser_addr', {
                                    initialValue: this.state.base_data!['ser_addr'] ? this.state.base_data!['ser_addr'] : '',
                                    rules: [{
                                        required: false,
                                        message: '请输入入服务点名称'
                                    }],
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label='负责人'>
                                {getFieldDecorator('duty_person', {
                                    initialValue: this.state.base_data!['duty_person'] ? this.state.base_data!['duty_person'] : '',
                                    rules: [{
                                        required: false,
                                        message: '请输入负责人'
                                    }],
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label='联系电话'>
                                {getFieldDecorator('tel', {
                                    initialValue: this.state.base_data!['tel'] ? this.state.base_data!['tel'] : '',
                                    rules: [{
                                        required: false,
                                        message: '请输入联系电话'
                                    }],
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                        </Col>
                    </MainCard>
                    <MainCard title='服务区域'>
                        {item_list}
                        <Button type='primary' onClick={this.addService}>新增服务区域</Button>
                    </MainCard>
                    <Row type="flex" justify="center">
                        <Button htmlType='submit' type='primary'>保存</Button>
                        <Button type='ghost' name='返回' htmlType='button' onClick={this.retBtn}>返回</Button>
                    </Row>
                </Form>
            </MainContent>
        );
    }
}

/**
 * 控件：机构信息档案新增页面
 * 描述
 */
@addon('ChangeOrgInfoRecoreView', '机构信息档案新增页面', '描述')
@reactControl(Form.create<any>()(ChangeOrgInfoRecoreView), true)
export class ChangeOrgInfoRecordViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}