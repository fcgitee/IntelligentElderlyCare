import { Button, message } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：机构信息档案状态
 */
export interface OrgInfoRecordState extends ReactViewState {/** 选择行Ids */
    select_ids?: string[];
    request_ulr?: string;
    delete_modal_visible?: boolean;
    order_id?: string;
}

/**
 * 组件：机构信息档案信息
 * 描述
 */
export class OrgInfoRecord extends ReactView<OrgInfoRecordControl, OrgInfoRecordState> {
    private columns_data_source = [{
        title: '机构名称',
        dataIndex: 'org_name',
        key: 'org_name',
    }, {
        title: '服务点名称',
        dataIndex: 'ser_addr',
        key: 'ser_addr',
    }, {
        title: '负责人',
        dataIndex: 'duty_person',
        key: 'duty_person',
    }, {
        title: '联系电话',
        dataIndex: 'tel',
        key: 'tel',
    }, {
        title: '省/市/区(县)',
        dataIndex: 'admin_area_id',
        key: 'admin_area_id',
        render: (text: string, record: any, index: number) => {
            // let node = record.org_info_record.map((e: any, i: number) => {
            //     return <p key={i}>{e.ser_zoom_info.name}</p>;
            // });
            let node = record.ser_zoom_info.name.map((e: any, i: number) => {
                return <p key={i}>{e}</p>;
            });
            return node;
        }
    }, {
        title: '镇街',
        dataIndex: 'org_info_record',
        key: 'org_info_record.street',
        render: (text: string, record: any, index: number) => {
            let node = record.org_info_record.map((e: any, i: number) => {
                return <p key={i}>{e.street}</p>;
            });
            return node;
        }
    }, {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        render: (text: string, record: any, index: number) => {
            let node = record.state ? '启用' : '停用';
            return node;
        }
    },
    {
        title: '是否启用',
        dataIndex: 'state',
        key: 'state',
        render: (text: string, record: any, index: number) => {
            return (<Button onClick={() => { this.switchState(record.id, !record.state); }}>{record.state ? '停用' : '启用'}</Button>);
        }
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            delete_modal_visible: false,
            order_id: '',
        };
    }

    switchState(id: string, state: boolean) {
        AppServiceUtility.person_org_manage_service.update_org_info_record!({ id: id, state: state })!
            .then((data) => {
                if (data === 'Success') {
                    message.info('设置成功');
                    this.props.history!.push(ROUTE_PATH.OrgInfoRecord);
                }
            });
    }

    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.ChangeOrgInfoRecore);
    }

    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.ChangeOrgInfoRecore + '/' + contents.id);
        }
    }
    render() {
        let table_param = {
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };

        let service_provider = {
            type_show: false,
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            edit_permission: this.props.edit_permission,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
            },
            searchExtraParam: this.props.select_param || {},
            select_permission: this.props.select_permission,
        };
        let service_order_list = Object.assign(service_provider, table_param);
        return (
            <div>
                <SignFrameLayout {...service_order_list} />
            </div>

        );
    }
}

/**
 * 控件：机构信息档案
 * 描述
 */
@addon('OrgInfoRecord', '控件：机构信息档案', '描述')
@reactControl(OrgInfoRecord, true)
export class OrgInfoRecordControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 额外参数 */
    public select_param?: any;
}