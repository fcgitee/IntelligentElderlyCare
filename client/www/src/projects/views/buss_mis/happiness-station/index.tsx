import { Row, Form, Button, Input, message, Col, Card, Icon } from 'antd';
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param, beforeUpload } from 'src/projects/app/util-tool';
import { MainContent } from 'src/business/components/style-components/main-content';
import TextArea from 'antd/lib/input/TextArea';
import { request } from 'src/business/util_tool';
import { FileUploadBtn } from 'src/business/components/buss-components/file-operation-btn/file-upload-btn';
import { remote } from 'src/projects/remote';
import { formatDateFunc } from '../elder-meals/change-elder-meals';
import ReactEcharts from 'echarts-for-react';

class EditForm extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this);
        }
    }
    resetForm = () => {
        this.props.form.setFieldsValue({
            name: '',
            description: '',
            price: '',
            unit: '',
            last_week_sales: '',
            picture: [],
        });
    }
    handleFeedbackSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: any) => {
            if (!err) {
                let formValue = {};
                formValue['name'] = values['name'];
                formValue['price'] = values['price'];
                formValue['unit'] = values['unit'];
                formValue['description'] = values['description'];
                formValue['picture'] = values['picture'];
                // 上周销量
                if (values['last_week_sales']) {
                    let formatDate = this.props.lastWeekCfg;
                    formValue['week_sales'] = values['last_week_sales'];
                    formValue['week_key'] = formatDate['nowWeekKey'];
                }
                // 编辑
                if (this.props.data_info && this.props.data_info.id) {
                    formValue['id'] = this.props.data_info['id'];
                }
                request(this, AppServiceUtility.person_org_manage_service.update_happiness_station_product!(formValue))
                    .then((datas: any) => {
                        if (datas === 'Success') {
                            message.info('保存成功', 1, () => {
                                if (this.props.onOk) {
                                    // this.resetForm();
                                    this.props.onOk();
                                }
                            });
                        }
                    })
                    .catch((error: any) => {
                        // console.log(error);
                    });
            }
        });
    }
    getOption = () => {
        let sales: any = [],
            xdata: any = [];
        if (this.props.data_info && this.props.data_info.sales_info) {
            let sales_info = this.props.data_info.sales_info;
            // 由于后台拿出来是倒序的，所以这里要反过来
            for (let i = sales_info.length - 1; i >= 0; i--) {
                xdata.push(sales_info[i]['week_key']);
                sales.push(sales_info[i]['week_sales']);
            }
        }
        return {
            title: {
                text: ''
            },
            tooltip: {},
            legend: {},
            xAxis: {
                data: xdata
            },
            yAxis: {},
            series: [{
                name: '销量',
                type: 'line',
                data: sales,
                itemStyle: { normal: { label: { show: true } } }
            }]
        };
    }
    render() {
        let { getFieldDecorator } = this.props.form;
        let { data_info, formItemLayout } = this.props;
        return (
            <Row>
                <Form {...formItemLayout} onSubmit={this.handleFeedbackSubmit}>
                    <Row>
                        <Col span={12}>
                            <Form.Item label='产品名称：'>
                                {getFieldDecorator('name', {
                                    initialValue: data_info['name'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入产品名称'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入产品名称" />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label='上周销量：'>
                                {getFieldDecorator('last_week_sales', {
                                    initialValue: data_info['last_week_sales'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入上周销量'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入上周销量" />
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            <Form.Item label='产品单价：'>
                                {getFieldDecorator('price', {
                                    initialValue: data_info['price'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入产品单价'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入产品单价" />
                                )}
                            </Form.Item>
                            <Form.Item label='产品单位：'>
                                {getFieldDecorator('unit', {
                                    initialValue: data_info['unit'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入产品单位'
                                    }],
                                })(
                                    <Input autoComplete="off" placeholder="请输入产品单位" />
                                )}
                            </Form.Item>
                            <Form.Item label='产品介绍：'>
                                {getFieldDecorator('description', {
                                    initialValue: data_info['description'] || '',
                                    rules: [{
                                        required: true,
                                        message: '请输入产品介绍'
                                    }],
                                })(
                                    <TextArea rows={4} placeholder="请输入产品介绍" autoComplete="off" />
                                )}
                            </Form.Item>
                            <Form.Item label='产品图片（大小小于2M，格式支持jpg/jpeg/png）：'>
                                {getFieldDecorator('picture', {
                                    initialValue: data_info['picture'] || [],
                                    rules: [{
                                        required: true,
                                        message: '请上传产品图片'
                                    }],
                                })(
                                    <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <ReactEcharts option={this.getOption()} />
                        </Col>
                    </Row>
                    <Row type='flex' justify='center' className="ctrl-btns">
                        <Button htmlType='submit' type='primary'>保存</Button>
                        {/* &nbsp;
                        &nbsp;
                        <Button type='primary' onClick={this.resetForm}>重置</Button> */}
                    </Row>
                </Form>
            </Row>
        );
    }
}
const ChangeHappinessStationForm = Form.create<any>()(EditForm);

/**
 * 组件：幸福小站列表状态
 */
export interface HappinessStationViewState extends ReactViewState {
    columns_data_source?: any;
    data_info?: any;
    is_add_show?: any;
    add_permission?: boolean;
}

/**
 * 组件：幸福小站列表
 * 描述
 */
export class HappinessStationView extends ReactView<HappinessStationViewControl, HappinessStationViewState> {
    private formCreator: any = null;
    constructor(props: any) {
        super(props);
        this.state = {
            data_info: [],
            is_add_show: false,
            add_permission: false,
            columns_data_source: [
                {
                    title: '序号',
                    dataIndex: 'code',
                    key: 'code',
                },
                {
                    title: '图片',
                    dataIndex: 'picture',
                    key: 'picture',
                    render: (text: any, record: any) => {
                        if (text) {
                            return (
                                <img src={text} style={{ maxWidth: '100px', maxHeight: '100px' }} />
                            );
                        }
                        return text;
                    }
                }, {
                    title: '产品名称',
                    dataIndex: 'name',
                    key: 'name',
                }, {
                    title: '产品单价',
                    dataIndex: 'price',
                    key: 'price',
                }, {
                    title: '产品单位',
                    dataIndex: 'unit',
                    key: 'unit',
                }, {
                    title: '上周销量',
                    dataIndex: 'last_week_sales',
                    key: 'last_week_sales',
                }, {
                    title: '产品介绍',
                    dataIndex: 'description',
                    key: 'description',
                }, {
                    title: '最近编辑时间',
                    dataIndex: 'modify_date',
                    key: 'modify_date',
                },
            ],
        };
    }
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({
                is_add_show: true,
                data_info: contents,
            });
        }
    }
    componentDidMount() {
        request(this, AppServiceUtility.person_org_manage_service.check_permission2!({ f: '幸福小站', p: '新增' })!)
            .then((datas: any) => {
                if (datas === true) {
                    this.setState({
                        add_permission: datas,
                    });
                }
            });
    }
    toggleAdd = () => {
        this.setState(
            {
                data_info: [],
                is_add_show: !this.state.is_add_show
            }
        );
    }
    onRef = (e: any) => {
        this.formCreator = e;
    }
    onOk = () => {
        this.formCreator && this.formCreator.reflash();
        this.setState({
            is_add_show: false,
        });
    }
    render() {
        let lastWeekCfg = formatDateFunc(new Date(new Date().getTime() - 86400 * 7 * 1000));
        let param: any = {
            week_key: lastWeekCfg['nowWeekKey'],
        };
        let happiness_station = {
            type_show: false,
            edit_form_items_props: [
            ],
            columns_data_source: this.state.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_happiness_station_product_list_all',
                    service_condition: [param, 1, 10]
                },
                delete: {
                    service_func: 'delete_happiness_station_product'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            searchExtraParam: param,
            onRef: this.onRef,
        };
        let happiness_station_list = Object.assign(happiness_station, table_param);
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };
        let { is_add_show, add_permission, data_info } = this.state;
        // 要求展开
        let is_icon_show: boolean = true;
        if (!add_permission) {
            // 没有新增权限
            if (data_info && !data_info.id) {
                is_icon_show = false;
            }
        }
        return (
            <Row>
                {is_add_show ? <MainContent>
                    <Card title={data_info && data_info.id ? '编辑单个产品信息' : '新增单个产品信息'}>
                        <ChangeHappinessStationForm formItemLayout={formItemLayout} data_info={data_info} onOk={this.onOk} lastWeekCfg={lastWeekCfg} />
                    </Card>
                </MainContent> : null}
                <MainContent>
                    <Card title='小站内产品列表' extra={is_icon_show ? <Icon type={this.state.is_add_show ? 'minus-circle' : 'plus-circle'} onClick={this.toggleAdd} /> : null}>
                        <SignFrameLayout {...happiness_station_list} />
                    </Card>
                </MainContent>
            </Row>
        );
    }
}

/**
 * 控件：幸福小站列表
 * 描述
 */
@addon('HappinessStationView', '幸福小站列表', '描述')
@reactControl(Form.create<any>()(HappinessStationView), true)
export class HappinessStationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}