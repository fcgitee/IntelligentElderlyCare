
import { addon } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Button, Table, DatePicker, Modal, Form, Radio, Select, Row, Col, message } from "antd";
import { AppServiceUtility } from 'src/projects/app/appService';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
const Option = Select.Option;
const { MonthPicker } = DatePicker;
import moment from 'moment';
import { request } from "src/business/util_tool";
import { exprot_excel } from "src/business/util_tool";
import { IntelligentElderlyCareAppStorage } from "src/projects/app/appStorage";
import './index.less';
let columns_total: any = [
    {
        title: "社区",
        dataIndex: 'social_worker',
        key: 'social_worker',
        width: 100,
    },
    {
        title: '结算月份',
        dataIndex: 'year_month',
        key: 'year_month',
        width: 150,
    },
    {
        title: '服务商个数',
        dataIndex: 'service_provider_quantity',
        key: 'service_provider_quantity',
        width: 100,
    },
    {
        title: '工单总数',
        dataIndex: 'order_quantity',
        key: 'phoneNum',
        width: 150,
    },
    {
        title: '账户类型',
        dataIndex: 'buy_type',
        key: 'buy_type',
        width: 100,
    },
    // {
    //     title: '合计金额',
    //     dataIndex: 'valuation_amount_total',
    //     key: 'valuation_amount_total',
    //     width: 100,
    // },
    {
        title: '账户合计',
        dataIndex: 'subsidy_amount_total',
        key: 'subsidy_amount_total',
        width: 100,
    },
    {
        title: '区级负担(50%)',
        dataIndex: 'district_burden',
        key: 'district_burden',
        width: 100,
    },
    {
        title: '镇街负担(50%)',
        dataIndex: 'town_burden',
        key: 'town_burden',
        width: 100,
    },
];

/** 政府补贴打印用table列 */
let bt_print_columns: any = [
    {
        title: "序号",
        dataIndex: 'index_no',
        key: 'index_no',
        align: 'center',
        width: 50,
        render: (text: any, record: any, index: number) => {
            if (record.service_provider_name !== '合计') {
                return index + 1;
            } else {
                return '';
            }
        }
    },
    {
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
        align: 'center',
        width: 100,
    },
    {
        title: '工单数',
        dataIndex: 'order_quantity',
        key: 'order_quantity',
        align: 'center',
        width: 150,
    },
    {
        title: '账户类型',
        dataIndex: 'buy_type',
        key: 'buy_type',
        align: 'center',
        width: 100,
    },
    {
        title: '合计金额(元)',
        dataIndex: 'subsidy_amount_total',
        key: 'subsidy_amount_total',
        align: 'center',
        width: 100,
    },
    {
        title: '区级负担(50%)',
        dataIndex: 'district_burden',
        key: 'district_burden',
        align: 'center',
        width: 100,
    },
    {
        title: '镇街负担(50%)',
        dataIndex: 'town_burden',
        key: 'town_burden',
        align: 'center',
        width: 100,
    },
];
/** 慈善/自费打印用table列 */
let print_columns: any = [
    {
        title: "序号",
        dataIndex: 'index_no',
        key: 'index_no',
        align: 'center',
        width: 50,
        render: (text: any, record: any, index: number) => {
            return index + 1;
        }
    },
    {
        title: '服务商',
        dataIndex: 'service_provider_name',
        key: 'service_provider_name',
        align: 'center',
        width: 100,
    },
    {
        title: '工单数',
        dataIndex: 'order_quantity',
        key: 'order_quantity',
        align: 'center',
        width: 150,
    },
    {
        title: '账户类型',
        dataIndex: 'buy_type',
        key: 'buy_type',
        align: 'center',
        width: 100,
    },
    {
        title: '合计金额(元)',
        dataIndex: 'subsidy_amount_total',
        key: 'subsidy_amount_total',
        align: 'center',
        width: 100,
    }
];
/** 状态：镇街结算组件数据 */
export interface townStreetSettlementViewState extends ReactViewState {
    /** 合计数据 */
    dataTotalSource: any[];
    /** 明细数据 */
    dataSource: any[];
    /** 日期条件 */
    query_month?: any;
    /** 控制弹框显示 */
    show?: any;
    /** 导出的条件 */
    select_type?: any;
    /** 服务商列表 */
    org_list: any[];
    /** 选择的服务商Id */
    org_id: string;
    /** 社工局数组 */
    social_worker_list?: any[];
    /** 所选社工局 */
    social_worker_id?: string;
    /** 所选社工局中文 */
    social_worker_name?: string;
    /** 所选账户类型 */
    buy_type?: string;
    /** 打印状态 */
    preview_modal?: boolean;
    /** 打印内容数据 */
    data_source_print?: any[];
    /** 发送按钮权限 */
    is_send_btn?: boolean;
    /** 确认权限按钮 */
    is_confirm_btn?: boolean;
    /** 结算状态 */
    accounting_confirm?: string;
    // 控制重复点击
    is_send?: boolean;
    /** 发送结算提示框 */
    model_fs_show?: boolean;
    /** 确认结算提示框 */
    model_qr_show?: boolean;
    /** 弹框操作的服务商id */
    op_service_provider_id?: string;
    /** 弹框操作的年月id */
    op_year_month?: string;
    /** 弹框操作的购买类型id */
    op_buy_type?: string;
    /** 弹框操作的服务产品id */
    op_service_product_id?: string;
    /** 弹框操作的社工局id */
    op_social_worker_id?: string;

}
/** 组件：镇街结算组件视图 */
export class townStreetSettlementView extends React.Component<townStreetSettlementViewControl, townStreetSettlementViewState> {
    columns: any = [
        {
            title: "社区",
            dataIndex: 'social_worker',
            key: 'social_worker',
            width: 100,
        },
        {
            title: '服务商',
            dataIndex: 'service_provider_name',
            key: 'service_provider_name',
            width: 100,
        },
        {
            title: '服务项目',
            dataIndex: 'service_product_name',
            key: 'service_product_name',
            width: 100,
        },
        {
            title: '结算月份',
            dataIndex: 'year_month',
            key: 'year_month',
            width: 150,
        },
        {
            title: '工单数',
            dataIndex: 'order_quantity',
            key: 'order_quantity',
            width: 150,
        },
        {
            title: '账户类型',
            dataIndex: 'buy_type',
            key: 'buy_type',
            width: 100,
        },
        {
            title: '账户合计',
            dataIndex: 'subsidy_amount_total',
            key: 'subsidy_amount_total',
            width: 100,
        },
        {
            title: '区级负担(50%)',
            dataIndex: 'district_burden',
            key: 'district_burden',
            width: 100,
        },
        {
            title: '镇街负担(50%)',
            dataIndex: 'town_burden',
            key: 'town_burden',
            width: 100,
        },
        {
            title: '结算状态',
            dataIndex: 'accounting_confirm',
            key: 'accounting_confirm',
            width: 100,
            render: (text: any, record: any, index: number) => {
                if (text === '待结算') {
                    if (this.state.is_send_btn) {
                        return (<span><span>{text}</span><Button style={{ marginLeft: '10px' }} className='right-table-button-type' key={index} onClick={() => { this.operation_fs(record['service_provider_id'], record['year_month'], record['buy_type'], record['service_product_id'], record['social_worker_id']); }} >发送结算</Button></span>);
                    } else {
                        return text;
                    }
                }
                if (text === '确认中') {
                    let org_id = IntelligentElderlyCareAppStorage.getCurrentUser().org_id;
                    if (this.state.is_confirm_btn && record['service_provider_id'] && record['service_provider_id'].length > 0 && org_id === record['service_provider_id'][0]) {
                        return (<span><span>{text}</span><Button style={{ marginLeft: '10px' }} className='right-table-button-type' key={index} onClick={() => { this.operation_qr(record['service_provider_id'], record['year_month'], record['buy_type'], record['service_product_id'], record['social_worker_id']); }} >结算确认</Button></span>);
                    } else {
                        return text;
                    }
                }
                if (text === '已结算') {
                    return text;
                }
            }
        }
    ];
    constructor(props: townStreetSettlementViewControl) {
        super(props);
        this.state = {
            dataTotalSource: [],
            dataSource: [],
            select_type: 1,
            show: false,
            query_month: moment().format('YYYY/MM'),
            org_list: [],
            org_id: '',
            social_worker_list: [],
            social_worker_id: '',
            social_worker_name: '',
            buy_type: '补贴账户',
            preview_modal: false,
            is_send_btn: false,
            is_confirm_btn: false,
            // accounting_confirm:'待结算',
            data_source_print: [],
            is_send: false,
            model_fs_show: false,
            model_qr_show: false,
        };
    }
    operation_fs = (service_provider_id: string, year_month: string, buy_type: string, service_product_id: string, social_worker_id: string) => {
        this.setState({
            op_service_provider_id: service_provider_id,
            op_year_month: year_month,
            op_buy_type: buy_type,
            op_service_product_id: service_product_id,
            op_social_worker_id: social_worker_id,
            model_fs_show: true
        });
    }
    operation_qr = (service_provider_id: string, year_month: string, buy_type: string, service_product_id: string, social_worker_id: string) => {
        this.setState({
            op_service_provider_id: service_provider_id,
            op_year_month: year_month,
            op_buy_type: buy_type,
            op_service_product_id: service_product_id,
            op_social_worker_id: social_worker_id,
            model_qr_show: true
        });
    }
    handleFsCancel = () => {
        this.setState({
            model_fs_show: false
        });
    }
    handleQrCancel = () => {
        this.setState({
            model_qr_show: false
        });
    }
    // 待确认发送方法
    confirm_send = () => {
        let service_provider_id = this.state.op_service_provider_id;
        let year_month = this.state.op_year_month;
        let buy_type = this.state.op_buy_type;
        let service_product_id = this.state.op_service_product_id;
        let social_worker_id = this.state.op_social_worker_id;
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                if (service_provider_id && service_provider_id.length > 0 && year_month && buy_type && service_product_id && social_worker_id) {
                    request(this, AppServiceUtility.report_management.confirm_send!(service_provider_id[0], year_month, buy_type, service_product_id, social_worker_id))!
                        .then((data: any) => {
                            if (data === 'Success') {
                                message.info('发送成功，待服务商确认！');
                                this.search();
                                // this.select_func({ 'month': this.state.query_month, 'org_id': this.state.org_id, 'social_worker_id': this.state.social_worker_id, 'buy_type': this.state.buy_type });

                            } else {
                                message.info('发送失败，请联系管理员！');
                            }
                            this.setState({
                                is_send: false,
                                model_fs_show: false
                            });
                        })
                        .catch((error: any) => {
                            this.setState({
                                is_send: false,
                                model_fs_show: false
                            });
                            console.error(error);
                        });
                }
            }
        );
    }
    // 服务商核算确认方法
    accounting_confirm = () => {
        let service_provider_id = this.state.op_service_provider_id;
        let year_month = this.state.op_year_month;
        let buy_type = this.state.op_buy_type;
        let service_product_id = this.state.op_service_product_id;
        let social_worker_id = this.state.op_social_worker_id;
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                if (service_provider_id && service_provider_id.length > 0 && year_month && buy_type && service_product_id && social_worker_id) {
                    request(this, AppServiceUtility.report_management.accounting_confirm!(service_provider_id[0], year_month, buy_type, service_product_id, social_worker_id))!
                        .then((data: any) => {
                            if (data === 'Success') {
                                message.info('确认成功！');
                                this.search();
                                // this.select_func({ 'month': this.state.query_month, 'org_id': this.state.org_id, 'social_worker_id': this.state.social_worker_id, buy_type: this.state.buy_type });
                            } else {
                                message.info('确认失败，请联系管理员！');
                            }
                            this.setState({
                                is_send: false,
                                model_qr_show: false
                            });
                        })
                        .catch((error: any) => {
                            this.setState({
                                is_send: false,
                                model_qr_show: false
                            });
                            console.error(error);
                        });
                }
            }
        );
    }
    // 导出类型选择
    onChangeRadio(value: any) {
        // 全部
        if (value === 1) {
            this.setState({
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                select_type: value
            });
        }
    }
    /** 查询方法 */
    search = () => {
        let condition = {};
        if (this.state.org_id) {
            condition['org_id'] = this.state.org_id;
        }
        if (this.state.query_month) {
            condition['month'] = this.state.query_month;
        }
        if (this.state.social_worker_id) {
            condition['social_worker_id'] = this.state.social_worker_id;
        }
        if (this.state.buy_type) {
            condition['buy_type'] = this.state.buy_type;
        }
        if (this.state.accounting_confirm) {
            condition['accounting_confirm'] = this.state.accounting_confirm;
        }
        this.select_func(condition);
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    /** 打印 */
    print = () => {
        let { query_month, org_id, social_worker_id, buy_type, accounting_confirm } = this.state;
        if (query_month && accounting_confirm) {
            let dy_condition = { 'month': query_month, 'accounting_confirm': accounting_confirm, 'no_service_product': true };
            if (org_id) {
                dy_condition['org_id'] = org_id;
            }
            if (social_worker_id) {
                dy_condition['social_worker_id'] = social_worker_id;
            }
            if (buy_type) {
                dy_condition['buy_type'] = buy_type;
            }
            request(this, AppServiceUtility.report_management.get_service_provider_settlement!(dy_condition))!
                .then((res: any) => {
                    this.setState(
                        {
                            preview_modal: true,
                            data_source_print: res.result
                        },
                        () => {
                            setTimeout(
                                () => {
                                    document.body.style.visibility = "hidden";
                                    if (document.querySelector('#root')) {
                                        document.querySelector('#root')!['style'].height = "0%";
                                    }
                                    if (document.querySelector('.ant-modal-wrap')) {
                                        document.querySelector('.ant-modal-wrap')!.className = 'ant-modal-wrap2';
                                    }
                                    // document.body.innerHTML = document.querySelector('.hideAll')!.innerHTML;
                                    window.print();
                                    document.body.style.visibility = "";
                                    this.setState({
                                        preview_modal: false,
                                    });
                                    if (document.querySelector('#root')) {
                                        document.querySelector('#root')!['style'].height = "100%";
                                    }
                                    if (document.querySelector('.ant-modal-wrap2')) {
                                        document.querySelector('.ant-modal-wrap2')!.className = 'ant-modal-wrap';
                                    }

                                    // let bdhtml = window.document.body.innerHTML;
                                    // var jubuData = document.querySelector('.table-detail')!.innerHTML;
                                    // // 把获取的 局部div内容赋给body标签, 相当于重置了 body里的内容
                                    // window.document.body.innerHTML = jubuData;
                                    // // 调用打印功能
                                    // window.print();
                                    // window.document.body.innerHTML = bdhtml;// 重新给页面内容赋值；

                                    // 判断iframe是否存在，不存在则创建iframe
                                    // var iframe = document.getElementById("print-iframe");
                                    // if (!iframe) {
                                    //     var el = document.querySelector('.table-detail');
                                    //     el!.setAttribute('style', 'visibility:visible;');
                                    //     iframe = document.createElement('IFRAME');
                                    //     var doc = null;
                                    //     iframe.setAttribute("id", "print-iframe");
                                    //     iframe.setAttribute('style', 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;');
                                    //     document.body.appendChild(iframe);
                                    //     doc = iframe['contentWindow'].document;
                                    //     // 这里可以自定义样式
                                    //     // doc.write("<LINK rel="stylesheet" type="text/css" href="css/print.css">");
                                    //     // console.log(el!.innerHTML);
                                    //     doc.write(el!.innerHTML);
                                    //     doc.close();
                                    //     iframe['contentWindow'].focus();
                                    // }
                                    // iframe['contentWindow'].print();
                                    // if (navigator.userAgent.indexOf("MSIE") > 0) {
                                    //     document.body.removeChild(iframe);
                                    // }
                                },
                                300
                            );
                        }
                    );
                });
        } else {
            message.info('结算状态、日期都需要选择！');
        }
    }
    // 下载
    async download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // let export_data: any = [];
        // 全部
        if (select_type === 1) {
            let condition = {};
            if (this.state.query_month) {
                condition['month'] = this.state.query_month;
            }
            if (this.state.org_id) {
                condition['org_id'] = this.state.org_id;
            }
            if (this.state.social_worker_id) {
                condition['social_worker_id'] = this.state.social_worker_id;
            }
            if (this.state.buy_type) {
                condition['buy_type'] = this.state.buy_type;
            }
            if (this.state.accounting_confirm) {
                condition['accounting_confirm'] = this.state.accounting_confirm;
            }
            let new_data: any = [];
            let new_total_data: any = [];
            await request(this, AppServiceUtility.report_management.get_service_provider_settlement_total!(condition))!
                .then((data: any) => {
                    if (data['result']) {
                        data['result'].map((item: any) => {
                            new_total_data.push({
                                "结算月份": item.year_month,
                                "服务商个数": item.service_provider_quantity,
                                "工单总数": item.order_quantity,
                                "合计金额": item.valuation_amount_total,
                                "补贴账户合计": item.subsidy_amount_total,
                                "区级负担(50%)": item.district_burden,
                                "镇街负担(50%)": item.town_burden,
                            });
                        });
                    }
                });
            await request(this, AppServiceUtility.report_management.get_service_provider_settlement!(condition))!
                .then((data: any) => {
                    if (data['result']) {
                        data['result'].map((item: any) => {
                            new_data.push({
                                "服务商": item.service_provider_name ? (item.service_provider_name === '合计' ? item.service_provider_name : item.service_provider_name[0]) : '',
                                "服务项目": item.service_product_name ? item.service_product_name[0] : '',
                                "结算月份": item.year_month,
                                "工单数": item.order_quantity,
                                "补贴账户": item.subsidy_amount_total,
                                "区级负担(50%)": item.district_burden,
                                "镇街负担(50%)": item.town_burden,
                                "结算状态": item.accounting_confirm,
                            });
                        });
                        exprot_excel([{ name: '服务商结算报表（汇总）', value: new_total_data }, { name: '服务商结算报表（明细）', value: new_data }], data['result'][0].year_month + '月服务商结算', 'xls');
                    }
                });

        }
        this.setState({ 'show': false });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    componentWillMount() {
        (async () => {
            // 查询社工局列表
            await request(this, AppServiceUtility.person_org_manage_service.get_organization_all_list!({ 'personnel_category': '街道' }))!
                .then((data: any) => {
                    this.setState({
                        social_worker_list: data!.result!,
                        social_worker_id: data!.result!.length > 0 ? data!.result![0]['id'] : '',
                        social_worker_name: data!.result!.length > 0 ? data!.result![0]['name'] : '',
                        query_month: moment().format('YYYY/MM')
                    });
                })
                .catch(err => {
                    console.info(err);
                });
            // 查询服务商列表
            request(this, AppServiceUtility.person_org_manage_service.get_organization_list!({ 'personnel_category': '服务商', 'contract_status': '签约' }))!
                .then((data: any) => {
                    this.setState({
                        org_list: data!.result!,
                        org_id: data!.result!.length > 0 ? data!.result![0]['id'] : ''
                    });
                })
                .catch(err => {
                    console.info(err);
                });
            // this.select_func({ 'month': moment().format('YYYY/MM'), 'org_id': this.state.org_id, 'social_worker_id': this.state.social_worker_id, buy_type: this.state.buy_type });
        })();
        // 获取权限列表
        request(this, AppServiceUtility.login_service.get_function_list())!
            .then((data: any) => {
                if (data.length > 0) {
                    // console.log('data>>>>>', data);
                    let is_send_btn = false, is_confirm_btn = false;
                    data.forEach((value: any) => {
                        if (value.function + value.permission === '结算报表发送确认编辑') {
                            is_send_btn = true;
                        }
                        if (value.function + value.permission === '服务商结算确认编辑') {
                            is_confirm_btn = true;
                        }
                    });
                    this.setState({ is_send_btn, is_confirm_btn });
                }
            })
            .catch(err => {
                console.info(err);
            });
    }

    /**
     * 查询方法
     */
    private select_func = (condition: {}) => {
        request(this, AppServiceUtility.report_management.get_service_provider_settlement_total!(condition))!
            .then((res: any) => {
                this.setState({
                    dataTotalSource: res.result
                });
            });
        request(this, AppServiceUtility.report_management.get_service_provider_settlement!(condition))!
            .then((res: any) => {
                this.setState({
                    dataSource: res.result
                });
            });
    }
    /** 机构树形选择框改变事件 */
    onChangeTree = (value: any) => {
        this.setState({
            org_id: value
        });
        // let condition = { 'org_id': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // if (this.state.accounting_confirm) {
        //     condition['accounting_confirm'] = this.state.accounting_confirm;
        // }
        // this.select_func(condition);
    }
    /** 社工局下拉框改变事件 */
    onChangeSelect = (value: any, option: any) => {
        this.setState({
            social_worker_id: value,
            social_worker_name: option && option.props ? option.props.children : ''
        });
        // let condition = { 'social_worker_id': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // if (this.state.accounting_confirm) {
        //     condition['accounting_confirm'] = this.state.accounting_confirm;
        // }
        // this.select_func(condition);
    }
    /** 结算状态选择框改变事件 */
    onChangeAccountingConfirm = (value: any) => {
        this.setState({
            accounting_confirm: value
        });
        // let condition = { 'accounting_confirm': value };
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // this.select_func(condition);
    }
    /** 账户类型选择框改变事件 */
    onChangeBuyType = (value: any) => {
        this.setState({
            buy_type: value
        });
        // let condition = { 'buy_type': value };
        // if (this.state.query_month) {
        //     condition['month'] = this.state.query_month;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.accounting_confirm) {
        //     condition['accounting_confirm'] = this.state.accounting_confirm;
        // }
        // this.select_func(condition);
    }
    /** 日期范围回调 */
    time_change = (date: any, dateString: string) => {
        this.setState({
            query_month: dateString
        });
        // let condition = { 'month': dateString };
        // if (this.state.org_id) {
        //     condition['org_id'] = this.state.org_id;
        // }
        // if (this.state.social_worker_id) {
        //     condition['social_worker_id'] = this.state.social_worker_id;
        // }
        // if (this.state.buy_type) {
        //     condition['buy_type'] = this.state.buy_type;
        // }
        // if (this.state.accounting_confirm) {
        //     condition['accounting_confirm'] = this.state.accounting_confirm;
        // }
        // this.select_func(condition);
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        const { preview_modal, query_month, social_worker_name, buy_type } = this.state;
        return (
            <MainContent>
                <MainCard title='服务商结算' style={{ position: 'relative' }}>
                    <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                        <Button type="primary" onClick={this.export} style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                        <Button type="primary" onClick={this.print} style={{ float: 'right', marginRight: '3px' }}>打印</Button>
                        <Button type="primary" onClick={this.search} style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                        {/* <Button type="primary" style={{ float: 'right', marginRight: '3px' }}>查询</Button> */}
                        <div style={{ float: 'right', marginRight: '3px' }}>
                            <MonthPicker onChange={this.time_change} defaultValue={moment(moment(), 'YYYY/MM')} format={'YYYY/MM'} />
                        </div>
                        <div style={{ float: 'right', marginRight: '10px' }}>
                            <Select
                                showSearch={true}
                                allowClear={true}
                                onChange={this.onChangeAccountingConfirm}
                                placeholder={"请选择结算状态"}
                                style={{
                                    width: '12em'
                                }}
                                value={this.state.accounting_confirm}
                            >
                                {['待结算', '确认中', '已结算'].map((item: any) => <Option key={item} value={item}>{item}</Option>)}
                            </Select>
                        </div>
                        <div style={{ float: 'right', marginRight: '10px' }}>
                            <Select
                                showSearch={true}
                                // allowClear={true}
                                onChange={this.onChangeBuyType}
                                placeholder={"请选择账户类型"}
                                style={{
                                    width: '12em'
                                }}
                                value={this.state.buy_type}
                            >
                                {['自费', '补贴账户', '慈善账户'].map((item: any) => <Option key={item} value={item}>{item}</Option>)}
                            </Select>
                        </div>
                        <div style={{ float: 'right', marginRight: '10px' }}>
                            <Select
                                showSearch={true}
                                allowClear={true}
                                onChange={this.onChangeTree}
                                placeholder={"请选择服务商"}
                                style={{
                                    width: '16em'
                                }}
                                value={this.state.org_id}
                            >
                                {this.state.org_list!.length > 0 ? this.state.org_list!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>) : null}
                            </Select>
                        </div>
                        <div style={{ float: 'right', marginRight: '10px' }}>
                            <Select
                                showSearch={true}
                                allowClear={true}
                                onChange={this.onChangeSelect}
                                placeholder={"请选择社工局"}
                                style={{
                                    width: '12em'
                                }}
                                value={this.state.social_worker_id}
                            >
                                {this.state.social_worker_list!.length > 0 ? this.state.social_worker_list!.map((item: any) => <Option key={item.id} value={item.id}>{item.name}</Option>) : null}
                            </Select>
                        </div>
                    </div>
                    <p style={{ fontSize: '24px', fontWeight: 'bold' }}> 合计</p>
                    <Table columns={columns_total} dataSource={this.state.dataTotalSource} pagination={{ pageSize: 10 }} bordered={true} size="middle" />
                    <p style={{ fontSize: '24px', fontWeight: 'bold', marginTop: '20px' }}> 明细</p>
                    <Table columns={this.columns} dataSource={this.state.dataSource} pagination={{ pageSize: 10 }} bordered={true} size="middle" />
                </MainCard>
                <Modal
                    title="打印预览"
                    width="100%"
                    visible={preview_modal}
                    okText={'打印'}
                    // onOk={previewing ? () => this.print() : () => this.set()}
                    // onCancel={() => this.handleCancel()}
                    className={'hideAll'}
                >
                    <Row>
                        <Col>
                            <div style={{ fontSize: '20px', fontWeight: 'bold', textAlign: 'center', margin: '0 auto' }}>南海区智慧养老综合服务管理平台</div>
                        </Col>
                        <Col>
                            <div style={{ fontSize: '20px', fontWeight: 'bold', textAlign: 'center', margin: '0 auto' }}>服务汇总表</div>
                        </Col>
                        <Col style={{ marginTop: '10px' }}>
                            <div style={{ fontSize: '12px', textAlign: 'center', margin: '0 auto' }}>日期:{query_month}</div>
                        </Col>
                        <Col style={{ margin: '10px 0px 10px 0px' }}>
                            <div style={{ fontSize: '16px', fontWeight: 'bold', }}>所属镇街:{social_worker_name}</div>
                        </Col>
                    </Row>
                    <Table className='table-detail' pagination={false} columns={buy_type === '补贴账户' ? bt_print_columns : print_columns} dataSource={this.state.data_source_print} bordered={true} size="middle" />
                    <Row>
                        <Col>
                            <div style={{ fontSize: '10px', fontWeight: 'bold', }}>注：以上数据已经各服务商核对确认。</div>
                        </Col>
                        <Col>
                            <p style={{ fontSize: '12px', display: 'inline' }}>经办人:</p><p style={{ fontSize: '12px', marginLeft: '100px', display: 'inline' }}>审核:</p>
                        </Col>
                        <Col>
                            <div style={{ fontSize: '12px', fontWeight: 'bold', textAlign: 'center', paddingLeft: '205px' }}>平台运营方： 广东壹佰健大健康科技有限公司</div>
                        </Col>
                        <Col>
                            <div style={{ fontSize: '12px', textAlign: 'center' }}>盖章：</div>
                        </Col>
                        <Col>
                            <div style={{ fontSize: '12px', textAlign: 'center', marginTop: '15px', paddingLeft: '85px' }}>打印日期：{moment().format('YYYY/MM/DD')}</div>
                        </Col>
                    </Row>
                </Modal>
                <Modal
                    title="请选择导出类型"
                    visible={this.state.show}
                    onOk={() => this.download()}
                    onCancel={() => this.onCancel({ show: !this.state.show })}
                    okText="下载"
                    cancelText="取消"
                >
                    <MainCard>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('isAll', {
                                    initialValue: ''
                                })(
                                    <Radio.Group onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                        <Radio key={1} value={1}>导出数据</Radio>
                                    </Radio.Group>
                                )}
                            </Form.Item>
                        </Form>
                    </MainCard >
                </Modal>
                <Modal
                    title="温馨提示"
                    visible={this.state.model_fs_show}
                    onOk={this.confirm_send}
                    onCancel={this.handleFsCancel}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <div style={{ textAlign: 'center' }}>确定要发送给服务商确认结算吗？</div>
                </Modal>
                <Modal
                    title="温馨提示"
                    visible={this.state.model_qr_show}
                    onOk={this.accounting_confirm}
                    onCancel={this.handleQrCancel}
                    okButtonProps={{ disabled: this.state.is_send }}
                    cancelButtonProps={{ disabled: this.state.is_send }}
                >
                    <div style={{ textAlign: 'center' }}>该条结算记录确定无误吗？</div>
                </Modal>
            </MainContent>
        );
    }
}

/**
 * 控件：镇街结算组件控制器
 * @description 镇街结算组件
 * @author
 */
@addon('townStreetSettlementView', '镇街结算组件', '镇街结算组件')
@reactControl(Form.create<any>()(townStreetSettlementView), true)
export class townStreetSettlementViewControl extends ReactViewControl {
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
}