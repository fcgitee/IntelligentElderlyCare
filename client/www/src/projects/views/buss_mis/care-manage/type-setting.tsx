
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { Input, Button, Row, Col, Icon, Form, message } from 'antd';
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';

/** 状态：长者关怀类型设置视图 */
export interface CareTypeSettingViewState extends ReactViewState {
    show?: any;
    type?: any;
    remark?: any;
    is_send?: boolean;
}
/** 组件：长者关怀类型设置视图 */
export class CareTypeSettingView extends React.Component<CareTypeSettingViewControl, CareTypeSettingViewState> {
    private columns_data_source = [{
        title: '关怀类型名称',
        dataIndex: 'type',
        key: 'type',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    },];
    constructor(props: CareTypeSettingViewControl) {
        super(props);
        this.state = {
            show: false,
            type: '',
            remark: '',
            is_send: false,
        };
    }
    componentDidMount = () => {
        if (this.props.match!.params.key !== undefined) {
            this.setState({
                show: true
            });
            AppServiceUtility.family_files_service.get_care_type_list!({ id: this.props.match!.params.key })!
                .then((data: any) => {
                    this.setState({
                        type: data.result[0].type,
                        remark: data.result[0].remark
                    });
                });
        }
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.setState({
                show: true
            });
            this.props.history!.push(ROUTE_PATH.careTypeSetting + '/' + contents.id);
        }
    }

    iconClick = () => {
        let show = this.state.show;
        this.setState({
            show: !show
        });
    }

    // 取消
    cancle = () => {
        this.setState({
            show: false
        });
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        if (this.state.is_send === true) {
            message.info('请勿重复操作！');
            return;
        }
        this.setState(
            {
                is_send: true,
            },
            () => {
                this.props.form.validateFields((err: any, values: any) => {
                    if (this.props.match!.params.key) {
                        values = { ...values, "id": this.props.match!.params.key };
                    }
                    AppServiceUtility.family_files_service.update_care_type!(values)!
                        .then(data => {
                            if (data === 'Success') {
                                message.success('添加成功！', 1, () => {
                                    this.props.history!.push(ROUTE_PATH.careTypeSetting);
                                });
                            } else {
                                message.error('添加失败！');
                                this.setState({
                                    is_send: false
                                });
                                return;
                            }
                        })
                        .catch(error => {
                            message.error(error.message);
                            this.setState({
                                is_send: false
                            });
                        });
                });
            }
        );
    }
    nameChange = (e: any) => {
        this.setState({
            type: e.target.value
        });
    }

    remarkChange = (e: any) => {
        this.setState({
            remark: e.target.value
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.family_files_service,
            service_option: {
                select: {
                    service_func: 'get_care_type_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_care_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            delete_permission: this.props.delete_permission,
            add_permission: this.props.add_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <div style={{ position: 'relative', backgroundColor: '#ffffff', height: this.state.show === true ? '120px' : '60px', margin: '36px 36px 0px 36px', paddingTop: '20px' }}>
                    {this.state.show === true ?
                        <div>
                            <Form onSubmit={this.handleSubmit}>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('type', {
                                                initialValue: this.state.type ? this.state.type : '',
                                            })(
                                                <div>关怀类型名称：<Input value={this.state.type} onChange={this.nameChange} style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={16}>
                                        <Form.Item>
                                            {getFieldDecorator('remark', {
                                                initialValue: this.state.remark ? this.state.remark : '',
                                            })(

                                                <div>备注：<Input value={this.state.remark} onChange={this.remarkChange} style={{ width: '400px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <div style={{ paddingLeft: '45%' }}>
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }} disabled={this.state.is_send}>保存</Button>
                                    <Button disabled={this.state.is_send} onClick={this.cancle}>取消</Button>
                                </div>
                            </Form>
                        </div>
                        :
                        ''
                    }
                    {this.state.show === false ?
                        <div onClick={this.iconClick} style={{ backgroundColor: '#ffffff', width: '50px', height: '50px', position: 'absolute', top: '10px', right: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon type='plus' />
                        </div> : ''
                    }
                </div>
                <SignFrameLayout {...allowance_read_list} />
            </div >
        );
    }
}

/**
 * 控件：长者关怀类型设置视图控制器
 * @description 长者关怀类型设置视图
 * @author
 */
@addon(' CareTypeSettingView', '长者关怀类型设置视图', '长者关怀类型设置视图')
@reactControl(Form.create<any>()(CareTypeSettingView), true)
export class CareTypeSettingViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}