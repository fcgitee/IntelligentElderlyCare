
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { Button } from 'antd';
// import { ROUTE_PATH } from 'src/projects/router';
/** 状态：长者关怀记录视图 */
export interface ElderCareRecordViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
}
/** 组件：长者关怀记录视图 */
export class ElderCareRecordView extends React.Component<ElderCareRecordViewControl, ElderCareRecordViewState> {
    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'concerned_name',
        key: 'concerned_name',
    },
    {
        title: '性别',
        dataIndex: 'concerned_sex',
        key: 'concerned_sex',
    },
    {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    }, {
        title: '手机号码',
        dataIndex: 'concerned_telephone',
        key: 'concerned_telephone',
    },
    {
        title: '关怀信息',
        dataIndex: 'specifications',
        key: 'specifications',
        render: (text: any, record: any, index: number) => (
            <span>
                <span style={{ marginRight: '10px' }}>{record.year + '年' + record.current_month + '月' + record.current_day + '日'}</span>
                <span>{record.main_name + '关注了您的' + record.care_type_name + '情况'}</span>
            </span>
        )
    },
    {
        title: '操作',
        dataIndex: 'manufactor',
        key: 'manufactor',
        render: (text: any, record: any) => (
            <Button type="primary">查看</Button>
        ),
    },
    ];
    constructor(props: ElderCareRecordViewControl) {
        super(props);
        this.state = {
            request_ulr: '',
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        // this.props.history!.push(ROUTE_PATH.addElderCareRecord);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            // this.props.history!.push(ROUTE_PATH.addElderCareRecord + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        AppServiceUtility.family_files_service.get_care_record!({})!
            .then((data: any) => {
                // console.log('记录', data);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.family_files_service,
            service_option: {
                select: {
                    service_func: 'get_care_record',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_medicine_file'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <SignFrameLayout {...allowance_read_list} />
        );
    }
}

/**
 * 控件：长者关怀记录视图控制器
 * @description 长者关怀记录视图
 * @author
 */
@addon('ElderCareRecordView', '长者关怀记录视图', '长者关怀记录视图')
@reactControl(ElderCareRecordView, true)
export class ElderCareRecordViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}