import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Button } from "antd";
/**
 * 组件：平台运营-运营机构状态
 */
export interface PlatformOrganizationListViewState extends ReactViewState {
    org_id?: any;
    is_edit_btn?: any;
}

/**
 * 组件：平台运营-运营机构视图
 */
export class PlatformOrganizationListView extends ReactView<PlatformOrganizationListViewControl, PlatformOrganizationListViewState> {
    private columns_data_source = [
        {
            title: '机构名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '服务类型',
            dataIndex: 'organization_info.personnel_category',
            key: 'organization_info.personnel_category',
        },
        {
            title: '简介',
            dataIndex: 'organization_info.describe',
            key: 'organization_info.describe',
        },
        {
            title: '操作',
            dataIndex: '',
            key: '',
            render: (text: any, record: any) => (
                <div>
                    {
                        this.state.is_edit_btn ?
                            <Button style={{ marginRight: '5px' }} type="primary" onClick={() => { this.edit(record); }}>编辑</Button>

                            : ''
                    }
                </div>
            ),
        }];
    private Table: any;
    constructor(props: PlatformOrganizationListViewControl) {
        super(props);
        this.state = {
            org_id: '',
            is_edit_btn: false,
        };
    }

    /** 编辑 */
    edit = (record: any) => {
        this.props.history!.push(ROUTE_PATH.changeOrganization + '/' + record.id);
    }

    componentWillMount = () => {
        // 获取当前登陆人信息
        AppServiceUtility.person_org_manage_service.get_current_user_info!()!
            .then((data: any) => {
                // console.log('当前登陆', data);
                this.setState(
                    {
                        org_id: data[0]['organization_id']
                    },
                    () => {
                        this.Table.reflash();
                    }
                );
            });

        // 权限
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        }
                    });
                }
            });
    }

    onRef = (ref: any) => {
        this.Table = ref;
    }

    render() {
        let service_item = {
            type_show: false,
            columns_data_source: this.columns_data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_organization_list',
                    service_condition: [{ id: this.state.org_id }, 1, 1]
                },
                delete: {
                    service_func: 'delete_service_situation_byid'
                }
            },
            onRef: this.onRef,
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <div>
                <SignFrameLayout {...service_item_list} />
            </div>
        );
    }
}

/**
 * 控件：个案服务情况列表控件
 * @description 个案服务情况列表控件
 * @author
 */
@addon('PlatformOrganizationListView', '个案服务情况列表控件', '个案服务情况列表控件')
@reactControl(PlatformOrganizationListView, true)
export class PlatformOrganizationListViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}