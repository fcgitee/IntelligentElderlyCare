import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState, ReactView } from "pao-aop-client";
import { MainContent } from "src/business/components/style-components/main-content";
import { isPermission } from "src/projects/app/permission";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';
let { Option } = Select;
/**
 * 状态：社会群体类型新增页面
 */
export interface ChangeSocialGroupsTypeViewState extends ReactViewState {
}

/**
 * 组件：社会群体类型新增页面视图
 */
export class ChangeSocialGroupsTypeView extends ReactView<ChangeSocialGroupsTypeViewControl, ChangeSocialGroupsTypeViewState> {
    constructor(props: ChangeSocialGroupsTypeViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const type_list: JSX.Element[] = ['标准类型', '自定义类型'].map((item, idx) => <Option key={item}>{item}</Option>);
        let edit_props = {
            form_items_props: [{
                title: "社会群体类型信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "类型名称",
                        decorator_id: "name",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入类型名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入类型名称"
                        }
                    }, {
                        type: InputType.select,
                        label: "类型",
                        decorator_id: "type",
                        field_decorator_option: {
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请选择类型",
                            childrens: type_list,
                        }
                    }, {
                        type: InputType.text_area,
                        label: "描述",
                        decorator_id: "describe",
                        field_decorator_option: {
                            // rules: [{ required: true, message: "请输入描述" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入描述",
                            row: 3,
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    // console.log("返回回调");
                    this.props.history!.push(ROUTE_PATH.socialGroupsType);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.social_groups_type_service,
                operation_option: {
                    query: {
                        func_name: "get_social_groups_type_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_social_groups_type"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.socialGroupsType); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：社会群体类型新增页面控件
 * @description 社会群体类型新增页面控件
 * @author
 */
@addon('ChangeSocialGroupsTypeViewControl', '社会群体类型新增页面控件', '社会群体类型新增页面控件')
@reactControl(ChangeSocialGroupsTypeView, true)
export class ChangeSocialGroupsTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}