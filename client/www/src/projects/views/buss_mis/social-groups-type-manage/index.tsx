import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import { ROUTE_PATH } from "src/projects/router";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { Select } from 'antd';
let { Option } = Select;

/**
 * 组件：社会群体类型列表状态
 */
export interface SocialGroupsTypeViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：社会群体类型列表
 * 描述
 */
export class SocialGroupsTypeView extends ReactView<SocialGroupsTypeViewControl, SocialGroupsTypeViewState> {
    private columns_data_source = [{
        title: '类型名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '类型',
        dataIndex: 'type',
        key: 'type',
    }, {
        title: '描述',
        dataIndex: 'describe',
        key: 'describe',
    }];
    constructor(props: SocialGroupsTypeViewControl) {
        super(props);
        this.state = {
            request_url:'',
        };
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeSocialGroupsType);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeSocialGroupsType + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    componentDidMount() {
    }
    render() {
        const type_list: JSX.Element[] = ['标准类型', '自定义类型'].map((item, idx) => <Option key={item}>{item}</Option>);
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "社会群体类型名称",
                decorator_id: "name",
                option: {
                    placeholder: "请输入社会群体类型名称",
                }
            }, {
                type: InputType.select,
                label: "类型",
                decorator_id: "type",
                option: {
                    placeholder: "请选择类型",
                    childrens: type_list,
                }
            }],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.social_groups_type_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: []
                },
                delete: {
                    service_func: 'del_social_groups_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 社会群体类型列表页面
 */
@addon('SocialGroupsTypeView', '社会群体类型列表页面', '描述')
@reactControl(SocialGroupsTypeView, true)
export class SocialGroupsTypeViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 