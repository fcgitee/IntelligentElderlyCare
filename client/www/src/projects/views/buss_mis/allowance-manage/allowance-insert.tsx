import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState, CookieUtil } from "pao-aop-client";
import React from "react";
import { Button, Row, Select, Radio, Checkbox, Input, Modal, message } from "antd";
import Form, { WrappedFormUtils } from "antd/lib/form/Form";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from 'src/projects/app/appService';
// import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from 'src/projects/router';
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { remote } from "src/projects/remote";
import { beforeUpload } from "src/projects/app/util-tool";
import { User } from "src/business/models/user";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
const { TextArea } = Input;
const { Option } = Select;
/**
 * 组件：补贴申请录入状态
 */
export interface AllowanceInsertViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
    /** 长者列表 */
    elder_list?: any[];
    /** 模板列表 */
    template_list?: any[];
    /** 长者 */
    elder?: string;
    /** 模板 */
    template?: string;
    /*当前用户 */
    user_info?: any;
    /*申请表里的选项和值 */
    project?: any;
    /*单选按钮的值 */
    radioValue?: any;
    /*多行文本的值 */
    textAreaValue?: any;
    name?: any;
    id_card?: any;
    checkValue?: any;
    checked?: any;
    selectValue?: any;
    CheckValue?: any;
    disabled?: any;
    template_id?: any;
    file?: any;
    visible?: any;
    Readvisible?: any;
    timeOutvisible?: any;
    allowance_type?: any;
    ideaData?: any;
    ideaShowState?: any;
    idea_show?: any;
    info_type?: any;
    showState?: any;
}
export interface CompetenceAssessmentListFormValues {
    id?: string;
}
/**
 * 组件：补贴申请录入视图
 */
export default class AllowanceInsertView extends ReactView<AllowanceInsertViewControl, AllowanceInsertViewState> {
    // private columns_data_source = [{
    //     title: '编号',
    //     dataIndex: 'code',
    //     key: 'code',
    // }, {
    //     title: '姓名',
    //     dataIndex: 'name',
    //     key: 'name',
    // }, {
    //     title: '身份证号',
    //     dataIndex: 'id_card',
    //     key: 'id_card',
    // }, {
    //     title: '性别',
    //     dataIndex: 'sex',
    //     key: 'sex',
    // }, {
    //     title: '出生年月',
    //     dataIndex: 'birth_date',
    //     key: 'birth_date',
    // }];
    constructor(props: any) {
        super(props);
        this.state = {
            user_info: {},
            elder_list: [],
            template_list: [],
            project_list: [],
            elder: '',
            template: '',
            project: [],
            radioValue: [],
            textAreaValue: [],
            name: '',
            id_card: '',
            checked: true,
            selectValue: [],
            CheckValue: [],
            disabled: false,
            template_id: '',
            file: [],
            visible: false,
            Readvisible: false,
            timeOutvisible: false,
            allowance_type: '',
            ideaData: [],
            ideaShowState: false,
            idea_show: [],
            info_type: '',
            showState: false
        };
    }
    /** 提交方法 */
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: CompetenceAssessmentListFormValues) => {
            // // console.log("限制住了吗？？？？？？？", err);
            // let list: any = {
            //     name: this.state.name,
            //     id_card: this.state.id_card,
            //     Operator: values['Operator'],
            //     elder: values['elder'],
            //     template: values['template'],
            //     project_list: [...this.state.project,...this.state.radioValue,...this.state.textAreaValue,...this.state.CheckValue,...this.state.selectValue],
            // };
            // // console.log("list",list);
            // // console.log(values, 'fddfsdfsdf?>>>>>>');
            if (err! === null) {
                let obj: any = {}, project_list = [];
                let list = [];
                for (let key in values) {
                    if (key.indexOf('___') > 0) {
                        if (key.split('___').length > 2) {
                            list = key.split('___');
                            let radio_list = [];
                            radio_list = list[1].split(';');
                            let new_radio_list: any = [];
                            radio_list.forEach((item: any) => {
                                new_radio_list.push(JSON.parse(item));
                            });
                            let project = {
                                ass_info: {
                                    project_name: list[0],
                                    dataSource: new_radio_list,
                                    selete_type: list[2],
                                    value: values[key]
                                }
                            };
                            project_list.push(project);
                        } else {
                            let project = {
                                ass_info: {
                                    project_name: key.split('___')[0],
                                    selete_type: key.split('___')[1],
                                    value: values[key]
                                }
                            };
                            project_list.push(project);
                        }

                    } else {
                        obj[key] = values[key];
                    }
                }
                obj['project_list'] = project_list;
                // // console.log("obj", obj);
                if (this.state.name !== '' && this.state.name !== null && this.state.id_card !== '' && this.state.id_card !== null) {
                    AppServiceUtility.allowance_service.update_allowance!(obj)!
                        .then((data: any) => {
                            if (data === 'Success') {
                                // // console.log(data);
                                message.info('保存成功', 1, () => {
                                    this.props.history!.push(ROUTE_PATH.allowanceRead);
                                });
                            } else {
                                message.error(data);
                            }
                            // if (data === '同一个人每次只能申请一次同类型的补贴') {
                            //     message.error('申请失败！！！同一个人每次只能申请一次同类型的补贴。', 5);
                            //     this.props.history!.push(ROUTE_PATH.allowanceRead);
                            // }
                        });
                }
            }
            // } else if (this.state.name !== '' && this.state.name !== null && this.state.id_card !== '' && this.state.id_card !== null) {
            //     AppServiceUtility.allowance_service.update_allowance!(list)!
            //             .then((data:any) => {
            //                 if (data) {
            //                     message.info('保存成功');
            //                     // this.props.history!.push(ROUTE_PATH.allowanceRead);
            //                 }
            //             });
            // }
        });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.allowanceRead);
    }
    componentDidMount() {
        // // console.log(this.props.match!.params.key);
        // var dayNow = new Date();
        // // console.log(dayNow);
        // var dayBegin = new Date(dayNow.getFullYear() + '-' + '11-1');
        // var dayEnd = new Date(dayNow.getFullYear() + 1 + '-' + '11-18');
        // // console.log(dayBegin);
        // // console.log(dayNow >= dayBegin);
        // if (dayNow > dayEnd || dayNow < dayBegin) {
        //     this.setState({
        //         timeOutvisible: true
        //     });
        // } else {
        if (this.props.match!.params.key === undefined) {
            this.setState({
                Readvisible: true
            });
            // }
        }
        // AppServiceUtility.person_org_manage_service.get_current_user_info!()
        //     .then((data: any) => {
        //         // console.log("乐园>>>>>>>>", data);
        //         if (data) {
        //             this.setState({
        //                 user_info: data[0],
        //             });
        //         }
        //     });
        AppServiceUtility.person_org_manage_service.get_personnel_worker!({})!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        elder_list: data.result
                    });
                }

            });
        AppServiceUtility.allowance_service.get_allowance_define_list!({})!
            .then((data: any) => {
                // // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        template_list: data.result
                    });
                }
            });
        if (this.props.match!.params.key) {
            AppServiceUtility.allowance_service.get_allowance_list_all!({ id: this.props.match!.params.key })!
                .then((data: any) => {
                    let id = '';
                    // // console.log("当前申请的数据>>>>>>>>>>>>", data);
                    this.setState({
                        info_type: data.result[0].allowance_type
                    });
                    // 拿到当前申请的id去过程表里查询每一步的相关信息
                    AppServiceUtility.allowance_service.get_reviewed_idea!({ 'business_id': data.result[0].id })!
                        .then((datas: any) => {
                            // // console.log("过程表的数据", datas);
                            this.setState({
                                ideaData: datas.result,
                                ideaShowState: true
                            });
                        });
                    if (data.result.length > 0) {
                        if (data.result[0].status !== "打回") {
                            this.setState({
                                disabled: true
                            });
                        }
                        // // console.log("1", data);
                        id = data.result[0].template;
                    }
                    AppServiceUtility.allowance_service.get_allowance_define_list!({ id })!
                        .then((data: any) => {
                            // // console.log("2", data);
                            if (data.result.length > 0) {
                                // // console.log("测试>>>>>>>>>>>>", data);
                                if (data.result[0].hasOwnProperty("id")) {
                                    this.setState({
                                        template_id: data.result[0].id,
                                        template: data.result[0].name
                                    });
                                }
                            }
                        });
                    this.setState({
                        name: data.result[0].name,
                        id_card: data.result[0].id_card,
                        project_list: data.result[0].project_list
                    });
                });
        }
    }
    /** 选择模板选择框值改变 */
    template_change = (id: any) => {
        // console.info(id);
        AppServiceUtility.allowance_service.get_allowance_define_list!({ id })!
            .then((data: any) => {
                if (data.result.length > 0) {
                    this.setState({
                        template_id: data.result[0].template_id,
                        file: data.result[0].file ? data.result[0].file : []
                    });
                    AppServiceUtility.assessment_template_service.get_assessment_template_list!({ id: data.result[0].template_id })!
                        .then((data: any) => {
                            // console.info("模板", data);
                            if (data.result.length > 0) {
                                this.setState({
                                    project_list: data.result
                                });
                            }
                        });
                }
            });
    }
    /** 返回按钮 */
    retLisit = () => {
        this.props.history!.push(ROUTE_PATH.allowanceRead);
    }
    /*获取input框里面的值 */
    projectChange = (e: any, option_content: any, type: any) => {
        let project = this.state.project;
        let i = 0;
        let projectList = [];
        for (i = 0; i < project.length; i++) {
            projectList.push(project[i]["ass_info"]['project_name']);
        }
        // // console.log(projectList);
        if (projectList.includes(option_content)) {
            project.map((item: any, idx: any) => {
                if (item["ass_info"]["project_name"] === option_content) {
                    item["ass_info"]["value"] = e.target.value;
                }
            });
        } else {
            let ass_info = {};
            ass_info["project_name"] = option_content;
            ass_info["value"] = e.target.value;
            ass_info['selete_type'] = type;
            project.push({ ass_info });
            this.setState({
                project
            });
        }
        // // console.log("input", this.state.project);
    }
    /*获取单选按钮的值 */
    radioChange = (e: any, option_content: any, type: any, other: any) => {
        let radioValue = this.state.radioValue;
        let i = 0;
        let radioValueList = [];
        for (i = 0; i < radioValue.length; i++) {
            radioValueList.push(radioValue[i]["ass_info"]['project_name']);
        }
        // // console.log(radioValueList);
        if (radioValueList.includes(option_content)) {
            radioValue.map((item: any, idx: any) => {
                if (item["ass_info"]["project_name"] === option_content) {
                    item["ass_info"]["value"] = e.target.value;
                }
            });
        } else {
            let ass_info = {};
            ass_info["project_name"] = option_content;
            ass_info["value"] = e.target.value;
            ass_info["dataSource"] = other;
            ass_info['selete_type'] = type;
            radioValue.push({ ass_info });
            this.setState({
                radioValue
            });
        }
        // // console.log("单选", this.state.radioValue);
    }
    textAreaChange = (e: any, option_content: any, type: any) => {
        let textAreaValue = this.state.textAreaValue;
        let i = 0;
        let textAreaValueList = [];
        for (i = 0; i < textAreaValue.length; i++) {
            textAreaValueList.push(textAreaValue[i]["ass_info"]['project_name']);
        }
        // // console.log(textAreaValueList);
        if (textAreaValueList.includes(option_content)) {
            textAreaValue.map((item: any, idx: any) => {
                if (item["ass_info"]["project_name"] === option_content) {
                    item["ass_info"]["value"] = e.target.value;
                }
            });
        } else {
            let ass_info = {};
            ass_info["project_name"] = option_content;
            ass_info["value"] = e.target.value;
            ass_info['selete_type'] = type;
            textAreaValue.push({ ass_info });
            this.setState({
                textAreaValue
            });
        }
        // // console.log(this.state.textAreaValue);
    }
    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }
    idCardChange = (e: any) => {
        this.setState({
            id_card: e.target.value
        });
    }
    allowanceTypeChange = (e: any) => {
        this.setState({
            allowance_type: e.target.value
        });
    }
    allowanceTypeShowChange = (e: any) => {
        this.setState({
            showState: e.target.value
        });
    }
    CheckChange = (checkedValues: any, option_content: any, type: any, other: any) => {
        let CheckValue = this.state.CheckValue;
        let i = 0;
        let CheckValueList = [];
        for (i = 0; i < CheckValue.length; i++) {
            CheckValueList.push(CheckValue[i]["ass_info"]['project_name']);
        }
        // // console.log(CheckValueList);
        if (CheckValueList.includes(option_content)) {
            CheckValue.map((item: any, idx: any) => {
                if (item["ass_info"]["project_name"] === option_content) {
                    item["ass_info"]["value"] = checkedValues;
                }
            });
        } else {
            let ass_info = {};
            ass_info["project_name"] = option_content;
            ass_info["value"] = checkedValues;
            ass_info["dataSource"] = other;
            ass_info['selete_type'] = type;
            CheckValue.push({ ass_info });
            this.setState({
                CheckValue
            });
        }
        // // console.log("单选", this.state.CheckValue);
    }
    selectChange = (value: any, option_content: any, type: any, other: any) => {
        let selectValue = this.state.selectValue;
        let i = 0;
        let selectValueList = [];
        for (i = 0; i < selectValue.length; i++) {
            selectValueList.push(selectValue[i]["ass_info"]['project_name']);
        }
        // console.log(selectValueList);
        if (selectValueList.includes(option_content)) {
            selectValue.map((item: any, idx: any) => {
                if (item["ass_info"]["project_name"] === option_content) {
                    item["ass_info"]["value"] = value;
                }
            });
        } else {
            let ass_info = {};
            ass_info["project_name"] = option_content;
            ass_info["value"] = value;
            ass_info["dataSource"] = other;
            ass_info['selete_type'] = type;
            selectValue.push({ ass_info });
            this.setState({
                selectValue
            });
        }
        // // console.log("单选", this.state.selectValue);
    }
    /*查看文件 */
    readFile = () => {
        this.setState({
            visible: true,
        });
    }
    handleOk = (e: any) => {
        // // console.log(e);
        this.setState({
            Readvisible: false,
        });
    }
    timeOut = () => {
        this.props.history!.push(ROUTE_PATH.allowanceRead);
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        const { getFieldDecorator } = this.props.form!;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        // let modal_search_items_props = {
        //     edit_form_items_props: [
        //         {
        //             type: ListInputType.input,
        //             label: "长者名称",
        //             decorator_id: "name"
        //         },
        //     ],
        //     columns_data_source: this.columns_data_source,
        //     service_name: AppServiceUtility.personnel_service,
        //     service_func: 'get_personnel_list',
        //     title: '长者查询',
        //     select_option: {
        //         placeholder: "请选择长者",
        //     }
        // };
        let template = this.state.template_list!.map((key, value) => {
            return <Select.Option key={key.id} value={key.id}>{key.name}</Select.Option>;
        });

        let project: any = [];
        this.state.project_list!.forEach((item, index) => {
            if (item.hasOwnProperty("ass_info")) {
                if (item.ass_info.hasOwnProperty("selete_type")) {
                    if (item.ass_info.selete_type === "1") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            // // console.log(radio_list, "a-----------------------------------");
                            // // // console.log(item.ass_info.project_name + '___' + JSON.stringify(item.ass_info.dataSource) + '___1','dataS>>>>>>>>>>>>>');
                            // let dd = {'key':item.ass_info.dataSource};
                            // let d = item.ass_info.project_name + '___' + JSON.stringify(dd) + '___1';
                            // // console.log(a,'dataS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                            item_content = (
                                <Form.Item label={item.ass_info.project_name}>
                                    {getFieldDecorator(item.ass_info.project_name + "___" + radio_list + "___1", {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Radio.Group disabled={this.state.disabled} onChange={(e) => this.radioChange(e, item.ass_info.project_name, item.ass_info.selete_type, item.ass_info.dataSource)} value={this.state.radioValue}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Radio value={i.serial_number} key={idx}>{i.option_content}</Radio>;
                                            })}
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "2") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name} >
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___2', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Checkbox.Group disabled={this.state.disabled} onChange={(checkedValues) => this.CheckChange(checkedValues, item.ass_info.project_name, item.ass_info.selete_type, item.ass_info.dataSource)}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Checkbox key={idx} value={i.serial_number}>{i.option_content}</Checkbox>;
                                            })}
                                        </Checkbox.Group>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "3") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name}>
                                {getFieldDecorator(item.ass_info.project_name + '___3', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <Input disabled={this.state.disabled} value={this.state.project} onChange={(e) => this.projectChange(e, item.ass_info.project_name, item.ass_info.selete_type)} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "4") {
                        let radio_list = '';
                        let item_content: any;
                        if (item.ass_info.hasOwnProperty("dataSource")) {
                            item.ass_info.dataSource.forEach((item: any) => {
                                radio_list = radio_list + JSON.stringify(item) + ";";
                            });
                            radio_list = radio_list.slice(0, radio_list.length - 1);
                            item_content = (
                                <Form.Item label={item.ass_info.project_name}>
                                    {getFieldDecorator(item.ass_info.project_name + '___' + radio_list + '___4', {
                                        initialValue: item.ass_info.value ? item.ass_info.value : '',
                                        rules: [{
                                            required: true,
                                            message: '请选择' + item.ass_info.project_name,
                                        }],
                                    })(
                                        <Select disabled={this.state.disabled} defaultValue={item.ass_info.dataSource[0].name} style={{ width: 120 }} onChange={(value: any) => this.selectChange(value, item.ass_info.project_name, item.ass_info.selete_type, item.ass_info.dataSource)}>
                                            {item.ass_info.dataSource!.map((i: any, idx: any) => {
                                                return <Option key={idx} value={i.serial_number}>{i.option_content}</Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            );
                        }
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "5") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name}>
                                {getFieldDecorator(item.ass_info.project_name + '___5', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: true,
                                        message: '请输入' + item.ass_info.project_name,
                                    }],
                                })(
                                    <TextArea
                                        disabled={this.state.disabled}
                                        value={this.state.textAreaValue}
                                        onChange={(e) => this.textAreaChange(e, item.ass_info.project_name, item.ass_info.selete_type)}
                                        placeholder="请输入"
                                        autosize={{ minRows: 3, maxRows: 5 }}
                                    />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                        return project;
                    }
                    if (item.ass_info.selete_type === "6") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'} >
                                {getFieldDecorator(item.ass_info.project_name + '___6', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: false,
                                    }],
                                })(
                                    <FileUploadBtn disabled={this.state.disabled} list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                    } else if (item.ass_info.selete_type === "7") {
                        const item_content = (
                            <Form.Item label={item.ass_info.project_name + '（大小小于2M，格式支持jpg/png/doc/docx/excel）'}>
                                {getFieldDecorator(item.ass_info.project_name + '___7', {
                                    initialValue: item.ass_info.value ? item.ass_info.value : '',
                                    rules: [{
                                        required: false,
                                    }],
                                })(
                                    <FileUploadBtn disabled={this.state.disabled} contents={"button"} action={remote.upload_url} />
                                )}
                            </Form.Item>
                        );
                        project.push(item_content);
                    }
                }
            } else {
                return true;
            }
        });
        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <MainContent>
                        <MainCard title='补贴申请'>
                            <Form.Item label='当前操作人'>
                                {getFieldDecorator('Operator', {
                                    initialValue: CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : 'admin',
                                    rules: [{
                                        required: true,
                                    }],
                                })(<span style={{ fontSize: '18px', fontWeight: 'bold' }}>{CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : 'admin'}</span>)}
                            </Form.Item>
                            <Form.Item label='姓名'>
                                {getFieldDecorator('name', {
                                    initialValue: this.state.name ? this.state.name : '',
                                    rules: [{
                                        required: true,
                                        message: '姓名'
                                    }],
                                })(
                                    <Input value={this.state.name} onChange={this.nameChange} disabled={this.state.disabled} />

                                )}
                            </Form.Item>
                            <Form.Item label='身份证'>
                                {getFieldDecorator('id_card', {
                                    initialValue: this.state.id_card ? this.state.id_card : '',
                                    rules: [{
                                        required: true,
                                        message: '身份证'
                                    }],
                                })(
                                    <Input value={this.state.id_card} onChange={this.idCardChange} disabled={this.state.disabled} />

                                )}
                            </Form.Item>
                            <Form.Item label='申请补贴类型'>
                                {getFieldDecorator('allowance_type', {
                                    initialValue: this.state.allowance_type ? this.state.allowance_type : '',
                                    rules: [{
                                        required: true,
                                        message: '申请补贴类型'
                                    }],
                                })(
                                    <Radio.Group defaultValue={'就业补贴'} onChange={this.allowanceTypeChange} value={this.state.allowance_type} disabled={this.state.disabled}>
                                        <Radio value={'就业补贴'}>就业补贴</Radio>
                                        <Radio value={'岗位补贴'}>岗位补贴</Radio>
                                        <Radio value={'养老补贴A类'}>养老补贴A类</Radio>
                                        <Radio value={'养老补贴B类'}>养老补贴B类</Radio>
                                        <Radio value={'养老补贴C类'}>养老补贴C类</Radio>
                                    </Radio.Group>

                                )}
                            </Form.Item>
                            <Form.Item label='选择模板'>
                                {getFieldDecorator('template', {
                                    initialValue: this.state.template ? this.state.template : '',
                                    rules: [{
                                        required: true,
                                        message: '请选择模板'
                                    }],
                                })(
                                    <Select showSearch={true} onChange={this.template_change} disabled={this.state.disabled}>
                                        {template}
                                    </Select>
                                )}
                            </Form.Item>
                            {/* 渲染模板 */}
                            {project}
                            {/* 渲染审核意见 */}
                            {/* {this.state.ideaShowState === true ?
                                this.state.info_type.indexOf("养老补贴") !== -1 ?
                                    <MainCard title='审核意见'>
                                        <Form.Item label='村审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[0].opinion.length > 0 ? this.state.ideaData[0].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                        <Form.Item label='镇审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[1].opinion.length > 0 ? this.state.ideaData[1].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                        <Form.Item label='区审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[2].opinion.length > 0 ? this.state.ideaData[2].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                    </MainCard>
                                    :
                                    <MainCard title='审核意见'>
                                        <Form.Item label='镇审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[0].opinion.length > 0 ? this.state.ideaData[0].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                        <Form.Item label='平台审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[1].opinion.length > 0 ? this.state.ideaData[1].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                        <Form.Item label='区审核意见'>
                                            <TextArea
                                                disabled={this.state.disabled}
                                                value={this.state.ideaData[2].opinion.length > 0 ? this.state.ideaData[2].opinion[0] : "等待审核中，还未有审批意见"}
                                            />
                                        </Form.Item>
                                    </MainCard>
                                :
                                ''
                            } */}
                            {this.state.disabled ? "" :
                                <Row type='flex' justify='center'>
                                    <Button htmlType='submit' type='primary'>保存</Button>&nbsp;&nbsp;&nbsp;
                            <Button type='ghost' name='返回' htmlType='button' onClick={() => this.props.history!.push(ROUTE_PATH.allowanceRead)}>返回</Button>&nbsp;&nbsp;&nbsp;
                            {this.state.file.length > 0 ? <Button type='primary' icon="search" onClick={this.readFile}>查看文件</Button> : ""}
                                </Row>
                            }
                        </MainCard>
                    </MainContent>
                </Form>
                <Modal title="文件资料 （大小小于2M，格式支持jpg/png/doc/docx/excel）" visible={this.state.visible} onOk={() => this.setState({ visible: false })} onCancel={() => this.setState({ visible: false })} width={'800px'}>
                    <FileUploadBtn value={this.state.file} disabled={true} contents={"button"} />
                </Modal>
                <Modal
                    title="补贴申请相关条款"
                    visible={this.state.Readvisible}
                    onOk={this.handleOk}
                    onCancel={this.handleOk}
                    width={1000}
                    okText={"确认"}
                    cancelText={'已阅读'}
                >
                    <h2>就业补贴</h2>
                    <p>申请人申请就业补贴应当符合以下条件：</p>
                    <p>（一）与养老机构签订劳动合同，且在该养老机构中从事护理等养老服务一线工作满 3 年；</p>
                    <p>（二）属养老机构在职人员，且未满 60 周岁；</p>
                    <p>（三）中等职业技术学校（技工学校）、高等院校全日制毕业生，或技工院校全日制高级工班、预备技师班毕业生（申请人在合同期内学历发生变化的，按最高学历计算）。</p>
                    <p>（四）申请人在公办养老机构（含农村敬老院）从业的，应当为该机构的编制外人员。</p>
                    <p>就业补贴标准：</p>
                    <p>（一）中等职业技术学校（技工学校）全日制毕业生在所供职养老机构连续工作满 3 年的，给予一次性就业补贴 5000 元；</p>
                    <p>（二）高等院校全日制毕业生及技工院校全日制高级工班、预备技师班毕业生在所供职养老机构工作连续满 3 年且未获得前款第（一）项就业补贴的，给予一次性就业补贴 10000 元；</p>
                    <p>（三）已获得前款第（一）项就业补贴的申请人，在申请前款第（二）项就业补贴时，按照 5000 元的补差标准给予一次性补贴。</p>
                    <p>申请人申请就业补贴应向所供职养老机构填写后续相关材料！！！</p>
                    <h2>岗位补贴</h2>
                    <p>申请人申请岗位补贴应当符合以下条件：</p>
                    <p>（一）与养老机构签订劳动合同，且在该养老机构中从事护理等养老服务一线工作满 5 年、满 10 年；</p>
                    <p>（二）属养老机构在职人员，且未满 60 周岁；</p>
                    <p>（三）持有养老护理员职业资格证书、专项证书或岗前培训证书。</p>
                    <p>（四）申请人在公办养老机构（含农村敬老院）从业的，应当为该机构的编制外人员。</p>
                    <p>岗位补贴标准：</p>
                    <p>（一）养老护理员从事一线养老护理连续工作满 5 年但未满10 年的，给予一次性岗位补贴 5000 元；</p>
                    <p>（二）养老护理员从事一线养老护理连续工作满 10 年且未获得前款第（一）项就业补贴的，给予一次性就业补贴 20000 元；</p>
                    <p>（三）已获得前款第（一）项就业补贴的申请人，在申请前款第（二）项就业补贴时，按照 15000 元的补差标准给予一次性补贴。</p>
                    <p>申请人申请就业补贴应向所供职养老机构填写后续相关材料！！！</p>
                    <h3 style={{ color: 'red' }}>养老机构服务人员同时符合就业补贴及岗位补贴申请条件的，可以同时申请就业补贴及岗位补贴。</h3>
                </Modal>
                <Modal
                    title="已超过申请报名时间"
                    visible={this.state.timeOutvisible}
                    onOk={this.timeOut}
                    onCancel={this.timeOut}
                >
                    <h3>已超过申请报名时间,补贴申请时间为每年的6月1号至6月15号！！！</h3>
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：补贴申请录入编辑控件
 * @description 补贴申请录入编辑控件
 * @author
 */
@addon('AllowanceInsertView', '补贴申请录入编辑控件', '补贴申请录入编辑控件')
@reactControl(Form.create<any>()(AllowanceInsertView), true)
export class AllowanceInsertViewControl extends ReactViewControl {
    form?: WrappedFormUtils<any>;
    /** 视图权限 */
    public permission?: Permission;
}