import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';
import { request } from "src/business/util_tool";
const { Option } = Select;
/**
 * 组件：补贴模板列表
 */
export interface AllowanceTemplateViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 申请状态 */
    status?: any[];
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 模板数据 */
    templates?: any[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：补贴模板管理
 * 描述
 */
export class AllowanceManageView extends ReactView<AllowanceTemplateViewControl, AllowanceTemplateViewState> {
    private columns_data_source = [{
        title: '定义名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '评估模板',
        dataIndex: 'template_name',
        key: 'template_name',
    }, {
        title: '创建日期',
        dataIndex: 'create_time',
        key: 'create_time',
    }];
    constructor(props: AllowanceTemplateViewControl) {
        super(props);
        this.state = {
            condition: {},
            status: ["已申请", "通过", "不通过"],
            select_ids: [],
            templates: [],
            request_url: '',
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.allowance_service['get_assessment_template']!())
            .then((datas: any) => {
                this.setState({
                    templates: datas.result,
                });
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.allowanceTemplateInsert);
    }
    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.allowanceTemplateInsert + '/' + contents.id);
        }
    }
    /*
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    render() {
        const credentials_list: JSX.Element[] = this.state.templates!.map((item, idx) => <Option key={item.id} value={item.template_name}>{item.template_name}</Option>);
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "定义名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.select,
                    label: "评估模板",
                    decorator_id: "template_name",
                    option: {
                        placeholder: "请选择评估模板",
                        showSearch: true,
                        childrens: credentials_list,
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "创建日期",
                    decorator_id: "create_time"
                },
            ],

            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_allowance_define'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let role_permission_list = Object.assign(role_permission, table_param);
        return (
            (
                <SignFrameLayout {...role_permission_list} />
            )
        );
    }
}

/**
 * 控件：补贴模板管理
 * 描述
 */
@addon('AllowanceManageView', '补贴模板管理', '描述')
@reactControl(AllowanceManageView, true)
export class AllowanceTemplateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}