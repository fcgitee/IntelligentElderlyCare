import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { edit_props_info } from "../../../../app/util-tool";
import { ROUTE_PATH } from '../../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { request } from 'src/business/util_tool';
import { remote } from 'src/projects/remote';
let { Option } = Select;
/**
 * 组件：补贴定义新增
 */
export interface AllowanceTempViewState extends ReactViewState {
    /** 模板数据 */
    templates?: any[];
}

/**
 * 组件：补贴定义视图
 */
export default class AllowanceTemplatetView extends ReactView<AllowanceTemplateInsertViewControl, AllowanceTempViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            templates: [],
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.allowance_service['get_assessment_template']!())
            .then((datas: any) => {
                this.setState({
                    templates: datas.result,
                });
            });
    }

    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        // console.log('保存按钮回调方法');
    }

    render() {
        const
            credentials_list: JSX.Element[] = this.state.templates!.map((item, idx) => <Option key={item.id} value={item.id}>{item.template_name}</Option>),
            layout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 5 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 19 },
                },
            },
            edit_props = {
                form_items_props: [
                    {
                        title: "补贴定义",
                        need_card: true,
                        input_props: [
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.antd_input,
                                            col_span: 20,
                                            label: "补贴定义名称",
                                            decorator_id: "name",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择" }],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入定义名称",
                                            },
                                            layout: layout
                                        },
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.select,
                                            col_span: 20,
                                            label: "补贴类型",
                                            decorator_id: "template_id",
                                            option: {
                                                childrens: credentials_list,
                                                placeholder: "请选择评估模板",
                                                showSearch: true
                                            },
                                            layout: layout
                                        }
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {   
                                            type: InputType.file_upload,
                                            label: "文件资料（大小小于2M，格式支持jpg/png/doc/docx/excel）",
                                            decorator_id: "file",
                                            field_decorator_option: {
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                action: remote.upload_url,
                                                // contents: 'button'
                                            },
                                        }
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.input_uneditable,
                                            col_span: 20,
                                            label: "创建时间",
                                            decorator_id: "create_time",
                                            field_decorator_option: {
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "创建时间",
                                            },
                                            layout: layout
                                        }
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.text_area,
                                            col_span: 20,
                                            label: "备注",
                                            decorator_id: "remark",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请输入备注" }],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入备注",
                                                rows: 3
                                            },
                                            layout: layout
                                        },
                                    ]
                                },
                            },
                        ]
                    }
                ],
                other_btn_propps: [
                    {
                        text: "取消",
                        cb: () => {
                            this.props.history!.push(ROUTE_PATH.allowanceTemplate);
                        }
                    },
                ],
                submit_btn_propps: {
                    text: "提交",
                },
                service_option: {
                    service_object: AppServiceUtility.allowance_service,
                    operation_option: {
                        query: {
                            func_name: "get_allowance_define_list_all",
                            arguments: [{ id: this.props.match!.params.key }, 1, 1]
                        },
                        save: {
                            func_name: "add_allowance_define"
                        },
                    },
                },
                succ_func: () => { this.props.history!.push(ROUTE_PATH.allowanceTemplate); },
                id: this.props.match!.params.key,
            };
        let edit_props_list = Object.assign(edit_props, edit_props_info);

        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：补贴定义录入编辑控件
 * @description 补贴定义录入编辑控件
 * @author
 */
@addon('AllowanceTemplatetView', '补贴定义录入编辑控件', '补贴定义录入编辑控件')
@reactControl(AllowanceTemplatetView, true)
export class AllowanceTemplateInsertViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}