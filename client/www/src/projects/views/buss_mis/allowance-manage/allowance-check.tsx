import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：补贴申请审核状态
 */
export interface AllowanceCheckViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 接口名 */
    request_ulr?: string;
}

/**
 * 组件：补贴申请审核
 * 描述
 */
export class AllowanceCheckView extends React.Component<AllowanceCheckViewControl, AllowanceCheckViewState> {
    private columns_data_source = [{
        title: '申请人姓名',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '申请补贴类型',
        dataIndex: 'allowance_type',
        key: 'allowance_type',
    },
    {
        title: '申请时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '申请状态',
        dataIndex: 'status',
        key: 'status',
    }];
    constructor(props: AllowanceCheckViewControl) {
        super(props);
        this.state = {
            condition: {}
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.allowanceReviewed + '/' + contents.id);
        }
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "申请人姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "申请补贴类型",
                    decorator_id: "allowance_type"
                },
                {
                    type: InputType.date,
                    label: "申请时间",
                    decorator_id: "create_date"
                },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [this.state.condition, 1, 10]
                },
                delete: {
                    service_func: 'delete_nursing_record'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
            // searchExtraParam: this.props.list_type ? { allowance_type: this.props.list_type } : { allowance_type: '养老' },
        };
        let role_permission_list = Object.assign(role_permission, table_param);
        return (
            (
                <SignFrameLayout {...role_permission_list} />
            )
        );
    }
}

/**
 * 控件：补贴申请审核
 * 描述
 */
@addon('AllowanceCheckView', '补贴申请审核', '描述')
@reactControl(AllowanceCheckView, true)
export class AllowanceCheckViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 列表类型 */
    public list_type?: string;
}