import { addon, Permission } from "pao-aop";
import { reactControl, CookieUtil, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { User } from "src/business/models/user";
import { Steps, Card } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";
import { COOKIE_KEY_CURRENT_USER } from "src/business/mainForm/backstageManageMainForm";
import { AppServiceUtility } from 'src/projects/app/appService';

const { Step } = Steps;
/**
 * 组件：补贴申请状态查看状态
 */
export interface AllowanceManageViewState extends ReactViewState {
    current_jy?: any;
    current_gw?: any;
    status?: any;
    content_jy?: any;
    content_gw?: any;
    jy_state?: any;
    gw_state?: any;
}

/**
 * 组件：补贴申请状态查看
 * 描述
 */
export class AllowanceManageView extends ReactView<AllowanceManageViewControl, AllowanceManageViewState> {
    constructor(props: AllowanceManageViewControl) {
        super(props);
        this.state = {
            current_jy: '',
            current_gw: '',
            status: '',
            content_jy: '',
            jy_state: false,
            gw_state: false,
        };
    }
    componentDidMount() {
        // AppServiceUtility.user_service.get_current_user!()!
        //     .then((data:any) => {
        //         // console.log(data);
        //     });
        // console.log(CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).id);
        // 获取当前登陆人的id
        // let page = 0 ;
        let currentUserId = CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).id;
        // TODO: 查询这个人今年的就业补贴
        AppServiceUtility.allowance_service.get_currentUser_allowance_list!({ 'apply_user_id': currentUserId, 'allowance_type': '就业补贴' })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    if (data.result[0].step_no && data.result[0].status) {
                        this.setState({
                            current_jy: data.result[0].step_no,
                            jy_state: true
                        });
                        if (data.result[0].step_no === 1) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>你的资料已经提交,请耐心等待审核结果。7 月 5 日前会有审核结果。申请人对本人初审结果有异议的，应在 7 月 10 日前向区民政局提交书面复核申请。</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === 2) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>请等待第三方机构审核</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === 3) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>20 个工作日内会有审批结果,通过名单将在南海区人民政府网站区民政局栏目公示 5 个工作日</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === -1) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_jy: (
                                        <Card><div style={{ textAlign: 'center' }}>次年的4月 30 日前将有关补贴资金全额拨付至养老机构服务人员个人账户。</div></Card>
                                    )
                                });
                            }
                        }
                    }
                }
                // page = data.result[0].step_no !== -1 ? data.result[0].step_no - 1 : 2;
                // // console.log(page);
            });
        // TODO: 查询这个人今年的岗位补贴
        AppServiceUtility.allowance_service.get_currentUser_allowance_list!({ 'apply_user_id': currentUserId, 'allowance_type': '岗位补贴' })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    if (data.result[0].step_no && data.result[0].status) {
                        this.setState({
                            current_gw: data.result[0].step_no,
                            gw_state: true
                        });
                        if (data.result[0].step_no === 1) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>你的资料已经提交,请耐心等待审核结果。7 月 5 日前会有审核结果。申请人对本人初审结果有异议的，应在 7 月 10 日前向区民政局提交书面复核申请。</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === 2) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>请等待第三方机构审核</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === 3) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>20 个工作日内会有审批结果,通过名单将在南海区人民政府网站区民政局栏目公示 5 个工作日</div></Card>
                                    )
                                });
                            }
                        }
                        if (data.result[0].step_no === -1) {
                            if (data.result[0].status === '不通过' || data.result[0].status === '打回') {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>你的申请结果为：不通过</div></Card>
                                    )
                                });
                            } else {
                                this.setState({
                                    content_gw: (
                                        <Card><div style={{ textAlign: 'center' }}>次年的4月 30 日前将有关补贴资金全额拨付至养老机构服务人员个人账户。</div></Card>
                                    )
                                });
                            }
                        }
                    }
                }
                // page = data.result[0].step_no !== -1 ? data.result[0].step_no - 1 : 2;
                // // console.log(page);
            });
    }
    render() {
        let { current_jy, current_gw } = this.state;
        let steps = [
            {
                title: '已提交申请'
            },
            {
                title: '镇（街道）民政部门审核'
            },
            {
                title: '第三方机构审核'
            },
            {
                title: '区民政部门审核'
            },
            {
                title: '审核完成'
            },
        ];
        return (
            <div>
                <MainContent>
                    <MainCard title='就业补贴--申请状态查看'>
                        <Steps current={current_jy !== -1 ? current_jy : 4}>
                            {steps.map(item => (
                                <Step key={item.title} title={item.title} />
                            ))}
                        </Steps>
                    </MainCard>
                    {this.state.jy_state === true ? this.state.content_jy
                        :
                        <Card><div style={{ textAlign: 'center' }}>你目前还没有申请就业补贴</div></Card>
                    }
                    <MainCard title='岗位补贴--申请状态查看'>
                        <Steps current={current_gw !== -1 ? current_gw : 4}>
                            {steps.map(item => (
                                <Step key={item.title} title={item.title} />
                            ))}
                        </Steps>
                    </MainCard>
                    {this.state.gw_state === true ? this.state.content_gw
                        :
                        <Card><div style={{ textAlign: 'center' }}>你目前还没有申请岗位补贴</div></Card>
                    }
                </MainContent>
            </div>
        );
    }
}

/**
 * 控件：补贴申请状态查看
 * 描述
 */
@addon('AllowanceManageView', '补贴申请状态查看', '描述')
@reactControl(AllowanceManageView, true)
export class AllowanceManageViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}