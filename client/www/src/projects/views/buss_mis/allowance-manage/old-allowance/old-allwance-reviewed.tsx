import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Descriptions, Button, Row, Radio, message } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from 'src/projects/app/appService';
import { MainContent } from "src/business/components/style-components/main-content";
import { MainCard } from "src/business/components/style-components/main-card";

/**
 * 组件：服务人员审核页面
 */
export interface OldAllwanceReviewedViewState extends ReactViewState {
    data?: any;
    reason?: any;
    result?: any;
    id_card?: any;
    credit_card_num?: any;
    bank?: any;
    id_card_address?: any;
}

/**
 * 组件：服务人员审核页面
 * 描述
 */
export class OldAllwanceReviewedView extends ReactView<OldAllwanceReviewedViewControl, OldAllwanceReviewedViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            result: '',
            reason: '',
            id_card: '',
            credit_card_num: '',
            bank: '',
            id_card_address: ''
        };
    }
    componentDidMount = () => {
        let id = this.props.match!.params.key;
        AppServiceUtility.allowance_service.get_audit_old_allowance_list!({ id })!
            .then((data: any) => {
                // console.log(data);
                if (data.result.length > 0) {
                    this.setState({
                        data: data.result[0],
                        result: data.result[0].apply_status
                    });
                }
            });
    }
    // 提交
    handleSubmit = () => {
        let id = this.props.match!.params.key;
        const { result } = this.state;
        var params = {
            id,
            apply_status: result
        };
        // console.log(params);
        AppServiceUtility.allowance_service.audit_old_allowance!(params)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('提交成功');
                    this.props.history!.push(ROUTE_PATH.oldallowanceCheck);
                } else {
                    message.info('提交失败');
                }
            });
    }

    // 审核结果
    resultChange = (e: any) => {
        console.info('我切换了选择');
        this.setState({
            result: e.target.value
        });
    }

    // 原因
    reasonChange = (e: any) => {
        this.setState({
            reason: e.target.value
        });
    }
    render() {
        const { data } = this.state;
        return (
            <div>
                <MainContent>
                    <MainCard title='服务人员申请审核'>
                        <Descriptions
                            title="申请信息"
                            bordered={true}
                            column={3}
                        >
                            <Descriptions.Item label="姓名">{data.name}</Descriptions.Item>
                            <Descriptions.Item label="性别">{data.sex}</Descriptions.Item>
                            <Descriptions.Item label="身份证">{data.id_card_num}</Descriptions.Item>
                            <Descriptions.Item label="个人照片"><img src={data.photo} style={{ width: '40px', height: '40px' }} /></Descriptions.Item>
                            <Descriptions.Item label="身份证照片"><img src={data.id_card_img} style={{ width: '40px', height: '40px' }} /></Descriptions.Item>

                        </Descriptions>
                        <br />
                        <br />
                        <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>审核结果<span>(请选择审核结果)</span></Row>
                        <br />
                        <Radio.Group onChange={this.resultChange} value={this.state.result}>
                            <Radio value={'已生成台账'}>已生成台账</Radio>
                            <Radio value={'未通过'}>不通过</Radio>
                        </Radio.Group>
                        <br />
                        <br />
                        <br />
                        <Row type="flex" justify="center" className="ctrl-btns">
                            <Button type="primary" onClick={this.handleSubmit}>提交</Button>
                        </Row>
                    </MainCard>
                </MainContent>
            </div >
        );
    }
}

/**
 * 控件：服务人员审核页面
 * 描述
 */
@addon(' OldAllwanceReviewedView', '服务人员审核页面', '提示')
@reactControl(OldAllwanceReviewedView, true)
export class OldAllwanceReviewedViewControl extends ReactViewControl {
}