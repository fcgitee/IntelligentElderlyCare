import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Modal, Form, Radio, Checkbox, DatePicker, Input, Select, InputNumber } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import { exprot_excel } from "src/business/util_tool";
const { RangePicker } = DatePicker;
const { Option } = Select;
/**
 * 组件：残疾人认证列表状态
 */
export interface DisabledPersonCheckViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 接口名 */
    request_ulr?: string;
    upload?: any;
    show?: any;
    preview?: any;
    preview_parm?: any;
    isAll?: any;
    financial_list?: any;
    request_url?: any;
    checkedList?: any;
    indeterminate?: any;
    checkAll?: any;
    /** 导出的条件 */
    select_type?: any;
    /** 查询回来的数据 */
    data?: any;
    /** 筛选条件-姓名 */
    name?: any;
    id_card?: any;
    condition_s?: any;
    /** 日期框 */
    mode?: any;
    timevalue?: any;
    // export_data?: any;
    /** 筛选时间 */
    start_time?: any;
    end_time?: any;
    /** 社区 */
    village?: any;
    /** 镇街 */
    town_street?: any;
    /** 年龄 */
    start_age?: any;
    end_age?: any;
}
export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：残疾人认证列表
 * 描述
 */
const slect_condition = [{ label: '认证成功', value: '认证成功' }, { label: '已生成台账', value: '已生成台账' }, { label: '未认证', value: '未认证' }];
export class DisabledPersonCheckView extends ReactView<DisabledPersonCheckViewControl, DisabledPersonCheckViewState> {
    private columns_data_source = [{
        title: '认证人姓名',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card_num',
        key: 'id_card_num',
    },
    {
        title: '认证时间',
        dataIndex: 'create_date',
        key: 'create_date',
    }, {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
    }];
    constructor(props: DisabledPersonCheckViewControl) {
        super(props);
        this.state = {
            condition: {},
            upload: false,
            show: false,
            preview: false,
            preview_parm: [],
            isAll: false,
            financial_list: [],
            request_url: '',
            indeterminate: true,
            checkAll: false,
            select_type: 1,
            data: {},
            name: '',
            id_card: '',
            condition_s: [],
            mode: ['month', 'month'],
            timevalue: [],
            // export_data: [],
            start_time: '',
            end_time: '',
            village: '',
            town_street: '',
            start_age: '',
            end_age: ''
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.oldallowanceReviewed + '/' + contents.id);
        }
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    /** 一键通过按钮 */
    print = () => {
        this.setState({ upload: true });
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    // 导出类型选择
    onChangeRadio(value: any) {
        // console.log(value);
        // 全部
        if (value === 1) {
            this.setState({
                isAll: false,
                select_type: value
            });
        }
        // 筛选记录进行导出
        if (value === 0) {
            this.setState({
                isAll: true,
                select_type: value
            });
        }
    }
    // 下载
    download() {
        // 判断导出选择的条件
        const { select_type } = this.state;
        // let export_data: any = [];
        // 全部
        if (select_type === 1) {
            AppServiceUtility.allowance_service.get_audit_old_allowance_list!({ "export": true })!
                .then((data: any) => {
                    if (data['result']) {
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "认证时间": item.create_date,
                                "镇（街）": item.town_street,
                                "村（居委）": item.village,
                                "姓名": item.name,
                                "性别": item.sex,
                                "身份证号码": item.id_card_num,
                                "备注": item.status
                            });
                        });
                        exprot_excel([{ name: '残疾人认证名单', value: new_data }], '残疾人认证名单', 'xls');
                        this.setState({ show: false });
                    }
                });
            // this.setState({
            //     preview: true,
            //     preview_parm: [{}]
            // });
        }
        // 带筛选条件
        if (select_type === 0) {
            const { name, id_card, condition_s, start_time, end_time, village, town_street, start_age, end_age } = this.state;
            let params = { "export": true };
            let newParams = {};
            // let selectParams = {};
            if (name !== '') {
                newParams = { ...params, ...newParams, "name": name };
                // selectParams = { ...selectParams, "name": name };
            }
            if (id_card !== '') {
                newParams = { ...params, ...newParams, "id_card_num": id_card };
                // selectParams = { ...selectParams, "id_card_num": id_card };
            }
            if (condition_s.length === 1) {
                newParams = { ...params, ...newParams, "status": condition_s[0] };
                // selectParams = { ...selectParams, "status": condition_s[0] };
            }
            if (start_time !== '' && end_time !== '') {
                newParams = { ...params, ...newParams, "start_time": start_time, "end_time": end_time };
                // selectParams = { ...selectParams, "start_time": start_time, "end_time": end_time };
            }
            if (village !== '') {
                newParams = { ...params, ...newParams, "village": village };
                // selectParams = { ...selectParams, "village": village };
            }
            if (town_street !== '') {
                newParams = { ...params, ...newParams, "town_street": town_street };
                // selectParams = { ...selectParams, "town_street": town_street };
            }
            if (start_age !== '' && end_age !== '' && start_age !== null && end_age !== null) {
                newParams = { ...params, ...newParams, "start_age": start_age, "end_age": end_age };
                // selectParams = { ...selectParams, "start_age": start_age, "end_age": end_age };
            }
            // console.log('条件》》》', newParams);
            AppServiceUtility.allowance_service.get_audit_old_allowance_list!(newParams)!
                .then((data: any) => {
                    // console.log('data>>', data);
                    if (data['result']) {
                        // export_data = data['result'];
                        let new_data: any = [];
                        data['result'].map((item: any) => {
                            new_data.push({
                                "认证时间": item.create_date,
                                "镇（街）": item.town_street,
                                "村（居委）": item.village,
                                "姓名": item.name,
                                "性别": item.sex,
                                "身份证号码": item.id_card_num,
                                "备注": item.status
                            });
                        });
                        exprot_excel([{ name: '高龄津贴认证名单', value: new_data }], '高龄津贴认证名单', 'xls');
                    }
                });
            // this.setState({
            //     preview: true,
            //     preview_parm: [selectParams]
            // });
            // // console.log(this.state.preview_parm);
        }

    }

    // 查询成功
    onsucess_func(data: any) {
        // if (data.result.length) {
        //     this.setState({ data });
        // }
        // console.info('查询成功了');
        // console.info(data);
    }
    // 筛选-姓名
    nameChange = (e: any) => {
        // console.log("姓名", e.target.value);
        this.setState({
            name: e.target.value
        });
    }
    // 筛选-身份证
    idCardChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            id_card: e.target.value
        });
    }
    // 筛选-状态
    conditionChange = (value: any) => {
        // console.log(value);
        this.setState({
            condition_s: value
        });
    }
    // 筛选-时间
    timeChange = (value: any, dateString: any) => {
        this.setState({
            start_time: dateString[0],
            end_time: dateString[1]
        });
    }
    // 镇街
    zjonChange = (value: any) => {
        // console.log(value);
        this.setState({
            town_street: value
        });
    }
    // 社区
    sqonChange = (value: any) => {
        // console.log(value);
        this.setState({
            village: value
        });
    }

    handlePanelChange = (value: any, mode: any) => {
        this.setState({
            timevalue: value,
            mode: [mode[0] === 'date' ? 'month' : mode[0], mode[1] === 'date' ? 'month' : mode[1]],
        });
    }
    // 年龄
    startageonChange = (value: any) => {
        // console.log(value);
        this.setState({
            start_age: value
        });
    }
    endageonChange = (value: any) => {
        // console.log(value);
        this.setState({
            end_age: value
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        const { getFieldDecorator } = this.props.form!;
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.export,
                icon: 'download'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "认证人姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.date,
                    label: "认证时间",
                    decorator_id: "create_date"
                },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            // edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        // console.log('this.state.preview_parm>>', this.state.preview_parm);
        // 导出账单预览配置
        let json_list = {
            type_show: false,
            columns_data_source: [{
                title: '认证人姓名',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '身份证号码',
                dataIndex: 'id_card_num',
                key: 'id_card_num',
            },
            {
                title: '认证时间',
                dataIndex: 'create_date',
                key: 'create_date',
            }, {
                title: '状态',
                dataIndex: 'status',
                key: 'status',
            }],
            onsucess_func: (data: any) => { this.onsucess_func(data); },
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.props.request_url,
                    service_condition: this.state.preview_parm,
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let role_permission_list = Object.assign(role_permission, table_param);
        const area: any = ["大沥", "丹灶", "桂城", "九江", "里水", "狮山", "西樵"];
        const shequ: any = ["夏东", "联星", "五星"];
        return (
            (
                <div>
                    <SignFrameLayout {...role_permission_list} />
                    <Modal
                        title="请选择导出类型"
                        visible={this.state.show}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ show: !this.state.show })}
                        okText="下载"
                        cancelText="取消"
                    >
                        <MainCard>
                            <Form>
                                <Form.Item>
                                    {getFieldDecorator('isAll', {
                                        initialValue: ''
                                    })(
                                        <Radio.Group onChange={(e: any) => { this.onChangeRadio(e.target.value); }}>
                                            <Radio key={1} value={1}>导出全部认证记录</Radio>
                                            <Radio key={0} value={0}>筛选记录进行导出</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                                {
                                    this.state.isAll ? (
                                        <Form.Item>
                                            {getFieldDecorator('financial_id', {
                                                initialValue: ''
                                            })(
                                                <div>
                                                    <div style={{ borderBottom: '1px solid #E9E9E9' }} />
                                                    <br />
                                                    姓名：<Input placeholder="姓名" onChange={this.nameChange} style={{ width: 300 }} />
                                                    <br />
                                                    <br />
                                                    身份证：<Input placeholder="身份证号码" onChange={this.idCardChange} style={{ width: 300 }} />
                                                    <br />
                                                    <br />
                                                    认证状态：<Checkbox.Group
                                                        options={slect_condition}
                                                        onChange={this.conditionChange}
                                                    />
                                                    <br />
                                                    <br />
                                                    申请时间：<RangePicker placeholder={['开始日期', '结束日期']} format="YYYY-MM-DD" onChange={this.timeChange} />
                                                    <br />
                                                    <br />
                                                    年龄段：<InputNumber min={1} max={200} onChange={this.startageonChange} />&nbsp;~&nbsp;<InputNumber min={1} max={200} onChange={this.endageonChange} />
                                                    <br />
                                                    <br />
                                                    所属镇街：<Select
                                                        style={{ width: 200 }}
                                                        placeholder="请选择所属镇街"
                                                        onChange={this.zjonChange}
                                                    >
                                                        {area.map((item: any) => {
                                                            return <Option key={item} value={item}>{item}</Option>;
                                                        })}
                                                    </Select>
                                                    <br />
                                                    <br />
                                                    所属社区：<Select
                                                        style={{ width: 200 }}
                                                        placeholder="请选择所属社区"
                                                        onChange={this.sqonChange}
                                                    >
                                                        {shequ.map((item: any) => {
                                                            return <Option key={item} value={item}>{item}</Option>;
                                                        })}
                                                    </Select>
                                                </div>
                                            )}
                                        </Form.Item>
                                    ) : null
                                }
                            </Form>
                        </MainCard >
                    </Modal>
                    <Modal
                        title="预览"
                        visible={this.state.preview}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ preview: !this.state.preview })}
                        okText="下载"
                        width={1000}
                        cancelText="取消"
                    >
                        {this.state.preview ? <SignFrameLayout {...json_list} /> : null}
                    </Modal>
                </div>
            )
        );
    }
}

/**
 * 控件：残疾人认证列表
 * 描述
 */
@addon('DisabledPersonCheckView', '残疾人认证列表', '描述')
@reactControl(Form.create<any>()(DisabledPersonCheckView), true)
export class DisabledPersonCheckViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}