import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { DatePicker, Button, Table, LocaleProvider, Form, Modal } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import moment from "moment";
import { MainCard } from "src/business/components/style-components/main-card";
import zh_CN from 'antd/lib/locale-provider/zh_CN';
const { MonthPicker } = DatePicker;
import { exprot_excel } from "src/business/util_tool";

let columns: any = [
    {
        title: '月份',
        dataIndex: 'month',
        key: 'month',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '镇（街)',
        dataIndex: 'town_street',
        key: 'town_street',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '村（居委）',
        dataIndex: 'village',
        key: 'village',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '70-79周岁',
        dataIndex: '70',
        key: '70',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 's_elder_quantity',
            key: 's_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 's_elder_authentication_quantity',
            key: 's_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '80-89周岁',
        dataIndex: '80',
        key: '80',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'e_elder_quantity',
            key: 'e_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'e_elder_authentication_quantity',
            key: 'e_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '90-99周岁',
        dataIndex: '90',
        key: '90',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'n_elder_quantity',
            key: 'n_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'n_elder_authentication_quantity',
            key: 'n_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '100周岁以上',
        dataIndex: '100',
        key: '100',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'h_elder_quantity',
            key: 'h_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'h_elder_authentication_quantity',
            key: 'h_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '合计',
        dataIndex: 'total',
        key: 'total',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'elder_total_quantity',
            key: 'elder_total_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'elder_authentication_total_quantity',
            key: 'elder_authentication_total_quantity',
            width: 150,
            align: 'center'
        }, {
            title: '认证完成率',
            dataIndex: 'elder_complete_percent_total_quantity',
            key: 'elder_complete_percent_total_quantity',
            width: 150,
            align: 'center'
        }, {
            title: '未认证人数',
            dataIndex: 'elder_uncertified_total_quantity',
            key: 'elder_uncertified_total_quantity',
            width: 150,
            align: 'center'
        }]
    },
];
/**
 * 组件：高龄津贴统计报表状态
 */
export interface OldAgeStatisticsViewState extends ReactViewState {
    /** 数据源 */
    data_source?: [];
    /** 日期条件 */
    query_month?: any;
    /** 控制弹框显示 */
    show?: any;
}

/**
 * 组件：高龄津贴统计报表
 * 描述
 */
export class OldAgeStatisticsView extends ReactView<OldAgeStatisticsViewControl, OldAgeStatisticsViewState> {
    constructor(props: OldAgeStatisticsViewControl) {
        super(props);
        this.state = {
            show: false,
            query_month: moment().format('YYYY'),
        };
    }
    componentDidMount() {
        request(this, AppServiceUtility.allowance_service!.get_old_age_statistics!({ 'month': this.state.query_month }))
            .then((data: any) => {
                if (data) {
                    this.setState({
                        data_source: data.result
                    });
                }
            });
    }
    /** 日期范围回调 */
    time_change = (date: any, dateString: string) => {
        this.setState({
            query_month: dateString
        });
    }
    /** 查询方法 */
    search = () => {
        let condition = {};
        if (this.state.query_month) {
            condition['month'] = this.state.query_month;
        }
        request(this, AppServiceUtility.allowance_service!.get_old_age_statistics!(condition))
            .then((data: any) => {
                if (data) {
                    this.setState({
                        data_source: data.result
                    });
                }
            });
    }
    // 下载
    download() {
        // let export_data: any = [];
        // 全部
        let condition = {};
        if (this.state.query_month) {
            condition['month'] = this.state.query_month;
        }
        request(this, AppServiceUtility.allowance_service!.get_old_age_statistics!(condition))!
            .then((data: any) => {
                if (data['result']) {
                    let new_data: any = [];
                    data['result'].map((item: any) => {
                        new_data.push({
                            "月份": item.month,
                            "镇（街)": item.town_street ? item.town_street : '',
                            "村（居委）": item.village,
                            "70-79周岁长者人数": item.s_elder_quantity,
                            "70-79周岁认证人数": item.s_elder_authentication_quantity,
                            "80-89周岁长者人数": item.e_elder_quantity,
                            "80-89周岁认证人数": item.e_elder_authentication_quantity,
                            "90-99周岁长者人数": item.n_elder_quantity,
                            "90-99周岁认证人数": item.n_elder_authentication_quantity,
                            "100周岁以上长者人数": item.h_elder_quantity,
                            "100周岁以上认证人数": item.h_elder_authentication_quantity,
                            "合计长者人数": item.elder_total_quantity,
                            "合计认证人数": item.elder_authentication_total_quantity,
                            "合计认证完成率": item.elder_complete_percent_total_quantity,
                            "合计未认证人数": item.elder_uncertified_total_quantity,
                        });
                    });
                    exprot_excel([{ name: '高龄津贴统计报表', value: new_data }], '高龄津贴统计报表', 'xls');
                }
            });
        this.setState({ 'show': false });
    }
    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }
    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }
    render() {
        const { getFieldDecorator } = this.props.form!;
        return (
            <MainContent>
                <LocaleProvider locale={zh_CN}>
                    <MainCard title='高龄津贴统计报表' style={{ position: 'relative' }}>
                        <div style={{ overflow: 'hidden', position: 'absolute', top: '13px', right: '25px' }}>
                            <Button type="primary" onClick={this.export} style={{ float: 'right', marginRight: '3px' }}>导出</Button>
                            <Button type="primary" onClick={this.search} style={{ float: 'right', marginRight: '3px' }}>查询</Button>
                            <div style={{ float: 'right', marginRight: '3px' }}>
                                <MonthPicker onChange={this.time_change} defaultValue={moment(moment(), 'YYYY')} format={'YYYY'} />
                            </div>
                        </div>
                        <Table
                            columns={columns}
                            dataSource={this.state.data_source}
                            bordered={true}
                            size="middle"
                            scroll={{ x: 'calc(600px+50%)' }}
                        />
                    </MainCard>
                    <Modal
                        title="请选择导出类型"
                        visible={this.state.show}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ show: !this.state.show })}
                        okText="下载"
                        cancelText="取消"
                    >
                        <MainCard>
                            <Form>
                                <Form.Item>
                                    {getFieldDecorator('isAll', {
                                        initialValue: ''
                                    })(
                                        <p>导出数据</p>
                                    )}
                                </Form.Item>
                            </Form>
                        </MainCard >
                    </Modal>
                </LocaleProvider>
            </MainContent>
        );
    }
}

/**
 * 控件：高龄津贴统计报表
 * 描述
 */
@addon('OldAgeStatisticsView', '高龄津贴统计报表', '描述')
@reactControl(Form.create<any>()(OldAgeStatisticsView), true)
export class OldAgeStatisticsViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 判断类型 */
    public select_type?: any;
}