import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { exprot_excel } from "src/business/util_tool";
import { Button, Modal, Form, Input, message } from "antd";
import { request } from "src/business/util_tool";

/**
 * 组件：残疾人名单状态
 */
export interface DisabledPersonViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 接口名 */
    request_ulr?: string;
    /** 导入参数 */
    config: any;
    is_edit_btn?: any;
    show?: any;
    user_info?: any;
    id?: any;
}

/**
 * 组件：残疾人名单
 * 描述
 */
export class DisabledPersonView extends ReactView<DisabledPersonViewControl, DisabledPersonViewState> {
    private columns_data_source = [{
        title: '镇（街）',
        dataIndex: 'town_street',
        key: 'town_street',
    }, {
        title: '村（居委）',
        dataIndex: 'village',
        key: 'village',
    },
    {
        title: '长者名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card_num',
        key: 'id_card_num',
    },
    {
        title: '编辑',
        dataIndex: '',
        key: '',
        render: (text: any, record: any) => (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                {
                    this.state.is_edit_btn ?
                        <Button style={{ marginRight: '5px', textAlign: 'center' }} type="primary" onClick={() => { this.goDetail(record.id); }}>编辑</Button>
                        : ''
                }
            </div>
        ),
    },
    ];
    constructor(props: DisabledPersonViewControl) {
        super(props);
        this.state = {
            is_edit_btn: false,
            user_info: {},
            id: '',
            condition: {},
            config: {
                upload: false,
                show: false,
                func: AppServiceUtility.allowance_service.upload_disabled_person_list,
                title: '导入残疾人名单'
            }
        };
    }

    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    export_template = () => {
        const data = [{
            name: '残疾人名单', value: [{
                '镇街': '大沥',
                '村': '白沙',
                '姓名': '林小刀',
                '性别': '男',
                '身份证': '4416251997090xx835',
                '年龄': 70
            }]
        }];
        exprot_excel(data, data[0].name, 'xls');
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        this.props.permission_class[this.props.get_permission_name!]()
            .then((data: any) => {
                if (data.length > 0) {
                    data.forEach((value: any) => {
                        if (value.function + value.permission === this.props.edit_permission) {
                            this.setState({
                                is_edit_btn: true
                            });
                        }
                    });
                }
            });
    }

    goDetail = (id: any) => {
        this.setState({
            id
        });
        request(this, AppServiceUtility.allowance_service.get_disabled_person_list!({ 'id': id })!)
            .then((datas: any) => {
                console.log(datas);
                if (datas.result.length > 0) {
                    this.setState({
                        user_info: datas.result[0]
                    });
                }
            })
            .catch((error: any) => {
                // console.log(error);
            });
        this.setState({
            show: true
        });
    }

    handleOndutyCancel = () => {
        this.setState({
            show: false
        });
    }

    submit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                values = { ...values, "id": this.state.id };
                request(this, AppServiceUtility.allowance_service.update_disabled_info!({ ...values })!)
                    .then((datas: any) => {
                        console.log(datas);
                        if (datas === 'Success') {
                            message.success('修改成功');
                            this.setState({
                                show: false
                            });
                        } else {
                            message.error('修改失败');
                        }
                    })
                    .catch((error: any) => {
                        // console.log(error);
                    });
            }
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card_num"
                },
                {
                    type: InputType.input,
                    label: "镇（街）",
                    decorator_id: "town_street"
                },
                {
                    type: InputType.input,
                    label: "村（居委）",
                    decorator_id: "village"
                },
            ],
            btn_props: [{
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            }, {
                label: '下载模板',
                btn_method: this.export_template,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let role_permission_list = Object.assign(role_permission, table_param);
        const { getFieldDecorator } = this.props.form!;
        const { user_info } = this.state;
        return (
            (
                <div>
                    <SignFrameLayout {...role_permission_list} />
                    <UploadFile {...this.state.config} />
                    <Modal
                        title="编辑"
                        visible={this.state.show}
                        footer={null}
                        onCancel={this.handleOndutyCancel}
                    >
                        <Form onSubmit={this.submit}>
                            <Form.Item label="镇街">
                                {getFieldDecorator('town_street', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.town_street : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label="村（居委）">
                                {getFieldDecorator('village', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.village : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label="长者名称">
                                {getFieldDecorator('name', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.name : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label="年龄">
                                {getFieldDecorator('age', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.age : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label="性别">
                                {getFieldDecorator('sex', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.sex : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item label="身份证号码">
                                {getFieldDecorator('id_card_num', {
                                    initialValue: JSON.stringify(user_info) !== "{}" ? user_info.id_card_num : ''
                                })(
                                    <Input />
                                )}
                            </Form.Item>
                            <Form.Item style={{ textAlign: 'center' }}>
                                <Button type="primary" htmlType="submit">
                                    提交
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                </div>
            )
        );
    }
}

/**
 * 控件：残疾人名单
 * 描述
 */
@addon('DisabledPersonView', '残疾人名单', '描述')
@reactControl(Form.create<any>()(DisabledPersonView), true)
export class DisabledPersonViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}