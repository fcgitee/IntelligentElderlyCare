import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";

let columns: any = [
    {
        title: '月份',
        dataIndex: 'month',
        key: 'month',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '镇（街)',
        dataIndex: 'town_street',
        key: 'town_street',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '村（居委）',
        dataIndex: 'village',
        key: 'village',
        width: 120,
        // fixed: 'left',
        align: 'center'

    },
    {
        title: '70-79周岁',
        dataIndex: '70',
        key: '70',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 's_elder_quantity',
            key: 's_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 's_elder_authentication_quantity',
            key: 's_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '80-89周岁',
        dataIndex: '80',
        key: '80',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'e_elder_quantity',
            key: 'e_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'e_elder_authentication_quantity',
            key: 'e_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '90-99周岁',
        dataIndex: '90',
        key: '90',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'n_elder_quantity',
            key: 'n_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'n_elder_authentication_quantity',
            key: 'n_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '100周岁以上',
        dataIndex: '100',
        key: '100',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'h_elder_quantity',
            key: 'h_elder_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'h_elder_authentication_quantity',
            key: 'h_elder_authentication_quantity',
            width: 150,
            align: 'center'
        }]
    },
    {
        title: '合计',
        dataIndex: 'total',
        key: 'total',
        width: 120,
        align: 'center',
        children: [{
            title: '长者人数',
            dataIndex: 'elder_total_quantity',
            key: 'elder_total_quantity',
            width: 150,
            align: 'center',
        }, {
            title: '认证人数',
            dataIndex: 'elder_authentication_total_quantity',
            key: 'elder_authentication_total_quantity',
            width: 150,
            align: 'center'
        }, {
            title: '认证完成率',
            dataIndex: 'elder_complete_percent_total_quantity',
            key: 'elder_complete_percent_total_quantity',
            width: 150,
            align: 'center'
        }, {
            title: '未认证人数',
            dataIndex: 'elder_uncertified_total_quantity',
            key: 'elder_uncertified_total_quantity',
            width: 150,
            align: 'center'
        }]
    },
];
/**
 * 组件：残疾人认证统计报表状态
 */
export interface DisabledPersonStatisticsViewState extends ReactViewState {
    /** 数据源 */
    data_source?: [];
}

/**
 * 组件：残疾人认证统计报表
 * 描述
 */
export class DisabledPersonStatisticsView extends ReactView<DisabledPersonStatisticsViewControl, DisabledPersonStatisticsViewState> {
    componentDidMount() {
        request(this, AppServiceUtility.allowance_service!.get_disabled_person_statistics!({}))
            .then((data: any) => {
                if (data) {
                    this.setState({
                        data_source: data.result
                    });
                }
            });
    }

    render() {
        return (
            <MainContent>
                <Table
                    columns={columns}
                    dataSource={this.state.data_source}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                /></MainContent>
        );
    }
}

/**
 * 控件：残疾人认证统计报表
 * 描述
 */
@addon('DisabledPersonStatisticsView', '残疾人认证统计报表', '描述')
@reactControl(DisabledPersonStatisticsView, true)
export class DisabledPersonStatisticsViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 判断类型 */
    public select_type?: any;
}