import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { UploadFile } from "src/business/components/buss-components/upload-file";
import { exprot_excel } from "src/business/util_tool";
/**
 * 组件：高龄津贴名单状态
 */
export interface OldAgeListViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 接口名 */
    request_ulr?: string;
    /** 导入参数 */
    config: any;
}

/**
 * 组件：高龄津贴名单
 * 描述
 */
export class OldAgeListView extends ReactView<OldAgeListViewControl, OldAgeListViewState> {
    private columns_data_source = [{
        title: '镇（街）',
        dataIndex: 'town_street',
        key: 'town_street',
    }, {
        title: '村（居委）',
        dataIndex: 'village',
        key: 'village',
    },
    {
        title: '长者名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card_num',
        key: 'id_card_num',
    },
    {
        title: '存折帐号',
        dataIndex: 'bank_card_num',
        key: 'bank_card_num',
    },
    {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
    },
    ];
    constructor(props: OldAgeListViewControl) {
        super(props);
        this.state = {
            condition: {},
            config: {
                upload: false,
                func: AppServiceUtility.allowance_service.upload_old_age_list,
                title: '导入高龄津贴名单'
            }
        };
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    export_template = () => {
        const data = [{
            name: '高龄津贴导入模板', value: [{
                '序号': '1',
                '镇（街）': '大沥镇',
                '村（居委）': '白沙社区',
                '姓名': '林小刀',
                '性别': '男',
                '身份证号': '4416251997090xx835',
                '年龄': 70,
                '存折帐号': '',
                '金额': 100,
                '补发金额': 0,
                '总金额': 100
            }]
        }];
        exprot_excel(data, data[0].name, 'xlsx');
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card_num"
                },
                {
                    type: InputType.input,
                    label: "镇（街）",
                    decorator_id: "town_street"
                },
                {
                    type: InputType.input,
                    label: "村（居委）",
                    decorator_id: "village"
                },
            ],
            btn_props: [{
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            }, {
                label: '下载模板',
                btn_method: this.export_template,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let role_permission_list = Object.assign(role_permission, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...role_permission_list} />
                    <UploadFile {...this.state.config} />
                </div>
            )
        );
    }
}

/**
 * 控件：高龄津贴名单
 * 描述
 */
@addon('OldAgeListView', '高龄津贴名单', '描述')
@reactControl(OldAgeListView, true)
export class OldAgeListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}