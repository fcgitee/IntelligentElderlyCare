import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { Modal, Form, Radio } from 'antd';
import { MainCard } from "src/business/components/style-components/main-card";
import moment from "moment";
import { exprot_excel } from "src/business/util_tool";
// const { RangePicker } = DatePicker;
// const { Option } = Select;
// const { MonthPicker } = DatePicker;
/**
 * 组件：补贴申请审核状态
 */
export interface OldAllowanceNoCheckViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 接口名 */
    request_ulr?: string;
    upload?: any;
    show?: any;
    preview?: any;
    preview_parm?: any;
    isAll?: any;
    financial_list?: any;
    request_url?: any;
    checkedList?: any;
    indeterminate?: any;
    checkAll?: any;
    /** 导出的条件 */
    select_type?: any;
    /** 查询回来的数据 */
    data?: any;
    /** 筛选条件-姓名 */
    name?: any;
    id_card?: any;
    condition_s?: any;
    /** 日期框 */
    mode?: any;
    timevalue?: any;
    // export_data?: any;
    /** 筛选时间 */
    start_time?: any;
    end_time?: any;
    /** 社区 */
    village?: any;
    /** 镇街 */
    town_street?: any;
    /** 年龄 */
    start_age?: any;
    end_age?: any;
    panding?: any;
    select_month?: any;
}
export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：补贴申请审核
 * 描述
 */
// const slect_condition = [{ label: '认证成功', value: '认证成功' }, { label: '已生成台账', value: '已生成台账' }, { label: '未认证', value: '未认证' }];
export class OldAllowanceNoCheckView extends ReactView<OldAllowanceNoCheckViewControl, OldAllowanceNoCheckViewState> {
    private columns_data_source = [{
        title: '镇街',
        dataIndex: 'town_street',
        key: 'town_street',
    }, {
        title: '村（居委）',
        dataIndex: 'village',
        key: 'village',
    }, {
        title: '认证人姓名',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '身份证号码',
        dataIndex: 'id_card_num',
        key: 'id_card_num',
    }, {
        title: '申请金额',
        dataIndex: 'amount',
        key: 'amount',
    }];
    private formCreator: any = null;
    constructor(props: OldAllowanceNoCheckViewControl) {
        super(props);
        this.state = {
            condition: {},
            upload: false,
            show: false,
            preview: false,
            preview_parm: [],
            isAll: false,
            financial_list: [],
            request_url: '',
            indeterminate: true,
            checkAll: false,
            select_type: 1,
            data: {},
            name: '',
            id_card: '',
            condition_s: [],
            mode: ['month', 'month'],
            timevalue: [],
            // export_data: [],
            start_time: '',
            end_time: '',
            village: '',
            town_street: '',
            start_age: '',
            end_age: '',
            panding: false,
            select_month: undefined
        };
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.oldallowanceReviewed + '/' + contents.id);
        }
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
    }

    // 显示/取消弹窗
    onCancel(data: any) {
        this.setState(data);
    }

    /** 导出按钮 */
    export = () => {
        this.setState({ show: true });
    }

    onRef = (e: any) => {
        this.formCreator = e;
    }
    // 下载
    download() {
        let _this = this.formCreator;
        // 判断导出选择的条件
        AppServiceUtility.allowance_service.get_audit_old_allowance_no_check_list!(_this.state.condition)!
            .then((data: any) => {
                if (data['result']) {
                    let new_data: any = [];
                    data['result'].map((item: any) => {
                        new_data.push({
                            // "认证时间": item.create_date,
                            "镇（街）": item.town_street,
                            "村（居委）": item.village,
                            "姓名": item.name,
                            "性别": item.sex,
                            "身份证号码": item.id_card_num
                            // "存折帐号": item.bank_card_num,
                            // "金额": item.amount,
                            // "补发金额": item.replacement_amount,
                            // "总金额": item.total_amount,
                            // "备注": item.status
                        });
                    });
                    exprot_excel([{ name: '高龄津贴未认证名单', value: new_data }], '高龄津贴未认证名单', 'xls');
                    // window.setTimeout(
                    //     () => {
                    //         console.log('new_data>>', new_data.length);

                    //     },
                    //     1000,
                    // );

                }
            });
    }

    // 查询成功
    onsucess_func(data: any) {
        // if (data.result.length) {
        //     this.setState({ data });
        // }
        // console.info('查询成功了');
        // console.info(data);
    }

    selectMonth = (date: any, datestring: any) => {
        this.setState({
            select_month: datestring.split('-')[1].split('')[1]
        });
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        const { getFieldDecorator } = this.props.form!;
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            btn_props: [{
                label: '导出',
                btn_method: this.export,
                icon: 'download'
            }],
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "申请人姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "镇街",
                    decorator_id: "town_street"
                },
                {
                    type: InputType.input,
                    label: "村（居委）",
                    decorator_id: "village"
                },
                {
                    type: InputType.monthPicker,
                    label: "认证时间",
                    decorator_id: "allowance_create_date",
                    option: {
                        format: 'YYYY',
                        defaultValue: moment(moment(), 'YYYY')
                    }
                },
            ],
            onRef: this.onRef,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: []
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let role_permission_list = Object.assign(role_permission, table_param);
        return (
            (
                <div>
                    <SignFrameLayout {...role_permission_list} />
                    <Modal
                        title="请选择导出类型"
                        visible={this.state.show}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ show: !this.state.show })}
                        okText="下载"
                        cancelText="取消"
                    >
                        <MainCard>
                            <Form>
                                <Form.Item>
                                    {getFieldDecorator('isAll', {
                                        initialValue: ''
                                    })(
                                        <Radio.Group>
                                            <Radio key={1} value={1}>导出全部认证记录</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                            </Form>
                        </MainCard >
                    </Modal>
                    {/* <Modal
                        title="预览"
                        visible={this.state.preview}
                        onOk={() => this.download()}
                        onCancel={() => this.onCancel({ preview: !this.state.preview })}
                        okText="下载"
                        width={1000}
                        cancelText="取消"
                    >
                        {this.state.preview ? <SignFrameLayout {...json_list} /> : null}
                    </Modal> */}
                </div>
            )
        );
    }
}

/**
 * 控件：补贴申请审核
 * 描述
 */
@addon('OldAllowanceNoCheckView', '补贴申请审核', '描述')
@reactControl(Form.create<any>()(OldAllowanceNoCheckView), true)
export class OldAllowanceNoCheckViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}