import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { MainContent } from "src/business/components/style-components/main-content";
import { ROUTE_PATH } from '../../../router/index';
import { AppServiceUtility } from "src/projects/app/appService";
import { remote } from 'src/projects/remote';
/**
 * 组件：补贴申请状态查看状态
 */
export interface AllowanceProjectInsertViewState extends ReactViewState {
}

/**
 * 组件：补贴申请状态查看
 * 描述
 */
export class AllowanceProjectInsertView extends ReactView<AllowanceProjectInsertViewControl, AllowanceProjectInsertViewState> {
    constructor(props: AllowanceProjectInsertViewControl) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }
    render() {
        let edit_props = {
            form_items_props: [
                {
                    title: "补贴项目录入",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "项目名称",
                            decorator_id: "allowance_project_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目名称" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入项目名称"
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "项目类别",
                            decorator_id: "allowance_project_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择项目类别" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: 'A类',
                                    value: 'A类',
                                }, {
                                    label: 'B类',
                                    value: 'B类',
                                },{
                                    label: 'C类',
                                    value: 'C类',
                                }],
                                placeholder: "请选择项目类别"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "每月补贴金额",
                            decorator_id: "allowance_project_money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入每月补贴金额" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入每月补贴金额"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "补助对象",
                            decorator_id: "allowance_project_people",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入补助对象" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入补助对象"
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "项目详情介绍",
                            decorator_id: "allowance_project_details",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目详情介绍" }],
                                initialValue:'',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入项目详情介绍"
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {   
                                        type: InputType.upload,
                                        label: "补贴项目相关图片（大小小于2M，格式支持jpg/jpeg/png）",
                                        decorator_id: "allowance_project_photo",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请专责补贴项目相关图片" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            action: remote.upload_url,
                                            // contents: 'button'
                                        },
                                    }
                                ]
                            },
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "取消",
                    cb: () => {
                        this.props.history!.push(ROUTE_PATH.researchManage);
                    }
                }
            ],
            submit_btn_propps: {
                    text: "提交",
            },
            service_option: {
                service_object: AppServiceUtility.allowance_service,
                operation_option: {
                    save: {
                        func_name: "add_allowance_project"
                    },
                    query: {
                        func_name: "get_allowance_project_list_all",
                        arguments:[{id: this.props.match!.params.key}, 1, 1]
                    },
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.allowanceProjectList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <MainContent>
                    <FormCreator {...edit_props_list} />
                </MainContent>
            )
        );
}
}
/**
 * 控件：补贴申请状态查看
 * 描述
 */
@addon('AllowanceProjectInsertView', '补贴申请状态查看', '描述')
@reactControl(AllowanceProjectInsertView, true)
export class AllowanceProjectInsertViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}