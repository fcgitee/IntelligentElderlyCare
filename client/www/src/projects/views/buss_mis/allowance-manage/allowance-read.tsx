
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { Select } from 'antd';
const { Option } = Select;
/** 状态：申请审核查看视图 */
export interface AllowanceReadViewState extends ReactViewState {
    /** 申请状态 */
    status?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：申请审核查看视图 */
export class AllowanceReadView extends React.Component<AllowanceReadViewControl, AllowanceReadViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'org.name',
            key: 'org.name',
        },
        {
            title: '申请人姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '申请补贴类型',
            dataIndex: 'allowance_type',
            key: 'allowance_type',
        },
        {
            title: '申请时间',
            dataIndex: 'create_date',
            key: 'create_date',
        }, {
            title: '申请状态',
            dataIndex: 'status',
            key: 'status',
        },
    ];
    constructor(props: AllowanceReadViewControl) {
        super(props);
        this.state = {
            status: ["已申请", "通过", "不通过", "打回"],
            request_ulr: '',
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.allowanceInsert);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.allowanceInsert + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name"
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                {
                    type: InputType.input,
                    label: "申请姓名",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "申请补贴类型",
                    decorator_id: "allowance_type",
                },
                {
                    type: InputType.select,
                    label: "申请状态",
                    decorator_id: "status",
                    option: {
                        placeholder: "请选择状态",
                        showSearch: true,
                        childrens: this.state.status!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.date,
                    label: "申请时间",
                    decorator_id: "create_date"
                },
            ],
            btn_props: [{
                label: '申请补贴',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.allowance_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_allowance'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <SignFrameLayout {...allowance_read_list} />
        );
    }
}

/**
 * 控件：申请审核查看视图控制器
 * @description 申请审核查看视图
 * @author
 */
@addon(' AllowanceReadView', '申请审核查看视图', '申请审核查看视图')
@reactControl(AllowanceReadView, true)
export class AllowanceReadViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}