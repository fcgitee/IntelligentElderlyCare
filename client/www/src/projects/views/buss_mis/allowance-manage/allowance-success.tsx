import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import { Result,Button } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：成功页面
 */
export interface ALlowanceSuccessViewState extends ReactViewState {
}

/**
 * 组件：成功页面
 * 描述
 */
export class  ALlowanceSuccessView extends ReactView<ALlowanceSuccessViewControl,  ALlowanceSuccessViewState> {
    /** 回到首页 */
   goHome = () => {
        this.props.history!.push(ROUTE_PATH.home);
    }
    goBack = () => {
        this.props.history!.push(ROUTE_PATH.allowanceInsert);
    }
    render() {
        return (
            <Result status="success" title="成功啦" extra={[<Button type="primary" key="sure" onClick={this. goHome}>回到首页</Button>,<Button key="back" onClick={this. goBack}>返回上一页</Button>]}/>
        );
    }
}

/**
 * 控件：成功页面
 * 描述
 */
@addon(' ALlowanceSuccessView', '成功页面', '提示')
@reactControl( ALlowanceSuccessView, true)
export class  ALlowanceSuccessViewControl extends ReactViewControl {
}