import { Form, Steps, message, Descriptions, Button, Row, Radio, Input } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainCard } from "src/business/components/style-components/main-card";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from '../../../router/index';
import TextArea from "antd/lib/input/TextArea";

const { Step } = Steps;

/**
 * 组件：审核页面
 */
export interface AllowanceReviewedViewState extends ReactViewState {
    /** 步骤 */
    current?: number;
    /* 申请状态 */
    allowance_result?: any[];
    /*请求回来的数据*/
    project_list?: any;
    project?: any;
    layout?: any;
    result?: any;
    reason?: any;
    allowance_money?: any;
    step_no?: any;
    allowance_type?: any;
    showMoney?: any;
    data?: any;
    ideaData?: any;
    info_type?: any;
    money?: any;
}
/**
 * 组件：审核页面
 * 描述
 */
export class AllowanceReviewedView extends ReactView<AllowanceReviewedViewControl, AllowanceReviewedViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            current: 0,
            project_list: [],
            project: [],
            result: '',
            reason: '',
            allowance_money: 0,
            step_no: 0,
            allowance_type: '',
            showMoney: false,
            data: [],
            ideaData: [],
            info_type: '',
            money: []
        };
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error) => {
            let list: any = {
                id: this.props.match!.params.key,
                subsidy_res: this.state.result,
                opinion: { "result": this.state.result, "allowance_money": this.state.allowance_money, "reason": this.state.reason },
                allowance_money: this.state.allowance_money
            };
            if (this.state.result !== '' && this.state.result !== null && this.state.reason !== null && this.state.reason !== '') {
                AppServiceUtility.allowance_service.subsidy_approval!(list)!
                    .then((data: any) => {
                        if (data) {
                            message.info('保存成功');
                            this.props.history!.push(ROUTE_PATH.allowanceCheck);
                        }
                    });
            }
        });
    }
    infoSubmit = (err: any, values: any) => {
        let current = this.state.current! + 1;
        this.setState({
            current,
        });
    }
    componentDidMount() {
        let record_id = '';
        AppServiceUtility.allowance_service.get_allowance_list!({ id: this.props.match!.params.key }, 1, 1)!
            .then((data: any) => {
                // console.log("shenhe", data);
                record_id = data.result[0].id;
                this.setState({
                    data: data.result[0],
                    info_type: data.result[0].allowance_type,
                    step_no: data.result[0].step_no,
                });
                if (data.result[0].project_list) {
                    this.setState({
                        project_list: data.result[0].project_list ? data.result[0].project_list : []
                    });
                    // console.log(data.result[0].project_list);
                    // console.log(this.state.project_list);
                }
                // 根据记录id去过程表查找每一步的审核意见
                AppServiceUtility.allowance_service.get_reviewed_idea!({ 'business_id': record_id })!
                    .then((datas: any) => {
                        // console.log("过程表的数据", datas);
                        if (data.result.length > 0) {
                            let ideaData: any = [];
                            let money: any = [];
                            let result: any = [];
                            datas.result.map((item: any) => {
                                ideaData.push(item.opinion.length > 0 ? item.opinion[0].reason : "等待审核中，还未有审批意见");
                                money.push(item.opinion.length > 0 ? item.opinion[0].allowance_money : "");
                                result.push(item.opinion.length > 0 ? item.opinion[0].result : "");
                                this.setState({
                                    ideaData,
                                    money,
                                    result
                                });
                            });
                        }
                    });
            });

    }
    handleChange = (e: any) => {
        this.setState({
            result: e.target.value
        });
    }
    reasonChange = (e: any) => {
        // console.log(e.target.value);
        this.setState({
            reason: e.target.value
        });
    }
    moneyChange = (e: any) => {
        this.setState({
            allowance_money: e.target.value
        });
    }
    render() {
        const { data, project_list, step_no, info_type } = this.state;
        return (
            <MainContent>
                <MainCard title='申请审核页面'>
                    {info_type.indexOf("养老补贴") !== -1 ?
                        <Steps current={step_no}>
                            <Step title="已申请" description="已申请" />
                            <Step title="村民政初审" description="村民政初审" />
                            <Step title="镇民政终审" description="镇民政终审" />
                            <Step title="区备案" description="区备案" />
                        </Steps>
                        :
                        <Steps current={step_no}>
                            <Step title="已申请" description="已申请" />
                            <Step title="镇民政审核" description="镇民政审核" />
                            <Step title="平台审核" description="平台审核" />
                            <Step title="区备案" description="区备案" />
                        </Steps>
                    }
                    <br />
                    <br />
                    <Descriptions
                        title="申请信息"
                        bordered={true}
                    >
                        <Descriptions.Item label="补贴类型">{data.allowance_type}</Descriptions.Item>
                        <Descriptions.Item label="姓名">{data.name}</Descriptions.Item>
                        {project_list.length > 0 ? project_list.map((item: any) => {
                            if (item.ass_info.selete_type === "6") {
                                return <Descriptions.Item key={item.index} label={item.ass_info.project_name} ><img src={item.ass_info.value} /></Descriptions.Item>;
                            }
                            if (item.ass_info.selete_type === "1") {
                                // console.log(111111111);
                                item.ass_info.dataSource.length > 0 ? item.ass_info.dataSource.map((item_x: any) => {
                                    // console.log(2222222222222222222222222222);
                                    if (Number(item_x.serial_number) === Number(item.ass_info.value)) {
                                        // console.log(Number(item_x.serial_number) === Number(item.ass_info.value),33333333333333333,item.ass_info.project_name,item_x.option_content);
                                        return <Descriptions.Item key={item.ass_info.project_name} label={item.ass_info.project_name} >{item_x.option_content}</Descriptions.Item>;
                                    }
                                    return;
                                    // return <Descriptions.Item key={item.index} label={item.ass_info.project_name} >{item.ass_info.value}</Descriptions.Item>;
                                }) : "";
                            }
                            if (item.ass_info.selete_type !== "1" && item.ass_info.selete_type !== "6") {
                                return <Descriptions.Item key={item.index} label={item.ass_info.project_name} >{item.ass_info.value}</Descriptions.Item>;
                            }
                            return;
                        })
                            :
                            ''
                        }
                    </Descriptions>
                    <br />
                    <br />
                    <Descriptions
                        title="审核意见"
                        bordered={true}
                        layout="vertical"
                        column={3}
                    >
                        {this.state.ideaData[0] && this.state.money[0] && this.state.result[0] ?
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                审核意见：{this.state.ideaData[0]}<br />
                                审核金额：{this.state.money[0]}<br />
                                审核结果：{this.state.result[0]}<br />
                            </Descriptions.Item>
                            :
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                等待审核中，还未有审批意见
                            </Descriptions.Item>
                        }
                        {this.state.ideaData[1] && this.state.money[1] && this.state.result[1] ?
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                审核意见：{this.state.ideaData[1]}<br />
                                审核金额：{this.state.money[1]}<br />
                                审核结果：{this.state.result[1]}<br />
                            </Descriptions.Item>
                            :
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                等待审核中，还未有审批意见
                            </Descriptions.Item>
                        }
                        {this.state.ideaData[2] && this.state.money[2] && this.state.result[2] ?
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                审核意见：{this.state.ideaData[2]}<br />
                                审核金额：{this.state.money[2]}<br />
                                审核结果：{this.state.result[2]}<br />
                            </Descriptions.Item>
                            :
                            <Descriptions.Item label={info_type.indexOf("养老补贴") !== -1 ? "村审核意见" : "镇审核意见"} span={3}>
                                等待审核中，还未有审批意见
                            </Descriptions.Item>
                        }
                    </Descriptions>
                    <br />
                    <br />
                    <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>审核结果<span>(请选择审核结果)</span></Row>
                    <br />
                    <Radio.Group onChange={this.handleChange}>
                        <Radio value={'通过'}>通过</Radio>
                        <Radio value={'不通过'}>不通过</Radio>
                        <Radio value={'打回'}>打回</Radio>
                    </Radio.Group>
                    <br />
                    <br />
                    <br />
                    <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>填写补贴金额<span>(请填写补贴金额)</span></Row>
                    <br />
                    <Input prefix="￥" suffix="RMB" onChange={this.moneyChange} style={{ width: '300px' }} />
                    <br />
                    <br />
                    <br />
                    <Row style={{ fontWeight: 'bold', fontSize: '17px' }}>填写原因<span>(请填写原因)</span></Row>
                    <br />
                    <TextArea rows={4} onChange={this.reasonChange} />
                    <Row type="flex" justify="center" className="ctrl-btns">
                        <Button onClick={() => { history.back(); }}>返回</Button>
                        <Button type="primary" onClick={this.handleSubmit}>提交</Button>
                    </Row>
                </MainCard>
            </MainContent>
        );
    }
}

@addon('AllowanceReviewedView', '审核页面', '描述')
@reactControl(Form.create<any>()(AllowanceReviewedView), true)
export class AllowanceReviewedViewControl extends ReactViewControl {
}