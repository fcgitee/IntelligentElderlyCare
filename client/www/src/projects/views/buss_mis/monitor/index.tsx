import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import React from "react";
import { addon, Permission } from "pao-aop";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
/**
 * 组件：监控设备列表状态
 */
export interface MonitorViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：监控设备列表
 * 描述
 */
export class MonitorView extends ReactView<MonitorViewControl, MonitorViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'org.name',
            key: 'org.name',
        },
        {
            title: '监控名称',
            dataIndex: 'title',
            key: 'title',
        }, {
            title: '监控源URL',
            dataIndex: 'url',
            key: 'url',
        },
        // {
        //     title: '监控账号',
        //     dataIndex: 'account',
        //     key: 'account',
        // }, { 
        //     title: '监控密码',
        //     dataIndex: 'pwd',
        //     key: 'pwd',
        // },
        {
            title: '监控地址',
            dataIndex: 'address',
            key: 'address',
        }, {
            title: '上次修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        },
    ];

    constructor(props: MonitorViewControl) {
        super(props);
        this.state = {
            request_url: '',
            org_list: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeMonitor + '/' + contents.id);
        }
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.changeMonitor);
    }
    render() {
        let service_item = {
            type_show: false,
            edit_form_items_props: [{
                type: InputType.input,
                label: "监控名称",
                decorator_id: "title",
                option: {
                    placeholder: "请输入监控名称",
                }
            }, {
                type: InputType.input,
                label: "监控地址",
                decorator_id: "address",
                option: {
                    placeholder: "请输入监控地址",
                }
            },
            {
                type: InputType.tree_select,
                label: "组织机构",
                col_span: 8,
                decorator_id: "org_name",
                option: {
                    showSearch: true,
                    treeNodeFilterProp: 'title',
                    allowClear: true,
                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                    treeDefaultExpandAll: true,
                    treeData: this.state.org_list,
                    placeholder: "请选择组织机构",
                },
            },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构名称",
                //     }
                // }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.monitor_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_monitor'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_item_list = Object.assign(service_item, table_param);
        return (
            <SignFrameLayout {...service_item_list} />
        );
    }
}

/**
 * 监控设备列表页面
 */
@addon('MonitorView', '监控设备列表页面', '描述')
@reactControl(MonitorView, true)
export class MonitorViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
} 