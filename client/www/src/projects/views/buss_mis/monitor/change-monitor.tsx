import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { edit_props_info } from "src/projects/app/util-tool";
import { Select } from 'antd';
import { remote } from "src/projects/remote";
let { Option } = Select;
/**
 * 组件：编辑监控设备状态
 */
export interface ChangeMonitorViewState extends ReactViewState {
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：编辑监控设备视图
 */
export class ChangeMonitorView extends ReactView<ChangeMonitorViewControl, ChangeMonitorViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            org_list: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        console.info(this.props.match!.params.key);
        let edit_props = {
            form_items_props: [{
                title: "监控信息",
                need_card: true,
                input_props: [
                    {
                        type: InputType.antd_input,
                        label: "监控名称",
                        decorator_id: "title",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入监控名称" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入监控名称"
                        }
                    }, {
                        type: InputType.tree_select,
                        label: "组织机构",
                        decorator_id: "organization_id",
                        field_decorator_option: {
                            rules: [{ required: true, message: "组织机构" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            showSearch: true,
                            treeNodeFilterProp: 'title',
                            allowClear: true,
                            dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                            treeDefaultExpandAll: true,
                            treeData: this.state.org_list,
                            placeholder: "请选择组织机构",
                        },
                    }, {
                        type: InputType.antd_input,
                        label: "监控源URL",
                        decorator_id: "url",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入监控源URL" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入监控源URL"
                        }
                    },
                    // {
                    //     type: InputType.antd_input,
                    //     label: "ip",
                    //     decorator_id: "ip",
                    //     field_decorator_option: {
                    //         rules: [{ required: true, message: "请输入监控源ip" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请输入监控源ip"
                    //     }
                    // },
                    // {
                    //     type: InputType.input_number,
                    //     label: "端口号",
                    //     decorator_id: "post",
                    //     field_decorator_option: {
                    //         rules: [{ required: true, message: "请输入监控源端口号" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请输入监控源端口号"
                    //     }
                    // },
                    {
                        type: InputType.select,
                        label: "类型",
                        decorator_id: "type",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入监控源端口号" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入监控源端口号",
                            childrens: [
                                <Option key={'消防安全'}>{'消防安全'}</Option>,
                                <Option key={'食品安全'}>{'食品安全'}</Option>,
                            ]
                        }
                    },
                    // {
                    //     type: InputType.antd_input,
                    //     label: "监控账号",
                    //     decorator_id: "account",
                    //     field_decorator_option: {
                    //         rules: [{ required: true, message: "请输入监控账号" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请输入监控账号"
                    //     }
                    // }, {
                    //     type: InputType.antd_input,
                    //     label: "监控密码",
                    //     decorator_id: "pwd",
                    //     field_decorator_option: {
                    //         rules: [{ required: true, message: "请输入监控密码" }],
                    //     } as GetFieldDecoratorOptions,
                    //     option: {
                    //         placeholder: "请输入监控密码"
                    //     }
                    // },
                    {
                        type: InputType.upload,
                        label: "监控图片（大小小于2M，格式支持jpg/jpeg/png）",
                        decorator_id: "img_url",
                        field_decorator_option: {
                            rules: [{ required: false, message: "请上传监控图片" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            action: remote.upload_url
                        }
                    }, {
                        type: InputType.antd_input,
                        label: "监控地址",
                        decorator_id: "address",
                        field_decorator_option: {
                            rules: [{ required: true, message: "请输入监控地址" }],
                        } as GetFieldDecoratorOptions,
                        option: {
                            placeholder: "请输入监控地址"
                        }
                    },
                ]
            }],
            other_btn_propps: [{
                text: "返回",
                cb: () => {
                    this.props.history!.push(ROUTE_PATH.Monitor);
                }
            }],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.monitor_service,
                operation_option: {
                    query: {
                        func_name: "get_monitor_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_monitor"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.Monitor); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：编辑监控设备编辑控件
 * @description 编辑监控设备编辑控件
 * @author
 */
@addon('ChangeMonitorView', '编辑监控设备编辑控件', '编辑监控设备编辑控件')
@reactControl(ChangeMonitorView, true)
export class ChangeMonitorViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}