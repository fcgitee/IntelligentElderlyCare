
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { AppServiceUtility } from 'src/projects/app/appService';
import { Input, Button, Row, Col, Icon, Form, TreeSelect, Select, InputNumber, DatePicker, message } from 'antd';
import { isPermission } from "src/projects/app/permission";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import './order-list.less';
import moment from 'moment';
const { TextArea } = Input;
/** 状态：日托订单列表视图 */
export interface DayCareOrderListViewState extends ReactViewState {
    show?: any;
    org_id?: any;
    show_value?: any;
    option?: any;
    administrative_division_list?: any;
    service_Type_list?: any;
    service_product_list?: any;
    type?: any;
    service_product_id?: any;
    show_product_name?: any;
    order_date?: any;
    order_code?: any;
    amout?: any;
    comment?: any;
    show_name?: any;
}
/** 组件：日托订单列表视图 */
export class DayCareOrderListView extends React.Component<DayCareOrderListViewControl, DayCareOrderListViewState> {
    private columns_data_source = [{
        title: '订单编号',
        dataIndex: 'order_code',
        key: 'order_code',
    },
    {
        title: '机构名称',
        dataIndex: 'org_name',
        key: 'org_name',
    },
    {
        title: '长者姓名',
        dataIndex: 'user_name',
        key: 'user_name',
    },
    {
        title: '服务项目',
        dataIndex: 'product_name',
        key: 'product_name',
    },
    {
        title: '服务类型',
        dataIndex: 'type_name',
        key: 'type_name',
    },
    {
        title: '金额',
        dataIndex: 'amout',
        key: 'amout',
    },
    {
        title: '开始日期',
        dataIndex: 'order_date',
        key: 'order_date',
    },
    {
        title: '订单状态',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: '支付状态',
        dataIndex: 'pay_state',
        key: 'pay_state',
    },
    {
        title: '备注',
        dataIndex: 'comment',
        key: 'comment',
    },
    ];
    private columns_data_source2 = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: DayCareOrderListViewControl) {
        super(props);
        this.state = {
            show: false,
            org_id: '',
            show_value: '',
            administrative_division_list: [],
            option: {},
            service_Type_list: [],
            service_product_list: [],
            type: '',
            service_product_id: '',
            show_product_name: '',
            order_date: '',
            order_code: '',
            amout: '',
            comment: '',
            show_name: '',
        };
    }

    componentDidMount = () => {
        let param = {};
        if (this.props.match!.params.key) {
            param = { "id": this.props.match!.params.key };
            AppServiceUtility.comfirm_order_service.get_rt_order_list!(param)!
                .then((data: any) => {
                    // console.log('日托订单', data);
                    // 根据选择的服务产品带出相应的类型
                    AppServiceUtility.service_operation_service.get_service_type_list_tree!({ id: data.result[0].item_type[0] ? data.result[0].item_type[0] : '' })!
                        .then((data: any) => {
                            if (data.result.length > 0) {
                                // console.log(data);
                                this.setState({
                                    type: data.result[0].value
                                });
                            }
                        });
                    this.setState({
                        show: true,
                        order_code: data.result[0].order_code,
                        org_id: data.result[0].service_provider_id,
                        show_value: data.result[0].org_name[0],
                        show_name: data.result[0].purchaser_id,
                        service_product_id: data.result[0].product_id[0],
                        show_product_name: data.result[0].product_name[0],
                        amout: Number(data.result[0].amout),
                        order_date: moment(data.result[0].order_date, 'YYYY/MM/DD'),
                        comment: data.result[0].comment,
                    });
                });
        }
        // 组织机构
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({ type: '幸福院' })!
            .then(data => {
                // console.log('组织机构', data);
                this.setState({
                    option: { showSearch: true, treeNodeFilterProp: 'title', dropdownStyle: { maxHeight: 250, overflow: 'auto' }, allowClear: true, treeDefaultExpandAll: true, treeData: data!.result, placeholder: "请选择组织机构", },
                    administrative_division_list: data!.result
                });
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.dayCareOrderList + '/' + contents.id);
        }
    }

    iconClick = () => {
        let show = this.state.show;
        this.setState({
            show: !show
        });
    }

    handleSubmit = (e: any) => {
        const { org_id, service_product_id, order_date, order_code, amout } = this.state;
        e.preventDefault();
        this.props.form.validateFields((err: any, values: any) => {
            if (!err) {
                if (order_code !== '') {
                    values['order_code'] = order_code;
                }
                if (org_id !== '') {
                    values['service_provider_id'] = org_id;
                } else {
                    message.error('请选择组织机构');
                    return;
                }
                if (values['name'] === '') {
                    if (this.state.show_name !== '') {
                        values['name'] = this.state.show_name;
                    } else {
                        message.error('请选择长者');
                        return;
                    }
                }
                if (service_product_id !== '') {
                    values['service_product_id'] = service_product_id;
                } else {
                    message.error('请选择服务项目');
                    return;
                }
                if (amout === '') {
                    message.error('请输入金额');
                    return;
                } else {
                    values['amout'] = amout;
                }
                if (order_date === '') {
                    message.error('请选择开始日期');
                    return;
                } else {
                    values['order_date'] = order_date;
                }
                // console.log('提交的数据', values);
                if (this.props.match!.params.key) {
                    AppServiceUtility.comfirm_order_service.update_rt_order!({ "id": this.props.match!.params.key, ...values })!
                        .then((data: any) => {
                            // console.log(data);
                            if (data === 'Success') {
                                message.success('操作成功');
                                this.props.history!.push(ROUTE_PATH.dayCareOrderList);
                            } else {
                                message.error('操作失败');
                                return;
                            }
                        });
                } else {
                    AppServiceUtility.comfirm_order_service.create_product_order!({ ...values })!
                        .then((data: any) => {
                            // console.log(data);
                            if (data === 'Success') {
                                message.success('操作成功');
                                this.props.history!.push(ROUTE_PATH.dayCareOrderList);
                            } else {
                                message.error('操作失败');
                                return;
                            }
                        });
                }
            }
        });
    }

    // 取消
    cancle = () => {
        this.setState({
            show: false
        });
    }

    jgChange = (value: any, title: any) => {
        // console.log(value, title);
        this.setState({
            org_id: value,
            show_value: title[0]
        });
        // 服务产品
        AppServiceUtility.service_item_serviceinfo.get_service_product_item_package_list!({ organization_id: value })!
            .then((data: any) => {
                // console.log(data);
                this.setState({
                    service_product_list: data!.result,
                });
            });
    }

    productChange = (value: any) => {
        // console.log(value);
        this.setState({
            service_product_id: value['id'],
            show_product_name: value['name']
        });
        // 根据选择的服务产品带出相应的类型
        AppServiceUtility.service_operation_service.get_service_type_list_tree!({ id: value['item'][0]['item_type'] ? value['item'][0]['item_type'] : '' })!
            .then((data: any) => {
                if (data.result.length > 0) {
                    // console.log(data);
                    this.setState({
                        type: data.result[0].value
                    });
                }
            });
    }

    startChange = (date: any, dateString: any) => {
        // console.log(date, dateString);
        this.setState({
            order_date: dateString
        });
    }

    bzChange = (e: any) => {
        // console.log(e);
        this.setState({
            comment: e.target.value
        });
    }

    amoutChange = (e: any) => {
        // console.log(e);
        this.setState({
            amout: e
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
                {
                    type: ListInputType.input,
                    label: "身份证号码",
                    decorator_id: "id_card"
                },
            ],
            columns_data_source: this.columns_data_source2,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_elder_list',
            service_option: [{}, 1, 10],
            name: this.state.show_name,
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let option = {
            placeholder: "请选择长者姓名",
            modal_search_items_props: modal_search_items_props
        };
        let allowance_read = {
            type_show: false,
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.comfirm_order_service,
            service_option: {
                select: {
                    service_func: 'get_rt_order_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: ''
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.edit_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <div>
                <div style={{ position: 'relative', backgroundColor: '#ffffff', height: this.state.show === true ? '400px' : '60px', margin: '36px 36px 0px 36px', paddingTop: '20px' }}>
                    {this.state.show === true ?
                        <div>
                            <Form onSubmit={this.handleSubmit}>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('order_code', {
                                                initialValue: '',
                                            })(
                                                <div>订单编号：<Input value={this.state.order_code} disabled={true} placeholder='自动生成' style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('service_provider_id', {
                                                initialValue: '',
                                            })(

                                                <div>机构名称：<TreeSelect onChange={this.jgChange} value={this.state.show_value} style={{ width: '200px' }} {...this.state.option} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('name', {
                                                initialValue: '',
                                            })(
                                                <div className='modal-search'>长者姓名：<ModalSearch {...option} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('service_product_id', {
                                                initialValue: '',
                                            })(
                                                <div>服务项目：<Select style={{ width: '200px' }} value={this.state.show_product_name} showSearch={true} onChange={this.productChange}>
                                                    {this.state.service_product_list!.map((key: any) => {
                                                        return <Select.Option key={key} value={key}>{key['name']}</Select.Option>;
                                                    })}
                                                </Select></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('type', {
                                                initialValue: '',
                                            })(
                                                <div className='fwlx'>服务类型：<Input disabled={true} value={this.state.type} placeholder='选择服务项目自动带出' style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('amout', {
                                                initialValue: '',
                                            })(
                                                <div>金额/元：<InputNumber onChange={this.amoutChange} value={this.state.amout} style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={8}>
                                        <Form.Item>
                                            {getFieldDecorator('order_date', {
                                                initialValue: '',
                                            })(
                                                <div>开始日期：<DatePicker value={this.state.order_date} onChange={this.startChange} placeholder='请选择开始日期' style={{ width: '200px' }} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row justify="space-around" style={{ textAlign: 'center' }}>
                                    <Col span={12}>
                                        <Form.Item>
                                            {getFieldDecorator('comment', {
                                                initialValue: this.state.comment ? this.state.comment : '',
                                            })(
                                                <div>备注：<TextArea value={this.state.comment} onChange={this.bzChange} style={{ width: '420px' }} rows={4} /></div>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <div style={{ paddingLeft: '45%' }}>
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }}>保存</Button>
                                    <Button onClick={this.cancle}>取消</Button>
                                </div>
                            </Form>
                        </div>
                        :
                        ''
                    }
                    {this.state.show === false ?
                        <div onClick={this.iconClick} style={{ backgroundColor: '#ffffff', width: '50px', height: '50px', position: 'absolute', top: '10px', right: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon type='plus' />
                        </div> : ''
                    }
                </div>
                <SignFrameLayout {...allowance_read_list} />
            </div >
        );
    }
}

/**
 * 控件：日托订单列表视图控制器
 * @description 日托订单列表视图
 * @author
 */
@addon(' DayCareOrderListView', '日托订单列表视图', '日托订单列表视图')
@reactControl(Form.create<any>()(DayCareOrderListView), true)
export class DayCareOrderListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}