import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：机构运营情况状态
 */
export interface BasicFacilitiesViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：机构运营情况
 * 描述
 */
export class BasicFacilitiesView extends ReactView<BasicFacilitiesViewControl, BasicFacilitiesViewState> {
    componentWillMount() {
    }
    componentDidMount() {
    }
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 120,
        //         fixed: 'left',
        //         align: 'center'

        //     },
        //     {
        //         title: '设计床位数，含规划待建（个）',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 400,
        //         align: 'center'

        //     },
        //     {
        //         title: '设计床位数，含规划待建（个）',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 200,
        //         align: 'center'

        //     },
        //     {
        //         title: '许可或备案床位数',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '现时实际床位数',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '建筑面积',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '占地面积',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '绿化面积',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },

        //     {
        //         title: '功能室数（具体功能室名称/面积）',
        //         align: 'center',

        //         children: [
        //             {
        //                 title: '1',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             },
        //             {
        //                 title: '2',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             },
        //             {
        //                 title: '3',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             },
        //             {
        //                 title: '4',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             }, {
        //                 title: '5',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             }, {
        //                 title: '6',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             }, {
        //                 title: '7',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             }, {
        //                 title: '8',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             }, {
        //                 title: '9',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 align: 'center'

        //             },

        //         ],
        //     },
        //     {
        //         title: '医养结合情况',
        //         align: 'center',

        //         children: [
        //             {
        //                 title: '是否设有医疗机构',
        //                 dataIndex: 'age21',
        //                 key: 'age21',
        //                 width: 150,
        //                 align: 'center'

        //             },
        //             {
        //                 title: '是否与医疗合作',
        //                 dataIndex: 'age21',
        //                 key: 'age21',
        //                 width: 150,
        //                 align: 'center'

        //             },

        //         ],
        //     },
        // ];

        const data = [];
        for (let i = 0; i < 100; i++) {
            data.push({
                key: i,
                name: '南海福利院' + i + 1,
                name1: this.get_number(10000),
                name2: this.get_number(10000),
                age11: this.get_number(10000),
                street11: this.get_number(10000),
                street12: this.get_number(10000),
                street13: this.get_number(10000),
                age21: this.get_number(10000),
                street21: this.get_number(10000),
                street22: this.get_number(10000),
                street23: this.get_number(10000),
                age31: this.get_number(10000),
                street31: this.get_number(10000),
                street32: this.get_number(10000),
                street33: this.get_number(10000),
                street34: this.get_number(10000),
                building: 'C',
                number: 2035,
                companyAddress: 'Lake Street 42',
                companyName: 'SoftLake Co',
                gender: 'M',
            });
        }
        return (
            <MainContent>
                {/* <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/jjjbssqk.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                // scrolling="auto"
                />
            </MainContent>
        );
    }
}

/**
 * 控件：机构运营情况
 * 描述
 */
@addon('BasicFacilitiesView', '机构运营情况', '描述')
@reactControl(BasicFacilitiesView, true)
export class BasicFacilitiesViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}