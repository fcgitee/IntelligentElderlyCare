import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：消防资质情况状态
 */
export interface FireQualificationViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：消防资质情况
 * 描述
 */
export class FireQualificationView extends ReactView<FireQualificationViewControl, FireQualificationViewState> {
    componentDidMount() {
    }
    get_number(num: number) {
        return parseInt((Math.random() * num).toFixed(2), 10);
    }
    get_jioshu(num: number = 10000) {
        return (parseInt((Math.random() * num).toFixed(2), 10) % 2) !== 0 ? false : true;
    }
    random(lower: number, upper: number) {
        return Math.floor(Math.random() * (upper - lower)) + lower;
    }
    render() {
        let columns: any = [];
        let data = [];
        let name = ["双鸭山市第二社会福利院", "双鸭山市第三社会福利院", "怡心苑养老服务中心", "四方台区养老护理服务中心", "优居壹佰养护理院", "夕阳温情老年公寓", "世外桃园老年公寓", '安邦乡敬老院', '尖山区金和老年公寓', '尖山区双兴老年公寓', '尖山区惠民养老院', '双矿社会职能服务中心敬老院', '宝清县社会福利院'];
        let hd = ["歌唱大赛", "广场舞大赛", "中秋赏月", "户外郊游", "象棋大赛", "戏曲鉴赏", "你画我猜", "长者关爱"];
        if (this.props.select_type === '日托订单统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '日汇总',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '日成交量（单）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center',
                    },
                    {
                        title: '日成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center',
                    },
                    ]
                },
                {
                    title: '月汇总',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '月成交量（单）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '月成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
                {
                    title: '年汇总',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '年成交量（单）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '年成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '日托支付统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '微信',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '日成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center',
                    },
                    {
                        title: '月成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center',
                    },
                    {
                        title: '年成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center',
                    },
                    ]
                },
                {
                    title: '支付宝',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '日成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '月成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    },
                    {
                        title: '年成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
                {
                    title: '其他支付方式',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '日成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '月成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    },
                    {
                        title: '年成交金额（元）',
                        dataIndex: 'age11',
                        key: 'age11',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '活动报名统计') {
            columns = [
                {
                    title: '活动名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '本活动剩余可报名人数',
                    dataIndex: 'name1',
                    key: 'name1',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动已报名人数',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动签到人数',
                    dataIndex: 'street11',
                    key: 'street11',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动花费（元）',
                    dataIndex: 'age11',
                    key: 'age11',
                    width: 120,
                    align: 'center'

                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: hd[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '年龄统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '年龄分布',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '50-60年龄段人数（个）',
                            dataIndex: 'name1',
                            key: 'name1',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '60-70年龄段人数（个）',
                            dataIndex: 'age31',
                            key: 'age31',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '70-80年龄段人数（个）',
                            dataIndex: 'street12',
                            key: 'street12',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '80-90年龄段人数（个）',
                            dataIndex: 'street21',
                            key: 'street21',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '90-100年龄段人数（个）',
                            dataIndex: 'street22',
                            key: 'street22',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
                {
                    title: '是否申请补贴',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '已申请补贴人数（个）',
                            dataIndex: 'street32',
                            key: 'street32',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '还未申请补贴人数（个）',
                            dataIndex: 'street34',
                            key: 'street34',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '长者报表') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '年龄分布',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '50-60年龄段人数（个）',
                            dataIndex: 'name1',
                            key: 'name1',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '60-70年龄段人数（个）',
                            dataIndex: 'age31',
                            key: 'age31',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '70-80年龄段人数（个）',
                            dataIndex: 'street12',
                            key: 'street12',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '80-90年龄段人数（个）',
                            dataIndex: 'street21',
                            key: 'street21',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '90-100年龄段人数（个）',
                            dataIndex: 'street22',
                            key: 'street22',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
                {
                    title: '是否申请补贴',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '已申请补贴人数（个）',
                            dataIndex: 'street32',
                            key: 'street32',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '还未申请补贴人数（个）',
                            dataIndex: 'street34',
                            key: 'street34',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '长者数据') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '年龄分布',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '50-60年龄段人数（个）',
                            dataIndex: 'name1',
                            key: 'name1',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '60-70年龄段人数（个）',
                            dataIndex: 'age31',
                            key: 'age31',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '70-80年龄段人数（个）',
                            dataIndex: 'street12',
                            key: 'street12',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '80-90年龄段人数（个）',
                            dataIndex: 'street21',
                            key: 'street21',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '90-100年龄段人数（个）',
                            dataIndex: 'street22',
                            key: 'street22',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
                {
                    title: '是否申请补贴',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '已申请补贴人数（个）',
                            dataIndex: 'street32',
                            key: 'street32',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '还未申请补贴人数（个）',
                            dataIndex: 'street34',
                            key: 'street34',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '月收入统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '年龄段收入分布',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center',
                    children: [
                        {
                            title: '50-60年龄段人数（元）',
                            dataIndex: 'name1',
                            key: 'name1',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '60-70年龄段人数（元）',
                            dataIndex: 'age31',
                            key: 'age31',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '70-80年龄段人数（元）',
                            dataIndex: 'street12',
                            key: 'street12',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '80-90年龄段人数（元）',
                            dataIndex: 'street21',
                            key: 'street21',
                            width: 120,
                            align: 'center'

                        },
                        {
                            title: '90-100年龄段人数（元）',
                            dataIndex: 'street22',
                            key: 'street22',
                            width: 120,
                            align: 'center'

                        },
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '活动报告') {
            columns = [
                {
                    title: '活动名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '本活动剩余可报名人数',
                    dataIndex: 'name1',
                    key: 'name1',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动已报名人数',
                    dataIndex: 'name2',
                    key: 'name2',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动签到人数',
                    dataIndex: 'street11',
                    key: 'street11',
                    width: 120,
                    align: 'center'

                },
                {
                    title: '活动花费（元）',
                    dataIndex: 'age11',
                    key: 'age11',
                    width: 120,
                    align: 'center'

                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    age11: this.get_number(10000),
                    street11: this.get_number(10000),
                    street12: this.get_number(10000),
                    street13: this.get_number(10000),
                    age21: this.get_number(10000),
                    street21: this.get_number(10000),
                    street22: this.get_number(10000),
                    street23: this.get_number(10000),
                    age31: this.get_number(10000),
                    street31: this.get_number(10000),
                    street32: this.get_number(10000),
                    street33: this.get_number(10000),
                    street34: this.get_number(10000),
                    building: 'C',
                    number: 2035,
                    companyAddress: 'Lake Street 42',
                    companyName: 'SoftLake Co',
                    gender: 'M',
                });
            }
        } else if (this.props.select_type === '消防资质') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    fixed: 'left',
                    align: 'center'

                },
                {
                    title: '机构名称',
                    dataIndex: 'name1',
                    key: 'name1',
                    width: 80,
                    align: 'center'

                },
                {
                    title: '是否购买养老机构责任保险',
                    dataIndex: 'name12',
                    key: 'name2',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '保险截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '取得消防资质机构数（个）',
                    dataIndex: 'name4',
                    key: 'name4',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '未取得消防资质的主要原因类别',
                    dataIndex: 'name5',
                    key: 'name5',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '房屋产权、使用权不明晰机构数（个）',
                        dataIndex: 'name6',
                        key: 'name6',
                        width: 150,
                        align: 'center',
                    }, {
                        title: '未办理法人登记机构数（个）',
                        dataIndex: 'name7',
                        key: 'name7',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '房屋建筑、场所不符合消防要求机构数（个）',
                        dataIndex: 'name8',
                        key: 'name8',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '正在申请办理消防资质手续机构数（个）',
                        dataIndex: 'name9',
                        key: 'name9',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '消防设施设备不完善机构数（个）',
                        dataIndex: 'name10',
                        key: 'name10',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '其他原因（个）',
                        dataIndex: 'name11',
                        key: 'name11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '小计（个）',
                        dataIndex: 'name12',
                        key: 'name12',
                        width: 150,
                        align: 'center'
                    },
                    ]
                },
                {
                    title: '安全隐患排查资金投入情况',
                    dataIndex: 'name13',
                    key: 'name13',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '各级政府资金投入（万元）',
                        dataIndex: 'name14',
                        key: 'name14',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '机构自筹资金（万元）',
                        dataIndex: 'name15',
                        key: 'name15',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(10000),
                    name10: this.get_number(10000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_number(10000),
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '基础数据') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    width: 120,
                    fixed: 'left',
                    align: 'center'

                },
                {
                    title: '是否购买养老机构责任保险',
                    dataIndex: 'name12',
                    key: 'name2',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '保险截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '取得消防资质机构数（个）',
                    dataIndex: 'name4',
                    key: 'name4',
                    width: 120,
                    align: 'center'
                },
                {
                    title: '未取得消防资质的主要原因类别',
                    dataIndex: 'name5',
                    key: 'name5',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '房屋产权、使用权不明晰机构数（个）',
                        dataIndex: 'name6',
                        key: 'name6',
                        width: 150,
                        align: 'center',
                    }, {
                        title: '未办理法人登记机构数（个）',
                        dataIndex: 'name7',
                        key: 'name7',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '房屋建筑、场所不符合消防要求机构数（个）',
                        dataIndex: 'name8',
                        key: 'name8',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '正在申请办理消防资质手续机构数（个）',
                        dataIndex: 'name9',
                        key: 'name9',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '消防设施设备不完善机构数（个）',
                        dataIndex: 'name10',
                        key: 'name10',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '其他原因（个）',
                        dataIndex: 'name11',
                        key: 'name11',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '小计（个）',
                        dataIndex: 'name12',
                        key: 'name12',
                        width: 150,
                        align: 'center'
                    },
                    ]
                },
                {
                    title: '安全隐患排查资金投入情况',
                    dataIndex: 'name13',
                    key: 'name13',
                    width: 120,
                    align: 'center',
                    children: [{
                        title: '各级政府资金投入（万元）',
                        dataIndex: 'name14',
                        key: 'name14',
                        width: 150,
                        align: 'center'
                    }, {
                        title: '机构自筹资金（万元）',
                        dataIndex: 'name15',
                        key: 'name15',
                        width: 150,
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(10000),
                    name10: this.get_number(10000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_number(10000),
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '护照统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '人数',
                    dataIndex: 'name4',
                    key: 'name4',
                    align: 'center'
                },
                {
                    title: '政府资金',
                    dataIndex: 'name13',
                    key: 'name13',
                    align: 'center',
                    children: [{
                        title: '各级政府资金投入（万元）',
                        dataIndex: 'name14',
                        key: 'name14',
                        align: 'center'
                    }, {
                        title: '机构自筹资金（万元）',
                        dataIndex: 'name15',
                        key: 'name15',
                        align: 'center'
                    }, {
                        title: '个人资金（万元）',
                        dataIndex: 'name9',
                        key: 'name9',
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(10000),
                    name10: this.get_number(10000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_number(10000),
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '运营统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '人数',
                    dataIndex: 'name4',
                    key: 'name4',
                    align: 'center'
                },
                {
                    title: '运营情况',
                    dataIndex: 'name13',
                    key: 'name13',
                    align: 'center'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(10000),
                    name10: this.get_number(10000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_jioshu(10000) ? '良好' : '一般',
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '欠费一览') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '欠费统计',
                    dataIndex: 'name4',
                    key: 'name4',
                    align: 'center'
                },
                {
                    title: '运营情况',
                    dataIndex: 'name13',
                    key: 'name13',
                    align: 'center'
                },
                {
                    title: '政府资金',
                    dataIndex: 'name13',
                    key: 'name13',
                    align: 'center',
                    children: [{
                        title: '各级政府资金投入（万元）',
                        dataIndex: 'name14',
                        key: 'name14',
                        align: 'center'
                    }, {
                        title: '机构自筹资金（万元）',
                        dataIndex: 'name15',
                        key: 'name15',
                        align: 'center'
                    }, {
                        title: '个人资金（万元）',
                        dataIndex: 'name9',
                        key: 'name9',
                        align: 'center'
                    }
                    ]
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(10000),
                    name10: this.get_number(10000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_jioshu(10000) ? '良好' : '一般',
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '收费统计') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '截至日期',
                    dataIndex: 'name3',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '减免情况',
                    dataIndex: 'name9',
                    key: 'name9',
                    align: 'center'
                },
                {
                    title: '收费统计',
                    dataIndex: 'name5',
                    key: 'name5',
                    align: 'center'
                },
                {
                    title: '运营情况',
                    dataIndex: 'name13',
                    key: 'name13',
                    align: 'center'
                },
                {
                    title: '养老院资金情况',
                    dataIndex: 'name10',
                    key: 'name10',
                    align: 'center'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: this.get_number(10000),
                    name3: `2023-${i > 0 && i > 16 ? '10' : (i === 0 ? '10' : i)}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: '-' + this.get_number(10000),
                    name5: this.get_number(10000),
                    name6: this.get_number(10000),
                    name7: this.get_number(10000),
                    name8: this.get_number(10000),
                    name9: this.get_number(100),
                    name10: this.get_number(1000000),
                    name11: this.get_number(10000),
                    name12: this.get_number(10000),
                    name13: this.get_jioshu(10000) ? '良好' : '一般',
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '长者数量') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '长者数量',
                    dataIndex: 'name5',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '长者性别比例',
                    dataIndex: 'name2',
                    key: 'name2',
                    align: 'center'
                },
                {
                    title: '长者年龄（60-70）',
                    dataIndex: 'name6',
                    key: 'name6',
                    align: 'center'
                },
                {
                    title: '长者年龄（70-80）',
                    dataIndex: 'name7',
                    key: 'name7',
                    align: 'center'
                },
                {
                    title: '长者年龄（80-90）',
                    dataIndex: 'name8',
                    key: 'name8',
                    align: 'center'
                },
                {
                    title: '长者年龄（90-100）',
                    dataIndex: 'name11',
                    key: 'name11',
                    align: 'center'
                },
                {
                    title: '长者年龄（100-110）',
                    dataIndex: 'name10',
                    key: 'name10',
                    align: 'center'
                },
            ];

            for (let i = 0; i < 100; i++) {
                let datas = this.random(10, 100);
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(10000),
                    name2: datas + '% （男）' + '  ' + (100 - datas) + '% （女）',
                    name3: `2023-${i > 0 && i > 16 ? '10' : i}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: '-' + this.get_number(500),
                    name5: this.get_number(500),
                    name6: this.get_number(700),
                    name7: this.get_number(300),
                    name8: this.get_number(600),
                    name9: '-' + this.get_number(100),
                    name10: this.get_number(400),
                    name11: this.get_number(851),
                    name12: this.get_number(780),
                    name13: this.get_jioshu(10000) ? '良好' : '一般',
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '健康数据') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '高血压',
                    dataIndex: 'name5',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '高血脂',
                    dataIndex: 'name10',
                    key: 'name10',
                    align: 'center'
                },
                {
                    title: '糖尿病患者',
                    dataIndex: 'name6',
                    key: 'name6',
                    align: 'center'
                },
                {
                    title: '血脂异常',
                    dataIndex: 'name7',
                    key: 'name7',
                    align: 'center'
                },
                {
                    title: '脂肪肝患者',
                    dataIndex: 'name8',
                    key: 'name8',
                    align: 'center'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(600),
                    name2: this.get_number(600),
                    name3: `2023-${i > 0 && i > 16 ? '10' : i}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: '-' + this.get_number(600),
                    name5: this.get_number(600),
                    name6: this.get_number(600),
                    name7: this.get_number(600),
                    name8: this.get_number(600),
                    name9: '-' + this.get_number(600),
                    name10: this.get_number(600),
                    name11: this.get_number(600),
                    name12: this.get_number(600),
                    name13: this.get_number(600),
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        } else if (this.props.select_type === '巡检评估') {
            columns = [
                {
                    title: '机构名称',
                    dataIndex: 'name',
                    key: 'name',
                    align: 'center'
                },
                {
                    title: '自理',
                    dataIndex: 'name5',
                    key: 'name3',
                    align: 'center'
                },
                {
                    title: '介助1',
                    dataIndex: 'name10',
                    key: 'name10',
                    align: 'center'
                },
                {
                    title: '介助2',
                    dataIndex: 'name6',
                    key: 'name6',
                    align: 'center'
                },
                {
                    title: '介护1',
                    dataIndex: 'name7',
                    key: 'name7',
                    align: 'center'
                },
                {
                    title: '介护2',
                    dataIndex: 'name8',
                    key: 'name8',
                    align: 'center'
                }, {
                    title: '自定义护理',
                    dataIndex: 'name9',
                    key: 'name9',
                    align: 'center'
                },
            ];

            for (let i = 0; i < 100; i++) {
                data.push({
                    key: i,
                    name: name[i],
                    name1: this.get_number(1200),
                    name2: this.get_number(1200),
                    name3: `2023-${i > 0 && i > 16 ? '10' : i}-2${i > 0 && i > 9 ? '5' : i}`,
                    name4: '-' + this.get_number(1200),
                    name5: this.get_number(1200),
                    name6: this.get_number(1200),
                    name7: this.get_number(1200),
                    name8: this.get_number(1200),
                    name9: this.get_number(1200),
                    name10: this.get_number(1200),
                    name11: this.get_number(1200),
                    name12: this.get_number(1200),
                    name13: this.get_number(1200),
                    name14: this.get_number(10000),
                    name15: this.get_number(10000),
                });
            }
        }
        return (
            <MainContent>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                /></MainContent>
        );
    }
}

/**
 * 控件：消防资质情况
 * 描述
 */
@addon('FireQualificationView', '消防资质情况', '描述')
@reactControl(FireQualificationView, true)
export class FireQualificationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    /** 判断类型 */
    public select_type?: any;
}