import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：收养人员情况状态
 */
export interface AdoptionSituationViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：收养人员情况
 * 描述
 */
export class AdoptionSituationView extends ReactView<AdoptionSituationViewControl, AdoptionSituationViewState> {
    componentWillMount() {
    }
    componentDidMount() {
    }
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 120,
        //         fixed: 'left',
        //         align: 'center'

        //     },
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 260,

        //     },
        //     {
        //         title: '总人数',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 120,
        //         align: 'center'
        //     },
        //     {
        //         title: '女性人数',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'
        //     },
        //     {
        //         title: '按性质分',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center',
        //         children: [{
        //             title: '自费老人',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center',
        //         }, {
        //             title: '五保老人',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '三无老人',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '其他',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         },
        //         ]
        //     },
        //     {
        //         title: '按年龄分',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center',
        //         children: [{
        //             title: '79岁以下',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '80-99岁',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '100岁及以上',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'

        //         }
        //         ]
        //     },
        //     {
        //         title: '入住费用情况（元/人*月）',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center',
        //         children: [{
        //             title: '全自理',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '半自理',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '不能自理',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '一次性生活设施费',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '三五、五保供养标准',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }, {
        //             title: '康复和医疗门诊人次数',
        //             dataIndex: 'age11',
        //             key: 'age11',
        //             width: 150,
        //             align: 'center'
        //         }
        //         ]
        //     },
        // ];

        // const data = [];
        // for (let i = 0; i < 100; i++) {
        //     data.push({
        //         key: i,
        //         name: '南海福利院' + i + 1,
        //         name1: this.get_number(10000),
        //         name2: this.get_number(10000),
        //         age11: this.get_number(10000),
        //         street11: this.get_number(10000),
        //         street12: this.get_number(10000),
        //         street13: this.get_number(10000),
        //         age21: this.get_number(10000),
        //         street21: this.get_number(10000),
        //         street22: this.get_number(10000),
        //         street23: this.get_number(10000),
        //         age31: this.get_number(10000),
        //         street31: this.get_number(10000),
        //         street32: this.get_number(10000),
        //         street33: this.get_number(10000),
        //         street34: this.get_number(10000),
        //         building: 'C',
        //         number: 2035,
        //         companyAddress: 'Lake Street 42',
        //         companyName: 'SoftLake Co',
        //         gender: 'M',
        //     });
        // }
        return (
            <MainContent>
                {/* <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/3.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：收养人员情况
 * 描述
 */
@addon('AdoptionSituationView', '收养人员情况', '描述')
@reactControl(AdoptionSituationView, true)
export class AdoptionSituationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}