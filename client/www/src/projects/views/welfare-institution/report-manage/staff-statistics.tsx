import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { Table, Row, Col, Select } from "antd";
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：福利院工作人员统计状态
 */
export interface staffStatisticsViewState extends ReactViewState {
    /** 数据 */
    data?: any[];
}

/**
 * 组件：福利院工作人员统计
 * 描述
 */
export class staffStatisticsView extends ReactView<staffStatisticsViewControl, staffStatisticsViewState> {
    constructor(props: staffStatisticsViewControl) {
        super(props);
        this.state = {
            data: []
        };
    }
    componentDidMount() {
        this.selectdata();
    }
    selectdata() {
        /** 获取数据 */
        AppServiceUtility.report_management.get_welfare_worker_list!({}, 1, 10)!
            .then((res: any) => {
                this.setState({
                    data: res.result
                });
            });
    }
    selectdatanature(e: any) {
        AppServiceUtility.report_management.get_welfare_worker_list!({ Organization_nature: e })!
            .then((res: any) => {
                this.setState({
                    data: res.result
                });
            });
    }
    selectdatatype(e: any) {
        AppServiceUtility.report_management.get_welfare_worker_list!({ organization_category: e })!
            .then((res: any) => {
                this.setState({
                    data: res.result
                });
            });
    }
    render() {
        const columns: any = [
            {
                title: '镇街',
                dataIndex: 'town',
                key: 'town',
                align: 'center'
            },
            {
                title: '社区',
                dataIndex: 'org_name',
                key: 'org_name',
                align: 'center'
            },
            {
                title: '幸福院名称',
                dataIndex: 'key2',
                key: 'key2',
                align: 'center'
            },
            {
                title: '工作人员总数',
                dataIndex: 'total_worker',
                key: 'total_worker',
                align: 'center'
            },
            {
                title: '女性人数',
                align: 'center',
                dataIndex: 'female_worker',
                key: 'female_worker',
            },
            {
                title: '管理人员',
                align: 'center',
                dataIndex: 'manager_worker',
                key: 'manager_worker',
            },
            {
                title: '专业技能人员',
                align: 'center',
                dataIndex: 'normal_worker',
                key: 'normal_worker',
            },
            {
                title: '社工人员',
                align: 'center',
                dataIndex: 'key7',
                key: 'key7',
            },
            {
                title: '后勤人员',
                align: 'center',
                dataIndex: 'key8',
                key: 'key8',
            },
            {
                title: '义工服务队数',
                align: 'center',
                dataIndex: 'key9',
                key: 'key9',
            },
            {
                title: '义工人数',
                align: 'center',
                dataIndex: 'key10',
                key: 'key10',
            }
        ];

        let bodyType: any;
        bodyType = [
            {
                value: '福利院',
            },
            {
                value: '幸福院'
            }
        ];
        let institutional: any;
        institutional = [
            {
                value: '公办公营',
            },
            {
                value: '公办民营'
            },
            {
                value: '民办民营'
            },
            {
                value: '事业单位'
            },
            {
                value: '民办非企'
            },
            {
                value: '工商'
            }
        ];
        return (
            <MainContent>
                <Row style={{ marginBottom: 20 }}>
                    <Col span={8} style={{ textAlign: 'center' }}>福利院工作人员统计</Col>
                    <Col span={8} offset={8}>
                        <Select
                            onChange={this.selectdatatype = this.selectdatatype.bind(this)}
                            placeholder="请选择机构类型"
                            style={{ width: 200 }}
                        >
                            {
                                bodyType.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                        <Select
                            placeholder="请选择机构性质"
                            style={{ width: 200, marginLeft: 30 }}
                            onChange={this.selectdatanature = this.selectdatanature.bind(this)}
                        >
                            {
                                institutional.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                    </Col>
                </Row>
                <Table
                    columns={columns}
                    dataSource={this.state.data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：福利院工作人员统计
 * 描述
 */
@addon('staffStatisticsView', '福利院工作人员统计', '描述')
@reactControl(staffStatisticsView, true)
export class staffStatisticsViewControl extends ReactViewControl {
}