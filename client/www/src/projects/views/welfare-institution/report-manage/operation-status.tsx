import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { Table, Row, Col, Select } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：机构运营情况状态
 */
export interface OperationStatusViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：机构运营情况
 * 描述
 */
export class OperationStatusView extends ReactView<OperationStatusViewControl, OperationStatusViewState> {
    componentWillMount() {
    }
    componentDidMount() {
    }
    handleChange() {

    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         fixed: 'left',
        //         align: 'center',
        //         width: 120
        //     },
        //     {
        //         title: '开办时间',
        //         dataIndex: 'key1',
        //         key: 'key1',
        //         align: 'center'
        //     },
        //     {
        //         title: '机构性质',
        //         dataIndex: 'key2',
        //         key: 'key2',
        //         align: 'center'
        //     },
        //     {
        //         title: '法人代表',
        //         dataIndex: 'key3',
        //         key: 'key3',
        //         align: 'center'
        //     },
        //     {
        //         title: '地址',
        //         align: 'center',
        //         dataIndex: 'key4',
        //         key: 'key4',
        //     },
        //     {
        //         title: '院长电话',
        //         align: 'center',
        //         dataIndex: 'key5',
        //         key: 'key5',
        //     },
        //     {
        //         title: '办事人员电话',
        //         align: 'center',
        //         dataIndex: 'key6',
        //         key: 'key6',
        //     },
        //     {
        //         title: '机构运营情况（万元）',
        //         align: 'center',
        //         children: [
        //             {
        //                 title: '历年投入建设资金',
        //                 align: 'center',
        //                 dataIndex: 'key7',
        //                 key: 'key7',
        //             },
        //             {
        //                 title: '固定资产原价',
        //                 align: 'center',
        //                 dataIndex: 'key8',
        //                 key: 'key8',
        //             },
        //             {
        //                 title: '本年度收入',
        //                 align: 'center',
        //                 children: [
        //                     {
        //                         title: '合计',
        //                         align: 'center',
        //                         dataIndex: 'key9',
        //                         key: 'key9',
        //                     },
        //                     {
        //                         title: '其中',
        //                         align: 'center',
        //                         children: [
        //                             {
        //                                 title: '事业收入',
        //                                 align: 'center',
        //                                 dataIndex: 'key10',
        //                                 key: 'key10',
        //                             }, {
        //                                 title: '捐助收入',
        //                                 align: 'center',
        //                                 dataIndex: 'key11',
        //                                 key: 'key11',
        //                             }, {
        //                                 title: '其他收入',
        //                                 align: 'center',
        //                                 dataIndex: 'key12',
        //                                 key: 'key12',
        //                             },
        //                         ]
        //                     },
        //                 ]
        //             },
        //             {
        //                 title: '本年度支出',
        //                 align: 'center',
        //                 children: [
        //                     {
        //                         title: '合计',
        //                         align: 'center',
        //                         dataIndex: 'key13',
        //                         key: 'key13',
        //                     },
        //                     {
        //                         title: '其中',
        //                         align: 'center',
        //                         children: [
        //                             {
        //                                 title: '工资福利支出',
        //                                 align: 'center',
        //                                 dataIndex: 'key14',
        //                                 key: 'key14',
        //                             }, {
        //                                 title: '商品和服务支出',
        //                                 align: 'center',
        //                                 dataIndex: 'key15',
        //                                 key: 'key15',
        //                             }, {
        //                                 title: '其他收入',
        //                                 align: 'center',
        //                                 dataIndex: 'key16',
        //                                 key: 'key16',
        //                             },
        //                         ]
        //                     },
        //                 ]
        //             },
        //             {
        //                 title: '年度建设投入资金',
        //                 align: 'center',
        //                 children: [
        //                     {
        //                         title: '合计',
        //                         align: 'center',
        //                         dataIndex: 'key17',
        //                         key: 'key17',
        //                     },
        //                     {
        //                         title: '其中',
        //                         align: 'center',
        //                         children: [
        //                             {
        //                                 title: '区级财政',
        //                                 align: 'center',
        //                                 dataIndex: 'key18',
        //                                 key: 'key18',
        //                             }, {
        //                                 title: '镇级财政',
        //                                 align: 'center',
        //                                 dataIndex: 'key19',
        //                                 key: 'key19',
        //                             }, {
        //                                 title: '福利金',
        //                                 align: 'center',
        //                                 dataIndex: 'key20',
        //                                 key: 'key20',
        //                             }, {
        //                                 title: '机构自筹',
        //                                 align: 'center',
        //                                 dataIndex: 'key21',
        //                                 key: 'key21',
        //                             }
        //                         ]
        //                     },
        //                 ]
        //             },
        //         ]
        //     }
        // ];

        // let bodyType: any;
        // bodyType = [
        //     {
        //         value: '公办公营',
        //     },
        //     {
        //         value: '公办民营'
        //     },
        //     {
        //         value: '民办民营'
        //     }
        // ];
        // let institutional: any;
        // institutional = [
        //     {
        //         value: '事业单位',
        //     },
        //     {
        //         value: '民办非企'
        //     },
        //     {
        //         value: '工商'
        //     }
        // ];
        // const data = [];
        // data.push(
        //     {
        //         key: 1,
        //         name: '佛山市南海区社会福利中心',
        //         key1: '2002.9',
        //         key2: '事业',
        //         key3: '朱远强',
        //         key4: '南海区罗村街道状元路1号',
        //         key5: 86480178,
        //         key6: '',
        //         key7: '',
        //         key8: '',
        //         key9: 0,
        //         key10: '',
        //         key11: '',
        //         key12: '',
        //         key13: 0,
        //         key14: '',
        //         key15: '',
        //         key16: '',
        //         key17: 0,
        //         key18: '',
        //         key19: '',
        //         key20: '',
        //         key21: ''
        //     },
        //     {
        //         key: 1,
        //         name: '佛山市南海区社会福利中心',
        //         key1: '2010.12.1',
        //         key2: '事业',
        //         key3: '周美霞',
        //         key4: '平洲旧城区太平路18号（原平洲医院）',
        //         key5: 81811292,
        //         key6: '',
        //         key7: '1754.66',
        //         key8: 0,
        //         key9: 0,
        //         key10: 744,
        //         key11: 744,
        //         key12: 0,
        //         key13: 830,
        //         key14: 0,
        //         key15: 0,
        //         key16: 830,
        //         key17: 330,
        //         key18: 0,
        //         key19: '',
        //         key20: '',
        //         key21: ''
        //     },
        // );
        return (
            <MainContent>
                {/* <Row style={{ marginBottom: 20 }}>
                    <Col span={8}>机构运营情况</Col>
                    <Col span={8} offset={8}>
                        <Select
                            onChange={this.handleChange}
                            placeholder="请选择机构类型"
                            style={{ width: 200 }}
                        >
                            {
                                bodyType.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                        <Select
                            onChange={this.handleChange}
                            placeholder="请选择机构性质"
                            style={{ width: 200, marginLeft: 30 }}
                        >
                            {
                                institutional.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                    </Col>
                </Row>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/1.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：机构运营情况
 * 描述
 */
@addon('OperationStatusView', '机构运营情况', '描述')
@reactControl(OperationStatusView, true)
export class OperationStatusViewControl extends ReactViewControl {
}