import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { Table, Row, Col, Select } from "antd";

/**
 * 组件：入住老人情况状态
 */
export interface elderlyResidentsViewState extends ReactViewState {
    /** 数据 */
    data?: any[];
}

/**
 * 组件：入住老人情况
 * 描述
 */
export class elderlyResidentsView extends ReactView<elderlyResidentsViewControl, elderlyResidentsViewState> {
    constructor(props: elderlyResidentsViewControl) {
        super(props);
        this.state = {
            data: []
        };
    }
    componentDidMount() {
        this.selectdata();
    }
    selectdata() {
        /** 获取数据 */
    }
    render() {
        const columns: any = [
            {
                title: '养老院名称',
                dataIndex: 'name',
                key: 'name',
                align: 'center'
            },
            {
                title: '床位数',
                dataIndex: 'key1',
                key: 'key1',
                align: 'center'
            },
            {
                title: '入住总人数',
                dataIndex: 'key2',
                key: 'key2',
                align: 'center'
            },
            {
                title: '女性入住人数',
                dataIndex: 'key3',
                key: 'key3',
                align: 'center'
            },
            {
                title: '79岁以下人数',
                align: 'center',
                dataIndex: 'key4',
                key: 'key4',
            },
            {
                title: '80-99岁人数',
                align: 'center',
                dataIndex: 'key5',
                key: 'key5',
            },
            {
                title: '100岁以上人数',
                align: 'center',
                dataIndex: 'key6',
                key: 'key6',
            },
            {
                title: '自费老人数',
                align: 'center',
                dataIndex: 'key7',
                key: 'key7',
            },
            {
                title: '五保人数',
                align: 'center',
                dataIndex: 'key8',
                key: 'key8',
            },
            {
                title: '三无人数',
                align: 'center',
                dataIndex: 'key9',
                key: 'key9',
            },
            {
                title: '孤寡人数',
                align: 'center',
                dataIndex: 'key10',
                key: 'key10',
            },
            {
                title: '享受低保救济人数',
                align: 'center',
                dataIndex: 'key11',
                key: 'key11',
            },
            {
                title: '生活自理人数',
                align: 'center',
                dataIndex: 'key12',
                key: 'key12',
            },
            {
                title: '协助号码',
                align: 'center',
                dataIndex: 'key13',
                key: 'key13',
            },
            {
                title: '介护人数',
                align: 'center',
                dataIndex: 'key14',
                key: 'key14',
            }
        ];

        let bodyType: any;
        bodyType = [
            {
                value: '福利院',
            },
            {
                value: '幸福院'
            }
        ];
        let institutional: any;
        institutional = [
            {
                value: '事业单位',
            },
            {
                value: '民办非企'
            },
            {
                value: '工商'
            }
        ];
        return (
            <MainContent>
                <Row style={{ marginBottom: 20 }}>
                    <Col span={8} style={{ textAlign: 'center' }}>入住老人情况</Col>
                    <Col span={8} offset={8}>
                        <Select
                            onChange={() => this.selectdata = this.selectdata.bind(this)}
                            placeholder="请选择机构类型"
                            style={{ width: 200 }}
                        >
                            {
                                bodyType.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                        <Select
                            placeholder="请选择机构性质"
                            style={{ width: 200, marginLeft: 30 }}
                            onChange={() => this.selectdata = this.selectdata.bind(this)}
                        >
                            {
                                institutional.map((provinceItem: any, index: any) => {
                                    return (
                                        <option key={index} value={provinceItem.value}>{provinceItem.value}</option>
                                    );
                                })
                            }
                        </Select>
                    </Col>
                </Row>
                <Table
                    columns={columns}
                    dataSource={this.state.data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(600px+50%)' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：入住老人情况
 * 描述
 */
@addon('elderlyResidentsView', '入住老人情况', '描述')
@reactControl(elderlyResidentsView, true)
export class elderlyResidentsViewControl extends ReactViewControl {
}