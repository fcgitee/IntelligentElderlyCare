import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：职工构成情况状态
 */
export interface WorkerConstituteViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：职工构成情况
 * 描述
 */
export class WorkerConstituteView extends ReactView<WorkerConstituteViewControl, WorkerConstituteViewState> {
    componentWillMount() {
    }
    componentDidMount() {
        // 查询住宿区域列表
    }
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 100,
        //         fixed: 'left',

        //     },
        //     {
        //         title: '工作人员总数',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 120,
        //     },
        //     {
        //         title: '女性人数',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //     },
        //     {
        //         title: '事业编制人数',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '受教育程度',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         children: [
        //             {
        //                 title: '大专以下',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150
        //             }, {
        //                 title: '大专',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150
        //             },{
        //                 title: '本科及以上',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150
        //             }
        //         ]
        //     },
        //     {
        //         title: '年龄结构',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         children: [
        //             {
        //                 title: '35岁及以下',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             },
        //             {
        //                 title: '36岁至45岁',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             },
        //             {
        //                 title: '46岁至55岁',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             },{
        //                 title: '56岁及以上',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             }
        //         ]
        //     },
        //     {
        //         title: '人员性质',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         children: [
        //             {
        //                 title: '机构管理人员',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             }, {
        //                 title: '院长',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             }, {
        //                 title: '专业技术技能人员',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             }, {
        //                 title: '其中持有本专业的技术技能证书人员数',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //                 children: [
        //                     {
        //                         title: '院长',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '医生',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '护士',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '养老护理人员',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '助理社会工作师',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '社会工作师',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     },{
        //                         title: '其他',
        //                         dataIndex: 'age11',
        //                         key: 'age11',
        //                         width: 150,
        //                     }
        //                 ]
        //             }, {
        //                 title: '养老护理人员（含无证）',
        //                 dataIndex: 'age11',
        //                 key: 'age11',
        //                 width: 150,
        //             },
        //         ]
        //     }
        // ];

        // const data = [];
        // for (let i = 0; i < 100; i++) {
        //     data.push({
        //         key: i,
        //         name: '南海福利院' + i + 1,
        //         name1: 1,
        //         name2: 1,
        //         age11: 1,
        //         street11: 1,
        //         street12: 1,
        //         street13: 1,
        //         age21: 1,
        //         street21: 1,
        //         street22: 1,
        //         street23: 1,
        //         age31: 1,
        //         street31: 1,
        //         street32: 1,
        //         street33: 1,
        //         street34: 1,
        //         building: 'C',
        //         number: 2035,
        //         companyAddress: 'Lake Street 42',
        //         companyName: 'SoftLake Co',
        //         gender: 'M',
        //     });
        // }
        return (
            <MainContent>
                {/* <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(700px + 50%)', y: 240 }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/5.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：职工构成情况
 * 描述
 */
@addon('WorkerConstituteView', '职工构成情况', '描述')
@reactControl(WorkerConstituteView, true)
export class WorkerConstituteViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}