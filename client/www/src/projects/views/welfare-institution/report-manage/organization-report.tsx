import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：机构报表情况状态
 */
export interface OrganizationReportViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：机构报表情况
 * 描述
 */
export class OrganizationReportView extends ReactView<OrganizationReportViewControl, OrganizationReportViewState> {
    componentWillMount() {
    }
    componentDidMount() {
    }
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 120,
        //         fixed: 'left',
        //         align: 'center'

        //     },
        //     {
        //         title: '开办时间',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 120,
        //         align: 'center'
        //     },
        //     {
        //         title: '机构性质',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 120,
        //         align: 'center'
        //     },
        //     {
        //         title: '法人代表',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'
        //     },
        //     {
        //         title: '地址',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '院长电话',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        //     {
        //         title: '办事人员电话',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //         align: 'center'

        //     },
        // ];

        // const data = [];
        // for (let i = 0; i < 100; i++) {
        //     data.push({
        //         key: i,
        //         name: '南海福利院' + i + 1,
        //         name1: this.get_number(10000),
        //         name2: this.get_number(10000),
        //         age11: this.get_number(10000),
        //         street11: this.get_number(10000),
        //         street12: this.get_number(10000),
        //         street13: this.get_number(10000),
        //         age21: this.get_number(10000),
        //         street21: this.get_number(10000),
        //         street22: this.get_number(10000),
        //         street23: this.get_number(10000),
        //         age31: this.get_number(10000),
        //         street31: this.get_number(10000),
        //         street32: this.get_number(10000),
        //         street33: this.get_number(10000),
        //         street34: this.get_number(10000),
        //         building: 'C',
        //         number: 2035,
        //         companyAddress: 'Lake Street 42',
        //         companyName: 'SoftLake Co',
        //         gender: 'M',
        //     });
        // }
        return (
            <MainContent>
                {/* <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(500px)' }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/7.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：机构报表情况
 * 描述
 */
@addon('OrganizationReportView', '机构报表情况', '描述')
@reactControl(OrganizationReportView, true)
export class OrganizationReportViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}