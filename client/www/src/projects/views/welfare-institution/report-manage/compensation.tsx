import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
// import { Table } from 'antd';
import { MainContent } from "src/business/components/style-components/main-content";
// import { AppServiceUtility } from "src/projects/app/appService";
// import { request } from "src/business/util_tool";

/**
 * 组件：薪酬待遇情况状态
 */
export interface CompensationViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
}

/**
 * 组件：薪酬待遇情况
 * 描述
 */
export class CompensationView extends ReactView<CompensationViewControl, CompensationViewState> {
    componentWillMount() {
    }
    componentDidMount() {
        // 查询住宿区域列表
    }
    get_number(num: number) {
        return (Math.random() * num).toFixed(2);
    }
    render() {
        // const columns: any = [
        //     {
        //         title: '机构名称',
        //         dataIndex: 'name',
        //         key: 'name',
        //         width: 100,
        //         fixed: 'left',

        //     },
        //     {
        //         title: '院长',
        //         dataIndex: 'name1',
        //         key: 'name1',
        //         width: 120,
        //     },
        //     {
        //         title: '医生',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120,
        //     },
        //     {
        //         title: '护士',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '养老护理人员',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '后勤人员',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '社工',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '养老机构设立许可证编号',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '统一社会信用代码或组织机构代码',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '土地证号',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '房产证号',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '卫生许可证',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '饭堂食品许可证',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     },
        //     {
        //         title: '消防许可证',
        //         dataIndex: 'name2',
        //         key: 'name2',
        //         width: 120
        //     }
        // ];

        // const data = [];
        // for (let i = 0; i < 100; i++) {
        //     data.push({
        //         key: i,
        //         name: '南海福利院' + i + 1,
        //         name1: 1,
        //         name2: 1,
        //         age11: 1,
        //         street11: 1,
        //         street12: 1,
        //         street13: 1,
        //         age21: 1,
        //         street21: 1,
        //         street22: 1,
        //         street23: 1,
        //         age31: 1,
        //         street31: 1,
        //         street32: 1,
        //         street33: 1,
        //         street34: 1,
        //         building: 'C',
        //         number: 2035,
        //         companyAddress: 'Lake Street 42',
        //         companyName: 'SoftLake Co',
        //         gender: 'M',
        //     });
        // }
        return (
            <MainContent>
                {/* <Table
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    size="middle"
                    scroll={{ x: 'calc(700px + 50%)', y: 240 }}
                /> */}
                <iframe
                    src='http://183.235.223.56:80/build/www/html/6.html'
                    style={{ width: '100%', border: '0px', height: '650px' }}
                />
            </MainContent>
        );
    }
}

/**
 * 控件：薪酬待遇情况
 * 描述
 */
@addon('CompensationView', '薪酬待遇情况', '描述')
@reactControl(CompensationView, true)
export class CompensationViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}