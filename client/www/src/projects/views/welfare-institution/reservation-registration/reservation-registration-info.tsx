import { Button, Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：预约入院视图控件状态
 */
export interface ChangeReservationViewState extends ReactViewState {
}

/**
 * 组件：预约入院视图控件
 * 描述
 */
export class ChangeReservationView extends ReactView<ChangeReservationViewControl, ChangeReservationViewState> {
    public layout?: any = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
        },
    };
    public edit_props?: any = {
        row_btn_props: {
            style: {
                justifyContent: " center"
            }
        },
        form_items_props: [
            {
                title: "预约登记",
                need_card: true,
                input_props: [
                    {
                        one_row_inputs: {
                            inputs_props: [
                                {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "申请日期",
                                    decorator_id: "reservation_date",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "长者姓名",
                                    decorator_id: "name",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }, {
                                    type: InputType.input_number,
                                    col_span: 8,
                                    label: "联系方式",
                                    decorator_id: "telephone",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }
                            ]
                        }
                    },
                    {
                        one_row_inputs: {
                            inputs_props: [
                                {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "身份证号码",
                                    decorator_id: "id_card",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "性别",
                                    decorator_id: "sex",
                                    layout: this.layout,
                                    option: {
                                        childrens: [],
                                        disabled: true
                                    }
                                }, {
                                    type: InputType.input_number,
                                    col_span: 8,
                                    label: "年龄",
                                    decorator_id: "age",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }
                            ]
                        }
                    },
                    {
                        one_row_inputs: {
                            inputs_props: [
                                {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "出生日期",
                                    decorator_id: "date_birth",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                },
                            ]
                        }
                    }
                ]
            }
            // ,
            // {
            //     title: "家属联系方式",
            //     need_card: true,
            //     input_props: [
            //         {
            //             one_row_inputs: {
            //                 inputs_props: [
            //                     {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "家属姓名1",
            //                         decorator_id: "first_family_name",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }

            //                     }, {
            //                         type: InputType.select,
            //                         col_span: 8,
            //                         label: "与长者关系",
            //                         decorator_id: "first_relationship",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }, {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "联系方式",
            //                         decorator_id: "first_contact_information",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }
            //                 ]
            //             }
            //         },
            //         {
            //             one_row_inputs: {
            //                 inputs_props: [
            //                     {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "家属姓名2",
            //                         decorator_id: "second_family_name",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }, {
            //                         type: InputType.select,
            //                         col_span: 8,
            //                         label: "与长者关系",
            //                         decorator_id: "second_relationship",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }, {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "联系方式2",
            //                         decorator_id: "second_contact_information",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }
            //                 ]
            //             }
            //         },
            //         {
            //             one_row_inputs: {
            //                 inputs_props: [
            //                     {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "家属姓名3",
            //                         decorator_id: "third_family_name",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }, {
            //                         type: InputType.select,
            //                         col_span: 8,
            //                         label: "与长者关系",
            //                         decorator_id: "third_relationship",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }, {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "联系方式",
            //                         decorator_id: "third_contact_information",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }
            //                 ]
            //             }
            //         }
            //     ]
            // },
            // {
            //     title: "护理评估",
            //     need_card: true,
            //     input_props: [
            //         {
            //             one_row_inputs: {
            //                 inputs_props: [
            //                     // {
            //                     //     type: InputType.select,
            //                     //     col_span: 8,
            //                     //     label: "房间要求",
            //                     //     decorator_id: "room_type_name",
            //                     //     layout: layout,
            //                     //     option: {
            //                     //         disabled: true
            //                     //     }
            //                     // }, 
            //                     {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "评估得分",
            //                         decorator_id: "score",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     },
            //                 ]
            //             }
            //         }
            //     ]
            // },
            // {
            //     title: "其他信息",
            //     need_card: true,
            //     input_props: [
            //         {
            //             one_row_inputs: {
            //                 inputs_props: [
            //                     // {
            //                     //     type: InputType.antd_input,
            //                     //     col_span: 8,
            //                     //     label: "优先级",
            //                     //     decorator_id: "priority",
            //                     //     layout: layout,
            //                     //     option: {
            //                     //         disabled: true
            //                     //     }
            //                     // }, 
            //                     {
            //                         type: InputType.antd_input,
            //                         col_span: 8,
            //                         label: "接待人",
            //                         decorator_id: "receptionist",
            //                         layout: layout,
            //                         option: {
            //                             disabled: true
            //                         }
            //                     }
            //                 ]
            //             }
            //         }
            //     ]
            // }
        ],
        service_option: {
            service_object: AppServiceUtility.reservation_registration_service,
            operation_option: {
                query: {
                    func_name: "get_reservation_registration_list_all",
                    arguments: [{ id: this.props.match!.params.key }, 1, 1]
                },
            }
        },
        reach_func: (data: any) => {
            this.DataProcessing(data);
        }
    };
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
    }

    DataProcessing = (data: any) => {
        let input_props: any[] = [];
        if (data.result[0].family_name.length > 0) {
            for (let i = 0; i < data.result[0].family_name[0].length; i++) {
                input_props.push(
                    {
                        one_row_inputs: {
                            inputs_props: [
                                {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "家属姓名1",
                                    decorator_id: "title",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    },
                                    field_decorator_option: {
                                        initialValue: data.result[0].family_name[0][i].title
                                    }
                                }, {
                                    type: InputType.select,
                                    col_span: 8,
                                    label: "与长者关系",
                                    decorator_id: "contents",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    },
                                    field_decorator_option: {
                                        initialValue: data.result[0].family_name[0][i].relation
                                    }
                                }, {
                                    type: InputType.input_number,
                                    col_span: 8,
                                    label: "联系方式",
                                    decorator_id: "relation",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    },
                                    field_decorator_option: {
                                        initialValue: data.result[0].family_name[0][i].contents
                                    }
                                }
                            ]
                        }
                    }
                );
            }
        }
        this.edit_props.form_items_props.push(
            {
                title: "家属联系方式",
                need_card: true,
                input_props: input_props
            },
            {
                title: "护理评估",
                need_card: true,
                input_props: [
                    {
                        one_row_inputs: {
                            inputs_props: [
                                // {
                                //     type: InputType.select,
                                //     col_span: 8,
                                //     label: "房间要求",
                                //     decorator_id: "room_type_name",
                                //     layout: layout,
                                //     option: {
                                //         disabled: true
                                //     }
                                // }, 
                                {
                                    type: InputType.input_number,
                                    col_span: 8,
                                    label: "评估得分",
                                    decorator_id: "score",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                },
                            ]
                        }
                    }
                ]
            },
            {
                title: "其他信息",
                need_card: true,
                input_props: [
                    {
                        one_row_inputs: {
                            inputs_props: [
                                // {
                                //     type: InputType.antd_input,
                                //     col_span: 8,
                                //     label: "优先级",
                                //     decorator_id: "priority",
                                //     layout: layout,
                                //     option: {
                                //         disabled: true
                                //     }
                                // }, 
                                {
                                    type: InputType.antd_input,
                                    col_span: 8,
                                    label: "接待人",
                                    decorator_id: "receptionist",
                                    layout: this.layout,
                                    option: {
                                        disabled: true
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        );
        this.forceUpdate();
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
    }
    render() {
        // const dataSource: any[] = [];
        // const columns = [
        //     {
        //         title: '需求项目',
        //         dataIndex: 'demand_projects',
        //         key: 'demand_projects',
        //     },
        //     {
        //         title: '特殊要求',
        //         dataIndex: 'address',
        //         key: 'address',
        //     },
        //     {
        //         title: '状态',
        //         dataIndex: 'state',
        //         key: 'state',
        //     },
        // ];
        // let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...this.edit_props} />
                {/* <Card title='需求清单'>
                    <Table dataSource={dataSource} columns={columns} />
                </Card> */}
                <Row type='flex' justify='center'>
                    <Button onClick={() => { this.props.history!.push(ROUTE_PATH.reservationRegistration); }}>返回</Button>
                </Row>
            </MainContent>
        );
    }
}

/**
 * 控件：预约入院视图控件
 * 描述
 */
@addon('ChangeReservationView', '预约登记视图控件', '描述')
@reactControl(ChangeReservationView, true)
export class ChangeReservationViewControl extends ReactViewControl {

}