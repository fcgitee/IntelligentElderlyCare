import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { Select } from 'antd';
const { Option } = Select;
/**
 * 组件：预约登记状态
 */
export interface ReservationRegistrationViewState extends ReactViewState {
    /** 查询条件 */
    condition?: object;
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /*状态 */
    state?: any[];
    /** 接口名 */
    request_ulr?: string;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：预约登记
 * 描述
 */
export class ReservationRegistrationView extends ReactView<ReservationRegistrationViewControl, ReservationRegistrationViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'organization_name',
            key: 'organization_name',
        },
        {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '年龄',
            dataIndex: 'age',
            key: 'age',
        }, {
            title: '性别',
            dataIndex: 'sex',
            key: 'sex',
        }, {
            title: '接待人',
            dataIndex: 'receptionist',
            key: 'receptionist',
        }, {
            title: '状态',
            dataIndex: 'state',
            key: 'state',
        }, {
            title: '是否黑名单',
            dataIndex: 'is_blacklist',
            key: 'is_blacklist',
        }, {
            title: '预约日期',
            dataIndex: 'reservation_date',
            key: 'reservation_date',
        },
        {
            title: '评估得分',
            dataIndex: 'score',
            key: 'score',
        }, {
            title: '评估意见',
            dataIndex: 'preliminary_assessment[0].assessment_opinions',
            key: 'preliminary_assessment[0].assessment_opinions',
        },];
    private table_params = {
        // other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }, { type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        other_label_type: [{ type: 'icon', label_key: 'icon_align_center', label_parameter: { icon: 'antd@align-center' } }],
        showHeader: true,
        bordered: false,
        show_footer: true,
        rowKey: 'id',
    };
    constructor(props: ReservationRegistrationViewControl) {
        super(props);
        this.state = {
            condition: {},
            state: ["网上预约", "现场预约"],
            org_list: [],
        };
    }
    /** 新增按钮 */
    addBtn = () => {
        this.props.history!.push(ROUTE_PATH.changeReservationRegistration);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeReservationRegistration + '/' + contents.id);
        } else if ('icon_align_center' === type) {
            this.props.history!.push(ROUTE_PATH.reservationRegistrationEditor + '/' + contents.id);
        }
    }
    /** 分页回调事件 */
    pageOnCick = (contents: any) => {
        // console.log('page:', contents);
        this.setState({
            page: contents
        });
    }
    /** 改变分页数量回调事件 */
    showSizeChange = (current: number, pageSize: number) => {
        // console.log('current: ' + current + 'pageSize: ' + pageSize);
        this.setState({
            pageSize: pageSize
        });
    }
    componentWillMount() {
        let request_ulr = this.props.request_url;
        this.setState({
            request_ulr
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let role_permission = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "elder_name"
                }, {
                    type: InputType.input,
                    label: "接待人",
                    decorator_id: "receptionist"
                }, {
                    type: InputType.select,
                    label: "状态",
                    decorator_id: "state",
                    option: {
                        placeholder: "请选择状态",
                        showSearch: true,
                        childrens: this.state.state!.map((item, idx) => <Option key={idx} value={item}>{item}</Option>),
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name"
                // },
                // {
                //     type: InputType.date,
                //     label: "预约日期",
                //     decorator_id: "date_range"
                // }, 
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBtn,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.reservation_registration_service,
            service_option: {
                select: {
                    service_func: this.state.request_ulr,
                    service_condition: [this.state.condition, 1, 10]
                },
                delete: {
                    service_func: 'delete_reservation_registration'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let role_permission_list = Object.assign(role_permission, this.table_params);

        return (
            (
                <SignFrameLayout {...role_permission_list} />
            )
        );
    }
}

/**
 * 控件：预约登记
 * 描述
 */
@addon('ReservationRegistrationView', '预约登记', '描述')
@reactControl(ReservationRegistrationView, true)
export class ReservationRegistrationViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}