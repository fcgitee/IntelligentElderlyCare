import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：医疗费用页面状态
 */
export interface MedicalCostState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：医疗费用页面
 * 描述
 */
export class MedicalCostManageView extends ReactView<MedicalCostManageViewControl, MedicalCostState> {
    private columns_data_source = [{
        title: '开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '金额',
        dataIndex: 'money',
        key: 'money',
    }];
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeMedicalCost);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeMedicalCost + '/' + contents.id);
        }
    }

    render() {
        let service_provider = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "time_range"
                }, {
                    type: InputType.input,
                    label: "金额",
                    decorator_id: "monney"
                }
            ],
            btn_props: [{
                label: '新增医疗费用',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.cost_manage_service,
            service_option: {
                select: {
                    service_func: 'get_medical_cost_list',
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'delete_bed'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let service_provider_list = Object.assign(service_provider, table_param);
        return (
            <SignFrameLayout {...service_provider_list} />
        );
    }
}

/**
 * 控件：医疗费用页面
 * 描述
 */
@addon('MedicalCostManageView', '医疗费用页面', '描述')
@reactControl(MedicalCostManageView, true)
export class MedicalCostManageViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}