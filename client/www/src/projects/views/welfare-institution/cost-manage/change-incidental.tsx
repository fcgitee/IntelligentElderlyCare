import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

/**
 * 组件：杂费管理状态
 */
export interface ChangeIncidentalViewState extends ReactViewState {
    // 床位列表
    hotel_list: any;
    base_data: any;
    id?: any;
}

/**
 * 组件：杂费管理视图
 */
export class ChangeIncidentalView extends ReactView<ChangeIncidentalViewControl, ChangeIncidentalViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            hotel_list: [],
            base_data: {},
            id: ''
        };
    }
    componentDidMount() {
        console.info('获取id', this.props.match);
        // this.setState({ id: this.props.match!.params.key });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.incidentalList);
    }
    render() {
        const columns_data_source = [{
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '身份证号',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '性别',
            dataIndex: 'personnel_info.sex',
            key: 'personnel_info.sex',
        }, {
            title: '出生年月',
            dataIndex: 'personnel_info.date_birth',
            key: 'personnel_info.date_birth',
        }];
        const base_data = this.state.base_data,
            modal_search_items_props = {
                edit_form_items_props: [
                    {
                        type: ListInputType.input,
                        label: "长者名称",
                        decorator_id: "name"
                    },
                ],
                id_field: 'user_id',
                columns_data_source: columns_data_source,
                service_name: AppServiceUtility.check_in_service,
                service_func: 'get_check_in_list',
                title: '长者查询',
                select_option: {
                    placeholder: "请选择长者",
                    service_condition: [{}, 1, 10]

                }
            };
        let edit_props = {
            form_items_props: [
                {
                    title: "杂费",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者",
                            decorator_id: "user_id",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入服务项目参数" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者",
                                modal_search_items_props: modal_search_items_props

                            }
                        },
                        {
                            type: InputType.antd_input_number,
                            label: "金额",
                            decorator_id: "money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入金额" }],
                                initialValue: base_data ? base_data.money : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        }, {
                            type: InputType.date,
                            label: "开始时间",
                            decorator_id: "start_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择开始时间" }],
                                initialValue: base_data ? base_data.start_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择开始时间"
                            }
                        },
                        {
                            type: InputType.date,
                            label: "结束时间",
                            decorator_id: "end_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择结束时间" }],
                                initialValue: base_data ? base_data.end_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择结束时间"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.incidentalList);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.cost_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_incidental"
                    },
                    query: {
                        func_name: "get_incidental_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.incidentalList); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：杂费管理编辑控件
 * @description 杂费管理编辑控件
 * @author
 */
@addon('ChangeIncidentalView', '杂费管理编辑控件', '杂费管理编辑控件')
@reactControl(ChangeIncidentalView, true)
export class ChangeIncidentalViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}