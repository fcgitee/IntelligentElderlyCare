import { Row, Collapse, Select } from "antd";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { edit_props_info } from "src/projects/app/util-tool";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType as InputTypeNew } from "src/business/components/buss-components/form-creator";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
const { Panel } = Collapse;
let { Option } = Select;

/**
 * 组件：费用类型状态
 */
export interface CostTypeViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 长者姓名 */
    data_base: any;
    /** 当前组织机构信息 */
    cur_org: any;
    /** 组织机构列表 */
    org_list: any;

}

/**
 * 组件：费用类型
 */
export class CostTypeView extends ReactView<CostTypeViewControl, CostTypeViewState> {

    private columns_data_source = [{
        title: '所属组织',
        dataIndex: 'org.name',
        key: 'org.name',
    }, {
        title: '费用类型',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '描述',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            data_base: {},
            cur_org: {},
            org_list: []
        };
    }
    componentWillMount() {
        AppServiceUtility.person_org_manage_service.get_cur_org_info!()!
            .then((data) => {
                this.setState({
                    cur_org: data[0],
                });
            });
        request(this, AppServiceUtility.person_org_manage_service.get_organization_list_all!({}))
            .then((datas: any) => {
                let org_list: any[] = [];
                datas.result!.map((item: any, idx: number) => {
                    org_list.push(<Option key={item['id']}>{item['name']}</Option>);
                });
                this.setState({
                    org_list
                });
            });

        this.setState({
            request_url: this.props.request_url
        });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.costType);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        'icon_edit' === type && this.props.history!.push(ROUTE_PATH.costType + '/' + contents.id);
    }
    render() {
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "费用类型",
                    decorator_id: "name",
                }
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.cost_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_cost_type'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);

        // 费用类型
        let edit_props = {
            form_items_props: [
                {
                    input_props: [
                        {
                            type: InputTypeNew.select,
                            label: "组织机构",
                            decorator_id: "organization_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入组织机构" }],
                                initialValue: this.state.data_base.organization_id ? this.state.data_base.organization_id : this.state.cur_org.id,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入组织机构",
                                disabled: true,
                                childrens: this.state.org_list,
                            }
                        },
                        {
                            type: InputTypeNew.antd_input,
                            label: "费用类型",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择费用类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择费用类型",
                            }
                        },
                        {
                            type: InputTypeNew.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                        },
                    ]
                }
            ],
            other_btn_propps: [],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.cost_service,
                operation_option: {
                    save: {
                        func_name: "update_cost_type"
                    },
                    query: {
                        func_name: 'get_cost_type',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            id: this.props.match!.params.key,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.costType); },

        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <Row>
                <MainContent>
                    <div className='form-bottom'>
                        <Collapse defaultActiveKey={['1']}>
                            <Panel header="费用类型" key="1">
                                <FormCreator {...edit_props_list} />
                            </Panel>
                        </Collapse>
                    </div>
                </MainContent>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：费用类型
 * 描述
 */
@addon('CostTypeView', '费用类型', '描述')
@reactControl(CostTypeView, true)
export class CostTypeViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}