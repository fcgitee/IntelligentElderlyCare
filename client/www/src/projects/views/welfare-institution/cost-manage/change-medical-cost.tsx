import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "src/projects/app/util-tool";

/**
 * 组件：医疗费用编辑状态
 */
export interface MedicalCostViewState extends ReactViewState {
    // 床位列表
    hotel_list: any;
    base_data: any;
}

/**
 * 组件：医疗费用编辑视图
 */
export class MedicalCostView extends ReactView<MedicalCostViewControl, MedicalCostViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            hotel_list: [],
            base_data: {}
        };
    }
    componentDidMount() {

    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.medicalCost);
    }
    render() {
        const base_data= this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "医疗费用",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input_number,
                            label: "金额",
                            decorator_id: "money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入金额" }],
                                initialValue: base_data ? base_data.water_number : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        }, {
                            type: InputType.date,
                            label: "开始时间",
                            decorator_id: "start_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择开始时间" }],
                                initialValue: base_data ? base_data.start_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择开始时间"
                            }
                        },
                        {
                            type: InputType.date,
                            label: "结束时间",
                            decorator_id: "end_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择结束时间" }],
                                initialValue: base_data ? base_data.end_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择结束时间"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.medicalCost);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.cost_manage_service,
                operation_option: {
                    save: {
                        func_name: "update_medical_cost"
                    },
                    query: {
                        func_name: "get_medical_cost_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.medicalCost); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：医疗费用编辑编辑控件
 * @description 医疗费用编辑编辑控件
 * @author
 */
@addon('MedicalCostView', '医疗费用编辑控件', '医疗费用编辑控件')
@reactControl(MedicalCostView, true)
export class MedicalCostViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}