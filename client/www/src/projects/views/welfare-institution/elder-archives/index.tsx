
import { ReactViewState, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";

/** 状态：长者档案视图 */
export interface ElderArchivesViewState extends ReactViewState {
    /** 当前第几页 */
    page?: number;
    /** 当前每页数 */
    pageSize?: number;
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
}
/** 组件：长者档案视图 */
export class ElderArchivesView extends React.Component<ElderArchivesViewControl, ElderArchivesViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }, {
        title: '户籍',
        dataIndex: 'household_register',
        key: 'household_register',
    }, {
        title: '居住地或单位',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '护理级别',
        dataIndex: 'nursing_level',
        key: 'nursing_level',
    },
    {
        title: '楼宇(房间)',
        dataIndex: 'room',
        key: 'room',
    }, {
        title: '入住时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '退住时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }, {
        title: '家属联系电话',
        dataIndex: 'mobile',
        key: 'mobile',
    }, {
        title: '职业',
        dataIndex: 'job',
        key: 'job',
    }, {
        title: '政治面貌',
        dataIndex: 'political_status',
        key: 'political_status',
    }, {
        title: '宗教',
        dataIndex: 'religion',
        key: 'religion',
    }, {
        title: '退休情况',
        dataIndex: 'retirement',
        key: 'retirement',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }, {
        title: '银行名称',
        dataIndex: 'bank_name',
        key: 'bank_name',
    }, {
        title: '代扣名',
        dataIndex: 'bank_host',
        key: 'bank_host',
    }, {
        title: '银行帐号',
        dataIndex: 'bank_number',
        key: 'bank_number',
    }, {
        title: '押金',
        dataIndex: 'deposit',
        key: 'deposit',
    }, {
        title: '合同签注日期',
        dataIndex: 'contract_start_date',
        key: 'contract_start_date',
    }, {
        title: '合同到期日期',
        dataIndex: 'contract_end_date',
        key: 'contract_end_date',
    }, {
        title: '状态',
        dataIndex: 'statue',
        key: 'statue',
    }];
    constructor(props: ElderArchivesViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {}
        };
    }
    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.archivesEditor);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.archivesEditor + '/' + contents.id);
        }
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let elder_archives = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "编号",
                    decorator_id: "code"
                }, {
                    type: InputType.input,
                    label: "姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "身份证号",
                    decorator_id: "id_card"
                },
            ],
            btn_props: [{
                label: '新增档案',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.nursing_service,
            service_option: {
                select: {
                    service_func: 'get_nursing_list',
                    service_condition: [this.state.condition, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
        };
        let elder_archives_list = Object.assign(elder_archives, table_param);
        return (
            <SignFrameLayout {...elder_archives_list} />
        );
    }
}

/**
 * 控件：长者档案视图控制器
 * @description 长者档案视图
 * @author
 */
@addon('ElderArchivesView', '长者档案视图', '长者档案视图')
@reactControl(ElderArchivesView, true)
export class ElderArchivesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}