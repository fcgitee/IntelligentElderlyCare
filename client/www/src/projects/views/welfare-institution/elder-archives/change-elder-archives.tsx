import { ReactViewState, reactControl, ReactViewControl, ReactView } from "pao-aop-client";
import React from "react";
import { addon, Permission, } from "pao-aop";
import { edit_props_info } from "../../../app/util-tool";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
/**
 * 组件：长者档案状态
 */
export interface ChangeElderArchivesViewState extends ReactViewState {

    /** 滚动条 */
    loading?: boolean;
    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：长者档案视图
 */
export class ChangeElderArchivesView extends ReactView<ChangeElderArchivesViewControl, ChangeElderArchivesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            base_data: {},
            id: '',
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.nursingArchives);
    }

    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let base_data = this.state.base_data;

        let edit_props = {
            form_items_props: [
                {
                    title: "档案信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "护理类别",
                            decorator_id: "nursing_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入护理类别" }],
                                initialValue: base_data ? base_data.nursing_type : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入护理类别"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "说明",
                            decorator_id: "Description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入说明" }],
                                initialValue: base_data ? base_data.Description : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入说明"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "费用",
                            decorator_id: "fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入费用" }],
                                initialValue: base_data ? base_data.fee : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入费用"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "状态",
                            decorator_id: "state",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入状态" }],
                                initialValue: base_data ? base_data.state : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入状态"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.nursingArchives);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.nursing_service,
                operation_option: {
                    save: {
                        func_name: "update"
                    },
                    query: {
                        func_name: "get_nursing_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingArchives); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：长者档案编辑控件
 * @description 长者档案编辑控件
 * @author
 */
@addon('ChangeElderArchivesView', '长者档案编辑控件', '长者档案编辑控件')
@reactControl(ChangeElderArchivesView, true)
export class ChangeElderArchivesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}