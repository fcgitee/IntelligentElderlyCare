import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, IPermissionService, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "../../../app/util-tool";
// import { Select } from 'antd';
// let { Option } = Select;
/**
 * 组件：护理档案状态
 */
export interface ChangeNursingItemViewState extends ReactViewState {

    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：护理档案视图
 */
export class ChangeNursingItemView extends ReactView<ChangeNursingItemViewControl, ChangeNursingItemViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: ''
        };
    }
    /** 权限服务 */
    permissionService?() {
        return getObject(this.props.permissionService_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.nursingItem);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "护理项目设置",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "项目名称",
                            decorator_id: "nursing_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入项目名称" }],
                                initialValue: base_data ? base_data.nursing_type : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入项目名称"
                            }
                        }, {
                            type: InputType.select,
                            label: "项目类型",
                            decorator_id: "service_provider_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择项目类型" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择项目类型",
                                childrens: [],
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "关联费用项目",
                            decorator_id: "fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入关联费用项目" }],
                                initialValue: base_data ? base_data.fee : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入关联费用项目"
                            }
                        }, {
                            type: InputType.select,
                            label: "执法部门",
                            decorator_id: "state",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择执法部门" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择执法部门",
                                childrens: []
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "单价",
                            decorator_id: "fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入单价" }],
                                initialValue: base_data ? base_data.state : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入单价"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "单位",
                            decorator_id: "unit",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择单位" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择单位",
                                childrens: [],
                                showSearch: true
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入备注" }],
                                initialValue: base_data.remark ? base_data.remark : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                rows: 4
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.nursingItem);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.nursing_item_service,
                operation_option: {
                    save: {
                        func_name: "update"
                    },
                    query: {
                        func_name: "get_nursing_item_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingItem); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：护理档案编辑控件
 * @description 护理档案编辑控件
 * @author
 */
@addon('ChangeNursingItemView', '护理项目档案编辑控件', '护理项目档案编辑控件')
@reactControl(ChangeNursingItemView, true)
export class ChangeNursingItemViewControl extends ReactViewControl {
    /** 权限服务接口 */
    public permissionService_Fac?: Ref<IPermissionService>;
    /** 视图权限 */
    public permission?: Permission;

}