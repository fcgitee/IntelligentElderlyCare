
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：护理档案视图 */
export interface NursingItemViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
}
/** 组件：护理档案视图 */
export class NursingItemView extends React.Component<NursingItemViewControl, NursingItemViewState> {
    private columns_data_source = [{
        title: '项目名称',
        dataIndex: 'nursing_type',
        key: 'nursing_type',
    }, {
        title: '项目类型',
        dataIndex: 'Description',
        key: 'Description',
    }, {
        title: '关系费用项',
        dataIndex: 'fee',
        key: 'fee',
    }, {
        title: '备注',
        dataIndex: 'state',
        key: 'state',
    }];
    constructor(props: NursingItemViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {}
        };
    }

    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.nursingItemEdit);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.nursingItemEdit + '/' + contents.id);
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let nursing = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "服务商名称",
                    decorator_id: "nursing_type"
                }, {
                    type: InputType.input,
                    label: "电话",
                    decorator_id: "Description"
                }, {
                    type: InputType.input,
                    label: "交易乙方",
                    decorator_id: "fee"
                }
            ],
            btn_props: [{
                label: '新增档案',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.nursing_item_service,
            service_option: {
                select: {
                    service_func: 'get_nursing_item_list',
                    service_condition: []
                },
                delete: {
                    service_func: 'delete'
                }
            },
        };
        let nursing_list = Object.assign(nursing, table_param);
        return (
            <SignFrameLayout {...nursing_list} />
        );
    }
}

/**
 * 控件：护理档案视图控制器
 * @description 护理档案视图
 * @author
 */
@addon('NursingItemView', '护理档案视图', '护理档案视图')
@reactControl(NursingItemView, true)
export class NursingItemViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}