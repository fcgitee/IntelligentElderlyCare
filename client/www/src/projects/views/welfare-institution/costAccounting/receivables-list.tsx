import { Row, Modal, Form, Radio, message } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request_func, exprot_excel } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { DatePicker, Select } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
import moment from 'moment';
const { Option } = Select;
const { RangePicker, MonthPicker } = DatePicker;

// import { ROUTE_PATH } from "src/projects/router";
// const Option = Select.Option;
/**
 * 组件：应收款明细表状态
 */
export interface ReceivablesViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
    /** 弹窗是否显示 */
    visible: boolean;
    /** 是否核算全部长者 */
    isAll: boolean;
    /** 核算长者id */
    elder: any;
    /** 核算时间 */
    date: any;
    /** 费用项目 */
    columns_data_source: Array<object>;
    /** 审核弹窗 */
    check_status: boolean;
    /** 发送弹窗 */
    send_status: boolean;
    /** 是否审核全部 */
    IsCheckAll: boolean;
    /** 审批长者 */
    check_elder: Array<string>;
    /** 导出弹窗显示 */
    download_status: boolean;
    /** 打印弹窗显示 */
    print_status: boolean;
    /** 审核状态 */
    checkStatus: Array<string>;
    user_tpyes: Array<string>;
    down_condition: object;
    product_list: Array<object>; // 产品数据
    personnel_classification_list: any;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：应收款明细表
 */
export class ReceivablesView extends ReactView<ReceivablesViewControl, ReceivablesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            visible: false,
            check_status: false,
            send_status: false,
            isAll: true,
            IsCheckAll: true,
            check_elder: [],
            elder: [],
            date: [],
            columns_data_source: [],
            download_status: false,
            print_status: false,
            checkStatus: ['通过', '待审核'],
            user_tpyes: ['自费', '补贴'],
            product_list: [],
            personnel_classification_list: [],
            down_condition: {
                type: '预收',
                date: moment().format('YYYY-MM')
            },
            org_list: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 产品列表
        request_func(this, AppServiceUtility.service_item_package.get_service_product_list, [{}], (data: any) => {
            let cloums: Array<object> = [];
            this.setState({ product_list: data });
            data!.map((val: any) => {
                console.info(val);
                val.is_service_item === 'true' && cloums.push({ title: val.name + '/元', dataIndex: val.id, key: val.id });
            });
            let columns_data_source = [{
                title: '开始时间',
                dataIndex: 'start_date',
                key: 'start_date',
            }, {
                title: '结束时间',
                dataIndex: 'end_date',
                key: 'end_date',
            }
                , {
                title: '长者姓名',
                dataIndex: 'user.name',
                key: 'user.name',
            }, {
                title: '身份证',
                dataIndex: 'user.id_card',
                key: 'user.id_card',
            }, {
                title: '性别',
                dataIndex: 'user.personnel_info.sex',
                key: 'user.sex',
            }, {
                title: '户籍',
                dataIndex: 'user.sex',
                key: 'user.sex',
            },
            // {
            // title: '床位',
            // dataIndex: 'bed.name',
            // key: 'user.sex',
            // },
            {
                title: '长者类别',
                dataIndex: 'class.name',
                key: 'user.name',
            },
            //     {
            //     title: '入住日期',
            //     dataIndex: 'check_in.create_date',
            //     key: 'check_in.create_date',
            // },
            ...cloums,
            {
                title: '审核状态',
                dataIndex: 'check_status',
                key: 'check_status',
            },
            {
                title: '上月差额',
                dataIndex: 'balance',
                key: 'balance',
            },
            {
                title: '合计费用/元',
                dataIndex: 'total',
                key: 'total'
            },
            ];
            this.setState({ columns_data_source });
        });
        // 人员类别
        request_func(this, AppServiceUtility.person_org_manage_service.get_personnel_classification_list, [{}], (data: any) => {
            this.setState({
                personnel_classification_list: data!.result
            });
        });
    }
    // 核算
    cost_func = () => {
        if (!this.state.date.length) {
            message.info('请选择核算时间');
            return;
        }
        const condition = this.state.isAll ? [] : this.state.elder;
        this.setState({ visible: false });
        request_func(this, AppServiceUtility.cost_accounting_service.add_receivables_data, [condition, this.state.date], (data: any) => {
            alert(data);
            this.goback();
        });
    }
    goback = () => {
        this.props!.history!.push(ROUTE_PATH.receivablesList);
    }
    /** 自定义图标按钮回调事件 */
    on_row_selection = (value: any) => {
        let elder: Array<string> = value.map((item: any) => {
            return item.id;
        });
        this.setState({ elder });
    }
    // 审核
    onIconClick = (row: string, content: any) => {
        this.setState({ check_status: true, check_elder: [content.user_id] });
    }
    send_func = () => {
        this.setState({ send_status: false });
        request_func(this, AppServiceUtility.cost_accounting_service.send_receivables_data, []);
    }
    check_func = (condition: Array<string>) => {
        this.setState({ check_status: false });
        request_func(this, AppServiceUtility.cost_accounting_service.check_bill, [this.state.check_elder], (data: any) => {
            alert(data);
            this.goback();
        });
    }
    /** 导出 */
    download_func = () => {
        request_func(this, AppServiceUtility.cost_accounting_service.bill_down, [this.state.down_condition], (data: any) => {
            if (!data!.length) {
                message.info('暂无数据');
                return;
            }
            data.map((val: any, idx: any) => {
                for (var y of this.state.product_list) {
                    for (var x of val.recode) {
                        if (y['is_service_item'] === 'true') {
                            val[y['name']] = x.service_product_id === y['id'] ? x.valuation_amount : 0;
                        }
                    }
                }
                delete val.recode;
                delete val.balance;
            });
            this.setState({ download_status: false });
            exprot_excel([{ name: '预收款账单', value: data }], '预收款账单', 'xls');
        });
    }
    time_change = (date: any, dateString: string) => {
        console.info(date, dateString);
        let condition = this.state.down_condition;
        let down_condition = Object.assign(condition, { date: dateString });
        this.setState({ down_condition });
    }
    onChangeTime = (value: any, dateString: any) => {
        this.setState({ date: dateString });
    }
    set_down_condition = (param: any) => {
        let condition = this.state.down_condition;
        let down_condition = Object.assign(condition, param);
        this.setState({ down_condition });
    }
    render() {
        let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        let info_list = {
            type_show: false,
            showHeader: true,
            bordered: false,
            show_footer: true,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name",
                },
                {
                    type: InputType.select,
                    label: "长者类别",
                    decorator_id: "user_type",
                    option: {
                        childrens: personnel_classification_list,
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date"
                }, {
                    type: InputType.select,
                    label: "审核状态",
                    decorator_id: "check_status",
                    option: {
                        placeholder: "请选择审核状态",
                        childrens: this.state.checkStatus!.map(
                            (item: any, index: any) => <Option key={index} value={item}>{item}</Option>),
                    }

                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                // },
            ],
            btn_props: [{
                label: '生成账单',
                btn_method: () => { this.setState({ visible: true }); },
                icon: 'plus'
            }, {
                label: '批量审核',
                btn_method: () => { this.setState({ check_status: true, check_elder: [] }); },
                icon: 'plus'
            }, {
                label: '发送账单',
                btn_method: () => { this.setState({ send_status: true }); },
                icon: 'plus'
            },
            {
                label: '导出账单',
                btn_method: () => { this.setState({ download_status: true }); },
                icon: 'plus'
            },
            {
                label: '打印',
                btn_method: () => { this.setState({ print_status: true }); },
                icon: 'plus'
            }
            ],
            other_label_type: [{ type: 'button', label_key: 'check', label_text: '审核' }],
            on_icon_click: this.onIconClick,
            columns_data_source: this.state.columns_data_source,
            service_object: AppServiceUtility.cost_accounting_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        // table_param.other_label_type = [{ type: 'button', label_key: 'check', label_text: '审核' }];
        // let info_list = Object.assign(json_info, table_param);
        // 弹窗数据
        const data_source = [{
            title: '证件号码',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '年龄',
            dataIndex: 'personnel_info.age',
            key: 'age',
        }, {
            title: '性别',
            dataIndex: 'personnel_info.sex',
            key: 'personnel_info.sex',
        },
            // {
            //     title: '入住时间',
            //     dataIndex: 'create_date',
            //     key: 'create_date',
            //     },
        ];
        let nursing = {
            type_show: false,
            columns_data_source: data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_elder_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                },
            ],
            permission_class: AppServiceUtility.login_service,
            get_permission_name: 'get_function_list',
            rowKey: 'id',
            on_row_selection: this.on_row_selection

        };
        let nursing_list = Object.assign(nursing, table_param);
        // const { getFieldDecorator } = this.props.form!;
        return (
            <Row>
                <SignFrameLayout {...info_list} />
                <Modal
                    title="费用核算"
                    visible={this.state.visible}
                    onOk={this.cost_func}
                    okText='确定'
                    cancelText='取消'
                    onCancel={() => { this.setState({ visible: false }); }}
                    width={1000}
                >
                    <Row type={"flex"} justify={"center"}><Form.Item>
                        核算时间段：<RangePicker
                            placeholder={['开始时间', '结束时间']}
                            format={'YYYY-MM-DD'}
                            onChange={this.onChangeTime}
                        />
                    </Form.Item></Row>
                    <Row type={"flex"} justify={"center"}>
                        <Radio.Group style={{ margin: '0 20px' }} value={this.state.isAll} onChange={(e: any) => { this.setState({ isAll: e.target.value }); }}>
                            <Radio value={true} defaultChecked={true}>对长者进行批量核算</Radio>
                            <Radio value={false}>选择单个长者进行核算</Radio>
                        </Radio.Group></Row>
                    <div style={{ "display": (!this.state.isAll ? 'block' : 'none') }}>
                        <SignFrameLayout {...nursing_list} />
                    </div>
                    {/* {!this.state.isAll && <SignFrameLayout {...nursing_list} />} */}
                </Modal>
                {
                    this.state.check_status && <Modal
                        title="账单审核"
                        visible={this.state.check_status}
                        onOk={() => { this.check_func([]); }}
                        okText='确定'
                        cancelText='取消'
                        onCancel={() => { this.setState({ check_status: false }); }}
                    >
                        <div>对所选长者账单审批通过</div>
                    </Modal>
                }
                {
                    this.state.send_status && <Modal
                        title="账单发送"
                        visible={this.state.send_status}
                        onOk={this.send_func}
                        okText='确定'
                        cancelText='取消'
                        onCancel={() => { this.setState({ send_status: false }); }}
                    >
                        <div>确定发送已审批通过的账单？</div>
                    </Modal>
                }
                {
                    this.state.download_status && <Modal
                        title="账单导出"
                        visible={this.state.download_status}
                        onOk={() => { this.download_func(); }}
                        okText='确定'
                        cancelText='取消'
                        onCancel={() => { this.setState({ download_status: false }); }}
                    >
                        时间：<MonthPicker onChange={this.time_change} defaultValue={moment(moment(), 'YYYY-MM')} format={'YYYY-MM'} />
                        <br />
                    审核状态：
                        <Radio.Group style={{ margin: '0 20px' }} onChange={(e: any) => { this.set_down_condition({ check_status: e.target.value }); }}>
                            <Radio value={'通过'} defaultChecked={true}>通过</Radio>
                            <Radio value={'待审核'}>待审核</Radio>
                        </Radio.Group>
                        <br />
                    发送状态：<Radio.Group style={{ margin: '0 20px' }} onChange={(e: any) => { this.set_down_condition({ send_status: e.target.value }); }}>
                            <Radio value={'已发送'} defaultChecked={true}>已发送</Radio>
                            <Radio value={'未发送'}>未发送</Radio>
                        </Radio.Group>
                    </Modal >
                }
            </Row >
        );
    }
}

/**
 * 控件：应收款明细表
 * 描述
 */
@addon('ReceivablesView', '应收款明细表', '描述')
@reactControl(Form.create<any>()(ReceivablesView), true)
export class ReceivablesViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}