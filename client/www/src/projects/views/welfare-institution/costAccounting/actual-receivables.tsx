import { Row, Modal, Form, Radio, message, Input } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { DatePicker, Select } from 'antd';
import { ROUTE_PATH } from "src/projects/router";
const { Option } = Select;

const { RangePicker } = DatePicker;
// import { ROUTE_PATH } from "src/projects/router";
// const Option = Select.Option;
/**
 * 组件：实收款明细表状态
 */
export interface ActualReceivablesViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
    /** 弹窗是否显示 */
    visible: boolean;
    /** 是否核算全部长者 */
    isAll: boolean;
    /** 核算长者id */
    elder: any;
    /** 核算时间 */
    date: any;
    /** 费用项目 */
    columns_data_source: Array<object>;
    /** 审核弹窗 */
    check_status: boolean;
    /** 发送弹窗 */
    send_status: boolean;
    /** 是否审核全部 */
    IsCheckAll: boolean;
    /** 审批长者 */
    check_elder: Array<string>;
    /** 审核长者信息 */
    check_info: any;
    /** 单个长者审核弹窗 */
    check_alone_status?: boolean;
    personnel_classification_list: any;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：实收款明细表
 */
export class ActualReceivablesView extends ReactView<ActualReceivablesViewControl, ActualReceivablesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            visible: false,
            check_status: false,
            send_status: false,
            isAll: true,
            IsCheckAll: true,
            check_elder: [],
            elder: [],
            date: [],
            columns_data_source: [],
            check_info: { name: '', user_id: '', reduction: 0 },
            check_alone_status: false,
            personnel_classification_list: [],
            org_list: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 人员类别
        request_func(this, AppServiceUtility.person_org_manage_service.get_personnel_classification_list, [{}], (data: any) => {
            this.setState({
                personnel_classification_list: data!.result
            });
        });
        request_func(this, AppServiceUtility.service_item_package.get_service_product_list, [{}], (data: any) => {
            let cloums: Array<object> = [];
            data!.map((val: any) => {
                console.info(val);
                val.is_service_item === 'true' && cloums.push({ title: val.name + '/元', dataIndex: val.id, key: val.id });
            });
            let columns_data_source = [{
                title: '开始时间',
                dataIndex: 'start_date',
                key: 'start_date',
            }, {
                title: '结束时间',
                dataIndex: 'end_date',
                key: 'end_date',
            }
                , {
                title: '长者姓名',
                dataIndex: 'user.name',
                key: 'user.name',
            }, {
                title: '身份证',
                dataIndex: 'user.id_card',
                key: 'user.id_card',
            }, {
                title: '性别',
                dataIndex: 'user.personnel_info.sex',
                key: 'user.sex',
            }, {
                title: '户籍',
                dataIndex: 'user.sex',
                key: 'user.sex',
            },
            // {
            // title: '床位',
            // dataIndex: 'bed.name',
            // key: 'user.sex',
            // },
            {
                title: '长者类别',
                dataIndex: 'user.sex',
                key: 'user.sex',
            },
            //     {
            //     title: '护理等级',
            //     dataIndex: 'user.sex',
            //     key: 'user.sex',
            //     },
            //     {
            //     title: '入住日期',
            //     dataIndex: 'check_in.create_date',
            //     key: 'check_in.create_date',
            // },
            ...cloums,
            {
                title: '审核费用补充',
                dataIndex: 'reduction',
                key: 'reduction',
            },
            {
                title: '审核状态',
                dataIndex: 'check_status',
                key: 'check_status',
            },
            {
                title: '合计费用/元',
                dataIndex: 'total',
                key: 'total'
            },
            {
                title: '组织机构',
                dataIndex: 'org_name',
                key: 'org_name'
            },
            ];
            this.setState({ columns_data_source });
        });
    }
    // 核算
    cost_func = () => {
        if (!this.state.date.length) {
            message.info('请选择核算时间');
            return;
        }
        const condition = this.state.isAll ? [] : this.state.elder;
        // this.setState({ visible: false });
        request_func(this, AppServiceUtility.cost_accounting_service.add_actual_receivables_data, [condition, this.state.date], (data: any) => {
            alert(data);
            this.goback();
        });
    }
    /** 自定义图标按钮回调事件 */
    on_row_selection = (value: any) => {
        let elder: Array<string> = value.map((item: any) => {
            return item.user_id;
        });
        this.setState({ elder });
    }
    // 审核
    onIconClick = (row: string, content: any) => {
        this.setState({ check_alone_status: true, check_elder: [content.user_id], check_info: { name: content.user.name } });
    }

    check_func = (condition: Array<string>) => {
        this.setState({ check_status: false });
        request_func(this, AppServiceUtility.cost_accounting_service.check_bill_actual, [this.state.check_elder]);
    }
    // 核减单个长者
    check_func_alone = () => {
        this.setState({ check_alone_status: false });
        request_func(this, AppServiceUtility.cost_accounting_service.check_bill_actual, [this.state.check_elder, { ...this.state.check_info }], (data: any) => {
            alert(data);
            this.props!.history!.push(ROUTE_PATH.actualReceivablesList);
        });
    }
    goback = () => {
        this.props!.history!.push(ROUTE_PATH.actualReceivablesList);
    }
    onChangeTime = (value: any, dateString: any) => {
        this.setState({ date: dateString });
    }
    handleTitleChange = (e: any) => {
        let check_info = this.state.check_info;
        check_info.reduction = Number(e.target.value);
        this.setState({ check_info });
        console.info(e.target.value);
    }
    handleRemarkChange = (e: any) => {
        let check_info = this.state.check_info;
        check_info.remark = e.target.value;
        this.setState({ check_info });
    }
    render() {
        let personnel_classification_list: JSX.Element[] = this.state.personnel_classification_list.map((value: any) => {
            return (
                <Option key={value.id}>{value.name}</Option>
            );
        });
        const checkStatus = ['通过', '待审核'];
        let info_list = {
            type_show: false,
            showHeader: true,
            bordered: false,
            show_footer: true,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name",
                },
                {
                    type: InputType.select,
                    label: "长者类别",
                    decorator_id: "user_type",
                    option: {
                        childrens: personnel_classification_list
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date"
                }, {
                    type: InputType.select,
                    label: "审核状态",
                    decorator_id: "check_status",
                    option: {
                        placeholder: "请选择审核状态",
                        childrens: checkStatus!.map(
                            (item: any, index: any) => <Option key={index} value={item}>{item}</Option>),
                    }

                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                // },
            ],
            btn_props: [{
                label: '核算',
                btn_method: () => { this.setState({ visible: true }); },
                icon: 'plus'
            }, {
                label: '批量审核',
                btn_method: () => { this.setState({ check_status: true, check_elder: [] }); },
                icon: 'plus'
            }],
            other_label_type: [{ type: 'button', label_key: 'check', label_text: '审核' }],
            on_icon_click: this.onIconClick,
            columns_data_source: this.state.columns_data_source,
            service_object: AppServiceUtility.cost_accounting_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        // table_param.other_label_type = [{ type: 'button', label_key: 'check', label_text: '审核' }];
        // let info_list = Object.assign(json_info, table_param);
        // 弹窗数据
        const data_source = [{
            title: '证件号码',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '年龄',
            dataIndex: 'personnel_info.age',
            key: 'age',
        }, {
            title: '性别',
            dataIndex: 'personnel_info.sex',
            key: 'personnel_info.sex',
        },
            // {
            //     title: '入住时间',
            //     dataIndex: 'create_date',
            //     key: 'create_date',
            // },
        ];
        let nursing = {
            type_show: false,
            columns_data_source: data_source,
            service_object: AppServiceUtility.person_org_manage_service,
            service_option: {
                select: {
                    service_func: 'get_elder_list_all',
                    service_condition: [{}, 1, 10]
                },
            },
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                },
            ],
            permission_class: AppServiceUtility.login_service,
            get_permission_name: 'get_function_list',
            rowKey: 'user_id',
            on_row_selection: this.on_row_selection

        };
        let nursing_list = Object.assign(nursing, table_param);
        // const { getFieldDecorator } = this.props.form!;
        return (
            <Row>
                <SignFrameLayout {...info_list} />
                {this.state.visible && <Modal
                    title="费用核算"
                    visible={this.state.visible}
                    onOk={this.cost_func}
                    okText='确定'
                    cancelText='取消'
                    onCancel={() => { this.setState({ visible: false }); }}
                    width={1000}
                >
                    <Row type={"flex"} justify={"center"}><Form.Item>
                        核算时间段：<RangePicker
                            placeholder={['开始时间', '结束时间']}
                            format={'YYYY-MM-DD'}
                            onChange={this.onChangeTime}
                        />
                    </Form.Item></Row>
                    <Row type={"flex"} justify={"center"}>
                        <Radio.Group style={{ margin: '0 20px' }} onChange={(e: any) => { this.setState({ isAll: e.target.value }); }}>
                            <Radio value={true} defaultChecked={true}>对长者进行批量核算</Radio>
                            <Radio value={false}>选择单个长者进行核算</Radio>
                        </Radio.Group></Row>
                    {!this.state.isAll && <SignFrameLayout {...nursing_list} />}
                </Modal>}
                <Modal
                    title="账单审核"
                    visible={this.state.check_status}
                    onOk={() => { this.check_func([]); }}
                    okText='确定'
                    cancelText='取消'
                    onCancel={() => { this.setState({ check_status: false }); }}
                >
                    <div>对所选长者账单审批通过</div>
                </Modal>
                {this.state.check_alone_status && <Modal
                    title="审核"
                    visible={this.state.check_alone_status}
                    onOk={() => { this.check_func_alone(); }}
                    okText='确定'
                    width={500}
                    cancelText='取消'
                    onCancel={() => { this.setState({ check_alone_status: false }); }}
                >
                    <Row type={"flex"} justify={"center"}><Form.Item>
                        长者姓名：{this.state.check_info.name ? this.state.check_info.name : ''}
                    </Form.Item></Row>
                    <Row type={"flex"} justify={"center"}>
                        核增/减：
                        <Form.Item>
                            <Input width={10} placeholder="请输入金额" onChange={this.handleTitleChange} />
                        </Form.Item>
                    </Row>
                    <Row type={"flex"} justify={"center"}>
                        备注：
                        <Form.Item>
                            <Input width={10} placeholder="请输入备注" onChange={this.handleRemarkChange} />
                        </Form.Item>
                    </Row>
                </Modal>}
            </Row>
        );
    }
}

/**
 * 控件：实收款明细表
 * 描述
 */
@addon('ActualReceivablesView', '实收款明细表', '描述')
@reactControl(Form.create<any>()(ActualReceivablesView), true)
export class ActualReceivablesViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}