import { Row, Form } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
// import { ROUTE_PATH } from "src/projects/router";
// const Option = Select.Option;
/**
 * 组件：差额明细表状态
 */
export interface DifferenceDetailsViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
    /** 弹窗是否显示 */
    visible: boolean;
    /** 是否核算全部长者 */
    isAll: boolean;
    /** 核算长者id */
    elder: any;
    /** 核算时间 */
    date: any;
    /** 费用项目 */
    columns_data_source: Array<object>;
    /** 审核弹窗 */
    check_status: boolean;
    /** 发送弹窗 */
    send_status: boolean;
    /** 是否审核全部 */
    IsCheckAll: boolean;
    /** 审批长者 */
    check_elder: Array<string>;
    /** 审核长者信息 */
    check_info: any;
    /** 单个长者审核弹窗 */
    check_alone_status?: boolean;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：差额明细表
 */
export class DifferenceDetailsView extends ReactView<DifferenceDetailsViewControl, DifferenceDetailsViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            visible: false,
            check_status: false,
            send_status: false,
            isAll: true,
            IsCheckAll: true,
            check_elder: [],
            elder: [],
            date: [],
            columns_data_source: [],
            check_info: { name: '', user_id: '', reduction: 0 },
            check_alone_status: false,
            org_list: [],
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        request_func(this, AppServiceUtility.service_item_package.get_service_product_list, [{}], (data: any) => {
            let cloums: Array<object> = [];
            data!.map((val: any) => {
                console.info(val);
                val.is_service_item === 'true' && cloums.push({ title: val.name + '/元', dataIndex: val.id, key: val.id });
            });
            let columns_data_source = [{
                title: '开始时间',
                dataIndex: 'start_date',
                key: 'start_date',
            }, {
                title: '结束时间',
                dataIndex: 'end_date',
                key: 'end_date',
            }
                , {
                title: '长者姓名',
                dataIndex: 'user.name',
                key: 'user.name',
            }, {
                title: '身份证',
                dataIndex: 'user.id_card',
                key: 'user.id_card',
            },
            {
                title: '应收款金额',
                dataIndex: 'receive',
                key: 'receive',
            },
            {
                title: '实收款金额',
                dataIndex: 'total',
                key: 'total',
            },
            {
                title: '合计差额',
                dataIndex: 'balance',
                key: 'balance',
            },
            ...cloums,
            {
                title: '审核费用补充',
                dataIndex: 'reduction',
                key: 'reduction',
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',

            },
            {
                title: '组织机构',
                dataIndex: 'org_name',
                key: 'org_name',

            }
            ];
            this.setState({ columns_data_source });
        });
    }
    goback = () => {
        this.props!.history!.push(ROUTE_PATH.actualReceivablesList);
    }
    onChangeTime = (value: any, dateString: any) => {
        this.setState({ date: dateString });
    }
    render() {
        let info_list = {
            type_show: false,
            showHeader: true,
            bordered: false,
            show_footer: true,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name",
                },
                {
                    type: InputType.rangePicker,
                    label: "核算时间",
                    decorator_id: "date"
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name"
                // },
            ],
            btn_props: [{
                label: '导出',
                btn_method: () => { this.setState({ visible: true }); },
                icon: 'download'
            }],
            columns_data_source: this.state.columns_data_source,
            service_object: AppServiceUtility.cost_accounting_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
        };

        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：差额明细表
 * 描述
 */
@addon('DifferenceDetailsView', '差额明细表', '描述')
@reactControl(Form.create<any>()(DifferenceDetailsView), true)
export class DifferenceDetailsViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;

}