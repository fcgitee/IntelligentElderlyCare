import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info, personnelCategory, personnel_type } from "../../../app/util-tool";
let { Option } = Select;
/**
 * 组件：护理档案状态
 */
export interface NursingRelationShipEditViewState extends ReactViewState {

    /** 基础数据 */
    base_data: any;
    /** 护理人员 */
    user_list: any;
    /** 护理（住宿区域） */
    area_list: any;
}

/**
 * 组件：护理档案视图
 */
export class NursingRelationShipEditView extends ReactView<NursingRelationShipEditViewControl, NursingRelationShipEditViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            user_list: [],
            area_list: []
        };
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.nursingRelationShip);
    }
    componentDidMount = () => {
        // 获取护理人员
        const param = { personnel_category: personnelCategory.staff, personnel_type: personnel_type.person };
        request_func(this, AppServiceUtility.personnel_service['get_personnel_list'], [param], (data: any) => {
            let user_list: any[] = [];
            const data_list: any[] = data.result;
            data_list!.map((item, idx) => {
                user_list.push(<Option key={item['id']}>{item['name']}</Option>);
            });
            this.setState({
                user_list: user_list
            });
        });

        // 获取护理(住宿)区域
        request_func(this, AppServiceUtility.hotel_zone_service['get_list'], [{}], (data: any) => {
            let area_list: any[] = [];
            const data_list: any[] = data.result;
            data_list!.map((item, idx) => {
                area_list.push(<Option key={item['id']}>{item['zone_name']}</Option>);
            });
            this.setState({
                area_list: area_list
            });
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "护理关系信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "护理人员",
                            decorator_id: "nursing_staff",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择项目人员" }],
                                // initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择项目人员",
                                childrens: this.state.user_list,
                            }
                        }, {
                            type: InputType.select,
                            label: "护理区域",
                            decorator_id: "nursing_area",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择护理区域" }],
                                initialValue: '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择护理区域",
                                childrens: this.state.area_list,
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "特殊护理要求",
                            decorator_id: "special_requirements",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请特殊护理要求" }],
                                // initialValue: base_data ? base_data.special_requirements : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入特殊护理要求"
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remarks",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入备注" }],
                                // initialValue: base_data ? base_data.remarks : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.nursingRelationShip);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.nursing_service,
                operation_option: {
                    save: {
                        func_name: "nursing_relationship_update"
                    },
                    query: {
                        func_name: "get_nursing_relationship_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingRelationShip); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：护理档案编辑控件
 * @description 护理档案编辑控件
 * @author
 */
@addon('NursingRelationShipEditView', '护理档案编辑控件', '护理档案编辑控件')
@reactControl(NursingRelationShipEditView, true)
export class NursingRelationShipEditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}