
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：护理档案视图 */
export interface NursingRelationShipViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
}
/** 组件：护理档案视图 */
export class NursingRelationShipView extends React.Component<NursingRelationShipViewControl, NursingRelationShipViewState> {
    private columns_data_source = [{
        title: '护理人员',
        dataIndex: 'staff_name',
        key: 'staff_name',
    }, {
        title: '护理区域',
        dataIndex: 'hotel_name',
        key: 'hotel_name',
    }, {
        title: '特殊护理要求',
        dataIndex: 'special_requirements',
        key: 'special_requirements',
    }, {
        title: '备注',
        dataIndex: 'remarks',
        key: 'remarks',
    },];
    constructor(props: NursingRelationShipViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {}
        };
    }

    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.nursingRelationShipEdit);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.nursingRelationShipEdit + '/' + contents.id);
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let nursing = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "护理人员",
                    decorator_id: "staff_name"
                }, {
                    type: InputType.input,
                    label: "护理区域",
                    decorator_id: "hotel_name"
                }, {
                    type: InputType.input,
                    label: "特殊护理要求",
                    decorator_id: "special_requirements"
                }, {
                    type: InputType.input,
                    label: "备注",
                    decorator_id: "remarks"
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addArchives,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.nursing_service,
            service_option: {
                select: {
                    service_func: 'get_nursing_relationship_list',
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'nursing_relationship_delete'
                }
            },
        };
        let nursing_list = Object.assign(nursing, table_param);
        return (
            <SignFrameLayout {...nursing_list} />
        );
    }
}

/**
 * 控件：护理档案视图控制器
 * @description 护理档案视图
 * @author
 */
@addon('NursingRelationShipView', '护理关系列表视图', '护理关系列表视图')
@reactControl(NursingRelationShipView, true)
export class NursingRelationShipViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}