import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "../../../app/util-tool";

/**
 * 组件：护理档案状态
 */
export interface ChangeNursingArchivesViewState extends ReactViewState {

    /** 基础数据 */
    base_data: any;
    /** 数据id */
    id?: string;
}

/**
 * 组件：护理档案视图
 */
export class ChangeNursingArchivesView extends ReactView<ChangeNursingArchivesViewControl, ChangeNursingArchivesViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            id: ''
        };
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.nursingArchives);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "档案信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "护理类别",
                            decorator_id: "nursing_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入护理类别" }],
                                initialValue: base_data ? base_data.nursing_type : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入护理类别"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "说明",
                            decorator_id: "Description",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入说明" }],
                                initialValue: base_data ? base_data.Description : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入说明"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "费用",
                            decorator_id: "fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入费用" }],
                                initialValue: base_data ? base_data.fee : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入费用"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "状态",
                            decorator_id: "state",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入状态" }],
                                initialValue: base_data ? base_data.state : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入状态"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.nursingArchives);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.nursing_service,
                operation_option: {
                    save: {
                        func_name: "update"
                    },
                    query: {
                        func_name: "get_nursing_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingArchives); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：护理档案编辑控件
 * @description 护理档案编辑控件
 * @author
 */
@addon('ChangeNursingArchivesView', '护理档案编辑控件', '护理档案编辑控件')
@reactControl(ChangeNursingArchivesView, true)
export class ChangeNursingArchivesViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}