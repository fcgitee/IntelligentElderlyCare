import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
// import { Select } from 'antd';
// import { request } from "src/business/util_tool";
// let { Option } = Select;

/**
 * 组件：编辑业务区域管理状态
 */
export interface NursingTypeEditViewState extends ReactViewState {
    /** 基础数据 */
    base_data: any;
}

/**
 * 组件：编辑业务区域管理
 * 描述
 */
export class NursingTypeEditView extends ReactView<NursingTypeEditViewControl, NursingTypeEditViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
        };
    }
    componentDidMount() {

    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        console.info(this.props.match!.params.key, 'id');
        let columns_data_source = [{
            title: '配置项',
            dataIndex: 'item_id',
            key: 'item_id',
            type: 'input' as ColTypeStr,
        }, {
            title: '配置值',
            dataIndex: 'value',
            key: 'value',
            type: 'input' as ColTypeStr
        }];
        let edit_props = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "护理类型",
                    decorator_id: "nursing_name",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入护理类型" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入护理类型"
                    }
                }, {
                    type: InputTypeTable.input,
                    label: "编号",
                    decorator_id: "nursing_number",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入编号" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入编号"
                    }
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.nursingType);
                    }
                }
            ],
            submit_props: {
                text: "保存",
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            // table配置
            columns_data_source: columns_data_source,
            add_row_text: "新增选项",
            table_field_name: 'aera_dispose',
            service_option: {
                service_object: AppServiceUtility.nursing_service,
                operation_option: {
                    query: {
                        func_name: "get_nursing_type_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "nursing_type_update"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.nursingType); },
            id: this.props.match!.params.key
        };
        return (
            <TableList {...edit_props} />
        );
    }
}

/**
 * 控件：编辑业务区域管理
 * 描述
 */
@addon('NursingTypeEditViewView', '编辑护理类型', '描述')
@reactControl(NursingTypeEditView, true)
export class NursingTypeEditViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}