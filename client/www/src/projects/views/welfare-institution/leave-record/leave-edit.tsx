import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import moment from 'moment';
import { addon } from "pao-aop";
import { CookieUtil, reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { MainContent } from "src/business/components/style-components/main-content";
import { COOKIE_KEY_CURRENT_USER } from 'src/business/mainForm/backstageManageMainForm';
import { User } from "src/business/models/user";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
let { Option } = Select;

/**
 * 组件：住宿记录编辑状态
 */
export interface LeaveEditViewState extends ReactViewState {
    /** 入住人列表 */
    residents_list?: [];
    /** 床位编号列表 */
    bed_list?: [];
    /** 离开原因列表 */
    leave_reason_list?: [];
    /** 床位编号 */
    bed_id?: any;
    /** 人员当前状态 */
    status?: string;
    /** 当前id */
    id?: string;
    /** 记录说明 */
    record?: string;
    /** 记录原因 */
    record_reason_id?: string;
    /** 请假时间 */
    leave_time?: any;
    /** 请假天数 */
    leave_total_days?: string;
    /** 接出人 */
    pickout_name?: string;
    /** 入住人 */
    residents_name?: string;
    /** 送入人 */
    enter_id?: string;
}

/**
 * 组件：住宿记录编辑视图
 * 描述
 */
export class LeaveEditView extends ReactView<LeaveEditViewControl, LeaveEditViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    },];

    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            residents_list: [],
            leave_reason_list: [],
            bed_id: [],
            status: '离开',
        };
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            AppServiceUtility.leaveRecord_service.get_leave_list_all!({ id: this.props.match!.params.key })!
                .then((datas: any) => {
                    if (datas.result.length > 0) {
                        const data = datas.result[0];
                        this.setState({
                            record: data.record,
                            residents_name: data.user.name,
                            bed_id: data.bed_id,
                            status: data.status,
                            leave_time: moment(data.leave_time),
                            leave_total_days: data.leave_total_days,
                            enter_id: data.enter_id,
                            record_reason_id: data.record_reason_id
                        });
                    }
                });
        }
        // 查询床位列表
        request(this, AppServiceUtility.hotel_zone_type_service.get_bed_list!({}))
            .then((datas: any) => {
                this.setState({
                    bed_list: datas.result,
                });
            });
        request(this, AppServiceUtility.person_org_manage_service.get_personnel_elder!({}))
            .then((datas: any) => {
                this.setState({
                    residents_list: datas.result,
                });
            });
        // 获取离开原因列表
        request(this, AppServiceUtility.leaveRecord_service.get_leave_reason_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    leave_reason_list: datas.result,
                });
            });
    }
    elderChange = (value: any) => {
        // console.log(value);
        request(this, AppServiceUtility.check_in_service.get_check_in_list!({ "user_id": value }, 1, 1))
            .then((datas: any) => {
                if (datas.result.length > 0) {
                    this.setState({
                        bed_id: datas.result[0].bed_id
                    });
                    request(this, AppServiceUtility.leaveRecord_service.get_leave_list!({ "residents_id": datas.result[0].user_id }))
                        .then((datas: any) => {
                            if (datas.result.length > 0) {
                                if (datas.result[0].status === '入住') {
                                    this.setState({
                                        status: datas.result[0].status,
                                        id: datas.result[0].id,
                                        record: datas.result[0].record,
                                    });
                                }

                            }
                        });
                }
            });

    }
    render() {
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'get_check_in_list',
            title: '入住人查询',
            id_field: 'user_id',
            name_field: 'name',
            select_option: {
                placeholder: "请选择入住人",
            },
        };

        let bed = this.state.bed_list;
        let bed_list: any[] = [];
        bed!.map((item, idx) => {
            bed_list.push(<Option key={item['id']}>{item['bed_code']}</Option>);
        });
        let residents_list: any[] = [];
        this.state.residents_list!.map((item, idx) => {
            residents_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let leave_reason = this.state.leave_reason_list;
        let leave_reason_list: any[] = [];
        leave_reason!.map((item, idx) => {
            leave_reason_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let operation_personnel_name = CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER) && CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name ? CookieUtil.read<User>(COOKIE_KEY_CURRENT_USER).name : '...';
        const that = this;
        function ifNeedSubmitBtn() {
            if (that.props.match!.params.key) {
                return undefined;
            } else {
                return {
                    submit_btn_propps: {
                        text: "保存"
                    }
                };
            }
        }

        let leave_mes = {
            title: "离开信息",
            need_card: true,
            input_props: [
                // {
                //     type: InputType.date,
                //     label: "离开时间",
                //     decorator_id: "checkout_date",
                //     field_decorator_option: {
                //         rules: [{ required: true, message: "请输入离开时间" }],
                //         initialValue: moment(now_date),
                //     } as GetFieldDecoratorOptions,
                //     option: {
                //         placeholder: "请输入离开时间",
                //         disabled: true,
                //         // value: moment().format("YYYY-MM-DD HH:mm:ss") // 当前时间
                //     }
                // },
                {
                    type: InputType.antd_input,
                    label: "接出人",
                    decorator_id: "pickout_id",
                    field_decorator_option: {
                        rules: [{ message: "请输入接出人" }],
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入接出人",
                        disabled: false,
                    }
                }, {
                    type: InputType.antd_input,
                    label: "操作人员",
                    decorator_id: "operating_personnel_id2",
                    field_decorator_option: {
                        // rules: [{ message: "请选择操作人员" }],
                        initialValue: operation_personnel_name,
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "操作人员",
                        // childrens: residents_list,
                        disabled: true,
                    }
                },
            ],
        };
        let check_in = {
            title: "入住信息",
            need_card: true,
            input_props: [
                // {
                //     type: InputType.date,
                //     label: "入住时间",
                //     decorator_id: "checkin_date",
                //     field_decorator_option: {
                //         rules: [{ required: true, message: "请输入入住时间" }],
                //     } as GetFieldDecoratorOptions,
                //     option: {
                //         placeholder: "请输入入住时间",
                //         // childrens: service_bakage_list
                //         disabled: false,
                //         value: moment().format("YYYY-MM-DD HH:mm:ss") // 当前时间
                //     }
                // }, 
                {
                    type: InputType.antd_input,
                    label: "送入人",
                    decorator_id: "enter_id",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入送入人" }],
                        initialValue: this.state.enter_id ? this.state.enter_id : '',
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入送入人",
                        disabled: false
                    }
                }, {
                    type: InputType.antd_input,
                    label: "操作人员",
                    decorator_id: "operating_personnel_id3",
                    field_decorator_option: {
                        // rules: [{ message: "请选择操作人员" }],
                        initialValue: operation_personnel_name,
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "操作人员",
                        // modal_search_items_props: modal_search_items_props4,
                        disabled: true
                    }
                },
            ],
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "长者信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "入住人",
                            decorator_id: "residents_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择入住人" }],
                                initialValue: this.state.residents_name ? this.state.residents_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择入住人",
                                modal_search_items_props: modal_search_items_props,
                                onChange: (value: any) => this.elderChange(value),
                            }
                        }, {
                            type: InputType.select,
                            label: "床位编号",
                            decorator_id: "bed_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入床位编号" }],
                                initialValue: this.state.bed_id ? this.state.bed_id : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位编号",
                                childrens: bed_list,
                                disabled: true,
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "状态",
                            decorator_id: "status",
                            field_decorator_option: {
                                initialValue: this.state.status ? this.state.status : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                disabled: true,
                            }
                        },
                    ],
                },
                {
                    title: "记录登记",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "记录原因",
                            decorator_id: "record_reason_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择记录原因" }],
                                initialValue: this.state.record_reason_id ? this.state.record_reason_id : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择记录原因",
                                childrens: leave_reason_list,
                                disabled: false,
                            }
                        },
                        //  {
                        //     type: InputType.date,
                        //     label: "申请时间",
                        //     decorator_id: "apply_date",
                        //     field_decorator_option: {
                        //         rules: [{ required: true, message: "请输入申请时间" }],
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         placeholder: "请输入申请时间",
                        //         disabled: false,
                        //         value: moment().format("YYYY-MM-DD HH:mm:ss") // 当前时间
                        //     }
                        // }, 
                        {
                            type: InputType.text_area,
                            label: "记录说明",
                            decorator_id: "record",
                            field_decorator_option: {
                                rules: [{ message: "请输入记录说明" }],
                                initialValue: this.state.record ? this.state.record : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入记录说明",
                                disabled: false,
                            }
                        },
                    ] as any[]
                },
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.goBack();
                    }
                }
            ],
            ...ifNeedSubmitBtn(),
            service_option: {
                service_object: AppServiceUtility.leaveRecord_service,
                operation_option: {
                    query: {
                        func_name: "get_leave_list_all",
                        arguments: [{ id: this.state.id }, 1, 1]
                    },
                    save: {
                        func_name: "update_leave_record"
                    }
                },
            },
            succ_func: () => { this.props.history!.goBack(); },
            id: this.state.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        if (this.state.status === '入住') {
            edit_props_list.form_items_props.push(leave_mes);
            edit_props_list.form_items_props[1].input_props.push(
                {
                    type: InputType.date,
                    label: "请假时间",
                    decorator_id: "leave_time",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入请假时间" }],
                        initialValue: this.state.leave_time ? this.state.leave_time : undefined,
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入请假时间",
                        disabled: false
                    }
                },
                // {
                //     type: InputType.date,
                //     label: "销假时间",
                //     decorator_id: "cancellation_leave_time",
                //     field_decorator_option: {
                //         // rules: [{ required: true, message: "销假时间" }],
                //         initialValue: this.state.leave_time ? this.state.leave_time : '',
                //     } as GetFieldDecoratorOptions,
                //     option: {
                //         placeholder: "销假时间",
                //         disabled: false
                //     }
                // },
                {
                    type: InputType.antd_input_number,
                    label: "请假天数",
                    decorator_id: "leave_total_days",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入请假天数" }],
                        initialValue: this.state.leave_total_days ? this.state.leave_total_days : '',
                    } as GetFieldDecoratorOptions,
                    option: {
                        placeholder: "请输入请假天数",
                        min: 0.5,
                        disabled: false
                    }
                }
            );
        }
        if (this.state.status === '离开') {
            edit_props_list.form_items_props.push(check_in);

        }
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：住宿记录编辑视图
 * 描述
 */
@addon('LeaveEditView', '住宿记录编辑视图', '描述')
@reactControl(LeaveEditView, true)
export class LeaveEditViewControl extends ReactViewControl {
}