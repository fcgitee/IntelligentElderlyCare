import { message, Modal, Row, Select } from "antd";
import { addon, getObject, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ILeaveRecordService } from "src/projects/models/leave-record";
import { ROUTE_PATH } from "src/projects/router";

const Option = Select.Option;

/**
 * 组件：住宿记录信息状态
 */
export interface LeaveRecordViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 住宿记录项目集合 */
    bed_list?: any[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 住宿记录总条数 */
    total?: number;
    /** 标题 */
    title?: string;
    /** 表格列信息 */
    columns_service_item?: any[];
    /** 接口名 */
    request_url?: string;
    destory_modal?: boolean;
    destory_data?: any;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：住宿记录信息
 * 描述
 */
export class LeaveRecordView extends ReactView<LeaveRecordViewControl, LeaveRecordViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'org.name',
            key: 'org.name',
        },
        {
            title: '长者名称',
            dataIndex: 'user.name',
            key: 'user.name',
        },
        {
            title: '床位编号',
            dataIndex: 'bed.name',
            key: 'bed.name',
        }, {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
        }, {
            title: '入住时间',
            dataIndex: 'checkin_date',
            key: 'checkin_date',
        }, {
            title: '离开时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }
        // {
        //     title: '离开时间',
        //     dataIndex: 'checkout_date',
        //     key: 'checkout_date',
        // }
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            bed_list: [],
            destory_modal: false,
            modal_visible: false,
            destory_data: undefined,
            org_list: [],
        };
    }
    componentDidMount() {
        // 床位编号
        request(this, AppServiceUtility.hotel_zone_type_service.get_bed_list!({}))
            .then((datas: any) => {
                this.setState({
                    bed_list: datas.result,
                });
            });
    }
    /** 住宿记录服务 */
    serviceService_Fac() {
        return getObject(this.props.serviceService_Fac!);
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.leaveEdit);
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
            destory_modal: false,
        });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.leaveEdit + '/' + contents.id);
        }
        if ('icon_destory' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.destoryLeave(contents);
        }
    }

    destoryLeave(data: any) {
        this.setState({
            destory_data: data,
            destory_modal: true
        });
    }

    modalDestoryBtn = () => {
        const { destory_data } = this.state;
        AppServiceUtility.leaveRecord_service.get_leave_list_all!({
            residents_id: destory_data.residents_id
        })!
            .then((result: any) => {
                if (result) {
                    // console.log('记录', result);
                    if (result.result[0].status === '入住') {
                        message.info('该长者已入住，无需销假');
                        this.setState({ destory_modal: false });
                        return;
                    } else {
                        AppServiceUtility.leaveRecord_service.update_leave_record!({
                            residents_id: destory_data.residents_id,
                            bed_id: destory_data.bed_id,
                            status: '入住'
                        })!
                            .then((result: any) => {
                                if (result) {
                                    message.info('销假成功');
                                    // this.props.history!.push(ROUTE_PATH.leaveRecord);
                                    this.setState({
                                        destory_modal: false
                                    });
                                    this.props.history!.push(ROUTE_PATH.leaveRecord);
                                }
                            });
                    }
                }
            });
    }

    render() {
        // 床位编号
        let bed_list = this.state.bed_list;
        let bed_option_list: any[] = [];
        bed_list!.map((item, idx) => {
            bed_option_list.push(<Option key={item.id}>{item.name}</Option>);
        });
        let leave_datalist: any[] = [];
        let leave_data: any[] = [
            {
                id: '离开',
                name: '离开'
            },
            {
                id: '入住',
                name: '入住'
            }
        ];
        leave_data!.map((item, idx) => {
            leave_datalist.push(<Option key={item.id}>{item.name}</Option>);
        });

        // let tab_par_new = table_param;
        // tab_par_new.other_label_type.push(
        //     { type: 'icon', label_key: 'icon_destory', label_parameter: { icon: 'antd@minus-circle' } }
        // );
        // let operation_icon = [...table_param.other_label_type];
        // operation_icon.push({ type: 'icon', label_key: 'icon_destory', label_parameter: { icon: 'antd@minus-circle' } });
        let tab_par_new = {
            other_label_type: [
                {
                    type: 'button',
                    label_text: '',
                    label_key: 'icon_destory',
                    label_parameter: { size: 'small' },
                    label_text_func(record: any) {
                        return record.hasOwnProperty('status') && record['status'] === '离开' && record.hasOwnProperty('is_new_checkout') && record['is_new_checkout'] === true ? '销假' : undefined;
                    }
                },
                { ...table_param.other_label_type[0] }
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let accommodation_record = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名称",
                    decorator_id: "elder_name"
                },
                {
                    type: InputType.input,
                    label: "床位名称",
                    decorator_id: "bed_name",
                },
                {
                    type: InputType.select,
                    label: '状态',
                    decorator_id: "status",
                    option: {
                        childrens: leave_datalist,
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                // },
            ],
            on_icon_click: this.onIconClick,
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],

            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            // on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.leaveRecord_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_leave_record'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let accommodation_record_list = Object.assign(accommodation_record, tab_par_new);
        // delete accommodation_record_list.other_label_type;
        return (
            <Row>
                <Modal
                    title={this.state.title}
                    visible={this.state.modal_visible}
                    onCancel={this.handleCancel}
                    width={900}
                >
                    <NTOperationTable
                        data_source={this.state.bed_list}
                        columns_data_source={this.state.columns_service_item}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal>
                <SignFrameLayout {...accommodation_record_list} />
                <Modal
                    title="确认销假"
                    visible={this.state.destory_modal}
                    onOk={this.modalDestoryBtn}
                    onCancel={this.handleCancel}
                    okText={"确认"}
                    cancelText={'取消'}
                >
                    <p>是否确认销假</p>
                </Modal>
            </Row>
        );
    }
}

/**
 * 控件：离开原因信息
 * 描述
 */
@addon('LeaveRecordView', '离开原因信息', '描述')
@reactControl(LeaveRecordView, true)
export class LeaveRecordViewControl extends ReactViewControl {
    /** 住宿记录接口服务 */
    public serviceService_Fac?: Ref<ILeaveRecordService>;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;

}