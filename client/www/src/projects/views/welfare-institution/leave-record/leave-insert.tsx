import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
let { Option } = Select;

/**
 * 组件：住宿记录编辑状态
 */
export interface LeaveInsertViewState extends ReactViewState {
    /** 入住人列表 */
    residents_list?: [];
    /** 床位编号列表 */
    bed_list?: [];
    /** 离开原因列表 */
    leave_reason_list?: [];
    bed_num?: any;
}

/**
 * 组件：住宿记录编辑视图
 * 描述
 */
export class LeaveInsertView extends ReactView<LeaveInsertViewControl, LeaveInsertViewState> {
    private columns_data_source = [{
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    },];
    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            residents_list: [],
            leave_reason_list: [],
            bed_num: []
        };
    }
    componentDidMount() {
        // 获取离开原因列表
        request(this, AppServiceUtility.leaveRecord_service.get_leave_reason_list!({}, 1, 999))
            .then((datas: any) => {
                this.setState({
                    leave_reason_list: datas.result,
                });
            });
        // request(this, AppServiceUtility.check_in_service.get_check_in_list!({}, 1, 999))
        //     .then((datas: any) => {
        //         // console.log("啥子情况", datas);
        //     });
    }
    elderChange = (value: any) => {
        // console.log(value);
        request(this, AppServiceUtility.check_in_service.get_check_in_list!({ "id": value }, 1, 1))
            .then((datas: any) => {
                if (datas.result.length > 0) {
                    this.setState({
                        bed_num: datas.result[0].bed_number
                    });
                }
            });
    }
    render() {
        let modal_search_items_props = {
            type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'get_check_in_list',
            title: '入住人查询',
            select_option: {
                placeholder: "请选择入住人",
            },
        };
        let modal_search_items_props3 = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_personnel_list',
            title: '接出人查询',
            select_option: {
                placeholder: "请选择接出人",
            }
        };
        let modal_search_items_props4 = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "姓名",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_personnel_list',
            title: '操作人查询',
            select_option: {
                placeholder: "请选择操作人",
            }
        };
        let leave_reason = this.state.leave_reason_list;
        let leave_reason_list: any[] = [];
        leave_reason!.map((item, idx) => {
            leave_reason_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props = {
            form_items_props: [
                {
                    title: "长者信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "入住人",
                            decorator_id: "residents_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择入住人" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择入住人",
                                modal_search_items_props: modal_search_items_props,
                                onChange: (value: any) => this.elderChange(value),
                            }
                        },
                    ],
                },
                {
                    title: "记录登记",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "记录原因",
                            decorator_id: "record_reason_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择记录原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择记录原因",
                                childrens: leave_reason_list,
                            }
                        }, {
                            type: InputType.date,
                            label: "申请时间",
                            decorator_id: "apply_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入申请时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入申请时间"
                            }
                        }, {
                            type: InputType.text_area,
                            label: "记录说明",
                            decorator_id: "record",
                            field_decorator_option: {
                                rules: [{ message: "请输入记录说明" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入记录说明"
                            }
                        },
                    ]
                },
                {
                    title: "离开信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "床位编号",
                            decorator_id: "bed_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入床位编号" }],
                                initialValue: this.state.bed_num ? this.state.bed_num : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位编号",
                                disabled: true
                            }
                        }, {
                            type: InputType.date,
                            label: "离开时间",
                            decorator_id: "checkout_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入离开时间" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入离开时间",
                                // childrens: service_bakage_list
                            }
                        },
                        {
                            type: InputType.modal_search,
                            label: "接出人",
                            decorator_id: "pickout_id",
                            field_decorator_option: {
                                rules: [{ message: "请输入接出人" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "接出人",
                                modal_search_items_props: modal_search_items_props3
                            }
                        }, {
                            type: InputType.modal_search,
                            label: "操作人员",
                            decorator_id: "operating_personnel_id2",
                            field_decorator_option: {
                                rules: [{ message: "请选择操作人员" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "操作人员",
                                modal_search_items_props: modal_search_items_props4
                                // disabled: true,
                            }
                        },
                    ],
                },
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.leaveRecord);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.leaveRecord_service,
                operation_option: {
                    save: {
                        func_name: "update_leave_record"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.leaveRecord); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：住宿记录编辑视图
 * 描述
 */
@addon('LeaveInsertView', '住宿记录编辑视图', '描述')
@reactControl(LeaveInsertView, true)
export class LeaveInsertViewControl extends ReactViewControl {
}