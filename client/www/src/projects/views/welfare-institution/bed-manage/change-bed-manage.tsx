import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

let { Option } = Select;

/**
 * 组件：床位编辑视图状态
 */
export interface ChangeBedManageViewState extends ReactViewState {
    /** 区域列表 */
    area_list?: [];
    /** 入住人列表 */
    residents_list?: [];
    /** 服务产品列表 */
    service_bakage_list?: [];
    detail?: any;
    product_name?: any;
}

/**
 * 组件：床位编辑视图
 * 描述
 */
export class ChangeBedManageView extends ReactView<ChangeBedManageViewControl, ChangeBedManageViewState> {
    private columns_data_source_pack = [{
        title: '服务产品名称',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '适用范围',
        dataIndex: 'scope_name',
        key: 'scope_name',
    },
    {
        title: '所属组织机构',
        dataIndex: 'organizational_name',
        key: 'organizational_name',
    },
    {
        title: '备注',
        dataIndex: 'remarck',
        key: 'remarck',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            area_list: [],
            residents_list: [],
            service_bakage_list: [],
            detail: undefined,
            product_name: ''
        };
    }
    componentDidMount() {
        // 查询住宿区域列表
        request(this, AppServiceUtility.hotel_zone_service.get_list!({ hotel_zone_type_name: '房' }))
            .then((datas: any) => {
                this.setState({
                    area_list: datas.result,
                });
            });
        if (this.props.match!.params.key) {
            request(this, AppServiceUtility.person_org_manage_service.get_elder_list_all!({}))
                .then((datas: any) => {
                    this.setState({
                        residents_list: datas.result,
                    });
                });
        }
        // // 获取服务项目包信息
        request(this, AppServiceUtility.hotel_zone_type_service.get_bed_list_all!({ id: this.props.match!.params.key }))
            .then((datas: any) => {
                this.setState({
                    product_name: datas.result[0].service_item_package[0],
                });
            });
    }

    render() {
        let area = this.state.area_list;
        let area_list: any[] = [];
        area!.map((item, idx) => {
            area_list.push(<Option key={item['id']}>{item['zone_name']}</Option>);
        });
        let residents_list: any[] = [];
        this.state.residents_list!.map((item, idx) => {
            residents_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        // let service_bakage_list: any[] = [];
        // this.state.service_bakage_list!.map((item, idx) => {
        //     service_bakage_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });
        // 床位类型
        let bed_type_list: any[] = [];
        ['护理床位', '养老床位'].map((item, idx) => {
            bed_type_list.push(<Option key={item}>{item}</Option>);
        });
        let modal_search_items_props_pack = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "服务产品名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source_pack,
            service_name: AppServiceUtility.service_item_package,
            service_func: 'get_service_item_package',
            title: '服务产品查询',
            name: this.state.product_name,
            select_option: {
                placeholder: "请选择服务产品",
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "床位",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "所属区域",
                            decorator_id: "area_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择住宿区域" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择住宿区域",
                                childrens: area_list,
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "床位名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入床位名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位名称"
                            }
                        }, {
                            type: InputType.select,
                            label: "床位类型",
                            decorator_id: "type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择床位类型" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择床位类型",
                                childrens: bed_type_list,
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "床位编号",
                            decorator_id: "bed_code",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入床位编号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位编号"
                            }
                        }, {
                            type: InputType.select,
                            label: "入住人",
                            decorator_id: "residents_id",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入服务项目参数" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入入住人",
                                childrens: residents_list,
                                disabled: true
                                // modal_search_items_props: modal_search_items_props

                            }
                        }, {
                            type: InputType.modal_search,
                            label: "服务产品",
                            decorator_id: "service_item_package_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入服务产品" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务产品",
                                modal_search_items_props: modal_search_items_props_pack
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.goBack();
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存"
            },
            service_option: {
                service_object: AppServiceUtility.hotel_zone_type_service,
                operation_option: {
                    query: {
                        func_name: "get_bed_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_bed"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.bedManage); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：床位编辑视图
 * 描述
 */
@addon('ChangeBedManageView', '床位编辑视图', '描述')
@reactControl(ChangeBedManageView, true)
export class ChangeBedManageViewControl extends ReactViewControl {
}