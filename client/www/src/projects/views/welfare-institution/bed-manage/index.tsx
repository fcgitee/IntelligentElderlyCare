import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { request } from "src/business/util_tool";
import { Select, message, Modal } from "antd";
const { confirm } = Modal;
let { Option } = Select;
/**
 * 组件：床位页面状态
 */
export interface BedManageViewState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
    /** 区域列表 */
    area_list?: [];
    is_send?: boolean;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：床位页面
 * 描述
 */
export class BedManageView extends ReactView<BedManageViewControl, BedManageViewState> {
    // private other_button: any = Element;
    private columns_data_source = [
        {
            title: '机构名称',
            dataIndex: 'org.name',
            key: 'org.name',
        },
        {
            title: '区域',
            dataIndex: 'area_name',
            key: 'area_name',
        }, {
            title: '床位名称',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '床位编号',
            dataIndex: 'bed_code',
            key: 'bed_code',
        }, {
            title: '入住人',
            dataIndex: 'residents',
            key: 'residents',
            render: (text: any, record: any) => {
                if (record.state !== undefined && record.state === false) {
                    // this.other_button = <Button type="primary" onClick={() => { this.updateStatus(record); }}>启用</Button>;
                    return record.residents;
                } else {
                    // this.other_button = <Button type="danger" onClick={() => { this.updateStatus(record); }}>停用</Button>;
                    return record.residents;
                }
            }
        }, {
            title: '使用状态',
            dataIndex: 'residents_id',
            key: 'residents_id',
            render: (text: any, record: any) => {
                if (text && text !== '') {
                    return '使用中';
                } else {
                    return '空闲';
                }
            }
        }, {
            title: '床位状态',
            dataIndex: 'state',
            key: 'state',
            render: (text: any, record: any) => {
                if (record.state !== undefined && record.state === false) {
                    return '停用中';
                } else {
                    return '启用中';
                }
            }
        }, {
            title: '上次修改时间',
            dataIndex: 'modify_date',
            key: 'modify_date',
        }, {
            title: '创建时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },];
    constructor(props: BedManageViewControl) {
        super(props);
        this.state = {
            request_url: '',
            area_list: [],
            is_send: false,
            org_list: [],
        };
    }
    updateStatus = (record: any) => {
        let state: boolean = false;
        if (record.state !== undefined && record.state === false) {
            // 启用
            state = true;
        } else {
            // 停用
            state = false;
        }
        if (state === false && record.residents_id && record.residents_id !== '') {
            message.info('该床位使用中，无法停用！');
            return;
        }
        let that = this;
        confirm({
            title: `是否确认${state === true ? '启用' : '停用'}该床位：【${record.name}】`,
            onOk() {
                if (that.state.is_send === true) {
                    message.info('请勿操作太快！');
                    return;
                }
                request(that, AppServiceUtility.hotel_zone_type_service.get_bed_list_byId!({ id: record.id }, 1, 1))
                    .then((data: any) => {
                        if (data.result.length > 0) {
                            let record: any = data.result[0];
                            record['state'] = state;
                            request(that, AppServiceUtility.hotel_zone_type_service.update_bed!(record))
                                .then((data: any) => {
                                    if (data === 'Success') {
                                        message.info('操作成功！', 1, () => {
                                            window.location.reload();
                                        });
                                    } else {
                                        message.info(data);
                                        that.setState({
                                            is_send: false,
                                        });
                                    }
                                });
                        }
                    });
            },
            onCancel() { },
        });
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeBedManage);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.changeBedManage + '/' + contents.id);
        } else if ('icon_state' === type) {
            this.updateStatus(contents);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    componentDidMount() {
        // 查询住宿区域列表
        request(this, AppServiceUtility.hotel_zone_service.get_list!({ in_select: true }))
            .then((datas: any) => {
                this.setState({
                    area_list: datas.result,
                });
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        let area = this.state.area_list;
        let area_list: any[] = [];
        if (area) {
            area!.map((item, idx) => {
                area_list.push(<Option key={item['id']}>{item['zone_name']}</Option>);
            });
        }
        let service_provider = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "床位名称",
                    decorator_id: "name",
                    option: {
                        placeholder: "请输入床位名称",
                    }
                }, {
                    type: InputType.input,
                    label: "床位编号",
                    decorator_id: "bed_code",
                    option: {
                        placeholder: "请输入床位编号",
                    }
                }, {
                    type: InputType.input,
                    label: "入住人名称",
                    decorator_id: "residents",
                    option: {
                        placeholder: "请输入入住人名称",
                    }
                }, {
                    type: InputType.select,
                    label: "所属区域",
                    decorator_id: "area_name",
                    option: {
                        placeholder: "请选择所属区域",
                        childrens: area_list
                    }
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "组织机构",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: "请输入组织机构",
                //     }
                // },
            ],
            btn_props: [{
                label: '新增床位',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            // other_button: this.other_button,
            service_object: AppServiceUtility.hotel_zone_type_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}]
                },
                delete: {
                    service_func: 'delete_bed'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let new_table_param: any = {
            other_label_type: [
                {
                    type: 'button',
                    label_text: '',
                    label_key: 'icon_state',
                    label_parameter: { size: 'small' },
                    label_text_func(record: any) {
                        if (record.residents_id && record.residents_id !== '') {
                            return undefined;
                        } else {
                            if (record.state !== undefined && record.state === false) {
                                return '启用';
                            } else {
                                return '停用';
                            }
                        }
                    }
                },
                { ...table_param.other_label_type[0] }
            ],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
        };
        let service_provider_list = Object.assign(service_provider, new_table_param);
        return (
            <SignFrameLayout {...service_provider_list} />
        );
    }
}

/**
 * 控件：床位页面
 * 描述
 */
@addon('BedManageView', '床位页面', '描述')
@reactControl(BedManageView, true)
export class BedManageViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}