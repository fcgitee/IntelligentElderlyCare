import { message, Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { IAssessmentProjectService } from "src/projects/models/assessment-project";
import { Organizational } from "src/projects/models/personnel-organizational";
import { RequirementOption, RequirementType } from "src/projects/models/requirement";
import { ROUTE_PATH } from "src/projects/router";
let { Option } = Select;

/** 项目状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：需求项目状态
 */
export interface ChangeRequirementProjectViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 组织机构列表 */
    organizationn_list?: Organizational[];
    /** 需求选项列表 */
    require_option_list?: RequirementOption[];
    /** 需求列表 */
    require_type_list?: RequirementType[];
}

/**
 * 组件：需求项目视图
 */
export class ChangeRequirementProjectView extends ReactView<ChangeRequirementProjectViewControl, ChangeRequirementProjectViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            organizationn_list: [],
            require_option_list: [],
            require_type_list: []
        };
    }

    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.requirementProject);
    }

    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        if (!this.props.match!.params.key) {
            value['state'] = SRAR_TUSING;
        }
        request(this, AppServiceUtility.requirement_project_service!.update!(value))
            .then((datas: any) => {
                if (datas === 'Success') {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.requirementProject);
                }
            });
    }

    componentDidMount() {
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
        }

        // 组织机构
        request(this, AppServiceUtility.personnel_service!.get_organizational_list!({}))
            .then((datas: any) => {
                this.setState({
                    organizationn_list: datas.result,
                });
            });

        // 需求选项
        request(this, AppServiceUtility.requirement_option_service!.get_list!({}))
            .then((datas: any) => {
                this.setState({
                    require_option_list: datas.result,
                });
            });
        // 需求类别
        request(this, AppServiceUtility.requirement_type_service!.get_list!({}))
            .then((datas: any) => {
                this.setState({
                    require_type_list: datas.result,
                });
            });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        // 组织机构
        let organization_list = this.state.organizationn_list;
        let organization_option_list: any[] = [];
        organization_list!.map((item, idx) => {
            organization_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        // 需求选项
        let require_option_list = this.state.require_option_list;
        let require_option_option_list: any[] = [];
        require_option_list!.map((item, idx) => {
            require_option_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        // 需求类别 
        let require_type_list = this.state.require_type_list;
        let require_type_option_list: any[] = [];
        require_type_list!.map((item, idx) => {
            require_type_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let columns_data_source = [{
            title: '选项内容',
            dataIndex: 'requirement_option_ids',
            key: 'requirement_option_ids',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择需求项目选项",
                childrens: require_option_option_list
            }
        }];

        let user_manege = {
            table_field_name: 'dataSource',
            // table配置
            columns_data_source: columns_data_source,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.requirement_project_service,
                operation_option: {
                    query: {
                        func_name: "get_list",
                        arguments: [this.state.selete_obj, 1, 10]
                    },
                },
            }
        };

        let form_items_props = [
            {
                type: InputTypeTable.input,
                label: "项目名称",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入项目名称", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "project_name",
            },
            {
                type: InputTypeTable.select,
                label: "需求类别",
                field_decorator_option: {
                    rules: [{ required: true, message: "请选择需求类别", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "requirement_type_id",
                option: {
                    childrens: require_type_option_list
                }
            },
            {
                type: InputTypeTable.input,
                label: "需求描述",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入需求描述", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "requirement_desc",
            },
            {
                type: InputTypeTable.input,
                label: "需求匹配公式",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入需求匹配公式", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "requirement_match_func",
            },
            {
                type: InputTypeTable.select,
                label: "所属组织机构",
                field_decorator_option: {
                    rules: [{ required: true, message: "请输入所属组织机构", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "organization_id",
                option: {
                    childrens: organization_option_list
                }
            }
        ];

        let submit_props = {
            text: "保存",
            cb: this.save_value
        };

        let add_row_text = "新增";

        return (
            <TableList {...user_manege} form_items_props={form_items_props} submit_props={submit_props} add_row_text={add_row_text} />
        );
    }
}

/**
 * 控件：需求项目编辑控件
 * @description 需求项目编辑控件
 * @author
 */
@addon('ChangeRequirementProjectView', '需求项目编辑控件', '需求项目编辑控件')
@reactControl(ChangeRequirementProjectView, true)
export class ChangeRequirementProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估项目服务 */
    public assessmentProjectServer_Fac?: Ref<IAssessmentProjectService>;

}