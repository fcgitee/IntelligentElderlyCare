
import { Select } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { Organizational } from "src/projects/models/personnel-organizational";
import { ROUTE_PATH } from "src/projects/router";

let { Option } = Select;

/** 状态：行为能力评估项目视图 */
export interface RequirementProjectViewState extends ReactViewState {
    /** 组织机构列表 */
    organizationn_list?: Organizational[];
    /** 需求类别列表 */
    require_type_list?: Organizational[];
}
/** 组件：行为能力评估项目视图 */
export class RequirementProjectView extends ReactView<RequirementProjectViewControl, RequirementProjectViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'project_name',
        key: 'project_name',
    }, {
        title: '需求类别',
        dataIndex: 'requirement_type_name',
        key: 'requirement_type_name',
    }, {
        title: '需求描述',
        dataIndex: 'requirement_desc',
        key: 'requirement_desc',
    }, {
        title: '需求匹配公式',
        dataIndex: 'requirement_match_func',
        key: 'requirement_match_func',
    }, {
        title: '所属组织机构',
        dataIndex: 'organization_name',
        key: 'organization_name',
    },];
    constructor(props: RequirementProjectViewControl) {
        super(props);
        this.state = {
            organizationn_list: [],
            require_type_list: []
        };
    }

    componentDidMount() {
        // 组织机构
        request(this, AppServiceUtility.personnel_service!.get_organizational_list!({}))
            .then((datas: any) => {
                this.setState({
                    organizationn_list: datas.result,
                });
            });
        // 需求类别
        request(this, AppServiceUtility.requirement_type_service!.get_list!({}))
            .then((datas: any) => {
                this.setState({
                    require_type_list: datas.result,
                });
            });
    }

    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementProject);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRequirementProject + '/' + contents.id);
        }
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }

        // 组织机构
        let organization_list = this.state.organizationn_list;
        let organization_option_list: any[] = [];
        organization_list!.map((item, idx) => {
            organization_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        // 需求类型
        let require_type_list = this.state.require_type_list;
        let require_type_option_list: any[] = [];
        require_type_list!.map((item, idx) => {
            require_type_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "project_name"
                }, {
                    type: InputType.select,
                    label: "需求类别",
                    decorator_id: "requirement_type_id",
                    option: {
                        childrens: require_type_option_list
                    }
                }, {
                    type: InputType.input,
                    label: "描述",
                    decorator_id: "requirement_desc"
                }, {
                    type: InputType.input,
                    label: "需求匹配公式",
                    decorator_id: "requirement_match_func"
                }, {
                    type: InputType.select,
                    label: "所属组织机构",
                    decorator_id: "organization_id",
                    option: {
                        childrens: organization_option_list,
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.requirement_project_service,
            service_option: {
                select: {
                    service_func: 'get_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：行为能力评估项目视图控制器
 * @description 行为能力评估项目视图
 * @author
 */
@addon('RequirementProjectView', '行为能力评估项目视图', '行为能力评估项目视图')
@reactControl(RequirementProjectView, true)
export class RequirementProjectViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}