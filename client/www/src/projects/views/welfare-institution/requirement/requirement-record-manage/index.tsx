
// import option from "src/business/report/template/heat-map/option";
import { Select } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;
/** 状态：需求记录视图 */
export interface RequirementRecordViewState extends ReactViewState {
    /** 需求类型列表 */
    type_list?: any[];
}
/** 组件：需求记录视图 */
export class RequirementRecordView extends React.Component<RequirementRecordViewControl, RequirementRecordViewState> {
    private columns_data_source = [
        {
            title: '需求编号',
            dataIndex: 'requirement_code',
            key: 'requirement_code',
        }, {
            title: '长者姓名',
            dataIndex: 'user_name',
            key: 'user_name',
        }, {
            title: '模板名称',
            dataIndex: 'template_name',
            key: 'template_name',
        }, {
            title: '需求类型',
            dataIndex: 'requirement_type',
            key: 'requirement_type',
        }, {
            title: '需求状态',
            dataIndex: 'requirement_status',
            key: 'requirement_status',
        }, {
            title: '创建日期',
            dataIndex: 'create_date',
            key: 'create_date',
        },];

    constructor(props: RequirementRecordViewControl) {
        super(props);
        this.state = {
            type_list: [],
        };
    }
    /** 新增按钮 */
    addBehavioralRequirementRecord = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementRecord);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRequirementRecord + '/' + contents.id);
        }
    }
    render() {
        // 组织机构
        let type_list = this.state.type_list;
        let type_option_list: any[] = [];
        type_list!.map((item, idx) => {
            type_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let requirement_record = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "user_name"
                },
                {
                    type: InputType.input,
                    label: "模板名称",
                    decorator_id: "template_name"
                },
                {
                    type: InputType.select,
                    label: "需求类型",
                    decorator_id: "requirement_type",
                    option: {
                        childrens: this.state.type_list,
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralRequirementRecord,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.requirement_record_service,
            service_option: {
                select: {
                    service_func: 'get_requirement_record_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_requirement_record'
                }
            },
        };
        let behavioral_requirement_record_list = Object.assign(requirement_record, table_param);
        return (
            <SignFrameLayout {...behavioral_requirement_record_list} />
        );
    }
}

/**
 * 控件：需求记录列表控制器
 * @description 需求记录列表
 * @author
 */
@addon('RequirementRecordView', '能力评估视图', '能力评估视图')
@reactControl(RequirementRecordView, true)
export class RequirementRecordViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}