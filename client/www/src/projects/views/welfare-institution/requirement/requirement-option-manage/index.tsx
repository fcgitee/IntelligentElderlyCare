
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：需求选项视图 */
export interface RequirementOptionViewState extends ReactViewState {
}
/** 组件：需求选项视图 */
export class RequirementOptionView extends ReactView<RequirementOptionViewControl, RequirementOptionViewState> {
    private columns_data_source = [{
        title: '选项名称',
        dataIndex: 'name',
        key: 'name',
    },
        //  {
        //     title: '选项内容',
        //     dataIndex: 'organizationn_name',
        //     key: 'organizationn_name',
        // },
    ];
    constructor(props: RequirementOptionViewControl) {
        super(props);
        this.state = {
        };
    }

    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementOption);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRequirementOption + '/' + contents.id);
        }
    }
    render() {
        let assessment_project = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                }
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.addBehavioralCompetenceAssessment,
                icon: 'plus'
            }],
            // search_cb: this.queryBtn,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.requirement_option_service,
            service_option: {
                select: {
                    service_func: 'get_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
        };
        let behavioral_competence_assessment_list = Object.assign(assessment_project, table_param);
        return (
            <SignFrameLayout {...behavioral_competence_assessment_list} />
        );
    }
}

/**
 * 控件：需求选项视图控制器
 * @description 需求选项视图
 * @author
 */
@addon('RequirementOptionView', '需求选项视图', '需求选项视图')
@reactControl(RequirementOptionView, true)
export class RequirementOptionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}