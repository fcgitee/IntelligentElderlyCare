import { message } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { IAssessmentProjectService } from "src/projects/models/assessment-project";
import { ROUTE_PATH } from "src/projects/router";

/** 项目状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：需求项目状态
 */
export interface ChangeRequirementOptionViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
}

/**
 * 组件：需求项目视图
 */
export class ChangeRequirementOptionView extends ReactView<ChangeRequirementOptionViewControl, ChangeRequirementOptionViewState> {
    private columns_data_source = [{
        title: '选项名称',
        dataIndex: 'option_name',
        key: 'option_name',
        type: 'input' as ColTypeStr
    }, {
        title: '选项内容',
        dataIndex: 'option_content',
        key: 'option_content',
        type: 'input' as ColTypeStr
    },
    {
        title: '权重',
        dataIndex: 'weight',
        key: 'weight',
        type: 'input' as ColTypeStr
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {}
        };
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.requirementOption);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        if (!this.props.match!.params.key) {
            value['state'] = SRAR_TUSING;
        }
        AppServiceUtility.requirement_option_service.update!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.requirementOption);
                }
            });
    }
    componentDidMount() {
        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let user_manege = {
            table_field_name: 'dataSource',
            // table配置
            columns_data_source: this.columns_data_source,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.requirement_option_service,
                operation_option: {
                    query: {
                        func_name: "get_list",
                        arguments: [this.state.selete_obj, 1, 10]
                    },
                },
            }
        };

        let form_items_props = [
            {
                type: InputTypeTable.input,
                label: "选项名称",
                field_decorator_option: {
                    rules: [{ required: true, message: "选项名称", }],
                } as GetFieldDecoratorOptions,
                decorator_id: "name",
            }
        ];

        let submit_props = {
            text: "保存",
            cb: this.save_value
        };

        let add_row_text = "新增";

        return (
            <TableList {...user_manege} form_items_props={form_items_props} submit_props={submit_props} add_row_text={add_row_text} />
        );
    }
}

/**
 * 控件：需求项目编辑控件
 * @description 需求项目编辑控件
 * @author
 */
@addon('ChangeRequirementOptionView', '需求项目编辑控件', '需求项目编辑控件')
@reactControl(ChangeRequirementOptionView, true)
export class ChangeRequirementOptionViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估项目服务 */
    public assessmentProjectServer_Fac?: Ref<IAssessmentProjectService>;

}