import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

/**
 * 组件：需求类型类型状态
 */
export interface RequirementTypeViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 需求类型类型集合 */
    // service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 需求类型类型总条数 */
    total?: number;
}

/**
 * 组件：需求类型类型
 * 描述
 */
export class RequirementTypeView extends ReactView<RequirementTypeViewControl, RequirementTypeViewState> {

    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '编号',
        dataIndex: 'number',
        key: 'number',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementType);
    }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }

    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRequirementType + '/' + contents.id);
        }
    }

    render() {
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "编号",
                    decorator_id: "number"
                },
                {
                    type: InputType.input,
                    label: "备注",
                    decorator_id: "remark"
                }
            ],
            btn_props: [{
                label: '新增需求类型类别',
                btn_method: this.add,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.requirement_type_service,
            service_option: {
                select: {
                    service_func: 'get_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                {/* <Modal
                    title="需求类型类型列表"
                    visible={this.state.modal_visible}
                    // onOk={() => this.handleOk(formProps)}
                    onCancel={this.handleCancel}
                >
                    <NTOperationTable
                        data_source={this.state.service_item_list}
                        columns_data_source={this.columns_service_item}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal> */}
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：需求类型类型
 * 描述
 */
@addon('RequirementTypeView', '需求类型类型', '描述')
@reactControl(RequirementTypeView, true)
export class RequirementTypeViewControl extends ReactViewControl {
}