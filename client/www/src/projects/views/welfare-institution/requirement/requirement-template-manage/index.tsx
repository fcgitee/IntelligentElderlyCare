import { Row, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';

let { Option } = Select;

/**
 * 组件：需求模板状态
 */
export interface RequirementTemplateViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 模板状态列表 */
    status_list?: any[];
    /** 组织机构列表 */
    organization_list?: any[];
}

/**
 * 组件：住宿区域
 */
export class RequirementTemplateView extends ReactView<RequirementTemplateViewControl, RequirementTemplateViewState> {

    private columns_data_source = [
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
        }, {
            title: '模板名称',
            dataIndex: 'template_name',
            key: 'template_name',
        }, {
            title: '描述',
            dataIndex: 'description',
            key: 'description',
        }, {
            title: '创建日期',
            dataIndex: 'create_date',
            key: 'create_date',
        }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            status_list: [],
            organization_list: [],

        };
    }

    componentDidMount() {
        // 模板状态
        // request(this, AppServiceUtility.requirement_template_service!.get_requirement_template_list!({}))
        //     .then((datas: any) => {
        //         this.setState({
        //             status_list: datas.result,
        //         });
        //     });
        AppServiceUtility.requirement_template_service.get_requirement_template_list!({}, 1, 100)!
            .then((data: any) => {
                console.info(data);
                let status_list: any[];
                if (data) {
                    status_list = data.result.map((key: any, value: any) => {
                        return <Option key={key.id}>{key.status}</Option>;
                    });
                    this.setState({
                        status_list
                    });
                }
            });
        // 组织机构
        request(this, AppServiceUtility.personnel_service!.get_organizational_list!({}))
            .then((datas: any) => {
                this.setState({
                    organization_list: datas.result,
                });
            });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementTemplate);
    }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }

    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeRequirementTemplate + '/' + contents.id);
        }
    }

    render() {
        // 状态
        // let status_list = this.state.status_list;
        // let status_option_list: any[] = [];
        // status_list!.map((item, idx) => {
        //     status_option_list.push(<Option key={item['id']}>{item['status']}</Option>);
        // });
        // 组织机构
        let organization_list = this.state.organization_list;
        let organization_option_list: any[] = [];
        organization_list!.map((item, idx) => {
            organization_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "模板名称",
                    decorator_id: "name",
                    option: {
                        // childrens: organization_option_list,
                    }
                },
                {
                    type: InputType.select,
                    label: "状态",
                    decorator_id: "status",
                    option: {
                        childrens: this.state.status_list,
                    }
                },
                {
                    type: InputType.date,
                    label: "创建日期",
                    decorator_id: "create_date",
                    option: {
                        // childrens: upper_hotel_zone_option_list,
                    }
                },

            ],
            btn_props: [{
                label: '新增需求模板',
                btn_method: this.add,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.requirement_template_service,
            service_option: {
                select: {
                    service_func: 'get_requirement_template_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_requirement_template'
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：需求模板
 * 描述
 */
@addon('RequirementTemplateView', '需求模板', '描述')
@reactControl(RequirementTemplateView, true)
export class RequirementTemplateViewControl extends ReactViewControl {
}