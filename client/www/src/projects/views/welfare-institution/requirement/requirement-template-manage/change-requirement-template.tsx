import { message, Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ColTypeStr, InputTypeTable, TableList } from "src/business/components/buss-components/table-list";
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
import { IRequirementTemplateService } from "src/projects/models/requirement-template";
import { ROUTE_PATH } from 'src/projects/router';

const { Option } = Select;
/** 模板状态为启用 */
const SRAR_TUSING = '1';
/**
 * 组件：编辑需求模板状态
 */
export interface ChangeRequirementTemplateViewState extends ReactViewState {
    /** 基础数据 */
    base_data?: any;
    /** 数据id */
    selete_obj?: object;
    /** 项目列表 */
    project_list?: any[];
    /** 需求类型列表 */
    requirement_type_list?: any[];
}

/**
 * 组件：编辑需求模板视图
 */
export class ChangeRequirementTemplateView extends ReactView<ChangeRequirementTemplateViewControl, ChangeRequirementTemplateViewState> {

    constructor(props: any) {
        super(props);
        this.state = {
            selete_obj: {},
            project_list: [],
            requirement_type_list: [],
        };
    }
    /** 权限服务 */
    requirementTemplateServer?() {
        return getObject(this.props.requirementTemplateServer_Fac!);
    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: any) => {
    }

    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.changeRequirementTemplate);
    }
    /** 保存按钮回调方法 */
    save_value = (value: any) => {
        if (!this.props.match!.params.key) {
            value['status'] = SRAR_TUSING;
        }
        this.requirementTemplateServer!()!.update_requirement_template!(value)!
            .then((data: any) => {
                if (data === 'Success') {
                    message.info('保存成功');
                    this.props.history!.push(ROUTE_PATH.requirementTemplate);
                }
            });
    }
    componentWillMount() {
        AppServiceUtility.requirement_project_service.get_list!({}, 1, 100)!
            .then((data: any) => {
                console.info(data);
                let project_list: any[];
                project_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.project_name}</Option>;
                });
                this.setState({
                    project_list
                });

            });
        AppServiceUtility.requirement_type_service.get_list!({}, 1, 100)!
            .then((data: any) => {
                console.info(data);
                let requirement_type_list: any[];
                requirement_type_list = data.result.map((key: any, value: any) => {
                    return <Option key={key.id}>{key.name}</Option>;
                });
                this.setState({
                    requirement_type_list
                });

            });
    }
    componentDidMount() {

        if (this.props.match!.params.key) {
            this.setState({
                selete_obj: {
                    id: this.props.match!.params.key
                }
            });
        }
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let columns_data_source = [{
            title: '分类名称',
            dataIndex: 'category_name',
            key: 'category_name',
            type: 'input' as ColTypeStr
        }, {
            title: '需求项目',
            dataIndex: 'requirement_projects',
            key: 'requirement_projects',
            type: 'select' as ColTypeStr,
            option: {
                placeholder: "请选择需求项目",
                childrens: this.state.project_list
            }
        }, {
            title: '优先级',
            dataIndex: 'priority',
            key: 'priority',
            type: 'input' as ColTypeStr
        }];
        let user_manege = {
            form_items_props: [
                {
                    type: InputTypeTable.input,
                    label: "模板名称",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入模板名称", }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "template_name",
                },
                {
                    type: InputTypeTable.select,
                    label: "需求类型",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请选择需求类型", }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "requirement_type",
                    option: {
                        childrens: this.state.requirement_type_list
                    }

                }, {
                    type: InputTypeTable.date,
                    label: "创建时间",
                    field_decorator_option: {
                        rules: [{ required: true, message: "请输入创建时间", }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "create_date",
                },
                {
                    type: InputTypeTable.input,
                    label: "备注",
                    field_decorator_option: {
                        rules: [{ required: false, message: "" }],
                    } as GetFieldDecoratorOptions,
                    decorator_id: "remark",
                }
            ],
            submit_props: {
                text: "保存",
                cb: this.save_value
            },
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            add_row_text: "新增",
            table_field_name: 'dataSource',
            // table配置
            columns_data_source: columns_data_source,
            other_label_type: [{ type: 'icon', label_key: 'icon_edit', label_parameter: { icon: 'antd@edit' } }],
            showHeader: true,
            bordered: false,
            show_footer: true,
            rowKey: 'id',
            service_option: {
                service_object: AppServiceUtility.requirement_template_service,
                operation_option: {
                    query: {
                        func_name: "get_requirement_template_list",
                        arguments: [this.state.selete_obj, 1, 10]
                    },
                    // save: {
                    //     func_name: "update_service_type"
                    // }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.requirementTemplate); },
            id: this.props.match!.params.key
        };

        return (
            <TableList {...user_manege} />
        );
    }
}

/**
 * 控件：需求模板编辑控件
 * @description 需求模板编辑控件
 * @author
 */
@addon('ChangeRequirementTemplateView', '需求模板编辑控件', '需求模板编辑控件')
@reactControl(ChangeRequirementTemplateView, true)
export class ChangeRequirementTemplateViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 评估模板服务 */
    public requirementTemplateServer_Fac?: Ref<IRequirementTemplateService>;

}