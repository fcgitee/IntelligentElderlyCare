import { Form } from 'antd';
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { RoomChangeView } from "../room-change/change-room";
import { MainContent } from "src/business/components/style-components/main-content";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { ROUTE_PATH } from "src/projects/router";
export interface ServiceItemPackageFormValues {
    id?: string;
}
/**
 * 组件：预约入院视图控件状态
 */
export interface ChangeFeeViewState extends ReactViewState {
    user_id: string;
    bed_id: string;

}

/**
 * 组件：预约入院视图控件
 * 描述
 */
export class ChangeFeeView extends ReactView<ChangeFeeViewControl, ChangeFeeViewState> {
    constructor(props: ChangeFeeViewControl) {
        super(props);
        this.state = {
            user_id: '',
            bed_id: ''
        };
    }

    componentWillMount() {
        request_func(this, AppServiceUtility.check_in_service.get_check_in_list, [{ "id": this.props.match!.params.key }, 1, 1], (data: any) => {
            this.setState({
                user_id: data.result[0].user_id,
                bed_id: data.result[0].bed_id
            });
        });

    }
    render() {
        return (
            <MainContent>
                {this.state.user_id && this.state.bed_id ? <RoomChangeView
                    showOld={true}
                    noChangeBed={true}
                    history={this.props.history}
                    backPath={ROUTE_PATH.feeList}
                    user_id={this.state.user_id}
                    bed_id={this.state.bed_id}
                    form={this.props.form}
                /> : null}</MainContent>
        );
    }
}

/**
 * 控件：预约入院视图控件
 * 描述
 */
@addon('ChangeFeeView', '预约登记视图控件', '描述')
@reactControl(Form.create<any>()(ChangeFeeView), true)
export class ChangeFeeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}