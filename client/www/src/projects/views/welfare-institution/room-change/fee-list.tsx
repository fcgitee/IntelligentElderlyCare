import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { addon, Permission } from "pao-aop";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from 'src/projects/router';
import { AppServiceUtility } from 'src/projects/app/appService';
import { isPermission } from "src/projects/app/permission";
/**
 * 组件：费用调整页面
 */
export interface FeeListState extends ReactViewState {
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：费用调整页面
 * 描述
 */
export class FeeList extends ReactView<FeeListControl, FeeListState> {
    private columns_data_source = [{
        title: '长者编号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '床位名称',
        dataIndex: 'bed_name',
        key: 'bed_name',
    },
    {
        title: '入住时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },];
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeFee + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let allowance_read = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者编号",
                    decorator_id: "user_number"
                }, {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "床位编号",
                    decorator_id: "bed_number"
                }, {
                    type: InputType.input,
                    label: "床位名称",
                    decorator_id: "bed_name",
                    option: {
                        placeholder: '请输入房间名称',
                        childrens: []
                    },

                },
                {
                    type: InputType.date,
                    label: "入住时间",
                    decorator_id: "create_date",
                    option: {
                        placeholder: '请选择入住时间',
                    },

                },
            ],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.check_in_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let allowance_read_list = Object.assign(allowance_read, table_param);
        return (
            <SignFrameLayout {...allowance_read_list} />
        );
    }
}

/**
 * 控件：费用调整页面
 * 描述
 */
@addon(' FeeList', '费用调整页面', '提示')
@reactControl(FeeList, true)
export class FeeListControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}