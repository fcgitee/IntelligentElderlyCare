import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "../../../../app/util-tool";
import { request } from "src/business/util_tool";
import { Select } from "antd";
const Option = Select.Option;

/**
 * 组件：水电抄表状态
 */
export interface ChangeHydropowerViewState extends ReactViewState {

    // 床位列表
    hotel_list: any;
    base_data: any;
}

/**
 * 组件：水电抄表视图
 */
export class ChangeHydropowerView extends ReactView<ChangeHydropowerViewControl, ChangeHydropowerViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            hotel_list: [],
            base_data: {}
        };
    }
    componentDidMount() {
        // 床位编号
        request(this, AppServiceUtility.hotel_zone_service.get_list!({}))
            .then((datas: any) => {
                this.setState({
                    hotel_list: datas.result,
                });
            });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.hydropower);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let hotel_list = this.state.hotel_list;
        let bed_option_list: any[] = [];
        hotel_list!.map((item: any) => {
            bed_option_list.push(<Option key={item.id}>{item.zone_name}</Option>);
        });
        const base_data = this.state.base_data;
        let edit_props = {
            form_items_props: [
                {
                    title: "水电表",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.select,
                            label: "区域",
                            decorator_id: "hotel_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择区域" }],
                                initialValue: base_data ? base_data.bed_name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择区域",
                                childrens: bed_option_list
                            }
                        }, {
                            type: InputType.antd_input_number,
                            label: "用电量",
                            decorator_id: "electricity_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入用电量" }],
                                initialValue: base_data ? base_data.electricity_number : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入用电量"
                            }
                        }, {
                            type: InputType.antd_input_number,
                            label: "用水量",
                            decorator_id: "water_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入用水量" }],
                                initialValue: base_data ? base_data.water_number : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入用水量"
                            }
                        }, {
                            type: InputType.date,
                            label: "开始时间",
                            decorator_id: "start_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择开始时间" }],
                                initialValue: base_data ? base_data.start_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择开始时间"
                            }
                        },
                        {
                            type: InputType.date,
                            label: "结束时间",
                            decorator_id: "end_date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择结束时间" }],
                                initialValue: base_data ? base_data.end_dete : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择结束时间"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                initialValue: base_data ? base_data.remark : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.hydropower);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.hydropower,
                operation_option: {
                    save: {
                        func_name: "update_hydropower"
                    },
                    query: {
                        func_name: "get_hydropower_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.hydropower); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：水电抄表编辑控件
 * @description 水电抄表编辑控件
 * @author
 */
@addon('ChangeHydropowerView', '水电抄表编辑控件', '水电抄表编辑控件')
@reactControl(ChangeHydropowerView, true)
export class ChangeHydropowerViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}