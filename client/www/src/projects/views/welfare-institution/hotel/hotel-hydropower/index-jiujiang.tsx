import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
import { UploadFile } from "src/business/components/buss-components/upload-file/index";
import { exprot_excel, request_func } from "src/business/util_tool";

// const Option = Select.Option;
/**
 * 组件：水电抄表状态
 */
export interface HydropowerJjViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
    config: any;

}

/**
 * 组件：水电抄表
 */
export class HydropowerJjView extends ReactView<HydropowerJjViewControl, HydropowerJjViewState> {

    private columns_data_source = [{
        title: '长者',
        dataIndex: 'user.name',
        key: 'user.name',
    }, {
        title: '身份证',
        dataIndex: 'user.id_card',
        key: 'user.id_card',
    }, {
        title: '用电量',
        dataIndex: 'electricity_number',
        key: 'electricity_number',
    },
    {
        title: '金额',
        dataIndex: 'money',
        key: 'money',
    },
    {
        title: '开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            config: {
                upload: false,
                func: AppServiceUtility.hydropower.upload_hydropower,
                title: '导入电费',
                isFormat: true,
            }
        };
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    upload = () => {
        let config = this.state.config;
        config.upload = true;
        this.setState({ config });
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeHydropowerJj);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeHydropowerJj + '/' + contents.id);
        }
    }
    // 下载
    export = () => {
        request_func(this, AppServiceUtility.hydropower.export_template, [], (data: any) => {
            console.info('返回数据', data);
            exprot_excel([{ name: '电费', value: data }], '电费模板', 'xls'); // 下载excel
        });
    }
    render() {
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者",
                    decorator_id: "name",
                    option: {
                    }
                },
                {
                    type: InputType.date,
                    label: "开始时间",
                    decorator_id: "start_date"
                }, {
                    type: InputType.date,
                    label: "结束时间",
                    decorator_id: "end_date"
                },

            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '导入',
                btn_method: this.upload,
                icon: 'plus'
            }, {
                label: '模板下载',
                btn_method: this.export,
                icon: 'download'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hydropower,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_jiujiang_hydropower'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
                <UploadFile {...this.state.config} />
            </Row>
        );
    }
}

/**
 * 控件：水电抄表
 * 描述
 */
@addon('HydropowerJjView', '水电抄表', '描述')
@reactControl(HydropowerJjView, true)
export class HydropowerJjViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}