import { Row, Select, Collapse } from "antd";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { edit_props_info } from "src/projects/app/util-tool";
import { MainContent } from "src/business/components/style-components/main-content";
import FormCreator, { InputType as InputTypeNew } from "src/business/components/buss-components/form-creator";
import { ROUTE_PATH } from "src/projects/router";
const { Panel } = Collapse;
const Option = Select.Option;
/**
 * 组件：水电抄表状态
 */
export interface WaterElectricityViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
    /** 长者姓名 */
    user_name: Array<string>;
    /** 水电数据 */
    data: any;
    data_base: any;

}

/**
 * 组件：水电抄表
 */
export class WaterElectricityView extends ReactView<WaterElectricityViewControl, WaterElectricityViewState> {

    private columns_data_source = [{
        title: '长者',
        dataIndex: 'user.name',
        key: 'user.name',
    }, {
        title: '用电量',
        dataIndex: 'electricity_number',
        key: 'electricity_number',
    },
    {
        title: '用电金额',
        dataIndex: 'electricity_money',
        key: 'electricity_money',
    },
    {
        title: '用水量',
        dataIndex: 'water_number',
        key: 'water_number',
    }, {
        title: '用水量',
        dataIndex: 'water_money',
        key: 'water_money',
    }, {
        title: '时间',
        dataIndex: 'date',
        key: 'date',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: [],
            user_name: [],
            data: {},
            data_base: {}
        };
    }

    componentDidMount() {
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });

        // 
        // request(this, AppServiceUtility.hydropower.get_water_electricity!.())
    }

    /** 新增按钮 */
    add = () => {
        // this.props.history!.push(ROUTE_PATH.changeHydropower);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        'icon_edit' === type && this.props.history!.push(ROUTE_PATH.waterElectricity + '/' + contents.id);
    }
    render() {
        let bed_list = this.state.bed_list;
        let bed_option_list: any[] = [];
        bed_list!.map((item: any) => {
            bed_option_list.push(<Option key={item.id}>{item.bed_code}</Option>);
        });
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者",
                    decorator_id: "user_name",
                },
                {
                    type: InputType.rangePicker,
                    label: "时间段",
                    decorator_id: "date"
                }
            ],
            btn_props: [{
                label: '导入',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hydropower,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_water_electricity'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);

        // 水电新增
        const columns_data_source = [{
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '身份证号',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '性别',
            dataIndex: 'personnel_info.sex',
            key: 'personnel_info.sex',
        }, {
            title: '出生年月',
            dataIndex: 'personnel_info.date_birth',
            key: 'personnel_info.date_birth',
        }];
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'get_check_in_list',
            id_field: 'user_id',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
                service_condition: [{}, 1, 10]
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    input_props: [
                        {
                            type: InputTypeNew.modal_search,
                            label: "长者",
                            decorator_id: "user_id",
                            id_field: 'user_id',
                            col_span: 12,
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择长者" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入入住人",
                                modal_search_items_props: modal_search_items_props

                            }
                        },
                        {
                            type: InputTypeNew.date,
                            label: "时间",
                            col_span: 12,
                            decorator_id: "date",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择开始时间" }],
                                // initialValue: this.state.data.start_date ? this.state.data.start_date : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择时间"
                            }
                        },
                        {
                            type: InputTypeNew.input_number,
                            label: "用水量（吨）",
                            col_span: 12,
                            decorator_id: "water_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入用水量" }],
                                initialValue: this.state.data_base.water_number ? this.state.data_base.water_number : 0,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        },
                        {
                            type: InputTypeNew.input_number,
                            label: "用水金额（元）",
                            col_span: 12,
                            decorator_id: "water_money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入金额" }],
                                initialValue: this.state.data_base.water_money ? this.state.data_base.water_money : 0,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        },
                        {
                            type: InputTypeNew.input_number,
                            label: "用电量（度）",
                            col_span: 12,
                            decorator_id: "electricity_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入用电" }],
                                initialValue: this.state.data_base.electricity_number ? this.state.data_base.electricity_number : 0,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入用电量"
                            }
                        },
                        {
                            type: InputTypeNew.input_number,
                            label: "用电金额（元）",
                            col_span: 12,
                            decorator_id: "electricity_money",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入金额" }],
                                initialValue: this.state.data_base.electricity_number ? this.state.data_base.electricity_number : 0,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入金额"
                            }
                        },
                        {
                            type: InputTypeNew.antd_input,
                            label: "备注",
                            col_span: 12,
                            decorator_id: "remark",
                            field_decorator_option: {
                                // initialValue: this.state.data_base.remark,
                            } as GetFieldDecoratorOptions,
                        },
                    ]
                }
            ],
            other_btn_propps: [],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.hydropower,
                operation_option: {
                    save: {
                        func_name: "update_water_electricity"
                    },
                    query: {
                        func_name: 'get_water_electricity',
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            id: this.props.match!.params.key,
            succ_func: () => { this.props.history!.push(ROUTE_PATH.waterElectricity); },

        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <Row>
                <MainContent>
                    <div className='form-bottom'>
                        <Collapse defaultActiveKey={['1']}>
                            <Panel header="水电抄表" key="1">
                                <FormCreator {...edit_props_list} />
                            </Panel>
                        </Collapse>
                    </div>
                </MainContent>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：水电抄表
 * 描述
 */
@addon('WaterElectricityView', '水电抄表', '描述')
@reactControl(WaterElectricityView, true)
export class WaterElectricityViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}