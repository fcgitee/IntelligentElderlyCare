import { Row, Select } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
const Option = Select.Option;
/**
 * 组件：水电抄表状态
 */
export interface HydropowerViewState extends ReactViewState {
    // 床位列表
    bed_list: any;
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：水电抄表
 */
export class HydropowerView extends ReactView<HydropowerViewControl, HydropowerViewState> {

    private columns_data_source = [{
        title: '房间编号',
        dataIndex: 'hotel_name',
        key: 'hotel_name',
    }, {
        title: '用水量',
        dataIndex: 'water_number',
        key: 'water_number',
    }, {
        title: '用电量',
        dataIndex: 'electricity_number',
        key: 'electricity_number',
    }, {
        title: '开始时间',
        dataIndex: 'start_date',
        key: 'start_date',
    }, {
        title: '结束时间',
        dataIndex: 'end_date',
        key: 'end_date',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            bed_list: []
        };
    }

    componentDidMount() {
        // 床位编号
        request(this, AppServiceUtility.hotel_zone_type_service.get_bed_list!({}))
            .then((datas: any) => {
                this.setState({
                    bed_list: datas.result,
                });
            });
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeHydropower);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeHydropower + '/' + contents.id);
        }
    }

    render() {
        let bed_list = this.state.bed_list;
        let bed_option_list: any[] = [];
        bed_list!.map((item: any) => {
            bed_option_list.push(<Option key={item.id}>{item.bed_code}</Option>);
        });
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.select,
                    label: "床位编号",
                    decorator_id: "bed_id",
                    option: {
                        childrens: bed_option_list,
                    }
                },
                {
                    type: InputType.rangePicker,
                    label: "开始时间",
                    decorator_id: "start_date"
                }, {
                    type: InputType.rangePicker,
                    label: "结束时间",
                    decorator_id: "end_date"
                },

            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hydropower,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_hydropower'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：水电抄表
 * 描述
 */
@addon('HydropowerView', '水电抄表', '描述')
@reactControl(HydropowerView, true)
export class HydropowerViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}