import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { HotelZone, HotelZoneType } from "src/projects/models/hotel_zone";
import { Organizational } from "src/projects/models/personnel-organizational";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：住宿区域状态
 */
export interface HotelZoneViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 住宿服务集合 */
    // service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 住宿服务总条数 */
    or?: number;
    /** 组织机构列表 */
    organizationn_list?: Organizational[];
    /** 区域类别列表 */
    zone_list?: HotelZoneType[];
    /** 上级区域列表 */
    upper_hotel_zone_list?: HotelZone[];
    /** 接口名 */
    request_url?: string;
    /** 组织机构 */
    org_list?: any;
}

/**
 * 组件：住宿区域
 */
export class HotelZoneView extends ReactView<HotelZoneViewControl, HotelZoneViewState> {

    private columns_data_source = [
        {
            title: '机构名称',
            dataIndex: 'organization.name',
            key: 'organization.name',
        },
        {
            title: '区域名称',
            dataIndex: 'zone_name',
            key: 'zone_name',
        }, {
            title: '区域类别',
            dataIndex: 'hotel_zone_type_name',
            key: 'hotel_zone_type_name',
        }, {
            title: '上级区域',
            dataIndex: 'upper_hotel_zone_name',
            key: 'upper_hotel_zone_name',
        }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false,
            organizationn_list: [],
            zone_list: [],
            upper_hotel_zone_list: [],
            org_list: [],
        };
    }

    componentDidMount() {
        // 组织机构
        // request(this, AppServiceUtility.personnel_service!.get_organizational_list!({}))
        //     .then((datas: any) => {
        //         this.setState({
        //             organizationn_list: datas.result,
        //         });
        //     });
        // 区域类别
        // request(this, AppServiceUtility.hotel_zone_type_service!.get_list!({}))
        //     .then((datas: any) => {
        //         this.setState({
        //             zone_list: datas.result,
        //         });
        //     });
        // 上级区域
        // request(this, AppServiceUtility.hotel_zone_service!.get_list!({}))
        //     .then((datas: any) => {
        //         this.setState({
        //             upper_hotel_zone_list: datas.result,
        //         });
        //     });
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeHotelZone);
    }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeHotelZone + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院", "幸福院", "民政", "服务商"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    render() {
        // 组织机构
        // let organization_list = this.state.organizationn_list;
        // let organization_option_list: any[] = [];
        // organization_list!.map((item, idx) => {
        //     organization_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        // });

        // 区域类别
        // let zone_list = this.state.zone_list;
        // let zone_option_list: any[] = [];
        // zone_list!.map((item, idx) => {
        //     zone_option_list.push(<Option key={item['name']}>{item['name']}</Option>);
        // });

        // 上级区域
        // let upper_hotel_zone_list = this.state.upper_hotel_zone_list;
        // let upper_hotel_zone_option_list: any[] = [];
        // upper_hotel_zone_list!.map((item, idx) => {
        //     upper_hotel_zone_option_list.push(<Option key={item['id']}>{item['zone_name']}</Option>);
        // });

        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "区域名称",
                    decorator_id: "zone_name"
                },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name"
                // },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hotel_zone_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [this.props.select_type ? this.props.select_type : {}, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：住宿区域
 * 描述
 */
@addon('HotelZoneView', '住宿区域', '描述')
@reactControl(HotelZoneView, true)
export class HotelZoneViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
    select_type?: any;
}