import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { HotelZone, HotelZoneType } from "src/projects/models/hotel_zone";
import { Organizational } from "src/projects/models/personnel-organizational";

let { Option } = Select;

/**
 * 组件：住宿区域页面状态
 */
export interface ChangeHotelZoneState extends ReactViewState {
    /** 组织机构列表 */
    organizationn_list?: Organizational[];
    /** 区域类别列表 */
    zone_list?: HotelZoneType[];
    /** 上级区域列表 */
    upper_hotel_zone_list?: HotelZone[];
}

/**
 * 组件：住宿区域页面
 * 描述
 */
export class ChangeHotelZone extends ReactView<ChangeHotelZoneControl, ChangeHotelZoneState> {
    constructor(props: any) {
        super(props);
        this.state = {
            organizationn_list: [],
            zone_list: [],
            upper_hotel_zone_list: [],
        };
    }
    componentDidMount() {
        /** 组织机构列表 */
        request(this, AppServiceUtility.personnel_service['get_organizational_list']!({}))
            .then((datas: any) => {
                // console.log('datas.result', datas.result);
                this.setState({
                    organizationn_list: datas.result,
                });
            });
        /** 区域类别列表 */
        request(this, AppServiceUtility.hotel_zone_type_service['get_list_all']!({}))
            .then((datas: any) => {
                this.setState({
                    zone_list: datas.result,
                });
            });
        /** 上级区域列表 */
        request(this, AppServiceUtility.hotel_zone_service['get_list']!({}))
            .then((datas: any) => {
                // console.log('datas.result', datas.result);
                this.setState({
                    upper_hotel_zone_list: datas.result,
                });
            });
    }
    render() {
        // 组织机构
        let organization_list = this.state.organizationn_list;
        let organization_option_list: any[] = [];
        organization_list!.map((item, idx) => {
            organization_option_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });

        // 区域类别
        let zone_list = this.state.zone_list;
        let zone_option_list: any[] = [];

        zone_list!.map((item, idx) => {
            zone_option_list.push(<Option key={item['name']}>{item['name']}</Option>);
        });

        // 上级区域
        let upper_hotel_zone_list = this.state.upper_hotel_zone_list;
        let upper_hotel_zone_option_list: any[] = [];
        upper_hotel_zone_list!.map((item, idx) => {
            upper_hotel_zone_option_list.push(<Option key={item['id']}>{item['zone_name']}</Option>);
        });

        let edit_props = {
            form_items_props: [
                {
                    title: "住宿区域",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "区域名称",
                            decorator_id: "zone_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入区域名称" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入区域名称",
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "区域编号",
                            decorator_id: "zone_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入区域编号" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入区域编号，格式：（区：字母，楼：字母，层：数字，房间：数字+字母）"
                            }
                        },
                        {
                            type: InputType.select,
                            label: "区域类别",
                            decorator_id: "hotel_zone_type_name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择区域类别" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择区域类别",
                                childrens: zone_option_list,
                            }
                        }, {
                            type: InputType.select,
                            label: "上级区域",
                            decorator_id: "upper_hotel_zone_id",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择上级区域",
                                childrens: upper_hotel_zone_option_list,
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        history.back();
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
            },
            service_option: {
                service_object: AppServiceUtility.hotel_zone_service,
                operation_option: {
                    query: {
                        func_name: "get_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update"
                    }
                },
            },
            succ_func: () => {
                history.back();
            },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：住宿区域页面
 * 描述
 */
@addon('ChangeHotelZone', '住宿区域页面', '描述')
@reactControl(ChangeHotelZone, true)
export class ChangeHotelZoneControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
}