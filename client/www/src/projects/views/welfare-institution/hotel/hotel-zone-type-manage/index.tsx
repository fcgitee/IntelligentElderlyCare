import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：住宿区域类型状态
 */
export interface HotelZoneTypeViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 住宿区域类型集合 */
    // service_item_list?: ServiceItem[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 住宿区域类型总条数 */
    total?: number;
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：住宿区域类型
 * 描述
 */
export class HotelZoneTypeView extends ReactView<HotelZoneTypeViewControl, HotelZoneTypeViewState> {

    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '编号',
        dataIndex: 'number',
        key: 'number',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeHotelZoneType);
    }

    /** 查询住宿区域类型 */
    // queryServiceItem = () => {
    //     let ids = this.state.select_ids;
    //     if (ids && ids!.length > 0) {
    //         request(this, AppServiceUtility.service_operation_service.get_service_item_list!({ 'service_type_id': ids }))
    //             .then((data: any) => {
    //                 this.setState({
    //                     service_item_list: data.result,
    //                     total: data.total,
    //                     modal_visible: true
    //                 });
    //             });
    //     }
    // }

    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }

    /** 行选择事件 */
    on_row_selection = (selectedRows: any) => {
        let ids: string[] = [];
        selectedRows!.map((item: any, idx: number) => {
            ids.push(item['id']);
        });
        this.setState({
            select_ids: ids
        });
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeHotelZoneType + '/' + contents.id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                },
                {
                    type: InputType.input,
                    label: "编号",
                    decorator_id: "number"
                },
                {
                    type: InputType.input,
                    label: "备注",
                    decorator_id: "remark"
                }
            ],
            btn_props: [{
                label: '新增住宿区域类别',
                btn_method: this.add,
                icon: 'plus'
            }],
            on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hotel_zone_type_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                {/* <Modal
                    title="住宿区域类型列表"
                    visible={this.state.modal_visible}
                    // onOk={() => this.handleOk(formProps)}
                    onCancel={this.handleCancel}
                >
                    <NTOperationTable
                        data_source={this.state.service_item_list}
                        columns_data_source={this.columns_service_item}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal> */}
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：住宿区域类型
 * 描述
 */
@addon('HotelZoneTypeView', '住宿区域类型', '描述')
@reactControl(HotelZoneTypeView, true)
export class HotelZoneTypeViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}