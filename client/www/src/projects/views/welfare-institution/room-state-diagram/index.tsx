import { Form } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
/**
 * 组件：房态图视图状态
 */
export interface RoomStateDiagramViewState extends ReactViewState {

}

/**
 * 组件：房态图视图
 * 描述
 */
export class RoomStateDiagramView extends ReactView<RoomStateDiagramViewControl, RoomStateDiagramViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
    }
    render() {
        return (
            null
        );
    }
}

/**
 * 控件：房态图视图控制器
 * 描述
 */
@addon('RoomStateDiagramView', '房态图', '房态图')
@reactControl(Form.create<any>()(RoomStateDiagramView), true)
export class RoomStateDiagramViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}