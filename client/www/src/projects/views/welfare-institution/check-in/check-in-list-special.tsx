
import { message, Modal, Row } from "antd";
import moment from "moment";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import SignFrameLayout, { InputType, InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：长者入住登记视图 */
export interface CheckInListJjViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
    /** 显示弹框 */
    visible?: boolean;
    /** 所选长者 */
    user_id?: string;
    /** 接口名 */
    request_url?: string;
    ischeckin?: boolean;
    no_pay_visable?: boolean;
    /** 组织机构 */
    org_list?: any;
}
/** 组件：长者入住登记视图 */
export class CheckInListJjView extends ReactView<CheckInListViewJjControl, CheckInListJjViewState> {
    private columns_data_source = [
        {
            title: '组织机构',
            dataIndex: 'organization_name',
            key: 'organization_name',
        },
        {
            title: '证件号码',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '年龄',
            dataIndex: 'age',
            key: 'age',
        }, {
            title: '性别',
            dataIndex: 'sex',
            key: 'sex',
        }, {
            title: '床位名称',
            dataIndex: 'bed_name',
            key: 'bed_name',
        },
        {
            title: '护理等级',
            dataIndex: 'nursing_level',
            key: 'nursing_level',
        },
        {
            title: '入住时间',
            dataIndex: 'create_date',
            key: 'create_date',
        },];
    private columns_data = [
        {
            title: '长者编号',
            dataIndex: 'id_card',
            key: 'id_card',
        }, {
            title: '长者姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '护理等级',
            dataIndex: 'recode_nursing_level',
            key: 'recode_nursing_level',
        },
        {
            title: '年龄',
            dataIndex: 'personnel_info.age',
            key: 'personnel_info.age',
        }, {
            title: '性别',
            dataIndex: 'personnel_info.sex',
            key: 'personnel_info.sex',
        }
    ];
    constructor(props: CheckInListViewJjControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {},
            visible: false,
            ischeckin: false,
            no_pay_visable: false,
            org_list: [],
        };
    }
    componentDidMount() {
        AppServiceUtility.person_org_manage_service.get_organization_tree_list_forName!({ type_list: ["福利院"] })!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
    }
    /** 新增按钮 */
    addArchives = () => {
        this.setState({ ischeckin: true });
        // this.props.history!.push(ROUTE_PATH.checkInNew);
    }
    /** 退住 */
    retreat = () => {
        this.setState({
            visible: true
        });
    }
    /** 修改状态 */
    setStateFun(data: any) {
        this.setState(data);
    }

    retreatCb = () => {
        // 退住接口
        request_func(this, AppServiceUtility.accommodation_process_service.check_out_process, [this.state.user_id], (data: any) => {
            if (data === 'Success') {
                message.info('退住成功！');
            } else {
                message.info(data);
            }
            this.props.history!.push(ROUTE_PATH.checkInJj);
        });
    }

    /** 调用退住方法 */
    check_out = () => {
        // 未存在当天扣扣，则未结算
        AppServiceUtility.check_in_service.check_if_pay_done!({ id: this.state.user_id, today: moment().minutes(0).hours(0).seconds(0).utc().format() })!
            .then((data) => {
                if (!data) {
                    this.setState({
                        no_pay_visable: true
                    });
                } else {
                    this.retreatCb();
                }
            });
    }
    /** 跳转到床位绑定页面 */
    check_in() {
        this.setState({ ischeckin: false });
        this.props.history!.push(ROUTE_PATH.checkInNewJj + '/' + this.state.user_id);

    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push(ROUTE_PATH.checkInDetail + '/' + contents.user_id + 'form=' + 'get_check_in_list_details');
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let modal_search_items_props = {
            // type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'get_check_in_list',
            title: '长者查询',
            id_field: 'user_id',
            name_field: 'name',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        const modal_search_props = {
            // type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data,
            service_name: AppServiceUtility.person_org_manage_service,
            service_func: 'get_wait_checkin_elder_list',
            title: '长者查询',
            id_field: 'id',
            name_field: 'name',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let nursing = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "身份证",
                    decorator_id: "id_card"
                }, {
                    type: InputType.input,
                    label: "床位名称",
                    decorator_id: "bed_name",
                    option: {
                        placeholder: '请输入房间名称',
                        childrens: []
                    },

                },
                {
                    type: InputType.date,
                    label: "入住时间",
                    decorator_id: "create_date",
                    option: {
                        placeholder: '请选择入住时间',
                    },

                },
                // {
                //     type: InputType.date,
                //     label: "退住时间",
                //     decorator_id: "retreat_date",
                //     option: {
                //         placeholder: '请选择入住时间',
                //     },
                // },
                {
                    type: InputType.tree_select,
                    label: "组织机构",
                    col_span: 8,
                    decorator_id: "org_name",
                    option: {
                        showSearch: true,
                        treeNodeFilterProp: 'title',
                        allowClear: true,
                        dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                        treeDefaultExpandAll: true,
                        treeData: this.state.org_list,
                        placeholder: "请选择组织机构",
                    },
                },
                // {
                //     type: InputType.input,
                //     label: "机构名称",
                //     decorator_id: "org_name",
                //     option: {
                //         placeholder: '请输入机构名称',
                //     },
                // },
            ],
            btn_props: [{
                label: '新增入住',
                btn_method: this.addArchives,
                icon: 'plus'
            },
            // {
            // label: '导出',
            // btn_method: () => {
            //     request_func(this, AppServiceUtility.check_in_service.export_excel, []);
            // },
            // icon: 'download',
            // },
            {
                label: '办理退住',
                btn_method: this.retreat,
                icon: 'logout',
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.check_in_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                // delete: {
                //     service_func: 'delete_check_in'
                // }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let nursing_list = Object.assign(nursing, table_param);
        return (
            <>
                <Row>
                    <SignFrameLayout {...nursing_list} />
                    <Modal title="选择退住长者" visible={this.state.visible} onOk={this.check_out} onCancel={() => this.setState({ visible: false })} width={'800px'}>
                        <ModalSearch modal_search_items_props={modal_search_items_props} onChange={(value: any) => this.setState({ user_id: value })} />
                    </Modal>
                    {this.state.ischeckin ?
                        <Modal title="长者入住" visible={true} onOk={() => this.check_in()} onCancel={() => this.setState({ ischeckin: false })} width={'800px'}>
                            <ModalSearch modal_search_items_props={modal_search_props} onChange={(value: any) => this.setState({ user_id: value })} /></Modal> : null}
                </Row>
                <Modal title="警告" visible={this.state.no_pay_visable} onOk={this.retreatCb} onCancel={() => this.setState({ no_pay_visable: false })} okText={'是'} cancelText={'否'} width={'800px'}>
                    该长者尚存在费用未结算情况，是否确定退住？
                </Modal>
            </>
        );
    }
}

/**
 * 控件：长者入住登记视图控制器
 * @description 长者入住登记视图
 * @author
 */
@addon('CheckInListJjView', '长者入住登记列表视图', '长者入住登记列表视图')
@reactControl(CheckInListJjView, true)
export class CheckInListViewJjControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}