import { ReactViewState, reactControl, ReactViewControl } from "pao-aop-client";
import React from "react";
import { MainContent } from "src/business/components/style-components/main-content";
import { addon, Permission } from "pao-aop";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
import { InputType } from "src/business/components/buss-components/form-creator";
import { Steps, Form, Select, Button, Row, Radio, Card, Col, Checkbox, Input, Icon, message } from 'antd';
import TextArea from "antd/lib/input/TextArea";
import { request_func } from "src/business/util_tool";
import { MainCard } from "src/business/components/style-components/main-card";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { IDCard, IDCardReader } from "src/business/device/id_card";
import { UserEntry } from "src/projects/components/user-entry";
import { remote } from "src/projects/remote";
import { FileUploadBtn } from "src/business/components/buss-components/file-operation-btn/file-upload-btn";
import { beforeUpload } from "src/projects/app/util-tool";
import PreliminaryAssessmentForm from "./../reservation-registration/preliminary-assessment-form";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

// import SignFrameLayout, { InputType as inputType } from "src/business/components/buss-components/sign-frame-layout";
let { Option } = Select;
const { Step } = Steps;

/** 状态：入住登记新建视图 */
export interface CheckInNewViewState extends ReactViewState {
    /** 当前步骤 */
    current: number;
    /**
     *  入住评估
     */
    /** 评估项目列表 */
    project_list?: any[];

    /** 模板列表 */
    template_list?: any[];

    /** 
     * 服务项目 
     */
    /** 项目列表 */
    item_list?: any[];
    /** 项目列表集合 */
    all_item_list?: any[];
    /** 基础数据 */
    base_data?: {};
    /** 适用范围 */
    application_scope?: any[];
    /** 组织机构 */
    organizational?: any[];
    /** 床位 */
    bed_list: any[];
    /** 登记成功时间 */
    create_date: string;
    /** 身份证 */
    id_card?: IDCard;
    /**
     *  提交数据 
     */
    /** 长者信息 */
    user_info?: object;
    /** 入住评估 */
    evaluation_info?: object;
    /** 服务项目 */
    serviceValue?: object;
    /*申请表里的选项和值 */
    project?: any;
    /*单选按钮的值 */
    radioValue?: any;
    /*多行文本的值 */
    textAreaValue?: any;
    checkValue?: any;
    pro_id?: string;
    /** 表单值集合 */
    form_value_list?: {};
    bed_info: any;
}
export interface CompetenceAssessmentListFormValues {
    id?: string;
}
export interface ServiceItemPackageFormValues {
    id?: string;
}
/** 组件：入住登记新建视图 */
export class CheckInNewView extends React.Component<CheckInNewViewControl, CheckInNewViewState> {
    constructor(props: CheckInNewViewControl) {
        super(props);
        this.state = {
            current: 0,
            user_info: {},
            template_list: [],
            project_list: [],
            item_list: [],
            all_item_list: [],
            evaluation_info: {},
            application_scope: [],
            organizational: [],
            serviceValue: {},
            bed_list: [],
            base_data: {},
            create_date: '',
            id_card: {},
            project: [],
            radioValue: [],
            textAreaValue: [],
            form_value_list: {},
            bed_info: {}
        };
    }
    /** 下一步 */
    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }
    /** 上一步 */
    prev = () => {
        const current = this.state.current - 1;
        this.setState({ current });
    }
    /** 处理身份证读取 */
    handleIDCardReader?() {
        // 调试环境下（npm start）驱动地址为/drive/ZKIDROnline.exe
        // 正式环境下（python）驱动地址为/download/build/www/drive/ZKIDROnline.exe
        const dev = process.env.NODE_ENV === 'development',
            drive_path = dev ? `/drive/ZKIDROnline.exe` : `/download/build/www/drive/ZKIDROnline.exe`;
        new IDCardReader(drive_path)
            .read_async!()!
            .then(id_card => {
                this.setState({ id_card });
            })
            .catch(error => {
                message.warning(error.message);
            });
    }
    /** 新增按钮 */
    addBehavioralCompetenceAssessment = () => {
        this.props.history!.push(ROUTE_PATH.competenceAssessment);
    }
    // 评估提交
    handleSubmit = (err: any, evaluation_info: any) => {
        this.setState({ evaluation_info });
        this.next();
    }
    goBack = () => {
        this.props.history!.goBack();
    }
    componentDidMount() {
        // 入住评估模板
        AppServiceUtility.assessment_template_service.get_assessment_template!({ template_type: '详细能力评估' })!
            .then((data: any) => {
                if (data.result) {
                    this.setState({
                        template_list: data.result
                    });
                }
            });
        // 床位列表
        request_func(this, AppServiceUtility.hotel_zone_type_service.get_bed_list_no_user, [{ state: true }], (data: any) => {
            this.setState({
                bed_list: data.result
            });
        });
        // 服务项目
        AppServiceUtility.services_project_service.get_services_project_list!({})!
            .then((data: any) => {
                if (data) {
                    this.setState({
                        item_list: data['result']
                    });
                }
            });
        // 适用范围
        request_func(this, AppServiceUtility.service_scope_service.get_service_scope_list, [{}, 1, 10], (data: any) => {
            this.setState({
                application_scope: data.result!.map((val: any) => {
                    return { label: val.name, value: val.id };
                })
            });
        });
    }
    /** 选择模板选择框值改变 */
    template_change = (value: any) => {
        console.info(value);
        AppServiceUtility.assessment_template_service.get_assessment_template!({ id: value }, 1, 10)!
            .then((data: any) => {
                console.info(data);
                if (data.result) {
                    this.setState({
                        project_list: data.result
                    });
                }
            });
    }
    /**
     * 服务项目
     */
    /** 添加项目按钮 */
    addItem = () => {
        let list: any[] = this.state.all_item_list!;
        list.push({});
        this.setState({
            all_item_list: list
        });
    }
    itemOnChange = (value: any, option: any) => {
        let list: any[] = this.state.all_item_list!;

        AppServiceUtility.services_project_service.get_services_project_list!({ id: value })!
            .then((data: any) => {
                // console.log('-----------data', data);
                if (list.length > 0 && data) {
                    list[option.key] = data.result[0];
                    this.setState({
                        all_item_list: list
                    });
                }
            });
    }
    bedOnChange = (value: any) => {
        // 分割出服务包id和床位id
        this.state.bed_list.map!((val: any) => {
            if (val.id === value) {
                this.setState({ bed_info: val }, () => {
                    request_func(this, AppServiceUtility.service_item_package.get_service_item_package, [{ id: this.state.bed_info['service_item_package_id'] }], (data: any) => {
                        if (data.result.length > 0) {
                            data!.result![0]['bed_id'] = this.state.bed_info.id;
                            this.setState({
                                base_data: data!.result![0],
                                pro_id: data!.result![0]['id'],
                            });
                            request_func(this, AppServiceUtility.services_project_service.get_all_item_from_product, [{ 'id': data!.result![0]['id'] }], (data: any) => {
                                this.setState({
                                    all_item_list: data.result,
                                });
                            });
                        }
                    });
                });
            }
        });
    }
    handleSubmitService = (e: any) => {
        e.preventDefault();
        const { form } = this.props;
        form!.validateFields((err: Error, values: ServiceItemPackageFormValues) => {
            // console.log(values);
            let serviceValue = {};
            let service_item: any = [];
            let details: any = [];
            this.state.all_item_list!.map((item) => {
                service_item.push({ service_options: [] });
                let detail = {
                    product_id: item.product_id,
                    product_name: item.product_name,
                    valuation_formula: item.valuation_formula,
                    service_option: [],
                    contract_terms: item.contract_terms,
                    count: 1,
                };
                details.push(detail);
            });
            for (let key in values) {
                if (key.indexOf('___') !== -1) {
                    // console.log('keys', key);
                    let keys = key.split('___');
                    if (keys[0] === 'service_item' || keys[0] === 'service_type') {
                        service_item[parseInt(keys[1], undefined)][keys[0]] = values[key];
                    } else {
                        details.map((item: any) => {
                            // console.log('aaa', item, item.product_id, keys[0]);
                            if (item.product_id === keys[0]) {
                                let option_values = values[key].split('___');
                                let option = {
                                    name: keys[1],
                                    option_value: option_values[0],
                                    option_type: keys[2] || '',
                                };
                                item['service_option'].push(option);
                            }
                        });
                    }

                } else {
                    serviceValue[key] = values[key];
                }
            }
            const service_info: object = {
                service_item: service_item,
                service_provider_id: this.state.base_data ? this.state.base_data['org_id'] : '', // 获取服务提供者
                bed_id: this.state.base_data ? this.state.base_data['bed_id'] : '', // 床位id
                details: details,
            };
            request_func(this, AppServiceUtility.check_in_service['add_check_in'], [this.state.user_info, this.state.evaluation_info, service_info], (data: any) => {
                this.setState({
                    create_date: data.create_date
                });
                this.next();
            });
        });
    }
    /*获取input框里面的值 */
    projectChange = (e: any, option_content: any) => {
        let project = this.state.project;
        let i = 0;
        let projectList = [];
        for (i = 0; i < project.length; i++) {
            projectList.push(project[i]['name']);
        }
        // // console.log(projectList);
        if (projectList.includes(option_content)) {
            project.map((item: any, idx: any) => {
                if (item["name"] === option_content) {
                    item["value"] = e.target.value;
                }
            });
        } else {
            let list = {};
            list["name"] = option_content;
            list["value"] = e.target.value;
            project.push(list);
            this.setState({
                project
            });
        }
        // // console.log(this.state.project);
    }
    /*获取单选按钮的值 */
    radioChange = (e: any, option_content: any) => {
        let radioValue = this.state.radioValue;
        let i = 0;
        let radioValueList = [];
        for (i = 0; i < radioValue.length; i++) {
            radioValueList.push(radioValue[i]['name']);
        }
        // // console.log(radioValueList);
        if (radioValueList.includes(option_content)) {
            radioValue.map((item: any, idx: any) => {
                if (item["name"] === option_content) {
                    item["value"] = e.target.value;
                }
            });
        } else {
            let list = {};
            list["name"] = option_content;
            list["value"] = e.target.value;
            radioValue.push(list);
            this.setState({
                radioValue
            });
        }
    }
    textAreaChange = (e: any, option_content: any) => {
        let textAreaValue = this.state.textAreaValue;
        let i = 0;
        let textAreaValueList = [];
        for (i = 0; i < textAreaValue.length; i++) {
            textAreaValueList.push(textAreaValue[i]['name']);
        }
        if (textAreaValueList.includes(option_content)) {
            textAreaValue.map((item: any, idx: any) => {
                if (item["name"] === option_content) {
                    item["value"] = e.target.value;
                }
            });
        } else {
            let list = {};
            list["name"] = option_content;
            list["value"] = e.target.value;
            textAreaValue.push(list);
            this.setState({
                textAreaValue
            });
        }
    }
    CheckChange = (value: any) => {
        this.setState({
            checkValue: value
        });
    }
    assSubmit = (value: any) => {
        console.info(value, '数据');
    }
    render = () => {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const edit_props = {
            row_btn_props: {
                style: {
                    justifyContent: " center"
                }
            },
            id: this.props.match!.params.key,
        };
        // 性别
        const sex_list = ['男', '女'];
        let sex_option: any[] = [];
        sex_list!.map((item: any) => {
            sex_option.push(<Option key={item}>{item}</Option>);
        });
        const relationship = ['父子', '母子', '父女', '母女', '其他'];
        let relationship_option: any[] = [];
        relationship!.map((item: any) => {
            relationship_option.push(<Option key={item}>{item}</Option>);
        });
        // 长者信息
        const elder_info = {
            submit_btn_propps: {
                text: '下一步',
                icon: 'plus',
                cb: (err: any, user_info: any) => {
                    // 缓存长者信息
                    if (!err) {
                        let current = this.state.current! + 1;
                        let formValue = this.state.form_value_list;
                        formValue!['basic_information'] = { ...user_info };
                        formValue!['basic_information']['personnel_type'] = '1';
                        formValue!['basic_information']['personnel_info'] = user_info;
                        this.setState({
                            current,
                            form_value_list: formValue,
                        });
                        // console.log('Received values of form: ', user_info);
                    }
                    this.setState({ user_info });
                    this.next();
                }
            },
            form_items_props: [
                {
                    title: "长者联系方式",
                    need_card: true,
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "家属姓名1",
                                        decorator_id: "family_name1",
                                        layout: layout,
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入家属姓名1" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入家属姓名1"
                                        }
                                    }, {
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "与长者关系",
                                        decorator_id: "relationship1",
                                        layout: layout,
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请选择与长者关系" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请选择与长者关系",
                                            childrens: relationship_option,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "联系方式",
                                        decorator_id: "tel1",
                                        layout: layout,
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入联系方式" }],
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入联系方式"
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "家属姓名2",
                                        decorator_id: "family_name2",
                                        layout: layout,
                                    }, {
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "与长者关系",
                                        decorator_id: "relationship2",
                                        layout: layout,
                                        // field_decorator_option: {
                                        //     rules: [{ required: true, message: "请选择与长者关系" }],
                                        // } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请选择与长者关系",
                                            childrens: relationship_option,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "联系方式",
                                        decorator_id: "tel2",
                                        layout: layout
                                    },
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "家属姓名3",
                                        decorator_id: "family_name3",
                                        layout: layout,
                                        option: {
                                        }
                                    }, {
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "与长者关系",
                                        decorator_id: "relationship3",
                                        layout: layout,
                                        // field_decorator_option: {
                                        //     rules: [{ required: true, message: "请选择与长者关系" }],
                                        // } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请选择与长者关系",
                                            childrens: relationship_option,
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "联系方式",
                                        decorator_id: "tel3",
                                        layout: layout
                                    },
                                ]
                            }
                        },
                    ]
                }
            ],
            service_option: {
                service_object: AppServiceUtility.check_in_service,
                operation_option: {
                    query: {
                        func_name: "get_check_in_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
        };
        Object.assign(elder_info, edit_props);
        // 评估

        const { getFieldDecorator } = this.props.form!;

        // 如果服务项目不为空则生成
        const formItemLayout_service = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        // 床位配置
        const columns_data_source = [{
            title: '床位编号',
            dataIndex: 'bed_code',
            key: 'bed_code',
        }, {
            title: '名称',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '所属房间',
            dataIndex: 'room_name',
            key: 'room_name',
        },
        {
            title: '房间床位数',
            dataIndex: 'bed_num',
            key: 'bed_num',
        },
        {
            title: '已住人数',
            dataIndex: 'user_num',
            key: 'user_num',
        }];
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "床位名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: columns_data_source,
            service_name: AppServiceUtility.hotel_zone_type_service,
            service_func: 'get_bed_list_no_user',
            title: '床位查询',
            select_option: {
                placeholder: "请选择床位",
            },
            service_option: [{
                state: true
            }],
        };
        let item_list;
        if (this.state.all_item_list && this.state.all_item_list!.length > 0) {
            // console.log('====================', this.state.all_item_list);
            item_list = this.state.all_item_list!.map((value, index) => {
                console.info('fjdssds>>>>>>>>>>>>>>.', index, value);
                return (
                    <Card key={index}>
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Form.Item label='服务产品'>
                                    {getFieldDecorator('service_item___' + index, {
                                        initialValue: value.name ? value.name : '',
                                        rules: [{
                                            message: '请选择服务产品'
                                        }],
                                    })(
                                        <Select showSearch={true} key={index} onChange={this.itemOnChange} disabled={true}>
                                            {this.state.item_list!.map((key) => {
                                                return <Select.Option key={index} value={key['id']}>{key['name']}</Select.Option>;
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                                {
                                    // let vf = value.valuation_formula ? value.valuation_formula : (value.valuation_formula ? value.valuation_formula : value.valuation_formula);
                                    value.valuation_formula ? (
                                        <Form.Item label='计价方式'>
                                            {getFieldDecorator('valuation_formula___' + '0', {
                                                initialValue: value.valuation_formula ? value.valuation_formula[0].name : "",
                                                rules: [{
                                                    required: false,
                                                    message: '请选择计价方式'
                                                }],
                                                option: {
                                                    disabled: true,
                                                    placeholder: '请选择计价方式'
                                                }
                                            })(
                                                <Select disabled={true}>
                                                    <Select.Option key={0} value={value.valuation_formula}>{value.valuation_formula.name}</Select.Option>
                                                </Select>
                                            )}
                                        </Form.Item>
                                    ) : ''
                                }
                                {
                                    value.type_name ? (
                                        <Form.Item label='服务类型'>
                                            {getFieldDecorator('service_type___' + 0, {
                                                initialValue: value.item_type ? value.item_type : "",
                                                rules: [{
                                                    required: false,
                                                }],
                                                option: {
                                                    disabled: true
                                                }
                                            })(
                                                <Select disabled={true}>
                                                    <Select.Option key={value.item_type} >{value.type_name}</Select.Option>
                                                </Select>
                                            )}
                                        </Form.Item>
                                    ) : ''
                                }
                                {(() => {
                                    if (!value.service_option) {
                                        return null;
                                    }
                                    return value.service_option.map((service_option: any, indexs: any) => {
                                        if (service_option.option_type === 'service_after') {
                                            return (
                                                <Row style={{ display: 'none' }}>
                                                    <Form.Item label={service_option.name} key={indexs}>
                                                        {getFieldDecorator(value.product_id + '___' + service_option.name + '___' + service_option.option_type, {
                                                            initialValue: service_option.option_value || (service_option.default_value || ''),
                                                            rules: [{
                                                                required: false,
                                                            }],
                                                        })(
                                                            <Input type="hidden" />
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            );
                                        }
                                        if (service_option.option_value_type === '1') {
                                            return (
                                                <Form.Item label={service_option.name} key={indexs}>
                                                    {getFieldDecorator(value.product_id + '___' + service_option.name + '___' + service_option.option_type, {
                                                        initialValue: service_option.option_value || (service_option.default_value || ''),
                                                        rules: [{
                                                            required: false,
                                                        }],
                                                    })(
                                                        <Radio.Group>
                                                            {
                                                                service_option.option_content!.map((item: any, content: any) => {
                                                                    return <Radio key={content} value={item.option_value}>{item.option_content}</Radio>;
                                                                })
                                                            }
                                                        </Radio.Group>
                                                    )}
                                                </Form.Item>
                                            );
                                        } else if (service_option.option_value_type === '2') {
                                            return (
                                                <Form.Item label={service_option.name} key={indexs}>
                                                    {getFieldDecorator(value.product_id + '___' + service_option.name + '___' + service_option.option_type, {
                                                        initialValue: service_option.option_value || (service_option.default_value || ''),
                                                        rules: [{
                                                            required: false,
                                                        }],
                                                    })(
                                                        <Checkbox.Group>
                                                            {
                                                                service_option.option_content!.map((item: any, idx: any) => {
                                                                    return <Checkbox key={idx} value={item.option_value}>{item.option_content}</Checkbox>;
                                                                })
                                                            }
                                                        </Checkbox.Group>
                                                    )}
                                                </Form.Item>
                                            );
                                        } else if (service_option.option_value_type === '3') {
                                            return (
                                                <Form.Item label={service_option.name} key={indexs}>
                                                    {getFieldDecorator(value.product_id + '___' + service_option.name + '___' + service_option.option_type, {
                                                        initialValue: service_option.option_value || (service_option.default_value || ''),
                                                        rules: [{
                                                            required: false,
                                                        }],
                                                    })(
                                                        <Input />
                                                    )}
                                                </Form.Item>
                                            );
                                        } else {
                                            return null;
                                        }
                                    });
                                })()}
                                {
                                    // value.service_option ? value.service_option.map((service_options: any, indexs: any) => {
                                    //     return service_options.option_type  ? (
                                    //         <Form.Item label={service_options.name} key={indexs}>
                                    //             {getFieldDecorator(value.product_id + '___' + indexs, {
                                    //                 initialValue: service_options.value ? service_options.value : '',
                                    //                 rules: [{
                                    //                     required: false,
                                    //                 }],
                                    //             })(
                                    //                 <Radio.Group>
                                    //                     {
                                    //                         service_options.option_content ? service_options.option_content!.map((item: any, content: any) => {
                                    //                             return <Radio key={content} value={service_options.name + '___' + item.option_value + '___' + service_options.option_type}>{item.option_content}</Radio>;
                                    //                         }) : <Radio key={0} value={service_options.name + '___' + service_options.default_value + '___' + service_options.option_type}>{service_options.default_value}</Radio>
                                    //                     }
                                    //                 </Radio.Group>
                                    //             )}
                                    //         </Form.Item>
                                    //     ) : '';
                                    // }) : ''
                                }
                            </Col>
                        </Row>
                    </Card >
                );
            });
        }
        // 步骤组件配置
        const { current } = this.state;
        const steps = [
            {
                title: '基本资料',
                content: (
                    <Form>
                        <UserEntry base_info={this.state.form_value_list!['basic_information']} is_detailed_info={false} base_info_submit_btn_propps={elder_info.submit_btn_propps} />
                    </Form>
                )
            },
            {
                title: '评估记录',
                content: (
                    <Card>
                        <PreliminaryAssessmentForm formSubmit={this.handleSubmit} lastStep={this.prev} isNursingLevel={true} />
                    </Card>
                )
            },
            {
                title: '服务产品',
                content: (
                    <MainContent>
                        <Form {...formItemLayout_service} onSubmit={this.handleSubmitService}>
                            <MainCard title='服务产品'>
                                <Row type="flex" justify="center">
                                    <Col span={12}>
                                        <Form.Item label='床位'>
                                            {getFieldDecorator('service_item_package_id', {
                                                initialValue: this.state.base_data!['service_item_package_id'] ? this.state.base_data!['service_item_package_id'] : [],
                                                rules: [{
                                                    required: false,
                                                    message: '请选择床位'
                                                }],
                                            })(
                                                <ModalSearch onChange={this.bedOnChange} modal_search_items_props={modal_search_items_props} />
                                            )}
                                        </Form.Item>

                                        <Form.Item label='服务产品名称'>
                                            {getFieldDecorator('name', {
                                                initialValue: this.state.base_data ? this.state.base_data['name'] : '',
                                            })(
                                                <Input disabled={true} />
                                            )}
                                        </Form.Item>
                                        <Form.Item label='是否启用'>
                                            {getFieldDecorator('state', {
                                                initialValue: this.state.base_data ? this.state.base_data['state'] : [],
                                            })(
                                                <Radio.Group disabled={true}>
                                                    <Radio value={1}>启用</Radio>
                                                    <Radio value={0}>停用</Radio>
                                                </Radio.Group>
                                            )}
                                        </Form.Item>
                                        <Form.Item label='所属组织机构'>
                                            {getFieldDecorator('org_id', {
                                                initialValue: this.state.base_data ? this.state.base_data['organizational_name'] : '',
                                            })(
                                                <Input disabled={true} />
                                            )}
                                        </Form.Item>
                                        <Form.Item label='服务包图片（大小小于2M，格式支持jpg/jpeg/png）'>
                                            {getFieldDecorator('picture_collection', {
                                                initialValue: this.state.base_data!['picture_collection'] ? this.state.base_data!['picture_collection'] : '',
                                            })(
                                                <FileUploadBtn list_type={"picture-card"} contents={"plus"} action={remote.upload_url} beforeUpload={beforeUpload} upload_amount={-1} />
                                            )}
                                        </Form.Item>
                                        <Form.Item label='附加合同条款'>
                                            {getFieldDecorator('additonal_contract_terms', {
                                                initialValue: this.state.base_data ? this.state.base_data['additonal_contract_terms'] : '',
                                            })(
                                                <TextArea rows={4} disabled={true} />
                                            )}
                                        </Form.Item>
                                        <Form.Item label='服务产品介绍'>
                                            {getFieldDecorator('service_product_introduce', {
                                                initialValue: this.state.base_data ? this.state.base_data['service_product_introduce'] : '',
                                            })(
                                                <TextArea rows={4} disabled={true} />
                                            )}
                                        </Form.Item>
                                        <Form.Item label='是否服务项目'>
                                            {getFieldDecorator('is_service_item', {
                                                initialValue: this.state.base_data!['is_service_item'] ? this.state.base_data!['is_service_item'] : '',
                                            })(
                                                <Select showSearch={true} disabled={true}>
                                                    <Option value="true">是</Option>
                                                    <Option value="false">否</Option>
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </MainCard>
                            <MainCard title='服务项目清单'>
                                {item_list}
                            </MainCard>
                            <Row type="flex" justify="center">
                                <Button type='ghost' name='返回' htmlType='button' onClick={this.prev}>上一步</Button>
                                <Button htmlType='submit' type='primary'>完成</Button>
                            </Row>
                        </Form>
                    </MainContent>
                ),
            },
            {
                title: '办理成功',
                content: (
                    <Row>
                        <Row type='flex' justify='center'>
                            <Col span={8}>
                                <Row type='flex' justify='center'>
                                    <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
                                </Row>
                                <Row type='flex' justify='center'>
                                    登记成功
                            </Row>
                                <Row type='flex' justify='center'>
                                    长者姓名：{this.state.user_info ? this.state.user_info['name'] : ''}
                                </Row>
                                <Row type='flex' justify='center'>
                                    身份证号码：{this.state.user_info ? this.state.user_info['id_card'] : ''}
                                </Row>
                                <Row type='flex' justify='center'>
                                    床位：{this.state.bed_info ? this.state.bed_info.name : ''}
                                </Row>
                                <Row type='flex' justify='center'>
                                    登记时间：{this.state.create_date}
                                </Row>
                            </Col>
                        </Row>
                        <MainContent>
                            <Row type='flex' justify='center'>
                                <Button type='ghost' name='返回' htmlType='button' onClick={this.goBack}>返回</Button>
                            </Row>
                        </MainContent>
                    </Row>
                ),
            },
        ];
        return (
            <Form>
                <MainContent>
                    <MainCard title="入住登记">
                        <div>
                            <Steps current={current}>
                                {steps.map(item => (
                                    <Step key={item.title} title={item.title} />
                                ))}
                            </Steps>
                            <div className="steps-content">
                                {steps[current].content}
                            </div>
                        </div>
                    </MainCard>
                </MainContent>
            </Form>

        );
    }
}

/**
 * 控件：入住登记新建视图
 * @description 入住登记新建视图
 * @author
 */
@addon('CheckInNewView', '入住登记新建', '入住登记新建视图')
@reactControl(Form.create<any>()(CheckInNewView), true)
export class CheckInNewViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    constructor() {
        super();
    }

}