
import { message, Modal, Row } from "antd";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { ModalSearch } from "src/business/components/buss-components/modal-search";
import SignFrameLayout, { InputType, InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
import { request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

/** 状态：长者入住登记视图 */
export interface CheckInListViewState extends ReactViewState {
    /** 数据总条数 */
    total_data?: number;
    /** 滚动条 */
    loading?: boolean;
    /** 查询条件 */
    condition?: object;
    /** 显示弹框 */
    visible?: boolean;
    /** 所选长者 */
    user_id?: string;
    /** 接口名 */
    request_url?: string;
}
/** 组件：长者入住登记视图 */
export class CheckInListView extends ReactView<CheckInListViewControl, CheckInListViewState> {
    private columns_data_source = [{
        title: '证件号码',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '年龄',
        dataIndex: 'user.personnel_info.age',
        key: 'user.personnel_info.age',
    }, {
        title: '性别',
        dataIndex: 'user.personnel_info.sex',
        key: 'user.personnel_info.sex',
    }, {
        title: '床位名称',
        dataIndex: 'bed_name',
        key: 'bed_name',
    },
    // {
    // title: '状态',
    // dataIndex: 'bed_number',
    // key: 'bed_number',
    // },
    {
        title: '入住时间',
        dataIndex: 'create_date',
        key: 'create_date',
    },];
    constructor(props: CheckInListViewControl) {
        super(props);
        this.state = {
            loading: false,
            total_data: 0,
            condition: {},
            visible: false
        };
    }

    /** 新增按钮 */
    addArchives = () => {
        this.props.history!.push(ROUTE_PATH.checkInNew);
    }
    /** 退住 */
    retreat = () => {
        this.setState({
            visible: true
        });
    }
    /** 调用退住方法 */
    check_out = () => {
        // 退住接口
        request_func(this, AppServiceUtility.accommodation_process_service.check_out_process, [this.state.user_id], (data: any) => {
            if (data === 'Success') {
                message.info('退住成功！');
            } else {
                message.info(data);
            }
            this.props.history!.push(ROUTE_PATH.checkIn);
        });
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.checkInDetail + '/' + contents.user_id);
        }
    }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
        });
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        let modal_search_items_props = {
            // type: 'input',
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.check_in_service,
            service_func: 'get_check_in_list',
            title: '长者查询',
            id_field: 'user_id',
            name_field: 'name',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let nursing = {
            loading: this.state.loading,
            type_show: false,
            edit_form_items_props: [
                // {
                //     type: InputType.input,
                //     label: "长者编号",
                //     decorator_id: "user_number"
                // }, 
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name"
                }, {
                    type: InputType.input,
                    label: "床位名称",
                    decorator_id: "bed_name",
                    option: {
                        placeholder: '请输入房间名称',
                    },

                },
                {
                    type: InputType.date,
                    label: "入住时间",
                    decorator_id: "create_date",
                    option: {
                        placeholder: '请选择入住时间',
                    },
                },
                {
                    type: InputType.date,
                    label: "退住时间",
                    decorator_id: "retreat_date",
                    option: {
                        placeholder: '请选择退住时间',
                    },
                },
            ],
            btn_props: [{
                label: '新增入住',
                btn_method: this.addArchives,
                icon: 'plus'
            },
            // {
            // label: '导出',
            // btn_method: () => {
            //     request_func(this, AppServiceUtility.check_in_service.export_excel, []);
            // },
            // icon: 'download',
            // },
            {
                label: '办理退住',
                btn_method: this.retreat,
                icon: 'logout',
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.check_in_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                // delete: {
                //     service_func: 'delete_check_in'
                // }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let nursing_list = Object.assign(nursing, table_param);
        return (
            <Row>
                <SignFrameLayout {...nursing_list} />
                <Modal title="选择退住长者" visible={this.state.visible} onOk={this.check_out} onCancel={() => this.setState({ visible: false })} width={'800px'}>
                    <ModalSearch modal_search_items_props={modal_search_items_props} onChange={(value: any) => this.setState({ user_id: value })} />
                </Modal>
            </Row>
        );
    }
}

/**
 * 控件：长者入住登记视图控制器
 * @description 长者入住登记视图
 * @author
 */
@addon('CheckInListView', '长者入住登记列表视图', '长者入住登记列表视图')
@reactControl(CheckInListView, true)
export class CheckInListViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}