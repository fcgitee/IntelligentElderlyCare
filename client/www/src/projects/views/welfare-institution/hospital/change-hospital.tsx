import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, Permission } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { AppServiceUtility } from "src/projects/app/appService";
import { isPermission } from "src/projects/app/permission";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from "../../../app/util-tool";
import { request_func } from "src/business/util_tool";
import { Select } from "antd";
import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";

const Option = Select.Option;

/**
 * 组件：医疗欠费状态
 */
export interface ChangeHospitalViewState extends ReactViewState {

    base_data: any;
    // 长者
    user_list: any;
}

/**
 * 组件：医疗欠费视图
 */
export class ChangeHospitalView extends ReactView<ChangeHospitalViewControl, ChangeHospitalViewState> {
    private columns_data_source = [{
        title: '编号',
        dataIndex: 'code',
        key: 'code',
    }, {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '身份证号',
        dataIndex: 'id_card',
        key: 'id_card',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '出生年月',
        dataIndex: 'birth_date',
        key: 'birth_date',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            base_data: {},
            user_list: []
        };
    }
    componentDidMount() {
        // 长者
        request_func(this, AppServiceUtility.personnel_service['get_personnel_list'], [{}], (data: any) => {
            let user_list: any[] = [];
            const data_list: any[] = data.result;
            data_list!.map((item, idx) => {
                user_list.push(<Option key={item['id']}>{item['name']}</Option>);
            });
            this.setState({
                user_list
            });
        });
    }
    returnBtn = () => {
        this.props.history!.push(ROUTE_PATH.hydropower);
    }
    render() {
        const redeirect = isPermission(this.props.permission!);
        if (redeirect) {
            return redeirect;
        }
        const base_data = this.state.base_data;
        // const user_list = this.state.user_list;
        let modal_search_items_props = {
            edit_form_items_props: [
                {
                    type: ListInputType.input,
                    label: "长者名称",
                    decorator_id: "name"
                },
            ],
            columns_data_source: this.columns_data_source,
            service_name: AppServiceUtility.personnel_service,
            service_func: 'get_personnel_list',
            title: '长者查询',
            select_option: {
                placeholder: "请选择长者",
            }
        };
        let edit_props = {
            form_items_props: [
                {
                    title: "医疗欠费",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.modal_search,
                            label: "长者姓名",
                            decorator_id: "elder",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择长者" }],
                                initialValue: base_data ? base_data.name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择长者",
                                modal_search_items_props: modal_search_items_props

                            }
                        }, {
                            type: InputType.antd_input,
                            label: "已报销",
                            decorator_id: "reimburse",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入用电量" }],
                                initialValue: base_data ? base_data.reimburse : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入已报销金额"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "优惠减免",
                            decorator_id: "discount_reduction",
                            field_decorator_option: {
                                // rules: [{ required: true, message: "请输入用水量" }],
                                initialValue: base_data ? base_data.discount_reduction : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入优惠减免金额"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "预收诊金",
                            decorator_id: "advance_receipt",
                            field_decorator_option: {
                                initialValue: base_data ? base_data.advance_receipt : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入预收诊金金额"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "合计",
                            decorator_id: "total",
                            field_decorator_option: {
                                initialValue: base_data ? base_data.advance_receipt : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入合计金额"
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "欠费总额",
                            decorator_id: "arrears",
                            field_decorator_option: {
                                initialValue: base_data ? base_data.arrears : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入欠费总额"
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                initialValue: base_data ? base_data.remark : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        },
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.hospitalArrears);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.hospital_manage,
                operation_option: {
                    save: {
                        func_name: "update_hospital"
                    },
                    query: {
                        func_name: "get_hospital_list",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.hospitalArrears); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 控件：医疗欠费编辑控件
 * @description 医疗欠费编辑控件
 * @author
 */
@addon('ChangeHospitalView', '医疗欠费编辑控件', '医疗欠费编辑控件')
@reactControl(ChangeHospitalView, true)
export class ChangeHospitalViewControl extends ReactViewControl {
    /** 视图权限 */
    public permission?: Permission;

}