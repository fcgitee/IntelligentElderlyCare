import { Row } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";
// const Option = Select.Option;
/**
 * 组件：医疗欠费状态
 */
export interface hospitalArrearsViewState extends ReactViewState {
}

/**
 * 组件：医疗欠费
 */
export class hospitalArrearsView extends ReactView<hospitalArrearsViewControl, hospitalArrearsViewState> {

    private columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '已报销',
        dataIndex: 'reimburse',
        key: 'water_number',
    }, {
        title: '优惠减免',
        dataIndex: 'discount_reduction',
        key: 'discount_reduction',
    }, {
        title: '预收诊金',
        dataIndex: 'advance_receipt',
        key: 'advance_receipt',
    }, {
        title: '合计',
        dataIndex: 'total',
        key: 'discount_reduction',
    }, {
        title: '欠费总额',
        dataIndex: 'arrears',
        key: 'arrears',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];

    constructor(props: any) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
    }

    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.hospitalArrearsEdit);
    }

    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.hospitalArrearsEdit + '/' + contents.id);
        }
    }

    render() {

        let json_info = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "长者姓名",
                    decorator_id: "name",
                    option: {
                        placeholder: '请输入长者姓名'
                    }
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            }, {
                label: '导入',
                btn_method: () => { },
                icon: 'plus'
            }],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.hospital_manage,
            service_option: {
                select: {
                    service_func: 'get_hospital_list',
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'delete_hospital'
                }
            },
        };

        let info_list = Object.assign(json_info, table_param);
        return (
            <Row>
                <SignFrameLayout {...info_list} />
            </Row>
        );
    }
}

/**
 * 控件：医疗欠费
 * 描述
 */
@addon('hospitalArrearsView', '医疗欠费', '描述')
@reactControl(hospitalArrearsView, true)
export class hospitalArrearsViewControl extends ReactViewControl {
}