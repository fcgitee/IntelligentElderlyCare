import { addon, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
// 房间档案列表配置
import { IRoomService } from "src/projects/models/room";
import { table_param } from '../../../app/util-tool';
/**
 * 组件：名称状态
 */
export interface RoomArchivesViewState extends ReactViewState {
}

/**
 * 组件：名称
 * 描述
 */
export class RoomArchivesView extends ReactView<RoomArchivesViewControl, RoomArchivesViewState> {
    private columns_data_source = [{
        title: '楼宇',
        dataIndex: 'building',
        key: 'building',
    }, {
        title: '房号',
        dataIndex: 'room_number',
        key: 'room_number'
    },
    {

        title: '居住人数',
        dataIndex: 'living_population',
        key: 'living_population'
    },
    {
        title: '房间类型',
        dataIndex: 'room_type',
        key: 'room_type'
    },
    {
        title: '费用',
        dataIndex: 'fee',
        key: 'fee',
    }];
    /** 房间服务 */
    // private roomService = getObject(this.props.roomService_Fac!);
    constructor(props: RoomArchivesViewControl) {
        super(props);
    }
    addRoom = () => {
        this.props.history!.push('/room-edit');
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            this.props.history!.push('/room-edit/' + contents.id);
        }
    }
    render() {
        // 房间档案列表配置
        let room_list_info = {
            // 查询条件
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "楼宇",
                    decorator_id: "building",
                    option: {
                        placeholder: "请输入楼宇名称"
                    }
                },
                {
                    type: InputType.input,
                    label: "房间号",
                    decorator_id: "room_number",
                    option: {
                        placeholder: "请输入房间号"
                    }
                },
            ],
            btn_props: [{
                label: '新增房间',
                btn_method: this.addRoom,
                icon: 'plus'
            }],
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.room_service,
            service_option: {
                select: {
                    service_func: 'query',
                    service_condition: ['IEC_Room_Archives'] as any[]
                },
                delete: {},
            }
        };
        let room_list = Object.assign(room_list_info, table_param);
        return (
            (
                <SignFrameLayout {...room_list} />
            )
        );
    }
}

/**
 * 控件：名称控制器
 * 描述
 */
@addon('RoomArchivesView', '房间档案', '管理房间')
@reactControl(RoomArchivesView, true)
export class RoomArchivesViewControl extends ReactViewControl {
    public roomService_Fac: Ref<IRoomService>;

}