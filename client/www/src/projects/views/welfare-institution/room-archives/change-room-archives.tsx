import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { IRoomService, Room } from "src/projects/models/room";
import { ROUTE_PATH } from "src/projects/router";
import { edit_props_info } from '../../../app/util-tool';

/**
 * 组件：首页视图控件状态
 */
export interface ChangeRoomArchivesViewState extends ReactViewState {
    // 房间详情
    room_data?: any;
    // 是否新增
    isadd?: any;
}

/**
 * 组件：首页视图控件
 */
export class ChangeRoomArchivesView extends ReactView<ChangeRoomArchivesViewControl, ChangeRoomArchivesViewState> {
    private roomService = getObject(this.props.roomService_Fac!);
    constructor(props: ChangeRoomArchivesViewControl) {
        super(props);
        this.state = {
            isadd: false,
            room_data: {}
        };
    }
    componentDidMount() {
        const id = this.props.match!.params.key ? this.props.match!.params.key : false;
        this.setState({ isadd: id ? id : false });
        id ? request(this, this.roomService!.query!('IEC_Room_Archives', { 'id': id }))
            .then((data: any) => {
                this.setState({ room_data: data.total ? data.result[0] : [] });
            }) : '';

    }
    /** 确定回调 */
    handleSubmit = (err: Error, values: Room) => {
        console.info(values, '编辑', this.state.isadd);
        values['id'] = this.state.isadd ? this.state.isadd : null;
        request(this, this.roomService!.insert!('IEC_Room_Archives', values)).then((data: any) => {
            alert(data.msg);
            this.props.history!.push(ROUTE_PATH.roomArchives);
        });
    }
    render() {
        const room_data = this.state.room_data;
        const edit_props = {
            form_items_props: [
                {
                    title: "房间信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "楼宇",
                            decorator_id: "building",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入楼宇名称" }],
                                initialValue: room_data.building ? room_data.building : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入楼宇名称"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "房号",
                            decorator_id: "room_number",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入房号" }],
                                initialValue: room_data.room_number ? room_data.room_number : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入房号"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "费用",
                            decorator_id: "fee",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入费用" }],
                                initialValue: room_data.fee ? room_data.fee : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入费用"
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "入住人数",
                            decorator_id: "living_population",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入入住人数" }],
                                initialValue: room_data.living_population ? room_data.living_population : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入入住人数"
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "房间类型",
                            decorator_id: "room_type",
                            field_decorator_option: {
                                rules: [{ required: true, message: "选择房间类型" }],
                                initialValue: room_data.room_type ? room_data.room_type : '1',
                            } as GetFieldDecoratorOptions,
                            option: {
                                options: [{
                                    label: '普通',
                                    value: '1',
                                }, {
                                    label: '豪华',
                                    value: '2',
                                }]
                            }

                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入备注" }],
                                initialValue: room_data.remark ? room_data.remark : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                rows: 4
                            }
                        },
                    ]
                }
            ],
            form_item_layout: edit_props_info.form_item_layout,  // 表单项大小配置
            form_props: edit_props_info.form_props,
            row_btn_props: edit_props_info.row_btn_props,
            // 返回按钮
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        this.props.history!.push('/room-archives');
                    }
                }
            ],
            // 保存按钮
            submit_btn_propps: {
                text: "保存",
                cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.room_service,
                operation_option: {
                    save: {
                        func_name: "insert"
                    },
                    query: {
                        func_name: "query",
                        arguments: ["command_id", { id: this.props.match!.params.key }, 1, 1]
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.roomArchives); },
            id: this.state.isadd,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            (
                <FormCreator {...edit_props_list} />
            )
        );
    }
}

/**
 * 组件：首页视图控件
 * 控制首页视图控件
 */
@addon('HomeView', '首页视图控件', '控制首页视图控件')
@reactControl(ChangeRoomArchivesView, true)
export class ChangeRoomArchivesViewControl extends ReactViewControl {
    public roomService_Fac: Ref<IRoomService>;
}