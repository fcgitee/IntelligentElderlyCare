import { Col, message, Modal, Row, Form, Spin } from "antd";
import { addon } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { request, RoomStatus, request_func } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { DraggableOlderElements, DroppableBedElements, RoomStatusWrapElements } from "src/projects/components/room-status-style";
import { Bed, HotelZone } from "src/projects/models/hotel_zone";
import { BedStatuss } from "./bedStatus";
import { RoomChangeView } from "../room-change/change-room";

const { confirm } = Modal;

/**
 * 组件：房态图视图状态
 */
export interface RoomStatusViewState extends ReactViewState {
    roomStatusElement: RoomStatus[];
    bedInfo?: Bed;
    upperElement?: HotelZone[];
    /** 调房弹框状态 */
    roomstatus?: boolean;
    /**
     * TODO: 人员没有接口对应，所以先返回any
     */
    residentInfo?: any;
    /**
     * 颜色池
     */
    colorPool?: string[];
    /** 显示弹框_退住 */
    visible_retreat?: boolean;
    /** 用户id */
    user_id?: string;
    /** 调床位，当前所在床位id */
    bed_id?: string;
    /** 调床位，调整后的床位id */
    destination_bed_id?: string;
    select?: any;
    num?: any;
    /** 旧床位名称 */
    old_bed_name?: string;
    // 是否加载
    isLoading?: any;
    organization_list?: any;
    selected_organization_id?: any;
}

/**
 * 组件：房态图视图
 * 描述
 */
export class RoomStatusView extends ReactView<RoomStatusViewControl, RoomStatusViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            isLoading: true,
            roomStatusElement: [],
            roomstatus: false,
            bedInfo: undefined,
            num: 0,
            upperElement: [],
            organization_list: [],
            user_id: '',
            bed_id: '',
            select: '',
            destination_bed_id: '',
            colorPool: ["rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)"],
            visible_retreat: false,
            old_bed_name: '',
            selected_organization_id: '',
        };
    }

    componentDidMount() {
        // this.getRoomStatus({});

        request(this, AppServiceUtility.hotel_zone_service!.get_organization_list!({}))
            .then((datas: any) => {
                this.setState(
                    {
                        organization_list: datas
                    },
                    () => {
                        if (datas.length === 1) {
                            this.getRoomStatus({});
                        } else {
                            this.setState({
                                isLoading: false,
                            });
                        }
                    }
                );
            });
    }

    /**
     * 通过接口获取房态图信息
     */
    getRoomStatus(condition?: {}, select?: any) {
        let { organization_list, selected_organization_id } = this.state;
        if (organization_list && organization_list.length && organization_list.length > 1) {
            if (!selected_organization_id) {
                message.info('请先选择要查看的机构！');
                return;
            } else {
                // 携带选中的
                condition!['organization_id'] = selected_organization_id;
            }
        }
        request(this, AppServiceUtility.hotel_zone_service!.get_room_status_pure_list!(condition))
            .then((datas: any) => {
                this.setState({
                    isLoading: false
                });
                // console.info('房态图信息', datas);
                let roomStatusInstance = new RoomStatus();
                // 排序，按照顶级元素数量排序，顶级元素排在前头
                (datas.result as RoomStatus[]).sort((a, b) =>
                    a.upper_element.length - b.upper_element.length
                );
                if (select) {
                    datas.result.map((item: any, index: any) => {
                        item.bed.map((items: any, indexs: any) => {
                            if (JSON.stringify(items.user) === '{}') {
                                if (items.id) {
                                    return;
                                }
                                datas.result[index].bed = [];
                            }
                        });
                    });
                    // console.info('处理好得房态图信息', datas);
                }
                setTimeout(
                    () => {
                        // console.info('开始setstate', roomStatusInstance.buildOrAddElements!(datas.result));
                        this.setState({
                            roomStatusElement: roomStatusInstance.buildOrAddElements!(datas.result)!
                        });
                    },
                    0
                );
            });
    }

    /**
     * 获取床位位置
     */
    getBedPosition(id: string) {
        request(this, AppServiceUtility.hotel_zone_type_service!.get_bed_position!({ id: id }))
            .then((datas: any) => {
                this.setState({
                    upperElement: datas.result[0].upper_element
                });
            });
    }

    /**
     * 获取长者信息
     * @param id 床位id
     */
    getResidentInfo(id: string) {
        request(this, AppServiceUtility.hotel_zone_type_service!.get_residents_of_bed!({ id: id }))
            .then((datas: any) => {
                // console.warn(datas.result[0].user[0]);
                this.setState({
                    residentInfo: datas.result[0].user[0]
                });
            });
    }

    /**
     * 获取床位详细信息
     */
    getBedInfo(id: string) {
        request(this, AppServiceUtility.hotel_zone_type_service!.get_bed_list!({ id: id }))
            .then((datas: any) => {
                this.setState({
                    bedInfo: datas.result[0]
                });
            });
    }

    /**
     * 根据长者id获取护理级别
     */
    get_nursing_level(id: string) {
        request(this, AppServiceUtility.hotel_zone_service!.get_nursing_level_Byid!({ id: id }))
            .then((datas: any) => {
                // // console.log('护理级别', datas);
                return datas;
            });
    }

    /**
     * 床位信息、床位位置、床位相关长者信息获取
     */
    getAllInfo(id: string) {
        this.getBedInfo(id);
        this.getBedPosition(id);
        this.getResidentInfo(id);
    }

    /**
     * 获取床位视图渲染元素
     */
    getBedEle(bed: Bed[], user: any[]) {
        // 数据库出来的user字段与bed字段的顺序不是一一对应的，只能这样寻找床位对应的长者
        function getResidentsOfBed(Bed: Bed) {
            // if (user.length === 1) {
            //     let min = level_info[0];
            //     level_info.map((item:any) => {

            //     }); 
            // }
            if (user.length > 0 && Bed.residents_id) {
                for (let iUser = 0; iUser < user.length; iUser++) {
                    const eUser = user[iUser];
                    if (eUser.id !== undefined) {
                        if (Bed.residents_id === eUser.id) {
                            return eUser;
                        }
                    }
                }
            }
            return undefined;
        }

        return bed.length > 0 ? bed.map((e, i) => {
            const userOfBed = getResidentsOfBed(e);
            // // console.log('长者信息？？？？？？？？', userOfBed);
            let backgroundColor = '';
            if (userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' && userOfBed.nursing_level !== undefined && userOfBed.nursing_level === '自理') {
                backgroundColor = 'zili';
            } else if (userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' && userOfBed.nursing_level !== undefined && userOfBed.nursing_level === '介助1') {
                backgroundColor = 'jiezhu1';
            } else if (userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' && userOfBed.nursing_level !== undefined && userOfBed.nursing_level === '介助2') {
                backgroundColor = 'jiezhu2';
            } else if (userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' && userOfBed.nursing_level !== undefined && userOfBed.nursing_level === '介护1') {
                backgroundColor = 'jiehu1';
            } else if (userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' && userOfBed.nursing_level !== undefined && userOfBed.nursing_level === '介护2') {
                backgroundColor = 'jiehu2';
            }
            return (
                // 可拖拽元素
                <div key={i} onClick={() => this.getAllInfo(e.id ? e.id : '')} className={userOfBed && JSON.stringify(userOfBed) !== '{}' ? "bed" : 'none_user_bed'}>
                    {e.name ? e.name : ''}
                    {
                        // 因为长者数组长度一定不会超过床位长度，所以可以这么判断是否需要增加长者元素
                        userOfBed && userOfBed !== undefined && JSON.stringify(userOfBed) !== '{}' ?
                            <DraggableOlderElements
                                residentsId={userOfBed && userOfBed.id ? userOfBed.id : ''}
                                residentsName={userOfBed && userOfBed.name ? userOfBed.name : ''}
                                residentsNursingLevel={backgroundColor}
                                residentsSex={userOfBed && userOfBed.sex ? userOfBed.sex : ''}
                                index={i}
                            />
                            :
                            undefined}
                </div>
            );
        })
            :
            undefined;
    }

    /**
     * 拖拽结束回调函数
     */
    onDragEnd = (result: DropResult) => {
        // console.log('onDragEnd');
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        request_func(this, AppServiceUtility.hotel_zone_type_service.get_bed_list, [{ id: result.source!.droppableId }], (data: any) => {
            if (data.result.length) {
                this.setState({ old_bed_name: data.result[0].name });
            }
        });
        const that = this;
        // // console.log(that);
        function showConfirm() {
            confirm({
                title: '警告',
                content: '是否换床位？',
                // TODO: 确定取消文本可以用区域取代
                okText: "确定",
                cancelText: "取消",
                onOk() {
                    that.setState({
                        roomstatus: true,
                        user_id: result.draggableId,
                        bed_id: result.source!.droppableId,
                        destination_bed_id: result.destination!.droppableId,
                    });
                    // AppServiceUtility.accommodation_process_service!.change_room_process!({ "user_id": result.draggableId, "new_bed_id": result.destination!.droppableId })!.then((data: any) => {
                    //     if (data === "Success") {
                    //         message.info("床位调换成功");
                    //         that.getRoomStatus();
                    //     }
                    // });
                },
                onCancel() {

                },
            });
        }

        if (result.source.droppableId === result.destination.droppableId) {
            // 相同床位就不做处理
            return;
        } else {
            showConfirm();
        }
    }

    /**
     * 递归生成房态图节点
     * @param level 房态图
     */
    traversalRoomStatusAndGetHtml(roomStatusElement: RoomStatus[], level: number): any {
        // // console.log('处理好得数据', roomStatusElement);
        let currentEle = roomStatusElement;
        const levelNow = level;
        // 遍历当前节点
        return currentEle.length > 0 ? currentEle.map((roomStatus: any, i: any) => {
            // // console.log('roomStatus>>', roomStatus);
            let childEle;
            let bedEle;

            if (roomStatus.down_element && roomStatus.down_element.length > 0) {
                // 当前节点有下级元素，则遍历子节点
                childEle = this.traversalRoomStatusAndGetHtml(roomStatus.down_element, levelNow + 1);

            }
            // // console.log('哈哈哈哈', childEle);

            // 无下级元素，目前来看，则一定有床位元素
            if (roomStatus.bed && roomStatus.bed.length > 0) {
                // 床位排序
                roomStatus.bed.map((item: any, index: any) => {
                    if (!item.bed_code) {
                        roomStatus.bed.splice(index, 1);
                    }
                });
                (roomStatus.bed).sort(function (a: any, b: any) {
                    return a.bed_code.localeCompare(b.bed_code);
                }
                );
                // // console.log('xixixixixi', roomStatus);
                // console.info('床位：', roomStatus.bed, roomStatus.user);
                bedEle = this.getBedEle(roomStatus.bed, roomStatus.user!);
                // // console.log('bedEle0', bedEle);
            }
            // 返回当前节点生成的元素
            return (
                <RoomStatusWrapElements key={i} roomStatus={roomStatus} colorPool={this.state.colorPool!} level={levelNow} >
                    {
                        childEle && childEle !== undefined && childEle.length > 0 ?
                            childEle.map((e: any, i: number) =>
                                (
                                    <Col key={i}>
                                        {e}
                                    </Col>
                                )
                            )
                            :
                            bedEle && bedEle !== undefined && bedEle.length > 0 ?
                                <Row type={"flex"}>
                                    {
                                        bedEle.map((e: any, i: number) =>
                                            (
                                                <DroppableBedElements
                                                    key={i}
                                                    bedId={roomStatus.bed && roomStatus.bed.length > 0 && roomStatus.bed![i].id ? roomStatus.bed![i].id : ''}
                                                    bedJSXElement={e}
                                                />
                                            )
                                        )
                                    }
                                </Row>
                                :
                                undefined
                    }
                </RoomStatusWrapElements>
            );
        })
            :
            undefined;
    }

    /** 退住 */
    retreat = () => {
        this.setState({
            visible_retreat: true
        });
    }
    /** 调用退住方法 */
    check_out = () => {
        // 退住接口
        request_func(this, AppServiceUtility.accommodation_process_service.check_out_process, [this.state.bedInfo!['residents_id']], (data: any) => {
            if (data === 'Success') {
                message.info('退住成功！');
            } else {
                message.info(data);
            }
            this.setState({
                visible_retreat: false
            });
            this.getRoomStatus({});
        });
    }

    handleclose() {
        this.setState({
            roomstatus: false
        });
    }
    select = (value?: any) => {
        this.getRoomStatus({});
        this.setState({
            select: value
        });
    }
    organization_change = (e: any) => {
        this.setState(
            {
                selected_organization_id: e,
                // 切换机构的时候把床位信息和长者信息去掉
                bedInfo: undefined,
                residentInfo: undefined,
            },
            () => {
                this.getRoomStatus({});
            }
        );
    }
    render() {
        let { roomStatusElement, bedInfo, upperElement, residentInfo } = this.state;
        const bedStatusButton = [
            {
                name: '全部',
                color: 'white',
                click_back: () => { this.getRoomStatus({}); },
            },
            {
                name: '空床',
                color: 'white',
                click_back: () => { this.getRoomStatus({ is_none_user: 'one' }, true); },
            },
            {
                name: '单人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 1 }, true); },
            },
            {
                name: '双人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 2 }, true); },
            },
            {
                name: '三人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 3 }, true); },
            },
            {
                name: '四人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 4 }, true); },
            },
            {
                name: '五人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 5 }, true); },
            },
            {
                name: '六人房',
                color: 'white',
                click_back: () => { this.getRoomStatus({ room_type: 6 }, true); },
            },
        ];
        const peopleStatusButton = [
            {
                name: '全部',
                backgroundColor: 'white',
                color: 'black',
                click_back: () => { this.getRoomStatus({}); },
            },
            {
                name: '自理',
                backgroundColor: 'rgb(254, 221, 179)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ nursing_level: '自理' }, true); },
            },
            {
                name: '介助1',
                backgroundColor: 'rgb(237, 183, 114)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ nursing_level: '介助1' }, true); },
            },
            {
                name: '介助2',
                backgroundColor: 'rgb(255, 151, 19)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ nursing_level: '介助2' }, true); },
            },
            {
                name: '介护1',
                backgroundColor: 'rgb(172, 100, 8)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ nursing_level: '介护1' }, true); },
            },
            {
                name: '介护2',
                backgroundColor: 'rgb(87, 51, 5)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ nursing_level: '介护2' }, true); },
            },
            {
                name: '男',
                backgroundColor: 'rgb(64, 159, 255)',
                color: 'black',
                click_back: () => { this.getRoomStatus({ sex: '男' }, true); },
            },
            {
                name: '女',
                backgroundColor: 'rgb(64, 159, 255)',
                color: 'white',
                click_back: () => { this.getRoomStatus({ sex: '女' }, true); },
            },
        ];
        // // console.log(roomStatusElement);
        return (
            (
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Row type={"flex"} className="room-status-display">
                        <Col span={20}>
                            {this.traversalRoomStatusAndGetHtml(roomStatusElement, 0)}
                        </Col>
                        <Col span={4}>
                            <BedStatuss
                                bed_status_arr={bedStatusButton}
                                people_status_arr={peopleStatusButton}
                                bed_status_info={bedInfo!}
                                bed_position_info={upperElement!}
                                resident_info={residentInfo}
                                organization_list={this.state.organization_list}
                                organization_change={this.organization_change}
                                retreat={this.retreat}
                            />
                            <Modal
                                title="床位调整"
                                visible={this.state.roomstatus}
                                onCancel={this.handleclose = this.handleclose.bind(this)}
                                footer={false}
                                width={1000}
                                destroyOnClose={true}
                            >
                                <Row>
                                    {/* <Col span={12}>
                                        <h5>旧房间信息</h5>
                                        <RoomChangeView key={'old'} user_id={this.state.user_id} bed_id={this.state.bed_id} form={this.props.form} />
                                    </Col> */}
                                    <Col span={24}>
                                        <h5>新房间信息</h5>
                                        <RoomChangeView
                                            key={'new'}
                                            user_id={this.state.user_id}
                                            bed_id={this.state.destination_bed_id}
                                            old_bed_id={this.state.bed_id}
                                            old_bed_name={this.state.old_bed_name}
                                            form={this.props.form}
                                            back_fuc={() => { this.setState({ roomstatus: false }); this.getRoomStatus({}); }}
                                        />
                                    </Col>
                                </Row>
                            </Modal>
                        </Col>
                    </Row>
                    <Modal title="退住操作" visible={this.state.visible_retreat} onOk={this.check_out} okText="确定" onCancel={() => this.setState({ visible_retreat: false })} cancelText="取消" destroyOnClose={true} width={'400px'}>
                        <p>是否要退住该长者:</p>
                        <p>长者名称：{this.state.bedInfo ? this.state.bedInfo!['residents'] : ''}</p>
                        <p>房间：{this.state.bedInfo ? this.state.bedInfo!['area_name'] : ''}</p>
                        <p>床位：{this.state.bedInfo ? this.state.bedInfo!['name'] : ''}</p>
                    </Modal>
                    <Spin tip="加载中..." style={{ display: this.state.isLoading ? undefined : 'none' }} />
                </DragDropContext>
            )
        );
    }
}

/**
 * 控件：房态图视图控制器
 * 描述
 */
@addon('RoomStatusView', '房态图', '房态图')
@reactControl(Form.create<any>()(RoomStatusView), true)
export class RoomStatusViewControl extends ReactViewControl {
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}