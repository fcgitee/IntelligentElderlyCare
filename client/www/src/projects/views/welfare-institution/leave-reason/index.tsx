import { Modal, Row } from "antd";
import { addon, getObject, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import SignFrameLayout, { InputType } from "src/business/components/buss-components/sign-frame-layout";
import { AppServiceUtility } from "src/projects/app/appService";
import { table_param } from "src/projects/app/util-tool";
// import { request } from "src/business/util_tool";
import { ILeaveRecordService } from "src/projects/models/leave-record";
import { ROUTE_PATH } from "src/projects/router";

/**
 * 组件：离开原因信息状态
 */
export interface LeaveReasonViewState extends ReactViewState {
    /** 选择行的Id集合 */
    select_ids?: string[];
    /** 离开原因项目集合 */
    leave_list?: any[];
    /** 弹出框是否显示 */
    modal_visible?: boolean;
    /** 离开原因总条数 */
    total?: number;
    /** 标题 */
    title?: string;
    /** 表格列信息 */
    columns_service_item?: any[];
    /** 接口名 */
    request_url?: string;
}

/**
 * 组件：离开原因信息
 * 描述
 */
export class LeaveReasonView extends ReactView<LeaveReasonViewControl, LeaveReasonViewState> {
    private columns_data_source = [{
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }];
    constructor(props: any) {
        super(props);
        this.state = {
            select_ids: [],
            modal_visible: false
        };
    }

    /** 住宿记录服务 */
    serviceService_Fac() {
        return getObject(this.props.serviceService_Fac!);
    }
    /** 新增按钮 */
    add = () => {
        this.props.history!.push(ROUTE_PATH.changeLeaveReason);
    }
    /** 自定义图标按钮回调事件 */
    onIconClick = (type: string, contents: any) => {
        if ('icon_edit' === type) {
            // console.log('自定义按钮edit返回值：', contents);
            this.props.history!.push(ROUTE_PATH.changeLeaveReason + '/' + contents.id);
        }
    }
    /** 模版选择框取消回调方法 */
    handleCancel = (e: any) => {
        this.setState({
            modal_visible: false,
        });
    }
    /** 行选择事件 */
    // on_row_selection = (selectedRows: any) => {
    //     let ids: string[] = [];
    //     selectedRows!.map((item: any, idx: number) => {
    //         ids.push(item['leave_reason_id']);
    //     });
    //     this.setState({
    //         select_ids: ids
    //     });
    // }
    componentWillMount() {
        let request_url = this.props.request_url;
        this.setState({
            request_url
            // condition: [{ personnel_category: personnelCategory.elder }, 1, 10] // 长者资料
        });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let leave_reason = {
            type_show: false,
            edit_form_items_props: [
                {
                    type: InputType.input,
                    label: "名称",
                    decorator_id: "name"
                },
            ],
            btn_props: [{
                label: '新增',
                btn_method: this.add,
                icon: 'plus'
            },],
            // on_row_selection: this.on_row_selection,
            columns_data_source: this.columns_data_source,
            on_icon_click: this.onIconClick,
            service_object: AppServiceUtility.leaveRecord_service,
            service_option: {
                select: {
                    service_func: this.state.request_url,
                    service_condition: [{}, 1, 10]
                },
                delete: {
                    service_func: 'del_leave_reason'
                }
            },
            permission_class: this.props.permission_class,
            get_permission_name: this.props.get_permission_name,
            select_permission: this.props.select_permission,
            edit_permission: this.props.edit_permission,
            add_permission: this.props.add_permission,
            delete_permission: this.props.delete_permission,
        };
        let leave_reason_list = Object.assign(leave_reason, table_param);
        return (
            <Row>
                <Modal
                    title={this.state.title}
                    visible={this.state.modal_visible}
                    onCancel={this.handleCancel}
                    width={900}
                >
                    <NTOperationTable
                        data_source={this.state.leave_list}
                        columns_data_source={this.state.columns_service_item}
                        table_size='middle'
                        showHeader={true}
                        bordered={true}
                        total={this.state.total}
                        default_page_size={10}
                        total_pages={Math.ceil((this.state.total ? this.state.total : 0) / 10)}
                        show_footer={true}
                    />
                </Modal>
                <SignFrameLayout {...leave_reason_list} />
            </Row>
        );
    }
}

/**
 * 控件：离开原因信息
 * 描述
 */
@addon('LeaveReasonView', '离开原因信息', '描述')
@reactControl(LeaveReasonView, true)
export class LeaveReasonViewControl extends ReactViewControl {
    /** 住宿记录接口服务 */
    public serviceService_Fac?: Ref<ILeaveRecordService>;
    /** 请求接口名 */
    public request_url?: string;
    /** 获取权限接口类 */
    public permission_class?: any;
    /** 获取权限接口方法名 */
    public get_permission_name?: string;
    /** 查询权限 */
    public select_permission?: string;
    /** 编辑权限 */
    public edit_permission?: string;
    /** 删除权限 */
    public delete_permission?: string;
    /** 新增权限 */
    public add_permission?: string;
}