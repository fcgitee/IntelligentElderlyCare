import { Select } from 'antd';
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon, getObject, IPermissionService, Permission, Ref } from "pao-aop";
import { reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { MainContent } from "src/business/components/style-components/main-content";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info } from "src/projects/app/util-tool";
import { ROUTE_PATH } from "src/projects/router";

let { Option } = Select;

/**
 * 组件：离开原因编辑页面状态
 */
export interface ChangeLeaveReasonViewState extends ReactViewState {
    /** 离开原因列表 */
    leave_reason_list?: [];
}
/**
 * 组件：离开原因编辑页面
 * 描述
 */
export class ChangeLeaveReasonView extends ReactView<ChangeLeaveReasonViewControl, ChangeLeaveReasonViewState> {
    constructor(props: any) {
        super(props);
        this.state = {
            leave_reason_list: []
        };
    }
    /** 权限服务 */
    permissionService?() {
        return getObject(this.props.permissionService_Fac!);
    }
    componentDidMount() {
        request(this, AppServiceUtility.personnel_service['get_organizational_list']!({}, 1, 999))
            .then((datas: any) => {
                // console.log('datas.result', datas.result);
                this.setState({
                    leave_reason_list: datas.result,
                });
            });
    }
    render() {
        // const redeirect = isPermission(this.props.permission!);
        // if (redeirect) {
        //     return redeirect;
        // }
        let leave_reason = this.state.leave_reason_list;
        let leave_reason_list: any[] = [];
        leave_reason!.map((item, idx) => {
            leave_reason_list.push(<Option key={item['id']}>{item['name']}</Option>);
        });
        let edit_props = {
            form_items_props: [
                {
                    title: "离开原因信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入离开原因" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入离开原因",
                                // childrens: leave_reason_list,
                            }
                        }, {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "remark",
                            field_decorator_option: {
                                rules: [{ message: "请输入备注" }],
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注"
                            }
                        }
                    ]
                }
            ],
            other_btn_propps: [
                {
                    text: "返回",
                    cb: () => {
                        // console.log("返回回调");
                        this.props.history!.push(ROUTE_PATH.leaveReason);
                    }
                }
            ],
            submit_btn_propps: {
                text: "保存",
                // cb: this.handleSubmit
            },
            service_option: {
                service_object: AppServiceUtility.leaveRecord_service,
                operation_option: {
                    query: {
                        func_name: "get_leave_reason_list_all",
                        arguments: [{ id: this.props.match!.params.key }, 1, 1]
                    },
                    save: {
                        func_name: "update_leave_reason"
                    }
                },
            },
            succ_func: () => { this.props.history!.push(ROUTE_PATH.leaveReason); },
            id: this.props.match!.params.key,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <MainContent>
                <FormCreator {...edit_props_list} />
            </MainContent>
        );
    }
}

/**
 * 控件：离开原因编辑页面
 * 描述
 */
@addon('ChangeLeaveReasonView', '离开原因编辑页面', '描述')
@reactControl(ChangeLeaveReasonView, true)
export class ChangeLeaveReasonViewControl extends ReactViewControl {
    /** 权限服务接口 */
    public permissionService_Fac?: Ref<IPermissionService>;
    /** 视图权限 */
    public permission?: Permission;
}