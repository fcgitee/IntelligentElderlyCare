import { ReactViewState, ReactView, reactControl, ReactViewControl } from "pao-aop-client";

import React from "react";
import './index.less';
import { addon } from "pao-aop";
import { Row, Col, Card } from "antd";
import { NTOperationTable } from "src/business/components/buss-components/operation-table";
import { HomeCard } from "src/projects/components/home-card";
// import { ROUTE_PATH } from "src/projects/router";
import { AppServiceUtility } from "src/projects/app/appService";
// import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 组件：首页视图控件状态
 */
export interface HomeViewState extends ReactViewState {
    /** 任务列表数据源 */
    task_data_source?: any;
    /** 入住信息列表数据源 */
    check_in_data_source?: any;
    /** 散点图数据源 */
    company_model_line_data?: any;
    /** 执行任务数量 */
    taskcount?: number;
    /** 服务订单数量 */
    service_order?: number;
    /** 入住人数数量 */
    checkin_count?: number;
    /** 床位总数 */
    bed_count?: number;
    /** 菜单 */
    menudata?: any;
    /** 任务列表是否显示 */
    is_task?: boolean;
    /** 长者入住 */
    is_older?: boolean;
    /** 待执行任务数 */
    istaskcount?: number;
    /** 预约人数 */
    reservationcount?: number;
}
/**
 * 组件：首页视图控件
 */
export class HomeView extends ReactView<HomeViewControl, HomeViewState> {
    // private columns_data_source = [{
    //     title: '长者姓名',
    //     dataIndex: 'name',
    //     key: 'name',
    // }, {
    //     title: '性别',
    //     dataIndex: 'sex',
    //     key: 'sex',
    // }, {
    //     title: '年龄',
    //     dataIndex: 'age',
    //     key: 'age',
    // }, {
    //     title: '登记时间',
    //     dataIndex: 'create_date',
    //     key: 'create_date',
    // }];
    private check_columns_data_source = [{
        title: '长者姓名',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    }, {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    }, {
        title: '床位',
        dataIndex: 'bed',
        key: 'bed',
    }, {
        title: '入住时间',
        dataIndex: 'date',
        key: 'date',
    }];
    private menudata = [
        // {
        //     url: ROUTE_PATH.reservationRegistration,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '预约登记',
        //     permission: '预约登记'
        // }, {
        //     url: ROUTE_PATH.elderInfo,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '长者资料',
        //     permission: '长者资料'
        // }, {
        //     url: ROUTE_PATH.wokerPersonnel,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '工作人员',
        //     permission: '工作人员管理'
        // }, {
        //     url: ROUTE_PATH.organization,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '组织机构',
        //     permission: '组织机构'
        // }, {
        //     url: ROUTE_PATH.administrationDivision,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '行政区划',
        //     permission: '行政区划管理'
        // }, {
        //     url: ROUTE_PATH.administrationDivision,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '监控管理',
        //     permission: '监控管理'
        // }, {
        //     url: ROUTE_PATH.administrationDivision,
        //     icon: <Icon type="copy" theme="filled" />,
        //     text: '社会群体',
        //     permission: '社会群体类型管理'
        // }, {
        //     url: ROUTE_PATH.checkIn,
        //     icon: <Icon type="home" theme="filled" />,
        //     text: '办理入住',
        //     permission: '办理入住'
        // }, {
        //     url: ROUTE_PATH.serviceProjectindex,
        //     icon: <Icon type="folder" theme="filled" />,
        //     text: '服务套餐',
        //     permission: '服务套餐'
        // }, {
        //     url: ROUTE_PATH.activityManage,
        //     icon: <Icon type="flag" theme="filled" />,
        //     text: '活动发布',
        //     permission: '活动发布'
        // }, {
        //     url: ROUTE_PATH.competenceAssessment,
        //     icon: <Icon type="edit" theme="filled" />,
        //     text: '能力评估',
        //     permission: '能力评估'
        // }, {
        //     url: ROUTE_PATH.newsList,
        //     icon: <Icon type="snippets" theme="filled" />,
        //     text: '新闻管理',
        //     permission: '新闻管理'
        // }, {
        //     url: ROUTE_PATH.announcementList,
        //     icon: <Icon type="carry-out" theme="filled" />,
        //     text: '公告管理',
        //     permission: '公告管理'
        // }, {
        //     url: ROUTE_PATH.roomStatus,
        //     icon: <Icon type="database" theme="filled" />,
        //     text: '房态图',
        //     permission: '房态图'
        // }, {
        //     url: ROUTE_PATH.leaveRecord,
        //     icon: <Icon type="book" theme="filled" />,
        //     text: '住宿记录',
        //     permission: '住宿记录'
        // }, {
        //     url: ROUTE_PATH.serviceProject,
        //     icon: <Icon type="appstore" theme="filled" />,
        //     text: '服务项目',
        //     permission: '服务项目'
        // }, {
        //     url: ROUTE_PATH.serviceItemPackage,
        //     icon: <Icon type="calendar" theme="filled" />,
        //     text: '服务包',
        //     permission: '服务包'
        // }, {
        //     url: ROUTE_PATH.allowanceManage,
        //     icon: <Icon type="credit-card" theme="filled" />,
        //     text: '补贴申请',
        //     permission: '补贴申请'
        // },
    ];
    constructor(props: any) {
        super(props);
        this.state = {
            menudata: [],
            is_older: false,
            is_task: false,
            task_data_source: [],
            company_model_line_data: [
                { longitude: '113.14958', latitude: '23.035191', value: 1 },
                { longitude: '113.14958', latitude: '23.045191', value: 1 },
                { longitude: '113.14958', latitude: '23.043191', value: 1 },
                { longitude: '113.14958', latitude: '23.040191', value: 1 },
            ],
            check_in_data_source: [],
            istaskcount: 0,
            taskcount: 0,
            reservationcount: 0,
            bed_count: 0
        };
    }
    hrefUrl = (url: string) => {
        this.props.history!.push(url);
    }
    analyzeIDCard = (IDCard: any) => {
        let userCard = IDCard;
        if (!userCard) {
            return;
        }
        let yearBirth = userCard.substring(6, 10);
        let monthBirth = userCard.substring(10, 12);
        let dayBirth = userCard.substring(12, 14);
        let myDate = new Date();
        let monthNow = myDate.getMonth() + 1;
        let dayNow = myDate.getDay();
        let age = myDate.getFullYear() - yearBirth;
        if (monthNow < monthBirth || (monthNow === monthBirth && dayNow < dayBirth)) {
            age--;
        }
        return age;
    }
    componentDidMount() {
        AppServiceUtility.login_service.get_function_list!()!
            .then(data => {
                let menu: any[] = [];
                let { is_older, is_task } = this.state;
                this.menudata.map((value: any) => {
                    data.map((functions: any) => {
                        if (value.permission + '查询' === functions.function + functions.permission) {
                            menu.push(value);
                        }
                        if ('任务列表查询' === functions.function + functions.permission) {
                            is_task = true;
                        }
                        if ('长者入住查询' === functions.function + functions.permission) {
                            is_older = true;
                        }
                    });
                });
                this.setState({
                    menudata: menu,
                    is_older,
                    is_task
                });
            });
        AppServiceUtility.service_operation_service.get_service_order_list!({}, 1, 10)!
            .then((res: any) => {
                this.setState({
                    service_order: res.total
                });
            });
        // AppServiceUtility.task_service.get_all_task_list_all!({}, 1, 10)!
        //     .then((res: any) => {
        //         if (res.result.length > 0) {
        //             let count = 0;
        //             let iscount = 0;
        //             res.result.forEach((item: any, index: number) => {
        //                 if (item.task_state === '待处理' || item.task_state === '审核通过' || item.task_state === '待接收' || item.task_state === '已拒绝') {
        //                     iscount = iscount + 1;
        //                 } else if (item.task_state === '进行中' || item.task_state === '已完成' || item.task_state === '已评价') {
        //                     count = count + 1;
        //                 }
        //                 this.setState({
        //                     istaskcount: iscount,
        //                     taskcount: count
        //                 });
        //             });
        //         }
        //     });
        // AppServiceUtility.person_org_manage_service.get_personnel_elder!({}, 1, 10)!
        //     .then((res: any) => {
        //         let data: any = [];
        //         res.result.forEach((item: any) => {
        //             let info: object = {};
        //             info['name'] = item.name ? item.name : '';
        //             info['sex'] = item.sex ? item.sex : item.personnel_info.sex ? item.personnel_info.sex : '';
        //             info['age'] = item.id_card ? this.analyzeIDCard(item.id_card) : '';
        //             info['create_date'] = item.create_date ? item.create_date : '';
        //             data.push(info);
        //         });
        //         this.setState({
        //             task_data_source: data
        //         });
        //     });
        AppServiceUtility.reservation_registration_service.get_reservation_registration_list_all!({}, 1, 10)!
            .then((res: any) => {
                this.setState({
                    reservationcount: res.total
                });
            });
        AppServiceUtility.check_in_service.get_check_in_list_all!({}, 1, 10)!
            .then((res: any) => {
                this.setState({
                    checkin_count: res.total
                });
            });
        AppServiceUtility.check_in_service.get_check_in_list!({}, 1, 10)!
            .then((res: any) => {
                // console.log(res);
                let data: any = [];
                if (res.result.length > 0) {
                    res.result.forEach((item: any, index: number) => {
                        data.push({
                            name: item.name,
                            sex: item.sex,
                            age: (item && item.user ? this.analyzeIDCard(item.user.id_card) : ''),
                            bed: item.bed_name,
                            date: item.create_date
                        });
                    });
                    this.setState({
                        check_in_data_source: data
                    });
                }
            });
        AppServiceUtility.hotel_zone_type_service.get_valid_bed_count!({})!
            .then((res: any) => {
                this.setState({
                    bed_count: res
                });
            });
    }
    render() {
        return (
            <Row style={{ padding: '20px' }}>
                <Row gutter={20}>
                    {
                        this.state.menudata!.map((item: any, index: number) => {
                            return (
                                <Col span={4} key={index} onClick={this.hrefUrl.bind(this, item.url)}>
                                    <HomeCard>
                                        {item.icon}
                                        <strong>{item.text}</strong>
                                    </HomeCard>
                                </Col>
                            );
                        })
                    }
                </Row>
                <Row className='home-center' gutter={20}>
                    {/* {this.state.is_task ? <Col span={10}>
                        <Row style={{ marginBottom: '20px', }}>
                            <Card>
                                <Row>
                                    <Col span={8}>
                                        <strong>
                                            <Row>服务订单</Row>
                                            <Row>+&nbsp;{this.state.service_order}单</Row>
                                        </strong>
                                    </Col>
                                    <Col span={8}>
                                        <strong>
                                            <Row>执行任务</Row>
                                            <Row>{this.state.taskcount}个</Row>
                                        </strong>
                                    </Col>
                                    <Col span={8}>
                                        <strong>
                                            <Row>待执行任务</Row>
                                            <Row>{this.state.istaskcount}个</Row>
                                        </strong>
                                    </Col>
                                </Row>
                            </Card>
                        </Row>
                        <Card title='任务列表' extra={<a href="#">查看更多</a>}>
                            <NTOperationTable
                                data_source={this.state.task_data_source}
                                columns_data_source={this.columns_data_source}
                                showHeader={true}
                                bordered={true}
                                total={4}
                                default_page_size={10}
                                total_pages={Math.ceil((4 ? 10 : 0) / 10)}
                                show_footer={false}
                                is_pagination={false}
                            />
                        </Card>
                    </Col> : ""} */}
                    {this.state.is_older ? <Col span={24}>
                        {/* 右下角 */}
                        <Row style={{ marginBottom: '20px', }}>
                            <Card>
                                <Row>
                                    <Col span={6}>
                                        <strong>
                                            <Row>服务订单</Row>
                                            <Row>+&nbsp;{this.state.service_order}单</Row>
                                        </strong>
                                    </Col>
                                    <Col span={6}>
                                        <strong>
                                            <Row>预约人数</Row>
                                            <Row>+&nbsp; {this.state.reservationcount}人</Row>
                                        </strong>
                                    </Col>
                                    <Col span={6}>
                                        <strong>
                                            <Row>入住人数</Row>
                                            <Row>{this.state.checkin_count}个</Row>
                                        </strong>
                                    </Col>
                                    <Col span={6}>
                                        <strong>
                                            <Row>床位总数</Row>
                                            <Row>{this.state.bed_count}个</Row>
                                        </strong>
                                    </Col>
                                </Row>
                            </Card>
                        </Row>
                        <Card title='长者入住' extra={<a href="/check-in-list">查看更多</a>}>
                            <NTOperationTable
                                data_source={this.state.check_in_data_source}
                                columns_data_source={this.check_columns_data_source}
                                showHeader={true}
                                bordered={true}
                                total={4}
                                default_page_size={10}
                                total_pages={Math.ceil((4 ? 10 : 0) / 10)}
                                show_footer={false}
                                is_pagination={false}
                            />
                        </Card>
                    </Col> : ''}
                </Row>
            </Row>

        );

    }
}

/**
 * 组件：首页视图控件
 * 控制首页视图控件
 */
@addon('HomeView', '首页视图控件', '控制首页视图控件')
@reactControl(HomeView, true)
export class HomeViewControl extends ReactViewControl {
    constructor() {
        super();
    }
}
