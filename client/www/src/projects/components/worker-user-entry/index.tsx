import { message, Select } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import moment from 'moment';
import { addon, isIDCardAvailable } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { IDCard, IDCardReader } from "src/business/device/id_card";
import { AppServiceUtility } from "src/projects/app/appService";
import { edit_props_info, getAge, getBirthday, getSex } from "src/projects/app/util-tool";
import { remote } from 'src/projects/remote';
let { Option } = Select;
// import { InputType as ListInputType } from "src/business/components/buss-components/sign-frame-layout";
// import { request } from "src/business/util_tool";
/**
 * 组件：工作人员信息输入组件状态
 */
export interface WorkerUserEntryState extends BaseReactElementState {
    /** 身份证 */
    id_card?: IDCard;
    /** 证件类型 */
    id_card_type?: string;
    /** 人员数据 */
    personel_data?: any;
    /** 角色列表 */
    role_list?: any;
    /** 选择角色 */
    select?: any;
    /** 行政区划列表 */
    administrative_division_list?: any;
    /** 组织机构 */
    org_list?: any;
    // 长者类型列表
    personnel_classification_list?: any;
    // 选中的组织机构
    selected_organization?: any;
}
/**
 * 组件：工作人员信息输入组件
 * 工作人员信息输入组件
 */
export class WorkerUserEntry extends BaseReactElement<WorkerUserEntryControl, WorkerUserEntryState> {
    private formCreater: any = null;
    constructor(props: WorkerUserEntryControl) {
        super(props);
        this.state = {
            id_card: {},
            personel_data: {
                id_card: '',
                sex: '',
                name: '',
                date_birth: '',
                age: '',
                telephone: '',
                remarks: '',
                family_name: [],
                guardian_name: '',
                guardian_id_card: '',
                guardian_date_birth: '',
                guardian_sex: '',
                guardian_age: '',
                guardian_dress: '',
                guardian_telephone: '',
                guardian_remarks: '',
                work_state: '',
                post: '',
            },
            role_list: [],
            administrative_division_list: [],
            org_list: [],
            personnel_classification_list: [],
            selected_organization: '',
        };
    }
    /** 处理身份证读取 */
    handleIDCardReader?() {
        // 调试环境下（npm start）驱动地址为/drive/ZKIDROnline.exe
        // 正式环境下（python）驱动地址为/download/build/www/drive/ZKIDROnline.exe
        const dev = process.env.NODE_ENV === 'development',
            drive_path = dev ? `/drive/ZKIDROnline.exe` : `/download/build/www/drive/ZKIDROnline.exe`;

        new IDCardReader(drive_path)
            .read_async!()!
            .then(id_card => {
                AppServiceUtility.person_org_manage_service.get_personnel_elder!({ id_card: id_card.id_number }, 1, 1)!
                    .then((data) => {
                        if (data.result!.length > 0) {
                            let personel_data = {};
                            let info = data.result![0];
                            personel_data['id_card'] = info['personnel_info'].id_card;
                            personel_data['sex'] = info['personnel_info'].sex;
                            personel_data['name'] = info['personnel_info'].name;
                            personel_data['date_birth'] = moment(getBirthday(info['personnel_info'].id_card));
                            personel_data['age'] = getAge(info['personnel_info'].id_card);
                            personel_data['telephone'] = info['personnel_info'].telephone;
                            personel_data['remarks'] = info['remarks'];
                            personel_data['post'] = info['post'];
                            personel_data['work_state'] = info['work_state'];

                            this.setState({
                                personel_data
                            });
                        } else {
                            let personel_data = {};
                            personel_data['id_card'] = id_card.id_number;
                            personel_data['sex'] = id_card.sex;
                            personel_data['date_birth'] = moment(getBirthday(id_card!.id_number!));
                            personel_data['age'] = getAge(id_card!.id_number!);
                            personel_data['name'] = id_card!.name;
                            this.setState({
                                personel_data,
                            });
                        }
                    })
                    .catch((err) => {
                        console.info(err);
                    });
                this.setState({ id_card });
            })
            .catch(error => {
                message.warning(error.message);
            });
    }
    guardianHandleIDCardReader?() {
        // 调试环境下（npm start）驱动地址为/drive/ZKIDROnline.exe
        // 正式环境下（python）驱动地址为/download/build/www/drive/ZKIDROnline.exe
        const dev = process.env.NODE_ENV === 'development',
            drive_path = dev ? `/drive/ZKIDROnline.exe` : `/download/build/www/drive/ZKIDROnline.exe`;

        new IDCardReader(drive_path)
            .read_async!()!
            .then(id_card => {
                AppServiceUtility.person_org_manage_service.get_personnel_elder!({ id_card: id_card.id_number }, 1, 1)!
                    .then((data) => {
                        if (data.result!.length > 0) {
                            let personel_data = {};
                            let info = data.result![0];
                            personel_data['guardian_id_card'] = info['personnel_info'].id_card;
                            personel_data['guardian_sex'] = info['personnel_info'].sex;
                            personel_data['guardian_name'] = info['personnel_info'].name;
                            personel_data['guardian_date_birth'] = moment(getBirthday(info['personnel_info'].id_card));
                            personel_data['guardian_age'] = getAge(info['personnel_info'].id_card);
                            personel_data['guardian_telephone'] = info['personnel_info'].telephone;
                            personel_data['guardian_remarks'] = info['remarks'];
                            personel_data['post'] = info['post'];
                            personel_data['work_state'] = info['work_state'];

                            this.setState({
                                personel_data
                            });
                        } else {
                            let personel_data = {};
                            personel_data['guardian_id_card'] = id_card.id_number;
                            personel_data['guardian_sex'] = id_card.sex;
                            personel_data['guardian_date_birth'] = moment(getBirthday(id_card!.id_number!));
                            personel_data['guardian_age'] = getAge(id_card!.id_number!);
                            personel_data['guardian_name'] = id_card!.name;
                            this.setState({
                                personel_data,
                            });
                        }
                    })
                    .catch((err) => {
                        console.info(err);
                    });
                this.setState({ id_card });
            })
            .catch(error => {
                message.warning(error.message);
            });
    }
    id_card_type_change = (value: any) => {
        this.setState({
            id_card_type: value,
        });
    }
    // 校验身份证号码
    checkIdCard = (rule: any, value: any, callback: any) => {
        let is_id_card = true;
        if (value === '') {
            callback('请输入证件号码');
        }
        // if (this.state.id_card_type) {
        //     if (this.state.id_card_type === '身份证') {
        //         is_id_card = isIDCardAvailable(value);
        //     }
        // } else {
        //     is_id_card = isIDCardAvailable(value);
        // }
        is_id_card ? callback() : callback('请输入正确的身份证号码');
    }
    id_card_change = (e: any) => {
        // console.info(e.target.value);
        if (e.target.value) {
            let birthday = getBirthday(e.target.value);
            let age = getAge(e.target.value);
            let sex = getSex(e.target.value);
            let base_date = this.state.personel_data;
            base_date['date_birth'] = birthday;
            base_date['age'] = age;
            base_date['sex'] = sex;
            base_date['guardian_date_birth'] = this.state.personel_data.guardian_date_birth ? this.state.personel_data.guardian_date_birth : null;
            base_date['guardian_age'] = this.state.personel_data.guardian_age ? this.state.personel_data.guardian_age : null;
            base_date['guardian_sex'] = this.state.personel_data.guardian_sex ? this.state.personel_data.guardian_sex : null;

            this.setState({
                personel_data: base_date
            });
        }
    }
    jianhuguardian_id_card_change = (e: any) => {
        if (isIDCardAvailable(e.target.value)) {
            let guardian_date_birth = getBirthday(e.target.value);
            let guardian_age = getAge(e.target.value);
            let guardian_sex = getSex(e.target.value);
            let base_date = this.state.personel_data;
            base_date['guardian_date_birth'] = guardian_date_birth;
            base_date['guardian_age'] = guardian_age;
            base_date['guardian_sex'] = guardian_sex;
            base_date['date_birth'] = this.state.personel_data.date_birth ? this.state.personel_data.date_birth : null;
            base_date['age'] = this.state.personel_data.age ? this.state.personel_data.age : null;
            base_date['sex'] = this.state.personel_data.sex ? this.state.personel_data.sex : null;

            this.setState({
                personel_data: base_date
            });
        }
    }
    componentWillReceiveProps(nextProps: any) {
        if (nextProps.id) {
            AppServiceUtility.person_org_manage_service[nextProps.select_face]!({ id: nextProps.id })!
                .then((data: any) => {
                    let obj = {};
                    obj = data.result![0];
                    Object.assign(obj, data.result![0].personnel_info);
                    // console.info(obj);
                    this.setState({
                        personel_data: obj,
                    });
                })
                .catch((err: any) => {
                    console.info(err);
                });
        }
    }
    componentDidMount() {
        AppServiceUtility.role_service.get_cur_person_role_pure_list!({ is_platform: true })!
            .then(data => {
                this.setState({
                    role_list: data.result,
                });
            })
            .catch(err => {
                console.info(err);
            });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        // 长者类型列表
        AppServiceUtility.person_org_manage_service.get_personnel_classification_list!({})!
            .then(data => {
                this.setState({
                    personnel_classification_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        if (this.props.id) {
            AppServiceUtility.person_org_manage_service.get_personnel_elder!({ id: this.props.id })!
                .then(data => {
                    let obj = {};
                    obj = data.result![0];
                    Object.assign(obj, data.result![0].personnel_info);
                    this.setState({
                        personel_data: obj
                    });
                })
                .catch(err => {
                    console.info(err);
                });
        }
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                this.setState({
                    org_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });

    }
    filter = (inputValue: any, path: any) => {
        return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    }
    onRef2 = (ref: any) => {
        this.formCreater = ref;
    }
    render() {
        // 通用信息配置
        const layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const edit_props = {
            row_btn_props: {
                style: {
                    justifyContent: "center"
                }
            },
            id: '',
        };
        let { base_info, is_detailed_info, } = this.props;
        let { personel_data } = this.state;

        // 初始化，抑制报错
        if (!base_info || !base_info.hasOwnProperty('personnel_info')) {
            base_info = {
                personnel_info: {}
            };
        }
        // 名字
        base_info['name'] = personel_data.name ? personel_data.name : (base_info['personnel_info'].name ? base_info['personnel_info'].name : "");
        // 身份证
        base_info['id_card'] = personel_data.id_card ? personel_data.id_card : (base_info['personnel_info'].id_card ? base_info['personnel_info'].id_card : "");
        // 身份证类型
        base_info['id_card_type'] = personel_data.id_card_type ? personel_data.id_card_type : (base_info['personnel_info'].id_card_type ? base_info['personnel_info'].id_card_type : "");
        // 生日
        base_info['date_birth'] = personel_data.date_birth ? moment(personel_data.date_birth) : (base_info['personnel_info'].date_birth ? base_info['personnel_info'].date_birth : '');
        // 性别
        base_info['sex'] = personel_data.sex ? personel_data.sex : (base_info['personnel_info'].sex ? base_info['personnel_info'].sex : "");
        // 年龄
        base_info['age'] = personel_data.age ? personel_data.age : (base_info['personnel_info'].age ? base_info['personnel_info'].age : (base_info['personnel_info'].id_card ? getAge(base_info['personnel_info'].id_card) : ""));
        // 联系方式
        base_info['telephone'] = personel_data.telephone ? personel_data.telephone : (base_info['personnel_info'].telephone ? base_info['personnel_info'].telephone : "");
        // 备注
        base_info['remarks'] = personel_data.remarks ? personel_data.remarks : (base_info['personnel_info'].remarks ? base_info['personnel_info'].remarks : "");
        // 监护人姓名
        base_info['guardian_name'] = personel_data.guardian_name ? personel_data.guardian_name : (base_info['personnel_info'].guardian_name ? base_info['personnel_info'].guardian_name : "");
        // 监护人证件号码
        base_info['guardian_id_card'] = personel_data.guardian_id_card ? personel_data.guardian_id_card : (base_info['personnel_info'].guardian_id_card ? base_info['personnel_info'].guardian_id_card : "");
        // 监护人出生日期
        base_info['guardian_date_birth'] = personel_data.guardian_date_birth ? moment(personel_data.guardian_date_birth) : (base_info['personnel_info'].guardian_date_birth ? moment(base_info['personnel_info'].guardian_date_birth) : '');
        // 监护人性别
        base_info['guardian_sex'] = personel_data.guardian_sex ? personel_data.guardian_sex : (base_info['personnel_info'].guardian_sex ? base_info['personnel_info'].guardian_sex : "");
        // 监护人年龄
        base_info['guardian_age'] = personel_data.guardian_age ? personel_data.guardian_age : (base_info['personnel_info'].guardian_age ? base_info['personnel_info'].guardian_age : "");
        // 监护人户籍地址
        base_info['guardian_dress'] = personel_data.guardian_dress ? personel_data.guardian_dress : (base_info['personnel_info'].guardian_dress ? base_info['personnel_info'].guardian_dress : "");
        // 监护人户联系方式
        base_info['guardian_telephone'] = personel_data.guardian_telephone ? personel_data.guardian_telephone : (base_info['personnel_info'].guardian_telephone ? base_info['personnel_info'].guardian_telephone : "");
        // 监护人备注
        base_info['guardian_remarks'] = personel_data.guardian_remarks ? personel_data.guardian_remarks : (base_info['personnel_info'].guardian_remarks ? base_info['personnel_info'].guardian_remarks : "");
        // 家庭联系方式
        base_info['family_name'] = personel_data.family_name ? personel_data.family_name : base_info['personnel_info'].family_name ? { initialValue: base_info['personnel_info'].family_name } : {};
        // 人员分类
        base_info['personnel_category'] = personel_data.personnel_info && personel_data.personnel_info.personnel_category ? personel_data.personnel_info.personnel_category : (base_info['personnel_info'].personnel_category ? base_info['personnel_info'].personnel_category : "");
        // 长者类型
        base_info['personnel_classification'] = personel_data.personnel_info && personel_data.personnel_info.personnel_classification ? personel_data.personnel_info.personnel_classification : (base_info['personnel_info'].personnel_classification ? base_info['personnel_info'].personnel_classification : "");
        base_info['id_card_type'] = personel_data.id_card_type ? personel_data.id_card_type : (base_info.id_card_type ? base_info.id_card_type : "");
        base_info['nation'] = personel_data.nation ? personel_data.nation : (base_info.nation ? base_info.nation : "");
        base_info['role_id'] = personel_data.personnel_info ? personel_data.personnel_info.role_id ? personel_data.personnel_info.role_id : (base_info.role_id ? base_info.role_id : "") : '';
        base_info['id_card_address'] = personel_data.id_card_address ? personel_data.id_card_address : (base_info.id_card_address ? base_info.id_card_address : "");
        base_info['card_number'] = personel_data.card_number ? personel_data.card_number : (base_info.card_number ? base_info.card_number : "");
        base_info['account_name'] = personel_data.login_info && personel_data.login_info.length > 0 && personel_data.login_info[0].login_check && personel_data.login_info[0].login_check.account_name ? personel_data.login_info[0].login_check.account_name : (base_info.account_name ? base_info.account_name : "");
        base_info['address'] = personel_data.address ? personel_data.address : (base_info.address ? base_info.address : "");
        base_info['native_place'] = personel_data.native_place ? personel_data.native_place : (base_info.native_place ? base_info.native_place : "");
        base_info['card_name'] = personel_data.card_name ? personel_data.card_name : (base_info.card_name ? base_info.card_name : "");
        base_info['admin_area_id'] = personel_data.admin_area_id ? personel_data.admin_area_id : (base_info.admin_area_id ? base_info.admin_area_id : "");
        base_info['organization_id'] = personel_data.organization_id ? personel_data.organization_id : (base_info.organization_id ? base_info.organization_id : "");
        base_info['picture'] = personel_data.picture ? personel_data.picture : (base_info.picture ? base_info.picture : []);
        base_info['die_state'] = personel_data.die_state ? personel_data.die_state : (base_info.die_state ? base_info.die_state : '');
        base_info['is_member'] = personel_data.is_member ? personel_data.is_member : (base_info.is_member ? base_info.is_member : '');
        base_info['marriage_state'] = personel_data.marriage_state ? personel_data.marriage_state : (base_info.marriage_state ? base_info.marriage_state : '');
        base_info['work_state'] = personel_data.work_state ? personel_data.work_state : (base_info.work_state ? base_info.work_state : '');
        base_info['post'] = personel_data.post ? personel_data.post : (base_info.post ? base_info.post : '');
        base_info['graduation_time'] = personel_data.graduation_time ? personel_data.graduation_time : (base_info.graduation_time ? base_info.graduation_time : '');
        base_info['graduated_school'] = personel_data.graduated_school ? personel_data.graduated_school : (base_info.graduated_school ? base_info.graduated_school : '');
        base_info['major'] = personel_data.major ? personel_data.major : (base_info.major ? base_info.major : '');
        base_info['professional_certificate'] = personel_data.professional_certificate ? personel_data.professional_certificate : (base_info.professional_certificate ? base_info.professional_certificate : '');
        base_info['diploma'] = personel_data.diploma ? personel_data.diploma : (base_info.diploma ? base_info.diploma : '');
        base_info['work_years'] = personel_data.personnel_info && personel_data.personnel_info.work_years ? personel_data.personnel_info.work_years : '';
        base_info['professional_qualification_name'] = personel_data.professional_qualification_name ? personel_data.professional_qualification_name : (base_info.professional_qualification_name ? base_info.professional_qualification_name : []);
        base_info['education'] = personel_data.personnel_info && personel_data.personnel_info.education ? personel_data.personnel_info.education : '';
        base_info['salary'] = personel_data.personnel_info && personel_data.personnel_info.salary ? personel_data.personnel_info.salary : '';
        base_info['person_introduce'] = personel_data.personnel_info && personel_data.personnel_info.person_introduce ? personel_data.personnel_info.person_introduce : '';
        // console.info(base_info['family_name']);
        // // console.log(base_info, 'base_info');
        // // console.log(this.props, 'props');
        let elder_info = {
            submit_btn_propps: this.props.base_info_submit_btn_propps,
            form_items_props: [
                {
                    title: "长者信息",
                    need_card: true,
                    side_props: {
                        col_span: 8,
                        childrens: {
                            input_props: [{
                                type: InputType.upload,
                                // col_span: 12,
                                label: "长者照片（大小小于2M，格式支持jpg/jpeg/png）",
                                decorator_id: "urls",
                                layout: layout,
                                field_decorator_option: {
                                    // rules: [{ validator: checkContact, type: 'mobile' }],
                                    // initialValue: base_info ? base_info.age : '',
                                } as GetFieldDecoratorOptions,
                                option: {
                                    action: remote.upload_url
                                }
                            }]
                        }
                    },
                    card_btn: [
                        {
                            text: "读取身份证",
                            cb: () => this.handleIDCardReader!()
                        }
                        // , {
                        //     text: "扫描身份证",
                        //     cb: () => {
                        //         // ？功能待定
                        //     }
                        // }
                    ],
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "长者姓名",
                                        decorator_id: "name",
                                        field_decorator_option: {
                                            rules: [{ required: true, message: "请输入长者姓名" }],
                                            initialValue: base_info.name,
                                        } as GetFieldDecoratorOptions,
                                        layout: layout,
                                        option: {
                                            placeholder: "请输入长者姓名",
                                            // modal_search_items_props: modal_search_items_props,
                                            // onChange: this.on_change_name
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "证件号码",
                                        decorator_id: "id_card",
                                        layout: layout,
                                        field_decorator_option: {
                                            // rules: [{ validator: this.checkIdCard }],
                                            rules: [{ required: true, message: "请输入证件号码" }],
                                            initialValue: base_info.id_card,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入证件号码",
                                            onChange: this.id_card_change
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.date,
                                    col_span: 12,
                                    label: "出生日期",
                                    decorator_id: "date_birth",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: true, message: "请选择出生日期" }],
                                        initialValue: base_info.date_birth,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请选择出生日期"
                                    }
                                }, {
                                    type: InputType.radioGroup,
                                    col_span: 12,
                                    label: "性别",
                                    decorator_id: "sex",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: true, message: "请选择性别" }],
                                        initialValue: base_info.sex,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请选择性别",
                                        options: [{
                                            label: '男',
                                            value: '男',
                                        }, {
                                            label: '女',
                                            value: '女',
                                        }]
                                    }
                                },

                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "年龄",
                                    decorator_id: "age",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: true, message: "请输入年龄" }],
                                        initialValue: base_info.age,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入年龄"
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "联系方式",
                                    decorator_id: "telephone",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: true, message: "请输入联系方式" }],
                                        initialValue: base_info.telephone,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入联系方式"
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "户籍地址",
                                    decorator_id: "dress",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入户籍地址" }],
                                        initialValue: base_info.dress,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入户籍地址"
                                    }
                                }
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.text_area,
                                    col_span: 12,
                                    label: "备注",
                                    decorator_id: "remarks",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入备注" }],
                                        initialValue: base_info.remarks,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入备注"
                                    }
                                }]
                            }
                        },
                    ]
                },
                {
                    title: "监护人信息",
                    need_card: true,
                    card_btn: [
                        {
                            text: "读取身份证",
                            cb: () => this.guardianHandleIDCardReader!()
                        }
                        // , {
                        //     text: "扫描身份证",
                        //     cb: () => {
                        //         // ？功能待定
                        //     }
                        // }
                    ],
                    input_props: [
                        {
                            one_row_inputs: {
                                inputs_props: [
                                    {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "监护人姓名",
                                        decorator_id: "guardian_name",
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "请输入监护人姓名" }],
                                            initialValue: base_info.guardian_name,
                                        } as GetFieldDecoratorOptions,
                                        layout: layout,
                                        option: {
                                            placeholder: "请输入监护人姓名",
                                            // modal_search_items_props: modal_search_items_props,
                                            // onChange: this.on_change_name
                                        }
                                    }, {
                                        type: InputType.antd_input,
                                        col_span: 12,
                                        label: "监护人证件号码",
                                        decorator_id: "guardian_id_card",
                                        layout: layout,
                                        field_decorator_option: {
                                            // rules: [{ validator: this.checkIdCard }],
                                            rules: [{ required: false, message: "请输入监护人证件号码" }],
                                            initialValue: base_info.guardian_id_card,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入监护人证件号码",
                                            onChange: this.jianhuguardian_id_card_change
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.date,
                                    col_span: 12,
                                    label: "监护人出生日期",
                                    decorator_id: "guardian_date_birth",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请选择监护人出生日期" }],
                                        initialValue: base_info.guardian_date_birth,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请选择监护人出生日期"
                                    }
                                }, {
                                    type: InputType.radioGroup,
                                    col_span: 12,
                                    label: "监护人性别",
                                    decorator_id: "guardian_sex",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请选择监护人性别" }],
                                        initialValue: base_info.guardian_sex,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请选择监护人性别",
                                        options: [{
                                            label: '男',
                                            value: '男',
                                        }, {
                                            label: '女',
                                            value: '女',
                                        }]
                                    }
                                }]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "监护人年龄",
                                    decorator_id: "guardian_age",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入监护人年龄" }],
                                        initialValue: base_info.guardian_age,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入监护人年龄"
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "监护人户籍地址",
                                    decorator_id: "guardian_dress",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入监护人户籍地址" }],
                                        initialValue: base_info.guardian_dress,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入监护人户籍地址"
                                    }
                                }, {
                                    type: InputType.antd_input,
                                    col_span: 12,
                                    label: "监护人联系方式",
                                    decorator_id: "guardian_telephone",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入监护人联系方式" }],
                                        initialValue: base_info.guardian_telephone,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入监护人联系方式"
                                    }
                                }
                                ]
                            }
                        },
                        {
                            one_row_inputs: {
                                inputs_props: [{
                                    type: InputType.text_area,
                                    col_span: 12,
                                    label: "备注",
                                    decorator_id: "guardian_remarks",
                                    layout: layout,
                                    field_decorator_option: {
                                        rules: [{ required: false, message: "请输入备注" }],
                                        initialValue: base_info.guardian_remarks,
                                    } as GetFieldDecoratorOptions,
                                    option: {
                                        placeholder: "请输入备注"
                                    }
                                }]
                            }
                        },
                    ]
                },
                {
                    title: "联系方式",
                    need_card: true,
                    input_props: [{
                        type: InputType.objectFieldList,
                        // col_span: 12,
                        label: "联系人",
                        decorator_id: "family_name",
                        layout: layout,
                        field_decorator_option: {
                            rules: [{ required: false }],
                            initialValue: base_info['family_name'].length > 0 ? base_info['family_name'] : ''
                        } as GetFieldDecoratorOptions,
                        option: {
                            befor_placeholder: "联系人",
                            after_placeholder: '手机号码',
                            third_placeholder: '与联系人的关系',
                            before_key: 'title',
                            after_key: 'contents',
                            third_key: 'relation'
                        }
                    }]
                }
            ],
            service_option: {
                service_object: AppServiceUtility.reservation_registration_service,
                operation_option: {},
            },
        };
        Object.assign(elder_info, edit_props);
        // // console.log('elder_info', elder_info);
        // // console.log('这里>>>>', base_info.personnel_info.personnel_classification);
        let ns = [{ "id": "01", "name": "汉族" }, { "id": "02", "name": "蒙古族" }, { "id": "03", "name": "回族" },
        { "id": "04", "name": "藏族" }, { "id": "05", "name": "维吾尔族" }, { "id": "06", "name": "苗族" },
        { "id": "07", "name": "彝族" }, { "id": "08", "name": "壮族" }, { "id": "09", "name": "布依族" },
        { "id": "10", "name": "朝鲜族" }, { "id": "11", "name": "满族" }, { "id": "12", "name": "侗族" },
        { "id": "13", "name": "瑶族" }, { "id": "14", "name": "白族" }, { "id": "15", "name": "土家族" },
        { "id": "16", "name": "哈尼族" }, { "id": "17", "name": "哈萨克族" }, { "id": "18", "name": "傣族" },
        { "id": "19", "name": "黎族" }, { "id": "20", "name": "傈僳族" }, { "id": "21", "name": "佤族" },
        { "id": "22", "name": "畲族" }, { "id": "23", "name": "高山族" }, { "id": "24", "name": "拉祜族" },
        { "id": "25", "name": "水族" }, { "id": "26", "name": "东乡族" }, { "id": "27", "name": "纳西族" },
        { "id": "28", "name": "景颇族" }, { "id": "29", "name": "柯尔克孜族" }, { "id": "30", "name": "土族" },
        { "id": "31", "name": "达斡尔族" }, { "id": "32", "name": "仫佬族" }, { "id": "33", "name": "羌族" },
        { "id": "34", "name": "布朗族" }, { "id": "35", "name": "撒拉族" }, { "id": "36", "name": "毛难族" },
        { "id": "37", "name": "仡佬族" }, { "id": "38", "name": "锡伯族" }, { "id": "39", "name": "阿昌族" },
        { "id": "40", "name": "普米族" }, { "id": "41", "name": "塔吉克族" }, { "id": "42", "name": "怒族" },
        { "id": "43", "name": "乌孜别克族" }, { "id": "44", "name": "俄罗斯族" }, { "id": "45", "name": "鄂温克族" },
        { "id": "46", "name": "崩龙族" }, { "id": "47", "name": "保安族" }, { "id": "48", "name": "裕固族" },
        { "id": "49", "name": "京族" }, { "id": "50", "name": "塔塔尔族" }, { "id": "51", "name": "独龙族" },
        { "id": "52", "name": "鄂伦春族" }, { "id": "53", "name": "赫哲族" }, { "id": "54", "name": "门巴族" },
        { "id": "55", "name": "珞巴族" }, { "id": "56", "name": "基诺族" }];
        // 详细信息
        let layout_personnel = {
            labelCol: {
                xs: { span: 24 },
                md: { span: 8 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                md: { span: 16 },
                sm: { span: 20 },
            },
        };
        let work = ['在职', '离职'];
        let work_dom: JSX.Element[] = work.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let post = ['院长', '管理人员', '专业技能人员', '医生', '护士', '护理员', '后勤人员', '书记', '分管民政人员', '社工', '义工'];
        let post_dom: JSX.Element[] = post.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let education = ['小学', '初中', '高中', '专科', '本科', '本科以上'];
        let education_dom: JSX.Element[] = education.map((value: any) => {
            return (
                <Option key={value}>{value}</Option>
            );
        });
        let org_role_list: any[] = [];
        if (this.state.selected_organization) {
            this.state.role_list.map((item: any) => {
                if (item['organization_id'] === this.state.selected_organization || item['is_platform']) {
                    org_role_list.push(item);
                }
            });
        } else {
            org_role_list = this.state.role_list;
        }
        console.log(this.state.selected_organization, org_role_list);
        let document_type: JSX.Element[] = [<Option key='身份证'>身份证</Option>, <Option key='军官证'>军官证</Option>, <Option key='港澳居民来往内地通行证'>港澳居民来往内地通行证</Option>, <Option key='其他'>其他</Option>],
            personnel_category_list: JSX.Element[] = [<Option key='平台管理员'>平台管理员</Option>, <Option key='长者'>长者</Option>, <Option key='工作人员'>工作人员</Option>, <Option key='志愿者'>志愿者</Option>, <Option key='义工'>义工</Option>, <Option key='护理员'>护理员</Option>],
            role: JSX.Element[] = org_role_list.map((value: any) => {
                return (
                    <Option key={value.id}>{value.name}</Option>
                );
            }),
            nation: JSX.Element[] = ns.map((value: any) => {
                return (
                    <Option key={value.name}>{value.name}</Option>
                );
            }),
            // id_card = this.state.id_card,
            edit_props_personnel = {
                form_items_props: [
                    {
                        title: "人员资料",
                        need_card: true,
                        input_props: [
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "人员分类",
                                            decorator_id: "personnel_category",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择人员分类" }],
                                                initialValue: this.props.personnel_category ? this.props.personnel_category : base_info.personnel_category ? base_info.personnel_category : ''
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: personnel_category_list,
                                                placeholder: "请选择人员分类",
                                                disabled: this.props.personnel_category ? true : false
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "所属角色",
                                            decorator_id: "role_id",
                                            field_decorator_option: {
                                                rules: [{ required: this.props.personnel_category === '长者' ? false : true, message: "请选择所属角色" }],
                                                initialValue: this.props.personnel_category === '长者' ? '8ed260f6-e355-11e9-875e-a0a4c57e9ebe' : base_info ? base_info.role_id : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: role,
                                                placeholder: "请选择所属角色",
                                                disabled: this.props.personnel_category === '长者' ? true : false
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "登录账号",
                                            decorator_id: "account_name",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请输入登录账号" }],
                                                initialValue: base_info ? base_info.account_name : '',

                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入登录账号"
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "证件类型",
                                            decorator_id: "id_card_type",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择证件类型" }],
                                                initialValue: base_info ? base_info.id_card_type : ''
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: document_type,
                                                placeholder: "请选择证件类型",
                                                onChange: this.id_card_type_change
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "证件号码",
                                            decorator_id: "id_card",
                                            field_decorator_option: {
                                                rules: [{ required: true, validator: this.checkIdCard }],
                                                initialValue: base_info ? base_info.id_card : ''
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入证件号码",
                                                onChange: this.id_card_change
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "姓名",
                                            decorator_id: "name",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请输入姓名" }],
                                                initialValue: base_info ? base_info.name : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入姓名",
                                                // modal_search_items_props: modal_search_items_props,
                                                // onChange: this.on_change_name
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.radioGroup,
                                            col_span: 8,
                                            label: "性别",
                                            decorator_id: "sex",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择性别" }],
                                                initialValue: base_info ? base_info.sex : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请选择性别",
                                                options: [{
                                                    label: '男',
                                                    value: '男',
                                                }, {
                                                    label: '女',
                                                    value: '女',
                                                }]

                                            },
                                            layout: layout_personnel
                                        },

                                        {
                                            type: InputType.date,
                                            col_span: 8,
                                            label: "出生日期",
                                            decorator_id: "date_birth",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择出生日期" }],
                                                initialValue: base_info ? base_info.date_birth : new Date(),
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请选择出生日期",
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "电话",
                                            decorator_id: "telephone",
                                            field_decorator_option: {
                                                rules: [{ message: "请输入电话" }],
                                                initialValue: base_info ? base_info.telephone : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入电话"
                                            },
                                            layout: layout_personnel
                                        }
                                    ]
                                },
                            },
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "民族",
                                            decorator_id: "nation",
                                            field_decorator_option: {
                                                rules: [{ message: "请选择民族" }],
                                                initialValue: base_info ? base_info.nation : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: nation,
                                                placeholder: "请选择民族",
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "籍贯",
                                            decorator_id: "native_place",
                                            field_decorator_option: {
                                                rules: [{ message: "请输入籍贯" }],
                                                initialValue: base_info ? base_info.native_place : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入籍贯"
                                            },
                                            layout: layout_personnel
                                        }, {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "家庭住址",
                                            decorator_id: "address",
                                            field_decorator_option: {
                                                rules: [{ message: "请输入家庭住址" }],
                                                initialValue: base_info ? base_info.address : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入家庭住址"
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        // {
                                        //     type: InputType.antd_input,
                                        //     col_span: 8,
                                        //     label: "银行卡号",
                                        //     decorator_id: "card_number",
                                        //     field_decorator_option: {
                                        //         rules: [{ message: "请输入卡号" }],
                                        //         initialValue: base_info ? base_info.card_number : '',
                                        //     } as GetFieldDecoratorOptions,
                                        //     option: {
                                        //         placeholder: "请输入卡号"
                                        //     },
                                        //     layout: layout_personnel
                                        // },
                                        // {
                                        //     type: InputType.antd_input,
                                        //     col_span: 8,
                                        //     label: "持卡人",
                                        //     decorator_id: "card_name",
                                        //     field_decorator_option: {
                                        //         rules: [{ message: "请输入持卡人" }],
                                        //         initialValue: base_info ? base_info.card_name : '',

                                        //     } as GetFieldDecoratorOptions,
                                        //     option: {
                                        //         placeholder: "请输入持卡人"
                                        //     },
                                        //     layout: layout_personnel
                                        // },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "身份证地址",
                                            decorator_id: "id_card_address",
                                            field_decorator_option: {
                                                rules: [{ message: "请输入身份证地址" }],
                                                initialValue: base_info ? base_info.id_card_address : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入身份证地址",
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.cascader,
                                            col_span: 8,
                                            label: "行政区划",
                                            decorator_id: "admin_area_id",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "请选择行政区划" }],
                                                initialValue: base_info ? base_info.admin_area_id : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                showSearch: this.filter,
                                                options: this.state.administrative_division_list,
                                                placeholder: "请选择行政区划",
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.tree_select,
                                            label: "组织机构",
                                            col_span: 8,
                                            decorator_id: "organization_id",
                                            field_decorator_option: {
                                                rules: [{ required: true, message: "组织机构" }],
                                                initialValue: base_info.organization_id ? base_info.organization_id : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                showSearch: true,
                                                treeNodeFilterProp: 'title',
                                                allowClear: true,
                                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                                treeDefaultExpandAll: true,
                                                treeData: this.state.org_list,
                                                placeholder: "请选择组织机构",
                                                onChange: (e: any) => {
                                                    this.formCreater && this.formCreater.setFieldsValue({
                                                        'role_id': '',
                                                    });
                                                    this.setState({
                                                        selected_organization: e,
                                                    });
                                                }
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            }, {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.upload,
                                            col_span: 8,
                                            label: "照片（大小小于2M，格式支持jpg/jpeg/png）",
                                            decorator_id: "picture",
                                            field_decorator_option: {
                                                rules: [{ message: "请上传照片", required: false }],
                                                initialValue: base_info && base_info.picture ? base_info.picture : [],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                action: remote.upload_url
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.text_area,
                                            col_span: 8,
                                            label: "备注",
                                            decorator_id: "remarks",
                                            field_decorator_option: {
                                                rules: [{ message: "请输入备注" }],
                                                initialValue: base_info ? base_info.remarks : '',

                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入备注"
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            },
                            // {
                            //     one_row_inputs: {
                            //         inputs_props: [
                            //             {
                            //                 type: InputType.fieldList,
                            //                 col_span: 8,
                            //                 label: "联系人",
                            //                 decorator_id: "family_name",
                            //                 layout: layout,
                            //                 field_decorator_option: {
                            //                     rules: [{ required: false }],
                            //                     initialValue: base_info['family_name'].length > 0 ? base_info['family_name'] : ''
                            //                 } as GetFieldDecoratorOptions,
                            //                 option: {
                            //                     befor_placeholder: "联系人",
                            //                     after_placeholder: '手机号码',
                            //                     third_placeholder: '与联系人的关系',
                            //                     before_key: 'title',
                            //                     after_key: 'contents',
                            //                     third_key: 'relation'
                            //                 }
                            //             },
                            //             {
                            //                 type: InputType.select,
                            //                 col_span: 8,
                            //                 label: "在职状态",
                            //                 decorator_id: "work_state",
                            //                 field_decorator_option: {
                            //                     rules: [{ required: true, message: "请选择在职状态" }],
                            //                     initialValue: base_info && base_info.work_state ? base_info.work_state : '',
                            //                 } as GetFieldDecoratorOptions,
                            //                 option: {
                            //                     childrens: work_dom,
                            //                     placeholder: "请选择在职状态",
                            //                 },
                            //                 layout: layout_personnel
                            //             },
                            //         ]
                            //     },
                            // },
                            {
                                one_row_inputs: {
                                    inputs_props: [{
                                        type: InputType.select,
                                        col_span: 8,
                                        label: "学历",
                                        decorator_id: "education",
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "请选择学历" }],
                                            initialValue: base_info && base_info.education ? base_info.education : '',
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            childrens: education_dom,
                                            placeholder: "请选择学历",
                                        },
                                        layout: layout_personnel
                                    },
                                    {
                                        type: InputType.antd_input,
                                        col_span: 8,
                                        label: "薪酬",
                                        decorator_id: "salary",
                                        field_decorator_option: {
                                            rules: [{ required: false, message: "请输入薪酬" }],
                                            initialValue: base_info && base_info.salary ? base_info.salary : '',
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入薪酬",
                                        },
                                        layout: layout_personnel
                                    }, {
                                        type: InputType.antd_input_number,
                                        col_span: 8,
                                        label: "从业年限",
                                        decorator_id: "work_years",
                                        field_decorator_option: {
                                            rules: [{ message: "请输入从业年限", required: false }],
                                            initialValue: base_info && base_info.work_years ? base_info.work_years : 0,
                                        } as GetFieldDecoratorOptions,
                                        option: {
                                            placeholder: "请输入从业年限",
                                        },
                                        layout: layout_personnel
                                    }]
                                },
                            },
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "在职状态",
                                            decorator_id: "work_state",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请选择在职状态" }],
                                                initialValue: base_info && base_info.work_state ? base_info.work_state : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: work_dom,
                                                placeholder: "请选择在职状态",
                                            },
                                            layout: layout_personnel
                                        }, {
                                            type: InputType.select,
                                            col_span: 8,
                                            label: "职务",
                                            decorator_id: "post",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请输入职务" }],
                                                initialValue: base_info.post,
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                childrens: post_dom,
                                                placeholder: "请输入职务",
                                            },
                                            layout: layout_personnel
                                        }, {
                                            type: InputType.text_area,
                                            col_span: 8,
                                            label: "个人介绍",
                                            decorator_id: "person_introduce",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请输入个人介绍" }],
                                                initialValue: base_info && base_info.person_introduce ? base_info.person_introduce : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入个人介绍",
                                            },
                                            layout: layout_personnel
                                        }]
                                },
                            },
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.date,
                                            col_span: 8,
                                            label: "毕业时间",
                                            decorator_id: "graduation_time",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请选择毕业时间" }],
                                                initialValue: base_info ? base_info.graduation_time : undefined,
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请选择毕业时间",
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "毕业院校",
                                            decorator_id: "graduated_school",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请输入毕业院校" }],
                                                initialValue: base_info ? base_info.graduated_school : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入毕业院校",
                                            },
                                            layout: layout_personnel
                                        }, {
                                            type: InputType.antd_input,
                                            col_span: 8,
                                            label: "专业",
                                            decorator_id: "major",
                                            field_decorator_option: {
                                                rules: [{ required: false, message: "请输入专业" }],
                                                initialValue: base_info ? base_info.major : '',
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                placeholder: "请输入专业",
                                            },
                                            layout: layout_personnel
                                        },]
                                },
                            },
                            {
                                one_row_inputs: {
                                    inputs_props: [
                                        {
                                            type: InputType.fieldList,
                                            col_span: 10,
                                            label: "专业资格职称",
                                            decorator_id: "professional_qualification_name",
                                            layout: layout,
                                            field_decorator_option: {
                                                rules: [{ required: false }],
                                                initialValue: base_info['professional_qualification_name'].length > 0 ? base_info['professional_qualification_name'] : ''
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                befor_placeholder: "专业资格职称",
                                                before_key: 'professional_qualification_name',
                                            }
                                        },
                                        {
                                            type: InputType.upload,
                                            col_span: 7,
                                            label: "专业资格证书（大小小于2M，格式支持jpg/jpeg/png）",
                                            decorator_id: "professional_certificate",
                                            field_decorator_option: {
                                                rules: [{ message: "请上传专业资格证书", required: false }],
                                                initialValue: base_info && base_info.professional_certificate ? base_info.professional_certificate : [],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                action: remote.upload_url
                                            },
                                            layout: layout_personnel
                                        },
                                        {
                                            type: InputType.upload,
                                            col_span: 7,
                                            label: "学历毕业证书（大小小于2M，格式支持jpg/jpeg/png）",
                                            decorator_id: "diploma",
                                            field_decorator_option: {
                                                rules: [{ message: "请上传学历毕业证书", required: false }],
                                                initialValue: base_info && base_info.diploma ? base_info.diploma : [],
                                            } as GetFieldDecoratorOptions,
                                            option: {
                                                action: remote.upload_url
                                            },
                                            layout: layout_personnel
                                        },
                                    ]
                                },
                            },
                        ]
                    },
                ],
                other_btn_propps: [
                    {
                        text: "读取身份证",
                        cb: () => this.handleIDCardReader!()
                    },
                    // 空处理，否则会报错/NY
                    ...(this.props.detailed_info_other_btn_propps! || [])
                ],
                submit_btn_propps: this.props.base_info_submit_btn_propps,
                service_option: {
                    service_object: AppServiceUtility.person_org_manage_service,
                    operation_option: {
                        save: {
                            func_name: this.props.submit_face ? this.props.submit_face : "update_personnel_info"
                        },
                        query: {
                            func_name: this.props.select_face ? this.props.select_face : "get_personnel_list_all",
                            arguments: [{ id: this.props.id }, 1, 1]
                        }
                    },
                },
                succ_func: this.props.detailed_info_succ_func,
                id: this.props.id,
                onRef2: this.onRef2
            };
        let edit_props_list = Object.assign(edit_props_personnel, edit_props_info);
        return (
            <div>
                {
                    is_detailed_info ?
                        <FormCreator {...edit_props_list} /> :
                        <FormCreator {...elder_info} />
                }
            </div>
        );
    }
}

/**
 * 控件：工作人员信息输入组件控制器
 * 工作人员信息输入组件
 */
@addon('WorkerUserEntry', '工作人员信息输入组件', '工作人员信息输入组件')
@reactControl(WorkerUserEntry)
export class WorkerUserEntryControl extends BaseReactElementControl {
    /** 是否为详细信息 */
    public is_detailed_info?: boolean;
    /** 基本信息 */
    public base_info?: any;
    /** 基本信息按钮 */
    public base_info_submit_btn_propps?: any;
    /** 详细信息其他按钮 */
    public detailed_info_other_btn_propps?: any[];
    /** 信息id */
    public id?: string;
    /** 详细信息保存成功回调 */
    public detailed_info_succ_func?: any;
    /** 提交接口 */
    public submit_face?: string;
    /** 查询接口 */
    public select_face?: string;
    /** 人员分类 */
    public personnel_category?: string;
    /** 长者类型 */
    public personnel_classification?: string;

}