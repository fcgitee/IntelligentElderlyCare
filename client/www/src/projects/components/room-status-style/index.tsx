import { Draggable, DraggableProvided, DraggableStateSnapshot, Droppable, DroppableProvided, DroppableStateSnapshot } from "react-beautiful-dnd";
import React from "react";
import "./index.less";
import { BaseReactElementControl } from "pao-aop-client";
import { Row, Col } from "antd";
import { RoomStatus, ROOM_NULL_STATUS } from "src/business/util_tool";

/**
 * 可拖拽的长者组件
 */
export function DraggableOlderElements(props: {
    /**
     * 长者id
     */
    residentsId: string;
    /**
     * 长者名字
     */
    residentsName: string;
    /**
     * 长者性别
     */
    residentsSex: string;
    /**
     * 长者护理等级
     */
    residentsNursingLevel: string;
    /**
     * 拖拽元素索引号
     */
    index: number;
}) {
    return (
        // 可拖拽元素
        <Draggable draggableId={props.residentsId} index={props.index}>
            {
                (provided: DraggableProvided, snapshot: DraggableStateSnapshot) => {
                    return (
                        <div
                            className={"older" + ' ' + (props.residentsSex === '男' ? 'manStyle' : 'womenStyle') + ' ' + props.residentsNursingLevel}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                        >
                            {props.residentsName}
                        </div>
                    );
                }
            }
        </Draggable>
    );
}

/**
 * 可拖拽区域（可放置拖拽组件）
 */
export function DroppableBedElements(props: {
    /**
     * 床位id
     */
    bedId: string;
    /**
     * 床位元素（可能是空床，可能是有长者的床）
     */
    bedJSXElement: JSX.Element;
}) {
    return (
        // 可拖拽区域
        <Droppable droppableId={props.bedId} direction={"vertical"}>
            {
                (provided: DroppableProvided, snapshot: DroppableStateSnapshot) => (
                    <div
                        // className={"bed"}
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                    >
                        {props.bedJSXElement}
                    </div>
                )
            }
        </Droppable>
    );
}

export function RoomStatusWrapElements(props: BaseReactElementControl & {
    /**
     * 房态图区域元素
     */
    roomStatus: RoomStatus,
    /**
     * 颜色池
     */
    colorPool: string[],
    /**
     * 层级
     */
    level: number
}) {
    return (
        <Row type={"flex"} justify={"space-between"} className={"divBorder"} style={{ background: props.colorPool[props.level] }}>
            <Col className={"roomStaus"} span={24}>
                {props.roomStatus.zone_name === ROOM_NULL_STATUS ? undefined : props.roomStatus.zone_name}
            </Col>
            {
                props.children
            }
        </Row>
    );
}