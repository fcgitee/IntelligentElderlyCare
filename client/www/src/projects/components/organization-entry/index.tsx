import { Select, Modal } from "antd";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";
import FormCreator, { InputType } from "src/business/components/buss-components/form-creator";
import { request } from "src/business/util_tool";
import { AppServiceUtility } from 'src/projects/app/appService';
import { edit_props_info } from "src/projects/app/util-tool";
import { remote } from "src/projects/remote";

let { Option } = Select;
/**
 * 组件：组织机构输入组件状态
 */
export interface OrganizationEntryState extends BaseReactElementState {
    /** 行政区划列表 */
    administrative_division_list?: any;
    /** 基础数据 */
    base_data?: any;
    /** 组织机构列表 */
    org_list?: any[];
    // 坐标拾取系统
    isBaiduMapShow?: boolean;
    // 是否可以编辑
    edit?: any;
}

/**
 * 组件：组织机构输入组件
 * 组织机构输入组件
 */
export class OrganizationEntry extends BaseReactElement<OrganizationEntryControl, OrganizationEntryState> {
    constructor(props: any) {
        super(props);
        this.state = {
            administrative_division_list: [],
            isBaiduMapShow: false,
            base_data: { 'qualification_info': [] },
            edit: true
        };
    }
    // 校验手机号码
    checkPhone = (rule: any, value: any, callback: any) => {
        var phone = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if (value === '') {
            callback('请输入手机号码');
        }
        if (value.length !== 11) {
            callback('格式不正确');
            if (!phone.test(value)) {
                callback('格式不正确');
            }
        }
    }
    // 校验座机号码
    checkLinePhone = (rule: any, value: any, callback: any) => {
        var phone = /^0\d{2,3}-\d{7,8}$/;
        if (value === '') {
            callback('请输入固定电话号码');
        }
        if (value.length !== 11 && value.length !== 12) {
            callback('格式不正确');
            if (!phone.test(value)) {
                callback('格式不正确');
            }
        }
    }
    componentWillMount() {
        request(this, AppServiceUtility.role_service.get_current_user_role_name!())
            .then((data: any) => {
                if (data.length > 0) {
                    if (data[0]['name'].indexOf("管理员") !== -1) {
                        this.setState({
                            edit: false
                        });
                    }
                }
            });

        // AppServiceUtility.business_area_service.get_business_area_list!({})!
        //     .then((data: any) => {
        //         if (data.result) {
        //             this.setState({
        //                 administrative_division_list: data.result
        //             });
        //         }
        //     });
        AppServiceUtility.business_area_service.get_admin_division_list!({})!
            .then(data => {
                let handleData = (value: any, title?: string) => {
                    if (value && value.length > 0) {
                        value.forEach((item: any) => {
                            item["title"] = title ? (title + "-" + item.title) : item.title;
                            if (item.children && item.children.length > 0) {
                                handleData(item.children, item.title);
                            }
                        });
                    }
                };
                handleData(data!.result);
                this.setState({
                    administrative_division_list: data!.result
                });
            })
            .catch(err => {
                console.info(err);
            });
        AppServiceUtility.person_org_manage_service.get_organization_tree_list!({})!
            .then(data => {
                let { base_data } = this.state;
                let org_list: any = [];
                if (!this.props.id || base_data.organization_info.super_org_id === '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67') {
                    org_list = [{
                        id: '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67',
                        key: '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67',
                        title: '智慧养老平台(民政)',
                        name: '智慧养老平台',
                        value: "7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67",
                        children: data!.result
                    }];
                } else {
                    org_list = data!.result;
                }
                this.setState({ org_list });
            })
            .catch(err => {
                console.info(err);
            });
        let { id } = this.props;
        if (id) {
            let { service_option } = this.props;
            let operation_option = service_option.operation_option;
            AppServiceUtility.person_org_manage_service.get_organization_list_all!(...operation_option.query.arguments)!
                .then((data: any) => {
                    this.setState({
                        base_data: data.result[0]
                    });
                });
        }
    }
    toggleBaiduMap(type: boolean) {
        window.open('http://api.map.baidu.com/lbsapi/getpoint/');
        // this.setState({
        //     isBaiduMapShow: type,
        // });
    }
    render() {
        const org_type = ['福利院', '幸福院', '服务商', '平台', '民政'];
        const org_type_list: JSX.Element[] = org_type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        const org_nature = ['公办', '公办民营', '民办非营利', '民办企业'];
        const org_nature_list: JSX.Element[] = org_nature!.map((item) => <Option key={item} value={item}>{item}</Option>);
        const nashui_level = ['A级', 'B级', 'M级', 'C级', 'D级'];
        const taxes_level_list: JSX.Element[] = nashui_level!.map((item) => <Option key={item} value={item}>{item}</Option>);
        const contract_status_type = ['签约', '解约'];
        const contract_status_type_list: JSX.Element[] = contract_status_type!.map((item) => <Option key={item} value={item}>{item}</Option>);
        let { base_data } = this.state;
        let edit_props: any = {
            form_items_props: [
                {
                    title: "基本信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "机构名称",
                            decorator_id: "name",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入机构名称" }],
                                initialValue: base_data ? base_data.name : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构名称",
                                autocomplete: 'off',
                                disabled: this.state.edit
                            }
                        },
                        {
                            type: InputType.select,
                            label: "养老服务类型",
                            decorator_id: "personnel_category",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择养老服务类型" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.personnel_category : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择养老服务类型",
                                childrens: org_type_list,
                                autocomplete: 'off',
                                disabled: this.state.edit
                            }
                        },
                        {
                            type: InputType.select,
                            label: "机构性质",
                            decorator_id: "organization_nature",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择机构性质" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.organization_nature : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择机构性质",
                                childrens: org_nature_list,
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.date,
                            label: "成立时间",
                            decorator_id: "reg_date",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择成立时间" }],
                                initialValue: base_data && base_data.reg_date ? base_data.reg_date : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择成立时间",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.tree_select,
                            label: "行政区划",
                            decorator_id: "admin_area_id",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请选择行政区划" }],
                                initialValue: base_data ? base_data.admin_area_id : ''
                            } as GetFieldDecoratorOptions,
                            option: {
                                showSearch: true,
                                treeNodeFilterProp: 'title',
                                dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                allowClear: true,
                                treeDefaultExpandAll: true,
                                treeData: this.state.administrative_division_list,
                                placeholder: "请选择行政区划",
                                autocomplete: 'off',
                                disabled: this.state.edit
                            },
                        },
                        {
                            type: InputType.antd_input,
                            label: "机构地址",
                            decorator_id: "address",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入机构地址" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.address : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构地址",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.btn_input,
                            label: "经度",
                            decorator_id: "lon",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入经度" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.lon : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入经度",
                                enterButton: "点击拾取坐标",
                                onSearch: () => this.toggleBaiduMap(true),
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "纬度",
                            decorator_id: "lat",
                            field_decorator_option: {
                                rules: [{ required: true, message: "请输入纬度" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.lat : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入纬度",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "法定代表人",
                            decorator_id: "legal_person",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入法定代表人" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.legal_person : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入法定代表人",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "统一社会信用代码",
                            decorator_id: "unified_social",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入统一社会信用代码" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.unified_social : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入统一社会信用代码",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "统一社会信用代码证（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "unified_social_credit",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传统一社会信用代码证" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.unified_social_credit : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传统一社会信用代码证",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否建成消防喷淋系统",
                            decorator_id: "if_build_fire_spray_sys",
                            field_decorator_option: {
                                rules: [{ required: false, message: "是否建成消防喷淋系统" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.if_build_fire_spray_sys : false,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择是否建成消防喷淋系统",
                                options: [{
                                    label: '是',
                                    value: true,
                                }, {
                                    label: '否',
                                    value: false,
                                }],
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "消防喷淋系统相关图（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "if_build_fire_spray_sys_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传消防喷淋系统相关图" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.if_build_fire_spray_sys_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传消防喷淋系统相关图",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.radioGroup,
                            label: "是否建成烟感系统",
                            decorator_id: "if_build_smoke_sense_sys",
                            field_decorator_option: {
                                rules: [{ required: false, message: "是否建成烟感系统" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.if_build_smoke_sense_sys : false,
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择是否建成烟感系统",
                                options: [{
                                    label: '是',
                                    value: true,
                                }, {
                                    label: '否',
                                    value: false,
                                }],
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "烟感系统相关图（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "if_build_smoke_sense_sys_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传烟感系统相关图" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.if_build_smoke_sense_sys_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传烟感系统相关图",
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
                {
                    title: "资质证明材料",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.antd_input,
                            label: "养老机构许可证编号",
                            decorator_id: "older_org_permission_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入养老机构许可证编号" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.older_org_permission_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入养老机构许可证编号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "养老机构许可证证件（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "older_org_permission_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入养老机构许可证证件" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.older_org_permission_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请输入养老机构许可证证件",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "土地证号",
                            decorator_id: "land_deed_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入土地证号" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.land_deed_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入土地证号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "土地证件（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "land_deed_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传土地证件" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.land_deed_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传土地证件",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "房产证号",
                            decorator_id: "property_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入房产证号" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.property_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入房产证号",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "房产证件（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "property_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传房产证件" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.property_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传房产证件",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "卫生许可证号",
                            decorator_id: "health_license_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入卫生许可证号" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.health_license_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入卫生许可证号",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "卫生许可证件（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "health_license_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传卫生许可证件" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.health_license_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传卫生许可证件',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "消防安全许可证号",
                            decorator_id: "fire_safety_permit_no",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入消防安全许可证号" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.fire_safety_permit_no : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入消防安全许可证号",
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.upload,
                            label: "消防安全许可证件（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "fire_safety_permit_file",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传消防安全许可证件" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.fire_safety_permit_file : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: "请上传消防安全许可证件",
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
                {
                    title: "服务信息",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.nt_rich_text,
                            label: "机构简介",
                            decorator_id: "description",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入机构简介" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.description : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入机构简介",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.nt_rich_text,
                            label: "医养结合简介",
                            decorator_id: "medical_introducte",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入医养结合简介" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.medical_introducte : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入医养结合简介",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "占地面积/㎡",
                            decorator_id: "cover_area",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入占地面积" }],
                                initialValue: base_data.organization_info && base_data.organization_info.cover_area ? base_data.organization_info.cover_area : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入占地面积",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "建筑面积/㎡",
                            decorator_id: "build_area",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入建筑面积" }],
                                initialValue: base_data.organization_info && base_data.organization_info.build_area ? base_data.organization_info.build_area : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入建筑面积",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "配置医疗机构名称",
                            decorator_id: "configured_medical",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入配置医疗机构名称" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.configured_medical : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入配置医疗机构名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.antd_input,
                            label: "合作医疗机构名称",
                            decorator_id: "cooperative_medical",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入合作医疗机构名称" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.cooperative_medical : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入合作医疗机构名称",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.rate,
                            label: "机构星级",
                            decorator_id: "star_level",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择机构星级" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.star_level : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择机构星级",
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
                {
                    title: "服务收费标准",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.text_area,
                            label: "床位费介绍",
                            decorator_id: "bed_price_describe",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入床位费介绍" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.bed_price_describe : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位费介绍",
                                autocomplete: 'off',
                                row: 4,
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "床位费",
                            decorator_id: "bed_price",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入床位费" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.bed_price : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入床位费",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "护理费介绍",
                            decorator_id: "nurse_price_describe",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入护理费介绍" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.nurse_price_describe : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入护理费介绍",
                                autocomplete: 'off',
                                row: 4,
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "护理费",
                            decorator_id: "nurse_price",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入护理费" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.nurse_price : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入护理费",
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.text_area,
                            label: "伙食费介绍",
                            decorator_id: "meals_price_describe",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入伙食费介绍" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.meals_price_describe : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入伙食费介绍",
                                autocomplete: 'off',
                                row: 4,
                            }
                        },
                        {
                            type: InputType.input_number,
                            label: "伙食费",
                            decorator_id: "meals_price",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入伙食费" }],
                                initialValue: base_data.organization_info && base_data.organization_info.service_standard ? base_data.organization_info.service_standard.meals_price : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入伙食费",
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
                {
                    title: "机构风貌照片",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.upload,
                            label: "机构logo（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "organization_logo",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传机构logo" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.organization_logo : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传机构logo',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "风貌照片（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "picture_list",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传风貌照片" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.picture_list : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传风貌照片',
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
                {
                    title: "其他",
                    need_card: true,
                    input_props: [
                        {
                            type: InputType.text_area,
                            label: "服务协议",
                            decorator_id: "service_agreement",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入服务协议" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.service_agreement : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入服务协议",
                                row: 3,
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.select,
                            label: "机构纳税信用等级",
                            decorator_id: "taxes_level",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择机构纳税信用等级" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.taxes_level : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择机构纳税信用等级",
                                childrens: taxes_level_list,
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "机构纳税信用等级证书（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "taxes_level_certificate",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传机构纳税信用等级证书" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.taxes_level_certificate : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传机构纳税信用等级证书',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "平台备案承诺书（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "platform_filing_commitment",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传平台备案承诺书" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.platform_filing_commitment : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传平台备案承诺书',
                                autocomplete: 'off',
                            }
                        },
                        {
                            type: InputType.upload,
                            label: "营业执照（大小小于2M，格式支持jpg/jpeg/png）",
                            decorator_id: "business_license_url",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请上传营业执照" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.business_license_url : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                action: remote.upload_url,
                                placeholder: '请上传营业执照',
                                autocomplete: 'off',
                            }
                        }, {
                            type: InputType.antd_input,
                            label: "联系电话",
                            decorator_id: "telephone",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入联系电话" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.telephone : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入联系电话",
                                autocomplete: 'off',
                            }
                        },
                        // 20200715修改，顶级平台不需要选择上级组织机构
                        {
                            ...(base_data.id === '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67' ? {} : {
                                type: InputType.tree_select,
                                label: "上级组织机构",
                                decorator_id: "super_org_id",
                                field_decorator_option: {
                                    rules: [{ required: true, message: "上级组织机构" }],
                                    initialValue: base_data.organization_info ? base_data.organization_info.super_org_id : '7e7e2fec-d91d-11e9-8e1d-983b8f0bcd67',
                                } as GetFieldDecoratorOptions,
                                option: {
                                    showSearch: true,
                                    treeNodeFilterProp: 'title',
                                    dropdownStyle: { maxHeight: 400, overflow: 'auto' },
                                    allowClear: true,
                                    treeDefaultExpandAll: true,
                                    treeData: this.state.org_list,
                                    placeholder: "请选择上级组织机构",
                                    autocomplete: 'off',
                                    disabled: this.props.id
                                },
                            })
                        },
                        {
                            type: InputType.text_area,
                            label: "备注",
                            decorator_id: "comment",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请输入备注" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.comment : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请输入备注",
                                row: 3,
                                autocomplete: 'off',
                            }
                        },
                        // {
                        //     type: InputType.objectFieldList,
                        //     label: "资质信息",
                        //     decorator_id: "qualification_info",
                        //     field_decorator_option: {
                        //         rules: [],
                        //         initialValue: base_data['qualification_info'].length > 0 ? base_data['qualification_info'] : ''
                        //     } as GetFieldDecoratorOptions,
                        //     option: {
                        //         befor_placeholder: "资质名称",
                        //         after_placeholder: '资质等级',
                        //         before_key: 'name',
                        //         after_key: 'qualification_level',
                        //     }
                        // }
                        {
                            type: InputType.select,
                            label: "是否签约",
                            decorator_id: "contract_status",
                            field_decorator_option: {
                                rules: [{ required: false, message: "请选择签约状态" }],
                                initialValue: base_data.organization_info ? base_data.organization_info.contract_status : '',
                            } as GetFieldDecoratorOptions,
                            option: {
                                placeholder: "请选择签约状态",
                                childrens: contract_status_type_list,
                                autocomplete: 'off',
                            }
                        },
                    ]
                },
            ],
            other_btn_propps: [
                ...this.props.other_btn_propps!
            ],
            submit_btn_propps: this.props.submit_btn_propps,
            service_option: this.props.service_option,
            id: this.props.id,
        };
        let edit_props_list = Object.assign(edit_props, edit_props_info);
        return (
            <div>
                <FormCreator {...edit_props_list} />
                <Modal
                    title="坐标拾取系统"
                    width='90%'
                    bodyStyle={{ height: '700px' }}
                    visible={this.state.isBaiduMapShow}
                    onOk={() => this.toggleBaiduMap(false)}
                    onCancel={() => this.toggleBaiduMap(false)}
                    okText="复制后请手动输入"
                >
                    <iframe name="baiduMapName" id="baiduMapId" src="http://api.map.baidu.com/lbsapi/getpoint/" style={{ width: '100%', height: '100%' }} />
                </Modal>
            </div>
        );
    }
}

/**
 * 控件：组织机构输入组件控制器
 * 组织机构输入组件
 */
@addon('OrganizationEntry', '组织机构输入组件', '组织机构输入组件')
@reactControl(OrganizationEntry)
export class OrganizationEntryControl extends BaseReactElementControl {
    /** 组织机构id */
    id?: string;
    /** 基本数据 */
    public base_data?: any;
    /** 提交按钮 */
    public submit_btn_propps?: any;
    /** 其他按钮 */
    public other_btn_propps?: any[];
    /** 服务配置 */
    public service_option?: any;
}