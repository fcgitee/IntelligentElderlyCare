import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import { Card } from "antd";
import React from "react";
import './index.less';
/**
 * 组件：首页卡片状态
 */
export interface HomeCardState extends BaseReactElementState {
}

/**
 * 组件：首页卡片
 * 首页卡片
 */
export class HomeCard extends BaseReactElement<HomeCardControl, HomeCardState> {
    render() {
        return (
            <Card className='home-card' style={{ lineHeight: '100px', width: '100%', textAlign: 'center', margin: '0 0 20px 0', fontSize: '30px', cursor: 'pointer', borderRadius: '10px', }}>
                {this.props.children}
            </Card>
        );
    }
}

/**
 * 控件：首页卡片控制器
 * 首页卡片
 */
@addon('HomeCard', '首页卡片', '首页卡片')
@reactControl(HomeCard)
export class HomeCardControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}