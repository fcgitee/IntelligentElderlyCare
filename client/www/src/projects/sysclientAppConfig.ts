// /*
//  * @Description: In User Settings Edit
//  * @Author: your name
//  * @Date: 2019-06-20 17:37:29
//  * @LastEditTime: 2019-11-24 18:37:09
//  * @LastEditors: Please set LastEditors
//  */
// /*
//  * 版权：Copyright (c) 2019 红网
//  *
//  * 创建日期：Tuesday April 23rd 2019
//  * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
//  *
//  * 修改日期: Tuesday, 23rd April 2019 10:26:24 am
//  * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
//  *
//  * 说明
//  * 		1、政务物联网运维APP配置
//  */
// import { createObject, IPermissionService, IRoleService, ISecurityService } from 'pao-aop';
// import { IPermissionService as IPermissionServices } from "src/business/models/role";
// import { AjaxJsonRpcFactory, AppReactRouterControl, BlankMainFormControl, CookieUtil } from 'pao-aop-client';
// import { BackstageManageMainFormControl, COOKIE_KEY_CURRENT_USER, COOKIE_KEY_USER_ROLE } from 'src/business/mainForm/backstageManageMainForm';
// import { HeadMainFormControl } from 'src/business/mainForm/headMainForm';
// import { KEY_PARAM, CODE_PARAM } from 'src/business/util_tool';
// import { AddRoleUserViewControl } from 'src/business/views/role-user/change-role-user';
// import { BindPhoneControl } from 'src/business/views/bind-phone';
// import { LoginViewControl } from 'src/business/views/login';
// import { ModifyLoginPasswordViewControl } from 'src/business/views/modify-password';
// import { ModifyEmailViewControl } from 'src/business/views/modify_email';
// import { ModifyMobileViewControl } from 'src/business/views/modify_mobile';
// import { RetrievePasswordControl } from 'src/business/views/retrieve-password';
// import { RolePermissionViewControl } from 'src/business/views/role-user';
// import { SecuritySettingsViewControl } from 'src/business/views/security-setting';
// import { IRoomService } from "src/projects/models/room";
// import { IUserService } from "src/projects/models/user";
// import { AjaxJsonRpcLoadingFactory } from './../business/components/buss-components/ajax-loading/index';
// import { IntelligentElderlyCareApplication } from './app';
// import { PermissionList } from './app/permission';
// import { IAssessmentProjectService } from './models/assessment-project';
// import { IAssessmentTemplateService } from './models/assessment-template';
// import { ChangeAssessmentProjectViewControl } from './views/buss_iec/assessment-project/change-assessment-project';
// import { AssessmentProjectViewControl } from './views/buss_iec/assessment-project/index';
// import { ChangeAssessmentTemplateViewControl } from './views/buss_iec/assessment-template/change-assessment-template';
// import { AssessmentTemplateViewControl } from './views/buss_iec/assessment-template/index';
// import { ChangeBehavioralCompetenceAssessmentViewControl } from './views/buss_iec/behavioral_competence_assessment/change-behavioral-competence-assessment';
// import { BehavioralCompetenceAssessmentViewControl } from './views/buss_iec/behavioral_competence_assessment/index';
// import { ChangeCompetenceAssessmentViewControl } from './views/buss_iec/competence_assessment/change-competence-assessment';
// import { CompetenceAssessmentViewControl } from './views/buss_iec/competence_assessment/index';
// import { ChangeBusinessAreaViewControl } from './views/buss_mis/business-area-manage/change-business-area';
// import { BusinessAreaManageViewControl } from './views/buss_mis/business-area-manage/index';
// import { ChangeAccountViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-account';
// import { ChangeAccountBookViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-account-book';
// import { FinancialAccountViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/index-account';
// import { FinancialAccountBookViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/index-account-book';
// import { ChangePaymentViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-payment';
// import { ChangeReceiveViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-receive';
// import { ChangeAccountReturnViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-return';
// import { ChangeTransferViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-transfer';
// import { FinancialAccountFlowViewControl } from './views/buss_mis/financial-manage/financial-account-manage/index';
// import { ChangeServiceItemViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/change-service-item';
// import { ServiceItemViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/index';
// import { ChangeServiceOrderViewControl } from './views/buss_mis/service-operation-manage/service-order-manage/change-service-order';
// import { ServiceOrderViewControl } from './views/buss_mis/service-operation-manage/service-order-manage/index';
// import { ServiceOrderTaskViewControl } from './views/buss_mis/service-operation-manage/service-order-task-manage/index';
// import { ChangeServiceProjectViewControl } from './views/buss_mis/service-operation-manage/service-project-manage/change-service-project';
// import { ServiceProjectViewControl } from './views/buss_mis/service-operation-manage/service-project-manage/index';
// import { ChangeServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/change-service-provider';
// import { ServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/index';
// import { ChangeServiceTypeViewControl } from './views/buss_mis/service-operation-manage/service-type-manage/change-service-type';
// import { ServiceTypeViewControl } from './views/buss_mis/service-operation-manage/service-type-manage/index';
// import { ChangeServicesItemCategoryViewControl } from './views/buss_mis/service-operation-manage/services-item-category-manage/change-services-item-category';
// import { ServicesItemCategoryViewControl } from './views/buss_mis/service-operation-manage/services-item-category-manage/index';
// import { ChangeTransactionViewControl } from './views/buss_mis/transaction-management/change-transaction';
// import { TransactionManageViewControl } from './views/buss_mis/transaction-management/index';
// import { TransactionCommentViewControl } from './views/buss_mis/transaction-management/index-comment';
// import { ChangeOrganizationViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/change-organization';
// import { OrganizationViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/index';
// import { ChangePersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/change-personnel';
// import { PersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/index';
// import { UserEditorViewControl } from './views/buss_pub/user-editor/index';
// import { UserManageViewControl } from './views/buss_pub/user-manage/index';
// import { HomeViewControl } from './views/home/index';
// import { ChangeBedManageViewControl } from './views/welfare-institution/bed-manage/change-bed-manage';
// import { BedManageViewControl } from './views/welfare-institution/bed-manage/index';
// import { ChangeHotelZoneControl } from './views/welfare-institution/hotel/hotel-zone-manage/change-hotel-zone';
// import { ChangeHotelZoneTypeViewControl } from './views/welfare-institution/hotel/hotel-zone-type-manage/change-hotel-zone-type';
// import { HotelZoneTypeViewControl } from './views/welfare-institution/hotel/hotel-zone-type-manage/index';
// import { HydropowerViewControl } from './views/welfare-institution/hotel/hotel-hydropower';
// import { ChangeHydropowerViewControl } from './views/welfare-institution/hotel/hotel-hydropower/change-hydropower';
// import { ChangeNursingArchivesViewControl } from './views/welfare-institution/nursing-archives/change-nursing-archives';
// import { NursingTypeEditViewControl } from './views/welfare-institution/nursing-archives/change-nursing-type';
// import { NursingArchivesViewControl } from './views/welfare-institution/nursing-archives/index';
// import { NursingRelationShipEditViewControl } from './views/welfare-institution/nursing-archives/nursing-relationship-edit';
// import { NursingRelationShipViewControl } from './views/welfare-institution/nursing-archives/nursing-relationship-list';
// import { NursingTypeViewControl } from './views/welfare-institution/nursing-archives/nursing-type';
// import { ChangeNursingItemViewControl } from './views/welfare-institution/nursing-item/change-nursing-item';
// import { NursingItemViewControl } from './views/welfare-institution/nursing-item/index';
// import { ChangeRequirementProjectViewControl } from './views/welfare-institution/requirement/requirement-project-manage/change-requirement-project';
// import { ChangeRequirementTypeViewControl } from './views/welfare-institution/requirement/requirement-type-manage/change-requirement-type';
// import { RequirementTypeViewControl } from './views/welfare-institution/requirement/requirement-type-manage/index';
// import { CheckInDetailViewControl } from './views/welfare-institution/check-in/check-in-detail';
// import { CheckInListViewControl } from './views/welfare-institution/check-in/check-in-list';
// import { CheckInListViewJjControl } from './views/welfare-institution/check-in/check-in-list-special';
// import { ChangeRoomArchivesViewControl } from './views/welfare-institution/room-archives/change-room-archives';
// import { HotelZoneViewControl } from './views/welfare-institution/hotel/hotel-zone-manage/index';
// import { RequirementProjectViewControl } from './views/welfare-institution/requirement/requirement-project-manage/index';
// import { RequirementOptionViewControl } from './views/welfare-institution/requirement/requirement-option-manage/index';
// import { ChangeRequirementOptionViewControl } from './views/welfare-institution/requirement/requirement-option-manage/change-requirement-option';
// import { ChangeServiceItemPackageViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/change-service-item-package';
// import { ServiceItemPackageViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/service-item-package';
// import { ServiceScopeViewControl } from './views/buss_mis/service-operation-manage/service-scope-manage/index';
// import { ChangeServicesScopeViewControl } from './views/buss_mis/service-operation-manage/service-scope-manage/change-service-scope';
// import { LeaveReasonViewControl } from './views/welfare-institution/leave-reason/index';
// import { ChangeLeaveReasonViewControl } from './views/welfare-institution/leave-reason/change-leave-reason';
// import { ServiceWokerViewControl } from './views/buss_mis/service-operation-manage/service-woker-manage/index';
// import { ChangeServiceWokerViewControl } from './views/buss_mis/service-operation-manage/service-woker-manage/change-service-woker';
// import { ServiceOptionViewControl } from './views/buss_mis/service-operation-manage/service-option-manage/index';
// import { ChangeServiceOptionViewControl } from './views/buss_mis/service-operation-manage/service-option-manage/change-service-option';
// import { RoomArchivesViewControl } from './views/welfare-institution/room-archives/index';
// import { RoomStatusViewControl } from './views/welfare-institution/room-status/index';
// import { FinancialVoucherViewControl } from './views/buss_mis/financial-manage/financial-account-voucher-manage/index';
// import { ChangeAccountVoucherViewControl } from './views/buss_mis/financial-manage/financial-account-voucher-manage/change';
// import { FinancialSubjectViewControl } from './views/buss_mis/financial-manage/financial-subject-manage/index';
// import { ChangeFinancialSubjectViewControl } from './views/buss_mis/financial-manage/financial-subject-manage/change';
// import { ServiceRecordViewControl } from './views/buss_mis/service-operation-manage/service-record-manage/index';
// import { CheckInNewViewControl } from './views/welfare-institution/check-in/index';
// import { CheckInNewJjViewControl } from './views/welfare-institution/check-in/index-special';
// import { ChangeServiceRecordViewControl } from './views/buss_mis/service-operation-manage/service-record-manage/change-service-record';
// import { RequirementTemplateViewControl } from './views/welfare-institution/requirement/requirement-template-manage/index';
// import { ChangeRequirementTemplateViewControl } from './views/welfare-institution/requirement/requirement-template-manage/change-requirement-template';
// import { RequirementRecordViewControl } from './views/welfare-institution/requirement/requirement-record-manage/index';
// import { ChangeRequirementRecordViewControl } from './views/welfare-institution/requirement/requirement-record-manage/change-requirement-record';
// import { ServiceLedgerViewControl } from './views/buss_mis/service-operation-manage/service-ledger-manage/index';
// import { ReservationRegistrationViewControl } from './views/welfare-institution/reservation-registration/index';
// import { ChangeReservationViewControl } from './views/welfare-institution/reservation-registration/reservation-registration-info';
// import { ChangereservationRegistrationViewControl } from './views/welfare-institution/reservation-registration/reservation-registration';
// import { hospitalArrearsViewControl } from './views/welfare-institution/hospital/index';
// import { ChangeHospitalViewControl } from './views/welfare-institution/hospital/change-hospital';
// import { TaskListViewControl } from './views/buss_mis/tasks/task-list';
// import { TaskToBeSendViewControl } from './views/buss_mis/tasks/task-to-be-send';
// import { TaskToBeProcessViewControl } from './views/buss_mis/tasks/task-to-be-process';
// import { ChangeTaskViewControl } from './views/buss_mis/tasks/change-task';
// import { TaskTypeListViewControl } from './views/buss_mis/tasks/task-type-list';
// import { ChangeTaskTypeViewControl } from './views/buss_mis/tasks/change-task-type';
// import { IRequirementTemplateService } from './models/requirement-template';
// import { billRouter } from './router/bill';
// import { remote } from './remote/index';
// import { ROUTE_PATH, selectedKeys } from './router/index';
// import './mock';
// import './style/default.less';
// import { ChangeCollectorChargeViewControl } from './views/buss_mis/financial-manage/financial-collector/collector-charge';
// // import { FinancialBankReconciliationViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/index';
// import { BillRecodeViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/bill_recode';
// import { ChangeCollectorPaymentViewControl } from './views/buss_mis/financial-manage/financial-collector/collector-payment';
// import { FinancialChargeRecordViewControl } from './views/buss_mis/financial-manage/financial-collector/index';
// import { FinancialReconciliationRecordViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/index-record';
// import { UnFinancialReconciliationRecordViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/unband-reconciliation';
// import { AnnouncementIssueViewControl } from './views/buss_mis/information-manage/announcement-manage/index';
// import { AnnouncementListViewControl } from './views/buss_mis/information-manage/announcement-manage/announcement-list';
// import { NewsListViewControl } from './views/buss_mis/information-manage/news-manage/index';
// import { ChangeNewsViewControl } from './views/buss_mis/information-manage/news-manage/change-news';
// import { CommentListViewControl } from './views/buss_mis/information-manage/comment-manage/index';
// import { ChangeCommentAuditControl } from './views/buss_mis/information-manage/comment-manage/comment-audit';
// import { CommentAuditViewControl } from './views/buss_mis/information-manage/comment-manage/audit-list';
// import { RegisterControl } from 'src/business/views/register';
// import { ActivityPublishViewControl } from './views/buss_mis/activity-manage/activityPublish';
// import { ChangeActivityPublishViewControl } from './views/buss_mis/activity-manage/changActivityPublish';
// import { ActivityParticipateViewControl } from './views/buss_mis/activity-manage/activityParticipate';
// import { ActivitySignInViewControl } from './views/buss_mis/activity-manage/activitySignIn';
// import { ServicePersonalViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/index';
// import { ChangeServicePersonalViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/change-service-personal';
// import { ServicePersonalSettlementViewControl } from './views/buss_mis/service-settlement/personal-settlement';
// import { ServiceProviderSettlementViewControl } from './views/buss_mis/service-settlement/provide-settlement';
// import { CommentViewControl } from './views/buss_mis/information-manage/comment-manage/comment';
// import { ArticleTypeViewControl } from './views/buss_mis/information-manage/article-type/index';
// import { ChangeArticleTypeViewControl } from './views/buss_mis/information-manage/article-type/change-article-type';
// import { ServicePersonalApplyViewControl } from './views/buss_mis/service-operation-manage/service-personal-apply';
// import { CommentTypeViewControl } from './views/buss_mis/information-manage/comment-type/index';
// import { ChangeCommentTypeViewControl } from './views/buss_mis/information-manage/comment-type/change-comment-type';
// import { costAccountingViewControl } from './views/welfare-institution/costAccounting/index';
// import { ArticleAuditViewControl } from './views/buss_mis/information-manage/article-audit/audit-list';
// import { ChangeArticleAuditControl } from './views/buss_mis/information-manage/article-audit/article-audit';
// import { LeaveInsertViewControl } from './views/welfare-institution/leave-record/leave-insert';
// import { LeaveRecordViewControl } from './views/welfare-institution/leave-record/index';
// import { LeaveEditViewControl } from './views/welfare-institution/leave-record/leave-edit';
// import { AppServiceUtility } from './app/appService';
// import { ServiceProjectControl } from './views/buss_mis/service-operation-manage/service-project-buy/index';
// import { MessageListViewControl } from './views/buss_pub/message-manage/index';
// import { ChangeMessageViewControl } from './views/buss_pub/message-manage/change-message';
// import { ServiceProjectDetailControl } from './views/buss_mis/service-operation-manage/service-project-buy/service-project-detail';
// import { ServerProjectBuyControl } from './views/buss_mis/service-operation-manage/service-project-buy/service-project-buy';
// import { ApplyServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-apply';
// import { PersonnelPermissionControl } from './views/buss_pub/personnel-permission/index';
// import { ActivityTypeViewControl } from './views/buss_mis/activity-manage/activityType';
// import { ChangeActivityTypeViewControl } from './views/buss_mis/activity-manage/changeActivityType';
// import { AdministrationDivisionViewControl } from './views/buss_mis/administration-division-manage';
// import { ChangeAdministrationDivisionViewControl } from './views/buss_mis/administration-division-manage/change-admin-division';
// import { DeviceViewControl } from './views/buss_mis/device/index';
// import { DeviceLogViewControl } from './views/buss_mis/device/device-log';
// import { SocialGroupsTypeViewControl } from './views/buss_mis/social-groups-type-manage';
// import { ChangeSocialGroupsTypeViewControl } from './views/buss_mis/social-groups-type-manage/change-social-groups-type';
// import { MonitorViewControl } from './views/buss_mis/monitor/index';
// import { ChangeMonitorViewControl } from './views/buss_mis/monitor/change-monitor';
// import { ActivityRoomViewControl } from './views/buss_mis/activity-manage/activityRoom';
// import { ChangeActivityRoomViewControl } from './views/buss_mis/activity-manage/changeActivityRoom';
// import { ActivityRoomReservationViewControl } from './views/buss_mis/activity-manage/activityRoomReservation';
// import { RoomListControl } from './views/welfare-institution/room-change';
// import { RoomChangeViewControl } from './views/welfare-institution/room-change/change-room';
// import { subsidyLink, subsidyRouter } from './router/subsidy/index';
// // import { researchLink, researchRouter } from './router/research/index';
// import { IncidentalManageViewControl } from './views/welfare-institution/cost-manage/incidental-index.';
// import { ChangeIncidentalViewControl } from './views/welfare-institution/cost-manage/change-incidental';
// import { FoodCostManageViewControl } from './views/welfare-institution/cost-manage/food-cost-index';
// import { FoodCostViewControl } from './views/welfare-institution/cost-manage/change-food-cost';
// import { MedicalCostManageViewControl } from './views/welfare-institution/cost-manage/medical-cost-index';
// import { MedicalCostViewControl } from './views/welfare-institution/cost-manage/change-medical-cost';
// import { DiaperCostManageViewControl } from './views/welfare-institution/cost-manage/diaper-cost-index';
// import { DiaperCostViewControl } from './views/welfare-institution/cost-manage/change-diaper-cost';
// import { IPersonOrgManageService } from './models/person-org-manage';
// // import { nursingContentControl } from './views/buss_mis/nursing-content';
// import { nursingContentService } from './models/nursing-content';
// import { addNursingControl } from './views/buss_mis/add-nursing';
// import { nursingTemplateControl } from './views/buss_mis/nursing-template';
// import { nursingTemplateService } from './models/nursing-template';
// import { addNursingTemplateControl } from './views/buss_mis/add-nursing-template';
// import { nursingRecordControl } from './views/buss_mis/nursing-record';
// import { addNursingRecordControl } from './views/buss_mis/add-nursing-record';
// import { CharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/index';
// import { ChangeCharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/change-charitable-project';
// import { CharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/index';
// import { ChangeCharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/change-charitable-donate';
// import { VolunteerServiceCategoryViewControl } from './views/buss_mis/charitable/volunteer-service-category';
// import { ChangeVolunteerServiceCategoryViewControl } from './views/buss_mis/charitable/volunteer-service-category/change-volunteer-service-category';
// import { VolunteerApplyViewControl } from './views/buss_mis/charitable/volunteer-apply';
// import { VolunteerApplyListViewControl } from './views/buss_mis/charitable/volunteer-apply/volunteer-apply-list';
// import { ViewCharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/view-charitable-donate';
// import { CharitableAppropriationViewControl } from './views/buss_mis/charitable/charitable-appropriation/';
// import { ChangeCharitableAppropriationViewControl } from './views/buss_mis/charitable/charitable-appropriation/change-charitable-appropriation';
// import { CharitableProjectListViewControl } from './views/buss_mis/charitable/charitable-project/charitable-project-list';
// import { ViewCharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/view-charitable-project';
// import { RecipientApplyViewControl } from './views/buss_mis/charitable/recipient-apply/recipient-apply';
// import { RecipientApplyListViewControl } from './views/buss_mis/charitable/recipient-apply/recipient-apply-list';
// import { OrganizationApplyViewControl } from './views/buss_mis/charitable/recipient-apply/organization-apply';
// import { OrganizationApplyListViewControl } from './views/buss_mis/charitable/recipient-apply/organization-apply-list';
// import { OperationStatusViewControl } from './views/welfare-institution/report-manage/operation-status';
// import { BasicFacilitiesViewControl } from './views/welfare-institution/report-manage/basic-facilities';
// import { AdoptionSituationViewControl } from './views/welfare-institution/report-manage/adoption-situation';
// import { FireQualificationViewControl } from './views/welfare-institution/report-manage/fire-qualification';
// import { WorkerConstituteViewControl } from './views/welfare-institution/report-manage/worker-constitute';
// import { CompensationViewControl } from './views/welfare-institution/report-manage/compensation';
// import { OrganizationReportViewControl } from './views/welfare-institution/report-manage/organization-report';
// import { CharitableInformationViewControl } from './views/buss_mis/charitable/charitable-information';
// import { ActivityInformationViewControl } from './views/buss_mis/activity-manage/activityInformation';
// import { PlatformSettlementViewControl } from './views/buss_mis/financial-manage/platform-settlement/index';
// import { ServiceProviderSettlementsViewControl } from './views/buss_mis/financial-manage/service-provider-settlement/index';
// import { ServicePersonnelSettlementViewControl } from './views/buss_mis/financial-manage/service-personnel-settlement/index';
// import { CharitySettlementViewControl } from './views/buss_mis/financial-manage/charity-settlement/index';
// import { staffStatisticsViewControl } from './views/welfare-institution/report-manage/staff-statistics';
// import { elderlyResidentsViewControl } from './views/welfare-institution/report-manage/elderly-residents';
// import { CharitableSocietyViewControl } from './views/buss_mis/charitable/charitable-society';
// import { elderCheckViewControl } from './views/welfare-institution/report-manage/elder-check';
// import { happyStaffStatisticsViewControl } from './views/welfare-institution/report-manage/happy-staff-statistics';
// import { ChangeCharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund/change-title-fund';
// import { CharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund';
// import { incomeAssessedSettlementViewControl } from './views/buss_mis/financial-manage/income-assessed-settlement/income-assessed-settlement';
// import { ChangeSquenceControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-squence';
// import { subsidyServiceProviderViewControl } from './views/buss_mis/financial-manage/income-assessed-settlement/subsidy-service-provider';
// import { ViewRecipientApplyViewControl } from './views/buss_mis/charitable/recipient-apply/view-recipient-apply-list';
// import { ChangeHydropowerJjViewControl } from './views/welfare-institution/hotel/hotel-hydropower/change-hydropower-jiujiang';
// import { HydropowerJjViewControl } from './views/welfare-institution/hotel/hotel-hydropower/index-jiujiang';
// import { SubsidyAutoRechargeViewControl } from './views/buss_pub/subsidy-auto-recharge-manage';
// import { ServicePersonReviewedViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/apply-reviewed';
// import { ServiceProviderReviewedViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/apply-reviewed';
// import { ViewCharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund/view-charitable-title-fund-list';
// import { BuyServiceViewControl } from './views/buss_pub/buy-service/index';
// import { SysbbViewControl } from './views/buss_mis/sysbb';
// const blank = new BlankMainFormControl();
// const layoutMain = 'layoutMainForm';
// const manage_logo = require("../static/img/logo_sys.png");
// const backstage_nain_form_logo = require("../static/img/logo_sys.png");

// // 主窗体
// const layoutMainForm = new BackstageManageMainFormControl(
//     "智慧养老",
//     [
//         {
//             title: '首页', icon: 'notification', link: '', key: 0, permission: "8", childrenComponent: [
//                 { key: 'home', link: ROUTE_PATH.home, title: '首页', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "长者信息管理系统", icon: 'notification', link: "", key: 1, permission: "8", childrenComponent: [
//                 { key: 's11', link: ROUTE_PATH.sysbb, title: '长者类型', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's12', link: ROUTE_PATH.elderInfo, title: '长者信息管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's13', link: ROUTE_PATH.elderInfo1, title: '长者资料维护', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's14', link: ROUTE_PATH.elderInfo2, title: '长者档案管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's15', link: ROUTE_PATH.changeElderInfo, title: '长者档案维护', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's16', link: ROUTE_PATH.elderInfo3, title: '长者档案展示', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's17', link: ROUTE_PATH.elderInfo4, title: '长者资料查询', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "服务订单系统", icon: 'notification', link: "", key: 2, permission: "8", childrenComponent: [
//                 { key: 's21', link: ROUTE_PATH.organization, title: '服务机构信息维护', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's23', link: ROUTE_PATH.assessmentProject, title: '评估项目设置', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's24', link: ROUTE_PATH.assessmentTemplate, title: '评估量表定义', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's25', link: ROUTE_PATH.servicesItemCategory, title: '服务类型', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's36', link: ROUTE_PATH.serviceProject, title: '服务项目', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's361', link: ROUTE_PATH.serviceOption, title: '服务选项', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's27', link: ROUTE_PATH.serviceItemPackage, title: '服务套餐', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's28', link: ROUTE_PATH.serviceItemPackage1, title: '评估类型服务项目设定', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's29', link: ROUTE_PATH.organization1, title: '服务商信息维护', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's30', link: ROUTE_PATH.incomeAssessedSettlement, title: '服务商结算', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's31', link: ROUTE_PATH.serviceProvider, title: '审核', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's32', link: ROUTE_PATH.taskToBeSend, title: '服务派工', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's33', link: ROUTE_PATH.serviceOrder1, title: '服务完成', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's34', link: ROUTE_PATH.serviceOrder, title: '订单统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's35', link: ROUTE_PATH.incomeAssessedSettlement1, title: 'APP结算', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's37', link: ROUTE_PATH.buyService, title: '服务购买', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "组织架构设置", icon: 'notification', link: "", key: 3, permission: "8", childrenComponent: [
//                 { key: 's41', link: ROUTE_PATH.organization2, title: '地址档案', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's42', link: ROUTE_PATH.organization3, title: '服务点信息', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's43', link: ROUTE_PATH.wokerPersonnel, title: '员工档案', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's44', link: ROUTE_PATH.wokerPersonnel1, title: '员工分组', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's45', link: ROUTE_PATH.organization4, title: '机构管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's46', link: ROUTE_PATH.personnel2, title: '人员管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's22', link: ROUTE_PATH.administrationDivision, title: '地区维护', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "民政管理系统", icon: 'notification', link: "", key: 4, permission: "8", childrenComponent: [
//                 { key: 's51', link: ROUTE_PATH.elderInfo5, title: '查看居家长者资料', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's52', link: ROUTE_PATH.organization5, title: '查看幸福院资料', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's53', link: ROUTE_PATH.organization6, title: '查看机构资料', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's54', link: ROUTE_PATH.sysbb1, title: '查看居家长者数据汇总', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's55', link: ROUTE_PATH.sysbb2, title: '查看幸福院数据汇总', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 's551', link: ROUTE_PATH.sysbb3, title: '查看机构院数据汇总', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 's56', link: ROUTE_PATH.organization7, title: '服务商资料管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 's57', link: ROUTE_PATH.serviceProviderSettlementLook1, title: '服务商结算', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' }
//             ]
//         },
//         {
//             title: "养老院综合服务", icon: 'notification', link: "", key: 1, permission: "8", childrenComponent: [
//                 // { key: 'reservation', link: ROUTE_PATH.reservationRegistration, title: '预约登记', permission: { 'permission': '预约登记查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'reservation', link: ROUTE_PATH.reservationRegistrationLook, title: '预约登记', permission: { 'permission': '查看预约登记查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplate, title: '评估模板', permission: { 'permission': '评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplateLook, title: '评估模板', permission: { 'permission': '查看评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'assessmentProject', link: ROUTE_PATH.assessmentProject, title: '评估项目', permission: { 'permission': '评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'assessmentProject', link: ROUTE_PATH.assessmentProjectLook, title: '评估项目', permission: { 'permission': '查看评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'competenceAssessmentList', link: ROUTE_PATH.competenceAssessmentList, title: '评估记录', permission: { 'permission': '评估记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'competenceAssessmentList', link: ROUTE_PATH.competenceAssessmentListLook, title: '评估记录', permission: { 'permission': '查看评估记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'checkIn', link: ROUTE_PATH.checkIn, title: '长者入住', permission: { 'permission': '长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'checkInJj', link: ROUTE_PATH.checkInJj, title: '入住办理', permission: { 'permission': '长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'checkInJjLook', link: ROUTE_PATH.checkInJjLook, title: '入住办理', permission: { 'permission': '查看长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'checkIn', link: ROUTE_PATH.checkInLook, title: '长者入住', permission: { 'permission': '查看长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'roomStatus', link: ROUTE_PATH.roomStatus, title: '房态图', permission: { 'permission': '房态图查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.hydropower, title: '水电抄表', permission: { 'permission': '水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.hydropowerLook, title: '水电抄表', permission: { 'permission': '查看水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.hydropowerJj, title: '九江水电抄表', permission: { 'permission': '九江水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.hydropowerJjLook, title: '九江水电抄表', permission: { 'permission': '查看九江水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'leaveReason', link: ROUTE_PATH.leaveReason, title: '离开原因', permission: { 'permission': '离开原因查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'researchInsert', link: ROUTE_PATH.leaveRecord, title: '住宿记录', permission: { 'permission': '住宿记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'seeresearchInsert', link: ROUTE_PATH.leaveRecordLook, title: '查看住宿记录', permission: { 'permission': '查看住宿记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'hotelZoneType', link: ROUTE_PATH.hotelZoneType, title: '区域类型', permission: { 'permission': '区域类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '区域管理', permission: { 'permission': '区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'hotelZone', link: ROUTE_PATH.hotelZoneLook, title: '区域管理', permission: { 'permission': '查看区域情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.bedManage, title: '床位管理', permission: { 'permission': '床位管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.bedManageLook, title: '床位管理', permission: { 'permission': '查看床位情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.costAccountingLook, title: '查看住宿核算', permission: { 'permission': '住宿核算查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'bed', link: ROUTE_PATH.costAccounting, title: '住宿核算', permission: { 'permission': '住宿核算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'roomChange', link: ROUTE_PATH.roomList, title: '房间调整', permission: { 'permission': '房间调整查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'incidental', link: ROUTE_PATH.incidentalList, title: '杂费管理', permission: { 'permission': '杂费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'incidentalLook', link: ROUTE_PATH.incidentalListLook, title: '杂费管理', permission: { 'permission': '查看杂费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'foodCost', link: ROUTE_PATH.foodCostList, title: '餐费管理', permission: { 'permission': '餐费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'foodCostLook', link: ROUTE_PATH.foodCostListLook, title: '餐费管理', permission: { 'permission': '查看餐费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'medicalCost', link: ROUTE_PATH.medicalCost, title: '医疗费用管理', permission: { 'permission': '医疗费用管理' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'diaperCost', link: ROUTE_PATH.diaperCost, title: '尿片费用管理', permission: { 'permission': '尿片费用管理' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "基础档案", icon: 'notification', link: "", key: 2, permission: "8", childrenComponent: [
//                 // { key: 'elderInfo', link: ROUTE_PATH.elderInfo, title: '长者资料', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'elderInfo', link: ROUTE_PATH.elderInfoLook, title: '长者资料', permission: { 'permission': '查看长者资料查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'elderInfo', link: ROUTE_PATH.orgElderInfo, title: '长者资料', permission: { 'permission': '机构长者管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'personnel', link: ROUTE_PATH.personnel, title: '人员管理', permission: { 'permission': '人员管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // // { key: 'personnel', link: ROUTE_PATH.personnel, title: '用户管理', permission: { 'permission': '用户管理查看' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnel, title: '工作人员管理', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'organization', link: ROUTE_PATH.organization, title: '机构信息管理', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'organization', link: ROUTE_PATH.organizationLook, title: '机构信息管理', permission: { 'permission': '查看组织机构查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // // { key: 'behavioralCompetenceAssessment', link: ROUTE_PATH.behavioralCompetenceAssessment, title: '行为能力评估', permission: PermissionList.RolePermission_Select, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'administrationDivision', link: ROUTE_PATH.administrationDivision, title: '行政区划管理', permission: { 'permission': '行政区划管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'administrationDivision', link: ROUTE_PATH.administrationDivisionLook, title: '行政区划管理', permission: { 'permission': '查看行政区划查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'businessArea', link: ROUTE_PATH.businessArea, title: '业务区域管理', permission: { 'permission': '业务区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'businessArea', link: ROUTE_PATH.businessArea, title: '业务区域管理', permission: { 'permission': '查看业务区域查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'device', link: ROUTE_PATH.Device, title: '设备管理', permission: { 'permission': '设备管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'device', link: ROUTE_PATH.DeviceLook, title: '设备管理', permission: { 'permission': '查看设备查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'monitor', link: ROUTE_PATH.Monitor, title: '监控管理', permission: { 'permission': '监控管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'monitor', link: ROUTE_PATH.MonitorLook, title: '监控管理', permission: { 'permission': '查看监控信息查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'socialGroupsType', link: ROUTE_PATH.socialGroupsType, title: '社会群体类型管理', permission: { 'permission': '社会群体类型管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'socialGroupsType', link: ROUTE_PATH.socialGroupsTypeLook, title: '社会群体类型管理', permission: { 'permission': '查看社会群体类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "护理管理", icon: 'notification', link: "", key: 3, permission: "8", childrenComponent: [
//                 // { key: 'nursingContent', link: ROUTE_PATH.assessmentProjectNursing, title: '护理内容', permission: { 'permission': '护理项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'nursingContentLook', link: ROUTE_PATH.assessmentProjectNursingLook, title: '护理内容', permission: { 'permission': '查看护理项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'nursingTemplate', link: ROUTE_PATH.assessmentTemplateLookNursing, title: '护理模板', permission: { 'permission': '护理模板查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'nursingTemplate', link: ROUTE_PATH.nursingRecord, title: '护理记录', permission: { 'permission': '护理记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "服务运营", icon: 'notification', link: "", key: 4, permission: "8", childrenComponent: [
//                 // { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '服务订单', permission: { 'permission': '服务订单查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceRecord', link: ROUTE_PATH.serviceRecord, title: '服务记录', permission: { 'permission': '服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceRecord', link: ROUTE_PATH.serviceRecordLook, title: '服务记录', permission: { 'permission': '查看服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'transaction', link: ROUTE_PATH.transaction, title: '交易管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'transactioncomment', link: ROUTE_PATH.transactionComment, title: '信用记录', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务项目类别', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategoryLook, title: '服务项目类别', permission: { 'permission': '查看服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProject', link: ROUTE_PATH.serviceProject, title: '服务项目', permission: { 'permission': '服务项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProject', link: ROUTE_PATH.serviceProjectLook, title: '服务项目', permission: { 'permission': '查看服务项目' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageLook, title: '服务产品', permission: { 'permission': '查看服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceScope', link: ROUTE_PATH.serviceScope, title: '服务适用范围', permission: { 'permission': '服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceScope', link: ROUTE_PATH.serviceScopeLook, title: '服务适用范围', permission: { 'permission': '查看服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceOption', link: ROUTE_PATH.serviceOption, title: '服务选项', permission: { 'permission': '服务选项查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceOption', link: ROUTE_PATH.serviceOptionLook, title: '服务选项', permission: { 'permission': '查看服务选项' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProvider', link: ROUTE_PATH.serviceProvider, title: '服务商登记', permission: { 'permission': '服务商登记新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProviderApply', link: ROUTE_PATH.serviceProviderApply, title: '服务商申请', permission: { 'permission': '服务商申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicePersonalApply', link: ROUTE_PATH.servicePersonalApply, title: '服务人员申请', permission: { 'permission': '服务人员申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicePersonal', link: ROUTE_PATH.servicePersonalManage, title: '服务人员登记', permission: { 'permission': '服务人员登记查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlement, title: '服务商结算', permission: { 'permission': '服务商结算查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlementLook, title: '服务商结算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlement, title: '服务人员结算', permission: { 'permission': '服务人员结算查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlementLook, title: '服务人员结算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务订单分派', permission: { 'permission': '服务订单分派查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTaskLook, title: '服务订单分派', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceWoker', link: ROUTE_PATH.serviceWoker, title: '服务提供者登记信息', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         // ...billLink,
//         {
//             title: "财务系统", icon: 'notification', link: "", key: 5, permission: "8", childrenComponent: [
//                 // { key: 'changeCollectorCharge', link: ROUTE_PATH.changeCollectorCharge, title: '收费员收费', permission: { 'permission': '收费员收费查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'changeCollectorCharge', link: ROUTE_PATH.changeCollectorCharge, title: '收费员收费', permission: { 'permission': '收费员收费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'changeCollectorPayment', link: ROUTE_PATH.changeCollectorPayment, title: '收费员退款', permission: { 'permission': '收费员退款查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'changeCollectorPayment', link: ROUTE_PATH.changeCollectorPayment, title: '收费员退款', permission: { 'permission': '收费员退款管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecordLook, title: '查看收费记录', permission: { 'permission': '查看收费记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecord, title: '查看收费记录', permission: { 'permission': '收费记录管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliation, title: '打印/导出银行扣款单', permission: { 'permission': '打印/导出银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliationManage, title: '打印/导出银行扣款单', permission: { 'permission': '打印/导出银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliation, title: '非银行扣款单', permission: { 'permission': '非银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliationLook, title: '非银行扣款单', permission: { 'permission': '查看非银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialReconciliationRecord', link: ROUTE_PATH.financialReconciliationRecord, title: '查看银行扣款记录', permission: { 'permission': '银行扣款记录管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialReconciliationRecord', link: ROUTE_PATH.financialReconciliationRecordLook, title: '查看银行扣款记录', permission: { 'permission': '查看银行扣款记录查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialAccountFlow', link: ROUTE_PATH.financialAccountFlow, title: '账户流水', permission: { 'permission': '账户流水查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialAccountFlow', link: ROUTE_PATH.financialAccountFlow, title: '账户流水', permission: { 'permission': '账户流水管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'financialAccountReceive', link: ROUTE_PATH.changereceive, title: '收款', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'financialAccountPayment', link: ROUTE_PATH.changepayment, title: '付款', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'financialAccountTransfer', link: ROUTE_PATH.changetransfer, title: '转账', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'financialAccountBook', link: ROUTE_PATH.financialAccountBook, title: '账簿管理', permission: { 'permission': '账簿管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'financialAccountBook', link: ROUTE_PATH.financialAccountBook, title: '账簿管理', permission: { 'permission': '全部账薄管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialAccountManage', link: ROUTE_PATH.financialAccountManage, title: '账户管理', permission: { 'permission': '全部账薄管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialVoucher', link: ROUTE_PATH.financialAccountVoucherManage, title: '记账凭证', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'financialSubject', link: ROUTE_PATH.financialSubjectManage, title: '会计科目', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'platformSettlement', link: ROUTE_PATH.platformSettlement, title: '结算报表', permission: { 'permission': '平台结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceProviderSettlements', link: ROUTE_PATH.serviceProviderSettlements, title: '服务商结算报表', permission: { 'permission': '服务商结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'servicePersonnelSettlement', link: ROUTE_PATH.servicePersonnelSettlement, title: '服务人员结算报表', permission: { 'permission': '服务人员结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitySettlement', link: ROUTE_PATH.charitySettlement, title: '慈善结算报表', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'incomeAssessedSettlement', link: ROUTE_PATH.incomeAssessedSettlement, title: '收入分摊报表', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'subsidyServiceProvider', link: ROUTE_PATH.subsidyServiceProvider, title: '服务商补贴收入', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "医院管理", icon: 'notification', link: "", key: 6, permission: "8", childrenComponent: [
//                 // { key: 'hospitalArrears', link: ROUTE_PATH.hospitalArrears, title: '医疗欠费', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' }
//             ]
//         },
//         {
//             title: "活动管理", icon: 'notification', link: "", key: 7, permission: "8", childrenComponent: [
//                 // { key: 'activityTypeManage', link: ROUTE_PATH.activityTypeManage, title: '活动类型', permission: { 'permission': '活动类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityTypeManage', link: ROUTE_PATH.activityTypeManageLook, title: '活动类型', permission: { 'permission': '查看活动类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityManage', link: ROUTE_PATH.activityManage, title: '活动列表', permission: { 'permission': '活动列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityManage', link: ROUTE_PATH.activityManageLook, title: '活动列表', permission: { 'permission': '查看活动列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityParticipate', link: ROUTE_PATH.activityParticipate, title: '报名列表', permission: { 'permission': '报名列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityParticipate', link: ROUTE_PATH.activityParticipateLook, title: '报名列表', permission: { 'permission': '查看报名列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activitySignIn', link: ROUTE_PATH.activitySignIn, title: '签到列表', permission: { 'permission': '签到列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activitySignIn', link: ROUTE_PATH.activitySignInLook, title: '签到列表', permission: { 'permission': '查看签到列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityRoom', link: ROUTE_PATH.activityRoom, title: '活动室列表', permission: { 'permission': '活动室列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityRoom', link: ROUTE_PATH.activityRoomLook, title: '活动室列表', permission: { 'permission': '查看活动室列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityRoomReservation', link: ROUTE_PATH.activityRoomReservation, title: '活动室预约列表', permission: { 'permission': '活动室预约列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityInformation', link: ROUTE_PATH.activityInformation, title: '活动信息汇总', permission: { 'permission': '活动信息查询' }, icon: 'font@pao_font_icon_user_bright' }
//             ]
//         },
//         {
//             title: "任务管理", icon: 'notification', link: "", key: 8, permission: "8", childrenComponent: [
//                 // { key: 'taskList', link: ROUTE_PATH.taskList, title: '任务列表', permission: { 'permission': '任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskList', link: ROUTE_PATH.taskListLook, title: '任务列表', permission: { 'permission': '查看任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskCreate', link: ROUTE_PATH.changeTask, title: '创建任务', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskTypeList', link: ROUTE_PATH.taskTypeList, title: '任务类型', permission: { 'permission': '任务类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskTypeList', link: ROUTE_PATH.taskTypeListLook, title: '任务类型', permission: { 'permission': '查看任务类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskUnassignList', link: ROUTE_PATH.taskToBeProcess, title: '审核任务列表', permission: { 'permission': '审核任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'taskUnexamineList', link: ROUTE_PATH.taskToBeSend, title: '分派任务列表', permission: { 'permission': '分派任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "信息管理", icon: 'notification', link: "", key: 9, permission: "8", childrenComponent: [
//                 // { key: 'articleType', link: ROUTE_PATH.articleType, title: '文章类型', permission: { 'permission': '文章类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'articleType', link: ROUTE_PATH.articleTypeLook, title: '查看文章类型', permission: { 'permission': '查看文章类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentType', link: ROUTE_PATH.commentType, title: '评论类型', permission: { 'permission': '评论类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentType', link: ROUTE_PATH.commentTypeLook, title: '评论类型', permission: { 'permission': '查看评论类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'announcementList', link: ROUTE_PATH.announcementList, title: '公告管理', permission: { 'permission': '公告管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'announcementList', link: ROUTE_PATH.announcementListLook, title: '公告管理', permission: { 'permission': '查看公告列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'NewsList', link: ROUTE_PATH.newsList, title: '新闻管理', permission: { 'permission': '新闻管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'NewsList', link: ROUTE_PATH.newsListLook, title: '新闻管理', permission: { 'permission': '查看新闻列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentAudit', link: ROUTE_PATH.commentAudit, title: '评论审核', permission: { 'permission': '评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentAudit', link: ROUTE_PATH.commentAuditLook, title: '评论审核', permission: { 'permission': '查看评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'articleAudit', link: ROUTE_PATH.articleAudit, title: '文章审核', permission: { 'permission': '文章审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'articleAudit', link: ROUTE_PATH.articleAuditLook, title: '文章审核', permission: { 'permission': '查看文章审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentList', link: ROUTE_PATH.commentList, title: '评论管理', permission: { 'permission': '评论管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'commentList', link: ROUTE_PATH.commentListLook, title: '查看评论管理', permission: { 'permission': '查看评论管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "补贴管理", icon: 'notification', link: "", key: 10, permission: "8", childrenComponent: [
//                 // ...subsidyLink
//             ]
//         },
//         {
//             title: "调研系统", icon: 'notification', link: "", key: 11, permission: "8", childrenComponent: [
//                 // ...researchLink
//             ]
//         },
//         {
//             title: "服务套餐", icon: 'notification', link: "", key: 12, permission: "8", childrenComponent: [
//                 // { key: 'serviceProjectindex', link: ROUTE_PATH.serviceProjectindex, title: '服务套餐列表', permission: { 'permission': '服务套餐列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "慈善管理", icon: 'notification', link: "", key: 13, permission: "8", childrenComponent: [
//                 // // { key: 'charitableProject', link: ROUTE_PATH.charitableProject, title: '慈善项目管理', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'changeCharitableTitleFund', link: ROUTE_PATH.changeCharitableTitleFund, title: '申请冠名基金', permission: { 'permission': '冠名基金新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableTitleFund', link: ROUTE_PATH.charitableTitleFund, title: '冠名基金审核', permission: { 'permission': '冠名基金查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'viewCharitableTitleFund', link: ROUTE_PATH.viewCharitableTitleFund, title: '冠名基金申请记录', permission: { 'permission': '冠名基金查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'changeCharitableDonate', link: ROUTE_PATH.changeCharitableDonate, title: '我要捐赠', permission: { 'permission': '慈善捐赠新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableDonate', link: ROUTE_PATH.charitableDonate, title: '捐赠信息明细', permission: { 'permission': '慈善捐赠查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'charitableDonateList', link: ROUTE_PATH.charitableDonateApproval, title: '匹配捐款', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'recipientApply', link: ROUTE_PATH.recipientApply, title: '个人募捐申请', permission: { 'permission': '募捐申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'organizationApply', link: ROUTE_PATH.organizationApply, title: '机构募捐申请', permission: { 'permission': '募捐申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'recipientApplyList', link: ROUTE_PATH.recipientApplyList, title: '募捐申请审核', permission: { 'permission': '募捐申请查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'viewRecipientApplyList', link: ROUTE_PATH.viewRecipientApplyList, title: '募捐申请列表', permission: { 'permission': '募捐申请查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'charitableAppropriation', link: ROUTE_PATH.charitableAppropriation, title: '审核拨款', permission: { 'permission': '慈善拨款查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableInformation', link: ROUTE_PATH.charitableInformation, title: '慈善汇总', permission: { 'permission': '慈善信息查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableSociety', link: ROUTE_PATH.charitableSociety, title: '慈善会资料', permission: { 'permission': '慈善会资料查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'volunteerApply', link: ROUTE_PATH.volunteerApply, title: '志愿者申请', permission: { 'permission': '志愿者申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'volunteerApplyList', link: ROUTE_PATH.volunteerApplyList, title: '志愿者列表', permission: { 'permission': '志愿者申请查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // // { key: 'volunteerServiceCategory', link: ROUTE_PATH.volunteerServiceCategory, title: '志愿者服务类别', permission: { 'permission': '志愿者服务类别查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "报表管理", icon: 'notification', link: "", key: 14, permission: "8", childrenComponent: [
//                 // { key: 'operationStatus', link: ROUTE_PATH.operationStatus, title: '机构运营情况', permission: { 'permission': '机构运营情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'basicFacilities', link: ROUTE_PATH.basicFacilities, title: '机构基本设施情况', permission: { 'permission': '基本设施情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'adoptionSituation', link: ROUTE_PATH.adoptionSituation, title: '机构收养人员情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification, title: '机构消防资质情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'workerConstitute', link: ROUTE_PATH.workerConstitute, title: '机构职工构成情况', permission: { 'permission': '职工构成情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'compensation', link: ROUTE_PATH.compensation, title: '机构薪酬待遇情况', permission: { 'permission': '薪酬待遇情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'compensation', link: ROUTE_PATH.organizationReport, title: '机构报表情况', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'compensation', link: ROUTE_PATH.staffStatistics, title: '福利院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'compensation', link: ROUTE_PATH.happyStaffStatistics, title: '幸福院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'compensation', link: ROUTE_PATH.elderCheck, title: '福利院长者入住统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "系统管理", icon: 'notification', link: "", key: 33, permission: "8", childrenComponent: [
//                 // { key: 'userManage', link: ROUTE_PATH.userManage, title: '用户管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'rolePermission', link: ROUTE_PATH.rolePermission, title: '角色设置', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'rolePermissionLook', link: ROUTE_PATH.rolePermissionLook, title: '角色设置', permission: { 'permission': '角色管理查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 // { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
//                 // { key: 'billSignature', link: ROUTE_PATH.billSignature, title: '单据签核', permission: { 'permission': '单据签核查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'initiatorBill', link: ROUTE_PATH.initiatorBill, title: '单据查询', permission: { 'permission': '单据查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'subsidyAutoRecharge', link: ROUTE_PATH.subsidyAutoRecharge, title: '补贴账户充值', permission: { 'permission': '单据签核查询' }, icon: 'font@pao_font_icon_user_bright' }
//             ]
//         },
//         {
//             title: "幸福院管理系统", icon: 'notification', link: "", key: 38, permission: "8", childrenComponent: [
//                 { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '楼宇信息', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'hotelZone', link: ROUTE_PATH.hotelZoneLook, title: '楼宇信息', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_zzbb', link: ROUTE_PATH.fireQualification_zzbb, title: '长者统计报表', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '日托订单', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'activityRoom', link: ROUTE_PATH.activityRoom, title: '活动室管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityRoom', link: ROUTE_PATH.activityRoomLook, title: '活动室管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'activityRoomReservation', link: ROUTE_PATH.activityRoomReservation, title: '活动室预约', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'activityManage', link: ROUTE_PATH.activityManage, title: '活动发布', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityManage', link: ROUTE_PATH.activityManageLook, title: '活动发布', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'activityParticipate', link: ROUTE_PATH.activityParticipate, title: '活动报名', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activityParticipate', link: ROUTE_PATH.activityParticipateLook, title: '活动报名', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'activitySignIn', link: ROUTE_PATH.activitySignIn, title: '活动签到', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'activitySignIn', link: ROUTE_PATH.activitySignInLook, title: '活动签到', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'personnel_zyz', link: ROUTE_PATH.personnel_zyz, title: '志愿者管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 { key: 'personnel_yg', link: ROUTE_PATH.personnel_yg, title: '义工档案', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 { key: 'adoptionSituation_zzsj', link: ROUTE_PATH.fireQualification_zzsj, title: '长者数据统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_rtdd', link: ROUTE_PATH.fireQualification_rtdd, title: '日托订单统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_rtzf', link: ROUTE_PATH.fireQualification_rtzf, title: '日托支付统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_hdbm', link: ROUTE_PATH.fireQualification_hdbm, title: '活动报名统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_nntj', link: ROUTE_PATH.fireQualification_nntj, title: '年龄统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_ysr', link: ROUTE_PATH.fireQualification_ysr, title: '月收入统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation_hdbg', link: ROUTE_PATH.fireQualification_hdbg, title: '活动报告', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "养老机构管理系统", icon: 'notification', link: "", key: 39, permission: "8", childrenComponent: [
//                 { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '楼宇信息', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'bed', link: ROUTE_PATH.bedManage, title: '床位信息', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'roomStatus', link: ROUTE_PATH.roomStatus, title: '套房设置', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'roomStatus', link: ROUTE_PATH.roomStatusexcel, title: '入住全景', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'roomStatus', link: ROUTE_PATH.changeReservationRegistration, title: '在线预约排队', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'checkInJj', link: ROUTE_PATH.checkInJj, title: '入住', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'checkIn', link: ROUTE_PATH.checkIn, title: '入住确认', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'researchInsert', link: ROUTE_PATH.leaveRecord, title: '请假销假', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'roomStatus', link: ROUTE_PATH.roomStatusexceltow, title: '换床', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'roomStatus', link: ROUTE_PATH.roomStatusexcelthree, title: '退住', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'nursingTemplate', link: ROUTE_PATH.nursingRecord, title: '护理日志', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '订单管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '收费项目', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'changeCollectorCharge', link: ROUTE_PATH.changeCollectorCharge, title: '预约收费', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliationManage, title: '护理收费', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'wokerPersonnel', link: ROUTE_PATH.personnel_yg, title: '义工档案', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 { key: 'wokerPersonnel', link: ROUTE_PATH.personnel_ygtow, title: '志愿者管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification, title: '基础数据统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification2, title: '年龄统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification3, title: '照护级别统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification4, title: '运营情况统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification5, title: '欠费一览表', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification6, title: '收费减免统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification7, title: '长者数量统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification8, title: '健康数据统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification9, title: '巡检评估统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification10, title: '活动报名统计', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//         {
//             title: "平台管理", icon: 'notification', link: "", key: 33, permission: "8", childrenComponent: [
//                 { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '居家养老服务管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageLook, title: '居家养老服务管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'personnel', link: ROUTE_PATH.personnel, title: '基础档案管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_role_bright' },
//                 { key: 'device', link: ROUTE_PATH.Device, title: '设备管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'device', link: ROUTE_PATH.DeviceLook, title: '设备管理', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 ...subsidyLink,
//                 // { key: 'charitableProject', link: ROUTE_PATH.charitableProject, title: '慈善项目管理', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'changeCharitableTitleFund', link: ROUTE_PATH.changeCharitableTitleFund, title: '申请冠名基金', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'charitableTitleFund', link: ROUTE_PATH.charitableTitleFund, title: '冠名基金审核', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'viewCharitableTitleFund', link: ROUTE_PATH.viewCharitableTitleFund, title: '冠名基金申请记录', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'changeCharitableDonate', link: ROUTE_PATH.changeCharitableDonate, title: '我要捐赠', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'charitableDonate', link: ROUTE_PATH.charitableDonate, title: '捐赠信息明细', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableDonateList', link: ROUTE_PATH.charitableDonateApproval, title: '匹配捐款', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'recipientApply', link: ROUTE_PATH.recipientApply, title: '个人募捐申请', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'organizationApply', link: ROUTE_PATH.organizationApply, title: '机构募捐申请', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'recipientApplyList', link: ROUTE_PATH.recipientApplyList, title: '募捐申请审核', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'viewRecipientApplyList', link: ROUTE_PATH.viewRecipientApplyList, title: '募捐申请列表', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'charitableAppropriation', link: ROUTE_PATH.charitableAppropriation, title: '审核拨款', permission: { 'permission': '慈善拨款查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'charitableInformation', link: ROUTE_PATH.charitableInformation, title: '慈善汇总', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 { key: 'charitableSociety', link: ROUTE_PATH.charitableSociety, title: '慈善会资料', permission: { 'permission': '双鸭山账号查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'volunteerApply', link: ROUTE_PATH.volunteerApply, title: '志愿者申请', permission: { 'permission': '志愿者申请新增' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'volunteerApplyList', link: ROUTE_PATH.volunteerApplyList, title: '志愿者列表', permission: { 'permission': '志愿者申请查询' }, icon: 'font@pao_font_icon_user_bright' },
//                 // { key: 'volunteerServiceCategory', link: ROUTE_PATH.volunteerServiceCategory, title: '志愿者服务类别', permission: { 'permission': '志愿者服务类别查询' }, icon: 'font@pao_font_icon_user_bright' },
//             ]
//         },
//     ],
//     [
//         // { key: "scan", icon: "antd@scan", link: "/scan" }
//     ],
//     function () {
//         CookieUtil.remove(COOKIE_KEY_CURRENT_USER);
//         CookieUtil.remove(COOKIE_KEY_USER_ROLE);
//     },
//     '/login',
//     undefined,
//     (pathname: string) => selectedKeys(pathname),
//     AppServiceUtility.login_service,
//     { logo_path: backstage_nain_form_logo, history_path: '/home' }
// );

// /** 带头部窗体 */
// const HeadMainForm = new HeadMainFormControl(
//     // 'src/projects/industryInternetSign/img/logo.png',
//     manage_logo,
//     '智慧养老服务平台'
// );
// const headMainID = 'HeadMainForm';
// const blankID = 'blank';

// /** 权限列表 */
// const permission_list = [
//     { module: "SystemManage", module_name: "系统管理", per_name: "用户增删改查权限", permission: "UserManage_Select", permission_state: "default" },
//     { module: "SystemManage", module_name: "系统管理", per_name: "角色增删改查权限", permission: "RolePermission_Select", permission_state: "default" },
//     { module: "SystemManage", module_name: "系统管理", per_name: "安全设置增删改查权限", permission: "SecuritySettings_Select", permission_state: "default" },
// ];

// const router = new AppReactRouterControl(
//     {
//         [blankID]: blank,
//         [layoutMain]: layoutMainForm,
//         [headMainID]: HeadMainForm
//     },
//     [
//         {
//             path: '/',
//             exact: true,
//             redirect: '/login'
//         },
//         /** =================================服务项目包部分============================= */
//         {
//             path: `${ROUTE_PATH.changeServiceItemPackage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceItemPackageViewControl,
//                 {
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeServiceItemPackage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceItemPackageViewControl,
//                 {
//                 })
//         },
//         // 服务产品
//         {
//             path: ROUTE_PATH.serviceItemPackage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceItemPackageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_item_package_all',
//                     select_permission: '服务产品查看',
//                     edit_permission: '服务产品编辑',
//                     delete_permission: '服务产品删除',
//                     add_permission: '服务产品新增',
//                 }),
//         },
//         // 服务产品
//         {
//             path: ROUTE_PATH.serviceItemPackage1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceItemPackageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_item_package_all',
//                     select_permission: '服务产品查看',
//                     edit_permission: '服务产品编辑',
//                     delete_permission: '服务产品删除',
//                     add_permission: '服务产品新增',
//                 }),
//         },
//         // 查看服务产品
//         {
//             path: ROUTE_PATH.serviceItemPackageLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceItemPackageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_item_package_look',
//                     select_permission: '查看服务产品查看',
//                 }),
//         },
//         /** =================================交易管理部分=============================== */
//         {
//             path: `${ROUTE_PATH.changeBedManage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBedManageViewControl,
//                 {
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeBedManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBedManageViewControl,
//                 {
//                 })
//         },
//         {
//             path: ROUTE_PATH.bedManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BedManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '床位管理查看',
//                     edit_permission: '床位管理编辑',
//                     delete_permission: '床位管理删除',
//                     add_permission: '床位管理新增',
//                 }),
//         },
//         {
//             path: ROUTE_PATH.bedManageLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BedManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_look',
//                     select_permission: '查看床位情况查看',
//                 }),
//         },
//         {
//             path: `${ROUTE_PATH.changeTransaction}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTransactionViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.transaction,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TransactionManageViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 }),
//         },
//         {
//             path: ROUTE_PATH.transactionComment,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TransactionCommentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         /** =================================财务出纳部分=============================== */
//         {
//             path: `${ROUTE_PATH.changeFinancialReturn}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountReturnViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         }, {
//             path: ROUTE_PATH.changeFinancialReturn,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountReturnViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.financialAccountFlow,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialAccountFlowViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_account_flow_list_look',
//                     select_permission: '账户流水查看',
//                     edit_permission: '账户流水编辑',
//                     add_permission: '账户流水新增',
//                 }),
//         }, {
//             path: ROUTE_PATH.changereceive,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeReceiveViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: ROUTE_PATH.changepayment,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePaymentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: ROUTE_PATH.changetransfer,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTransferViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         /** =================================财务账簿账户管理部分=============================== */
//         {
//             path: ROUTE_PATH.financialAccountBook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialAccountBookViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_account_book_list_manage',
//                     select_permission: '账簿管理查看',
//                     edit_permission: '账簿管理编辑',
//                     add_permission: '账簿管理新增',
//                 }),
//         }, {
//             path: ROUTE_PATH.financialAccountManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 FinancialAccountViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_account_list_all',
//                     select_permission: '床位管理查看',
//                     edit_permission: '床位管理编辑',
//                     delete_permission: '床位管理删除',
//                     add_permission: '床位管理新增',
//                 }),
//         }, {
//             path: `${ROUTE_PATH.financialAccountManage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialAccountViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: `${ROUTE_PATH.changefinancialAccountBook}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountBookViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: ROUTE_PATH.changefinancialAccountBook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountBookViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: `${ROUTE_PATH.changefinancialAccount}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: ROUTE_PATH.changefinancialAccount,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         /** =========================记账凭证===================================== */
//         {
//             path: ROUTE_PATH.financialAccountVoucherManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialVoucherViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: `${ROUTE_PATH.changeFinancialVoucher}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountVoucherViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         {
//             path: ROUTE_PATH.changeFinancialVoucher,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAccountVoucherViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         /** =========================会计科目管理================================= */
//         {
//             path: ROUTE_PATH.financialSubjectManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialSubjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         }, {
//             path: `${ROUTE_PATH.changeFinancialSubject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeFinancialSubjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         {
//             path: ROUTE_PATH.changeFinancialSubject,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeFinancialSubjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }),
//         },
//         /** =========================服务运营管理部分============================== */
//         // 服务商登记
//         {
//             path: ROUTE_PATH.serviceProvider,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceProviderViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_Subsidy_service_provider_list_all',
//                     add_permission: '服务商登记新增',
//                     select_permission: '服务商登记查询',
//                     edit_permission: '服务商登记编辑',
//                 }),
//         },
//         // 服务商审核
//         {
//             path: `${ROUTE_PATH.ProviderApplyReviewed}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceProviderReviewedViewControl,
//                 {
//                 }),
//         },
//         // 服务商申请
//         {
//             path: ROUTE_PATH.serviceProviderApply,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ApplyServiceProviderViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     // 没有配服务权限，先还原
//                     // request_url: 'get_current_service_provider_add',
//                     request_url: 'get_current_service_provider',
//                     add_permission: '服务商申请新增',
//                 }),
//         },
//         {
//             path: `${ROUTE_PATH.changeServiceProvider}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceProviderViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeServiceProvider,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceProviderViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.platformSettlement,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PlatformSettlementViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.serviceProviderSettlements,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceProviderSettlementsViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.servicePersonnelSettlement,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServicePersonnelSettlementViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.charitySettlement,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CharitySettlementViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.incomeAssessedSettlement,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 incomeAssessedSettlementViewControl,
//                 {
//                     select_param: { organization_type: '服务商' },
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.incomeAssessedSettlement1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 incomeAssessedSettlementViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.subsidyServiceProvider,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 subsidyServiceProviderViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.serviceType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceTypeViewControl,
//                 {
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeServiceType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServiceTypeViewControl)
//         },
//         {
//             path: ROUTE_PATH.changeServiceType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServiceTypeViewControl)
//         },
//         {
//             path: ROUTE_PATH.serviceItem,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceItemViewControl,
//                 {}
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeServiceItem}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceItemViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.changeServiceItem,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceItemViewControl,
//                 {}
//             )
//         },
//         // 双鸭山报表
//         {
//             path: ROUTE_PATH.sysbb,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 SysbbViewControl,
//                 {
//                     select_param: { type: '长者类型' },
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.sysbb1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 SysbbViewControl,
//                 {
//                     select_param: { type: '居家长者' },
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.sysbb2,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 SysbbViewControl,
//                 {
//                     select_param: { type: '幸福院' },
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.sysbb3,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 SysbbViewControl,
//                 {
//                     select_param: { type: '机构' },
//                 }
//             )
//         },
//         // 服务订单
//         {
//             path: ROUTE_PATH.serviceOrder,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOrderViewControl,
//                 {
//                     request_url: 'get_service_order_list',
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     select_permission: '工作人员用户管理查看',
//                 }
//             )
//         },
//         // 服务订单完成
//         {
//             path: ROUTE_PATH.serviceOrder1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOrderViewControl,
//                 {
//                     request_url: 'get_service_order_list',
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     select_permission: '工作人员用户管理查看',
//                     select_param: { status: '已完成' },
//                 }
//             )
//         },
//         /** 新增/修改护理内容 */
//         {
//             path: `${ROUTE_PATH.addNursing}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingControl,
//                 {
//                     assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(nursingContentService, remote.url, "INursingService"),
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /** 新增/修改护理内容 */
//         {
//             path: ROUTE_PATH.addNursing,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingControl,
//                 {
//                     assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })

//         },
//         // 护理内容
//         {
//             path: ROUTE_PATH.assessmentProjectNursing,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_nursing_project_list',
//                     del_url: 'delete_nursing_project',
//                     select_permission: '护理项目查询',
//                     edit_permission: '护理项目编辑',
//                     delete_permission: '护理项目删除',
//                     add_permission: '护理项目新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.assessmentProjectNursingLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_nursing_project_list_look',
//                     select_permission: '查看护理项目查询'
//                 }
//             )
//         },
//         /** 护理模板列表 */
//         {
//             path: ROUTE_PATH.nursingTemplate,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 nursingTemplateControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_nursing_template_list',
//                     select_permission: '评估项目查看',
//                     edit_permission: '评估项目编辑',
//                     delete_permission: '评估项目删除',
//                     add_permission: '评估项目新增',
//                 }
//             )
//         },
//         /** 护理记录列表页 */
//         {
//             path: ROUTE_PATH.nursingRecord,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 nursingRecordControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_nursing_recode_list',
//                     select_permission: '评估项目查看',
//                     edit_permission: '评估项目编辑',
//                     delete_permission: '评估项目删除',
//                     add_permission: '评估项目新增',
//                 }
//             )
//         },
//         /** 新增/修改护理模板 */
//         {
//             path: `${ROUTE_PATH.addNursingTemplate}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingTemplateControl,
//                 {
//                     assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(nursingTemplateService, remote.url, "INursingTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         /** 新增/修改护理模板 */
//         {
//             path: ROUTE_PATH.addNursingTemplate,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingTemplateControl,
//                 {
//                     assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(nursingTemplateService, remote.url, "INursingTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         /** 新增/修改护理记录 */
//         {
//             path: `${ROUTE_PATH.addNursingRecord}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingRecordControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.addNursingRecord,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 addNursingRecordControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         // 服务订单分派
//         {
//             path: ROUTE_PATH.serviceOrderTask,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOrderTaskViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_order_task_list_all',
//                     select_permission: '服务订单分派查看',
//                     edit_permission: '服务订单分派编辑',
//                     delete_permission: '服务订单分派删除',
//                     add_permission: '服务订单分派新增',
//                 }
//             )
//         },
//         // 服务订单分派管理
//         {
//             path: ROUTE_PATH.serviceOrderTaskLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOrderTaskViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_order_task_list_look',
//                     select_permission: '服务订单分派管理查看',
//                     edit_permission: '服务订单分派管理编辑',
//                     delete_permission: '服务订单分派管理删除',
//                     add_permission: '服务订单分派管理新增',
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeServiceOrder}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceOrderViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.changeServiceOrder,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceOrderViewControl,
//                 {}
//             )
//         },
//         ...billRouter,
//         // 业务区域管理
//         {
//             path: ROUTE_PATH.businessArea,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BusinessAreaManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_business_area_list_all',
//                     select_permission: '行政区划管理查看',
//                     edit_permission: '行政区划管理编辑',
//                     delete_permission: '行政区划管理删除',
//                     add_permission: '行政区划管理新增',
//                 }),
//         },
//         // 查看业务区域
//         {
//             path: ROUTE_PATH.businessArea,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BusinessAreaManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_business_area_list_look_delete',
//                     select_permission: '查看行政区划查看',
//                 }),
//         },
//         {
//             path: `${ROUTE_PATH.changeBusinessArea}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBusinessAreaViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeBusinessArea,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBusinessAreaViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         // 行政区划管理
//         {
//             path: ROUTE_PATH.administrationDivision,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(AdministrationDivisionViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_admin_division_list',
//                 select_permission: '行政区划管理查看',
//                 edit_permission: '行政区划管理编辑',
//                 delete_permission: '行政区划管理删除',
//                 add_permission: '行政区划管理新增',
//             }),
//         },
//         // 查看行政区划
//         {
//             path: ROUTE_PATH.administrationDivisionLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(AdministrationDivisionViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_admin_division_list_look',
//                 select_permission: '查看行政区划查看',
//             }),
//         },
//         {
//             path: `${ROUTE_PATH.changeAdministrationDivision}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAdministrationDivisionViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeAdministrationDivision,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAdministrationDivisionViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.reservationRegistration,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ReservationRegistrationViewControl,
//                 {
//                     request_url: 'get_reservation_registration_list_all',
//                     get_permission_name: 'get_function_list',
//                     permission_class: AppServiceUtility.login_service,
//                     select_permission: '预约登记查看',
//                     edit_permission: '预约登记编辑',
//                     delete_permission: '预约登记删除',
//                     add_permission: '预约登记新增',
//                 }),
//         },
//         {
//             path: ROUTE_PATH.reservationRegistrationLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ReservationRegistrationViewControl,
//                 {
//                     request_url: 'get_reservation_registration_list_look',
//                     get_permission_name: 'get_function_list',
//                     permission_class: AppServiceUtility.login_service,
//                     select_permission: '查看预约登记查看',
//                 }),
//         },
//         {
//             path: ROUTE_PATH.reservationRegistrationLook1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ReservationRegistrationViewControl,
//                 {
//                     request_url: 'get_reservation_registration_list_look',
//                     get_permission_name: 'get_function_list',
//                     permission_class: AppServiceUtility.login_service,
//                     select_permission: '查看预约登记查看',
//                 }),
//         },
//         {
//             path: `${ROUTE_PATH.reservationRegistrationEditor}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeReservationViewControl),
//         },
//         {
//             path: ROUTE_PATH.reservationRegistrationEditor,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeReservationViewControl),
//         },

//         {
//             path: `${ROUTE_PATH.changeReservationRegistration}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangereservationRegistrationViewControl),
//         },
//         {
//             path: ROUTE_PATH.changeReservationRegistration,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangereservationRegistrationViewControl),
//         },
//         {
//             path: ROUTE_PATH.roomArchives,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RoomArchivesViewControl, {
//                 roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
//             }),
//         },
//         {
//             path: ROUTE_PATH.roomEdit,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(ChangeRoomArchivesViewControl, {
//                 roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
//             }),
//         },
//         {
//             path: `${ROUTE_PATH.roomEdit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeRoomArchivesViewControl, {
//                 roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
//             }),
//         },
//         {
//             path: ROUTE_PATH.rolePermission,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RolePermissionViewControl,
//                 {
//                     roleService: AppServiceUtility.role_service,
//                     permission: PermissionList.RolePermission_Select,
//                     request_url: 'get_role_list_all',
//                     get_permission_name: 'get_function_list',
//                     permission_class: AppServiceUtility.login_service,
//                     select_permission: '全部角色管理查询',
//                     edit_permission: '全部角色管理编辑',
//                     delete_permission: '全部角色管理删除',
//                     add_permission: '全部角色管理新增',

//                 })
//         },
//         {
//             path: ROUTE_PATH.rolePermissionLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RolePermissionViewControl,
//                 {
//                     roleService: AppServiceUtility.role_service,
//                     permission: PermissionList.RolePermission_Select,
//                     request_url: 'get_role_list_look',
//                     get_permission_name: 'get_function_list',
//                     permission_class: AppServiceUtility.login_service,
//                     select_permission: '角色管理查询',

//                 })
//         },
//         {
//             path: ROUTE_PATH.nursingArchives,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingArchivesViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.nursingItem,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingItemViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.nursingType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.nursingTypeEdit,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingTypeEditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         },
//         {
//             path: `${ROUTE_PATH.nursingTypeEdit}/${KEY_PARAM}`,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingTypeEditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         },
//         {
//             path: ROUTE_PATH.nursingItemEdit,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNursingItemViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         },
//         {
//             path: `${ROUTE_PATH.nursingItemEdit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNursingItemViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         },
//         {
//             path: `${ROUTE_PATH.nursingRelationShip}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingRelationShipViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         },
//         {
//             path: `${ROUTE_PATH.nursingRelationShipEdit}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingRelationShipEditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         }, {
//             path: `${ROUTE_PATH.nursingRelationShipEdit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NursingRelationShipEditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.archivesEditor,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNursingArchivesViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.archivesEditor}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNursingArchivesViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         // 人员管理  志愿者管理
//         {
//             path: ROUTE_PATH.personnel,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                     select_type: {},
//                 })
//         },
//         // 人员管理  志愿者管理
//         {
//             path: ROUTE_PATH.personnel_zyz,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                     select_type: { type: '志愿者' },
//                 })
//         },
//         // 人员管理  义工管理
//         {
//             path: ROUTE_PATH.personnel_yg,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                     select_type: { type: '义工' },
//                 })
//         },
//         // 人员管理  义工管理
//         {
//             path: ROUTE_PATH.personnel_ygtow,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                     select_type: { type: '义工' },
//                 })
//         },
//         // 人员管理
//         {
//             path: ROUTE_PATH.personnel1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                 })
//         },
//         // 人员管理
//         {
//             path: ROUTE_PATH.personnel2,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     change_url: ROUTE_PATH.changePersonnel,
//                     delete_url: "delete_personnel_info",
//                     request_url: 'get_personnel_list_all',
//                     select_permission: '人员管理查询',
//                     edit_permission: '人员管理编辑',
//                     delete_permission: '人员管理删除',
//                     add_permission: '人员管理新增',
//                     // select_type: { type: '义工' },
//                 })
//         },
//         {
//             path: ROUTE_PATH.wokerPersonnel,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     change_url: ROUTE_PATH.changewokerPersonnel,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_worker_list_all',
//                     delete_url: 'delete_worker_info',
//                     select_permission: '工作人员管理查询',
//                     edit_permission: '工作人员管理编辑',
//                     delete_permission: '工作人员管理删除',
//                     add_permission: '工作人员管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.wokerPersonnel1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     change_url: ROUTE_PATH.changewokerPersonnel,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_worker_list_all',
//                     delete_url: 'delete_worker_info',
//                     select_permission: '工作人员管理查询',
//                     edit_permission: '工作人员管理编辑',
//                     delete_permission: '工作人员管理删除',
//                     add_permission: '工作人员管理新增',
//                 })
//         },
//         // 人员权限配置=====
//         // 长者管理
//         {
//             path: ROUTE_PATH.elderInfo,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.elderInfo1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.elderInfo2,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.elderInfo3,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.elderInfo4,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.elderInfo5,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_all',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '长者管理查询',
//                     edit_permission: '长者管理编辑',
//                     delete_permission: '长者管理删除',
//                     add_permission: '长者管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.orgElderInfo,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_by_org',
//                     delete_url: 'delete_elder_info',
//                     select_permission: '机构长者管理查询',
//                     edit_permission: '机构长者管理编辑',
//                     delete_permission: '机构长者管理删除',
//                     add_permission: '机构长者管理新增',
//                 })
//         },
//         // 查看长者资料
//         {
//             path: ROUTE_PATH.elderInfoLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelViewControl,
//                 {
//                     personnel_type: '4',
//                     change_url: ROUTE_PATH.changeElderInfo,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_elder_list_look',
//                     select_permission: '查看长者资料查寻',
//                     edit_permission: '查看长者资料编辑',
//                     delete_permission: '查看长者资料删除',
//                     add_permission: '查看长者资料新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changePersonnel,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_personnel_info',
//                     back_url: ROUTE_PATH.personnel

//                 })
//         }, {
//             path: `${ROUTE_PATH.changePersonnel}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_personnel_info',
//                     back_url: ROUTE_PATH.personnel
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeElderInfo,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     back_url: ROUTE_PATH.elderInfo,
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_elder_info',
//                     select_face: 'get_elder_list_all',
//                     personnel_category: '长者',
//                 })
//         }, {
//             path: `${ROUTE_PATH.changeElderInfo}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     back_url: ROUTE_PATH.elderInfo,
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_elder_info',
//                     select_face: 'get_elder_list_all',
//                     personnel_category: '长者',
//                 })
//         },
//         // 工作人员编辑页面 
//         {
//             path: ROUTE_PATH.changewokerPersonnel,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     back_url: ROUTE_PATH.wokerPersonnel,
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_worker_info',
//                     select_face: 'get_worker_list_all',
//                     personnel_category: '工作人员',
//                 })
//         }, {
//             path: `${ROUTE_PATH.changewokerPersonnel}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangePersonnelViewControl,
//                 {
//                     back_url: ROUTE_PATH.wokerPersonnel,
//                     permission: PermissionList.RolePermission_Select,
//                     submit_face: 'update_worker_info',
//                     select_face: 'get_worker_list_all',
//                     personnel_category: '工作人员',
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                     select_param: { 'organization_type': '服务商' },
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization1,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                     select_param: { 'organization_type': '服务商' },
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization2,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization3,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization4,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization5,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                     select_param: { 'organization_type': '幸福院' },
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization6,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                     select_param: { 'organization_type': '福利院' },
//                 })
//         },
//         // 组织机构
//         {
//             path: ROUTE_PATH.organization7,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_tree_list',
//                     select_permission: '组织机构查询',
//                     edit_permission: '组织机构编辑',
//                     delete_permission: '组织机构删除',
//                     add_permission: '组织机构新增',
//                     select_param: { 'organization_type': '服务商' },
//                 })
//         },
//         // 查看组织机构
//         {
//             path: ROUTE_PATH.organizationLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_organization_list_look',
//                     select_permission: '查看组织机构查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeOrganization,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeOrganizationViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         }, {
//             path: `${ROUTE_PATH.changeOrganization}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeOrganizationViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.behavioralCompetenceAssessment,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BehavioralCompetenceAssessmentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeBehavioralCompetenceAssessment,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBehavioralCompetenceAssessmentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeBehavioralCompetenceAssessment}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeBehavioralCompetenceAssessmentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.assessmentTemplate}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentTemplateViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_template_list_all',
//                     select_permission: '评估模板查看',
//                     edit_permission: '评估模板编辑',
//                     delete_permission: '评估模板删除',
//                     add_permission: '评估模板新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.assessmentTemplate,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentTemplateViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_template_list_all',
//                     select_permission: '评估模板查看',
//                     edit_permission: '评估模板编辑',
//                     delete_permission: '评估模板删除',
//                     add_permission: '评估模板新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.assessmentTemplateLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentTemplateViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_template_list_look',
//                     select_permission: '查看评估模板查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeAssessmentTemplate}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAssessmentTemplateViewControl,
//                 {
//                     assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeAssessmentTemplate,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAssessmentTemplateViewControl,
//                 {
//                     assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.assessmentProject}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_all',
//                     del_url: 'delete_assessment_project',
//                     select_permission: '评估项目查询',
//                     edit_permission: '评估项目编辑',
//                     delete_permission: '评估项目删除',
//                     add_permission: '评估项目新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.assessmentProjectLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AssessmentProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_look',
//                     select_permission: '查看评估项目查询',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeAssessmentProject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAssessmentProjectViewControl,
//                 {
//                     assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeAssessmentProject,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeAssessmentProjectViewControl,
//                 {
//                     assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.competenceAssessmentList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CompetenceAssessmentViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_competence_assessment_list_all',
//                     select_permission: '评估记录查看',
//                     edit_permission: '评估记录编辑',
//                     delete_permission: '评估记录删除',
//                     add_permission: '评估记录新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.competenceAssessmentListLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CompetenceAssessmentViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_competence_assessment_list_look',
//                     select_permission: '查看评估记录查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.competenceAssessment,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCompetenceAssessmentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.competenceAssessment}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCompetenceAssessmentViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.addRole}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AddRoleUserViewControl, {
//                     permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
//                     roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
//                     permission: PermissionList.RolePermission_Select,
//                     permission_list: permission_list,
//                     per_org_service: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
//                     permission_services: new AjaxJsonRpcLoadingFactory(IPermissionServices, remote.url, "IPermissionService"),
//                 })
//         },
//         {
//             path: ROUTE_PATH.userManage,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 UserManageViewControl,
//                 {
//                     userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
//                     permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
//                     roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
//                     permission: PermissionList.UserManage_Select,

//                 })
//         },
//         {
//             path: ROUTE_PATH.userManagePermission,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 PersonnelPermissionControl,
//                 {
//                     // user_type: ['服务人员', '长者']
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.userEditor}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 UserEditorViewControl,
//                 {
//                     userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
//                     permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
//                     roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
//                     permission: PermissionList.UserManage_Select,

//                 })
//         },
//         {
//             path: ROUTE_PATH.addRole,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 AddRoleUserViewControl,
//                 {
//                     permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
//                     roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
//                     permission: PermissionList.RolePermission_Select,
//                     permission_list: permission_list,
//                     per_org_service: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
//                     permission_services: new AjaxJsonRpcLoadingFactory(IPermissionServices, remote.url, "IPermissionService"),
//                 })
//         },
//         {
//             path: ROUTE_PATH.userEditor,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(UserEditorViewControl, {
//                 userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
//                 permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
//                 roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
//                 permission: PermissionList.UserManage_Select,

//             })
//         },
//         {
//             path: ROUTE_PATH.login,
//             mainFormID: headMainID,
//             targetObject: createObject(LoginViewControl, {
//                 home_path: ROUTE_PATH.home,
//                 bind_phone_path: ROUTE_PATH.bindPhone,
//                 modify_password_path: ROUTE_PATH.modifyLoginPassword,
//                 retrieve_password: ROUTE_PATH.retrievePassword,
//                 iPersonnelOrganizationalService: AppServiceUtility.ipersonnel_service,
//                 login_service: AppServiceUtility.login_service,
//                 login_type: 'web',
//             })
//         }, {
//             path: ROUTE_PATH.register,
//             mainFormID: headMainID,
//             targetObject: createObject(RegisterControl)
//         }, {
//             path: ROUTE_PATH.bindPhone,
//             mainFormID: headMainID,
//             targetType: "secure",
//             targetObject: createObject(BindPhoneControl, { link_url: ROUTE_PATH.home })
//         }, {
//             path: ROUTE_PATH.retrievePassword,
//             mainFormID: headMainID,
//             targetObject: createObject(RetrievePasswordControl)
//         },
//         // 安全设置
//         {
//             path: ROUTE_PATH.securitySettings,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(SecuritySettingsViewControl, {
//                 userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
//                 modify_password: ROUTE_PATH.modifyLoginPassword,
//                 modify_mobile: ROUTE_PATH.modifyMobile,
//                 modify_email: ROUTE_PATH.modifyEmail,
//                 // permission_class: AppServiceUtility.login_service,
//                 // get_permission_name: 'get_function_list',
//                 // request_url: 'get_user_list',
//                 // edit_permission: '用户管理编辑',
//             })
//         }, {
//             path: ROUTE_PATH.modifyLoginPassword,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ModifyLoginPasswordViewControl, { userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService") })
//         }, {
//             path: ROUTE_PATH.modifyMobile,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ModifyMobileViewControl, { userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService") })
//         }, {
//             path: ROUTE_PATH.modifyEmail,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ModifyEmailViewControl, { userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService") })
//         },
//         {
//             path: ROUTE_PATH.home,
//             mainFormID: layoutMain,
//             targetObject: createObject(HomeViewControl)
//         },
//         // 服务项目
//         {
//             path: ROUTE_PATH.serviceProject,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_services_project_list_all',
//                     select_permission: '服务项目查看',
//                     edit_permission: '服务项目编辑',
//                     delete_permission: '服务项目删除',
//                     add_permission: '服务项目新增',
//                 })
//         },
//         // 查看服务项目
//         {
//             path: ROUTE_PATH.serviceProjectLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceProjectViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_services_project_list_look',
//                     select_permission: '查看服务项目查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeServiceProject,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceProjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         }, {
//             path: `${ROUTE_PATH.changeServiceProject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceProjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         /** ======== 住宿区域类型路由 start ======== */
//         {
//             path: ROUTE_PATH.hotelZoneType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HotelZoneTypeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_list_all',
//                     select_permission: '区域类型查看',
//                     edit_permission: '区域类型编辑',
//                     delete_permission: '区域类型删除',
//                     add_permission: '区域类型新增',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHotelZoneType}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeHotelZoneTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeHotelZoneType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeHotelZoneTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /** ======== 住宿区域类型路由 end ======== */
//         /** 服务项目类别路由 */
//         // 服务项目类别
//         {
//             path: ROUTE_PATH.servicesItemCategory,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServicesItemCategoryViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_services_item_category_list_all',
//                     select_permission: '服务项目类别查看',
//                     edit_permission: '服务项目类别编辑',
//                     delete_permission: '服务项目类别删除',
//                     add_permission: '服务项目类别新增',
//                 })
//         },
//         // 查看服务项目类别
//         {
//             path: ROUTE_PATH.servicesItemCategoryLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServicesItemCategoryViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_services_item_category_list_look',
//                     select_permission: '服务项目类别查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeServicesItemCategory,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServicesItemCategoryViewControl)
//         }, {
//             path: `${ROUTE_PATH.changeServicesItemCategory}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServicesItemCategoryViewControl)
//         },
//         /** ======== 住宿区域路由 start ======== */
//         {
//             path: ROUTE_PATH.hydropower,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HydropowerViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_hydropower_list_all',
//                     select_permission: '水电抄表查看',
//                     edit_permission: '水电抄表编辑',
//                     delete_permission: '水电抄表删除',
//                     add_permission: '水电抄表新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.hydropower,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HydropowerViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_hydropower_list_look',
//                     select_permission: '查看水电抄表查询',
//                     edit_permission: '水电抄表编辑',
//                     delete_permission: '水电抄表删除',
//                     add_permission: '水电抄表新增',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHydropower}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeHydropowerViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHydropower}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeHydropowerViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.hydropowerJj,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HydropowerJjViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_jiujiang_hydropower_list_all',
//                     select_permission: '九江水电抄表查询',
//                     edit_permission: '九江水电抄表编辑',
//                     delete_permission: '九江水电抄表删除',
//                     add_permission: '九江水电抄表新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.hydropowerJjLook,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HydropowerJjViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_jiujiang_hydropower_list_look',
//                     select_permission: '九江水电抄表查询',
//                     edit_permission: '九江水电抄表编辑',
//                     delete_permission: '九江水电抄表删除',
//                     add_permission: '九江水电抄表新增',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHydropowerJj}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeHydropowerJjViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHydropowerJj}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeHydropowerJjViewControl, {
//                     permission: PermissionList.RolePermission_Select
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.costAccountingLook}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 costAccountingViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_cost_account_list_all',
//                     select_permission: '住宿核算查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.costAccounting}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 costAccountingViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_cost_account_list_manage',
//                     select_permission: '住宿核算管理查看',
//                     edit_permission: '住宿核算管理编辑',
//                     add_permission: '住宿核算管理新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.hotelZone,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HotelZoneViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_list_all',
//                     select_permission: '区域管理查看',
//                     edit_permission: '区域管理编辑',
//                     delete_permission: '区域管理删除',
//                     add_permission: '区域管理新增',
//                     select_type: { 'organization_type': '幸福院' }
//                 })
//         },
//         {
//             path: ROUTE_PATH.hotelZoneLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 HotelZoneViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_list_look',
//                     select_permission: '查看区域情况查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeHotelZone}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeHotelZoneControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeHotelZone}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeHotelZoneControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         // 服务人员申请
//         {
//             path: ROUTE_PATH.servicePersonalApply,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServicePersonalApplyViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: '',
//                 add_permission: '服务人员申请新增',
//             })
//         },
//         /** ======== 住宿区域路由 end ======== */

//         /** ======== 需求类型 start ======== */
//         {
//             path: ROUTE_PATH.requirementType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RequirementTypeViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementType}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /** ======== 需求类型 end ======== */

//         /** ======== 需求项目 start ======== */
//         {
//             path: ROUTE_PATH.requirementProject,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RequirementProjectViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementProject}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementProjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementProject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementProjectViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /** ======== 需求项目 end ======== */

//         /** ======== 需求项目选项 start ======== */
//         {
//             path: ROUTE_PATH.requirementOption,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RequirementOptionViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementOption}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementOptionViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementOption}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeRequirementOptionViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /** ======== 需求项目选项 end ======== */

//         /** ======== 房态图 start ======== */
//         {
//             path: ROUTE_PATH.roomStatus,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RoomStatusViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_all',
//                     select_permission: '房态图查看',
//                     edit_permission: '房态图编辑',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.roomStatusexcel,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RoomStatusViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_all',
//                     select_permission: '房态图查看',
//                     edit_permission: '房态图编辑',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.roomStatusexcelthree,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RoomStatusViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_all',
//                     select_permission: '房态图查看',
//                     edit_permission: '房态图编辑',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.roomStatusexceltow,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RoomStatusViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_assessment_project_list_all',
//                     select_permission: '房态图查看',
//                     edit_permission: '房态图编辑',
//                 }
//             )
//         },
//         /** ======== 房态图 end ======== */

//         /** 服务项目类别路由 */
//         // 服务适用范围
//         {
//             path: ROUTE_PATH.serviceScope,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceScopeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_scope_list_all',
//                     select_permission: '服务适用范围查看',
//                     edit_permission: '服务适用范围编辑',
//                     delete_permission: '服务适用范围删除',
//                     add_permission: '服务适用范围新增',
//                 }
//             )
//         },
//         // 查看服务适用范围
//         {
//             path: ROUTE_PATH.serviceScopeLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceScopeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_scope_list_look',
//                     select_permission: '查看服务适用范围查看',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeServiceScope,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServicesScopeViewControl)
//         }, {
//             path: `${ROUTE_PATH.changeServiceScope}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServicesScopeViewControl)
//         },
//         {
//             path: ROUTE_PATH.taskList,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_all_task_list_all',
//                     select_permission: '任务列表查询',
//                     edit_permission: '任务列表编辑',
//                     add_permission: '任务列表新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.taskListLook,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_all_task_list_look',
//                     select_permission: '任务列表查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.taskToBeSend,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskToBeSendViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_to_be_send_task_list_look_edit',
//                     select_permission: '分派任务列表查询',
//                     edit_permission: '分派任务列表编辑',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.taskToBeProcess,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskToBeProcessViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_to_be_processed_task_list_look',
//                     select_permission: '审核任务列表查询',
//                     add_permission: '审核任务列表审核',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.taskTypeList,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskTypeListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_task_type_list_all',
//                     select_permission: '任务类型查询',
//                     edit_permission: '任务类型编辑',
//                     add_permission: '任务类型新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.taskTypeListLook,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 TaskTypeListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_task_type_list_look',
//                     select_permission: '查看任务类型查询',
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeTaskType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTaskTypeViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.changeTaskType,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTaskTypeViewControl,
//                 {}
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeTask}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTaskViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.changeTask,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeTaskViewControl,
//                 {}
//             )
//         },

//         {
//             path: ROUTE_PATH.messageList,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 MessageListViewControl,
//                 {}
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeMessage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeMessageViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.changeMessage,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeMessageViewControl,
//                 {}
//             )
//         },
//         {
//             path: ROUTE_PATH.leaveReason,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 LeaveReasonViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_leave_reason_list_all',
//                     select_permission: '离开原因查询',
//                     edit_permission: '离开原因编辑',
//                     delete_permission: '离开原因删除',
//                     add_permission: '离开原因新增',
//                 }
//             )
//         }, {
//             path: ROUTE_PATH.changeLeaveReason,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeLeaveReasonViewControl,
//                 {
//                     // permission: PermissionList.UserManage_Select,

//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeLeaveReason}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeLeaveReasonViewControl,
//                 {
//                     // permission: PermissionList.UserManage_Select,

//                 })
//         },
//         {
//             path: ROUTE_PATH.serviceWoker,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceWokerViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,

//                 })
//         }, {

//             path: ROUTE_PATH.ChangeServiceWoker,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 ChangeServiceWokerViewControl, { permission: PermissionList.RolePermission_Select, })
//         },

//         /** 服务选项路由 */
//         {
//             path: ROUTE_PATH.serviceOption,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOptionViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_servicre_option_list_all',
//                     select_permission: '服务选项查看',
//                     edit_permission: '服务选项编辑',
//                     delete_permission: '服务选项删除',
//                     add_permission: '服务选项新增',
//                 })
//         },
//         // 查看服务选项
//         {
//             path: ROUTE_PATH.serviceOptionLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceOptionViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_servicre_option_list_look',
//                     select_permission: '查看服务选项查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeServiceOption,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceOptionViewControl, { permission: PermissionList.RolePermission_Select, })
//         }, {
//             path: `${ROUTE_PATH.ChangeServiceWoker}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceWokerViewControl, { permission: PermissionList.RolePermission_Select, })
//         },
//         {
//             path: `${ROUTE_PATH.changeServiceOption}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceOptionViewControl)
//         },
//         /** =================================接待管理部分=============================== */
//         {
//             path: ROUTE_PATH.checkInNew,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInNewViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.checkIn,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_check_in_list_all',
//                     select_permission: '长者入住查询',
//                     edit_permission: '长者入住编辑',
//                     delete_permission: '长者入住删除',
//                     add_permission: '长者入住新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.checkInLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_check_in_list_look',
//                     select_permission: '查看长者入住查询',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.checkInDetail}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CheckInDetailViewControl)
//         },
//         /** 九江入住办理 */
//         {
//             path: ROUTE_PATH.checkInNewJj,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInNewJjViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.checkInNewJj}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInNewJjViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.checkInJj,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInListViewJjControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_check_in_list_all',
//                     select_permission: '长者入住查看',
//                     edit_permission: '长者入住编辑',
//                     delete_permission: '长者入住删除',
//                     add_permission: '长者入住新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.checkInJjLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 CheckInListViewJjControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_check_in_list_look',
//                     select_permission: '查看长者入住查看',
//                 })
//         },
//         // 服务记录 
//         {
//             path: ROUTE_PATH.serviceRecord,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_record_list_all',
//                     select_permission: '服务记录查看',
//                     edit_permission: '服务记录编辑',
//                     delete_permission: '服务记录删除',
//                     add_permission: '服务记录新增',
//                 })
//         },
//         // 查看服务记录
//         {
//             path: ROUTE_PATH.serviceRecordLook,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServiceRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_service_record_list_look',
//                     select_permission: '服务记录查看',
//                     edit_permission: '服务记录编辑',
//                     delete_permission: '服务记录删除',
//                     add_permission: '服务记录新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.serviceLedger,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(ServiceLedgerViewControl)
//         }, {
//             path: ROUTE_PATH.changeServiceRecord,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceRecordViewControl, { permission: PermissionList.RolePermission_Select, })
//         }, {
//             path: `${ROUTE_PATH.changeServiceRecord}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeServiceRecordViewControl)
//         },
//         /** =====调房===== */
//         {
//             path: ROUTE_PATH.roomList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RoomListControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_check_in_list_room_all',
//                     select_permission: '房间调整查看',
//                     edit_permission: '房间调整编辑',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.roomChange}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RoomChangeViewControl, {})
//         },
//         /** ===========需求模板============= */
//         {
//             path: ROUTE_PATH.requirementTemplate,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RequirementTemplateViewControl)
//         }, {
//             path: ROUTE_PATH.changeRequirementTemplate,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeRequirementTemplateViewControl,
//                 {
//                     requirementTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IRequirementTemplateService, remote.url, "IRequirementTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementTemplate}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeRequirementTemplateViewControl,
//                 {
//                     requirementTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IRequirementTemplateService, remote.url, "IRequirementTemplateService"),
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         }, {
//             path: ROUTE_PATH.requirementRecord,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 RequirementRecordViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeRequirementRecord,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeRequirementRecordViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeRequirementRecord}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeRequirementRecordViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         /** ===========医院管理============= */
//         {
//             path: ROUTE_PATH.hospitalArrears,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(hospitalArrearsViewControl)
//         },
//         {
//             path: ROUTE_PATH.hospitalArrearsEdit,
//             mainFormID: layoutMain,
//             exact: true,
//             targetType: 'secure',
//             targetObject: createObject(ChangeHospitalViewControl, {
//                 permission: PermissionList.RolePermission_Select,
//             })
//         },
//         {
//             path: `${ROUTE_PATH.hospitalArrearsEdit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeHospitalViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         /** ============================账务管理============================ */
//         {
//             path: `${ROUTE_PATH.changeCollectorCharge}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCollectorChargeViewControl)
//         }, {
//             path: `${ROUTE_PATH.changeCollectorPayment}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCollectorPaymentViewControl)
//         }, {
//             path: `${ROUTE_PATH.financialChargeRecord}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialChargeRecordViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_charge_record_list_all',
//                     select_permission: '收费记录管理查询',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.financialChargeRecordLook}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialChargeRecordViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_charge_record_list_look',
//                     select_permission: '查看收费记录查询',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.financialBankReconciliation}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BillRecodeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bank_unreconciliation_list_look_export',
//                     select_permission: '打印/导出银行扣款单查询'
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.financialBankReconciliationManage}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BillRecodeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bank_unreconciliation_list_manage',
//                     select_permission: '打印/导出银行扣款单管理查询',
//                     add_permission: '打印/导出银行扣款单管理导出',
//                     export_url: 'exprot_excel_manage'
//                 }
//             )
//         },
//         // 非银行扣款单
//         {
//             path: `${ROUTE_PATH.unfinancialBankReconciliation}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 UnFinancialReconciliationRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_unbank_reconciliation_list_manage',
//                     select_permission: '非银行扣款单管理查询',
//                     edit_permission: '非银行扣款单管理编辑',
//                     add_permission: '非银行扣款单管理新增'
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.unfinancialBankReconciliationLook}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 UnFinancialReconciliationRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_unbank_reconciliation_list_Look',
//                     select_permission: '查看非银行扣款单查询',
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.financialReconciliationRecord}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialReconciliationRecordViewControl, {
//                     request_url: 'get_bank_reconciliation_list_all'
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.financialReconciliationRecordLook}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FinancialReconciliationRecordViewControl, {
//                     request_url: 'get_bank_reconciliation_list_look'
//                 })
//         },

//         {
//             path: ROUTE_PATH.announcementIssue,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AnnouncementIssueViewControl,
//                 // {
//                 //     permission: PermissionList.RolePermission_Select,
//                 // }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.announcementIssue}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AnnouncementIssueViewControl,
//                 // {
//                 //     permission: PermissionList.RolePermission_Select,
//                 // }
//             )
//         },
//         {
//             path: ROUTE_PATH.announcementList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AnnouncementListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_announcement_list_all',
//                     select_permission: '公告管理查询',
//                     edit_permission: '公告管理编辑',
//                     delete_permission: '公告管理删除',
//                     add_permission: '公告管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.announcementListLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AnnouncementListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_announcement_list_look',
//                     select_permission: '查看公告列表查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.activityTypeManage,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityTypeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_type_list_all',
//                     select_permission: '活动类型查看',
//                     edit_permission: '活动类型编辑',
//                     delete_permission: '活动类型删除',
//                     add_permission: '活动类型新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activityTypeManageLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityTypeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_type_list_look',
//                     select_permission: '查看活动类型查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeActivityTypeManage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeActivityTypeManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.activityManage,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityPublishViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_list_all',
//                     select_permission: '活动列表查看',
//                     edit_permission: '活动列表编辑',
//                     delete_permission: '活动列表删除',
//                     add_permission: '活动列表新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activityManageLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityPublishViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_list_look',
//                     select_permission: '查看活动列表查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeActivityManage}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityPublishViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.newsList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NewsListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_news_list_all',
//                     select_permission: '新闻管理查询',
//                     edit_permission: '新闻管理编辑',
//                     delete_permission: '新闻管理删除',
//                     add_permission: '新闻管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.newsListLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 NewsListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_news_list_look',
//                     select_permission: '查看新闻列表查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeActivityManage,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityPublishViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeNews,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNewsViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeNews}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeNewsViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.commentList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_list_all',
//                     select_permission: '评论管理查询',
//                     edit_permission: '评论管理编辑',
//                     delete_permission: '评论管理删除',
//                     add_permission: '评论管理新增',
//                 })
//         }, {
//             path: ROUTE_PATH.commentListLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentListViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_list_look',
//                     select_permission: '查看评论管理查询',
//                 })
//         }, {
//             path: ROUTE_PATH.commentAudit,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentAuditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_audit_list_all',
//                     select_permission: '评论审核查询',
//                     edit_permission: '评论审核编辑',
//                     delete_permission: '评论审核删除',
//                     add_permission: '评论审核新增',
//                 })
//         }, {
//             path: ROUTE_PATH.commentAuditLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentAuditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_audit_list_look',
//                     select_permission: '查看评论审核查询',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeCommentAudit,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCommentAuditControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeCommentAudit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCommentAuditControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.activityParticipate,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityParticipateViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_participate_list_all',
//                     select_permission: '报名列表查看',
//                     delete_permission: '报名列表删除',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activityParticipateLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityParticipateViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_participate_list_look',
//                     select_permission: '查看报名列表查看',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activitySignIn,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivitySignInViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_sign_in_list_all',
//                     select_permission: '签到列表查看',
//                     delete_permission: '签到列表删除',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activitySignInLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivitySignInViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_sign_in_list_look',
//                     select_permission: '查看签到列表查看',
//                 })
//         },
//         // 服务人员登记
//         {
//             path: ROUTE_PATH.servicePersonalManage,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServicePersonalViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_personal_list',
//                 select_permission: '服务人员登记查看',
//                 edit_permission: '服务人员登记编辑',
//                 delete_permission: '服务人员登记删除',
//                 add_permission: '服务人员登记录新增',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeServicePersonal}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServicePersonalViewControl, {})
//         },
//         {
//             path: ROUTE_PATH.changeServicePersonal,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeServicePersonalViewControl, {})
//         },
//         // 服务商结算
//         {
//             path: ROUTE_PATH.serviceProviderSettlement,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServiceProviderSettlementViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_record_list_all',
//                 select_permission: '服务商结算查看',
//                 edit_permission: '服务商结算编辑',
//                 delete_permission: '服务商结算删除',
//                 add_permission: '服务商结算新增',
//             })
//         },
//         // 服务商结算管理
//         {
//             path: ROUTE_PATH.serviceProviderSettlementLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServiceProviderSettlementViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_record_list_look',
//                 select_permission: '服务商结算管理查看',
//                 edit_permission: '服务商结算管理编辑',
//                 delete_permission: '服务商结算管理删除',
//                 add_permission: '服务商结算管理新增',
//             })
//         },
//         {
//             path: ROUTE_PATH.serviceProviderSettlementLook1,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServiceProviderSettlementViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_record_list_look',
//                 select_permission: '服务商结算管理查看',
//                 edit_permission: '服务商结算管理编辑',
//                 delete_permission: '服务商结算管理删除',
//                 add_permission: '服务商结算管理新增',
//             })
//         },
//         // 服务人员结算
//         {
//             path: ROUTE_PATH.servicePersonalSettlement,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServicePersonalSettlementViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_record_list_all',
//                 select_permission: '服务人员结算查看',
//                 edit_permission: '服务人员结算编辑',
//                 delete_permission: '服务人员结算删除',
//                 add_permission: '服务人员结算新增',
//             })
//         },
//         // 服务人员结算管理
//         {
//             path: ROUTE_PATH.servicePersonalSettlementLook,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServicePersonalSettlementViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_record_list_look',
//                 select_permission: '服务人员结算管理查看',
//                 edit_permission: '服务人员结算管理编辑',
//                 delete_permission: '服务人员结算管理删除',
//                 add_permission: '服务人员结算管理新增',
//             })
//         },
//         {
//             path: ROUTE_PATH.comment,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CommentViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.comment}/${KEY_PARAM}`,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CommentViewControl, { permission: PermissionList.RolePermission_Select, })
//         },
//         {
//             path: ROUTE_PATH.articleType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ArticleTypeViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_article_type_list_all',
//                     select_permission: '文章类型查询',
//                     edit_permission: '文章类型编辑',
//                     delete_permission: '文章类型删除',
//                     add_permission: '文章类型新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.articleTypeLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ArticleTypeViewControl, {
//                     permission: PermissionList.RolePermission_Select,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_article_type_list_look',
//                     select_permission: '查看文章类型查询',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeArticleType,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeArticleTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeArticleType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeArticleTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },

//         /* 补贴管理*/
//         // 补贴管理模块路由
//         ...subsidyRouter,
//         /* 调研管理*/
//         // ...researchRouter,
//         {
//             path: ROUTE_PATH.leaveInsert,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(LeaveInsertViewControl, {})
//         },
//         {
//             path: ROUTE_PATH.leaveRecord,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 LeaveRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_leave_list_all',
//                     select_permission: '住宿记录查看',
//                     edit_permission: '住宿记录编辑',
//                     delete_permission: '住宿记录删除',
//                     add_permission: '住宿记录新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.leaveRecord,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 LeaveRecordViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_leave_list_look',
//                     select_permission: '查看住宿记录查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.leaveEdit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(LeaveEditViewControl, {})
//         },
//         {
//             path: ROUTE_PATH.leaveEdit,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(LeaveEditViewControl, {})
//         },
//         /* 评论类型 */
//         {
//             path: ROUTE_PATH.commentType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentTypeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_type_list_all',
//                     select_permission: '评论类型查询',
//                     edit_permission: '评论类型编辑',
//                     delete_permission: '评论类型删除',
//                     add_permission: '评论类型新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.commentTypeLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CommentTypeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_comment_type_list_look',
//                     select_permission: '查看评论类型查询',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeCommentType,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCommentTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeCommentType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeCommentTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         /* 文章审核*/
//         {
//             path: ROUTE_PATH.articleAudit,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ArticleAuditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_article_audit_list_all',
//                     select_permission: '文章审核查询',
//                     edit_permission: '文章审核编辑',
//                     delete_permission: '文章审核删除',
//                     add_permission: '文章审核新增',
//                 })
//         },
//         {
//             path: ROUTE_PATH.articleAuditLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ArticleAuditViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_article_audit_list_look',
//                     select_permission: '查看文章审核查询',
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeArticleAudit,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeArticleAuditControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeArticleAudit}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeArticleAuditControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         // 服务套餐列表
//         {
//             path: ROUTE_PATH.serviceProjectindex,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServiceProjectControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_service_product_package_list',
//                 select_permission: '服务人员结算查看',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.ServiceProjectDetail}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServiceProjectDetailControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.ServiceProjectBuy}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServerProjectBuyControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.ServiceProjectBuy}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ServerProjectBuyControl, {})
//         },
//         // 慈善管理
//         // 冠名基金
//         {
//             path: ROUTE_PATH.charitableTitleFund,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableTitleFundViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_title_fund_list_all',
//                 select_permission: '冠名基金查询',
//                 edit_permission: '冠名基金编辑',
//                 add_permission: '冠名基金新增',
//                 delete_permission: '冠名基金删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}/${CODE_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableTitleFund}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
//         },
//         {
//             path: ROUTE_PATH.viewCharitableTitleFund,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ViewCharitableTitleFundViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_title_fund_list_all',
//                 select_permission: '冠名基金查询',
//                 edit_permission: '冠名基金编辑',
//                 add_permission: '冠名基金新增',
//             })
//         },
//         // 慈善项目管理
//         {
//             path: ROUTE_PATH.charitableProject,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableProjectViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_project_list_all',
//                 select_permission: '慈善项目查询',
//                 edit_permission: '慈善项目编辑',
//                 add_permission: '慈善项目新增',
//                 delete_permission: '慈善项目删除',
//             })
//         },
//         {
//             path: ROUTE_PATH.charitableProjectList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableProjectListViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_project_list_all',
//                 select_permission: '慈善项目查询',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.viewCharitableProject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ViewCharitableProjectViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableProject}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableProjectViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableProject}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableProjectViewControl, {})
//         },
//         // 慈善捐赠
//         {
//             path: ROUTE_PATH.charitableDonate,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableDonateViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_donate_list_all',
//                 select_permission: '慈善捐赠查询',
//                 edit_permission: '慈善捐赠编辑',
//                 add_permission: '慈善捐赠新增',
//                 delete_permission: '慈善捐赠删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.viewCharitableDonate}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ViewCharitableDonateViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableDonate}/${KEY_PARAM}/${CODE_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableDonateViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableDonate}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableDonateViewControl, {})
//         },
//         // 慈善拨款
//         {
//             path: ROUTE_PATH.charitableAppropriation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableAppropriationViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_recipient_list_all',
//                 select_permission: '募捐申请查询',
//                 edit_permission: '募捐申请编辑',
//                 add_permission: '募捐申请新增',
//                 delete_permission: '募捐申请删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeCharitableAppropriation}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeCharitableAppropriationViewControl, {})
//         },
//         // 慈善信息
//         {
//             path: ROUTE_PATH.charitableInformation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableInformationViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_information_list_all',
//                 select_permission: '慈善信息查询',
//                 edit_permission: '慈善信息编辑',
//                 add_permission: '慈善信息新增',
//                 delete_permission: '慈善信息删除',
//             })
//         },
//         // 慈善会资料
//         {
//             path: ROUTE_PATH.charitableSociety,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(CharitableSocietyViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_society_list_all',
//                 select_permission: '慈善会资料查询',
//                 edit_permission: '慈善会资料编辑',
//                 add_permission: '慈善会资料新增',
//                 delete_permission: '慈善会资料删除',
//             })
//         },
//         // 募捐查看页面
//         {
//             path: ROUTE_PATH.viewRecipientApplyList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ViewRecipientApplyViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_recipient_apply_list_all',
//                 select_permission: '募捐申请查询',
//                 edit_permission: '募捐申请编辑',
//                 add_permission: '募捐申请新增',
//                 delete_permission: '募捐申请删除',
//             })
//         },
//         // 个人募捐申请
//         {
//             path: ROUTE_PATH.recipientApplyList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RecipientApplyListViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_recipient_apply_list_all',
//                 select_permission: '募捐申请查询',
//                 edit_permission: '募捐申请编辑',
//                 add_permission: '募捐申请新增',
//                 delete_permission: '募捐申请删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.recipientApply}/${KEY_PARAM}/${CODE_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RecipientApplyViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.recipientApply}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RecipientApplyViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.recipientApply}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(RecipientApplyViewControl, {})
//         },
//         // 机构申请
//         {
//             path: ROUTE_PATH.organizationApplyList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(OrganizationApplyListViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_charitable_organization_apply_list_all',
//                 select_permission: '机构募捐申请查询',
//                 edit_permission: '机构募捐申请编辑',
//                 add_permission: '机构募捐申请新增',
//                 delete_permission: '机构募捐申请删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.organizationApply}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(OrganizationApplyViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.organizationApply}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(OrganizationApplyViewControl, {})
//         },
//         // 志愿者服务类别管理
//         {
//             path: ROUTE_PATH.volunteerServiceCategory,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(VolunteerServiceCategoryViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_volunteer_service_category_list_all',
//                 select_permission: '志愿者服务类别查询',
//                 edit_permission: '志愿者服务类别编辑',
//                 add_permission: '志愿者服务类别新增',
//                 delete_permission: '志愿者服务类别删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeVolunteerServiceCategory}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeVolunteerServiceCategoryViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeVolunteerServiceCategory}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeVolunteerServiceCategoryViewControl, {})
//         },
//         // 志愿者申请
//         {
//             path: ROUTE_PATH.volunteerApplyList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(VolunteerApplyListViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_volunteer_apply_list_all',
//                 select_permission: '志愿者申请查询',
//                 edit_permission: '志愿者申请编辑',
//                 add_permission: '志愿者申请新增',
//                 delete_permission: '志愿者申请删除',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.volunteerApply}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(VolunteerApplyViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.volunteerApply}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(VolunteerApplyViewControl, {})
//         },
//         // 设备管理
//         {
//             path: `${ROUTE_PATH.Device}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(DeviceViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_device_all',
//                 select_permission: '设备管理查看',
//                 edit_permission: '设备管理编辑',
//                 delete_permission: '设备管理删除',
//                 add_permission: '设备管理新增',
//             })
//         },
//         // 查看设备
//         {
//             path: `${ROUTE_PATH.DeviceLook}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(DeviceViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_device_look',
//                 select_permission: '查看设备查看',
//             })
//         },
//         // 监控管理
//         {
//             path: `${ROUTE_PATH.Monitor}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(MonitorViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_monitor_list_all',
//                 select_permission: '监控管理查看',
//                 edit_permission: '监控管理编辑',
//                 delete_permission: '监控管理删除',
//                 add_permission: '监控管理新增',
//             })
//         },
//         // 查看监控
//         {
//             path: `${ROUTE_PATH.MonitorLook}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(MonitorViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_monitor_list_look',
//                 select_permission: '监控管理查看',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeMonitor}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeMonitorViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.changeMonitor}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ChangeMonitorViewControl, {})
//         },
//         {
//             path: `${ROUTE_PATH.DeviceLog}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(DeviceLogViewControl, {})
//         },
//         // 社会群体类型管理
//         {
//             path: ROUTE_PATH.socialGroupsType,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(SocialGroupsTypeViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_social_groups_type_list_all',
//                 select_permission: '社会群体类型管理查看',
//                 edit_permission: '社会群体类型管理编辑',
//                 delete_permission: '社会群体类型管理删除',
//                 add_permission: '社会群体类型管理新增',
//             })
//         },
//         // 查看社会群体类型
//         {
//             path: ROUTE_PATH.socialGroupsTypeLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(SocialGroupsTypeViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_social_groups_type_list_look',
//                 select_permission: '社会群体类型管理查看',
//             })
//         },
//         {
//             path: `${ROUTE_PATH.changeSocialGroupsType}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeSocialGroupsTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeSocialGroupsType,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeSocialGroupsTypeViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         }, {
//             path: ROUTE_PATH.activityRoom,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityRoomViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_room_list_all',
//                     select_permission: '活动室列表查看',
//                     delete_permission: '活动室列表删除',
//                     add_permission: '活动室列表新增',
//                     edit_permission: '活动室列表编辑',
//                 })
//         },
//         {
//             path: ROUTE_PATH.activityRoomLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityRoomViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_room_list_look',
//                     select_permission: '查看活动室列表查看',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.changeActivityRoom}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityRoomViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeActivityRoom,
//             exact: true,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeActivityRoomViewControl,
//                 {
//                     permission: PermissionList.RolePermission_Select,
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.activityRoomReservation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ActivityRoomReservationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_activity_room_reservation_list_all',
//                     select_permission: '活动室预约列表查看',
//                     delete_permission: '活动室预约列表删除',
//                 })
//         },
//         // 活动信息
//         {
//             path: ROUTE_PATH.activityInformation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(ActivityInformationViewControl, {
//                 permission_class: AppServiceUtility.login_service,
//                 get_permission_name: 'get_function_list',
//                 request_url: 'get_activity_information_list_all',
//                 select_permission: '活动信息查询',
//                 edit_permission: '活动信息编辑',
//                 add_permission: '活动信息新增',
//                 delete_permission: '活动信息删除',
//             })
//         },
//         /** =====费用管理===== */
//         {
//             path: ROUTE_PATH.incidentalList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 IncidentalManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_incidental_list',
//                     select_permission: '杂费管理查询',
//                     edit_permission: '杂费管理编辑',
//                     delete_permission: '杂费管理删除',
//                     add_permission: '杂费管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.incidentalListLook,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             exact: true,
//             targetObject: createObject(
//                 IncidentalManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_incidental_list_look',
//                     select_permission: '查看杂费管理查询'
//                 }
//             )
//         },
//         {
//             path: `${ROUTE_PATH.changeIncidental}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeIncidentalViewControl,
//                 {
//                 })
//         },
//         {
//             path: ROUTE_PATH.changeIncidental,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeIncidentalViewControl,
//                 {
//                 })
//         },

//         {
//             path: ROUTE_PATH.foodCostList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FoodCostManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_food_cost_list',
//                     select_permission: '餐费管理查询',
//                     edit_permission: '餐费管理编辑',
//                     delete_permission: '餐费管理删除',
//                     add_permission: '餐费管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.foodCostList,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FoodCostManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_food_cost_list_look',
//                     select_permission: '查看餐费管理查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeFoodCost,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(FoodCostViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeFoodCost}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(FoodCostViewControl)
//         },
//         {
//             path: ROUTE_PATH.medicalCost,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 MedicalCostManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '床位管理查看',
//                     edit_permission: '床位管理编辑',
//                     delete_permission: '床位管理删除',
//                     add_permission: '床位管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeMedicalCost,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(MedicalCostViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeMedicalCost}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(MedicalCostViewControl)
//         },
//         {
//             path: ROUTE_PATH.diaperCost,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 DiaperCostManageViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '床位管理查看',
//                     edit_permission: '床位管理编辑',
//                     delete_permission: '床位管理删除',
//                     add_permission: '床位管理新增',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeDiaperCost,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(DiaperCostViewControl)
//         },
//         {
//             path: `${ROUTE_PATH.changeDiaperCost}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(DiaperCostViewControl)
//         },
//         /** 报表管理 */
//         {
//             path: ROUTE_PATH.operationStatus,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(OperationStatusViewControl)
//         },
//         /** 福利院--工作人员统计--报表 */
//         {
//             path: ROUTE_PATH.staffStatistics,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(staffStatisticsViewControl)
//         },
//         {
//             /** 幸福院--工作人员统计--报表 */
//             path: ROUTE_PATH.happyStaffStatistics,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(happyStaffStatisticsViewControl)
//         },
//         /** 入住老人情况统计-报表 */
//         {
//             path: ROUTE_PATH.elderlyResidents,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(elderlyResidentsViewControl)
//         },
//         {
//             path: ROUTE_PATH.elderCheck,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(elderCheckViewControl)
//         },
//         {
//             path: ROUTE_PATH.basicFacilities,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BasicFacilitiesViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '基本设施情况查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.adoptionSituation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 AdoptionSituationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '收养人员情况查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.fireQualification,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '基础数据'
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification2,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '年龄统计',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification3,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '护照统计',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification4,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '运营统计',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification5,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '欠费一览',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification6,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '收费统计',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification7,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '长者数量',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification8,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '健康数据',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification9,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '巡检评估',
//                 })
//         },
//         {
//             path: ROUTE_PATH.fireQualification10,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '活动报名统计',
//                 })
//         },
//         // 日托订单
//         {
//             path: ROUTE_PATH.fireQualification_rtdd,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '日托订单统计'
//                 })
//         },
//         // 日托支付
//         {
//             path: ROUTE_PATH.fireQualification_rtzf,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '日托支付统计'
//                 })
//         },
//         // 活动报名统计
//         {
//             path: ROUTE_PATH.fireQualification_hdbm,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '活动报名统计'
//                 })
//         },
//         // 年龄统计
//         {
//             path: ROUTE_PATH.fireQualification_nntj,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '年龄统计'
//                 })
//         },
//         // 长者数据
//         {
//             path: ROUTE_PATH.fireQualification_zzsj,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '长者数据'
//                 })
//         },
//         // 长者报表
//         {
//             path: ROUTE_PATH.fireQualification_zzbb,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '长者报表'
//                 })
//         },
//         // 月收入统计
//         {
//             path: ROUTE_PATH.fireQualification_ysr,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '月收入统计'
//                 })
//         },
//         // 活动报告
//         {
//             path: ROUTE_PATH.fireQualification_hdbg,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 FireQualificationViewControl, {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '消防资质情况查询',
//                     select_type: '活动报告'
//                 })
//         },
//         {

//             path: ROUTE_PATH.workerConstitute,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 WorkerConstituteViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '职工构成情况查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.compensation,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 CompensationViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '薪酬待遇情况查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.organizationReport,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 OrganizationReportViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_bed_list_all',
//                     select_permission: '机构报表情况查询',
//                 }
//             )
//         },
//         {
//             path: ROUTE_PATH.changeSquence,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ChangeSquenceControl
//             )
//         },
//         {
//             path: ROUTE_PATH.subsidyAutoRecharge,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 SubsidyAutoRechargeViewControl,
//                 {
//                     permission_class: AppServiceUtility.login_service,
//                     get_permission_name: 'get_function_list',
//                     request_url: 'get_journal_list',
//                     select_permission: '单据签核查看',
//                     add_permission: '单据签核编辑',
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.ServicePersonApplyReviewed}/${KEY_PARAM}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 ServicePersonReviewedViewControl,
//                 {
//                 })
//         },
//         {
//             path: `${ROUTE_PATH.buyService}`,
//             mainFormID: layoutMain,
//             targetType: 'secure',
//             targetObject: createObject(
//                 BuyServiceViewControl,
//                 {
//                 })
//         },
//     ]
// );

// export let defaultObject = new IntelligentElderlyCareApplication(
//     router,
//     new AjaxJsonRpcFactory(IUserService, remote.url, "IUserService"),
//     new AjaxJsonRpcFactory(ISecurityService, remote.url, 'ISecurityService')
// );

// export default defaultObject;