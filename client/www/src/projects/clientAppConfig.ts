/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-06-20 17:37:29
 * @LastEditTime: 2020-03-09 10:35:36
 * @LastEditors: Please set LastEditors
 */
/*
 * 版权：Copyright (c) 2019 红网
 *
 * 创建日期：Tuesday April 23rd 2019
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 *
 * 修改日期: Tuesday, 23rd April 2019 10:26:24 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 *
 * 说明
 * 		1、政务物联网运维APP配置
 */
import { createObject, IPermissionService, IRoleService, ISecurityService } from 'pao-aop';
import { AjaxJsonRpcFactory, AppReactRouterControl, BlankMainFormControl, CookieUtil } from 'pao-aop-client';
import { BackstageManageMainFormControl, COOKIE_KEY_CURRENT_USER, COOKIE_KEY_USER_ROLE, COOKIE_OLDER_CALL_CENTER } from 'src/business/mainForm/backstageManageMainForm';
import { HeadMainFormControl } from 'src/business/mainForm/headMainForm';
import { CODE_PARAM, KEY_PARAM } from 'src/business/util_tool';
import { BindPhoneControl } from 'src/business/views/bind-phone';
import { LoginViewControl } from 'src/business/views/login';
import { ModifyLoginPasswordViewControl } from 'src/business/views/modify-password';
import { ModifyEmailViewControl } from 'src/business/views/modify_email';
import { ModifyMobileViewControl } from 'src/business/views/modify_mobile';
import { RegisterControl } from 'src/business/views/register';
import { RetrievePasswordControl } from 'src/business/views/retrieve-password';
import { SecuritySettingsViewControl } from 'src/business/views/security-setting';
import { IRoomService } from "src/projects/models/room";
import { IUserService } from "src/projects/models/user";
import { CallCenterViewControl } from 'src/projects/views/buss_mis/call-center';
import { EmergencyCallViewControl } from 'src/projects/views/buss_mis/call-center/emergency-call';
import { EmergencyCallDetailsViewControl } from 'src/projects/views/buss_mis/call-center/emergency-call/details';
import { KnowledgeBaseListViewControl } from 'src/projects/views/buss_mis/call-center/knowledge-base';
import { KnowledgeDetailsViewControl } from 'src/projects/views/buss_mis/call-center/knowledge-details';
import { KnowledgeEditViewControl } from 'src/projects/views/buss_mis/call-center/konwledge-edit';
import { RolePermissionViewControl } from 'src/projects/views/buss_pub/role-user';
import { AddRoleUserViewControl } from 'src/projects/views/buss_pub/role-user/change-role-user';
import { AddRoleViewControl } from 'src/projects/views/buss_pub/role/change-role';
import { RoleViewControl } from 'src/projects/views/buss_pub/role/index';
import { AjaxJsonRpcLoadingFactory } from './../business/components/buss-components/ajax-loading/index';
import { IntelligentElderlyCareApplication } from './app';
import { AppServiceUtility } from './app/appService';
import { PermissionList } from './app/permission';
import { IAssessmentProjectService } from './models/assessment-project';
import { IAssessmentTemplateService } from './models/assessment-template';
import { nursingTemplateService } from './models/nursing-template';
import { IPersonOrgManageService } from './models/person-org-manage';
import { IRequirementTemplateService } from './models/requirement-template';
import { remote } from './remote/index';
import { billRouter } from './router/bill';
import { ROUTE_PATH, selectedKeys } from './router/index';
import { researchRouter } from './router/research/index';
import { subsidyRouter } from './router/subsidy/index';
import './style/default.less';
import { AccountDetailsViewControl } from './views/buss_iec/account-recharge/account-details';
import { AccountListViewControl } from './views/buss_iec/account-recharge/index';
import { ChangeAssessmentProjectViewControl } from './views/buss_iec/assessment-project/change-assessment-project';
import { AssessmentProjectViewControl } from './views/buss_iec/assessment-project/index';
import { ChangeAssessmentTemplateViewControl } from './views/buss_iec/assessment-template/change-assessment-template';
import { AssessmentTemplateViewControl } from './views/buss_iec/assessment-template/index';
import { ChangeBehavioralCompetenceAssessmentViewControl } from './views/buss_iec/behavioral_competence_assessment/change-behavioral-competence-assessment';
import { BehavioralCompetenceAssessmentViewControl } from './views/buss_iec/behavioral_competence_assessment/index';
import { NursingGradeViewControl } from './views/buss_iec/behavioral_competence_assessment/nursing-grade';
import { ChangeCompetenceAssessmentViewControl } from './views/buss_iec/competence_assessment/change-competence-assessment';
import { CompetenceAssessmentViewControl } from './views/buss_iec/competence_assessment/index';
import { ActivityInformationViewControl } from './views/buss_mis/activity-manage/activityInformation';
import { ActivityParticipateViewControl } from './views/buss_mis/activity-manage/activityParticipate';
import { ActivityPublishViewControl } from './views/buss_mis/activity-manage/activityPublish';
import { ActivityRoomViewControl } from './views/buss_mis/activity-manage/activityRoom';
import { ActivityRoomReservationViewControl } from './views/buss_mis/activity-manage/activityRoomReservation';
import { ActivitySignInViewControl } from './views/buss_mis/activity-manage/activitySignIn';
import { ActivityTypeViewControl } from './views/buss_mis/activity-manage/activityType';
import { ChangeActivityPublishViewControl } from './views/buss_mis/activity-manage/changeActivityPublish';
import { ChangeActivityRoomViewControl } from './views/buss_mis/activity-manage/changeActivityRoom';
import { ChangeActivityRoomReservationViewControl } from './views/buss_mis/activity-manage/changeActivityRoomReservation';
import { ChangeActivityTypeViewControl } from './views/buss_mis/activity-manage/changeActivityType';
// import { addNursingControl } from './views/buss_mis/add-nursing';
import { addNursingTemplateControl } from './views/buss_mis/add-nursing-template';
import { AdministrationDivisionViewControl } from './views/buss_mis/administration-division-manage';
import { ChangeAdministrationDivisionViewControl } from './views/buss_mis/administration-division-manage/change-admin-division';
import { AllowanceReportDetailsViewControl } from './views/buss_mis/allowance-report/allowance-report-details';
import { AllowanceReportMonthViewControl } from './views/buss_mis/allowance-report/allowance-report-month';
// import { AllowanceReportMonthViewControl } from './views/buss_mis/allowance-report/allowance-report-month';
// import { AllowanceReportDetailsViewControl } from './views/buss_mis/allowance-report/allowance-report-details';
import { WorkPeopleStatisticsViewControl } from './views/buss_mis/allowance-report/work-people-statistics';
import { AnnouncementViewControl } from './views/buss_mis/announcement';
import { AddTargetDetailsViewControl } from './views/buss_mis/announcement/add-targetDetails';
import { ChangeAnnouncementViewControl } from './views/buss_mis/announcement/change-announcement';
import { PictureManageViewControl } from './views/buss_mis/announcement/picture-manage';
import { RatingtargetDetailsViewControl } from './views/buss_mis/announcement/ratingtarget-details';
import { RatingtargetSettingViewControl } from './views/buss_mis/announcement/ratingtarget-setting';
import { BaseSituationControl } from './views/buss_mis/base-situation-manage/index';
import { ChangeBusinessAreaViewControl } from './views/buss_mis/business-area-manage/change-business-area';
import { BusinessAreaManageViewControl } from './views/buss_mis/business-area-manage/index';
import { ElderCareRecordViewControl } from './views/buss_mis/care-manage/care-record';
import { CareTypeSettingViewControl } from './views/buss_mis/care-manage/type-setting';
import { ChangeChargeTypeViewControl } from './views/buss_mis/charge-type/change-charge-type';
import { ChargeTypeViewControl } from './views/buss_mis/charge-type/index';
import { CharitableAccountDetailsViewControl } from './views/buss_mis/charitable/charitable-account/charitable-account-details';
import { CharitableAccountViewControl } from './views/buss_mis/charitable/charitable-account/index';
import { CharitableAppropriationViewControl } from './views/buss_mis/charitable/charitable-appropriation/';
import { ChangeCharitableAppropriationViewControl } from './views/buss_mis/charitable/charitable-appropriation/change-charitable-appropriation';
import { ChangeCharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/change-charitable-donate';
import { CharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/index';
import { ViewCharitableDonateViewControl } from './views/buss_mis/charitable/charitable-donate/view-charitable-donate';
import { CharitableInformationViewControl } from './views/buss_mis/charitable/charitable-information';
import { ChangeCharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/change-charitable-project';
import { CharitableProjectListViewControl } from './views/buss_mis/charitable/charitable-project/charitable-project-list';
import { CharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/index';
import { ViewCharitableProjectViewControl } from './views/buss_mis/charitable/charitable-project/view-charitable-project';
import { CharitableSocietyViewControl } from './views/buss_mis/charitable/charitable-society';
import { CharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund';
import { ChangeCharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund/change-title-fund';
import { ViewCharitableTitleFundViewControl } from './views/buss_mis/charitable/charitable-title-fund/view-charitable-title-fund-list';
import { OrganizationApplyViewControl } from './views/buss_mis/charitable/recipient-apply/organization-apply';
import { OrganizationApplyListViewControl } from './views/buss_mis/charitable/recipient-apply/organization-apply-list';
import { RecipientApplyViewControl } from './views/buss_mis/charitable/recipient-apply/recipient-apply';
import { RecipientApplyListViewControl } from './views/buss_mis/charitable/recipient-apply/recipient-apply-list';
import { ViewRecipientApplyViewControl } from './views/buss_mis/charitable/recipient-apply/view-recipient-apply-list';
import { VolunteerApplyViewControl } from './views/buss_mis/charitable/volunteer-apply';
import { VolunteerApplyListViewControl } from './views/buss_mis/charitable/volunteer-apply/volunteer-apply-list';
import { VolunteerServiceCategoryViewControl } from './views/buss_mis/charitable/volunteer-service-category';
import { ChangeVolunteerServiceCategoryViewControl } from './views/buss_mis/charitable/volunteer-service-category/change-volunteer-service-category';
import { CostStatisticsViewControl } from './views/buss_mis/cost-manage/cost-statistics';
import { OrderAccountingViewControl } from './views/buss_mis/cost-manage/order-accounting';
import { DataImportControl } from './views/buss_mis/data-import/index';
import { DayCareOrderListViewControl } from './views/buss_mis/day-care/order-list';
import { DayCareOrderStatisticsViewControl } from './views/buss_mis/day-care/order-statistics';
import { DepositSettingViewControl } from './views/buss_mis/deposit-setting';
import { ChangeDepositViewControl } from './views/buss_mis/deposit-setting/change-deposit';
import { ChangeDeviceViewControl } from './views/buss_mis/device/change-device';
import { DeviceLogViewControl } from './views/buss_mis/device/device-log';
import { DeviceViewControl } from './views/buss_mis/device/index';
import { AddOperationSituationControl } from './views/buss_mis/file-manage/add-operation-situation';
import { OrganizationManageViewControl } from './views/buss_mis/file-manage/organization-manage';
import { CharitySettlementViewControl } from './views/buss_mis/financial-manage/charity-settlement/index';
import { ChangeAccountViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-account';
import { ChangeAccountBookViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-account-book';
import { ChangeSquenceControl } from './views/buss_mis/financial-manage/financial-account-book-manage/change-squence';
import { FinancialAccountViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/index-account';
import { FinancialAccountBookViewControl } from './views/buss_mis/financial-manage/financial-account-book-manage/index-account-book';
import { ChangePaymentViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-payment';
import { ChangeReceiveViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-receive';
import { ChangeAccountReturnViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-return';
import { ChangeTransferViewControl } from './views/buss_mis/financial-manage/financial-account-manage/change-transfer';
import { FinancialAccountFlowViewControl } from './views/buss_mis/financial-manage/financial-account-manage/index';
import { ChangeAccountVoucherViewControl } from './views/buss_mis/financial-manage/financial-account-voucher-manage/change';
import { FinancialVoucherViewControl } from './views/buss_mis/financial-manage/financial-account-voucher-manage/index';
// import { FinancialBankReconciliationViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/index';
import { BillRecodeViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/bill_recode';
import { FinancialReconciliationRecordViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/index-record';
import { UnFinancialReconciliationRecordViewControl } from './views/buss_mis/financial-manage/financial-bank-reconciliation/unband-reconciliation';
import { ChangeCollectorChargeViewControl } from './views/buss_mis/financial-manage/financial-collector/collector-charge';
import { ChangeCollectorPaymentViewControl } from './views/buss_mis/financial-manage/financial-collector/collector-payment';
import { FinancialChargeRecordViewControl } from './views/buss_mis/financial-manage/financial-collector/index';
import { ChangeFinancialSubjectViewControl } from './views/buss_mis/financial-manage/financial-subject-manage/change';
import { FinancialSubjectViewControl } from './views/buss_mis/financial-manage/financial-subject-manage/index';
import { incomeAssessedSettlementViewControl } from './views/buss_mis/financial-manage/income-assessed-settlement/income-assessed-settlement';
import { subsidyServiceProviderViewControl } from './views/buss_mis/financial-manage/income-assessed-settlement/subsidy-service-provider';
import { PlatformSettlementViewControl } from './views/buss_mis/financial-manage/platform-settlement/index';
import { ServicePersonnelSettlementViewControl } from './views/buss_mis/financial-manage/service-personnel-settlement/index';
import { ServiceProviderSettlementsViewControl } from './views/buss_mis/financial-manage/service-provider-settlement/index';
import { AddAllergyViewControl } from './views/buss_mis/health-care/add-allergy';
import { AddDiseaseViewControl } from './views/buss_mis/health-care/add-disease';
import { AddMedicineViewControl } from './views/buss_mis/health-care/add-medicine';
import { AllergyFileViewControl } from './views/buss_mis/health-care/allergy-file';
import { DiseaseFileViewControl } from './views/buss_mis/health-care/disease-file';
import { MedicineFileViewControl } from './views/buss_mis/health-care/medicine-file';
import { OrderAllJViewControl } from './views/buss_mis/home-service/order-all';
import { OrderStatisticsViewControl } from './views/buss_mis/home-service/order-statistics';
import { AnnouncementListViewControl } from './views/buss_mis/information-manage/announcement-manage/announcement-list';
import { AnnouncementIssueViewControl } from './views/buss_mis/information-manage/announcement-manage/index';
import { ChangeArticleAuditControl } from './views/buss_mis/information-manage/article-audit/article-audit';
import { ArticleAuditViewControl } from './views/buss_mis/information-manage/article-audit/audit-list';
import { ChangeArticleTypeViewControl } from './views/buss_mis/information-manage/article-type/change-article-type';
import { ArticleTypeViewControl } from './views/buss_mis/information-manage/article-type/index';
import { CommentAuditViewControl } from './views/buss_mis/information-manage/comment-manage/audit-list';
import { CommentViewControl } from './views/buss_mis/information-manage/comment-manage/comment';
import { ChangeCommentAuditControl } from './views/buss_mis/information-manage/comment-manage/comment-audit';
import { CommentListViewControl } from './views/buss_mis/information-manage/comment-manage/index';
import { ChangeCommentTypeViewControl } from './views/buss_mis/information-manage/comment-type/change-comment-type';
import { CommentTypeViewControl } from './views/buss_mis/information-manage/comment-type/index';
import { ChangeNewsViewControl } from './views/buss_mis/information-manage/news-manage/change-news';
import { ChangeNewsShViewControl } from './views/buss_mis/information-manage/news-manage/change-news-sh';
import { NewsListViewControl } from './views/buss_mis/information-manage/news-manage/index';
import { CallSettingsViewControl } from './views/buss_mis/intelligent-monitoring/call-settings';
import { CareManageViewControl } from './views/buss_mis/intelligent-monitoring/care-manage';
import { ElectronicFenceViewControl } from './views/buss_mis/intelligent-monitoring/electronic-fence';
import { GuardianshipOperationViewControl } from './views/buss_mis/intelligent-monitoring/guardianship-operation';
import { OperationMapViewControl } from './views/buss_mis/intelligent-monitoring/operation-map';
import { ChangeMonitorViewControl } from './views/buss_mis/monitor/change-monitor';
import { MonitorViewControl } from './views/buss_mis/monitor/index';
import { NursingPayListViewControl } from './views/buss_mis/nursing-pay';
import { ChangeNursingPayViewControl } from './views/buss_mis/nursing-pay/change-nursing-pay';
import { nursingRecordControl } from './views/buss_mis/nursing-record';
import { officialAccountStatisticsControl } from './views/buss_mis/official-account-manage';
import { OlderBirthNotifyControl } from './views/buss_mis/older-birth-notify';
import { OperateSituationStatisticViewControl } from './views/buss_mis/operate-situation-statistic/index';
// import { townStreetSettlementViewControl } from './views/buss_mis/social-work-bureau';
// import { AccountListViewControl } from './views/buss_iec/account-recharge/index';
import { OrderReportControl } from './views/buss_mis/order-report/order-report';
import { OrgInfoRecordControl } from './views/buss_mis/org-info-record';
import { ChangeOrgInfoRecordViewControl } from './views/buss_mis/org-info-record/change-info-record';
import { AddQuestionnaireViewControl } from './views/buss_mis/returnVisit-questionnaire/add-questionnaire';
import { AddProjectViewControl } from './views/buss_mis/returnVisit-questionnaire/add-subject';
import { QuestionnaireListViewControl } from './views/buss_mis/returnVisit-questionnaire/questionnaire-list';
import { AddReturnVisitControl } from './views/buss_mis/returnVisit-questionnaire/return-visit';
import { ReturnVisitListViewControl } from './views/buss_mis/returnVisit-questionnaire/returnVisit-list';
import { SubjectListViewControl } from './views/buss_mis/returnVisit-questionnaire/subject-setting';
import { ServiceEvaluateViewControl } from './views/buss_mis/service-operation-manage/service-evaluate-manage';
import { ChangeServiceItemViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/change-service-item';
import { ChangeServiceItemPackageViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/change-service-item-package';
import { ServiceProductShControl } from './views/buss_mis/service-operation-manage/service-item-manage/change-service-item-package-sh';
import { ServiceItemViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/index';
import { ServiceItemPackageViewControl } from './views/buss_mis/service-operation-manage/service-item-manage/service-item-package';
import { ServiceLedgerViewControl } from './views/buss_mis/service-operation-manage/service-ledger-manage/index';
import { ChangeServiceOptionViewControl } from './views/buss_mis/service-operation-manage/service-option-manage/change-service-option';
import { ServiceOptionViewControl } from './views/buss_mis/service-operation-manage/service-option-manage/index';
import { ChangeServiceOrderViewControl } from './views/buss_mis/service-operation-manage/service-order-manage/change-service-order';
import { ServiceOrderViewControl } from './views/buss_mis/service-operation-manage/service-order-manage/index';
import { ServiceOrderTaskViewControl } from './views/buss_mis/service-operation-manage/service-order-task-manage/index';
import { ServicePersonalApplyViewControl } from './views/buss_mis/service-operation-manage/service-personal-apply';
import { ServicePersonReviewedViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/apply-reviewed';
import { ChangeServicePersonalViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/change-service-personal';
import { ServicePersonalViewControl } from './views/buss_mis/service-operation-manage/service-personal-manage/index';
import { ServiceProjectControl } from './views/buss_mis/service-operation-manage/service-project-buy/index';
import { ServerProjectBuyControl } from './views/buss_mis/service-operation-manage/service-project-buy/service-project-buy';
import { ServiceProjectDetailControl } from './views/buss_mis/service-operation-manage/service-project-buy/service-project-detail';
import { ChangeServiceProjectViewControl } from './views/buss_mis/service-operation-manage/service-project-manage/change-service-project';
import { ServiceProjectViewControl } from './views/buss_mis/service-operation-manage/service-project-manage/index';
import { ApplyServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-apply';
import { ServiceProviderReviewedViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/apply-reviewed';
import { ChangeServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/change-service-provider';
import { ServiceProviderViewControl } from './views/buss_mis/service-operation-manage/service-provider-manage/index';
import { ChangeServiceRecordViewControl } from './views/buss_mis/service-operation-manage/service-record-manage/change-service-record';
import { ServiceRecordViewControl } from './views/buss_mis/service-operation-manage/service-record-manage/index';
import { ServiceReturnVisitListViewControl } from './views/buss_mis/service-operation-manage/service-return-visit-manage';
import { ChangeServicesScopeViewControl } from './views/buss_mis/service-operation-manage/service-scope-manage/change-service-scope';
import { ServiceScopeViewControl } from './views/buss_mis/service-operation-manage/service-scope-manage/index';
import { ChangeServiceTypeViewControl } from './views/buss_mis/service-operation-manage/service-type-manage/change-service-type';
import { ServiceTypeViewControl } from './views/buss_mis/service-operation-manage/service-type-manage/index';
import { ChangeServiceWokerViewControl } from './views/buss_mis/service-operation-manage/service-woker-manage/change-service-woker';
import { ServiceWokerViewControl } from './views/buss_mis/service-operation-manage/service-woker-manage/index';
import { ChangeServicesItemCategoryViewControl } from './views/buss_mis/service-operation-manage/services-item-category-manage/change-services-item-category';
import { ServicesItemCategoryViewControl } from './views/buss_mis/service-operation-manage/services-item-category-manage/index';
import { serviceRecordSettlementViewControl } from './views/buss_mis/service-record-detail-Settlement';
import { ServicePersonalSettlementViewControl } from './views/buss_mis/service-settlement/personal-settlement';
import { ServiceProviderSettlementViewControl } from './views/buss_mis/service-settlement/provide-settlement';
import { SocialGroupsTypeViewControl } from './views/buss_mis/social-groups-type-manage';
import { ChangeSocialGroupsTypeViewControl } from './views/buss_mis/social-groups-type-manage/change-social-groups-type';
import { townStreetSettlementViewControl } from './views/buss_mis/social-work-bureau';
import { CheckManageViewControl } from './views/buss_mis/statistics-manage/check-manage';
import { CheckInElderViewControl } from './views/buss_mis/statistics-manage/checkIn-elder';
import { ComprehensiveStatisticsViewControl } from './views/buss_mis/statistics-manage/comprehensive-statistics';
import { OrganizationImformationViewControl } from './views/buss_mis/statistics-manage/organization-information';
import { SysbbViewControl } from './views/buss_mis/sysbb';
import { ChangeTaskViewControl } from './views/buss_mis/tasks/change-task';
import { ChangeTaskServiceOrderViewControl } from './views/buss_mis/tasks/change-task-service-order';
import { ChangeTaskTypeViewControl } from './views/buss_mis/tasks/change-task-type';
import { TaskListViewControl } from './views/buss_mis/tasks/task-list';
import { TaskServiceOrderViewControl } from './views/buss_mis/tasks/task-service-order';
import { TaskToBeProcessViewControl } from './views/buss_mis/tasks/task-to-be-process';
import { TaskToBeSendViewControl } from './views/buss_mis/tasks/task-to-be-send';
import { TaskTypeListViewControl } from './views/buss_mis/tasks/task-type-list';
import { ChangeThingArchivesViewControl } from './views/buss_mis/thing-manage/change-thing-archives';
import { ThingArchivesViewControl } from './views/buss_mis/thing-manage/thing-archives';
import { ThingSortViewControl } from './views/buss_mis/thing-manage/thing-sort';
import { UnitsViewControl } from './views/buss_mis/thing-manage/units';
import { ChangeTransactionViewControl } from './views/buss_mis/transaction-management/change-transaction';
import { TransactionManageViewControl } from './views/buss_mis/transaction-management/index';
import { TransactionCommentViewControl } from './views/buss_mis/transaction-management/index-comment';
import { AppPageConfigListViewControl } from './views/buss_pub/app-page-config';
import { ChangeAppPageConfigViewControl } from './views/buss_pub/app-page-config/change-app-page-config';
import { BuyServiceViewControl } from './views/buss_pub/buy-service/index';
import { DeductionListViewControl } from './views/buss_pub/deduction-manage';
import { ChangeDeductionViewControl } from './views/buss_pub/deduction-manage/change-deduction';
import { ElderFoodListViewControl } from './views/buss_pub/elder-food';
import { ChangeEdlerFoodViewControl } from './views/buss_pub/elder-food/change-elder-food';
import { ElderHealthyListViewControl } from './views/buss_pub/elder-healthy';
import { MessageListNewViewControl } from './views/buss_pub/message-list/index';
import { ChangeMessageViewControl } from './views/buss_pub/message-manage/change-message';
import { ChangeMessageTypeViewControl } from './views/buss_pub/message-manage/change-message-type';
import { MessageListViewControl } from './views/buss_pub/message-manage/index';
import { MessageTypeListViewControl } from './views/buss_pub/message-manage/message-type-list';
import { ChangeMonthSalaryStatisticViewControl } from './views/buss_pub/month-salary-statistic/change-month-salary-statistic';
import { MonthSalaryStatisticViewControl } from './views/buss_pub/month-salary-statistic/index';
import { PersonnelClassificationViewControl } from './views/buss_pub/personnel-classification';
import { ChangePersonnelClassificationViewControl } from './views/buss_pub/personnel-classification/change-personnel-classification';
import { ChangeHappinessViewControl } from './views/buss_pub/personnel-organization-management/happiness-manage/change-organization';
import { ChangeOrganizationViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/change-organization';
import { OrganizationViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/index';
import { OrganizationXFYViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/organization-xfy';
import { AllPersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/all-personnel';
import { ChangePersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/change-personnel';
import { ChangePersonnelWorkerViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/change-personnel-worker';
import { PersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/index';
import { WorkerPersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/worker-personnel';
// import { ChangePlateformViewControl } from './views/buss_pub/personnel-organization-management/plateform-manage/change-organization';
import { PersonnelPermissionControl } from './views/buss_pub/personnel-permission/index';
import { RatingListViewControl } from './views/buss_pub/rating';
import { RatingPlanListViewControl } from './views/buss_pub/rating-plan';
import { ChangeRatingPlanViewControl } from './views/buss_pub/rating-plan/change-rating-plan';
import { ChangeRatingViewControl } from './views/buss_pub/rating/change-rating';
import { ReserveBuyServiceEditViewControl } from './views/buss_pub/reserve-buy-service/change-reserve-buy-service';
import { ReserveBuyServiceViewControl } from './views/buss_pub/reserve-buy-service/index';
import { ServiceSettlementStatementViewControl } from './views/buss_pub/service-settlement-statement/index';
import { SetClassListViewControl } from './views/buss_pub/set-class';
import { SetClassTypeListViewControl } from './views/buss_pub/set-class-type';
import { ChangeSetClassTypeViewControl } from './views/buss_pub/set-class-type/change-set-class-type';
import { ChangeSetClassViewControl } from './views/buss_pub/set-class/change-set-class';
import { SubsidyAutoRechargeViewControl } from './views/buss_pub/subsidy-auto-recharge-manage';
import { UserEditorViewControl } from './views/buss_pub/user-editor/index';
import { UserManageViewControl } from './views/buss_pub/user-manage/index';
import { HomeViewControl } from './views/home/index';
import { ChangeFeeViewControl } from './views/welfare-institution//room-change/change-fee';
import { FeeListControl } from './views/welfare-institution//room-change/fee-list';
import { ChangeBedManageViewControl } from './views/welfare-institution/bed-manage/change-bed-manage';
import { BedManageViewControl } from './views/welfare-institution/bed-manage/index';
import { CheckInDetailViewControl } from './views/welfare-institution/check-in/check-in-detail';
import { CheckInListViewControl } from './views/welfare-institution/check-in/check-in-list';
import { CheckInListViewJjControl } from './views/welfare-institution/check-in/check-in-list-special';
import { CheckInNewViewControl } from './views/welfare-institution/check-in/index';
import { CheckInNewJjViewControl } from './views/welfare-institution/check-in/index-special';
// import { DiaperCostManageViewControl } from './views/welfare-institution/cost-manage/diaper-cost-index';
import { DiaperCostViewControl } from './views/welfare-institution/cost-manage/change-diaper-cost';
import { FoodCostViewControl } from './views/welfare-institution/cost-manage/change-food-cost';
import { ChangeIncidentalViewControl } from './views/welfare-institution/cost-manage/change-incidental';
// import { MedicalCostManageViewControl } from './views/welfare-institution/cost-manage/medical-cost-index';
import { MedicalCostViewControl } from './views/welfare-institution/cost-manage/change-medical-cost';
import { FoodCostManageViewControl } from './views/welfare-institution/cost-manage/food-cost-index';
import { IncidentalManageViewControl } from './views/welfare-institution/cost-manage/incidental-index.';
import { costAccountingViewControl } from './views/welfare-institution/costAccounting/index';
import { ChangeHospitalViewControl } from './views/welfare-institution/hospital/change-hospital';
import { hospitalArrearsViewControl } from './views/welfare-institution/hospital/index';
import { HydropowerViewControl } from './views/welfare-institution/hotel/hotel-hydropower';
import { ChangeHydropowerViewControl } from './views/welfare-institution/hotel/hotel-hydropower/change-hydropower';
import { ChangeHydropowerJjViewControl } from './views/welfare-institution/hotel/hotel-hydropower/change-hydropower-jiujiang';
import { HydropowerJjViewControl } from './views/welfare-institution/hotel/hotel-hydropower/index-jiujiang';
import { ChangeHotelZoneControl } from './views/welfare-institution/hotel/hotel-zone-manage/change-hotel-zone';
import { HotelZoneViewControl } from './views/welfare-institution/hotel/hotel-zone-manage/index';
import { ChangeHotelZoneTypeViewControl } from './views/welfare-institution/hotel/hotel-zone-type-manage/change-hotel-zone-type';
import { HotelZoneTypeViewControl } from './views/welfare-institution/hotel/hotel-zone-type-manage/index';
import { ChangeLeaveReasonViewControl } from './views/welfare-institution/leave-reason/change-leave-reason';
import { LeaveReasonViewControl } from './views/welfare-institution/leave-reason/index';
import { LeaveRecordViewControl } from './views/welfare-institution/leave-record/index';
import { LeaveEditViewControl } from './views/welfare-institution/leave-record/leave-edit';
import { LeaveInsertViewControl } from './views/welfare-institution/leave-record/leave-insert';
import { ChangeNursingArchivesViewControl } from './views/welfare-institution/nursing-archives/change-nursing-archives';
import { NursingTypeEditViewControl } from './views/welfare-institution/nursing-archives/change-nursing-type';
import { NursingArchivesViewControl } from './views/welfare-institution/nursing-archives/index';
import { NursingRelationShipEditViewControl } from './views/welfare-institution/nursing-archives/nursing-relationship-edit';
import { NursingRelationShipViewControl } from './views/welfare-institution/nursing-archives/nursing-relationship-list';
import { NursingTypeViewControl } from './views/welfare-institution/nursing-archives/nursing-type';
import { ChangeNursingItemViewControl } from './views/welfare-institution/nursing-item/change-nursing-item';
import { NursingItemViewControl } from './views/welfare-institution/nursing-item/index';
import { AdoptionSituationViewControl } from './views/welfare-institution/report-manage/adoption-situation';
import { BasicFacilitiesViewControl } from './views/welfare-institution/report-manage/basic-facilities';
import { CompensationViewControl } from './views/welfare-institution/report-manage/compensation';
import { elderCheckViewControl } from './views/welfare-institution/report-manage/elder-check';
import { elderlyResidentsViewControl } from './views/welfare-institution/report-manage/elderly-residents';
import { FireQualificationViewControl } from './views/welfare-institution/report-manage/fire-qualification';
import { happyStaffStatisticsViewControl } from './views/welfare-institution/report-manage/happy-staff-statistics';
import { OperationStatusViewControl } from './views/welfare-institution/report-manage/operation-status';
import { OrganizationReportViewControl } from './views/welfare-institution/report-manage/organization-report';
import { staffStatisticsViewControl } from './views/welfare-institution/report-manage/staff-statistics';
import { WorkerConstituteViewControl } from './views/welfare-institution/report-manage/worker-constitute';
import { ChangeRequirementOptionViewControl } from './views/welfare-institution/requirement/requirement-option-manage/change-requirement-option';
import { RequirementOptionViewControl } from './views/welfare-institution/requirement/requirement-option-manage/index';
import { ChangeRequirementProjectViewControl } from './views/welfare-institution/requirement/requirement-project-manage/change-requirement-project';
import { RequirementProjectViewControl } from './views/welfare-institution/requirement/requirement-project-manage/index';
import { ChangeRequirementRecordViewControl } from './views/welfare-institution/requirement/requirement-record-manage/change-requirement-record';
import { RequirementRecordViewControl } from './views/welfare-institution/requirement/requirement-record-manage/index';
import { ChangeRequirementTemplateViewControl } from './views/welfare-institution/requirement/requirement-template-manage/change-requirement-template';
import { RequirementTemplateViewControl } from './views/welfare-institution/requirement/requirement-template-manage/index';
import { ChangeRequirementTypeViewControl } from './views/welfare-institution/requirement/requirement-type-manage/change-requirement-type';
import { RequirementTypeViewControl } from './views/welfare-institution/requirement/requirement-type-manage/index';
import { ReservationRegistrationViewControl } from './views/welfare-institution/reservation-registration/index';
import { ChangereservationRegistrationViewControl } from './views/welfare-institution/reservation-registration/reservation-registration';
import { ChangeReservationViewControl } from './views/welfare-institution/reservation-registration/reservation-registration-info';
import { ChangeRoomArchivesViewControl } from './views/welfare-institution/room-archives/change-room-archives';
import { RoomArchivesViewControl } from './views/welfare-institution/room-archives/index';
import { RoomListControl } from './views/welfare-institution/room-change';
import { RoomChangeViewControl } from './views/welfare-institution/room-change/change-room';
import { RoomStatusViewControl } from './views/welfare-institution/room-status/index';
import { olderAbilityAssessmentViewControl } from './views/buss_mis/old-ability-assessment';
import { InputManageViewControl } from './views/buss_mis/statistics-manage/input-manage';
import { EvaluationManageViewControl } from './views/buss_mis/statistics-manage/evaluation-manage';
import { ChangeGroupsViewControl } from './views/buss_pub/groups-manage/change-groups';
import { GroupsListViewControl } from './views/buss_pub/groups-manage';
import { ChangeActivityShViewControl } from './views/buss_mis/activity-manage/changeActivitySh';
import { ChangeServicePlanViewControl } from './views/buss_mis/service-plan/change-service-plan';
import { ServicePlanListViewControl } from './views/buss_mis/service-plan';
import { nursingContentControl } from './views/buss_mis/nursing-manage/nursing-content';
import { AddNursingContentViewControl } from './views/buss_mis/nursing-manage/add-nursing-content';
import { ElderBursingRecordControl } from './views/buss_mis/nursing-manage/elder_nursing_record';
import { AddNursingRecordViewControl } from './views/buss_mis/nursing-manage/add-nursing-record';
import { EmployeesViewControl } from './views/buss_mis/statistics-manage/employees-manage';
// import { SheQuWorkerListViewControl } from './views/buss_pub/personnel-organization-management/happiness-manage/work-personnel-entry';
import { OperationHappinessViewControl } from './views/buss_mis/statistics-manage/operation-happiness';
import { AcceptancRatingViewControl } from './views/buss_mis/statistics-manage/AcceptancRating';
import { JiGouWorkerListViewControl } from './views/buss_pub/personnel-organization-management/organization-manage/work-personnel-entry';
import { ChangeServiceSituationViewControl } from './views/buss_mis/elder-manage/shegong-manage/change-service-situation';
import { ServiceSituationListViewControl } from './views/buss_mis/elder-manage/shegong-manage';
import { ElderInfoListViewControl } from './views/buss_mis/elder-manage/essential-information/elder-info-list';
import { ChangeElderInfoViewControl } from './views/buss_mis/elder-manage/essential-information/change-elder-info';
// import { AllowanceCheckViewControl } from './views/buss_mis/allowance-manage/allowance-check';
import { VisitSituationListViewControl } from './views/buss_mis/elder-manage/shegong-manage/visit-situation-list';
import { ChangeVisitSituationViewControl } from './views/buss_mis/elder-manage/shegong-manage/change-visit-situation';
import { WaterElectricityViewControl } from './views/welfare-institution/hotel/hotel-hydropower/water-electricity';
import { OtherCostViewControl } from './views/welfare-institution/cost-manage/other-cost';
import { CostTypeViewControl } from './views/welfare-institution/cost-manage/cost-type';
// import { ChangeDeviceViewControl } from './views/buss_mis/device/change-device';
// import { OrderStatisticsViewControl } from './views/buss_mis/home-service/order-statistics';
// import { WorkerPersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/worker-personnel';
// import { AllPersonnelViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/all-personnel';
// import { UnitsViewControl } from './views/buss_mis/thing-manage/units';
// import { ThingSortViewControl } from './views/buss_mis/thing-manage/thing-sort';
// import { ThingArchivesViewControl } from './views/buss_mis/thing-manage/thing-archives';
// import { AddArchivesViewControl } from './views/buss_mis/thing-manage/add-archives';
// import { ChangePersonnelWorkerViewControl } from './views/buss_pub/personnel-organization-management/personnel-manage/change-personnel-worker';
// import { MedicineFileViewControl } from './views/buss_mis/health-care/medicine-file';
// import { AddMedicineViewControl } from './views/buss_mis/health-care/add-medicine';
// import { DiseaseFileViewControl } from './views/buss_mis/health-care/disease-file';
// import { AddDiseaseViewControl } from './views/buss_mis/health-care/add-disease';
// import { AllergyFileViewControl } from './views/buss_mis/health-care/allergy-file';
// import { AddAllergyViewControl } from './views/buss_mis/health-care/add-allergy';
import { ReceivablesViewControl } from './views/welfare-institution/costAccounting/receivables-list';
import { ActualReceivablesViewControl } from './views/welfare-institution/costAccounting/actual-receivables';
import { BaseinfoServicerViewControl } from './views/buss_mis/statistics-manage/baseinfo-servicer';
import { AppUserManageViewControl } from './views/buss_iec/app-user-manage';
import { ChangeAppUserManageViewControl } from './views/buss_iec/app-user-manage/app-user-detail';
import { PlatformOrganizationListViewControl } from './views/buss_mis/platform-organization';
import { XFYOrganizationListViewControl } from './views/buss_mis/xfy-organization';
import { AddXFYOrganizatonInfoViewControl } from './views/buss_mis/xfy-organization/add-info';
import { DifferenceDetailsViewControl } from './views/welfare-institution/costAccounting/difference-details';
import { AppFeedbackViewControl } from './views/buss_iec/app-feedback';
import { ChangeAppFeedbackViewControl } from './views/buss_iec/app-feedback/change-app-feedback';
import { DealAndReportViewControl } from './views/buss_mis/deal-report';
import { ChangeDealAndReportViewControl } from './views/buss_mis/deal-report/change-deal-report';
import { ChangeYearplanAndSummaryViewControl } from './views/buss_mis/yearplan-summary/change-yearplan-summary';
import { YearplanAndSummaryViewControl } from './views/buss_mis/yearplan-summary';
import { OrgOperationFundViewControl } from './views/buss_mis/org-operation-fund';
import { OrgBedFundViewControl } from './views/buss_mis/org-bed-fund';
import { MonthPlanViewControl } from './views/buss_mis/month-plan';
import { ChangeMonthPlanViewControl } from './views/buss_mis/month-plan/change-month-plan';
import { ChangeAppFriendsCircleViewControl } from './views/buss_iec/app-friends-circle/change-friends-circle';
import { AppFriendsCircleViewControl } from './views/buss_iec/app-friends-circle';
import { ChangeCircleViewControl } from './views/buss_iec/circle-manage/change-circle';
import { CircleViewControl } from './views/buss_iec/circle-manage';
import { AppOnlineTalkViewControl } from './views/buss_iec/app-online-talk';
import { ChangeAppOnlineTalkViewControl } from './views/buss_iec/app-online-talk/change-app-online-talk';
import { AppSettingsViewControl } from './views/buss_iec/app-settings';
import { SeatOperationViewControl } from './views/buss_mis/statistics-manage/seat-operation';
import { BaseinfoOrganViewControl } from './views/buss_mis/statistics-manage/baseinfo-organ';
import { OperationOrganViewControl } from './views/buss_mis/statistics-manage/organ-operation';
import { ChangeElderMealsViewControl } from './views/buss_mis/elder-meals/change-elder-meals';
import { ChangeUnitsViewControl } from './views/buss_mis/thing-manage/change-units';
import { ChangeThingSortViewControl } from './views/buss_mis/thing-manage/change-thing-sort';
import { HappinessStationViewControl } from './views/buss_mis/happiness-station';
import { YearSelfAssessmentListViewControl } from './views/buss_mis/year-self-assessment';
import { ChangeYearSelfAssessmentViewControl } from './views/buss_mis/year-self-assessment/change-year-self-assessment';
import { HappinessYearSelfAssessmentListViewControl } from './views/buss_mis/year-self-assessment/happiness-year-self-assessment';
import { ChangeHappinessYearSelfAssessmentViewControl } from './views/buss_mis/year-self-assessment/change-happiness-year-self-assessment';
import { XxhDeviceViewControl } from './views/buss_mis/device/xxh-device';
import { ChangeFireEquipmentViewControl } from './views/buss_mis/fire-equipment/change-fire-equipment';
import { FireEquipmentViewControl } from './views/buss_mis/fire-equipment';
import { ImportExcelViewControl } from './views/buss_mis/import-excel';
import { CallCenterNhViewControl } from './views/buss_mis/call-center-nh';
import { FirstModifyControl } from 'src/business/views/modify-password/fist_modify';
import { ModifyPasswordPhoneControl } from 'src/business/views/modify-password-phone';
import { JujiaEpidemicPreventionViewControl } from './views/buss_mis/epidemic-prevention/jujia-epidemic-prevention';
import { EpidemicPreventionDetailViewControl } from './views/buss_mis/epidemic-prevention/epidemic-prevention-detail';
import { OrgDayInfoPreventionViewControl } from './views/buss_mis/epidemic-prevention/org-day-info';
import { EditOrgDayInfoViewControl } from './views/buss_mis/epidemic-prevention/edit-org-day-info';
import { ChangeCallCenterZhViewControl } from './views/buss_mis/call-center-nh/change-call-center-zh';
import { CallCenterZhViewControl } from './views/buss_mis/call-center-nh/call-center-zh';
import { SysUseStaticsticsViewControl } from './views/buss_pub/sys-use-staticstics/sys-use-staticstics';
import { ChangeActivityParticipateViewControl } from './views/buss_mis/activity-manage/changeActivityParticipate';
import { DeviceWarningViewControl } from './views/buss_mis/device-warning/device-warning';
import { ChangeDeviceWarningViewControl } from './views/buss_mis/device-warning/change-device-warning';
import { ChangeDeviceManageViewControl } from './views/buss_mis/device-warning/change-device-manage';
import { DeviceManageViewControl } from './views/buss_mis/device-warning/device-manage';
import { ChangeDeviceFileViewControl } from './views/buss_mis/device-warning/change-device-file';
import { RoomStateDiagramViewControl } from './views/welfare-institution/room-state-diagram';
import { AppPrivacyAgreeViewControl } from './views/buss_iec/app-settings/app-privacy-agree';
import { OrganizationManageNewViewControl } from './views/buss_mis/organization-manage';
import { DataDictionaryViewControl } from './views/buss_pub/shopping-mall/data-dictionary';
import { ChangeDataDictionaryViewControl } from './views/buss_pub/shopping-mall/change-data-dictionary';
import { ProductsViewControl } from './views/buss_pub/shopping-mall/products';
import { ChangeProductViewControl } from './views/buss_pub/shopping-mall/change-product';
import { ProductOrdersViewControl } from './views/buss_pub/shopping-mall/product-orders';
import { ChangeProductOrderControl } from './views/buss_pub/shopping-mall/change-product-order';
import { ProductsShViewControl } from './views/buss_pub/shopping-mall/products-sh';
import { ChangeProductShControl } from './views/buss_pub/shopping-mall/change-product-sh';
import { FinancialOverviewViewControl } from '././views/buss_pub/shopping-mall/financial-overview';
import { EntryRecordViewControl } from './views/buss_pub/shopping-mall/entry-record';
import { FinancialDetailControl } from '././views/buss_pub/shopping-mall/financial-detail';
import { BlocksViewControl } from './views/buss_pub/shopping-mall/blocks';
import { ChangeBlockViewControl } from './views/buss_pub/shopping-mall/change-block';
import { BlockDetailsViewControl } from './views/buss_pub/shopping-mall/block-details';
import { ChangeBlockDetailsViewControl } from './views/buss_pub/shopping-mall/change-block-detail';
import { CapitalDetailViewControl } from './views/buss_pub/shopping-mall/capital-detail';
import { SeparateAccountsSHControl } from './views/buss_pub/shopping-mall/separate-accounts-sh';
import { SysUseStaticsticsUserViewControl } from './views/buss_pub/sys-use-staticstics/sys-user-count';
import { ProductPreviewControl } from './views/buss_pub/shopping-mall/product-preview';
import { DeviceMessageViewControl } from './views/buss_mis/intelligent-monitoring/device-message';
import { DeviceLocationViewControl } from './views/buss_mis/intelligent-monitoring/device-location';
import { CallCenterLogViewControl } from './views/buss_mis/call-center-nh/call-center-log';
import { DeviceTermViewControl } from './views/buss_mis/intelligent-monitoring/device-term';

const blank = new BlankMainFormControl();
const layoutMain = 'layoutMainForm';
const manage_logo = require("../static/img/logo.png");
const backstage_nain_form_logo = require("../static/img/logo.png");

const callCenterInstance = createObject(
    CallCenterViewControl,
    {});

// 活动管理
const activity_manage = [
    { key: 'activityTypeManage', link: ROUTE_PATH.activityTypeManage, title: '活动类型', permission: { 'permission': '活动类型查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityTypeManage', link: ROUTE_PATH.activityTypeManageLook, title: '活动类型', permission: { 'permission': '查看活动类型查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityManage', link: ROUTE_PATH.activityManage, title: '活动列表', permission: { 'permission': '活动列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    // { key: 'activityManageTs', link: ROUTE_PATH.activityManageTs, title: '活动推送列表', permission: { 'permission': '活动推送列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityManage', link: ROUTE_PATH.activityManageLook, title: '活动列表', permission: { 'permission': '查看活动列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityParticipate', link: ROUTE_PATH.activityParticipate, title: '报名列表', permission: { 'permission': '报名列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityParticipate', link: ROUTE_PATH.activityParticipateLook, title: '报名列表', permission: { 'permission': '查看报名列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activitySignIn', link: ROUTE_PATH.activitySignIn, title: '签到列表', permission: { 'permission': '签到列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activitySignIn', link: ROUTE_PATH.activitySignInLook, title: '签到列表', permission: { 'permission': '查看签到列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityRoom', link: ROUTE_PATH.activityRoom, title: '活动室列表', permission: { 'permission': '活动室列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'activityRoom', link: ROUTE_PATH.activityRoomLook, title: '活动室列表', permission: { 'permission': '查看活动室列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    // { key: 'activityRoomReservation', link: ROUTE_PATH.activityRoomReservation, title: '活动室预约', permission: { 'permission': '活动室预约列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    // { key: 'activityRoomReservationRecord', link: ROUTE_PATH.activityRoomReservationRecord, title: '活动室预约记录', permission: { 'permission': '活动室预约列表查询' }, icon: 'font@pao_font_icon_user_bright' },
    // { key: 'activityInformation', link: ROUTE_PATH.activityInformation, title: '活动信息汇总', permission: { 'permission': '活动信息查询' }, icon: 'font@pao_font_icon_user_bright' }
];
// // 信息管理
// const news_manage = [
//     { key: 'articleType', link: ROUTE_PATH.articleType, title: '资讯类型', permission: { 'permission': '文章类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'articleType', link: ROUTE_PATH.articleTypeLook, title: '查看资讯类型', permission: { 'permission': '查看文章类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentType', link: ROUTE_PATH.commentType, title: '评论类型', permission: { 'permission': '评论类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentType', link: ROUTE_PATH.commentTypeLook, title: '评论类型', permission: { 'permission': '查看评论类型查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'announcementList', link: ROUTE_PATH.announcementList, title: '公告管理', permission: { 'permission': '公告管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'announcementList', link: ROUTE_PATH.announcementListLook, title: '公告管理', permission: { 'permission': '查看公告列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'NewsList', link: ROUTE_PATH.newsList, title: '资讯管理', permission: { 'permission': '新闻管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'NewsList', link: ROUTE_PATH.newsListLook, title: '资讯管理', permission: { 'permission': '查看新闻列表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'NewsList', link: ROUTE_PATH.newsListSh, title: '资讯审核列表', permission: { 'permission': '新闻审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//     // { key: 'articleAudit', link: ROUTE_PATH.articleAudit, title: '文章审核', permission: { 'permission': '文章审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//     // { key: 'articleAudit', link: ROUTE_PATH.articleAuditLook, title: '文章审核', permission: { 'permission': '查看文章审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentList', link: ROUTE_PATH.commentList, title: '评论管理', permission: { 'permission': '评论管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentList', link: ROUTE_PATH.commentListLook, title: '查看评论管理', permission: { 'permission': '查看评论管理查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentAudit', link: ROUTE_PATH.commentAudit, title: '评论审核', permission: { 'permission': '评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'commentAudit', link: ROUTE_PATH.commentAuditLook, title: '评论审核', permission: { 'permission': '查看评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
// ];

// 报表管理
// const report_manage = [
//     { key: 'operationStatus', link: ROUTE_PATH.operationStatus, title: '机构运营情况', permission: { 'permission': '机构运营情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'basicFacilities', link: ROUTE_PATH.basicFacilities, title: '机构基本设施情况', permission: { 'permission': '基本设施情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'adoptionSituation', link: ROUTE_PATH.adoptionSituation, title: '机构收养人员情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification, title: '机构消防资质情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'workerConstitute', link: ROUTE_PATH.workerConstitute, title: '机构职工构成情况', permission: { 'permission': '职工构成情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'compensation', link: ROUTE_PATH.compensation, title: '机构薪酬待遇情况', permission: { 'permission': '薪酬待遇情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'compensation', link: ROUTE_PATH.organizationReport, title: '机构报表情况', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'compensation', link: ROUTE_PATH.staffStatistics, title: '福利院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'compensation', link: ROUTE_PATH.happyStaffStatistics, title: '幸福院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'compensation', link: ROUTE_PATH.elderCheck, title: '福利院长者入住统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
// ];

// 居家服务报表
// const jujiafuwubaobiao = [

//     { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'orderReport', link: ROUTE_PATH.orderReport, title: '订单统计报表', permission: { 'permission': '订单统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'allowanceReportMonth', link: ROUTE_PATH.allowanceReportMonth, title: '居家养老服务对象月报表', permission: { 'permission': '居家养老服务对象月报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'allowanceReportDetails', link: ROUTE_PATH.allowanceReportDetails, title: '居家养老服务对象明细表', permission: { 'permission': '居家养老服务对象明细表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'workPeopleStatistics', link: ROUTE_PATH.workPeopleStatistics, title: '工作人员统计', permission: { 'permission': '工作人员统计查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'allowanceBaseSituation', link: ROUTE_PATH.baseSituation, title: '基本情况统计', permission: { 'permission': '基本情况统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
//     { key: 'xfyFollowStatistics', link: ROUTE_PATH.xfyFollowStatistics, title: '公众号关注数统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
// ];

// 主窗体
const layoutMainForm = new BackstageManageMainFormControl(
    "智慧养老",
    [
        {
            title: '首页', icon: 'notification', link: '', key: 0, permission: "8", childrenComponent: [
                { key: 'home', link: ROUTE_PATH.home, title: '首页', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
            ]
        },
        {
            title: "机构养老", icon: 'notification', link: "", key: 5072, permission: { 'permission': '机构养老菜单' }, childrenComponent: [
                {
                    title: "入住管理", icon: 'notification', link: "", key: 1, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        { key: 'roomStatus', link: ROUTE_PATH.roomStatus, title: '床位全览', permission: { 'permission': '房态图查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'roomStateDiagram', link: ROUTE_PATH.roomStateDiagram, title: '房态图', permission: { 'permission': '房态图2查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'reservation', link: ROUTE_PATH.reservationRegistration, title: '预约登记', permission: { 'permission': '预约登记查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'checkInJj', link: ROUTE_PATH.checkInJj, title: '入住长者', permission: { 'permission': '入住办理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'checkInJjLook', link: ROUTE_PATH.checkInJjLook, title: '入住长者', permission: { 'permission': '查看入住办理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'checkIn', link: ROUTE_PATH.checkInLook, title: '长者入住', permission: { 'permission': '查看长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'leaveReason', link: ROUTE_PATH.leaveReason, title: '离开原因', permission: { 'permission': '离开原因查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'hotelZoneType', link: ROUTE_PATH.hotelZoneType, title: '区域类型', permission: { 'permission': '区域类型查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '区域管理', permission: { 'permission': '区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'hotelZone', link: ROUTE_PATH.hotelZoneLook, title: '区域管理', permission: { 'permission': '查看区域情况查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'bed', link: ROUTE_PATH.bedManage, title: '床位管理', permission: { 'permission': '床位管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'bed', link: ROUTE_PATH.bedManageLook, title: '床位管理', permission: { 'permission': '查看床位情况查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'roomChange', link: ROUTE_PATH.roomList, title: '床位更换', permission: { 'permission': '房间调整查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'researchInsert', link: ROUTE_PATH.leaveRecord, title: '请销假记录', permission: { 'permission': '住宿记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },

                {
                    title: "服务管理", icon: 'notification', link: "", key: 1, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        { key: 'nursingContent', link: ROUTE_PATH.nursingContent, title: '护理服务内容', permission: { 'permission': '护理内容查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'elderNursingRecord', link: ROUTE_PATH.elderNursingRecord, title: '长者护理记录', permission: { 'permission': '长者护理记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                // {
                //     title: "信息审核", icon: 'notification', link: "", key: 88, permission: { permission: '机构养老菜单' }, childrenComponent: [
                //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品审核', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //     ]
                // },
                // 费用管理，开发中...
                {
                    title: "费用管理", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        {
                            title: "弹性费用支出", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                { key: 'otherCost', link: ROUTE_PATH.otherCost, title: '其他费用', permission: { 'permission': '其他费用查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "费用核算", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                { key: 'receivablesList', link: ROUTE_PATH.receivablesList, title: '预收款账单', permission: { 'permission': '应收款账单查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'actualReceivablesList', link: ROUTE_PATH.actualReceivablesList, title: '实收款核算及审核', permission: { 'permission': '实收款账单查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'differenceDetail', link: ROUTE_PATH.differenceDetail, title: '差额明细表', permission: { 'permission': '差额明细表查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]

                        }
                    ]
                },
                {
                    title: "财务系统", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecordLook, title: '收费记录', permission: { 'permission': '查看收费记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecord, title: '收费记录', permission: { 'permission': '收费记录管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliation, title: '银行扣款', permission: { 'permission': '打印/导出银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliationManage, title: '银行扣款', permission: { 'permission': '打印/导出银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliation, title: '非银行扣款', permission: { 'permission': '非银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliationLook, title: '非银行扣款', permission: { 'permission': '查看非银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "资讯管理", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        { key: 'NewsList', link: ROUTE_PATH.newsListWelfare, title: '发布资讯管理', permission: { 'permission': '新闻管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'commentAudit', link: ROUTE_PATH.commentAuditWelfare, title: '资讯评论管理', permission: { 'permission': '评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                // 床位预约审核--需求不确定
                {
                    title: "档案管理", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        // { key: 'organization', link: ROUTE_PATH.organization, title: '机构信息档案', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'organization', link: ROUTE_PATH.organizationFly, title: '机构信息档案', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'organization', link: ROUTE_PATH.organizationManage, title: '机构运营情况', permission: { 'permission': '机构运营情况查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'elderInfo', link: ROUTE_PATH.elderInfo, title: '长者档案', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'assessmentProject', link: ROUTE_PATH.assessmentProject, title: '评估项目', permission: { 'permission': '评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplate, title: '评估模板管理', permission: { 'permission': '评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'competenceAssessmentList', link: ROUTE_PATH.competenceAssessmentList, title: '长者评估', permission: { 'permission': '评估记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "从业人员档案", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnel, title: '员工档案', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'allowanceRead', link: ROUTE_PATH.allowanceRead, title: '从业人员补贴申请', permission: { 'permission': '服务人员补贴申请查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "区域床位管理", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                // { key: 'hotelZoneType', link: ROUTE_PATH.hotelZoneType, title: '区域类型', permission: { 'permission': '区域类型查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '区域管理', permission: { 'permission': '区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'bed', link: ROUTE_PATH.bedManage, title: '床位管理', permission: { 'permission': '床位管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "服务产品管理", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                // { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务类型', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        { key: 'activityRoom', link: ROUTE_PATH.activityRoom, title: '功能室管理', permission: { 'permission': '活动室列表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: '物料管理', icon: 'notification', link: '', key: 95, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                { key: 'Units', link: ROUTE_PATH.Units, title: '计量单位', permission: { 'permission': '计量单位查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'thingSort', link: ROUTE_PATH.thingSort, title: '物料分类', permission: { 'permission': '物料分类查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'thingArchives', link: ROUTE_PATH.thingArchives, title: '物料档案', permission: { 'permission': '物料档案查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: '健康照护', icon: 'notification', link: '', key: 86, permission: { permission: '机构养老菜单' }, childrenComponent: [
                                { key: 'medicineFile', link: ROUTE_PATH.medicineFile, title: '药品档案', permission: { 'permission': '药品档案查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'diseaseFile', link: ROUTE_PATH.diseaseFile, title: '疾病档案', permission: { 'permission': '疾病档案查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'allergyFile', link: ROUTE_PATH.allergyFile, title: '过敏档案', permission: { 'permission': '过敏档案查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        { key: 'monitor', link: ROUTE_PATH.Monitor, title: '监控管理', permission: { 'permission': '监控管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "综合统计", icon: 'notification', link: "", key: 5, permission: { permission: '机构养老菜单' }, childrenComponent: [
                        { key: 'ruzhuzhangzhe', link: ROUTE_PATH.ckeckInElder, title: '入住长者情况统计', permission: { 'permission': '入住长者情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'congyerenyuan', link: ROUTE_PATH.employees, title: '从业人员情况统计', permission: { 'permission': '从业人员情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "系统管理", icon: 'notification', link: "", key: 33, permission: { 'permission': '机构养老菜单' }, childrenComponent: [
                        { key: 'userRole', link: ROUTE_PATH.roleUser, title: '用户管理', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色管理', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
                    ]
                },
                {
                    title: "疫情复工汇总表", icon: 'notification', link: "", key: 33, permission: { 'permission': '机构养老菜单' }, childrenComponent: [
                        { key: 'jujiaEpidemicPreventionJg', link: ROUTE_PATH.jujiaEpidemicPreventionJg, title: '养老机构工作人员出入登记', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'OrgDayInfoPrevention', link: ROUTE_PATH.OrgDayInfoPrevention, title: '全省民政部门重点场所防疫工作情况统计表', permission: { 'permission': '全省民政部门重点场所防疫查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
            ]
        },
        {
            title: "社区幸福院", icon: 'notification', link: "", key: 5072, permission: { 'permission': '社区幸福院菜单' }, childrenComponent: [
                {
                    title: "基础信息管理", icon: 'notification', link: "", key: 5878, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'changeHappiness', link: ROUTE_PATH.changeHappiness, title: '基本信息', permission: { 'permission': '基本信息查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'HappinessWorkerEntryControl', link: ROUTE_PATH.HappinessWorkerEntryControl, title: '员工档案', permission: { 'permission': '社区服务人员查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "运营机构管理", icon: 'notification', link: "", key: 5978, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'xfyOrgList', link: ROUTE_PATH.xfyOrgList, title: '运营机构信息', permission: { 'permission': '幸福院运营机构信息查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'jigouWorkerEntry', link: ROUTE_PATH.jigouWorkerEntry, title: '服务人员信息', permission: { 'permission': '机构服务人员查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "服务计划与总结", icon: 'notification', link: "", key: 3678, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        // { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务范围', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'xmxyybg', link: ROUTE_PATH.dealAndReport, title: '项目协议与报告', permission: { 'permission': '项目协议与报告查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'ndjhyzj', link: ROUTE_PATH.yearplanAndSummary, title: '年度计划与总结', permission: { 'permission': '年度计划与总结查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'ndzp', link: ROUTE_PATH.yearSelfAssessment, title: '年度自评', permission: { 'permission': '社区幸福院年度自评查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "服务执行管理", icon: 'notification', link: "", key: 'fwzxgl', permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'monthPlan', link: ROUTE_PATH.monthPlan, title: '月度计划', permission: { 'permission': '月度计划查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "活动管理", icon: 'notification', link: "", key: 6, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                                ...activity_manage
                            ]
                        },
                        {
                            title: "资讯管理", icon: 'notification', link: "", key: 5, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                                { key: 'NewsList', link: ROUTE_PATH.newsListHappiness, title: '发布资讯管理', permission: { 'permission': '新闻管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'commentAudit', link: ROUTE_PATH.commentAuditHappiness, title: '资讯评论管理', permission: { 'permission': '评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                    ]
                },
                {
                    title: "老人档案", icon: 'notification', link: "", key: 3978, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'elderInfoList', link: ROUTE_PATH.elderInfoList, title: '长者列表', permission: { 'permission': '长者列表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'elderHealthy', link: ROUTE_PATH.elderHealthy, title: '健康信息', permission: { 'permission': '健康信息查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "社工服务", icon: 'notification', link: "", key: 50078, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                                { key: 'serviceSituationList', link: ROUTE_PATH.serviceSituationList, title: '个案服务情况', permission: { 'permission': '个案服务情况查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'visitSituationList', link: ROUTE_PATH.visitSituationList, title: '探访服务情况', permission: { 'permission': '探访服务情况查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        { key: 'olderAbilityAssessment', link: ROUTE_PATH.olderAbilityAssessment, title: '老人能力评估', permission: { 'permission': '老人能力评估查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: '团体管理', icon: 'notification', link: "", key: 'ttgl', permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'groups', link: ROUTE_PATH.groups, title: '团体列表', permission: { 'permission': '团体列表查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: '特色服务', icon: 'notification', link: "", key: 'ttgl', permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'elderMeals', link: ROUTE_PATH.elderMeals, title: '长者膳食', permission: { 'permission': '长者膳食查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'happinessStation', link: ROUTE_PATH.happinessStation, title: '幸福小站', permission: { 'permission': '幸福小站查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: '档案管理', icon: 'notification', link: "", key: 'dagl', permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'fireEquipment', link: ROUTE_PATH.fireEquipment, title: '消防设施管理', permission: { 'permission': '消防设施查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: '统计管理', icon: 'notification', link: "", key: 3378, permission: { permission: '社区幸福院菜单' }, childrenComponent: [
                        { key: 'checkManage', link: ROUTE_PATH.checkManage, title: '验收管理', permission: { 'permission': '验收管理列表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'inputManage', link: ROUTE_PATH.inputManage, title: '投入管理', permission: { 'permission': '幸福院投入管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'evaluationManage', link: ROUTE_PATH.evaluationManage, title: '评比管理', permission: { 'permission': '幸福院评比管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'xfyFollowStatistics', link: ROUTE_PATH.xfyFollowStatistics, title: '公众号关注数统计', permission: { 'permission': '幸福院评比管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "系统管理", icon: 'notification', link: "", key: 33, permission: { 'permission': '社区幸福院菜单' }, childrenComponent: [
                        { key: 'userRole', link: ROUTE_PATH.roleUser, title: '用户管理', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色管理', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
                    ]
                },
                {
                    title: "疫情复工汇总表", icon: 'notification', link: "", key: 33, permission: { 'permission': '机构养老菜单' }, childrenComponent: [
                        { key: 'jujiaEpidemicPreventionSq', link: ROUTE_PATH.jujiaEpidemicPreventionSq, title: '社区服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
            ]
        },
        {
            title: "居家服务", icon: 'notification', link: "", key: 5073, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                {
                    title: "商城管理", icon: 'notification', link: "", key: 'scgl', permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        // { key: 'dataDictionary', link: ROUTE_PATH.dataDictionary, title: '数据字典', permission: { 'permission': '数据字典查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "商品管理", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                                { key: 'products', link: ROUTE_PATH.products, title: '商品信息', permission: { 'permission': '商品信息查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "商品订单", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                                { key: 'productOrders', link: ROUTE_PATH.productOrders, title: '商品订单', permission: { 'permission': '商品订单查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "对账", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                                { key: 'SPFinancialOverview', link: ROUTE_PATH.SPFinancialOverview, title: '服务商财务总览', permission: { 'permission': '服务商对账查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                    ]
                },
                {
                    title: "居家上门服务", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        { key: 'orderAllJ', link: ROUTE_PATH.orderAllJ, title: '订单全景', permission: { 'permission': '订单全景查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务派工', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceRecord', link: ROUTE_PATH.serviceRecordProvider, title: '服务记录', permission: { 'permission': '服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '订单统计', permission: { 'permission': '服务订单查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceEvaluate', link: ROUTE_PATH.serviceEvaluate, title: '服务评价', permission: { 'permission': '服务评价查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "费用管理", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "资讯管理", icon: 'notification', link: "", key: 5, permission: { permission: '居家服务菜单' }, childrenComponent: [
                        { key: 'NewsList', link: ROUTE_PATH.newsListProvider, title: '发布资讯管理', permission: { 'permission': '新闻管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'commentAudit', link: ROUTE_PATH.commentAuditProvider, title: '资讯评论管理', permission: { 'permission': '评论审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "档案管理", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        { key: 'organization', link: ROUTE_PATH.organizationServicer, title: '机构信息档案', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnelProvider, title: '员工档案', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                // {
                //     title: "服务运营", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                //         { key: 'serviceRecord', link: ROUTE_PATH.serviceRecord, title: '服务记录', permission: { 'permission': '服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceRecord', link: ROUTE_PATH.serviceRecordLook, title: '服务记录', permission: { 'permission': '查看服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'transaction', link: ROUTE_PATH.transaction, title: '交易管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'transactioncomment', link: ROUTE_PATH.transactionComment, title: '信用记录', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务类型', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategoryLook, title: '服务类型', permission: { 'permission': '查看服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceItemPackage', link: ROUTE_PATH.serviceType, title: '服务产品分类', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceProject', link: ROUTE_PATH.serviceProject, title: '服务细项', permission: { 'permission': '服务项目查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceProject', link: ROUTE_PATH.serviceProjectLook, title: '服务细项', permission: { 'permission': '查看服务项目' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageSh, title: '服务产品审核', permission: { 'permission': '服务产品审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageLook, title: '服务产品', permission: { 'permission': '查看服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceScope', link: ROUTE_PATH.serviceScope, title: '服务适用范围', permission: { 'permission': '服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceScope', link: ROUTE_PATH.serviceScopeLook, title: '服务适用范围', permission: { 'permission': '查看服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceOption', link: ROUTE_PATH.serviceOption, title: '服务选项', permission: { 'permission': '服务选项查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceOption', link: ROUTE_PATH.serviceOptionLook, title: '服务选项', permission: { 'permission': '查看服务选项' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceProvider', link: ROUTE_PATH.serviceProvider, title: '服务商登记', permission: { 'permission': '服务商登记新增' }, icon: 'font@pao_font_icon_user_bright' },
                //         // // { key: 'serviceProviderApply', link: ROUTE_PATH.serviceProviderApply, title: '服务商申请', permission: { 'permission': '服务商申请新增' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'servicePersonalApply', link: ROUTE_PATH.servicePersonalApply, title: '服务人员申请', permission: { 'permission': '服务人员申请新增' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'servicePersonal', link: ROUTE_PATH.servicePersonalManage, title: '服务人员登记', permission: { 'permission': '服务人员登记查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlement, title: '服务费用核算', permission: { 'permission': '服务商结算查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlementLook, title: '服务费用核算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlement, title: '服务人员结算', permission: { 'permission': '服务人员结算查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlementLook, title: '服务人员结算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务订单分派', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTaskLook, title: '服务订单分派', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceWoker', link: ROUTE_PATH.serviceWoker, title: '服务提供者登记信息', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
                //         // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务订单分派', permission: { 'permission': '服务订单分派查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 's37', link: ROUTE_PATH.buyService, title: '服务购买', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 's37', link: ROUTE_PATH.reserveBuyService, title: '预定服务', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 's37', link: ROUTE_PATH.serviceSettlementStatement, title: '结算统计', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'acclist', link: ROUTE_PATH.accountList, title: '政府补贴账户', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceEvaluate', link: ROUTE_PATH.serviceEvaluate, title: '服务评价', permission: { 'permission': '服务评价查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceReturnVisit', link: ROUTE_PATH.serviceReturnVisit, title: '服务回访', permission: { 'permission': '服务回访查询' }, icon: 'font@pao_font_icon_user_bright' },
                //     ]
                // },
                {
                    title: "服务产品管理", icon: 'notification', link: "", key: 4, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        // { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务类型', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },

                    ]
                },
                // {
                //     title: "居家服务报表", icon: 'notification', link: "", key: 153, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                //         ...jujiafuwubaobiao
                //     ]
                // },
                {
                    title: "系统管理", icon: 'notification', link: "", key: 33, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        { key: 'userRole', link: ROUTE_PATH.roleUser, title: '用户管理', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色管理', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
                    ]
                },
                {
                    title: "疫情复工汇总表", icon: 'notification', link: "", key: 33, permission: { 'permission': '居家服务菜单' }, childrenComponent: [
                        { key: 'jujiaEpidemicPreventionJj', link: ROUTE_PATH.jujiaEpidemicPreventionJj, title: '居家服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
            ]
        },
        {
            title: "平台运营方", icon: 'notification', link: "", key: 5073, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                {
                    title: "商城管理", icon: 'notification', link: "", key: 'scgl', permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'dataDictionary', link: ROUTE_PATH.dataDictionary, title: '数据字典', permission: { 'permission': '数据字典查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "商品管理", icon: 'notification', link: "", key: 4, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'products', link: ROUTE_PATH.productsSh, title: '商品审核', permission: { 'permission': '商品审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                                {
                                    title: "首页配置", icon: 'notification', link: "", key: 4, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                        { key: 'products', link: ROUTE_PATH.blocks, title: '板块信息', permission: { 'permission': '板块信息查询' }, icon: 'font@pao_font_icon_user_bright' },
                                    ]
                                },
                                {
                                    title: "对账", icon: 'notification', link: "", key: 4, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                        { key: 'products', link: ROUTE_PATH.PTFinancialOverview, title: '财务总览', permission: { 'permission': '平台对账查询' }, icon: 'font@pao_font_icon_user_bright' },
                                        // { key: 'products', link: ROUTE_PATH.separateAccountsSH, title: '分账审核', permission: { 'permission': '平台对账查询' }, icon: 'font@pao_font_icon_user_bright' },
                                        // { key: 'products', link: ROUTE_PATH.PTEntryRecord, title: '入账记录', permission: { 'permission': '平台对账查询' }, icon: 'font@pao_font_icon_user_bright' },
                                        { key: 'products', link: ROUTE_PATH.PTFinancialDetail, title: '对账单明细详情', permission: { 'permission': '平台对账查询' }, icon: 'font@pao_font_icon_user_bright' },
                                    ]
                                },
                                { key: 'productOrders', link: ROUTE_PATH.productOrders, title: '全部订单', permission: { 'permission': '商品订单查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                    ]
                },
                {
                    title: "呼叫中心", icon: 'notification', link: "", key: 53, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        // { key: 'callCenter', link: ROUTE_PATH.callCenter, title: '呼叫中心', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'callCenterNh', link: ROUTE_PATH.callCenterNh, title: '移动呼叫中心', permission: { 'permission': '移动呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'callCenterZh', link: ROUTE_PATH.callCenterZh, title: '移动呼叫中心帐号', permission: { 'permission': '移动呼叫中心帐号查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'emergencyCall', link: ROUTE_PATH.emergencyCall, title: '紧急呼叫', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'knowage', link: ROUTE_PATH.knowledgeBase, title: '知识库', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "平台运营", icon: 'notification', link: "", key: 154, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'announcement', link: ROUTE_PATH.announcement, title: '系统公告管理', permission: { 'permission': '系统公告查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            key: 'announcement', link: ROUTE_PATH.announcement, title: '社区幸福院评比管理', permission: { 'permission': '平台运营方菜单' }, icon: 'font@pao_font_icon_user_bright', childrenComponent: [
                                { key: 'targetSetting', link: ROUTE_PATH.targetSetting, title: '社区幸福院评比指标设置', permission: { 'permission': '社区幸福院评比指标设置查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'targetDetails', link: ROUTE_PATH.targetDetails, title: '社区幸福院评比详细指标', permission: { 'permission': '社区幸福院评比详细指标查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'targetDetails', link: ROUTE_PATH.happinessYearSelfAssessment, title: '社区幸福院自评汇总', permission: { 'permission': '社区幸福院自评汇总查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        // 社区幸福院评比管理 待开发
                        {
                            title: "居家服务运营", icon: 'notification', link: "", key: 63, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '服务订单', permission: { 'permission': '服务订单查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceRecord', link: ROUTE_PATH.serviceRecord, title: '服务记录', permission: { 'permission': '服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceReturnVisit', link: ROUTE_PATH.serviceReturnVisit, title: '服务回访', permission: { 'permission': '服务回访查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 's37', link: ROUTE_PATH.buyService, title: '服务购买', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 's38', link: ROUTE_PATH.reserveBuyService, title: '服务预订', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'acclist', link: ROUTE_PATH.accountList, title: '政府补贴账户', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'xfyFollowStatistics', link: ROUTE_PATH.xfyFollowStatistics, title: '公众号关注数统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                    ]
                },
                // 长者管理 已开发
                {
                    title: "长者管理", icon: 'notification', link: "", key: 154, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        {
                            title: "长者关怀管理", icon: 'notification', link: "", key: 1564, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'elderCareRecord', link: ROUTE_PATH.elderCareRecord, title: '长者关怀记录', permission: { 'permission': '长者关怀记录查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'olderBirthNotify', link: ROUTE_PATH.olderBirthNotify, title: '长者生日提醒', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'careTypeSetting', link: ROUTE_PATH.careTypeSetting, title: '长者关怀类型设置', permission: { 'permission': '长者关怀类型设置查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        { key: 'device', link: ROUTE_PATH.Device, title: '长者设备管理', permission: { 'permission': '设备管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]

                },
                {
                    title: "综合数据统计", icon: 'notification', link: "", key: 153, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'comprehensiveStatistics', link: ROUTE_PATH.comprehensiveStatistics, title: '综合统计', permission: { 'permission': '综合统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "机构养老统计", icon: 'notification', link: "", key: 151, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'jgyltj', link: ROUTE_PATH.organList, title: '基本信息展示', permission: { 'permission': '机构基本情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'jgyyqktj', link: ROUTE_PATH.organOperation, title: '机构运营情况统计', permission: { 'permission': '机构运营情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'ruzhuzhangzhe', link: ROUTE_PATH.ckeckInElder, title: '入住长者情况统计', permission: { 'permission': '入住长者情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'congyerenyuan', link: ROUTE_PATH.employees, title: '从业人员情况统计', permission: { 'permission': '从业人员情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "社区幸福院统计", icon: 'notification', link: "", key: 152, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'allowanceBaseSituation', link: ROUTE_PATH.baseSituation, title: '基本信息展示', permission: { 'permission': '基本情况统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'yunyingqingkuagntongji', link: ROUTE_PATH.operationHappiness, title: '幸福院运营情况统计', permission: { 'permission': '幸福院运营情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'yanshoujipingjiqingkuang', link: ROUTE_PATH.acceptancRating, title: '验收及评级情况统计', permission: { 'permission': '验收及评级情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'workPeopleStatistics', link: ROUTE_PATH.workPeopleStatistics, title: '从业人员情况统计', permission: { 'permission': '工作人员统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        {
                            title: "居家服务统计", icon: 'notification', link: "", key: 153, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'jibenqingkuangtongji', link: ROUTE_PATH.servicerList, title: '基本情况统计', permission: { 'permission': '服务商基本情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'orderStatistics', link: ROUTE_PATH.orderStatistics, title: '订单情况统计', permission: { 'permission': '订单统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'allowanceReportMonth', link: ROUTE_PATH.allowanceReportMonth, title: '居家养老服务对象月报表', permission: { 'permission': '居家养老服务对象月报表查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'allowanceReportDetails', link: ROUTE_PATH.allowanceReportDetails, title: '居家养老服务对象明细表', permission: { 'permission': '居家养老服务对象明细表查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        { key: 'zuoxiyunying', link: ROUTE_PATH.seatOperation, title: '坐席运营统计', permission: { 'permission': '坐席运营统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "信息管理", icon: 'notification', link: "", key: 154, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'NewsList', link: ROUTE_PATH.newsListSh, title: '资讯管理', permission: { 'permission': '新闻审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'activityManageSh', link: ROUTE_PATH.activityManageSh, title: '活动管理', permission: { 'permission': '活动审核列表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageSh, title: '服务产品管理', permission: { 'permission': '服务产品审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "基础信息设置", icon: 'notification', link: "", key: 63, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'personnelClassification', link: ROUTE_PATH.personnelClassification, title: '长者类型设置', permission: { 'permission': '人员类别管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'assessmentProject', link: ROUTE_PATH.assessmentProject, title: '评估项目', permission: { 'permission': '评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplate, title: '长者评估模板', permission: { 'permission': '评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'nursingGrade', link: ROUTE_PATH.nursingGrade, title: '照护等级设定', permission: { 'permission': '护理等级查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'hotelZoneType', link: ROUTE_PATH.hotelZoneType, title: '区域类型', permission: { 'permission': '区域类型查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'articleType', link: ROUTE_PATH.articleType, title: '资讯类型', permission: { 'permission': '文章类型查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "服务设置", icon: 'notification', link: "", key: 63, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务类型设置', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceProject', link: ROUTE_PATH.serviceProject, title: '服务细项设置', permission: { 'permission': '服务项目查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceOption', link: ROUTE_PATH.serviceOption, title: '服务选项', permission: { 'permission': '服务选项查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'serviceScope', link: ROUTE_PATH.serviceScope, title: '服务适用范围', permission: { 'permission': '服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                        // 回访问卷设定
                        { key: 'administrationDivision', link: ROUTE_PATH.administrationDivision, title: '行政区划维护', permission: { 'permission': '行政区划管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "APP应用管理", icon: 'notification', link: "", key: 154, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'appyonghuguanli', link: ROUTE_PATH.appUserManage, title: 'APP用户管理', permission: { 'permission': 'APP用户管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "运营后台管理", icon: 'notification', link: "", key: 'yunyinghoutai', permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                                { key: 'laoyouquan', link: ROUTE_PATH.appFriendCircle, title: '老友圈', permission: { 'permission': 'APP老友圈查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'quanzi', link: ROUTE_PATH.circle, title: '圈子管理', permission: { 'permission': '圈子管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'appshezhi', link: ROUTE_PATH.appSettings, title: 'APP设置', permission: { 'permission': 'APP设置查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'zaixianzixun', link: ROUTE_PATH.appOnlineTalk, title: '在线咨询', permission: { 'permission': 'APP在线咨询查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'yijianfankui', link: ROUTE_PATH.appFeedback, title: 'APP意见反馈', permission: { 'permission': 'APP意见反馈查询' }, icon: 'font@pao_font_icon_user_bright' },
                            ]
                        },
                    ]
                },
                {
                    title: "档案管理", icon: 'notification', link: "", key: 1133, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        // { key: 'platformOrgList', link: ROUTE_PATH.platformOrgList, title: '运营机构信息档案', permission: { 'permission': '平台运营机构信息档案查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'organizationPt', link: ROUTE_PATH.organizationPt, title: '运营机构信息档案', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'elderInfoPT', link: ROUTE_PATH.elderInfo, title: '长者档案', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnelPlatform, title: '员工档案', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                // {
                //     title: "居家养老报表", icon: 'notification', link: "", key: 152, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                //         { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'orderReport', link: ROUTE_PATH.orderReport, title: '订单统计报表', permission: { 'permission': '订单统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'allowanceReportMonth', link: ROUTE_PATH.allowanceReportMonth, title: '居家养老服务对象月报表', permission: { 'permission': '居家养老服务对象月报表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'allowanceReportDetails', link: ROUTE_PATH.allowanceReportDetails, title: '居家养老服务对象明细表', permission: { 'permission': '居家养老服务对象明细表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'workPeopleStatistics', link: ROUTE_PATH.workPeopleStatistics, title: '工作人员统计', permission: { 'permission': '工作人员统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'allowanceBaseSituation', link: ROUTE_PATH.baseSituation, title: '基本情况统计', permission: { 'permission': '基本情况统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //         { key: 'xfyFollowStatistics', link: ROUTE_PATH.xfyFollowStatistics, title: '公众号关注数统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                //     ]
                // },
                {
                    title: "统计管理", icon: 'notification', link: "", key: 1993, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'evaluationManage', link: ROUTE_PATH.evaluationManage, title: '评比管理', permission: { 'permission': '评比管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: '智能监护', icon: 'notification', link: '', key: 9486, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'XxhDevice', link: ROUTE_PATH.XxhDevice, title: '智能设备管理', permission: { 'permission': '设备管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'callSettings', link: ROUTE_PATH.callSettings, title: '通话设置', permission: { 'permission': '通话设置查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'careManage', link: ROUTE_PATH.careManage, title: '智能监护管理', permission: { 'permission': '智能监护管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'guardianshipOperation', link: ROUTE_PATH.guardianshipOperation, title: '智能监护运营', permission: { 'permission': '智能监护运营查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'deviceMessage', link: ROUTE_PATH.deviceMessage, title: '智能设备警报', permission: { 'permission': '智能监护运营查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'deviceTerm', link: ROUTE_PATH.deviceTerm, title: '智能设备期限', permission: { 'permission': '智能监护运营查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "系统管理", icon: 'notification', link: "", key: 33, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'userManage', link: ROUTE_PATH.userManage, title: '用户管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色管理', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermissionLook', link: ROUTE_PATH.roleLook, title: '角色管理', permission: { 'permission': '角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'userRole', link: ROUTE_PATH.roleUser, title: '用户管理', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'userRoleLook', link: ROUTE_PATH.roleUserLook, title: '用户管理', permission: { 'permission': '查看角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
                        { key: 'sysuserstatistics', link: ROUTE_PATH.sysUseStatistics, title: '系统使用情况统计', permission: { 'permission': '系统使用情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'sysuserstatistics', link: ROUTE_PATH.sysStatistics, title: '在线用户统计', permission: { 'permission': '系统使用情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },

                    ]
                },
                {
                    title: "疫情复工汇总表", icon: 'notification', link: "", key: 33, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'jujiaEpidemicPreventionJj', link: ROUTE_PATH.jujiaEpidemicPreventionJj, title: '居家服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'jujiaEpidemicPreventionSq', link: ROUTE_PATH.jujiaEpidemicPreventionSq, title: '社区服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'jujiaEpidemicPreventionJg', link: ROUTE_PATH.jujiaEpidemicPreventionJg, title: '养老机构工作人员出入登记', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'OrgDayInfoPrevention', link: ROUTE_PATH.OrgDayInfoPrevention, title: '全省民政部门重点场所防疫工作情况统计表', permission: { 'permission': '全省民政部门重点场所防疫查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "适老化设备需求", icon: 'notification', link: "", key: 53, permission: { 'permission': '平台运营方菜单' }, childrenComponent: [
                        { key: 'deviceWarning', link: ROUTE_PATH.deviceWarning, title: '适老化设备警报', permission: { 'permission': '适老化设备警报查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'deviceManage', link: ROUTE_PATH.deviceManage, title: '适老化设备管理', permission: { 'permission': '适老化设备管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
            ]
        },
        {
            title: "民政部门", icon: 'notification', link: "", key: 5072, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                {
                    title: "档案管理", icon: 'notification', link: "", key: 1133, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'organizationMz', link: ROUTE_PATH.organizationMz, title: '机构信息档案', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnelCivil, title: '员工档案', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },

                    ]
                },
                {
                    title: "申请受理", icon: 'notification', link: "", key: 31, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'allowanceCheck', link: ROUTE_PATH.allowanceOldCheck, title: '养老补贴受理', permission: { 'permission': '补贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'allowanceCheck', link: ROUTE_PATH.allowanceCheck, title: '从业人员补贴受理', permission: { 'permission': '补贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                        {
                            title: "高龄津贴", icon: 'notification', link: "", key: 31, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                                { key: 'oldAgeList', link: ROUTE_PATH.oldAgeList, title: '高龄津贴名单', permission: { 'permission': '高龄津贴名单查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'oldallowanceCheck', link: ROUTE_PATH.oldallowanceCheck, title: '高龄津贴认证列表', permission: { 'permission': '查看高龄津贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'oldallowanceNoCheck', link: ROUTE_PATH.oldallowanceNoCheck, title: '高龄津贴未认证列表', permission: { 'permission': '查看高龄津贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'oldAgeStatistics', link: ROUTE_PATH.oldAgeStatistics, title: '高龄津贴统计报表', permission: { 'permission': '高龄津贴统计报表查询' }, icon: 'font@pao_font_icon_user_bright' }
                            ]
                        },
                        {
                            title: "残疾人认证", icon: 'notification', link: "", key: 31, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                                { key: 'disabledPerson', link: ROUTE_PATH.disabledPerson, title: '残疾人名单', permission: { 'permission': '高龄津贴名单查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'disabledPersonCheck', link: ROUTE_PATH.disabledPersonCheck, title: '残疾人认证列表', permission: { 'permission': '查看高龄津贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
                                { key: 'DisabledPersonStatistics', link: ROUTE_PATH.disabledPersonStatistics, title: '残疾人认证统计报表', permission: { 'permission': '高龄津贴统计报表查询' }, icon: 'font@pao_font_icon_user_bright' }
                            ]
                        }

                    ]
                },

                {
                    title: "资助统计", icon: 'notification', link: "", key: 31, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'userRole', link: ROUTE_PATH.orgOperationFund, title: '民办非营机构运营资助统计', permission: { 'permission': '民办非营机构运营资助统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.orgBedFund, title: '民办非营机构床位资助统计', permission: { 'permission': '民办非营机构床位资助查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "社工局", icon: 'notification', link: "", key: 31, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "系统管理", icon: 'notification', link: "", key: 33, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'userRole', link: ROUTE_PATH.roleUser, title: '用户管理', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色管理', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
                        { key: 'sysuserstatistics', link: ROUTE_PATH.sysUseStatistics, title: '系统使用情况统计', permission: { 'permission': '系统使用情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'sysuserstatistics', link: ROUTE_PATH.sysStatistics, title: '在线用户统计', permission: { 'permission': '系统使用情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                        // { key: 'sysuserstatistics', link: ROUTE_PATH.dataImport, title: '数据导入', permission: { 'permission': '系统使用情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
                {
                    title: "疫情复工汇总表", icon: 'notification', link: "", key: 33, permission: { 'permission': '民政部门菜单' }, childrenComponent: [
                        { key: 'jujiaEpidemicPreventionJj', link: ROUTE_PATH.jujiaEpidemicPreventionJj, title: '居家服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'jujiaEpidemicPreventionSq', link: ROUTE_PATH.jujiaEpidemicPreventionSq, title: '社区服务人员汇总表', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'jujiaEpidemicPreventionJg', link: ROUTE_PATH.jujiaEpidemicPreventionJg, title: '养老机构工作人员出入登记', permission: { 'permission': '防疫汇总表查询' }, icon: 'font@pao_font_icon_user_bright' },
                        { key: 'OrgDayInfoPrevention', link: ROUTE_PATH.OrgDayInfoPrevention, title: '全省民政部门重点场所防疫工作情况统计表', permission: { 'permission': '全省民政部门重点场所防疫查询' }, icon: 'font@pao_font_icon_user_bright' },
                    ]
                },
            ]
        },
        // {
        //     title: "养老院综合服务", icon: 'notification', link: "", key: 1, permission: "8", childrenComponent: [
        //         { key: 'reservation', link: ROUTE_PATH.reservationRegistration, title: '预约登记', permission: { 'permission': '预约登记查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'reservation', link: ROUTE_PATH.reservationRegistrationLook, title: '预约登记', permission: { 'permission': '查看预约登记查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplate, title: '评估模板', permission: { 'permission': '评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'assessmentTemplate', link: ROUTE_PATH.assessmentTemplateLook, title: '评估模板', permission: { 'permission': '查看评估模板查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'assessmentProject', link: ROUTE_PATH.assessmentProject, title: '评估项目', permission: { 'permission': '评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'assessmentProject', link: ROUTE_PATH.assessmentProjectLook, title: '评估项目', permission: { 'permission': '查看评估项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'competenceAssessmentList', link: ROUTE_PATH.competenceAssessmentList, title: '长者评估', permission: { 'permission': '评估记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'competenceAssessmentList', link: ROUTE_PATH.competenceAssessmentListLook, title: '长者评估', permission: { 'permission': '查看评估记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'checkIn', link: ROUTE_PATH.checkIn, title: '长者入住', permission: { 'permission': '长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'checkInJj', link: ROUTE_PATH.checkInJj, title: '入住退住办理', permission: { 'permission': '入住办理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'checkInJjLook', link: ROUTE_PATH.checkInJjLook, title: '入住退住办理', permission: { 'permission': '查看入住办理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'checkIn', link: ROUTE_PATH.checkInLook, title: '长者入住', permission: { 'permission': '查看长者入住查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'roomStatus', link: ROUTE_PATH.roomStatus, title: '房态图', permission: { 'permission': '房态图查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hydropower', link: ROUTE_PATH.hydropower, title: '水电抄表', permission: { 'permission': '水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hydropowerLook', link: ROUTE_PATH.hydropowerLook, title: '水电抄表', permission: { 'permission': '查看水电抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hydropowerJj', link: ROUTE_PATH.hydropowerJj, title: '电费抄表', permission: { 'permission': '电费抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hydropowerJjLook', link: ROUTE_PATH.hydropowerJjLook, title: '电费抄表', permission: { 'permission': '查看电费抄表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'leaveReason', link: ROUTE_PATH.leaveReason, title: '离开原因', permission: { 'permission': '离开原因查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'researchInsert', link: ROUTE_PATH.leaveRecord, title: '请销假', permission: { 'permission': '住宿记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'seeresearchInsert', link: ROUTE_PATH.leaveRecordLook, title: '查看请销假', permission: { 'permission': '查看住宿记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hotelZoneType', link: ROUTE_PATH.hotelZoneType, title: '区域类型', permission: { 'permission': '区域类型查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hotelZone', link: ROUTE_PATH.hotelZone, title: '区域管理', permission: { 'permission': '区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'hotelZone', link: ROUTE_PATH.hotelZoneLook, title: '区域管理', permission: { 'permission': '查看区域情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'bed', link: ROUTE_PATH.bedManage, title: '床位管理', permission: { 'permission': '床位管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'bed', link: ROUTE_PATH.bedManageLook, title: '床位管理', permission: { 'permission': '查看床位情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'bed', link: ROUTE_PATH.costAccountingLook, title: '住宿核算', permission: { 'permission': '住宿核算查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'bed', link: ROUTE_PATH.costAccounting, title: '住宿核算', permission: { 'permission': '住宿核算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'roomChange', link: ROUTE_PATH.roomList, title: '床位调整', permission: { 'permission': '房间调整查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'feeChange', link: ROUTE_PATH.feeList, title: '费用调整', permission: { 'permission': '房间调整查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'incidental', link: ROUTE_PATH.incidentalList, title: '杂费管理', permission: { 'permission': '杂费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'incidentalLook', link: ROUTE_PATH.incidentalListLook, title: '杂费管理', permission: { 'permission': '查看杂费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'foodCost', link: ROUTE_PATH.foodCostList, title: '餐费管理', permission: { 'permission': '餐费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'foodCostLook', link: ROUTE_PATH.foodCostListLook, title: '餐费管理', permission: { 'permission': '查看餐费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'medicalCost', link: ROUTE_PATH.medicalCost, title: '医疗费用管理', permission: { 'permission': '医疗费用管理' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'diaperCost', link: ROUTE_PATH.diaperCost, title: '尿片费用管理', permission: { 'permission': '尿片费用管理' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'nursingGrade', link: ROUTE_PATH.nursingGrade, title: '护理等级', permission: { 'permission': '护理等级查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'userFood', link: ROUTE_PATH.elderFood, title: '用餐统计', permission: { 'permission': '用餐管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'deduction', link: ROUTE_PATH.deduction, title: '收费减免', permission: { 'permission': '收费减免查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'setClassType', link: ROUTE_PATH.setClassType, title: '排班类型', permission: { 'permission': '排班类型查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'setClass', link: ROUTE_PATH.setClass, title: '排班管理', permission: { 'permission': '排班管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'depositSetting', link: ROUTE_PATH.depositSetting, title: '押金结算设置', permission: { 'permission': '押金结算设置查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "基础档案", icon: 'notification', link: "", key: 2, permission: "8", childrenComponent: [
        //         { key: 'elderInfo', link: ROUTE_PATH.elderInfo, title: '长者资料', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'olderBirthNotify', link: ROUTE_PATH.olderBirthNotify, title: '长者生日提醒', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'OrgInfoRecord', link: ROUTE_PATH.OrgInfoRecord, title: '机构信息档案', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'OperateSituationStatistic', link: ROUTE_PATH.OperateSituationStatistic, title: '社区养老运营情况统计', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'monthSalaryStatistic', link: ROUTE_PATH.monthSalaryStatistic, title: '月收入统计', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'ChargeType', link: ROUTE_PATH.ChargeType, title: '收费类型', permission: { 'permission': '长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'elderInfo', link: ROUTE_PATH.elderInfoLook, title: '长者资料', permission: { 'permission': '查看长者资料查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'elderInfo', link: ROUTE_PATH.orgElderInfo, title: '长者资料', permission: { 'permission': '机构长者管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'personnel', link: ROUTE_PATH.personnel, title: '人员管理', permission: { 'permission': '人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'personnel', link: ROUTE_PATH.personnel, title: '用户管理', permission: { 'permission': '用户管理查看' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'wokerPersonnel', link: ROUTE_PATH.wokerPersonnel, title: '工作人员管理', permission: { 'permission': '工作人员管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'personnelClassification', link: ROUTE_PATH.personnelClassification, title: '人员类别管理', permission: { 'permission': '人员类别管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'organization', link: ROUTE_PATH.organization, title: '机构信息管理', permission: { 'permission': '组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'organization', link: ROUTE_PATH.organizationLook, title: '机构信息管理', permission: { 'permission': '查看组织机构查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'behavioralCompetenceAssessment', link: ROUTE_PATH.behavioralCompetenceAssessment, title: '行为能力评估', permission: PermissionList.RolePermission_Select, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'administrationDivision', link: ROUTE_PATH.administrationDivision, title: '行政区划管理', permission: { 'permission': '行政区划管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'administrationDivision', link: ROUTE_PATH.administrationDivisionLook, title: '行政区划管理', permission: { 'permission': '查看行政区划查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'businessArea', link: ROUTE_PATH.businessArea, title: '业务区域管理', permission: { 'permission': '业务区域管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'businessArea', link: ROUTE_PATH.businessArea, title: '业务区域管理', permission: { 'permission': '查看业务区域查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'device', link: ROUTE_PATH.Device, title: '设备管理', permission: { 'permission': '设备管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'device', link: ROUTE_PATH.DeviceLook, title: '设备管理', permission: { 'permission': '查看设备查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'monitor', link: ROUTE_PATH.Monitor, title: '监控管理', permission: { 'permission': '监控管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'monitor', link: ROUTE_PATH.MonitorLook, title: '监控管理', permission: { 'permission': '查看监控信息查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'socialGroupsType', link: ROUTE_PATH.socialGroupsType, title: '社会群体类型管理', permission: { 'permission': '社会群体类型管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'socialGroupsType', link: ROUTE_PATH.socialGroupsTypeLook, title: '社会群体类型管理', permission: { 'permission': '查看社会群体类型查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "幸福院管理", icon: 'notification', link: "", key: 673, permission: "8", childrenComponent: [
        //         { key: 'organizationXFY', link: ROUTE_PATH.organization_xfy, title: '幸福院列表', permission: { 'permission': '幸福院查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'ratingPlan', link: ROUTE_PATH.ratingPlan, title: '幸福院评比方案', permission: { 'permission': '评比方案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'rating', link: ROUTE_PATH.rating, title: '幸福院评比', permission: { 'permission': '评比填表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'groups', link: ROUTE_PATH.groups, title: '团体列表', permission: { 'permission': '团体列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "护理管理", icon: 'notification', link: "", key: 3, permission: "8", childrenComponent: [
        //         { key: 'nursingContent', link: ROUTE_PATH.assessmentProjectNursing, title: '护理内容', permission: { 'permission': '护理项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'nursingContentLook', link: ROUTE_PATH.assessmentProjectNursingLook, title: '护理内容', permission: { 'permission': '查看护理项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'nursingTemplate', link: ROUTE_PATH.nursingTemplate, title: '护理模板', permission: { 'permission': '护理模板查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'nursingTemplate', link: ROUTE_PATH.nursingRecord, title: '长者护理', permission: { 'permission': '护理记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'nursingPay', link: ROUTE_PATH.nursingPay, title: '护理收费', permission: { 'permission': '护理收费查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "服务运营", icon: 'notification', link: "", key: 4, permission: "8", childrenComponent: [
        //         { key: 'serviceOrder', link: ROUTE_PATH.serviceOrder, title: '服务订单', permission: { 'permission': '服务订单查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceRecord', link: ROUTE_PATH.serviceRecord, title: '服务记录', permission: { 'permission': '服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceRecord', link: ROUTE_PATH.serviceRecordLook, title: '服务记录', permission: { 'permission': '查看服务记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'transaction', link: ROUTE_PATH.transaction, title: '交易管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'transactioncomment', link: ROUTE_PATH.transactionComment, title: '信用记录', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategory, title: '服务类型', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicesItemCategory', link: ROUTE_PATH.servicesItemCategoryLook, title: '服务类型', permission: { 'permission': '查看服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'serviceItemPackage', link: ROUTE_PATH.serviceType, title: '服务产品分类', permission: { 'permission': '服务项目类别查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProject', link: ROUTE_PATH.serviceProject, title: '服务细项', permission: { 'permission': '服务项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProject', link: ROUTE_PATH.serviceProjectLook, title: '服务细项', permission: { 'permission': '查看服务项目' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackage, title: '服务产品', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageSh, title: '服务产品审核', permission: { 'permission': '服务产品审核查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceItemPackage', link: ROUTE_PATH.serviceItemPackageLook, title: '服务产品', permission: { 'permission': '查看服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceScope', link: ROUTE_PATH.serviceScope, title: '服务适用范围', permission: { 'permission': '服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceScope', link: ROUTE_PATH.serviceScopeLook, title: '服务适用范围', permission: { 'permission': '查看服务适用范围查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceOption', link: ROUTE_PATH.serviceOption, title: '服务选项', permission: { 'permission': '服务选项查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceOption', link: ROUTE_PATH.serviceOptionLook, title: '服务选项', permission: { 'permission': '查看服务选项' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProvider', link: ROUTE_PATH.serviceProvider, title: '服务商登记', permission: { 'permission': '服务商登记新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProviderApply', link: ROUTE_PATH.serviceProviderApply, title: '服务商申请', permission: { 'permission': '服务商申请新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicePersonalApply', link: ROUTE_PATH.servicePersonalApply, title: '服务人员申请', permission: { 'permission': '服务人员申请新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicePersonal', link: ROUTE_PATH.servicePersonalManage, title: '服务人员登记', permission: { 'permission': '服务人员登记查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlement, title: '服务费用核算', permission: { 'permission': '服务商结算查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProviderSettlement', link: ROUTE_PATH.serviceProviderSettlementLook, title: '服务费用核算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlement, title: '服务人员结算', permission: { 'permission': '服务人员结算查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicePersonalSettlement', link: ROUTE_PATH.servicePersonalSettlementLook, title: '服务人员结算', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务订单分派', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTaskLook, title: '服务订单分派', permission: { 'permission': '服务订单分派管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'serviceWoker', link: ROUTE_PATH.serviceWoker, title: '服务提供者登记信息', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'serviceTaskSend', link: ROUTE_PATH.serviceOrderTask, title: '服务订单分派', permission: { 'permission': '服务订单分派查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 's37', link: ROUTE_PATH.buyService, title: '服务购买', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 's37', link: ROUTE_PATH.reserveBuyService, title: '预定服务', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 's37', link: ROUTE_PATH.serviceSettlementStatement, title: '结算统计', permission: { 'permission': '服务产品查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'acclist', link: ROUTE_PATH.accountList, title: '政府补贴账户', permission: { 'permission': '服务商结算管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceEvaluate', link: ROUTE_PATH.serviceEvaluate, title: '服务评价', permission: { 'permission': '服务评价查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceReturnVisit', link: ROUTE_PATH.serviceReturnVisit, title: '服务回访', permission: { 'permission': '服务回访查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // ...billLink,
        // {
        //     title: "财务系统", icon: 'notification', link: "", key: 5, permission: "8", childrenComponent: [
        //         // { key: 'changeCollectorCharge', link: ROUTE_PATH.changeCollectorCharge, title: '收费员收费', permission: { 'permission': '收费员收费查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'changeCollectorCharge', link: ROUTE_PATH.changeCollectorCharge, title: '收费员收费', permission: { 'permission': '收费员收费管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'changeCollectorPayment', link: ROUTE_PATH.changeCollectorPayment, title: '收费员退款', permission: { 'permission': '收费员退款查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'changeCollectorPayment', link: ROUTE_PATH.changeCollectorPayment, title: '收费员退款', permission: { 'permission': '收费员退款管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecordLook, title: '查看收费记录', permission: { 'permission': '查看收费记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialChargeRecord', link: ROUTE_PATH.financialChargeRecord, title: '查看收费记录', permission: { 'permission': '收费记录管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliation, title: '打印/导出银行扣款单', permission: { 'permission': '打印/导出银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialBankReconciliation', link: ROUTE_PATH.financialBankReconciliationManage, title: '打印/导出银行扣款单', permission: { 'permission': '打印/导出银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliation, title: '非银行扣款单', permission: { 'permission': '非银行扣款单管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialBankReconciliation', link: ROUTE_PATH.unfinancialBankReconciliationLook, title: '非银行扣款单', permission: { 'permission': '查看非银行扣款单查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialReconciliationRecord', link: ROUTE_PATH.financialReconciliationRecord, title: '查看银行扣款记录', permission: { 'permission': '银行扣款记录管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialReconciliationRecord', link: ROUTE_PATH.financialReconciliationRecordLook, title: '查看银行扣款记录', permission: { 'permission': '查看银行扣款记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountFlow', link: ROUTE_PATH.financialAccountFlow, title: '账户流水', permission: { 'permission': '账户流水查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountFlow', link: ROUTE_PATH.financialAccountFlow, title: '账户流水', permission: { 'permission': '账户流水管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountReceive', link: ROUTE_PATH.changereceive, title: '收款', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountPayment', link: ROUTE_PATH.changepayment, title: '付款', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountTransfer', link: ROUTE_PATH.changetransfer, title: '转账', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountBook', link: ROUTE_PATH.financialAccountBook, title: '账簿管理', permission: { 'permission': '账簿管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialAccountBook', link: ROUTE_PATH.financialAccountBook, title: '账簿管理', permission: { 'permission': '全部账薄管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'financialAccountManage', link: ROUTE_PATH.financialAccountManage, title: '账户管理', permission: { 'permission': '全部账薄管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialVoucher', link: ROUTE_PATH.financialAccountVoucherManage, title: '记账凭证', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'financialSubject', link: ROUTE_PATH.financialSubjectManage, title: '会计科目', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'platformSettlement', link: ROUTE_PATH.platformSettlement, title: '结算报表', permission: { 'permission': '平台结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProviderSettlements', link: ROUTE_PATH.serviceProviderSettlements, title: '服务商结算报表', permission: { 'permission': '服务商结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'servicePersonnelSettlement', link: ROUTE_PATH.servicePersonnelSettlement, title: '服务人员结算报表', permission: { 'permission': '服务人员结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'charitySettlement', link: ROUTE_PATH.charitySettlement, title: '慈善结算报表', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'incomeAssessedSettlement', link: ROUTE_PATH.incomeAssessedSettlement, title: '收入分摊报表', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'subsidyServiceProvider', link: ROUTE_PATH.subsidyServiceProvider, title: '服务商补贴收入', permission: { 'permission': '慈善结算报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "医院管理", icon: 'notification', link: "", key: 6, permission: "8", childrenComponent: [
        //         { key: 'hospitalArrears', link: ROUTE_PATH.hospitalArrears, title: '医疗欠费', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' }
        //     ]
        // },
        // {
        //     title: "活动管理", icon: 'notification', link: "", key: 6, permission: { permission: '机构养老菜单' }, childrenComponent: [
        //         ...activity_manage
        //     ]
        // },
        // {
        //     title: "任务管理", icon: 'notification', link: "", key: 8, permission: "8", childrenComponent: [
        //         { key: 'taskList', link: ROUTE_PATH.taskList, title: '任务列表', permission: { 'permission': '任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskList', link: ROUTE_PATH.taskListLook, title: '任务列表', permission: { 'permission': '查看任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskCreate', link: ROUTE_PATH.changeTask, title: '创建任务', permission: { 'permission': '任务类型新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskTypeList', link: ROUTE_PATH.taskTypeList, title: '任务类型', permission: { 'permission': '任务类型查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskTypeList', link: ROUTE_PATH.taskTypeListLook, title: '任务类型', permission: { 'permission': '查看任务类型查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskUnassignList', link: ROUTE_PATH.taskToBeProcess, title: '审核任务列表', permission: { 'permission': '审核任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'taskServiceOrder', link: ROUTE_PATH.taskServiceOrder, title: '分派任务列表', permission: { 'permission': '分派任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'taskUnexamineList', link: ROUTE_PATH.taskToBeSend, title: '分派任务列表', permission: { 'permission': '分派任务列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "信息管理", icon: 'notification', link: "", key: 9, permission: "8", childrenComponent: [
        //         ...news_manage
        //     ]
        // },
        // {
        //     title: "补贴管理", icon: 'notification', link: "", key: 10, permission: "8", childrenComponent: [
        //         ...subsidyLink
        //     ]
        // },
        // {
        //     title: "调研系统", icon: 'notification', link: "", key: 11, permission: "8", childrenComponent: [
        //         ...researchLink
        //     ]
        // },
        // {
        //     title: "服务套餐", icon: 'notification', link: "", key: 12, permission: "8", childrenComponent: [
        //         { key: 'serviceProjectindex', link: ROUTE_PATH.serviceProjectindex, title: '服务套餐列表', permission: { 'permission': '服务套餐列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "居家服务", icon: 'notification', link: "", key: 19, permission: "8", childrenComponent: [
        //         { key: 'orderAllJ', link: ROUTE_PATH.orderAllJ, title: '订单全景', permission: { 'permission': '订单全景查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'orderStatistics', link: ROUTE_PATH.orderStatistics, title: '订单统计', permission: { 'permission': '订单统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "慈善管理", icon: 'notification', link: "", key: 13, permission: "8", childrenComponent: [
        //         // { key: 'charitableProject', link: ROUTE_PATH.charitableProject, title: '慈善项目管理', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'changeCharitableTitleFund', link: ROUTE_PATH.changeCharitableTitleFund, title: '新增慈善基金', permission: { 'permission': '冠名基金新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'viewCharitableTitleFund', link: ROUTE_PATH.viewCharitableTitleFund, title: '慈善基金列表', permission: { 'permission': '冠名基金查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'charitableTitleFund', link: ROUTE_PATH.charitableTitleFund, title: '慈善基金审核', permission: { 'permission': '冠名基金查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'changeCharitableDonate', link: ROUTE_PATH.changeCharitableDonate, title: '我要捐赠', permission: { 'permission': '慈善捐赠新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableDonate', link: ROUTE_PATH.charitableDonate, title: '捐赠信息明细', permission: { 'permission': '慈善捐赠查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableDonateList', link: ROUTE_PATH.charitableDonateApproval, title: '匹配捐款', permission: { 'permission': '慈善项目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'recipientApply', link: ROUTE_PATH.recipientApply, title: '个人募捐申请', permission: { 'permission': '募捐申请新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'organizationApply', link: ROUTE_PATH.organizationApply, title: '机构募捐申请', permission: { 'permission': '募捐申请新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'recipientApplyList', link: ROUTE_PATH.recipientApplyList, title: '募捐申请审核', permission: { 'permission': '募捐申请查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'viewRecipientApplyList', link: ROUTE_PATH.viewRecipientApplyList, title: '募捐申请列表', permission: { 'permission': '募捐申请查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableAppropriation', link: ROUTE_PATH.charitableAppropriation, title: '审核拨款', permission: { 'permission': '慈善拨款查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableInformation', link: ROUTE_PATH.charitableInformation, title: '慈善汇总', permission: { 'permission': '慈善信息查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableSociety', link: ROUTE_PATH.charitableSociety, title: '慈善会资料', permission: { 'permission': '慈善会资料查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'charitableAccount', link: ROUTE_PATH.charitableAccount, title: '慈善账户充值', permission: { 'permission': '慈善会资料查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'volunteerApply', link: ROUTE_PATH.volunteerApply, title: '志愿者申请', permission: { 'permission': '志愿者申请新增' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'volunteerApplyList', link: ROUTE_PATH.volunteerApplyList, title: '志愿者列表', permission: { 'permission': '志愿者申请查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'volunteerServiceCategory', link: ROUTE_PATH.volunteerServiceCategory, title: '志愿者服务类别', permission: { 'permission': '志愿者服务类别查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "报表管理", icon: 'notification', link: "", key: 14, permission: "8", childrenComponent: [
        //         { key: 'operationStatus', link: ROUTE_PATH.operationStatus, title: '机构运营情况', permission: { 'permission': '机构运营情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'basicFacilities', link: ROUTE_PATH.basicFacilities, title: '机构基本设施情况', permission: { 'permission': '基本设施情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'adoptionSituation', link: ROUTE_PATH.adoptionSituation, title: '机构收养人员情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'adoptionSituation', link: ROUTE_PATH.fireQualification, title: '机构消防资质情况', permission: { 'permission': '收养人员情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'workerConstitute', link: ROUTE_PATH.workerConstitute, title: '机构职工构成情况', permission: { 'permission': '职工构成情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'compensation', link: ROUTE_PATH.compensation, title: '机构薪酬待遇情况', permission: { 'permission': '薪酬待遇情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'compensation', link: ROUTE_PATH.organizationReport, title: '机构报表情况', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'compensation', link: ROUTE_PATH.staffStatistics, title: '福利院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'compensation', link: ROUTE_PATH.happyStaffStatistics, title: '幸福院工作人员统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'compensation', link: ROUTE_PATH.elderCheck, title: '福利院长者入住统计', permission: { 'permission': '机构报表情况查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "平台运营方", icon: 'notification', link: "", key: 34, permission: "8", childrenComponent: [
        //         { key: 'announcement', link: ROUTE_PATH.announcement, title: '系统公告', permission: { 'permission': '系统公告查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'announcement', link: ROUTE_PATH.announcementLook, title: '系统公告', permission: { 'permission': '查看系统公告查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'pictureManage', link: ROUTE_PATH.pictureManage, title: '图片管理', permission: { 'permission': '图片管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'targetSetting', link: ROUTE_PATH.targetSetting, title: '社区幸福院评比指标设置', permission: { 'permission': '社区幸福院评比指标设置查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'targetDetails', link: ROUTE_PATH.targetDetails, title: '社区幸福院评比详细指标', permission: { 'permission': '社区幸福院评比详细指标查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'appHome', link: ROUTE_PATH.appPageConfig, title: 'APP页面设置', permission: { 'permission': 'APP页面设置查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "系统管理", icon: 'notification', link: "", key: 33, permission: "8", childrenComponent: [
        //         { key: 'userManage', link: ROUTE_PATH.userManage, title: '用户管理', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'rolePermission', link: ROUTE_PATH.role, title: '角色设置', permission: { 'permission': '全部角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'rolePermissionLook', link: ROUTE_PATH.roleLook, title: '角色设置', permission: { 'permission': '角色管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'userRole', link: ROUTE_PATH.roleUser, title: '角色关联', permission: { 'permission': '角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'userRoleLook', link: ROUTE_PATH.roleUserLook, title: '角色关联', permission: { 'permission': '查看角色关联查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'usermanage', link: ROUTE_PATH.securitySettings, title: '安全设置', permission: { 'permission': '安全设置编辑' }, icon: 'font@pao_font_icon_set_bright' },
        //         { key: 'billSignature', link: ROUTE_PATH.billSignature, title: '单据签核', permission: { 'permission': '单据签核查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'initiatorBill', link: ROUTE_PATH.initiatorBill, title: '单据查询', permission: { 'permission': '单据查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         // { key: 'subsidyAutoRecharge', link: ROUTE_PATH.subsidyAutoRecharge, title: '补贴账户充值', permission: { 'permission': '单据签核查询' }, icon: 'font@pao_font_icon_user_bright' }
        //     ]
        // },
        // {
        //     title: "呼叫中心", icon: 'notification', link: "", key: 53, permission: "8", childrenComponent: [
        //         { key: 'callCenter', link: ROUTE_PATH.callCenter, title: '呼叫中心', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'emergencyCall', link: ROUTE_PATH.emergencyCall, title: '紧急呼叫', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'knowage', link: ROUTE_PATH.knowledgeBase, title: '知识库', permission: { 'permission': '呼叫中心查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: "居家养老报表", icon: 'notification', link: "", key: 153, permission: "8", childrenComponent: [
        //         { key: 'serviceProviderSettlement', link: ROUTE_PATH.townStreetSettlement, title: '服务商结算表', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'serviceProviderRecordSettlement', link: ROUTE_PATH.serviceProviderRecordSettlement, title: '服务商记录明细统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'orderReport', link: ROUTE_PATH.orderReport, title: '订单统计报表', permission: { 'permission': '订单统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'allowanceReportMonth', link: ROUTE_PATH.allowanceReportMonth, title: '居家养老服务对象月报表', permission: { 'permission': '居家养老服务对象月报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'allowanceReportDetails', link: ROUTE_PATH.allowanceReportDetails, title: '居家养老服务对象明细表', permission: { 'permission': '居家养老服务对象明细表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'workPeopleStatistics', link: ROUTE_PATH.workPeopleStatistics, title: '工作人员统计', permission: { 'permission': '工作人员统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'allowanceBaseSituation', link: ROUTE_PATH.baseSituation, title: '基本情况统计', permission: { 'permission': '基本情况统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'xfyFollowStatistics', link: ROUTE_PATH.xfyFollowStatistics, title: '公众号关注数统计', permission: { 'permission': '服务商结算表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '数据导入', icon: 'dataImport', link: '', key: 55, permission: "8", childrenComponent: [
        //         { key: 'callCenter', link: ROUTE_PATH.dataImport, title: '数据导入', permission: { 'permission': '数据导入查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '物料管理', icon: 'notification', link: '', key: 95, permission: "8", childrenComponent: [
        //         { key: 'Units', link: ROUTE_PATH.Units, title: '计量单位', permission: { 'permission': '计量单位查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'thingSort', link: ROUTE_PATH.thingSort, title: '物料分类', permission: { 'permission': '计量单位查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'thingArchives', link: ROUTE_PATH.thingArchives, title: '物料档案', permission: { 'permission': '物料档案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '健康照护', icon: 'notification', link: '', key: 86, permission: "8", childrenComponent: [
        //         { key: 'medicineFile', link: ROUTE_PATH.medicineFile, title: '药品档案', permission: { 'permission': '药品档案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'diseaseFile', link: ROUTE_PATH.diseaseFile, title: '疾病档案', permission: { 'permission': '疾病档案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'allergyFile', link: ROUTE_PATH.allergyFile, title: '过敏档案', permission: { 'permission': '过敏档案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '费用管理', icon: 'notification', link: '', key: 186, permission: "8", childrenComponent: [
        //         { key: 'orderAccounting', link: ROUTE_PATH.orderAccounting, title: 'app工单核算', permission: { 'permission': 'app工单核算查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'costStatistics', link: ROUTE_PATH.costStatistics, title: '费用统计', permission: { 'permission': '费用统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '日托服务', icon: 'notification', link: '', key: 286, permission: "8", childrenComponent: [
        //         { key: 'dayCareOrderList', link: ROUTE_PATH.dayCareOrderList, title: '日托订单列表', permission: { 'permission': '日托订单列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'dayCareOrderStatistics', link: ROUTE_PATH.dayCareOrderStatistics, title: '日托订单统计', permission: { 'permission': '日托订单统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '统计管理', icon: 'notification', link: '', key: 386, permission: "8", childrenComponent: [
        //         { key: 'checkManage', link: ROUTE_PATH.checkManage, title: '验收管理', permission: { 'permission': '验收管理列表查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'comprehensiveStatistics', link: ROUTE_PATH.comprehensiveStatistics, title: '综合统计', permission: { 'permission': '综合统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'organizationImformation', link: ROUTE_PATH.organizationImformation, title: '养老机构基本信息', permission: { 'permission': '养老机构基本信息查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'ckeckInElder', link: ROUTE_PATH.ckeckInElder, title: '入住长者情况统计', permission: { 'permission': '入住长者情况统计查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '智能监护', icon: 'notification', link: '', key: 486, permission: "8", childrenComponent: [
        //         { key: 'callSettings', link: ROUTE_PATH.callSettings, title: '通话设置', permission: { 'permission': '通话设置查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'careManage', link: ROUTE_PATH.careManage, title: '智能监护管理', permission: { 'permission': '智能监护管理查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'guardianshipOperation', link: ROUTE_PATH.guardianshipOperation, title: '智能监护运营', permission: { 'permission': '智能监护运营查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '长者管理', icon: 'notification', link: '', key: 586, permission: "8", childrenComponent: [
        //         { key: 'elderCareRecord', link: ROUTE_PATH.elderCareRecord, title: '长者关怀记录', permission: { 'permission': '长者关怀记录查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'careTypeSetting', link: ROUTE_PATH.careTypeSetting, title: '长者关怀类型设置', permission: { 'permission': '长者关怀类型设置查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '回访问卷', icon: 'notification', link: '', key: 686, permission: "8", childrenComponent: [
        //         { key: 'questionnaireList', link: ROUTE_PATH.questionnaireList, title: '回访问卷', permission: { 'permission': '回访问卷查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'subjectList', link: ROUTE_PATH.subjectList, title: '问卷题目', permission: { 'permission': '回访问卷题目查询' }, icon: 'font@pao_font_icon_user_bright' },
        //         { key: 'returnVisitList', link: ROUTE_PATH.returnVisitList, title: '意向回访', permission: { 'permission': '意向回访查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // },
        // {
        //     title: '档案管理', icon: 'notification', link: '', key: 786, permission: "8", childrenComponent: [
        //         { key: 'organizationManage', link: ROUTE_PATH.organizationManage, title: '机构信息档案', permission: { 'permission': '机构信息档案查询' }, icon: 'font@pao_font_icon_user_bright' },
        //     ]
        // }
    ],

    [
        // { key: "scan", icon: "antd@scan", link: "/scan" }
    ],
    function () {
        CookieUtil.remove(COOKIE_KEY_CURRENT_USER);
        CookieUtil.remove(COOKIE_KEY_USER_ROLE);
        CookieUtil.remove(COOKIE_OLDER_CALL_CENTER);
    },
    '/login',
    undefined,
    (pathname: string) => selectedKeys(pathname),
    AppServiceUtility.login_service,
    { logo_path: backstage_nain_form_logo, history_path: '/home', callCenterInstance: callCenterInstance },
    ROUTE_PATH.callCenter,
    ROUTE_PATH
);

/** 带头部窗体 */
const HeadMainForm = new HeadMainFormControl(
    // 'src/projects/industryInternetSign/img/logo.png',
    manage_logo,
    '智慧养老服务平台'
);
const headMainID = 'HeadMainForm';
const blankID = 'blank';

/** 权限列表 */
// const permission_list = [
//     { module: "SystemManage", module_name: "系统管理", per_name: "用户增删改查权限", permission: "UserManage_Select", permission_state: "default" },
//     { module: "SystemManage", module_name: "系统管理", per_name: "角色增删改查权限", permission: "RolePermission_Select", permission_state: "default" },
//     { module: "SystemManage", module_name: "系统管理", per_name: "安全设置增删改查权限", permission: "SecuritySettings_Select", permission_state: "default" },
// ];

const router = new AppReactRouterControl(
    {
        [blankID]: blank,
        [layoutMain]: layoutMainForm,
        [headMainID]: HeadMainForm
    },
    [
        {
            path: '/',
            exact: true,
            redirect: '/login'
        },
        /** =================================系统公告================================= */
        {
            path: ROUTE_PATH.announcement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AnnouncementViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_annoutment_list_all',
                    select_permission: '系统公告查看',
                    edit_permission: '系统公告编辑',
                    delete_permission: '系统公告删除',
                    add_permission: '系统公告新增',
                }),
        },

        {
            path: `${ROUTE_PATH.changeAnnouncement}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAnnouncementViewControl,
                {
                })
        },
        {
            path: ROUTE_PATH.changeAnnouncement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAnnouncementViewControl,
                {
                })
        },
        /** =================================服务项目包部分============================= */
        {
            path: `${ROUTE_PATH.changeServiceItemPackage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceItemPackageViewControl,
                {
                })
        },
        {
            path: `${ROUTE_PATH.changeServiceItemPackageSh}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProductShControl,
                {
                })
        },
        {
            path: ROUTE_PATH.changeServiceItemPackage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceItemPackageViewControl,
                {
                })
        },
        // 服务产品
        {
            path: ROUTE_PATH.serviceItemPackage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceItemPackageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_item_package_pure',
                    select_permission: '服务产品查看',
                    edit_permission: '服务产品编辑',
                    delete_permission: '服务产品删除',
                    add_permission: '服务产品新增',
                }),
        },
        // 服务产品审核
        {
            path: ROUTE_PATH.serviceItemPackageSh,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceItemPackageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    // request_url: 'get_service_item_package_all',
                    request_url: 'get_service_item_package_pure',
                    select_permission: '服务产品审核查看',
                    edit_permission: '服务产品审核编辑',
                    list_type: 'sh',
                }),
        },
        // 服务产品
        {
            path: ROUTE_PATH.serviceItemPackage1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceItemPackageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_item_package_pure',
                    select_permission: '服务产品查看',
                    edit_permission: '服务产品编辑',
                    delete_permission: '服务产品删除',
                    add_permission: '服务产品新增',
                }),
        },
        // 查看服务产品
        {
            path: ROUTE_PATH.serviceItemPackageLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceItemPackageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_item_package_look',
                    select_permission: '查看服务产品查看',
                }),
        },
        /** =================================交易管理部分=============================== */
        {
            path: `${ROUTE_PATH.changeBedManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBedManageViewControl,
                {
                })
        },
        {
            path: ROUTE_PATH.changeBedManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBedManageViewControl,
                {
                })
        },
        {
            path: ROUTE_PATH.bedManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BedManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '床位管理查看',
                    edit_permission: '床位管理编辑',
                    delete_permission: '床位管理删除',
                    add_permission: '床位管理新增',
                }),
        },
        {
            path: ROUTE_PATH.bedManageLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BedManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_look',
                    select_permission: '查看床位情况查看',
                }),
        },
        {
            path: `${ROUTE_PATH.changeTransaction}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTransactionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.transaction,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                TransactionManageViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                }),
        },
        {
            path: ROUTE_PATH.transactionComment,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                TransactionCommentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        /** =================================财务出纳部分=============================== */
        {
            path: `${ROUTE_PATH.changeFinancialReturn}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountReturnViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        }, {
            path: ROUTE_PATH.changeFinancialReturn,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountReturnViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.financialAccountFlow,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialAccountFlowViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_account_flow_list_look',
                    select_permission: '账户流水查看',
                    edit_permission: '账户流水编辑',
                    add_permission: '账户流水新增',
                }),
        }, {
            path: ROUTE_PATH.changereceive,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeReceiveViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: ROUTE_PATH.changepayment,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePaymentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: ROUTE_PATH.changetransfer,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTransferViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        /** =================================财务账簿账户管理部分=============================== */
        {
            path: ROUTE_PATH.financialAccountBook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialAccountBookViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_account_book_list_manage',
                    select_permission: '账簿管理查看',
                    edit_permission: '账簿管理编辑',
                    add_permission: '账簿管理新增',
                }),
        }, {
            path: ROUTE_PATH.financialAccountManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                FinancialAccountViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_account_list_all',
                    select_permission: '床位管理查看',
                    edit_permission: '床位管理编辑',
                    delete_permission: '床位管理删除',
                    add_permission: '床位管理新增',
                }),
        }, {
            path: `${ROUTE_PATH.financialAccountManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialAccountViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: `${ROUTE_PATH.changefinancialAccountBook}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountBookViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: ROUTE_PATH.changefinancialAccountBook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountBookViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: `${ROUTE_PATH.changefinancialAccount}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: ROUTE_PATH.changefinancialAccount,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        /** =========================记账凭证===================================== */
        {
            path: ROUTE_PATH.financialAccountVoucherManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialVoucherViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: `${ROUTE_PATH.changeFinancialVoucher}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountVoucherViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        {
            path: ROUTE_PATH.changeFinancialVoucher,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAccountVoucherViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        /** =========================会计科目管理================================= */
        {
            path: ROUTE_PATH.financialSubjectManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialSubjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        }, {
            path: `${ROUTE_PATH.changeFinancialSubject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeFinancialSubjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        {
            path: ROUTE_PATH.changeFinancialSubject,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeFinancialSubjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }),
        },
        /** =========================服务运营管理部分============================== */
        // 服务商登记
        {
            path: ROUTE_PATH.serviceProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProviderViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_Subsidy_service_provider_list_all',
                    add_permission: '服务商登记新增',
                    select_permission: '服务商登记查询',
                    edit_permission: '服务商登记编辑',
                }),
        },
        // 服务商审核
        {
            path: `${ROUTE_PATH.ProviderApplyReviewed}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProviderReviewedViewControl,
                {
                }),
        },
        // 服务商申请
        {
            path: ROUTE_PATH.serviceProviderApply,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ApplyServiceProviderViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    // 没有配服务权限，先还原
                    // request_url: 'get_current_service_provider_add',
                    request_url: 'get_current_service_provider',
                    add_permission: '服务商申请新增',
                }),
        },
        {
            path: `${ROUTE_PATH.changeServiceProvider}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceProviderViewControl,
                {
                }
            )
        },
        // {
        //     path: ROUTE_PATH.serviceProviderApply,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         ApplyServiceProviderViewControl,
        //         {
        //             permission_class: AppServiceUtility.login_service,
        //             get_permission_name: 'get_function_list',
        //             // 没有配服务权限，先还原
        //             // request_url: 'get_current_service_provider_add',
        //             request_url: 'get_current_service_provider',
        //             add_permission: '服务商申请新增',
        //         }),
        // },
        {
            path: `${ROUTE_PATH.ChangeOrgInfoRecore}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrgInfoRecordViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.ChangeOrgInfoRecore}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrgInfoRecordViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.changeServiceProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceProviderViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.platformSettlement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PlatformSettlementViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.serviceProviderSettlements,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProviderSettlementsViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.servicePersonnelSettlement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServicePersonnelSettlementViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.charitySettlement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CharitySettlementViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.incomeAssessedSettlement,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                incomeAssessedSettlementViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.incomeAssessedSettlement1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                incomeAssessedSettlementViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.subsidyServiceProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                subsidyServiceProviderViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.serviceType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceTypeViewControl,
                {
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_type_list',
                    select_permission: '服务项目类别查看',
                    edit_permission: '服务项目类别编辑',
                    delete_permission: '服务项目类别删除',
                    add_permission: '服务项目类别新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeServiceType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServiceTypeViewControl)
        },
        {
            path: ROUTE_PATH.changeServiceType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServiceTypeViewControl)
        },
        {
            path: ROUTE_PATH.serviceItem,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceItemViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeServiceItem}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceItemViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeServiceItem,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceItemViewControl,
                {}
            )
        },
        // 双鸭山报表
        {
            path: ROUTE_PATH.sysbb,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysbbViewControl,
                {
                    select_param: { type: '长者类型' },
                }
            )
        },
        {
            path: ROUTE_PATH.sysbb1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysbbViewControl,
                {
                    select_param: { type: '居家长者' },
                }
            )
        },
        {
            path: ROUTE_PATH.sysbb2,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysbbViewControl,
                {
                    select_param: { type: '幸福院' },
                }
            )
        },
        {
            path: ROUTE_PATH.sysbb3,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysbbViewControl,
                {
                    select_param: { type: '机构' },
                }
            )
        },
        // 服务订单
        {
            path: ROUTE_PATH.serviceOrder,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOrderViewControl,
                {
                    request_url: 'get_service_order_fwyy_fwdd_list',
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '服务订单查询',
                    add_permission: '服务订单导出',
                    delete_permission: '服务订单撤销',
                    // select_param: { 'need_fp': true }
                }
            )
        },
        // 服务订单完成
        {
            path: ROUTE_PATH.serviceOrder1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOrderViewControl,
                {
                    request_url: 'get_service_order_list',
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '工作人员用户管理查看',
                    select_param: { status: '已完成' },
                }
            )
        },
        // /** 新增/修改护理内容 */
        // {
        //     path: `${ROUTE_PATH.addNursing}/${KEY_PARAM}`,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         addNursingControl,
        //         {
        //             assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(nursingContentService, remote.url, "INursingService"),
        //             permission: PermissionList.RolePermission_Select,
        //         }
        //     )
        // },
        // /** 新增/修改护理内容 */
        // {
        //     path: ROUTE_PATH.addNursing,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         addNursingControl,
        //         {
        //             assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
        //             permission: PermissionList.RolePermission_Select,
        //         })

        // },
        // 评估项目
        {
            path: ROUTE_PATH.assessmentProjectNursing,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_project_list',
                    del_url: 'delete_nursing_project',
                    select_permission: '评估项目查询',
                    edit_permission: '评估项目编辑',
                    delete_permission: '评估项目删除',
                    add_permission: '评估项目新增',
                }
            )
        },
        {
            path: ROUTE_PATH.assessmentProjectNursingLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_project_list_look',
                    select_permission: '查看护理项目查询'
                }
            )
        },
        // 护理管理
        {
            path: ROUTE_PATH.nursingContent,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                nursingContentControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_content_list',
                    select_permission: '护理内容查看',
                    edit_permission: '护理内容编辑',
                    delete_permission: '护理内容删除',
                    add_permission: '护理内容新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.addNursingContent}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddNursingContentViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.addNursingContent,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddNursingContentViewControl,
                {}
            )
        },
        // 长者护理记录
        {
            path: ROUTE_PATH.elderNursingRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElderBursingRecordControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_record_list',
                    select_permission: '长者护理记录查看',
                    edit_permission: '长者护理记录编辑',
                    delete_permission: '长者护理记录删除',
                    add_permission: '长者护理记录新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.addElderNursingRecord}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddNursingRecordViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.addElderNursingRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddNursingRecordViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.nursingRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                nursingRecordControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_competence_assessment_list_all',
                    select_permission: '护理内容查看',
                    edit_permission: '护理内容编辑',
                    delete_permission: '护理内容删除',
                    add_permission: '护理内容新增',
                }
            )
        },
        /** 新增/修改护理模板 */
        {
            path: `${ROUTE_PATH.addNursingTemplate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                addNursingTemplateControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(nursingTemplateService, remote.url, "INursingTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        /** 新增/修改护理模板 */
        {
            path: ROUTE_PATH.addNursingTemplate,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                addNursingTemplateControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(nursingTemplateService, remote.url, "INursingTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        /** 新增/修改护理内容 */
        {
            path: `${ROUTE_PATH.competenceAssessmentList}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 服务订单分派
        {
            path: ROUTE_PATH.serviceOrderTask,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOrderTaskViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_order_fwddfp_list',
                    select_permission: '服务订单分派管理查看',
                    edit_permission: '服务订单分派管理编辑',
                    delete_permission: '服务订单分派管理删除',
                    add_permission: '服务订单分派管理新增',
                }
            )
        },
        // 服务订单分派管理
        {
            path: ROUTE_PATH.serviceOrderTaskLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOrderTaskViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_order_list',
                    select_permission: '服务订单分派管理查看',
                    edit_permission: '服务订单分派管理编辑',
                    delete_permission: '服务订单分派管理删除',
                    add_permission: '服务订单分派管理新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeServiceOrder}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceOrderViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeServiceOrder,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceOrderViewControl,
                {}
            )
        },
        ...billRouter,
        // 业务区域管理
        {
            path: ROUTE_PATH.businessArea,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BusinessAreaManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_business_area_list_all',
                    select_permission: '行政区划管理查看',
                    edit_permission: '行政区划管理编辑',
                    delete_permission: '行政区划管理删除',
                    add_permission: '行政区划管理新增',
                }),
        },
        // 查看业务区域
        {
            path: ROUTE_PATH.businessArea,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BusinessAreaManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_business_area_list_look_delete',
                    select_permission: '查看行政区划查看',
                }),
        },
        {
            path: `${ROUTE_PATH.changeBusinessArea}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBusinessAreaViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.changeBusinessArea,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBusinessAreaViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        // 行政区划管理
        {
            path: ROUTE_PATH.administrationDivision,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AdministrationDivisionViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_admin_division_list',
                select_permission: '行政区划管理查看',
                edit_permission: '行政区划管理编辑',
                delete_permission: '行政区划管理删除',
                add_permission: '行政区划管理新增',
            }),
        },
        // 查看行政区划
        {
            path: ROUTE_PATH.administrationDivisionLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AdministrationDivisionViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_admin_division_list_look',
                select_permission: '查看行政区划查看',
            }),
        },
        {
            path: `${ROUTE_PATH.changeAdministrationDivision}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAdministrationDivisionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.changeAdministrationDivision,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAdministrationDivisionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.reservationRegistration,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReservationRegistrationViewControl,
                {
                    request_url: 'get_reservation_registration_list_all',
                    get_permission_name: 'get_function_list',
                    permission_class: AppServiceUtility.login_service,
                    select_permission: '预约登记查看',
                    edit_permission: '预约登记编辑',
                    delete_permission: '预约登记删除',
                    add_permission: '预约登记新增',
                }),
        },
        {
            path: ROUTE_PATH.reservationRegistrationLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReservationRegistrationViewControl,
                {
                    request_url: 'get_reservation_registration_list_look',
                    get_permission_name: 'get_function_list',
                    permission_class: AppServiceUtility.login_service,
                    select_permission: '查看预约登记查看',
                }),
        },
        {
            path: ROUTE_PATH.reservationRegistrationLook1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReservationRegistrationViewControl,
                {
                    request_url: 'get_reservation_registration_list_look',
                    get_permission_name: 'get_function_list',
                    permission_class: AppServiceUtility.login_service,
                    select_permission: '查看预约登记查看',
                }),
        },
        {
            path: `${ROUTE_PATH.reservationRegistrationEditor}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeReservationViewControl),
        },
        {
            path: ROUTE_PATH.reservationRegistrationEditor,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeReservationViewControl),
        },

        {
            path: `${ROUTE_PATH.changeReservationRegistration}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangereservationRegistrationViewControl),
        },
        {
            path: ROUTE_PATH.changeReservationRegistration,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangereservationRegistrationViewControl),
        },
        {
            path: ROUTE_PATH.roomArchives,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RoomArchivesViewControl, {
                roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
            }),
        },
        {
            path: ROUTE_PATH.roomEdit,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(ChangeRoomArchivesViewControl, {
                roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
            }),
        },
        {
            path: `${ROUTE_PATH.roomEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeRoomArchivesViewControl, {
                roomService_Fac: new AjaxJsonRpcLoadingFactory(IRoomService, remote.url, "IServiceProcess"),
            }),
        },
        /** 角色列表 */
        {
            path: ROUTE_PATH.role,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoleViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_role_list_all',
                    select_permission: '全部角色管理查询',
                    edit_permission: '全部角色管理编辑',
                    delete_permission: '全部角色管理删除',
                    add_permission: '全部角色管理新增',

                })
        },
        {
            path: ROUTE_PATH.roleLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoleViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_role_list_look',
                    select_permission: '查看角色管理查询'

                })
        },
        {
            path: `${ROUTE_PATH.changeRole}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AddRoleViewControl)
        },
        {
            path: ROUTE_PATH.changeRole,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AddRoleViewControl)
        },
        /** end */
        /** 角色关联 */
        {
            path: ROUTE_PATH.roleUser,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RolePermissionViewControl,
                {
                    roleService: AppServiceUtility.role_service,
                    permission: PermissionList.RolePermission_Select,
                    request_url: 'get_role_user_list_all',
                    get_permission_name: 'get_function_list',
                    permission_class: AppServiceUtility.login_service,
                    select_permission: '角色关联查询',
                    edit_permission: '角色关联编辑',
                    delete_permission: '角色关联删除',
                    add_permission: '角色关联新增',

                })
        },
        {
            path: ROUTE_PATH.roleUserLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RolePermissionViewControl,
                {
                    roleService: AppServiceUtility.role_service,
                    permission: PermissionList.RolePermission_Select,
                    request_url: 'get_role_user_list_look',
                    get_permission_name: 'get_function_list',
                    permission_class: AppServiceUtility.login_service,
                    select_permission: '查看角色关联查询',

                })
        },
        {
            path: `${ROUTE_PATH.changeRoleUser}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AddRoleUserViewControl)
        },
        {
            path: ROUTE_PATH.changeRoleUser,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(AddRoleUserViewControl)
        },
        /** 角色关联 end */
        {
            path: ROUTE_PATH.nursingArchives,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingArchivesViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.nursingItem,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingItemViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.nursingType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.nursingTypeEdit,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingTypeEditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        },
        {
            path: `${ROUTE_PATH.nursingTypeEdit}/${KEY_PARAM}`,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingTypeEditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        },
        {
            path: ROUTE_PATH.nursingItemEdit,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingItemViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        },
        {
            path: `${ROUTE_PATH.nursingItemEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingItemViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        },
        {
            path: `${ROUTE_PATH.nursingRelationShip}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                NursingRelationShipViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        },
        {
            path: `${ROUTE_PATH.nursingRelationShipEdit}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                NursingRelationShipEditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        }, {
            path: `${ROUTE_PATH.nursingRelationShipEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                NursingRelationShipEditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.archivesEditor,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingArchivesViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.archivesEditor}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingArchivesViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 人员类别
        {
            path: ROUTE_PATH.personnelClassification,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelClassificationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_personnel_classification_list',
                    select_permission: '人员类别管理查询',
                    edit_permission: '人员类别管理编辑',
                    delete_permission: '人员类别管理删除',
                    add_permission: '人员类别管理新增',
                    // select_type: {},
                })
        },
        // 修改新增人员类别
        {
            path: `${ROUTE_PATH.changePersonnelClassification}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelClassificationViewControl,
                {
                })
        },
        {
            path: ROUTE_PATH.changePersonnelClassification,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelClassificationViewControl,
                {
                })
        },
        // 人员管理  志愿者管理
        {
            path: ROUTE_PATH.personnel,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AllPersonnelViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    change_url: ROUTE_PATH.changePersonnel,
                    delete_url: "delete_personnel_info",
                    request_url: 'get_personnel_list_all',
                    select_permission: '人员管理查询',
                    edit_permission: '人员管理编辑',
                    delete_permission: '人员管理删除',
                    add_permission: '人员管理新增',
                    select_type: {},
                })
        },
        // 人员管理  志愿者管理
        {
            path: ROUTE_PATH.personnel_zyz,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    change_url: ROUTE_PATH.changePersonnel,
                    delete_url: "delete_personnel_info",
                    request_url: 'get_personnel_list_all',
                    select_permission: '人员管理查询',
                    edit_permission: '人员管理编辑',
                    delete_permission: '人员管理删除',
                    add_permission: '人员管理新增',
                    select_type: { type: '志愿者' },
                })
        },
        // 人员管理  义工管理
        {
            path: ROUTE_PATH.personnel_yg,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    change_url: ROUTE_PATH.changePersonnel,
                    delete_url: "delete_personnel_info",
                    request_url: 'get_personnel_list_all',
                    select_permission: '人员管理查询',
                    edit_permission: '人员管理编辑',
                    delete_permission: '人员管理删除',
                    add_permission: '人员管理新增',
                    select_type: { type: '义工' },
                })
        },
        // 人员管理
        {
            path: ROUTE_PATH.personnel1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    change_url: ROUTE_PATH.changePersonnel,
                    delete_url: "delete_personnel_info",
                    request_url: 'get_personnel_list_all',
                    select_permission: '人员管理查询',
                    edit_permission: '人员管理编辑',
                    delete_permission: '人员管理删除',
                    add_permission: '人员管理新增',
                })
        },
        // 人员管理
        {
            path: ROUTE_PATH.personnel2,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    change_url: ROUTE_PATH.changePersonnel,
                    delete_url: "delete_personnel_info",
                    request_url: 'get_personnel_list_all',
                    select_permission: '人员管理查询',
                    edit_permission: '人员管理编辑',
                    delete_permission: '人员管理删除',
                    add_permission: '人员管理新增',
                    select_type: { type: '义工' },
                })
        },
        {
            path: ROUTE_PATH.wokerPersonnel,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerPersonnelViewControl,
                {
                    change_url: ROUTE_PATH.changewokerPersonnel,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    delete_url: 'delete_worker_info',
                    select_permission: '工作人员管理查询',
                    edit_permission: '工作人员管理编辑',
                    delete_permission: '工作人员管理删除',
                    add_permission: '工作人员管理新增',
                    type: '福利院'
                })
        },
        {
            path: ROUTE_PATH.wokerPersonnelProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerPersonnelViewControl,
                {
                    change_url: ROUTE_PATH.changewokerPersonnelFws,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    delete_url: 'delete_worker_info',
                    select_permission: '工作人员管理查询',
                    edit_permission: '工作人员管理编辑',
                    delete_permission: '工作人员管理删除',
                    add_permission: '工作人员管理新增',
                    type: '服务商'
                })
        },
        {
            path: ROUTE_PATH.HappinessWorkerEntryControl,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerPersonnelViewControl,
                {
                    change_url: ROUTE_PATH.changewokerPersonnelXfy,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    delete_url: 'delete_worker_info',
                    select_permission: '工作人员管理查询',
                    edit_permission: '工作人员管理编辑',
                    delete_permission: '工作人员管理删除',
                    add_permission: '工作人员管理新增',
                    type: '幸福院'
                })
        },
        {
            path: ROUTE_PATH.wokerPersonnelPlatform,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerPersonnelViewControl,
                {
                    change_url: ROUTE_PATH.changewokerPersonnel,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    delete_url: 'delete_worker_info',
                    select_permission: '工作人员管理查询',
                    edit_permission: '工作人员管理编辑',
                    delete_permission: '工作人员管理删除',
                    add_permission: '工作人员管理新增',
                    type: '平台'
                })
        },
        {
            path: ROUTE_PATH.wokerPersonnelCivil,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerPersonnelViewControl,
                {
                    change_url: ROUTE_PATH.changewokerPersonnel,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    delete_url: 'delete_worker_info',
                    select_permission: '工作人员管理查询',
                    edit_permission: '工作人员管理编辑',
                    delete_permission: '工作人员管理删除',
                    add_permission: '工作人员管理新增',
                    type: '民政'
                })
        },
        // 人员权限配置=====
        // 长者健康信息
        {
            path: ROUTE_PATH.elderHealthy,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElderHealthyListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_healthy_list_all',
                    select_permission: '健康信息查询',
                })
        },
        {
            path: ROUTE_PATH.messageListNew,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                MessageListNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_message_list_new',
                    select_permission: '消息列表查询',
                })
        },
        // 长者用餐统计信息
        {
            path: ROUTE_PATH.elderFood,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElderFoodListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_food_list_all',
                    select_permission: '用餐管理查询',
                    edit_permission: '用餐管理编辑',
                    delete_permission: '用餐管理删除',
                    add_permission: '用餐管理新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeElderFood}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeEdlerFoodViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeElderFood,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeEdlerFoodViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 排班类型
        {
            path: ROUTE_PATH.setClassType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SetClassTypeListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_set_class_list_all',
                    select_permission: '排班管理查询',
                    edit_permission: '排班管理编辑',
                    delete_permission: '排班管理删除',
                    add_permission: '排班管理新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeSetClassType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSetClassTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeSetClassType,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSetClassTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 护理收费
        {
            path: ROUTE_PATH.nursingPay,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingPayListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_pay_list_all',
                    select_permission: '护理收费查询',
                    edit_permission: '护理收费编辑',
                    delete_permission: '护理收费除',
                    add_permission: '护理收费新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeNursingPay}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingPayViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeNursingPay,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNursingPayViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // APP首页设置
        {
            path: ROUTE_PATH.appPageConfig,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppPageConfigListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_pay_list_all',
                    select_permission: 'APP页面设置查询',
                    edit_permission: 'APP页面设置编辑',
                    delete_permission: 'APP页面设置删除',
                    add_permission: 'APP页面设置新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeAppPageConfig}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppPageConfigViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeAppPageConfig,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppPageConfigViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 社区幸福院自评汇总
        {
            path: ROUTE_PATH.happinessYearSelfAssessment,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                HappinessYearSelfAssessmentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '社区幸福院自评汇总查询',
                })
        },
        {
            path: `${ROUTE_PATH.changeHappinessYearSelfAssessment}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHappinessYearSelfAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 社区幸福院年度自评
        {
            path: ROUTE_PATH.yearSelfAssessment,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                YearSelfAssessmentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '社区幸福院年度自评查询',
                    edit_permission: '社区幸福院年度自评编辑',
                    delete_permission: '社区幸福院年度自评删除',
                    add_permission: '社区幸福院年度自评新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeYearSelfAssessment}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeYearSelfAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeYearSelfAssessment,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeYearSelfAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 服务计划
        {
            path: ROUTE_PATH.servicePlan,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServicePlanListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_plan_list_all',
                    select_permission: '服务计划查询',
                    edit_permission: '服务计划编辑',
                    delete_permission: '服务计划删除',
                    add_permission: '服务计划新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeServicePlan}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServicePlanViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeServicePlan,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServicePlanViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 团体列表
        {
            path: ROUTE_PATH.groups,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                GroupsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_groups_list_all',
                    select_permission: '团体列表查询',
                    edit_permission: '团体列表编辑',
                    delete_permission: '团体列表删除',
                    add_permission: '团体列表新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeGroups}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeGroupsViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeGroups,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeGroupsViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 评比方案
        {
            path: ROUTE_PATH.ratingPlan,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RatingPlanListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_rating_plan_list_all',
                    select_permission: '评比方案查询',
                    edit_permission: '评比方案编辑',
                    delete_permission: '评比方案删除',
                    add_permission: '评比方案新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeRatingPlan}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRatingPlanViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeRatingPlan,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRatingPlanViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 评比
        {
            path: ROUTE_PATH.rating,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RatingListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_rating_list_all',
                    select_permission: '评比填表查询',
                    edit_permission: '评比填表编辑',
                    delete_permission: '评比填表删除',
                    add_permission: '评比填表新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeRating}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRatingViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeRating,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRatingViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 排班管理
        {
            path: ROUTE_PATH.setClass,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SetClassListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_set_class_list_all',
                    select_permission: '排班管理查询',
                    edit_permission: '排班管理编辑',
                    delete_permission: '排班管理删除',
                    add_permission: '排班管理新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeSetClass}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSetClassViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeSetClass,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSetClassViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 收费减免
        {
            path: ROUTE_PATH.deduction,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeductionListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_deduction_list_all',
                    select_permission: '收费减免查询',
                    edit_permission: '收费减免编辑',
                    delete_permission: '收费减免删除',
                    add_permission: '收费减免新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeDeduction}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeductionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeDeduction,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeductionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 长者管理
        {
            path: ROUTE_PATH.elderInfo,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        // 长者档案
        {
            path: ROUTE_PATH.elderInfoPT,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                    type: '平台运营'
                })
        },
        {
            path: ROUTE_PATH.monthSalaryStatistic,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                MonthSalaryStatisticViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_month_salary_list',
                    personnel_category: '工作人员',
                    select_permission: '月收入统计查询',
                    edit_permission: '月收入统计编辑',
                    add_permission: '月收入统计新增',
                })
        }, {
            path: `${ROUTE_PATH.changeMonthSalaryStatistic}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMonthSalaryStatisticViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeMonthSalaryStatistic}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMonthSalaryStatisticViewControl,
                {
                }
            )
        }, {
            path: ROUTE_PATH.ChargeType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChargeTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charge_list',
                    select_permission: '收费类型查询',
                    edit_permission: '收费类型编辑',
                    delete_permission: '收费类型删除',
                    add_permission: '收费类型新增',
                })
        },
        {
            path: `${ROUTE_PATH.ChangeChargeType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeChargeTypeViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.ChangeChargeType}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeChargeTypeViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.olderBirthNotify,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OlderBirthNotifyControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_user_elder_birth_list',
                    select_permission: '长者管理查询',
                })
        },
        {
            path: ROUTE_PATH.OrgInfoRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrgInfoRecordControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_org_info_record_list',
                    select_permission: '机构信息档案查询',
                    edit_permission: '机构信息档案编辑',
                })
        },
        {
            path: ROUTE_PATH.OperateSituationStatistic,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OperateSituationStatisticViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_operate_sitation_statistic',
                    select_permission: '社区养老运营情况统计查询',
                })
        },
        {
            path: ROUTE_PATH.elderInfo1,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: ROUTE_PATH.elderInfo2,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: ROUTE_PATH.elderInfo3,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: ROUTE_PATH.elderInfo4,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: ROUTE_PATH.elderInfo5,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_all',
                    delete_url: 'delete_elder_info',
                    select_permission: '长者管理查询',
                    edit_permission: '长者管理编辑',
                    delete_permission: '长者管理删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: ROUTE_PATH.orgElderInfo,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_by_org',
                    delete_url: 'delete_elder_info',
                    select_permission: '机构长者管理查询',
                    edit_permission: '机构长者管理编辑',
                    delete_permission: '机构长者管理删除',
                    add_permission: '机构长者管理新增',
                })
        },
        // 查看长者资料
        {
            path: ROUTE_PATH.elderInfoLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelViewControl,
                {
                    personnel_type: '4',
                    change_url: ROUTE_PATH.changeElderInfo,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_elder_list_look',
                    select_permission: '查看长者资料查寻',
                    edit_permission: '查看长者资料编辑',
                    delete_permission: '查看长者资料删除',
                    add_permission: '查看长者资料新增',
                })
        },
        {
            path: ROUTE_PATH.changePersonnel,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_personnel_info',
                    select_face: 'get_personnel_list_all',
                    back_url: ROUTE_PATH.personnel

                })
        }, {
            path: `${ROUTE_PATH.changePersonnel}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_personnel_info',
                    select_face: 'get_personnel_list_all',
                    back_url: ROUTE_PATH.personnel
                })
        },
        {
            path: ROUTE_PATH.changeElderInfo,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelViewControl,
                {
                    back_url: ROUTE_PATH.elderInfo,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_elder_info',
                    select_face: 'get_elder_list_all',
                    personnel_category: '长者',
                })
        }, {
            path: `${ROUTE_PATH.changeElderInfo}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelViewControl,
                {
                    back_url: ROUTE_PATH.elderInfo,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_elder_info',
                    select_face: 'get_elder_list_all',
                    // personnel_category: '长者',
                })
        },
        // 工作人员编辑页面 
        {
            path: ROUTE_PATH.changewokerPersonnel,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.wokerPersonnel,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        }, {
            path: `${ROUTE_PATH.changewokerPersonnel}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.wokerPersonnel,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        },
        // 工作人员编辑页面 
        {
            path: ROUTE_PATH.changewokerPersonnelXfy,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.HappinessWorkerEntryControl,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        }, {
            path: `${ROUTE_PATH.changewokerPersonnelXfy}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.HappinessWorkerEntryControl,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        },
        // 工作人员编辑页面 
        {
            path: ROUTE_PATH.changewokerPersonnelFws,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.wokerPersonnelProvider,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        }, {
            path: `${ROUTE_PATH.changewokerPersonnelFws}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangePersonnelWorkerViewControl,
                {
                    back_url: ROUTE_PATH.wokerPersonnelProvider,
                    permission: PermissionList.RolePermission_Select,
                    submit_face: 'update_worker_info',
                    select_face: 'get_worker_list_all',
                    personnel_category: '工作人员',
                })
        },
        // 组织机构-old
        {
            path: ROUTE_PATH.organization,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        },
        // 组织机构-new
        {
            path: ROUTE_PATH.organizationFly,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_new',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    org_type: '福利院'
                })
        },
        // 组织机构-new
        {
            path: ROUTE_PATH.organizationFws,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_new',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    org_type: '服务商'
                })
        },
        // 组织机构-new
        {
            path: ROUTE_PATH.organizationPt,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_new',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        },
        // 组织机构-new
        {
            path: ROUTE_PATH.organizationXfy,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_new',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    org_type: '幸福院'
                })
        },
        // 组织机构-new
        {
            path: ROUTE_PATH.organizationMz,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageNewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_new',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    org_type: '民政'
                })
        },
        // 幸福院
        {
            path: ROUTE_PATH.organization_xfy,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationXFYViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_xfy_tree_list',
                    select_permission: '幸福院查询',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organizationServicer,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    organization_type: '服务商',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization2,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization3,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization4,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        }
        , {
            path: ROUTE_PATH.waterElectricity,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                WaterElectricityViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_water_electricity',
                    select_permission: '水电抄表查看',
                    edit_permission: '水电抄表编辑',
                    delete_permission: '水电抄表删除',
                    add_permission: '水电抄表新增',
                })
        },
        {
            path: `${ROUTE_PATH.waterElectricity}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                WaterElectricityViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_water_electricity',
                    select_permission: '水电抄表查看',
                    edit_permission: '水电抄表编辑',
                    delete_permission: '水电抄表删除',
                    add_permission: '水电抄表新增',
                })
        },
        {
            path: `${ROUTE_PATH.otherCost}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                OtherCostViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_other_cost',
                    select_permission: '其他费用查询',
                    edit_permission: '其他费用编辑',
                    delete_permission: '其他费用删除',
                    add_permission: '其他费用新增',
                })
        },
        {
            path: ROUTE_PATH.otherCost,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                OtherCostViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_other_cost',
                    select_permission: '其他费用查询',
                    edit_permission: '其他费用编辑',
                    delete_permission: '其他费用删除',
                    add_permission: '其他费用新增',
                })
        },
        {
            path: `${ROUTE_PATH.costType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                CostTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_cost_type',
                    select_permission: '费用类型查询',
                    edit_permission: '费用类型编辑',
                    delete_permission: '费用类型删除',
                    add_permission: '费用类型新增',
                })
        },
        {
            path: ROUTE_PATH.costType,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                CostTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_cost_type',
                    select_permission: '费用类型查询',
                    edit_permission: '费用类型编辑',
                    delete_permission: '费用类型删除',
                    add_permission: '费用类型新增',
                })
        },
        {
            path: ROUTE_PATH.organization4,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization5,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    organization_type: '幸福院',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization6,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    organization_type: '福利院',
                })
        },
        // 组织机构
        {
            path: ROUTE_PATH.organization7,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_tree_list',
                    select_permission: '组织机构查询',
                    edit_permission: '组织机构编辑',
                    delete_permission: '组织机构删除',
                    add_permission: '组织机构新增',
                    organization_type: '服务商',
                })
        },
        // 查看组织机构
        {
            path: ROUTE_PATH.organizationLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_organization_list_look',
                    select_permission: '查看组织机构查看',
                })
        },
        {
            path: ROUTE_PATH.changeOrganization,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeOrganization}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeOrganizationServicer,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeOrganizationServicer}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeHappiness}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHappinessViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 平台运营方，编辑组织机构
        {
            path: `${ROUTE_PATH.changeOrganizationJG}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 平台运营方，编辑幸福院
        {
            path: `${ROUTE_PATH.changeOrganizationXYF}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHappinessViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 平台运营方，编辑服务商
        {
            path: `${ROUTE_PATH.changeOrganizationFWS}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeOrganizationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.receivablesList}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ReceivablesViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_receivables_list',
                    select_permission: '应收款账单查看',
                    edit_permission: '应收款账单编辑',
                    add_permission: '应收款账单新增',
                })
        },
        {
            path: `${ROUTE_PATH.actualReceivablesList}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ActualReceivablesViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_actual_receivables_list',
                    select_permission: '实收款账单查看',
                    edit_permission: '实收款账单编辑',
                    add_permission: '实收款账单新增',
                })
        },
        {
            path: `${ROUTE_PATH.differenceDetail}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                DifferenceDetailsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_difference_detail_list',
                    select_permission: '差额明细表查看',
                })
        },
        {
            path: ROUTE_PATH.changeHappiness,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHappinessViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // {
        //     path: ROUTE_PATH.changePlateform,
        //     exact: true,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         ChangePlateformViewControl,
        //         {
        //             permission: PermissionList.RolePermission_Select,
        //         })
        // }, {
        //     path: `${ROUTE_PATH.changePlateform}/${KEY_PARAM}`,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         ChangePlateformViewControl,
        //         {
        //             permission: PermissionList.RolePermission_Select,
        //         })
        // },
        {
            path: ROUTE_PATH.behavioralCompetenceAssessment,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BehavioralCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeBehavioralCompetenceAssessment,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBehavioralCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeBehavioralCompetenceAssessment}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeBehavioralCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.assessmentTemplate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentTemplateViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list',
                    select_permission: '评估模板查看',
                    edit_permission: '评估模板编辑',
                    delete_permission: '评估模板删除',
                    add_permission: '评估模板新增',
                })
        },
        {
            path: ROUTE_PATH.assessmentTemplate,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentTemplateViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list',
                    select_permission: '评估模板查看',
                    edit_permission: '评估模板编辑',
                    delete_permission: '评估模板删除',
                    add_permission: '评估模板新增',
                })
        },
        {
            path: ROUTE_PATH.assessmentTemplateLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentTemplateViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list_look',
                    select_permission: '查看评估模板查看',
                })
        },
        {
            path: `${ROUTE_PATH.changeAssessmentTemplate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAssessmentTemplateViewControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeAssessmentTemplate,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAssessmentTemplateViewControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.assessmentProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    del_url: 'delete_assessment_project',
                    select_permission: '评估项目查询',
                    edit_permission: '评估项目编辑',
                    delete_permission: '评估项目删除',
                    add_permission: '评估项目新增',
                })
        },
        {
            path: `${ROUTE_PATH.assessmentProject}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    del_url: 'delete_assessment_project',
                    select_permission: '评估项目查询',
                    edit_permission: '评估项目编辑',
                    delete_permission: '评估项目删除',
                    add_permission: '评估项目新增',
                })
        },
        {
            path: ROUTE_PATH.assessmentProjectLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AssessmentProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_look',
                    select_permission: '查看评估项目查询',
                })
        },
        {
            path: `${ROUTE_PATH.changeAssessmentProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAssessmentProjectViewControl,
                {
                    assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeAssessmentProject,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAssessmentProjectViewControl,
                {
                    assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.competenceAssessmentList}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CompetenceAssessmentViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_competence_assessment_list_all',
                    select_permission: '评估记录查看',
                    edit_permission: '评估记录编辑',
                    delete_permission: '评估记录删除',
                    add_permission: '评估记录新增',
                })
        },
        {
            path: ROUTE_PATH.competenceAssessmentList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CompetenceAssessmentViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_competence_assessment_list_all',
                    select_permission: '评估记录查看',
                    edit_permission: '评估记录编辑',
                    delete_permission: '评估记录删除',
                    add_permission: '评估记录新增',
                })
        },
        {
            path: ROUTE_PATH.competenceAssessmentListLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CompetenceAssessmentViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_competence_assessment_list_look',
                    select_permission: '查看评估记录查看',
                })
        },
        {
            path: `${ROUTE_PATH.competenceAssessment}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.competenceAssessment,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.addNursingRecord}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.addNursingRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCompetenceAssessmentViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // {
        //     path: `${ROUTE_PATH.addRole}/${KEY_PARAM}`,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         AddRoleViewControl, {
        //             permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
        //             roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
        //             permission: PermissionList.RolePermission_Select,
        //             permission_list: permission_list,
        //             per_org_service: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
        //             permission_services: new AjaxJsonRpcLoadingFactory(IPermissionServices, remote.url, "IPermissionService"),
        //         })
        // },
        {
            path: ROUTE_PATH.userManage,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                UserManageViewControl,
                {
                    userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
                    permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
                    roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
                    permission: PermissionList.UserManage_Select,

                })
        },
        {
            path: ROUTE_PATH.sysUseStatistics,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysUseStaticsticsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_sys_use_staticstics_list',
                    select_permission: '系统使用情况统计查询',
                    add_permission: '系统使用情况统计导出'
                })
        },
        {
            path: ROUTE_PATH.sysStatistics,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SysUseStaticsticsUserViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_sys_use_staticstics_list',
                    select_permission: '系统使用情况统计查询',
                    add_permission: '系统使用情况统计导出'
                })
        },
        {
            path: ROUTE_PATH.userManagePermission,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PersonnelPermissionControl,
                {
                    // user_type: ['服务人员', '长者']
                })
        },
        {
            path: `${ROUTE_PATH.userEditor}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                UserEditorViewControl,
                {
                    userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
                    permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
                    roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
                    permission: PermissionList.UserManage_Select,

                })
        },
        {
            path: ROUTE_PATH.userEditor,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(UserEditorViewControl, {
                userService_Fac: new AjaxJsonRpcLoadingFactory(IUserService, remote.url, "IUserService"),
                permissionService_Fac: new AjaxJsonRpcLoadingFactory(IPermissionService, remote.url, "IPermissionService"),
                roleService_Fac: new AjaxJsonRpcLoadingFactory(IRoleService, remote.url, "IPermissionService"),
                permission: PermissionList.UserManage_Select,

            })
        },
        {
            path: ROUTE_PATH.login,
            mainFormID: headMainID,
            targetObject: createObject(LoginViewControl, {
                home_path: ROUTE_PATH.home,
                call_center_path: ROUTE_PATH.callCenter,
                bind_phone_path: ROUTE_PATH.bindPhone,
                modify_password_path: ROUTE_PATH.modifyfirstPsw,
                retrieve_password: ROUTE_PATH.retrievePassword,
                iPersonnelOrganizationalService: AppServiceUtility.ipersonnel_service,
                login_service: AppServiceUtility.login_service,
                login_type: 'web',
                modify_psw_and_phone_path: ROUTE_PATH.modifyPswAndPhone,
            })
        }, {
            path: ROUTE_PATH.register,
            mainFormID: headMainID,
            targetObject: createObject(RegisterControl, {
                login_service: AppServiceUtility.login_service,
                sms_service: AppServiceUtility.sms_manage_service,
                back_url: ROUTE_PATH.login
            })
        }, {
            path: ROUTE_PATH.bindPhone,
            mainFormID: headMainID,
            targetType: "secure",
            targetObject: createObject(BindPhoneControl, {
                link_url: ROUTE_PATH.login,
                login_service: AppServiceUtility.login_service,
                sms_service: AppServiceUtility.sms_manage_service,
            })
        }, {
            path: ROUTE_PATH.modifyfirstPsw,
            mainFormID: headMainID,
            targetType: "secure",
            targetObject: createObject(FirstModifyControl, {
                link_url: ROUTE_PATH.login,
                login_service: AppServiceUtility.login_service,
            })
        }, {
            path: ROUTE_PATH.modifyPswAndPhone,
            mainFormID: headMainID,
            targetType: "secure",
            targetObject: createObject(ModifyPasswordPhoneControl, {
                link_url: ROUTE_PATH.login,
                login_service: AppServiceUtility.login_service,
                sms_service: AppServiceUtility.sms_manage_service
            })
        }, {
            path: ROUTE_PATH.retrievePassword,
            mainFormID: headMainID,
            targetObject: createObject(RetrievePasswordControl, {
                sms_service: AppServiceUtility.sms_manage_service,
                login_service: AppServiceUtility.login_service,
                login_url: ROUTE_PATH.login,
            })
        },
        // 安全设置
        {
            path: ROUTE_PATH.securitySettings,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(SecuritySettingsViewControl, {
                userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
                modify_password: ROUTE_PATH.modifyLoginPassword,
                modify_mobile: ROUTE_PATH.modifyMobile,
                modify_email: ROUTE_PATH.modifyEmail,
                modify_personnel: ROUTE_PATH.changePersonnel,
                // permission_class: AppServiceUtility.login_service,
                // get_permission_name: 'get_function_list',
                // request_url: 'get_user_list',
                // edit_permission: '用户管理编辑',
            })
        }, {
            path: ROUTE_PATH.modifyLoginPassword,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ModifyLoginPasswordViewControl, {
                userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
                login_service: AppServiceUtility.login_service,
                sms_service: AppServiceUtility.sms_manage_service,
            })
        }, {
            path: ROUTE_PATH.modifyMobile,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ModifyMobileViewControl, {
                userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService"),
                login_service: AppServiceUtility.login_service,
                sms_service: AppServiceUtility.sms_manage_service,
            })
        }, {
            path: ROUTE_PATH.modifyEmail,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ModifyEmailViewControl, { userService_Fac: new AjaxJsonRpcLoadingFactory(IPersonOrgManageService, remote.url, "IPersonOrgManageService") })
        },
        {
            path: ROUTE_PATH.home,
            mainFormID: layoutMain,
            targetObject: createObject(HomeViewControl)
        },
        // 服务项目
        {
            path: ROUTE_PATH.serviceProject,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_services_project_list_all',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 查看服务项目
        {
            path: ROUTE_PATH.serviceProjectLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceProjectViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_services_project_list_look',
                    select_permission: '查看服务项目查看',
                })
        },
        {
            path: ROUTE_PATH.changeServiceProject,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceProjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        }, {
            path: `${ROUTE_PATH.changeServiceProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceProjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        /** ======== 住宿区域类型路由 start ======== */
        {
            path: ROUTE_PATH.hotelZoneType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                HotelZoneTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_list_all',
                    select_permission: '区域类型查看',
                    edit_permission: '区域类型编辑',
                    delete_permission: '区域类型删除',
                    add_permission: '区域类型新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeHotelZoneType}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeHotelZoneTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeHotelZoneType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeHotelZoneTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        /** ======== 住宿区域类型路由 end ======== */
        /** 服务项目类别路由 */
        // 服务项目类别
        {
            path: ROUTE_PATH.servicesItemCategory,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServicesItemCategoryViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_type_list',
                    select_permission: '服务项目类别查看',
                    edit_permission: '服务项目类别编辑',
                    delete_permission: '服务项目类别删除',
                    add_permission: '服务项目类别新增',
                })
        },
        // 查看服务项目类别
        {
            path: ROUTE_PATH.servicesItemCategoryLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServicesItemCategoryViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_services_item_category_list_look',
                    select_permission: '服务项目类别查看',
                })
        },
        {
            path: ROUTE_PATH.changeServicesItemCategory,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServicesItemCategoryViewControl)
        }, {
            path: `${ROUTE_PATH.changeServicesItemCategory}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServicesItemCategoryViewControl)
        },
        /** ======== 住宿区域路由 start ======== */
        {
            path: ROUTE_PATH.hydropower,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                HydropowerViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_hydropower_list_all',
                    select_permission: '水电抄表查看',
                    edit_permission: '水电抄表编辑',
                    delete_permission: '水电抄表删除',
                    add_permission: '水电抄表新增',
                })
        },
        {
            path: ROUTE_PATH.hydropower,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                HydropowerViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_hydropower_list_look',
                    select_permission: '查看水电抄表查询',
                    edit_permission: '水电抄表编辑',
                    delete_permission: '水电抄表删除',
                    add_permission: '水电抄表新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeHydropower}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHydropowerViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: `${ROUTE_PATH.changeHydropower}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHydropowerViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: ROUTE_PATH.hydropowerJj,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                HydropowerJjViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_jiujiang_hydropower_list_all',
                    select_permission: '电费抄表查询',
                    edit_permission: '电费抄表编辑',
                    delete_permission: '电费抄表删除',
                    add_permission: '电费抄表新增',
                })
        },
        {
            path: ROUTE_PATH.hydropowerJjLook,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                HydropowerJjViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_jiujiang_hydropower_list_look',
                    select_permission: '九江水电抄表查询'
                })
        },
        {
            path: `${ROUTE_PATH.changeHydropowerJj}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHydropowerJjViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: `${ROUTE_PATH.changeHydropowerJj}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHydropowerJjViewControl, {
                permission: PermissionList.RolePermission_Select
            })
        },
        {
            path: `${ROUTE_PATH.costAccountingLook}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                costAccountingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_cost_account_list_all',
                    select_permission: '住宿核算查看',
                })
        },
        {
            path: `${ROUTE_PATH.costAccounting}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                costAccountingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_cost_account_list_manage',
                    select_permission: '住宿核算管理查看',
                    edit_permission: '住宿核算管理编辑',
                    add_permission: '住宿核算管理新增',
                })
        },
        {
            path: ROUTE_PATH.hotelZone,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                HotelZoneViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_list_all',
                    select_permission: '区域管理查看',
                    edit_permission: '区域管理编辑',
                    delete_permission: '区域管理删除',
                    add_permission: '区域管理新增',
                })
        },
        {
            path: ROUTE_PATH.hotelZoneLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                HotelZoneViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_list_look',
                    select_permission: '查看区域情况查看',
                })
        },
        {
            path: `${ROUTE_PATH.changeHotelZone}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeHotelZoneControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeHotelZone}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeHotelZoneControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        // 服务人员申请
        {
            path: ROUTE_PATH.servicePersonalApply,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServicePersonalApplyViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: '',
                add_permission: '服务人员申请新增',
            })
        },
        /** ======== 住宿区域路由 end ======== */

        /** ======== 需求类型 start ======== */
        {
            path: ROUTE_PATH.requirementType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RequirementTypeViewControl)
        },
        {
            path: `${ROUTE_PATH.changeRequirementType}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeRequirementType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        /** ======== 需求类型 end ======== */

        /** ======== 需求项目 start ======== */
        {
            path: ROUTE_PATH.requirementProject,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RequirementProjectViewControl)
        },
        {
            path: `${ROUTE_PATH.changeRequirementProject}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementProjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeRequirementProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementProjectViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        /** ======== 需求项目 end ======== */

        /** ======== 需求项目选项 start ======== */
        {
            path: ROUTE_PATH.requirementOption,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RequirementOptionViewControl)
        },
        {
            path: `${ROUTE_PATH.changeRequirementOption}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementOptionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeRequirementOption}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeRequirementOptionViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        /** ======== 需求项目选项 end ======== */
        // 尝试重构的房态图
        {
            path: ROUTE_PATH.roomStateDiagram,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoomStateDiagramViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '房态图查看',
                }
            )
        },
        /** ======== 房态图 start ======== */
        {
            path: ROUTE_PATH.roomStatus,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoomStatusViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    select_permission: '房态图查看',
                    edit_permission: '房态图编辑',
                }
            )
        },
        {
            path: ROUTE_PATH.roomStatusexcel,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoomStatusViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    select_permission: '房态图查看',
                    edit_permission: '房态图编辑',
                }
            )
        },
        {
            path: ROUTE_PATH.roomStatusexceltow,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoomStatusViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    select_permission: '房态图查看',
                    edit_permission: '房态图编辑',
                }
            )
        },
        /** ======== 房态图 end ======== */

        /** 服务项目类别路由 */
        // 服务适用范围
        {
            path: ROUTE_PATH.serviceScope,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceScopeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_scope_list_all',
                    select_permission: '服务适用范围查看',
                    edit_permission: '服务适用范围编辑',
                    delete_permission: '服务适用范围删除',
                    add_permission: '服务适用范围新增',
                }
            )
        },
        // 查看服务适用范围
        {
            path: ROUTE_PATH.serviceScopeLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceScopeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_scope_list_look',
                    select_permission: '查看服务适用范围查看',
                }
            )
        },
        {
            path: ROUTE_PATH.changeServiceScope,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServicesScopeViewControl)
        }, {
            path: `${ROUTE_PATH.changeServiceScope}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServicesScopeViewControl)
        },
        {
            path: ROUTE_PATH.taskList,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_all_task_list_all',
                    select_permission: '任务列表查询',
                    edit_permission: '任务列表编辑',
                    add_permission: '任务列表新增',
                }
            )
        },
        {
            path: ROUTE_PATH.taskListLook,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_all_task_list_look',
                    select_permission: '任务列表查询',
                }
            )
        },
        {
            path: ROUTE_PATH.taskServiceOrder,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskServiceOrderViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_record_list_all',
                    select_permission: '分派任务列表查询',
                    edit_permission: '分派任务列表编辑',
                    delete_permission: '分派任务列表删除',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeTaskServiceOrder}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTaskServiceOrderViewControl,
                {
                }
            )
        },
        {
            path: ROUTE_PATH.taskToBeSend,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskToBeSendViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_to_be_send_task_list_look_edit',
                    select_permission: '分派任务列表查询',
                    edit_permission: '分派任务列表编辑',
                }
            )
        },
        {
            path: ROUTE_PATH.taskToBeProcess,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskToBeProcessViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_to_be_processed_task_list_look',
                    select_permission: '审核任务列表查询',
                    add_permission: '审核任务列表审核',
                }
            )
        },
        {
            path: ROUTE_PATH.taskTypeList,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskTypeListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_task_type_list_all',
                    select_permission: '任务类型查询',
                    edit_permission: '任务类型编辑',
                    add_permission: '任务类型新增',
                }
            )
        },
        {
            path: ROUTE_PATH.taskTypeListLook,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                TaskTypeListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_task_type_list_look',
                    select_permission: '查看任务类型查询',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeTaskType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTaskTypeViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeTaskType,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTaskTypeViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeTask}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTaskViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeTask,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeTaskViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.messageTypeList,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                MessageTypeListViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeMessageType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMessageTypeViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeMessageType,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMessageTypeViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.messageList,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                MessageListViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeMessage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMessageViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeMessage,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMessageViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.leaveReason,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                LeaveReasonViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_leave_reason_list_all',
                    select_permission: '离开原因查询',
                    edit_permission: '离开原因编辑',
                    delete_permission: '离开原因删除',
                    add_permission: '离开原因新增',
                }
            )
        }, {
            path: ROUTE_PATH.changeLeaveReason,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeLeaveReasonViewControl,
                {
                    // permission: PermissionList.UserManage_Select,

                })
        },
        {
            path: `${ROUTE_PATH.changeLeaveReason}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeLeaveReasonViewControl,
                {
                    // permission: PermissionList.UserManage_Select,

                })
        },
        {
            path: ROUTE_PATH.serviceWoker,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ServiceWokerViewControl,
                {
                    permission: PermissionList.RolePermission_Select,

                })
        }, {

            path: ROUTE_PATH.ChangeServiceWoker,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                ChangeServiceWokerViewControl, { permission: PermissionList.RolePermission_Select, })
        },

        /** 服务选项路由 */
        {
            path: ROUTE_PATH.serviceOption,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOptionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_servicre_option_list_all',
                    select_permission: '服务选项查看',
                    edit_permission: '服务选项编辑',
                    delete_permission: '服务选项删除',
                    add_permission: '服务选项新增',
                })
        },
        // 查看服务选项
        {
            path: ROUTE_PATH.serviceOptionLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceOptionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_servicre_option_list_look',
                    select_permission: '查看服务选项查看',
                })
        },
        {
            path: ROUTE_PATH.changeServiceOption,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceOptionViewControl, { permission: PermissionList.RolePermission_Select, })
        }, {
            path: `${ROUTE_PATH.ChangeServiceWoker}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceWokerViewControl, { permission: PermissionList.RolePermission_Select, })
        },
        {
            path: `${ROUTE_PATH.changeServiceOption}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceOptionViewControl)
        },
        /** =================================接待管理部分=============================== */
        {
            path: ROUTE_PATH.checkInNew,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInNewViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: ROUTE_PATH.checkIn,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_check_in_list_all',
                    select_permission: '长者入住查询',
                    edit_permission: '长者入住编辑',
                    delete_permission: '长者入住删除',
                    add_permission: '长者入住新增',
                })
        },
        {
            path: ROUTE_PATH.checkInLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_check_in_list_look',
                    select_permission: '查看长者入住查询',
                })
        },
        {
            path: `${ROUTE_PATH.checkInDetail}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                CheckInDetailViewControl,
                {
                    edit_permission: '入住办理编辑',
                    add_permission: '入住办理新增',
                })
        },
        /** 九江入住办理 */
        {
            path: ROUTE_PATH.checkInNewJj,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInNewJjViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: `${ROUTE_PATH.checkInNewJj}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInNewJjViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: ROUTE_PATH.checkInJj,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInListViewJjControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'check_in_alone_all',
                    select_permission: '入住办理查询',
                    edit_permission: '入住办理编辑',
                    delete_permission: '入住办理删除',
                    add_permission: '入住办理新增',
                })
        },
        {
            path: ROUTE_PATH.checkInJjLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                CheckInListViewJjControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_check_in_list_look',
                    select_permission: '入住办理查看',
                })
        },
        // 服务记录 
        {
            path: ROUTE_PATH.serviceRecord,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ServiceRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_record_list_all',
                    select_permission: '服务记录查看',
                    edit_permission: '服务记录编辑',
                    // delete_permission: '服务记录删除',
                    add_permission: '服务记录重置',
                })
        },
        // 居家服务商服务记录 
        {
            path: ROUTE_PATH.serviceRecordProvider,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ServiceRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_record_list_all',
                    select_permission: '服务记录查看',
                    edit_permission: '服务记录编辑',
                    // delete_permission: '服务记录删除',
                    add_permission: '服务记录新增',
                    provider: true
                })
        },
        // 查看服务记录
        {
            path: ROUTE_PATH.serviceRecordLook,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ServiceRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_record_list_look',
                    select_permission: '服务记录查看',
                    edit_permission: '服务记录编辑',
                    delete_permission: '服务记录删除',
                    add_permission: '服务记录新增',
                })
        },
        {
            path: ROUTE_PATH.serviceLedger,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(ServiceLedgerViewControl)
        }, {
            path: ROUTE_PATH.changeServiceRecord,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceRecordViewControl, { permission: PermissionList.RolePermission_Select, })
        }, {
            path: `${ROUTE_PATH.changeServiceRecord}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceRecordViewControl)
        },
        /** =======费用调整========= */
        {
            path: ROUTE_PATH.feeList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FeeListControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_check_in_list_room_all',
                    select_permission: '房间调整查看',
                    edit_permission: '房间调整编辑',
                })
        },
        {
            path: `${ROUTE_PATH.changeFee}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeFeeViewControl)
        },
        /** =====调房===== */
        {
            path: ROUTE_PATH.roomList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RoomListControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_check_in_list_room_all',
                    select_permission: '房间调整查看',
                    // edit_permission: '房间调整编辑',
                    add_permission: '房间调整新增',
                })
        },
        {
            path: `${ROUTE_PATH.roomChange}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RoomChangeViewControl, {})
        },
        {
            path: `${ROUTE_PATH.roomChange}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RoomChangeViewControl, {})
        },
        /** ===========需求模板============= */
        {
            path: ROUTE_PATH.requirementTemplate,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RequirementTemplateViewControl)
        }, {
            path: ROUTE_PATH.changeRequirementTemplate,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRequirementTemplateViewControl,
                {
                    requirementTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IRequirementTemplateService, remote.url, "IRequirementTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeRequirementTemplate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRequirementTemplateViewControl,
                {
                    requirementTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IRequirementTemplateService, remote.url, "IRequirementTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        }, {
            path: ROUTE_PATH.requirementRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RequirementRecordViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeRequirementRecord,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRequirementRecordViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeRequirementRecord}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeRequirementRecordViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        /** ===========医院管理============= */
        {
            path: ROUTE_PATH.hospitalArrears,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(hospitalArrearsViewControl)
        },
        {
            path: ROUTE_PATH.hospitalArrearsEdit,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(ChangeHospitalViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        {
            path: `${ROUTE_PATH.hospitalArrearsEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHospitalViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },
        /** ============================账务管理============================ */
        {
            path: `${ROUTE_PATH.changeCollectorCharge}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCollectorChargeViewControl)
        }, {
            path: `${ROUTE_PATH.changeCollectorPayment}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCollectorPaymentViewControl)
        }, {
            path: `${ROUTE_PATH.financialChargeRecord}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialChargeRecordViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charge_record_list_all',
                select_permission: '收费记录管理查询',
            })
        },
        {
            path: `${ROUTE_PATH.financialChargeRecordLook}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialChargeRecordViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charge_record_list_look',
                select_permission: '查看收费记录查询',
            })
        },
        {
            path: `${ROUTE_PATH.financialBankReconciliation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BillRecodeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bank_unreconciliation_list_look_export',
                    select_permission: '打印/导出银行扣款单查询'
                }
            )
        },
        {
            path: `${ROUTE_PATH.financialBankReconciliationManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BillRecodeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bank_unreconciliation_list_manage',
                    select_permission: '打印/导出银行扣款单管理查询',
                    add_permission: '打印/导出银行扣款单管理导出',
                    export_url: 'exprot_excel_manage'
                }
            )
        },
        // 非银行扣款单
        {
            path: `${ROUTE_PATH.unfinancialBankReconciliation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                UnFinancialReconciliationRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_unbank_reconciliation_list_manage',
                    select_permission: '非银行扣款单管理查询',
                    edit_permission: '非银行扣款单管理编辑',
                    add_permission: '非银行扣款单管理新增'
                }
            )
        },
        {
            path: `${ROUTE_PATH.unfinancialBankReconciliationLook}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                UnFinancialReconciliationRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_unbank_reconciliation_list_Look',
                    select_permission: '查看非银行扣款单查询',
                }
            )
        },
        {
            path: `${ROUTE_PATH.financialReconciliationRecord}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialReconciliationRecordViewControl, {
                request_url: 'get_bank_reconciliation_list_all'
            })
        },
        {
            path: `${ROUTE_PATH.financialReconciliationRecordLook}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialReconciliationRecordViewControl, {
                request_url: 'get_bank_reconciliation_list_look'
            })
        },

        {
            path: ROUTE_PATH.announcementIssue,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AnnouncementIssueViewControl,
                // {
                //     permission: PermissionList.RolePermission_Select,
                // }
            )
        },
        {
            path: `${ROUTE_PATH.announcementIssue}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AnnouncementIssueViewControl,
                // {
                //     permission: PermissionList.RolePermission_Select,
                // }
            )
        },
        {
            path: ROUTE_PATH.announcementList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AnnouncementListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_announcement_list_all',
                    select_permission: '公告管理查询',
                    edit_permission: '公告管理编辑',
                    delete_permission: '公告管理删除',
                    add_permission: '公告管理新增',
                }
            )
        },
        {
            path: ROUTE_PATH.announcementListLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AnnouncementListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_announcement_list_look',
                    select_permission: '查看公告列表查询',
                }
            )
        },
        {
            path: ROUTE_PATH.activityTypeManage,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_type_list_all',
                    select_permission: '活动类型查看',
                    edit_permission: '活动类型编辑',
                    delete_permission: '活动类型删除',
                    add_permission: '活动类型新增',
                })
        },
        {
            path: ROUTE_PATH.activityTypeManageLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_type_list_look',
                    select_permission: '查看活动类型查看',
                })
        },
        {
            path: `${ROUTE_PATH.changeActivityTypeManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeActivityTypeManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.activityManage,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityPublishViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_pure_list_all',
                    select_permission: '活动列表查看',
                    edit_permission: '活动列表编辑',
                    delete_permission: '活动列表删除',
                    add_permission: '活动列表新增',
                })
        },
        {
            path: ROUTE_PATH.activityManageSh,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityPublishViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_pure_list_all',
                    select_permission: '活动审核列表查看',
                    edit_permission: '活动审核列表编辑',
                    list_type: 'sh',
                })
        },
        {
            path: ROUTE_PATH.activityManageTs,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityPublishViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_list_all',
                    select_permission: '活动推送列表查看',
                    list_type: 'ts',
                })
        },
        {
            path: ROUTE_PATH.activityManageLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityPublishViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_list_look',
                    select_permission: '查看活动列表查看',
                })
        },
        {
            path: `${ROUTE_PATH.changeActivityManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityPublishViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    select_param: { type: 'change' },
                })
        },
        {
            path: `${ROUTE_PATH.viewActivityManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityPublishViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    select_param: { type: 'view' },
                })
        },
        {
            path: `${ROUTE_PATH.changeActivitySh}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityShViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.newsList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_pure_list_all',
                    select_permission: '新闻管理查询',
                    edit_permission: '新闻管理编辑',
                    delete_permission: '新闻管理删除',
                    add_permission: '新闻管理新增',
                }
            )
        },
        {
            path: ROUTE_PATH.newsListWelfare,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_pure_list_all',
                    select_permission: '新闻管理查询',
                    edit_permission: '新闻管理编辑',
                    delete_permission: '新闻管理删除',
                    add_permission: '新闻管理新增',
                    org_type: '福利院',
                }
            )
        },
        {
            path: ROUTE_PATH.newsListHappiness,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_pure_list_all',
                    select_permission: '新闻管理查询',
                    edit_permission: '新闻管理编辑',
                    delete_permission: '新闻管理删除',
                    add_permission: '新闻管理新增',
                    org_type: '幸福院',
                }
            )
        },
        {
            path: ROUTE_PATH.newsListProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_pure_list_all',
                    select_permission: '新闻管理查询',
                    edit_permission: '新闻管理编辑',
                    delete_permission: '新闻管理删除',
                    add_permission: '新闻管理新增',
                    org_type: '服务商',
                }
            )
        },
        {
            path: ROUTE_PATH.newsListSh,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_pure_list_all',
                    select_permission: '新闻审核查询',
                    edit_permission: '新闻审核编辑',
                    // delete_permission: '新闻审核删除',
                    // add_permission: '新闻审核新增',
                    list_type: 'sh',
                }
            )
        },
        {
            path: ROUTE_PATH.newsListLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NewsListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_news_list_look',
                    select_permission: '查看新闻列表查询',
                }
            )
        },
        {
            path: ROUTE_PATH.changeActivityManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityPublishViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.changeNews,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNewsViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeNewsSh}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNewsShViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: `${ROUTE_PATH.changeNews}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeNewsViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.commentList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_list_all',
                    select_permission: '评论管理查询',
                    edit_permission: '评论管理编辑',
                    delete_permission: '评论管理删除',
                    add_permission: '评论管理新增',
                    list_type: '',
                })
        }, {
            path: ROUTE_PATH.commentListLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_list_look',
                    select_permission: '查看评论管理查询',
                    list_type: '',
                })
        }, {
            path: ROUTE_PATH.commentAudit,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_audit_list_all',
                    select_permission: '评论审核查询',
                    edit_permission: '评论审核编辑',
                    delete_permission: '评论审核删除',
                    add_permission: '评论审核新增',
                    list_type: 'sh',
                })
        },
        {
            path: ROUTE_PATH.commentAuditWelfare,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_audit_list_all',
                    select_permission: '评论审核查询',
                    edit_permission: '评论审核编辑',
                    delete_permission: '评论审核删除',
                    add_permission: '评论审核新增',
                    list_type: 'sh',
                    org_type: '福利院'
                })
        },
        {
            path: ROUTE_PATH.commentAuditHappiness,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_audit_list_all',
                    select_permission: '评论审核查询',
                    edit_permission: '评论审核编辑',
                    delete_permission: '评论审核删除',
                    add_permission: '评论审核新增',
                    list_type: 'sh',
                    org_type: '幸福院'

                })
        },
        {
            path: ROUTE_PATH.commentAuditProvider,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_audit_list_all',
                    select_permission: '评论审核查询',
                    edit_permission: '评论审核编辑',
                    delete_permission: '评论审核删除',
                    add_permission: '评论审核新增',
                    list_type: 'sh',
                    org_type: '服务商'

                })
        },
        {
            path: ROUTE_PATH.commentAuditLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentAuditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_audit_list_look',
                    select_permission: '查看评论审核查询',
                    edit_permission: '评论审核编辑',
                })
        },
        {
            path: ROUTE_PATH.changeCommentAudit,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCommentAuditControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeCommentAudit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCommentAuditControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        // PC端活动报名
        {
            path: `${ROUTE_PATH.chagneActivityParticipate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityParticipateViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.chagneActivityParticipate}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityParticipateViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.activityParticipate,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityParticipateViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_participate_pure_list_all',
                    select_permission: '报名列表查询',
                    add_permission: '报名列表新增',
                    delete_permission: '报名列表删除',
                })
        },
        {
            path: ROUTE_PATH.activityParticipateLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityParticipateViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_participate_list_look',
                    select_permission: '查看报名列表查看',
                })
        },
        {
            path: ROUTE_PATH.activitySignIn,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivitySignInViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_sign_in_list_all',
                    select_permission: '签到列表查看',
                    delete_permission: '签到列表删除',
                })
        },
        {
            path: ROUTE_PATH.activitySignInLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivitySignInViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_sign_in_list_look',
                    select_permission: '查看签到列表查看',
                })
        },
        // 服务人员登记
        {
            path: ROUTE_PATH.servicePersonalManage,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServicePersonalViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_personal_list',
                select_permission: '服务人员登记查看',
                edit_permission: '服务人员登记编辑',
                delete_permission: '服务人员登记删除',
                add_permission: '服务人员登记录新增',
            })
        },
        {
            path: `${ROUTE_PATH.changeServicePersonal}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServicePersonalViewControl, {})
        },
        {
            path: ROUTE_PATH.changeServicePersonal,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeServicePersonalViewControl, {})
        },
        // 服务商结算
        {
            path: ROUTE_PATH.serviceProviderSettlement,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServiceProviderSettlementViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_record_list_all',
                select_permission: '服务商结算查看',
                edit_permission: '服务商结算编辑',
                delete_permission: '服务商结算删除',
                add_permission: '服务商结算新增',
            })
        },
        // 服务商结算管理
        {
            path: ROUTE_PATH.serviceProviderSettlementLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServiceProviderSettlementViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_record_list_look',
                select_permission: '服务商结算管理查看',
                edit_permission: '服务商结算管理编辑',
                delete_permission: '服务商结算管理删除',
                add_permission: '服务商结算管理新增',
            })
        },
        {
            path: ROUTE_PATH.serviceProviderSettlementLook1,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServiceProviderSettlementViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_record_list_look',
                select_permission: '服务商结算管理查看',
                edit_permission: '服务商结算管理编辑',
                delete_permission: '服务商结算管理删除',
                add_permission: '服务商结算管理新增',
            })
        },
        // 服务人员结算
        {
            path: ROUTE_PATH.servicePersonalSettlement,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServicePersonalSettlementViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_record_list_all',
                select_permission: '服务人员结算查看',
                edit_permission: '服务人员结算编辑',
                delete_permission: '服务人员结算删除',
                add_permission: '服务人员结算新增',
            })
        },
        // 服务人员结算管理
        {
            path: ROUTE_PATH.servicePersonalSettlementLook,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServicePersonalSettlementViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_record_list_look',
                select_permission: '服务人员结算管理查看',
                edit_permission: '服务人员结算管理编辑',
                delete_permission: '服务人员结算管理删除',
                add_permission: '服务人员结算管理新增',
            })
        },
        {
            path: ROUTE_PATH.comment,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CommentViewControl, {})
        },
        {
            path: `${ROUTE_PATH.comment}/${KEY_PARAM}`,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CommentViewControl, { permission: PermissionList.RolePermission_Select, })
        },
        {
            path: ROUTE_PATH.articleType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ArticleTypeViewControl, {
                permission: PermissionList.RolePermission_Select,
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_article_type_list_all',
                select_permission: '文章类型查询',
                edit_permission: '文章类型编辑',
                delete_permission: '文章类型删除',
                add_permission: '文章类型新增',
            }
            )
        },
        {
            path: ROUTE_PATH.articleTypeLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ArticleTypeViewControl, {
                permission: PermissionList.RolePermission_Select,
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_article_type_list_look',
                select_permission: '查看文章类型查询',
            })
        },
        {
            path: ROUTE_PATH.changeArticleType,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeArticleTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeArticleType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeArticleTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },

        /* 补贴管理*/
        // 补贴管理模块路由
        ...subsidyRouter,
        /* 调研管理*/
        ...researchRouter,
        {
            path: ROUTE_PATH.leaveInsert,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(LeaveInsertViewControl, {})
        },
        {
            path: ROUTE_PATH.leaveRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                LeaveRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_leave_list_all',
                    select_permission: '住宿记录查看',
                    edit_permission: '住宿记录编辑',
                    delete_permission: '住宿记录删除',
                    add_permission: '住宿记录新增',
                })
        },
        {
            path: ROUTE_PATH.leaveRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                LeaveRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_leave_list_look',
                    select_permission: '查看住宿记录查看',
                })
        },
        {
            path: `${ROUTE_PATH.leaveEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(LeaveEditViewControl, {})
        },
        {
            path: ROUTE_PATH.leaveEdit,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(LeaveEditViewControl, {})
        },
        /* 评论类型 */
        {
            path: ROUTE_PATH.commentType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_type_list_all',
                    select_permission: '评论类型查询',
                    edit_permission: '评论类型编辑',
                    delete_permission: '评论类型删除',
                    add_permission: '评论类型新增',
                })
        },
        {
            path: ROUTE_PATH.commentTypeLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CommentTypeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_comment_type_list_look',
                    select_permission: '查看评论类型查询',
                })
        },
        {
            path: ROUTE_PATH.changeCommentType,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCommentTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeCommentType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCommentTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        /* 文章审核*/
        {
            path: ROUTE_PATH.articleAudit,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ArticleAuditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_article_audit_list_all',
                    select_permission: '文章审核查询',
                    edit_permission: '文章审核编辑',
                    delete_permission: '文章审核删除',
                    add_permission: '文章审核新增',
                })
        },
        {
            path: ROUTE_PATH.articleAuditLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ArticleAuditViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_article_audit_list_look',
                    select_permission: '查看文章审核查询',
                })
        },
        {
            path: ROUTE_PATH.changeArticleAudit,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeArticleAuditControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeArticleAudit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeArticleAuditControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        // 服务套餐列表
        {
            path: ROUTE_PATH.serviceProjectindex,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServiceProjectControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_service_product_package_list',
                select_permission: '服务人员结算查看',
            })
        },
        {
            path: `${ROUTE_PATH.ServiceProjectDetail}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServiceProjectDetailControl, {})
        },
        {
            path: `${ROUTE_PATH.ServiceProjectBuy}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServerProjectBuyControl, {})
        },
        {
            path: `${ROUTE_PATH.ServiceProjectBuy}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ServerProjectBuyControl, {})
        },
        // 慈善管理
        // 冠名基金
        {
            path: ROUTE_PATH.charitableTitleFund,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableTitleFundViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_title_fund_list_all',
                select_permission: '冠名基金查询',
                edit_permission: '冠名基金编辑',
                add_permission: '冠名基金新增',
                delete_permission: '冠名基金删除',
            })
        },
        {
            path: `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}/${CODE_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableTitleFund}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableTitleFundViewControl, {})
        },
        {
            path: ROUTE_PATH.viewCharitableTitleFund,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ViewCharitableTitleFundViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_title_fund_list_all',
                select_permission: '冠名基金查询',
                edit_permission: '冠名基金编辑',
                add_permission: '冠名基金新增',
            })
        },
        // 慈善项目管理
        {
            path: ROUTE_PATH.charitableProject,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableProjectViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_project_list_all',
                select_permission: '慈善项目查询',
                edit_permission: '慈善项目编辑',
                add_permission: '慈善项目新增',
                delete_permission: '慈善项目删除',
            })
        },
        {
            path: ROUTE_PATH.charitableProjectList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableProjectListViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_project_list_all',
                select_permission: '慈善项目查询',
            })
        },
        {
            path: `${ROUTE_PATH.viewCharitableProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ViewCharitableProjectViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableProject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableProjectViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableProject}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableProjectViewControl, {})
        },
        // 慈善捐赠
        {
            path: ROUTE_PATH.charitableDonate,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableDonateViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_donate_list_all',
                select_permission: '慈善捐赠查询',
                edit_permission: '慈善捐赠编辑',
                add_permission: '慈善捐赠新增',
                delete_permission: '慈善捐赠删除',
            })
        },
        {
            path: `${ROUTE_PATH.viewCharitableDonate}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ViewCharitableDonateViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableDonate}/${KEY_PARAM}/${CODE_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableDonateViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeCharitableDonate}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableDonateViewControl, {})
        },
        // 慈善拨款
        {
            path: ROUTE_PATH.charitableAppropriation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableAppropriationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_recipient_list_all',
                select_permission: '募捐申请查询',
                edit_permission: '募捐申请编辑',
                add_permission: '募捐申请新增',
                delete_permission: '募捐申请删除',
            })
        },
        {
            path: `${ROUTE_PATH.changeCharitableAppropriation}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeCharitableAppropriationViewControl, {})
        },
        // 慈善信息
        {
            path: ROUTE_PATH.charitableInformation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableInformationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_information_list_all',
                select_permission: '慈善信息查询',
                edit_permission: '慈善信息编辑',
                add_permission: '慈善信息新增',
                delete_permission: '慈善信息删除',
            })
        },
        // 慈善会资料
        {
            path: ROUTE_PATH.charitableSociety,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(CharitableSocietyViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_society_list_all',
                select_permission: '慈善会资料查询',
                edit_permission: '慈善会资料编辑',
                add_permission: '慈善会资料新增',
                delete_permission: '慈善会资料删除',
            })
        },
        // 募捐查看页面
        {
            path: ROUTE_PATH.viewRecipientApplyList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ViewRecipientApplyViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_recipient_apply_list_all',
                select_permission: '募捐申请查询',
                edit_permission: '募捐申请编辑',
                add_permission: '募捐申请新增',
                delete_permission: '募捐申请删除',
            })
        },
        // 个人募捐申请
        {
            path: ROUTE_PATH.recipientApplyList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RecipientApplyListViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_recipient_apply_list_all',
                select_permission: '募捐申请查询',
                edit_permission: '募捐申请编辑',
                add_permission: '募捐申请新增',
                delete_permission: '募捐申请删除',
            })
        },
        {
            path: `${ROUTE_PATH.recipientApply}/${KEY_PARAM}/${CODE_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RecipientApplyViewControl, {})
        },
        {
            path: `${ROUTE_PATH.recipientApply}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RecipientApplyViewControl, {})
        },
        {
            path: `${ROUTE_PATH.recipientApply}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(RecipientApplyViewControl, {})
        },
        // 机构申请
        {
            path: ROUTE_PATH.organizationApplyList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(OrganizationApplyListViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_charitable_organization_apply_list_all',
                select_permission: '机构募捐申请查询',
                edit_permission: '机构募捐申请编辑',
                add_permission: '机构募捐申请新增',
                delete_permission: '机构募捐申请删除',
            })
        },
        {
            path: `${ROUTE_PATH.organizationApply}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(OrganizationApplyViewControl, {})
        },
        {
            path: `${ROUTE_PATH.organizationApply}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(OrganizationApplyViewControl, {})
        },
        // 志愿者服务类别管理
        {
            path: ROUTE_PATH.volunteerServiceCategory,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(VolunteerServiceCategoryViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_volunteer_service_category_list_all',
                select_permission: '志愿者服务类别查询',
                edit_permission: '志愿者服务类别编辑',
                add_permission: '志愿者服务类别新增',
                delete_permission: '志愿者服务类别删除',
            })
        },
        {
            path: `${ROUTE_PATH.changeVolunteerServiceCategory}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeVolunteerServiceCategoryViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeVolunteerServiceCategory}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeVolunteerServiceCategoryViewControl, {})
        },
        // 志愿者申请
        {
            path: ROUTE_PATH.volunteerApplyList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(VolunteerApplyListViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_volunteer_apply_list_all',
                select_permission: '志愿者申请查询',
                edit_permission: '志愿者申请编辑',
                add_permission: '志愿者申请新增',
                delete_permission: '志愿者申请删除',
            })
        },
        {
            path: `${ROUTE_PATH.volunteerApply}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(VolunteerApplyViewControl, {})
        },
        {
            path: `${ROUTE_PATH.volunteerApply}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(VolunteerApplyViewControl, {})
        },
        // 设备管理
        {
            path: `${ROUTE_PATH.Device}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(DeviceViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_device_all',
                select_permission: '设备管理查看',
                edit_permission: '设备管理编辑',
                delete_permission: '设备管理删除',
                add_permission: '设备管理新增',
            })
        },
        // 设备管理-智能设备
        {
            path: `${ROUTE_PATH.XxhDevice}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(XxhDeviceViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_device_all',
                select_permission: '设备管理查看',
                edit_permission: '设备管理编辑',
                delete_permission: '设备管理删除',
                add_permission: '设备管理新增',
            })
        },
        // 编辑设备
        {
            path: `${ROUTE_PATH.ChangeDevice}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDeviceViewControl, {
            })
        },
        // 新增设备
        {
            path: `${ROUTE_PATH.ChangeDevice}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDeviceViewControl, {
            })
        },
        // 查看设备
        {
            path: `${ROUTE_PATH.DeviceLook}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(DeviceViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_device_look',
                select_permission: '设备管理查询',
                edit_permission: '设备管理编辑',
                delete_permission: '设备管理删除',
                add_permission: '设备管理新增',
            })
        },
        // 监控管理
        {
            path: `${ROUTE_PATH.Monitor}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(MonitorViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_monitor_list_all',
                select_permission: '监控管理查询',
                edit_permission: '监控管理编辑',
                delete_permission: '监控管理删除',
                add_permission: '监控管理新增',
            })
        },
        // 查看监控
        {
            path: `${ROUTE_PATH.MonitorLook}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(MonitorViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_monitor_list_look',
                select_permission: '监控管理查看',
            })
        },
        {
            path: `${ROUTE_PATH.changeMonitor}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeMonitorViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeMonitor}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeMonitorViewControl, {})
        },
        {
            path: `${ROUTE_PATH.DeviceLog}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(DeviceLogViewControl, {})
        },
        // 社会群体类型管理
        {
            path: ROUTE_PATH.socialGroupsType,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(SocialGroupsTypeViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_social_groups_type_list_all',
                select_permission: '社会群体类型管理查看',
                edit_permission: '社会群体类型管理编辑',
                delete_permission: '社会群体类型管理删除',
                add_permission: '社会群体类型管理新增',
            })
        },
        // 查看社会群体类型
        {
            path: ROUTE_PATH.socialGroupsTypeLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(SocialGroupsTypeViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_social_groups_type_list_look',
                select_permission: '社会群体类型管理查看',
            })
        },
        {
            path: `${ROUTE_PATH.changeSocialGroupsType}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSocialGroupsTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.changeSocialGroupsType,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSocialGroupsTypeViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        }, {
            path: ROUTE_PATH.activityRoom,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityRoomViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_room_list_all',
                    select_permission: '活动室列表查看',
                    delete_permission: '活动室列表删除',
                    add_permission: '活动室列表新增',
                    edit_permission: '活动室列表编辑',
                })
        },
        {
            path: ROUTE_PATH.activityRoomLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityRoomViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_room_list_look',
                    select_permission: '查看活动室列表查看',
                })
        },
        {
            path: `${ROUTE_PATH.changeActivityRoom}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityRoomViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.changeActivityRoom,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityRoomViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.activityRoomReservation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityRoomReservationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_room_reservation_list_all',
                    select_permission: '活动室预约列表查询',
                    edit_permission: '活动室预约列表编辑',
                    add_permission: '活动室预约列表新增',
                    delete_permission: '活动室预约列表删除',
                    select_param: { type: 'my' }
                })
        },
        {
            path: ROUTE_PATH.activityRoomReservationRecord,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ActivityRoomReservationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_activity_room_reservation_list_all',
                    select_permission: '活动室预约列表查询',
                    edit_permission: '活动室预约列表编辑',
                    select_param: { type: 'record' }
                })
        },
        {
            path: `${ROUTE_PATH.changeActivityRoomReservation}/${KEY_PARAM}`,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityRoomReservationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        {
            path: ROUTE_PATH.changeActivityRoomReservation,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeActivityRoomReservationViewControl,
                {
                    permission: PermissionList.RolePermission_Select,
                }
            )
        },
        // 活动信息
        {
            path: ROUTE_PATH.activityInformation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ActivityInformationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_activity_information_list_all',
                select_permission: '活动信息查询',
                edit_permission: '活动信息编辑',
                add_permission: '活动信息新增',
                delete_permission: '活动信息删除',
            })
        },
        /** =====费用管理===== */
        {
            path: ROUTE_PATH.incidentalList,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                IncidentalManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_incidental_list',
                    select_permission: '杂费管理查询',
                    edit_permission: '杂费管理编辑',
                    delete_permission: '杂费管理删除',
                    add_permission: '杂费管理新增',
                }
            )
        },
        {
            path: ROUTE_PATH.incidentalListLook,
            mainFormID: layoutMain,
            targetType: 'secure',
            exact: true,
            targetObject: createObject(
                IncidentalManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_incidental_list_look',
                    select_permission: '查看杂费管理查询'
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeIncidental}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeIncidentalViewControl,
                {
                })
        },
        {
            path: ROUTE_PATH.changeIncidental,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeIncidentalViewControl,
                {
                })
        },

        {
            path: ROUTE_PATH.foodCostList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FoodCostManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_food_cost_list',
                    select_permission: '餐费管理查询',
                    edit_permission: '餐费管理编辑',
                    delete_permission: '餐费管理删除',
                    add_permission: '餐费管理新增',
                }
            )
        },
        {
            path: ROUTE_PATH.foodCostList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FoodCostManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_food_cost_list_look',
                    select_permission: '查看餐费管理查询',
                }
            )
        },
        {
            path: ROUTE_PATH.changeFoodCost,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(FoodCostViewControl)
        },
        {
            path: `${ROUTE_PATH.changeFoodCost}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(FoodCostViewControl)
        },
        // {
        //     path: ROUTE_PATH.medicalCost,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         MedicalCostManageViewControl,
        //         {
        //             permission_class: AppServiceUtility.login_service,
        //             get_permission_name: 'get_function_list',
        //             request_url: 'get_bed_list_all',
        //             select_permission: '床位管理查看',
        //             edit_permission: '床位管理编辑',
        //             delete_permission: '床位管理删除',
        //             add_permission: '床位管理新增',
        //         }
        //     )
        // },
        {
            path: ROUTE_PATH.changeMedicalCost,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(MedicalCostViewControl)
        },
        {
            path: `${ROUTE_PATH.changeMedicalCost}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(MedicalCostViewControl)
        },
        // {
        //     path: ROUTE_PATH.diaperCost,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         DiaperCostManageViewControl,
        //         {
        //             permission_class: AppServiceUtility.login_service,
        //             get_permission_name: 'get_function_list',
        //             request_url: 'get_bed_list_all',
        //             select_permission: '床位管理查看',
        //             edit_permission: '床位管理编辑',
        //             delete_permission: '床位管理删除',
        //             add_permission: '床位管理新增',
        //         }
        //     )
        // },
        {
            path: ROUTE_PATH.changeDiaperCost,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(DiaperCostViewControl)
        },
        {
            path: `${ROUTE_PATH.changeDiaperCost}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(DiaperCostViewControl)
        },
        /** 报表管理 */
        {
            path: ROUTE_PATH.operationStatus,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(OperationStatusViewControl)
        },
        /** 福利院--工作人员统计--报表 */
        {
            path: ROUTE_PATH.staffStatistics,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(staffStatisticsViewControl)
        },
        {
            /** 幸福院--工作人员统计--报表 */
            path: ROUTE_PATH.happyStaffStatistics,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(happyStaffStatisticsViewControl)
        },
        /** 入住老人情况统计-报表 */
        {
            path: ROUTE_PATH.elderlyResidents,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(elderlyResidentsViewControl)
        },
        {
            path: ROUTE_PATH.elderCheck,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(elderCheckViewControl)
        },
        {
            path: ROUTE_PATH.basicFacilities,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BasicFacilitiesViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '基本设施情况查询',
                }
            )
        },
        {
            path: ROUTE_PATH.adoptionSituation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AdoptionSituationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '收养人员情况查询',
                }
            )
        },
        {
            path: ROUTE_PATH.fireQualification,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '消防资质'
            })
        },
        {
            path: ROUTE_PATH.fireQualification2,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification3,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification4,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification5,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification6,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification7,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification8,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification9,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification10,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        {
            path: ROUTE_PATH.fireQualification11,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
            })
        },
        // 日托订单
        {
            path: ROUTE_PATH.fireQualification_rtdd,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '日托订单统计'
            })
        },
        // 日托支付
        {
            path: ROUTE_PATH.fireQualification_rtzf,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '日托支付统计'
            })
        },
        // 活动报名统计
        {
            path: ROUTE_PATH.fireQualification_hdbm,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '活动报名统计'
            })
        },
        // 年龄统计
        {
            path: ROUTE_PATH.fireQualification_nntj,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '年龄统计'
            })
        },
        // 长者数据
        {
            path: ROUTE_PATH.fireQualification_zzsj,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '长者数据'
            })
        },
        // 长者报表
        {
            path: ROUTE_PATH.fireQualification_zzbb,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '长者报表'
            })
        },
        // 月收入统计
        {
            path: ROUTE_PATH.fireQualification_ysr,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '月收入统计'
            })
        },
        // 活动报告
        {
            path: ROUTE_PATH.fireQualification_hdbg,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireQualificationViewControl, {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_bed_list_all',
                select_permission: '消防资质情况查询',
                select_type: '活动报告'
            })
        },
        {

            path: ROUTE_PATH.workerConstitute,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkerConstituteViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '职工构成情况查询',
                }
            )
        },
        {
            path: ROUTE_PATH.compensation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CompensationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '薪酬待遇情况查询',
                }
            )
        },
        {
            path: ROUTE_PATH.organizationReport,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationReportViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_bed_list_all',
                    select_permission: '机构报表情况查询',
                }
            )
        },
        {
            path: ROUTE_PATH.changeSquence,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeSquenceControl
            )
        },
        {
            path: ROUTE_PATH.subsidyAutoRecharge,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SubsidyAutoRechargeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_journal_list',
                    select_permission: '单据签核查看',
                    add_permission: '单据签核编辑',
                })
        },
        {
            path: `${ROUTE_PATH.ServicePersonApplyReviewed}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServicePersonReviewedViewControl,
                {
                })
        },
        {
            path: `${ROUTE_PATH.buyService}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BuyServiceViewControl,
                {
                })
        },
        {
            path: `${ROUTE_PATH.buyService}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BuyServiceViewControl,
                {
                })
        },
        // 呼叫中心
        {
            path: `${ROUTE_PATH.callCenter}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: callCenterInstance
        },
        // 呼叫中心
        {
            path: `${ROUTE_PATH.callCenter}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: callCenterInstance
        },
        // 南海移动呼叫中心
        {
            path: `${ROUTE_PATH.callCenterNh}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CallCenterNhViewControl,
                {})
        },
        // 南海移动呼叫中心帐号
        {
            path: `${ROUTE_PATH.changeCallCenterZh}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCallCenterZhViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeCallCenterZh}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCallCenterZhViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.callCenterZh}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CallCenterZhViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_call_center_zh_list_all',
                    select_permission: '移动呼叫中心帐号查询',
                    edit_permission: '移动呼叫中心帐号编辑',
                    delete_permission: '移动呼叫中心帐号删除',
                    add_permission: '移动呼叫中心帐号新增',
                }
            )
        },
        // 南海移动呼叫中心日志
        {
            path: `${ROUTE_PATH.callCenterLog}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CallCenterLogViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_call_center_log_list_all',
                }
            )
        },
        // 适老化设备档案
        {
            path: `${ROUTE_PATH.changeDeviceFile}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeviceFileViewControl,
                {
                }
            )
        },
        // 适老化设备管理
        {
            path: `${ROUTE_PATH.changeDeviceManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeviceManageViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.deviceManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_manage_list_all',
                    select_permission: '适老化设备管理查询',
                    edit_permission: '适老化设备管理编辑',
                    delete_permission: '适老化设备管理删除',
                    add_permission: '适老化设备管理新增',
                }
            )
        },
        // 适老化设备警报
        {
            path: `${ROUTE_PATH.changeDeviceWarning}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeviceWarningViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeDeviceWarning}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDeviceWarningViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.deviceWarning}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceWarningViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_warning_list_all',
                    select_permission: '适老化设备警报查询',
                    edit_permission: '适老化设备警报编辑',
                    delete_permission: '适老化设备警报删除',
                    add_permission: '适老化设备警报新增',
                }
            )
        },
        // 知识库
        {
            path: `${ROUTE_PATH.knowledgeBase}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                KnowledgeBaseListViewControl,
                {})
        },
        // 知识库详情
        {
            path: `${ROUTE_PATH.knowledgeDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                KnowledgeDetailsViewControl,
                {})
        },
        // 知识库录入
        {
            path: `${ROUTE_PATH.knowledgeEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                KnowledgeEditViewControl,
                {})
        },
        {
            path: `${ROUTE_PATH.knowledgeEdit}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                KnowledgeEditViewControl,
                {})
        },
        // 紧急呼叫
        {
            path: `${ROUTE_PATH.emergencyCall}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EmergencyCallViewControl,
                {})
        },
        // 紧急呼叫详情
        {
            path: `${ROUTE_PATH.emergencyCallDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EmergencyCallDetailsViewControl,
                {})
        },
        // 商城管理
        // 数据字典
        {
            path: `${ROUTE_PATH.dataDictionary}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DataDictionaryViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_data_dictionary_list_all',
                    select_permission: '数据字典查询',
                    edit_permission: '数据字典编辑',
                    delete_permission: '数据字典删除',
                    add_permission: '数据字典新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeDataDictionary}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDataDictionaryViewControl, {})
        },
        // 新增一级节点
        {
            path: `${ROUTE_PATH.changeDataDictionary}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDataDictionaryViewControl, {})
        },
        // 默认父类新增子字典，KEY_PARAM一定为parent
        {
            path: `${ROUTE_PATH.changeChildDataDictionary}/${KEY_PARAM}/${CODE_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDataDictionaryViewControl, {
                type: 'changeChild'
            })
        },
        {
            path: `${ROUTE_PATH.changeChildDataDictionary}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDataDictionaryViewControl, {
                type: 'changeChild'
            })
        },
        {
            path: `${ROUTE_PATH.changeChildDataDictionary}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeDataDictionaryViewControl, {
                type: 'changeChild'
            })
        },
        // 板块列表
        {
            path: `${ROUTE_PATH.blocks}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BlocksViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_blocks_list_all',
                    select_permission: '商品信息查询',
                    edit_permission: '商品信息编辑',
                    delete_permission: '商品信息删除',
                    add_permission: '商品信息新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeBlock}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeBlockViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeBlock}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeBlockViewControl, {})
        },
        // 板块推荐
        {
            path: `${ROUTE_PATH.blockDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BlockDetailsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_recommend_list_all',
                    select_permission: '板块信息查询',
                    edit_permission: '板块信息编辑',
                    delete_permission: '板块信息删除',
                    add_permission: '板块信息新增',
                })
        },
        // 新增板块的推荐信息，KEY_PARAM一定为parent
        {
            path: `${ROUTE_PATH.changeBlockDetails}/${KEY_PARAM}/${CODE_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeBlockDetailsViewControl, {
                // type: 'changeChild'
            })
        },
        // {
        //     path: `${ROUTE_PATH.changeBlockDetails}/${KEY_PARAM}`,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(ChangeBlockDetailsViewControl, {})
        // },
        // 不允许空添加
        // {
        //     path: `${ROUTE_PATH.changeBlockDetails}`,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(ChangeBlockDetailsViewControl, {})
        // },
        // 商品列表
        {
            path: `${ROUTE_PATH.products}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ProductsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_goods_list',
                    select_permission: '商品信息查询',
                    edit_permission: '商品信息编辑',
                    delete_permission: '商品信息删除',
                    add_permission: '商品信息新增',
                })
        },
        {
            path: `${ROUTE_PATH.productPreview}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ProductPreviewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_goods_list',
                    select_permission: '商品信息查询',
                    edit_permission: '商品信息编辑',
                    delete_permission: '商品信息删除',
                    add_permission: '商品信息新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeProduct}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeProductViewControl, {})
        },
        {
            path: `${ROUTE_PATH.changeProduct}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeProductViewControl, {})
        },
        // 商品订单列表
        {
            path: `${ROUTE_PATH.productOrders}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ProductOrdersViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_goods_list',
                    select_permission: '商品订单查询',
                    edit_permission: '商品订单编辑',
                    delete_permission: '商品订单删除',
                    add_permission: '商品订单新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeProductOrder}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeProductOrderControl, {
                edit_permission: '商品订单编辑'
            })
        },
        {
            path: `${ROUTE_PATH.changeProductOrder}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeProductOrderControl, {})
        },
        // 商品审核
        {
            path: `${ROUTE_PATH.productsSh}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ProductsShViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_goods_list',
                    select_permission: '商品审核查询',
                    edit_permission: '商品审核编辑',
                    // delete_permission: '商品审核删除',
                    // add_permission: '商品审核新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeProductSh}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(ChangeProductShControl, {})
        },
        // 服务商财务总览
        {
            path: `${ROUTE_PATH.SPFinancialOverview}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialOverviewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '服务商对账查询',
                    request_url: 'get_sp_financial_overview_list_all',
                })
        },
        // 服务商对账明细
        {
            path: `${ROUTE_PATH.SPFinancialDetail}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(FinancialDetailControl, {
                select_permission: '服务商对账查询',
            })
        },
        // 服务商入账记录
        {
            path: `${ROUTE_PATH.SPEntryRecord}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EntryRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '服务商对账查询',
                    request_url: 'get_sp_entry_record_list_all',
                })
        },
        // 平台财务总览
        {
            path: `${ROUTE_PATH.PTFinancialOverview}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FinancialOverviewViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '平台对账查询',
                    request_url: 'get_pt_financial_overview_list_all'
                })
        },
        // 平台资金明细
        {
            path: `${ROUTE_PATH.capitalDetail}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CapitalDetailViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '平台对账查询',
                })
        },
        // 平台入账记录
        {
            path: `${ROUTE_PATH.PTEntryRecord}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EntryRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    select_permission: '平台对账查询',
                    request_url: 'get_pt_entry_record_list_all',
                })
        },
        // 平台对账明细
        {
            path: `${ROUTE_PATH.PTFinancialDetail}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(FinancialDetailControl, {
                select_permission: '平台对账查询',
            })
        },
        // 分账审核
        {
            path: `${ROUTE_PATH.separateAccountsSH}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(SeparateAccountsSHControl, {
                select_permission: '分账审核查询',
                edit_permission: '分账审核编辑',
            })
        },
        // 服务预约编辑
        {
            path: `${ROUTE_PATH.reserveBuyService}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReserveBuyServiceViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_reserve_service',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 服务预约编辑
        {
            path: `${ROUTE_PATH.reserveBuyServiceEdit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReserveBuyServiceEditViewControl,
                {
                })
        },
        {
            path: `${ROUTE_PATH.reserveBuyServiceEdit}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReserveBuyServiceEditViewControl,
                {
                })
        },
        {
            path: `${ROUTE_PATH.serviceSettlementStatement}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceSettlementStatementViewControl,
                {
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_record_list_all',
                    select_permission: '服务项目查看',
                })
        },
        {
            path: `${ROUTE_PATH.serviceEvaluate}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceEvaluateViewControl,
                {
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_evaluate_list',
                    select_permission: '服务评价查询',
                    edit_permission: '服务评价编辑',
                })
        },
        {
            path: `${ROUTE_PATH.serviceReturnVisit}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceReturnVisitListViewControl,
                {
                    get_permission_name: 'get_function_list',
                    request_url: 'get_service_return_visit_list',
                    select_permission: '服务回访查询',
                    edit_permission: '服务回访编辑',
                    add_permission: '服务回访新增'
                })
        },
        // 居家养老服务对象月报表
        {
            path: `${ROUTE_PATH.allowanceReportMonth}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AllowanceReportMonthViewControl,
                {
                })
        },

        // 居家养老服务对象明细表
        {
            path: `${ROUTE_PATH.allowanceReportDetails}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AllowanceReportDetailsViewControl,
                {
                })
        },
        // 工作人员统计
        {
            path: `${ROUTE_PATH.workPeopleStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                WorkPeopleStatisticsViewControl,
                {})
        },
        // 订单统计
        {
            path: `${ROUTE_PATH.orderReport}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(OrderReportControl)
        },
        {
            path: `${ROUTE_PATH.dataImport}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DataImportControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_order_report',
                    select_permission: '订单统计报表',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: `${ROUTE_PATH.townStreetSettlement}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                townStreetSettlementViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                })
        },
        {
            path: `${ROUTE_PATH.serviceProviderRecordSettlement}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(serviceRecordSettlementViewControl)
        },
        // 账户充值
        {
            path: `${ROUTE_PATH.accountList}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AccountListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_subsidy_list',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 账户充值
        {
            path: `${ROUTE_PATH.baseSituation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BaseSituationControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    // select_permission: '基本情况统计报表查询',
                    edit_permission: '组织机构编辑',
                }
            )
        },
        {
            path: `${ROUTE_PATH.accountDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AccountDetailsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_subsidy_account_detail',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 慈善账户充值
        {
            path: `${ROUTE_PATH.charitableAccount}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CharitableAccountViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_subsidy_list',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 慈善账户详情
        {
            path: `${ROUTE_PATH.charitableAccountDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CharitableAccountDetailsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '服务项目查看',
                    edit_permission: '服务项目编辑',
                    delete_permission: '服务项目删除',
                    add_permission: '服务项目新增',
                })
        },
        // 微信公众号统计
        {
            path: `${ROUTE_PATH.xfyFollowStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                officialAccountStatisticsControl,
                {}
            )
        },
        // 订单全景
        {
            path: `${ROUTE_PATH.orderAllJ}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrderAllJViewControl,
                {}
            )
        },
        // 
        {
            path: `${ROUTE_PATH.orderStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrderStatisticsViewControl,
                {}
            )
        },
        // 计量单位
        {
            path: `${ROUTE_PATH.Units}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                UnitsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '计量单位查询',
                    edit_permission: '计量单位编辑',
                    delete_permission: '计量单位删除',
                    add_permission: '计量单位新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeUnits}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeUnitsViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeUnits}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeUnitsViewControl,
                {}
            )
        },
        // 物料分类
        {
            path: `${ROUTE_PATH.thingSort}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ThingSortViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '物料分类查询',
                    edit_permission: '物料分类编辑',
                    delete_permission: '物料分类删除',
                    add_permission: '物料分类新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeThingSort}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeThingSortViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeThingSort}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeThingSortViewControl,
                {}
            )
        },
        // 物料档案
        {
            path: `${ROUTE_PATH.thingArchives}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ThingArchivesViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '物料档案查询',
                    edit_permission: '物料档案编辑',
                    delete_permission: '物料档案删除',
                    add_permission: '物料档案新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeThingArchives}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeThingArchivesViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.changeThingArchives}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeThingArchivesViewControl,
                {}
            )
        },
        // 药品档案
        {
            path: `${ROUTE_PATH.medicineFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                MedicineFileViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '药品档案查看',
                    edit_permission: '药品档案编辑',
                    delete_permission: '药品档案删除',
                    add_permission: '药品档案新增',
                }
            )
        },
        // 药品档案新增
        {
            path: `${ROUTE_PATH.addMedicineFile}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddMedicineViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addMedicineFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddMedicineViewControl,
                {}
            )
        },
        // 疾病档案
        {
            path: `${ROUTE_PATH.diseaseFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DiseaseFileViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '疾病档案查看',
                    edit_permission: '疾病档案编辑',
                    delete_permission: '疾病档案删除',
                    add_permission: '疾病档案新增',
                }
            )
        },
        // 疾病档案新增
        {
            path: `${ROUTE_PATH.addDiseaseFile}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddDiseaseViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addDiseaseFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddDiseaseViewControl,
                {}
            )
        },
        // 过敏档案
        {
            path: `${ROUTE_PATH.allergyFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AllergyFileViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '过敏档案查看',
                    edit_permission: '过敏档案编辑',
                    delete_permission: '过敏档案删除',
                    add_permission: '过敏档案新增',
                }
            )
        },
        // 过敏档案新增
        {
            path: `${ROUTE_PATH.addAllergyFile}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddAllergyViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addAllergyFile}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddAllergyViewControl,
                {}
            )
        },
        // app工单核算
        {
            path: `${ROUTE_PATH.orderAccounting}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrderAccountingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: 'app工单核算查看',
                    add_permission: 'app工单核算新增'
                }
            )
        },
        // 费用统计
        {
            path: `${ROUTE_PATH.costStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CostStatisticsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '费用统计查看',
                    add_permission: '费用统计新增'
                }
            )
        },
        // 日托订单列表
        {
            path: `${ROUTE_PATH.dayCareOrderList}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DayCareOrderListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '日托订单列表查看',
                    edit_permission: '日托订单列表编辑',
                    add_permission: '日托订单列表新增',
                }
            )
        },
        // 日托订单统计
        {
            path: `${ROUTE_PATH.dayCareOrderStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DayCareOrderStatisticsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '日托订单统计查看',
                    add_permission: '日托订单统计新增'
                }
            )
        },
        {
            path: `${ROUTE_PATH.dayCareOrderList}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DayCareOrderListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '日托订单列表查看',
                    edit_permission: '日托订单列表编辑',
                    delete_permission: '日托订单列表删除',
                    add_permission: '日托订单列表新增',
                }
            )
        },
        // 验收管理列表
        {
            path: `${ROUTE_PATH.checkManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CheckManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '验收管理列表查询',
                    edit_permission: '验收管理列表编辑',
                    delete_permission: '验收管理列表删除',
                    add_permission: '验收管理列表新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.checkManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CheckManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '验收管理列表查看',
                    edit_permission: '验收管理列表编辑',
                    delete_permission: '验收管理列表删除',
                    add_permission: '验收管理列表新增',
                }
            )
        },
        // 长者膳食
        {
            path: `${ROUTE_PATH.elderMeals}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeElderMealsViewControl,
                {}
            )
        },
        // 幸福小站
        {
            path: `${ROUTE_PATH.happinessStation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                HappinessStationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '幸福小站查询',
                    edit_permission: '幸福小站编辑',
                    delete_permission: '幸福小站删除',
                    add_permission: '幸福小站新增',
                }
            )
        },
        // 综合统计
        {
            path: `${ROUTE_PATH.comprehensiveStatistics}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ComprehensiveStatisticsViewControl,
                {}
            )
        },
        // 养老机构基本信息
        {
            path: `${ROUTE_PATH.organizationImformation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationImformationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '养老机构基本信息查看',
                    add_permission: '养老机构基本信息新增'
                }
            )
        },
        // 入住长者情况统计
        {
            path: `${ROUTE_PATH.ckeckInElder}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CheckInElderViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '入住长者情况统计查询',
                    add_permission: '入住长者情况统计新增',
                }
            )
        },
        // 从业人员情况统计
        {
            path: `${ROUTE_PATH.employees}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EmployeesViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '从业人员情况统计查询',
                }
            )
        },
        // 幸福院运营情况统计
        {
            path: `${ROUTE_PATH.operationHappiness}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OperationHappinessViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '幸福院运营情况统计查询',
                }
            )
        },
        // 民办非营机构运营资助统计
        {
            path: `${ROUTE_PATH.orgOperationFund}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrgOperationFundViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '民办非营机构运营资助统计查询',
                }
            )
        },
        // 民办非营机构床位资助统计
        {
            path: `${ROUTE_PATH.orgBedFund}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrgBedFundViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '民办非营机构床位资助查询',
                }
            )
        },
        // 验收及评级情况统计
        {
            path: `${ROUTE_PATH.acceptancRating}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AcceptancRatingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '验收及评级情况统计查询',
                }
            )
        },
        // 坐席运营统计
        {
            path: `${ROUTE_PATH.seatOperation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                SeatOperationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '坐席运营统计查询',
                }
            )
        },
        // 项目协议与报告
        {
            path: `${ROUTE_PATH.changeDealAndReport}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDealAndReportViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeDealAndReport}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDealAndReportViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.dealAndReport}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DealAndReportViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_deal_and_report_list_all',
                    select_permission: '项目协议与报告查询',
                    edit_permission: '项目协议与报告编辑',
                    delete_permission: '项目协议与报告删除',
                    add_permission: '项目协议与报告新增',
                }
            )
        },
        // 年度计划与总结
        {
            path: `${ROUTE_PATH.changeYearplanAndSummary}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeYearplanAndSummaryViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeYearplanAndSummary}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeYearplanAndSummaryViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.yearplanAndSummary}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                YearplanAndSummaryViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_yearplan_and_summary_list_all',
                    select_permission: '年度计划与总结查询',
                    edit_permission: '年度计划与总结编辑',
                    delete_permission: '年度计划与总结删除',
                    add_permission: '年度计划与总结新增',
                }
            )
        },
        // 月度计划
        {
            path: `${ROUTE_PATH.changeMonthPlan}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMonthPlanViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeMonthPlan}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeMonthPlanViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.monthPlan}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                MonthPlanViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_month_plan_list_all',
                    select_permission: '月度计划查询',
                    edit_permission: '月度计划编辑',
                    delete_permission: '月度计划删除',
                    add_permission: '月度计划新增',
                }
            )
        },
        // 消防设施
        {
            path: `${ROUTE_PATH.changeFireEquipment}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeFireEquipmentViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeFireEquipment}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeFireEquipmentViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.fireEquipment}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                FireEquipmentViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_fire_equipment_list_all',
                    select_permission: '消防设施查询',
                    edit_permission: '消防设施编辑',
                    delete_permission: '消防设施删除',
                    add_permission: '消防设施新增',
                }
            )
        },
        // 服务商基本情况统计
        {
            path: `${ROUTE_PATH.servicerList}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BaseinfoServicerViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '服务商基本情况统计查询',
                    edit_permission: '组织机构编辑',
                }
            )
        },
        // 机构基本情况统计
        {
            path: `${ROUTE_PATH.organList}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                BaseinfoOrganViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '机构基本情况统计查询',
                    edit_permission: '组织机构编辑',
                }
            )
        },
        // 机构运营统计情况
        {
            path: `${ROUTE_PATH.organOperation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OperationOrganViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '机构运营统计情况查询',
                }
            )
        },
        // APP用户管理
        {
            path: `${ROUTE_PATH.changeAppUserManage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppUserManageViewControl,
                {}
            )
        },
        // APP用户管理
        {
            path: `${ROUTE_PATH.appUserManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppUserManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: 'APP用户管理查询',
                }
            )
        },
        // APP意见反馈
        {
            path: `${ROUTE_PATH.changeAppFeedback}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppFeedbackViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.appFeedback}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppFeedbackViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: 'APP意见反馈查询',
                    edit_permission: 'APP意见反馈编辑',
                }
            )
        },
        // APP在线咨询
        {
            path: `${ROUTE_PATH.changeAppOnlineTalk}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppOnlineTalkViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.appOnlineTalk}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppOnlineTalkViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_app_online_message_list',
                    select_permission: 'APP在线咨询查询',
                }
            )
        },
        // APP设置
        {
            path: `${ROUTE_PATH.appSettings}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppSettingsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: 'APP设置查询',
                }
            )
        },
        {
            path: `${ROUTE_PATH.appPrivacyAgree}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppPrivacyAgreeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '超管查询',
                    delete_permission: '活动列表查询',
                }
            )
        },
        // APP老友圈
        {
            path: `${ROUTE_PATH.changeAppFriendCircle}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeAppFriendsCircleViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.appFriendCircle}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AppFriendsCircleViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_friends_circle_list',
                    select_permission: 'APP老友圈查询',
                }
            )
        },
        // 圈子管理
        {
            path: `${ROUTE_PATH.changeCircle}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCircleViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.changeCircle}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeCircleViewControl,
                {
                }
            )
        },
        {
            path: `${ROUTE_PATH.circle}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CircleViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_circle_list',
                    select_permission: '圈子管理查询',
                    edit_permission: '圈子管理编辑',
                    delete_permission: '圈子管理删除',
                    add_permission: '圈子管理新增',
                }
            )
        },
        // 智能监护-通话设置
        {
            path: `${ROUTE_PATH.callSettings}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CallSettingsViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.callSettings}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CallSettingsViewControl,
                {}
            )
        },
        // 智能监护管理
        {
            path: `${ROUTE_PATH.careManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CareManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '智能监护管理查看',
                    edit_permission: '智能监护管理编辑',
                    delete_permission: '智能监护管理删除',
                    add_permission: '智能监护管理新增',
                }
            )
        },
        // 智能监护-电子围栏
        {
            path: `${ROUTE_PATH.electronicFence}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElectronicFenceViewControl,
                {}
            )
        },
        // 智能监护运营
        {
            path: `${ROUTE_PATH.guardianshipOperation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                GuardianshipOperationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '智能监护运营查询',
                    edit_permission: '智能监护运营编辑',
                    delete_permission: '智能监护运营删除',
                    add_permission: '智能监护运营新增',
                }
            )
        },
        // 智能监护-监护运营-查看信息
        {
            path: `${ROUTE_PATH.operationMap}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OperationMapViewControl,
                {}
            )
        },
        // 智能设备期限
        {
            path: `${ROUTE_PATH.deviceTerm}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceTermViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_term_list_all',
                    select_permission: '智能监护运营查询',
                }
            )
        },
        // 智能设备警报
        {
            path: `${ROUTE_PATH.deviceMessage}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceMessageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_message_list_all',
                    select_permission: '智能监护运营查询',
                    edit_permission: '智能监护运营编辑',
                }
            )
        },
        {
            path: `${ROUTE_PATH.deviceMessage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceMessageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_message_list_all',
                    select_permission: '智能监护运营查询',
                    edit_permission: '智能监护运营编辑',
                }
            )
        },
        // 智能定位/轨迹
        {
            path: `${ROUTE_PATH.deviceLocation}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                DeviceLocationViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_device_location',
                    select_permission: '智能监护运营查询',
                    edit_permission: '智能监护运营编辑',
                }
            )
        },
        // 图片管理
        {
            path: `${ROUTE_PATH.pictureManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PictureManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '图片管理查看',
                    edit_permission: '图片管理编辑',
                    delete_permission: '图片管理删除',
                    add_permission: '图片管理新增',
                }
            )
        },
        // 社区幸福院评比指标设置
        {
            path: `${ROUTE_PATH.targetSetting}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RatingtargetSettingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '社区幸福院评比指标设置查询',
                    edit_permission: '社区幸福院评比指标设置编辑',
                    delete_permission: '社区幸福院评比指标设置删除',
                    add_permission: '社区幸福院评比指标设置新增',
                }
            )
        },
        // 社区幸福院评比详细指标
        {
            path: `${ROUTE_PATH.targetDetails}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                RatingtargetDetailsViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '社区幸福院评比详细指标查询',
                    edit_permission: '社区幸福院评比详细指标编辑',
                    delete_permission: '社区幸福院评比详细指标删除',
                    add_permission: '社区幸福院评比详细指标新增',
                }
            )
        },
        // 评比详细指标新增
        {
            path: `${ROUTE_PATH.addTargetDetails}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddTargetDetailsViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addTargetDetails}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddTargetDetailsViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.nursingGrade}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingGradeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_grade_list',
                    select_permission: '护理等级查询',
                    edit_permission: '护理等级编辑',
                    delete_permission: '护理等级删除',
                    add_permission: '护理等级新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.nursingGrade}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                NursingGradeViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_nursing_grade_list',
                    select_permission: '护理等级查询',
                    edit_permission: '护理等级编辑',
                    delete_permission: '护理等级删除',
                    add_permission: '护理等级新增',
                }
            )
        },
        // 长者关怀记录
        {
            path: `${ROUTE_PATH.elderCareRecord}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElderCareRecordViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '长者关怀记录查询',
                    edit_permission: '长者关怀记录编辑',
                    delete_permission: '长者关怀记录删除',
                    add_permission: '长者关怀记录新增',
                }
            )
        },
        // 长者关怀类型设置
        {
            path: `${ROUTE_PATH.careTypeSetting}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CareTypeSettingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '长者关怀类型设置查看',
                    edit_permission: '长者关怀类型设置编辑',
                    delete_permission: '长者关怀类型设置删除',
                    add_permission: '长者关怀类型设置新增',
                }
            )
        },
        {
            path: `${ROUTE_PATH.careTypeSetting}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                CareTypeSettingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_charitable_account_detail',
                    select_permission: '长者关怀类型设置查看',
                    edit_permission: '长者关怀类型设置编辑',
                    delete_permission: '长者关怀类型设置删除',
                    add_permission: '长者关怀类型设置新增',
                }
            )
        },
        // 押金结算设置
        {
            path: ROUTE_PATH.depositSetting,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                DepositSettingViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_jiujiang_hydropower_list_all',
                    select_permission: '押金结算设置查询',
                    edit_permission: '押金结算设置编辑',
                    delete_permission: '押金结算设置删除',
                    add_permission: '押金结算设置新增',
                })
        },
        {
            path: `${ROUTE_PATH.chanegDepositSetting}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDepositViewControl, {
                permission: PermissionList.RolePermission_Select
            })
        },
        {
            path: `${ROUTE_PATH.chanegDepositSetting}`,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                ChangeDepositViewControl, {
                permission: PermissionList.RolePermission_Select,
            })
        },

        // 回访问卷题目
        {
            path: ROUTE_PATH.subjectList,
            mainFormID: layoutMain,
            exact: true,
            targetType: 'secure',
            targetObject: createObject(
                SubjectListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_project_list_all',
                    select_permission: '回访问卷题目查询',
                    edit_permission: '回访问卷题目编辑',
                    delete_permission: '回访问卷题目删除',
                    add_permission: '回访问卷题目新增',
                })
        },
        {
            path: `${ROUTE_PATH.addSubject}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddProjectViewControl,
                {
                    assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.addSubject,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddProjectViewControl,
                {
                    assessmentProjectServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentProjectService, remote.url, "IAssessmentProjectService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 回访问卷
        {
            path: `${ROUTE_PATH.questionnaireList}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                QuestionnaireListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list',
                    select_permission: '回访问卷查看',
                    edit_permission: '回访问卷编辑',
                    delete_permission: '回访问卷删除',
                    add_permission: '回访问卷新增',
                })
        },
        {
            path: ROUTE_PATH.questionnaireList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                QuestionnaireListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list',
                    select_permission: '回访问卷查看',
                    edit_permission: '回访问卷编辑',
                    delete_permission: '回访问卷删除',
                    add_permission: '回访问卷新增',
                })
        },
        {
            path: `${ROUTE_PATH.addQuestionnaire}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddQuestionnaireViewControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        {
            path: ROUTE_PATH.addQuestionnaire,
            exact: true,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddQuestionnaireViewControl,
                {
                    assessmentTemplateServer_Fac: new AjaxJsonRpcLoadingFactory(IAssessmentTemplateService, remote.url, "IAssessmentTemplateService"),
                    permission: PermissionList.RolePermission_Select,
                })
        },
        // 意向回访
        {
            path: ROUTE_PATH.returnVisitList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ReturnVisitListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_assessment_template_list',
                    select_permission: '意向回访查看',
                    edit_permission: '意向回访编辑',
                    delete_permission: '意向回访删除',
                    add_permission: '意向回访新增',
                })
        },
        {
            path: `${ROUTE_PATH.addReturnVisit}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddReturnVisitControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addReturnVisit}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddReturnVisitControl,
                {}
            )
        },
        // 机构运营情况
        {
            path: ROUTE_PATH.organizationManage,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrganizationManageViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: '',
                    select_permission: '机构运营情况查询',
                    edit_permission: '机构运营情况编辑',
                    delete_permission: '机构运营情况删除',
                    add_permission: '机构运营情况新增',
                })
        },
        {
            path: `${ROUTE_PATH.addOperationSituation}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddOperationSituationControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.addOperationSituation}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddOperationSituationControl,
                {}
            )
        },
        // 老人能力评估
        {
            path: `${ROUTE_PATH.olderAbilityAssessment}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                olderAbilityAssessmentViewControl,
                {}
            )
        },
        // 投入管理
        {
            path: `${ROUTE_PATH.inputManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                InputManageViewControl,
                {}
            )
        },
        // 幸福院-评比管理
        {
            path: `${ROUTE_PATH.evaluationManage}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EvaluationManageViewControl,
                {}
            )
        },
        // 幸福院基础信息
        {
            path: `${ROUTE_PATH.HappinessEntryControl}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeHappinessViewControl,
                {}
            )
        },
        // 社区服务人员
        // {
        //     path: ROUTE_PATH.HappinessWorkerEntryControl,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         SheQuWorkerListViewControl,
        //         {
        //             permission_class: AppServiceUtility.login_service,
        //             get_permission_name: 'get_function_list',
        //             request_url: 'get_worker_list_all',
        //             select_permission: '社区服务人员查看',
        //             edit_permission: '社区服务人员编辑',
        //             delete_permission: '社区服务人员删除',
        //             add_permission: '社区服务人员新增',
        //         })
        // },
        // 机构服务人员
        {
            path: ROUTE_PATH.jigouWorkerEntry,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                JiGouWorkerListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '机构服务人员查看',
                    edit_permission: '机构服务人员编辑',
                    delete_permission: '机构服务人员删除',
                    add_permission: '机构服务人员新增',
                })
        },
        // 老人档案-个案服务情况
        {
            path: ROUTE_PATH.serviceSituationList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ServiceSituationListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '个案服务情况查看',
                    edit_permission: '个案服务情况编辑',
                    delete_permission: '个案服务情况删除',
                    add_permission: '个案服务情况新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeServiceSituation}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceSituationViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeServiceSituation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeServiceSituationViewControl,
                {})
        },
        // 老人档案-探访服务情况
        {
            path: ROUTE_PATH.visitSituationList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                VisitSituationListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '探访服务情况查看',
                    edit_permission: '探访服务情况编辑',
                    delete_permission: '探访服务情况删除',
                    add_permission: '探访服务情况新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeVisitSituation}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeVisitSituationViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.changeVisitSituation,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeVisitSituationViewControl,
                {})
        },
        // 老人档案-长者列表
        {
            path: ROUTE_PATH.elderInfoList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ElderInfoListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'user_org_link_list',
                    select_permission: '长者列表查询',
                    edit_permission: '长者列表编辑',
                    delete_permission: '长者列表删除',
                    add_permission: '长者管理新增',
                })
        },
        {
            path: `${ROUTE_PATH.changeElderInfoXfy}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ChangeElderInfoViewControl,
                {}
            )
        },
        // {
        //     path: ROUTE_PATH.allowanceCheck,
        //     mainFormID: layoutMain,
        //     targetType: 'secure',
        //     targetObject: createObject(
        //         AllowanceCheckViewControl,
        //         {
        //             permission_class: AppServiceUtility.login_service,
        //             get_permission_name: 'get_function_list',
        //             request_url: 'get_allowance_applied_list_look_edit',
        //             select_permission: '补贴审核查询',
        //             edit_permission: '补贴审核编辑',
        //             delete_permission: '补贴审核删除',
        //             add_permission: '补贴审核新增',
        //         }
        //     )
        // },
        // 平台运营-运营机构信息档案-旧
        {
            path: ROUTE_PATH.platformOrgList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                PlatformOrganizationListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '平台运营机构信息档案查询',
                    edit_permission: '平台运营机构信息档案编辑',
                    delete_permission: '平台运营机构信息档案删除',
                    add_permission: '平台运营机构信息档案新增',

                })
        },
        // 社区幸福院-运营机构信息
        {
            path: ROUTE_PATH.xfyOrgList,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                XFYOrganizationListViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '幸福院运营机构信息查询',
                    edit_permission: '幸福院运营机构信息编辑',
                    delete_permission: '幸福院运营机构信息删除',
                    add_permission: '幸福院运营机构信息新增',
                })
        },
        // 防疫-居家服务人员汇总表
        {
            path: ROUTE_PATH.jujiaEpidemicPreventionJj,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                JujiaEpidemicPreventionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '防疫汇总表查询',
                    edit_permission: '防疫汇总表判定',
                    delete_permission: '防疫汇总表删除',
                    add_permission: '防疫汇总表导出',
                    type: '服务商'
                })
        },
        {
            path: ROUTE_PATH.jujiaEpidemicPreventionSq,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                JujiaEpidemicPreventionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '防疫汇总表查询',
                    edit_permission: '防疫汇总表判定',
                    delete_permission: '防疫汇总表删除',
                    add_permission: '防疫汇总表导出',
                    type: '幸福院'
                })
        },
        {
            path: ROUTE_PATH.jujiaEpidemicPreventionJg,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                JujiaEpidemicPreventionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '防疫汇总表查询',
                    edit_permission: '防疫汇总表判定',
                    delete_permission: '防疫汇总表删除',
                    add_permission: '防疫汇总表导出',
                    type: '福利院'
                })
        },
        // 防疫-居家服务人员汇总表-详情
        {
            path: `${ROUTE_PATH.EpidemicPreventionDetail}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EpidemicPreventionDetailViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    edit_permission: '防疫汇总表判定',
                }
            )
        },
        {
            path: `${ROUTE_PATH.addXfyOrgInfo}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddXFYOrganizatonInfoViewControl,
                {}
            )
        },
        {
            path: ROUTE_PATH.addXfyOrgInfo,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                AddXFYOrganizatonInfoViewControl,
                {})
        },
        {
            path: ROUTE_PATH.importExcel,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                ImportExcelViewControl,
                {})
        },
        // 防疫-全省民政部门重点场所防疫
        {
            path: ROUTE_PATH.OrgDayInfoPrevention,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                OrgDayInfoPreventionViewControl,
                {
                    permission_class: AppServiceUtility.login_service,
                    get_permission_name: 'get_function_list',
                    request_url: 'get_worker_list_all',
                    select_permission: '全省民政部门重点场所防疫查询',
                    add_permission: '全省民政部门重点场所防疫导出',
                })
        },
        {
            path: `${ROUTE_PATH.editOrgDayInfoPrevention}/${KEY_PARAM}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EditOrgDayInfoViewControl,
                {}
            )
        },
        {
            path: `${ROUTE_PATH.editOrgDayInfoPrevention}`,
            mainFormID: layoutMain,
            targetType: 'secure',
            targetObject: createObject(
                EditOrgDayInfoViewControl,
                {}
            )
        },
    ]
);

export let defaultObject = new IntelligentElderlyCareApplication(
    router,
    new AjaxJsonRpcFactory(IUserService, remote.url, "IUserService"),
    new AjaxJsonRpcFactory(ISecurityService, remote.url, 'ISecurityService')
);

export default defaultObject;