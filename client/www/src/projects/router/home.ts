
/*
 * 版权：Copyright (c) 2019 中国
 * 
 * 创建日期：Thursday August 1st 2019
 * 创建者：胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Thursday, 1st August 2019 3:18:19 pm
 * 修改者: 胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 说明
 *    1、首页路由及链接配置
 */
import { createObject } from "pao-aop";
import { HomeViewControl } from "src/projects/views/home";
import { PermissionList } from "../app/permission";
import { ROUTE_PATH, blankID } from "./index";
import { AppRouteObject } from "pao-aop-client";

/**
 * 首页路由
 * @param mainFormID 主窗体
 */
export const homeRouter: AppRouteObject[] = [
    {
        path: ROUTE_PATH.home,
        mainFormID: blankID,
        targetObject: createObject(HomeViewControl)
    }
];

/**
 * 首页链接
 */
export const homeLink = [
    {
        title: "测试", icon: 'notification', link: "", key: 1, permission: "8", childrenComponent: [
            { key: 'home', link: ROUTE_PATH.home, title: '首页', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
        ]
    }
];