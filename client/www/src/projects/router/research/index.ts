import { AppRouteObject } from "pao-aop-client";
import { ROUTE_PATH, layoutMain } from "../index";
import { createObject } from "pao-aop";
import { ResearchManageViewControl } from "src/projects/views/buss_mis/research-manage/research-manage";
import { ResearchInsertViewControl } from "src/projects/views/buss_mis/research-manage/research-insert";
import { ResearchInsertDisabledViewControl } from "src/projects/views/buss_mis/research-manage/research-insert-disabled";
import { ResearchAnalysisViewControl } from "src/projects/views/buss_mis/research-manage/research-analysis";
import { AppServiceUtility } from "src/projects/app/appService";

/**
 * 调研系统路由
 */
export const researchRouter: AppRouteObject[] = [
    {
        path: ROUTE_PATH.researchManage,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            ResearchManageViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_all_task_list',
                select_permission: '调研现场管理查询',
                edit_permission: '调研现场管理编辑',
                add_permission: '调研现场管理新增',
            }
        )
    },
    {
        path: ROUTE_PATH.researchInsert,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(ResearchInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.researchInsertDisabled,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(ResearchInsertDisabledViewControl, {})
    },
    {
        path: ROUTE_PATH.researchAnalysis,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(ResearchAnalysisViewControl, {})
    },
];

/**
 * 调研系统模块链接
 */
export const researchLink = [
    { key: 'researchManage', link: ROUTE_PATH.researchManage, title: '调研现场管理', permission: { 'permission': '调研现场管理查询' }, icon: 'font@pao_font_icon_user_bright' },
    // { key: 'researchInsert', link: ROUTE_PATH.researchInsert, title: '调研任务录入', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
    { key: 'researchInsert', link: ROUTE_PATH.researchAnalysis, title: '调研数据分析', permission: { 'permission': '调研数据分析查询' }, icon: 'font@pao_font_icon_user_bright' },
];