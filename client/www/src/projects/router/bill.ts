import { PermissionList } from "../app/permission";
import { layoutMain, ROUTE_PATH } from "./index";
import { createObject } from "pao-aop";
import { BillSignatureManageViewControl } from "src/projects/views/buss_pub/bill-signature-manage";
import { KEY_PARAM } from "src/business/util_tool";
import { ChangeBillSignatureControl } from "../views/buss_pub/bill-signature-manage/change-bill-signature";
import { InitiatorBillViewControl } from "../views/buss_pub/bill-signature-manage/initiator-bill";
import { AppRouteObject } from "pao-aop-client";
import { AppServiceUtility } from '../app/appService';

/** 单据路由 */
export const billRouter: AppRouteObject[] = [
    {
        path: ROUTE_PATH.billSignature,
        mainFormID: layoutMain,
        targetType: 'secure',
        exact: true,
        targetObject: createObject(
            BillSignatureManageViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_signature_bill_by_personId_all',
                select_permission: '单据签核查看',
                edit_permission: '单据签核编辑',
            }),
    },
    {
        path: `${ROUTE_PATH.changeBillSignature}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            ChangeBillSignatureControl,
            {
                permission: PermissionList.RolePermission_Select,
            }
        )
    },
    {
        path: ROUTE_PATH.initiatorBill,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            InitiatorBillViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_signature_bill_by_initiatorId_look',
                select_permission: '单据查询查看',
            }),
    },
];

/** 单据链接 */
export const billLink = [
    {
        title: "单据管理", icon: 'notification', link: "", key: 6, permission: "8", childrenComponent: [
            { key: 'billSignature', link: ROUTE_PATH.billSignature, title: '单据签核', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' },
            { key: 'initiatorBill', link: ROUTE_PATH.initiatorBill, title: '单据查询', permission: PermissionList.UserManage_Select, icon: 'font@pao_font_icon_user_bright' }
        ]
    }
];