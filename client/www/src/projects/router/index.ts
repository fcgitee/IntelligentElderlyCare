import { CODE_PARAM, KEY_PARAM } from "src/business/util_tool";

/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-02 22:48:24
 * @LastEditTime: 2020-03-02 13:50:03
 * @LastEditors: Please set LastEditors
 */

/*
 * 版权：Copyright (c) 2019 中国
 * 
 * 创建日期：Thursday August 1st 2019
 * 创建者：胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Thursday, 1st August 2019 3:27:04 pm
 * 修改者: 胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 说明
 *    1、
 */

/** 主窗体ID */
export const layoutMain = 'layoutMainForm', headMainID = 'HeadMainForm', blankID = 'blank';

/** 角色Match参数名称 */
export const ROLE_ID = 'role_id';

/** 角色路由参数名称 */
export const ROLE_ID_PARAM = `:${ROLE_ID}`;

/** 用户Match参数名称 */
export const USER_ID = 'user_id';

/** 用户路由参数名称 */
export const USER_ID_PARAM = `:${USER_ID}`;

/** 菜单选中 */
export function selectedKeys(pathname: string) {
    switch (pathname) {
        case ROUTE_PATH.changeAssessmentTemplate:
            return ROUTE_PATH.assessmentTemplate;
        case `${ROUTE_PATH.changeAssessmentTemplate}/${KEY_PARAM}`:
            return ROUTE_PATH.assessmentTemplate;
        case ROUTE_PATH.changeReservationRegistration:
            return ROUTE_PATH.changeReservationRegistration;
        case `${ROUTE_PATH.changeReservationRegistration}/${KEY_PARAM}`:
            return ROUTE_PATH.changeReservationRegistration;
        case ROUTE_PATH.changeAssessmentProject:
            return ROUTE_PATH.assessmentProject;
        case `${ROUTE_PATH.changeAssessmentProject}/${KEY_PARAM}`:
            return ROUTE_PATH.assessmentProject;
        case ROUTE_PATH.competenceAssessment:
            return ROUTE_PATH.competenceAssessmentList;
        case `${ROUTE_PATH.competenceAssessment}/${KEY_PARAM}`:
            return ROUTE_PATH.competenceAssessmentList;
        case ROUTE_PATH.checkInNew:
            return ROUTE_PATH.checkIn;
        case `${ROUTE_PATH.checkInNew}/${KEY_PARAM}`:
            return ROUTE_PATH.checkIn;
        case ROUTE_PATH.changeHydropower:
            return ROUTE_PATH.hydropower;
        case `${ROUTE_PATH.changeHydropower}/${KEY_PARAM}`:
            return ROUTE_PATH.hydropower;
        case ROUTE_PATH.changeLeaveReason:
            return ROUTE_PATH.leaveReason;
        case `${ROUTE_PATH.changeLeaveReason}/${KEY_PARAM}`:
            return ROUTE_PATH.leaveReason;
        case ROUTE_PATH.leaveInsert:
            return ROUTE_PATH.leaveRecord;
        case `${ROUTE_PATH.leaveInsert}/${KEY_PARAM}`:
            return ROUTE_PATH.leaveRecord;
        case ROUTE_PATH.changeHotelZoneType:
            return ROUTE_PATH.hotelZoneType;
        case `${ROUTE_PATH.changeHotelZoneType}/${KEY_PARAM}`:
            return ROUTE_PATH.hotelZoneType;
        case ROUTE_PATH.changeHotelZone:
            return ROUTE_PATH.hotelZone;
        case `${ROUTE_PATH.changeHotelZone}/${KEY_PARAM}`:
            return ROUTE_PATH.hotelZone;
        case ROUTE_PATH.changeBedManage:
            return ROUTE_PATH.bedManage;
        case `${ROUTE_PATH.changeBedManage}/${KEY_PARAM}`:
            return ROUTE_PATH.bedManage;
        // case ROUTE_PATH.changeElderInfo:
        //     return ROUTE_PATH.elderInfo;
        // case `${ROUTE_PATH.changeElderInfo}/${KEY_PARAM}`:
        //     return ROUTE_PATH.elderInfo;
        case ROUTE_PATH.changePersonnel:
            return ROUTE_PATH.personnel;
        case `${ROUTE_PATH.changePersonnel}/${KEY_PARAM}`:
            return ROUTE_PATH.personnel;
        case ROUTE_PATH.changeOrganization:
            return ROUTE_PATH.organization;
        case `${ROUTE_PATH.changeOrganization}/${KEY_PARAM}`:
            return ROUTE_PATH.organization;
        case ROUTE_PATH.changeHappiness:
            return ROUTE_PATH.changeHappiness;
        case `${ROUTE_PATH.changeHappiness}/${KEY_PARAM}`:
            return ROUTE_PATH.changeHappiness;
        case ROUTE_PATH.changePlateform:
            return ROUTE_PATH.changePlateform;
        case `${ROUTE_PATH.changePlateform}/${KEY_PARAM}`:
            return ROUTE_PATH.changePlateform;
        case ROUTE_PATH.changeBusinessArea:
            return ROUTE_PATH.businessArea;
        case `${ROUTE_PATH.changeBusinessArea}/${KEY_PARAM}`:
            return ROUTE_PATH.businessArea;
        case ROUTE_PATH.changeServicesItemCategory:
            return ROUTE_PATH.servicesItemCategory;
        case `${ROUTE_PATH.changeServicesItemCategory}/${KEY_PARAM}`:
            return ROUTE_PATH.servicesItemCategory;
        case ROUTE_PATH.changeServiceProject:
            return ROUTE_PATH.serviceProject;
        case `${ROUTE_PATH.changeServiceProject}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceProject;
        case ROUTE_PATH.changeServiceItemPackage:
            return ROUTE_PATH.serviceItemPackage;
        case `${ROUTE_PATH.changeServiceItemPackage}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceItemPackage;
        case ROUTE_PATH.changeServiceScope:
            return ROUTE_PATH.serviceScope;
        case `${ROUTE_PATH.changeServiceScope}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceScope;
        case ROUTE_PATH.changeServiceOption:
            return ROUTE_PATH.serviceOption;
        case `${ROUTE_PATH.changeServiceOption}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceOption;
        case ROUTE_PATH.changeServiceProvider:
            return ROUTE_PATH.serviceProvider;
        case `${ROUTE_PATH.changeServiceProvider}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceProvider;
        case ROUTE_PATH.changeServicePersonal:
            return ROUTE_PATH.servicePersonalManage;
        case `${ROUTE_PATH.changeServicePersonal}/${KEY_PARAM}`:
            return ROUTE_PATH.servicePersonalManage;
        case ROUTE_PATH.changefinancialAccountBook:
            return ROUTE_PATH.financialAccountBook;
        case `${ROUTE_PATH.changefinancialAccountBook}/${KEY_PARAM}`:
            return ROUTE_PATH.financialAccountBook;
        case ROUTE_PATH.changefinancialAccount:
            return ROUTE_PATH.financialAccountManage;
        case `${ROUTE_PATH.changefinancialAccount}/${KEY_PARAM}`:
            return ROUTE_PATH.financialAccountManage;
        case ROUTE_PATH.changeFinancialVoucher:
            return ROUTE_PATH.financialAccountVoucherManage;
        case `${ROUTE_PATH.changeFinancialVoucher}/${KEY_PARAM}`:
            return ROUTE_PATH.financialAccountVoucherManage;
        case ROUTE_PATH.changeFinancialSubject:
            return ROUTE_PATH.financialSubjectManage;
        case `${ROUTE_PATH.changeFinancialSubject}/${KEY_PARAM}`:
            return ROUTE_PATH.financialSubjectManage;
        case ROUTE_PATH.hospitalArrearsEdit:
            return ROUTE_PATH.hospitalArrears;
        case `${ROUTE_PATH.hospitalArrearsEdit}/${KEY_PARAM}`:
            return ROUTE_PATH.hospitalArrears;
        case `${ROUTE_PATH.changeActivityManage}/${KEY_PARAM}/${CODE_PARAM}`:
            return ROUTE_PATH.activityManage;
        case `${ROUTE_PATH.viewActivityManage}/${KEY_PARAM}`:
            return ROUTE_PATH.activityManageSh;
        case `${ROUTE_PATH.changeActivitySh}/${KEY_PARAM}`:
            return ROUTE_PATH.activityManageSh;
        case `${ROUTE_PATH.changeActivityManage}/${KEY_PARAM}`:
            return ROUTE_PATH.activityManage;
        case ROUTE_PATH.changeActivityManage:
            return ROUTE_PATH.activityManage;
        case ROUTE_PATH.changeActivityTypeManage:
            return ROUTE_PATH.activityTypeManage;
        case `${ROUTE_PATH.changeActivityTypeManage}/${KEY_PARAM}`:
            return ROUTE_PATH.activityTypeManage;
        case `${ROUTE_PATH.changeActivityRoom}/${KEY_PARAM}`:
        case ROUTE_PATH.changeActivityRoom:
            return ROUTE_PATH.activityRoom;
        case ROUTE_PATH.changeTaskType:
            return ROUTE_PATH.taskTypeList;
        case `${ROUTE_PATH.changeTaskType}/${KEY_PARAM}`:
            return ROUTE_PATH.taskTypeList;
        case ROUTE_PATH.changeArticleType:
            return ROUTE_PATH.articleType;
        case `${ROUTE_PATH.changeArticleType}/${KEY_PARAM}`:
            return ROUTE_PATH.articleType;
        case ROUTE_PATH.changeCommentType:
            return ROUTE_PATH.commentType;
        case `${ROUTE_PATH.changeCommentType}/${KEY_PARAM}`:
            return ROUTE_PATH.commentType;
        case ROUTE_PATH.announcementIssue:
            return ROUTE_PATH.announcementList;
        case `${ROUTE_PATH.announcementIssue}/${KEY_PARAM}`:
            return ROUTE_PATH.announcementList;
        case ROUTE_PATH.changeNews:
            return ROUTE_PATH.newsList;
        case `${ROUTE_PATH.changeNews}/${KEY_PARAM}`:
            return ROUTE_PATH.newsList;
        case ROUTE_PATH.changeNewsSh:
            return ROUTE_PATH.newsListSh;
        case `${ROUTE_PATH.changeNewsSh}/${KEY_PARAM}`:
            return ROUTE_PATH.newsListSh;
        case ROUTE_PATH.changeActivityManage:
            return ROUTE_PATH.allowanceManage;
        case `${ROUTE_PATH.changeActivityManage}/${KEY_PARAM}`:
            return ROUTE_PATH.allowanceManage;
        case ROUTE_PATH.allowanceInsert:
            return ROUTE_PATH.allowanceRead;
        case `${ROUTE_PATH.allowanceInsert}/${KEY_PARAM}`:
            return ROUTE_PATH.allowanceRead;
        case ROUTE_PATH.allowanceTemplateInsert:
            return ROUTE_PATH.allowanceTemplate;
        case `${ROUTE_PATH.allowanceTemplateInsert}/${KEY_PARAM}`:
            return ROUTE_PATH.allowanceTemplate;
        case `${ROUTE_PATH.allowanceReviewed}/${KEY_PARAM}`:
            return ROUTE_PATH.allowanceCheck;
        case ROUTE_PATH.changeTask:
            return ROUTE_PATH.researchManage;
        case `${ROUTE_PATH.changeTask}/${KEY_PARAM}`:
            return ROUTE_PATH.researchManage;
        case ROUTE_PATH.userEditor:
            return ROUTE_PATH.userManage;
        case `${ROUTE_PATH.userEditor}/${KEY_PARAM}`:
            return ROUTE_PATH.userManage;
        case ROUTE_PATH.addRole:
            return ROUTE_PATH.role;
        case `${ROUTE_PATH.addRole}/${KEY_PARAM}`:
            return ROUTE_PATH.role;
        case ROUTE_PATH.leaveEdit:
            return ROUTE_PATH.leaveRecord;
        case `${ROUTE_PATH.roomChange}/${KEY_PARAM}`:
            return ROUTE_PATH.roomList;
        case ROUTE_PATH.changeMonitor:
            return ROUTE_PATH.Monitor;
        case ROUTE_PATH.addNursingRecord:
            return ROUTE_PATH.nursingRecord;
        case `${ROUTE_PATH.viewCharitableDonate}/${KEY_PARAM}`:
            return ROUTE_PATH.charitableDonate;
        case ROUTE_PATH.changeCharitableProject:
            return ROUTE_PATH.charitableProject;
        case `${ROUTE_PATH.viewCharitableProject}/${KEY_PARAM}`:
            return ROUTE_PATH.charitableProject;
        case `${ROUTE_PATH.recipientApply}/${KEY_PARAM}`:
            return ROUTE_PATH.recipientApplyList;
        case `${ROUTE_PATH.recipientApply}/${KEY_PARAM}/${CODE_PARAM}`:
            return ROUTE_PATH.viewRecipientApplyList;
        case ROUTE_PATH.changeVolunteerServiceCategory:
            return ROUTE_PATH.volunteerServiceCategory;
        case ROUTE_PATH.volunteerApply:
            return ROUTE_PATH.volunteerApply;
        case `${ROUTE_PATH.changeServiceRecord}/${KEY_PARAM}`:
            return ROUTE_PATH.serviceRecord;
        case `${ROUTE_PATH.changewokerPersonnel}/${KEY_PARAM}`:
            return ROUTE_PATH.wokerPersonnel;
        case `${ROUTE_PATH.changeAdministrationDivision}/${KEY_PARAM}`:
            return ROUTE_PATH.administrationDivision;
        case ROUTE_PATH.changeAdministrationDivision:
            return ROUTE_PATH.administrationDivision;
        case `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}`:
            return ROUTE_PATH.charitableTitleFund;
        case `${ROUTE_PATH.changeCharitableTitleFund}/${KEY_PARAM}/${CODE_PARAM}`:
            return ROUTE_PATH.viewCharitableTitleFund;
        case `${ROUTE_PATH.changeActivityRoomReservation}`:
            return ROUTE_PATH.activityRoomReservation;
        case `${ROUTE_PATH.changeActivityRoomReservation}/${KEY_PARAM}`:
            return ROUTE_PATH.activityRoomReservation;
        case `${ROUTE_PATH.changePersonnelClassification}`:
            return ROUTE_PATH.personnelClassification;
        case `${ROUTE_PATH.changePersonnelClassification}/${KEY_PARAM}`:
            return ROUTE_PATH.personnelClassification;
        case `${ROUTE_PATH.callCenter}`:
        case `${ROUTE_PATH.callCenterLog}`:
            return ROUTE_PATH.callCenter;
        case `${ROUTE_PATH.callCenter}/${KEY_PARAM}`:
            return ROUTE_PATH.callCenter;
        case `${ROUTE_PATH.olderBirthNotify}`:
            return ROUTE_PATH.olderBirthNotify;
        case `${ROUTE_PATH.olderBirthNotify}/${KEY_PARAM}`:
            return ROUTE_PATH.olderBirthNotify;
        case `${ROUTE_PATH.OrgInfoRecord}`:
            return ROUTE_PATH.OrgInfoRecord;
        case `${ROUTE_PATH.OrgInfoRecord}/${KEY_PARAM}`:
            return ROUTE_PATH.OrgInfoRecord;
        case `${ROUTE_PATH.emergencyCall}`:
            return ROUTE_PATH.emergencyCall;
        case `${ROUTE_PATH.knowledgeBase}`:
            return ROUTE_PATH.knowledgeBase;
        case `${ROUTE_PATH.ChargeType}`:
            return ROUTE_PATH.ChargeType;
        case `${ROUTE_PATH.ChargeType}/${KEY_PARAM}`:
            return ROUTE_PATH.ChargeType;
        case `${ROUTE_PATH.ChangeChargeType}`:
            return ROUTE_PATH.ChargeType;
        case `${ROUTE_PATH.ChangeChargeType}/${KEY_PARAM}`:
            return ROUTE_PATH.ChargeType;
        case `${ROUTE_PATH.OperateSituationStatistic}`:
            return ROUTE_PATH.OperateSituationStatistic;
        case `${ROUTE_PATH.changeElderFood}`:
            return ROUTE_PATH.elderFood;
        case `${ROUTE_PATH.changeElderFood}/${KEY_PARAM}`:
            return ROUTE_PATH.elderFood;
        case `${ROUTE_PATH.deduction}`:
            return ROUTE_PATH.elderFood;
        case `${ROUTE_PATH.changeDeduction}/${KEY_PARAM}`:
            return ROUTE_PATH.elderFood;
        case `${ROUTE_PATH.changeSetClass}`:
            return ROUTE_PATH.setClass;
        case `${ROUTE_PATH.changeSetClass}/${KEY_PARAM}`:
            return ROUTE_PATH.setClass;
        case `${ROUTE_PATH.changeSetClassType}`:
            return ROUTE_PATH.setClassType;
        case `${ROUTE_PATH.changeSetClassType}/${KEY_PARAM}`:
            return ROUTE_PATH.setClassType;
        case `${ROUTE_PATH.changeNursingPay}`:
            return ROUTE_PATH.nursingPay;
        case `${ROUTE_PATH.changeNursingPay}/${KEY_PARAM}`:
            return ROUTE_PATH.nursingPay;
        case `${ROUTE_PATH.changeAppPageConfig}`:
            return ROUTE_PATH.appPageConfig;
        case `${ROUTE_PATH.changeAppPageConfig}/${KEY_PARAM}`:
            return ROUTE_PATH.appPageConfig;
        case `${ROUTE_PATH.changeRatingPlan}`:
            return ROUTE_PATH.ratingPlan;
        case `${ROUTE_PATH.changeRatingPlan}/${KEY_PARAM}`:
            return ROUTE_PATH.ratingPlan;
        case `${ROUTE_PATH.changeRating}`:
            return ROUTE_PATH.rating;
        case `${ROUTE_PATH.changeRating}/${KEY_PARAM}`:
            return ROUTE_PATH.rating;
        case `${ROUTE_PATH.changeMonthSalaryStatistic}`:
            return ROUTE_PATH.monthSalaryStatistic;
        case `${ROUTE_PATH.changeMonthSalaryStatistic}/${KEY_PARAM}`:
            return ROUTE_PATH.monthSalaryStatistic;
        case `${ROUTE_PATH.monthSalaryStatistic}`:
            return ROUTE_PATH.monthSalaryStatistic;
        case `${ROUTE_PATH.changeGroups}/${KEY_PARAM}`:
            return ROUTE_PATH.groups;
        case `${ROUTE_PATH.changeGroups}`:
            return ROUTE_PATH.groups;
        case `${ROUTE_PATH.changeServicePlan}/${KEY_PARAM}`:
            return ROUTE_PATH.servicePlan;
        case `${ROUTE_PATH.changeServicePlan}`:
            return ROUTE_PATH.servicePlan;
        case `${ROUTE_PATH.messageListNew}`:
            return ROUTE_PATH.messageListNew;
        case `${ROUTE_PATH.changeRole}/${KEY_PARAM}`:
            return ROUTE_PATH.role;
        case `${ROUTE_PATH.changeRole}`:
            return ROUTE_PATH.role;
        case `${ROUTE_PATH.allowanceProjectInsert}/${KEY_PARAM}`:
            return ROUTE_PATH.allowanceProjectList;
        case `${ROUTE_PATH.allowanceProjectInsert}`:
            return ROUTE_PATH.allowanceProjectList;
        case `${ROUTE_PATH.changeFoodCost}/${KEY_PARAM}`:
            return ROUTE_PATH.foodCostList;
        case `${ROUTE_PATH.changeFoodCost}`:
            return ROUTE_PATH.foodCostList;
        case `${ROUTE_PATH.changeOrganizationServicer}/${KEY_PARAM}`:
            return ROUTE_PATH.foodCostList;
        case `${ROUTE_PATH.changeOrganizationServicer}`:
            return ROUTE_PATH.foodCostList;
        case `${ROUTE_PATH.changeAppUserManage}/${KEY_PARAM}`:
            return ROUTE_PATH.appUserManage;
        case `${ROUTE_PATH.changeAppFeedback}/${KEY_PARAM}`:
            return ROUTE_PATH.appFeedback;
        case `${ROUTE_PATH.changeYearplanAndSummary}`:
            return ROUTE_PATH.yearplanAndSummary;
        case `${ROUTE_PATH.changeYearplanAndSummary}/${KEY_PARAM}`:
            return ROUTE_PATH.yearplanAndSummary;
        case `${ROUTE_PATH.changeDealAndReport}`:
            return ROUTE_PATH.dealAndReport;
        case `${ROUTE_PATH.changeDealAndReport}/${KEY_PARAM}`:
            return ROUTE_PATH.dealAndReport;
        case `${ROUTE_PATH.changeMonthPlan}`:
            return ROUTE_PATH.monthPlan;
        case `${ROUTE_PATH.changeMonthPlan}/${KEY_PARAM}`:
            return ROUTE_PATH.monthPlan;
        case `${ROUTE_PATH.changeAppFriendCircle}/${KEY_PARAM}`:
            return ROUTE_PATH.appFriendCircle;
        case `${ROUTE_PATH.changeOrganizationJG}/${KEY_PARAM}`:
            return ROUTE_PATH.organList;
        case `${ROUTE_PATH.changeOrganizationXYF}/${KEY_PARAM}`:
            return ROUTE_PATH.baseSituation;
        case `${ROUTE_PATH.changeOrganizationFWS}/${KEY_PARAM}`:
            return ROUTE_PATH.servicerList;
        case `${ROUTE_PATH.changeThingSort}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeThingSort}`:
            return ROUTE_PATH.thingSort;
        case `${ROUTE_PATH.changeUnits}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeUnits}`:
            return ROUTE_PATH.Units;
        case `${ROUTE_PATH.changeHappinessStation}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeHappinessStation}`:
            return ROUTE_PATH.happinessStation;
        case `${ROUTE_PATH.changeYearSelfAssessment}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeYearSelfAssessment}`:
            return ROUTE_PATH.yearSelfAssessment;
        case `${ROUTE_PATH.changeHappinessYearSelfAssessment}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeHappinessYearSelfAssessment}`:
            return ROUTE_PATH.happinessYearSelfAssessment;
        case `${ROUTE_PATH.changeFireEquipment}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeFireEquipment}`:
            return ROUTE_PATH.fireEquipment;
        case `${ROUTE_PATH.changeCallCenterZh}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeCallCenterZh}`:
            return ROUTE_PATH.callCenterZh;
        case `${ROUTE_PATH.changeDeviceWarning}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeDeviceWarning}`:
            return ROUTE_PATH.deviceWarning;
        case `${ROUTE_PATH.changeDeviceManage}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeDeviceManage}`:
        case `${ROUTE_PATH.changeDeviceFile}/${KEY_PARAM}`:
            return ROUTE_PATH.deviceManage;
        case `${ROUTE_PATH.chagneActivityParticipate}`:
            return ROUTE_PATH.activityParticipate;
        case `${ROUTE_PATH.changeChildDataDictionary}`:
        case `${ROUTE_PATH.changeChildDataDictionary}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeChildDataDictionary}/${KEY_PARAM}/${CODE_PARAM}`:
        case `${ROUTE_PATH.changeDataDictionary}`:
        case `${ROUTE_PATH.changeDataDictionary}/${KEY_PARAM}`:
            return ROUTE_PATH.dataDictionary;
        case `${ROUTE_PATH.changeProduct}`:
        case `${ROUTE_PATH.changeProduct}/${KEY_PARAM}`:
            return ROUTE_PATH.products;
        case `${ROUTE_PATH.changeProductOrder}`:
        case `${ROUTE_PATH.changeProductOrder}/${KEY_PARAM}`:
            return ROUTE_PATH.productOrders;
        case `${ROUTE_PATH.changeProductSh}/${KEY_PARAM}`:
            return ROUTE_PATH.productsSh;
        case `${ROUTE_PATH.SPEntryRecord}`:
        case `${ROUTE_PATH.SPFinancialDetail}/${KEY_PARAM}`:
            return ROUTE_PATH.SPFinancialOverview;
        case `${ROUTE_PATH.changeBlock}`:
        case `${ROUTE_PATH.changeBlock}/${KEY_PARAM}`:
        case `${ROUTE_PATH.blockDetails}/${KEY_PARAM}`:
        case `${ROUTE_PATH.changeBlockDetails}/${KEY_PARAM}/${CODE_PARAM}`:
        case `${ROUTE_PATH.changeBlockDetails}/${KEY_PARAM}`:
            return ROUTE_PATH.blocks;
        case `${ROUTE_PATH.capitalDetail}`:
            return ROUTE_PATH.PTFinancialOverview;
        case `${ROUTE_PATH.deviceMessage}/${KEY_PARAM}`:
        case `${ROUTE_PATH.deviceLocation}/${KEY_PARAM}`:
            return ROUTE_PATH.guardianshipOperation;
        default:
            return pathname;
    }
}

/** 路由URL */
export const ROUTE_PATH = {
    /** 安全路由 */
    roleUser: '/role-user',
    roleUserLook: '/role-user-look',
    changeRoleUser: '/change-role-user',
    role: '/role-permission',
    roleLook: '/role-permission-look',
    changeRole: '/change-role',
    addRole: '/add-role',
    userManage: '/user-manage',
    userManagePermission: '/user-manage-permission',
    userEditor: '/user-editor',
    login: '/login',
    bindPhone: '/bind-phone',
    modifyfirstPsw: '/modify_first_psw',
    modifyPswAndPhone: '/modify_psw_and_phone',
    modifyLoginPassword: '/modify-login-password',
    modifyMobile: '/modify-mobile',
    modifyEmail: '/modify-email',
    securitySettings: '/security-settings',
    Abnormity: '/abnormity',
    retrievePassword: '/retrieve-password',
    /** 首页 */
    home: '/home',
    /** 护理档案 */
    nursingArchives: '/nursing-archives',
    roomArchives: '/room-archives',   // 房间列表
    roomEdit: '/room-edit',     // 编辑
    /** 护理级别档案编辑 */
    archivesEditor: '/archives-editor',
    /** 护理项目列表 */
    nursingItem: '/nursing-item',
    /** 护理项目表单 */
    nursingItemEdit: '/nursing-item-edit',
    /** 护理类型列表 */
    nursingType: '/nursing-type',
    /** 护理类型详情 */
    nursingTypeEdit: '/nursing-type-edit',
    /** 护理关系 */
    nursingRelationShip: '/nursing-relation-ship',
    /** 护理关系表单 */
    nursingRelationShipEdit: '/nursing-relation-ship-edit',
    /** 预约登记 */
    reservationRegistration: '/reservation-registration',
    /** 预约登记查看 */
    reservationRegistrationLook: '/reservation-registration-look',
    /** 预约登记查看 */
    reservationRegistrationLook1: '/reservation-registration-look1',
    /** 编辑预约登记 */
    reservationRegistrationEditor: '/reservation-registration-editor',
    // 人员类别
    personnelClassification: '/personnel-classification',
    // 人员类别
    changePersonnelClassification: '/change-personnel-classification',
    /** 人员清单列表 */
    personnel: '/personnel',
    personnel1: '/personnel1',
    personnel2: '/personnel2',
    /** 人员清单列表 志愿者 */
    personnel_zyz: '/personnel_zyz',
    /** 人员清单列表 义工 */
    personnel_yg: '/personnel_yg',
    /** 人员清单列表 义工 tow */
    personnel_ygtow: '/personnel_yg_tow',
    /** 工作人员管理 */
    wokerPersonnel: '/worker-personnel',
    wokerPersonnelHappiness: '/worker-personnel-happiness',
    wokerPersonnelProvider: '/worker-personnel-provider',
    wokerPersonnel1: '/worker-personnel1',
    wokerPersonnelCivil: '/worker-personnel-civil', // 民政
    wokerPersonnelPlatform: '/worker-personnel-platform',
    /** 人员编辑 */
    changePersonnel: '/change-personnel',
    /** 修改工作人员 */
    changewokerPersonnel: '/change-worker-personnel',
    /** 修改工作人员 */
    changewokerPersonnelXfy: '/change-worker-personnel-xfy',
    /** 修改工作人员 */
    changewokerPersonnelFws: '/change-worker-personnel-fws',
    /** 机构服务人员 */
    jigouWorkerEntry: '/jigouWorker-entry',
    /** 组织机构 */
    organization: '/organization',
    organization2: '/organization2',
    organization3: '/organization3',
    organization4: '/organization4',
    organization5: '/organization5',
    organization6: '/organization6',
    organization7: '/organization7',
    // 服务商
    organizationServicer: '/organization-servicer',
    // 编辑服务商
    changeOrganizationServicer: '/change-servicer',
    // 幸福院
    organization_xfy: '/organization_xfy',
    /** 组织机构查看 */
    organizationLook: '/organization-look',
    /** 组织机构编辑 */
    changeOrganization: '/change-organization',
    /** 幸福院编辑 */
    changeHappiness: '/change-happiness',
    /** 平台运营信息编辑 */
    changePlateform: '/change-plateform',
    /** 行为能力评估项目 */
    behavioralCompetenceAssessment: '/behavioral-competence-assessment',
    /** 行为能力评估项目编辑 */
    changeBehavioralCompetenceAssessment: '/change-behavioral-competence-assessment',
    /** 单据审批列表 */
    billSignature: '/bill-signature',
    /** 单据审批编辑 */
    changeBillSignature: '/change-bill-signature',
    /** 查询发起单据列表 */
    initiatorBill: '/initiator-bill',
    /** 交易管理列表 */
    transaction: '/transaction',
    /** 交易管理编辑 */
    changeTransaction: '/change-transaction',
    /** 信用记录列表 */
    transactionComment: '/transaction-comment',
    /** 业务区域编辑 */
    changeBusinessArea: '/change-business-area',
    /** 业务区域管理列表 */
    businessArea: '/business-area',
    /** 行政区划编辑 */
    changeAdministrationDivision: '/change-administration-division',
    /** 行政区划管理列表 */
    administrationDivision: '/administration-division',
    /** 行政区划管理列表查看 */
    administrationDivisionLook: '/administration-division-look',

    /** 服务商登记编辑 */
    changeServiceProvider: '/change-service-provider',
    /** 机构信息档案 */
    ChangeOrgInfoRecore: '/change-org-info-recore',
    /** 收费类型 */
    ChangeChargeType: '/change-charge-type',
    /** 收费类型 */
    ChargeType: '/charge-type',
    /** 服务商登记管理列表 */
    serviceProvider: '/service-provider',
    /** 服务商申请页面 */
    serviceProviderApply: '/service-provider-apply',
    /** 服务商审核页面 */
    ProviderApplyReviewed: '/service-provider-reviewed',
    /** 服务类型编辑 */
    changeServiceType: '/change-service-type',
    /** 服务类型管理列表 */
    serviceType: '/service-type',
    /** 服务项目登记编辑 */
    changeServiceItem: '/change-service-item',
    /** 服务项目登记列表 */
    serviceItem: '/service-item',
    /** 评估模板列表 */
    assessmentTemplate: '/assessment-template',
    /** 评估模板列表查看 */
    assessmentTemplateLook: '/assessment-template-look',
    /** 评估模板编辑 */
    changeAssessmentTemplate: '/change-assessment-template',
    /** 评估模板列表 */
    assessmentProject: '/assessment-project',
    /** 评估模板列表查看 */
    assessmentProjectLook: '/assessment-project-look',
    /** 评估模板编辑 */
    changeAssessmentProject: '/change-assessment-project',
    /** 服务订单列表 */
    serviceOrder: '/service-order',
    serviceOrder1: '/service-order1',
    /** 服务订单分派 */
    serviceOrderTask: '/service-order-task',
    /** 服务订单分派查看 */
    serviceOrderTaskLook: '/service-order-task-look',
    /** 服务订单分派任务查看 */
    taskServiceOrder: '/task-service-order',
    /** 服务订单分派任务编辑 */
    changeTaskServiceOrder: '/change-task-service-order',
    /** 服务订单新增 */
    changeServiceOrder: '/change-service-order',
    /** 能力评估列表 */
    competenceAssessmentList: '/competence-assessment-list',
    /** 能力评估列表查看 */
    competenceAssessmentListLook: '/competence-assessment-list-look',
    /** 能力评估 */
    competenceAssessment: '/competence-assessment',
    /** 财务账户流水列表 */
    financialAccountFlow: '/financial-account',
    /** 财务账户收款页面 */
    changereceive: '/financial-account-receive',
    /** 财务账户付款页面 */
    changepayment: '/financial-account-payment',
    /** 财务账户转账页面 */
    changetransfer: '/financial-account-transfer',
    /** 财务账户红冲页面 */
    changeFinancialReturn: '/financial-account-return',
    /** 财务账薄管理列表 */
    financialAccountBook: '/financial-account-book',
    /** 财务账户管理列表 */
    financialAccountManage: '/financial-account-manage',
    /** 财务账簿编辑 */
    changefinancialAccountBook: '/change-financial-account-book',
    /** 财务账户编辑 */
    changefinancialAccount: '/change-financial-account',
    /** 记账凭证明细账列表 */
    financialAccountVoucherManage: '/financial-account-voucher-manage',
    /** 记账凭证编辑 */
    changeFinancialVoucher: '/change-account-voucher',
    /** 会计科目管理列表 */
    financialSubjectManage: '/financial-subject-manage',
    /** 会计科目编辑列表 */
    changeFinancialSubject: '/change-financial-subject-manage',
    /** 长者资料 */
    elderInfo: '/elder-info',
    /** 长者档案 */
    elderInfoPT: '/elder-info-pt',
    monthSalaryStatistic: '/month-salary-statistic',
    changeMonthSalaryStatistic: '/change-month-salary-statistic',
    olderBirthNotify: '/older-birth-notify',
    // 机构信息档案
    OrgInfoRecord: '/org-info-record',
    // 运营情况统计
    OperateSituationStatistic: '/operate-situation-statistic',
    elderInfo1: '/elder-info1',
    elderInfo2: '/elder-info2',
    elderInfo3: '/elder-info3',
    elderInfo4: '/elder-info4',
    elderInfo5: '/elder-info5',
    /** 长者资料 */
    elderInfoLook: '/elder-info-look',
    /** 有机构长者资料 */
    orgElderInfo: '/org-elder-info',
    /** 编辑长者资料 */
    changeElderInfo: '/change-elder-info',
    /** 服务项目 */
    serviceProject: '/service-project',
    /** 服务项目查看 */
    serviceProjectLook: '/service-project-look',
    /** 编辑服务项目 */
    changeServiceProject: '/change-service-project',
    /** 服务项目类别 */
    servicesItemCategory: '/services-item-category',
    /** 服务项目类别查看 */
    servicesItemCategoryLook: '/services-item-category-look',
    /** 编辑服务项目类别 */
    changeServicesItemCategory: '/change-services-item-category',
    /** 床位管理 */
    bedManage: '/bed-manage',
    /** 床位管理查看 */
    bedManageLook: '/bed-manage-look',
    /** 编辑床位 */
    changeBedManage: '/change-bed-manage',
    /** 水电列表 */
    hydropower: '/hydropower-list',
    hydropowerLook: '/hydropower-list-look',
    changeHydropower: '/change-hydropower',
    waterElectricity: '/water-electricity',
    otherCost: '/other-cost',
    costType: '/cost-type',
    hydropowerJj: '/hydropower-jiujiang-list',
    hydropowerJjLook: '/hydropower-jiujiang-list-look',
    changeHydropowerJj: '/change-hydropower-jiujiang',
    /** 住宿区域类型 */
    hotelZoneType: '/home-zone-type', // 列表
    changeHotelZoneType: '/change-home-zone-type', // 编辑
    /** 住宿区域 */
    hotelZone: '/home-zone', // 列表
    /** 住宿区域查看 */
    hotelZoneLook: '/home-zone-look',
    changeHotelZone: '/change-home-zone', // 编辑
    /** 房态图 */
    roomStatus: '/room-status', // 列表
    /** 入住--双鸭山文档 */
    roomStatusexcel: '/room-status-all',
    /** 入住--双鸭山文档 */
    roomStatusexceltow: '/room-status-all-tow',
    /** 入住--双鸭山文档 */
    roomStatusexcelthree: '/room-status-all-three',
    /** 需求类型 */
    requirementType: '/requirement-type', // 列表
    changeRequirementType: '/change-requirement-type', // 编辑

    /** 需求项目 */
    requirementProject: '/requirement-project', // 列表
    changeRequirementProject: '/change-requirement-project', // 编辑

    /** 需求项目选项 */
    requirementOption: '/requirement-option', // 列表
    changeRequirementOption: '/change-requirement-option', // 编辑

    /** 服务包 */
    serviceItemPackage: '/service-item-package', // 列表
    serviceItemPackage1: '/service-item-package1', // 列表
    // 服务产品审核
    serviceItemPackageSh: '/service-item-package-sh',
    /** 服务包查看 */
    serviceItemPackageLook: '/service-item-package-look',
    /** 服务包 */
    changeServiceItemPackage: '/change-service-item-package', // 编辑
    changeServiceItemPackageSh: '/change-service-item-package-sh', // 审核编辑
    /** 服务适用范围 */
    serviceScope: '/service-scope',
    /** 服务适用范围查看 */
    serviceScopeLook: '/service-scope-look',
    /** 编辑服务适用范围 */
    changeServiceScope: '/change-service-scope',
    /** 离开原因编辑页面 */
    changeLeaveReason: '/change-leave-reason',
    /** 离开原因列表 */
    leaveReason: '/leave-reason',
    /** 请假列表 */
    leaveInsert: '/leave-insert',
    /** 销假列表 */
    leaveEdit: '/leave-edit',
    /** 请假销假管理列表 */
    leaveRecord: '/leave-Record',
    /** 请假销假管理列表查看 */
    leaveRecordLook: '/leave-Record-look',
    /** 服务提供者登记表单 */
    ChangeServiceWoker: '/change-service-woker',
    /** 服务提供者列表 */
    serviceWoker: '/service-woker',
    /** 服务选项 */
    serviceOption: '/service-option',
    /** 服务选项查看 */
    serviceOptionLook: '/service-option-look',
    /** 编辑服务选项 */
    changeServiceOption: '/change-service-option',
    /** 入住长者列表 */
    checkIn: '/check-in-list',
    /** 入住列表-九江 */
    checkInJj: '/check-in-list-special',
    checkInNewJj: '/check-in-index-special',
    checkInJjLook: '/check-in-list-special-look',
    /** 入住长者列表查看 */
    checkInLook: '/check-in-list-look',
    /** 入住长者详情 */
    checkInDetail: '/check-in-detail',
    /** 入住登记新建 */
    checkInNew: '/check-in-index',
    /** 服务记录列表 */
    serviceRecord: '/service-record',
    /** 服务商服务记录列表 */
    serviceRecordProvider: '/service-record-provider',
    /** 服务记录列表查看 */
    serviceRecordLook: '/service-record-look',
    /** 服务记录编辑页面 */
    changeServiceRecord: '/change-service-record',
    /** 台账列表 */
    serviceLedger: '/service-ledger',
    /** 编辑台账页面 */
    changeServiceLedger: '/change-service-ledger',
    /** 预约登记步骤 */
    changeReservationRegistration: '/change-reservation-registration',
    /** 需求模板列表 */
    requirementTemplate: '/requirement-template',
    /** 需求模板新增 */
    changeRequirementTemplate: '/change-requirement-template',
    /** 收费员收款页面 */
    changeCollectorCharge: '/change-collector-charge',
    /** 收费员收款页面查看 */
    changeCollectorChargeLook: '/change-collector-charge-look',
    /** 收费员退款页面 */
    changeCollectorPayment: '/change-collector-payment',
    /** 查看全部收费记录 */
    financialChargeRecord: '/financial-charge-record',
    /** 查看收费记录 */
    financialChargeRecordLook: '/financial-charge-record',
    /** 查看银行扣款记录 */
    financialReconciliationRecord: '/financial-reconciliation-record-manage',
    financialReconciliationRecordLook: '/financial-reconciliation-record-look',
    /** 打印/导出银行扣款单 */
    financialBankReconciliation: '/financial-bank-reconciliation',
    financialBankReconciliationLook: '/financial-bank-reconciliation-look',
    /** 打印/导出银行扣款单 */
    financialBankReconciliationManage: '/financial-bank-reconciliation-manage',
    /** 需求记录列表 */
    requirementRecord: '/requirement-record',
    /** 需求记录新增 */
    changeRequirementRecord: '/change-requirement-record',
    /**  医疗欠费列表 */
    hospitalArrears: '/hospital-arrears-list',
    hospitalArrearsEdit: '/hospital-arrears-edit',
    /** 创建|查看任务 */
    changeTask: '/change-task',
    /** 所有任务列表 */
    taskList: '/task-list',
    /** 所有任务列表 */
    taskListLook: '/task-list-look',
    /** 任务类型列表 */
    taskTypeList: '/task-type-list',
    /** 任务类型列表查看 */
    taskTypeListLook: '/task-type-list-look',
    /** 修改任务类型 */
    changeTaskType: '/change-task-type',
    /** 未分派任务列表 */
    taskToBeSend: '/task-to-be-send',
    /** 未审核任务列表 */
    taskToBeProcess: '/task-to-be-process',
    /** 所有消息列表 */
    messageList: '/message-list',
    /** 创建消息列表 */
    changeMessage: '/change-message',
    /** 所有消息类型列表 */
    messageTypeList: '/message-type-list',
    /** 创建消息列表 */
    changeMessageType: '/change-message-type',
    /** 公告发布 */
    announcementIssue: '/announcement_issue',
    /** 公告列表 */
    announcementList: '/announcement_list',
    /** 公告列表查看 */
    announcementListLook: '/announcement_list-look',
    /** 新闻列表 */
    newsList: '/news_list',
    newsListWelfare: '/news_list-welfare',
    newsListHappiness: '/news_list-happiness',
    newsListProvider: '/news_list-provider',
    /** 新闻列表 */
    newsListSh: '/news_list_sh',
    /** 新闻列表 */
    newsListLook: '/news_list-look',
    /** 新闻新增编辑 */
    changeNews: '/change-news',
    // 审核新闻
    changeNewsSh: '/change-news-sh',
    /** 新闻新增编辑 */
    viewNews: '/view-news',
    /** 评论列表 */
    commentList: '/comment-list',
    /** 评论列表查看 */
    commentListLook: '/comment-list-look',
    /** 新增评论 */
    changeComment: '/change-comment',
    /** 评论审核列表 */
    commentAudit: '/comment-audit',
    commentAuditWelfare: '/comment-audit-welfare',
    commentAuditHappiness: '/comment-audit-happiness',
    commentAuditProvider: '/comment-audit-provider',
    /** 评论审核列表查看 */
    commentAuditLook: '/comment-audit-look',
    /** 评论审核 */
    changeCommentAudit: '/change-comment-audit',
    /** 注册 */
    register: '/register',
    /** 活动类型 */
    activityTypeManage: '/activity-type-manage',
    /** 活动类型查看 */
    activityTypeManageLook: '/activity-type-manage-look',
    /** 活动类型新增页面 */
    changeActivityTypeManage: '/change-activity-type',
    /** 活动管理列表 */
    activityManage: '/activity-manage',
    /** 活动审核列表 */
    activityManageSh: '/activity-manage-sh',
    /** 活动推送列表 */
    activityManageTs: '/activity-manage-ts',
    /** 活动管理列表查看 */
    activityManageLook: '/activity-manage-look',
    /** 活动新增页面 */
    changeActivityManage: '/change-activity',
    // 活动审核
    changeActivitySh: '/change-activity-sh',
    /** 活动查看页面 */
    viewActivityManage: '/view-activity',
    /** 活动参与列表 */
    chagneActivityParticipate: '/change-activity-participate',
    /** 活动参与列表 */
    activityParticipate: '/activity-participate',
    /** 活动参与列表查看 */
    activityParticipateLook: '/activity-participate-look',
    /** 活动签到列表 */
    activitySignIn: '/activity-sign-in',
    /** 活动签到列表查看 */
    activitySignInLook: '/activity-sign-in-look',
    /** 活动室列表 */
    activityRoom: '/activity-room',
    /** 活动室列表查看 */
    activityRoomLook: '/activity-room-look',
    /** 活动室新增页面 */
    changeActivityRoom: '/change-activity-room',
    // 活动室预约
    changeActivityRoomReservation: '/change-activity-room-reservation',
    /** 活动室预约列表 */
    activityRoomReservation: '/activity-room-reservation',
    /** 活动室预约列表 */
    activityRoomReservationRecord: '/activity-room-reservation-record',
    /** 服务人员登记列表 */
    servicePersonalManage: '/service-personal',
    /** 服务人员新增页面 */
    changeServicePersonal: '/change-service-personal',
    /** 服务人员申请页面 */
    servicePersonalApply: '/service-personal-apply',
    /** 服务商结算列表页面 */
    serviceProviderSettlement: '/service-provider-settlement',
    /** 服务商结算列表页面查看 */
    serviceProviderSettlementLook: '/service-provider-settlement-look',
    serviceProviderSettlementLook1: '/service-provider-settlement-look1',
    /** 服务商结算新增页面 */
    changeServiceProviderSettlement: '/change-service-provider-settlement',
    /** 服务人员结算列表页面 */
    servicePersonalSettlement: '/service-personal-settlement',
    /** 服务人员结算列表页面查看 */
    servicePersonalSettlementLook: '/service-personal-settlement-look',
    /** 服务人员结算新增页面 */
    changeServicePersonalSettlement: '/change-service-personal-settlement',
    /** 评论页面 */
    comment: '/comment',
    /** 文章类型列表页面 */
    articleType: '/article-type',
    /** 文章类型列表页面查看 */
    articleTypeLook: '/article-type-look',
    /** 文章类型新增页面 */
    changeArticleType: '/change-article-type',
    /*补贴信息填写页面 */
    allowanceInsert: '/allowance-insert',
    /*补贴模板填写页面 */
    allowanceTemplateInsert: '/allowance-template-insert',
    /*补贴信息管理页面 */
    allowanceManage: '/allowance-manage',
    /*补贴状态查看页面 */
    allowanceRead: '/allowance-read',
    /*补贴模板管理页面 */
    allowanceTemplate: '/allowance-template',
    /*长者补贴项目列表页面 */
    allowanceProjectList: '/allowance-project-list',
    /*长者补贴项目录入页面 */
    allowanceProjectInsert: '/allowance-project-insert',
    /*高龄补贴审核查看 */
    oldallowanceCheck: '/old-allowance-check',
    /*高龄津贴未认证列表 */
    oldallowanceNoCheck: '/old-allowance-no-check',
    disabledPersonCheck: '/disabled-person-check',
    /*高龄补贴审核 */
    oldallowanceReviewed: '/old-allowance-reviewed',
    /** 高龄津贴名单查询页面 */
    oldAgeList: '/old-age-list',
    disabledPerson: '/disabled-person',
    /** 高龄津贴统计报表页面 */
    oldAgeStatistics: '/old-age-statistics',
    disabledPersonStatistics: '/disabled-person-statistics',

    /*成功页面 */
    allowanceSuccess: '/allowance-success',
    /*已申请查看页面 */
    allowanceCheck: '/allowance-check',
    allowanceOldCheck: '/allowance-old-check',
    /*补贴申请审核页面 */
    allowanceReviewed: '/allowance-reviewed',
    /*调研现场管理 */
    researchManage: '/research-manage',
    /*调研数据录入 */
    researchInsert: '/research-insert',
    /*残疾人调研数据录入 */
    researchInsertDisabled: '/research-insert-disabled',
    /*调研数据分析 */
    researchAnalysis: '/research-analysis',
    /** 评论类型列表页面 */
    commentType: '/comment-type',
    /** 评论类型列表页面查看 */
    commentTypeLook: '/comment-type-look',
    /** 评论类型新增页面 */
    changeCommentType: '/change-comment-type',
    /** 费用核算列表 */
    costAccounting: '/costAccounting-list',
    costAccountingLook: '/costAccounting-list-look',
    /** 新增/编辑费用核算 */
    costAccountingEdit: '/costAccounting-edit',
    /** 文章审核列表页面 */
    articleAudit: '/article-audit',
    /** 文章审核列表页面查看 */
    articleAuditLook: '/article-audit-look',
    /** 文章审核页面 */
    changeArticleAudit: '/change-article-audit',
    /** 服务套餐页面 */
    serviceProjectindex: '/service-project-index',
    /** 服务套餐详情页 */
    ServiceProjectDetail: '/service-project-detail',
    /** 服务套餐购买页 */
    ServiceProjectBuy: '/service-project-buy',
    /** 设备列表页 */
    Device: '/device',
    /** 设备列表页-智能设备 */
    XxhDevice: '/Xxhdevice',
    /** 设备编辑 */
    ChangeDevice: '/change-device',
    /** 设备列表页查看 */
    DeviceLook: '/device-look',
    /** 设备日志页 */
    DeviceLog: '/device-log',
    /** 社会群体类型管理 */
    socialGroupsType: '/social-groups-type',
    /** 社会群体类型管理查看 */
    socialGroupsTypeLook: '/social-groups-type-look',
    /** 社会群体类型新增 */
    changeSocialGroupsType: '/change-social-groups-type',
    /** 监控列表 */
    Monitor: '/monitor',
    /** 监控列表--查看 */
    MonitorLook: '/monitor-look',
    /** 监控编辑 */
    changeMonitor: '/change-monitor',
    /** 已入住房间列表 */
    roomList: '/room-list',
    /** 费用调整 */
    feeList: '/fee-list',
    changeFee: '/change-fee',
    /** 房间调整 */
    roomChange: '/room-change',
    /** 杂费管理 */
    incidentalList: '/incidental',
    incidentalListLook: '/incidental-look',
    /** 新增杂费 */
    changeIncidental: '/change-incidental',
    /** 餐费管理 */
    foodCostList: '/food-cost',
    foodCostListLook: '/food-cost-look',
    /** 新增餐费 */
    changeFoodCost: '/change-food-cost',
    /** 医疗费用 */
    medicalCost: '/medical-cost',
    /** 新增医疗费用 */
    changeMedicalCost: '/change-medical-cost',
    /** 尿片费用 */
    diaperCost: '/diaper-cost',
    /** 新增尿片费用 */
    changeDiaperCost: '/change-diaper-cost',
    /** 护理管理模块 */
    /** 护理内容列表 */
    nursingContent: '/nursing-content',
    /** 长者护理记录列表 */
    elderNursingRecord: '/elder-nursing-record',
    /** 新增长者护理记录列表 */
    addElderNursingRecord: '/add-elder-nursing-record',
    /** 护理内容列表 */
    assessmentProjectNursing: '/assessment-project/护理',
    assessmentProjectNursingLook: '/assessment-project-look/护理',
    /** 新增/修改护理内容 */
    addNursingContent: '/add-nursing-content',
    /** 护理模板 */
    nursingTemplate: '/assessment-template/护理',
    /** 护理模板查看 */
    assessmentTemplateLookNursing: '/assessment-template-look/护理',
    /** 新增/修改/护理模板 */
    addNursingTemplate: '/add-nursing-template',
    /** 护理记录列表页 */
    nursingRecord: '/competence-assessment-list/护理',
    /** 新增/修改护理记录 */
    addNursingRecord: '/competence-assessment/护理',
    // 慈善项目列表
    charitableProject: '/charitable-project',
    // 查看慈善项目
    viewCharitableProject: '/view-charitable-project',
    // 选择慈善项目列表
    charitableProjectList: '/charitable-project-list',
    // 慈善项目新增编辑
    changeCharitableProject: '/change-charitable-project',
    // 慈善捐赠列表
    charitableDonate: '/charitable-donate',
    // 慈善拨款列表
    charitableAppropriation: '/charitable-appropriation',
    // 审核慈善拨款
    changeCharitableAppropriation: '/change-charitable-appropriation',
    // 慈善项目捐赠
    changeCharitableDonate: '/change-charitable-donate',
    // 查看慈善项目捐赠
    viewCharitableDonate: '/view-charitable-donate',
    // 志愿者服务类别列表
    volunteerServiceCategory: '/volunteer-service-category',
    // 慈善项目捐赠
    changeVolunteerServiceCategory: '/change-volunteer-service-category',
    // 慈善汇总信息
    charitableInformation: '/charitable-information',
    // 志愿者申请列表
    volunteerApply: '/volunteer-apply',
    // 志愿者申请
    volunteerApplyList: '/volunteer-apply-list',
    // 个人募捐申请列表
    recipientApply: '/recipient-apply',
    // 个人募捐申请列表
    viewRecipientApplyList: '/view-recipient-apply',
    // 个人募捐申请
    recipientApplyList: '/recipient-apply-list',
    // 机构募捐申请列表
    organizationApply: '/organization-apply',
    // 机构募捐申请
    organizationApplyList: '/organization-apply-list',
    /** 机构运营情况 */
    operationStatus: '/operation-status',
    /** 幸福院运营情况 */
    operationHappiness: '/operation-happiness',
    /** 服务商基本情况统计 */
    servicerList: '/servicer-list',
    /** 机构养老基本情况统计 */
    organList: '/organ-list',
    /** 机构运营统计情况 */
    organOperation: '/organ-opeartion',
    // 幸福院验收及评级情况统计
    acceptancRating: '/acceptanc-rating',
    /** 基本设施情况 */
    basicFacilities: '/basic-facilities',
    /** 收养人员情况 */
    adoptionSituation: '/adoption-situation',
    /** 消防资质 */
    fireQualification: '/fire-qualification',
    /** 消防资质 --双鸭山 */
    fireQualification2: '/fire-qualification-tow',
    /** 消防资质 --双鸭山 */
    fireQualification3: '/fire-qualification-three',
    /** 消防资质 --双鸭山 */
    fireQualification4: '/fire-qualification-four',
    /** 消防资质 --双鸭山 */
    fireQualification5: '/fire-qualification-five',
    /** 消防资质 --双鸭山 */
    fireQualification6: '/fire-qualification-six',
    /** 消防资质 --双鸭山 */
    fireQualification7: '/fire-qualification-serven',
    /** 消防资质 --双鸭山 */
    fireQualification8: '/fire-qualification-eig',
    /** 消防资质 --双鸭山 */
    fireQualification9: '/fire-qualification-ning',
    /** 消防资质 --双鸭山 */
    fireQualification10: '/fire-qualification-tine',
    /** 消防资质 --双鸭山 */
    fireQualification11: '/fire-qualification-eleone',
    /** 日托订单 */
    fireQualification_rtdd: '/fire-qualification_rtdd',
    /** 日托支付 */
    fireQualification_rtzf: '/fire-qualification_rtzf',
    /** 活动报名统计 */
    fireQualification_hdbm: '/fire-qualification_hdbm',
    /** 年龄统计 */
    fireQualification_nntj: '/fire-qualification_nntj',
    /** 年龄统计 */
    fireQualification_zzsj: '/fire-qualification_zzsj',
    /** 年龄统计 */
    fireQualification_zzbb: '/fire-qualification_zzbb',
    /** 月收入统计 */
    fireQualification_ysr: '/fire-qualification_ysr',
    /** 活动报告统计 */
    fireQualification_hdbg: '/fire-qualification_hdbg',
    /** 职工构成情况 */
    workerConstitute: '/worker-constitute',
    /** 薪酬待遇情况 */
    compensation: '/compensation',
    /** 机构报表情况 */
    organizationReport: '/organization-reportView',
    /** 福利院--工作人员统计--报表 */
    staffStatistics: '/staff-statistics',
    /** 幸福院--工作人员统计--报表 */
    happyStaffStatistics: '/happy-staff-statistics',
    /** 入住老人情况统计--报表 */
    elderlyResidents: '/elderly-residents',
    // 活动信息汇总
    activityInformation: '/activity-information',
    // 平台结算报表
    platformSettlement: '/platform-settlement',
    // 服务商结算报表
    serviceProviderSettlements: '/service-provider-settlements',
    // 服务人员结算报表
    servicePersonnelSettlement: '/service-personnel-settlement',
    // 慈善结算报表
    charitySettlement: '/charity-settlement',
    // 慈善会资料
    charitableSociety: '/charitable-society',
    // 冠名基金列表
    charitableTitleFund: '/charitable-title-fund',
    // 冠名基金申请记录
    viewCharitableTitleFund: '/view-charitable-title-fund-list',
    // 增加编辑冠名基金列表
    changeCharitableTitleFund: '/change-charitable-title-fund',
    /** 福利院长者入住统计 */
    elderCheck: '/elder-check',
    // 服务分摊报表
    incomeAssessedSettlement: '/income-assessed-settlement',
    incomeAssessedSettlement1: '/income-assessed-settlement1',
    // 修改代付账户顺序
    changeSquence: '/change-squence',
    // 服务商补贴收入
    subsidyServiceProvider: '/subsidy-service-provider',
    // 补贴账户自动充值控制页面
    subsidyAutoRecharge: '/subsidy-auto-recharge',
    // 服务人员申请审核页面
    ServicePersonApplyReviewed: '/service-apply-reviewed',
    // 非银行扣款单
    unfinancialBankReconciliation: '/unfinancial-bank-reconciliation',
    // 帮长者购买服务
    buyService: '/buy-service',
    unfinancialBankReconciliationLook: '/unfinancial-bank-reconciliation-look',
    announcement: '/announcement-all',
    announcementLook: '/announcement-look',
    changeAnnouncement: '/change-announcement',
    // 双鸭山报表
    sysbb: '/records',
    sysbb1: '/records1',
    sysbb2: '/records2',
    sysbb3: '/records3',
    // 呼叫中心
    callCenter: '/call-center',
    // 南海指挥中心
    callCenterNh: '/call-center-nh',
    callCenterZh: '/call-center-zh',
    callCenterLog: '/call-center-log',
    changeCallCenterZh: '/change-call-center-zh',
    // 知识库
    knowledgeBase: '/knowledge-base',
    // 知识库详情
    knowledgeDetails: '/knowledge-details',
    // 知识库录入
    knowledgeEdit: '/knowledge-edit',
    // 紧急呼叫
    emergencyCall: '/emergency-call',
    // 紧急呼叫详情
    emergencyCallDetails: '/emergency-call-details',
    // 服务预定列表
    reserveBuyService: '/reserveBuyService',
    // 服务预约编辑
    reserveBuyServiceEdit: '/reserve-buy-service-edit',
    // 平台查看服务结算报表
    serviceSettlementStatement: '/service-settlement-statement',
    // 居家养老服务对象月报表
    allowanceReportMonth: '/allowance-report-month',
    // 居家养老服务对象明细表
    allowanceReportDetails: '/allowance-report-details',
    // 工作人员统计
    workPeopleStatistics: '/work-people-statistics',
    // 社工局--镇街结算报表
    // 服务商结算报表
    townStreetSettlement: '/town-street-settlement',
    // 服务商服务记录明细统计
    serviceProviderRecordSettlement: '/service-provider-record-settlement',
    // 账户充值
    accountList: '/account-list',
    // 账户明细
    accountDetails: '/account-details',
    // 基本情况统计
    baseSituation: '/base-situation',
    orderReport: '/order-report',
    dataImport: '/data-import',
    // 慈善账户
    charitableAccount: '/ charitable-account',
    // 慈善账户明细
    charitableAccountDetails: '/charitable-account-details',
    // 公众号关注数统计
    xfyFollowStatistics: '/xfy-follow-statistics',
    // 居家服务-订单全景
    orderAllJ: '/order-allJ',
    // 居家服务-订单统计
    orderStatistics: '/order-statistics',
    // 物料管理-计量单位
    Units: '/units',
    changeUnits: '/change-units',
    // 物料管理-物料分类
    thingSort: '/thing-sort',
    changeThingSort: '/change-thing-sort',
    // 物料管理-物料档案
    thingArchives: '/thing-archives',
    // 物料管理-物料档案新增
    changeThingArchives: '/change-thing-archives',
    // 健康照护-药品档案
    medicineFile: '/medicine-file',
    // 健康照护-药品档案新增
    addMedicineFile: '/add-medicine-file',
    // 健康照护-疾病档案
    diseaseFile: '/disease-file',
    // 健康照护-疾病档案新增
    addDiseaseFile: '/add-disease-file',
    // 健康照护-过敏档案
    allergyFile: '/allergy-file',
    // 健康照护-过敏档案新增
    addAllergyFile: '/add-allergy-file',
    // 费用管理-app工单核算
    orderAccounting: '/order-accounting',
    // 服务评价
    serviceEvaluate: '/service-evaluate',
    // 服务回访
    serviceReturnVisit: '/service-return-visit',
    // 费用管理-费用统计
    costStatistics: '/cost-statistics',
    // 日托服务-日托订单列表
    dayCareOrderList: '/daycare-orderlist',
    // 日托服务-日托订单统计
    dayCareOrderStatistics: '/daycare-orderStatistics',
    // 统计管理-验收管理列表
    checkManage: '/check-manage',
    // 统计管理-综合统计
    comprehensiveStatistics: '/comprehensive-statistics',
    // 统计管理-投入管理
    inputManage: '/input-manage',
    // 统计管理-评比管理
    evaluationManage: '/evaluation-manage',
    // 档案管理-机构信息档案
    organizationManage: '/organization-manage',
    // 档案管理-运营情况新增
    addOperationSituation: '/add-operationsituation',
    // 养老机构基本信息
    organizationImformation: '/organization-imformation',
    // 入住长者情况统计
    ckeckInElder: '/checkIn-elder',
    // 智能监护-通话设置
    callSettings: '/call-settings',
    // 智能监护-监护管理
    careManage: '/care-manage',
    // 智能监护-电子围栏
    electronicFence: '/electronic-fence',
    // 智能监护-监护运营
    guardianshipOperation: '/guardianship-operation',
    // 智能监护-监护运营-查看信息
    operationMap: '/operation-map',
    // 长者健康信息
    elderHealthy: '/elder-healthy',
    // 消息列表
    messageListNew: '/message-list-new',
    // 用餐管理
    elderFood: '/elder-food',
    // 编辑用餐管理
    changeElderFood: '/change-elder-food',
    // 图片管理
    pictureManage: '/picture-manage',
    // 社区幸福院评比指标设置
    targetSetting: '/target-setting',
    // 社区幸福院评比详细指标
    targetDetails: '/target-details',
    // 社区幸福院评比详细指标新增
    addTargetDetails: '/add-target-details',
    // 长者关怀记录
    elderCareRecord: '/elder-care-record',
    // 长者关怀类型设置
    careTypeSetting: '/careType-setting',
    // 押金结算设置
    depositSetting: '/deposit-setting',
    // 回访问卷题目列表
    subjectList: '/subject-list',
    // 回访问卷题目新增
    addSubject: '/add-subject',
    // 回访问卷列表
    questionnaireList: '/questionnaire-list',
    // 回访问卷新增
    addQuestionnaire: '/add-questionnaire',
    // 意向回访列表
    returnVisitList: '/returnVisit-list',
    // 意向回访新增
    addReturnVisit: '/add-returnVisit',
    // 新增押金结算设置
    chanegDepositSetting: '/change-deposit-setting',
    // 长者能力评估
    olderAbilityAssessment: '/older-ability-assessment',
    nursingGrade: '/nursing-grade',
    // 收费减免
    deduction: '/deduction',
    changeDeduction: '/change-deduction',
    // 排班
    setClass: '/set-class',
    changeSetClass: '/change-set-class',
    setClassType: '/set-class-type',
    changeSetClassType: '/change-set-class-type',
    // 护理收费
    nursingPay: '/nursing-pay',
    changeNursingPay: '/change-nursing-pay',
    // APP页面设置
    appPageConfig: '/app-page-config',
    changeAppPageConfig: '/change-app-page-config',
    // 幸福院评比方案
    ratingPlan: '/rating-plan',
    changeRatingPlan: '/change-rating-plan',
    // 幸福院评比填表
    rating: '/rating',
    changeRating: '/change-rating',
    // 团体列表
    groups: '/groups',
    // 团体详情
    changeGroups: '/change-groups',
    // 服务计划
    servicePlan: '/service-plan',
    changeServicePlan: '/change-service-plan',
    // 社区幸福院-基础信息
    HappinessEntryControl: '/HappinessEntryControl',
    // 从业人员统计
    employees: '/employees',
    // 社区幸福院-社区服务人员
    HappinessWorkerEntryControl: '/happiness-work-entryControl',
    // 老人档案-个案服务情况
    serviceSituationList: '/service-situation-list',
    changeServiceSituation: '/change-service-situation',
    // 老人档案-长者列表
    elderInfoList: '/elder-info-list',
    changeElderInfoXfy: '/change-elder-info-xfy',
    // 老人档案-探访服务情况
    visitSituationList: '/visit-situation-list',
    changeVisitSituation: '/change-visit-situation',
    // APP用户管理
    appUserManage: '/app-user-manage',
    // APP意见反馈
    appFeedback: '/app-feedback',
    changeAppFeedback: '/change-app-feedback',
    // 老友圈
    appFriendCircle: '/app-firend-circle',
    changeAppFriendCircle: '/change-app-friend-circle',
    // APP在线咨询
    appOnlineTalk: '/app-online-talk',
    changeAppOnlineTalk: '/change-app-online-talk',
    // 圈子管理
    circle: '/circle',
    changeCircle: '/change-circle',
    changeAppUserManage: '/change-app-user-manage',
    receivablesList: '/receivables-list',
    actualReceivablesList: '/actual-receivables-list',
    // 平台运营-运营机构信息档案
    platformOrgList: '/platform-org-list',
    // 社区幸福院-运营机构信息
    xfyOrgList: '/xfy-org-list',
    addXfyOrgInfo: '/add-xfy-org-info',
    differenceDetail: '/difference-detail',
    // 项目协议与报告
    dealAndReport: '/deal-Report',
    changeDealAndReport: '/change-deal-Report',
    // 年度计划与总结
    yearplanAndSummary: '/yearplan-summary',
    changeYearplanAndSummary: '/change-yearplan-summary',
    // 月度计划
    monthPlan: '/month-plan',
    changeMonthPlan: '/change-month-plan',
    // 民办非营机构运营资助统计
    orgOperationFund: '/org-operation-fund',
    // 民办非营机构床位资助统计
    orgBedFund: '/org-bed-fund',
    // APP设置
    appSettings: '/app-settings',
    // 坐席运营统计
    seatOperation: '/seat-operation',
    // 长者膳食
    elderMeals: '/elder-meals',
    changeElderMeals: '/change-elder-meals',
    // 平台运营方，编辑机构
    changeOrganizationJG: '/change-organization-jg',
    changeOrganizationXYF: '/change-organization-xfy',
    changeOrganizationFWS: '/change-organization-fws',
    // 幸福小站
    happinessStation: '/happiness-station',
    changeHappinessStation: '/change-happiness-station',
    // 年度自评
    yearSelfAssessment: '/year-self-assessment',
    changeYearSelfAssessment: '/change-year-self-assessment',
    // 年度自评汇总
    happinessYearSelfAssessment: '/happiness-year-selfAssessment',
    changeHappinessYearSelfAssessment: '/change-happiness-year-selfAssessment',
    // 消防设施
    fireEquipment: '/fire-equipment',
    changeFireEquipment: '/change-fire-equipment',
    // 导入excel
    importExcel: '/import-excel',
    // 防疫-居家服务人员汇总表
    jujiaEpidemicPrevention: '/jujia-epidemic-prevention',
    jujiaEpidemicPreventionJj: '/jujia-epidemic-prevention-jj',
    jujiaEpidemicPreventionSq: '/jujia-epidemic-prevention-sq',
    jujiaEpidemicPreventionJg: '/jujia-epidemic-prevention-jg',
    // 防疫-居家服务人员汇总表-详情
    EpidemicPreventionDetail: '/epidemic-prevention-detail',
    // 防疫-全省民政部门重点场所防疫
    OrgDayInfoPrevention: '/org-day-info-prevention',
    // 防疫-全省民政部门重点场所防疫编辑
    editOrgDayInfoPrevention: '/edit-org-day-info-prevention',
    sysUseStatistics: '/sys-use-statistics',
    sysStatistics: '/sys-statistics',
    // 适老化设备警报
    deviceWarning: '/device-warning',
    changeDeviceWarning: '/change-device-warning',
    // 适老化设备管理
    deviceManage: '/device-manage',
    changeDeviceManage: '/change-device-manage',
    // 适老化设备管理
    changeDeviceFile: '/change-device-file',
    // 房态图
    roomStateDiagram: '/ftt',
    // 组织机构-福利院
    organizationFly: '/organization-fly',
    // 组织机构-幸福院
    organizationXfy: '/organization-xfy',
    // 组织机构-服务商
    organizationFws: '/organization-fws',
    // 组织机构-平台
    organizationPt: '/organization-pt',
    // 组织机构-民政
    organizationMz: '/organization-mz',
    // 查询是否同意隐私
    appPrivacyAgree: '/app-privacy-agree',
    // 商城管理
    dataDictionary: '/data-dictionary',
    changeDataDictionary: '/change-data-dictionary',
    changeChildDataDictionary: '/change-child-data-dictionary',
    products: '/products',
    changeProduct: '/change-product',
    productsSh: '/products-sh',
    changeProductSh: '/change-product-sh',
    productOrders: '/product-orders',
    changeProductOrder: '/change-product-order',
    // 服务商财务
    SPFinancialOverview: '/sp-financial-overview',
    SPFinancialDetail: '/sp-financial-detail',
    SPEntryRecord: '/sp-entry-record',
    // 平台财务
    PTFinancialOverview: '/pt-financial-overview',
    PTFinancialDetail: '/pt-financial-detail',
    PTEntryRecord: '/pt-entry-record',
    capitalDetail: '/capital-detail',
    separateAccountsSH: '/separate-accounts-sh',
    blocks: '/blocks',
    changeBlock: '/change-blocks',
    blockDetails: '/block-details',
    changeBlockDetails: '/change-block-details',
    productPreview: '/product-preview',
    // 设备警报
    deviceMessage: '/device-message',
    // 设备定位/轨迹
    deviceLocation: '/device-location',
    // 设备期限
    deviceTerm: '/device-term',
};