/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-30 18:16:42
 * @LastEditTime: 2019-12-10 16:13:37
 * @LastEditors: Please set LastEditors
 */

import { ROUTE_PATH, layoutMain } from "../index";
import { AppRouteObject } from "pao-aop-client";
import { createObject } from "pao-aop";
import { AllowanceReadViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-read";
import { AppServiceUtility } from "src/projects/app/appService";
import { AllowanceCheckViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-check";
import { AllowanceTemplateViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-template/allowance-template";
import { KEY_PARAM } from "src/business/util_tool";
import { AllowanceInsertViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-insert";
import { AllowanceManageViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-manage";
import { ALlowanceSuccessViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-success";
import { AllowanceReviewedViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-reviewed";
import { AllowanceProjectInsertViewControl } from "src/projects/views/buss_mis/allowance-manage/project_insert";
import { AllowanceProjectListViewControl } from "src/projects/views/buss_mis/allowance-manage/project_list";
import { AllowanceTemplateInsertViewControl } from "src/projects/views/buss_mis/allowance-manage/allowance-template/template-insert";
import { OldAllowanceCheckViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance";
import { OldAllwanceReviewedViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/old-allwance-reviewed";
import { OldAgeListViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/old-age-list";
import { OldAgeStatisticsViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/old-age-statistics";
import { DisabledPersonViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/disabled-person-list";
import { DisabledPersonCheckViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/disabled-person-check";
import { DisabledPersonStatisticsViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/disabled-person-statistics";
import { OldAllowanceNoCheckViewControl } from "src/projects/views/buss_mis/allowance-manage/old-allowance/no-check-list";

/**
 * 补贴管理路由
 */
export const subsidyRouter: AppRouteObject[] = [
    {
        path: ROUTE_PATH.allowanceRead,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            AllowanceReadViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_allowance_list_all',
                select_permission: '服务人员补贴申请查询',
                edit_permission: '服务人员补贴申请编辑',
                delete_permission: '服务人员补贴申请删除',
                add_permission: '服务人员补贴申请新增',

            })
    },
    {
        path: ROUTE_PATH.allowanceRead,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            AllowanceReadViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_allowance_list_look',
                select_permission: '查看服务人员补贴申请查询',
            })
    },
    {
        path: ROUTE_PATH.allowanceCheck,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            AllowanceCheckViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_allowance_applied_list_look_edit',
                select_permission: '补贴审核查询',
                edit_permission: '补贴审核编辑',
            }
        )
    },
    {
        path: ROUTE_PATH.allowanceOldCheck,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            AllowanceCheckViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_allowance_applied_list_look_edit',
                select_permission: '补贴审核查询',
                edit_permission: '补贴审核编辑',
            }
        )
    },
    // 高龄补贴审核
    {
        path: ROUTE_PATH.oldallowanceCheck,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            OldAllowanceCheckViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_audit_old_allowance_list',
                select_permission: '查看高龄津贴审核查询',
                edit_permission: '查看高龄津贴审核编辑',
                delete_permission: '查看高龄津贴审核删除',
                add_permission: '查看高龄津贴审核新增',
            }
        )
    },
    // 高龄补贴审核
    {
        path: ROUTE_PATH.oldallowanceNoCheck,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            OldAllowanceNoCheckViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_audit_old_allowance_no_check_list',
                select_permission: '查看高龄津贴审核查询',
                edit_permission: '查看高龄津贴审核编辑',
                delete_permission: '查看高龄津贴审核删除',
                add_permission: '查看高龄津贴审核新增',
            }
        )
    },
    // 高龄津贴名单查询页面
    {
        path: ROUTE_PATH.oldAgeList,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            OldAgeListViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_old_age_list',
                select_permission: '高龄津名单查询',
                add_permission: '高龄津贴名单新增',
            }
        )
    },
    // 残疾人名单
    {
        path: ROUTE_PATH.disabledPerson,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            DisabledPersonViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_disabled_person_list',
                select_permission: '高龄津名单查询',
                add_permission: '高龄津贴名单新增',
                edit_permission: '高龄津贴名单编辑',
            }
        )
    },
    // 残疾人认证列表
    {
        path: ROUTE_PATH.disabledPersonCheck,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            DisabledPersonCheckViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_audit_disabled_person_allowance_list',
                select_permission: '查看高龄津贴审核查询',
                edit_permission: '查看高龄津贴审核编辑',
                delete_permission: '查看高龄津贴审核删除',
                add_permission: '查看高龄津贴审核新增',
            }
        )
    },
    // 残疾人认证统计
    {
        path: ROUTE_PATH.disabledPersonStatistics,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(DisabledPersonStatisticsViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceTemplate,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(
            AllowanceTemplateViewControl,
            {
                permission_class: AppServiceUtility.login_service,
                get_permission_name: 'get_function_list',
                request_url: 'get_allowance_define_list_all',
                select_permission: '补贴定义查询',
                edit_permission: '补贴定义编辑',
                delete_permission: '补贴定义删除',
                add_permission: '补贴定义新增',
            }
        )
    },
    {
        path: `${ROUTE_PATH.allowanceInsert}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceInsert,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceManage,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceManageViewControl, {})
    },
    {
        path: `${ROUTE_PATH.allowanceProjectInsert}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceProjectInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceProjectInsert,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceProjectInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceProjectList,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceProjectListViewControl, {
            permission_class: AppServiceUtility.login_service,
            get_permission_name: 'get_function_list',
            request_url: 'get_allowance_project_list_all',
            select_permission: '补贴项目查询',
            edit_permission: '补贴项目编辑',
            delete_permission: '补贴项目删除',
            add_permission: '补贴项目新增',
        })
    },
    {
        path: ROUTE_PATH.allowanceSuccess,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(ALlowanceSuccessViewControl, {})
    },
    {
        path: `${ROUTE_PATH.allowanceReviewed}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceReviewedViewControl, {})
    },
    // 高龄补贴审核
    {
        path: `${ROUTE_PATH.oldallowanceReviewed}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(OldAllwanceReviewedViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceReviewed,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceReviewedViewControl, {})
    },
    {
        path: `${ROUTE_PATH.allowanceTemplateInsert}/${KEY_PARAM}`,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceTemplateInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.allowanceTemplateInsert,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(AllowanceTemplateInsertViewControl, {})
    },
    {
        path: ROUTE_PATH.oldAgeStatistics,
        mainFormID: layoutMain,
        targetType: 'secure',
        targetObject: createObject(OldAgeStatisticsViewControl, {})
    },
];
/**
 * 补贴管理模块链接
 */
export const subsidyLink = [
    { key: 'allowanceRead', link: ROUTE_PATH.allowanceRead, title: '服务人员补贴申请', permission: { 'permission': '服务人员补贴申请查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'allowanceRead', link: ROUTE_PATH.allowanceRead, title: '服务人员补贴申请', permission: { 'permission': '查看服务人员补贴申请查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'allowanceCheck', link: ROUTE_PATH.allowanceCheck, title: '补贴审核', permission: { 'permission': '补贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'allowanceManage', link: ROUTE_PATH.allowanceManage, title: '补贴状态查看', permission: { 'permission': '补贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'allowanceProjectInsert', link: ROUTE_PATH.allowanceProjectList, title: '补贴项目', permission: { 'permission': '补贴项目查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'allowanceTemplate', link: ROUTE_PATH.allowanceTemplate, title: '补贴定义', permission: { 'permission': '补贴定义查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'oldallowanceCheck', link: ROUTE_PATH.oldallowanceCheck, title: '高龄津贴认证列表', permission: { 'permission': '查看高龄津贴审核查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'oldAgeList', link: ROUTE_PATH.oldAgeList, title: '高龄津贴名单', permission: { 'permission': '高龄津贴名单查询' }, icon: 'font@pao_font_icon_user_bright' },
    { key: 'oldAgeStatistics', link: ROUTE_PATH.oldAgeStatistics, title: '高龄津贴统计报表', permission: { 'permission': '高龄津贴统计报表查询' }, icon: 'font@pao_font_icon_user_bright' },
];