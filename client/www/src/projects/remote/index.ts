/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-02 22:48:24
 * @LastEditTime : 2020-01-02 12:26:07
 * @LastEditors  : Please set LastEditors
 */
/*
 * 版权：Copyright (c) 2019 中国
 * 
 * 创建日期：Thursday August 1st 2019
 * 创建者：胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Thursday, 1st August 2019 3:31:11 pm
 * 修改者: 胡燕龙(Xiodra) - y.dragon.hu@hotmail.com
 * 
 * 说明
 *    1、
 */
const path = 'http://localhost:3100';
/** 远程 */
export const remote = {
    url: path + '/remoteCall',
    /** 文件上传路径 */
    upload_url: path + '/upload',
    /** 代表部署的正式环境地址 */
    path: path,
};