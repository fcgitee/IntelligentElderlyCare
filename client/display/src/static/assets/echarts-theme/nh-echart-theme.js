/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-10 17:04:27
 * @LastEditTime: 2019-09-02 17:50:46
 * @LastEditors: Please set LastEditors
 */
const BORDERCOLOR = "#080808";
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['exports', 'echarts'], factory);
    } else if (typeof exports === 'object' && typeof exports.nodeName !== 'string') {
        // CommonJS
        factory(exports, require('echarts'));
    } else {
        // Browser globals
        factory({}, root.echarts);
    }
}(this, function(exports, echarts) {
    var log = function(msg) {
        if (typeof console !== 'undefined') {
            console && console.error && console.error(msg);
        }
    };
    if (!echarts) {
        log('ECharts is not Loaded');
        return;
    }
    echarts.registerTheme('nh-echart-theme', {
        "color": [{
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0,
                    color: '#007BFD' // 0% 处的颜色
                }, {
                    offset: 1,
                    color: '#0045F9' // 100% 处的颜色
                }],
            },
            {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0,
                    color: '#E500E2' // 0% 处的颜色
                }, {
                    offset: 1,
                    color: '#FA33B6' // 100% 处的颜色
                }],
            }, {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0,
                    color: '#FCB711' // 0% 处的颜色
                }, {
                    offset: 1,
                    color: '#F7831C' // 100% 处的颜色
                }],
            }, {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0,
                    color: '#00CC3B' // 0% 处的颜色
                }, {
                    offset: 1,
                    color: '#00CC3B' // 100% 处的颜色
                }],
            }
        ],
        "backgroundColor": "rgba(0,0,0,0)",
        "textStyle": {},
        "title": {
            "left": 'left',
            "textStyle": {
                "color": "white",
                "fontSize": 40,
                "fontWeight": 500,
            },
            "padding": 10
        },
        // 修改标注
        "legend": {
            "align": 'auto',
            "textStyle": {
                "color": "white",
                "fontSize": 36
            },
            "top": 80
        },
        // 布局
        grid: {
            top: 60,
            left: 120,
            right: 50
        },
        "line": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "8"
                }
            },
            "lineStyle": {
                "width": "4",
                // "normal": {
                //     "width": "8"
                // }
            },
            "symbolSize": "0",
            "symbol": "circle",
            "smooth": true
        },
        "radar": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "8"
                }
            },
            "lineStyle": {
                "normal": {
                    "width": "3"
                }
            },
            "symbolSize": "0",
            "symbol": "circle",
            "smooth": true
        },
        "bar": {
            label: {
                show: true,
                fontSize: 32,
                position: 'top',
                color: "white",
            },
            // "barWidth": 60,
            "barGap": '0',
            "itemStyle": {
                "normal": {
                    "barBorderWidth": "0",
                    "barBorderColor": BORDERCOLOR
                },
                "emphasis": {
                    "barBorderWidth": "0",
                    "barBorderColor": BORDERCOLOR
                }
            }
        },
        "pie": {
            radius: ['32.5%', '65%'],
            center: ['50%', '60%'],
            label: {
                fontSize: 32,
                color: "white",
                formatter: '{b}: {d}%'
            },
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            }
        },
        "scatter": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            }
        },
        "boxplot": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            }
        },
        "parallel": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            }
        },
        "sankey": {
            label: {
                show: true,
                fontSize: 24,
                color: "white",
            },
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            },
            lineStyle: {
                color: 'source',
                opacity: 0.6
            }
        },
        "sunburst": {
            label: {
                show: true,
                fontSize: 24,
                color: "white",
            }
        },
        "funnel": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            }
        },
        "gauge": {
            center: ['50%', '50%'],
            splitNumber: 0, // 刻度数量
            startAngle: 220,
            endAngle: -40,
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                },
                "emphasis": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            },
            splitLine: {
                show: false,
            },
            axisLabel: {
                show: false
            },
            pointer: {
                show: false
            },
            detail: {
                show: true,
                offsetCenter: [0, '15%'],
                color: 'white',
                // formatter: function(params) {
                //     return params + "%";
                // },
                textStyle: {
                    fontSize: 44
                }
            }
        },
        "candlestick": {
            "itemStyle": {
                "normal": {
                    "color": "#fc97af",
                    "color0": "transparent",
                    "borderColor": "#fc97af",
                    "borderColor0": "#87f7cf",
                    "borderWidth": "2"
                }
            }
        },
        "graph": {
            "itemStyle": {
                "normal": {
                    "borderWidth": "0",
                    "borderColor": BORDERCOLOR
                }
            },
            "lineStyle": {
                "normal": {
                    "width": "1",
                    "color": "#ffffff"
                }
            },
            "symbolSize": "0",
            "symbol": "circle",
            "smooth": true,
            "color": [
                "#54ca76",
                "#9664e2",
                "#fbd436",
                "#37cbcb",
                "#f2627b",
                "#40a3fc"
            ],
            "label": {
                "normal": {
                    "textStyle": {
                        "color": "white"
                    }
                }
            }
        },
        "map": {
            "itemStyle": {
                "normal": {
                    "areaColor": "#f3f3f3",
                    "borderColor": "#999999",
                    "borderWidth": 0.5
                },
                "emphasis": {
                    "areaColor": "rgba(255,178,72,1)",
                    "borderColor": "#eb8146",
                    "borderWidth": 1
                }
            },
            "label": {
                "normal": {
                    "textStyle": {
                        "color": "#893448"
                    }
                },
                "emphasis": {
                    "textStyle": {
                        "color": "rgb(137,52,72)"
                    }
                }
            }
        },
        "geo": {
            "itemStyle": {
                "normal": {
                    "areaColor": "#f3f3f3",
                    "borderColor": "#999999",
                    "borderWidth": 0.5
                },
                "emphasis": {
                    "areaColor": "rgba(255,178,72,1)",
                    "borderColor": "#eb8146",
                    "borderWidth": 1
                }
            },
            "label": {
                "normal": {
                    "textStyle": {
                        "color": "#893448"
                    }
                },
                "emphasis": {
                    "textStyle": {
                        "color": "rgb(137,52,72)"
                    }
                }
            }
        },
        // x轴
        "categoryAxis": {
            "axisLine": {
                // "show": false,
                lineStyle: {
                    color: '#333'
                }
            },
            nameTextStyle: {
                fontSize: 32,
                color: "white"
            },
            "axisTick": {
                "show": false,
                "lineStyle": {
                    "color": "#333"
                }
            },
            "axisLabel": {
                "show": true,
                "textStyle": {
                    fontSize: 32,
                    color: "white"
                },
                // 显示所有x轴
                // interval: 0,
            },
            "splitLine": {
                "show": false,
            },
            "splitArea": {
                "show": false,
                "areaStyle": {
                    "color": [
                        "rgba(250,250,250,0.05)",
                        "rgba(200,200,200,0.02)"
                    ]
                }
            }
        },
        "valueAxis": {
            nameTextStyle: {
                fontSize: 32,
                color: "white"
            },
            axisLine: {
                lineStyle: {
                    color: '#4c4c4c'
                },
                showMinLabel: false
            },
            "axisLabel": {
                "show": true,
                "textStyle": {
                    fontSize: 32,
                    "color": "white"
                }
            },
            "splitLine": {
                "show": true,
                "lineStyle": {
                    "color": [
                        "#979797"
                    ],
                    type: "dotted"
                }
            },
            "splitArea": {
                "show": false,
                "areaStyle": {
                    "color": [
                        "rgba(250,250,250,0.05)",
                        "rgba(200,200,200,0.02)"
                    ]
                }
            }
        },
        "logAxis": {
            "axisLine": {
                "show": true,
                "lineStyle": {
                    "color": "white"
                }
            },
            "axisTick": {
                "show": true,
                "lineStyle": {
                    "color": "white"
                }
            },
            "axisLabel": {
                "show": true,
                "textStyle": {
                    "color": "white"
                }
            },
            "splitLine": {
                "show": true,
                "lineStyle": {
                    "color": [
                        "#979797"
                    ]
                }
            },
            "splitArea": {
                "show": false,
                "areaStyle": {
                    "color": [
                        "rgba(250,250,250,0.05)",
                        "rgba(200,200,200,0.02)"
                    ]
                }
            }
        },
        "timeAxis": {
            "axisLine": {
                "show": true,
                "lineStyle": {
                    "color": "white"
                }
            },
            "axisTick": {
                "show": true,
                "lineStyle": {
                    "color": "white"
                }
            },
            "axisLabel": {
                "show": true,
                "textStyle": {
                    "color": "white"
                }
            },
            "splitLine": {
                "show": true,
                "lineStyle": {
                    "color": [
                        "#979797"
                    ]
                }
            },
            "splitArea": {
                "show": false,
                "areaStyle": {
                    "color": [
                        "rgba(250,250,250,0.05)",
                        "rgba(200,200,200,0.02)"
                    ]
                }
            }
        },
        "toolbox": {
            "iconStyle": {
                "normal": {
                    "borderColor": "white"
                },
                "emphasis": {
                    "borderColor": "#000000"
                }
            }
        },
        "tooltip": {
            "axisPointer": {
                "lineStyle": {
                    "color": "#211919",
                    "width": 1
                },
                "crossStyle": {
                    "color": "#211919",
                    "width": 1
                }
            },
            "textStyle": {
                fontSize: 44
            }
        },
        "timeline": {
            "lineStyle": {
                "color": "#87f7cf",
                "width": 1
            },
            "itemStyle": {
                "normal": {
                    "color": "#00ff9f",
                    "borderWidth": 1
                },
                "emphasis": {
                    "color": "#f7f494"
                }
            },
            "controlStyle": {
                "normal": {
                    "color": "#87f7cf",
                    "borderColor": "#87f7cf",
                    "borderWidth": 0.5
                },
                "emphasis": {
                    "color": "#87f7cf",
                    "borderColor": "#87f7cf",
                    "borderWidth": 0.5
                }
            },
            "checkpointStyle": {
                "color": "#fc97af",
                "borderColor": "rgba(252,151,175,0.3)"
            },
            "label": {
                "normal": {
                    "textStyle": {
                        "color": "#87f7cf"
                    }
                },
                "emphasis": {
                    "textStyle": {
                        "color": "#87f7cf"
                    }
                }
            }
        },
        "visualMap": {
            "color": [
                "#b53e5d",
                "#87f7cf"
            ]
        },
        "dataZoom": {
            "backgroundColor": "rgba(255,255,255,0)",
            "dataBackgroundColor": "rgba(114,204,255,1)",
            "fillerColor": "rgba(114,204,255,0.2)",
            "handleColor": "#72ccff",
            "handleSize": "100%",
            "textStyle": {
                "color": "#333333"
            }
        },
        "markPoint": {
            "label": {
                "normal": {
                    "textStyle": {
                        "color": "white"
                    }
                },
                "emphasis": {
                    "textStyle": {
                        "color": "white"
                    }
                }
            }
        }
    });
}));