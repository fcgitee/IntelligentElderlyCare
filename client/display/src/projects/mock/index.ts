/*
 * 版权：Copyright (c) 2018 红网
 * 
 * 创建日期：Wednesday November 14th 2018
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Wednesday, 14th November 2018 4:53:20 pm
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、Mock测试数据
 *      2、Mock数据返回格式必须如：{result:data}
 */

import Mock, { mock } from "mockjs";
import { startMock } from "pao-aop-client";

const dev = process.env.NODE_ENV === 'development';

if (dev) {
    Mock.setup({ timeout: 2000 });
    startMock('http://localhost:3000/remoteCall', (serviceName, args) => {
        if (serviceName === "IBigScreenService") {
            return get_elder_live_time();
        }
        if (serviceName === "IDisplayControlService") {
            switch (args) {
                case "on_report":
                    return on_report_control();
                case "control_report":
                    return control_report();
                case "is_response":
                    return is_response();
                default:
                    return null;
            }
        }
        return null;
    });
}

let control = {
    /** 客户端 */
    client_host: "",
    /** 参数 */
    params: { "id": "123aaaa" },
    /** 弹窗数据源ID */
    drillDataSources: ["data-source-pie-huli-jibie"],
    /** 弹窗模块ID */
    drillDialogModules: ["dialog-module"],
    /** 报表ID */
    report: []
};

function get_elder_live_time() {
    return mock({
        result: [
            { "name": "呼叫热线量", 'value': 36978 },
            { "name": "呼入总量", 'value': 23137 },
            { "name": "呼出总量", 'value': 13841 },
            { "name": "报警呼叫", 'value': 11264 },
            { "name": "普通呼叫", 'value': 25714 }
        ]
    });
}

function on_report_control() {
    let temp = { ...control };
    return mock({
        result: temp
    });
}

function control_report() {
    control = {
        /** 客户端 */
        client_host: "",
        /** 参数 */
        params: { "id": "123aaaa" },
        /** 弹窗数据源ID */
        drillDataSources: ["data-source-pie-huli-jibie"],
        /** 弹窗模块ID */
        drillDialogModules: ["dialog-module"],
        /** 报表ID */
        report: []
    };
    return;
}

function is_response() {
    return mock({
        result: control ? false : true
    });
}