/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-10 18:25:19
 * @LastEditTime: 2019-07-10 18:25:19
 * @LastEditors: your name
 */
/*
 * 版权：Copyright (c) 2019 红网
 * 
 * 创建日期：Tuesday April 23rd 2019
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Tuesday, 23rd April 2019 10:26:24 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、政务物联网运维APP配置
 */
import { createObject } from 'pao-aop';
import { AppReactRouterControl, BlankMainFormControl } from 'pao-aop-client';
import { BlankMainFormForNhControl } from 'src/business/mainForm/blankMainFromForNh';
import 'src/static/assets/echarts-theme/nh-echart-theme.js';
import { BlankMainFormForNhAllViewControl } from './../business/mainForm/blankMainFromForNhAllView/index';
import { GovNetThingOPApplication } from './app';
import { blankID, blankMainFromForNhAllID, blankMainFromForNhID, RemotePath } from './router/index';
import { iecRouter } from './router/nanhai-report';
import { selectIframeReport } from './views/iframe-view';
import { pageReport } from './views/page-view';
import { pageReportTest } from './views/page-view-test';
import { report } from './views/test';

const blank = new BlankMainFormControl();

const blankMainFrom = new BlankMainFormForNhControl();
const blankMainFromForAll = createObject(
    BlankMainFormForNhAllViewControl,
    {
        // background_img: require("./style/img/background.png")
    }
);

const router = new AppReactRouterControl(
    {
        [blankID]: blank,
        [blankMainFromForNhID]: blankMainFrom,
        [blankMainFromForNhAllID]: blankMainFromForAll,
    },
    [
        {
            path: RemotePath.index,
            redirect: RemotePath.organization,
            exact: true
        },
        {
            path: `${RemotePath.home}`,
            mainFormID: blankMainFromForNhAllID,
            targetObject: report
        },
        {
            path: `${RemotePath.selectIframe}`,
            mainFormID: blankMainFromForNhAllID,
            targetObject: selectIframeReport
        },
        {
            path: `${RemotePath.pageView}`,
            mainFormID: blankMainFromForNhAllID,
            targetObject: pageReport
        }, {
            path: `${RemotePath.pageViewTest}`,
            mainFormID: blankMainFromForNhAllID,
            targetObject: pageReportTest
        },
        ...iecRouter,
    ]
);

export let defaultObject = new GovNetThingOPApplication(
    router
);

export default defaultObject;