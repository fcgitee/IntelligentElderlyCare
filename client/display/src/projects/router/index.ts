/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-05 15:32:42
 * @LastEditTime: 2019-09-05 15:35:36
 * @LastEditors: Please set LastEditors
 */
export const blankID = 'blank', blankMainFromForNhID = 'BlankForNh', blankMainFromForNhAllID = 'blankMainFromForNhAllID';

export const RemotePath = {
    index: "/display/",
    home: "/display/home",
    selectIframe: "/display/select-iframe",
    /** 南海福利--8月13日 新需求 */
    ecosystem: "/display/ecosystem",
    /** 机构养老 */
    organization: "/display/organization",
    /** 机构养老 */
    organizationDetail: "/display/organization-detail",
    /** 社区养老 */
    happiness: "/display/happiness",
    /** 社区养老 */
    happinessDetail: "/display/happiness-detail",
    /** 居家养老 */
    homePension: "/display/home-pension",
    /** 居家养老--具体机构详情 */
    homePensionDetail: "/display/home-pension-detail",
    /** 辛福院 */
    communityHome: "/display/community-home",
    /** 综合 */
    comprehensive: "/display/comprehensive",
    /** 养老院 */
    welfareCentre: "/display/welfare-Centre",
    /** 长者 */
    elder: "/display/elder",
    /** 服务商 */
    serviceProvider: "/display/service-provider",
    /** 服务人员 */
    servicePersonal: "/display/service-personal",
    /** 测试tabs */
    tabs: "/display/tabs",
    /** 平板控制 */
    computercontrol: '/display/computercontrol',
    pageView: '/display/page-report',
    /** 
     * 9-25南海新原型 
     */
    /** 养老院统计分析总体 */
    welfareSum: '/display/welfare-sum',
    /** 居家服务商综合分析 */
    serviceAanalysis: '/display/service-analysis',
    /** 养老大数据整体统计分析 */
    welfareBigDataStatistice: '/display/welfare-big-data-statistics',
    /** 幸福院统计分析 */
    happinessStatistics: "/display/happiness-sum",
    /** 下拉切换-test */
    pageViewTest: '/display/page-report-test',
    /** 双鸭山大屏页面--机构总览 */
    agencyOverview: '/display/agency-overview',
    /** 平板控制--测试 */
    computercontrolTest: '/display/computercontrol-test'
};