/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-05 15:32:42
 * @LastEditTime: 2019-09-05 15:35:13
 * @LastEditors: Please set LastEditors
 */
import { RemotePath, blankID, blankMainFromForNhAllID } from "./index";
import { irCommunityHomeControl } from "src/projects/views/ir-community-home";
import { irComprehensiveControl } from "src/projects/views/ir-comprehensive";
import { irElderControl } from "src/projects/views/ir-elder";
import { irServiceProviderControl } from "src/projects/views/ir-service-provider";
import { irServicePersonalControl } from "src/projects/views/ir-service-personal";
import { TestTabsControl } from "../views/test-tabs";
import { computercontrol } from "../views/tablet-computer-control";
import { computercontrolTest } from '../views/tablet-computer-control-test';
import { createObject } from "pao-aop";
import { irWelfareSumControl } from '../views/ir-welfare-sum';
import { irAgencyOverviewTestControl } from '../views/ir-agency-overview-test';
import { irServiceAnalysisControl } from '../views/ir-service-analysis';
import { irWelfareBigDataStatisticsControl } from "../views/ir-welfare-big-data-statistics";
import { irWelfareCentreControl } from "../views/ir-welfare-centre";
import { irHappinessStatisticsControl } from "../views/ir-happiness-statistics";

/** 智慧养老大屏（8月13日 南海大屏新需求） */
export const iecRouter = [
    {
        path: RemotePath.communityHome,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irCommunityHomeControl
    },
    {
        path: RemotePath.comprehensive,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irComprehensiveControl
    },
    {
        path: RemotePath.elder,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irElderControl
    },
    {
        path: RemotePath.serviceProvider,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irServiceProviderControl
    },
    {
        path: RemotePath.servicePersonal,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irServicePersonalControl
    },
    {
        path: RemotePath.welfareCentre,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irWelfareCentreControl
    },
    {
        path: RemotePath.tabs,
        mainFormID: blankMainFromForNhAllID,
        targetObject: TestTabsControl
    },
    {
        path: RemotePath.computercontrol,
        mainFormID: blankID,
        targetObject: createObject(computercontrol)
    },
    /** 9-25 新大屏原型 */
    {
        path: RemotePath.welfareBigDataStatistice,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irWelfareBigDataStatisticsControl
    },
    {
        path: RemotePath.happinessStatistics,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irHappinessStatisticsControl
    }, {
        path: RemotePath.welfareSum,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irWelfareSumControl
    }, {
        path: RemotePath.serviceAanalysis,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irServiceAnalysisControl
    }, {
        path: RemotePath.computercontrolTest,
        mainFormID: blankID,
        targetObject: createObject(computercontrolTest)
    },
    /** 双鸭山大屏 */
    {
        path: RemotePath.agencyOverview,
        mainFormID: blankMainFromForNhAllID,
        targetObject: irAgencyOverviewTestControl
    }
];