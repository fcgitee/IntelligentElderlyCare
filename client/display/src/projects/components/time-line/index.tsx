import React from "react";
import { reactControl } from "pao-aop-client";
import { addon } from "pao-aop";
import './index.less';
import { Row } from "antd";
import { DisplayerState, BaseDisplayer, BaseDisplayerControl } from "src/business/report";
/**
 * 组件：时间轴组件状态
 */
export interface TimeLineState extends DisplayerState {
    /** 选中的圈，默认最后一个 */
    active?: number;
}

/**
 * 组件：时间轴组件
 * 时间轴组件
 */
export class TimeLine extends BaseDisplayer<TimeLineControl, TimeLineState> {
    constructor(props: TimeLineControl) {
        super(props);
        this.state = {
            data: props.data! || {},
        };
    }
    componentDidMount() {

    }

    timeClick = (value: any, index: number) => {
        console.info(value);
        this.setState({
            active: index,
        });
    }

    render() {
        const { data: dataSet, active: select } = this.state;
        let data = [];
        let active: any;
        for (const dataViewID in dataSet) {
            if (dataSet.hasOwnProperty(dataViewID)) {
                data = dataSet[dataViewID];
            }
        }

        if (data.length <= 0) {
            data = [
                { 'value': '1', 'text': '2019年' },
                { 'value': '1', 'text': '1月' },
                { 'value': '2', 'text': '2月' },
                { 'value': '3', 'text': '3月' },
                { 'value': '4', 'text': '4月' },
                { 'value': '5', 'text': '5月' }
            ];
        }
        if (!select && select !== 0) {
            active = data.length - 1;
        } else {
            active = select;
        }
        return (
            <div>
                <Row className='time-line'>
                    {
                        data ? data.map((value: any, index: any) => {
                            if (value.text.indexOf("年") !== -1) {
                                return (<span className='line-item' key={value.value}>
                                    <span className='line' />
                                    <span className={active === index ? 'line-content big-time activity-time' : 'line-content big-time'} onClick={this.timeClick.bind(this, value.value, index)}>{value.text}</span>
                                </span>);
                            } else {
                                return (
                                    <span className='line-item' key={value.value}>
                                        <span className='line' />
                                        <span className={active === index ? 'line-content activity-time' : 'line-content'} onClick={this.timeClick.bind(this, value.value, index)}>{value.text}</span>
                                    </span>
                                );
                            }

                        }) : ''
                    }
                </Row>
            </div>
        );
    }
}

/**
 * 控件：时间轴组件控制器
 * 时间轴组件
 */
@addon('TimeLine', '时间轴组件', '时间轴组件')
@reactControl(TimeLine)
export class TimeLineControl extends BaseDisplayerControl {
}