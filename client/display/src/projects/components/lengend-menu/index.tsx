/*
 * 版权：Copyright (c) 2018 红网
 * 
 * 创建日期：Thursday June 14th 2019
 * 创建者：陈旭霖 - 376473979@qq.com
 * 
 * 修改日期: Thursday, 10th October 2019 2:08:57 pm
 * 修改者: 何伟聪(hewc) - avenger463010817@live.com
 * 
 * 说明
 * 		1、图例组件
 */
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl, BaseReactElement } from "pao-aop-client";
import React from "react";
import { Menu, Icon } from "antd";
import "./index.less";
import { ClickParam } from "antd/lib/menu";

/** 分隔符 */
const SPILT_STR = "|";

/**
 * 组件：图例组件状态
 */
export interface LegendMenuState {
}

/**
 * 组件：图例组件
 */
export class LegendMenu extends BaseReactElement<LegendMenuControl, LegendMenuState> {
    handleClick = (e: ClickParam) => {
        let keysArr = e.key.split(SPILT_STR);
        this.props.display_control.toggleLegend!(keysArr);
    }

    render() {
        // let se_key = this.props.option!.map((e, i) => {
        //     if (!e.children) {
        //         return e.key;
        //     } else {
        //         return;
        //     }
        // });
        let selectedData: any = [];
        this.props.option!.forEach((item) => {
            if (item.selected === undefined || (item.selected !== undefined && item.selected)) {
                selectedData.push(item.title);
            }
        });
        return (
            <Menu
                onClick={this.handleClick}
                theme={"dark"}
                style={{ width: 350 }}
                defaultSelectedKeys={selectedData}
                defaultOpenKeys={['sub1']}
                mode="inline"
                className={"legend-menu"}
                multiple={true}
            >
                {
                    this.props.option!.map((e, i) => {
                        const keysStr = e.key.join(SPILT_STR);
                        return (
                            <Menu.Item style={{ height: '100px', lineHeight: '100px', margin: '0px' }} key={keysStr}>
                                {/* <Icon type="pie-chart" /> */}
                                {
                                    e.icon ?
                                        <img className={"icon-img"} src={e.icon} alt="" width={e.width ? e.width : 50} />
                                        :
                                        <Icon type="pie-chart" />
                                }
                                <span>{e.title}</span>
                            </Menu.Item>
                        );
                    })
                }
            </Menu>
        );
    }
}

/**
 * 控件：图例组件
 */
@addon('LegendMenuControl', '图例组件', '控制图例组件')
@reactControl(LegendMenu)
export class LegendMenuControl extends BaseReactElementControl {
    /** echart实例 */
    display_control?: any;
    /**
     * 图例选项
     */
    option?: {
        /**
         * 图例图标
         */
        icon?: string;
        /**
         * 图例标题
         */
        title?: string;
        /**
         * 图例控制的地图key值数组
         */
        key: string[];
        /**
         * 默认图片宽度
         */
        width?: number;
        /** 默认选中 */
        selected?: boolean;
    }[];
}
