import { reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import "./index.less";
import { Select, Icon } from "antd";
const { Option } = Select;

/**
 * 组件：基础视图选择组件状态
 */
export class ViewSelectState {
}

/**
 * 组件：基础视图选择组件
 */
export class ViewSelect extends React.Component<ViewSelectControl, ViewSelectState> {
    constructor(props: ViewSelectControl) {
        super(props);
    }

    render() {
        return (
            <Select
                className={"view-select-test"}
                size={"large"}
                dropdownMatchSelectWidth={true}
                suffixIcon={<Icon type="caret-down" />}
                onSelect={this.props.doSelect}
                defaultValue={'irWelfareBigDataStatistics'}
                value={this.props.default_select}
                dropdownClassName={"layout-dropdown"}
            >
                <Option value={'irWelfareBigDataStatistics'}>养老大数据整体统计分析</Option>
                <Option value={'irWelfareSum'}>养老院统计分析总体</Option>
                <Option value={'irHappinessStatistics'}>幸福院统计分析总体</Option>
                <Option value={'irServiceAnalysis'}>居家服务商统计分析</Option>
            </Select>
        );
    }
}

/**
 * 控件：“基础视图选择组件”控制器
 */
@addon('ViewSelect', '基础视图选择组件', '基础视图选择组件视图结构')
@reactControl(ViewSelect, true)
export class ViewSelectControl extends BaseReactElementControl {
    default_select?: string;
    doSelect?: (value: string) => void;
}