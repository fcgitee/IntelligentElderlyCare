import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import React from "react";
import { Row, Icon, Col, Divider } from "antd";
import './index.less';

/**
 * 组件：活动详情组件状态
 */
export interface ActivityDetailsState extends BaseReactElementState {
    /** 组件是否显示 */
    isShow?: boolean;
}

/**
 * 组件：活动详情组件
 * 活动详情组件
 */
export class ActivityDetails extends BaseReactElement<ActivityDetailsControl, ActivityDetailsState> {
    constructor(props: ActivityDetailsControl) {
        super(props);
        this.state = {
            isShow: false,
        };
    }
    // 关闭详情
    close = () => {
        this.setState({
            isShow: false,
        });
    }
    render() {
        return (
            <div className={this.state.isShow ? 'activity-details-content' : 'activity-details-content-hidden'} >
                <div className='close-btn' onClick={this.close}>
                    <Icon type='close' />
                </div>
                <Row type='flex' justify='center'>
                    <img src='http://news.xkb.com.cn/uploads/2019-01-02/yiah3x32qf4.jpg' />
                </Row>
                <Row>
                    <span className='activity-state'>报名中</span>
                    “子孙乐融融，欢笑满松岗” 暑假子孙同乐小组
                </Row>
                <Row className='activity-item'>
                    <Icon type="clock-circle" />
                    08.08 9:00-08.09 09:50
                        </Row>
                <Row className='activity-item'>
                    <span className='activity-cost'>免费</span>
                    已报名&nbsp;0/10
                </Row>
                <Row className='detail-item'>
                    <Col span={2} className='icon-content'>
                        <Icon type='reconciliation' />
                    </Col>
                    <Col span={20}>
                        <p>养老机构</p>
                        <p>地址</p>
                    </Col>
                    <Col className='icon-content'>
                        <Icon type="environment" theme="filled" />
                    </Col>
                </Row>
                <Row className='detail-item'>
                    <Col span={2} className='icon-content'>
                        <Icon type='user' />
                    </Col>
                    <Col span={20}>
                        <p>工作人员</p>
                        <p>86000000</p>
                    </Col>
                    <Col className='icon-content'>
                        <Icon type="phone" theme="filled" />
                    </Col>
                </Row>
                <Row className='detail-item'>
                    <Divider className='detail-divider'>活动详情</Divider>
                    <div className='activity-detail-content'>活动详情正文</div>
                </Row>
            </div>
        );
    }
}

/**
 * 控件：活动详情组件控制器
 * 活动详情组件
 */
@addon('ActivityDetails', '活动详情组件', '活动详情组件')
@reactControl(ActivityDetails)
export class ActivityDetailsControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}