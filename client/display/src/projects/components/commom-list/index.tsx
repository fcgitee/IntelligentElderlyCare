/*
 * 版权：Copyright (c) 2018 红网
 * 
 * 创建日期：Thursday June 14th 2019
 * 创建者：陈旭霖 - 376473979@qq.com
 * 
 * 修改日期: Thursday, June 14th 2019 11:35:20 
 * 修改者: 陈旭霖 - 376473979@qq.com
 * 
 * 说明
 * 		1、通用列表组件
 */
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl, BaseReactElement } from "pao-aop-client";
import React from "react";
import { List, Typography, Row } from "antd";
import "./index.less";

const { Title } = Typography;

/**
 * 组件：通用列表显示器组件状态
 */
export interface CommonListState {
}

/**
 * 组件：通用列表显示器组件
 */
export class CommonList extends BaseReactElement<CommonListControl, CommonListState> {
    constructor(props: CommonListControl) {
        super(props);
        this.state = { data: [] };
    }

    render() {
        // const { data } = this.state;

        const data = [
            {
                title: "123",
                description: (
                    <Row>
                        08.14 09:00-08.14 09:59<br />
                        免费 已报名 0/10
                    </Row>
                ),
            },
            {
                title: "123",
                description: (
                    <Row>
                        08.14 09:00-08.14 09:59<br />
                        免费 已报名 0/10
                    </Row>
                ),
            }
        ];

        return (
            <List
                className={"common-list"}
                style={{ display: "inline-block" }}
                header={<Title>23111</Title>}
                itemLayout={"vertical"}
                dataSource={data}
                renderItem={item => (
                    <List.Item
                        key={item.title}
                    >
                        <List.Item.Meta
                            avatar={<img src={"http://www.pig66.com/uploadfile/2017/0511/20170511074941292.jpg"} />}
                            // TODO: 带前缀标签的标题
                            title={"其乐融融"}
                            // title={<Title></Title>}
                            // TODO: 这里就是主要不一致的内容
                            description={item.description}
                        />
                    </List.Item>
                )
                }
            />
        );
    }

    onSetData(dataView: DataView, data?: any) {
        this.setState({
            data
        });
    }
}

/**
 * 控件：通用列表显示器组件
 */
@addon('CommonListControl', '通用列表显示器组件', '控制显示通用列表显示器组件')
@reactControl(CommonList)
export class CommonListControl extends BaseReactElementControl {
    /** echart实例 */
    echar_instance?: any;
}
