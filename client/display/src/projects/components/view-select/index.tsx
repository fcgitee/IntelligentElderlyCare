import { reactControl, BaseReactElementControl } from "pao-aop-client";
import React from "react";
import { addon } from "pao-aop";
import "./index.less";
import { Select, Icon } from "antd";
const { Option } = Select;

/**
 * 组件：基础视图选择组件状态
 */
export class ViewSelectState {
}

/**
 * 组件：基础视图选择组件
 */
export class ViewSelect extends React.Component<ViewSelectControl, ViewSelectState> {
    constructor(props: ViewSelectControl) {
        super(props);
    }

    render() {
        return (
            <Select
                className={"view-select"}
                size={"large"}
                dropdownMatchSelectWidth={true}
                suffixIcon={<Icon type="caret-down" />}
                onSelect={this.props.doSelect}
                defaultValue={'irWelfareCentre'}
                value={this.props.default_select}
                dropdownClassName={"layout-dropdown"}
            >
                <Option value={'irWelfareCentre'}>机构养老</Option>
                <Option value={'irCommunityHome'}>社区养老</Option>
                <Option value={'irServiceProvider'}>服务商</Option>
                <Option value={'irServicePersonal'}>服务人员</Option>
                <Option value={'irElder'}>长者</Option>
                <Option value={'irComprehensive'}>综合</Option>
            </Select>
        );
    }
}

/**
 * 控件：“基础视图选择组件”控制器
 */
@addon('ViewSelect', '基础视图选择组件', '基础视图选择组件视图结构')
@reactControl(ViewSelect, true)
export class ViewSelectControl extends BaseReactElementControl {
    default_select?: string;
    doSelect?: (value: string) => void;
}