import React from "react";
import { BaseReactElementState, BaseReactElement, reactControl, BaseReactElementControl } from "pao-aop-client";
import { addon } from "pao-aop";
import './index.less';
import { Row, Col, Icon } from "antd";
import Paragraph from "antd/lib/typography/Paragraph";
/**
 * 组件：活动列表组件状态
 */
export interface ActivityListState extends BaseReactElementState {
    /** 组件是否显示 */
    isShow?: boolean;
}

/**
 * 组件：活动列表组件
 * 活动列表组件
 */
export class ActivityList extends BaseReactElement<ActivityListControl, ActivityListState> {
    constructor(props: ActivityListControl) {
        super(props);
        this.state = {
            isShow: true,
        };
    }
    // 关闭列表
    close = () => {
        this.setState({
            isShow: false,
        });
    }
    render() {
        return (
            <div className={this.state.isShow ? 'activity-list-content' : 'activity-list-content-hidden'}>
                <div className='close-btn' onClick={this.close}>
                    <Icon type='close' />
                </div>
                <Row className='list-title' type='flex' justify='center'>
                    标题名字
                </Row>
                <Row className='list-item'>
                    <Col span={8}>
                        <img src='http://news.xkb.com.cn/uploads/2019-01-02/yiah3x32qf4.jpg' />
                    </Col>
                    <Col span={16}>
                        <Row>
                            <span className='activity-title'>
                                <Paragraph ellipsis={true}>
                                    <span className='activity-state'>报名中</span>
                                    “子孙乐融融，欢笑满松岗” 暑假子孙同乐小组
                                </Paragraph>
                            </span>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="reconciliation" />
                                地址
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="clock-circle" />
                                08.08 9:00-08.09 09:50
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <span className='activity-cost'>免费</span>
                                已报名&nbsp;0/10
                            </Paragraph>
                        </Row>
                    </Col>
                </Row>
                <Row className='list-item'>
                    <Col span={8}>
                        <img src='http://news.xkb.com.cn/uploads/2019-01-02/yiah3x32qf4.jpg' />
                    </Col>
                    <Col span={16}>
                        <Row>
                            <span className='activity-title'>
                                <Paragraph ellipsis={true}>
                                    <span className='activity-state'>报名中</span>
                                    “子孙乐融融，欢笑满松岗” 暑假子孙同乐小组
                                </Paragraph>
                            </span>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="reconciliation" />
                                地址
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="clock-circle" />
                                08.08 9:00-08.09 09:50
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <span className='activity-cost'>免费</span>
                                已报名&nbsp;0/10
                            </Paragraph>
                        </Row>
                    </Col>
                </Row>
                <Row className='list-item'>
                    <Col span={8}>
                        <img src='http://news.xkb.com.cn/uploads/2019-01-02/yiah3x32qf4.jpg' />
                    </Col>
                    <Col span={16}>
                        <Row>
                            <span className='activity-title'>
                                <Paragraph ellipsis={true}>
                                    <span className='activity-state'>报名中</span>
                                    “子孙乐融融，欢笑满松岗” 暑假子孙同乐小组
                                </Paragraph>
                            </span>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="reconciliation" />
                                地址
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="clock-circle" />
                                08.08 9:00-08.09 09:50
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <span className='activity-cost'>免费</span>
                                已报名&nbsp;0/10
                            </Paragraph>
                        </Row>
                    </Col>
                </Row>
                <Row className='list-item'>
                    <Col span={8}>
                        <img src='http://news.xkb.com.cn/uploads/2019-01-02/yiah3x32qf4.jpg' />
                    </Col>
                    <Col span={16}>
                        <Row>
                            <span className='activity-title'>
                                <Paragraph ellipsis={true}>
                                    <span className='activity-state'>报名中</span>
                                    “子孙乐融融，欢笑满松岗” 暑假子孙同乐小组
                                </Paragraph>
                            </span>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="reconciliation" />
                                地址
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <Icon type="clock-circle" />
                                08.08 9:00-08.09 09:50
                            </Paragraph>
                        </Row>
                        <Row>
                            <Paragraph ellipsis={true} className='activity-item'>
                                <span className='activity-cost'>免费</span>
                                已报名&nbsp;0/10
                            </Paragraph>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

/**
 * 控件：活动列表组件控制器
 * 活动列表组件
 */
@addon('ActivityList', '活动列表组件', '活动列表组件')
@reactControl(ActivityList)
export class ActivityListControl extends BaseReactElementControl {
    constructor() {
        super();
    }
}