import { BaseReactElementControl } from "pao-aop-client";
import { RoomStatus, ROOM_NULL_STATUS } from "src/business/util_tool";
import { Row, Col } from "antd";
import React from "react";
import "./index.less";

/**
 * 房态图
 */
export function RoomStatusWrapElements(props: BaseReactElementControl & {
    /**
     * 房态图区域元素
     */
    roomStatus: RoomStatus,
    /**
     * 颜色池
     */
    colorPool: string[],
    /**
     * 层级
     */
    level: number
}) {
    return (
        <Row type={"flex"} justify={"space-between"} className={"divBorder"} style={{ background: props.colorPool[props.level] }}>
            {
                // 首层为空，不渲染
                props.level === -1 ?
                    undefined
                    :
                    <Col className={"roomStaus"} span={24}>
                        {props.roomStatus.zone_name === ROOM_NULL_STATUS ? undefined : props.roomStatus.zone_name}
                    </Col>
            }
            {
                props.children
            }
        </Row>
    );
}

/**
 * 长者组件
 */
export function OlderElements(props: {
    /**
     * 长者名字
     */
    residentsName: string;
}) {
    return (
        <div
            className={"older"}
        >
            {props.residentsName}
        </div>
    );
}

/**
 * 可拖拽区域（可放置拖拽组件）
 */
export function BedElements(props: {
    /**
     * 床位元素（可能是空床，可能是有长者的床）
     */
    bedJSXElement: JSX.Element;
}) {
    return (
        <div
            className={"bed"}
        >
            {props.bedJSXElement}
        </div>
    );
}