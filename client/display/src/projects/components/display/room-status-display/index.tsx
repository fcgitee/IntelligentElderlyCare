/*
 * 版权：Copyright (c) 2018 红网
 * 
 * 创建日期：Thursday June 14th 2019
 * 创建者：陈旭霖 - 376473979@qq.com
 * 
 * 修改日期: Thursday, June 14th 2019 11:35:20 
 * 修改者: 陈旭霖 - 376473979@qq.com
 * 
 */
import { addon } from "pao-aop";
import { reactControl } from "pao-aop-client";
import { DisplayerState, BaseDisplayer, BaseDisplayerControl } from "src/business/report/smart";
import React from "react";
import { Row, Col } from "antd";
import "./index.less";
import { RoomStatus } from "src/business/util_tool";
import { RoomStatusWrapElements, OlderElements, BedElements } from "../room-status-wrap-elements";

interface Bed {
    /** id */
    id: string;
    /** 区域 */
    area_id: string;
    /** 区域 */
    area_name: string;
    /** 床位名称 */
    name: string;
    /** 床位编号 */
    bed_code: string;
    /** 入住人id */
    residents_id: string;
    /** 入住人 */
    residents: string;
    /** 服务包 */
    service_item_package_id: string;
    /** 服务包 */
    service_item_package: string;
    /** 备注 */
    remark: string;
}

/**
 * 组件：房态图显示器状态
 */
export interface RoomStatusDisplayState extends DisplayerState {
    /**
     * 颜色池
     */
    colorPool?: string[];
}

/**
 * 组件：房态图显示器
 */
export class RoomStatusDisplay extends BaseDisplayer<RoomStatusDisplayControl, RoomStatusDisplayState> {
    constructor(props: RoomStatusDisplayControl) {
        super(props);
        this.state = {
            data: props.data! || [],
            colorPool: ["rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)", "rgba(41, 186, 254, 0.08)"]
        };
    }

    /**
     * 获取床位视图渲染元素
     */
    getBedEle(bed: Bed[], user: any[]) {
        // 数据库出来的user字段与bed字段的顺序不是一一对应的，只能这样寻找床位对应的长者
        function getResidentsOfBed(bed: Bed) {
            for (let iUser = 0; iUser < user.length; iUser++) {
                const eUser = user[iUser];
                if (bed.residents_id === eUser.id) {
                    return eUser;
                }
            }
            return undefined;
        }

        return bed.map((e, i) => {
            const userOfBed = getResidentsOfBed(e);
            return (
                <div key={i}>
                    {e.name}
                    {
                        // 因为长者数组长度一定不会超过床位长度，所以可以这么判断是否需要增加长者元素
                        userOfBed ?
                            <OlderElements
                                residentsName={userOfBed.name}
                            />
                            :
                            undefined
                    }
                </div>
            );
        });
    }

    /**
     * 递归生成房态图节点
     * @param level 房态图
     */
    traversalRoomStatusAndGetHtml(roomStatusElement: RoomStatus[], level: number): any {
        let currentEle = roomStatusElement;
        const levelNow = level;
        // 遍历当前节点
        return currentEle.map((roomStatus, i) => {
            let childEle;
            let bedEle: JSX.Element[];

            if (roomStatus.down_element.length > 0) {
                // 当前节点有下级元素，则遍历子节点
                childEle = this.traversalRoomStatusAndGetHtml(roomStatus.down_element, levelNow + 1);

            }

            // 无下级元素，目前来看，则一定有床位元素
            if (roomStatus.bed && roomStatus.bed.length > 0) {
                bedEle = this.getBedEle(roomStatus.bed, roomStatus.user!);
            }

            // 返回当前节点生成的元素
            return (
                <RoomStatusWrapElements key={i} roomStatus={roomStatus} colorPool={this.state.colorPool!} level={levelNow} >
                    {
                        childEle ?
                            childEle.map((e: any, i: number) =>
                                (
                                    <Col key={i}>
                                        {e}
                                    </Col>
                                )
                            )
                            :
                            bedEle! ?
                                <Row type={"flex"}>
                                    {
                                        bedEle!.map((e: any, i: number) =>
                                            (
                                                <BedElements
                                                    key={i}
                                                    bedJSXElement={e}
                                                />
                                            )
                                        )
                                    }
                                </Row>
                                :
                                undefined
                    }
                </RoomStatusWrapElements>
            );
        });
    }

    roomDatas() {
        return (
            <div>
                <Row gutter={16} className="room-datas">
                    <Col className="room-datas-row">
                        <div className="gutter-box"><span className="st">床位总数</span><br />31</div>
                    </Col>
                    <Col className="room-datas-row">
                        <div className="gutter-box"><span className="st">入住人数</span><br />23</div>
                    </Col>
                    <Col className="room-datas-row">
                        <div className="gutter-box"><span className="st">请假人数</span><br />0</div>
                    </Col>
                    <Col className="room-datas-row">
                        <div className="gutter-box"><span className="st">空床数</span><br />8</div>
                    </Col>
                </Row>
            </div>
        );
    }

    render() {
        const { data: dataSet } = this.state;
        const { max_height } = this.props;

        let data = [];
        for (const dataViewID in dataSet) {
            if (dataSet.hasOwnProperty(dataViewID)) {
                data = dataSet[dataViewID];
            }
        }

        // TODO: 床位数据以及房态图数据视图
        // for (const dataViewID in dataSet) {
        //     if (dataSet.hasOwnProperty(dataViewID)) {
        //         switch (dataViewID) {
        //             case num_display_data_view_id:
        //                 dataNumDisplay = dataSet[dataViewID];
        //                 continue;
        //             case carousel_img_data_view_id:
        //                 dataCarouselImg = dataSet[dataViewID];
        //                 continue;
        //             default:
        //                 continue;
        //         }
        //     }
        // }

        // 排序，按照顶级元素数量排序，顶级元素排在前头
        (data as RoomStatus[]).sort((a, b) =>
            a.upper_element.length - b.upper_element.length
        );

        let roomStatusElement = new RoomStatus().buildOrAddElements!(data)!;

        return (
            <div className="room-status-display" style={{ maxHeight: max_height }}>
                {this.roomDatas()}
                <Row type={"flex"} className="room-status-graph">
                    <Col style={{ display: "flex", flex: "1 1", flexDirection: "column" }}>
                        {this.traversalRoomStatusAndGetHtml(roomStatusElement, 0)}
                    </Col>
                </Row>
            </div>
        );
    }
}

/**
 * 控件：显示房态图显示器
 */
@addon('RoomStatusDisplayControl', '显示房态图显示器', '控制显示房态图显示器')
@reactControl(RoomStatusDisplay)
export class RoomStatusDisplayControl extends BaseDisplayerControl {
    /**
     * 最大高度
     */
    max_height?: number | string;
}
