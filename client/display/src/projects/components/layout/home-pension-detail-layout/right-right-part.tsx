import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './right-right-part.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Header, Content } = Layout;
/**
 * 组件：居家养老右部（右部的右半边布局）布局
 */
export class HomePensionDetailLayoutRightRightPart extends React.Component<HomePensionDetailLayoutRightRightPartControl> {
    render() {
        const { childControls, title } = this.props;
        return (
            <Layout className="HomePensionDetailLayoutRightRightPart">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type="flex" style={{ flex: "1 1", flexDirection: "column" }} >
                                    {/* 此处样式不提取，容易查看布局比例 */}
                                    <Row type={"flex"} style={{ flex: "1 1" }}>
                                        <Col span={10} style={{ display: "flex" }}>
                                            {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                        </Col>
                                        <Col span={14} style={{ display: "flex" }}>
                                            {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                        </Col>
                                    </Row>
                                    <Row type={"flex"} style={{ flex: "1 1" }}>
                                        <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                            {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                        </Col>
                                        <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                            {childControls[3] ? childControls[3].createElement!() : <Skeleton />}
                                        </Col>
                                    </Row>
                                    <Row type={"flex"} style={{ flex: "1 1" }}>
                                        <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                            {childControls[4] ? childControls[4].createElement!() : <Skeleton />}
                                        </Col>
                                        <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                            {childControls[5] ? childControls[5].createElement!() : <Skeleton />}
                                        </Col>
                                    </Row>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：居家养老右部（右部的右半边布局）布局
 * 同时也适用于上下布局（上半部分布局又分左右布局）
 */
@addon('HomePensionDetailLayoutRightRightPartControl', '养老院整体统计分析', '用于数据分析图的布局控件')
@reactControl(HomePensionDetailLayoutRightRightPart)
export class HomePensionDetailLayoutRightRightPartControl extends BaseLayoutControl {
    /**
     * 居家养老右部（右部的右半边布局）布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}