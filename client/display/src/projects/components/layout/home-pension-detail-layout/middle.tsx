import React from "react";
import { addon, createObject } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './middle.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { ViewSelectControl } from "src/projects/components/view-select";
import { RemotePath } from "src/projects/router";
const { Header, Content } = Layout;

/**
 * 组件：居家养老中部布局
 */
export class HomePensionDetailLayoutMiddle extends React.Component<HomePensionDetailLayoutMiddleControl> {
    render() {
        const { childControls, title } = this.props;
        const viewSelectControl = createObject(ViewSelectControl, { default_select: RemotePath.homePension });
        return (
            <Layout className="HomePensionDetailLayoutMiddle">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                            <Col span={6} className={"nh-select"} >
                                                {viewSelectControl.createElement!()}
                                            </Col>
                                            <Col span={18}>
                                                {childControls[110] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={24} style={{ display: "flex", flex: "8 8", maxHeight: "100%" }}>
                                        {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：居家养老中部布局
 * 同时也适用于上下布局（上半部分布局又分左右布局）
 */
@addon('HomePensionDetailLayoutMiddleControl', '养老院整体统计分析', '用于数据分析图的布局控件')
@reactControl(HomePensionDetailLayoutMiddle)
export class HomePensionDetailLayoutMiddleControl extends BaseLayoutControl {
    /**
     * 居家养老中部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}