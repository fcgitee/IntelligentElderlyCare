import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './index.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Content } = Layout;

/**
 * 组件：居家养老布局
 */
export class HomePensionDetailLayout extends React.Component<HomePensionDetailLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            <Layout className="HomePensionDetailLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col span={4} className={"left-col"}>
                                        {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                    </Col>
                                    <Col span={10} className={"middle-col"}>
                                        {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                    </Col>
                                    <Col span={5} className={"right-left-part-col"}>
                                        {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                    </Col>
                                    <Col span={5} className={"right-col"}>
                                        {childControls[3] ? childControls[3].createElement!() : <Skeleton />}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：居家养老布局控件
 */
@addon('HomePensionDetailLayoutControl', '居家养老布局', '用于数据分析图的布局控件')
@reactControl(HomePensionDetailLayout)
export class HomePensionDetailLayoutControl extends BaseLayoutControl {
    /**
     * 居家养老布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}