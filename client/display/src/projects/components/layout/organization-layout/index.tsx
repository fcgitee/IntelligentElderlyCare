import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './index.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Content } = Layout;

/**
 * 组件：机构养老布局
 */
export class OrganizationLayout extends React.Component<OrganizationLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            <Layout className="OrganizationLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col className={"left-col"}>
                                        {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                    </Col>
                                    <Col className={"middle-col"}>
                                        {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                    </Col>
                                    <Col className={"right-col"}>
                                        {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：养老院整体统计分析
 */
@addon('OrganizationLayoutControl', '机构养老布局', '用于数据分析图的布局控件')
@reactControl(OrganizationLayout)
export class OrganizationLayoutControl extends BaseLayoutControl {
    /**
     * 机构养老布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}