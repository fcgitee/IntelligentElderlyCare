import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './right.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Header, Content } = Layout;
/**
 * 组件：机构组织右部布局
 */
export class OrganizationLayoutRight extends React.Component<OrganizationLayoutRightControl> {
    render() {
        const { childControls, title } = this.props;
        return (
            <Layout className="OrganizationLayoutRight">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col span={12} style={{ display: "flex" }}>
                                        <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%", overflow: "auto" }}>
                                                        {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%", overflow: "auto" }}>
                                                        {childControls[3] ? childControls[3].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[4] ? childControls[4].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={12} style={{ display: "flex" }}>
                                        <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[5] ? childControls[5].createElement!() : <Skeleton />}
                                            </Col>
                                            <Row className={"rightPartTitle"} type={"flex"} style={{ flex: "2 2", flexDirection: "column" }}>
                                                <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                    <Row type={"flex"} style={{ flex: "1 1" }}>
                                                        <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                            {childControls[6] ? childControls[6].createElement!() : <Skeleton />}
                                                        </Col>
                                                        <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                            {childControls[7] ? childControls[7].createElement!() : <Skeleton />}
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col span={24} style={{ display: "flex", flex: "1 1", flexDirection: "column" }}>
                                                    {childControls[8] ? childControls[8].createElement!() : <Skeleton />}
                                                </Col>
                                            </Row>
                                        </Row>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：机构组织右部布局
 * 同时也适用于上下布局（上半部分布局又分左右布局）
 */
@addon('OrganizationLayoutRightControl', '养老院整体统计分析', '用于数据分析图的布局控件')
@reactControl(OrganizationLayoutRight)
export class OrganizationLayoutRightControl extends BaseLayoutControl {
    /**
     * 机构组织右部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}