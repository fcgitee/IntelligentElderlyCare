import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton, Select } from "antd";
import './index.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Content } = Layout;
const { Option } = Select;

/**
 * 组件：机构养老详情布局
 */
export class OrganizationDetailLayout extends React.Component<OrganizationDetailLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            // <Layout className="OrganizationDetailLayout" style={{ height: "3240px", width: "9600px", backgroundColor: "rgb(0, 8, 59)" }}>
            <Layout className="OrganizationDetailLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col span={4} style={{ display: "flex", flexDirection: "column" }}>
                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                        <Row type={"flex"} style={{ flex: "2 2" }}>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                        <Row type={"flex"} style={{ flex: "2 2" }}>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[3] ? childControls[3].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[4] ? childControls[4].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                        <Row type={"flex"} style={{ flex: "2 2" }}>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[5] ? childControls[5].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[6] ? childControls[6].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={10} style={{ display: "flex" }}>
                                        <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                            <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                <Row style={{ flex: "1 1" }}>
                                                    <Col span={4}>
                                                        <Select defaultValue={1}>
                                                            <Option value={1}>宜养生态系统</Option>
                                                            <Option value={2}>机构养老</Option>
                                                            <Option value={3}>社区养老</Option>
                                                            <Option value={4}>居家养老</Option>
                                                        </Select>
                                                    </Col>
                                                    <Col span={20}>
                                                        {childControls[7] ? childControls[7].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>

                                            </Col>
                                            <Col span={24} style={{ display: "flex", flex: "8 8" }}>
                                                {childControls[8] ? childControls[8].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={10} style={{ display: "flex" }}>
                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                            <Col span={12} style={{ display: "flex" }}>
                                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                                {childControls[9] ? childControls[9].createElement!() : <Skeleton />}
                                                            </Col>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                                {childControls[10] ? childControls[10].createElement!() : <Skeleton />}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%", overflow: "auto" }}>
                                                                {childControls[11] ? childControls[11].createElement!() : <Skeleton />}
                                                            </Col>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                                {childControls[12] ? childControls[12].createElement!() : <Skeleton />}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={12} style={{ display: "flex" }}>
                                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[13] ? childControls[13].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                                {childControls[14] ? childControls[14].createElement!() : <Skeleton />}
                                                            </Col>
                                                            <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                                {childControls[15] ? childControls[15].createElement!() : <Skeleton />}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col span={24} style={{ display: "flex", flex: "1 1", flexDirection: "column" }}>
                                                        {childControls[16] ? childControls[16].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：机构养老详情布局
 */
@addon('OrganizationDetailLayoutControl', '机构养老详情布局', '用于数据分析图的布局控件')
@reactControl(OrganizationDetailLayout)
export class OrganizationDetailLayoutControl extends BaseLayoutControl {
    /**
     * 机构养老详情布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}