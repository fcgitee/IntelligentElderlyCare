/*
 * 版权：Copyright (c) 2018 红网
 * 
 * 创建日期：Thursday November 22nd 2018
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Thursday, 22nd November 2018 1:18:57 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、仅使用在智能报表的报表模块都是iframe报表模块的情况的布局控件
 */
import { Layout } from "antd";
import { addon } from "pao-aop";
import { reactControl } from "pao-aop-client";
import React, { useEffect, useRef } from "react";
import { BaseLayoutControl } from "src/business/report/layout";
import { SwitchReportAction } from "src/business/report/reducer";
import { useStateMachine } from "src/business/report/state";
import { ViewSelect } from "../../view-select";
import ReactDOM from "react-dom";

const { Content } = Layout;

/**
 * 组件：iframe布局控件
 * 描述
 */
export function SelectIframeLayout(props: SelectIframeLayoutControl) {
    const { childControls, layoutStateMachine } = props;
    const iframeElements: { key: string, element: JSX.Element }[] = [];
    const ref = useRef() as any;
    childControls!.forEach(control => {
        const { $reactElement: Component, id, ...props } = control;
        iframeElements.push({
            key: id!,
            element: <Component key={id!} {...props} />
        });
    });
    const { state, dispatch } = useStateMachine(layoutStateMachine!);
    const current = state.redirectReport || iframeElements[0].key,
        currentElement = iframeElements.find(pannl => pannl.key === current) || iframeElements[0];
    useEffect(() => {
        return () => {
            ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(ref.current) as Element);
        };
    });
    return (
        <Layout style={{ background: "inherit", height: "100%" }}>
            <ViewSelect
                default_select={current && current !== "" ? current : undefined}
                doSelect={(value) => dispatch(SwitchReportAction, value)}
            />
            <Content ref={ref} style={{ display: 'flex', background: "inherit" }}>{currentElement!.element || null}</Content>
        </Layout>
    );
}

/**
 * 控件：Antd布局控件控制器
 * 描述
 */
@addon('AntdLayout', 'Antd布局控件', '描述')
@reactControl(SelectIframeLayout)
export class SelectIframeLayoutControl extends BaseLayoutControl {

}