import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './right-left-part.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Header, Content } = Layout;
/**
 * 组件：居家养老右部（右部的左半边布局）布局
 */
export class HomePensionLayoutRightLeftPart extends React.Component<HomePensionLayoutRightLeftPartControl> {
    render() {
        const { childControls, title } = this.props;
        return (
            <Layout className="HomePensionLayoutRightLeftPart">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                            <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={24} style={{ display: "flex", flex: "2 2" }}>
                                        <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                            {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                        </Col>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：居家养老右部（右部的左半边布局）布局
 * 同时也适用于上下布局（上半部分布局又分左右布局）
 */
@addon('HomePensionLayoutRightLeftPartControl', '养老院整体统计分析', '用于数据分析图的布局控件')
@reactControl(HomePensionLayoutRightLeftPart)
export class HomePensionLayoutRightLeftPartControl extends BaseLayoutControl {
    /**
     * 居家养老右部（右部的左半边布局）布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}