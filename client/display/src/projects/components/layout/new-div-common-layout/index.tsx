import { Layout } from "antd";
import { addon } from "pao-aop";
import { BaseReactElementControl, reactControl } from "pao-aop-client";
import React from "react";
import { BaseLayoutControl } from "src/business/report/layout";
import './index.less';

const { Content } = Layout;
/**
 * 组件：大屏通用系统布局
 */
export class NewDivCommonLayout extends React.Component<NewDivCommonLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            <Layout className="NewDivCommonLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <div style={{ display: 'flex', flexDirection: 'row', width: '100%', height: '100%' }}>
                                    <div style={{ width: 1920, height: "100%" }}>
                                        {childControls[1] ? childControls[1].createElement!() : undefined}
                                    </div>
                                    {/* <div style={{width: '1920', display: 'flex', flexDirection: 'column'}}>
                                        <div style={{flex: 'auto'}}>
                                            
                                        </div>       
                                        <div style={{height: '1080px'}}>

                                        </div>
                                        <div style={{width: '1080px'}}>
                                            
                                        </div>
                                    </div> */}
                                    <div style={{ flex: 'auto' }}>
                                        {childControls[2] ? childControls[2].createElement!() : undefined}
                                    </div>
                                    <div style={{ width: '1920px', height: "100%" }}>
                                        {childControls[3] ? childControls[3].createElement!() : undefined}
                                    </div>
                                    <div style={{ width: '1920px', height: "100%" }}>
                                        {childControls[4] ? childControls[4].createElement!() : undefined}
                                    </div>
                                </div>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：养老院整体统计分析
 */
@addon('NewDivCommonLayoutControl', '大屏通用系统布局', '用于数据分析图的布局控件')
@reactControl(NewDivCommonLayout)
export class NewDivCommonLayoutControl extends BaseLayoutControl {
    /**
     * 大屏通用系统布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}