import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import './right-left.less';
import { BaseLayoutControl } from "src/business/report/layout";

/**
 * 组件：大数据右部-左部布局
 */
export class NewDivCommonLayouRightLeft extends React.Component<NewDivCommonLayouRightLeftControl> {
    render() {
        const { childControls } = this.props;

        return (
            <div style={{ width: 1920, display: 'flex', flexDirection: 'column', height: "100%" }}>
                <div style={{ display: "flex", flex: 'auto' }}>
                    {childControls![0] ? childControls![0].createElement!() : undefined}
                </div>
                <div style={{ height: '1080px', display: "flex" }}>
                    {childControls![1] ? childControls![1].createElement!() : undefined}
                </div>
                <div style={{ height: '1080px', display: "flex", flexDirection: "column" }}>
                    <div style={{ display: "flex", flex: "1 1" }}>
                        <div style={{ flex: "1 1" }}>
                            {childControls![2] ? childControls![2].createElement!() : undefined}
                        </div>
                        <div style={{ flex: "1 1" }}>
                            {childControls![3] ? childControls![3].createElement!() : undefined}
                        </div>
                    </div>
                    <div style={{ flex: "1 1" }}>
                        {childControls![4] ? childControls![4].createElement!() : undefined}
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * 控件：大数据右部-左部布局
 */
@addon('NewDivCommonLayouRightLeftControl', '大数据右部-左部布局', '大数据右部-左部布局')
@reactControl(NewDivCommonLayouRightLeft)
export class NewDivCommonLayouRightLeftControl extends BaseLayoutControl {
    /**
     * 大数据右部-左部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}