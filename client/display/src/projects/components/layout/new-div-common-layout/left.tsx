import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import './left.less';
import { BaseLayoutControl } from "src/business/report/layout";
/**
 * 组件：左部通用6格布局
 */
export class NewDivCommonLayoutLeft extends React.Component<NewDivCommonLayoutLeftControl> {
    render() {
        const { childControls } = this.props;

        return (
            <div style={{ width: 1920, display: 'flex', flexDirection: 'column', height: "100%" }}>
                <div style={{ display: "flex", flex: 'auto' }}>
                    {/* paddingTop为给select框让位置 */}
                    <div style={{ paddingTop: 120, display: "flex", flex: "1 1" }} >
                        {/* 三个泡泡 */}
                        <div style={{ flex: "1 1" }}>
                            {childControls![0] ? childControls![0].createElement!() : undefined}
                        </div>
                        <div style={{ flex: "1 1" }}>
                            {childControls![1] ? childControls![1].createElement!() : undefined}
                        </div>
                        <div style={{ flex: "1 1" }}>
                            {childControls![2] ? childControls![2].createElement!() : undefined}
                        </div>
                    </div>
                </div>
                <div style={{ height: '1080px', display: "flex" }}>
                    <div style={{ flex: "1 1" }}>
                        {childControls![3] ? childControls![3].createElement!() : undefined}
                    </div>
                    <div style={{ flex: "1 1" }}>
                        {childControls![4] ? childControls![4].createElement!() : undefined}
                    </div>
                </div>
                <div style={{ height: '1080px' }}>
                    {childControls![5] ? childControls![5].createElement!() : undefined}
                </div>
            </div>
        );
    }
}

/**
 * 控件：左部通用6格布局
 */
@addon('NewDivCommonLayoutLeftControl', '左部通用6格布局', '左部通用6格布局')
@reactControl(NewDivCommonLayoutLeft)
export class NewDivCommonLayoutLeftControl extends BaseLayoutControl {
    /**
     * 左部通用6格布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}