import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Typography } from "antd";
import './middle.less';
import { BaseLayoutControl } from "src/business/report/layout";

/**
 * 组件：div中部布局
 */
export class NewDivCommonLayoutMiddle extends React.Component<NewDivCommonLayoutMiddleControl> {
    render() {
        const { childControls, legend_control } = this.props;

        return (
            <div className="NewDivCommonLayoutMiddle" style={{ display: "flex", flexDirection: "column", height: "100%" }}>
                {/* 标题 */}
                <div style={{ height: 270 }}>
                    <Typography.Title className={"big-title"}>南海区智慧养老综合服务平台</Typography.Title>
                </div>
                <div style={{ display: "flex", flex: "1 1", position: "relative" }}>
                    {legend_control ? legend_control.createElement!() : null}
                    {childControls![0] ? childControls![0].createElement!() : null}
                </div>
            </div>
        );
    }
}

/**
 * 控件：div中部布局
 */
@addon('NewDivCommonLayoutMiddleControl', 'div中部布局', 'div中部布局控件')
@reactControl(NewDivCommonLayoutMiddle)
export class NewDivCommonLayoutMiddleControl extends BaseLayoutControl {
    /**
     * div中部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], public legend_control?: BaseReactElementControl) {
        super(childControls);
    }
}