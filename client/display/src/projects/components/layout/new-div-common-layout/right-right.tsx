import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import './right-left.less';
import { BaseLayoutControl } from "src/business/report/layout";

/**
 * 组件：大数据右部-右部布局
 */
export class NewDivCommonLayouRightRight extends React.Component<NewDivCommonLayouRightRightControl> {
    render() {
        const { childControls } = this.props;

        return (
            <div style={{ width: 1920, display: 'flex', flexDirection: 'column', height: "100%" }}>
                <div style={{ display: "flex", flex: 'auto' }}>
                    {childControls![0] ? childControls![0].createElement!() : undefined}
                </div>
                <div style={{ height: '1080px', display: "flex" }}>
                    {childControls![1] ? childControls![1].createElement!() : undefined}
                </div>
                <div style={{ height: '1080px', display: "flex", flexDirection: "column" }}>
                    <div style={{ display: "flex", flex: "1 1", flexDirection: 'row' }}>
                        <div style={{ width: '960px' }}>
                            {childControls![2] ? childControls![2].createElement!() : undefined}
                        </div>
                        <div style={{ width: '100px', display: "flex", flexDirection: 'row', flex: "1 1 auto" }}>
                            <div style={{ width: '480px' }}>
                                {childControls![3] ? childControls![3].createElement!() : undefined}
                            </div>
                            <div style={{ flex: "1 1" }}>
                                {childControls![4] ? childControls![4].createElement!() : undefined}
                            </div>
                        </div>
                    </div>
                    <div style={{ height: '540px', paddingTop: '5px' }}>
                        {childControls![5] ? childControls![5].createElement!() : undefined}
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * 控件：大数据右部-右部布局
 */
@addon('NewDivCommonLayouRightRightControl', '大数据右部-右部布局', '大数据右部-右部布局')
@reactControl(NewDivCommonLayouRightRight)
export class NewDivCommonLayouRightRightControl extends BaseLayoutControl {
    /**
     * 大数据右部-右部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}