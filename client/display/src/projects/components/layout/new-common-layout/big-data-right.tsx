import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col } from "antd";
import './big-data-right.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { CellLayout } from "../cell-layout";
const { Header, Content } = Layout;

// const Layout =( <div style={{display: 'flex', flexDirection: 'row', width:'100%', height: '100%'}}>

//     <div style={{width: '1920', display: 'flex', flexDirection: 'column'}}>
//     <div style={{flex: 'auto'}}></div>       
//     <div style={{height: '1080px'}}></div>
//     <div style={{width: '1080px'}}></div>
//     </div>
//     <div style={{flex: 'auto'}}></div>
//     <div style={{width: '1920px'}}></div>
//     <div style={{width: '1920px'}}></div>
// </div>);

{/* <div style={{display: 'flex', flexDirection: 'row', width:'100%', height: '100%'}}>
<div style={{width: '1920', display: 'flex', flexDirection: 'column'}}>
    <div style={{flex: 'auto'}}>
        
    </div>       
    <div style={{height: '1080px'}}>

    </div>
    <div style={{width: '1080px'}}>
        
    </div>
</div>
<div style={{flex: 'auto'}}></div>
<div style={{width: '1920px'}}></div>
<div style={{width: '1920px'}}></div>
</div> */}
/**
 * 组件：大数据右部布局
 */
export class BigDataRight extends React.Component<BigDataRightControl> {
    render() {
        const { childControls, title } = this.props;
        const FIRST_ROW_HEIGHT = 900;
        const CELL_WIDTH_LEFT = 1920;
        const CELL_WIDTH_RIGHT = 1920;
        const PADDING_RIGHT_COL = "20px 40px 20px 20px";

        return (
            <Layout className="BigDataRight">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type="flex" style={{ height: "100%", flex: "1 1", flexDirection: "column-reverse" }} >
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} flex_direction_column={true} height={FIRST_ROW_HEIGHT} width={CELL_WIDTH_LEFT} >
                                            <Col span={24} style={{ flex: "1 1" }}>
                                                <Row type={"flex"}>
                                                    <Col span={12}>
                                                        {childControls[4] ? childControls[4].createElement!() : undefined}
                                                    </Col>
                                                    <Col span={12}>
                                                        {childControls[5] ? childControls[5].createElement!() : undefined}
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ flex: "1 1" }}>
                                                {childControls[6] ? childControls[6].createElement!() : undefined}
                                            </Col>
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} height={FIRST_ROW_HEIGHT} width={CELL_WIDTH_RIGHT}>
                                            <Col span={24} style={{ flex: "1 1" }}>
                                                <Row type={"flex"}>
                                                    <Col span={12}>
                                                        {childControls[7] ? childControls[7].createElement!() : undefined}
                                                    </Col>
                                                    <Col span={12}>
                                                        <Row type={"flex"}>
                                                            <Col span={12}>
                                                                {childControls[8] ? childControls[8].createElement!() : undefined}
                                                            </Col>
                                                            <Col span={12}>
                                                                {childControls[9] ? childControls[9].createElement!() : undefined}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ flex: "1 1" }}>
                                                {childControls[6] ? childControls[6].createElement!() : undefined}
                                            </Col>
                                        </CellLayout>
                                    </Col>
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} width={CELL_WIDTH_LEFT}>
                                            {childControls[2] ? childControls[2].createElement!() : undefined}
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} width={CELL_WIDTH_RIGHT}>
                                            {childControls[3] ? childControls[3].createElement!() : undefined}
                                        </CellLayout>
                                    </Col>
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} width={CELL_WIDTH_LEFT}>
                                            {childControls[0] ? childControls[0].createElement!() : undefined}
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} width={CELL_WIDTH_RIGHT}>
                                            {childControls[1] ? childControls[1].createElement!() : undefined}
                                        </CellLayout>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：大数据右部布局
 */
@addon('BigDataRightControl', '大数据右部布局', '大数据右部布局')
@reactControl(BigDataRight)
export class BigDataRightControl extends BaseLayoutControl {
    /**
     * 大数据右部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}