import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col } from "antd";
import './happiness-left-detail.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { CellLayout } from "../cell-layout";
const { Header, Content } = Layout;
/**
 * 组件：左部通用6格布局
 */
export class HappinessLayoutDetailLeft extends React.Component<HappinessLayoutDetailLeftControl> {
    render() {
        const { childControls, title } = this.props;
        const LEFT_HEIGHT = 910;
        const WIDTH = 1920;
        const PADDING = 20;
        const PADDING_LEFT = 40;
        const LAYOUT_PADDING_TOP = 90;
        return (
            <Layout className="HappinessLayoutDetailLeft" >
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null
                            :
                            (
                                <Row type="flex" className={"left-row"} style={{ height: "100%", flex: "1 1", flexDirection: "column", paddingTop: LAYOUT_PADDING_TOP }} >
                                    <CellLayout height={LEFT_HEIGHT} width={WIDTH}>
                                        <Col span={12} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            {childControls[0] ? childControls[0].createElement!() : null}
                                        </Col>
                                        <Col span={12} style={{ padding: PADDING, height: "100%" }}>
                                            {childControls[1] ? childControls[1].createElement!() : null}
                                        </Col>
                                    </CellLayout>
                                    <CellLayout width={WIDTH}>
                                        <Col span={12} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            {childControls[2] ? childControls[2].createElement!() : null}
                                        </Col>
                                        <Col span={12} style={{ padding: PADDING, height: "100%" }}>
                                            {childControls[3] ? childControls[3].createElement!() : null}
                                        </Col>
                                    </CellLayout>
                                    <CellLayout width={WIDTH}>
                                        <Col span={24} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            {childControls[3] ? childControls[3].createElement!() : null}
                                        </Col>
                                    </CellLayout>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：左部通用6格布局
 */
@addon('HappinessLayoutDetailLeftControl', '左部通用6格布局', '左部通用6格布局')
@reactControl(HappinessLayoutDetailLeft)
export class HappinessLayoutDetailLeftControl extends BaseLayoutControl {
    /**
     * 左部通用6格布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}