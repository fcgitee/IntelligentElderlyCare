import { Col, Layout, Row } from "antd";
import { addon } from "pao-aop";
import { BaseReactElementControl, reactControl } from "pao-aop-client";
import React from "react";
import { BaseLayoutControl } from "src/business/report/layout";
import './index.less';

const { Content } = Layout;
/**
 * 组件：大屏通用系统布局
 */
export class NewCommonLayout extends React.Component<NewCommonLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            // <Layout className="NewCommonLayout" style={{ height: "3240px", width: "9600px", backgroundColor: "rgb(0, 8, 59)" }}>
            <Layout className="NewCommonLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col className={"left-col"}>
                                        <Row type="flex" style={{ height: "100%", flexDirection: "column-reverse" }} >
                                            {/* <Col span={24}>
                                                <Row>
                                                    <Col offset={6} span={18}>
                                                        {childControls[0] ? childControls[0].createElement!() : undefined}
                                                    </Col>
                                                </Row>
                                            </Col> */}
                                            <Col span={24} style={{ display: "flex" }}>
                                                {childControls[1] ? childControls[1].createElement!() : undefined}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col className={"middle-col"}>
                                        {childControls[2] ? childControls[2].createElement!() : undefined}
                                    </Col>
                                    <Col className={"right-col"}>
                                        {childControls[3] ? childControls[3].createElement!() : undefined}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：养老院整体统计分析
 */
@addon('NewCommonLayoutControl', '大屏通用系统布局', '用于数据分析图的布局控件')
@reactControl(NewCommonLayout)
export class NewCommonLayoutControl extends BaseLayoutControl {
    /**
     * 大屏通用系统布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}