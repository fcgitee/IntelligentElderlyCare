import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col } from "antd";
import './big-data-left.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { CellLayout } from "../cell-layout";
const { Header, Content } = Layout;
/**
 * 组件：左部通用6格布局
 */
export class BigDataLayoutLeft extends React.Component<BigDataLayoutLeftControl> {
    render() {
        const { childControls, title } = this.props;
        const LEFT_HEIGHT = 900;
        const WIDTH = 1920;
        const PADDING = 20;
        const PADDING_LEFT = 40;

        return (
            <Layout className="BigDataLayoutLeft">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null
                            :
                            (
                                <Row type="flex" style={{ height: "100%", flex: "1 1", flexDirection: "column" }} >
                                    <CellLayout height={LEFT_HEIGHT} width={WIDTH}>
                                        <Col span={24} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            <Row type={"flex"} style={{ height: "100%" }}>
                                                <Col span={8}>
                                                    {childControls[0] ? childControls[0].createElement!() : null}
                                                </Col>
                                                <Col span={8} style={{ paddingTop: "92px" }}>
                                                    {childControls[1] ? childControls[1].createElement!() : null}
                                                </Col>
                                                <Col span={8} style={{ paddingTop: "92px" }}>
                                                    {childControls[2] ? childControls[2].createElement!() : null}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </CellLayout>
                                    <CellLayout width={WIDTH}>
                                        <Col span={12} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            {childControls[3] ? childControls[3].createElement!() : null}
                                        </Col>
                                        <Col span={12} style={{ padding: PADDING, height: "100%" }}>
                                            {childControls[4] ? childControls[4].createElement!() : null}
                                        </Col>
                                    </CellLayout>
                                    <CellLayout width={WIDTH}>
                                        <Col span={24} style={{ padding: PADDING, paddingLeft: PADDING_LEFT, height: "100%" }}>
                                            {childControls[5] ? childControls[5].createElement!() : null}
                                        </Col>
                                    </CellLayout>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：左部通用6格布局
 */
@addon('BigDataLayoutLeftControl', '左部通用6格布局', '左部通用6格布局')
@reactControl(BigDataLayoutLeft)
export class BigDataLayoutLeftControl extends BaseLayoutControl {
    /**
     * 左部通用6格布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}