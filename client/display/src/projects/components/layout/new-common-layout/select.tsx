import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Row } from "antd";
import './left.less';
import { BaseLayoutControl } from "src/business/report/layout";
/**
 * 组件：选择界面布局
 */
export class SelectLayout extends React.Component<SelectLayoutControl> {
    render() {
        const { childControls } = this.props;

        return (
            <div className="SelectLayout">
                {
                    !childControls ?
                        null :
                        (
                            <Row type="flex" >
                                {childControls[0] ? childControls[0].createElement!() : undefined}
                            </Row>
                        )
                }
            </div>
        );
    }
}

/**
 * 控件：选择界面布局
 */
@addon('SelectLayoutControl', '选择界面布局', '选择界面布局')
@reactControl(SelectLayout)
export class SelectLayoutControl extends BaseLayoutControl {
    /**
     * 选择界面布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}