import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Row } from "antd";
import './index.less';
import { BaseLayoutControl } from "src/business/report/layout";
/**
 * 组件：单元格布局
 */
export class CellLayout extends React.Component<CellLayoutControl, { time: string }> {
    render() {
        const { children, height, padding, flex_direction_column, need_padding, width } = this.props;
        const PADDING_NUM = 20;
        return (
            <Row
                className={"CellLayout"}
                style={{
                    flexDirection: flex_direction_column ? "column" : "row",
                    height: height,
                    padding: need_padding ? padding ? padding : PADDING_NUM : undefined,
                    width: width
                }}
                type={"flex"}
            >
                {children}
            </Row>
        );
    }
}

/**
 * 控件：单元格布局
 */
@addon('CellLayoutControl', '单元格布局', '单元格布局')
@reactControl(CellLayout)
export class CellLayoutControl extends BaseLayoutControl {
    flex_direction_column?: boolean;
    height?: number | string;
    width?: number | string;
    need_padding?: boolean;
    padding?: number | string;
    /**
     * 单元格布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}