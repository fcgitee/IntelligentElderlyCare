import { Layout } from "antd";
import { addon } from "pao-aop";
import { reactControl } from "pao-aop-client";
import React, { useEffect, useRef } from "react";
import { BaseLayoutControl } from "src/business/report/layout";
import { SwitchReportAction } from "src/business/report/reducer";
import { useStateMachine } from "src/business/report/state";
import { ViewSelect } from "../../view-select-test";
import { scrollAnimation } from "src/business/util_tool";

const { Content } = Layout;

/**
 * 组件：iframe布局控件
 * 描述
 */
export function SelectIframeLayoutTest(props: SelectIframeLayoutControlTest) {
    const { childControls, layoutStateMachine } = props;
    const iframeElements: { key: string, element: JSX.Element }[] = [];
    const ref = useRef() as any;
    childControls!.forEach(control => {
        const { $reactElement: Component, id, ...props } = control;
        iframeElements.push({
            key: id!,
            element: <Component key={id!} {...props} />
        });
    });
    const { state, dispatch } = useStateMachine(layoutStateMachine!);
    const current = state.redirectReport || iframeElements[0].key,
        currentElement = iframeElements.find(pannl => pannl.key === current) || iframeElements[0];
    useEffect(() => {
        /** 选项页滚动动画 */
        scrollAnimation("list-div");
        scrollAnimation("ant-table-tbody");
        scrollAnimation("#scrollrich");
        return () => {
            // ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(ref.current) as Element);
        };
    });
    return (
        <Layout style={{ background: "inherit", height: "100%" }}>
            <ViewSelect
                default_select={current && current !== "" ? current : undefined}
                doSelect={(value) => dispatch(SwitchReportAction, value)}
            />
            <Content ref={ref} style={{ display: 'flex', background: "inherit" }}>{currentElement!.element || null}</Content>
        </Layout>
    );
}

/**
 * 控件：Antd布局控件控制器
 * 描述
 */
@addon('AntdLayout', 'Antd布局控件', '描述')
@reactControl(SelectIframeLayoutTest)
export class SelectIframeLayoutControlTest extends BaseLayoutControl {
}