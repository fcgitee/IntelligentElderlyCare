import { Col, Layout, Row } from "antd";
import { addon } from "pao-aop";
import { BaseReactElementControl, reactControl } from "pao-aop-client";
import React from "react";
import { BaseLayoutControl } from "src/business/report/layout";
import './index.less';

const { Content } = Layout;
/**
 * 组件：大屏通用系统布局
 */
export class CommonLayout extends React.Component<CommonLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            // <Layout className="CommonLayout" style={{ height: "3240px", width: "9600px", backgroundColor: "rgb(0, 8, 59)" }}>
            <Layout className="CommonLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col className={"left-col"}>
                                        <Row type="flex" style={{ height: "100%", flexDirection: "column-reverse" }} >
                                            {/* <Col span={24}>
                                                <Row>
                                                    <Col offset={6} span={18}>
                                                        {childControls[0] ? childControls[0].createElement!() : undefined}
                                                    </Col>
                                                </Row>
                                            </Col> */}
                                            <Col span={24} style={{ display: "flex" }}>
                                                {childControls[1] ? childControls[1].createElement!() : undefined}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col className={"middle-col"}>
                                        {childControls[2] ? childControls[2].createElement!() : undefined}
                                    </Col>
                                    <Col className={"right-col"}>
                                        {childControls[3] ? childControls[3].createElement!() : undefined}
                                    </Col>
                                </Row>

                                //  <table style={{ width: "100%" }}>
                                //     <tr>
                                //         <td style={{ width: "20%" }}>
                                //             <Row type="flex" style={{ height: "100%", flexDirection: "column" }} >
                                //                 <Col span={24}>
                                //                     <Row>
                                //                         <Col span={6}>
                                //                             {viewSelectControl.createElement!()}
                                //                         </Col>
                                //                         <Col span={18}>
                                //                             {childControls[0] ? childControls[0].createElement!() : undefined}
                                //                         </Col>
                                //                     </Row>
                                //                 </Col>
                                //                 <Col span={24}>
                                //                     {childControls[1] ? childControls[1].createElement!() : undefined}
                                //                 </Col>
                                //             </Row>
                                //         </td>
                                //         <td style={{ width: "40%" }}>
                                //             {childControls[2] ? childControls[2].createElement!() : undefined}
                                //         </td>
                                //         <td style={{ width: "40%" }}>
                                //             {childControls[3] ? childControls[3].createElement!() : undefined}
                                //         </td>
                                //     </tr>
                                // </table>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：养老院整体统计分析
 */
@addon('CommonLayoutControl', '大屏通用系统布局', '用于数据分析图的布局控件')
@reactControl(CommonLayout)
export class CommonLayoutControl extends BaseLayoutControl {
    /**
     * 大屏通用系统布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}