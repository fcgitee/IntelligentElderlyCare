import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Typography } from "antd";
import './middle.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Header, Content } = Layout;

/**
 * 组件：宜养生态系统中部布局
 */
export class CommonLayoutMiddle extends React.Component<CommonLayoutMiddleControl> {
    render() {
        const { childControls, title, legend_control } = this.props;
        return (
            <Layout className="CommonLayoutMiddle">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                    <Typography.Title className={"big-title"}>南海区智慧养老综合服务平台</Typography.Title>
                                    <Col span={24} style={{ display: "flex", flex: "1 1", flexDirection: "column", overflow: "hidden" }}>
                                        {/* 地图放大倍数 */}
                                        <div style={{ display: "flex", flex: "1 1", transform: "scale(1, 1)" }}>
                                            {legend_control ? legend_control.createElement!() : null}
                                            {childControls[0] ? childControls[0].createElement!() : null}
                                        </div>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：宜养生态系统中部布局
 */
@addon('CommonLayoutMiddleControl', '宜养生态系统中部布局', '宜养生态系统中部布局控件')
@reactControl(CommonLayoutMiddle)
export class CommonLayoutMiddleControl extends BaseLayoutControl {
    /**
     * 宜养生态系统中部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], public legend_control?: BaseReactElementControl) {
        super(childControls);
    }
}