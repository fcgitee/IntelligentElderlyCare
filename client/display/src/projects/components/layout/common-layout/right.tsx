import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col } from "antd";
import './right.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { CellLayout } from "../cell-layout";
const { Content } = Layout;
/**
 * 组件：右部通用6格布局
 */
export class CommonLayoutRight extends React.Component<CommonLayoutRightControl> {
    render() {
        const { childControls } = this.props;
        const FIRST_ROW_HEIGHT = 1080;
        const CELL_WIDTH_LEFT = 1920;
        const CELL_WIDTH_RIGHT = 1920;
        const PADDING_RIGHT_COL = "20px 40px 20px 20px";

        return (
            <Layout className="CommonLayoutRight">
                {/* {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                } */}
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type="flex" style={{ height: "100%", flex: "1 1", flexDirection: "column-reverse" }} >
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} width={CELL_WIDTH_LEFT}>
                                            {childControls[4] ? childControls[4].createElement!() : undefined}
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} width={CELL_WIDTH_RIGHT}>
                                            {childControls[5] ? childControls[5].createElement!() : undefined}
                                        </CellLayout>
                                    </Col>
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} width={CELL_WIDTH_LEFT}>
                                            {childControls[2] ? childControls[2].createElement!() : undefined}
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} width={CELL_WIDTH_RIGHT}>
                                            {childControls[3] ? childControls[3].createElement!() : undefined}
                                        </CellLayout>
                                    </Col>
                                    <Col span={24} style={{ display: "flex" }}>
                                        <CellLayout need_padding={true} height={FIRST_ROW_HEIGHT} width={CELL_WIDTH_LEFT} >
                                            {childControls[0] ? childControls[0].createElement!() : undefined}
                                        </CellLayout>
                                        <CellLayout need_padding={true} padding={PADDING_RIGHT_COL} height={FIRST_ROW_HEIGHT} width={CELL_WIDTH_RIGHT}>
                                            {childControls[1] ? childControls[1].createElement!() : undefined}
                                        </CellLayout>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：右部通用6格布局
 */
@addon('CommonLayoutRightControl', '右部通用6格布局', '右部通用6格布局')
@reactControl(CommonLayoutRight)
export class CommonLayoutRightControl extends BaseLayoutControl {
    /**
     * 右部通用6格布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[], title?: string) {
        super(childControls);
    }
}