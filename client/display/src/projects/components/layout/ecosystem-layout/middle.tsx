import React from "react";
import { addon, createObject } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './middle.less';
import { BaseLayoutControl } from "src/business/report/layout";
import { ViewSelectControl } from "src/projects/components/view-select";
import { RemotePath } from "src/projects/router";
const { Header, Content } = Layout;

/**
 * 组件：宜养生态系统中部布局
 */
export class EcosystemLayoutMiddle extends React.Component<EcosystemLayoutMiddleControl> {
    render() {
        const viewSelectControl = createObject(ViewSelectControl, { default_select: RemotePath.ecosystem });
        const { childControls, title, legen_control, introduce_list_control } = this.props;
        return (
            <Layout className="EcosystemLayoutMiddle">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                        <Row type={"flex"} style={{ flex: "1 1" }}>
                                            <Col span={6} className={"nh-select"} >
                                                {viewSelectControl.createElement!()}
                                                {/* <ViewSelect /> */}
                                            </Col>
                                            <Col span={18}>
                                                {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={24} style={{ display: "flex", flex: "8 8" }}>
                                        {legen_control ? legen_control.createElement!() : undefined}
                                        {introduce_list_control ? introduce_list_control.createElement!() : undefined}
                                        {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：宜养生态系统中部布局
 */
@addon('EcosystemLayoutMiddleControl', '宜养生态系统中部布局', '宜养生态系统中部布局控件')
@reactControl(EcosystemLayoutMiddle)
export class EcosystemLayoutMiddleControl extends BaseLayoutControl {
    /** 图例控制器 */
    legen_control?: BaseReactElementControl;

    /** 介绍列表控制器 */
    introduce_list_control?: BaseReactElementControl;

    /**
     * 宜养生态系统中部布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}