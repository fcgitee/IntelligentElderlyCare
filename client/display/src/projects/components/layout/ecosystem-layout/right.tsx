import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import './right.less';
import { BaseLayoutControl } from "src/business/report/layout";
const { Header, Content } = Layout;
/**
 * 组件：平均住院时长、长者类型、护理级别布局
 */
export class EcosystemLayoutRight extends React.Component<EcosystemLayoutRightControl> {
    render() {
        const { childControls, title } = this.props;
        return (
            <Layout className="EcosystemLayoutRight">
                {
                    !title ?
                        null :
                        <Header >
                            <span>{title!}</span>
                        </Header>
                }
                <Content>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col span={12} style={{ display: "flex" }}>
                                        <Row style={{ flex: "1 1" }}>
                                            <Col span={24} style={{ display: "flex", height: 1040 }}>
                                                {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                            </Col>
                                            <Col span={24} style={{ display: "flex", height: 1060 }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[1] ? childControls[1].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                        {childControls[2] ? childControls[2].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ display: "flex", height: 1040 }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[3] ? childControls[3].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                        {childControls[4] ? childControls[4].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={12} style={{ display: "flex" }}>
                                        <Row style={{ flex: "1 1", flexDirection: "column" }}>
                                            <Col span={24} style={{ display: "flex", height: 1040 }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={8}>
                                                        {childControls[5] ? childControls[5].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={8}>
                                                        {childControls[6] ? childControls[6].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={8} style={{ display: "flex" }}>
                                                        <Row type={"flex"} style={{ flex: "1 1", flexDirection: "column" }}>
                                                            <Col style={{ flex: "1 1" }}>
                                                                {childControls[7] ? childControls[7].createElement!() : <Skeleton />}
                                                            </Col>
                                                            <Col style={{ flex: "2 2" }}>
                                                                {childControls[8] ? childControls[8].createElement!() : <Skeleton />}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ display: "flex", height: 1060 }}>
                                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1" }}>
                                                        {childControls[9] ? childControls[9].createElement!() : <Skeleton />}
                                                    </Col>
                                                    <Col span={12} style={{ display: "flex", flex: "1 1", maxHeight: "100%" }}>
                                                        {childControls[10] ? childControls[10].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24} style={{ display: "flex", height: 1040, flexDirection: "column" }}>
                                                <Row type="flex" style={{ flex: "1 1", flexDirection: "column" }} >
                                                    <Col span={24} style={{ display: "flex", flex: "1 1" }}>
                                                        <Row type="flex" style={{ flex: "1 1" }}>
                                                            <Col span={12} style={{ display: 'flex', flex: "1 1" }}>
                                                                {childControls[11] ? childControls[11].createElement!() : <Skeleton />}
                                                            </Col>
                                                            <Col span={12} style={{ display: 'flex', flex: "1 1" }}>
                                                                <Row type="flex" style={{ flex: "1 1", flexDirection: "column" }} >
                                                                    <Col span={24} style={{ display: 'flex', flex: "1 1" }}>
                                                                        {childControls[12] ? childControls[12].createElement!() : <Skeleton />}
                                                                    </Col>
                                                                    <Row type="flex" style={{ flex: "2 2" }}>
                                                                        <Col span={12} style={{ display: 'flex', flex: "1 1" }}>
                                                                            {childControls[13] ? childControls[13].createElement!() : <Skeleton />}
                                                                        </Col>
                                                                        <Col span={12} style={{ display: 'flex', flex: "1 1" }}>
                                                                            {childControls[14] ? childControls[14].createElement!() : <Skeleton />}
                                                                        </Col>
                                                                    </Row>
                                                                </Row>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col span={24} style={{ display: 'flex', flex: "1 1" }}>
                                                        {childControls[15] ? childControls[15].createElement!() : <Skeleton />}
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：平均住院时长、长者类型、护理级别布局
 * 同时也适用于上下布局（上半部分布局又分左右布局）
 */
@addon('EcosystemLayoutRightControl', '养老院整体统计分析', '用于数据分析图的布局控件')
@reactControl(EcosystemLayoutRight)
export class EcosystemLayoutRightControl extends BaseLayoutControl {
    /**
     * 平均住院时长、长者类型、护理级别布局
     * @param childControls 子控件列表
     */
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}