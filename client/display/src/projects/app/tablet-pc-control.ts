import { addon, Nullable } from "pao-aop";

/**
 * 插件：表格数据获取服务
 * 表格数据获取服务
 */
@addon('TabletControlService', '表格数据获取服务', '表格数据获取服务')
export class TabletControlService {
    /** 获取养老机构  */
    get_welfare_center_list?(): Nullable<any> {
        return undefined;
    }
    /** 获取社区幸福院  */
    get_community_home_list?(): Nullable<any> {
        return undefined;
    }
    /** 获取服务商  */
    get_service_provider_list?(): Nullable<any> {
        return undefined;
    }
    /** 获取服务人员  */
    get_service_personal_list?(): Nullable<any> {
        return undefined;
    }
    /** 获取长者统计  */
    get_elder_list?(): Nullable<any> {
        return undefined;
    }
    /** 获取综合统计  */
    get_comprehensive_list?(): Nullable<any> {
        return undefined;
    }
}