import { getAddonTypeName, getObject, IAnyType, IType } from "pao-aop";
import { AjaxJsonRpcFactory } from "pao-aop-client";
import { remote } from "../remote";
import { IDisplayControlService } from "src/business/models/control";

/**
 * 应用服务工具类
 */
export class AppServiceUtility {
    /**
     * 服务对象
     * @param service_interface 接口
     * @type T 接口类型
     */
    static service<T>(service_interface: IType<T> | Object): T {
        if (typeof service_interface === 'function') {
            let destFunc = service_interface as IAnyType;
            service_interface = new destFunc();
        }
        let service_name = getAddonTypeName(service_interface as any);
        // console.log(service_name);
        let factory = new AjaxJsonRpcFactory(service_interface, remote.url, service_name);
        let service = getObject(factory);
        return service as T;
    }
    /** 大屏控制服务 */
    static get displayControlService(): IDisplayControlService {
        return AppServiceUtility.service(IDisplayControlService);
    }

}