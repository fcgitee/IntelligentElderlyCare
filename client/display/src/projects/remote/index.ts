
/** 远程调用 */
export const remote = {
    /** mock */
    mock: "http://localhost:3000/remoteCall",
    /** 服务器 */
    // url: "http://183.235.223.56/remoteCall"
    /** 本地 */
    url: "http://localhost:3100/remoteCall"
    /** docker */
    // url: "http://192.168.1.10/remoteCall"
};