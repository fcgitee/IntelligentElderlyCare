import { Button, Input, Divider, Row, Col } from "antd";
import { addon, Ref } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl, ReactView, ReactViewControl, ReactViewState } from "pao-aop-client";
import React, { createContext } from "react";
import { IHomeService } from "src/models/home";
import { ActivityList } from "src/projects/components/activity-list";

export class HomeContext {
    data?: { title: string; };
    onClick?: (title: string) => void;
}

export const HomeWrapper = createContext<HomeContext>({});

/**
 * 组件：测试状态
 */
export interface HomeConsumerState extends BaseReactElementState {
    title?: string;
    sub_title?: string;
}

/**
 * 组件：测试
 * 描述
 */
export class HomeConsumer extends BaseReactElement<HomeConsumerControl, HomeConsumerState> {
    onTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value: title } = event.target;
        this.setState({ title });
    }
    onSubtitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value: sub_title } = event.target;
        this.setState({ sub_title });
    }
    render() {
        return (
            <HomeWrapper.Consumer>
                {
                    (context) => (
                        <div>
                            <p style={{ color: 'wheat' }}>{context.data!.title}</p>
                            <Input onChange={this.onTitleChange} defaultValue={context.data!.title} />
                            <br />
                            <Button type={"primary"} onClick={() => context.onClick!(this.state.title!)}>修改父组件</Button>
                        </div>
                    )
                }
            </HomeWrapper.Consumer>
        );
    }
}

/**
 * 控件：测试控制器
 * 描述
 */
@addon('HomeConsumer', '测试', '描述')
@reactControl(HomeConsumer)
export class HomeConsumerControl extends BaseReactElementControl {
}

/** 状态：主页 */
export interface HomeViewState extends ReactViewState {
    title: string;
}

/** 组件：主页 */
export class HomeView extends ReactView<HomeViewControl, HomeViewState> {
    constructor(props: HomeViewControl) {
        super(props);
        this.state = {
            title: "默认标题",
        };
    }

    onClick = (title: string) => {
        this.setState({ title });
    }

    onTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value: title } = event.target;
        this.setState({ title });
    }
    render() {
        return (
            <HomeWrapper.Provider
                value={{
                    data: { ...this.state },
                    onClick: this.onClick
                }}
            >
                <Input onChange={this.onTitleChange} value={this.state.title} />
                <Divider />
                <HomeConsumer />
                <ActivityList />
            </HomeWrapper.Provider>);
    }
}

/**
 * 控件：主页控制器
 * @description 主页
 */
@addon('HomeView', '主页', '主页')
@reactControl(HomeView, true)
export class HomeViewControl extends ReactViewControl {
    /** 主页服务 */
    public homeService_Fac?: Ref<IHomeService>;
}

/**
 * 组件：名称状态
 */
export interface MutliHomeViewState extends ReactViewState {
}

/**
 * 组件：名称
 * 描述
 */
export class MutliHomeView extends ReactView<MutliHomeViewControl, MutliHomeViewState> {
    render() {
        return (
            <Row gutter={24}>
                <Col span={12}>
                    <HomeView />
                </Col>
                <Col span={12}>
                    <HomeView />
                </Col>
            </Row>
        );
    }
}

/**
 * 控件：名称控制器
 * 描述
 */
@addon('MutliHomeView', '名称', '描述')
@reactControl(MutliHomeView)
export class MutliHomeViewControl extends ReactViewControl {

}