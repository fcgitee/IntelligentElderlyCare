import { IFrameReportModuleControl, SmartReportControl } from 'src/business/report/smart';
import { createObject } from 'pao-aop';
import { AjaxJsonRpcFactory } from 'pao-aop-client';
import { IDisplayControlService } from 'src/business/models/control';
import { SelectIframeLayoutControl } from 'src/projects/components/layout/select-iframe-layout';
import { remote } from 'src/projects/remote';
import { RemotePath } from 'src/projects/router';

const selectiframeLayout = createObject(SelectIframeLayoutControl);

const iframeWelfareCentreModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.welfareCentre, id: 'irWelfaceCentre' });
const iframeCommunityHomeModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.communityHome, id: 'irCommunityHome' });
const iframeServiceProviderModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.serviceProvider, id: 'irServiceProvider' });
const iframeServicePersonalModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.servicePersonal, id: 'irServicePersonal' });
const iframeElderModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.elder, id: 'irElder' });
const iframeComprehensiveModule = createObject(IFrameReportModuleControl, { report_uri: RemotePath.comprehensive, id: 'irComprehensive' });

export const selectIframeReport = createObject(
    SmartReportControl,
    {
        modules: [iframeWelfareCentreModule, iframeCommunityHomeModule, iframeServiceProviderModule, iframeServicePersonalModule, iframeElderModule, iframeComprehensiveModule],
        layout: selectiframeLayout,
        iframeable: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);